#include <unistd.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <vector>
#include <string>

#include <ACG.h>
#include <Normal.h>

#include <HistogramCache.h>

int
main(int argc, char** argv)
{
  int number = 2000;
  int N = 20;
  int iseed = 11;
  
  int c;
  while (1) {
    c = getopt (argc, argv, "N:n:i:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'N':
	N = atoi(optarg); break;
      case 'n':
	number = atoi(optarg); break;
      case 's':
	iseed = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " <vector of components weights>\n";
	msg += "\t-N i\t\tnumber of divisions\n";
	msg += "\t-n i\t\tnumber of sample points\n";
	msg += "\t-s i\t\trandom number seed\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  double dz = 10.0/N;
  vector<double> lo(2), hi(2), wid(2), z(2);
  for (int i=0; i<2; i++) {
    lo[i] = -5.0;
    hi[i] =  5.0-0.0001;
    wid[i] = dz;
  }
  
  HistogramCache histo(lo, hi, wid);

  int dim = (int)( (hi[0]-lo[0])/dz) + 1;
  typedef vector<unsigned int> ivector;
  vector<ivector> thisto(dim);
  for (int i=0; i<dim; i++) thisto[i] = vector<unsigned int>(dim, 0);

  ACG gen(iseed, 20);
  Normal normal(0.0, 1.0, &gen);

  for (int n=0; n<number; n++) {
    z[0] = normal();
    z[1] = normal();

    histo.addValue(z);

    if (z[0]>lo[0] && z[0]<hi[0] && z[1]>lo[1] && z[1]<hi[1]) {
      int i1 = (int)( (z[0] - lo[0])/dz );
      int i2 = (int)( (z[1] - lo[1])/dz );
      
      thisto[i1][i2]++;
    }
  }

				// Check

  vector<unsigned short> k(2);
  double t1, t2, t3;

  for (int i=0; i<N; i++) {
    z[0] = lo[0] + dz*(0.5+i);
    for (int j=0; j<N; j++) {
      z[1] = lo[1] + dz*(0.5+j);

      if ((t1=thisto[i][j]) != (t2=histo.getValue(z))) {
	k[0] = i;
	k[1] = j;
	t3 = histo.getValue(k);
	cout << "oops\n";
      }
    }
  }

  histo.printStats();
  histo.printList();

  return 0;
}
