#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>

using namespace std;

#include <Populations.h>
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Populations)


using namespace BIE;

// --------------------------------------------------------
// a multiplication hash function
// --------------------------------------------------------
size_t popkeyHash(PopKey x)
{
  /*
    In Knuth's "The Art of Computer Programming", section 6.4, a
    multiplicative hashing scheme is introduced as a way to write hash
    function. The key is multiplied by the golden ratio of 2^32
    (2654435761) to produce a hash result.

    Since 2654435761 and 2^32 has no common factors in common, the
    multiplication produces a complete mapping of the key to hash
    result with no overlap. This method works pretty well if the keys
    have small values. Bad hash results are produced if the keys vary
    in the upper bits. As is true in all multiplications, variations
    of upper digits do not influence the lower digits of the
    multiplication result.  
  */

  const int q = 128;
  unsigned long i = 0;
  for (int k=0; k<x.get_dim(); k++)
    i = i*q + x.get_I(k);
  i += x.get_Z();

  return i*0x9e3779b1u;
}


Populations::Populations(string &dir, clivectori* marge)
{
  double value;
  string word;

  datadir = dir;
				// Read parameters out of index file

  string contents = datadir + "/.contents";
  ifstream init(contents.c_str());
  if (!init) {
    throw FileOpenException(contents, errno, __FILE__, __LINE__);
  }
				// Read in number of metallicties and 
				// string suffixes
  init >> nmetal;
  for (int i=0; i<nmetal; i++) {
    init >> value;
    metal.push_back(value);
    init >> word;
    smetal.push_back(word);
  }

				// Read in number of ages
  init >> nages;		// and age grid data
  for (int i=0; i<nages; i++) {
    init >> value;
    ages.push_back(value);
    age_boundary.push_back(value-0.15);
    init >> word;
    sages.push_back(word);
  }
  age_boundary.push_back(value+0.15);

				// Read in number of fluxes
  init >> nflux;
				// Marginalize input stream?
  nMarge=0;
  use_flux = vector<bool>(nflux, true);
  if (marge) {

    if ((int)(*marge)().size() != nflux) {
      string msg("Populations: marginalization vector must match flux dimension");
      throw DimNotMatchException(msg, __FILE__, __LINE__);
    }

    for (int i=0; i<nflux; i++) {
      if ((*marge)()[i]!=0) {
	use_flux[i] = false;
	nMarge++;
      }
    }
    
  }

  nflux0 = nflux;
  nflux -= nMarge;

				// Read in binning
  for (int i=0; i<nflux0; i++) {
    init >> value;
    if (use_flux[i]) minmag.push_back(value);
    init >> value;
    if (use_flux[i]) maxmag.push_back(value);
    init >> value;
    if (use_flux[i]) wmag.push_back(value);
  }

  for (int i=0; i<nflux0; i++)
    if (use_flux[i]) 
      nmag.push_back((int) ( (maxmag[i] - minmag[i] + 1.001*wmag[i])/wmag[i] ));

  flux = vector<double>(nflux);
  ii = vector<int>(nflux);
  iflx = vector<int>(nflux);
  iflxmin = vector<int>(nflux);
  iflxmax = vector<int>(nflux);
  iflxoff = vector<int>(nflux);
  flxwght = vector<double>(nflux);
  f2 = vector<dvector>(nflux);
  i2 = vector<ivector>(nflux);
  for (int i=0; i<nflux; i++) {
    f2[i] = vector<double>(2);
    i2[i] = vector<int>(2);
  }

  read_data();

}

void Populations::read_data(void)
{
  string thead("t");
  string zhead("z");
  const int linesize = 1024;
  char line[linesize];
  double z, value;
  int zz;
  vector<double>::iterator pos;

				// Need caches for each time and variance
  
  Hash = vector<HMapKey>(nages);
  for (int i=0; i<nages; i++) {

    for (int j=0; j<nmetal; j++) {
      string file = datadir + "/" + thead + sages[i] + zhead + smetal[j];
      ifstream infile(file.c_str());
      if (!infile) continue;

      while (infile) {
	infile.getline(line, linesize);
	if (strlen(line)==0) continue;
	istringstream istr(line);
	if (index(line,'#')) continue;
	if (istr) {
	  istr >> z;
	  for (int k=0, ik=0; k<nflux0; k++) {
	    istr >> value;
	    if (use_flux[k]) flux[ik++] = value;
	  }
	  istr >> value;
	} 
	if (istr && value!=0) {	// Only save non-zero values

	  if (z <= metal[0]) zz = 0;
	  else if (z >= metal[nmetal-1]+1.0e-8) zz = nmetal - 1;
	  else {
	    pos = lower_bound(metal.begin(), metal.end(), z+1.0e-8);
	    zz = pos - metal.begin() - 1;
	  }
	  
	  
	  for (int k=0, ik=0; k<nflux0; k++)
	    if (use_flux[k]) {
	      iflx[ik] = 
		(int)( (flux[ik] - minmag[ik] + 0.5*wmag[ik])/wmag[ik] );
	      ik++;
	    }
	  
	  key.reset(zz, iflx);
	  Hash[i][key] = value;
	}
      }

    }

  }
}


double Populations::GetValue(vector<double>& Flux, double age, double z)
{
  int indx, zz;
  vector<double>::iterator pos;

  if (age < age_boundary[0]) indx = 0;
  else if (age >= age_boundary[nages]) indx = nages-1;
  else {
    pos = lower_bound(age_boundary.begin(), age_boundary.end(), age);
    indx = pos - age_boundary.begin() - 1;
  }

  if (z <= metal[0]) zz = 0;
  else if (z >= metal[nmetal-1]+1.0e-8) zz = nmetal - 1;
  else {
    pos = lower_bound(metal.begin(), metal.end(), z+1.0e-8);
    zz = pos - metal.begin() - 1;
  }

  if ( fabs(ages[indx]-age)>1.0e-6 || indx>=nages) {
    cout << "Could not find requested age\n";
  }
  for (int k=0; k<nflux; k++)
    iflx[k] = (int)( (Flux[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );
  
  key.reset(zz, iflx);
  HMapKey::iterator p = Hash[indx].find(key);
  if (p != Hash[indx].end()) return p->second;
  else return 0.0;
}


double Populations::Interpolate(vector<double>& Flux, double age, double z)
{
  int indx, zz;
  vector<double>::iterator pos;

  if (age < age_boundary[0]) indx = 0;
  else if (age >= age_boundary[nages]) indx = nages-1;
  else {
    pos = lower_bound(age_boundary.begin(), age_boundary.end(), age);
    indx = pos - age_boundary.begin() - 1;
  }

  if (z <= metal[0]) zz = 0;
  else if (z >= metal[nmetal-1]+1.0e-8) zz = nmetal - 1;
  else {
    pos = lower_bound(metal.begin(), metal.end(), z+1.0e-8);
    zz = pos - metal.begin() - 1;
  }

  if ( fabs(ages[indx]-age)>1.0e-6 || indx>=nages) {
    cout << "Could not find requested age\n";
  }

  for (int k=0; k<nflux; k++) {

    iflx[k] = (int)( (Flux[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );

    if (iflx[k] < nmag[k]-1) {
      i2[k][0] = iflx[k];
      i2[k][1] = iflx[k]+1;
    } else if (iflx[k]<0) {
      i2[k][0] = 0;
      i2[k][1] = 1;
    } else {
      i2[k][0] = nmag[k]-2;
      i2[k][1] = nmag[k]-1;
    }

    f2[k][0] = (minmag[k] + wmag[k]*i2[k][1] - Flux[k])/wmag[k];
    f2[k][1] = (Flux[k] - minmag[k] - wmag[k]*i2[k][0])/wmag[k];

  }

  HMapKey::iterator p;
  double value, ans = 0.0;

  int ndim = 1;
  for (int k=0; k<nflux; k++) ndim *= 2;

  unsigned mask=1;
  for (int i=0; i<ndim; i++) {
    
    // Magic here to use each bit to represent a dimension
    for (int k=0; k<nflux; k++) iflx[k] = i2[k][(ii[k]=(i>>k) & mask)];
    key.reset(zz, iflx);
    p = Hash[indx].find(key);

    if (p != Hash[indx].end()) {
      value = p->second;
      for (int k=0; k<nflux; k++) value *= f2[k][ii[k]];
      ans += value;
    }
    
  }

  return ans;
}


double Populations::Bin(vector<double>& fmin, vector<double>& fmax,
			double age, double z)
{
  int indx, zz;
  vector<double>::iterator pos;

  if (age < age_boundary[0]) indx = 0;
  else if (age >= age_boundary[nages]) indx = nages-1;
  else {
    pos = lower_bound(age_boundary.begin(), age_boundary.end(), age);
    indx = pos - age_boundary.begin() - 1;
  }

  if (z <= metal[0]) zz = 0;
  else if (z >= metal[nmetal-1]+1.0e-8) zz = nmetal - 1;
  else {
    pos = lower_bound(metal.begin(), metal.end(), z+1.0e-8);
    zz = pos - metal.begin() - 1;
  }

  if ( fabs(ages[indx]-age)>1.0e-6 || indx>=nages) {
    cout << "Could not find requested age\n";
  }

  int ndim = 1;
  for (int k=0; k<nflux; k++) {

    iflxmin[k] = (int)( (fmin[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );
    iflxmax[k] = (int)( (fmax[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );
    iflxoff[k] = ndim;

    ndim *= iflxmax[k] - iflxmin[k] + 1;

  }

  HMapKey::iterator p;
  double value, ans = 0.0, fbmin, fbmax;

  int icnt;
  for (int i=0; i<ndim; i++) {
    
    // Magic here to get offset for each dimension
    icnt = i;
    for (int k=nflux-1; k>=0; k--) {
      iflx[k] = icnt/iflxoff[k];
      icnt -= iflx[k]*iflxoff[k];
    }

    for (int k=0; k<nflux; k++) {
      iflx[k] += iflxmin[k];
      fbmin = max<double>(minmag[k] + wmag[k]*((double)iflx[k] - 0.5), 
			  minmag[k] - 0.5*wmag[k]);
      fbmax = min<double>(fbmin + wmag[k], maxmag[k]+0.5*wmag[k]);
      flxwght[k] = 
	(min<double>(fmax[k], fbmax) - max<double>(fmin[k], fbmin)) / wmag[k];
    }

    key.reset(zz, iflx);
    p = Hash[indx].find(key);

    if (p != Hash[indx].end()) {
      value = p->second;
      for (int k=0; k<nflux; k++) value *= flxwght[k];
      ans += value;
    }
    
  }
  
  return ans;
}


double Populations::GetValue(vector<int>& Iflx, int indx, int zz)
{
  if (indx<0 || indx>=nages) return 0.0;

  for (int k=0; k<nflux; k++) {
    if (Iflx[k]<0 || Iflx[k]>=nmag[k]) return 0.0;
  }

  if (zz<0 || zz>=nmetal) return 0.0;


  key.reset(zz, Iflx);
  HMapKey::iterator p = Hash[indx].find(key);
  if (p != Hash[indx].end()) return p->second;
  else return 0.0;
}

void Populations::VerifyDouble(void)
{
  string thead("t");
  string zhead("z");
  const int linesize = 1024;
  char line[linesize];
  double z, value, tvalue;
  int wrong = 0;
  bool ok = true;

  for (int i=0; i<nages; i++) {

    for (int j=0; j<nmetal; j++) {
      string file = datadir + "/" + thead + sages[i] + zhead + smetal[j];
      ifstream infile(file.c_str());
      if (!infile) continue;

      while (infile) {
	infile.getline(line, linesize);
	if (strlen(line)==0) continue;
	istringstream istr(line);
	if (index(line,'#')) continue;
	if (istr) {
	  istr >> z;
	  for (int k=0, ik=0; k<nflux0; k++) {
	    istr >> value;
	    if (use_flux[k]) flux[ik++] = value;
	  }
	  istr >> value;
	} 
	
	// tvalue = GetValue(flux, ages[i], z);
	tvalue = Interpolate(flux, ages[i], z);

	if (fabs(tvalue - value) > 1.0e-6) {
	  ok = false;
	  wrong++;
	}
      }
    }
    
  }

  if (ok)
    cout << "Cached data agrees with original tables, good!\n";
  else
    cout << "Ooops, " << wrong << " values are incorrect\n";
}


void Populations::VerifyInt(void)
{
  string thead("t");
  string zhead("z");
  const int linesize = 1024;
  char line[linesize];
  double z, value, tvalue;
  int wrong = 0;
  bool ok = true;

  for (int i=0; i<nages; i++) {

    for (int j=0; j<nmetal; j++) {
      string file = datadir + "/" + thead + sages[i] + zhead + smetal[j];
      ifstream infile(file.c_str());
      if (!infile) continue;

      while (infile) {
	infile.getline(line, linesize);
	if (strlen(line)==0) continue;
	istringstream istr(line);
	if (index(line,'#')) continue;
	if (istr) {
	  istr >> z;
	  for (int k=0, ik=0; k<nflux0; k++) {
	    istr >> value;
	    if (use_flux[k]) {
	      iflx[ik] = (int)( (value - minmag[ik] + 0.5*wmag[ik])/wmag[ik] );
	      ik++;
	    }
	  }
	  istr >> value;
	} 
	
	tvalue = GetValue(iflx, i, j);

	if (fabs(tvalue - value) > 1.0e-6) {
	  ok = false;
	  wrong++;
	}
      }
    }
    
  }

  if (ok)
    cout << "Cached data agrees with original tables, good!\n";
  else
    cout << "Ooops, " << wrong << " values are incorrect\n";
}


