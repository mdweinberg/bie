#include <iostream>
#include <utility>
#include <unordered_map>
#include <cmath>

typedef std::pair<double, double> coordPair;

union coordUnion {
  float z;
  unsigned int  i;
  unsigned char c[4];
};

size_t coordHash(coordPair P) {
  coordUnion u1, u2, u;
  u1.z = P.first;
  u2.z = P.second;

  u.c[0] = u1.c[1];
  u.c[1] = u2.c[1];
  u.c[2] = u1.c[2];
  u.c[3] = u2.c[2];
  
  return u.i*0x9e3779b1u;
}

namespace std
{
  template<>
  struct hash<coordPair> 
  {
    size_t operator()(coordPair s) const 
    { 
      return coordHash(s); 
    }
  };
}
  
struct eqcoord
{
  bool operator()(coordPair c1, coordPair c2) const
  {
    return ( std::fabs(c1.first  - c2.first ) < 1.0e-6 &&
	     std::fabs(c1.second - c2.second) < 1.0e-6   );
  }
};

typedef std::unordered_map<coordPair, float, std::hash<coordPair>, eqcoord> mmap;

int main()
{
  mmap values;
  coordPair P;
    

  P.first = 1.0;
  P.second = 2.0;
  values[P] = 11.0;

  P.first = 3.0;
  P.second = 4.0;
  values[P] = 12.0;
  
  mmap::iterator it;

  std::cout << "Have " << values.size() << " values" << std::endl;

  for (auto v : values) {
    
    std::cout << "[" << v.first.first
	      << ", " << v.first.second
	      << "] = " << v.second
	      << std::endl;

  }

  std::cout << "Get [" << P.first << ", " << P.second << "]: ";
  if ( (it=values.find(P)) == values.end() )
    std::cout << "not in map\n";
  else
    std::cout << it->second << std::endl;

  P.first = 13.0;
  P.second = 14.0;

  std::cout << "Get [" << P.first << ", " << P.second << "]: ";
  if ( (it=values.find(P)) == values.end() )
    std::cout << "not in map\n";
  else
    std::cout << it->second << std::endl;

  return 0;
}

