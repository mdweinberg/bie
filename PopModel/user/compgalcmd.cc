// This may look like C code, but it is really -*- C++ -*-

#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;


#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <BasicType.h>
#define IS_MAIN

#include <HistogramND.h>
#include <CMDModelCache.h>
#include <Timer.h>

using namespace BIE;

/**
   @name compgalcmd: main
   Compare data with output of CMDModelCache
*/
int
main(int argc, char** argv)
{
  const double onedeg = M_PI/180.0;
  Timer timer(true);

				// Line of sight
  double L1 = 30.0;
  double L2 = 40.0;
  double B3 = -2.0;
  double B4 =  2.0;
  int nptl = 1;
  int nptb = 1;
				// Minimum line-of-sight distance
  double RMIN = 0.010;
				// Maximum line-of-sight distance
  double RMAX = 100.0;
				// Integration knots
  int NUM = 200;

  double A1 = 2.5;
  double Z1 = 150.0;
  double EFAC = 1.0;
  bool RING = false;
  double zerotol = 1.0e-12;
				// Default init file
  string extfile = "extinction.dat";
				// Default init file
  string initfile = "init.dat.cmd";
				// Output suffix
  string outfile = "tmp.tmp";
				// Data directory
  string DataDir = "/data/weinberg/3Mtab";
				// CMD file
  string CMDFile = "lmc.cmd";
				// Particle file
  string PartFile = "jkk.data";

				// Log abscissa
  bool RLOG = false;


  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "1:2:3:4:5:6:r:R:i:o:d:e:m:p:n:w:A:Z:E:F:z:g:rxh");
    if (c == -1) break;
     
    switch (c)
      {
      case '1': L1 = atof(optarg); break;
      case '2': L2 = atof(optarg); break;
      case '3': B3 = atof(optarg); break;
      case '4': B4 = atof(optarg); break;
      case '5': nptl = atoi(optarg); break;
      case '6': nptb = atoi(optarg); break;
      case 'r': RMIN = atof(optarg); break;
      case 'R': RMAX = atof(optarg); break;
      case 'n': NUM = atoi(optarg); break;
      case 'A': A1 = atof(optarg); break;
      case 'Z': Z1 = atof(optarg); break;
      case 'E': EFAC = atof(optarg); break;
      case 'G': RING = true; break;
      case 'z': zerotol = atof(optarg); break;
      case 'i': initfile.erase(); initfile = optarg; break;
      case 'o': outfile.erase(); outfile = optarg; break;
      case 'd': DataDir.erase(); DataDir = optarg; break;
      case 'e': extfile.erase(); extfile = optarg; break;
      case 'm': CMDFile.erase(); CMDFile = optarg; break;
      case 'p': PartFile.erase(); PartFile = optarg; break;
      case 'g': if (atoi(optarg)) RLOG = true; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-1 L1\t\tlongitude in degrees (default: 30 deg)\n";
	msg += "\t-2 L2\t\tlongitude in degrees (default: 40 deg)\n";
	msg += "\t-3 B3\t\tlongitude in degrees (default: -2.0 deg)\n";
	msg += "\t-4 B4\t\tlongitude in degrees (default: 2.0 deg)\n";
	msg += "\t-5 n\t\tnumber of lines of sight in azimulth";
	msg += "\t-6 n\t\tnumber of lines of sight in altitude\n";
	msg += "\t-R R\t\tline-of-sight maximum integration radius (100 kpc)\n";
	msg += "\t-n n\t\tnumber of integration knots (200)\n";
	msg += "\t-i file\t\tname of initialization file (init.dat.galaxy)\n";
	msg += "\t-o file\t\toutput file suffix (dat)\n";
	msg += "\t-d path\t\tisochone data directory\n";
	msg += "\t-m file\t\tcolor-mangnitude template\n";
	msg += "\t-p file\t\tparticle file\n";
	msg += "\t-w size\t\tJ & K bin width\n";
	msg += "\t-A size\t\textinction scale length\n";
	msg += "\t-H size\t\textinction scale height\n";
	msg += "\t-E val\t\tmultiply extinction by factor (default: 1)\n";
	msg += "\t-G z\t\tuse extincition ring and central blob\n";
	msg += "\t-J z\t\tMaximum J flux (default: 15.8)\n";
	msg += "\t-K z\t\tMaximum K flux (default: 14.3)\n";
	msg += "\t-e file\t\tfile containing extinction ring & blob parameters\n";
	msg += "\t-z tol\t\tMinimum value to keep in data bin cache\n";
	msg += "\t-c \t\tAdditional evaluations to check memory\n";
	msg += "\t-l bool\t\tUse logarithmic abcissa\n";
	msg += "\t-g bool\t\tUse logarithmic integration (1=yes, 0=no)\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  L1 *= onedeg;
  L2 *= onedeg;
  B3 *= onedeg;
  B4 *= onedeg;
  double X1 = sin(B3);
  double X2 = sin(B4);

  // Set global parameters
  //

  CMDModelCache::DataDir = DataDir;
  CMDModelCache::RMIN = RMIN;
  CMDModelCache::RMAX = RMAX;
  CMDModelCache::NUM = NUM;
  SFDS::A1 = A1;
  SFDS::Z1 = Z1;
  CMDModelCache::EFAC = EFAC;
  CMDModelCache::zerotol = zerotol;
  if (RLOG) cout << "Using log integration\n";
  else cout << "Using linear integration\n";
  CMDModelCache::RLOG = RLOG;

				// Set ring extinction parameters
  if (RING) {

    SFDS::RING = RING;

    ifstream ifile(extfile.c_str());
    char line[256];

    while (1) {

      ifile.getline(line, 256); 

      if (strlen(line)==0) break;
      
      istringstream istr(line);
      string(label);
      istr >> label;

      if (label == string("RINGWIDTH"))
	istr >> SFDS::RINGWIDTH;
      else if (label == string("RINGLOC"))
	istr >> SFDS::RINGLOC;
      else if (label == string("RINGAMP"))
	istr >> SFDS::RINGAMP;
      else if (label == string("CENTERWIDTH"))
	istr >> SFDS::CENTERWIDTH;
      else if (label == string("CENTERAMP"))
	istr >> SFDS::CENTERAMP;
      else if (label == string("NEARBYWIDTH"))
	istr >> SFDS::NEARBYWIDTH;
      else if (label == string("NEARBYAMP"))
	istr >> SFDS::NEARBYAMP;
      else {
	cerr << "No such key: " << label << "\n";
	exit(-1);
      }

    }

  }

  // Hold instantiation of classes define the simuation
  //
  CMDModelCache *model;
  HistogramND *histo;

  int ndim = 2;			// 2

				// Factor for histograms in each tile
  int nfc, nfk;
  double kmin = 0.0, kmax = 16.0, kwid = 0.5;
  double cmin = 1.5, cmax = 5.0,  cwid = 0.1;

  nfk = (int)((kmax-kmin-1.0e-8)/kwid) + 1;
  nfc = (int)((cmax-cmin-1.0e-8)/cwid) + 1;

				// Order is Mag, Color, Color, . . .

  clivectord lo_b(2), hi_b(2), w_b(2);
  lo_b()[0] = kmin - 0.5*kwid;
  lo_b()[1] = cmin - 0.5*cwid;
  hi_b()[0] = kmax + 0.5*kwid;
  hi_b()[1] = cmax + 0.5*cwid;
  w_b()[0]  = kwid;
  w_b()[1]  = cwid;
  
  RecordType * rt = new RecordType();
  rt = rt->insertField(1, "magnitude", BasicType::Real);
  rt = rt->insertField(2, "color",     BasicType::Real);

  histo = new HistogramND(&lo_b, &hi_b, &w_b, rt);

				// Read data
  ifstream pdata(PartFile.c_str());
  if (!pdata) {
    cout << "Couldn't read particle data file: " << PartFile << endl;
    exit(-1);
  }

  const int bufsize = 1024;
  char buffer[bufsize];
  for (int i=0; i<5; i++) pdata.getline(buffer, bufsize);

  double l, x, b, colr, kmag;
  int intk, intc, total=0;

  typedef vector<int> ivector;
  vector<ivector> lumc(nfk);

  for (int j=0; j<nfk; j++) {
    lumc[j] = vector<int>(nfc, 0);
  }

  while (pdata.getline(buffer, bufsize)) {

    istringstream ins(buffer); 
    ins >> l;
    ins >> x;
    ins >> colr;
    ins >> kmag;

    b = asin(x);

    if (l>L1 && l<L2  &&
	b>B3 && b<B4   )
      {
	intk = (int)( (kmag-(kmin-0.5*kwid))/kwid );
	intc = (int)( (colr-(cmin-0.5*cwid))/cwid );
	if (intk>=0 && intk<nfk && intc>=0 && intc<nfc) {
	  lumc[intk][intc]++;
	  total++;
	}
      }

  }
				// Initialize population model
				// 

				// This is hardwired to J & K for now . . .
				// To be generalized

  string filename = DataDir + "/" + CMDFile;

  timer.start();
  CMD *cmd = new CMD(filename, 1.0e-8);
  timer.stop();
  cout << "Constructing CMD took " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() )
       << " seconds" << endl;

  cmd->HashStat();

				// Galaxy model
  try
    {
      model = new CMDModelCache(ndim, histo, cmd);
    }
  catch (const char *msg)
    {
      cout << msg << endl;
      exit(0);
    }
  
  int ndim1;
  ifstream initf(initfile.c_str());
  if (!initf) {
    cerr << "Couldn't open init file <" << initfile << ">\n";
    exit(0);
  }
  initf >> ndim1;

  StateInfo si(ndim1);
  State pstate(&si);
  for (int i=0; i<ndim1; i++) initf >> pstate[i];
  
  model->Initialize(pstate);

  int nhisto = histo->numberData();
  vector<double> cc(nhisto), kk(nhisto), mod(nhisto, 0.0), lum(nhisto);
  double delL = (L2 - L1)/nptl;
  double delX = (X2 - X1)/nptb;
  double totalnorm = 0.0;
  double normcheck = 0.0;
  
  for (int n1=0; n1<nptl; n1++) {
    for (int n2=0; n2<nptb; n2++) {

      double L = L1 + delL*(0.5+n1);
      double X = X1 + delX*(0.5+n2);

      double norm = model->NormEval(L, X, histo);
      
      cout << "L=" << L << " X=" << X << " Norm = " << norm << endl;
      totalnorm += norm * delL*delX;

      vector<double> ans = model->Evaluate(L, X, histo);
    
      for (int i=0; i<nhisto; i++) {

	kmag = 0.5*(histo->getLow(i)[0]+histo->getHigh(i)[0]);
	colr = 0.5*(histo->getLow(i)[1]+histo->getHigh(i)[1]);
      
	intk = (int)( (kmag-(kmin-0.5*kwid))/kwid );
	intc = (int)( (colr-(cmin-0.5*cwid))/cwid );

#ifdef DEBUG

	if ( fabs(histo->getLow(i)[0] - (kmin+((float)intk-0.5)*kwid)) 
	     > 1.0e-5 )
	  {
	    cout << "Out of bounds!  histo=" 
		 << histo->getLow(i)[0] << "  k_lo=" 
		 << kmin+((float)intk-0.5)*kwid
		 << endl;
	  }
      
	if ( fabs(histo->getHigh(i)[0] - (kmin+((float)intk+0.5)*kwid)) 
	     > 1.0e-5 )
	  {
	    cout << "Out of bounds!  histo=" 
		 << histo->getHigh(i)[0] << "  k_hi=" 
		 << kmin+((float)intk+0.5)*kwid
		 << endl;
	  }

	if ( fabs(histo->getLow(i)[1] - (cmin+((float)intc-0.5)*cwid))
	     > 1.0e-5 ) 
	  {
	    cout << "Out of bounds!  histo=" 
		 << histo->getLow(i)[1] << "  c_lo="
		 << cmin+((float)intc-0.5)*cwid
		 << endl;
	  }

	if ( fabs(histo->getHigh(i)[1] - (cmin+((float)intc+0.5)*cwid))
	     > 1.0e-5 ) 
	  {
	    cout << "Out of bounds!  histo=" 
		 << histo->getHigh(i)[1] << "  c_hi="
		 << cmin+((float)intc+0.5)*cwid
		 << endl;
	  }

#endif

	kk[i] = kmin + kwid*intk;
	cc[i] = cmin + cwid*intc;

	if (colr>cmin-0.5*cwid && colr<cmax+0.5*cwid) {
	  if (intk>=0 && intk<nfk && intc>=0 && intc<nfc && norm>0.0) {
	    mod[i] += ans[i] * delL*delX;
	    normcheck += ans[i] * delL*delX;
	    lum[i] = lumc[intk][intc];
	  }
	}
      }
    }
  }

  double lprob = 0.0;

  ofstream outf(outfile.c_str());

  outf << "# Total=" << total << endl;
  outf << "# Norm=" << totalnorm << endl;

  for (int i=0; i<nhisto; i++) {
    cout << setw(15) << cc[i]
	 << setw(15) << kk[i]
	 << setw(15) << mod[i] * total/totalnorm
	 << setw(15) << lum[i]
	 << endl;
    outf << setw(15) << cc[i]
	 << setw(15) << kk[i]
	 << setw(15) << mod[i] * total/totalnorm
	 << setw(15) << lum[i]
	 << endl;

    if (mod[i] <= 0.0 && lum[i]>0)
      lprob += -1.0e30;
    else if (mod[i] <= 0.0 && lum[i]==0)
      lprob += 0.0;
    else
      lprob += -mod[i]*total/totalnorm + log(mod[i] * total/totalnorm)*lum[i]
	- lgamma(1.0+lum[i]);
  }

  cout << "Total=" << total << endl;
  cout << "Total norm=" << totalnorm << endl;
  cout << "Check norm=" << normcheck << endl;
  cout << "Log prob=" << lprob << endl;

				// Anal delete
  delete model;
  delete cmd;
  delete histo;
  delete rt;

  return 0;
}
