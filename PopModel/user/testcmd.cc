// -*- C++ -*-

/* 
   Test cmd
*/

#include <math.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

#include <unistd.h>
#include <stdlib.h>


#include <CMD.h>

using namespace BIE;

int
main(int argc, char** argv)
{
  string cmdfile("/data/weinberg/3Mtab/lmc.cmd");
  string outfile("jkk.test");
  string datafile("jkk.cmd");
  bool printsm = false;
  int realize = 0;
  int num = 300;

  int c;
  while (1) {
    c = getopt (argc, argv, "d:f:pn:N:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'd': cmdfile.erase(); cmdfile = optarg; break;
      case 'f': outfile.erase(); outfile = optarg;
      case 's': datafile.erase(); datafile = optarg;
      case 'p': printsm = true; break;
      case 'n': num = atoi(optarg); break;
      case 'N': realize = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-d path\t\tdata file\n";
	msg += "\t-f file\t\toutput file\n";
	msg += "\t-p \t\tprint cmd\n";
	msg += "\t-n int\t\toutput grid per dimension\n";
	msg += "\t-N int\t\trealize CMD\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  CMD cmd(cmdfile, 1.0e-10);

  vector<double> fmin = cmd.MinMag();
  vector<double> fmax = cmd.MaxMag();
  double distmod = cmd.DistMod();

  cout << "Integral: " << cmd.Bin(fmin, fmax, distmod) << "\n";

#ifndef MARGE  
  cout << "--------------------\n";
  cout << "Integer accessor: ";
  cmd.VerifyInt();

  cout << "--------------------\n";
  cout << "Double accessor: ";
  cmd.VerifyDouble(distmod);

  cout << "--------------------\n";
#endif

  if (printsm) {
    vector<double> dx(2);
    dx[0] = (fmax[0] - fmin[0])/(num-1);
    dx[1] = (fmax[1] - fmin[1])/(num-1);
    vector<double> influx(2);
    float z;

    ofstream out(outfile.c_str());
    if (!out) {
      cerr << "Can't open <" << outfile << ">\n";
      exit(0);
    }
    out.write(reinterpret_cast<char*>(&num), sizeof(int));
    out.write(reinterpret_cast<char*>(&num), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmin[0])), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmax[0])), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmin[1])), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmax[1])), sizeof(int));

    for (int j=0; j<num; j++) {
      influx[1] = fmin[1] + dx[1]*j;

      for (int i=0; i<num; i++) {
	influx[0] = fmin[0] + dx[0]*i;

	//	z = cmd.GetValue(influx);
	z = cmd.Interpolate(influx, 18.4);
	out.write(reinterpret_cast<char*>(&z), sizeof(float));
      }
    }
  }

  if (realize) {

    ofstream out(datafile.c_str());

    vector<double> ans;
    vector<double>::iterator it;

    for (int n=0; n<realize; n++) {
      ans = cmd.genPoint();
      for (it=ans.begin(); it != ans.end(); it++) out << setw(15) << *it;
      out << endl;
    }

  }

}

