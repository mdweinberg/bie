// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <sstream>
#include <iomanip>
#include <utility>  // for <pair>
#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;


#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <BasicType.h>
#include <Uniform.h>
#include <Normal.h>
#define IS_MAIN


#include <HistogramND.h>
#include <PopModelCache.h>

#include <ACG.h>

using namespace BIE;

				// True for debugging
bool verbose = true;

				// Global data
int nf;
double Fmin = 6.0, Fmax = 14.0, Fwid = 1.0;
ACG *gen;
double match;

class PairLess {
public:

  bool operator() (const pair<double, int>& p) const
    {
      return p.first >= match;
    }
};



class FluxMap {

private:

  class Datum {
  public:
    double jmin, jmax, kmin, kmax;
    double value;
  };

  vector<Datum> data;
  vector< pair<double, int> > cum;
  double norm;

  Uniform *pick;
  Uniform *unit;

public:

  FluxMap(void) : norm(0.0), pick(NULL), unit(NULL) {}
  ~FluxMap();

  void Load(int& nf, vector<double>& ans, HistogramND* histo);

  double GetNorm(void) { return norm; }
  vector<double> Generate(void);

};


vector<double> FluxMap::Generate(void)
{
  vector<double> ret(2, 0.0);
  if (!pick) return ret;

  match = (*pick)();

  vector< pair<double, int> >::iterator it = 
    find_if(cum.begin(), cum.end(), PairLess());
  
  ret[0] = data[it->second].jmin + 
    (*unit)()*(data[it->second].jmax - data[it->second].jmin);
  ret[1] = data[it->second].kmin + 
    (*unit)()*(data[it->second].kmax - data[it->second].kmin);

  return ret;
}

FluxMap::~FluxMap()
{
  delete pick;
  delete unit;
}


void FluxMap::Load(int& nf, vector<double> &ans, HistogramND* histo)
{

  for (int i=0; i<histo->numberData(); i++) {
    
    int ii = 
      (int)( (0.5*(histo->getLow(i)[0] + histo->getHigh(i)[0]) - Fmin)/Fwid );
  
    int jj = 
      (int)( (0.5*(histo->getLow(i)[1] + histo->getHigh(i)[1]) - Fmin)/Fwid );
  
    if (ii>=0 && ii<nf && jj>=0 && jj<nf) {
      struct Datum datum;
      datum.jmin = histo->getLow(i)[0];
      datum.jmax = histo->getHigh(i)[0];
      datum.kmin = histo->getLow(i)[1];
      datum.kmax = histo->getHigh(i)[1];
      datum.value = ans[i];
      data.push_back(datum);
    }
  }

  for (int i=0; i<(int)data.size(); i++) {
    pair<double, int> Pair;
    Pair.first = data[i].value;
    Pair.second = i;
    cum.push_back(Pair);
    if (i) cum[i].first += cum[i-1].first;
  }

  norm = cum[cum.size()-1].first;

				// Pseudorandom number generators
  pick = new Uniform(0.0, norm, gen);
  unit = new Uniform(0.0, 1.0, gen);
}

/**
   @name makegalmod2: main
   Generate data from line of sight grid
*/
int
main(int argc, char** argv)
{
  const double onedeg = M_PI/180.0;

				// Maximum line-of-sight distance
  double RMAX = 50.0;
				// Integration knots
  int NUM = 100;
				// Exponential birt rate exponent
  double NU = 0.0;
				// Metallicity
  double METAL = 0.019;
				// Extinction scale length and height
  double A1 = 2.5;
  double Z1 = 150.0;
  double EFAC = 1.0;
  bool RING = false;

				// Default init file
  string extfile = "extinction.dat";
				// Default init file
  string initfile = "init.dat.galaxy";
				// Lines of sight file
  string losfile = "lines.of.sight";
				// Output data
  string outfile = "data.dat";
				// Data directory
  string DataDir = "/data/weinberg/3Mtab";

  int nmix = 1;			// One component mixture

  int iseed = 11;		// Random number generator seed

  int ntotal = 1000;		// Number of simulated datums

				// Birth rate type
  BirthRate Model = Exponential;
				// Burst fraction
  double BURST = 0.0;

  bool verbose = false;		// Print output to stdout if true

  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "S:L:B:R:M:i:o:d:e:n:N:a:w:m:A:Z:E:F:brxvh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'S': iseed = atoi(optarg); break;
      case 'N': ntotal = atoi(optarg); break;
      case 'R': RMAX = atof(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'n': NUM = atoi(optarg); break;
      case 'a': NU = atof(optarg); break;
      case 'm': METAL = atof(optarg); break;
      case 'w': Fwid = atof(optarg); break;
      case 'A': A1 = atof(optarg); break;
      case 'Z': Z1 = atof(optarg); break;
      case 'E': EFAC = atof(optarg); break;
      case 'b': Model = Burst; break;
      case 'F': BURST = atof(optarg); break;
      case 'r': RING = true; break;
      case 'i': initfile.erase(); initfile = optarg; break;
      case 'l': losfile.erase(); losfile = optarg; break;
      case 'o': outfile.erase(); outfile = optarg; break;
      case 'd': DataDir.erase(); DataDir = optarg; break;
      case 'e': extfile.erase(); extfile = optarg; break;
      case 'v': verbose = true; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-S i\t\trandom number generator seed (default: 11)\n";
	msg += "\t-N n\t\tnumberof simulated datums (default: 1000)\n";
	msg += "\t-R R\t\tline-of-sight maximum integration radius (100 kpc)\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-n n\t\tnumber of integration knots (200)\n";
	msg += "\t-i file\t\tname of initialization file (init.dat.galaxy)\n";
	msg += "\t-l file\t\tcontains a list of lines of sight in l, b (lines.of.sight)\n";
	msg += "\t-o file\t\toutput file suffix (dat)\n";
	msg += "\t-d path\t\tisochone data directory\n";
	msg += "\t-a Nu\t\tpowerlaw birthrate exponent (units of H_o)\n";
	msg += "\t-w size\t\tJ & K bin width\n";
	msg += "\t-s val\t\tRadius in color excess map\n";
	msg += "\t-A size\t\textinction scale length\n";
	msg += "\t-H size\t\textinction scale height\n";
	msg += "\t-E val\t\tmultiply extinction by factor (default: 1)\n";
	msg += "\t-r z\t\tuse extincition ring and central blob\n";
	msg += "\t-e file\t\tfile containing extinction ring & blob parameters\n";
	msg += "\t-m z\t\tmetallicity\n";
	msg += "\t-b \t\tUse burst birthrate model\n";
	msg += "\t-F F\t\tFraction of total in the burst\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  // Random
  //

  gen = new ACG(iseed, 20); 

  // Open output file
  //

  ofstream out(outfile.c_str());
  if (!out) {
    cerr << "Couldn't open output file: " << outfile << endl;
    exit(-1);
  }


  // Open lines of sight file
  //

  ifstream los(losfile.c_str());
  if (!los) {
    cerr << "Couldn't open line of sight file: " << losfile << endl;
    exit(-1);
  }

  vector<double> L, B, dL, dB;
  const int bufsize = 1024;
  char line[bufsize];
  int nlos = 0;

  while (los.getline(line, bufsize)) {
    istringstream ins(line);
    double lmin, lmax, bmin, bmax;

    ins >> lmin; if (!ins) break;
    ins >> lmax; if (!ins) break;
    ins >> bmin; if (!ins) break;
    ins >> bmax; if (!ins) break;

    dL.push_back(0.5*(lmax - lmin)*onedeg);
    dB.push_back(0.5*(bmax - bmin)*onedeg);
    L.push_back(0.5*(lmin+lmax)*onedeg);
    B.push_back(0.5*(bmin+bmax)*onedeg);
    nlos++;
  }
  los.close();

  // Set global parameters
  //

  PopModelCache::DataDir = DataDir;
  PopModelCache::RMAX = RMAX;
  PopModelCache::NUM = NUM;
  PopModelCache::NU = NU;
  PopModelCache::A1 = A1;
  PopModelCache::Z1 = Z1;
  PopModelCache::EFAC = EFAC;
  PopModelCache::METAL = METAL;
  PopModelCache::RING = RING;
  if (Model == Burst) {		// Sanity check on BURST value
    if (BURST<0.0) BURST = 0.0;
    if (BURST>1.0) BURST = 1.0;
    PopModelCache::BRmodel = Burst;
    PopModelCache::BURST = BURST;
  }

				// Set ring extinction parameters
  if (RING) {
    ifstream ifile(extfile.c_str());
    char line[256];

    while (1) {

      ifile.getline(line, 256); 

      if (strlen(line)==0) break;
      
      istringstream istr(line);
      string(label);
      istr >> label;

      if (label == string("RINGWIDTH"))
	istr >> PopModelCache::RINGWIDTH;
      else if (label == string("RINGLOC"))
	istr >> PopModelCache::RINGLOC;
      else if (label == string("RINGAMP"))
	istr >> PopModelCache::RINGAMP;
      else if (label == string("CENTERWIDTH"))
	istr >> PopModelCache::CENTERWIDTH;
      else if (label == string("CENTERAMP"))
	istr >> PopModelCache::CENTERAMP;
      else if (label == string("NEARBYWIDTH"))
	istr >> PopModelCache::NEARBYWIDTH;
      else if (label == string("NEARBYAMP"))
	istr >> PopModelCache::NEARBYAMP;
      else {
	cerr << "No such key: " << label << "\n";
	exit(-1);
      }

    }

  }

  // Hold instantiation of classes define the simuation
  //
  PopModelCache *model;
  HistogramND *histo;

  int ndim = 2;			// Two model parameters

				// Factor for histograms in each tile
  nf = (int)((Fmax-Fmin-1.0e-8)/Fwid) + 1;
  clivectord lo_b(2, Fmin), hi_b(2, Fmax), w_b(2, Fwid);

  RecordType * rt = new RecordType();
  rt = rt->insertField(1, "flux1", BasicType::Real);
  rt = rt->insertField(2, "flux2", BasicType::Real);

  histo = new HistogramND(&lo_b, &hi_b, &w_b, rt);

				// Galaxy model
  model = new PopModelCache(ndim, nmix, histo);

  vector<double> winit, *pinit;
  pinit = new vector<double> [nmix];

  ifstream initf(initfile.c_str());
  winit = vector<double>(nmix);
  for (int i=0; i<nmix; i++) {
    initf >> winit[i];
    pinit[i] = vector<double>(ndim);
    for (int j=0; j<ndim; j++) initf >> pinit[i][j];
  }
  
  model->Initialize(winit, pinit);

  vector<FluxMap> maps(nlos);
  vector< pair<double, int> > cum(nlos);

  for (int n=0; n<nlos; n++) {
    
    double norm = model->NormEval(L[n], B[n], histo);

    cout << " L = " << L[n] << " B = " << B[n] << " Norm = " << norm << endl;

    model->ResetCache();

    vector<double> ans = model->Evaluate(L[n], B[n], histo);

    maps[n].Load(nf, ans, histo);

    cum[n].first = maps[n].GetNorm();
    cum[n].second = n;
    if (n) cum[n].first += cum[n-1].first;
  }
  
  for (int n=0; n<nlos; n++)  
    cout << setw(5) << cum[n].second << setw(15) << cum[n].first << endl;

  Uniform pick(0.0, cum[nlos-1].first, gen);
  Uniform unit(-1.0, 1.0, gen);

  for (int i=0; i<ntotal; i++) {
				// Generate a random #
    match = pick();

				// Find the line of sight
    vector< pair<double, int> >::iterator it = 
      find_if(cum.begin(), cum.end(), PairLess());

				// Check . . .
    if (it == cum.end()){
      cout << "Ooops: bad value for match=" << match << endl;
      break;
    }

    int indx = it->second;

				// Verbose diagnostics
    if (verbose) {
      if (it->second == 0)
	cout << setw(15) << indx << setw(15) << match 
	     << setw(15) << 0.0 << setw(15) << cum[indx].first
	     << endl;
      else
	cout << setw(15) << indx << setw(15) << match 
	     << setw(15) << cum[indx-1].first << setw(15) << cum[indx].first
	     << endl;

    }

				// Generate point in tile (in radians)
    double l = (L[indx] + dL[indx]*unit());
    double b = (B[indx] + dB[indx]*unit());

				// Generate flux
    vector<double> flux = maps[indx].Generate();

				// Print it out
    out << setw(15) << l << setw(15) << b
	<< setw(15) << flux[0] << setw(15) << flux[1] << endl;

  }

  return 0;
}


