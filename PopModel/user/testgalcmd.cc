// This may look like C code, but it is really -*- C++ -*-

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;

#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <BasicType.h>
#define IS_MAIN

#include <HistogramND.h>
#include <CMDModelCache.h>
#include <Timer.h>

using namespace BIE;

MatrixM dat;
int nfc, nfk;
double kmin = 0.0, kmax = 16.0, kwid = 0.1;
double cmin = 1.0, cmax = 5.0, cwid = 0.05;
// double Jmax = 15.8, Kmax=14.3;

/**
   Bilinear interpolation on J & K grid
*/
double getValue(double K, double C)
{
				// Bin limits

  if (K<kmin || K>=kmax || C<cmin || C>=cmax) return 0.0;


  int ik = (int)( (K-kmin)/kwid );
  int ic = (int)( (C-cmin)/cwid );

				// Pin at grid boundaries
  if (ik<0) ik=0;
  if (ic<0) ic=0;
  if (ik>nfk-2) ik=nfk-2;
  if (ic>nfc-2) ic=nfc-2;

				// Weight factors
  double k2 = (K - (kmin + kwid*ik))/kwid;
  double k1 = 1.0 - k2;

  double c2 = (C - (cmin + cwid*ic))/cwid;
  double c1 = 1.0 - c2;

				// Do not interpolate past center of
				// grid edge
  if (k2 > 1.0) {k1 = 0.0; k2 = 1.0;}
  if (k2 < 0.0) {k1 = 1.0; k2 = 0.0;}
  if (c2 > 1.0) {c1 = 0.0; c2 = 1.0;}
  if (c2 < 0.0) {c1 = 1.0; c2 = 0.0;}

  return 
    dat[ik  ][ic  ]*k1*c1 + dat[ik+1][ic  ]*k2*c1 +
    dat[ik  ][ic+1]*k1*c2 + dat[ik+1][ic+1]*k2*c2 ;
}


/**
   Get current memory usage
*/
unsigned get_total_memory(pid_t pid)
{
  ostringstream proc;
  proc << "/proc/" << pid << "/stat";
  ifstream stat(proc.str().c_str());
  
  if (!stat) {
    cout << "Couldn't open " << proc.str() << endl;
    return 0;
  }

  string temp;
  unsigned vsize;
   
  for (int i=0; i<22; i++) stat >> temp;
  stat >> vsize;
  
  return vsize;
}

/**
   @name testgalcmd: main
   Check out of CMDModelCache
*/
int
main(int argc, char** argv)
{
  const double onedeg = M_PI/180.0;
  pid_t pid = getpid();
  Timer timer(true);

  get_total_memory(pid);
				// Line of sight
  double L = 30.0;
  double B = 30.0;
				// Minimum line-of-sight distance
  double RMIN = 0.010;
				// Maximum line-of-sight distance
  double RMAX = 100.0;
				// Integration knots
  int NUM = 200;

  double A1 = 2.5;
  double Z1 = 150.0;
  double EFAC = 1.0;
  bool RING = false;
  double zerotol = 1.0e-12;
				// Default init file
  string extfile = "extinction.dat";
				// Default init file
  string initfile = "init.dat.cmd";
				// Output suffix
  string outfile = "dat";
				// Data directory
  string DataDir = "/data/weinberg/3Mtab";
				// CMD file
  string CMDFile = "lmc.cmd";

  int nout = 40;		// Number of points in CMD

  bool verbose = false;		// Print output to stdout if true

  bool mcheck = false;		// Do additional evaluations to check
				// memory usage

				// Log abscissa
  bool RLOG = false;


  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "L:B:r:R:i:o:d:e:m:n:w:A:Z:E:F:z:l:rxcvh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'L': L = atof(optarg); break;
      case 'B': B = atof(optarg); break;
      case 'r': RMIN = atof(optarg); break;
      case 'R': RMAX = atof(optarg); break;
      case 'n': NUM = atoi(optarg); break;
      case 'A': A1 = atof(optarg); break;
      case 'Z': Z1 = atof(optarg); break;
      case 'E': EFAC = atof(optarg); break;
      case 'G': RING = true; break;
      case 'z': zerotol = atof(optarg); break;
      case 'i': initfile.erase(); initfile = optarg; break;
      case 'o': outfile.erase(); outfile = optarg; break;
      case 'd': DataDir.erase(); DataDir = optarg; break;
      case 'e': extfile.erase(); extfile = optarg; break;
      case 'm': CMDFile.erase(); CMDFile = optarg; break;
      case 'l': if (atoi(optarg)) RLOG = true; break;
      case 'c': mcheck = true; break;
      case 'v': verbose = true; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-L L\t\tlongitude in degrees (default: 30 deg)\n";
	msg += "\t-B B\t\tlatitude in degrees (default: 30 deg)\n";
	msg += "\t-R R\t\tline-of-sight maximum integration radius (100 kpc)\n";
	msg += "\t-n n\t\tnumber of integration knots (200)\n";
	msg += "\t-i file\t\tname of initialization file (init.dat.galaxy)\n";
	msg += "\t-o file\t\toutput file suffix (dat)\n";
	msg += "\t-d path\t\tisochone data directory\n";
	msg += "\t-m file\t\tcolor-mangnitude template\n";
	msg += "\t-w size\t\tJ & K bin width\n";
	msg += "\t-s val\t\tRadius in color excess map\n";
	msg += "\t-A size\t\textinction scale length\n";
	msg += "\t-H size\t\textinction scale height\n";
	msg += "\t-E val\t\tmultiply extinction by factor (default: 1)\n";
	msg += "\t-G z\t\tuse extincition ring and central blob\n";
	msg += "\t-J z\t\tMaximum J flux (default: 15.8)\n";
	msg += "\t-K z\t\tMaximum K flux (default: 14.3)\n";
	msg += "\t-e file\t\tfile containing extinction ring & blob parameters\n";
	msg += "\t-z tol\t\tMinimum value to keep in data bin cache\n";
	msg += "\t-1 fmin\t\tMinimum flux value for each band (6.0)\n";
	msg += "\t-2 fmax\t\tMaximum flux value for each band (16.0)\n";
	msg += "\t-c \t\tAdditional evaluations to check memory\n";
	msg += "\t-l bool\t\tUse logarithmic abcissa\n";
	msg += "\t-v \t\tVerobse output\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  L *= onedeg;
  B *= onedeg;
  double X = sin(B);

  // Set global parameters
  //

  CMDModelCache::DataDir = DataDir;
  CMDModelCache::RMIN = RMIN;
  CMDModelCache::RMAX = RMAX;
  CMDModelCache::NUM = NUM;
  CMDModelCache::EFAC = EFAC;
  CMDModelCache::zerotol = zerotol;
  if (RLOG) cout << "Using log integration\n";
  else cout << "Using linear integration\n";
  CMDModelCache::RLOG = RLOG;

				// Set ring extinction parameters
  if (RING) {

    SFDS::RING = RING;

    ifstream ifile(extfile.c_str());
    char line[256];

    while (1) {

      ifile.getline(line, 256); 

      if (strlen(line)==0) break;
      
      istringstream istr(line);
      string(label);
      istr >> label;

      if (label == string("RINGWIDTH"))
	istr >> SFDS::RINGWIDTH;
      else if (label == string("RINGLOC"))
	istr >> SFDS::RINGLOC;
      else if (label == string("RINGAMP"))
	istr >> SFDS::RINGAMP;
      else if (label == string("CENTERWIDTH"))
	istr >> SFDS::CENTERWIDTH;
      else if (label == string("CENTERAMP"))
	istr >> SFDS::CENTERAMP;
      else if (label == string("NEARBYWIDTH"))
	istr >> SFDS::NEARBYWIDTH;
      else if (label == string("NEARBYAMP"))
	istr >> SFDS::NEARBYAMP;
      else {
	cerr << "No such key: " << label << "\n";
	exit(-1);
      }

    }

  }

  // Hold instantiation of classes define the simuation
  //
  CMDModelCache *model;
  HistogramND *histo;

  int ndim = 5;			// 2

				// Factor for histograms in each tile
  nfk = (int)((kmax-kmin-1.0e-8)/kwid) + 1;
  nfc = (int)((cmax-cmin-1.0e-8)/cwid) + 1;

				// Order is Mag, Color, Color, . . .

  clivectord lo_b(2), hi_b(2), w_b(2);
  lo_b()[0] = kmin - 0.5*kwid;
  lo_b()[1] = cmin - 0.5*cwid;
  hi_b()[0] = kmax + 0.5*kwid;
  hi_b()[1] = cmax + 0.5*cwid;
  w_b()[0] = kwid;
  w_b()[1] = cwid;
  
  RecordType * rt = new RecordType();
  rt = rt->insertField(1, "magnitude", BasicType::Real);
  rt = rt->insertField(2, "color",     BasicType::Real);

  histo = new HistogramND(&lo_b, &hi_b, &w_b, rt);

				// Initialize population model
				// 

				// This is hardwired to J & K for now . . .
				// To be generalized

  string filename = DataDir + "/" + CMDFile;

  timer.start();
  CMD *cmd = new CMD(filename, 1.0e-8);
  timer.stop();
  cout << "Constructing CMD took " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() )
       << " seconds" << endl;

  cmd->HashStat();

  unsigned base_mem = get_total_memory(pid);
  cout << "Base total memory=" << base_mem << endl;

				// Galaxy model
  try
    {
      model = new CMDModelCache(ndim, histo, cmd);
    }
  catch (const char *msg)
    {
      cout << msg << endl;
      exit(0);
    }
  
  int ndim1;
  ifstream initf(initfile.c_str());
  if (!initf) {
    cerr << "Couldn't open init file <" << initfile << ">\n";
    exit(0);
  }
  initf >> ndim1;

  StateInfo si(ndim1);
  State pstate(&si);
  for (int i=0; i<ndim1; i++) initf >> pstate[i];
  
  model->Initialize(pstate);

  unsigned mem1 = get_total_memory(pid);
  cout << "Memory after initialization=" << mem1 
       << ", delta=" << mem1 - base_mem << endl;
  

  double norm = model->NormEval(L, X, histo);

  unsigned mem2 = get_total_memory(pid);
  cout << "Memory after normeval=" << mem2
       << ", delta=" << mem2 - mem1 << endl;
  

  cout << " Norm = " << norm << endl;

  vector<double> ans = model->Evaluate(L, X, histo);

  unsigned mem3 = get_total_memory(pid);
  cout << "Memory after evaluation=" << mem3
       << ", delta=" << mem3 - mem2 << endl;

  dat.setsize(0, nfk-1, 0, nfc-1);
  dat.zero();

  double sum = 0.0;

  for (int i=0; i<histo->numberData(); i++) {

    if (verbose) {
      cout
	<< setw(15) << histo->getLow(i)[0]
	<< setw(15) << histo->getHigh(i)[0]
	<< setw(15) << histo->getLow(i)[1]
	<< setw(15) << histo->getHigh(i)[1];
      if (norm>0.0)
	cout << setw(15) << ans[i]/norm;
      else
	cout << setw(15) << 0.0;
      cout << endl;
    }
    
    int ii = 
      (int)( (0.5*(histo->getLow(i)[0] + histo->getHigh(i)[0]) - kmin
	      - 0.5*kwid)/kwid );

    int jj = 
      (int)( (0.5*(histo->getLow(i)[1] + histo->getHigh(i)[1]) - cmin 
	      - 0.5*cwid)/cwid );
    
    if (ii>=0 && ii<nfk && jj>=0 && jj<nfc && norm>0.0) {
      dat[ii][jj] = ans[i]/norm;
      sum += dat[ii][jj];
    }
  }

  float z;
  string fout;
				// J-K vs K distribution

  double  dK = (kmax - kmin)/(nout - 1);
  double  dC = (cmax - cmin)/(nout - 1);
  double  K, C, normcmd=0.0;
  MatrixM cmdout(0, nout-1, 0, nout-1);
  VectorM Kout(0, nout-1);

  Kout.zero();

  for (int j=0; j<nout; j++) {
    C = cmin + dC*j;
    for (int i=0; i<nout; i++) {
      K = kmin + dK*i;
      cmdout[i][j] = getValue(K, C);
      Kout[i] += cmdout[i][j];
      normcmd += cmdout[i][j];
    }
  }
  if (normcmd>0.0) cmdout /= normcmd;

  fout = "jkk." + outfile;
  ofstream out2(fout.c_str());
  out2.write(reinterpret_cast<char*>(&nout), sizeof(int));
  out2.write(reinterpret_cast<char*>(&nout), sizeof(int));
  out2.write(reinterpret_cast<char*>(&(z=cmin)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=cmax)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=kmax)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=kmin)), sizeof(float));
  for (int j=nout-1; j>=0; j--) {
    for (int i=0; i<nout; i++) {
      out2.write(reinterpret_cast<char*>(&(z=cmdout[j][i])), sizeof(float));
    }
  }

				// Luminosity function
  
  fout = "klum." + outfile;
  ofstream out3(fout.c_str());
  for (int i=0; i<nout; i++) {
    K = kmin + dK*(0.5+i);
    out3 << setw(15) << K << setw(15) << Kout[i] << endl;
  }

  // Do some additional evaluations to check memory usage

  if (mcheck) {

    const int nummods = 5;
    unsigned lastmem = mem3, curmem;
    for (int i=0; i<nummods; i++) {
      
      /*
      L += onedeg*(i+1);
      B += onedeg*(i+1);
      X = sin(B);
      */
      
      pstate[1] += 0.25;
      
      timer.reset();
      timer.start();
      model->Initialize(pstate);
      norm = model->NormEval(L, X, histo);
      ans = model->Evaluate(L, X, histo);
      timer.stop();
      
      cout << " Norm[L=" << L/onedeg << ", B=" << B/onedeg << "] = " 
	   << norm << endl;

      curmem = get_total_memory(pid);
      cout << "Memory after model[" << i << "] =" << curmem
	   << ", delta=" << curmem - lastmem << endl;
      lastmem = curmem;

      cout << "Model computation took " 
	   << 1.0e-6*(timer.getTime().getUserTime() +
		      timer.getTime().getSystemTime() )
	   << " seconds" << endl;
      
      pstate[1] += 0.25;

      timer.reset();
      timer.start();
      model->Initialize(pstate);
      norm = model->NormEval(L, X, histo);
      ans = model->Evaluate(L, X, histo);
      timer.stop();
      
      cout << " Norm[L=" << L/onedeg << ", B=" << B/onedeg << "] = " 
	   << norm << endl;
      
      cout << "Model reevaluation took " 
	   << 1.0e-6*(timer.getTime().getUserTime() +
		      timer.getTime().getSystemTime() )
	   << " seconds" << endl;
    }

  }
  
  cout << endl;
  model->PrintCache();
  cout << endl;

				// Anal delete
  delete model;
  delete cmd;
  delete histo;
  delete rt;

  return 0;
}
