#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

#include <unistd.h>

int
main(int argc, char** argv)
{
  string basisfile = "basis.data";

  int c;
  while (1) {
    c = getopt (argc, argv, "f:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'f':
	basisfile.erase(); basisfile = optarg; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " <vector of components weights>\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

				// Read component vector off of command line
  vector<double> comp;
  while(optind < argc)
    comp.push_back(atof(argv[optind++]));
  
  double sum = 0.0;
  for (unsigned i=0; i<comp.size(); i++) sum += comp[i];
  for (unsigned i=0; i<comp.size(); i++) comp[i] /= sum;

				// Make basis
  ifstream bin(basisfile.c_str());
  if (!bin) {
    cerr << "ratebasis: error opening <" << basisfile << ">\n";
    exit(-1);
  }

  int nbasis, nages;

  bin >> nbasis;
  bin >> nages;
  if (nages != 14) {
    cerr << "ratebasis: time bins=" << nages 
	 << " rather than the expected 14\n";
  }

  vector<float> ages(nages);
  for (int i=0; i<nages; i++) ages[i] = 6.0 + 0.3*(0.5+i);
  vector< vector<float> > basis(nbasis);

  for (int j=0; j<nbasis; j++) {
    basis[j] = vector<float>(nages);
    for (int i=0; i<nages; i++) bin >> basis[j][i];
  }
				// Compute history function

  vector<double> history(nages, 0.0);
  for (int j=0; j<nbasis; j++)
    for (int i=0; i<nages; i++)
      history[i] += comp[j]*basis[j][i];

				// Weight by time in bin
  double fac = pow(10.0, 0.3);
  sum = 0.0;
  for (int i=0; i<nages; i++) {
    history[i] /= pow(10.0, (6.0 + 0.3*i) - 9.0) * (fac - 1.0);
    sum += history[i];
  }
  for (int i=0; i<nages; i++) history[i] /= sum;

  sum = 0.0;
  for (int i=0; i<nages; i++) {
    sum += history[i];
    cout << setw(3) << i
	 << setw(15) << 6.0 + 0.3*(0.0+i)
	 << setw(15) << 6.0 + 0.3*(0.5+i)
	 << setw(15) << 6.0 + 0.3*(1.0+i)
	 << setw(15) << history[i]
	 << setw(15) << sum
	 << endl;
  }

  return 0;
}
