// This may look like C code, but it is really -*- C++ -*-

#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;


#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <BasicType.h>

#define IS_MAIN

#include <HistogramND.h>
#include <PopModelCacheF.h>
#include <Timer.h>

using namespace BIE;

MatrixM dat;
int nf;
double Fmin = 0.0, Fmax = 16.0, Fwid = 0.1;
double Jmax = 15.8, Kmax=14.3;

/**
   Bilinear interpolation on J & K grid
*/
double getValue(double J, double K)
{
				// Flux limits
  if (J>Jmax || K>Kmax) return 0.0;

				// Bin limits

  if (J<Fmin || J>=Fmax || K<Fmin || K>=Fmax) return 0.0;


  int ij = (int)( (J-Fmin)/Fwid );
  int ik = (int)( (K-Fmin)/Fwid );

				// Pin at grid boundaries

  if (ij<0) ij=0;
  if (ik<0) ik=0;
  if (ij>nf-2) ij=nf-2;
  if (ik>nf-2) ik=nf-2;

				// Weight factors
  double j2 = (J - (Fmin + Fwid*ij))/Fwid;
  double j1 = 1.0 - j2;

  double k2 = (K - (Fmin + Fwid*ik))/Fwid;
  double k1 = 1.0 - k2;

				// Do not interpolate past center of
				// grid edge
  if (j2 > 1.0) {j1 = 0.0; j2 = 1.0;}
  if (j2 < 0.0) {j1 = 1.0; j2 = 0.0;}
  if (k2 > 1.0) {k1 = 0.0; k2 = 1.0;}
  if (k2 < 0.0) {k1 = 1.0; k2 = 0.0;}

  return 
    dat[ij  ][ik  ]*j1*k1 + dat[ij+1][ik  ]*j2*k1 +
    dat[ij  ][ik+1]*j1*k2 + dat[ij+1][ik+1]*j2*k2 ;
}


/**
   Get current memory usage
*/
unsigned get_total_memory(pid_t pid)
{
  ostringstream proc;
  proc << "/proc/" << pid << "/stat";
  ifstream stat(proc.str().c_str());
  
  if (!stat) {
    cout << "Couldn't open " << proc.str() << endl;
    return 0;
  }

  string temp;
  unsigned vsize;
   
  for (int i=0; i<22; i++) stat >> temp;
  stat >> vsize;
  
  return vsize;
}


/**
   @name testgalmod: main
   Check out of PopModelCacheF (two flux case)
*/
int
main(int argc, char** argv)
{
  const double onedeg = M_PI/180.0;
  pid_t pid = getpid();
  Timer timer(true);

  get_total_memory(pid);
				// Line of sight
  double L = 30.0;
  double B = 30.0;
				// Minimum line-of-sight distance
  double RMIN = 0.010;
				// Maximum line-of-sight distance
  double RMAX = 100.0;
				// Integration knots
  int NUM = 200;
				// Power law birth rate index
  double NU = 1.0;
				// Metallicity
  double METAL = 0.019;
				// Extinction scale length and height
  double A1 = 2.5;
  double Z1 = 150.0;
  double EFAC = 1.0;
  bool RING = false;
  bool RLOG = true;
  double zerotol = 1.0e-12;
				// Default init file
  string extfile = "extinction.dat";
				// Default init file
  string initfile = "init.dat.galaxy";
				// Output suffix
  string outfile = "dat";
				// Data directory
  string DataDir = "/data/weinberg/3Mtab";

  int nmix = 1;			// One component mixture

  int nout = 40;		// Number of points in CMD

				// Birth rate type
  BirthRate Model = Exponential;
				// Burst fraction
  double BURST = 0.0;

  bool verbose = false;		// Print output to stdout if true

  bool mcheck = false;		// Do additional evaluations to check
				// memory usage

  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "L:B:r:R:M:i:o:d:e:n:N:a:w:m:A:Z:E:F:J:K:z:1:2:brlxcvh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'L': L = atof(optarg); break;
      case 'B': B = atof(optarg); break;
      case 'r': RMIN = atof(optarg); break;
      case 'R': RMAX = atof(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'N': nout = atoi(optarg); break;
      case 'n': NUM = atoi(optarg); break;
      case 'a': NU = atof(optarg); break;
      case 'm': METAL = atof(optarg); break;
      case 'w': Fwid = atof(optarg); break;
      case 'A': A1 = atof(optarg); break;
      case 'Z': Z1 = atof(optarg); break;
      case 'E': EFAC = atof(optarg); break;
      case 'b': Model = Burst; break;
      case 'F': BURST = atof(optarg); break;
      case 'G': RING = true; break;
      case 'l': RLOG =false; break;
      case 'J': Jmax = atof(optarg); break;
      case 'K': Kmax = atof(optarg); break;
      case 'z': zerotol = atof(optarg); break;
      case '1': Fmin = atof(optarg); break;
      case '2': Fmax = atof(optarg); break;
      case 'i': initfile.erase(); initfile = optarg; break;
      case 'o': outfile.erase(); outfile = optarg; break;
      case 'd': DataDir.erase(); DataDir = optarg; break;
      case 'e': extfile.erase(); extfile = optarg; break;
      case 'c': mcheck = true; break;
      case 'v': verbose = true; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-L L\t\tlongitude in degrees (default: 30 deg)\n";
	msg += "\t-B B\t\tlatitude in degrees (default: 30 deg)\n";
	msg += "\t-R R\t\tline-of-sight maximum integration radius (100 kpc)\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-N n\t\tnumber of points in output grid (40)\n";
	msg += "\t-n n\t\tnumber of integration knots (200)\n";
	msg += "\t-i file\t\tname of initialization file (init.dat.galaxy)\n";
	msg += "\t-o file\t\toutput file suffix (dat)\n";
	msg += "\t-d path\t\tisochone data directory\n";
	msg += "\t-a Nu\t\tpowerlaw birthrate exponent (units of H_o)\n";
	msg += "\t-w size\t\tJ & K bin width\n";
	msg += "\t-s val\t\tRadius in color excess map\n";
	msg += "\t-A size\t\textinction scale length\n";
	msg += "\t-H size\t\textinction scale height\n";
	msg += "\t-E val\t\tmultiply extinction by factor (default: 1)\n";
	msg += "\t-G z\t\tuse extincition ring and central blob\n";
	msg += "\t-J z\t\tMaximum J flux (default: 15.8)\n";
	msg += "\t-K z\t\tMaximum K flux (default: 14.3)\n";
	msg += "\t-e file\t\tfile containing extinction ring & blob parameters\n";
	msg += "\t-m z\t\tmetallicity\n";
	msg += "\t-b \t\tUse burst birthrate model\n";
	msg += "\t-F F\t\tFraction of total in the burst\n";
	msg += "\t-z tol\t\tMinimum value to keep in data bin cache\n";
	msg += "\t-1 Fmin\t\tMinimum flux value for each band (6.0)\n";
	msg += "\t-2 Fmax\t\tMaximum flux value for each band (16.0)\n";
	msg += "\t-c \t\tAdditional evaluations to check memory\n";
	msg += "\t-v \t\tVerobse output\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  L *= onedeg;
  B *= onedeg;


  // Set global parameters
  //

  PopModelCacheF::DataDir = DataDir;
  PopModelCacheF::RMIN = RMIN;
  PopModelCacheF::RMAX = RMAX;
  PopModelCacheF::NU = NU;
  PopModelCacheF::A1 = A1;
  PopModelCacheF::Z1 = Z1;
  PopModelCacheF::EFAC = EFAC;
  PopModelCacheF::METAL = METAL;
  PopModelCacheF::RING = RING;
  PopModelCacheF::zerotol = zerotol;
  PopModelCacheF::RLOG = RLOG;
  if (Model == Burst) {		// Sanity check on BURST value
    if (BURST<0.0) BURST = 0.0;
    if (BURST>1.0) BURST = 1.0;
    PopModelCacheF::BRmodel = Burst;
    PopModelCacheF::BURST = BURST;
  }

				// Set ring extinction parameters
  if (RING) {
    ifstream ifile(extfile.c_str());
    char line[256];

    while (1) {

      ifile.getline(line, 256); 

      if (strlen(line)==0) break;
      
      istringstream istr(line);
      string(label);
      istr >> label;

      if (label == string("RINGWIDTH"))
	istr >> PopModelCacheF::RINGWIDTH;
      else if (label == string("RINGLOC"))
	istr >> PopModelCacheF::RINGLOC;
      else if (label == string("RINGAMP"))
	istr >> PopModelCacheF::RINGAMP;
      else if (label == string("CENTERWIDTH"))
	istr >> PopModelCacheF::CENTERWIDTH;
      else if (label == string("CENTERAMP"))
	istr >> PopModelCacheF::CENTERAMP;
      else if (label == string("NEARBYWIDTH"))
	istr >> PopModelCacheF::NEARBYWIDTH;
      else if (label == string("NEARBYAMP"))
	istr >> PopModelCacheF::NEARBYAMP;
      else {
	cerr << "No such key: " << label << "\n";
	exit(-1);
      }

    }

  }

  // Hold instantiation of classes define the simuation
  //
  PopModelCacheF *model;
  HistogramND *histo;

  int ndim = 2;			// Two model parameters

				// Factor for histograms in each tile
  nf = (int)((Fmax-Fmin-1.0e-8)/Fwid) + 1;
  clivectord lo_b(2, Fmin-0.5*Fwid), hi_b(2, Fmax+0.5*Fwid), w_b(2, Fwid);

  RecordType * rt = new RecordType();
  rt = rt->insertField(1, "flux1", BasicType::Real);
  rt = rt->insertField(2, "flux2", BasicType::Real);

  histo = new HistogramND(&lo_b, &hi_b, &w_b, rt);

				// This is hardwired to J & K for now . . .
				// To be generalized
  clivectori marge(3);
  marge()[0] = 0;		// Marginalize J-band (yes=1, no=0)
  marge()[1] = 1;		// Marginalize H-band
  marge()[2] = 0;		// Marginalize K-band

  timer.start();
  PopulationsApp *pops = new PopulationsApp(DataDir, &marge);
  timer.stop();
  cout << "Constructing PopulationsApp took " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() )
       << " seconds" << endl;

  unsigned base_mem = get_total_memory(pid);
  cout << "Base total memory=" << base_mem << endl;

				// Galaxy model
  model = new PopModelCacheF(ndim, nmix, NUM, histo, pops);

  vector<double> winit, *pinit;
  pinit = new vector<double> [nmix];

  ifstream initf(initfile.c_str());
  winit = vector<double>(nmix);
  int nmix1;
  initf >> nmix1;
  if (nmix1 != nmix) {
    cerr << "Inconsistent input: wrong number components, nmix=" 
	 << nmix << endl;
    exit(-1);
  }
  for (int i=0; i<nmix; i++) {
    initf >> winit[i];
    pinit[i] = vector<double>(ndim);
    for (int j=0; j<ndim; j++) initf >> pinit[i][j];
  }
  
  model->Initialize(winit, pinit);

  unsigned mem1 = get_total_memory(pid);
  cout << "Memory after initialization=" << mem1 
       << ", delta=" << mem1 - base_mem << endl;
  

  double norm = model->NormEval(L, B, histo);

  unsigned mem2 = get_total_memory(pid);
  cout << "Memory after normeval=" << mem2
       << ", delta=" << mem2 - mem1 << endl;
  

  cout << " Norm = " << norm << endl;

  vector<double> ans = model->Evaluate(L, B, histo);

  unsigned mem3 = get_total_memory(pid);
  cout << "Memory after evaluation=" << mem3
       << ", delta=" << mem3 - mem2 << endl;

  dat.setsize(0, nf-1, 0, nf-1);
  dat.zero();

  for (int i=0; i<histo->numberData(); i++) {

    if (verbose) {
      cout
	<< setw(15) << histo->getLow(i)[0]
	<< setw(15) << histo->getHigh(i)[0]
	<< setw(15) << histo->getLow(i)[1]
	<< setw(15) << histo->getHigh(i)[1]
	<< setw(15) << ans[i]/norm
	<< endl;
    }
    
    int ii = 
      (int)( (0.5*(histo->getLow(i)[0] + histo->getHigh(i)[0]) - Fmin 
	      - 0.5*Fwid)/Fwid );

    int jj = 
      (int)( (0.5*(histo->getLow(i)[1] + histo->getHigh(i)[1]) - Fmin
	      - 0.5*Fwid)/Fwid );

    if (ii>=0 && ii<nf && jj>=0 && jj<nf) dat[ii][jj] = ans[i]/norm;
    
  }

  float z;
  string fout;
				// J vs K distribution

  fout = "jk." + outfile;
  ofstream out(fout.c_str());
  out.write(reinterpret_cast<char*>(&nf), sizeof(int));
  out.write(reinterpret_cast<char*>(&nf), sizeof(int));
  out.write(reinterpret_cast<char*>(&(z=Fmin)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmax)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmin)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmax)), sizeof(float));
  for (int j=0; j<nf; j++) {
    for (int i=0; i<nf; i++) {
      out.write(reinterpret_cast<char*>(&(z=dat[i][j])), sizeof(float));
    }
  }

				// J-K vs K distribution
  double  jkmin = -0.5;
  double  jkmax =  2.5;
  double  djk = (jkmax - jkmin)/(nout - 1);
  double  dK = (Fmax - Fmin)/(nout - 1);
  double  JK, K, normcmd=0.0;

  MatrixM cmd(0, nout-1, 0, nout-1);
  VectorM lum(0, nout-1); lum.zero();

  for (int j=0; j<nout; j++) {
    K = Fmin + dK*j;
    for (int i=0; i<nout; i++) {
      JK = jkmin + djk*i;
      cmd[i][j] = getValue(JK+K, K);
      lum[j] += cmd[i][j];
      normcmd += cmd[i][j];
    }
  }
  cmd /= normcmd;
  lum /= normcmd;

  fout = "jkk." + outfile;
  ofstream out2(fout.c_str());
  out2.write(reinterpret_cast<char*>(&nout), sizeof(int));
  out2.write(reinterpret_cast<char*>(&nout), sizeof(int));
  out2.write(reinterpret_cast<char*>(&(z=jkmin)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=jkmax)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=Fmax)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=Fmin)), sizeof(float));
  for (int j=nout-1; j>=0; j--) {
    for (int i=0; i<nout; i++) {
      JK = jkmin + djk*i;
      out2.write(reinterpret_cast<char*>(&(z=cmd[i][j])), sizeof(float));
    }
  }

  // Luminosity function

  fout = "klum." + outfile;
  ofstream out3(fout.c_str());
  for (int j=0; j<nout; j++) {
    out3 << setw(15) << Fmin + dK*(0.5+j) << setw(15) << lum[j] << endl;
  }


  // Do some additional evaluations to check memory usage

  if (mcheck) {

    const int nummods = 5;
    unsigned lastmem = mem3, curmem;
    for (int i=0; i<nummods; i++) {
      
      L += onedeg*(i+1);
      B += onedeg*(i+1);
      
      timer.reset();
      timer.start();
      model->Initialize(winit, pinit);
      norm = model->NormEval(L, B, histo);
      ans = model->Evaluate(L, B, histo);
      timer.stop();
      
      cout << " Norm[L=" << L/onedeg << ", B=" << B/onedeg << "] = " 
	   << norm << endl;

      curmem = get_total_memory(pid);
      cout << "Memory after model[" << i << "] =" << curmem
	   << ", delta=" << curmem - lastmem << endl;
      lastmem = curmem;

      cout << "Model computation took " 
	   << 1.0e-6*(timer.getTime().getUserTime() +
		      timer.getTime().getSystemTime() )
	   << " seconds" << endl;
      
      timer.reset();
      timer.start();
      model->Initialize(winit, pinit);
      norm = model->NormEval(L, B, histo);
      ans = model->Evaluate(L, B, histo);
      timer.stop();
      
      cout << " Norm[L=" << L/onedeg << ", B=" << B/onedeg << "] = " 
	   << norm << endl;
      
      cout << "Model reevaluation took " 
	   << 1.0e-6*(timer.getTime().getUserTime() +
		      timer.getTime().getSystemTime() )
	   << " seconds" << endl;
    }

  }
  
  cout << endl;
  model->PrintCache();
  cout << endl;

  return 0;
}
