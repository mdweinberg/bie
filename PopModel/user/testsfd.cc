#include <unistd.h>
#include <stdlib.h>
#include <math.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

#include <Timer.h>
#include <SFD.H>

const double onedeg = 180.0/M_PI;

void getsfd(float l, float b, float q, 
	    float& ebv, float& sig, float& emn, float& emx,
	    const string& DataDir);

int
main(int argc, char **argv)
{
  string outfile = "sfd.surface";
  bool surface = false;
  int numl = 40, numb = 40;

  int c;
  while (1) {
    c = getopt (argc, argv, "f:l:b:sh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'f': outfile.erase(); outfile = optarg; break;
      case 'l': numl = atoi(optarg); break;
      case 'b': numb = atoi(optarg); break;
      case 's': surface = true; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-f file\t\toutput file\n";
	msg += "\t-s x\t\tlogitudinal bins\n";
	msg += "\t-s x\t\tlatitudinal bins\n";
	msg += "\t-s x\t\tmake surface plot\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  Timer timer(true);

  timer.start();
  string datadir("/home/weinberg/3Mtab");
  SFD sfd(datadir, true);
  timer.stop();
  
  cout << "Constructing SFD took " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() )
       << " seconds" << endl;

  string datafile = datadir + "/sfd98_rtab.txt\0";

  ifstream in(datafile.c_str());
  if (!in) {
    cerr << "testsfd: couldn't open data file <" << datafile << ">\n";
    exit(-1);
  }

  const int bufsize = 2048;
  char line[bufsize];
  const int nlin=257662;

  float lon, lat, red, var, rmn, rmx;
  float ebv, sig, emn, emx;

  float ebvmax=0.0, sigmax=0.0, emnmax=0.0, emxmax=0.0;

  timer.reset();
  timer.start();

  for (int j=1; j<=nlin; j++) {

    in.getline(line, bufsize);
    istringstream ins(line);
    
    ins >> lon;
    ins >> lat;
    ins >> red;
    ins >> var;
    ins >> rmn;
    ins >> rmx;

    // sfd.getsfd(lon, lat, 0.0001, ebv, sig, emn, emx);
    sfd.getsfd_closest(lon+0.01, lat+0.01, ebv, sig, emn, emx);

    ebvmax = max<float>(fabs(red-ebv), ebvmax);
    sigmax = max<float>(fabs(var-sig), sigmax);
    emnmax = max<float>(fabs(rmn-emn), emnmax);
    emxmax = max<float>(fabs(rmx-emx), emxmax);
  }

  timer.stop();
  
  cout << "SFD check took " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() )
       << " seconds which is " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() ) / nlin
       << " seconds per evaluation" << endl;
      
  cout << endl;

  cout << "Max differences:" << endl
       << "  Ebv=" << ebvmax << endl
       << "  Sig=" << sigmax << endl
       << "  Emn=" << emnmax << endl
       << "  Emx=" << emxmax << endl;



  ebvmax=0.0;
  sigmax=0.0;
  emnmax=0.0;
  emxmax=0.0;

  in.close();
  in.open(datafile.c_str());

  timer.reset();
  timer.start();

  float ebv1, sig1, emn1, emx1;

  for (int j=1; j<=nlin; j++) {

    in.getline(line, bufsize);
    istringstream ins(line);
    
    ins >> lon;
    ins >> lat;

    sfd.getsfd(lon, lat, 1.0, ebv, sig, emn, emx);
    getsfd(lon, lat, 1.0, ebv1, sig1, emn1, emx1, datadir);
    
    ebvmax = max<float>(fabs(ebv1-ebv), ebvmax);
    sigmax = max<float>(fabs(sig1-sig), sigmax);
    emnmax = max<float>(fabs(emn1-emn), emnmax);
    emxmax = max<float>(fabs(emx1-emx), emxmax);
  }

  timer.stop();
  
  cout << "SFD radius check took " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() )
       << " seconds which is " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() ) / nlin
       << " seconds per evaluation" << endl;
      
  cout << endl;

  cout << "Max differences:" << endl
       << "  Ebv=" << ebvmax << endl
       << "  Sig=" << sigmax << endl
       << "  Emn=" << emnmax << endl
       << "  Emx=" << emxmax << endl;



  if (surface) {

    double dl = 2.0*M_PI/(numl-1);
    double db = M_PI/(numb-1);
    float z;
    
    ofstream fout(outfile.c_str());
    fout.write(reinterpret_cast<char*>(&numl), sizeof(int));
    fout.write(reinterpret_cast<char*>(&numb), sizeof(int));
    fout.write(reinterpret_cast<char*>(&(z=-M_PI)), sizeof(float));
    fout.write(reinterpret_cast<char*>(&(z= M_PI)), sizeof(float));
    fout.write(reinterpret_cast<char*>(&(z=-0.5*M_PI)), sizeof(float));
    fout.write(reinterpret_cast<char*>(&(z= 0.5*M_PI)), sizeof(float));

    for (int k=0; k<numb; k++) {
      lat = -0.5*M_PI + db*k;
      for (int j=0; j<numl; j++) {
	lon = -M_PI + dl*j;
	sfd.getsfd_closest(lon*onedeg, lat*onedeg, ebv, sig, emn, emx);
	fout.write(reinterpret_cast<char*>(&ebv), sizeof(float));
      }
    }

  }

  return 0;
}

