
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

#include <unistd.h>
#include <math.h>


void usage(char *prog)
{
  cout << "Usage: " << prog 
       << " [-x xmin -X xmax -y ymin -Y ymax -c dx -k dy -i -H]\n";
  exit(0);
}


int main(int argc, char **argv)
{
  double xmin = 0.05;
  double xmax = 18.45;
  double ymin = -0.2375;
  double ymax = 1.7375;
  double dx = 0.1;
  double dy = 0.025;
  bool Hband = false;
  bool inverse = false;
  
  int c;

  while (1) {
    
    c = getopt(argc, argv, "x:X:y:Y:c:k:iHh");
    if (c == -1) break;

    switch (c) {

    case 'x':
      xmin = atof(optarg);
      break;

    case 'X':
      xmax  = atof(optarg);
      break;

    case 'y':
      ymin = atof(optarg);
      break;

    case 'Y':
      ymax = atof(optarg);
      break;

    case 'c':
      dx = atof(optarg);
      break;

    case 'k':
      dy = atof(optarg);
      break;

    case 'i':
      inverse = true;
      break;

    case 'H':
      Hband = true;
      break;

    case 'h':
    default:
      usage(argv[0]);
    }

  }

  int nx = (int)( (xmax-xmin)/dx + 1.0e-8);
  int ny = (int)( (ymax-ymin)/dy + 1.0e-8);
  
  vector< vector<double> > data(nx);
  for(int i=0; i<nx; i++) data[i] = vector<double>(ny, 0.0);
    
  double metal, dmod, x, y, jk, jh, val;
  int ix, iy;

  while (cin) {
    cin >> metal;
    cin >> dmod;
    cin >> x;
    cin >> jk;
    cin >> jh;
    cin >> val;

    if (Hband)
      y = jh;
    else
      y = jk;

    ix = (int)( (x-xmin)/dx + 0.5);
    iy = (int)( (y-ymin)/dy + 0.5);

    if (ix < 0 || ix >=nx ||
	iy < 0 || iy >=ny ) 
      {
	cerr << "out of bounds: x,y=" << x << ", " << y << "\n";
	continue;
      }
    
    // Debug

    if ( fabs(x-(xmin+dx*ix)) > 1.0e-10) {
      cerr << "Error: x=" << x << "  y=" << y 
	   << "  dx=" << x-(xmin+dx*ix) << "\n";
    }

    if ( fabs(y-(ymin+dy*iy)) > 1.0e-10) {
      cerr << "Error: x=" << x << "  y=" << y 
	   << "  dy=" << y-(ymin+dy*iy) << "\n";
    }

    data[ix][iy] += val;
  }

  float f;

  cout.write(reinterpret_cast<char*>(&ny), sizeof(int));
  cout.write(reinterpret_cast<char*>(&nx), sizeof(int));
  cout.write(reinterpret_cast<char*>(&(f=ymin)), sizeof(float));
  cout.write(reinterpret_cast<char*>(&(f=ymax)), sizeof(float));
  if (inverse) {
    cout.write(reinterpret_cast<char*>(&(f=xmax)), sizeof(float));
    cout.write(reinterpret_cast<char*>(&(f=xmin)), sizeof(float));
    for (int i=nx-1; i>-1; i--) 
      for (int j=0; j<ny; j++)
	cout.write(reinterpret_cast<char*>(&(f=data[i][j])), sizeof(float));

  } else {
    cout.write(reinterpret_cast<char*>(&(f=xmin)), sizeof(float));
    cout.write(reinterpret_cast<char*>(&(f=xmax)), sizeof(float));
    for (int i=0; i<nx; i++) 
      for (int j=0; j<ny; j++)
	cout.write(reinterpret_cast<char*>(&(f=data[i][j])), sizeof(float));
  }


  return 0;
}
