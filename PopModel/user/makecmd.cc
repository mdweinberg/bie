// -*- C++ -*-

/* 
   Make a color-magnitude diagram
*/

#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;


#include <math.h>


#include <Populations.h>

using namespace BIE;

int
main(int argc, char** argv)
{
  string dir("/data/weinberg/3Mtab");

  clivectori marge(3);
  marge()[0] = 0;		// Marginalize J-band (yes=1, no=0)
  marge()[1] = 1;		// Marginalize H-band
  marge()[2] = 0;		// Marginalize K-band
  Populations pops(dir, &marge);

  /*
    contrh style output
  */

  int nx=20;
  int ny=100;

  float Kmin = 12.7;
  float Kmax = -9.0;
  float dK = (Kmax - Kmin)/(ny-1);

  float JKmin = -0.5;
  float JKmax =  2.0;
  float dJK = (JKmax - JKmin)/(nx-1);
  float z;

  ofstream out("test.dat");

  out.write(reinterpret_cast<char*>(&nx), sizeof(int));
  out.write(reinterpret_cast<char*>(&ny), sizeof(int));
  out.write(reinterpret_cast<char*>(&JKmin), sizeof(float));
  out.write(reinterpret_cast<char*>(&JKmax), sizeof(float));
  out.write(reinterpret_cast<char*>(&Kmin), sizeof(float));
  out.write(reinterpret_cast<char*>(&Kmax), sizeof(float));

  vector<double> fmin(2);
  vector<double> fmax(2);
  vector<double> ages = pops.Ages();

  for (int j=0; j<ny; j++) {

    fmax[1] = Kmin + dK*((double)j - 0.5);
    fmin[1] = Kmin + dK*((double)j + 0.5);

    for (int i=0; i<nx; i++) {
      
      fmin[0] = JKmin + dJK*i + fmin[1];
      fmax[0] = JKmin + dJK*i + fmax[1];

      z = 0.0;
      for (int n=0; n<(int)ages.size(); n++) {
	z += pops.Bin(fmin, fmax, ages[n], 0.03);
      }

      out.write(reinterpret_cast<char*>(&z), sizeof(float));
    }
  }
      
  return 0;
}

