// -*- C++ -*-

/* 
   Test popmodel
*/

#define MARGE			// Define for marginalization test

#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;


#include <math.h>

#include <Populations.h>

using namespace BIE;

int
main(int argc, char** argv)
{
  double METAL = 0.008;
  double TIME = 6.15;
  string dir("/home/weinberg/3Mtab");

  int c;
  while (1) {
    c = getopt (argc, argv, "m:t:d:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'm': METAL = atof(optarg); break;
      case 't': TIME = atof(optarg); break;
      case 'd': dir.erase(); dir = optarg; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-m z\t\tmetallicity\n";
	msg += "\t-m t\t\ttime\n";
	msg += "\t-d path\t\tisochone data directory\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

#ifndef MARGE
  Populations pops(dir);
#else
  clivectori marge(3);
  marge()[0] = 0;		// Marginalize J-band (yes=1, no=0)
  marge()[1] = 1;		// Marginalize H-band
  marge()[2] = 0;		// Marginalize K-band
  Populations pops(dir, &marge);
#endif

  vector<double> fmin = pops.MinMag();
  vector<double> fmax = pops.MaxMag();

  cout << "Integral: " << pops.Bin(fmin, fmax, TIME, METAL) << "\n";

#ifndef MARGE  
  cout << "--------------------\n";
  cout << "Integer accessor: ";
  pops.VerifyInt();

  cout << "--------------------\n";
  cout << "Double accessor: ";
  pops.VerifyDouble();

  cout << "--------------------\n";
#endif

  return 0;
}

