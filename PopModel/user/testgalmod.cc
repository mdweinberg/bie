// This may look like C code, but it is really -*- C++ -*-

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <BasicType.h>

#define IS_MAIN

#include <HistogramND.h>
#include <PopModelND.h>

using namespace BIE;

MatrixM dat;
int nf;
double Fmin = 6.0, Fmax = 16.0, Fwid = 0.25;

/**
   Bilinear interpolation on J & K grid
*/
double getValue(double J, double K)
{
  if (J<Fmin || J>=Fmax || K<Fmin || K>=Fmax) return 0.0;

  int ij = (int)( (J-Fmin)/Fwid );
  int ik = (int)( (K-Fmin)/Fwid );

				// Pin at grid boundaries

  if (ij<0) ij=0;
  if (ik<0) ik=0;
  if (ij>nf-2) ij=nf-2;
  if (ik>nf-2) ik=nf-2;

				// Weight factors
  double j2 = (J - (Fmin + Fwid*(0.5+ij)))/Fwid;
  double j1 = 1.0 - j2;

  double k2 = (K - (Fmin + Fwid*(0.5+ik)))/Fwid;
  double k1 = 1.0 - k2;

				// Do not interpolate past center of
				// grid edge
  if (j2 > 1.0) {j1 = 0.0; j2 = 1.0;}
  if (j2 < 0.0) {j1 = 1.0; j2 = 0.0;}
  if (k2 > 1.0) {k1 = 0.0; k2 = 1.0;}
  if (k2 < 0.0) {k1 = 1.0; k2 = 0.0;}

  return 
    dat[ij  ][ik  ]*j1*k1 + dat[ij+1][ik  ]*j2*k1 +
    dat[ij  ][ik+1]*j1*k2 + dat[ij+1][ik+1]*j2*k2 ;
}

/**
   @name testgalmod: main
   Check out of PopModelND (two flux case)
*/
int
main(int argc, char** argv)
{
  const double onedeg = M_PI/180.0;

				// Line of sight
  double L = 30.0;
  double B = 30.0;
				// Maximum line-of-sight distance
  double RMAX = 100.0;
				// Integration knots
  int NUM = 200;
				// Power law birth rate index
  double NU = 1.0;
				// Default init file
  string initfile = "init.dat.galaxy";
				// Output suffix
  string outfile = "dat";
				// Data directory
  string DataDir = "/data/weinberg/3Mtab";

  int nmix = 1;			// One component mixture


  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "L:B:R:M:i:o:d:n:a:w:1:2:xh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'L': L = atof(optarg); break;
      case 'B': B = atof(optarg); break;
      case 'R': RMAX = atof(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'n': NUM = atoi(optarg); break;
      case 'a': NU = atof(optarg); break;
      case '1': Fmin = atof(optarg); break;
      case '2': Fmax = atof(optarg); break;
      case 'w': Fwid = atof(optarg); break;
      case 'i': initfile.erase(); initfile = optarg; break;
      case 'o': outfile.erase(); outfile = optarg; break;
      case 'd': DataDir.erase(); DataDir = optarg; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-L L\t\tlongitude in degrees\n";
	msg += "\t-B B\t\tlatitude in degrees\n";
	msg += "\t-R R\t\tline-of-sight integration maximum\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-n n\t\tnumber of integration knots\n";
	msg += "\t-i file\t\tname of initialization file\n";
	msg += "\t-o file\t\toutput file suffix\n";
	msg += "\t-d path\t\tdata directory\n";
	msg += "\t-a Nu\t\tpowerlaw birthrate exponent\n";
	msg += "\t-1 flux\t\tminimum\n";
	msg += "\t-2 flux\t\tmaximum\n";
	msg += "\t-w size\t\tJ & K bin width\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  L *= onedeg;
  B *= onedeg;

  // Set global parameters
  //

  PopModelND::DataDir = DataDir;
  PopModelND::RMAX = RMAX;
  PopModelND::NUM = NUM;
  PopModelND::NU = NU;

  // Hold instantiation of classes define the simuation
  //
  PopModelND *model;
  HistogramND *histo;

  int ndim = 2;			// Two model parameters

				// Factor for histograms in each tile
  nf = (int)((Fmax-Fmin-1.0e-8)/Fwid);
  clivectord lo_b(2, Fmin), hi_b(2, Fmax), w_b(2, Fwid);

  RecordType * rt = new RecordType();
  rt = rt->insertField(1, "flux1", BasicType::Real);
  rt = rt->insertField(2, "flux2", BasicType::Real);

  histo = new HistogramND(&lo_b, &hi_b, &w_b, rt);

				// Galaxy model
  model = new PopModelND(ndim, nmix, histo);

  vector<double> winit, *pinit;
  pinit = new vector<double> [nmix];

  ifstream initf(initfile.c_str());
  winit = vector<double>(nmix);
  int nmix1;
  initf >> nmix1;
  if (nmix != nmix1) {
    cerr << "Mismatch wit # mixtures in initial state\n";
    exit(-1);
  }
  for (int i=0; i<nmix; i++) {
    initf >> winit[i];
    pinit[i] = vector<double>(ndim);
    for (int j=0; j<ndim; j++) initf >> pinit[i][j];
  }
  
  model->Initialize(winit, pinit);
  double norm = model->NormEval(L, B, histo);

  cout << " Norm = " << norm << endl;

  model->ResetCache();

  vector<double> ans = model->Evaluate(L, B, histo);

  dat.setsize(0, nf-1, 0, nf-1);
  dat.zero();

  for (int i=0; i<histo->numberData(); i++) {
    cout
      << setw(15) << histo->getLow(i)[0]
      << setw(15) << histo->getHigh(i)[0]
      << setw(15) << histo->getLow(i)[1]
      << setw(15) << histo->getHigh(i)[1]
      << setw(15) << ans[i]/norm
      << endl;
    
    int ii = 
      (int)( (0.5*(histo->getLow(i)[0] + histo->getHigh(i)[0]) - Fmin )/Fwid );

    int jj = 
      (int)( (0.5*(histo->getLow(i)[1] + histo->getHigh(i)[1]) - Fmin )/Fwid );

    if (ii>=0 && ii<nf && jj>=0 && jj<nf) dat[ii][jj] = ans[i]/norm;
    
  }

  float z;
  string fout;
				// J vs K distribution

  fout = "jk." + outfile;
  ofstream out(fout.c_str());
  out.write(reinterpret_cast<char*>(&nf), sizeof(int));
  out.write(reinterpret_cast<char*>(&nf), sizeof(int));
  out.write(reinterpret_cast<char*>(&(z=Fmin)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmax)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmin)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmax)), sizeof(float));
  for (int j=0; j<nf; j++) {
    for (int i=0; i<nf; i++) {
      out.write(reinterpret_cast<char*>(&(z=dat[i][j])), sizeof(float));
    }
  }

				// J-K vs K distribution
  double  jkmin = -0.5;
  double  jkmax =  2.5;
  double  djk = (jkmax - jkmin)/(nf-1);
  double  JK, K, normcmd=0.0;

  MatrixM cmd(0, nf-1, 0, nf-1);
  VectorM lum(0, nf-1); lum.zero();

  for (int j=0; j<nf; j++) {
    K = Fmin + Fwid*j;
    for (int i=0; i<nf; i++) {
      JK = jkmin + djk*i;
      cmd[i][j] = getValue(JK+K, K);
      lum[j] += cmd[i][j];
      normcmd += cmd[i][j];
    }
  }
  cmd /= normcmd;
  lum /= normcmd;

  fout = "jkk." + outfile;
  ofstream out2(fout.c_str());
  out2.write(reinterpret_cast<char*>(&nf), sizeof(int));
  out2.write(reinterpret_cast<char*>(&nf), sizeof(int));
  out2.write(reinterpret_cast<char*>(&(z=jkmin)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=jkmax)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=Fmax)), sizeof(float));
  out2.write(reinterpret_cast<char*>(&(z=Fmin)), sizeof(float));
  for (int j=nf-1; j>=0; j--) {
    for (int i=0; i<nf; i++) {
      JK = jkmin + djk*i;
      out2.write(reinterpret_cast<char*>(&(z=cmd[i][j])), sizeof(float));
    }
  }

  fout = "klum." + outfile;
  ofstream out3(fout.c_str());
  for (int j=0; j<nf; j++)
    out3 << setw(15) << Fmin + Fwid*(0.5+j)
	 << setw(15) << lum[j]
	 << endl;

  return 0;
}


