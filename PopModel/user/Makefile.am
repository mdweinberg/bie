## Makefile for the PopModel directory of galaxy.
##
## Copyright (C) 2001 Martin Weinberg
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

AUTOMAKE_OPTIONS = dejagnu
RUNTESTDEFAULTFLAGS = --tool alltests --srcdir $(srcdir)/testsuite

## Process this file with automake to produce Makefile.in

# Include flags.  It does not matter that some of these locations might not exist.

noinst_PROGRAMS = testpops testpopsdm makecmd makecmdm testgal		\
	testgalnc testgalcf testgalsfcm makegalnc testgalsf ratebasis	\
	testcmd testgalcmd testsfd makecdata compgalcmd slice sliceP

AM_CXXFLAGS = -std=c++11 -Wall @bie_cxxflags@ @bie_debug_flags@ -D_REENTRANT

AM_CPPFLAGS	= -I$(top_srcdir)/include -I$(top_srcdir)/libVector \
	          -I$(top_srcdir)/libsrnd @INCLUDES_ALL@ @INCLUDES_CCXX@

# Libraries to link in.

BIE_LIBS = -L$(top_srcdir)/libVector/.libs				\
-L$(top_srcdir)/libsrnd/.libs -L$(top_srcdir)/src/.libs			\
-L$(top_srcdir)/persistence/.libs -L$(top_srcdir)/PopModel/.libs	\
-L$(top_srcdir)/Galphat/.libs -L$(top_srcdir)/Reflect/.libs

BIE_ADD = @LDADD_ALL@ @bie_vtk_lib@ @bie_vtk_ldlib@ -lm -lnetcdf	\
-lreadline -ltermcap @LIBADD_CCXX@ -lgsl -lgslcblas -lfftw3 -lcfitsio	\
-lpthread -lfl

BIE_FLAGS = @LDFLAGS_ALL@ -L$(top_srcdir)/PopModel/.libs -lPopModel

AM_LIBTOOLFLAGS = --preserve-dup-deps

testpops_SOURCES  = testpops.cc
testpops_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testpopsdm_SOURCES  = testpopsdm.cc
testpopsdm_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

makecmd_SOURCES  = makecmd.cc
makecmd_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

makecmdm_SOURCES  = makecmdm.cc
makecmdm_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testgal_SOURCES  = testgalmod.cc
testgal_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testgalnc_SOURCES = testgalmod2.cc
testgalnc_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testgalcf_SOURCES = testgalmod3.cc
testgalcf_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

makegalnc_SOURCES = makegalmod2.cc
makegalnc_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testgalsf_SOURCES = testgalmod4.cc
testgalsf_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testgalsfcm_SOURCES = testgalmod5.cc
testgalsfcm_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

ratebasis_SOURCES = ratebasis.cc
ratebasis_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testcmd_SOURCES = testcmd.cc
testcmd_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testgalcmd_SOURCES = testgalcmd.cc
testgalcmd_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

testsfd_SOURCES = testsfd.cc
testsfd_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

makecdata_SOURCES = makecdata.cc MakeStar.cc
makecdata_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

compgalcmd_SOURCES = compgalcmd.cc
compgalcmd_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

slice_SOURCES = sliceSimple.cc
slice_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

sliceP_SOURCES = slicePopApp.cc
sliceP_LDADD    =  ../../Reflect/.libs/*.o $(BIE_FLAGS) $(BIE_ADD)

## PopModel/Makefile.am ends here
