// -*- C++ -*-

/* 
   Make a color-magnitude diagram
*/

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>

using namespace std;

#include <PopulationsApp.h>

using namespace BIE;

void usage(char *prog)
{
  cout << prog << ": [-c -d directory -m distmod -h]\n";
  exit(0);
}

int
main(int argc, char** argv)
{
  string dir("/data/weinberg/3Mtab");
  bool cmd = false;
  double distmod = 3.70;

  int c;
  while (1) {
    c = getopt (argc, argv, "m:cd:h");
    if (c == -1) break;

    switch (c) {
    
    case 'm':
      distmod = atof(optarg);
      break;

    case 'c':
      cmd = true;
      break;

    case 'd':
      dir.erase();
      dir = optarg;
      break;

    case 'h':
    case '?':
      usage(argv[0]);
      break;

    default:
      cout << "?? getopt returned character code " << oct << c << "\n";
      break;
    }
  }

  
				// Direct flux  (yes=1, no=0)             CMD
  clivectori marge(3);          // ------------------                     ------
  marge()[0] = 0;               // Marginalize J-band                     K-band
  marge()[1] = 1;               // Marginalize H-band                     J-H
  marge()[2] = 0;               // Marginalize K-band                     J-K

  PopulationsApp pops(dir, &marge, cmd);

  /*
    contrh style output
  */

  int nx=100;
  int ny=100;

  float Kmin = 18.5;
  float Kmax = 0.0;
  float dK = (Kmax - Kmin)/(ny-1);

  float JKmin = -0.5;
  float JKmax =  2.0;
  float dJK = (JKmax - JKmin)/(nx-1);
  float z;

  vector<double> ages = pops.Ages();
  ofstream *out = new ofstream [ages.size()];
  for (int n=0; n<(int)ages.size(); n++) {
    ostringstream name;
    name << "cmdage." << n;
    out[n].open(name.str().c_str());

    out[n].write(reinterpret_cast<char*>(&nx), sizeof(int));
    out[n].write(reinterpret_cast<char*>(&ny), sizeof(int));
    out[n].write(reinterpret_cast<char*>(&JKmin), sizeof(float));
    out[n].write(reinterpret_cast<char*>(&JKmax), sizeof(float));
    out[n].write(reinterpret_cast<char*>(&Kmin), sizeof(float));
    out[n].write(reinterpret_cast<char*>(&Kmax), sizeof(float));
  }

  vector<double> fmin(2);
  vector<double> fmax(2);
  double K[2];

  for (int j=0; j<ny; j++) {

    K[0] = Kmin + dK*((double)j - 0.5);
    K[1] = Kmin + dK*((double)j + 0.5);

    for (int i=0; i<nx; i++) {
      
      if (cmd) {
	fmin[0] = K[1];
	fmax[0] = K[0];
	fmin[1] = JKmin + dJK*(-0.5+i);
	fmax[1] = JKmin + dJK*( 0.5+i);
      } else {
	fmin[1] = K[1];
	fmax[1] = K[0];
	fmin[0] = JKmin + dJK*i + fmin[1];
	fmax[0] = JKmin + dJK*i + fmax[1];
      }

      for (int n=0; n<(int)ages.size(); n++) {
	z = pops.Bin(fmin, fmax, ages[n], 0.0190, distmod);
	out[n].write(reinterpret_cast<char*>(&z), sizeof(float));
      }

    }
  }
      
  for (int n=0; n<(int)ages.size(); n++) out[n].close();
  delete [] out;

  return 0;
}

