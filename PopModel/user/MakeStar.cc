#include <MakeStar.H>

using namespace std;

int MakeStar::MAXITER = 1000;
double MakeStar::TOL = 1.0e-10;
double MakeStar::R0 = 8.0;
double MakeStar::Z0 = 15.0;

MakeStar::MakeStar(long int seed, double a, double h)
{
  A = a;
  H = h;

  gen = new ACG(seed);
  unit = new Uniform(0.0, 1.0, gen);
}

MakeStar::~MakeStar()
{
  delete gen;
  delete unit;
}


vector<double> MakeStar::realize()
{
    
  vector<double> ret(3);

  double fac, del, rran = log(1.0 - (*unit)());
  double z = 1.0e-3*(H*atanh(2.0*(*unit)() - 1.0) - Z0);

  double r = -rran;
  for (int i=0; i < MAXITER; i++) {
    fac = 1.0 + r;
    del = fac*(log(fac) - r - rran)/r;
    r += del;
    if (fabs(del) < TOL) break;
  }
  r *= A;
  

  double phi = 2.0*M_PI*(*unit)();
  double x = r*cos(phi) + R0;
  double y = r*sin(phi);
  double s = sqrt(x*x + y*y + z*z);
  
  double L = atan2(y, x);
  double B = asin(z/s);
  
  ret[0] = L;
  ret[1] = B;
  ret[2] = s;

  return ret;
}

