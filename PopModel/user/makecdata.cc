// -*- C++ -*-

/* 
   Realize data from an exponential disk with fluxes according to CMD
*/

#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;


#include <math.h>



#include <ACG.h>
#include <Uniform.h>

#include <CMD.h>
#include <SFDS.H>
#include <MakeStar.H>

using namespace BIE;

const double onedeg = 180.0/M_PI;

int
main(int argc, char** argv)
{
  string cmdfile("/data/weinberg/3Mtab/lmc.cmd");
  string outfile("jkk.test");
  string datafile("jkk.data");
  string datadir("/home/weinberg/3Mtab");
  bool printsm = false;
  int realize = 0;
  int num = 300;
  int NUM = 400;
  double A = 3.5;
  double H = 325.0;
  double R0 = 8.0;
  double EFAC = 1.0;
  long int seed = 11;
  bool testdata = false;

  int c;
  while (1) {
    c = getopt (argc, argv, "d:D:f:s:pn:N:A:H:E:i:I:th");
    if (c == -1) break;
     
    switch (c)
      {
      case 'd': cmdfile.erase(); cmdfile = optarg; break;
      case 'D': datadir.erase(); datadir = optarg; break;
      case 'f': outfile.erase(); outfile = optarg;
      case 's': datafile.erase(); datafile = optarg;
      case 'p': printsm = true; break;
      case 'n': num = atoi(optarg); break;
      case 'N': realize = atoi(optarg); break;
      case 't': testdata = true; break;
      case 'A': A = atof(optarg); break;
      case 'H': H = atof(optarg); break;
      case 'E': EFAC = atof(optarg); break;
      case 'i': seed = atoi(optarg); break;
      case 'I': NUM = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-d path\t\tdata file\n";
	msg += "\t-f file\t\toutput file\n";
	msg += "\t-s file\t\tsynthetic data file\n";
	msg += "\t-p \t\tprint cmd\n";
	msg += "\t-n int\t\toutput grid per dimension\n";
	msg += "\t-N int\t\trealize CMD\n";
	msg += "\t-Q int\t\tsmoothing scale for SFD extinction map\n";
	msg += "\t-A z\t\texponential disk scale length\n";
	msg += "\t-H z\t\tdisk scale height\n";
	msg += "\t-E z\t\textinction scale factor\n";
	msg += "\t-t z\t\tprint datafile diagnostics\n";
	msg += "\t-i x\t\tseed for random number generator\n";
	msg += "\t-I x\t\tnumber of knots for line-of-sight integral\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  CMD cmd(cmdfile);

  vector<double> fmin = cmd.MinMag();
  vector<double> fmax = cmd.MaxMag();
  double distmod = cmd.DistMod();

  cout << "Integral: " << cmd.Bin(fmin, fmax, distmod) << "\n";

#ifndef MARGE  
  cout << "--------------------\n";
  cout << "Integer accessor: ";
  cmd.VerifyInt();

  cout << "--------------------\n";
  cout << "Double accessor: ";
  cmd.VerifyDouble(distmod);

  cout << "--------------------\n";
#endif

  if (printsm) {
    vector<double> dx(2);
    dx[0] = (fmax[0] - fmin[0])/(num-1);
    dx[1] = (fmax[1] - fmin[1])/(num-1);
    vector<double> influx(2);
    float z;

    ofstream out(outfile.c_str());
    if (!out) {
      cerr << "Can't open <" << outfile << ">\n";
      exit(0);
    }
    out.write(reinterpret_cast<char*>(&num), sizeof(int));
    out.write(reinterpret_cast<char*>(&num), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmin[0])), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmax[0])), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmin[1])), sizeof(int));
    out.write(reinterpret_cast<char*>(&(z=fmax[1])), sizeof(int));

    for (int j=0; j<num; j++) {
      influx[1] = fmin[1] + dx[1]*j;

      for (int i=0; i<num; i++) {
	influx[0] = fmin[0] + dx[0]*i;

	//	z = cmd.GetValue(influx);
	z = cmd.Interpolate(influx, distmod);
	out.write(reinterpret_cast<char*>(&z), sizeof(float));
      }
    }
  }

  if (realize) {

    ACG gen(seed+1, 20);
    Uniform uniform(0.0, 1.0, &gen);

    ofstream out(datafile.c_str());
    if (!out) {
      cerr << "Couldn't open data file <" << datafile << ">\n";
      exit(0);
    }

				// BIE data stream Header
    if (!testdata) {
      out << "real \"x\"\n";
      out << "real \"y\"\n";
      out << "real \"color\"\n";
      out << "real \"magnitude\"\n";
      out <<  endl;
    }


    SFDS sfd(datadir, 1.0);
    SFDS::NUM = NUM;

    MakeStar star(seed, A, H);
    vector<double> pos, ans;

    float L, B, s, lfrac;
    float ebv, ebvmin, ebvmax, sig;
    float lebv, lebvmin, lebvmax, lsig;
    float aj, ah, ak;

    for (int n=0; n<realize; n++) {
				// Get postion
      pos = star.realize();

      L = pos[0];
      B = pos[1];
      s = pos[2];

				// Get extinction
      sfd.getsfd_distance(L, B, s, lfrac,
			  ebv, ebvmin, ebvmax, sig,
			  lebv, lebvmin, lebvmax, lsig);

      if (uniform()<lfrac) {
	aj = 0.90*exp(lebv);
	ah = 0.53*exp(lebv);
	ak = 0.34*exp(lebv);
      } else {
	aj = ah = ak = 0.0;
      }

				// Get J-K, K
      ans = cmd.genPoint();

      ans[0] += 5.0*log(s*1.0e3)/log(10.0) - 5.0 - distmod + EFAC*ak;
      ans[1] += EFAC*(aj - ak);

      if (testdata) {
	out << setw(15) << L
	    << setw(15) << B
	    << setw(15) << s
	    << setw(15) << s*cos(L)*cos(B) - R0
	    << setw(15) << s*sin(L)*cos(B)
	    << setw(15) << s*sin(B)
	    << setw(15) << ans[1]
	    << setw(15) << ans[0]
	    << endl;
      } else {
	if (pos[0] < 0.0) pos[0] += 2.0*M_PI;
	out << setw(15) << pos[0]
	  // << setw(15) << sin(pos[1])
	    << setw(15) << pos[1]
	    << setw(15) << ans[1]
	    << setw(15) << ans[0]
	    << endl;
      }
    }

  }
  
}

