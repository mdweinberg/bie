// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;

#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
// #include <BasicType.h>

#define IS_MAIN

#include <PopulationsApp.h>
#include <Timer.h>

using namespace BIE;

int nfk, nfc;
double Fmin = 0.05, Fmax = 18.45, Fwid = 0.1;
double Jmax = 15.8, Kmax=14.3;
double Cmin = -0.2375, Cmax = 1.7375, Cwid = 0.025;

/**
   @name testgalmod: main
   Check out of PopModelCacheF (two flux case)
*/
int
main(int argc, char** argv)
{
  Timer timer(true);

				// Metallicity
  double METAL = 0.019;
				// Age
  double AGE = 10.05;
				// Distance mod
  double DISTMOD = 11.11;

				// Output file
  string outfile = "slice.dat";

				// Data directory
  string DataDir = "/data/weinberg/CMDtab";

				// Data directory
  string CDFfile = "isochrone2.cdf";

  bool CDF = false;

  bool interp = false;

  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "a:z:m:o:d:f:Clh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'a': AGE = atof(optarg); break;
      case 'z': METAL = atof(optarg); break;
      case 'm': DISTMOD = atof(optarg); break;
      case 'o': outfile.erase(); outfile = optarg; break;
      case 'd': DataDir.erase(); DataDir = optarg; break;
      case 'f': CDFfile.erase(); CDFfile = optarg; break;
      case 'C': CDF = true; break;
      case 'l': interp = true; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-a float\t\tAge (default: 10.05)\n";
	msg += "\t-z float\t\tMetallicity (default: 0.019)\n";
	msg += "\t-m float\t\tDistance modulus (default: 11.11)\n";
	msg += "\t-o file \t\toutput file suffix (default: slice.dat)\n";
	msg += "\t-d path \t\tisochrone data directory\n";
	msg += "\t-f path \t\tisochrone CDF data file\n";
	msg += "\t-C      \t\tuse CDF isochrone file\n";
	msg += "\t-l      \t\tinterpolate in grid (default: binning)\n";
	msg += "\t-h      \t\tthis help message\n";
	cerr << msg; exit(0);
      }
  }

  // Hold instantiation of classes define the simulation
  //

				// This is hardwired to J & K for now . . .
				// To be generalized
  clivectori marge(3);
  marge()[0] = 0;		// Marginalize K-band
  marge()[1] = 0;		// Marginalize J-K
  marge()[2] = 1;		// Marginalize J-H

  timer.start();
  PopulationsApp *pops;
  if (CDF)
    pops = new PopulationsApp(DataDir, CDFfile, &marge);
  else
    pops = new PopulationsApp(DataDir, &marge, true);

  //                                             ^
  // Use Flux:Color:Color not Flux:Flux:Flux ----|
  //

  timer.stop();
  cout << "Constructing PopulationsApp took " 
       << 1.0e-6*(timer.getTime().getUserTime() +
		  timer.getTime().getSystemTime() )
       << " seconds" << endl;
  

  float z;
				// K vs J-K distribution

  nfc = int( (Cmax - Cmin)/Cwid + 0.5);
  nfk = int( (Fmax - Fmin)/Fwid + 0.5);

  ofstream out(outfile.c_str());
  out.write(reinterpret_cast<char*>(&nfc), sizeof(int));
  out.write(reinterpret_cast<char*>(&nfk), sizeof(int));
  out.write(reinterpret_cast<char*>(&(z=Cmin)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Cmax)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmin)), sizeof(float));
  out.write(reinterpret_cast<char*>(&(z=Fmax)), sizeof(float));

  vector<double> fmin(2), fmax(2);
  for (int i=0; i<nfk; i++) {
    for (int j=0; j<nfc; j++) {
      if (interp) {
	fmin[0] = Fmin + Fwid*i;
	fmin[1] = Cmin + Cwid*j;
	out.write(reinterpret_cast<char*>(&(z=pops->Interpolate(fmin, AGE, METAL, DISTMOD))), sizeof(float));
      } else {
	fmin[0] = Fmin + Fwid*(-0.5+i);
	fmax[0] = Fmin + Fwid*( 0.5+i);
	fmin[1] = Cmin + Cwid*(-0.5+j);
	fmax[1] = Cmin + Cwid*( 0.5+j);
	out.write(reinterpret_cast<char*>(&(z=pops->Bin(fmin, fmax, AGE, METAL, DISTMOD))), sizeof(float));
      }
    }
  }

  cout << endl;

  return 0;
}
