// -*- C++ -*-

/* 
   Test popmodel by comparing interpolation with table at grid points
   and computing overall normalization for a particular Time, Metallicity,
   and Distance modulus
*/

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>

using namespace std;

#include <PopulationsApp.h>

using namespace BIE;

int
main(int argc, char** argv)
{
  double DISTM = 14.81;
  double METAL = 0.008;
  double TIME = 6.15;
  string dir("/data/weinberg/Isochrones");
  string lim("");
  string cdffile("isochrone.cdf");
  bool MARGE = false;
  bool HISTO = false;
  bool CDF = false;
  bool CMD = false;

  int c;
  while (1) {
    c = getopt (argc, argv, "m:t:s:d:l:Hf:cCMh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'm': METAL = atof(optarg); break;
      case 't': TIME = atof(optarg); break;
      case 's': DISTM = atof(optarg); break;
      case 'd': dir.erase(); dir = optarg; break;
      case 'f': cdffile.erase(); cdffile = optarg; break;
      case 'l': lim.erase(); lim = optarg; break;
      case 'M': MARGE = true; break;
      case 'H': HISTO = true; break;
      case 'c': CDF = true; break;
      case 'C': CMD = true; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-m float\t\tmetallicity\n";
	msg += "\t-t float\t\ttime\n";
	msg += "\t-s float\t\tdistance modulus\n";
	msg += "\t-d path \t\tisochone data directory\n";
	msg += "\t-d fiie \t\tread bin limits from file\n";
	msg += "\t-l fiie \t\tlimits for cumulative distribution\n";
	msg += "\t-H      \t\t1-d histogram\n";
	msg += "\t-S      \t\tprint out binning statistics\n";
	msg += "\t-M      \t\tmarginalize on second flux/color\n";
	msg += "\t-C      \t\tAssume flux-color-color table\n";
	msg += "\t-h      \t\tthis help message\n";
	cerr << msg; exit(0);
      }
  }

  clivectori marge(3);
  marge()[0] = 0;		// Marginalize J-band (yes=1, no=0)
  marge()[1] = 0;		// Marginalize H-band
  marge()[2] = 0;		// Marginalize K-band

  if (MARGE) {
    if (CMD) marge()[2] = 1;
    else     marge()[1] = 1;
  }

  PopulationsApp *pops;

  if (CDF) 
    pops = new PopulationsApp(dir, cdffile, &marge);
  else
    pops = new PopulationsApp(dir, &marge, CMD);


  if ((CMD=pops->CMDflag()))
    cout << "***** This is a CMD (flux-color-color) isochone table *****\n";
  else
    cout << "***** This is a FLUX (flux-flux-flux) isochone table *****\n";

  vector<double> fmin = pops->MinMag();
  vector<double> fmax = pops->MaxMag();
  vector<double> fwid = pops->MagDelta();

  if (lim.size() > 0) {
    ifstream in(lim.c_str());
    if (!in) {
      cout << "Can't open <" << lim << "> . . . continuing\n";
    } else {
      in >> fmin[0];
      in >> fmax[0];

      if (MARGE) {
	in >> fmin[1];
	in >> fmax[1];
      } else {
	in >> fmin[2];
	in >> fmax[2];
      }
    }
  }

  try {
    cout << "Integral: " << setprecision(10)
	 << pops->Bin(fmin, fmax, TIME, METAL, DISTM) << "\n";
  }
  catch (BIEException err) {
    cerr << err.getErrorMessage() << endl;
  }

  if (HISTO) {

    fmin = pops->MinMag();
    fmax = pops->MaxMag();
    vector<double> fmin1(fmin);
    vector<double> fmax1(fmax);

    const int num=40;
    int idx;

    if (CMD) idx = 0;
    else idx = fmin.size()-1;

				// Histogram in K-band
    double dk = (fmax[idx] - fmin[idx])/(num-1);
      
				// Mean values
    double mmean=0.0, mavg=0.0, mval, bval;

    for (int i=0; i<num; i++) {
      fmin1[idx] = fmin[idx] + dk*i;
      fmax1[idx] = fmin[idx] + dk*(i+1);

      try {
	mval = 0.5*(fmin1[idx]+fmax1[idx]);
	bval = pops->Bin(fmin1, fmax1, TIME, METAL, DISTM);
	cout << setw(5)  << i
	     << setw(18) << mval
	     << setw(18) << bval
	     << endl;
	mmean += dk*bval * mval;
	mavg  += dk*bval;
      }
      catch (BIEException err) {
	cerr << err.getErrorMessage() << endl;
      }
    }

    if (mavg>0.0) mmean /= mavg;

    cout << endl
	 << "Norm mag = " << mavg  << endl
	 << "Mean mag = " << mmean << endl
	 << endl;
  }
    

  if (!MARGE) {
    cout << "--------------------\n";
    cout << "Integer accessor: ";
    pops->VerifyInt();

    cout << "--------------------\n";
    cout << "Double accessor: ";
    pops->VerifyDouble();
    
    cout << "--------------------\n";
  }

  // Test destructor
  delete pops;

  return 0;
}

