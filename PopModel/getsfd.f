	subroutine getsfd(l,b,q,ebv,sig,emn,emx)
c	program getsfd

c*******************************************************
c Probes table of SFD98 E(B-V) maps, blocked into 
c  0.4x0.4 degree bins, for mean reddening, variance,
c  minimum and maximum, given a latitude and longitude,
c  and radius around that point. (Input in degrees)
c 
c  AAC 5/19/01 
c*******************************************************

	real l,b,q,ebv,sig,emn,emx,r,pi
	real lon,lat,red,var,rmn,rmx
	real lr,br,qr,latr,lonr
	integer nlin,npix,k

c	character*80 ctmp

	parameter (pi=3.141593)
	parameter (nlin=257662)

c	if (iargc().lt.3) then
c	  write(*,*)' Format: GETSFD <lat> <lon> <radius> (degrees)'
c	  stop
c	else
c	  call getarg(1,ctmp)
c	  read (ctmp,*) l
c	  call getarg(2,ctmp)
c	  read (ctmp,*) b
c	  call getarg(3,ctmp)
c	  read (ctmp,*) q
c	endif
c
c	write(*,*)'longitude = ',l
c	write(*,*)'latitude = ',b
c	write(*,*)'radius = ',q

c Convert from degrees to radians for distance calc

	lr = l*pi/180.0
	br = b*pi/180.0
	qr = q*pi/180.0

	open(12,file='sfd98_rtab.txt',status='unknown')
10	format(2f7.2,3f6.2,f7.2)

	npix = 0
	k = 0
	ebv = 0.0
	sig = 0.0
	emn = 1000.0
	emx = 0.0


	do 20 j = 1,nlin
	  read(12,10)lon,lat,red,var,rmn,rmx
	  lonr = lon*pi/180.0
	  latr = lat*pi/180.0
	  r = acos(sin(br)*sin(latr)+cos(br)*cos(latr)*cos(lonr-lr))
	  if (r.le.qr) then
c	    write(*,10)lon,lat,red,var,rmn,rmx
	    k = k+1
	    ebv = ebv + red
	    sig = sig + var*var
	    emn = min(emn,rmn)
	    emx = max(emx,rmx)
	  endif
20	continue
	close(12)

	npix = k

	ebv = ebv/npix
	sig = sqrt(sig/npix)

c	write(*,*)'Npix = ',npix
c	write(*,*)'E(B-V) = ',ebv
c	write(*,*)'sigma = ',sig
c	write(*,*)'min = ',emn
c	write(*,*)'max = ',emx

	return
	end
