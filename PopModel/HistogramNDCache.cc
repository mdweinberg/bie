
#include <iostream>
#include <fstream>
#include <limits>
using namespace std;

#include <HistogramNDCache.h>
#include <errno.h>
#include <BIEException.h>
#include <BasicType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::HistogramNDCache)

using namespace BIE;

HistogramNDCache::HistogramNDCache
(clivectord *lo_in, clivectord *hi_in, clivectord *w_in, RecordType *type)
  : low((*lo_in)()), high((*hi_in)()), width((*w_in)())
{
  // Make our own copy of the record type.
  recordtype = new RecordType(type);
  
  // Common initialization
  initialize();
}

HistogramNDCache::HistogramNDCache
(clivectord *lo_in,  clivectord *hi_in,  clivectord *w_in, clivectors *names)
  : low((*lo_in)()), high((*hi_in)()), width((*w_in)())
{
  // Construct the record type.
  recordtype = new RecordType();
  
  for (int fieldindex = 0; fieldindex < (int)(*names)().size(); fieldindex++)
  {
    RecordType * rt = recordtype->insertField(fieldindex+1, (*names)()[fieldindex],
                                              BasicType::Real);
    
    delete recordtype;
    recordtype = rt;
  }

  // Common initialization
  initialize();
}

// Common initialization routine.
void HistogramNDCache::initialize()
{
  // The dimension is the number of fields in the record type.
  dim = recordtype->numFields();
  
  // Check there is at least one field in the type.
  if (dim  < 1) 
  { throw TypeException("The record type of a Histogram cannot be empty", __FILE__, __LINE__); }
  
  // Check that the vector dimensions match the RecordType dimensions.
  if ((int)low.size() != dim) 
  { 
    throw TypeException("Dimension of low vector does not match record type",
                        __FILE__, __LINE__); 
  }
  
  if ((int)high.size() != dim) 
  { 
    throw TypeException("Dimension of high vector does not match record type",
                        __FILE__, __LINE__); 
  }

  if ((int)width.size() != dim) 
  { 
    throw TypeException("Dimension of width vector does not match record type",
                        __FILE__, __LINE__); 
  }

  // Check the bounds and width of the bins.
  for (int dimindex = 0; dimindex < (int) low.size(); dimindex++)
  {
    if (low[dimindex] >= high[dimindex])
    {
      ostringstream errorbuffer;
      errorbuffer << "Low bound less or equal to high bound: ";
      errorbuffer << "dimension index: " << dimindex << ".";
      throw BIEException("HistogramNDCache Exception",errorbuffer.str(), __FILE__, __LINE__);
    }
    
    if (width[dimindex] <= 0)
    {
      ostringstream errorbuffer;
      errorbuffer << "Bin width cannot be zero or less: ";
      errorbuffer << "dimension index: " << dimindex << ".";
      throw BIEException("HistogramNDCache Exception",errorbuffer.str(), __FILE__, __LINE__);
    }
  }

  // Check every field is real valued.
  for (int fieldindex = 1; fieldindex <= recordtype->numFields(); fieldindex++)
  {
    BasicType * fieldtype = recordtype->getFieldType(fieldindex);

    if (! (fieldtype->equals(BasicType::Real)))
    { throw TypeException("Every field in the histogram type must be a real type.", __FILE__, __LINE__); }
  }  
  
  irank = vector<int>(dim);
  vol = 1.0;
  for (int i=0; i<dim; i++) {
    irank[i] = (int)( (high[i]-low[i])/(fabs(width[i])+1.0e-12) ) + 1;
    high[i] = low[i] + width[i]*irank[i];
    vol *= width[i];
  }

  histo = boost::shared_ptr<HistogramCache>
    (new HistogramCache(low, high, width));

  populate_cache();

  mass = 0.0;
}

// Use to populate cache
void HistogramNDCache::populate_cache()
{
  // Default all bins in n-dim rectangular prism
  int num = 1;
  vector<double> z(dim);
  vector<int> marg(dim, 1);
  vector<unsigned short> indx(dim);
  for (int i=0; i<dim; i++) num *= irank[i];

  // Compute the offsets for each dimension
  int tmp = num;
  for (int i=0; i<dim; i++) {
    tmp /= irank[i];
    marg[i] = tmp;
  }

  // Validate this cell using its midpoint
  for (int j=0; j<num; j++) {
    int jj = j;
    for (int i=0; i<dim; i++) {
      indx[i] = jj/marg[i];
      jj -= indx[i]*marg[i];
      z[i] = low[i] + width[i]*(0.5+indx[i]);
    }

    if (OK(z)) {
      histo->addValue(z, 0);
    }

  }

}

// Null constructor
HistogramNDCache::HistogramNDCache()
{
  dim = 0;
  recordtype = 0;
}

// Factory
HistogramNDCache* HistogramNDCache::New()
{
  HistogramNDCache *p = new HistogramNDCache();

  p->dim = dim;
  p->mass = mass;

  p->low = low;
  p->high = high;
  p->width = width;

  p->irank = irank;

  p->mean = mean;
  p->square = square;
  p->stdev = stdev;

  p->histo = 
    boost::shared_ptr<HistogramCache>(new HistogramCache(low, high, width));
  p->populate_cache();

  // Make our own copy of the record type.
  p->recordtype = new RecordType(recordtype);

  return p;
}

bool HistogramNDCache::AccumulateData(double v, RecordBuffer* datapoint)
{
  vector<double> x (recordtype->numFields());

  // foreach field THIS CLASS knows about and expects...
  for (int fieldindex = 1; fieldindex <= recordtype->numFields(); fieldindex++)
  {
    // Get the fieldname
    string fieldname = recordtype->getFieldName(fieldindex);
    
    // Get the value of the field with this name.
    if (datapoint->isValidFieldName(fieldname))
    {
      x[fieldindex-1]  = datapoint->getRealValue(fieldname);
    }
    else
    {
      // This data point is no use to us.
      return false;
    }
  }

  return AccumulateData(v, x);
}

bool HistogramNDCache::OK(vector<double>& x) 
{
				// Flux check
  for (int i=0; i<dim; i++) 
    if (x[i]<=low[i] || x[i] >= high[i]) return false;
  return true;
}


bool HistogramNDCache::AccumulateData(double v, vector<double>& x)
{
  if (histo->addToExistingValue(x, v)) {
    mass += v;
    return true;
  }
  return false;
}

double HistogramNDCache::PDF(vector<double>& x)
{
  if (OK(x)) return histo->getValue(x);
  return 0.0;
}

double HistogramNDCache::logPDF(vector<double>& x) {
  if (OK(x) && histo->getValue(x)>0.0) return log(histo->getValue(x));
  return numeric_limits<double>::min(); // -MAXDOUBLE;
}

double HistogramNDCache::CDF(vector<double>& x)
{
  if (OK(x) && mass>0.0) {

    vector<unsigned short> in0(dim);

    for (int i=0; i<dim; i++)
      in0[i] = (unsigned short)( (x[i] - low[i])/width[i] );
    
    vector<unsigned short> in(dim);
    double sum = 0.0;
    
    int number = histo->getNumber();
    mmapHK::iterator itr = histo->Reset(); 
    for (int i=0; i<number; i++) {
      for (int indx=0; indx<dim; indx++) {
	if (itr->first.get_I(indx) > in0[indx]) continue;
      }
      sum += itr->second;
      histo->Next();
    }
    return sum/mass;
  }
  else
    return 0.0;
}

void HistogramNDCache::ComputeDistribution(void)
{
  mean = vector<double>(dim, 0.0);
  square = vector<double>(dim, 0.0);
  stdev = vector<double>(dim);

  vector<unsigned short> in(dim);
    int number = histo->getNumber();
  mmapHK::iterator itr = histo->Reset();

  for (int i=0; i<number; i++) {
    for (int indx=0; indx<dim; indx++) {
      double pos = low[indx] + width[indx]*(0.5+itr->first.get_I(indx));
      mean[indx] += itr->second * pos;
      square[indx] += bins[i] * pos*pos;
    }
    histo->Next();
  }

  for (int indx=0; indx<dim; indx++) {
    mean[indx] /= mass;
    square[indx] /= mass;
    stdev[indx] = sqrt(square[indx] - mean[indx]*mean[indx]);
  }

}

