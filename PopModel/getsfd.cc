/*******************************************************
  Probes table of SFD98 E(B-V) maps, blocked into 
  0.4x0.4 degree bins, for mean reddening, variance,
  minimum and maximum, given a latitude and longitude,
  and radius around that point. (Input in degrees)
 
  AAC 5/19/01 
 *******************************************************/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

#include <cmath>
#include <string>
#include <cstdlib>

using namespace std;

void getsfd(float l, float b, float q, 
	    float& ebv, float& sig, float& emn, float& emx,
	    const string& DataDir)
{
  float r, lon, lat, red, var, rmn, rmx;
  float lr, br, qr, latr, lonr;
  int npix;

  const float onedeg = 57.29577951;
  const int nlin=257662;

  // Convert from degrees to radians for distance calc
  
  lr = l/onedeg;
  br = b/onedeg;
  qr = q/onedeg;

  string datafile = DataDir + "/sfd98_rtab.txt\0";

  ifstream sfd(datafile.c_str());
  if (!sfd) {
    cerr << "getsfd: couldn't open data file <" << datafile << ">\n";
    exit(-1);
  }

  ebv = 0.0;
  sig = 0.0;
  emn = 1000.0;
  emx = 0.0;
  npix = 0;

  const int bufsize = 2048;
  char line[bufsize];

  for (int j=1; j<=nlin; j++) {

    sfd.getline(line, bufsize);
    istringstream in(line);
    
    in >> lon;
    in >> lat;
    in >> red;
    in >> var;
    in >> rmn;
    in >> rmx;

    lonr = lon/onedeg;
    latr = lat/onedeg;
    r = acos(sin(br)*sin(latr)+cos(br)*cos(latr)*cos(lonr-lr));
    if (r <= qr) {
      ebv = ebv + red;
      sig = sig + var*var;
      emn = min<float>(emn, rmn);
      emx = max<float>(emx, rmx);

      npix++;
    }
  }

  if (npix) {
    ebv = ebv/npix;
    sig = sqrt(sig/npix);
  }
}


/************************************************
  Measure pathlength through a uniform dust disk
  with a given height.   The total reddening 
  integrated from zero to infinite distance is
  supplied by another routine that samples from
  the Schlegel et al. (1998) maps.
  Use the reddening relations
  given in Bessell & Brett (1988) to go from 
  reddening to extinction in the bandpass.

  Requires: galactic coordinates in degrees,
  a distance in parsecs, and a total reddening
  E(B-V) from a prior routine.

  Returns:  A(J), A(H), A(K)

**************************************************/

void getext(float s, float l0, float b0, float ebv, 
	    float& aj, float& ah, float& ak)
{

  float path, rgc, l, b;

  // Galactic parameters:
  //   z0 = height of Sun above galactic plane (pc)
  //   zmax = half-thickness of uniform dust disk (pc)
  //   r0 = distance of Sun from galactic center (pc)
  //   rmax = radius of galactic dust disk (pc)

  const float z0=15.0;
  const float zmax=150.0;
  const float r0=8500.0;
  const float rmax=15000.0;

  // Convert to radians

  const float onedeg = 5.729577951e+01;

  b = b0/onedeg;
  l = l0/onedeg;

  // Is the field at (l,b,s) within the dust disk?

  double term1 = s*cos(b)*cos(l) - r0;
  double term2 = s*cos(b)*sin(l);
  rgc = sqrt(term1*term1 + term2*term2);

				// Outside cylindrical edge of disk
  if (rgc < rmax) {
				// Outside faces of disk
    path = s*fabs(sin(b))/(zmax-copysign(z0, b));
    if (path >= 1.0) 
      path = 1.0;
    else			// Inside disk
      path = s/(cos(l)*r0+sqrt(r0*r0*((cos(l)*cos(l))-1.0)+rmax*rmax));
  }
  else
    path = 1.0;

  aj = 0.90*ebv*path;
  ah = 0.53*ebv*path;
  ak = 0.34*ebv*path;
}
