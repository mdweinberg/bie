#include <iomanip>
#include <sstream>
using namespace std;

#include <PopModelCache.h>
BIE_CLASS_EXPORT_IMPLEMENT(BIE::PopModelCache)

using namespace BIE;

int       PopModelCache::NUM=800;
double    PopModelCache::ALPHA=0.0;
double    PopModelCache::BETA=0.0;
BirthRate PopModelCache::BRmodel=Exponential;
double    PopModelCache::BURST=0.0;
double    PopModelCache::NU=0.0;
double    PopModelCache::METAL=0.019;
double    PopModelCache::A1=2.5;
double    PopModelCache::Z0=15.0;
double    PopModelCache::Z1=150.0;
double    PopModelCache::EFAC=1.0;
bool      PopModelCache::RING=false;
double    PopModelCache::RINGWIDTH=0.25;
double    PopModelCache::RINGLOC=4.0;
double    PopModelCache::RINGAMP=1.0;
double    PopModelCache::CENTERWIDTH=1.0;
double    PopModelCache::CENTERAMP=1.0;
double    PopModelCache::NEARBYWIDTH=0.2;
double    PopModelCache::NEARBYAMP=0.0;
double    PopModelCache::R0=8.0;
bool      PopModelCache::RLOG=false;
double    PopModelCache::RMIN=0.010;
double    PopModelCache::RMAX=20.0;
double    PopModelCache::AMIN=0.5;
double    PopModelCache::AMAX=8.0;
double    PopModelCache::HMIN=100.0;
double    PopModelCache::HMAX=3500.0;
double    PopModelCache::Log10 = log(10.0);
double    PopModelCache::onedeg = M_PI/180.0;
string    PopModelCache::DataDir = "/home/weinberg/3Mtab";

/*
  Andrew Cole's extinction routines based on Schlegel's map
*/

void getsfd(float l, float b, float q, float& ebv, float& sig,
	    float& emn, float& emx, const string& DataDir);


PopModelCache::PopModelCache(int ndim, int mdim, SampleDistribution* _dist)
{
  Ndim = ndim;
  M = mdim;
  Mcur = M;

  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

				// This is hardwired to J & K for now . . .
				// To be generalized
  clivectori marge(3);
  marge()[0] = 0;		// Marginalize J-band (yes=1, no=0)
  marge()[1] = 1;		// Marginalize H-band
  marge()[2] = 0;		// Marginalize K-band
  pops = new PopulationsApp(DataDir, &marge);
  Nflux = 2;

				// Cache the bin boundaries for the data
				// histogram
				// Sanity check
  if (_dist->getdim(0) != Nflux) {
    ostringstream msg;
    msg << "PopModelCache: SampleDistribution is not " << _dist->getdim(0)
	<< " dimensional, model has " << Nflux << " fluxes";
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }
    

  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
    

				// Create bin boundary vectors
    for (int j=0; j<Nflux; j++) {
      dvector t;
      lowb.push_back(t);
      highb.push_back(t);
    }
				// Cache the bin boundaries for the data
    nbins = histo->numberData(); // histogram
    for (int i=0; i<nbins; i++) {
      for (int j=0; j<Nflux; j++) {
	lowb[j].push_back(histo->getLow(i)[j]);
	highb[j].push_back(histo->getHigh(i)[j]);
      }
    }
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
    nbins = 1;
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);

				// Default line-of-sight integration knots
				// 
  intgr = JacoQPtr(new JacoQuad(NUM, ALPHA, BETA));
}

PopModelCache::~PopModelCache()
{
  delete pops;
}

				// Model: exponential * sech^2 disk

vector<string> PopModelCache::ParameterLabels()
{
  vector<string> ret;
  ret.push_back("Length");
  ret.push_back("Height");
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void PopModelCache::Initialize(State& s)
{
  Mcur = (int)floor(s[0]+0.01);

  for (int k=0; k<Mcur; k++) wt[k] = s[k+1];
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s[1+Mcur+k*Ndim+j];
  }

  check_bounds();
}

void PopModelCache::Initialize(vector<double>& w, vector<double>*& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}

void PopModelCache::check_bounds()
{
  good_bounds = true;
				// Check bounds
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;
  }
  for (int k=0; k<Mcur; k++) {
    if (pt[k][0] > AMAX || pt[k][0] < AMIN   ||
	pt[k][1] > HMAX || pt[k][1] < HMIN    )  good_bounds = false;
  }

				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<2; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
#endif
}

void PopModelCache::generate(double L, double B)
{
  double s, R, z, fac;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

  ages = pops->Ages();
  nages = (int)ages.size();
  popweight = vector<double>(nages);

				// Set star formation history

  if (BRmodel == Burst) {
				// Continuous creation + Burst
    double sum = 0.0;
    for (int nn=0; nn<nages; nn++) {
      popweight[nn] = pow(10.0, ages[nn] - 10.0);
      sum += popweight[nn];
    }
    for (int nn=0; nn<nages; nn++) popweight[nn] *= (1.0-BURST)/sum;
    popweight[nages-1] += BURST;
    
  } else {
				// Exponential
    double sum = 0.0;
    for (int nn=0; nn<nages; nn++) {
      popweight[nn] = pow(10.0, ages[nn]-8.0) *
	exp( -NU*(pow(10.0, 0.2) - pow(10.0, ages[nn]-10.0)) );
      sum += popweight[nn];
    }
    for (int nn=0; nn<nages; nn++) popweight[nn] /= sum;
  }

				// Call extinction routine
  const double onedeg = 180.0/M_PI;
  float aj, ah, ak, lf, lb, lq, ebv, sig, emn, emx;
  lf = L*onedeg;
  lb = B*onedeg;
  lq = 1.0;
  getsfd(lf, lb, lq, ebv, sig, emn, emx, DataDir);

				// Set up line-of-sight cache
  cache[listCntr]->R = vector<real>(NUM);
  cache[listCntr]->z = vector<real>(NUM);
  cache[listCntr]->fac = vector<real>(NUM);
  cache[listCntr]->dmod = vector<real>(NUM);
  cache[listCntr]->ext = vector<rvector>(NUM);

  vector<double> absorb(NUM), dist(NUM);
  double curabs, sech, lastabs=0.0, lasts=0.0;

  if (RLOG) RMIN = max<double>(RMIN, 0.001);

				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    if (RLOG) {
      s = RMIN*exp(intgr->knot(n) * (log(RMAX) - log(RMIN)));
      fac = s*s*s*intgr->weight(n) * (log(RMAX) - log(RMIN));
    } else {
      s = RMIN + intgr->knot(n) * (RMAX - RMIN);
      fac = s*s*intgr->weight(n) * RMAX;
    }

    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB - Z0*1.0e-3;

    cache[listCntr]->R[n-1]    = R;
    cache[listCntr]->z[n-1]    = z;
    cache[listCntr]->fac[n-1]  = fac;
    cache[listCntr]->dmod[n-1] = 5.0*log(1.0e3*s)/Log10 - 5.0;
    
				// Contribution to extinction
    dist[n-1] = s;
				// Ring and central
    if (RING) {
      double x = s*cosB*cosL - R0;
      double y = s*cosB*sinL;

      curabs = 
	CENTERAMP*exp(-(R*R+z*z)/(2.0*CENTERWIDTH*CENTERWIDTH)) /
	pow(2.0*M_PI*CENTERWIDTH*CENTERWIDTH, 1.5)
	+
	RINGAMP*exp(-(R-RINGLOC)*(R-RINGLOC)/(2.0*RINGWIDTH*RINGWIDTH)) *
	exp(-z*z/(2.0*Z1*Z1*1.0e-6)) / (4.0*M_PI*M_PI*RINGWIDTH*Z1*1.0e-3)
	+
	NEARBYAMP*exp(-(x*x + y*y + z*z)/(2.0*NEARBYWIDTH*NEARBYWIDTH)) /
	(4.0*M_PI*M_PI*NEARBYWIDTH*NEARBYWIDTH)
	;
				// Standard disk extinction
    } else {
      sech = 0.5/( exp(z*0.5e3/Z1) + exp(-z*0.5e3/Z1) );
      curabs = exp(-R/A1)*sech*sech * (2.5e2/Z1) / (A1*A1*2.0*M_PI);
    }

    absorb[n-1] = 0.5*(s - lasts)*(lastabs+curabs);
    if (n>1) absorb[n-1] += absorb[n-2];
    lasts = s;
    lastabs = curabs;

  }

				// Compute extinction
  for (int n=1; n<=NUM; n++) {
    aj = 0.90*EFAC*ebv*absorb[n-1]/absorb[NUM-1];
    ah = 0.53*EFAC*ebv*absorb[n-1]/absorb[NUM-1];
    ak = 0.34*EFAC*ebv*absorb[n-1]/absorb[NUM-1];
      
    cache[listCntr]->ext[n-1].push_back(aj);
    cache[listCntr]->ext[n-1].push_back(ak);
  }

}

void PopModelCache::compute_bins(SampleDistribution *sd)
{
  vector<double> fluxmin(Nflux), fluxmax(Nflux);
  double sech, R, z, fac, dens, dmod;
				// Zero out theoretical bins
  cache[listCntr]->bins = vector<real>(nbins, 0.0);
  
  for (int n=1; n<=NUM; n++) {
				// Coordinates
    fac = cache[listCntr]->fac[n-1];
    R = cache[listCntr]->R[n-1];
    z = cache[listCntr]->z[n-1];
    dmod = cache[listCntr]->dmod[n-1];
				// Loop over each components
    for (int k=0; k<Mcur; k++) {
				// Disk model
      sech = 2.0/(exp(z*0.5e3/pt[k][1]) + exp(-z*0.5e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);

      for (int j=0; j<nbins; j++) {

				// Make fluxes for each component
	for (int l=0; l<Nflux; l++) {
	  if (type == binned) {
	    fluxmin[l] = lowb[l][j] - cache[listCntr]->ext[n-1][l];
	    fluxmax[l] = highb[l][j] - cache[listCntr]->ext[n-1][l];
	  } else if (type == point) {
	    fluxmin[l] = ((PointDistribution*)sd)->Point()[l] - cache[listCntr]->ext[n-1][l];
	  }
	}
      
	for (int nn=0; nn<nages; nn++)
	  if (type == binned) 
	    cache[listCntr]->bins[j] += wt[k]*fac*dens *
	      pops->Bin(fluxmin, fluxmax, ages[nn], METAL, dmod) * popweight[nn];
	  else if (type == point) 
	    cache[listCntr]->bins[j] += wt[k]*fac*dens *
	      pops->Interpolate(fluxmin, ages[nn], METAL, dmod) * popweight[nn];
	
      }
    }
  }

}

double PopModelCache::NormEval(double L, double B, SampleDistribution *sd)
{
  if (!good_bounds) return 1.0;

  if (listCntr>=(int)cache.size()) {

    cache.push_back(PopCPtr(new PopCache(L, B)));
    generate(L, B);

  } else {

    if (fabs(cache[listCntr]->L - L) > 1.0e-8 ||
	fabs(cache[listCntr]->B - B) > 1.0e-8 )
      {
	cache[listCntr] = PopCPtr(new PopCache(L, B));
	generate(L, B);
	missed++;
#ifdef DEBUG	
	cerr << "PopModelCache: cache miss [" << missed << "]\n";
#endif
      }
  }

  compute_bins(sd);

  double norm = 0.0;
#ifdef DEBUG
  double zerotol=1.0e-12, bmin=1.0e20, bmax=0.0;
  int zerocnt=0;
  for (int j=0; j<nbins; j++)  {
    norm += cache[listCntr]->bins[j];
    bmin = min<double>(bmin, cache[listCntr]->bins[j]);
    bmax = max<double>(bmax, cache[listCntr]->bins[j]);
    if (cache[listCntr]->bins[j]<zerotol) zerocnt++;
  }
  cout << "Min bin val: " << bmin << endl;
  cout << "Max bin val: " << bmax << endl;
  cout << "Found " << zerocnt 
       << " with val < " << zerotol 
       << " out of " << nbins << endl;
#else
  for (int j=0; j<nbins; j++)  norm += cache[listCntr]->bins[j];
#endif

  listCntr++;

  return norm;
}




vector<double> PopModelCache::Evaluate(double L, double B, 
				       SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);

  if (listCntr>=(int)cache.size()) {
    cache.push_back(PopCPtr(new PopCache(L, B)));
    generate(L, B);
    compute_bins(d);
  } else {
    if (fabs(cache[listCntr]->L - L) > 1.0e-8 ||
	fabs(cache[listCntr]->B - B) > 1.0e-8 )
      {
	cache[listCntr] = PopCPtr(new PopCache(L, B));
	generate(L, B);
	compute_bins(d);
	missed++;
#ifdef DEBUG	
	cerr << "PopModelCache: cache miss [" << missed << "]\n";
#endif
      }
  }
  
  int n = cache[listCntr]->bins.size();
  if ((int)work.size() != n) work = vector<double>(n);
  for (int i=0; i<n; i++) work[i] = cache[listCntr]->bins[i];
  listCntr++;
  return work;
}


