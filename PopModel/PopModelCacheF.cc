// This may look like C code, but it is really -*- C++ -*-

#include <sys/timeb.h>

#include <iomanip>
#include <sstream>

using namespace std;

#include <BIEmpi.h>
#include <gvariable.h>
#include <gfunction.h>

#include <gaussQ.h>
#include <PopModelCacheF.h>
BIE_CLASS_EXPORT_IMPLEMENT(BIE::PopModelCacheF)


using namespace BIE;

#define USE_LIST

double PopModelCacheF::ALPHA=0.0;
double PopModelCacheF::BETA=0.0;
BirthRate PopModelCacheF::BRmodel=Exponential;
double PopModelCacheF::BURST=0.0;
double PopModelCacheF::NU=0.0;
double PopModelCacheF::METAL=0.019;
double PopModelCacheF::A1=2.5;
double PopModelCacheF::Z0=15.0;
double PopModelCacheF::Z1=150.0;
double PopModelCacheF::EFAC=1.0;
double PopModelCacheF::EMAX=2.0;
bool   PopModelCacheF::DELTA =false;
double PopModelCacheF::RT0= 0.3;
double PopModelCacheF::DRT0 = 0.05;
int    PopModelCacheF::NEBV=5;
bool   PopModelCacheF::RING=false;
double PopModelCacheF::RINGWIDTH=0.25;
double PopModelCacheF::RINGLOC=4.0;
double PopModelCacheF::RINGAMP=1.0;
double PopModelCacheF::CENTERWIDTH=1.0;
double PopModelCacheF::CENTERAMP=1.0;
double PopModelCacheF::NEARBYWIDTH=0.2;
double PopModelCacheF::NEARBYAMP=0.0;
double PopModelCacheF::R0=8.0;
double PopModelCacheF::RMIN=0.01;
double PopModelCacheF::RMAX=20.0;
bool   PopModelCacheF::RLOG=true;
double PopModelCacheF::AMIN=0.5;
double PopModelCacheF::AMAX=8.0;
double PopModelCacheF::HMIN=30.0;
double PopModelCacheF::HMAX=3500.0;
double PopModelCacheF::Log10 = log(10.0);
double PopModelCacheF::onedeg = M_PI/180.0;
double PopModelCacheF::zerotol = 1.0e-17;
string PopModelCacheF::DataDir = "/home/weinberg/3Mtab";
bool   PopModelCacheF::use_vector=true;

/*
union coordUnion {
  float z;
  unsigned int i;
  unsigned char c[4];
};

size_t coordHash(coordPair P) {
  coordUnion u1, u2, u;
  u1.z = P.first;
  u2.z = P.second;

  u.c[0] = u1.c[1];
  u.c[1] = u2.c[1];
  u.c[2] = u1.c[2];
  u.c[3] = u2.c[2];
  
  return u.i*0x9e3779b1u;
}
*/

const coordPair toobig(100.0, 100.0);


/*
  Based on Andrew Cole's extinction routines on Schlegel's map
*/

void getsfd(float l, float b, float q, float& ebv, float& sig,
	    float& emn, float& emx, const string& DataDir);

void getext(float s, float l, float b, float ebv, 
	    float& aj, float& ah, float& ak);


PopModelCacheF::PopModelCacheF(int ndim, int mdim, int num,
			       SampleDistribution* _dist,
			       PopulationsApp* Pops)
{
  Ndim = ndim;
  M = mdim;
  Mcur = M;
  NUM = num;
  cmd = Pops->CMDflag();
  pid = getpid();
  mnorm = 0.0;
  mback = 1.0;
  mm_use = false;
  mm_computed = false;

  cout << "Process " << myid << ": my pid=" << pid << "\n";

  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

  pops = Pops;
  marge = pops->get_marge();
  Nflux = 0;
  for (unsigned i=0; i<marge.size(); i++) if (marge[i]) Nflux++;

				// Cache the bin boundaries for the data
				// histogram
				// Sanity check
  if (_dist->getdim(0) != Nflux) {
    ostringstream msg;
    msg << "PopModelCacheF: SampleDistribution is not " << _dist->getdim(0)
	<< " dimensional, model has " << Nflux << " fluxes";
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }
    
  
  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
    
				// Create bin boundary vectors
    for (int j=0; j<Nflux; j++) {
      dvector t;
      lowb.push_back(t);
      highb.push_back(t);
    }
				// Cache the bin boundaries for the data
    nbins = histo->numberData(); // histogram
    for (int i=0; i<nbins; i++) {
      for (int j=0; j<Nflux; j++) {
	lowb[j].push_back(histo->getLow(i)[j]);
	highb[j].push_back(histo->getHigh(i)[j]);
      }
    }
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
    nbins = 1;
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);

				// Default line-of-sight integration knots
				// 
  intgr = JacoQPtr(new JacoQuad(NUM, ALPHA, BETA));
  lastP = toobig;

#ifdef USE_LIST
  maxvmsize = get_max_vm();
  vmsize = 0;
  max_element = 0;
  
  cout << "Process " << myid << ": pid=" << pid 
       << "  vmsize=" << vmsize/1024 
       << "  maxvmsize=" << maxvmsize/1024
       << "\n";
#endif
}


PopModelCacheF::~PopModelCacheF()
{
  delete pops;
}

				// Model: exponential * sech^2 disk

vector<string> PopModelCacheF::ParameterLabels()
{
  vector<string> ret;
  ret.push_back("Length");
  ret.push_back("Height");
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void PopModelCacheF::Initialize(State& s)
{
  Mcur = (int)floor(s[0]+0.01);
  for (int k=0; k<Mcur; k++) wt[k] = s[k+1];
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s[1+Mcur+k*Ndim+j];
  }

  check_bounds();
  lastP = toobig;
}

void PopModelCacheF::Initialize(vector<double>& w, vector<double>*& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}

void PopModelCacheF::check_bounds()
{
  good_bounds = true;
				// Check bounds
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;
  }
  for (int k=0; k<Mcur; k++) {
    if (pt[k][0] > AMAX || pt[k][0] < AMIN   ||
	pt[k][1] > HMAX || pt[k][1] < HMIN    )  good_bounds = false;
  }

				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<2; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
#endif
}

PopCFPtr PopModelCacheF::generate(const coordPair& P, SampleDistribution *d)
{
  double s=0.0, R, z;

  double L = P.first;
  double B = P.second;

  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

  ages = pops->Ages();
  nages = (int)ages.size();
  popweight = vector<double>(nages);

				// Make hash table entry
  PopCFPtr p(new PopCacheF(L, B));
  cache[P] = p;

#ifdef USE_LIST
				// Add to cache list
  cachelist.push_back(p);
  p->it = --cachelist.end();

  unsigned vmsize1 = get_cur_vm(pid);

  if (vmsize) max_element = max<unsigned>(vmsize1-vmsize, max_element);
  vmsize = vmsize1;

  cout << "Process " << myid << ": max_element=" << max_element
       << "  vmsize=" << vmsize << "\n";
  
  if (maxvmsize - vmsize - 3*max_element < 0) {
				// Delete the element's pointer from hash_map
    coordPair P(cachelist.front()->L, cachelist.front()->B);
    cache.erase(cache.find(P));
				// Delete the element's pointer from the list
    cachelist.pop_front();
  }

#endif  
				// Set star formation history

  if (BRmodel == Burst) {
				// Continuous creation + Burst
    double sum = 0.0;
    for (int nn=0; nn<nages; nn++) {
      popweight[nn] = pow(10.0, ages[nn] - 10.0);
      sum += popweight[nn];
    }
    for (int nn=0; nn<nages; nn++) popweight[nn] *= (1.0-BURST)/sum;
    popweight[nages-1] += BURST;
    
  } else {
				// Exponential
    double sum = 0.0;
    for (int nn=0; nn<nages; nn++) {
      popweight[nn] = pow(10.0, ages[nn]-8.0) *
	exp( -NU*(pow(10.0, 0.2) - pow(10.0, ages[nn]-10.0)) );
      sum += popweight[nn];
    }
    for (int nn=0; nn<nages; nn++) popweight[nn] /= sum;

  }

				// Debug
#ifdef DEBUG
  cout << "============================================================\n";
  if (mpi_used) cout << "Process " << myid << " . . .\n";
  cout << nages << " ages:\n";
  for (int nn=0; nn<nages; nn++)
    cout << setw(15) << ages[nn] << setw(15) << popweight[nn] << endl;
  cout << "============================================================\n"
       << flush;
#endif
				// Call extinction routine

  const double onedeg = 180.0/M_PI;
  float aj, ah, ak, lf, lb, lq, ebv, ebv1, sig, emn, emx;
  lf = L*onedeg;
  lb = B*onedeg;
  lq = 1.0;
  getsfd(lf, lb, lq, ebv, sig, emn, emx, DataDir);
  ebv = min<double>(EMAX, ebv);

  HermQuad hq(NEBV);
  double ebvfac = 1.0+sig*sig/(ebv*ebv);
  float MM = log(ebv/sqrt(ebvfac));
  float SS = log(ebvfac);

				// Set up line-of-sight cache
  p->R = vector<real>(NUM);
  p->z = vector<real>(NUM);
  p->mm0 = vector<real>(NUM);
  p->mm1 = vector<real>(NUM);
  if (DELTA) {
    p->s  = vector<real>(NUM);
    p->dm = vector<real>(NUM);
  }
  if (use_vector)
    p->bins1 = vector<real>(NUM*nbins, 0.0);
  else
    p->reset_bin1();

  vector<double> fluxmin(Nflux), fluxmax(Nflux), ext(Nflux);
  vector<double> absorb(NUM), fac(NUM), dmod(NUM);
  double curabs, sech, lastabs=0.0, lasts=0.0;

  if (RLOG) RMIN = max<double>(RMIN, 0.001);

				// Begin integration over line of sight
  for (int n=0; n<NUM; n++) {

    if (RLOG) {
      s = RMIN*exp(intgr->knot(n+1) * (log(RMAX) - log(RMIN)));
      fac[n] = s*s*s*intgr->weight(n+1) * (log(RMAX) - log(RMIN));
    } else {
      s = RMIN + intgr->knot(n+1) * (RMAX - RMIN);
      fac[n] = s*s*intgr->weight(n+1) * (RMAX - RMIN);
    }

    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB - Z0*1.0e-3;

    p->R[n] = R;
    p->z[n] = z;

    dmod[n] = 5.0*log(1.0e3*s)/Log10 - 5.0;
    
    if (DELTA) {
      p->s[n] = s;
      p->dm[n] = dmod[n];
    }
				// Ring and central extinction
    if (RING) {
      double x = s*cosB*cosL - R0;
      double y = s*cosB*sinL;

      curabs = 
	CENTERAMP*exp(-(R*R+z*z)/(2.0*CENTERWIDTH*CENTERWIDTH)) /
	pow(2.0*M_PI*CENTERWIDTH*CENTERWIDTH, 1.5)
	+
	RINGAMP*exp(-(R-RINGLOC)*(R-RINGLOC)/(2.0*RINGWIDTH*RINGWIDTH)) *
	exp(-z*z/(2.0*Z1*Z1*1.0e-6)) / (4.0*M_PI*M_PI*RINGWIDTH*Z1*1.0e-3)
	+
	NEARBYAMP*exp(-(x*x + y*y + z*z)/(2.0*NEARBYWIDTH*NEARBYWIDTH)) /
	(4.0*M_PI*M_PI*NEARBYWIDTH*NEARBYWIDTH)
	;
				// Standard disk extinction
    } else {
      sech = 0.5/( exp(z*0.5e3/Z1) + exp(-z*0.5e3/Z1) );
      curabs = exp(-R/A1)*sech*sech * (2.5e2/Z1) / (A1*A1*2.0*M_PI);
    }

    absorb[n] = 0.5*(s - lasts)*(lastabs+curabs);
    if (n>0) absorb[n] += absorb[n-1];
    lasts = s;
    lastabs = curabs;
    
  }

  for (int n=0; n<NUM; n++) {

				// For confusion calculation

    ak = 0.34*EFAC*ebv*absorb[n]/absorb[NUM-1];
    p->mm0[n] = fac[n];
    p->mm1[n] = 5.0*log(s*100.0)/log(10.0) + ak;

				// Population cache
    vector<double> binsT(nbins, 0.0);

    //==========================================================
    
    for (int mm=1; mm<=NEBV; mm++) {

      ebv1 = exp(MM + sqrt(2.0*SS)*hq.knot(mm));

				// Compute extinction

      aj = 0.90*EFAC*ebv*absorb[n]/absorb[NUM-1];
      ah = 0.53*EFAC*ebv*absorb[n]/absorb[NUM-1];
      ak = 0.34*EFAC*ebv*absorb[n]/absorb[NUM-1];
      

      //=====!!!!NOT GENERAL: assumes three band K, J-H, J-K!!!!======
      if (cmd)
	{
	  int kk = 0;
	  if (marge[0]) ext[kk++] = ak;
	  if (marge[1]) ext[kk++] = aj-ak;
	  if (marge[2]) ext[kk++] = aj-ah;
	}
      //=====!!!!NOT GENERAL: assumes three band J, H, K!!!!======
      else
	{
	  int kk = 0;
	  if (marge[0]) ext[kk++] = aj;
	  if (marge[1]) ext[kk++] = ah;
	  if (marge[2]) ext[kk++] = ak;
	}

      //==========================================================
    
      double ebvpi = hq.weight(mm)/sqrt(M_PI);

      for (int j=0; j<nbins; j++) {

				// Make fluxes for each component
	for (int l=0; l<Nflux; l++) {
	  if (type==binned) {
	    fluxmin[l] = lowb[l][j] - ext[l];
	    fluxmax[l] = highb[l][j] - ext[l];
	  } else if (type==point) {
	    fluxmin[l] = ((PointDistribution*)d)->Point()[l] - ext[l];
	  }
	}
      
	for (int nn=0; nn<nages; nn++) {
	  if (type==binned) {
	    binsT[j] += fac[n] * popweight[nn] * ebvpi *
	      pops->Bin(fluxmin, fluxmax, ages[nn], METAL, dmod[n]);
	  }
	  else if (type==point) {
	    binsT[j] += fac[n] * popweight[nn] * ebvpi *
	      pops->Interpolate(fluxmin, ages[nn], METAL, dmod[n]);
	  }
	}
      
      } // End bin loop

    } // End extinction loop

				// Load hash table
    for (int j=0; j<nbins; j++) {
      if (use_vector)
	p->bins1[n*nbins + j] = binsT[j];
      else if (binsT[j] > zerotol) 
	p->bins1_map[n*nbins + j] = binsT[j];
    }

#ifdef BIN_DEBUG
    {
      ostringstream bdname;
      bdname << "bin.debug." << n << '\0';
      ofstream bindebug(bdname.str().c_str());
      
      double s;

      if (RLOG)
	s = RMIN*exp(intgr->knot(n+1) * (log(RMAX) - log(RMIN)));
      else
	s = RMIN + intgr->knot(n+1) * (RMAX - RMIN);

      bindebug << "# s=" << s << "  L=" << L << "  B=" << B << endl << "#" << endl;
      for (int j=0; j<nbins; j++) {
	bindebug << setw(6) << j;
	for (int l=0; l<Nflux; l++) bindebug << setw(15) <<  lowb[l][j]
					     << setw(15) << highb[l][j];
	bindebug << setw(15) << binsT[j];
	if (use_vector)
	  bindebug << setw(15) << p->bins1[n*nbins + j] << endl;
	else
	  bindebug << setw(15) << p->bins1_map[n*nbins + j] << endl;
      }
    }
#endif
  } // End line-of-sight loop

  if (!use_vector) {
#ifdef DEBUG
    double bmin=1.0e20, bmax=0.0;
    map<int, double, less<int> >::iterator j = p->bins1_map.begin();
    while (j != p->bins1_map.end()) {
      bmin = min<double>(bmin, j->second);
      bmax = max<double>(bmax, j->second);
      j++;
    }
    cout << "        Min bin val: " << bmin << endl;
    cout << "        Max bin val: " << bmax << endl;
    cout << "Cache contains " << p->bins1_map.size() 
	 << " nonzero bins out of " << nbins*NUM << " total\n";
#endif
    cout << "Cache contains " << p->bins1_map.size() 
	 << " nonzero bins out of " << nbins*NUM << " total\n";
  }

  return p;
}


double PopModelCacheF::mm_fraction(double mm) 
{
  const double facbzp = mback/pow(10.0, 19.8671*0.4), mzp = 14.5;

  double disp = m0flux*m2flux - m1flux*m1flux;
  if (disp<=0.0) return 1.0;
  
  double ff = ( pow(10.0, -0.4*(mm-mzp)) - 1.0 )*facbzp;
  return 0.5*(1.0+erf((ff-m1flux)/sqrt(2.0*disp)));
}

void PopModelCacheF::compute_mm()
{
  if (mm_computed) return;

  mm_computed = true;

  vector<double> fmin = pops->MinMag();
  vector<double> fmax = pops->MaxMag();
  vector<double> fwid = pops->MagDelta();

  vector<double> fmin1(fmin);
  vector<double> fmax1(fmax);

  const int num=40;
  int idx;

  if (cmd) idx = 0;
  else idx = fmin.size()-1;

				// Histogram in K-band
  double dk = (fmax[idx] - fmin[idx])/(num-1);
      
				// Mean values
  mmean  = 0.0;
  mmean2 = 0.0;
  mavg   = 0.0;
  double mval, bval;

  for (int nn=0; nn<nages; nn++) {

    for (int i=0; i<num; i++) {
      fmin1[idx] = fmin[idx] + dk*i;
      fmax1[idx] = fmin[idx] + dk*(i+1);

      try {
	mval = 0.5*(fmin1[idx]+fmax1[idx]);
	bval = popweight[nn] * pops->Bin(fmin1, fmax1, ages[nn], METAL, 0.0);
	mmean  += dk*bval * pow(10.0, -0.4*mval);
	mmean2 += dk*bval * pow(10.0, -0.8*mval);
	mavg   += dk*bval;
      }
      catch (BIEException err) {
	cerr << err.getErrorMessage() << endl;
      }
    }

  }
  
  if (mavg > 0.0) {
    mmean  /= mavg;
    mmean2 /= mavg;
  }

}


PopCFPtr PopModelCacheF::compute_bins(const coordPair& P, SampleDistribution *d)
{
  double sech, R, z, s=0.0, dens;
  vector<real> binsT(nbins, 0.0);

  PopCFPtr p;
  mmap::iterator it = cache.find(P);
  if (it == cache.end()) {
    // #ifdef DEBUG
    if (mpi_used) cout << "Process " << myid << ": ";
    cout << "Cache miss: computing line-of-sight L=" << P.first
	 << " B=" << P.second << endl;
    // #endif    
    p = generate(P, d);
  }  else {
#ifdef DEBUG
    if (mpi_used) cout << "Process " << myid << ": ";
    cout << "line-of-sight L=" << P.first
	 << " B=" << P.second << " found in cache" << endl;
#endif
    p = cache[P];
#ifdef USE_LIST
				// Move entry to back of list
    cachelist.erase(p->it);
    cachelist.push_back(p);
    p->it = --cachelist.end();
#endif
  }

  if (mm_use) {
    m0flux = m1flux = m2flux = 0.0;
    compute_mm();
  }

  for (int n=0; n<NUM; n++) {
				// Coordinates
    R = p->R[n];
    z = p->z[n];
    if (DELTA) s = p->s[n];

				// Loop over each components
    for (int k=0; k<Mcur; k++) {

      if (DELTA) {
				// Test pulse
	double dtr = (s-RT0)/DRT0;
	dens = exp(-0.5*dtr*dtr)/sqrt(2.0*M_PI*DRT0*DRT0);

	if (dens>1.0e-8) {
	  cout << "Dens=" << dens << "  DMod=" << p->dm[n] << "\n";
	}

      } else {
				// Disk model
	sech = 2.0/(exp(z*0.5e3/pt[k][1]) + exp(-z*0.5e3/pt[k][1]));
	dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      }

      if (mm_use) {
	m0flux += wt[k] * mnorm*p->mm0[n];
	m1flux += wt[k] * mnorm*p->mm0[n]* dens * mmean  * pow(10.0, -0.4*p->mm1[n]);
	m2flux += wt[k] * mnorm*p->mm0[n]* dens * mmean2 * pow(10.0, -0.8*p->mm1[n]);
      }

      for (int j=0; j<nbins; j++)
	if (use_vector)
	  binsT[j] += wt[k]*dens * p->bins1[n*nbins + j];
	else
	  binsT[j] += wt[k]*dens * p->bin1(n*nbins + j);
    }
  }

				// Enter data model into hash table
  if (use_vector)
    p->bins = vector<real>(nbins);
  else
    p->reset_bin();

  int idx;
  if (cmd) idx = 0;
  else     idx = Nflux-1;
  double binfac = 1;

  for (int j=0; j<nbins; j++) {

    if (use_vector) {
      if (mm_use) binfac = mm_fraction(0.5*(lowb[idx][j]+highb[idx][j]));
      p->bins[j] = max<real>(binsT[j]*binfac, 0.0);
    }
    else {
      if (mm_use) binfac = mm_fraction(0.5*(lowb[idx][j]+highb[idx][j]));
      if (binsT[j] > zerotol) p->bins_map[j] = binsT[j]*binfac;
    }
  }

  return p;
}

double PopModelCacheF::NormEval(double L, double B, SampleDistribution *d)
{
  if (!good_bounds) return 1.0;
  
  coordPair P(L, B);
  PopCFPtr p(compute_bins(P, d));
  lastP = P;

  double norm = 0.0;

  if (use_vector) {
    for(unsigned j=0; j<p->bins.size(); j++) norm += p->bins[j];
  } else {
    map<int, double, less<int> >::iterator j = p->bins_map.begin();
#ifdef DEBUG
    double bmin=1.0e20, bmax=0.0;
    int curcnt=0;
    for(; j != p->bins_map.end(); j++) {
      norm += j->second;
      bmin = min<double>(bmin, j->second);
      bmax = max<double>(bmax, j->second);
      if (j->second<zerotol) curcnt++;
      curcnt++;
    }
    cout << "        Min bin val: " << bmin << endl;
    cout << "        Max bin val: " << bmax << endl;
    cout << "Found a total of " << curcnt 
	 << " with val < " << zerotol 
	 << " out of " << nbins << endl;
#else
    for(; j != p->bins_map.end(); j++) norm += j->second;
#endif
  }

  return norm;
}




vector<double> PopModelCacheF::Evaluate(double L, double B, 
					SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);
  
  PopCFPtr p;
  eqcoord eq;

  coordPair P(L, B);
  if (eq(P, lastP))
    p = cache[P];
  else
    p = compute_bins(P, d);
  
  if ((int)work.size() != nbins) work = vector<double>(nbins);
  if (use_vector)
    for (int i=0; i<nbins; i++) work[i] = p->bins[i];
  else
    for (int i=0; i<nbins; i++) work[i] = p->bin(i);
  return work;
}


void PopModelCacheF::PrintCache()
{
  cout << "Cache contains " << cache.size() << " entries\n";

  int cnt=0;
  mmap::iterator it;
  for (it = cache.begin(); it != cache.end(); it++) {
    cout << setw(4) << cnt++ 
	 <<  setw(15) << it->first.first
	 <<  setw(15) << it->first.second
	 << endl;
  }
}

//======================================================================


void PopCacheF::reset_bin1(void)
{
  bins1_map.erase(bins1_map.begin(), bins1_map.end());
}

void PopCacheF::reset_bin(void)
{
  bins_map.erase(bins_map.begin(), bins_map.end());
}

double PopCacheF::bin1(int j)
{
  if (bins1_map.find(j) != bins1_map.end()) 
    return bins1_map[j];
  else
    return 0.0;
}

double PopCacheF::bin(int j)
{
  if (bins_map.find(j) != bins_map.end()) 
    return bins_map[j];
  else
    return 0.0;
}

