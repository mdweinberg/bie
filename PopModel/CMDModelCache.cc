// This may look like C code, but it is really -*- C++ -*-

#undef TIMER

#include <iomanip>
#include <sstream>

using namespace std;

#include <errno.h>

#include <BIEmpi.h>
#include <gvariable.h>

#include <BIEException.h>
#include <CMDModelCache.h>
BIE_CLASS_EXPORT_IMPLEMENT(BIE::CMDModelCache)


#ifdef TIMER
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <Timer.h>
static Timer *timer;
#endif

#include <gaussQ.h>

using namespace BIE;

int    CMDModelCache::NUM=200;	// 800?
double CMDModelCache::ALPHA=0.0;
double CMDModelCache::BETA=0.0;
double CMDModelCache::Z0=15.0;
double CMDModelCache::EFAC=1.0;
double CMDModelCache::METAL=0.019;
double CMDModelCache::R0=8.0;
double CMDModelCache::RMIN=0.01;
double CMDModelCache::RMAX=30.0;
bool   CMDModelCache::RLOG=true;
double CMDModelCache::AMIN=0.5;
double CMDModelCache::AMAX=8.0;
double CMDModelCache::HMIN=100.0;
double CMDModelCache::HMAX=3500.0;
double CMDModelCache::QMIN=0.0;
double CMDModelCache::QMAX=1.0;
double CMDModelCache::CUTOFF=5.0;
double CMDModelCache::Log10 = log(10.0);
double CMDModelCache::onedeg = M_PI/180.0;
double CMDModelCache::minval = 1.0e-8;
double CMDModelCache::zerotol = 1.0e-12;
string CMDModelCache::DataDir = "/home/weinberg/3Mtab";


union coordUnion {
  float z;
  unsigned int i;
  unsigned char c[4];
};


const coordPair toobig(100.0, 100.0);

CMDModelCache::CMDModelCache(int ndim, SampleDistribution* _dist, CMD* cmd0)
{
  Ndim = ndim;
  pt.resize(ndim);

  cmd = cmd0;
  marge = cmd->get_colors();
  Nflux = marge.size();

#ifdef TIMER
  timer = new Timer(true);
#endif
				// Sanity check
				// 
  if (_dist->getdim(0) != Nflux) {
    ostringstream msg;
    msg << "CMDModelCache: SampleDistribution is not " << _dist->getdim(0)
	  << " dimensional, the model has " << Nflux << " fluxes";
      throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }
    

				// Cache the bin boundaries for the data
				// histogram
				// 
  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
    

				// Create bin boundary vectors
    for (int j=0; j<Nflux; j++) {
      dvector t;
      lowb.push_back(t);
      highb.push_back(t);
    }
				// Cache the bin boundaries for the data
    nbins = histo->numberData(); // histogram
    for (int i=0; i<nbins; i++) {
      for (int j=0; j<Nflux; j++) {
	lowb[j].push_back(histo->getLow(i)[j]);
	highb[j].push_back(histo->getHigh(i)[j]);
      }
    }
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
    nbins = 1;
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);



				// Get extinction model
  sfd = boost::shared_ptr<SFDS>(new SFDS(DataDir, 1.0));

				// Default line-of-sight integration knots
				// 
  intgr = JacoQPtr(new JacoQuad(NUM, ALPHA, BETA));
  lastP = toobig;
}

CMDModelCache::~CMDModelCache()
{
#ifdef TIMER
  delete timer;
#endif
}

				// Model: exponential * sech^2 disk

vector<string> CMDModelCache::ParameterLabels()
{
  vector<string> ret;
  ret.push_back("Length");
  ret.push_back("Height");
  if (Ndim>2) {
    ret.push_back("Quad amplitude");
    ret.push_back("Quad length");
    ret.push_back("Position angle");
  }
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void CMDModelCache::Initialize(State& s)
{
  // Ignore the component weight and number of components
  for (int j=0; j<Ndim; j++) pt[j] = s[2+j];

  check_bounds();
  lastP = toobig;
}

void CMDModelCache::Initialize(vector<double>& w, vector<double>*& p)
{
  pt = p[0];

  check_bounds();
}

void CMDModelCache::check_bounds()
{
  good_bounds = true;
				// Check bounds

  if (Ndim==2) {

    if (pt[0] > AMAX || pt[0] < AMIN   ||
	pt[1] > HMAX || pt[1] < HMIN   
	)  good_bounds = false;

  } else {

    if (pt[0] > AMAX || pt[0] < AMIN   ||
	pt[1] > HMAX || pt[1] < HMIN   ||
	pt[2] > QMAX || pt[2] < QMIN   ||
	pt[3] > AMAX || pt[3] < AMIN   
	)  good_bounds = false;
  }
				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int j=0; j<Ndim; j++) cout << setw(15) << pt[j];
    cout << "\n";
  }
#endif

}

CMDCPtr CMDModelCache::generate(const coordPair& P, SampleDistribution *d)
{
  double s, R, x, y, z, r2;

  double L = P.first;
  double X = P.second;
  double B = asin(X);

  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

  float extinction[3] = {0.90, 0.53, 0.34};
#ifdef DEBUG
  vector<float> maxext(Nflux, 0.0);
#endif
  const int nebv = 3;
  HermQuad lq(nebv);

				// Make hash table entry
  CMDCPtr p(new CMDCache(L, X));
  cache[P] = p;


				// Set up line-of-sight cache
  p->R.resize(NUM);
  p->z.resize(NUM);
  p->cos2.resize(NUM);
  p->sin2.resize(NUM);
  p->erase_bin1();

  vector<double> fluxmin(Nflux), fluxmax(Nflux), ext(Nflux);
  vector<double> absorb(NUM), fac(NUM), dmod(NUM);

  if (RLOG) RMIN = max<double>(RMIN, 0.001);
  
				// Population cache
  vector<real> binsT(nbins);

				// Begin integration over line of sight
  for (int n=0; n<NUM; n++) {

    if (RLOG) {
      s = RMIN*exp(intgr->knot(n+1) * (log(RMAX) - log(RMIN)));
      fac[n] = s*s*s*intgr->weight(n+1) * (log(RMAX) - log(RMIN));
    } else {
      s = RMIN + intgr->knot(n+1) * (RMAX - RMIN);
      fac[n] = s*s*intgr->weight(n+1) * (RMAX - RMIN);
    }

    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    x = s*cosB*cosL - R0;
    y = s*cosB*sinL;
    z = s*sinB - Z0*1.0e-3;
    r2 = sqrt(R*R + z*z) + 1.0e-10;

    p->R[n] = R;
    p->z[n] = z;
    p->cos2[n] = (x*x - y*y)/r2;
    p->sin2[n] = 2.0*x*y/r2;

    dmod[n] = 5.0*log(1.0e3*s)/Log10 - 5.0;
    
      				// Get extinction
    float ebvmin=0, ebvmax=0, ebv, sig;
    float lebvmin=0, lebvmax=0, lebv, lsig;
    float lfrac, ebv1;
    sfd->getsfd_distance(L, B, s, lfrac,
			 ebv, ebvmin, ebvmax, sig, 
			 lebv, lebvmin, lebvmax, lsig); 
			 
    
    for (int j=0; j<nbins; j++) binsT[j] = 0.0;

    for (int nn=1; nn<=nebv; nn++) {
      
      ebv1 = exp(lebv + lq.knot(nn)*lsig);

      //=====!!!!NOT GENERAL: assumes three band J, H, K!!!!======
      for (int l=0; l<Nflux; l++) {
	ext[l] = 0.0;
	for (int i=0; i<3; i++) {
	  ext[l] += ebv1 * extinction[i]*marge[l][i];
	}
      }
#ifdef DEBUG
      for (int l=0; l<Nflux; l++) maxext[l] = max<float>(maxext[l], ext[l]);
#endif
      //==========================================================
    
    
      for (int j=0; j<nbins; j++) {
				// Make fluxes for each component
	for (int l=0; l<Nflux; l++) {
	  if (type==binned) {
	    fluxmin[l] = lowb[l][j] - ext[l];
	    fluxmax[l] = highb[l][j] - ext[l];
	  } else if (type==point) {
	    fluxmin[l] = ((PointDistribution*)d)->Point()[l] - ext[l];
	  }
	}
	
	if (type==binned)
	  binsT[j] += fac[n] * cmd->Bin(fluxmin, fluxmax, dmod[n]) *
	    lq.weight(nn)*lfrac*5.641895835477563e-01;
	else if (type==point)
	  binsT[j] += fac[n] * cmd->Interpolate(fluxmin, dmod[n]) *
	    lq.weight(nn)*lfrac*5.641895835477563e-01;
	//                    1/sqrt(pi)

      } // End bin loop

    } // End sfd loop

    
    //==========================================================
    
    
    for (int j=0; j<nbins; j++) {
				// Make fluxes for each component
      for (int l=0; l<Nflux; l++) {
	if (type==binned) {
	  fluxmin[l] = lowb[l][j];
	  fluxmax[l] = highb[l][j];
	} else if (type==point) {
	  fluxmin[l] = ((PointDistribution*)d)->Point()[l];
	}
      }
      
      if (type==binned)
	binsT[j] += fac[n] * cmd->Bin(fluxmin, fluxmax, dmod[n]) * (1.0 - lfrac);
      else if (type==point)
	binsT[j] += fac[n] * cmd->Interpolate(fluxmin, dmod[n]) * (1.0 - lfrac);
      
    } // End bin loop

				// Load list
    for (int j=0; j<nbins; j++)
      if (binsT[j] > zerotol) p->add_bin1(n*nbins+j, binsT[j]);
    
  } // End line-of-sight loop

#ifdef DEBUG
  double bmin=1.0e20, bmax=0.0;
  double ret;
    
  p->reset_bin1();
  for (int n=0; n<NUM; n++) {
    for (int j=0; j<nbins; j++) {
      if ((ret=p->get_bin1(n*nbins+j))>0.0) {
	bmin = min<double>(bmin, ret);
	bmax = max<double>(bmax, ret);
      }
    }
  }
  cout << "        Min bin val: " << bmin << endl;
  cout << "        Max bin val: " << bmax << endl;
  cout << "Cache contains " << p->bins1.size() 
       << " nonzero bins out of " << nbins*NUM << " total\n";

  cout << "Extinction:";
  for (int l=0; l<Nflux; l++) cout << " " << maxext[l];
  cout << "\n";
#endif

  return p;
}


CMDCPtr CMDModelCache::compute_bins(const coordPair& P, SampleDistribution *d)
{
  double sech, R, z, dens;
  vector<real> binsT(nbins, 0.0);

  CMDCPtr p;
  mmapCMD::iterator it = cache.find(P);
  if (it == cache.end()) {
#ifdef TIMER
    timer->reset();
    timer->start();
    p = generate(P, d);
    timer->stop();
    cout << "Computing line-of-sight L=" << P.first
	 << " sinB=" << P.second << " took "
	 << 1.0e-6*(timer->getTime().getUserTime() +
		    timer->getTime().getSystemTime() )
	 << " seconds" << endl;
#else
#ifdef DEBUG
    if (mpi_used) cout << "Process " << myid << ": ";
    cout << "Computing line-of-sight L=" << P.first
	 << " sinB=" << P.second << endl;
#endif    
  p = generate(P, d);
#endif
  } else {
    //    p = cache[P];
    p = it->second;
#ifndef TIMER
#ifdef DEBUG
    if (mpi_used) cout << "Process " << myid << ": ";
    cout << "line-of-sight L=" << P.first
	 << " sinB=" << P.second << " found in cache" << endl;
#endif
#endif
  }

#ifdef TIMER
  timer->reset();
  timer->start();
#endif

  double cos2p=0.0, sin2p=0.0;
  if (Ndim==5) {
    cos2p = cos(2.0*pt[4]);
    sin2p = sin(2.0*pt[4]);
  }

  p->reset_bin1();

  for (int n=0; n<NUM; n++) {
				// Coordinates
    R = p->R[n];
    z = p->z[n];
				// Disk model
    sech = 2.0/(exp(z*0.5e3/pt[1]) + exp(-z*0.5e3/pt[1]));
    dens = exp(-R/pt[0])*sech*sech / (pt[0]*pt[0]*pt[1]);
    if (Ndim==5) 
      dens *= 1.0 + 
	pt[2]*(p->cos2[n]*cos2p + p->sin2[n]*sin2p)/(1.0+pow(R/pt[3], 5.0));
    
#ifdef VDEBUG
    double bmax = 0.0;
#endif

    for (int j=0; j<nbins; j++) {
#ifdef VDEBUG
      double tmp = p->get_bin1(n*nbins+j);
      bmax = max<double>(bmax, tmp);

      binsT[j] += dens * tmp;
#else
      binsT[j] += dens * p->get_bin1(n*nbins + j);
#endif
    }

#ifdef VDEBUG    
    if (bmax>1.0e-16)
      cout << setw(15) << R << setw(15) << z << setw(15) << dens 
	   << setw(15) << bmax << endl;
#endif
  }
				// Enter data model into list
  p->erase_bin();
  int inum=0;

  double bmin=1.0e20, bmax=0.0;
  for (int j=0; j<nbins; j++) {
    bmin = min<double>(bmin, binsT[j]);
    bmax = max<double>(bmax, binsT[j]);
  }

  p->norm = 0.0;

  for (int j=0; j<nbins; j++) {
    if (bmax>zerotol && binsT[j] > bmax*zerotol) {
      p->add_bin(j, binsT[j]);
      p->norm += binsT[j];
      inum++;
    } 
  }

#ifdef DEBUG
  if (inum==0)
    cout << "Process " << myid << ": zero norm, bmax=" << bmax << "\n";
#endif

#ifdef TIMER
  timer->stop();
  cout << "Computing model L=" << P.first
       << " sinB=" << P.second << " took "
       << 1.0e-6*(timer->getTime().getUserTime() +
		  timer->getTime().getSystemTime() )
       << " seconds" << endl;
#endif

  return p;
}

double CMDModelCache::NormEval(double L, double X, SampleDistribution *d)
{
  if (!good_bounds) return 1.0;
  
  coordPair P(L, X);
  CMDCPtr p(compute_bins(P, d));

  lastP = P;
  lastC = p;

#ifdef DEBUG
  double bmin=1.0e20, bmax=0.0;
  double ret;

  int curcnt=0;
  cout << "Number in cache " << p->bins.size() << endl;

  p->reset_bin();
  for (int j=0; j<nbins; j++) {
    if ((ret=p->get_bin(j))>0.0) {
      bmin = min<double>(bmin, ret);
      bmax = max<double>(bmax, ret);
      if (ret<zerotol) curcnt++;
    }
  }
  cout << "        Min bin val: " << bmin << endl;
  cout << "        Max bin val: " << bmax << endl;
  cout << "Found a total of " << curcnt 
       << " with val < " << zerotol 
       << " out of " << nbins << endl;

  if (p->norm<=0.0) {
    cout << "Process " << myid << ": zero norm in NormEval\n";
    cout << "  a=" << pt[0] << "  h=" << pt[1];
    if (Ndim==5) 
      cout << "  qa=" << pt[2] << "  ql=" << pt[3] << "  qp=" << pt[4];

    /*
      vector<double> fmin = cmd->MinMag();
      vector<double> fmax = cmd->MaxMag();
      double distmod = cmd->DistMod();
      cout << "  cmd=" << cmd->Bin(fmin, fmax, distmod);
    */

    cout << endl;
  }
#endif

  return p->norm;
}




vector<double> CMDModelCache::Evaluate(double L, double X, 
				       SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);
  
  CMDCPtr p;
  eqcoord eq;

  coordPair P(L, X);
				// Use saved pointer from NormEval
  if (eq(P, lastP) && type==binned)
    p = lastC;
  else
    p = compute_bins(P, d);
  
  if ((int)work.size() != nbins) work.resize(nbins);

  p->reset_bin();
  for (int i=0; i<nbins; i++) work[i] = p->get_bin(i);

  return work;
}

void CMDModelCache::PrintCache()
{
  cout << "Cache contains " << cache.size() << " entries\n";

  int cnt=0;
  mmapCMD::iterator it;
  for (it = cache.begin(); it != cache.end(); it++) {
    cout << setw(4) << cnt++ 
	 <<  setw(15) << it->first.first
	 <<  setw(15) << it->first.second
	 << endl;
  }
}


//======================================================================


void CMDCache::erase_bin1(void)
{
  bins1.erase(bins1.begin(), bins1.end());
}

void CMDCache::erase_bin(void)
{
  bins.erase(bins.begin(), bins.end());
}

void CMDCache::reset_bin(void)
{
  pbin = bins.begin();
}

void CMDCache::reset_bin1(void)
{
  pbin1 = bins1.begin();
}

void CMDCache::add_bin1(int i, double x)
{
  pdata.first = i;
  pdata.second = x;
  bins1.push_back(pdata);
}

void CMDCache::add_bin(int i, double x)
{
  pdata.first = i;
  pdata.second = x;
  bins.push_back(pdata);
}

double CMDCache::get_bin1(int i)
{
  if (pbin1 == bins1.end()) return 0.0;

  double ans;

  if (pbin1->first == i) {
    ans = pbin1->second;
    pbin1++;
  } 
  else ans = 0.0;

  return ans;
}

double CMDCache::get_bin(int i)
{
  if (pbin == bins.end()) return 0.0;

  double ans;

  if (pbin->first == i) {
    ans = pbin->second;
    pbin++;
  } 
  else ans = 0.0;

  return ans;
}

