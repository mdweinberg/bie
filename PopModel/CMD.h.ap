// This may look like C code, but it is really -*- C++ -*-


#ifndef _CMD_h
#define _CMD_h

@include_persistence

#include <cmath>

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <map>

#include <boost/serialization/unordered_map.hpp>

using namespace std;

#include <ACG.h>
#include <Uniform.h>

#include <Populations.h>

size_t ivecHash(vector<int> P);


/**
   Hash object for color-magnitude diagram hashmap has to be
   specialized in namespace it was defined in
*/
namespace std
{
  template <>
  struct hash< vector<int> > 
  {
    //! The hash function for STL
    size_t operator()(vector<int> s) const 
    { 
      return ivecHash(s); 
    }
  };
}


//! Comparison for hash in color-magnitude diagram
struct eqivector
{
  //! Hash operator for STL
  bool operator()(vector<int> c1, vector<int> c2) const
  {
    int n1 = c1.size(), n2 = c2.size();
    if (n1 != n2) return false;
    for (int i=0; i<n1; i++) if (c1[i]!=c2[i]) return false;
    return true;
  }
};



namespace BIE
{
  
  /**
     Binned CMD model in absolute magnitudes with user supplied offset
     (e.g. so that one can use a cluster or LMC color-magnitude diagram
  */
  //+ CLICLASS CMD
  //! Reads binned isochrone in near-infrared K band and 2 colors
  class @persistent_root(CMD)
  {
    //! A mnemonic for a vector of doubles
    typedef vector<double> dvector;

    //! A mnemonic for a vector of ints
    typedef vector<int>    ivector;

    //! A mnemonic for a hasher for a vector of ints
    typedef std::unordered_map<ivector, float, std::hash<ivector>, eqivector> HMap;

  private:

    int @autopersist(ncolors);
    vector<double> @autopersist(colormin);
    vector<double> @autopersist(colormax);
    vector<double> @autopersist(wcolor);
    vector<int> @autopersist(nbins);
    vector<int> @autopersist(ncol);
    vector<ivector> @autopersist(colors);
    double @autopersist(distmod);

    vector<double> @autopersist(colr);
    vector<int> @autopersist(ii);
    vector<int> @autopersist(icol);
    vector<int> @autopersist(icolmin);
    vector<int> @autopersist(icolmax);
    vector<int> @autopersist(icoloff);
    vector<double> @autopersist(colwght), @autopersist(off);
    vector<dvector> @autopersist(f2);
    vector<ivector> @autopersist(i2);

    void initialize_realization();
    ACGPtr @autopersist(gen);
    UniformPtr @autopersist(uniform);
    bool @autopersist(realize_setup);
    double @autopersist(realize_norm);
    map<double, int, less<double> > @autopersist(realize_data);

    string @autopersist(datafile);

    void read_data(istream&, double);
    HMap @autopersist(Hash);

  public:

    /// Turn on debugging info (don't do this . . .)
    static bool @autopersist(verbose);

    /// Random seed
    static int @autopersist(seed);


    //+ CLICONSTR string
    //! Constructor
    //! -----------
    //! "directory" is the path to the CMD tables
    CMD(string directory, double minval=1.0e-10);

    /// Destructor 
    virtual ~CMD() {}

    /**@name Accessors */
    //@{
    /// Get star count value in bin with given flux
    double GetValue(vector<double>& flux);

    /// Get star count value in bin with given grid indices
    double GetValue(vector<int>& indx);

    /// Interpolate on flux on grid
    double Interpolate(vector<double>& flux, double distmod);

    /// Sum over bins beteen flux boundaries
    double Bin(vector<double>& fmin, vector<double>& fmax, double distmod);
    //@}

    /// Set parameters (not currently used
    void SetParameters(void) {};

    /**@name Grid properties */
    //@{
    /// Get color flags by entry
    vector<int> get_color(int n) {
      int nn = min<int>(max<int>(0, n), ncolors-1);
      return colors[nn];
    }
    /// Get all color flags
    vector<ivector> get_colors(void) { return colors; }

    /// Get number of colors
    int get_ncolors(void) { return ncolors; }

    /// Min flux
    vector<double> MinMag() { return colormin; }

    /// Max flux
    vector<double> MaxMag() { return colormax; }

    /// Bin widths
    vector<double> ColorDelta() { return wcolor; }

    /// Bin counts
    vector<int> NumBins() { return nbins; }

    /// Distance modulus
    double DistMod() { return distmod; }

    //@}

    /** @name Verify data against files (for debugging) */
    //@{
    void VerifyDouble(double distmod);
    void VerifyInt();
    //@}

    /// Realize a point in the CMD
    vector<double> genPoint();
    
    /// Hash statistics (for debugging)
    void HashStat();

    @persistent_end
  };
}

#endif

