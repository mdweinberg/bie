#ifdef CFITSIO

#include <math.h>
#include <string>
#include <SFDread.H>

std::string SFDread::directory("");
char SFDread::northfilename[]  = "SFD_dust_4096_ngp.fits";
char SFDread::southfilename[]  = "SFD_dust_4096_sgp.fits";

int SFDread::next( EBV* p, int nbuf )
{

  float buffer[nbuf];
  int nfound;
  status = 0;

  if (first) {

    std::string file = directory + "/" + northfilename;

    if ( fits_open_file(&fptr, file.c_str(), READONLY, &status) )
      printerror( status );
    
    first = false;
    north = true;
    char ftype[] = "NAXIS";

    if ( fits_read_keys_lng(fptr, ftype, 1, 2, naxes, &nfound, &status) )
      printerror( status );

    npixels  = naxes[0] * naxes[1]; // number of pixels in the image
    fpixel   = 1;
    nullval  = 0;		// don't check for null values in the image
    hemis    = 1;
    ndata    = 0;
  } 
  else if (npixels == 0 && north) {

    if ( fits_close_file(fptr, &status) )
      printerror( status );

    std::string file = directory + "/" + southfilename;

    if ( fits_open_file(&fptr, file.c_str(), READONLY, &status) )
      printerror( status );
    
    north = false;
    south = true;
    char ftype[] = "NAXIS";
  
    if ( fits_read_keys_lng(fptr, ftype, 1, 2, naxes, &nfound, &status) )
      printerror( status );

    npixels  = naxes[0] * naxes[1]; // number of pixels in the image
    fpixel   = 1;
    nullval  = 0;		// don't check for null values in the image
    hemis    = -1;
    ndata    = 0;
  }
  else if (npixels == 0 && south) {

    if ( fits_close_file(fptr, &status) )
      printerror( status );

    north = false;
    south = false;
  }

  if (!north && !south) return 0;

  int i, j, icnt=0;
  double x, y, l, b;
    
  if (npixels > 0)
    {
      nbuffer = npixels;
      if (npixels > nbuf)
        nbuffer = nbuf;		// read as many pixels as will fit in buffer

      if ( fits_read_img(fptr, TFLOAT, fpixel, nbuffer, &nullval,
			 buffer, &anynull, &status) )
	printerror( status );

      for (int ii = 0; ii < nbuffer; ii++)  {
	
	j = ndata/4096;
	i = ndata - 4096*j;

	x = ((double)i - 2047.5)/2048.0;
	y = -((double)j - 2047.5)*hemis/2048.0;

	l = atan2(y, x);
	b = asin( (1.0 - (x*x + y*y)));
	if (hemis>0) b = fabs(b);
	else b = -fabs(b);
	
	p[icnt].l = l;
	p[icnt].b = b;
	p[icnt].ebv = buffer[ii];

	ndata++;
	icnt++;
      }

      npixels -= nbuffer;	// increment remaining number of pixels
      fpixel  += nbuffer;	// next pixel to be read in image
    }

  return icnt;
}

void SFDread::printerror( int status)
{
  if (status)
    {
				// print error report
      fits_report_error(stderr, status);
      
      exit( status );		// quit and returning error status
    }
  return;
}


#endif
