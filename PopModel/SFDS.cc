/*******************************************************
  Probes table of SFD98 E(B-V) maps, blocked into 
  0.4x0.4 degree bins, for mean reddening, variance,
  minimum and maximum, given a latitude and longitude,
  and radius around that point. (Input in degrees)
 
  AAC 5/19/01 
 *******************************************************/

#include <math.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

#include <SFDS.H>
#ifdef CFITSIO
#include <SFDread.H>
#endif

int    SFDS::NUM=40;
double SFDS::A1=2.5;
double SFDS::Z1=150.0;
bool   SFDS::RING=false;
double SFDS::RINGWIDTH=0.25;
double SFDS::RINGLOC=4.0;
double SFDS::RINGAMP=1.0;
double SFDS::CENTERWIDTH=1.0;
double SFDS::CENTERAMP=1.0;
double SFDS::NEARBYWIDTH=0.2;
double SFDS::NEARBYAMP=0.0;
double SFDS::Z0=15.0;
double SFDS::R0=8.0;
double SFDS::RMIN=0.01;
double SFDS::RMAX=100.0;
bool   SFDS::RLOG=true;


SFDelev::SFDelev()
{
  numbin = 0;
  computed = false;
}

SFDelev::SFDelev(float ds)
{
  Set(ds);
}

void SFDelev::Set(float ds)
{
  const double onedeg = M_PI/180.0;

  numbin = (int)( 2.0*M_PI/(ds*onedeg) );
  dphi = 2.0*M_PI/numbin;

  vec = vector<SFDelem>(numbin);
}

void SFDelev::enter(sfddata& data)
{
  if (numbin==0) {
    cerr << "sfdlev: uninitalized\n";
    exit(-1);
  }

  float l = data.lon;
  if (data.lon > 0) 
    l -= 2.0*M_PI*(int)(l/(2.0*M_PI));
  else
    l += 2.0*M_PI*(1+(int)(-l/(2.0*M_PI)));

  int indx = (int)(l/dphi);
				// Safety
  indx = min<int>(indx, numbin-1);
  indx = max<int>(indx, 0);

  vec[indx].lon = l;
  vec[indx].red += data.red;
#ifdef CFITSIO
  vec[indx].redmin = min<float>(vec[indx].redmin, data.red);
  vec[indx].redmax = max<float>(vec[indx].redmax, data.red);
  vec[indx].std += data.red*data.red;
#else
  vec[indx].redmin = min<float>(vec[indx].redmin, data.rmn);
  vec[indx].redmax = max<float>(vec[indx].redmax, data.rmx);
  vec[indx].std += data.var*data.var;
#endif
  vec[indx].num++;

  if (data.red>0.0) {
    vec[indx].lred += log(data.red);
    vec[indx].lstd += log(data.red)*log(data.red);
    vec[indx].lrmin = min<float>(vec[indx].lrmin, log(data.red));
    vec[indx].lrmax = max<float>(vec[indx].lrmax, log(data.red));
    vec[indx].lnum++;
  }

  // DEBUG
  if (isnan(vec[indx].lstd)) {
    cout << "vec[" << indx << "].lstd = Nan\n";
  }

}

void SFDelev::compute()
{
  if (numbin==0) {
    cerr << "sfdlev: uninitalized\n";
    exit(-1);
  }

  if (computed) return;
  computed = true;

  for (int i=0; i<numbin; i++) {
    if (vec[i].num) {
      vec[i].red /= vec[i].num;
      if (vec[i].num>1) 
	vec[i].std = sqrt(max<double>(vec[i].std - vec[i].red*vec[i].red*vec[i].num, 0.0) /
			  (vec[i].num-1));
      else
	vec[i].std = 0;
    }
    if (vec[i].lnum) {
      vec[i].lred /= vec[i].lnum;
      if (vec[i].lnum>1) 
	vec[i].lstd = 
	  sqrt(max<double>(vec[i].lstd - vec[i].lred*vec[i].lred*vec[i].lnum, 0.0) /
	       (vec[i].lnum-1));
      else
	vec[i].lstd = 0;
    }
  }
}

SFDelem* SFDelev::getval(float l)
{
  if (numbin==0) {
    cerr << "sfdlev: uninitalized\n";
    exit(-1);
  }

  compute();

  if (l > 0) 
    l -= 2.0*M_PI*(int)(l/(2.0*M_PI));
  else
    l += 2.0*M_PI*(1+(int)(-l/(2.0*M_PI)));

  int indx = (int)(l/dphi);
				// Safety
  indx = min<int>(indx, numbin-1);
  indx = max<int>(indx, 0);

  return &vec[indx];
}

SFDmat::SFDmat()
{
}

SFDmat::SFDmat(float ds)
{
  Set(ds);
}

void SFDmat::Set(float ds)
{
  const double onedeg = M_PI/180.0;

  numbin = (int)( 2.0/(ds*onedeg) );
  dx = 2.0/numbin;

  vec = vector<SFDelev>(numbin);
  for (int i=0; i<numbin; i++) vec[i].Set(ds);
}
  

void SFDmat::enter(sfddata& data)
{
  float x = sin(data.lat);
  int indx = (int)((1.0+x)/dx);
				// Safety
  indx = min<int>(indx, numbin-1);
  indx = max<int>(indx, 0);

  vec[indx].enter(data);
}

SFDelem* SFDmat::getval(float l, float b)
{
  double x = sin(b);
  int indx = (int)((1.0+x)/dx);

				// Safety
  indx = min<int>(indx, numbin-1);
  indx = max<int>(indx, 0);

  return vec[indx].getval(l);
}

SFDS::SFDS(const string& DataDir, float ds)
{
  sfddata data;

  // Create the extinction matrix
  
  sfdmat.Set(ds);

  // Convert from degrees to radians for distance calc
  
#ifdef CFITSIO 

  int number;
  SFDread sfdr;
  SFDread::directory = "/home/weinberg/3Mtab";
  EBV *p;
  int nbuf = 10000;
  p = new EBV [nbuf];

  while( (number=sfdr.next(p, nbuf)) ) {
    for (int i=0; i<number; i++) {
      data.lon = p[i].l;
      data.lat = p[i].b;
      data.red = p[i].ebv;

      sfdmat.enter(data);
    }

  }

#else
  const int nlin=257662;
  string datafile = DataDir + "/sfd98_rtab.txt\0";

  ifstream sfd(datafile.c_str());
  if (!sfd) {
    cerr << "getsfd: couldn't open data file <" << datafile << ">\n";
    exit(-1);
  }

  const int bufsize = 2048;
  char line[bufsize];

  for (int j=1; j<=nlin; j++) {

    sfd.getline(line, bufsize);
    istringstream in(line);
    
    in >> data.lon;
    in >> data.lat;
    in >> data.red;
    in >> data.var;
    in >> data.rmn;
    in >> data.rmx;

    sfdmat.enter(data);
  }
#endif

}


void SFDS::compute_interp(double s, int& indx, double& c1, double& c2)
{
  if (s<=radius.front())
    indx = 0;

  else if (s>=radius.back())
    indx = radius.size()-2;

  else {
    if (RLOG)
      indx = (int)( (log(s) - log(RMIN))/dr );
    else
      indx = (int)( (s - RMIN)/dr );
  }


  if (RLOG)
    c2 = (log(s) - log(radius[indx]))/dr;
  else
    c2 = (s - radius[indx])/dr;

  c1 = 1.0 - c2;
}


void SFDS::compute_extinction(SFDelem* data)
{
  double curabs, sech, lastabs=0.0, lasts=0.0;

  if (radius.size() != (unsigned)NUM) radius = vector<float>(NUM);
  data->absorb = radius;

  double cosB = cos(data->lat);
  double sinB = sin(data->lat);
  double cosL = cos(data->lon);
  double sinL = sin(data->lon);

  if (RLOG) RMIN = max<double>(RMIN, 0.001);

  double s, R, x, y, z;

				// Begin integration over line of sight
  for (int n=0; n<NUM; n++) {

    if (RLOG) {
      dr = (log(RMAX) - log(RMIN))/NUM;
      s = RMIN*exp(dr*n);
      radius[n] = s;
    } else {
      dr = (RMAX- RMIN)/NUM;
      s = RMIN + dr*n;
      radius[n] = s;
    }

    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    x = s*cosB*cosL - R0;
    y = s*cosB*sinL;
    z = s*sinB - Z0*1.0e-3;

				// Ring and central extinction
    if (RING) {

      curabs = 
	CENTERAMP*exp(-(R*R+z*z)/(2.0*CENTERWIDTH*CENTERWIDTH)) /
	pow(2.0*M_PI*CENTERWIDTH*CENTERWIDTH, 1.5)
	+
	RINGAMP*exp(-(R-RINGLOC)*(R-RINGLOC)/(2.0*RINGWIDTH*RINGWIDTH)) *
	exp(-z*z/(2.0*Z1*Z1*1.0e-6)) / (4.0*M_PI*M_PI*RINGWIDTH*Z1*1.0e-3)
	+
	NEARBYAMP*exp(-(x*x + y*y + z*z)/(2.0*NEARBYWIDTH*NEARBYWIDTH)) /
	(4.0*M_PI*M_PI*NEARBYWIDTH*NEARBYWIDTH)
	;
				// Standard disk extinction
    } else {
      sech = 0.5/( exp(z*0.5e3/Z1) + exp(-z*0.5e3/Z1) );
      curabs = exp(-R/A1)*sech*sech * (2.5e2/Z1) / (A1*A1*2.0*M_PI);
    }

    data->absorb[n] = 0.5*(s - lasts)*(lastabs+curabs);
    if (n>0) data->absorb[n] += data->absorb[n-1];
    lasts = s;
    lastabs = curabs;
  }

				// Normalize absorption
  double abmax = data->absorb[NUM-1];
  if (abmax>0.0)
    for (int n=0; n<NUM; n++) data->absorb[n] /= abmax;
}


void SFDS::getsfd(float l, float b, float &lfrac,
		  float& ebv, float& ebvmin, float& ebvmax,
		  float& sig, 
		  float& lebv, float& lebvmin, float& lebvmax,
		  float& lsig)
{
  SFDelem* data = sfdmat.getval(l, b);

  ebv = data->red;
  ebvmin = data->redmin;
  ebvmax = data->redmax;
  sig = data->std;

  lebv = data->lred;
  lebvmin = data->lrmin;
  lebvmax = data->lrmax;
  lsig = data->lstd;

  if (data->num>0)
    lfrac = (float)data->lnum / data->num;
  else
    lfrac = 0.0;
}

void SFDS::getsfd_distance(float l, float b, float s, float &lfrac,
			   float& ebv, float& ebvmin, float& ebvmax, 
			   float& sig,
			   float& lebv, float& lebvmin, float& lebvmax, 
			   float& lsig)
{
  int indx;
  float path;
  double c1, c2;

  SFDelem* data = sfdmat.getval(l, b);

				// Only compute l.o.s. when needed
  if (data->absorb.size()==0) compute_extinction(data);
  compute_interp(s, indx, c1, c2);

  path = max<double>(data->absorb[indx]*c1+data->absorb[indx+1]*c2, 0.0);

  ebv = data->red * path;
  ebvmin = data->redmin * path;
  ebvmax = data->redmax * path;
  sig = sqrt(data->std*data->std * path);

  lebv = data->lred + log(path);
  lebvmin = data->redmin + log(path);
  lebvmax = data->redmax + log(path);
  lsig = sqrt(data->lstd*data->lstd * path);

  if (data->num>0)
    lfrac = (float)data->lnum / data->num;
  else
    lfrac = 0.0;
}


/************************************************
  Measure pathlength through a uniform dust disk
  with a given height.   The total reddening 
  integrated from zero to infinite distance is
  supplied by another routine that samples from
  the Schlegel et al. (1998) maps.
  Use the reddening relations
  given in Bessell & Brett (1988) to go from 
  reddening to extinction in the bandpass.

  Requires: galactic coordinates in degrees,
  a distance in parsecs, and a total reddening
  E(B-V) from a prior routine.

  Returns:  A(J), A(H), A(K)

**************************************************/
