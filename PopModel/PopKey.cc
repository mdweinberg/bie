#include <PopKey.h>
BIE_CLASS_EXPORT_IMPLEMENT(PopKey)

PopKey::PopKey() 
{
  z   = 0; 
  dim = 0; 
}

PopKey::PopKey(int Z, vector<int>& Indx)
{
  z = Z; 
  dim = Indx.size();
  indx = vector<unsigned short int>(dim);
  for (int k=0; k<dim; k++) indx[k] = Indx[k];
}


PopKey::PopKey(const PopKey& t) 
{
  z = t.z; 
  dim = t.dim; 
  indx = vector<unsigned short int> (dim);
  for (int k=0; k<dim; k++) indx[k] = t.indx[k];
}
  
void PopKey::reset(int Z, vector<int>& Indx) {
  if (Indx.size() != (unsigned)dim) {
    dim = Indx.size();
    indx = vector<unsigned short int>(dim);
  }
  z = Z; 
  for (unsigned k=0; k<Indx.size(); k++) indx[k] = Indx[k];
}
  
