// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;


#include <PopModelND.h>
BIE_CLASS_EXPORT_IMPLEMENT(BIE::PopModelND)

using namespace BIE;

int PopModelND::NUM=800;
double PopModelND::ALPHA=0.0;
double PopModelND::BETA=0.0;
double PopModelND::NU=0.0;
double PopModelND::METAL=0.019;
double PopModelND::R0=8.0;
double PopModelND::RMAX=50.0;
double PopModelND::AMIN=0.5;
double PopModelND::AMAX=8.0;
double PopModelND::HMIN=100.0;
double PopModelND::HMAX=3000.0;
double PopModelND::Log10 = log(10.0);
double PopModelND::onedeg = M_PI/180.0;
string PopModelND::DataDir = "/home/weinberg/3Mtab";

/*
  Based on Andrew Cole's extinction routines on Schlegel's map
*/

void getsfd(float l, float b, float q, float& ebv, float& sig,
	    float& emn, float& emx, const string& DataDir);

void getext(float s, float l, float b, float ebv, 
	    float& aj, float& ah, float& ak);


PopModelND::PopModelND(int ndim, int mdim, SampleDistribution* _dist)
{
  Ndim = ndim;
  M = mdim;
  Mcur = M;

  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

				// This is hardwired to J & K for now . . .
				// To be generalized
  clivectori marge(3);
  marge()[0] = 0;		// Marginalize J-band (yes=1, no=0)
  marge()[1] = 1;		// Marginalize H-band
  marge()[2] = 0;		// Marginalize K-band
  pops = new PopulationsApp(DataDir, &marge);
  Nflux = 2;

  
  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
    

				// Cache the bin boundaries for the data
				// histogram
				// Sanity check
    if (histo->getdim(0) != Nflux) {
      ostringstream msg;
      msg << "PopModelND: BinnedDistribution is not " << histo->getdim(0)
	  << " dimensional, model has " << Nflux << " fluxes\n";
      throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
    }
    

				// Create bin boundary vectors
    for (int j=0; j<Nflux; j++) {
      dvector t;
      lowb.push_back(t);
      highb.push_back(t);
    }
				// Cache the bin boundaries for the data
    nbins = histo->numberData(); // histogram
    for (int i=0; i<nbins; i++) {
      for (int j=0; j<Nflux; j++) {
	lowb[j].push_back(histo->getLow(i)[j]);
	highb[j].push_back(histo->getHigh(i)[j]);
      }
    }
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
    nbins = 1;
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);

  
				// Default line-of-sight integration knots
				// 
  intgr = JacoQPtr(new JacoQuad(NUM, ALPHA, BETA));
}

PopModelND::~PopModelND()
{
  delete pops;
}

				// Model: exponential * sech^2 disk

vector<string> PopModelND::ParameterLabels()
{
  vector<string> ret;
  ret.push_back("Length");
  ret.push_back("Height");
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void PopModelND::Initialize(State& s)
{
  Mcur = (int)floor(s[0]+0.01);
  for (int k=0; k<Mcur; k++) wt[k] = s[k+1];
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s[1+Mcur+k*Ndim+j];
  }

  check_bounds();
}

void PopModelND::Initialize(vector<double>& w, vector<double>*& p)
{
  wt = w;
  for (int k=0; k<Mcur; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}

void PopModelND::check_bounds()
{
  good_bounds = true;
				// Check bounds
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;
  }
  for (int k=0; k<Mcur; k++) {
    if (pt[k][0] > AMAX || pt[k][0] < AMIN   ||
	pt[k][1] > HMAX || pt[k][1] < HMIN    )  good_bounds = false;
  }

				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<2; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
#endif
}

void PopModelND::generate(double L, double B, SampleDistribution *sd)
{
  vector<double> fluxmin(Nflux), fluxmax(Nflux), ext(Nflux);
  double s, R, z, fac, dmod, val=0.0;
  double cosL = cos(L);
  // double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

  vector<double> ages = pops->Ages();
  nages = (int)ages.size();
  vector<double> popweight(nages);

				// Continuous creation
  double sum = 0.0;
  for (int nn=0; nn<nages; nn++) {
    popweight[nn] = pow(10.0, ages[nn]-8.0) *
      exp( -NU*(pow(10.0, 0.2) - pow(10.0, ages[nn]-10.0)) );
    sum += popweight[nn];
  }
  for (int nn=0; nn<nages; nn++) popweight[nn] /= sum;

				// Call extinction routine
  const double onedeg = 180.0/M_PI;
  float ls, aj, ah, ak, lf, lb, lq, ebv, sig, emn, emx;
  lf = L*onedeg;
  lb = B*onedeg;
  lq = 1.0;
  getsfd(lf, lb, lq, ebv, sig, emn, emx, DataDir);

				// Set up line-of-sight cache
  cache[listCntr]->SetNumberComp(nages, NUM);
  cache[listCntr]->R = vector<real>(NUM);
  cache[listCntr]->z = vector<real>(NUM);
  cache[listCntr]->fac = vector<real>(NUM);

				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    s = intgr->knot(n) * RMAX;
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;
    fac = s*s*intgr->weight(n) * RMAX;

    for (int nn=0; nn<nages; nn++)
      cache[listCntr]->work[nn][n-1] = vector<real>(nbins, 0.0);
    cache[listCntr]->R[n-1] = R;
    cache[listCntr]->z[n-1] = z;
    cache[listCntr]->fac[n-1] = fac;

    ls = s * 1.0e3;
    getext(ls, lf, lb, ebv, aj, ah, ak);
    ext[0] = aj;
    ext[1] = ak;

    dmod = 5.0*log(1.0e3*s)/Log10 - 5.0;
	
				// Bin visibility
    for (int j=0; j<nbins; j++) {

				// Make fluxes for each component
      for (int l=0; l<Nflux; l++) {
	if (type == binned) {
	  fluxmin[l] = lowb[l][j] - ext[l];
	  fluxmax[l] = highb[l][j] - ext[l];
	} else if (type == point) {
	  fluxmin[l] = ((PointDistribution*)sd)->Point()[j] - ext[l];
	}
      }
      
      for (int nn=0; nn<nages; nn++) {
	if (type == binned) 
	  val = pops->Bin(fluxmin, fluxmax, ages[nn], METAL, dmod);
	else if (type == point)
	  val = pops->Interpolate(fluxmin, ages[nn], METAL, dmod);

	cache[listCntr]->work[nn][n-1][j] = val * popweight[nn];
      }
    }

  }

}

void PopModelND::compute_bins()
{
  double sech, R, z, fac, dens;
				// Zero out theoretical bins
  cache[listCntr]->bins = vector<real>(nbins, 0.0);

  for (int n=1; n<=NUM; n++) {
				// Coordinates
    fac = cache[listCntr]->fac[n-1];
    R = cache[listCntr]->R[n-1];
    z = cache[listCntr]->z[n-1];
				// Loop over each components
    for (int k=0; k<Mcur; k++) {
				// Disk model
      sech = 2.0/(exp(z*0.5e3/pt[k][1]) + exp(-z*0.5e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	for (int nn=0; nn<nages; nn++)
	  cache[listCntr]->bins[j] += 
	    wt[k]*fac*dens * cache[listCntr]->work[nn][n-1][j];
    }
  }

}

double PopModelND::NormEval(double L, double B, SampleDistribution *sd)
{
  if (!good_bounds) return 1.0;

  if (listCntr>=(int)cache.size()) {

    cache.push_back(NDPopCPtr(new NDPopCachedModel(L, B)));
    generate(L, B, sd);

  } else {

    if (fabs(cache[listCntr]->L - L) > 1.0e-8 ||
	fabs(cache[listCntr]->B - B) > 1.0e-8 )
      {
	cache[listCntr] = NDPopCPtr(new NDPopCachedModel(L, B));
	generate(L, B, sd);
	missed++;
#ifdef DEBUG	
	cerr << "PopModelND: cache miss [" << missed << "]\n";
#endif
      }
  }

  compute_bins();

  double norm = 0.0;
  for (int j=0; j<nbins; j++)  norm += cache[listCntr]->bins[j];

  listCntr++;

  return norm;
}




vector<double> PopModelND::Evaluate(double L, double B, 
				    SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);

  if (listCntr>=(int)cache.size()) {
    cache.push_back(NDPopCPtr(new NDPopCachedModel(L, B)));
    generate(L, B, d);
    compute_bins();
  } else {
    if (fabs(cache[listCntr]->L - L) > 1.0e-8 ||
	fabs(cache[listCntr]->B - B) > 1.0e-8 )
      {
	cache[listCntr] = NDPopCPtr(new NDPopCachedModel(L, B));
	generate(L, B, d);
	compute_bins();
	missed++;
#ifdef DEBUG	
	cerr << "PopModelND: cache miss [" << missed << "]\n";
#endif
      }
  }
  
  int n = cache[listCntr]->bins.size();
  if ((int)work.size() != n) work = vector<double>(n);
  for (int i=0; i<n; i++) work[i] = cache[listCntr]->bins[i];
  listCntr++;
  return work;
}

