#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <climits>

#include <errno.h>

using namespace std;

#include <CMD.h>
BIE_CLASS_EXPORT_IMPLEMENT(BIE::CMD)

using namespace BIE;

bool CMD::verbose = false;
int CMD::seed = 11;

const int istep = 128;

size_t ivecHash(vector<int> P) 
{
  long int u = 0;
  vector<int>::iterator i = P.begin();
  for (; i!= P.end(); i++) u = istep*u + *i;
  
  return u * 0x9e3779b1u;
}


CMD::CMD(string file, double minval)
{
				// Read parameters out of index file

  datafile = file;		// Save file name

  ifstream init(datafile.c_str());
  if (!init) {
    throw FileOpenException(datafile, errno, __FILE__, __LINE__);
  }

				// Distance modulus
  init >> distmod;
				// Read in number of colors
  init >> ncolors;

  colormin.resize(ncolors);
  colormax.resize(ncolors);
  wcolor.resize(ncolors);
  nbins.resize(ncolors);

				// Read in colors
  ivector onecolor(3);
  for (int n=0; n<ncolors; n++) {
    init >> colormin[n];
    init >> colormax[n];
    init >> wcolor[n];
    for (int k=0; k<3; k++) init >> onecolor[k];
    colors.push_back(onecolor);
  }

  for (int i=0; i<ncolors; i++)
    ncol.push_back((int) ( (colormax[i] - colormin[i])/wcolor[i] ));

  colr = vector<double>(ncolors);
  ii = vector<int>(ncolors);
  icol = vector<int>(ncolors);
  icolmin = vector<int>(ncolors);
  icolmax = vector<int>(ncolors);
  icoloff = vector<int>(ncolors);
  colwght = vector<double>(ncolors);
  off = vector<double>(ncolors);
  f2 = vector<dvector>(ncolors);
  i2 = vector<ivector>(ncolors);
  for (int i=0; i<ncolors; i++) {
    f2[i] = vector<double>(2);
    i2[i] = vector<int>(2);
  }

  //  Hash.resize(100000);

  read_data(init, minval);

  realize_setup = false;

}

void CMD::read_data(istream &in, double minval)
{
  const int linesize = 1024;
  char line[linesize];
  double value;
				// Read data into cache
  
  while (in) {
    in.getline(line, linesize);
    if (strlen(line)==0) continue;
    if (index(line,'#')) continue;

    istringstream istr(line);

    for (int k=0; k<ncolors; k++) istr >> colr[k];
    istr >> value;

    if (istr && value>minval) {	// Only save non-zero values
      
      for (int k=0; k<ncolors; k++)
	icol[k] = 
	  (int)( (colr[k] - colormin[k])/wcolor[k] );
      
      Hash[icol] = value;
    }
  } // End While
  
}

double CMD::GetValue(vector<double>& Flux)
{
  for (int k=0; k<ncolors; k++)
    icol[k] = (int)( (Flux[k] - colormin[k])/wcolor[k] );
  
  HMap::iterator p = Hash.find(icol);
  if (p!=Hash.end()) return p->second;
  else return 0.0;
}


double CMD::Interpolate(vector<double>& Flux, double dm)
{
  double ofs = dm - distmod;

  for (int k=0; k<ncolors; k++) {

    if (!k) off[k] = ofs;
    else    off[k] = 0.0;

    icol[k] = (int)( (Flux[k]-off[k] - colormin[k])/wcolor[k] - 0.5);

    if (icol[k] <= ncol[k]-1) {
      i2[k][0] = icol[k];
      i2[k][1] = icol[k]+1;
    } else if (icol[k] <= 0) {
      i2[k][0] = 0;
      i2[k][1] = 1;
    } else {
      i2[k][0] = ncol[k]-2;
      i2[k][1] = ncol[k]-1;
    }

				// Don't allow extrapolation off of 
				// the grid on the LOW side
    if (Flux[k]-off[k] < colormin[k] + 0.5*wcolor[k]) {
      f2[k][0] = 1.0;
      f2[k][1] = 0.0;
				// Don't allow extrapolation off of 
				// the grid on the HIGH side
    } else if (Flux[k]-off[k] > colormax[k] - 0.5*wcolor[k]) {
      f2[k][0] = 0.0;
      f2[k][1] = 1.0;
    } else {			// Interpolate!
      f2[k][0] = (colormin[k] + wcolor[k]*(0.5+i2[k][1]) - (Flux[k]- off[k]))/wcolor[k];
      f2[k][1] = (Flux[k]-off[k] - colormin[k] - wcolor[k]*(0.5+i2[k][0]))/wcolor[k];
    }
  }

  HMap::iterator p;
  double value, ans = 0.0;

  int ndim = 1;
  for (int k=0; k<ncolors; k++) ndim *= 2;

  unsigned mask=1;
  for (int i=0; i<ndim; i++) {
    
    // Magic here to use each bit to represent a dimension
    for (int k=0; k<ncolors; k++) icol[k] = i2[k][(ii[k]=(i>>k) & mask)];
    p = Hash.find(icol);

    if (p != Hash.end()) {
      value = p->second;
      for (int k=0; k<ncolors; k++) value *= f2[k][ii[k]];
      ans += value;
    }
    
  }

  // debug
  if (ans < 0.0) {
    cout << "zero=" << ans << "  flux=";
    for (int k=0; k<ncolors; k++) cout << setw(18) << Flux[k];
    cout << endl;
    ans = 0.0;
  }
  //

  return ans;
}


double CMD::Bin(vector<double>& fmin, vector<double>& fmax, double dm)
{
  double ofs = dm - distmod;

  int ndim, icnt;
  HMap::iterator p = Hash.find(icol);
  double value, ans = 0.0, fbmin, fbmax;

  ndim = 1;
  for (int k=0; k<ncolors; k++) {

    // Compute indices for bounds of data hypercube

    if (!k) off[k] = ofs;
    else    off[k] = 0.0;

    icolmin[k] = 
      (int)( (fmin[k]-off[k] - colormin[k])/wcolor[k] );
    icolmax[k] = 
      (int)( (fmax[k]-off[k] - colormin[k])/wcolor[k] );
    icoloff[k] = ndim;

    ndim *= icolmax[k] - icolmin[k] + 1;

  }

  for (int i=0; i<ndim; i++) {
    
    // Magic here to get offset for each dimension
    icnt = i;
    for (int k=ncolors-1; k>=0; k--) {
      icol[k] = icnt/icoloff[k];
      icnt -= icol[k]*icoloff[k];
    }
      
    // Restore index array
    for (int k=0; k<ncolors; k++) icol[k] += icolmin[k];

    // Look up value in the hash table
    p = Hash.find(icol);

				// Null value means zero so only need to
				// process on non null . . .
    if (p != Hash.end()) {
      value = p->second;
      for (int k=0; k<ncolors; k++) {
	  
				// Edges of tabulated isochrone bins
	fbmin = max<double>(colormin[k] + wcolor[k]*icol[k], colormin[k]);

	fbmax = min<double>(fbmin + wcolor[k], colormax[k]);
	
	colwght[k] = 
	  (min<double>(fmax[k]-off[k], fbmax) - 
	   max<double>(fmin[k]-off[k], fbmin)) / wcolor[k];
      }
      for (int k=0; k<ncolors; k++) value *= colwght[k];
      
      ans += value;
    }
	
  }
  
  return ans;
}


double CMD::GetValue(vector<int>& Iflx)
{
  for (int k=0; k<ncolors; k++) {
    if (Iflx[k]<0 || Iflx[k]>=ncol[k]) return 0.0;
  }

  HMap::iterator p = Hash.find(Iflx);
  if (p != Hash.end()) return p->second;
  else return 0.0;
}

void CMD::VerifyDouble(double distmod)
{
  const int linesize = 1024;
  char line[linesize];
  double value, tvalue;
  int wrong = 0;
  bool ok = true;

  ifstream infile(datafile.c_str());
  if (!infile) {
    cerr << "CMD::VerifyDouble: can not open input file <" << datafile << ">\n";
    return;
  }

  infile.getline(line, linesize); // distmod
  infile.getline(line, linesize); // ncolors
  for (int n=0; n<ncolors; n++) infile.getline(line, linesize); // color bins

				// Read CMD data
  while (infile) {
    infile.getline(line, linesize);
    if (strlen(line)==0) continue;
    if (index(line,'#')) continue;

    istringstream istr(line);

    for (int k=0; k<ncolors; k++) istr >> colr[k];
    istr >> value;
	
    tvalue = Interpolate(colr, distmod);
    
    if (fabs(tvalue - value) > 1.0e-6) {
      ok = false;
      wrong++;
    }
  }

  if (ok)
    cout << "Cached data agrees with original tables, good!\n";
  else
    cout << "Ooops, " << wrong << " values are incorrect\n";
}


void CMD::VerifyInt(void)
{
  const int linesize = 1024;
  char line[linesize];
  double value, tvalue;
  int wrong = 0;
  bool ok = true;

  ifstream infile(datafile.c_str());
  if (!infile) {
    cerr << "CMD::VerifyInt: can not open input file <" << datafile << ">\n";
    return;
  }

  infile.getline(line, linesize); // distmod
  infile.getline(line, linesize); // ncolors
  for (int n=0; n<ncolors; n++) infile.getline(line, linesize); // color bins

				// Read CMD data
  while (infile) {
    infile.getline(line, linesize);
    if (strlen(line)==0) continue;
    if (index(line,'#')) continue;

    istringstream istr(line);

    for (int k=0; k<ncolors; k++) istr >> colr[k];
    istr >> value;
	
    for (int k=0; k<ncolors;k++)
      icol[k] = (int)( (colr[k] - colormin[k])/wcolor[k] );
    
    tvalue = GetValue(icol);

    if (fabs(tvalue - value) > 1.0e-6) {
      ok = false;
      wrong++;
    }
  }
  
  if (ok)
    cout << "Cached data agrees with original tables, good!\n";
  else
    cout << "Ooops, " << wrong << " values are incorrect\n";
}


void CMD::initialize_realization()
{
  gen = ACGPtr(new ACG(seed, 22));
  uniform = UniformPtr(new Uniform(0.0, 1.0, gen.get()));

  
  int ndim = 1;
  for (int k=0; k<ncolors; k++) {
    icoloff[k] = ndim;
    ndim *= ncol[k];
  }

  double current = 0.0;
  int icnt;

  for (int i=0; i<ndim; i++) {
    
    // Magic here to get offset for each dimension
    icnt = i;
    for (int k=ncolors-1; k>=0; k--) {
      icol[k] = icnt/icoloff[k];
      icnt -= icol[k]*icoloff[k];
    }
      
    // Look up value in the hash table
    HMap::iterator p = Hash.find(icol);

    if (p!=Hash.end()) {
      current += p->second;
      realize_data[current] = i;
    }

  }

				// Maximum value in map
  realize_norm = current;
  realize_setup = true;
}


vector<double> CMD::genPoint()
{
  if (!realize_setup) initialize_realization();
  
				// Locate position in map
  double value = (*uniform)() * realize_norm;
  map<double, int, less<double> >::iterator 
    it = realize_data.lower_bound(value);
  
				// Determine multi-d index
  int icnt = it->second;
  for (int k=ncolors-1; k>=0; k--) {
    icol[k] = icnt/icoloff[k];
    icnt -= icol[k]*icoloff[k];
  }
      
				// Realize point in CMD
  vector<double> ret(ncolors);
  for (int k=0; k<ncolors; k++) 
    ret[k] = colormin[k] + wcolor[k]*(icol[k] + (*uniform)());
    
  return ret;
}

void CMD::HashStat()
{
				// Scroll through the cache and get stats
  int imin = INT_MAX;
  int imax = 0;
  double avg = 0.0;
  double std = 0.0;
  int icnt=0;
  int val;

  HMap::iterator j;
  for (j=Hash.begin(); j!=Hash.end(); j++) {
    val = Hash.count(j->first);
    imin = min<int>(imin, val);
    imax = max<int>(imax, val);
    avg += val;
    std += val*val;
    icnt++;
  }

  cout << "Hash table statistics\n"
       << "---------------------\n"
       << "Load factor: " << (double)Hash.size()/Hash.bucket_count() << "\n"
       << "Min cnt: " << imin << "\n"
       << "Max cnt: " << imax << "\n"
       << "Avg cnt: " << avg/icnt << "\n"
       << "Std cnt: " << sqrt(std/icnt - avg*avg/icnt/icnt) << "\n"
       << "Number: " << icnt << "\n"
       << "---------------------\n";
}
