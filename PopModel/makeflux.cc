// -*- C++ -*-

/* 
   Make a color-magnitude diagram
*/

#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <strstream.h>
#include <math.h>

#include <stl.h>
#include <string>

#include <PopulationsApp.h>

using namespace BIE;

int
main(int argc, char** argv)
{
  PopulationsApp::numBuckets = 10000;

  string dir("/data/weinberg/3Mtab");

  clivectori marge(3);
  marge()[0] = 0;		// Marginalize J-band (yes=1, no=0)
  marge()[1] = 1;		// Marginalize H-band
  marge()[2] = 0;		// Marginalize K-band
  PopulationsApp pops(dir, &marge);

  /*
    contrh style output
  */

  int nx=40;
  int ny=40;

  float Kmin = 18.5;
  float Kmax = 0.0;
  float dK = (Kmax - Kmin)/(ny-1);

  float Jmin = 18.5;
  float Jmax =  0.0;
  float dJ = (Jmax - Jmin)/(nx-1);
  float z;

  ofstream out("test.dat");

  out.write(&nx, sizeof(int));
  out.write(&ny, sizeof(int));
  out.write(&Jmin, sizeof(float));
  out.write(&Jmax, sizeof(float));
  out.write(&Kmin, sizeof(float));
  out.write(&Kmax, sizeof(float));

  vector<double> fmin(2);
  vector<double> fmax(2);
  vector<double> ages = pops.Ages();

  for (int j=0; j<ny; j++) {

    fmax[1] = Kmin + dK*((double)j - 0.5);
    fmin[1] = Kmin + dK*((double)j + 0.5);

    for (int i=0; i<nx; i++) {
      
      fmin[0] = Jmin + dJ*((double)i - 0.5);
      fmax[0] = Jmin + dJ*((double)i + 0.5);

      z = 0.0;
      for (int n=0; n<(int)ages.size(); n++) {
	z += pops.Bin(fmin, fmax, ages[n], 0.03, 11.11);
      }

      out.write(&z, sizeof(float));
    }
  }
      
  return 0;
}

