
#include <HistogramNDCacheColor.h>
#include <BasicType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::HistogramNDCacheColor)

using namespace BIE;


HistogramNDCacheColor::HistogramNDCacheColor
(clivectord *lo_in, 
 clivectord *hi_in, 
 clivectord *w_in,
 clivectord *cmin_in,
 clivectord *cmax_in,
 RecordType *type) 
  : cmin((*cmin_in)()), cmax((*cmax_in)())
{
  low   = (*lo_in)();
  high  = (*hi_in)();
  width = (*w_in)();
  
  // Make our own copy of the record type.
  recordtype = new RecordType(type);
  
  // Common initialization
  initialize();
}


HistogramNDCacheColor::HistogramNDCacheColor
(clivectord *lo_in, 
 clivectord *hi_in, 
 clivectord *w_in,
 clivectord *cmin_in,
 clivectord *cmax_in,
 clivectors *names) 
  : cmin((*cmin_in)()), cmax((*cmax_in)())
{
  low   = (*lo_in)();
  high  = (*hi_in)();
  width = (*w_in)();

  // Construct the record type.
  recordtype = new RecordType();
  
  for (int fieldindex = 0; fieldindex < (int)(*names)().size(); fieldindex++)
  {
    RecordType * rt = recordtype->insertField(fieldindex+1, 
					      (*names)()[fieldindex],
                                              BasicType::Real);
    
    delete recordtype;
    recordtype = rt;
  }

  // Common initialization
  initialize();
}


// Factory
HistogramNDCacheColor* HistogramNDCacheColor::New()
{
  HistogramNDCacheColor *p = new HistogramNDCacheColor();

  p->dim = dim;
  p->mass = mass;

  p->low = low;
  p->high = high;
  p->width = width;

  p->cmin = cmin;
  p->cmax = cmax;

  p->irank = irank;

  p->mean = mean;
  p->square = square;
  p->stdev = stdev;

  p->histo = 
    boost::shared_ptr<HistogramCache>(new HistogramCache(low, high, width));
  p->populate_cache();

  // Make our own copy of the record type.
  p->recordtype = new RecordType(recordtype);

  return p;
}


// Null constructor
HistogramNDCacheColor::HistogramNDCacheColor()
{
  dim = 0;
  recordtype = 0;
}


bool HistogramNDCacheColor::OK(vector<double>& x)
{
				// Flux check
  for (int i=0; i<dim; i++) {
    if (x[i]<=low[i] || x[i] >= high[i]) return false;
  }
				// Color check
  for (int i=0; i<dim-1; i++) {
    if (x[i]-x[dim-1] <= cmin[i] || x[i]-x[dim-1] >= cmax[i]) return false;
  }
  
  return true;
}
