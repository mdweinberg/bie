#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>

using namespace std;


#include <errno.h>
#include <BIEException.h>
#include <PopulationsApp.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PopulationsApp)

// Defining this allows extrapolation on the distance modulus grid
// Undefining pegs the distance modulus to the extrema of the grid
// (It's always safer to not extrapolate . . . )
#undef EXTRAPOLATE

// Define this to print values of vectors of bools; you will not 
// need to use this.
#undef PRINT_BOOL

using namespace BIE;

bool PopulationsApp::verbose = false;
bool PopulationsApp::use_array = true; // Use hash map if false . . .

// For debugging
#ifdef PRINT_BOOL
static void printbool (vector<bool> &v) {
  for (unsigned int i=0; i<v.size(); ++i)
    cout << v[i];
  cout << endl;
};
#endif


// Only used for debugging
static double mmax = 1.0e-30;
static double mmin = 1.0e+30;
static double amax = 1.0e-30;
static double amin = 1.0e+30;

void PopulationsApp::init(string dir, vector<int, allocator<int> >& marge, bool CMD)
{
  double value;
  string word;

  datadir = dir;
  cmd = CMD;
				// Read parameters out of index file

  string contents = datadir + "/.contentsApp";
  ifstream init(contents.c_str());
  if (!init) {
    throw FileOpenException(contents, errno, __FILE__, __LINE__);
  }
				// Read in number of metallicties and 
				// string suffixes
  init >> nmetal;
  for (size_t i=0; i<nmetal; i++) {
    init >> value;
    metal.push_back(value);
    init >> word;
    smetal.push_back(word);
  }

				// Read in number of distance moduli
				// string suffixes
  init >> ndistm;
  for (size_t i=0; i<ndistm; i++) {
    init >> value;
    distm.push_back(value);
    init >> word;
    sdistm.push_back(word);
  }

				// Read in number of ages
  init >> nages;		// and age grid data
  for (size_t i=0; i<nages; i++) {
    init >> value;
    ages.push_back(value);
    age_boundary.push_back(value-0.15);
    init >> word;
    sages.push_back(word);
  }
  age_boundary.push_back(value+0.15);

				// Read in number of fluxes
  init >> nflux;
				// Marginalize input stream?
  nMarge=0;
  use_flux = vector<bool>(nflux, true);
#ifdef PRINT_BOOL
  printbool(use_flux);
#endif

  if (marge.size() > 0) {

    if (marge.size() != nflux) {
      string msg("Marginalization vector must match flux dimension");
      throw DimNotMatchException(msg, __FILE__, __LINE__);
    }

    for (size_t i=0; i<nflux; i++) {
      if (marge[i]!=0) {
	use_flux[i] = false;
	nMarge++;
      }
    }
    
  }

#ifdef PRINT_BOOL
  printbool(use_flux);
#endif

  nflux0 = nflux;
  nflux -= nMarge;

				// Read in binning (bin centers)
  for (size_t i=0; i<nflux0; i++) {
    init >> value;
    if (use_flux[i]) minmag.push_back(value);
    init >> value;
    if (use_flux[i]) maxmag.push_back(value);
    init >> value;
    if (use_flux[i]) wmag.push_back(value);
  }

  nSize = 1;
  nSize2 = 1;
  for (size_t i=0; i<nflux; i++) {
    nmag.push_back( (int) ( (maxmag[i] - minmag[i] + 1.001*wmag[i])/wmag[i] ) );
    nSize *= nmag[i];
    nSize2 *= nmag[i]+1;
  }
  
  flux = vector<double>(nflux);
  ii = vector<int>(nflux);
  iflx = vector<int>(nflux);
  iflx1 = vector<int>(nflux);
  jflx = vector<int>(nflux);
  jflx1 = vector<int>(nflux);
  iflxmin = vector<int>(nflux);
  iflxmax = vector<int>(nflux);
  iflxoff = vector<int>(nflux);
  flxwght = vector<double>(nflux);
  wght = vector<double>(nflux);
  f2 = vector<dvector>(nflux);
  i2 = vector<ivector>(nflux);
  for (size_t i=0; i<nflux; i++) {
    f2[i] = vector<double>(2);
    i2[i] = vector<int>(2);
  }

  ff = vector<double>(2);

  ncorners = 2 << (nflux-1);
  vvector = vector<double>(ncorners);
  fflux = vector<double>(nflux);

  read_data();

}

//
// Get index on bin centers for prob density
//
int PopulationsApp::get_index(vector<int>& indx)
{
  int ret = min<int>(indx[0], nmag[0]-1);
  for (size_t k=1; k<nflux; k++)
    ret = nmag[k]*ret + min<int>(indx[k], nmag[k]-1);

  if (ret >= (int)nSize) {
    cerr << "Oops!\n";
  }

  return ret;
}

//
// Get index on bin edges for cumulative prob
//

int PopulationsApp::get_index2(vector<int>& indx)
{
  int ret = min<int>(indx[0], nmag[0]);
  for (size_t k=1; k<nflux; k++)
    ret = (nmag[k]+1)*ret + min<int>(indx[k], nmag[k]);

  if (ret >= (int)nSize2) {
    cerr << "Oops!\n";
  }

  return ret;
}

//
// Interpolates relative to bin centers
//
double PopulationsApp::array_interp(double* array, double* flux)
{
				// Get the lower corner in the grid
				// NB: minmag and maxmax are flux centers!!
  for (size_t k=0; k<nflux; k++) {
    jflx[k] = (int)( (flux[k] - minmag[k])/wmag[k] );
    jflx[k] = min<int>(jflx[k], nmag[k]-2);
    jflx[k] = max<int>(jflx[k], 0);
    wght[k] = (flux[k] - minmag[k] - wmag[k]*jflx[k])/wmag[k];
  }

				// Linear interpolation in nflux dimensions
  int bin, ncorners = 2 << (nflux-1);
  double factor, answer = 0.0;

  for (int n=0; n<ncorners; n++) {
    jflx1 = jflx;
    bin = n;
    factor = 1;
    for (size_t k=0; k<nflux; k++) {
      if (bin & 0x1) {
	jflx1[k]++;
	factor *= wght[k];
      } else {
	factor *= 1.0 - wght[k];
      }
      bin = bin >> 1;
    }
    answer += factor * array[get_index(jflx1)];
  }
  
  return answer;
}

//
// Interpolates relative to bin edges
//
double PopulationsApp::cumul_interp(double* array, double* flux)
{
				// Get the lower corner in the grid
  for (size_t k=0; k<nflux; k++) {
    jflx[k] = (int)( (flux[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );
    if (jflx[k]<0) {
      jflx[k] = 0;
      wght[k] = 0.0;
    } else if (jflx[k]>=nmag[k]) {
      jflx[k] = nmag[k]-1;
      wght[k] = 1.0;
    } else
      wght[k] = (flux[k] - minmag[k] - wmag[k]*(-0.5+jflx[k]))/wmag[k];
  }

				// Linear interpolation in nflux dimensions
  int bin, ncorners = 2 << (nflux-1);
  double factor, answer = 0.0;

  for (int n=0; n<ncorners; n++) {
    jflx1 = jflx;
    bin = n;
    factor = 1;
    for (size_t k=0; k<nflux; k++) {
      if (bin & 0x1) {
	jflx1[k]++;
	factor *= wght[k];
      } else {
	factor *= 1.0 - wght[k];
      }
      bin = bin >> 1;
    }
    answer += factor * array[get_index2(jflx1)];
  }
  
  return answer;
}

void PopulationsApp::Cumulate(int iage, int idistm, int imetal)
{
  int idx1, idx2, idx3;

  double* array = &Array[iage][idistm][imetal][0];
  double* cumul = &Cumul[iage][idistm][imetal][0];

				// --------------------------------------------
  if (nflux == 1) {		// One dimension
				// --------------------------------------------

    for (int i=1; i<=nmag[0]; i++)
      cumul[i] = cumul[i-1] + array[i-1];


				// --------------------------------------------
  } else if (nflux == 2) {	// Two dimensions
				// --------------------------------------------


				// Temporary storage
    vector<double> work(nSize2, 0.0);

				// Do rows

    for (iflx[1]=1; iflx[1]<=nmag[1]; iflx[1]++) {
      
      for (iflx[0]=1; iflx[0]<=nmag[0]; iflx[0]++) {

	iflx1 = iflx;
	iflx1[0]--;

	idx1 = get_index2(iflx);
	idx2 = get_index2(iflx1);

	iflx1 = iflx;
	iflx1[0]--;
	iflx1[1]--;

	idx3 = get_index (iflx1);
				// Add previous row and current cell
	work[idx1] = work[idx2] + array[idx3];
      }
    }
				// Do columns

    for (iflx[1]=1; iflx[1]<=nmag[1]; iflx[1]++) {

      for (iflx[0]=0; iflx[0]<=nmag[0]; iflx[0]++) {

	iflx1 = iflx;
	iflx1[1]--;

	idx1 = get_index2(iflx);
	idx2 = get_index2(iflx1);

				// Add current column to previous 
				// accumulated result
	cumul[idx1] = cumul[idx2] + work[idx1];
      }
    }


				// --------------------------------------------
  } else if (nflux == 3) {	// Three dimensions
				// --------------------------------------------


				// Temporary storage
    vector<double> work3(nSize2, 0.0);

				// Do each plane

    for (iflx[2]=0; iflx[2]<=nmag[2]; iflx[2]++) {

      vector<double> work2((nmag[0]+1)*(nmag[1]+1), 0.0);

				// Do rows

      for (iflx[1]=1; iflx[1]<=nmag[1]; iflx[1]++) {

	for (iflx[0]=1; iflx[0]<nmag[0]; iflx[0]++) {

	  idx1 = (nmag[1]+1)*iflx[0] + iflx[1];
	  idx2 = nmag[1]*(iflx[0]-1) + iflx[1]-1;

				//  Add previous row and current cell
	  work2[idx1] += work2[(nmag[1]+1)*(iflx[0]-1)+iflx[1]] + array[idx2];
	  
	}
      }
				// Do columns

      for (iflx[1]=1; iflx[1]<=nmag[1]; iflx[1]++) {

	for (iflx[0]=0; iflx[0]<=nmag[0]; iflx[0]++) {

	  iflx1 = iflx;
	  iflx1[1]--;

	  idx1 = get_index2(iflx);
	  idx2 = get_index2(iflx1);
	  idx3 = (nmag[1]+1)*iflx[0]+iflx[1];
	
	  work3[idx1] = work3[idx2] + work2[idx3];
	}
      }
      
    }
				// Sum up planes

    for (iflx[2]=1; iflx[2]<=nmag[2]; iflx[2]++) {

      for (iflx[1]=0; iflx[1]<=nmag[1]; iflx[1]++) {

	for (iflx[0]=0; iflx[0]<=nmag[0]; iflx[0]++) {

	  iflx1 = iflx;
	  iflx1[2]--;

	  idx1 = get_index2(iflx);
	  idx2 = get_index2(iflx1);

	  cumul[idx1] = cumul[idx2] + work3[idx1];
	}
      }
    }

  } else {
    cerr << "Oops! Can't do this dimensionality\n";
    exit(-1);
  }
}


void PopulationsApp::debug_accumulation(int iage, int idistm, int imetal)
{
  static double tol = 1.0e-6;
  static double rtol1 = 1.0e-6;
  static double rtol2 = 1.0e-2;
  static int curcnt=0;
  ostringstream sout;
  sout << "debug_acc." << curcnt++;
  ofstream out(sout.str().c_str());  //ridiculous!
  if (!out) {
    cerr << "Couldn't open: <" << sout.str() << ">\n";
    return;
  }

  out << "# Age=" << ages[iage]
      << "   DistMod=" << distm[idistm]
      << "   Metal=" << metal[imetal]
      << "   Max dim=(" << nmag[0] << ", " << nmag[1] << ")"
      << "   Rtol(1)=" << rtol1
      << "   Rtol(2)=" << rtol2
      << endl;

  double minv = 1.0e+30;
  double maxv = 1.0e-30;
  double mina = 1.0e+30;
  double maxa = 1.0e-30;
  double mini = 1.0e+30;
  double maxi = 1.0e-30;
  double minf = 1.0e+30;
  double maxf = 1.0e-30;

  // Find the first non-zero element

  double valu, rval, rval1;
  vector<int> iflx1(nflux), iflx2(nflux), worst(nflux, 0);
  int idx1, idx2, idx3;
  int under1=0, under2=0, over=0, aunder=0, aover=0;

  for (iflx1[1]=0; iflx1[1]<nmag[1]; iflx1[1]++) {

    for (iflx1[0]=0; iflx1[0]<nmag[0]; iflx1[0]++) {

      iflx2 = iflx1;
      iflx2[0]++;
      iflx2[1]++;

      idx1 = get_index2(iflx2);
      idx2 = get_index (iflx1);
      valu = Cumul[iage][idistm][imetal][idx1];
      rval = Array[iage][idistm][imetal][idx2];

      flux[0] = minmag[0] + wmag[0]*iflx1[0];
      flux[1] = minmag[1] + wmag[1]*iflx1[1];
      rval1 = array_interp(&Array[iage][idistm][imetal][0], &flux[0]);
      minf = min<double>(minf, fabs(rval-rval1));
      maxf = max<double>(maxf, fabs(rval-rval1));

      flux[0] = minmag[0] - 0.5*wmag[0] + wmag[0]*iflx2[0];
      flux[1] = minmag[1] - 0.5*wmag[1] + wmag[1]*iflx2[1];
      rval1 = cumul_interp(&Cumul[iage][idistm][imetal][0], &flux[0]);
      mini = min<double>(mini, fabs(valu-rval1));
      maxi = max<double>(maxi, fabs(valu-rval1));

      valu -= rval;

      iflx2[0]--;
      idx3 = get_index2(iflx2);
      valu -= Cumul[iage][idistm][imetal][idx3];

      iflx2[1]--;
      idx3 = get_index2(iflx2);
      valu += Cumul[iage][idistm][imetal][idx3];

      iflx2[0]++;
      idx3 = get_index2(iflx2);
      valu -= Cumul[iage][idistm][imetal][idx3];

      if (rval>0.0) rval = valu/rval;

      if      (rval<rtol1) under1++;
      else if (rval<rtol2) under2++;
      else                 over++;

      if   (valu<tol) aunder++;
      else            aover++;

      // if (fabs(rval) > maxa) worst = iflx1;
      if (fabs(valu) > maxv) worst = iflx1;

      minv = min<double>(minv, fabs(valu));
      maxv = max<double>(maxv, fabs(valu));

      mina = min<double>(mina, fabs(rval));
      maxa = max<double>(maxa, fabs(rval));
      
    }
  }

  char prev = out.fill('-');

  out << setw(18) << "Min|" << setw(18) << "Max|"
      << setw(18) << "Rel min|"  << setw(18) << "Rel max|"
      << setw(18) << "IntrpV min|"  << setw(18) << "IntrpV max|"
      << setw(18) << "IntrpC min|"  << setw(18) << "IntrpC max|"
      << setw(10) << "Under1|" 
      << setw(10) << "Under2|" 
      << setw(10) << "Over|"
      << setw(10) << "Under(a)|" 
      << setw(10) << "Over(a)|"
      << setw(10) << "Dim 1|" << setw(10) << "Dim 2|"
      << endl;

  out.fill(prev);

  out << setw(18) << minv << setw(18) << maxv 
      << setw(18) << mina << setw(18) << maxa 
      << setw(18) << minf << setw(18) << maxf
      << setw(18) << mini << setw(18) << maxi
      << setw(10) << under1 << setw(10) << under2 << setw(10) << over
      << setw(10) << aunder << setw(10) << aover
      << setw(10) << worst[0] << setw(10) << worst[1]
      << endl;

  mmin = min<double>(mmin, minv);
  mmax = max<double>(mmax, maxv);

  amin = min<double>(amin, mina);
  amax = max<double>(amax, maxa);
}

void PopulationsApp::init(string dir, string name, vector<int, allocator<int> >& marge)
{
  datadir = dir;
  isoname = name;
				// Read parameters out of index file

  string cdffile = datadir + "/" + isoname;


  float value;    
  string word;
				// Status indicator returned by NetCDF calls
  int ncstatus = 0;
				// NetCDF IDs
  int in_ncid;
  int metaldim, moddim, timedim, fluxdim[3], indexdim, datadim;
  int vmetal, vdmod, vtime, vflux[3], begid, endid, indexid, valuid;
  int minmagid, maxmagid, wmagid, maxlenid, maxlen;

  // Open the CDF file

  ncstatus = nc_open(cdffile.c_str(), NC_NOWRITE, &in_ncid);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
				// Get the dimensions

  const char *metal_name = "metallicity";
  ncstatus = nc_inq_dimid (in_ncid, metal_name, &metaldim);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  const char *dmod_name = "distance_modulus";
  ncstatus = nc_inq_dimid (in_ncid, dmod_name, &moddim);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  const char *time_name = "log_time";
  ncstatus = nc_inq_dimid (in_ncid, time_name, &timedim);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  const char *flux1_name = "flux1";
  ncstatus = nc_inq_dimid (in_ncid, flux1_name, &fluxdim[0]);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  const char *flux2_name = "flux2";
  ncstatus = nc_inq_dimid (in_ncid, flux2_name, &fluxdim[1]);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  const char *flux3_name = "flux3";
  ncstatus = nc_inq_dimid (in_ncid, flux3_name, &fluxdim[2]);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  const char *indx_name = "index";
  ncstatus = nc_inq_dimid (in_ncid, indx_name, &indexdim);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  const char *unl_name = "unlimited";
  ncstatus = nc_inq_dimid (in_ncid, unl_name, &datadim);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }


  // Get the coordinate variable IDs

  ncstatus = nc_inq_varid (in_ncid, metal_name, &vmetal);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }

  ncstatus = nc_inq_varid (in_ncid, dmod_name,&vdmod);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }

  ncstatus = nc_inq_varid (in_ncid, time_name, &vtime);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }


  ncstatus = nc_inq_varid (in_ncid, flux1_name, &vflux[0]);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }


  ncstatus = nc_inq_varid (in_ncid, flux2_name, &vflux[1]);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }


  ncstatus = nc_inq_varid (in_ncid, flux3_name, &vflux[2]);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }


  // Flux array bounds


  ncstatus = nc_inq_varid (in_ncid, "MinMag", &minmagid);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }

  ncstatus = nc_inq_varid (in_ncid, "MaxMag", &maxmagid);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }

  ncstatus = nc_inq_varid (in_ncid, "MagWidth", &wmagid);
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }


  // Define flux variables

  ncstatus = nc_inq_varid (in_ncid, "begin_offset", &begid);

  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }
  
  ncstatus = nc_inq_varid (in_ncid, "end_offset", &endid);

  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }
  
  ncstatus = nc_inq_varid (in_ncid, "flux_index", &indexid);
  
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }
  
  ncstatus = nc_inq_varid (in_ncid, "Value", &valuid);
  
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }
  

  ncstatus = nc_inq_varid (in_ncid, "max_length", &maxlenid);
  
  if (ncstatus != NC_NOERR)
    { 
      throw NetCDFException(ncstatus, __FILE__, __LINE__); 
    }
  

  // Define the dimensions

  ncstatus = nc_inq_dimlen(in_ncid, indexdim, &nflux);
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  ncstatus = nc_inq_dimlen(in_ncid, metaldim, &nmetal);
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  if (verbose) cout << "Metal values:" << endl;
  for (size_t i=0; i<(unsigned)nmetal; i++) {
    ncstatus = nc_get_var1_float(in_ncid, vmetal, &i, &value);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    if (verbose) cout << value << endl;
    metal.push_back(value);
  }
  if (verbose) cout << endl;
  
  ncstatus = nc_inq_dimlen(in_ncid, moddim, &ndistm);
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  if (verbose) cout << "Distance modulus values:" << endl;
  for (size_t i=0; i<(unsigned)ndistm; i++) {
    ncstatus = nc_get_var1_float(in_ncid, vdmod, &i, &value);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    if (verbose) cout << value << endl;
    distm.push_back(value);
  }
  if (verbose) cout << endl;
    

  ncstatus = nc_inq_dimlen(in_ncid, timedim, &nages);
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  if (verbose) cout << "Distance time values:" << endl;
  for (size_t i=0; i<(unsigned)nages; i++) {
    ncstatus = nc_get_var1_float(in_ncid, vtime, &i, &value);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    if (verbose) cout << value << endl;
    ages.push_back(value);
    age_boundary.push_back(value-0.15);
  }
  age_boundary.push_back(value+0.15);
  if (verbose) cout << endl;

  char *long_name = new char [128];
  ncstatus =  nc_get_att_text(in_ncid, NC_GLOBAL, "long_name", long_name);
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  if (!string("flux-color-color").compare(long_name)) cmd = true;
  delete [] long_name;

				// Marginalize input stream?
  nMarge=0;
  use_flux = vector<bool>(nflux, true);
#ifdef PRINT_BOOL
  printbool(use_flux);
#endif

  if (marge.size()) {

    if (marge.size() != nflux) {
      string msg("Marginalization vector must match flux dimension");
      throw DimNotMatchException(msg, __FILE__, __LINE__);
    }

    for (size_t i=0; i<nflux; i++) {
      if (marge[i]!=0) {
	use_flux[i] = false;
	nMarge++;
      }
    }
    
  }

#ifdef PRINT_BOOL
  printbool(use_flux);
#endif

  nflux0 = nflux;
  nflux -= nMarge;


  // Define work vectors

  flux = vector<double>(nflux);
  ii = vector<int>(nflux);
  iflx = vector<int>(nflux);
  iflx1 = vector<int>(nflux);
  jflx = vector<int>(nflux);
  jflx1 = vector<int>(nflux);
  iflxmin = vector<int>(nflux);
  iflxmax = vector<int>(nflux);
  iflxoff = vector<int>(nflux);
  flxwght = vector<double>(nflux);
  wght = vector<double>(nflux);
  f2 = vector<dvector>(nflux);
  i2 = vector<ivector>(nflux);
  for (size_t i=0; i<nflux; i++) {
    f2[i] = vector<double>(2);
    i2[i] = vector<int>(2);
  }

  ff = vector<double>(2);

  ncorners = 2 << (nflux-1);
  vvector = vector<double>(ncorners);
  fflux = vector<double>(nflux);

  // Get data value from CDF file

  for (size_t n=0; n<3; n++) {
    ncstatus = nc_get_var1_float(in_ncid, minmagid, &n, &value);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    if (use_flux[n]) minmag.push_back(value);
  }
    
  for (size_t n=0; n<3; n++) {
    ncstatus = nc_get_var1_float(in_ncid, maxmagid, &n, &value);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    if (use_flux[n]) maxmag.push_back(value);
  }
    
  for (size_t n=0; n<3; n++) {
    ncstatus = nc_get_var1_float(in_ncid, wmagid, &n, &value);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    if (use_flux[n]) wmag.push_back(value);
  }
    
  {
    size_t n=0;
    ncstatus = nc_get_var1_int(in_ncid, maxlenid, &n, &maxlen);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  }    

  if (verbose) {
    for (size_t n=0, nn=0; n<nflux0; n++) {
      cout << "Flux " << n+1 << ": " ;
      if (use_flux[n]) {
	cout << setw(12) << minmag[nn] 
	     << setw(12) << maxmag[nn]
	     << setw(12) << wmag[nn] ;
	nn++;
      }
      else
	cout << "marginalized";
      cout << endl;
    }
  }


				// Total in array
  nSize = 1;
  nSize2 = 1;
  for (size_t i=0; i<nflux; i++) {
    nmag.push_back( (int) ( (maxmag[i] - minmag[i] + 1.001*wmag[i])/wmag[i] ) );
    nSize *= nmag[i];
    nSize2 *= nmag[i]+1;
  }
  
  size_t idims[3], ipos[2], icnt[2];
  int ibeg, iend, zz;
  float *data = new float [maxlen];
  short *indx = new short [maxlen*3];

				// Need caches for each time and variance
  HMapKey::iterator p;

  if (use_array) {
    Array = vector< vector< vector< vector<double> > > >(nages);
    Cumul = vector< vector< vector< vector<double> > > >(nages);
  }
  else
    Hash = vector< vector<HMapKey> >(nages);

  for (size_t i=0; i<nages; i++) {

    if (use_array) {
      Array[i] = vector< vector< vector<double > > >(ndistm);
      Cumul[i] = vector< vector< vector<double > > >(ndistm);
    }
    else
      Hash[i] = vector<HMapKey>(ndistm);

    for (size_t l=0; l<ndistm; l++) {

      if (use_array) {
	Array[i][l] = vector< vector<double> > (nmetal);
	Cumul[i][l] = vector< vector<double> > (nmetal);
      }

      for (size_t j=0; j<nmetal; j++) {

	zz = MetalIndex(metal[j]);

	if (use_array) {
	  Array[i][l][zz] = vector<double>(nSize, 0.0);
	  Cumul[i][l][zz] = vector<double>(nSize2, 0.0);
	}

				// Beginning offset
	idims[0] = j;
	idims[1] = l;
	idims[2] = i;

	ncstatus = nc_get_var1_int(in_ncid, begid, idims, &ibeg);
	if (ncstatus != NC_NOERR)
	  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

	ncstatus = nc_get_var1_int(in_ncid, endid, idims, &iend);
	if (ncstatus != NC_NOERR)
	  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

	ipos[0] = ibeg;
	ipos[1] = 0;
	icnt[0] = iend-ibeg;
	icnt[1] = 3;

	if ((int)icnt[0]>maxlen) {
	  cerr << "Required memory [" << icnt[0] 
	       << " exceeds claimed value of " << maxlen << endl;
	  exit(-1);
	}

	ncstatus = nc_get_vara_short(in_ncid, indexid, ipos, icnt, indx);
	if (ncstatus != NC_NOERR)
	  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

	ncstatus = nc_get_vara_float(in_ncid, valuid, ipos, icnt, data);
	if (ncstatus != NC_NOERR)
	  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

	for (size_t n=0; n<icnt[0]; n++) {
	  
	  for (size_t k=0, ik=0; k<nflux0; k++)
	    if (use_flux[k]) iflx[ik++] = indx[n*3+k];
	  
	  if (use_array) {

	    Array[i][l][zz][get_index(iflx)] += data[n];

	  } else {

	    key.reset(zz, iflx);
				// Key exists?
	    p = Hash[i][l].find(key);
	    if (p != Hash[i][l].end())
				// Then: add to previous value
	      p->second += data[n];
	    else		// Otherwise: enter the new value
	      Hash[i][l][key] = data[n];
	  }
	  
	} // End Database loop


	if (use_array) {
	  Cumulate(i, l, zz);
#ifdef DEBUG	  
	  debug_accumulation(i, l, zz);
#endif
	}

      }	// End Metal loop

    } // End Dist Mod loop

  } // End Age loop
  

#ifdef DEBUG	  
  cout << "Minimum error=" << mmin << endl;
  cout << "Maximum error=" << mmax << endl;
  debug_norm();
#endif

  // Close the NetCDF file
  nc_close(in_ncid);

  if (verbose) cout << "CDF file <" << cdffile << "> closed " << endl;

  delete [] indx;
  delete [] data;

}



PopulationsApp::~PopulationsApp()
{
}

void PopulationsApp::read_data(void)
{
  string thead("t");
  string zhead("z");
  string mhead("m");
  const int linesize = 1024;
  char line[linesize];
  double z, m, value;
  int zz, indx;

				// Need caches for each time and variance
  HMapKey::iterator p;

  if (use_array) {
    Array = vector< vector< vector< vector<double> > > > (nages);
    Cumul = vector< vector< vector< vector<double> > > > (nages);
  }
  else
    Hash = vector< vector<HMapKey> > (nages);

  for (size_t i=0; i<nages; i++) {

    if (use_array) {
      Array[i] = vector< vector< vector<double> > > (ndistm);
      Cumul[i] = vector< vector< vector<double> > > (ndistm);
    }
    else
      Hash[i] = vector<HMapKey> (ndistm);

    for (size_t l=0; l<ndistm; l++) {
      
      if (use_array) {
	Array[i][l] = vector< vector<double> > (nmetal);
	Cumul[i][l] = vector< vector<double> > (nmetal);
      }

      for (size_t j=0; j<nmetal; j++) {

	zz = MetalIndex(z);

	if (use_array) {
	  Array[i][l][zz] = vector<double>(nSize, 0.0);
	  Cumul[i][l][zz] = vector<double>(nSize2, 0.0);
	}

	string file = datadir + "/" 
	  + thead + sages[i] 
	  + zhead + smetal[j]
	  + mhead + sdistm[l];

	ifstream infile(file.c_str());
	if (!infile) continue;
	
	while (infile) {
	  infile.getline(line, linesize);
	  if (strlen(line)==0) continue;
	  istringstream istr(line);
	  if (index(line,'#')) continue;
	  if (istr) {
	    istr >> z;
	    istr >> m;
	    for (size_t k=0, ik=0; k<nflux0; k++) {
	      istr >> value;
	      if (use_flux[k]) flux[ik++] = value;
	    }
	    istr >> value;
	  } 
	  if (istr && value!=0) {	// Only save non-zero values
	    
	    for (size_t k=0; k<nflux; k++)
	      iflx[k] = (int)( (flux[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );

	    if (use_array) {

	      indx = get_index(iflx);

	      Array[i][l][zz][indx] += value;

	    } else {

	      key.reset(zz, iflx);
				// Key exists?
	      p = Hash[i][l].find(key);
	      if (p != Hash[i][l].end())
				// Then: add to previous value
		p->second += value;
	      else		// Otherwise: enter the new value
		Hash[i][l][key] = value;

	    }
	      
	  }
	} // End While

	if (use_array) Cumulate(i, l, zz);

      }	// End Metal loop

    } // End Dist Mod loop

  } // End Age loop

}


int PopulationsApp::AgeIndex(double age)
{
  size_t indx;
  vector<double>::iterator pos;

  if (age < age_boundary[0]) indx = 0;
  else if (age >= age_boundary[nages]) indx = nages-1;
  else {
    pos = lower_bound(age_boundary.begin(), age_boundary.end(), age);
    indx = pos - age_boundary.begin() - 1;
  }

				// Debug
  if ( verbose ) {
    if ( fabs(ages[indx]-age)>1.0e-6 || indx>=nages ) {
      cout << "Could not find requested age: " << age << "\n";
    }
  }

  return indx;
}

int PopulationsApp::MetalIndex(double z)
{
  size_t indx;
  vector<double>::iterator pos;

  if (z <= metal[0]) indx = 0;
  else if (z >= metal[nmetal-1]+1.0e-8) indx = nmetal - 1;
  else {
    pos = lower_bound(metal.begin(), metal.end(), z+1.0e-8);
    indx = pos - metal.begin() - 1;
  }
				// Debug
  if ( verbose ) {
    if ( fabs(metal[indx]-z)>1.0e-6 || indx>=nmetal ) {
      cout << "Could not find requested metallicity: " << z << "\n";
    }
  }

  return indx;
}

int PopulationsApp::DistModIndex(double m)
{
  size_t indx;
  vector<double>::iterator pos;

  if (m <= distm[0]) indx = 0;
  else if (m >= distm[ndistm-1]+1.0e-8) indx = ndistm - 1;
  else {
    pos = lower_bound(distm.begin(), distm.end(), m+1.0e-8);
    indx = pos - distm.begin() - 1;
  }
				// Debug
  if ( verbose ) {
    if ( fabs(distm[indx]-m)>1.0e-6 || indx>=ndistm ) {
      cout << "Could not find requested distance modulus: " << m << endl
	   << "Best fit is: " << distm[indx]
	   << ", lower bound is " << distm[0] 
	   << " and upper bound is " << distm[ndistm-1] << endl;
    }
  }

  return indx;
}



double PopulationsApp::GetValue(vector<double>& Flux, 
				double age, double z, double m)
{
  int tt = AgeIndex(age);
  int zz = MetalIndex(z);
  int mm = DistModIndex(m);

  for (size_t k=0; k<nflux; k++)
    iflx[k] = (int)( (Flux[k] - minmag[k])/wmag[k] );
  
  if (use_array) 
    return Array[tt][mm][zz][get_index(iflx)];
  else {
    key.reset(zz, iflx);
    HMapKey::iterator p = Hash[tt][mm].find(key);
    if (p != Hash[tt][mm].end()) return p->second;
    else return 0.0;
  }
}


double PopulationsApp::Interpolate(vector<double>& Flux, 
				   double age, double z, double m)
{
  int tt = AgeIndex(age);
  int zz = MetalIndex(z);
  int mm = DistModIndex(m);

  double ans = 0.0;

  if (use_array)
    return array_interp(&Array[tt][mm][zz][0], &Flux[0]);

  else {

    for (size_t k=0; k<nflux; k++) {

      iflx[k] = (int)( (Flux[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );

      if (iflx[k] < nmag[k]-1) {
	i2[k][0] = iflx[k];
	i2[k][1] = iflx[k]+1;
      } else if (iflx[k]<0) {
	i2[k][0] = 0;
	i2[k][1] = 1;
      } else {
	i2[k][0] = nmag[k]-2;
	i2[k][1] = nmag[k]-1;
      }
      
      f2[k][0] = (minmag[k] + wmag[k]*i2[k][1] - Flux[k])/wmag[k];
      f2[k][1] = (Flux[k] - minmag[k] - wmag[k]*i2[k][0])/wmag[k];

    }

    HMapKey::iterator p;
    double value, ans = 0.0;

    int ndim = 1;
    for (size_t k=0; k<nflux; k++) ndim *= 2;

    unsigned mask=1;
    for (int i=0; i<ndim; i++) {
    
      // Magic here to use each bit to represent a dimension
      for (size_t k=0; k<nflux; k++) iflx[k] = i2[k][(ii[k]=(i>>k) & mask)];
      key.reset(zz, iflx);
      p = Hash[tt][mm].find(key);
      
      if (p != Hash[tt][mm].end()) {
	value = p->second;
	for (size_t k=0; k<nflux; k++) value *= f2[k][ii[k]];
	ans += value;
      }
    
    }

  }

  return ans;
}


double PopulationsApp::Bin(vector<double>& fmin, vector<double>& fmax,
			   double age, double z, double m)
{
  int tt = AgeIndex(age);
  int zz = MetalIndex(z);
  int mm;

#ifdef EXTRAPOLATE
				// Extrapolate off distance modulus grid
  if (m>=distm[ndistm-1])
    mm = ndistm-2;
  else if (m<distm[0])
    mm = 0;
  else
    mm = DistModIndex(m);
#else
				// Don't extrapolate off distance modulus grid
  if (m>=distm[ndistm-1])
    return 0.0;
  else if (m<distm[0])
    return 0.0;
  else
    mm = DistModIndex(m);
#endif

  double dwid = distm[mm+1] - distm[mm];

  ff[0] = m - distm[mm];
  ff[1] = m - distm[mm+1];

  double value, ans = 0.0;
  double *pflux = &fflux[0];

  if (use_array) {
      
    int bin;
    double* cumul;

    for (int im=0; im<2; im++) {

      cumul = &Cumul[tt][mm+im][zz][0];

      for (int n=0; n<ncorners; n++) {
	fflux = fmin;
	bin = n;
	for (size_t k=0; k<nflux; k++) {
	  if (bin & 0x1) fflux[k] = fmax[k];
	  bin = bin >> 1;
	  if (!cmd || k==0) fflux[k] -= ff[im];
	};
	
	vvector[n] = cumul_interp(cumul, pflux);
      }

      if (nflux == 1) {
				// 1-d Line segment
	value = vvector[1] - vvector[0];

      } else if (nflux == 2) {	
				// 2-d Rectangle
	value = vvector[3] - vvector[2] - vvector[1] + vvector[0];

      } else if (nflux == 3) {	
				// 3-d (rectangular) Prism
	value = vvector[7] - vvector[6] - vvector[5] + vvector[4]
	  -vvector[3] + vvector[2] + vvector[1] - vvector[0];

      } else {			// Throw exception . . .
	cerr << "No such dimension\n";
	exit(-1);
      }
				// Rounding guard
      value = max<double>(0.0, value);

      // Linear interpolation on distance modulus
      if (im==0)
	ans += value*(1.0 - ff[im]/dwid);
      else
	ans += value*(1.0 + ff[im]/dwid);
      
    } // Dist mod loop

  } else {

    int ndim, icnt;
    HMapKey::iterator p, pe;
    double fbmin, fbmax;
    vector<double> fflux(nflux);

    for (int im=0; im<2; im++) {
      
      ndim = 1;
      for (size_t k=0; k<nflux; k++) {

	// Compute indices for surrouding bounds of data hypercube
	if (cmd && k>0) {
	  iflxmin[k] = 
	    (int)( (fmin[k] - minmag[k] - 0.5*wmag[k])/wmag[k] );
	  iflxmax[k] = 
	    (int)( (fmax[k] - minmag[k] + 0.5*wmag[k])/wmag[k] );
	} else {
	  iflxmin[k] = 
	    (int)( (fmin[k] - ff[im] - minmag[k] - 0.5*wmag[k])/wmag[k] );
	  iflxmax[k] = 
	    (int)( (fmax[k] - ff[im] - minmag[k] + 0.5*wmag[k])/wmag[k] );
	}
	iflxoff[k] = ndim;
	// Index guard . . .
	iflxmin[k] = max<int>(iflxmin[k], 0);
	iflxmax[k] = min<int>(iflxmin[k], nmag[k]);

	ndim *= iflxmax[k] - iflxmin[k] + 1;
      }


      for (int i=0; i<ndim; i++) {
    
	// Magic here to get offset for each dimension
	icnt = i;
	for (int k=nflux-1; k>=0; k--) {
	  iflx[k] = icnt/iflxoff[k];
	  icnt -= iflx[k]*iflxoff[k];
	}
	
	// Restore index array
	for (size_t k=0; k<nflux; k++) iflx[k] += iflxmin[k];

	// Look up value in the hash table
	if (!use_array) {
	  key.reset(zz, iflx);
	  p =  Hash[tt][mm+im].find(key);
	  pe = Hash[tt][mm+im].end();
	}

				// Null value means zero so only need to
				// process on non null . . .
	if (p != pe) {

	  value = p->second;

	  for (size_t k=0; k<nflux; k++) {
	  
				// Edges of tabulated isochrone bins
	    fbmin = max<double>(minmag[k] + wmag[k]*((double)iflx[k] - 0.5), 
				minmag[k] - 0.5*wmag[k]);

	    fbmax = min<double>(fbmin + wmag[k], 
				maxmag[k] + 0.5*wmag[k]);

	    if (cmd && k>0) {
	      flxwght[k] = 
		(min<double>(fmax[k], fbmax) - 
		 max<double>(fmin[k], fbmin)) / wmag[k];
	    } else {
	      flxwght[k] = 
		(min<double>(fmax[k]-ff[im], fbmax) - 
		 max<double>(fmin[k]-ff[im], fbmin)) / wmag[k];
	    }
	  }
	  for (size_t k=0; k<nflux; k++) value *= flxwght[k];

	
	  // Linear interpolation on distance modulus
	  if (im==0)
	    ans += value*(1.0 - ff[im]/dwid);
	  else
	    ans += value*(1.0 + ff[im]/dwid);

	}

      } // Dimension loop

    } // Dist mod loop

  } // Hash case

  return ans;
}


double PopulationsApp::GetValue(vector<int>& Iflx, int tt, int zz, int mm)
{
  if (tt<0 || tt>=(int)nages) return 0.0;

  for (size_t k=0; k<nflux; k++) {
    if (Iflx[k]<0 || Iflx[k]>=nmag[k]) return 0.0;
  }

  if (zz<0 || zz>=(int)nmetal) return 0.0;

  if (mm<0 || mm>=(int)ndistm) return 0.0;

  if (use_array)
    return Array[tt][mm][zz][get_index(Iflx)];
  else {
    key.reset(zz, Iflx);
    HMapKey::iterator p = Hash[tt][mm].find(key);
    if (p != Hash[tt][mm].end()) return p->second;
    else return 0.0;
  }
}

void PopulationsApp::VerifyDouble(void)
{
  string thead("t");
  string zhead("z");
  string mhead("m");
  const int linesize = 1024;
  char line[linesize];
  double z, m, value, tvalue;
  int wrong = 0;
  bool ok = true;

  for (size_t i=0; i<nages; i++) {

    for (size_t l=0; l<ndistm; l++) {

      for (size_t j=0; j<nmetal; j++) {

	string file = datadir + "/" 
	  + thead + sages[i] 
	  + zhead + smetal[j]
	  + mhead + sdistm[l];

	ifstream infile(file.c_str());
	if (!infile) continue;

	while (infile) {
	  infile.getline(line, linesize);
	  if (strlen(line)==0) continue;
	  istringstream istr(line);
	  if (index(line,'#')) continue;
	  if (istr) {
	    istr >> z;
	    istr >> m;
	    for (size_t k=0, ik=0; k<nflux0; k++) {
	      istr >> value;
	      if (use_flux[k]) flux[ik++] = value;
	    }
	    istr >> value;
	  } 
	
	  // tvalue = GetValue(flux, ages[i], z);
	  tvalue = Interpolate(flux, ages[i], z, m);

	  if (fabs(tvalue - value) > 1.0e-6) {
	    ok = false;
	    wrong++;
	  }
	}
      }
    }
    
  }

  if (ok)
    cout << "Cached data agrees with original tables, good!\n";
  else
    cout << "Ooops, " << wrong << " values are incorrect\n";
}


void PopulationsApp::VerifyInt(void)
{
  string thead("t");
  string zhead("z");
  string mhead("m");
  const int linesize = 1024;
  char line[linesize];
  double z, m, value, tvalue;
  int wrong = 0;
  bool ok = true;

  for (size_t i=0; i<nages; i++) {

    for (size_t l=0; l<ndistm; l++) {

      for (size_t j=0; j<nmetal; j++) {

	string file = datadir + "/" 
	  + thead + sages[i] 
	  + zhead + smetal[j]
	  + mhead + sdistm[l];

	ifstream infile(file.c_str());
	if (!infile) continue;

	while (infile) {
	  infile.getline(line, linesize);
	  if (strlen(line)==0) continue;
	  istringstream istr(line);
	  if (index(line,'#')) continue;
	  if (istr) {
	    istr >> z;
	    istr >> m;
	    for (size_t k=0, ik=0; k<nflux0; k++) {
	      istr >> value;
	      if (use_flux[k]) {
		iflx[ik] = (int)( (value - minmag[ik] + 0.5*wmag[ik])/wmag[ik] );
		ik++;
	      }
	    }
	    istr >> value;
	  } 
	  
	  tvalue = GetValue(iflx, i, j, l);

	  if (fabs(tvalue - value) > 1.0e-6) {
	    ok = false;
	    wrong++;
	  }
	}
      }
    }
    
  }
  
  if (ok)
    cout << "Cached data agrees with original tables, good!\n";
  else
    cout << "Ooops, " << wrong << " values are incorrect\n";
}


void PopulationsApp::debug_norm()
{
  ofstream out("norm.debug");
  HMapKey::iterator p;
  int zz;
  double value;
  
  if (use_array)
    out << "# Array\n";
  else
    out << "# Hash\n";

  for (size_t i=0; i<nages; i++) {

    for (size_t l=0; l<ndistm; l++) {

      for (size_t j=0; j<nmetal; j++) {

	zz = MetalIndex(metal[j]);

	for (iflx[1]=0; iflx[1]<nmag[1]; iflx[1]++) {

	  value = 0;

	  for (iflx[0]=0; iflx[0]<nmag[0]; iflx[0]++) {


	    if (use_array) {
	      
	      value += Array[i][l][j][get_index(iflx)];

	    } else {
	      
	      key.reset(zz, iflx);
	      p = Hash[i][l].find(key);
	      if (p != Hash[i][l].end()) value += p->second;
	    }

	  }

	  out << setw(5) << i << setw(5) << l << setw(5) << j 
	      << setw(5) << iflx[1]
	      << setw(18) << value << endl;


	}
	
      }	// End Metal loop

    } // End Dist Mod loop

  } // End Age loop
  
}
