/*******************************************************
  Probes table of SFD98 E(B-V) maps, blocked into 
  0.4x0.4 degree bins, for mean reddening, variance,
  minimum and maximum, given a latitude and longitude,
  and radius around that point. (Input in degrees)
 
  AAC 5/19/01 
 *******************************************************/

#include <math.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
using namespace std;

#include <SFD.H>
BIE_CLASS_EXPORT_IMPLEMENT(SFD)


int    SFD::NUM=40;
double SFD::A1=2.5;
double SFD::Z1=150.0;
bool   SFD::RING=false;
double SFD::RINGWIDTH=0.25;
double SFD::RINGLOC=4.0;
double SFD::RINGAMP=1.0;
double SFD::CENTERWIDTH=1.0;
double SFD::CENTERAMP=1.0;
double SFD::NEARBYWIDTH=0.2;
double SFD::NEARBYAMP=0.0;
double SFD::Z0=15.0;
double SFD::R0=8.0;
double SFD::RMIN=0.01;
double SFD::RMAX=100.0;
bool   SFD::RLOG=true;

size_t floatHash(float z) {
  floatUnion u;
  u.z = z;
  return u.i*0x9e3779b1u;
}


float less_lon::f = 0.0;
bool less_lon::operator()(const sfddata& s1) const
{
  return s1.lon < f;
}


float greater_lon::f = 0.0;
bool greater_lon::operator()(const sfddata& s1) const
{
  return s1.lon > f;
}


SFD::SFD(const string& DataDir, bool spatial)
{
  sfddata data;

  space = spatial;
  
  float lat;
  const float onedeg = 57.29577951;
  const int nlin=257662;

  // Convert from degrees to radians for distance calc
  
  string datafile = DataDir + "/sfd98_rtab.txt\0";

  ifstream sfd(datafile.c_str());
  if (!sfd) {
    cerr << "getsfd: couldn't open data file <" << datafile << ">\n";
    exit(-1);
  }

  const int bufsize = 2048;
  char line[bufsize];

  for (int j=1; j<=nlin; j++) {

    sfd.getline(line, bufsize);
    istringstream in(line);
    
    in >> data.lon;
    in >> lat;
    in >> data.red;
    in >> data.var;
    in >> data.rmn;
    in >> data.rmx;

    lat /= onedeg;
    data.lon /= onedeg;

    if (spatial) compute_extinction(lat, data);

    sfdmap[lat].push_back(data);
  }

  SFDmap::iterator p;
  for (p = sfdmap.begin(); p != sfdmap.end(); p++) {
    sort(p->second.begin(), p->second.end(), less_sfd());
    sfdlat.push_back(p->first);
  }
  sort(sfdlat.begin(), sfdlat.end());

}


void SFD::compute_interp(double s, int& indx, double& c1, double& c2)
{
  if (s<=radius.front())
    indx = 0;

  else if (s>=radius.back())
    indx = radius.size()-2;

  else {
    if (RLOG)
      indx = (int)( (log(s) - log(RMIN))/dr );
    else
      indx = (int)( (s - RMIN)/dr );
  }


  if (RLOG)
    c2 = (log(s) - log(radius[indx]))/dr;
  else
    c2 = (s - radius[indx])/dr;

  c1 = 1.0 - c2;
}

void SFD::compute_extinction(float& lat, sfddata& data)
{
  double curabs, sech, lastabs=0.0, lasts=0.0;

  if (radius.size() != (unsigned)NUM) radius = vector<float>(NUM);
  data.absorb = radius;

  double cosB = cos(lat);
  double sinB = sin(lat);
  double cosL = cos(data.lon);
  double sinL = sin(data.lon);

  if (RLOG) RMIN = max<double>(RMIN, 0.001);

  double s, R, x, y, z;

				// Begin integration over line of sight
  for (int n=0; n<NUM; n++) {

    if (RLOG) {
      dr = (log(RMAX) - log(RMIN))/NUM;
      s = RMIN*exp(dr*n);
      radius[n] = s;
    } else {
      dr = (RMAX- RMIN)/NUM;
      s = RMIN + dr*n;
      radius[n] = s;
    }

    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    x = s*cosB*cosL - R0;
    y = s*cosB*sinL;
    z = s*sinB - Z0*1.0e-3;

				// Ring and central extinction
    if (RING) {

      curabs = 
	CENTERAMP*exp(-(R*R+z*z)/(2.0*CENTERWIDTH*CENTERWIDTH)) /
	pow(2.0*M_PI*CENTERWIDTH*CENTERWIDTH, 1.5)
	+
	RINGAMP*exp(-(R-RINGLOC)*(R-RINGLOC)/(2.0*RINGWIDTH*RINGWIDTH)) *
	exp(-z*z/(2.0*Z1*Z1*1.0e-6)) / (4.0*M_PI*M_PI*RINGWIDTH*Z1*1.0e-3)
	+
	NEARBYAMP*exp(-(x*x + y*y + z*z)/(2.0*NEARBYWIDTH*NEARBYWIDTH)) /
	(4.0*M_PI*M_PI*NEARBYWIDTH*NEARBYWIDTH)
	;
				// Standard disk extinction
    } else {
      sech = 0.5/( exp(z*0.5e3/Z1) + exp(-z*0.5e3/Z1) );
      curabs = exp(-R/A1)*sech*sech * (2.5e2/Z1) / (A1*A1*2.0*M_PI);
    }

    data.absorb[n] = 0.5*(s - lasts)*(lastabs+curabs);
    if (n>0) data.absorb[n] += data.absorb[n-1];
    lasts = s;
    lastabs = curabs;
  }

				// Normalize absorption
  double abmax = data.absorb[NUM-1];
  if (abmax>0.0)
    for (int n=0; n<NUM; n++) data.absorb[n] /= abmax;
}


void SFD::getsfd(float l, float b, float q, 
		 float& ebv, float& sig, float& emn, float& emx)
{
  const float onedeg = 57.29577951;

  // Convert from degrees to radians for distance calc

  float lr = l/onedeg;
  float br = b/onedeg;
  float qr = q/onedeg;
  
				// Put in lr in [0, 2Pi]
  lr = lr - 2.0*M_PI*floor(lr/(2.0*M_PI));

  int npix = 0;
  ebv = 0.0;
  sig = 0.0;
  emn = 1000.0;
  emx = 0.0;

  vector<float>::iterator bb, bn;
  vector<sfddata>::iterator data, ll;
  float r;

  if ( br - qr <= sfdlat.front() )
    bn = sfdlat.begin();

  else if ( br - qr >= sfdlat.back() )	
    bn = sfdlat.end()-1;

  else {
    bn = find_if(sfdlat.begin(), sfdlat.end(), 
		 bind2nd(greater<float>(), br-qr));
    if (bn != sfdlat.begin()) bn = bn - 1;
  }


  for (bb=bn; bb != sfdlat.end(); bb++) {

    if (*bb - br > qr) break;

    greater_lon::f = lr-qr/min<double>(cos(*bb),cos(br));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=ll; data != sfdmap[*bb].end(); data++) {

      if (data->lon - lr > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	ebv = ebv + data->red;
	sig = sig + data->var*data->var;
	emn = min<float>(emn, data->rmn);
	emx = max<float>(emx, data->rmx);

	npix++;
      } 

    }

    greater_lon::f = 2.0*M_PI - (lr+qr/min<double>(cos(*bb),cos(br)));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll-1 != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=sfdmap[*bb].end()-1; data != ll; data--) {

      if (2.0*M_PI + lr - data->lon > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	ebv = ebv + data->red;
	sig = sig + data->var*data->var;
	emn = min<float>(emn, data->rmn);
	emx = max<float>(emx, data->rmx);

	npix++;
      } 
      else break;

    }

  }

  if (npix) {
    ebv = ebv/npix;
    sig = sqrt(sig/npix);
  }
}



void SFD::getsfd_closest(float l, float b,
		 float& ebv, float& sig, float& emn, float& emx)
{
  const float onedeg = 57.29577951;

  // Convert from degrees to radians for distance calc

  float lr = l/onedeg;
  float br = b/onedeg;
  
  lr = lr - 2.0*M_PI*floor(lr/(2.0*M_PI));

  // Compute closest in box about desired line-of-sight

  float bb[2];
  sfddata ll[2];
  float r = 1.0e20, r1;
  sfddata data;

  vector<sfddata>::iterator lp;
  vector<float>::iterator bn;

  if ( br <= sfdlat.front() )
    bb[0] = bb[1] = sfdlat.front();

  else if ( br >= sfdlat.back() )	
    bb[0] = bb[1] = sfdlat.back();

  else {
    bn = find_if(sfdlat.begin(), sfdlat.end(), 
		 bind2nd(greater<float>(), br));
    if ( bn == sfdlat.begin() ) bb[0] = *bn;
    else bb[0] = *(bn-1);
    bb[1] = *bn;
  }
  
  for (int j=0; j<2; j++) {

    greater_lon::f = lr;
    if (greater_lon::f <= sfdmap[bb[j]].front().lon ||
	greater_lon::f >= sfdmap[bb[j]].back().lon  ) {
      ll[0] = sfdmap[bb[j]].back();
      ll[1] = sfdmap[bb[j]].front();
  }
    else {
      lp = find_if(sfdmap[bb[j]].begin(), sfdmap[bb[j]].end(), greater_lon());
      if ( lp == sfdmap[bb[j]].begin() ) ll[0] = *lp;
      else ll[0] = *(lp-1);
      ll[1] = *lp;
    }

    for (int i=0; i<2; i++) {

      if ( (r1=acos(sin(br)*sin(bb[j])+cos(br)*cos(bb[j]) * 
		    cos(lr - ll[i].lon))) < r) {
	r = r1;
	data = ll[i];
      }
      
    }

  }

  ebv = data.red;
  sig = data.var;
  emn = data.rmn;
  emx = data.rmx;
}


void SFD::getext_distance(float l, float b, float q, float s,
			  float& aj, float& ah, float& ak)
{
  const float onedeg = 57.29577951;

  // Convert from degrees to radians for distance calc

  float lr = l/onedeg;
  float br = b/onedeg;
  float qr = q/onedeg;
  
  lr = lr - 2.0*M_PI*floor(lr/(2.0*M_PI));

  int indx;
  double c1, c2, path;

  compute_interp(s, indx, c1, c2);

  int npix = 0;
  double ebv = 0.0;

  vector<float>::iterator bb, bn;
  vector<sfddata>::iterator data, ll;
  float r;

  if ( br - qr <= sfdlat.front() )
    bn = sfdlat.begin();

  else if ( br - qr >= sfdlat.back() )	
    bn = sfdlat.end()-1;

  else {
    bn = find_if(sfdlat.begin(), sfdlat.end(), 
		 bind2nd(greater<float>(), br-qr));
    if (bn != sfdlat.begin()) bn = bn - 1;
  }

  for (bb=bn; bb != sfdlat.end(); bb++) {

    if (*bb - br > qr) break;

    greater_lon::f = lr-qr/min<double>(cos(*bb),cos(br));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=ll; data != sfdmap[*bb].end(); data++) {

      if (data->lon - lr > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	path = max<double>(data->absorb[indx]*c1+data->absorb[indx+1]*c2, 0.0);
	ebv += data->red * path;
	
	npix++;
      } 

    }

    greater_lon::f = 2.0*M_PI - (lr+qr/min<double>(cos(*bb),cos(br)));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll-1 != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=sfdmap[*bb].end()-1; data != ll; data--) {

      if (2.0*M_PI + lr - data->lon > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	path = max<double>(data->absorb[indx]*c1+data->absorb[indx+1]*c2, 0.0);
	ebv += data->red * path;

	npix++;
      } 
      else break;

    }

  }

  if (npix) {
    aj = 0.90*ebv/npix;
    ah = 0.53*ebv/npix;
    ak = 0.34*ebv/npix;
  } 
  else
    aj = ah = ak = 0.0;
}


int SFD::getext_distance(float l, float b, float q, float s, 
			 vector<float>& ebv)
{
  const float onedeg = 57.29577951;
  vector<float> ebv1;

  // Convert from degrees to radians for distance calc

  float lr = l/onedeg;
  float br = b/onedeg;
  float qr = q/onedeg;
  
  lr = lr - 2.0*M_PI*floor(lr/(2.0*M_PI));

  int indx;
  double c1, c2, path;

  compute_interp(s, indx, c1, c2);

  vector<float>::iterator bb, bn;
  vector<sfddata>::iterator data, ll;
  float r;

  if ( br - qr <= sfdlat.front() )
    bn = sfdlat.begin();

  else if ( br - qr >= sfdlat.back() )	
    bn = sfdlat.end()-1;

  else {
    bn = find_if(sfdlat.begin(), sfdlat.end(), 
		 bind2nd(greater<float>(), br-qr));
    if (bn != sfdlat.begin()) bn = bn - 1;
  }

  for (bb=bn; bb != sfdlat.end(); bb++) {

    if (*bb - br > qr) break;

    greater_lon::f = lr-qr/min<double>(cos(*bb),cos(br));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=ll; data != sfdmap[*bb].end(); data++) {

      if (data->lon - lr > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	path = max<double>(data->absorb[indx]*c1+data->absorb[indx+1]*c2, 0.0);
	ebv1.push_back(data->red * path);
      }


    }

    greater_lon::f = 2.0*M_PI - (lr+qr/min<double>(cos(*bb),cos(br)));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll-1 != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=sfdmap[*bb].end()-1; data != ll; data--) {

      if (2.0*M_PI + lr - data->lon > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	path = max<double>(data->absorb[indx]*c1+data->absorb[indx+1]*c2, 0.0);
	ebv1.push_back(data->red * path);
      } 
      else break;

    }

  }

  int ret;

  sort(ebv1.begin(), ebv1.end());

  if (ebv1.size() < ebv.size()) {

    ret = ebv1.size();
    for (int i=0; i<ret; i++) ebv[i] = ebv1[i];

  } else {

    ret = ebv.size();
    float j = 0.0;
    float skip = (float)ebv1.size()/ret;
    for (int i=0; i<ret-1; i++) {
      ebv[i] = ebv1[(int)j];
      j += skip;
    }
    ebv[ret-1] = ebv1.back();
  }

  return ret;
}


void SFD::getext_distance(float l, float b, float q, float s, 
			 float& ebvmin, float& ebvmax)
{
  const float onedeg = 57.29577951;
  float ebvt;

  ebvmax = 0.0;
  ebvmin = 1e20;

  // Convert from degrees to radians for distance calc

  float lr = l/onedeg;
  float br = b/onedeg;
  float qr = q/onedeg;
  
  lr = lr - 2.0*M_PI*floor(lr/(2.0*M_PI));

  int indx;
  double c1, c2, path;

  compute_interp(s, indx, c1, c2);

  vector<float>::iterator bb, bn;
  vector<sfddata>::iterator data, ll;
  float r;

  if ( br - qr <= sfdlat.front() )
    bn = sfdlat.begin();

  else if ( br - qr >= sfdlat.back() )	
    bn = sfdlat.end()-1;

  else {
    bn = find_if(sfdlat.begin(), sfdlat.end(), 
		 bind2nd(greater<float>(), br-qr));
    if (bn != sfdlat.begin()) bn = bn - 1;
  }

  for (bb=bn; bb != sfdlat.end(); bb++) {

    if (*bb - br > qr) break;

    greater_lon::f = lr-qr/min<double>(cos(*bb),cos(br));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=ll; data != sfdmap[*bb].end(); data++) {

      if (data->lon - lr > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	path = max<double>(data->absorb[indx]*c1+data->absorb[indx+1]*c2, 0.0);
	ebvt = data->red * path;
	ebvmin = min<float>(ebvmin, ebvt);
	ebvmax = max<float>(ebvmax, ebvt);
      }


    }

    greater_lon::f = 2.0*M_PI - (lr+qr/min<double>(cos(*bb),cos(br)));
    if (greater_lon::f < sfdmap[*bb].front().lon) ll = sfdmap[*bb].begin();
    else {
      ll = find_if(sfdmap[*bb].begin(), sfdmap[*bb].end(), greater_lon());
      if (ll-1 != sfdmap[*bb].begin()) ll = ll - 1;
    }
    
    for (data=sfdmap[*bb].end()-1; data != ll; data--) {

      if (2.0*M_PI + lr - data->lon > qr/min<double>(cos(*bb),cos(br))) break;

      r = acos(sin(br)*sin(*bb)+cos(br)*cos(*bb)*cos(lr - data->lon));

      if (r <= qr) {
	path = max<double>(data->absorb[indx]*c1+data->absorb[indx+1]*c2, 0.0);
	ebvt = data->red * path;
	ebvmin = min<float>(ebvmin, ebvt);
	ebvmax = max<float>(ebvmax, ebvt);
      } 
      else break;

    }

  }
}



void SFD::getext_closest_distance(float l, float b, float s,
				  float& aj, float& ah, float& ak)
  {
  const float onedeg = 57.29577951;

  // Convert from degrees to radians for distance calc

  float lr = l/onedeg;
  float br = b/onedeg;
  
  lr = lr - 2.0*M_PI*floor(lr/(2.0*M_PI));

  int indx;
  double c1, c2, path;

  compute_interp(s, indx, c1, c2);

  // Compute closest in box about desired line-of-sight

  float bb[2];
  sfddata ll[2];
  float r = 1.0e20, r1;
  sfddata data;

  vector<sfddata>::iterator lp;
  vector<float>::iterator bn;

  if ( br <= sfdlat.front() )
    bb[0] = bb[1] = sfdlat.front();

  else if ( br >= sfdlat.back() )	
    bb[0] = bb[1] = sfdlat.back();

  else {
    bn = find_if(sfdlat.begin(), sfdlat.end(), 
		 bind2nd(greater<float>(), br));
    if ( bn == sfdlat.begin() ) bb[0] = *bn;
    else bb[0] = *(bn-1);
    bb[1] = *bn;
  }
  
  for (int j=0; j<2; j++) {

    greater_lon::f = lr;
    if (greater_lon::f <= sfdmap[bb[j]].front().lon ||
	greater_lon::f >= sfdmap[bb[j]].back().lon  ) {
      ll[0] = sfdmap[bb[j]].back();
      ll[1] = sfdmap[bb[j]].front();
  }
    else {
      lp = find_if(sfdmap[bb[j]].begin(), sfdmap[bb[j]].end(), greater_lon());
      if ( lp == sfdmap[bb[j]].begin() ) ll[0] = *lp;
      else ll[0] = *(lp-1);
      ll[1] = *lp;
    }

    for (int i=0; i<2; i++) {

      if ( (r1=acos(sin(br)*sin(bb[j])+cos(br)*cos(bb[j]) * 
		    cos(lr - ll[i].lon))) < r) {
	r = r1;
	data = ll[i];
      }
      
    }

  }

  path = max<double>(data.absorb[indx]*c1 + data.absorb[indx+1]*c2, 0.0);

  aj = 0.90*data.red*path;
  ah = 0.53*data.red*path;
  ak = 0.34*data.red*path;

}


/************************************************
  Measure pathlength through a uniform dust disk
  with a given height.   The total reddening 
  integrated from zero to infinite distance is
  supplied by another routine that samples from
  the Schlegel et al. (1998) maps.
  Use the reddening relations
  given in Bessell & Brett (1988) to go from 
  reddening to extinction in the bandpass.

  Requires: galactic coordinates in degrees,
  a distance in parsecs, and a total reddening
  E(B-V) from a prior routine.

  Returns:  A(J), A(H), A(K)

**************************************************/

void SFD::getext(float s, float l0, float b0, float ebv, 
		 float& aj, float& ah, float& ak)
{

  float path, rgc, l, b;

  // Galactic parameters:
  //   z0 = height of Sun above galactic plane (pc)
  //   zmax = half-thickness of uniform dust disk (pc)
  //   r0 = distance of Sun from galactic center (pc)
  //   rmax = radius of galactic dust disk (pc)

  const float z0=15.0;
  const float zmax=150.0;
  const float r0=8500.0;
  const float rmax=15000.0;

  // Convert to radians

  const float onedeg = 5.729577951e+01;

  b = b0/onedeg;
  l = l0/onedeg;

  l = l - 2.0*M_PI*floor(l/(2.0*M_PI));

  // Is the field at (l,b,s) within the dust disk?

  float term1 = s*cos(b)*cos(l) - r0;
  float term2 = s*cos(b)*sin(l);
  rgc = sqrt(term1*term1 + term2*term2);

				// Outside cylindrical edge of disk
  if (rgc < rmax) {
				// Outside faces of disk
    path = s*fabs(sin(b))/(zmax-copysign(z0, b));
    if (path >= 1.0) 
      path = 1.0;
    else			// Inside disk
      path = s/(cos(l)*r0+sqrt(r0*r0*((cos(l)*cos(l))-1.0)+rmax*rmax));
  }
  else
    path = 1.0;

  aj = 0.90*ebv*path;
  ah = 0.53*ebv*path;
  ak = 0.34*ebv*path;
}
