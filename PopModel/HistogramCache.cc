#include <HistogramCache.h>
BIE_CLASS_EXPORT_IMPLEMENT(HistogramCache)

#include <cfloat>
#include <climits>
#include <sstream>
#include <iomanip>
using namespace std;

// --------------------------------------------------------
// a multiplication hash function
// --------------------------------------------------------
size_t munge_knuth(HistoKey& x)
{
  /*
    In Knuth's "The Art of Computer Programming", section 6.4, a
    multiplicative hashing scheme is introduced as a way to write hash
    function. The key is multiplied by the golden ratio of 2^32
    (2654435761) to produce a hash result.

    Since 2654435761 and 2^32 has no common factors in common, the
    multiplication produces a complete mapping of the key to hash
    result with no overlap. This method works pretty well if the keys
    have small values. Bad hash results are produced if the keys vary
    in the upper bits. As is true in all multiplications, variations
    of upper digits do not influence the lower digits of the
    multiplication result.  
  */

  const int q = 128;
  unsigned long i = 0;
  for (int k=0; k<x.get_dim(); k++)
    i = i*q + x.get_I(k);
  // i += x.get_Z();

  return i*0x9e3779b1u;
}

int hash_func_knuth(HistoKey& x, int Size)
{
  unsigned long i = munge_knuth(x);
  double z = 0.5*(double)i/LONG_MAX;

  unsigned int h = int(z*Size);

  if (h<0 || h>=(unsigned)Size) {
    cerr << "Error in hash_func_knuth\n";
  }
  return h;
}


int hash_func(HistoKey& x, int Size)
{
  unsigned long key = munge_knuth(x);

				// A 32 bit mix function
  key += (key << 12);
  key ^= (key >> 22);
  key += (key << 4);
  key ^= (key >> 9);
  key += (key << 10);
  key ^= (key >> 2);
  key += (key << 7);
  key ^= (key >> 12);

  double z = 0.5*(double)key/LONG_MAX;

  unsigned int h = int(z*Size);

  if (h<0 || h>=(unsigned)Size) {
    cerr << "Error in hash_func\n";
  }
  return h;
}


int HistogramCache::numBuckets=2000;

HistogramCache::HistogramCache(vector<double>& lo, vector<double>& hi, 
			       vector<double>& wid)
{
  low = lo;
  high = hi;
  width = wid;

  dim = lo.size();

  for (unsigned i=0; i<dim; i++) {
    unsigned short j = (unsigned short)( (high[i] - low[i])/width[i] ) + 1;

    high[i] = low[i] + width[i]*j;
    ndim.push_back(j);
  }

  indx = vector<unsigned short>(dim);
}

HistogramCache::HistogramCache(HistogramCache& p)
{
  low = p.low;
  high = p.high;
  width = p.width;

  dim = p.dim;

  ndim = p.ndim;
  indx = p.indx;

  histo = p.histo;
  key = p.key;
}

double HistogramCache::getValue(vector<double>& z)
{
  if (dim>z.size()) {
    ostringstream msg;
    msg << "HistogramCache::getvalue(): "
	<< "expected dimension [" << dim << "] is larger than the input "
	<< "rank of " << z.size();
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  for (unsigned i=0; i<dim; i++) {
    if (z[i]<=low[i] || z[i]>=high[i]) return 0;

    indx[i] = (unsigned short)( (z[i] - low[i])/width[i] );
  }
  
  key.reset(indx);

  mmapHK::iterator itr = histo.find(key);

  if (itr==histo.end()) return 0;

  return itr->second;
}

double HistogramCache::getValue(vector<unsigned short>& z)
{
  if (dim!=z.size()) {
    ostringstream msg;
    msg << "HistogramCache::getvalue: dimension [" << dim 
	<< "] does not match the input"	<< " rank of " << z.size();
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  key.reset(z);

  mmapHK::iterator itr = histo.find(key);

  if (itr==histo.end()) return 0;

  return itr->second;
}

void HistogramCache::addValue(vector<double>& z, double value)
{
  if (dim>z.size()) {
    ostringstream msg;
    msg << "HistogramCache::addValue: dimension [" 
	<< dim << "] does not match the input" << " rank of " << z.size();
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  for (unsigned i=0; i<dim; i++) {
    if (z[i]<=low[i] || z[i]>=high[i]) return;

    indx[i] = (unsigned short)( (z[i] - low[i])/width[i] );
  }
  
  key.reset(indx);

  histo[key] = value;
}

bool HistogramCache::addToExistingValue(vector<double>& z, double value)
{
  if (dim>z.size()) {
  }

  for (unsigned i=0; i<dim; i++) {
    if (z[i]<=low[i] || z[i]>=high[i]) return false;

    indx[i] = (unsigned short)( (z[i] - low[i])/width[i] );
  }
  
  key.reset(indx);

  mmapHK::iterator itr = histo.find(key);

  if (itr==histo.end()) {
    histo[key] = value;
    return false;
  } else {
    itr->second += value;
    return true;
  }
}

void HistogramCache::printList()
{
  mmapHK::iterator elist;
  
  cout << endl;
  for (unsigned j=0; j<dim; j++) {
    ostringstream label;
    label << "Dimension " << j;
    cout << setw(15) << label.str();
  }
  cout << setw(15) << "Value" << endl;

  for (unsigned j=0; j<dim; j++) {
    cout << setw(15) << "--------------";
  }
  cout << setw(15) << "--------------" << endl;

  for (elist=histo.begin(); elist!=histo.end(); elist++) {
    for (unsigned j=0; j<dim; j++) 
      cout << setw(15) << low[j] + 
	width[j]*(0.5 + elist->first.get_I(j));
    cout << setw(15) << elist->second << endl;
  }

}
