	subroutine getext(s,l0,b0,ebv,aj,ah,ak)
c	program getext

c************************************************
c Measure pathlength through a uniform dust disk
c  with a given height.   The total reddening 
c  integrated from zero to infinite distance is
c  supplied by another routine that samples from
c  the Schlegel et al. (1998) maps.
c Use the reddening relations
c  given in Bessell & Brett (1988) to go from 
c  reddening to extinction in the bandpass.
c
c Requires: galactic coordinates in degrees,
c  a distance in parsecs, and a total reddening
c  E(B-V) from a prior routine.
c
c Returns:  A(J), A(H), A(K)
c
c**************************************************

	real s,l0,b0,l,b,ebv,aj,ah,ak
	real path,rgc,x,y
	integer n

	real z0,zmax,r0,rmax
	real pi

	parameter (pi=3.14159265)

c Galactic parameters:
c   z0 = height of Sun above galactic plane (pc)
c zmax = half-thickness of uniform dust disk (pc)
c   r0 = distance of Sun from galactic center (pc)
c rmax = radius of galactic dust disk (pc)

	parameter (z0=15.0)
	parameter (zmax=150.0)
	parameter (r0=8500.0)
	parameter (rmax=15000.0)

c Convert to radians
	b = b0*pi/180.
	l = l0*pi/180.

c Is the field at (l,b,s) within the dust disk?

	rgc = sqrt((s*cos(b)*cos(l)-r0)**2+(s*cos(b)*sin(l))**2)
	if (rgc.lt.rmax) then
	  if (b.ne.0.0) then
	    path = s*abs(sin(b))/(zmax-sign(z0,b))
	    if (path.ge.1.0) then
	      path = 1.0
	    endif
	  else
	    path = s/(cos(l)*r0+sqrt(r0**2*((cos(l)**2)-1.0)+rmax**2))
	  endif
	else
	  path = 1.0
	endif

	aj = 0.90*ebv*path
	ah = 0.53*ebv*path
	ak = 0.34*ebv*path

	return
	end  
