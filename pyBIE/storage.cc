#include "storage.H"
#include "BIEmpi.h"

namespace BIE {

  // Choose the Boost archive type
  BIE_ENGINE_TYPE arType = BOOST_BINARY_ARCHIVE;

  // Boost text archives
  boost::archive::text_iarchive* ar_in_T;
  boost::archive::text_oarchive* ar_out_T;

  // Boost XML archives
  boost::archive::xml_iarchive* ar_in_X;
  boost::archive::xml_oarchive* ar_out_X;

  // Boost binary archives
  boost::archive::binary_iarchive* ar_in_B;
  boost::archive::binary_oarchive* ar_out_B;

  // IO streams
  static std::ofstream ar_out_F;
  static std::ifstream ar_in_F;

  // Open the boost archive for reading
  void openInArchive(std::string archive_name)
  {
    ar_in_F.open(archive_name.c_str());
    if (myid==0) std::cout << "Opened input archive <" << archive_name << ">" << std::endl;
    
    switch (arType) {
    case BOOST_XML_ARCHIVE:
      ar_in_X = new boost::archive::xml_iarchive(ar_in_F);
      break;
    case BOOST_BINARY_ARCHIVE:
      ar_in_B = new boost::archive::binary_iarchive(ar_in_F);
      break;
    case BOOST_TEXT_ARCHIVE:
    default:
      ar_in_T = new boost::archive::text_iarchive(ar_in_F);
      break;
    }
  }
  
  // Close the input archive 
  void closeInArchive()
  {
    switch (arType) {
    case BOOST_XML_ARCHIVE:
      delete ar_in_X;
      break;
    case BOOST_BINARY_ARCHIVE:
      delete ar_in_B;
      break;
    case BOOST_TEXT_ARCHIVE:
    default:
      delete ar_in_T;
      break;
    }
    ar_in_F.close();
  }

  // Open the boost archive for writing
  void openOutArchive(std::string archive_name)
  {
    ar_out_F.open(archive_name.c_str());
    std::cout << "Opened output archive <" << archive_name << ">" << std::endl;

    switch (arType) {
    case BOOST_XML_ARCHIVE:
      ar_out_X = new boost::archive::xml_oarchive(ar_out_F);
      break;
    case BOOST_BINARY_ARCHIVE:
      ar_out_B = new boost::archive::binary_oarchive(ar_out_F);
      break;
    case BOOST_TEXT_ARCHIVE:
    default:
      ar_out_T = new boost::archive::text_oarchive(ar_out_F);
      break;
    }
  }
  
  // Close the boost output archive
  void closeOutArchive()
  {
    switch (arType) {
    case BOOST_XML_ARCHIVE:
      delete ar_out_X;
      break;
    case BOOST_BINARY_ARCHIVE:
      delete ar_out_B;
      break;
    case BOOST_TEXT_ARCHIVE:
    default:
      delete ar_out_T;
      break;
    }
    ar_out_F.close();
  }
  
  // Serialize the BIE RNG instance
  void StoreBIEACG()
  {
    switch (arType) {
    case BOOST_XML_ARCHIVE:
      *ar_out_X << boost::serialization::make_nvp("pBIEgen", *BIEgen);
      break;
    case BOOST_BINARY_ARCHIVE:
      *ar_out_B << boost::serialization::make_nvp("pBIEgen", *BIEgen);
      break;
    case BOOST_TEXT_ARCHIVE:
    default:
      *ar_out_T << boost::serialization::make_nvp("pBIEgen", *BIEgen);
      break;
    }
    // Debugging . . .
    if (false) PrintBIEACG("in_store");
  }

  // Deserialize the BIE RNG instance
  void RetrieveBIEACG()
  {
    BIEgen = new BIEACG();
    switch (arType) {
    case BOOST_XML_ARCHIVE:
      *ar_in_X >> boost::serialization::make_nvp("pBIEgen", *BIEgen);
      break;
    case BOOST_BINARY_ARCHIVE:
      *ar_in_B >> boost::serialization::make_nvp("pBIEgen", *BIEgen);
      break;
    case BOOST_TEXT_ARCHIVE:
    default:
      *ar_in_T >> boost::serialization::make_nvp("pBIEgen", *BIEgen);
      break;
    }
    // Debugging . . .
    if (false) PrintBIEACG("in_retrieve");
  }

  // Serialize the BIE RNG instance for examining the state
  void PrintBIEACG(std::string suffix)
  {
    BIEgen->toXML(suffix);
  }

};
