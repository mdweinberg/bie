%module(package="BIE") Distribution

%{
#define SWIG_FILE_WITH_INIT

#include "SmplHist.h"
#include "Normal.h"
#include "State.h"
#include "Distribution.h"
#include "OneBin.h"
#include "SimpleStat.h"
#include "Ensemble.h"
#include "EnsembleKD.h"
#include "EnsembleStat.h"
#include "EnsembleDisc.h"
#include "Histogram1D.h"
#include "HistogramND.h"
#include "BetaRDist.h"
#include "Normal.h"
#include "InverseGammaDist.h"
#include "NormalDist.h"
#include "UniformDist.h"
#include "CauchyDist.h"
#include "MultiDistribution.h"
#include "WeibullDist.h"
#include "GammaDist.h"
#include "MultiNWishartDist.h"
#include "DiscUnifDist.h"
#include "MultiNWishartDist.h"
#include "DiscUnifDist.h"
#include "InverseGammaDist.h"
#include "Dirichlet.h"
#include "ErfDist.h"
#include "storage.H"
%}

%include "numpy.i"

%init %{
import_array();
%}

%apply (int* DIM1, int* DIM2, double** ARGOUTVIEW_ARRAY2) {(int* n1, int* n2, double **seq)};

%ignore BIE::MultiDistribution(cliDistribution*);

%import(module="BIEutil") "Serializable.h"

%import "boost_pickle.i"

%include "global.i"
%include "vectors.i"
%include "SmplStat.h"
%include "SmplHist.h"
%include "BIEconfig.h"
%include "State.h"
%include "Distribution.h"
%include "OneBin.h"
%include "SimpleStat.h"
%include "Ensemble.h"
%include "EnsembleKD.h"
%include "EnsembleStat.h"
%include "EnsembleDisc.h"
%include "Histogram1D.h"
%include "HistogramND.h"
%include "BetaRDist.h"
%include "InverseGammaDist.h"
%include "NormalDist.h"
%include "UniformDist.h"
%include "CauchyDist.h"
%include "MultiDistribution.h"
%include "WeibullDist.h"
%include "GammaDist.h"
%include "MultiNWishartDist.h"
%include "DiscUnifDist.h"
%include "MultiNWishartDist.h"
%include "DiscUnifDist.h"
%include "InverseGammaDist.h"
%include "ErfDist.h"
%include "Dirichlet.h"

%extend BIE::PointDistribution
{
  static BIE::PointDistribution* WithS(std::string name)
   {return new BIE::PointDistribution(name);}

  static BIE::PointDistribution* WithRT(BIE::RecordType type)
   {return new BIE::PointDistribution(&type);}

}

%extend BIE::HistogramND
{
  static BIE::HistogramND* WithRT(std::vector<double>& lo_in, 
				  std::vector<double>& hi_in, 
				  std::vector<double>& w_in,
				  BIE::RecordType type)
  { return new BIE::HistogramND(lo_in, hi_in, w_in, &type); }

  static BIE::HistogramND* WithVS(std::vector<double>& lo_in, 
				  std::vector<double>& hi_in, 
				  std::vector<double>& w_in,
				  std::vector<std::string>& names)
  { return new BIE::HistogramND(lo_in, hi_in, w_in, names); }
};

%extend BIE::MultiNWishartDist
{
  static MultiNWishartDist* WithV(double s, std::vector<double> d, int c)
  {
    return new MultiNWishartDist(s, d, c);
  }
};

%import "boost_pickle.i"

%boost_picklable(StateInfo)
%boost_picklable(State)
%boost_picklable(Distribution)
%boost_picklable(SampleDistribution)
%boost_picklable(BinnedDistribution)
%boost_picklable(PointDistribution)
%boost_picklable(NullDistribution)
%boost_picklable(OneBin)
%boost_picklable(SimpleStat)
%boost_picklable(Histogram1D)
%boost_picklable(HistogramND)
%boost_picklable(BetaRDist)
%boost_picklable(InverseGammaDist)
%boost_picklable(NormalDist)
%boost_picklable(UniformDist)
%boost_picklable(CauchyDist)
%boost_picklable(MultiDistribution)
%boost_picklable(WeibullDist)
%boost_picklable(GammaDist)
%boost_picklable(MultiNWishartDist)
%boost_picklable(DiscUnifDist)
%boost_picklable(InverseGammaDist)
%boost_picklable(ErfDist)
%boost_picklable(Dirichlet)


%pythonbegin %{
"""
General representation of probability distributions.
"""
%}
