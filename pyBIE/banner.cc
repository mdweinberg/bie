#include <iostream>
#include <iomanip>
#include <cstring>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <string>

#include <unistd.h>

#include "../config.h"

/// Some ascii art

static char rules[][23] = {
  "___.oOo.______.oOo.___",
  "______________________"};

/// By Felix Lee

static char sammi[][51] = {
  "            _,'|             _.-''``-...___..--';)",
  "           /_ \'.      __..-' ,      ,--...--'''  ",
  "          <\\    .`--'''       `     /'           ",
  "           `-';'               ;   ; ;            ",
  "     __...--''     ___...--_..'  .;.'             ",
  "    (,__....----'''       (,..--''                "};

static void banner(bool b, unsigned s)
{
  if (!b) return;
  for (unsigned n=0; n<72; n++) {
    if (n%4==0) std::cout << '-'  << std::flush;
    if (n%4==1) std::cout << '/'  << std::flush;
    if (n%4==2) std::cout << '|'  << std::flush;
    if (n%4==3) std::cout << '\\' << std::flush;
    usleep(20000);
  }
  std::cout << std::endl;
  sleep(s);
}

namespace BIE {

  // The BIE cat banner
  void make_banner()
  {
    const unsigned sz = 22;
    std::ostringstream a, b;
    a << std::setw((sz - std::strlen(PACKAGE))/2) << "" << PACKAGE;
    b << std::setw((sz - std::strlen(VERSION))/2) << "" << VERSION;
    banner(true, 0);
    std::cout << std::setfill(' ') << std::endl << std::endl;
    std::cout << std::setw(sz) << std::left << " "      << sammi[0] << std::endl;
    std::cout << std::setw(sz) << std::left << rules[0] << sammi[1] << std::endl;
    std::cout << std::setw(sz) << std::left << " "      << sammi[2] << std::endl;
    std::cout << std::setw(sz) << std::left << a.str()  << sammi[3] << std::endl;
    std::cout << std::setw(sz) << std::left << b.str()  << sammi[4] << std::endl;
    std::cout << std::setw(sz) << std::left << rules[1] << sammi[5] << std::endl;
    std::cout << std::endl << std::endl;
    banner(true, 2);
  }
}
