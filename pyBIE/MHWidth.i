%module(package="BIE") MHWidth
%{
#include "Serializable.h"
#include "MHWidth.h"
#include "MHWidthEns.h"
#include "MHWidthOne.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

%include "global.i"
%include "MHWidth.h"
%include "MHWidthEns.h"
%include "MHWidthOne.h"

%extend BIE::MHWidthOne
{
  static MHWidthOne* WithV(BIE::StateInfo *si, std::vector<double>* W)
  {
    return new MHWidthOne(si, W);
  }

  
  static MHWidthOne* WithF(BIE::StateInfo* si, std::string file)
  {
    return new MHWidthOne(si, file);
  }
};
    
%import "boost_pickle.i"

%boost_picklable(MHWidth)
%boost_picklable(MHWidthEns)
%boost_picklable(MHWidthOne)


%pythonbegin %{
"""
For specification of the Metropolis-Hastings transition probability.
"""
%}
