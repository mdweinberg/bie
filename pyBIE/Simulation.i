%module(package="BIE") Simulation

%{
#include "Serializable.h"
#include "Simulation.h"
#include "DifferentialEvolution.h"
#include "TemperedDifferentialEvolution.h"
#include "TemperedSimulation.h"
#include "RunSimulation.h"
#include "RunOneSimulation.h"
#include "PFSimulation.h"
#include "MultipleChains.h"
#include "ParallelChains.h"
#include "BlockSampler.h"
#include "SliceSampler.h"
#include "PosteriorProb.h"
#include "clivector.h"
#include "storage.H"
%}

%import(module="BIEutil") "ost.i"
%import(module="BIEutil") "Serializable.h"

%include "Simulation.h"
%include "global.i"
%include "PyVector.i"
%include "DifferentialEvolution.h"
%include "TemperedDifferentialEvolution.h"
%include "TemperedSimulation.h"
%include "RunSimulation.h"
%include "RunOneSimulation.h"
%include "PFSimulation.h"
%include "MultipleChains.h"
%include "ParallelChains.h"
%include "BlockSampler.h"
%include "SliceSampler.h"
%include "PosteriorProb.h"

// Factory constructors for TemperedSimulation
%extend BIE::TemperedSimulation
{
 static TemperedSimulation*
   WithD(BIE::StateInfo* si, int minmc_p, double maxT, BIE::MHWidth* width,
	 BIE::BaseDataTree* d, BIE::Model* m, BIE::Integration* i, BIE::Converge* c,
	 BIE::Prior* mp, BIE::LikelihoodComputation* l, BIE::MCAlgorithm* mca,
	 BIE::Simulation *last=0)
 {
   return new TemperedSimulation(si, minmc_p, maxT, width,
				 d, m, i, c, mp, l, mca, last);
 }

 static TemperedSimulation*
   WithL(BIE::StateInfo* si, int minmc_p, double maxT, BIE::MHWidth* width,
	 BIE::Converge* c, BIE::Prior* mp, BIE::LikelihoodComputation* l,
	 BIE::MCAlgorithm* mca, BIE::Simulation *last=0)
 {
   return new TemperedSimulation(si, minmc_p, maxT, width,
				 c, mp, l, mca, last);
 }
};

%extend BIE::DifferentialEvolution
{
  static DifferentialEvolution* WithSD
    (BIE::StateInfo* si, int number, std::string deinit,
     BIE::BaseDataTree* d, BIE::Model* m, BIE::Integration* i,
     BIE::Converge* c, BIE::Prior* mp, BIE::LikelihoodComputation* l,
     BIE::MCAlgorithm* mca, BIE::Simulation *last=NULL)
  {
    return new DifferentialEvolution(si, number, deinit, d, m, i, c, mp, l, mca, last);
  }

  static DifferentialEvolution* WithVD
    (BIE::StateInfo* si, int number, std::vector<BIE::Distribution*> v,
     BIE::BaseDataTree* d, BIE::Model* m, BIE::Integration* i,
     BIE::Converge* c, BIE::Prior* mp, BIE::LikelihoodComputation* l,
     BIE::MCAlgorithm* mca, BIE::Simulation *last=NULL)
  {
   return new DifferentialEvolution(si, number, v, d, m, i, c, mp, l, mca, last);
 }

  static DifferentialEvolution* WithSL
    (BIE::StateInfo* si, int number, std::string deinit,
     BIE::Converge* c, BIE::Prior* mp, BIE::LikelihoodComputation* l,
     BIE::MCAlgorithm* mca, BIE::Simulation *last=NULL)
  {
    return new DifferentialEvolution(si, number, deinit, c, mp, l, mca, last);
  }

  static DifferentialEvolution* WithVL
    (BIE::StateInfo* si, int number, std::vector<BIE::Distribution*> v,
     BIE::Converge* c, BIE::Prior* mp, BIE::LikelihoodComputation* l,
     BIE::MCAlgorithm* mca, BIE::Simulation *last=NULL)
  {
    return new DifferentialEvolution(si, number, v, c, mp, l, mca, last);
  }

};

%extend BIE::TemperedDifferentialEvolution
{
  static TemperedDifferentialEvolution* WithSD
    (BIE::StateInfo* si, int mchains, 
     std::string deinit, int minmc_p, double maxT,
     BIE::BaseDataTree* d, BIE::Model* m, BIE::Integration* i,
     BIE::Converge* c, BIE::Prior* mp,
     BIE::LikelihoodComputation* l, BIE::MCAlgorithm* mca,
     BIE::Simulation* last)
  {
    return new TemperedDifferentialEvolution(si, mchains, deinit, minmc_p, maxT, d, m, i, c, mp, l, mca, last);
  }

  static TemperedDifferentialEvolution* WithVD
    (BIE::StateInfo* si, int mchains, 
     int minmc_p, double maxT,
     std::vector<BIE::Distribution*> v, BIE::BaseDataTree* d,
     BIE::Model* m, BIE::Integration* i, BIE::Converge* c,
     BIE::Prior* mp, BIE::LikelihoodComputation* l,
     BIE::MCAlgorithm* mca, BIE::Simulation* last)
  {
    return new TemperedDifferentialEvolution(si, mchains, minmc_p, maxT, v, d, m, i, c, mp, l, mca, last);  
  }

  static TemperedDifferentialEvolution* WithSL
    (BIE::StateInfo* si, int mchains, 
     std::string deinit, int minmc_p, double maxT,
     BIE::Converge* c, BIE::Prior* mp,
     BIE::LikelihoodComputation* l, BIE::MCAlgorithm* mca,
     BIE::Simulation* last)
  {
    return new TemperedDifferentialEvolution(si, mchains, deinit, minmc_p, maxT, c, mp, l, mca, last); 
  }

  static TemperedDifferentialEvolution* WithVL
    (BIE::StateInfo* si, int mchains, 
     int minmc_p, double maxT,
     std::vector<BIE::Distribution*> v, BIE::Converge* c, BIE::Prior* mp,
     BIE::LikelihoodComputation* l, BIE::MCAlgorithm* mca,
     BIE::Simulation* last=NULL)
    
  {
    return new TemperedDifferentialEvolution(si, mchains, minmc_p, maxT, v, c, mp, l, mca, last);
  }
};

%extend BIE::PosteriorProb
{
  void RunVec(std::vector<double> vec)
  {
    $self->Run(vec);
  }

};


%import "boost_pickle.i"

%boost_picklable(Simulation)
%boost_picklable(DifferentialEvolution)
%boost_picklable(TemperedDifferentialEvolution)
%boost_picklable(TemperedSimulation)
%boost_picklable(RunSimulation)
%boost_picklable(RunOneSimulation)
%boost_picklable(PFSimulation)
%boost_picklable(MultipleChains)
%boost_picklable(ParallelChains)
%boost_picklable(BlockSampler)
%boost_picklable(SliceSampler)
%boost_picklable(PosteriorProb)

%pythonbegin %{
"""
MCMC simulation algorithms, including simulation drivers (e.g. RunSimulation and RunOneSimulation) and interactive test classes (e.g. PosteriorProb).
"""
%}
