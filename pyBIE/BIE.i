%module(package="BIE") BIE

%{
#define SWIG_FILE_WITH_INIT
%}

%pythonbegin %{
"""
These are the SWIG generated Python wrappers for the C++ BIE package

The BIE package is wrapped in 20 submodules described briefly below:

BIEutil:\tcontains MPI initialization and finalization routines as 
\t\twell as interfaces to the C++ global variable structure

Converge:\tthe MCMC convergence classes including Gelman-Rubin and
\t\tsimple counting, and boot-strap convergence for single-chain
\t\tsimulations

DataTree:\tcontainers for data various geometries to sort and
\t\tstore data by sky coordinates and internal attirbutes

Distribution:\ta collection of classes that generate, manipulate or 
\t\trepresent probability distributions

Ensemble:\ta particular probability distribution that accumulates
\t\tMCMC samples

Filter:\t\tfor manipulating input and output data streams

Integration:\tfor integrating three-dimensional models along the
\t\tobserved line of sight

Like:\t\tlikelihood methods and predefined likelihood functions

MarginalLikelihood: computes the marginal likelihood from an 
\t\tEnsemble instance produced by an MCMC simulation

MCAlgorithm:\tMonte Carlo algorithms for sampling the posterior 
\t\tdistribution

MHWidth:\tgenerates a Metropolis-Hastings transition probability 

Model:\t\tPredefined models for testing and production for various 
\t\tproblems

Prior:\t\tThe prior probability definition.  This is a Distribution 
\t\ttype.

PyVector:\tstd::vector wrappers for base data and BIE classes

Record:\t\tdata streams for populating internal data structures 
\t\tthat may be tessellated

Simulation:\tvarious MCMC simulation algorithms

State:\t\ta class that encapsulates the features and metadata for 
\t\tthe parameter vector

Tessellation:\ttessellation of the data by its spatial attributes

Galphat:\tGAlaxy PHotometric ATtributes

PopModel:\tA test class with spatial attributes

The Python command help (BIE.foo) will the classes and some documentation
for submodule foo.

The BIE library is an MPI library. If you make your Python script,
myscript.py, exectutable, you may start execution like this:

mpirun -np N mysript.py

where N is the number of processes. For many MPI implementations (e.g
openMPI), you will need to start with mpirun even if N=1.

Each script needs to "import BIE" and initialize BIE using the
BIE.BIEutil.init() command.

"""

# This module provides wrappers to the BIE library

# Use Python 3.x-style print
from __future__ import print_function
%}

%include "global.i"

%pythonbegin %{
#
# GLOBAL: Make symbols available for all objects.
#
# NOW: Do every dynamic linking resolutions during process loading, so
# that the dynamic linker does not need to be called for each request
# to an access to an external function.
#       
import sys, DLFCN
sys.setdlopenflags(DLFCN.RTLD_GLOBAL | DLFCN.RTLD_NOW)

import BIEutil
import Converge
import DataTree
import Distribution
import Ensemble
import Filter
import Integration
import Like
import MarginalLikelihood
import MCAlgorithm
import MHWidth
import Model
import Prior
import PyVector
import Record
import Simulation
import Tessellation
import State
import Galphat
import PopModel
%}
