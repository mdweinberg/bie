%{
#include "Serializable.h"
#include "storage.H"
%}

%include <std_string.i>

%define %boost_picklable(cls)
    %extend BIE:: ## cls {
        bool __getstate__()
        {
	  switch (BIE::arType) {
	  case BIE::BOOST_XML_ARCHIVE:
	    *BIE::ar_out_X << boost::serialization::make_nvp(#cls, ($self));
	    break;
	  case BIE::BOOST_BINARY_ARCHIVE:
	    *BIE::ar_out_B << ($self);
	    break;
	  case BIE::BOOST_TEXT_ARCHIVE:
	    *BIE::ar_out_T << ($self);
	    break;
	  }
	  return true;
        }

        cls* __deserialize()
        {
	  cls *ret;
	  switch (BIE::arType) {
	  case BIE::BOOST_XML_ARCHIVE:
	    *BIE::ar_in_X >> boost::serialization::make_nvp(#cls, ret);
	    break;
	  case BIE::BOOST_BINARY_ARCHIVE:
	    *BIE::ar_in_B >> ret;
	    break;
	  case BIE::BOOST_TEXT_ARCHIVE:
	    *BIE::ar_in_T >> ret;
	    break;
	  }
	  return ret;
        }

        %pythoncode %{
		def __setstate__(self, d):
			self.__init__()
			this = self.__deserialize()
			try:
				self.this.append(this)
			except Exception:
				self.this = this
        %}
    }
%enddef

%define %boost_picklable_generic(cls,name)
    %extend cls {
        bool __getstate__()
        {
	  switch (BIE::arType) {
	  case BIE::BOOST_XML_ARCHIVE:
	    *BIE::ar_out_X << boost::serialization::make_nvp(#name, ($self));
	    break;
	  case BIE::BOOST_BINARY_ARCHIVE:
	    *BIE::ar_out_B << ($self);
	    break;
	  case BIE::BOOST_TEXT_ARCHIVE:
	    *BIE::ar_out_T << ($self);
	    break;
	  }
	  return true;
        }

        cls* __deserialize()
        {
	  cls* ret;
	  switch (BIE::arType) {
	  case BIE::BOOST_XML_ARCHIVE:
	    *BIE::ar_in_X >> boost::serialization::make_nvp(#name, ret);
	    break;
	  case BIE::BOOST_BINARY_ARCHIVE:
	    *BIE::ar_in_B >> ret;
	    break;
	  case BIE::BOOST_TEXT_ARCHIVE:
	    *BIE::ar_in_T >> ret;
	    break;
	  }
	  return ret;
        }

        %pythoncode %{
		def __setstate__(self, d):
			self.__init__()
			this = self.__deserialize()
			try:
				self.this.append(this)
			except Exception:
				self.this = this
        %}
    }
%enddef
