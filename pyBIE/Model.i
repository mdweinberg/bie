%module(package="BIE") Model

%{
#include "storage.H"
#include "Model.h"
#include "SplatModel.h"
#include "GalaxyModelND.h"
#include "GalaxyModelOneD.h"
#include "GalaxyModelOneDHashed.h"
#include "GalaxyModelTwoD.h"
#include "GalaxyModelNDCached.h"
#include "GalaxyModelOneDCached.h"
#include "GalaxyModelTwoDCached.h"
#include "MultiDSplat.h"
#include "NullModel.h"
#include "SimpleGalaxyModel.h"
#include "SplatModel1d.h"
#include "SplatModel3dv.h"
#include "SplatModel3.h"
#include "SplatModel.h"
#include "SplatModelNdv.h"
#include "Uniform.h"
#include "Normal.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

// This is used internally by the likelihood computation.  Probably
// okay to ignore.
%ignore NormEval;

%include "global.i"
%include "Model.h"
%include "SplatModel.h"
%include "GalaxyModelND.h"
%include "GalaxyModelOneD.h"
%include "GalaxyModelOneDHashed.h"
%include "GalaxyModelTwoD.h"
%include "GalaxyModelNDCached.h"
%include "GalaxyModelOneDCached.h"
%include "GalaxyModelTwoDCached.h"
%include "MultiDSplat.h"
%include "NullModel.h"
%include "SimpleGalaxyModel.h"
%include "SplatModel1d.h"
%include "SplatModel3dv.h"
%include "SplatModel3.h"
%include "SplatModel.h"
%include "SplatModelNdv.h"

%import "boost_pickle.i"

%boost_picklable(Model)
%boost_picklable(SplatModel)
%boost_picklable(GalaxyModelND)
%boost_picklable(GalaxyModelOneD)
%boost_picklable(GalaxyModelOneDHashed)
%boost_picklable(GalaxyModelTwoD)
%boost_picklable(GalaxyModelNDCached)
%boost_picklable(GalaxyModelOneDCached)
%boost_picklable(GalaxyModelTwoDCached)
%boost_picklable(MultiDSplat)
%boost_picklable(NullModel)
%boost_picklable(SimpleGalaxyModel)
%boost_picklable(SplatModel1d)
%boost_picklable(SplatModel3dv)
%boost_picklable(SplatModel3)
%boost_picklable(SplatModel)
%boost_picklable(SplatModelNdv)

%pythonbegin %{
"""
Definitions of test and production models for various statistical and astronomical processes.
"""
%}
