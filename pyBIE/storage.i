%{
#include  "storage.H"
%}

void openInArchive  (std::string archive_name);
void openOutArchive (std::string archive_name);
void closeInArchive ();
void closeOutArchive();

void StoreBIEACG    ();
void RetrieveBIEACG ();
void PrintBIEACG    (std::string suffix);
