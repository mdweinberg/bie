%module(package="BIE") Integration

%{
#include "Integration.h"
#include "LegeIntegration.h"
#include "AdaptiveIntegration.h"
#include "AdaptiveLegeIntegration.h"
#include "FivePointIntegration.h"
#include "PointIntegration.h"
#include "QuadTreeIntegrator.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

%include "global.i"
%include "Integration.h"
%include "LegeIntegration.h"
%include "AdaptiveIntegration.h"
%include "AdaptiveLegeIntegration.h"
%include "FivePointIntegration.h"
%include "PointIntegration.h"
%include "QuadTreeIntegrator.h"

%import "boost_pickle.i"

%boost_picklable(Integration)
%boost_picklable(LegeIntegration)
%boost_picklable(AdaptiveLegeIntegration)
%boost_picklable(FivePointIntegration)
%boost_picklable(PointIntegration)


%pythonbegin %{
"""
Perform two dimensional quadrature of a model for comparison with data in a Tesselation cell. 
"""
%}
