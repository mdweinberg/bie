%module(package="BIE") PyVector

%{
#include <Normal.h>
#include "Serializable.h"
#include "Distribution.h"
#include "Tessellation.h"
#include "Simulation.h"
#include "LikelihoodComputation.h"
#include "ReversibleJumpTwoModel.h"
#include "storage.H"
%}

%import(module="BIEutil")      "Serializable.h"
%import(module="Tessellation") "Tessellation.h"
%import(module="Distribution") "Distribution.h"
%import(module="MCAlgorithm")  "MCAlgorithm.h"
%import(module="Simulation")   "Simulation.h" 

%include <std_container.i>
%include <std_vector.i>

%include "vectors.i"

class Distribution;
class BinnedDistribution;
class Tessellation;
class Simulation;
class RJMapping;

namespace std {
  %template(VectorTess)     vector<BIE::Tessellation*>;
  %template(VectorDist)     vector<BIE::Distribution*>;
  %template(VectorBinDist)  vector<BIE::BinnedDistribution*>;
  %template(VectorSim)      vector<BIE::Simulation*>;
  %template(VectorRJMap)    vector<BIE::RJMapping*>;
};

%import "boost_pickle.i"

%boost_picklable_generic(std::vector<double>,                   VectorDouble)
%boost_picklable_generic(std::vector< std::vector<double> >,    VectorVectorDouble)
%boost_picklable_generic(std::vector< std::string >,            VectorString)
%boost_picklable_generic(std::vector<int>,                      VectorInt)
%boost_picklable_generic(std::vector<bool>,                     VectorBool)
%boost_picklable_generic(std::vector<BIE::Tessellation*>,       VectorTess)
%boost_picklable_generic(std::vector<BIE::Distribution*>,       VectorDist)
%boost_picklable_generic(std::vector<BIE::BinnedDistribution*>, VectorBinDist)
%boost_picklable_generic(std::vector<BIE::Simulation*>,         VectorSimu)
%boost_picklable_generic(std::vector<BIE::RJMapping*>,          VectorRJMap)

%pythonbegin %{
"""
Wrapping of std::vector types containing basic data types and BIE classes.
"""
%}
