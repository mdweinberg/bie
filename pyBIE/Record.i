%module(package="BIE") Record

%{
#include "Serializable.h"
#include "RecordType.h"
#include "RecordBuffer.h"
#include "RecordStream.h"
#include "RecordInputStream.h"
#include "RecordOutputStream.h"
#include "RecordStream_Ascii.h"
#include "RecordStream_NetCDF.h"
#include "RecordStream_Binary.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

%ignore BIE::RecordBuffer::selectFields;
%ignore BIE::RecordType::selectFields;
%ignore BIE::RecordInputStream::selectFields;
%ignore BIE::RecordOutputStream::selectFields;

%include "vectors.i"
%include "BasicType.h"
%include "RecordType.h"
%include "RecordBuffer.h"
%include "RecordStream.h"
%include "RecordInputStream.h"
%include "RecordOutputStream.h"
%include "RecordStream_Ascii.h"
%include "RecordStream_NetCDF.h"
%include "RecordStream_Binary.h"

%extend BIE::RecordInputStream_Ascii
{
  static RecordInputStream_Ascii* WithRI(BIE::RecordType * type, std::istream * asciistream)
  {
    return new RecordInputStream_Ascii(type, asciistream);
  }

    
  static RecordInputStream_Ascii* WithRS(BIE::RecordType * type, std::string filename)
  {
    return new RecordInputStream_Ascii(type, filename);
  }


  static RecordInputStream_Ascii* WithI(std::istream * asciistream)
  {
    return new RecordInputStream_Ascii(asciistream);
  }

  static RecordInputStream_Ascii* WithS(std::string filename)
  {
    return new RecordInputStream_Ascii(filename);
  }

  static BIE::RecordInputStream*
    BIE::RecordInputStream_Ascii::Base(BIE::RecordInputStream_Ascii* p)
  { return (RecordInputStream*)p; }
}

%extend BIE::RecordBuffer {

  RecordBuffer* selectFields(RecordType * selection)
  {
    return $self->selectFields(selection);
  }

  RecordBuffer* selectFields_int(std::vector<int> * selection)
  {
    return $self->selectFields(selection);
  }

  RecordBuffer* selectFields_string(std::vector<std::string> * selection)
  {
    return $self->selectFields(selection);
  }
 }

%extend BIE::RecordType {

  RecordType* selectFields_int(std::vector<int> * selection)
  {
    return $self->selectFields(selection);
  }

  RecordType* selectFields_string(std::vector<std::string> * selection)
  {
    return $self->selectFields(selection);
  }
 }

%extend BIE::RecordInputStream {

  RecordInputStream* selectFields(RecordType * selection)
  {
    return $self->selectFields(selection);
  }

  RecordInputStream* selectFields_int(std::vector<int> * selection)
  {
    return $self->selectFields(selection);
  }

  RecordInputStream* selectFields_string(std::vector<std::string> * selection)
  {
    return $self->selectFields(selection);
  }
}

%extend BIE::RecordOutputStream {

  RecordOutputStream* selectFields(RecordType * selection)
  {
    return $self->selectFields(selection);
  }

  RecordOutputStream* selectFields_int(std::vector<int> * selection)
  {
    return $self->selectFields(selection);
  }

  RecordOutputStream* selectFields_string(std::vector<std::string> * selection)
  {
    return $self->selectFields(selection);
  }
}

%import "boost_pickle.i"

%boost_picklable(BasicType)
%boost_picklable(RecordType)
%boost_picklable(RecordBuffer)
%boost_picklable(RecordStream)
%boost_picklable(RecordInputStream)
%boost_picklable(RecordOutputStream)
%boost_picklable(RecordInputStream_Ascii)
%boost_picklable(RecordOutputStream_Ascii)
%boost_picklable(RecordInputStream_NetCDF)
%boost_picklable(RecordOutputStream_NetCDF)
%boost_picklable(RecordInputStream_Binary)
%boost_picklable(RecordOutputStream_Binary)

%pythonbegin %{
"""
These classes support input and output native BIE data streams in various formats.  See also: the Filter module.
"""
%}
