#!/usr/bin/python

#============================================================
#
# Example: one-dimensional PARALLEL CHAINS run
#
#============================================================

import BIE

#============================================================
# Initialize mpi environment
#============================================================

BIE.BIEutil.init()

# Use these lines to specify the random number seed
# seed = 11
# BIE.BIEutil.init(seed)

#============================================================
# Logfile name
#============================================================

BIE.cvar.nametag = "run8p12"
BIE.cvar.outfile = "run8p12.statelog"

#============================================================
# Model dimension
#============================================================

ndim = 1

#============================================================
# Number of mixture components
#============================================================

nmix = 2

#============================================================
# State metadata
#============================================================

labs = BIE.PyVector.VectorString(1, "Pos(x)")
si = BIE.State.StateInfo(nmix, ndim)
si.labelMixture(labs)

#============================================================
# Minimum number of temperature levels
#============================================================

minmc = 16

#============================================================
# Maximum number of steps
#============================================================

nsteps = 2000

#============================================================
# Proposal function scaling factor
#============================================================

width = 0.01

#============================================================
# Maximum temperature (T=1 means no heating at all)
#============================================================

maxT = 16.0

#============================================================
# Prior function
#============================================================

unif  = BIE.Distribution.UniformDist(-0.2, 1.0)
dist  = BIE.PyVector.VectorDist(1, unif)
prior = BIE.Prior.InitialMixturePrior.WithAD(si, 1.0, dist)

#============================================================
# Define the M-H proposal function
#============================================================

mca     = BIE.MCAlgorithm.MetropolisHastings()
mvec    = BIE.PyVector.VectorDouble(2, 0.001)
mhwidth = BIE.MHWidth.MHWidthOne.WithV(si, mvec)

#============================================================
# Define the simulation
#============================================================

use_disc  = False

if use_disc:
    sstat = BIE.Ensemble.EnsembleDisc(si)
else:
    sstat = BIE.Ensemble.EnsembleStat(si)

sstat.setVerboseOn()

convrg    = BIE.Converge.SubsampleConverge(nsteps, sstat, "run8.0")
like      = BIE.Like.LikelihoodComputationSerial()
sim       = BIE.Simulation.ParallelChains(si, minmc, maxT, mhwidth, convrg, prior, like, mca)
#   sim.SetAlgorithm(1)

#============================================================
# Define the model through the user-defined likelihood
#============================================================

fct = BIE.Like.GaussTestLikelihoodFunctionMulti(9, 10000, 6)
fct.SetDim(1)
sim.SetUserLikelihood(fct)

#============================================================
# Run the simulation
#============================================================

run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()

#============================================================
# Serialize
#============================================================

BIE.psave('test8', 'This is the first stage of the script8 persistence test')
BIE.BIEutil.quit()
#============================================================
