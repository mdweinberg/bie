#!/usr/bin/python

#============================================================
#
# Example: one-dimensional differential evolution run
#
#============================================================

import BIE

#============================================================
# Initialize mpi environment
#============================================================

BIE.BIEutil.init()

#============================================================
# Logfile name
#============================================================

BIE.cvar.nametag = "run10"
BIE.cvar.outfile = "run10.statelog"

#============================================================
# Number of dimensions (position and variance)
#============================================================

ndim = 1

#============================================================
# Number of mixture components
#============================================================

nmix = 2


#============================================================
# State metadata
#============================================================

labs = BIE.PyVector.VectorString(1)
labs[0] =  "Pos"
si = BIE.State.StateInfo(nmix, ndim)
si.setOrdering(0)
si.labelMixture(labs)

#============================================================
# DE randomization
#============================================================

uc1 = BIE.Distribution.CauchyDist(0.001)
uc2 = BIE.Distribution.CauchyDist(0.002)
eps = BIE.PyVector.VectorDist(2, uc1)
eps[1] = uc2

#============================================================
# Minimum number of chains
#============================================================

mchains = 32

#============================================================
# Number of steps
#============================================================

nsteps = 40000

#============================================================
# Width scaling
#============================================================

width = 0.1

#============================================================
# Mixture prior
#============================================================

use_disc = False

unif   = BIE.Distribution.UniformDist(-0.2, 1.2)
dist   = BIE.PyVector.VectorDist(1, unif)

prior0 = BIE.Prior.InitialMixturePrior.WithAD(si, 1.0, dist)

if use_disc:
    stat0 = BIE.Ensemble.EnsembleDisc(si)
else:
    stat0 = BIE.Ensemble.EnsembleStat(si)

#============================================================
# Simply count states
#============================================================

convrg0 = BIE.Converge.GelmanRubinConverge(500, stat0, "run9.0")
convrg0.setAlpha(0.05)
convrg0.setNskip(500)
convrg0.setNoutlier(500)
convrg0.setPoffset(-30.0)
convrg0.setMaxout(6)

#============================================================
# Metropolis-Hastings Monte Carlo algorithm
#============================================================

mca = BIE.MCAlgorithm.MetropolisHastings()

#============================================================
# Differential evolution
#============================================================

like  = BIE.Like.LikelihoodComputationSerial()
sim0  = BIE.Simulation.DifferentialEvolution.WithVL(si, mchains, eps, convrg0, prior0, like, mca)
sim0.EnableLogging()
sim0.SetLinearMapping(True)
sim0.SetJumpFreq(10)
#   sim.SetControl(True)
sim0.NewGamma(0.05)

#============================================================
# 1-d Gaussian model
#============================================================

fct = BIE.Like.GaussTestLikelihoodFunction()
fct.SetDim(ndim)
sim0.SetUserLikelihood(fct)

#============================================================
# Run the simulation
#============================================================

BIE.cvar.current_level = 0
run = BIE.Simulation.RunOneSimulation(nsteps, width, stat0, prior0, sim0)
run.Run()

BIE.PrintBIEACG('bietest.0')

#============================================================
# Compute distribribution for Level 0 and BIE.prior for Level 1
#============================================================

discard = convrg0.ConvergedIndex()
print("converged state is {}".format(discard))
stat0.ComputeDistribution(discard)
stat0.printWidth()

prior1  = BIE.Prior.PostMixturePrior(prior0, stat0)

if use_disc:
    stat1 = BIE.Ensemble.EnsembleDisc(si)
else:
    stat1 = BIE.Ensemble.EnsembleStat(si)

convrg1 = BIE.Converge.GelmanRubinConverge(500, stat1, "run9.1")

#============================================================
# BIE.simulation
#============================================================

sim1 = BIE.Simulation.DifferentialEvolution.WithDL(si, mchains, eps, convrg1, prior1, like, mca, sim0)
sim1.SetLinearMapping(True)
sim1.SetJumpFreq(10)
sim1.SetControl(True)
sim1.NewGamma(0.05)

#============================================================
# The multiresolution simulation driver
#============================================================

BIE.cvar.current_level = 1
width = 0.01
run = BIE.Simulation.RunOneSimulation(nsteps, width, stat1, prior1, sim1)

BIE.PrintBIEACG('bietest.1')

run.Run()
#============================================================
# Compute distribribution and BIE.prior
#============================================================
discard = convrg1.ConvergedIndex()
print("converged state is {}".format(discard))
stat1.ComputeDistribution(discard)
stat1.printWidth()

prior2  = BIE.Prior.PostMixturePrior(prior1, stat1)

if use_disc:
    stat2 = BIE.Ensemble.EnsembleDisc(si)
else:
    stat2 = BIE.Ensemble.EnsembleStat(si)

convrg2 = BIE.Converge.GelmanRubinConverge(500, stat2, "run9.2")

#============================================================
# BIE.simulation
#============================================================

sim2 = BIE.Simulation.DifferentialEvolution.WithDL(si, mchains, eps, convrg2, prior2, like, mca, sim1)
sim2.EnableLogging()
sim2.SetLinearMapping(True)
sim2.SetJumpFreq(10)
sim2.SetControl(True)
sim2.NewGamma(0.05)

#============================================================
# The multiresolution simulation driver
#============================================================

BIE.cvar.current_level = 2
width = 0.01
run = BIE.Simulation.RunOneSimulation(nsteps, width, stat2, prior2, sim2)
run.Run()


BIE.BIEutil.quit()
