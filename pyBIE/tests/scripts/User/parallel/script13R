#!/usr/bin/python

#============================================================
#
# Example: two-dimensional tempered differential evolution run
# Restart from log file
#
#============================================================

import BIE

#============================================================
# Initialize mpi environment
#============================================================

BIE.BIEutil.init()

#============================================================
# Tag for output files
#============================================================

BIE.cvar.nametag = "run13R"

#============================================================
# Logfile name
#============================================================

BIE.cvar.outfile = "run13R.statelog"

#============================================================
# Number of dimensions
#============================================================
#
ndim = 2
#
#============================================================
# DE randomization
#============================================================
#
uc = BIE.Distribution.CauchyDist(0.001)
eps = BIE.PyVector.VectorDist(2, uc)
#
#============================================================
# State metadata
#============================================================
#
labs = BIE.PyVector.VectorString(2)
labs[0] = "Pos1"
labs[1] = "Pos2"
si = BIE.State.StateInfo(ndim)
si.labelAll(labs)
#
#============================================================
# Minimum number of chains
#============================================================
#
mchains = 32
#
#============================================================
# Number of steps
#============================================================
#
nsteps = 40000
#
#============================================================
# Width scaling
#============================================================
#
width = 0.1
#
#============================================================
# Prior
#============================================================
#
unif = BIE.Distribution.UniformDist(-1.0, 2.0)
dist = BIE.PyVector.VectorDist(2, unif)
iprior = BIE.Prior.si, dist)
istat = BIE.Ensemble.EnsembleStat(si, 0, 20000, "run13.statelog", 0)
prior = BIE.Prior.PostPrior(iprior, istat)
sstat = BIE.Ensemble.EnsembleDisc(si)
#
#============================================================
# Gelman-Ruben convergence scheme
#============================================================
#
convrg = BIE.Converge.GelmanRubinConverge(500, sstat, "run9.0")
convrg.setAlpha(0.05)
convrg.setNskip(500)
convrg.setNoutlier(500)
convrg.setPoffset(-30.0)
convrg.setMaxout(6)
#
#============================================================
# Metropolis-Hastings Monte Carlo algorithm
#============================================================
#
mca = BIE.MCAlgorithm.MetropolisHastings()
#
#============================================================
# Differential evolution
#============================================================
#
minmc = 6
maxT = 32
like = BIE.Like.LikelihoodComputationSerial()
sim = BIE.Simulation.TemperedDifferentialEvolution.WithVD_Less(si, mchains, minmc, maxT, eps, convrg, prior, like, mca)
sim.SetLinearMapping(True)
sim.SetJumpFreq(10)
sim.SetControl(True)
sim.NewGamma(0.05)
#
#============================================================
# 1-d Gaussian model
#============================================================
#
fct = BIE.Like.GaussTestLikelihoodFunction()
fct.SetDim(ndim)
sim.SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
current_level = 0
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()

BIE.BIEutil.quit()