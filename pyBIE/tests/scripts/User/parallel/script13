#!/usr/bin/python

#============================================================
#
# Example: three-dimensional tempered evolution run
#
#============================================================

import BIE

#============================================================
# Initialize mpi environment
#============================================================

BIE.BIEutil.init()

#============================================================
# Tag for output files
#============================================================

BIE.cvar.nametag = "run13"

#============================================================
# Logfile name
#============================================================

BIE.cvar.outfile = "run13.statelog"

#============================================================
# Number of dimensions
#============================================================
#
ndim = 3
#
#============================================================
# DE randomization
#============================================================
#
uc = BIE.Distribution.CauchyDist(0.001)
eps = BIE.PyVector.VectorDist(3, uc)
#
#============================================================
# State metadata
#============================================================
#
labs = BIE.PyVector.VectorString(3)
labs[0] = "Pos1"
labs[1] = "Pos2"
labs[2] = "Pos3"
si = BIE.State.StateInfo(ndim)
si.labelAll(labs)
#
#============================================================
# Minimum number of chains
#============================================================
#
mchains = 32
#
#============================================================
# Number of steps
#============================================================
#
nsteps = 4000
#
#============================================================
# Width scaling
#============================================================
#
width = 0.1
#
#============================================================
# Prior
#============================================================
#
unif = BIE.Distribution.UniformDist(-1.0, 2.0)
dist = BIE.PyVector.VectorDist(3, unif)
prior = BIE.Prior.si, dist)
sstat = BIE.Ensemble.EnsembleDisc(si)
#
#============================================================
# Gelman-Ruben convergence scheme
#============================================================
#
convrg = BIE.Converge.GelmanRubinConverge(500, sstat, "run7")
convrg.setAlpha(0.05)
convrg.setNskip(500)
convrg.setNoutlier(500)
convrg.setPoffset(-30.0)
convrg.setMaxout(6)
#
#============================================================
# Metropolis-Hastings Monte Carlo algorithm
#============================================================
#
mca = BIE.MCAlgorithm.MetropolisHastings()
#
#============================================================
# Differential evolution
#============================================================
#
minmc = 6
maxT = 32
like = BIE.Like.LikelihoodComputationSerial()
sim = BIE.Simulation.TemperedDifferentialEvolution.WithVD_Less(si, mchains, minmc, maxT, eps, convrg, prior, like, mca)
sim.SetLinearMapping(True)
sim.SetJumpFreq(10)
sim.SetControl(True)
sim.NewGamma(0.05)
#
#============================================================
# 1-d Gaussian model
#============================================================
#
fct = BIE.Like.GaussTestLikelihoodFunction()
fct.SetDim(ndim)
sim.SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
current_level = 0
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()
#
#============================================================
# Compute distribribution and BIE.prior
#============================================================
#
discard = convrg.ConvergedIndex()
print discard
sstat.ComputeDistribution(discard)
sstat.printWidth()
nprior = BIE.Prior.PostPrior(prior, sstat)
nstat = BIE.Ensemble.EnsembleDisc(si)
convrg = BIE.Converge.GelmanRubinConverge(500, nstat, "run9.1")
#============================================================
# BIE.simulation
#============================================================
#
nsim = BIE.Simulation.TemperedDifferentialEvolution.WithVD_Less(si, mchains, minmc, maxT, eps, convrg, nprior, like, mca, sim)
nsim.SetLinearMapping(True)
nsim.SetJumpFreq(10)
nsim.SetControl(True)
nsim.NewGamma(0.05)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
current_level = 1
width = 0.01
run = BIE.Simulation.RunOneSimulation(nsteps, width, nstat, nprior, nsim)
run.Run()
#============================================================
# Compute distribribution and BIE.prior
#============================================================
discard = convrg.ConvergedIndex()
print discard
nstat.ComputeDistribution(discard)
nstat.printWidth()
prior = BIE.Prior.PostPrior(prior, nstat)
sstat = BIE.Ensemble.EnsembleDisc(si)
convrg = BIE.Converge.GelmanRubinConverge(500, sstat, "run9.2")
#
#============================================================
# BIE.simulation
#============================================================
#
sim = BIE.Simulation.TemperedDifferentialEvolution.WithVD_Less(si, mchains, minmc, maxT, eps, convrg, prior, like, mca, nsim)
sim.EnableLogging()
sim.SetLinearMapping(True)
sim.SetJumpFreq(10)
sim.SetControl(True)
sim.NewGamma(0.05)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
current_level = 2
width = 0.01
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()

BIE.BIEutil.quit()