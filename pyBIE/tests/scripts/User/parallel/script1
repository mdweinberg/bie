#!/usr/bin/python

#============================================================
#
# Example: three-dimensional Slice Sampler run
#
#============================================================


import BIE
BIE.BIEutil.init()
BIE.cvar.mpi_used = True

#============================================================
# Number of dimensions
#============================================================
#
ndim = 2
#
#============================================================
# State metadata
#============================================================
#
labs = BIE.PyVector.VectorString(2)
labs[0] = "Pos 1"
labs[1] = "Pos 2"
#labs.showall()
si = BIE.State.StateInfo(ndim)
si.labelAll(labs)
#
#
#============================================================
# Number of steps
#============================================================
#
nsteps = 10000
#
#============================================================
# Width scaling
#============================================================
#
width = 0.1
#
#============================================================
# Prior
#============================================================
#
unif = BIE.Distribution.UniformDist(-1.0, 2.0)
dist = BIE.PyVector.VectorDist(2, unif)
prior = BIE.Prior.Prior(si, dist)
sstat = BIE.Ensemble.EnsembleDisc(si)
#
#============================================================
# Gelman-Ruben convergence scheme
#============================================================
#
convrg = BIE.Converge.GelmanRubinConverge(500, sstat, "run1")
convrg.setAlpha(0.05)
convrg.setNskip(500)
convrg.setNoutlier(500)
convrg.setPoffset(-30.0)
convrg.setMaxout(6)
#
#============================================================
# Monte Carlo algorithm
#============================================================
#
mca = BIE.MCAlgorithm.StandardMC()
#
#============================================================
# Simple simulation
#============================================================
#
ninit = 1000
wfac  = 0.05
like = BIE.Like.LikelihoodComputationSerial()
sim = BIE.Simulation.SliceSampler(ninit, wfac, si, convrg, prior, like, mca)
#
#============================================================
# 1-d Gaussian model
#============================================================
#
fct = BIE.Like.GaussTestLikelihoodFunction()
fct.SetDim(1)
fct.useNcomp()
sim.SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
current_level = 0
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()
BIE.BIEutil.quit()

#
#============================================================
# Done
#============================================================
