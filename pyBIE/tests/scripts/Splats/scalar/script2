#!/usr/bin/python

import BIE

#------------------------------------------------------------------------
# Initialize mpi environment
#------------------------------------------------------------------------

BIE.BIEutil.init()

#------------------------------------------------------------------------
# Prefix for diagnostic output files
#------------------------------------------------------------------------

BIE.cvar.nametag = "run2"

#------------------------------------------------------------------------
# The chain output file
#------------------------------------------------------------------------

BIE.cvar.outfile = "run2.statelog"
#
# Simple multilevel test
#

ndim      = 2
nmix      = 2
tessdepth = 6
minmc     = 6
dxy       = 0.1
nsteps    = 40
width     = 0.1
maxT      = 1024.0
labs      = BIE.PyVector.VectorString(2)
labs[0] = "Pos(x)"
labs[1] = "Pos(y)"

si        = BIE.State.StateInfo(nmix, ndim)
si.labelMixture(labs)

mod       = BIE.Model.SplatModel(ndim,nmix)
hist      = BIE.Distribution.Histogram1D(0.0,10.0,10.0,"attribute")
unif      = BIE.Distribution.UniformDist(-0.5, 1.5)
pvec      = BIE.PyVector.VectorDist(2, unif)
prior0    = BIE.Prior.InitialMixturePrior.WithAD(si, 1.0, pvec)
stat0     = BIE.Ensemble.EnsembleStat(si)
tile      = BIE.Tessellation.SquareTile()
intgr     = BIE.Integration.AdaptiveLegeIntegration(dxy, dxy)
convrg0   = BIE.Converge.CountConverge(nsteps, stat0)
tess      = BIE.Tessellation.QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, tessdepth)
ris       = BIE.Record.RecordInputStream_Ascii("../../../../../scripts/Splats/data/splat.data.1");
dis       = BIE.DataTree.DataTree(ris,hist,tess)
like      = BIE.Like.LikelihoodComputationSerial()

mca       = BIE.MCAlgorithm.MetropolisHastings()
mvec      = BIE.PyVector.VectorDouble(3, 0.01)
mvec[0] = 0.002

mhwidth   = BIE.MHWidth.MHWidthOne.WithV(si, mvec)
sim0      = BIE.Simulation.TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg0, prior0, like, mca)
frontier  = dis.GetDefaultFrontier()
frontier.UpDownLevels(2)
run       = BIE.Simulation.RunOneSimulation(nsteps, width, stat0, prior0, sim0)
run.Run()
#
# Level 1
#
stat0.ComputeDistribution()

prior1    = BIE.Prior.PostMixturePrior(prior0, stat0)
stat1     = BIE.Ensemble.EnsembleStat(si)
convrg1   = BIE.Converge.CountConverge(nsteps, stat1)

BIE.cvar.current_level = 1

frontier  = dis.GetDefaultFrontier()
frontier.UpDownLevels(1)
sim1      = BIE.Simulation.TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg1, prior1, like, mca, sim0)
width     = 0.01
run       = BIE.Simulation.RunOneSimulation(nsteps, width, stat1, prior1, sim1)
run.Run()
#
# Level 2
#
stat1.ComputeDistribution()
prior2    = BIE.Prior.PostMixturePrior(prior1, stat1)
stat2     = BIE.Ensemble.EnsembleStat(si)
convrg2   = BIE.Converge.CountConverge(nsteps, stat2)

BIE.cvar.current_level = 2

frontier  = dis.GetDefaultFrontier()
frontier.UpDownLevels(1)

sim2      = BIE.Simulation.TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg2, prior2, like, mca, sim1)
width     = 0.01
run       = BIE.Simulation.RunOneSimulation(nsteps, width, stat2, prior2, sim2)
run.Run()
#
#------------------------------------------------------------------------
BIE.BIEutil.quit()
print("DONE")
