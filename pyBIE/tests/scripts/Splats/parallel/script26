#!/usr/bin/python

import BIE

#------------------------------------------------------------------------
# Initialize mpi environment
#------------------------------------------------------------------------

BIE.BIEutil.init()

#------------------------------------------------------------------------
# Prefix for diagnostic output files
#------------------------------------------------------------------------

BIE.cvar.nametag = "run26"

#------------------------------------------------------------------------
# The chain output file
#------------------------------------------------------------------------

BIE.cvar.outfile = "run26.statelog"

#
#----------------------------------------------------------------------
# Model parameters
#----------------------------------------------------------------------
#
ndim = 2
nmix = 2
si   = BIE.State.StateInfo(nmix, ndim)
labs = BIE.PyVector.VectorString(2)
labs[0] = "Pos(x)"
labs[1] = "Pos(y)"
si.labelMixture(labs)		
#
#----------------------------------------------------------------------
# Algorithm parameters
#----------------------------------------------------------------------
tessdepth = 8
nchains   = 16
dxy0      = 0.05
dxy       = 0.01
delta     = 0.01
nsteps    = 4000
width     = 0.1
#
#----------------------------------------------------------------------
# The model
#----------------------------------------------------------------------
mod = BIE.Model.SplatModel(ndim, nmix)
#----------------------------------------------------------------------
# Data structure
#----------------------------------------------------------------------
hist = BIE.Distribution.Histogram1D(0.0,10.0,10.0,"attribute")
#----------------------------------------------------------------------
# The prior distribution
#----------------------------------------------------------------------
unif = BIE.Distribution.UniformDist(0.0, 1.0)
pvec = BIE.PyVector.VectorDist(ndim, unif)
prior = BIE.Prior.InitialMixturePrior.WithAD(si, 1.0, pvec)
si.setOrdering(0)
#----------------------------------------------------------------------
# Posterior sample characterization
#----------------------------------------------------------------------
sstat = BIE.Ensemble.EnsembleDisc(si)
#----------------------------------------------------------------------
# Posterior convergence
#----------------------------------------------------------------------
convrg = BIE.Converge.GelmanRubinConverge(100, sstat, "")
convrg.setNgood(10)
#----------------------------------------------------------------------
# Data tessellation
#----------------------------------------------------------------------
tile = BIE.Tessellation.SquareTile()
# intgr = BIE.Integration.AdaptiveLegeIntegration(dxy, dxy)
intgr = BIE.Integration.AdaptiveIntegration(delta, dxy0, dxy0, dxy, dxy)
tess = BIE.Tessellation.QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, tessdepth)
ris = BIE.Record.RecordInputStream_Ascii("../data/splat.data.1")
dis = BIE.DataTree.DataTree(ris,hist,tess)
#----------------------------------------------------------------------
# The likelihood and MC algorithm
#----------------------------------------------------------------------
# like = BIE.Like.LikelihoodComputationMPI(si)
like = BIE.Like.LikelihoodComputationSerial()
mca = BIE.MCAlgorithm.MetropolisHastings()
#----------------------------------------------------------------------
# The frontier must be _before_ creating the simulation
#----------------------------------------------------------------------
frontier = dis.GetDefaultFrontier()
frontier.UpDownLevels(4)

#============================================================
# DE randomization
#============================================================

uc1 = BIE.Distribution.CauchyDist(0.001)
uc2 = BIE.Distribution.CauchyDist(0.002)
eps = BIE.PyVector.VectorDist(3, uc2)
eps[0] = uc1

#----------------------------------------------------------------------
# Create the simulation
#----------------------------------------------------------------------
sim = BIE.Simulation.DifferentialEvolution.WithVD(si, nchains, eps, dis, mod, intgr, convrg, prior, like, mca)
# sim.EnableLogging()
sim.SetLinearMapping(True)
sim.SetJumpFreq(10)
sim.SetControl(True)
#----------------------------------------------------------------------
# Create the simulation controller and start
#----------------------------------------------------------------------
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
BIE.cvar.current_level = 0
run.Run()
psave
#----------------------------------------------------------------------
# Compute distribribution for Level 0 and BIE.prior for Level 1
#----------------------------------------------------------------------
sstat.ComputeDistribution()
#----------------------------------------------------------------------
# Create the prior from old posterior, BIE.ensemble, convergence test
#----------------------------------------------------------------------
nprior = BIE.Prior.PostMixturePrior(prior, sstat)
nstat = BIE.Ensemble.EnsembleDisc(si)
convrg = BIE.Converge.SubsampleConverge(nsteps, nstat, "1")
#----------------------------------------------------------------------
# Increase the resolution by one level
#----------------------------------------------------------------------
current_level = 1
frontier.UpDownLevels(1)
#----------------------------------------------------------------------
# Create a BIE.simulation, a simulation controller and start again
#----------------------------------------------------------------------
nsim = BIE.Simulation.DifferentialEvolution.WithVD(si, nchains, eps, dis, mod, intgr, convrg, nprior, like, mca, sim)
width = 0.01
nsim.SetControl(True)
run = BIE.Simulation.RunOneSimulation(nsteps, width, nstat, nprior, nsim)
run.Run()
psave
#----------------------------------------------------------------------
# Compute distribribution for Level 1 and BIE.prior for Level 2
#----------------------------------------------------------------------
nstat.ComputeDistribution()
prior = BIE.Prior.PostMixturePrior(nprior, nstat)
sstat = BIE.Ensemble.EnsembleDisc(si)
convrg = BIE.Converge.SubsampleConverge(nsteps, sstat, "1")
current_level = 2
frontier.UpDownLevels(1)
sim = BIE.Simulation.DifferentialEvolution.WithVD(si, nchains, eps, dis, mod, intgr, convrg, prior, like, mca, nsim)
sim.SetControl(True)
width = 0.01
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()
psave
#----------------------------------------------------------------------
# Compute distribribution for Level 2 and BIE.prior for Level 3
#----------------------------------------------------------------------
sstat.ComputeDistribution()
nprior = BIE.Prior.PostMixturePrior(nprior, nstat)
nstat = BIE.Ensemble.EnsembleDisc(si)
convrg = BIE.Converge.SubsampleConverge(nsteps, sstat, "1")
current_level = 3
frontier.UpDownLevels(1)
sim = BIE.Simulation.DifferentialEvolution.WithVD(si, nchains, eps, dis, mod, intgr, convrg, prior, like, mca, nsim)
sim.SetControl(True)
width = 0.01
run = BIE.Simulation.RunOneSimulation(nsteps, width, nstat, nprior, nsim)
run.Run()
psave
#----------------------------------------------------------------------
BIE.BIEutil.quit()
