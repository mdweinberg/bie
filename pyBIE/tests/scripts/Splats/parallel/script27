#!/usr/bin/python

import BIE

#------------------------------------------------------------------------
# Initialize mpi environment
#------------------------------------------------------------------------

BIE.BIEutil.init()

#------------------------------------------------------------------------
# Prefix for diagnostic output files
#------------------------------------------------------------------------

BIE.cvar.nametag = "run27"

#------------------------------------------------------------------------
# The chain output file
#------------------------------------------------------------------------

BIE.cvar.outfile = "run27.statelog"

#------------------------------------------------------------------------
# The model dimension and number of components in the mixture
#------------------------------------------------------------------------

ndim = 2
nmix = 2
si   = BIE.State.StateInfo(nmix, ndim)
labs = BIE.PyVector.VectorString(2)
labs[0] = "Pos(x)"
labs[1] = "Pos(y)"
si.labelMixture(labs)		

#------------------------------------------------------------------------
# The number of node levels in the tessellation
#------------------------------------------------------------------------

tessdepth = 6

#------------------------------------------------------------------------
# Number of "particles" in the DE ensemble
#------------------------------------------------------------------------

nchains = 16
gamma   = 0.2

#------------------------------------------------------------------------
# The target knot spacing for the Gauss-Legendre 2-d integrator
#------------------------------------------------------------------------

dxy = 0.01

#------------------------------------------------------------------------
# The maximum number of steps per multiresolution level
#------------------------------------------------------------------------

nsteps = 40000
nburn  = 20000

#------------------------------------------------------------------------
# Scale factor for the variance estimated widths at successive levels
#------------------------------------------------------------------------

width = 0.1

#------------------------------------------------------------------------
# The MODEL
#------------------------------------------------------------------------

mod = BIE.Model.SplatModel(ndim, nmix)

#------------------------------------------------------------------------
# The data distribution
#------------------------------------------------------------------------

blo   = BIE.PyVector.VectorDouble(1, 0.0)
bhi   = BIE.PyVector.VectorDouble(1, 10.0)
bwid  = BIE.PyVector.VectorDouble(1, 10.0)
names = BIE.PyVector.VectorString(1, "attribute")
hist  = BIE.Distribution.HistogramND(blo, bhi, bwid, names)

#------------------------------------------------------------------------
# The initial prior distribution
#------------------------------------------------------------------------

unif = BIE.Distribution.UniformDist(-0.5, 1.5)
pvec = BIE.PyVector.VectorDist(2, unif)

prior = BIE.Prior.InitialMixturePrior.WithAD(si, 1.0, pvec)

#------------------------------------------------------------------------
# The posterior distribution estimator
#------------------------------------------------------------------------

sstat = BIE.Ensemble.EnsembleStat(si)

#------------------------------------------------------------------------
# The tile type
#------------------------------------------------------------------------

tile = BIE.Tessellation.SquareTile()

#------------------------------------------------------------------------
# The integrator over x-y to generate estimates for the data distribution
#------------------------------------------------------------------------

intgr = BIE.Integration.AdaptiveLegeIntegration(dxy, dxy)

#------------------------------------------------------------------------
# Assess the convergce of the chain 
# (CountConverge simply counts to nsteps)
#------------------------------------------------------------------------

convrg = BIE.Converge.CountConverge(nsteps, sstat, nburn)

#------------------------------------------------------------------------
# The spatial tesselation type
#------------------------------------------------------------------------

tess = BIE.Tessellation.QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, tessdepth)

#------------------------------------------------------------------------
# Populate the tesselation with data distributions
#------------------------------------------------------------------------

ris = BIE.Record.RecordInputStream_Ascii("../data/splat.data.1");
dis = BIE.DataTree.DataTree(ris, hist, tess)

#------------------------------------------------------------------------
# The likelihood computation
#------------------------------------------------------------------------

like = BIE.Like.LikelihoodComputationMPI(si)
# like.IntegrationGranularity()

#------------------------------------------------------------------------
# The Monte Carlo algorithm
#------------------------------------------------------------------------

mca = BIE.MCAlgorithm.MetropolisHastings()

#----------------------------------------------------------------------
# the randomization vector for DifferentialEvolution
#----------------------------------------------------------------------

cdf = BIE.Distribution.CauchyDist(0.002)
eps = BIE.PyVector.VectorDist(3, cdf)

#------------------------------------------------------------------------
# The simulation type
#------------------------------------------------------------------------

frontier = dis.GetDefaultFrontier()
frontier.UpDownLevels(3)
sim = BIE.Simulation.DifferentialEvolution.WithVD(si, nchains, eps, dis, mod, intgr, convrg, prior, like, mca)
# sim.EnableLogging()
sim.SetLinearMapping(True)
sim.SetJumpFreq(10)
sim.SetControl(False)
sim.NewGamma(gamma)

#------------------------------------------------------------------------
# The multiresolution simulation driver
#------------------------------------------------------------------------

run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)

# run.Restart()
#------------------------------------------------------------------------
# Let the simulation begin!
#------------------------------------------------------------------------

run.Run()
psave

#------------------------------------------------------------------------
# Compute distribribution for Level 0 and BIE.prior for Level 1
#------------------------------------------------------------------------

sstat.ComputeDistribution()
sstat.printWidth()
nprior = BIE.Prior.PostMixturePrior.WithAD(prior, sstat)
nstat = BIE.Ensemble.EnsembleStat(si)
convrg = BIE.Converge.CountConverge(nsteps, nstat, nburn)
#------------------------------------------------------------------------
# BIE.simulation for Level 1
#------------------------------------------------------------------------

BIE.cvar.current_level = 1
frontier.UpDownLevels(1)
nsim = BIE.Simulation.DifferentialEvolution.WithVD(si, nchains, eps, dis, mod, intgr, convrg, nprior, like, mca, sim)
nsim.SetLinearMapping(True)
nsim.SetJumpFreq(10)
nsim.SetControl(False)
nsim.NewGamma(gamma)
run = BIE.Simulation.RunOneSimulation(nsteps, width, nstat, nprior, nsim)

#------------------------------------------------------------------------
# Let the simulation begin!
#------------------------------------------------------------------------

run.Run()
# psave

#------------------------------------------------------------------------
# Compute distribribution for Level 1 and BIE.prior for Level 2
#------------------------------------------------------------------------

nstat.ComputeDistribution()
nstat.printWidth()
prior = BIE.Prior.PostMixturePrior.WithAD(nprior, nstat)
sstat = BIE.Ensemble.EnsembleStat(si)
convrg = BIE.Converge.CountConverge(nsteps, sstat)
BIE.cvar.current_level = 2
frontier.UpDownLevels(1)
sim = BIE.Simulation.DifferentialEvolution.WithVD(si, nchains, eps, dis, mod, intgr, convrg, prior, like, mca, nsim)
# sim.EnableLogging()
sim.SetLinearMapping(True)
sim.SetJumpFreq(10)
sim.SetControl(False)
sim.NewGamma(gamma)
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)

#------------------------------------------------------------------------
# Let the simulation begin!
#------------------------------------------------------------------------

run.Run()
psave

BIE.BIEutil.quit()
