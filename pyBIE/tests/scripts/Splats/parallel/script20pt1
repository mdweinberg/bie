#!/usr/bin/python

import BIE

#------------------------------------------------------------------------
# Initialize mpi environment
#------------------------------------------------------------------------

BIE.BIEutil.init()

#------------------------------------------------------------------------
# Prefix for diagnostic output files
#------------------------------------------------------------------------

BIE.cvar.nametag = "run20pt1"

#------------------------------------------------------------------------
# The chain output file
#------------------------------------------------------------------------

BIE.cvar.outfile = "run20pt1.statelog"


#------------------------------------------------------------------------
# Checkpointing (using the xml backend)
#
# pnewsession run20
# # pnewsession run20 xml
# ckon
# ckinterval 20
BIE.prestore('test20fid')
#
#------------------------------------------------------------------------
# Prefix for diagnostic output files
#
nametag = "run20"
#
#------------------------------------------------------------------------
# The chain output file
#
outfile = "run20p12.statelog"
#
#------------------------------------------------------------------------
# The model dimension and number of components in the mixture
#
ndim = 2
nmix = 2
si   = StateInfo(nmix, ndim)
labs = VectorString(2)
labs[0] = "Pos(x)"
labs[1] = "Pos(y)"
si.labelMixture(labs)		
#
#------------------------------------------------------------------------
# The number of node levels in the tessellation
#
tessdepth = 3
#
#------------------------------------------------------------------------
# The minimum number of temperature levels and the maximum temperature
# for the tempered states simulation
#
minmc = 6
maxT = 1024.0
#
#------------------------------------------------------------------------
# The target knot spacing for the Gauss-Legendre 2-d integrator
#
dxy = 0.02
#
#------------------------------------------------------------------------
# The maximum number of steps per multiresolution level
#
nsteps = 200
#
#------------------------------------------------------------------------
# Scale factor for the variance estimated widths at successive levels
#
width = 0.1
#
#------------------------------------------------------------------------
# The MODEL
#
mod = SplatModel(ndim,nmix)
#
#------------------------------------------------------------------------
# The data distribution
#
blo = VectorDouble(1, 0.0)
bhi = VectorDouble(1, 10.0)
bwid = VectorDouble(1, 10.0)
names = VectorString(1, "attribute")
hist = HistogramND(blo, bhi, bwid, names)
#
#------------------------------------------------------------------------
# The initial prior distribution
#
unif = UniformDist(-0.5, 1.5)
pvec = VectorDist(ndim, unif)
prior = InitialMixturePrior.WithAD(si, 1.0, pvec)
#
#------------------------------------------------------------------------
# The posterior distribution estimator
#
sstat = EnsembleStat(si)
#
#------------------------------------------------------------------------
# The tile type
#
tile = SquareTile()
#
#------------------------------------------------------------------------
# The integrator over x-y to generate estimates for the data distribution
#
nlb = 10
intgr = LegeIntegration(nlb, nlb)
#
#------------------------------------------------------------------------
# Assess the convergce of the chain 
# (CountConverge simply counts to nsteps)
#
convrg = CountConverge(nsteps, sstat)
#
#------------------------------------------------------------------------
# The spatial tesselation type
tess = QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, tessdepth)
#
#------------------------------------------------------------------------
# Populate the tesselation with data distributions
ris = RecordInputStream_Ascii("../data/splat.data.1");
dis = DataTree(ris,hist,tess)
#
#------------------------------------------------------------------------
# The likelihood computation
#
like = LikelihoodComputationMPI(si)
#
#------------------------------------------------------------------------
# The Monte Carlo algorithm
#
mca = MetropolisHastings()
#
#------------------------------------------------------------------------
# The Metropolis-Hastings proposal function
#
mvec = VectorDouble(3, 0.01)
mvec[0] = 0.002
mhwidth = MHWidthOne.WithV(si, mvec)
#
#------------------------------------------------------------------------
# The simulation type
#
frontier = dis.GetDefaultFrontier()
frontier.UpDownLevels(2)
sim = TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca)
#
#------------------------------------------------------------------------
# The multiresolution simulation driver
#
run = RunOneSimulation(nsteps, width, sstat, prior, sim)
# like.IntegrationGranularity()
# run.Restart()
#------------------------------------------------------------------------
# Let the simulation begin!
#
run.Run()
#
#------------------------------------------------------------------------
# psave
#
BIE.BIEutil.quit()
