#!/usr/bin/python

import BIE

#------------------------------------------------------------------------
# Initialize mpi environment
#------------------------------------------------------------------------

BIE.BIEutil.init()

#------------------------------------------------------------------------
# Prefix for diagnostic output files
#------------------------------------------------------------------------

BIE.cvar.nametag = "run25"

#------------------------------------------------------------------------
# The chain output file
#------------------------------------------------------------------------

BIE.cvar.outfile = "run25.statelog"
BIE.cvar.nametag = "run25"

ndim = 4
nmix = 2
si   = BIE.State.StateInfo(nmix, ndim)

labs    = BIE.PyVector.VectorString(4)
labs[0] = "Pos(x)"
labs[1] = "Pos(y)"
labs[2] = "Var(x)"
labs[3] = "Var(y)"
si.labelMixture(labs)		

tessdepth = 5
minmc     = 6
dxy0      = 0.05
dxy       = 0.001
nsteps    = 10000
width     = 0.001
maxT      = 128.0

mod       = BIE.Model.SplatModel(ndim, nmix)
blo       = BIE.PyVector.VectorDouble(1, 0.0)
bhi       = BIE.PyVector.VectorDouble(1, 10.0)
bwid      = BIE.PyVector.VectorDouble(1, 10.0)
names     = BIE.PyVector.VectorString(1, "attribute")
hist      = BIE.Distribution.HistogramND(blo, bhi, bwid, names)
pvec      = BIE.PyVector.VectorDist(4)
unif      = BIE.Distribution.UniformDist(0.0, 1.0)
weib      = BIE.Distribution.WeibullDist(0.01, 1.0, 1.0e-4, 10.0)

pvec[0] = unif
pvec[1] = unif
pvec[2] = weib
pvec[3] = weib
prior   = BIE.Prior.InitialMixturePrior.WithAD(si, 1.0, pvec)
sstat   = BIE.Ensemble.EnsembleDisc()
tile    = BIE.Tessellation.SquareTile()
intgr   = BIE.Integration.AdaptiveIntegration(0.000001, dxy0, dxy0, dxy, dxy)

convrg  = BIE.Converge.SubsampleConverge(nsteps, sstat, "")
convrg.setRcovTol(0.99)

tess = BIE.Tessellation.QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, tessdepth)
ris  = BIE.Record.RecordInputStream_Ascii("../data/splat.data.2");
dis  = BIE.DataTree.DataTree(ris,hist,tess)

like = BIE.Like.LikelihoodComputationMPI(si)
like.TileGranularity()

mca  = BIE.MCAlgorithm.MetropolisHastings()
mvec = BIE.PyVector.VectorDouble(5, 0.01)
mvec[0] = 0.002
mhwidth = BIE.MHWidth.MHWidthOne.WithV(si, mvec)
sim = BIE.Simulation.ParallelChains(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca)
sim.NewNumber(16)

run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()

BIE.BIEutil.quit()
