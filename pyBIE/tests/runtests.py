#!/usr/bin/python

from __future__ import print_function

# You will need to add the Python site package path to your Python
# search path to find this automatically

import BIE

help(BIE)

prior = BIE.Prior()

print("Created an instance of a BIE::Prior. Now let's print it:\n")
print(prior)

print("Done")

