%include <std_vector.i>
%include <std_string.i>

namespace std {
   %template(VectorDouble)       std::vector<double>;
   %template(VectorVectorDouble) std::vector< std::vector<double> >;
   %template(VectorString)       std::vector<std::string>;
   %template(VectorInt)          std::vector<int>;
   %template(VectorBool)         std::vector<bool>;
};

