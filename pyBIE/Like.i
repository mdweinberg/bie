%module(package="BIE") Like

%{
#include "LikelihoodFunction.h"
#include "PointLikelihoodFunction.h"
#include "VWLikelihoodFunction.h"
#include "PyLikelihoodFunction.h"
#include "BinnedLikelihoodFunction.h"
#include "MultiDimGaussMixture.h"
#include "MultiDimGaussTest.h"
#include "LinearRegression.h"
#include "BernsteinTest.h"
#include "FunnelTest.h"
#include "HLM.h"
#include "GaussTestLikelihoodFunction.h"
#include "GaussTestLikelihoodFunctionMulti.h"
#include "GaussTestLikelihoodFunctionRJ.h"
#include "GaussTest2D.h"
#include "GaussTestMultiD.h"
#include "LikelihoodComputation.h"
#include "LikelihoodComputationMPI.h"
#include "LikelihoodComputationMPITP.h"
#include "LikelihoodComputationSerial.h"
#include "Ensemble.h"
#include "storage.H"
%}

%feature("director") Callback;

%import(module="BIEutil") "Serializable.h"
%import(module="BIEutil") "ost.i"
%import(module="Distribution") "Distribution.h"
%import(module="Ensemble") "Ensemble.h"
%import(module="Ensemble") "CDFGenerator.h"

%include "global.i"
%include "LikelihoodFunction.h"
%include "PointLikelihoodFunction.h"
%include "VWLikelihoodFunction.h"
%include "PyLikelihoodFunction.h"
%include "BinnedLikelihoodFunction.h"
%include "MultiDimGaussMixture.h"
%include "MultiDimGaussTest.h"
%include "LinearRegression.h"
%include "BernsteinTest.h"
%include "FunnelTest.h"
%include "HLM.h"
%include "GaussTestLikelihoodFunction.h"
%include "GaussTestLikelihoodFunctionMulti.h"
%include "GaussTestLikelihoodFunctionRJ.h"
%include "GaussTest2D.h"
%include "GaussTestMultiD.h"
%include "LikelihoodComputation.h"
%include "LikelihoodComputationMPI.h"
%include "LikelihoodComputationMPITP.h"
%include "LikelihoodComputationSerial.h"

%extend BIE::GaussTestMultiD
{
  static GaussTestMultiD* WithI(int n, int l, std::vector<double> cen,
				std::vector<double> var, double b=-1.0)
  {
    return new GaussTestMultiD(n, l, cen, var, b);
  }

  static GaussTestMultiD* WithS(std::string n, int l, std::vector<double> cen,
				std::vector<double> var, double b=-1.0)
  {
    return new GaussTestMultiD(n, l, cen, var, b);
  }

  static GaussTestMultiD* WithWI(int n, int l,
				 std::vector<double> wght,
				 std::vector<double> cen,
				 std::vector<double> var, double b=-1.0)
  {
    return new GaussTestMultiD(n, l, wght, cen, var, b);		
  }

  static GaussTestMultiD* WithWS(std::string n, int l,
				 std::vector<double> wght,
				 std::vector<double> cen,
				 std::vector<double> var, double b=-1.0)
  {
    return new GaussTestMultiD(n, l, wght, cen, var, b);		
  }
};

%extend BIE::GaussTestLikelihoodFunctionMulti
{
  static GaussTestLikelihoodFunctionMulti* WithL(int p, int n, int l,
						 std::vector<double> cen,
						 std::vector<double> var,
						 std::vector<double> wght)
  {
    return new GaussTestLikelihoodFunctionMulti(p, n, l, cen, var, wght);
  }
  
  static GaussTestLikelihoodFunctionMulti* WithFive(int p, int n,
						    std::vector<double> cen,
						    std::vector<double> var,
						    std::vector<double> wght)
  {
    return new GaussTestLikelihoodFunctionMulti(p, n, cen, var, wght);
  }
  
};


%import "boost_pickle.i"

%boost_picklable(LikelihoodFunction)
%boost_picklable(PointLikelihoodFunction)
%boost_picklable(VWLikelihoodFunction)
%boost_picklable(Callback)
%boost_picklable(PyLikelihoodFunction)
%boost_picklable(BinnedLikelihoodFunction)
%boost_picklable(MultiDimGaussMixture)
%boost_picklable(MultiDimGaussTest)
%boost_picklable(LinearRegression)
%boost_picklable(BernsteinTest)
%boost_picklable(FunnelTest)
%boost_picklable(HLM)
%boost_picklable(GaussTestLikelihoodFunction)
%boost_picklable(GaussTestLikelihoodFunctionMulti)
%boost_picklable(GaussTestLikelihoodFunctionRJ)
%boost_picklable(GaussTest2D)
%boost_picklable(GaussTestMultiD)
%boost_picklable(LikelihoodComputation)
%boost_picklable(LikelihoodComputationMPI)
%boost_picklable(LikelihoodComputationMPITP)
%boost_picklable(LikelihoodComputationSerial)

%pythonbegin %{
"""
Definition for all forms of likelihood functions: native and user defined.
"""
%}
