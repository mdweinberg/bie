%module(package="BIE") Ensemble

%{
#include "Distribution.h"
#include "Ensemble.h"
#include "EnsembleStat.h"
#include "EnsembleDisc.h"
#include "EnsembleKD.h"
#include "CDFGenerator.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"
%import(module="Distribution") "Distribution.h"

%include "global.i"
%include "vectors.i"

%include "Ensemble.h"
%include "EnsembleStat.h"
%include "EnsembleDisc.h"
%include "EnsembleKD.h"
%include "CDFGenerator.h"

%import "boost_pickle.i"

%boost_picklable(Ensemble)
%boost_picklable(EnsembleKD)
%boost_picklable(EnsembleStat)
%boost_picklable(EnsembleDisc)
%boost_picklable(CDFGenerator)

%pythonbegin %{
"""
These classes maintain the simulation output and provide diagnostics and density estimates.
"""
%}
