%module(package="BIE") MarginalLikelihood

%{
#include "Serializable.h"
#include "MarginalLikelihood.h"
#include "GRanalyze.h"
#include "LikelihoodComputation.h"
#include "storage.H"
%}

%import(module="BIEutil") "ost.i"
%import(module="BIEutil") "Serializable.h"
%import(module="Like") "LikelihoodComputation.h"

%feature("notabstract") BIE::MarginalLikelihood;

%include "global.i"
%include "MarginalLikelihood.h"
%include "GRanalyze.h"

%import "boost_pickle.i"

%boost_picklable(MarginalLikelihood)
%boost_picklable(GRanalyze)

%pythonbegin %{
"""
Compute MarginalLikelihood quadrature using subsample selection from an MCMC simulation.
"""
%}
