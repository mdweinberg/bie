%module(package="BIE") Galphat

%{
#include "GalphatModel.h"
#include "GalphatLikelihoodFunction.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"
%import(module="Like") "LikelihoodFunction.h"

%include "global.i"
%include "vectors.i"

// Expose only the constructors and members needed by the user
//
%feature("notabstract") BIE::GalphatLikelihoodFunction;

namespace BIE {

  class GalphatParam : public Serializable
  {
  public:
    
    GalphatParam();

    GalphatParam(const std::string& galphat_input);
    
  };

  class GalphatLikelihoodFunction : public LikelihoodFunction
  {

  public:
    
    GalphatLikelihoodFunction();
    
    GalphatLikelihoodFunction(GalphatParam* pmap);
    
    GalphatLikelihoodFunction(GalphatParam* pmap, int level);

    void setNcrit (double n);

    void setTemp (double t);
    
    void imageDebug (bool tf);
    
    void noPSF();

    void use3Shear();

    void useBilinear();

    void setRotation (std::string s);
    
    void sampleImages (int stride, int total);
    
    void singleImage (std::string prefix);

    void gofStats (std::vector<double> vec,
		   std::string Xcenter, std::string Ycenter,
		   std::string PA, std::string Q, std::string Radius,
		   double radius_factor=1.0);

  };
  
} // namespace BIE

%extend BIE::GalphatLikelihoodFunction
{
  void gof(const std::vector<double>& vec,
	   const std::string& Xcenter,
	   const std::string& Ycenter,
	   const std::string& PA,
	   const std::string& Q,
	   const std::string& Radius,
	   double radius_factor)
  {
    $self->gofStats(vec, Xcenter, Ycenter, PA, Q, Radius, radius_factor);
  }
};

%import "boost_pickle.i"

%boost_picklable(GalphatLikelihoodFunction)
%boost_picklable(GalphatParam)

%pythonbegin %{
"""
GAlaxy PHotometric ATtributes (Galphat) for inference galaxy image features.
"""
%}
