%module(package="BIE") Filter
%{
#include "Serializable.h"
#include "RecordInputStream.h"
#include "RecordStream.h"
#include "RecordStreamFilter.h"
#include "BinaryFilters.h"
#include "SpecialFilters.h"
#include "SetFilters.h"
#include "UnaryFilters.h"
#include "RecordStreamFilter.h"
#include "MPIStreamFilter.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

%include "global.i"

%include "RecordStream.h"
%include "RecordInputStream.h"
%include "RecordStreamFilter.h"
%include "BinaryFilters.h"
%include "SpecialFilters.h"
%include "BinaryFilters.h"
%include "SetFilters.h"
%include "UnaryFilters.h"
%include "MPIStreamFilter.h"
#include "SpecialFilters.h"

%import "boost_pickle.i"

%boost_picklable(AdditionFilter)
%boost_picklable(SubtractionFilter)
%boost_picklable(ProductFilter)
%boost_picklable(DivisionFilter)
%boost_picklable(PowerFilter)
%boost_picklable(RPhiFilter)
%boost_picklable(SumFilter)
%boost_picklable(AverageFilter)
%boost_picklable(StatisticsFilter)
%boost_picklable(QuantileFilter)
%boost_picklable(MaxFilter)
%boost_picklable(MinFilter)
%boost_picklable(PSanityFilter)
%boost_picklable(AbsFilter)
%boost_picklable(LogFilter)
%boost_picklable(LogBaseNFilter)
%boost_picklable(ScaleFilter)
%boost_picklable(CosineFilter)
%boost_picklable(SineFilter)
%boost_picklable(MuFilter)
%boost_picklable(InverseMuFilter)
%boost_picklable(RecordStreamFilter)

%pythonbegin %{
"""
Defines computational and selection filters for native BIE data streams.
"""
%}
