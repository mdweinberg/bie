%module(package="BIE") PopModel

%{
#include "CMD.h"
#include "CMDModelCache.h"
#include "PopModelCacheF.h"
#include "PopModelCache.h"
#include "PopModelCacheSF.h"
#include "PopModelND.h"
#include "PopulationsApp.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"
%import(module="Model") "Model.h"
%include "global.i"

namespace BIE {

  class CMD : public Serializable
  {
    CMD(std::string directory, double minval=1.0e-10);
    virtual ~CMD() {}
  };

  class CMDModelCache : public Model
  {
  public:

    CMDModelCache(int ndim, SampleDistribution *dist, CMD *cmd);
    ~CMDModelCache();
    void SetKnots(int num);
    void SetDataDir(std::string datadir);
  };

  class PopModelCacheF : public Model
  {
    PopModelCacheF(int ndim, int mdim, int num,
		   SampleDistribution *histo, PopulationsApp *Pop);
    ~PopModelCacheF();
    void SetLineOfSight(double rmin, double rmax, bool rlog);
    void UseVectorCache();
    void UseHashCache();
    void SetMagNorm(double x, double y);
    double GetEMag0();
    double GetEMag1();
    double GetEMag2();
  };

  class PopModelCache : public Model
  {
  public:

    PopModelCache(int ndim, int mdim, SampleDistribution *dist);
    ~PopModelCache();
    void SetKnots(int num);
  };

  class PopModelCacheSF : public Model
  {
  public:

    PopModelCacheSF(int ndim, int mdim, int num,
		    SampleDistribution *histo, PopulationsApp *Pop,
		    std::string basisfile );
    ~PopModelCacheSF();

    void SetLineOfSight(double rmin, double rmax, bool rlog);
    void UseVectorCache();
    void UseHashCache();
    void SetMagNorm(double x, double y);
    double GetEMag0();
    double GetEMag1();
    double GetEMag2();
  };

  class PopModelND : public Model
  {
  public:
    PopModelND(int ndim, int mdim, SampleDistribution *dist);
    ~PopModelND();
    void SetKnots(int num);
  };

  class PopulationsApp : public Serializable
  {
  public:
    PopulationsApp(std::string directory, std::vector<int> Marginalize, bool CMD);
    PopulationsApp(std::string directory, std::string isoname, std::vector<int> Marginalize);
    ~PopulationsApp();
    void UseArray();
    void UseHash();
  };

}

%import "boost_pickle.i"

%boost_picklable(CMD)
%boost_picklable(CMDModelCache)
%boost_picklable(PopModelCacheF)
%boost_picklable(PopModelCache)
%boost_picklable(PopModelCacheSF)
%boost_picklable(PopModelND)
%boost_picklable(PopulationsApp)

%pythonbegin %{
"""
An implementation of stellar populations based on point-source magnitudes and sky coordinates.
"""
%}
