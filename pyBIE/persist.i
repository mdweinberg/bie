%pythoncode %{

import datetime
import inspect
import shelve
import types
import os
               
# Name of the persistence directory in the working directory
PERSISTENCE_DIR = './pdir'
               
# Name of the shelf file in the version directory
BIE_DB_NAME     = 'data.db'

# Name of the Boost Archive file in the version directory
BIE_AR_NAME     = 'data.ar'
               
# Name of the comment variable describing the save set in the shelf
BIE_COMMENT     = 'BIE_comment'

# Name of the timestamp variable for the save set in the shefl
BIE_TIMESTAMP   = 'BIE_timestamp'

# Globals to save
BIE_GLOBALS = ["current_level", "outfile", "nametag"]

# Everything can't be shelved.  Add stuff to the set of excluded types
# as necessary.

__EXCLUDE_T = set([types.ModuleType])

# Check the frame and make dict of stuff that would be shelved
#
def pcheck():
    """ 
    Check user variables in stack.  Use to examine the variables that
    would be picled in the shelf.  This is purely for developers.
    """
    # Get caller's frame
    cframe = inspect.getouterframes(inspect.currentframe())[1][0]
    
    # Make a dict of variables that would be shelved upon called save()
    globals_ = dict((key,value) for (key,value) in cframe.f_globals.items() if not key.startswith('__') and type(value) not in __EXCLUDE_T)
               
    return globals_
               
def plist_subdir(runtag):
    """
    List save sets in a named session
    """
    d = PERSISTENCE_DIR  
    d = os.path.join(d,runtag)
    x = [os.path.join(o) for o in os.listdir(d) if os.path.isdir(os.path.join(d,o))]
    # Sort the list by index and recreate
    i = [int(v) for v in x]
    i.sort()
    x = [str(v) for v in i]
    
    print('{}'.format(runtag))
    for f in x:
        filename  = os.path.join(d,f,BIE_DB_NAME)
        my_shelf  = shelve.open(filename)
        timestamp = ''
        comment   = ''

        if BIE_TIMESTAMP not in my_shelf:
            print('BIE save timestamp not found')
        else:
            timestamp = my_shelf[BIE_TIMESTAMP]

        if BIE_COMMENT not in my_shelf:
            print('BIE save comment not found')
        else:
            comment = my_shelf[BIE_COMMENT]

        print('     {:4s}: {:30s} {}'.format(f,timestamp,comment))
               
def plist (runtag=''):
    """
    With no argument: list save sets in all sessions or a particular named session
    With an argument: list save sets in a particular named session
    """
    try:
        os.stat(PERSISTENCE_DIR)
    except:
        print('No persistence directory <{}> found'.format(PERSISTENCE_DIR))
        return
               
    d = PERSISTENCE_DIR  
    if runtag !='':
        plist_subdir(runtag)
    else:
        x = [os.path.join(o) for o in os.listdir(d) if os.path.isdir(os.path.join(d,o))]
        for f in x:
            plist_subdir(f)
               
def prestore(runtag, icur=0):
    """
    Restore from a save set
    """
    # The following is for verbose debugging.  Set to False for
    # production
    Verbose = True

    # Get caller's frame
    cframe = inspect.getouterframes(inspect.currentframe())[1][0]
    
    # Get caller's globals
    globals_ = cframe.f_globals
               
    # Get the BIEutil module
    obj = globals_['BIE']

    # Create the persistence directory path
    d = os.path.join(PERSISTENCE_DIR,runtag)

    # Does directory exist?
    if not os.path.isdir(d):
        if obj.cvar.myid==0:
            print('I cannot find a run named <{}>.  Use "plist()" to see available sessions'.format(runtag))
        return

    # Look for saved sessions
    x = [os.path.join(o) for o in os.listdir(d) if os.path.isdir(os.path.join(d,o))]
               
    # These should be integers
    if icur==0:
        i = []
        for s in x: i.append(int(s))
        i.sort()
        # Last save set index
        icur = i[-1]
               
    dir = os.path.join(d, str(icur))
    try:
        os.stat(dir)
    except:
        if obj.cvar.myid==0:
            print('Directory <{}> does not exist'.format(dir))
        return
    
    shlfname = os.path.join(dir, BIE_DB_NAME)
    archname = os.path.join(dir, BIE_AR_NAME)
               
    # Open the Python shelf
    if obj.cvar.myid==0:
        print("Restoring from shelf file <{}>".format(shlfname))
    my_shelf = shelve.open(shlfname)
    
    # These need to be deserialized first and then ignored
    firstkeys = set(['BIE_globals', 'BIElist', 'BIEgen', 'BIE_comment', 'BIE_timestamp'])
    
    # These are the global variables
    BIE_globals = my_shelf['BIE_globals']
    for v in BIE_globals:
        if obj.cvar.myid==0:
            print('Restoring {} : {}'.format(v, BIE_globals[v]))
        exec('obj.cvar.{0:} = BIE_globals[\'{0:}\']'.format(v))
               
    # Get the packing list from the shelf
    BIElist = my_shelf['BIElist']
               
    # Open the Boost archive
    obj.BIEutil.openInArchive(archname)

    # Restore the random number generator (special case from std::string)
    obj.BIEutil.RetrieveBIEACG()
               
    for key in BIElist:
        # Already deserialized these
        if key in firstkeys: continue
               
        # Stuff the variable back into the global table
        globals_[key] = my_shelf[key]
        
	# Comment the next line for production
        if Verbose and obj.cvar.myid==0:
            print('unshelved: "%s"' % key)

    my_shelf.close()
    obj.BIEutil.closeInArchive()
               
               
def psave(runtag, comment=''):
    """
    Save the user variables
    """
    # The following is for verbose debugging.  Set to False for
    # production
    Verbose = True

    # Get the caller's frame
    cframe = inspect.getouterframes(inspect.currentframe())[1][0]
               
    # These will be caller's globals
    globals_ = cframe.f_globals
               
    # The BIE module
    obj = globals_['BIE']

    # Only write to PERSISTENCE_DIR at root process
    if obj.cvar.myid == 0:

        # Create the archive path
        dir = os.path.join(PERSISTENCE_DIR,runtag)
               
        try:
            os.stat(dir)
        except:
            print('Directory <{}> does not exist.  Creating . . . '.format(dir))
            os.makedirs(dir)
               
        x = [os.path.join(o) for o in os.listdir(dir) if os.path.isdir(os.path.join(dir,o))]
               
        if len(x)>0:
            # These should be integers
            i = []
            for s in x: i.append(int(s))
            i.sort()
            # Next save set index
            icur = i[-1] + 1
        else:
            icur = 1
               
        dir = os.path.join(dir, str(icur))
        try:
            os.stat(dir)
            print('Directory <{}> already exists'.format(dir))
            return
        except:
            os.mkdir(dir)
               
        # Make the archive file names
        shlfname = os.path.join(dir, BIE_DB_NAME)
        archname = os.path.join(dir, BIE_AR_NAME)
    else:
        shlfname = '/dev/null'
        archname = '/dev/null'
               
    # Save the timestamp
    globals_[BIE_TIMESTAMP] = datetime.datetime.now().isoformat()
               
    # Save the comment
    globals_[BIE_COMMENT] = comment
               
    # Save SWIG's BIE global variables by making a cvar dict and
    # saving the dict.  The swigvarlink object is not iterable so
    # the desired globals to save are listed in BIE_GLOBALS
    BIE_globals = {}
    for v in BIE_GLOBALS:
        exec('BIE_globals[\'{0:}\'] = obj.cvar.{0:}'.format(v))
    for v in BIE_globals:
        if obj.cvar.myid==0: print('{} : {}'.format(v, BIE_globals[v]))
    globals_['BIE_globals'] = BIE_globals
               
    # Open the Python shelf
    if obj.cvar.myid==0:
        my_shelf = shelve.open(shlfname, 'n')
    else:
        import dumbdbm
        import tempfile
        shlfname = tempfile.mkstemp()[1]
        db = dumbdbm.open(shlfname, 'n')
        my_shelf = shelve.Shelf(dict=db)
               
    # Open the Boost archive
    if obj.cvar.myid==0:
        print("Output archive file name={} Type={}".format(archname, type(archname)))
    obj.BIEutil.openOutArchive(archname)
               
    # The BIEACG pointer has to be handled separately.  Save the rng
    # to the archive.
    obj.BIEutil.StoreBIEACG()
               
    # Stored item list in storage order
    BIElist = []
               
    # The pickle call will invoke boost::serialization::serialize
    for key, value in globals_.items():
        if not key.startswith('__') and type(value) not in __EXCLUDE_T:
            try:
                my_shelf[key] = value
                BIElist.append(key)
	    except Exception as e:
                # Probably not fatal, just a warning
                print('ERROR shelving: "%s"' % key, 'Exception:', e)
            else:
                if Verbose and obj.cvar.myid==0:
                    print('shelved: "%s"' % key)
               
    # Shelve the packing list used to unpack the archive in FIFO order
    my_shelf['BIElist'] = BIElist

    if Verbose and obj.cvar.myid==0:
        print("The following variables have been archived")
        for v in BIElist:
            print("     {}".format(v))

    # Okay, done, close the archive
    my_shelf.close()
    obj.BIEutil.closeOutArchive()
    if obj.cvar.myid!=0: os.remove(shlfname)

%}
