// To force shared-library linkage using data members and functions
// below
%{
#include  "gvariable.h"
#include  "GlobalTable.h"
#include  "cblas.h"
%}

%include <std_string.i>
%include <std_container.i>
%include <std_vector.i>

%include "BIEmpi.h"

// The identifying label for the current simulation
std::string nametag;

// Used as an output file or suffix for diagnostic output
std::string outfile;

/* 
   The value of the current level for use by user Set by
   RunOneSimulation, RunMultilevelSimulation and by
   LikelihoodComputation
*/
int current_level;

// Defined by the system to indicate that MPI is used on invocation
bool mpi_used;

// Change default sampling in EnsembleStat
bool sample_discrete;

// The default base 1/kT value
double beta0;

// The boost archive type
std::string persistType;

// Reference to variable defined in libReflect to force linking
extern std::vector<global_table_struct> global_variables;

// Reference to variable defined in libblas to force linking
extern double cblas_dasum(const int N, const double *X, const int incX);
