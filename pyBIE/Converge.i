%module(package="BIE") Converge

%{
#include "Serializable.h"
#include "Distribution.h"
#include "Ensemble.h"
#include "Converge.h"
#include "CountConverge.h"
#include "GelmanRubinConverge.h"
#include "SubsampleConverge.h"
#include "storage.H"
%}

%include "global.i"
%include "vectors.i"

%import(module="BIEutil") "Serializable.h"
%import(module="Distribution") "Distribution.h"
%import(module="Ensemble") "Ensemble.h"

%include "Converge.h"
%include "CountConverge.h"
%include "GelmanRubinConverge.h"
%include "SubsampleConverge.h"

%import  "boost_pickle.i"

%boost_picklable(Converge)
%boost_picklable(CountConverge)
%boost_picklable(GelmanRubinConverge)
%boost_picklable(SubsampleConverge)

%pythonbegin %{
"""
These classes are used assess convergence and to stop the MCMC simulation.
"""
%}
