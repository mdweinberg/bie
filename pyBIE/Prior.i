%module(package="BIE") Prior

%{
#include "Serializable.h"
#include "MHWidth.h"
#include "Prior.h"
#include "PostPrior.h"
#include "MixturePrior.h"
#include "InitialMixturePrior.h"
#include "InitialMixturePriorPoisson.h"
#include "PostMixturePrior.h"
#include "PriorCollection.h"
#include "KDTree.h"
#include "TreePrior.h"
#include "VWPrior.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"
%import(module="Distribution") "Distribution.h"

%feature("notabstract") BIE::PostMixturePrior;

%include "global.i"
%include "Prior.h"
%include "PostPrior.h"
%include "MixturePrior.h"
%include "InitialMixturePrior.h"
%include "InitialMixturePriorPoisson.h"
%include "PostMixturePrior.h"
%include "PriorCollection.h"
%include "TreePrior.h"
%include "VWPrior.h"

%extend BIE::Prior
{
  static BIE::Prior* WithSI(BIE::StateInfo *si)
  { return new BIE::Prior(si); }
};

%extend BIE::InitialMixturePrior
{
  static BIE::InitialMixturePrior* WithAD
    (BIE::StateInfo *si, double alpha, std::vector<BIE::Distribution*>* d0)
  { return new BIE::InitialMixturePrior(si, alpha, *d0); }
  
  static BIE::InitialMixturePrior* WithAMD
    (BIE::StateInfo *si, double alpha, double mean, std::vector<BIE::Distribution*> d0)
  { return new BIE::InitialMixturePrior(si, alpha, mean, d0); }
  
  static BIE::InitialMixturePrior* WithAMD2
    (BIE::StateInfo *si, double alpha, double mean,
     std::vector<BIE::Distribution*> d0, std::vector<BIE::Distribution*> d1)
  { return new BIE::InitialMixturePrior(si, alpha, mean, d0, d1); }
};


%import "boost_pickle.i"

%boost_picklable(Prior)
%boost_picklable(PostPrior)
%boost_picklable(MixturePrior)
%boost_picklable(InitialMixturePrior)
%boost_picklable(InitialMixturePriorPoisson)
%boost_picklable(PostMixturePrior)
%boost_picklable(PriorCollection)
%boost_picklable(TreePrior)
%boost_picklable(VWPrior)


%pythonbegin %{
"""
Specification and definiton of the prior probability for various generic model classes.
"""
%}
