%module(package="BIE") SmplHist

%{
#include "Serializable.h"
#include "SmplHist.h"
#include "SmplStat.h"
%}

%import(module="BIEutil") "Serializable.h"
%import(module="SmplStat") "SmplStat.h"

%include "SmplHist.h"
