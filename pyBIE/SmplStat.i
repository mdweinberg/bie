%module(package="BIE") SmplStat
%{
#include "Serializable.h"
#include "SmplStat.h"
%}

%import(module="BIEutil") "Serializable.h"

%include "SmplStat.h"
