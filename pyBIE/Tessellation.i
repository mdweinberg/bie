%module(package="BIE") Tessellation

%{
#include "Tile.h"
#include "PointTile.h"
#include "SquareTile.h"
#include "CosBSquareTile.h"
#include "MuSquareTile.h"
#include "Data.h"
#include "Frontier.h"
#include "FrontierExpansionHeuristic.h"
#include "Tessellation.h"
#include "BinaryTessellation.h"
#include "ContainerTessellation.h"
#include "CursorTessellation.h" 
#include "KdTessellation.h"    
#include "PointTessellation.h"
#include "UniversalTessellation.h"
#include "MappedGrid.h"
#include "QuadGrid.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

%feature("notabstract") BIE::PointTile;
%feature("notabstract") BIE::SquareTile;
%feature("notabstract") BIE::MuSquareTile;

%include "Tile.h"
%include "PointTile.h"
%include "SquareTile.h"
%include "CosBSquareTile.h"
%include "MuSquareTile.h"
%include "Data.h"
%include "Frontier.h"
%include "FrontierExpansionHeuristic.h"
%include "Tessellation.h"
%include "BinaryTessellation.h"
%include "ContainerTessellation.h"
%include "CursorTessellation.h" 
%include "KdTessellation.h"    
%include "PointTessellation.h"
%include "UniversalTessellation.h"
%include "MappedGrid.h"
%include "QuadGrid.h"

%extend BIE::Data {
  double& __getitem__(int i) { return $self->operator[](i); }
}

%import "boost_pickle.i"

%boost_picklable(Tile)
%boost_picklable(PointTile)
%boost_picklable(SquareTile)
%boost_picklable(CosBSquareTile)
%boost_picklable(MuSquareTile)
%boost_picklable(Frontier)
%boost_picklable(FrontierExpansionHeuristic)
%boost_picklable(AlwaysIncreaseResolution)
%boost_picklable(KSDistanceHeuristic)
%boost_picklable(DataPointCountHeuristic)
%boost_picklable(Tessellation)
%boost_picklable(BinaryTessellation)
%boost_picklable(ContainerTessellation)
%boost_picklable(CursorTessellation)
%boost_picklable(KdTessellation)
%boost_picklable(PointTessellation)
%boost_picklable(UniversalTessellation)
%boost_picklable(MappedGrid)
%boost_picklable(QuadGrid)

%pythonbegin %{
"""
Classes to create and manipulate spatial tessellations (e.g. in sky coordinates) for use by native BIE line-of-sight models.
"""
%}
