%module(package="BIE") DataTree
%{
#include "BaseDataTree.h"
#include "DataTree.h"
#include "EmptyDataTree.h"
#include "storage.H"
%}

%include "global.i"

%import(module="BIEutil") "Serializable.h"
%import  "RecordBuffer.h"
%import  "RecordStream.h"
%import  "RecordInputStream.h"

%include "BaseDataTree.h"
%include "DataTree.h"
%include "EmptyDataTree.h"

%import "boost_pickle.i"

%boost_picklable(BaseDataTree)
%boost_picklable(DataTree)
%boost_picklable(EmptyDataTree)

%pythonbegin %{
"""
DataTree instances hold the input data in a summarized/decimated form.
"""
%}
