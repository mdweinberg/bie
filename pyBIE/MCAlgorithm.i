%module(package="BIE") MCAlgorithm

%{
#include "Serializable.h"
#include "MCAlgorithm.h"
#include "StandardMC.h"
#include "ReversibleJump.h"
#include "ReversibleJumpTwoModel.h"
#include "MetropolisHastings.h"
#include "TransitionProb.h"
#include "UniformAdd.h"
#include "UniformMult.h"
#include "NormalAdd.h"
#include "NormalMult.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

%include "global.i"
%include "Chain.h"
%include "MCAlgorithm.h"
%include "StandardMC.h"
%include "ReversibleJump.h"
%include "ReversibleJumpTwoModel.h"
%include "MetropolisHastings.h"
%include "TransitionProb.h"
%include "UniformAdd.h"
%include "UniformMult.h"
%include "NormalAdd.h"
%include "NormalMult.h"

%extend BIE::ReversibleJumpTwoModel
{
  static ReversibleJumpTwoModel* STDMap(StateInfo* si, std::vector<RJMapping*> m)
  {
    return new ReversibleJumpTwoModel(si, m);
  }
};

%import "boost_pickle.i"

%boost_picklable(MCAlgorithm)
%boost_picklable(StandardMC)
%boost_picklable(ReversibleJump)
%boost_picklable(ReversibleJumpTwoModel)
%boost_picklable(RJMapping)
%boost_picklable(MetropolisHastings)
%boost_picklable(TransitionProb)
%boost_picklable(UniformAdd)
%boost_picklable(UniformMult)
%boost_picklable(NormalAdd)
%boost_picklable(NormalMult)

%pythonbegin %{
"""
Implementations of various Markov Chain sampling algorithms.
"""
%}
