%module(package="BIE") BIEutil

%{
#include "BIEmpi.h"
#include "Serializable.h"
#include "storage.H"
%}

// To serialize and deserialize in Python and Boost
//
%include <std_string.i>
%include "Serializable.h"
%include "storage.i"
%include "persist.i"
%include "BIEmpi.h"
%include "ost.i"

%include "global.i"

%inline %{

  namespace BIE {

    extern void make_banner();

    void init(int seed=11)
    {
      const char* args[] = {"pyBIE"};
      mpi_init(1, const_cast<char **>(args));

      // Initialize the random number generator for each node
      BIEgen = new BIEACG(seed+myid, 25); 

      // Set mpi_used
      mpi_used = true;

      // BIE banner
      if (myid==0) make_banner();
      MPI_Barrier(MPI_COMM_WORLD);
    }

    void quit()
    {
      // Shutdown MPI
      MPI_Finalize();

      // Set mpi_used
      mpi_used = false;
    }

    void setArchiveType(const std::string & type)
    {
      std::string data = type;
      std::transform(data.begin(), data.end(), data.begin(), ::tolower);

      if (data == "xml")         arType = BOOST_XML_ARCHIVE;
      else if (data == "binary") arType = BOOST_BINARY_ARCHIVE;
      else if (data == "text")   arType = BOOST_TEXT_ARCHIVE;
      else {
	std::cout << "No such archive type <" << type << ">, using BINARY"
		  << std::endl;
	arType = BOOST_BINARY_ARCHIVE;
      }
    }
    void getArchiveType()
    {
      if (myid) return;
      
      std::cout << "Archive type is: ";

      switch (arType) {
      case BOOST_XML_ARCHIVE:
	std::cout << "XML" << std::endl;
	break;
      case BOOST_BINARY_ARCHIVE:
	std::cout << "BINARY" << std::endl;
	break;
      case BOOST_TEXT_ARCHIVE:
	std::cout << "TEXT" << std::endl;
	break;
      default:
	std::cout << "UNKNOWN (error!)" << std::endl;
	break;
      }
    }
    
  } // end namespace BIE

%}

%pythonbegin %{
"""
Wrappers for BIE utility routines used for overall initialiation, access to global data, multithreading, MPI initializaton and finalization, and persistence.
"""
%}
