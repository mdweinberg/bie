namespace ost {

  // This exposes the bare minimum of the interface needed by RunSimulation
  class Thread
  {
  public:
    Thread(bool isMain);
    Thread(int pri = 0, size_t stack = 0);
    Thread(const Thread &th);
    virtual ~Thread();
    void suspend(void);
    void resume(void);
  protected:
    virtual void run(void) = 0;
  };
};
