%module(package="BIE") State

%{
#include "Serializable.h"
#include "State.h"
#include "storage.H"
%}

%import(module="BIEutil") "Serializable.h"

%include "vectors.i"

%include "State.h"

// Overload the indexing operator for State
%extend BIE::State
{
  double __getitem__(int i) { return (*($self))[i]; }
};

%extend BIE::StateInfo
{
	static StateInfo* WithI(int i, int j, int k){
		return new StateInfo(i,j,k);
	}
	static StateInfo* WithV(std::vector<int> i, std::vector<int> j, std::vector<int> k ){
		clivectori c_i(i);
		clivectori c_j(j);
		clivectori c_k(k);
		return new StateInfo(&c_i, &c_j, &c_k);
	}
};

%import "boost_pickle.i"

%boost_picklable(StateInfo)

%pythonbegin %{
"""
General representation and access to the parameter vector and its metainfo.
"""
%}
