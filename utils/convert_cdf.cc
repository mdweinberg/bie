#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>
#include <cstring>

#include "BIEException.h"

using namespace BIE;

void usage(const char* prog)
{
  cout << "*********************************************************\n"
       << "******   Converts BIE ascii file to NetCDF file   *******\n"
       << "*********************************************************\n"
       << " Usage: " << prog << " [options] < ascii.file\n"
       << " Options: \n"
       << "    -f \"<cdf file>\"\n"
       << "    -t \"<title>\"\n"
       << "    -c \"<comment; multiple invocations allowd>\"\n"
       << "    -n \"<maximum character length for string field>\"\n"
       << "    -r \"<write floats for real fields instead of doubles>\"\n"
       << "*********************************************************\n";
  exit(0);
}

int
main (int argc, char **argv) 
{
  char *prog = argv[0];
  string title = "Data file";
  vector<string> comments;
  string cdffile = "tmp.cdf";
  string basisdata = "";
  long int charlen = 40;
  bool use_float = false;
  int c;
  
  while (1) {
    c = getopt (argc, argv, "f:t:d:n:rb:c:h");
    if (c == -1) break;

    switch (c) {
    case 'f':
      cdffile = string(optarg);
      break;
    case 't':
      title = string(optarg);
      break;
    case 'b':
      basisdata = string(optarg);
      break;
    case 'n':
      charlen = atoi(optarg);
      break;
    case 'r':
      use_float = true;
      break;
    case 'c':
      comments.push_back(optarg);
      break;
    case 'h':
    case '?':
    default:
      usage(prog);
    }
  }
  
  if (optind < argc) {
    cerr << "non-option ARGV-elements: ";
    while (optind < argc)
      cerr << argv[optind++] << " ";
    cerr << endl;
  }

				// Read in parameters
  int nparam=0;
  vector<double> vx, vy, sx, sy, w;
  const char *basisname[5] = {"X_pos", "Y_pos", "X_wid", "Y_wid", "Weight"};

  if (basisdata.size()) {

    ifstream in(basisdata.c_str());
    if (!in) {
      string msg = "could not open basisdata file: " + basisdata;
      cerr << msg << endl;
    }

    const int linesize = 512;
    char line[linesize];
    int nflux;
  
    in.getline(line, linesize);
    istringstream ins1(line);
    ins1 >> nparam;

    in.getline(line, linesize);
    istringstream ins2(line);
    ins2 >> nflux;

    for (int i=0; i<nparam; i++) {
      double xx, yy, sxx, syy, ww;
      in.getline(line, linesize);
      istringstream ins(line);
      ins >> xx;		vx.push_back(xx);
      ins >> yy;		vy.push_back(yy);
      ins >> sxx;		sx.push_back(sxx);
      ins >> syy;		sy.push_back(syy);
      ins >> ww;		w.push_back(ww);
    }
  }
  

				// Read the ascii input stream
  vector<string> type, name;
  char *e, *d, *line, *temp;
  const unsigned chrsize = 512;
  
  line = new char [chrsize];
  temp = new char [chrsize];

  cin.getline(line, chrsize);

				// Continue until blank line
  while (strlen(line) && cin) {
				
    e = line;
				// Skip blanks
    while (*e ==' '|| *e=='\t') e++;

    d = temp;			// Token 1
    while (*e !=' ' && *e!='\t') *d++ = *e++;
    *d = '\0';
    type.push_back(temp);
				// Skip ahead to quote
    while (*e !='"') e++;
    e++;
    d = temp;			// Token 2
    while (*e !='"') *d++ = *e++;
    *d = '\0';
    name.push_back(temp);

    cin.getline(line, 512);
  }

  unsigned nfields = name.size();

				// Status indicator returned by NetCDF calls
  int ncstatus = 0;
				// NetCDF file ID
  int out_ncid;
  int in_ncid;
  int unlimiteddim;

				// Text definitions
				// ----------------
  int  chdim;			// dimension ID for char positions
  const int TDIMS=2;		// dimensionality of tx variable
  int tx_dims[TDIMS];		// variable shape
  size_t tx_start[TDIMS];
  size_t tx_count[TDIMS];

  // Create a new NetCDF file.  Don't overwrite an existing file.
  ncstatus = nc_create(cdffile.c_str(), NC_NOCLOBBER, &out_ncid);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
  // Define the unlimited dimension.  Using the unlimited dimension allows
  // us to continue pushing out records for ever.
  ncstatus = nc_def_dim (out_ncid, "unlimited", NC_UNLIMITED, &unlimiteddim);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  // Define character dimension
  ncstatus = nc_def_dim (out_ncid, "chardim", charlen, &chdim);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }


  // Fields

  tx_dims[0] = unlimiteddim;
  tx_dims[1] = chdim;

  int varid[nfields];
  
  for (unsigned i=0; i<nfields; i++) {
    if (!type[i].compare("real"))
      if (use_float)
	ncstatus = nc_def_var (out_ncid, name[i].c_str(), NC_FLOAT, 1, 
			       &unlimiteddim, &varid[i]);
      else
	ncstatus = nc_def_var (out_ncid, name[i].c_str(), NC_DOUBLE, 1, 
			       &unlimiteddim, &varid[i]);
    else if (!type[i].compare("int"))
      ncstatus = nc_def_var (out_ncid, name[i].c_str(), NC_INT, 1, 
			     &unlimiteddim, &varid[i]);
    else if (!type[i].compare("string"))
      ncstatus = nc_def_var (out_ncid, name[i].c_str(), NC_CHAR, TDIMS, 
			     tx_dims, &varid[i]);
    else if (!type[i].compare("bool"))
      ncstatus = nc_def_var (out_ncid, name[i].c_str(), NC_BYTE, 1, 
			     &unlimiteddim, &varid[i]);
    else {
      throw NetCDFFormatException(name[i].c_str(), -1, cdffile.c_str(), 
				  NC_NAT, __FILE__, __LINE__);
    }

      
    if (ncstatus != NC_NOERR)
      { 
	throw NetCDFException(ncstatus, __FILE__, __LINE__); 
      }
  }
  
  ncstatus = nc_put_att_text(out_ncid, NC_GLOBAL, "title", 
			     title.size()+1, title.c_str());
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }


  for (unsigned int i=0; i<comments.size(); i++) {
    ostringstream ocom;
    ocom << "Comment_" << i+1;
    ncstatus = nc_put_att_text(out_ncid, NC_GLOBAL, ocom.str().c_str(), 
			       comments[i].size()+1, comments[i].c_str());
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  }


  // Write basis data

  if (nparam) {
    
    size_t cnt = nparam;

    ncstatus = nc_put_att_double(out_ncid, NC_GLOBAL, basisname[0], NC_DOUBLE,
				 cnt, &vx[0]);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

    ncstatus = nc_put_att_double(out_ncid, NC_GLOBAL, basisname[1], NC_DOUBLE,
				 cnt, &vy[0]);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

    ncstatus = nc_put_att_double(out_ncid, NC_GLOBAL, basisname[2], NC_DOUBLE,
				 cnt, &sx[0]);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

    ncstatus = nc_put_att_double(out_ncid, NC_GLOBAL, basisname[3], NC_DOUBLE,
				 cnt, &sy[0]);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

    ncstatus = nc_put_att_double(out_ncid, NC_GLOBAL, basisname[4], NC_DOUBLE,
				 cnt, &w[0]);
    if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  }


  // Exit define mode.  We are now ready to write data.
  ncstatus = nc_enddef(out_ncid);
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }


  // Write out each field.  If a field does not have a value then
  // an exception will be thrown.
  
  size_t outnc_recordindex = 0;
  float flt_value;
  double dbl_value;
  int int_value;
  string str_value;
  unsigned char byte_value;
  int nrecords = 0;

  cin.getline(line, chrsize);

  while (strlen(line) && cin) {

    istringstream sin(line);

    for (unsigned i=0; i<nfields; i++) {
    
      if (!type[i].compare("real")) {
	if (use_float) {
	  sin >> flt_value;
	  ncstatus = nc_put_var1_float(out_ncid, varid[i], 
				       &outnc_recordindex, &flt_value);
	} else {
	  sin >> dbl_value;
	  ncstatus = nc_put_var1_double(out_ncid, varid[i], 
					&outnc_recordindex, &dbl_value);
	}
      }
      else if (!type[i].compare("int")) {
	sin >> int_value;
	ncstatus = nc_put_var1_int(out_ncid, varid[i], 
				   &outnc_recordindex, &int_value);
      }
      else if (!type[i].compare("string")) {
	sin >> str_value;
	tx_start[0] = nrecords;	// Record number
	tx_start[1] = 0;
	tx_count[0] = 1;	// only write one record
	tx_count[1] =		// number of chars to write
	  min<int>(str_value.size() + 1, charlen); 

	ncstatus = nc_put_vara_text(out_ncid, varid[i], tx_start, tx_count,
				    str_value.c_str());
      }
      else if (!type[i].compare("bool")) {
	sin >> int_value;
	if (int_value) byte_value = 1;
	else byte_value = 0;
	ncstatus = nc_put_var1_uchar(out_ncid, varid[i], 
				     &outnc_recordindex, &byte_value);
      }

	
      if (ncstatus != NC_NOERR)
	{ throw NetCDFException(ncstatus, __FILE__, __LINE__); }

      // Flush the buffer to push out the record straight away.
      nc_sync(out_ncid);
      if (ncstatus != NC_NOERR)
	{ throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    
    nrecords++;
    outnc_recordindex++;

    cin.getline(line, chrsize);
  }
  
  // Close the NetCDF file
  nc_close(out_ncid);

  cout << "CDF file <" << cdffile << "> written " << endl;
  cout << "Testing . . . " << endl;

  // Open the NetCDF file for reading.
  ncstatus = nc_open(cdffile.c_str(), NC_NOWRITE, &in_ncid);
  if (ncstatus != NC_NOERR)
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
  // Get the number of variables.
  int numvariables;
  ncstatus = nc_inq_nvars(in_ncid, &numvariables);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  cerr << "Found " << numvariables << " variables, expected " 
       << nfields << endl;
  
  // Process each variable in turn to get and check the type of each.
  char    fieldname[NC_MAX_NAME];
  nc_type fieldtype;
  int     fielddim;
  int     dimensionids[NC_MAX_VAR_DIMS];
  int     unlimiteddimid;
  
  for (int varid = 0; varid < numvariables; varid++)
    {
      ncstatus = nc_inq_var(in_ncid, varid, fieldname, &fieldtype, 
                            &fielddim, dimensionids, &unlimiteddimid);
      if (ncstatus != NC_NOERR) 
	{ throw NetCDFException(ncstatus, __FILE__, __LINE__); }
      
      // Check that the variable has exactly one or two dimensions
      if (fielddim > 2)
	{
	  throw NetCDFFormatException(fieldname, varid, fielddim, 
				      cdffile, __FILE__, __LINE__);
	}
      
      // Check that the dimension size is set to "unlimited"
      if (unlimiteddimid != dimensionids[0])
	{
	  throw NetCDFFormatException(fieldname, varid, cdffile, __FILE__, __LINE__);
	}
      
      // Check for support fields types and dimensions
      if (fieldtype == NC_DOUBLE && fielddim == 1)
	{
	  cout << setw(4) << varid << ": type is double" << endl;
	}
      else if (fieldtype == NC_FLOAT && fielddim == 1)
	{
	  cout << setw(4) << varid << ": type is float" << endl;
	}
      else if (fieldtype == NC_INT && fielddim == 1)
	{
	  cout << setw(4) << varid << ": type is int" << endl;
	}
      else if (fieldtype == NC_CHAR && fielddim == 2)
	{
	  cout << setw(4) << varid << ": type is string" << endl;
	}
      else if (fieldtype == NC_BYTE && fielddim == 1)
	{
	  cout << setw(4) << varid << ": type is bool" << endl;
	}
      else
	{
	  // No other types are currently supported.
	  throw NetCDFFormatException(fieldname, varid, cdffile, 
				      fieldtype, __FILE__, __LINE__);
	}
      
    }
  
  size_t in_numrecords;
  
  // Get the number of records in the NetCDF file.
  ncstatus = nc_inq_unlimdim(in_ncid, &unlimiteddimid);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
  ncstatus = nc_inq_dimlen(in_ncid, unlimiteddimid, &in_numrecords);
  if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
  cerr << "Found " << in_numrecords << " records, expected " 
       << nrecords << endl;

  nc_close(in_ncid);
    
  exit (0);
}
