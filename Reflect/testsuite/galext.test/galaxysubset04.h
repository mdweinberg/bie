#ifndef QuadGrid_h
#define QuadGrid_h

#include <string>
#include <map>

#include <FullTessellation.h>

namespace BIE {
  
  //+ CLASS QuadGrid SUPER FullTessellation
  //! A hierarchical, recursive, rectangular tessellation of space 
  //! into equally sized bins.  Starting with one tile encompassing
  //! the whole tessellation, each tile is recursively split into four equally
  //! sized tiles.
  class QuadGrid : public FullTessellation {
  public:
    //+ CONSTR Tile* double double double double int
    //! Constructs a QuadGrid tessellation.  
    QuadGrid(Tile * factory, double minx, double maxx, 
             double miny, double maxy, int depth);
    
    ~QuadGrid();

    vector<int> GetRootTiles();
    vector<Node*> GetRootNodes();

  private:
    // These define the boundary of the tessellation.
    double Xmin, Xmax, Ymin, Ymax;
    // The depth of the tessellation.
    int maxdepth;
    /// Tessellation tree root.
    Node * treeroot;
    /// Tile factory
    Tile * tilefactory;

    /// Performs tessellation of the space
    void tessellate(Node **node, int depth, int tileid, vector<int>& leaves, 
                    double minx, double miny,double maxx, double maxy);
  };
}

#endif
