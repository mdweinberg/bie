// Automatically generated by ../../gal_static_gen at Mon Jul 15 20:44:10 2002

#include <galaxysubset01.h>
#include <galaxysubset02.h>
#include <galaxysubset03.h>
#include <galaxysubset04.h>
#include <galaxysubset05.h>
#include <galaxysubset06.h>
#include <galaxysubset07.h>
#include <galaxysubset08.h>
#include <galaxysubset09.h>
#include <galaxysubset10.h>
#include <galaxysubset11.h>
#include <galaxysubset12.h>
#include <galaxysubset13.h>
#include <galaxysubset14.h>
#include <galaxysubset15.h>
#include <string.h>
#include <ArgList.h>
#include <AData.h>

using namespace BIE;

void InvokeStatic(char *format, ArgList *args, AData *return_value)
{
  if (!strcmp(format,"BinaryTessellation::staticmethodIDZV")){
    int l1 = (int)(args->next())->data.ival;
    double l2 = (args->next())->data.dval;
    BinaryTessellation::staticmethod(l1,l2);
    return;
  }

  // None of the cases was met.
  throw InternalError(__FILE__, __LINE__);
}
