#ifndef RecordOutputStream_h
#define RecordOutputStream_h

#include <RecordStream.h>
#include <RecordStreamFilter.h>

namespace BIE {

  //+ CLASS RecordOutputStream SUPER RecordStream
  //! Record output streams are used to write a series of records to one or more
  //! sources.  New output streams are created by inheriting an existing stream.
  //! When inherited, streams can be modified,  processed by a filter,
  //! or the data can be output to a data sink (e.g. to a file or a network socket).
  //! Nodes can be inherited an unlimited number of times (with a few exceptions -
  //! see below)
  //!
  //! As with input streams, inheriting streams creates an implicit tree structure
  //! Generally leaves are data sinks and internal nodes apply operations but this
  //! is not a strict rule.  Data is pushed from the root node by the user/program
  //! to all inheriting streams - this is done by calling pushRecord() recursively
  //! (depth first).
  //!
  //! This class implements all operations on output streams - field deletion,
  //! filtering, field selection, field renaming, etc.  Subclasses implement
  //! systems to write to data sinks.  If a subclass implements it, then 
  //! these streams can be inherited, but a mechanism for telling when a stream
  //! cannot be inherited exists (so subclass writers are not required to always
  //! add this functionality).
  ///
  /// \todo registerforUpdates should not be public.  The class does not
  /// compile when it was protected (as it should be).
  class RecordOutputStream : public RecordStream {
  
  public:
    /// Virtual destructor.  This removes the stream from the output 
    /// stream register.
    virtual ~RecordOutputStream() { removeOldStream(this); }
  
    //+ CONSTR RecordType*
    //! Construct a new root output stream with the given type.
    RecordOutputStream(RecordType * streamtype);
    
    //+ METHOD void pushRecord
    /// Pushes the contents of the buffer out to inheriting streams.
    virtual void pushRecord() throw (NoValueException, EndofStreamException);
    
    //+ METHOD void close
    //! Indicates the end of the stream to all inheriting streams.
    virtual void close();
    
    //+ METHOD RecordOutputStream* deleteField string
    //! Create a new output stream which masks out the specified field, and 
    //! inherits all other buffers.
    RecordOutputStream* deleteField(string fieldname) 
      throw (NoSuchFieldException, StreamInheritanceException);
  
    //+ METHOD RecordOutputStream* deleteField int
    //! Create a new output stream which masks out the specified field, and 
    //! inherits all other buffers.
    RecordOutputStream* deleteField(int fieldindex) 
      throw (NoSuchFieldException, StreamInheritanceException);
      
    //+ METHOD RecordOutputStream* deleteRange int int
    //! Deletes a range of fields & inherits all other buffers. 
    RecordOutputStream * deleteRange (int startfield, int endfield) 
      throw (NoSuchFieldException, BadRangeException, StreamInheritanceException);
     
    //+ METHOD RecordOutputStream* selectFields RecordType*
    //! Creates a new output stream containing only the fields specified by 
    //! the type descriptor.  All the fields in the type descriptor must be present
    //! in the existing stream. Both field name and field type must match.
    RecordOutputStream * selectFields(RecordType * selection)      
      throw (NoSuchFieldException, StreamInheritanceException);
      
    //+ METHOD RecordOutputStream* selectFields vector<int>*
    //! Creates a new output stream containing only the fields specified by 
    //! the field indices.  All indices must be within the bounds of the existing
    //! stream.  No duplicate indices are allowed.
    RecordOutputStream * selectFields(vector<int>* selection)      
      throw (NoSuchFieldException, DuplicateFieldException, StreamInheritanceException);
  
    //+ METHOD RecordOutputStream* selectFields vector<string>*
    //! Creates a new stream containing only the fields specified by the field
    //! names.  A NoSuchFieldException is thrown if a name is not present in
    //! the existing stream. No duplicate names are allowed.
    RecordOutputStream * selectFields(vector<string>* selection)    
      throw (NoSuchFieldException, DuplicateFieldException, StreamInheritanceException);
  
    //+ METHOD RecordOutputStream* renameField string string
    //! Creates a new stream with the specified field renamed.
    RecordOutputStream * renameField(string oldname, string newname) 
     throw (NoSuchFieldException, StreamInheritanceException);
  
    //+ METHOD RecordOutputStream* renameField int string
    //! Creates a new stream with the specified field renamed.
    RecordOutputStream * renameField(int fieldindex, string newname) 
     throw(NoSuchFieldException, StreamInheritanceException);
       
    //+ METHOD RecordOutputStream* moveField string int  
    //! Moves a field (specified by name) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordOutputStream * moveField(string fieldname, int newposition)
     throw (NoSuchFieldException, StreamInheritanceException);
  
    //+ METHOD RecordOutputStream* moveField string int  
    //! Moves a field (specified by index) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordOutputStream * moveField(int oldposition, int newposition)
     throw (NoSuchFieldException, StreamInheritanceException);
    
    //+ METHOD RecordOutputStream* filterWith int RecordStreamFilter*
    //! Pipes the input through a filter which can add new fields based on 
    //! the values of the fields in the existing stream.  The filter must 
    //! be "useable".
    RecordOutputStream * filterWith (int position, RecordStreamFilter * filter)
      throw (UnusableFilterException, TypeException,
             InsertPositionException, StreamInheritanceException);
  	   
    //+ METHOD void setStringValue int string
    //! Sets the value of a string field (referenced by index) in a root output stream.
    void setStringValue(int fieldindex, string value)   
      throw (TypeException, NoSuchFieldException, NotRootException);
    //+ METHOD void setIntValue int int
    //! Sets the value of an integer field (referenced by index) in a root output stream.
    void setIntValue   (int fieldindex, int  value)   
      throw (TypeException, NoSuchFieldException, NotRootException);
    //+ METHOD void setRealValue int double
    //! Sets the value of a real field (referenced by index) in a root output stream.
    void setRealValue  (int fieldindex, double value)   
      throw (TypeException, NoSuchFieldException, NotRootException);
    //+ METHOD void setBoolValue int bool
    //! Sets the value of a boolean field (referenced by index) in a root output stream.
    void setBoolValue  (int fieldindex, bool value)   
      throw (TypeException, NoSuchFieldException, NotRootException);  
    
    //+ METHOD void setStringValue string string
    //! Sets the value of a string field (referenced by name) in a root output stream.
    void setStringValue(string fieldname, string value) 
      throw (TypeException, NoSuchFieldException, NotRootException);
    //+ METHOD void setIntValue string int
    //! Sets the value of an integer field (referenced by name) in a root output stream.
    void setIntValue   (string fieldname, int value) 
      throw (TypeException, NoSuchFieldException, NotRootException);
    //+ METHOD void setRealValue string double
    //! Sets the value of a real field (referenced by name) in a root output stream.
    void setRealValue  (string fieldname, double value) 
      throw (TypeException, NoSuchFieldException, NotRootException);
    //+ METHOD void setBoolValue string bool
    //! Sets the value of a boolean field (referenced by name) in a root output stream.
    void setBoolValue  (string fieldname, bool value) 
      throw (TypeException, NoSuchFieldException, NotRootException);  
  
    //+ METHOD string toString
    //! Returns a string representation of the stream.
    virtual string toString();
    
    //+ METHOD bool isRootStream
    //! Returns true if this stream is a root stream.
    bool isRootStream () { return ros_isroot; }
  
    //+ METHOD bool inheritable
    //! Returns true if this stream can be inherited.
    bool inheritable  () { return ros_inheritable; }
  
    /// This is called when another stream wants to receive updates (through
    /// pushRecord()).  The stream this is called on must be inheritable.
    void registerForUpdates(RecordOutputStream * stream)
      throw (StreamInheritanceException);

    /// Returns the stream corresponding to the given stream index.
    static RecordOutputStream* getStream(int streamindex)
      throw (InvalidStreamIDException);

    /// Returns a vector containing a pointer to all output streams that can 
    /// be inherited from.
    static vector<RecordOutputStream*> getInheritableStreams();
    
  protected:
    /// Constructs a new output stream with nothing set up - only used
    /// internally.
    RecordOutputStream() {}
  
    /// true if this is the root stream.
    bool ros_isroot;
    /// true if this stream can be inherited.
    bool ros_inheritable;
    /// true if this stream has been closed.
    bool ros_closed;
    
    /// Called when a new stream is created.  This information is used by the 
    /// getAvailableStreams() function.
    static void addNewStream(RecordOutputStream * newstream);
    
    /// Called to remove a stream when it is deleted.  Keeps a null reference
    /// in output stream list so other indices are not changed. 
    static void removeOldStream(RecordOutputStream * oldstream);
 
    /// A reference to the stream inherited from.  This is used
    /// to tell how this stream gets its data.
    RecordOutputStream * ros_myinputstream;
    
  private:
    /// A private method used to construct a new stream from the existing stream
    /// and a (presumably partly inherited) record buffer.
    RecordOutputStream * createDerivative (RecordBuffer * newbuffer)
      throw (StreamInheritanceException);
    
    /// A list of the streams inheriting from this stream.
    vector<RecordOutputStream *> ros_inheriting;
    
    /// Reference to a filter.  Set to 0 if no filter is being user by the object.
    RecordStreamFilter * ros_filter;
    
    /// A list of all inheritable output streams.
    static vector<RecordOutputStream*> ros_inheritablestreams;
  };
}

#endif
