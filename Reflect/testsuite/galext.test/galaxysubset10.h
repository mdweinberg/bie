#ifndef RecordType_h
#define RecordType_h

#include <TypedBuffer.h>

namespace BIE  {

  //+ CLASS RecordType
  //! Type descriptors for record (or table) types, which are a set of 
  //! (name, type) pairs like a C-struct declaration.
  //! 
  //! All names in a record type
  //! are distinct, and all pairs are ordered.   This means that referring to 
  //! a field makes sense whether by name or by field index number.  
  //! Field indices begin at 1. All RecordType objects are immutable.  
  //! Methods for creating new record 
  //! types do not modify the existing object and return completely new 
  //! record type objects.
  class RecordType {
  
  public:
    //+ CONSTR
    //! Create an empty record type.
    RecordType(){}
    
    //+ CONSTR RecordType*
    //! Create an exact copy of an existing record type object
    RecordType(RecordType *);

    /// This structure holds pairs of fieldname and fieldtype pairs.
    /// Arrays of these structures can be used to construct record types.
    typedef struct field { 
      /// Name of the field.
      const char *       fieldname;
      /// The type of the field. 
      const BasicType *  fieldtype;
    } fieldstruct;

    /// Create a type from a null terminiated array of fieldstructs
    RecordType(const fieldstruct * typetable) throw (DuplicateFieldException);
    
    //+ METHOD RecordType* deleteField string
    //! Delete a single field using field name.
    RecordType * deleteField (string) throw (NoSuchFieldException);
  
    //+ METHOD RecordType* deleteField int
    //! Delete a single field using field index.
    RecordType * deleteField (int) throw (NoSuchFieldException);
      
    //+ METHOD RecordType* deleteRange int int
    //! Delete a range of fields.  The first argument should specify
    //! a field with a lower or equal field  than the second.  
    //! All fields in this range including those used to specify the 
    //! boundary are deleted.
    RecordType * deleteRange (int startfield, int endfield) 
       throw (NoSuchFieldException, BadRangeException);
     
    //+ METHOD RecordType* insertField int string BasicType*
    //! Inserts a new field.  Can be used to append fields by inserting
    //! at position (n+1).  If a field with the given name already exists
    //! then the exception NameClashException is thrown. Where the specified
    //! insertion point is not in the range 1 to (n+1) then NoSuchFieldException
    //! is thrown.
    RecordType * insertField(int insertpos, string fieldname, BasicType * fieldtype) 
      throw (NameClashException, InsertPositionException);
    
    //+ METHOD RecordType* insertRecord int RecordType*
    //! Inserts another record type.  Can be used to append 
    //! by inserting at position (n+1).  If any fields in the record being
    //! inserted clash names with existing fields then the exception 
    //! NameClashException is thrown. Where the specified
    //! insertion point is not in the range 1 to (n+1) then NoSuchFieldException
    //! is thrown.
    RecordType * insertRecord(int position, RecordType * insertRecord)
      throw (NameClashException, InsertPositionException);
 
    //+ METHOD RecordType* unionWithRecord RecordType*
    //! Constructs a union of the two types by adding fields from the argument
    //! not already in the type instance this method is invoked on.
    //! A TypeException is thrown if there is a field in both types with the 
    //! same name but different type.
    RecordType * RecordType::unionWithRecord(RecordType * insertrec)
      throw (TypeException);
    
    //+ METHOD RecordType* selectFields vector<int>*
    //! Constructs a new record type by selecting the fields specified in the 
    //! vector on field indices.  No duplicate fields are allowed, and all fields
    //! must be present in the existing object.
    RecordType * selectFields(vector<int> * selection)    
      throw (NoSuchFieldException, DuplicateFieldException);
  
    //+ METHOD RecordType* selectFields vector<string>*
    //! Constructs a new record type by selecting the fields specified in the 
    //! vector of names.  No duplicate fields are allowed, and all fields
    //! must be present in the existing object.
    RecordType * selectFields(vector<string>* selection)    
      throw (NoSuchFieldException, DuplicateFieldException);    
    
    //+ METHOD RecordType* renameField string string
    //! Renames the field specified by the first argument to the name in the
    //! second argument. 
    RecordType * renameField(string oldname, string newname) 
     throw (NoSuchFieldException, NameClashException);
  
    //+ METHOD RecordType* renameField int string
    //! Renames the field specified by the first argument to the name in the
    //! second argument. 
    RecordType * renameField(int fieldindex, string newname) 
     throw (NoSuchFieldException, NameClashException);
       
    //+ METHOD RecordType* moveField string int
    //! Moves a field (specified by name) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordType * moveField(string fieldname, int newposition)
     throw (NoSuchFieldException);
  
    //+ METHOD RecordType* moveField int int
    //! Moves a field (specified by index) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordType * moveField(int oldposition, int newposition)
     throw (NoSuchFieldException);
  
    //+ METHOD int getFieldIndex string
    //! Get the field number from a field name.
    int getFieldIndex(string fieldname) throw (NoSuchFieldException);
    
    //+ METHOD string getFieldName int
    //! Get the name of a field from a field number.
    string getFieldName(int fieldindex) throw (NoSuchFieldException);
  
    //+ METHOD bool isValidFieldIndex int
    //! Says whether the field index is legal for this type.
    bool isValidFieldIndex(int fieldindex);
  
    //+ METHOD bool isValidFieldName string
    //! Says whether a field with this name exists in the type
    bool isValidFieldName(string fieldname);
  
    //+ METHOD BasicType* getFieldType int
    //! Get the type of a field specified by index.
    BasicType * getFieldType(int fieldindex) throw (NoSuchFieldException);
  
    //+ METHOD BasicType* getFieldType string
    //! Get the type of a field specified by name.
    BasicType * getFieldType(string fieldindex) throw (NoSuchFieldException);
    
    //+ METHOD int numFields
    //! Returns the total number of fields in the type.
    int numFields();
    
    //+ METHOD bool equals RecordType*
    //! Type equality -- both types must have the same (name,type) pairs.
    //! Fields need not be in the same order.
    bool equals (RecordType *);
  
    //+ METHOD bool orderedEquals RecordType*
    //! Ordered equality -- both types must have the same (name, type) pairs,
    //! and the fields must be in the same order.
    bool orderedEquals(RecordType *);
  
    //+ METHOD bool isSubset RecordType*
    //! Is the argument a subset of the existing type? 
    //! No equality of order implied.
    bool isSubset (RecordType *);
  	
    //+ METHOD bool isPrefix RecordType*
    //! Type inclusion -- prefix.  
    bool isPrefix (RecordType *);
  
    //+ METHOD string toString
    //! Returns a string representation of the type.
    string toString();
        
  private:
    /// Holds the name of each field.
    vector<string> record_fieldnames;
    
    /// Holds the type of each field in the RecordType.
    vector<BasicType *> record_fieldtypes;
  };  
}

#endif
	
