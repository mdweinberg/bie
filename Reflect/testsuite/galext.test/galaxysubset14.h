#ifndef BasicType_h
#define BasicType_h

#include <string>

namespace BIE {

  //+ CLASS BasicType
  //! Type descriptors for basic types: string, real, integer, and boolean.  
  //! In CLI, there are keywords describing these types: string, int,
  //! real and bool.
  ///
  /// \see RecordType

  class BasicType {
  
  public:
    /// Constructs a new type with the given name.  This is just a label
    /// used for printing/debugging purposes.  All the standard types
    /// are defined as static (class) variables, so this constructor is 
    /// should generally not be used.
    BasicType(string newtypename);
    
    //+ METHOD bool equals BasicType*
    //! Checks if two types are equal.
    bool equals(BasicType * type);
  
    //+ METHOD string toString
    //! Returns a string describing the type.
    string toString();
    
    /// The object representing the string type.
    static BasicType * String;
  
    /// The object representing the int type.
    static BasicType * Int;
  
    /// The object representing the real type.
    static BasicType * Real;
  
    /// The object representing the bool type.
    static BasicType * Bool;
  
  private:
    /// Name of the type set by constructor.
    string type_name;
  
    ///The objects that the publically available types point to.
    //@{
    static BasicType StringObj;
    static BasicType IntObj;
    static BasicType RealObj;
    static BasicType BoolObj;
    //@}
  };
}

#endif  
