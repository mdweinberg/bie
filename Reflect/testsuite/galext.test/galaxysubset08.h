#ifndef RecordInputStream_h
#define RecordInputStream_h

#include <RecordStream.h>
#include <RecordStreamFilter.h>

namespace BIE {

  //+ CLASS RecordInputStream SUPER RecordStream
  //! Record input streams are used to read a series of records from one or 
  //! more sources.  New record input streams can be created by reading from a
  //! data source (e.g. a file, or a network socket), by concatenating two
  //! streams, and by inheriting and modifying an existing stream.
  //!
  //! Inheriting streams constructs an implicit tree structure, in which the
  //! leaves are raw data sources, nodes with two children are where streams
  //! are combined, and nodes with one child modify the stream in some way.
  //! Data is always pulled from the 'root' stream in this tree - this is done
  //! by recursively calling the nextRecord() method (calls recurse in a depth
  //! first manner).  Requiring that a node is only inherited at most once 
  //! (each node has at most one parent) means that only one node controls 
  //! the flow of data (trees branch towards the source).
  //!
  //! This class implements all operations on streams - field deletion, filtering,
  //! selection, etc.  All reader classes are subclasses of this class.
  class RecordInputStream : public RecordStream {
  
  public:
    /// Virtual destructor.  Frees memory and makes inherited streams 
    /// root streams again.
    ~RecordInputStream();
  
    //+ METHOD bool nextRecord
    /// Gets the next record from the input stream.  
    /// Returns true if a record was successfully obtained, and false otherwise.
    virtual bool nextRecord() throw (FileFormatException, TypeException);
  
    //+ METHOD bool eos
    //! Returns true if the end of this, or an inherited stream has been reached.
    virtual bool eos();
    
    //+ METHOD bool error
    //! Returns true if an error has occured in this, or an inherited stream
    virtual bool error();
  
    //+ METHOD bool isRootStream
    //! Returns true if the stream is a root stream
    bool isRootStream();
  
    /// @name Get value methods.
    /// These methods return the value contained in the buffer.  The type
    /// of the field must match the type required by the method and the buffer
    /// must have a value.
    //@{
    string getStringValue(int fieldindex)   
    throw (TypeException, NoValueException, NoSuchFieldException)
      { return rs_buffer->getStringValue(fieldindex); }
    
    int getIntValue (int fieldindex)   
    throw (TypeException, NoValueException, NoSuchFieldException)
      { return rs_buffer->getIntValue(fieldindex); }
    
    double getRealValue  (int fieldindex)   
    throw (TypeException, NoValueException, NoSuchFieldException)
      { return rs_buffer->getRealValue(fieldindex); }
    
    bool   getBoolValue  (int fieldindex)   
    throw (TypeException, NoValueException, NoSuchFieldException)  
      { return rs_buffer->getBoolValue(fieldindex); }
    
    string getStringValue(string fieldname) 
    throw (TypeException, NoValueException, NoSuchFieldException)
      { return rs_buffer->getStringValue(fieldname); }
    
    int getIntValue(string fieldname) 
    throw (TypeException, NoValueException, NoSuchFieldException)
      { return rs_buffer->getIntValue(fieldname); }
    
    double getRealValue (string fieldname) 
    throw (TypeException, NoValueException, NoSuchFieldException)
      { return rs_buffer->getRealValue(fieldname); }
    
    bool getBoolValue (string fieldname) 
    throw (TypeException, NoValueException, NoSuchFieldException)  
      { return rs_buffer->getBoolValue(fieldname); }
    //@}
    
    //+ METHOD RecordInputStream* deleteField string
    //! Produces a new stream with the specified field masked out.
    RecordInputStream* deleteField(string fieldname) 
      throw (NoSuchFieldException, StreamInheritanceException);
    
    //+ METHOD RecordInputStream* deleteField int
    //! Produces a new stream with the specified field masked out.
    RecordInputStream* deleteField(int fieldindex) 
      throw (NoSuchFieldException, StreamInheritanceException);
      
    //+ METHOD RecordInputStream* deleteRange int int
    //! Deletes a range of fields & inherits all other buffers from the existing
    //! stream.
    RecordInputStream * deleteRange (int startfield, int endfield) 
      throw (NoSuchFieldException, BadRangeException, StreamInheritanceException);
     
    //+ METHOD RecordInputStream* joinWithStream int RecordInputStream*
    //! Stream Concatination.  The stream is "inserted" into the existing
    //! stream at the specified position.
    RecordInputStream * joinWithStream(int position, RecordInputStream * stream)
     throw (NameClashException, NoSuchFieldException, StreamInheritanceException);
        
    //+ METHOD RecordInputStream* selectFields RecordType*
    //! Creates a new stream containing only the fields specified by the type
    //! descriptor.  All the fields in the type descriptor must be present
    //! for this call to succeed.  Both field name and field type must match.
    RecordInputStream * selectFields(RecordType * selection)      
      throw (NoSuchFieldException, StreamInheritanceException);
      
    //+ METHOD RecordInputStream* selectFields vector<int>* 
    //! Creates a new input stream containing only the fields specified by 
    //! the field indices.  All indices must be within the bounds of the buffer.
    //! No duplicate indices are allowed.
    RecordInputStream * selectFields(vector<int>* selection)      
      throw (NoSuchFieldException, DuplicateFieldException, StreamInheritanceException);
  
    //+ METHOD RecordInputStream* selectFields vector<string>* 
    //! Creates a new stream containing only the fields specified by the field
    //! names.  A NoSuchFieldException is thrown if a name is not present in
    //! the buffer type. No duplicate names are allowed.
    RecordInputStream * selectFields(vector<string>* selection)    
      throw (NoSuchFieldException, DuplicateFieldException, StreamInheritanceException);
  
    //+ METHOD RecordInputStream* renameField string string
    //! Renames the the specified field.
    RecordInputStream * renameField(string oldname, string newname) 
     throw (NoSuchFieldException, StreamInheritanceException);
  
    //+ METHOD RecordInputStream* renameField int string
    //! Renames the the specified field.
    RecordInputStream * renameField(int fieldindex, string newname) 
     throw(NoSuchFieldException, StreamInheritanceException);
       
    //+ METHOD RecordInputStream* moveField string int
    //! Moves a field (specified by name) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordInputStream * moveField(string fieldname, int newposition)
     throw (NoSuchFieldException, StreamInheritanceException);
  
    //+ METHOD RecordInputStream* moveField int int
    //! Moves a field (specified by index) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordInputStream * moveField(int oldposition, int newposition)
     throw (NoSuchFieldException, StreamInheritanceException);
  
    //+ METHOD RecordInputStream* filterWith int RecordStreamFilter*
    //! Pipes the input through a filter  which can add new fields based on 
    //! the values of the fields in the existing stream.  The filter must 
    //! be "useable".
    RecordInputStream *  filterWith (int position, RecordStreamFilter * filter)
      throw (UnusableFilterException, TypeException,
             InsertPositionException, StreamInheritanceException);
  
    //+ METHOD string toString
    //! Returns a string representation of the stream.
    virtual string toString();
    
  protected:
    /// Called when another stream inherits part of the buffer of this stream
    void noLongerRoot();
    
    /// Called when a stream becomes a root stream (at creation time or after
    /// an inheriting stream is deleted).
    void setAsRoot();
    
    /// A list of the streams this stream inherits.
    vector<RecordInputStream *> ris_inherited;
  
  private:
    /// A method used to construct a new stream from the existing stream
    /// and a (presumably partly inherited) record buffer.
    /// The existing stream must be a root stream.
    RecordInputStream * createDerivative (RecordBuffer * newbuffer)
      throw (StreamInheritanceException);
  
    /// True if this stream is a root stream (not inherited by any other stream.
    bool ris_isroot;
    
    /// A filter.  Set to 0 if no filter is being used by the object.
    RecordStreamFilter * ris_filter;
  };
}

#endif
