
#ifndef RecordOutputStream_Binary_h
#define RecordOutputStream_Binary_h

#include <fstream>
#include <RecordInputStream.h>
#include <RecordOutputStream.h>
#include <byteswap.h>

namespace BIE {

  //+ CLASS RecordInputStream_Binary SUPER RecordInputStream
  //! Reads record input streams from a binary file.  The format
  //! of these files is as follows:
  //! hack
  class RecordInputStream_Binary : public RecordInputStream {
    
  public:
    //+ CONSTR RecordType* string
    //! Opens the file with the given name, and creates a record input stream
    //! using the type information provided.
    RecordInputStream_Binary (RecordType * type, string filename)
        throw (FileException);

    //+ CONSTR RecordType* istream*
    //! Create an input stream using the type provided by the caller and
    //! the (already open) binary input stream.
    RecordInputStream_Binary(RecordType * type, istream * binarystream);

    //+ CONSTR string
    //! Open the file with the specified name, and extract the type information 
    //! from a tag at the beginning of the file.
    RecordInputStream_Binary (string filename)
        throw (FileException, FileFormatException, NameClashException);
  
    //+ CONSTR RecordType* istream*
    //! Create an input stream using the (already open) binary input stream.
    //! and extract the type information from the current point in the stream.
    RecordInputStream_Binary(istream * binarystream) 
      throw (FileFormatException, NameClashException);
  
    /// Destructor.  Closes file if this class opened it.
    ~RecordInputStream_Binary();
    
    // Reads next record from file and puts the values in the record buffer.
    bool nextRecord() throw (FileFormatException);
    
    //+ METHOD bool eos
    //! Returns true if the end of the binary file/stream has been reached.
    bool eos();

    //+ METHOD bool error
    // Returns true if an error has occured while reading the file/stream
    bool error();

    //+ METHOD string toString
    //! Returns a string representation of the stream.
    string toString();
    
  private:
    // This reads the meta data describing the type of file contents.
    RecordType * readMetaData() throw (FileFormatException, NameClashException);
  
    // Performs common initialization.
    void initialize();
  
    // Helper functions for reading files.
    void readStream(unsigned char * buffer, int bytes);
    int readInt();
    double readReal();
    bool readBool();
    const char * readString();
  
    /// The file being accessed.
    istream * risb_binarystream;
    
    /// True if we created the input stream.
    bool risb_streamismine;
    
    char * risb_stringbuffer;
    int    risb_stringbuffersize;
    
    bool risb_eof;
    bool risb_error;  
  };
  
  //+ CLASS RecordOutputStream_Binary SUPER RecordOutputStream
  //! A record output stream that writes to a binary file.  The files are 
  //! written in the same format read by the RecordInputStream_Binary class.
  class RecordOutputStream_Binary :  public RecordOutputStream{
    
  public:
    //+ CONSTR RecordOutputStream* string bool
    //! Constructs an output stream which writes all records pushed to the
    //! specified stream into a file with the given name.  If the final argument
    //! field is set to true, then a type-tag is written at the beginning of the
    //! file.
    RecordOutputStream_Binary 
    (RecordOutputStream * stream, string filename, bool writemetadata)
        throw (FileException, StreamInheritanceException);
    
    //+ CONSTR RecordOutputStream* ofstream* bool
    //! Constructs a new record output stream which writes all records pushed to 
    //! the specified record stream to specified output stream (possibly a 
    //! file or network connection).  If the final argument
    //! field is set to true, then a type-tag is written to the stream.
    RecordOutputStream_Binary
    (RecordOutputStream * stream, ostream * binarystream, bool writemetadata)
      throw (StreamInheritanceException);
      
    /// Closes the output file if it was opened by this class.
    ~RecordOutputStream_Binary();
  
    /// Writes out the current record to the file.  
    virtual void pushRecord() throw (NoValueException, EndofStreamException);
 
    //+ METHOD string toString
    //! Returns a string representation of the stream.
    virtual string toString();
   
  private:
    // Common initialization for new objects.
    void initialize (RecordOutputStream * stream, bool writemetadata)
      throw (StreamInheritanceException);
   
    // hack
    void writeInt(int value);
    void writeReal(double value);
    void writeBool(bool value);
    void writeString(string value);
  
    // Writes out the meta data that describes the type of a file's contents.
    void writeMetaData();
  
    bool rosb_streamismine;
    
    // The output file.
    ostream * rosb_outputstream;
  };
}

#endif
