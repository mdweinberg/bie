
#ifndef FullTessellation_h
#define FullTessellation_h

#include <vector>

#include <Tile.h>
#include <Node.h>
#include <Frontier.h>
#include <TreeResolutionHeuristic.h>
#include <RecordInputStream.h>

namespace BIE {

  class Frontier;
  class TreeResolutionHeuristic;

  //+ CLASS FullTessellation
  //! An immutable tessellation of tiles with a mutable frontier.
  class FullTessellation {
    
  public:
    /// Destructor.
    virtual ~FullTessellation();

    //+ METHOD Tile* GetTile int
    //! Retrieve a tile given a tileid
    virtual Tile* GetTile(int tileid);
    
    //+ METHOD Tile* FindInFrontier double double
    //! Find a tile on the frontier with the given coordinates
    virtual Tile* FindInFrontier(double x, double y);
    
    /// Find all tiles in the tessellation tree with the given coordinates
    virtual void FindAll(double x, double y, vector<int> &found);
    
    /// Return the tile ids of the tree roots in the tessellation forest.  
    /// QuadTree  and Kd have forests with size one.  All MappedGrid
    /// are "root" nodes.
    virtual vector<int> GetRootTiles() = 0;

    /// Return the roots of the trees in the tessellation forest.  QuadTree
    /// and Kd have forests with size one.
    virtual vector<Node*> GetRootNodes() = 0;

    /// Return the frontier of the tree
    virtual vector<int> ExportFrontier();

    //+ METHOD void MutateFrontier FrontierMutator*
    //! Modify the frontier of the tree
    virtual void MutateFrontier(FrontierMutator *mut);
    
    //+ METHOD void RetractToTopLevel
    //! Retract the frontier so that it is at its most general.
    virtual void RetractToTopLevel();
    
    //+ METHOD void UpDownLevels int
    //! Expand (N) or contract (-N) frontier by N levels
    virtual void UpDownLevels(int n);
    
    //+ METHOD void SelectFrontier FunctionEvaluator*
    //! Modifies the frontier by selecting nodes specified by the evaluator.  
    //! Where tiles overlap, the most general tiles get preference.
    virtual void SelectFrontier(FunctionEvaluator * evaluator);
    
    //+ METHOD bool IncreaseResolution TreeResolutionHeuristic* int
    //! Increases the resolution of the frontier where recommended by heuristic.
    virtual bool IncreaseResolution 
                 (TreeResolutionHeuristic * heuristic, int numlevels = 1);

    //+ METHOD bool IsValidTileID int
    //! Returns true if the tile id is valid for this tessellation
    virtual bool IsValidTileID(int tileid);
    
    //+ METHOD int MinID 
    //! Returns the lowest tile ID used by the tessellation.
    virtual int MinID() { return 0; }
    
    //+ METHOD int MaxID 
    //! Returns the highest tile ID used by the tessellation.
    //! We really need an iterator for all tile ids.
    virtual int MaxID() { return NumberTiles() - 1; }
    
    //+ METHOD int NumberTiles
    //! Returns the total number of tiles in this tessellation.
    virtual int NumberTiles();
    
    /// Print the trees underneath the provided root nodes in
    /// pre-order.
    void PrintPreOrder(vector<Node*> rootnodes);
    
    /// Print the tessellation tree in pre-order.
    void PrintPreOrder();

    //+ METHOD int CurrentID
    //! Get tileid of current tile
    virtual int CurrentID();
    
    /// Returns a reference to the frontier.
    Frontier * GetFrontierReference() { return _frontier; }

    /**@name Iterator operations */
    //@{
    //+ METHOD Tile* First
    virtual Tile* First();
    //+ METHOD Tile* Last
    virtual Tile* Last();
    //+ METHOD Tile* Next
    virtual Tile* Next();
    //+ METHOD Tile* CurrentItem
    virtual Tile* CurrentItem();
    //+ METHOD bool IsDone
    virtual bool IsDone(); 
    //+ METHOD void Reset
    virtual void Reset();
    //@}

    protected:
    /// Constructor only called by subclasses.
    FullTessellation() { _frontier = 0; }
    
    /// Tileid indexed list of tiles
    map<int, Tile*> _tiles;
    
    /// Frontier of the tree
    Frontier *_frontier;
    
    /// Total number of tiles.
    int Ntiles;
    
    /// Prints out the tessellation tree to standard output.
    void PrintPreOrder(Node *tree);

    /// Find all children of a node containing the given coordinates
    void findall(double, double, vector<int>&, Node*);
    
    /// Data structure to hold sampled data.
    typedef struct twodcoordsstruct {double x; double y; } twodcoords;
    
    /// Samples a specified percentage of the input stream, and puts the 
    /// sample points into a vector.  
    void compute_sampling (RecordInputStream *, vector<twodcoords>*, 
      double pts, double minx, double maxx, double miny, double maxy);

    /// Compares the x values in two twodcoords structures.
    struct compx {bool operator()(const twodcoords &a, const twodcoords &b) const
		     { return a.x<b.x; } };

    /// Compares the x values in two twodcoords structures.
    compx compX;

    /// Compares the y values in two twodcoords structures.
    struct compy { bool operator()(const twodcoords &a, const twodcoords &b) const
		     { return a.y<b.y; } };

    /// Compares the y values in two twodcoords structures.
    compy compY;
  };    
  
}

#endif
