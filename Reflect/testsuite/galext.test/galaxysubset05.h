#ifndef RecordBuffer_h
#define RecordBuffer_h

#include <RecordType.h>

namespace BIE {

  //+ CLASS RecordBuffer
  //! Holds buffers for each field in a record type.
  ///
  /// \todo The destructor should eventually delete the typed buffers, but this
  /// will require a way of determining whether everyone else has finished using
  /// a typed buffer.  The typed buffers are generally small so this is not crucial.
  class RecordBuffer {
    
    public:
    //+ CONSTR
    //! Creates an empty RecordBuffer.
    RecordBuffer() { record_type = new RecordType(); }
  
    //+ CONSTR RecordType*
    //! Creates a RecordBuffer object with the specified record type.
    RecordBuffer(RecordType * rt);
      
    /// Destructor: Cleans up the allocated memory.  
    ~RecordBuffer() { delete record_type; }
  
    //+ METHOD RecordBuffer* deleteField string
    //! Delete a field specified by name and inherit all other buffers.
    RecordBuffer * deleteField (string fieldname) throw (NoSuchFieldException);

    //+ METHOD RecordBuffer* deleteField int
    //! Delete a field specified by field index and inherit all other buffers.
    RecordBuffer * deleteField (int fieldindex) throw (NoSuchFieldException);
    
    //+ METHOD RecordBuffer* deleteRange int int
    //! Delete a range of fields & inherit all other buffers. 
    RecordBuffer * deleteRange (int startfield, int endfield) 
       throw (NoSuchFieldException, BadRangeException);
     
    //+ METHOD RecordBuffer* insertField int string BasicType*
    //! Insert a new field.  A new TypedBuffer with the specified type is created
    //! and all others are inherited. This method can be used to append fields 
    //! by inserting at position (n+1).  The name must not be present already.
    RecordBuffer * insertField (int position, string name, BasicType * type) 
      throw (NameClashException, InsertPositionException);
    
    //+ METHOD RecordBuffer* insertRecord int RecordBuffer*
    //! Inserts a new RECORD buffer.  Typed buffers from both record buffers
    //! are inherited.   Can be used to append fields by 
    //! inserting at position (n+1).  None of the names in the record type being
    //! inserted can be present already. 
    RecordBuffer * insertRecord(int position, RecordBuffer * insertRecord)
     throw (NameClashException, InsertPositionException);
        
    //+ METHOD RecordBuffer* selectFields RecordType*
    //! Creates a new buffer containing only the fields specified by the type
    //! descriptor.  All the fields in the type descriptor must be present
    //! for this call to succeed.  Both field name and field type must match.
    RecordBuffer * selectFields(RecordType * selection)      
        throw (TypeException);
      
    //+ METHOD RecordBuffer* selectFields vector<int>*
    //! Creates a new buffer containing only the fields specified by the field
    //! indices.  All indices must be within the bounds of the buffer.
    //! No duplicate indices are allowed.
    RecordBuffer * selectFields(vector<int>* selection)      
        throw (NoSuchFieldException, DuplicateFieldException);
  
    //+ METHOD RecordBuffer* selectFields vector<string>*
    //! Creates a new buffer containing only the fields specified by the field
    //! names.  A NoSuchFieldException is thrown if a name is not present in
    //! the buffer type. No duplicate names are allowed.
    RecordBuffer * selectFields(vector<string>* selection)    
      throw (NoSuchFieldException, DuplicateFieldException);
  
    //+ METHOD RecordBuffer* renameField string string
    //! Renames the field specified by the first argument to the name
    //! given by the second argument.
    RecordBuffer * renameField(string oldname, string newname) 
     throw (NoSuchFieldException, NameClashException);

    //+ METHOD RecordBuffer* renameField int string
    //! Renames the field specified by the first argument to the name
    //! given by the second argument.
    RecordBuffer * renameField(int fieldindex, string newname) 
     throw(NoSuchFieldException, NameClashException);
       
    //+ METHOD RecordBuffer* moveField string int
    //! Moves a field (specified by name) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordBuffer * moveField(string fieldname, int newposition)
     throw (NoSuchFieldException);

    //+ METHOD RecordBuffer* moveField int int
    //! Moves a field (specified by index) to a new position (specified by index)
    //! and "slides" the fields in between to fill in the vacated position.  
    RecordBuffer * moveField(int oldposition, int newposition)
     throw (NoSuchFieldException);
        
    //+ METHOD TypedBuffer* getFieldBuffer int
    //! Returns the buffer of the field (specified by field index).
    TypedBuffer * getFieldBuffer(int fieldindex) throw (NoSuchFieldException);

    //+ METHOD TypedBuffer* getFieldBuffer int
    //! Returns the buffer of the field (specified by field name).
    TypedBuffer * getFieldBuffer(string fieldname) throw (NoSuchFieldException);
    
    /**
    * @name Value retrieving methods.
    * These methods return the value contained in the buffer.  The type
    * of the field must match the type required by the method and the buffer
    * must have a value.
    */
    //@{
    string getStringValue(int fieldindex)   
      throw (TypeException, NoValueException, NoSuchFieldException);
    int    getIntValue   (int fieldindex)   
      throw (TypeException, NoValueException, NoSuchFieldException);
    double getRealValue  (int fieldindex)   
      throw (TypeException, NoValueException, NoSuchFieldException);
    bool   getBoolValue  (int fieldindex)   
      throw (TypeException, NoValueException, NoSuchFieldException);  
    string getStringValue(string fieldname) 
      throw (TypeException, NoValueException, NoSuchFieldException);
    int    getIntValue   (string fieldname) 
      throw (TypeException, NoValueException, NoSuchFieldException);
    double getRealValue  (string fieldname) 
      throw (TypeException, NoValueException, NoSuchFieldException);
    bool   getBoolValue  (string fieldname) 
      throw (TypeException, NoValueException, NoSuchFieldException);  
    //@}
    
    /**
    * @name Value setting methods.
    * These methods are used to set the value of a field.  The type
    * of the field must match the type required by the method.
    */
    //@{
    void setStringValue(int fieldindex, string value)   
      throw (TypeException, NoSuchFieldException);
    void setIntValue   (int fieldindex, int  value)   
      throw (TypeException, NoSuchFieldException);
    void setRealValue  (int fieldindex, double value)   
      throw (TypeException, NoSuchFieldException);
    void setBoolValue  (int fieldindex, bool value)   
      throw (TypeException, NoSuchFieldException);  
    void setStringValue(string fieldname, string value) 
      throw (TypeException, NoSuchFieldException);
    void setIntValue   (string fieldname, int value) 
      throw (TypeException, NoSuchFieldException);
    void setRealValue  (string fieldname, double value) 
      throw (TypeException, NoSuchFieldException);
    void setBoolValue  (string fieldname, bool value) 
      throw (TypeException, NoSuchFieldException);  
    //@}
  
    //+ METHOD bool hasValue string
    //! The returns true if the field specified by name contains a value.
    bool hasValue(string fieldname) throw (NoSuchFieldException);
    
    //+ METHOD bool hasValue int
    //! The returns true if the field specified by index contains a value.
    bool hasValue(int fieldindex) throw (NoSuchFieldException);
    
    //+ METHOD bool hasValue
    //! Returns true if ALL fields have a value.
    bool hasValue();
    
    //+ METHOD void reset
    //! Forgets all values the buffer may be holding 
    void reset();
  
    //+ METHOD int getFieldIndex string
    //! Get the field number from a field name.
    int getFieldIndex(string fieldname) throw (NoSuchFieldException);
    
    //+ METHOD string getFieldName int
    //! Get the name of a field from a field number.
    string getFieldName(int fieldindex) throw (NoSuchFieldException);
  
    //+ METHOD bool isValidFieldIndex int
    //! Says whether the field index is legal for this type.
    bool isValidFieldIndex(int fieldindex);
  
    //+ METHOD bool isValidFieldName string
    //! Says whether a field with this name exists in the type
    bool isValidFieldName(string fieldname);
  
    //+ METHOD BasicType* getFieldType int
    //! Get the type of a field specified by number.
    BasicType * getFieldType(int fieldindex) throw (NoSuchFieldException);
  
    //+ METHOD BasicType* getFieldType string
    //! Get the type of a field specified by name.
    BasicType * getFieldType(string fieldindex) throw (NoSuchFieldException);
    
    //+ METHOD int numFields
    //! Returns the total number of fields in the type.
    int numFields();
    
    //+ METHOD string toString
    //! Returns a string representation of the type.
    string toString();
    
    //+ METHOD RecordType* getType
    //! Returns the record type descriptor.
    RecordType * getType();
  
    private:    
    /// This holds each typed buffer making up the record buffer.
    vector<TypedBuffer *> record_buffers;
    
    /// The type of this buffer.
    RecordType   * record_type;
  };
}

#endif
