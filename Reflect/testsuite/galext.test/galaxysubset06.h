#ifndef MappedGrid_h
#define MappedGrid_h

#include <string>
#include <map>

#include <FullTessellation.h>

namespace BIE {

  //+ CLASS MappedGrid SUPER FullTessellation
  //! Simple rectangular, non-hierarchical, tessellation defined according 
  //! to an a priori mapping.
  class MappedGrid : public FullTessellation 
  {
  public:
    //+ CONSTR Tile* double double double double int int
    MappedGrid(Tile * factory, double minx, double maxx, 
               double miny, double maxy, int nx, int ny);
    
    ~MappedGrid();

    /// Returns the tile id of the node with the given coordinates (there
    /// is only one in a mapped grid).  This is faster than the default 
    /// method in FullTessellation.
    void FindAll(double x, double y, vector<int> &found);

    //+ METHOD Tile* FindInFrontier double double
    //! Find a tile on the frontier with the given coordinates.  
    virtual Tile* FindInFrontier(double x, double y);

    // Redefine frontier mutating method to do nothing, as mutation will 
    // have no effect (saves pointless method calls).
    void MutateFrontier(FrontierMutator *mut){}
    
    // Redefine the method for increasing resolution as it will have no
    // effect (saves a pointless method call).
    bool IncreaseResolution (TreeResolutionHeuristic * h, int n){return false;}

    /// Returns a list of all tile IDs - all tiles are root tiles.
    vector<int> GetRootTiles() { return roottiles; }
    /// Returns a list of all nodes, each a root node with no children.
    vector<Node*> GetRootNodes() { return rootnodes; }
  
  private:
    /// These define the boundary of the tessellation.
    double Xmin, Xmax, Ymin, Ymax;
    /// These define the length and breadth of each tile.
    double dX, dY;
    /// The number of tiles in each dimension.
    int nx, ny;
    /// The tile factory.
    Tile * tilefactory;
    /// These hold all the tiles and corresponding nodes in the tessellation.
    /// since all tiles are root tiles in mapped grid.
    vector<int>   roottiles;
    vector<Node*> rootnodes;
  };
}
#endif
