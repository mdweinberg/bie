
//+ METHOD void setSomething int double
//! Comment for misplaced method.

//+ CONSTR int double
//! Comment for misplaced constructor.
//! Comment for misplaced constructor.
//* Depreciated comment for misplaced constructor

//+ CLICONSTR int double
//* Depreciated comment for misplaced constructor

//+ CLIMETHOD void misplaced
/** 
* C-style comment associated with misplaced method.
*/

//+ CLICLASS AClass
//! AClass description.
//+ CLIMETHOD void methodone int double
//! method documentation.
//+ METHOD void methodone int double
//* depricated but allowed documentation
//+ CLICONSTR int double double double
//! constructor documentation.
//+ METHOD int complex
/**
* Complex method computes:
*  $ \frac{4}{3} $
* \see whatever
*       Formatting should stay ths same.
*            nomatter what.
*/

//+ CLASS ClassWithNoDocumentation

//+ METHOD void MethodWithNoDocumentation

//+ CONSTR ConstructorWithNoDocumentation

//+ METHOD

//+ METHOD void 

//+ CLICLASS ClassWithNoDocumentation

//+ CLIMETHOD void MethodWithNoDocumentation

//+ CLICONSTR ConstructorWithNoDocumentation

//+ CLIMETHOD

//+ CLIMETHOD void 

//+ GLOBAL int globaltest, globaltest2 ,   globaltest3

//+ GLOBAL 

//+ GLOBAL double

//+ GLOBAL char* anotherglobaltest

//+ BADCOMMENTTYPE hello 

//+ GLOBALISH int variable

//+ CONSTRUCTOR int double

//+GLOBAL int hello

//+	GLOBAL int anotherhello

//! Stranded comment in the middle of nowhere.

//* Depricated comment in the middle of nowhere.

/**
* C-style comment in the middle of nowhere.  This should not generate a 
* warning.
*/



