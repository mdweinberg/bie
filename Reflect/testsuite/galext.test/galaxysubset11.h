
#ifndef RecordStreamFilter_h
#define RecordStreamFilter_h

#include <RecordStream.h>

namespace BIE {

  //+ CLASS RecordStreamFilter
  //! RecordStreamFilter objects can insert new fields into a stream
  //! by performing some computation on existing fields.  This class defines the 
  //! interface to all filters (and implements much of the functionality).  
  //!
  //! Using filters:
  //! Once a filter has been created, the filter must be hooked up to 
  //! a stream. This involves the following steps:
  //!
  //! -- Connection
  //! The inputs to the filter are connected to fields in the stream
  //! the filter is going to be attached to using the connect() methods.  
  //! Inputs can be either a scalar or a set - meaning that one stream 
  //! field, or one or more stream fields can be connected respectively.
  //! If the filter input is a single field and is already connected the 
  //! existing connection is overwritten.  If the filter input field is a set 
  //! field then the stream field is added to the set (unless already in it).
  //! Filter inputs are typed and the type must match with the stream field
  //! being connected.  Connections can be deleted using the disconnect() 
  //! methods.  Stream fields can be specified by name or index, and 
  //! filter inputs also by name or index (starting at 1).
  //! 
  //! -- Output field renaming
  //! Filters give default names to their output fields, but these might
  //! clash with field names in the stream being filtered (for instance,
  //! if the same filter was used more than once on the same stream).
  //! Output fields are renamed using the renameOutputField() methods.
  //!
  //! -- Attaching to the stream
  //! The final stage attaches the stream to the filter - this is done using 
  //! the filterWith() methods in the stream classes.  
  
  /**
  * Writing a filter class:
  * 
  * Subclasses need only implement code which defines the input and output, 
  * implements a constructor, and performs the computation.
  * 
  * - Defining input and output
  * Filter input and output is defined using arrays of structures that are
  * terminated with a NULL structure (all fields set to zero).
  * For each input, the filter class must specify the name, the BasicType,
  * and the format (set or scalar, where true corresponds to set).  
  * For each output, the filter class should specify the default name of the
  * output, and the BasicType.  The arrays defining the input and output must 
  * exist for the lifetime of the filter, since they are used (not copied) 
  * by this class.  If the inputs and 
  * outputs do not vary, then these arrays should be both const and static* 
  * 
  * - Writing a constructor:
  * All subclass constructors must call the initialize() method in the 
  * constructor.  Three variables are passed to this method - the array 
  * describing the input to the filter, the array 
  * describing the output from the filter, and the stream which is to be filtered.
  * 
  * - Performing the computation.
  * The computation should set the values of the filter output fields.
  * The methods set<Type>Output will set the value of output fields.
  * To read input field values, use the get<Type>Input methods.  When
  * an input is a set input, then the index of the item in the set must
  * must also be specified.
  */  
  class RecordStreamFilter {
  
  public:
    //+ METHOD void connect string string
    //! Connects a field from the incoming stream to an input of the filter.
    //! The first argument is the name of the field in the incoming stream,
    //! and the second is the name of the field in the filter.
    void connect(string streamfieldname, string inputname) 
      throw (NoSuchFieldException, TypeException, 
             AttachedFilterException, NoSuchFilterInputException);

    //+ METHOD void connect int string
    //! Connects a field from the incoming stream to an input of the filter.
    //! The first argument is the index of the field in the incoming stream,
    //! and the second is the name of the field in the filter.
    void connect(int streamfieldindex, string inputname) 
      throw (NoSuchFieldException, TypeException, 
             AttachedFilterException, NoSuchFilterInputException);
    
    //+ METHOD void connect string int
    //! Connects a field from the incoming stream to an input of the filter.
    //! The first argument is the name of the field in the incoming stream,
    //! and the second is the index of the field in the filter.
    void connect(string streamfieldname, int inputindex) 
      throw (NoSuchFieldException, TypeException, 
             AttachedFilterException, NoSuchFilterInputException);
  
    //+ METHOD void connect int int
    //! Connects a field from the incoming stream to an input of the filter.
    //! The first argument is the index of the field in the incoming stream,
    //! and the second is the index of the field in the filter.
    void connect(int streamfieldindex, int inputindex) 
      throw (NoSuchFieldException, TypeException, 
             AttachedFilterException, NoSuchFilterInputException);
    
    //+ METHOD void disconnect string string
    //! Disconnects a stream field from a filter input.
    //! 1st argument: stream field name, 2nd argument: filter input name.
    void disconnect(string streamfieldname, string inputname) 
      throw (NoSuchFieldException, NoSuchConnection, 
             AttachedFilterException, NoSuchFilterInputException);
  
    //+ METHOD void disconnect int string
    //! Disconnects a stream field from a filter input.
    //! 1st argument: stream field index, 2nd argument: filter input name.
    void disconnect(int streamfieldindex, string inputname) 
      throw (NoSuchFieldException, NoSuchConnection, 
             AttachedFilterException, NoSuchFilterInputException);
    
    //+ METHOD void disconnect string int
    //! Disconnects a stream field from a filter input.
    //! 1st argument: stream field name, 2nd argument: filter input index.
    void disconnect(string streamfieldname, int inputindex) 
      throw (NoSuchFieldException, NoSuchConnection, 
             AttachedFilterException, NoSuchFilterInputException);
  
    //+ METHOD void disconnect int int
    //! Disconnects a stream field from a filter input.
    //! 1st argument: stream field index, 2nd argument: filter input index.
    void disconnect(int streamfieldindex, int inputindex) 
      throw (NoSuchFieldException, NoSuchConnection, 
             AttachedFilterException, NoSuchFilterInputException);
    
    //+ METHOD void disconnect string
    //! Disconnect all connections made to a specific filter input.
    void disconnect(string inputname) 
      throw (NoSuchFilterInputException, AttachedFilterException);

    //+ METHOD void disconnect int
    //! Disconnect all connections made to a specific filter input.
    void disconnect(int inputindex) 
      throw (NoSuchFilterInputException, AttachedFilterException);
      
    //+ METHOD void disconnect
    //! Disconnect all connections.
    void RecordStreamFilter::disconnect () throw (AttachedFilterException);

    //+ METHOD void renameOutputField int string
    //! Renames an output field.  Output fields are assigned default names
    //! which may have to be changed to aboid clashes with the stream input.
    //! 1st argument: index of output field, 2nd argument: the new name.
    void renameOutputField(int fieldindex, string newname)
      throw (NoSuchFilterOutputException, NameClashException); 

    //+ METHOD void renameOutputField string string
    //! Renames an output field.  Output fields are assigned default names
    //! which may have to be changed to aboid clashes with the stream input.
    //! 1st argument: the old name, 2nd argument: the new name.
    void renameOutputField(string fieldname, string newname)
      throw (NoSuchFilterOutputException, NameClashException); 
    
    //+ METHOD bool isConnected
    //! Returns true if all filter inputs have a connection
    bool isConnected();
  
    //+ METHOD bool isConnected int
    //! Returns true if the filter input (specified by index) has a connection
    bool isConnected(int inputindex) throw (NoSuchFilterInputException);

    //+ METHOD bool isConnected string
    //! Returns true if the filter input (specified by name) has a connection
    bool isConnected(string inputname) throw (NoSuchFilterInputException);
  
    //+ METHOD int getInputCardinality string
    //! Returns the number of connections an input has (this can only
    //! be one or zero for scalars, but can be larger for set inputs).
    int getInputCardinality(string inputname)throw (NoSuchFilterInputException);

    //+ METHOD int getInputCardinality string
    //! Returns the number of connections an input has (this can only
    //! be one or zero for scalars, but can be larger for set inputs).
    int getInputCardinality(int inputindex) throw (NoSuchFilterInputException);
    
    //+ METHOD bool isAttached
    //! Returns true if the filter is attached to a stream.
    bool isAttached();
  
    //+ METHOD bool nameClash
    //! Returns true if the output field names of the filter clash with
    //! names in the stream type.
    bool nameClash();
  
    //+ METHOD bool isUseable
    //! The default definition of a "useable filter" is:
    //! the filter is not already being used, all inputs are connected 
    //! and output names do not clash.  Certain filters might have
    //! more specific requirements, and they can override this method.
    virtual bool isUseable();
  
    //+ METHOD RecordBuffer* getBuffer
    //! Returns the output buffer of the filter.
    RecordBuffer * getBuffer();
    
    //+ METHOD RecordType* getOutputType
    /// Returns the type of the output buffer.
    RecordType   * getOutputType();
  
    //+ METHOD RecordType* getStreamType
    //! Returns the type of the stream which this filter can be attached to.
    RecordType * getStreamType();
    
    //+ METHOD int numInputs
    //! Returns the number of inputs (whether set or scalar).
    int numInputs();
    
    //+ METHOD int getInputIndex string
    //! Returns the index corresponding to a filter input name.
    int getInputIndex(string inputname) throw(NoSuchFilterInputException);
    
    //+ METHOD string getInputName int
    //! Returns the name of an input field when given the index.
    string getInputName(int inputindex) throw(NoSuchFilterInputException);
    
    //+ METHOD bool isValidInputName string
    //! Returns true if the string is the name of an input to this filter.
    bool isValidInputName(string inputname);
    
    //+ METHOD bool isValidInputIndex int
    //! Returns true if this is the index of an input to this filter.
    bool isValidInputIndex(int inputindex);
    
    //+ METHOD string toString
    //! Returns a string description of the filter.
    string toString();
    
/*******************************************************************************
* These are called only by input and output streams.  perhaps
* they should be protected, and accessed by "friends" only
*******************************************************************************/
    /// Called when new input is available by the inheriting stream, this
    /// fills the filter output buffers.
    virtual void compute() = 0;
    
    /// This puts a filter to use by attaching it to a stream.  
    /// The filter must be in a useable state, with inputs connected,
    /// no name clashes etc.
    void attachToStream(RecordStream * stream) 
      throw (UnusableFilterException, TypeException);
  
  protected:
    /// Structure that holds a description of a filter input
    typedef struct filterinputstruct {
      /// Name of this input field.
      char      * inputname;
      /// Type of this input field.
      BasicType * inputtype;    
      /// Is this a set or scalar field?  Set to true if this is a set.
      bool        isset;         
    } filterinputstruct;
  
    /// Structure that holds a description of a filter output field.
    typedef struct filteroutputstruct {
      /// Name of this output field.
      char      * outputname;
      /// Type of this output field.
      BasicType * outputtype;
    } filteroutputstruct;
  
    /// Initialize: Called by subclasses (ie actual filters) to initialize
    /// variables in this class.  The input and output specifications
    /// are arrays of structures terminated with a null record (all zero).
    void initialize(const filterinputstruct  * input, 
                    const filteroutputstruct * output,
  		  RecordStream             * stream) 
  		  throw (DuplicateFieldException);
  
    /// Get the default name of a filter field by specifying an index.
    string RecordStreamFilter::getDefaultOutputName(int outputindex) 
      throw(NoSuchFilterOutputException);
  
    /// Get the index of a filter field by specifying the default name.
    int RecordStreamFilter::getDefaultOutputIndex(string outputname) 
      throw(NoSuchFilterOutputException);
    
    /// Returns true if the string is the default name of an output field.
    bool RecordStreamFilter::isValidDefaultOutputName(string outputname);
  
    /// Returns true if the index refers to an output field of the filter.
    bool RecordStreamFilter::isValidOutputIndex(int outputindex); 
  
  
    /// These methods get the value of a scalar filter input.  Either the
    /// input name or input index can be used to specify the input field.
    /// An attempt to access a set input with these methods will result
    /// in a TypeException.
    //@{
    int    getIntInput   (string inputname)
      throw (NoSuchFilterInputException, TypeException);
    int    getIntInput   (int    inputindex)
      throw (NoSuchFilterInputException, TypeException);
    double getRealInput  (string inputname)
      throw (NoSuchFilterInputException, TypeException);
    double getRealInput  (int    inputindex)
      throw (NoSuchFilterInputException, TypeException);
    bool   getBoolInput  (string inputname)
      throw (NoSuchFilterInputException, TypeException);
    bool   getBoolInput  (int    inputindex)
      throw (NoSuchFilterInputException, TypeException);
    string getStringInput(string inputname)
      throw (NoSuchFilterInputException, TypeException);
    string getStringInput(int    inputindex)
      throw (NoSuchFilterInputException, TypeException);
    //@}
      
    /// These methods get the value of a member of a set input.  
    /// Either the input name or input index can be used to specify the 
    /// input field.  An attempt to access a scalar variable with these
    /// methods will result in a TypeException.
    //@{
    int    getIntInput   (string inputname,  int setindex)
      throw (NoSuchFilterInputException, TypeException);
    int    getIntInput   (int    inputindex, int setindex)
      throw (NoSuchFilterInputException, TypeException);
    double getRealInput  (string inputname,  int setindex)
      throw (NoSuchFilterInputException, TypeException);
    double getRealInput  (int    inputindex, int setindex)
      throw (NoSuchFilterInputException, TypeException);
    bool   getBoolInput  (string inputname,  int setindex)
      throw (NoSuchFilterInputException, TypeException);
    bool   getBoolInput  (int    inputindex, int setindex)
      throw (NoSuchFilterInputException, TypeException);
    string getStringInput(string inputname,  int setindex)
      throw (NoSuchFilterInputException, TypeException);
    string getStringInput(int    inputindex, int setindex)
      throw (NoSuchFilterInputException, TypeException);
    //@}
    
    /// @name Filter output field value setting methods.
    /// These methods set the value of a filter output. The field can be referred
    /// to by the original (default) name of the output, or by index.
    /// Indices start at 1.
    //@{
    void setIntOutput   (string inputname, int value)
      throw (NoSuchFilterOutputException, TypeException);
    void setIntOutput   (int    inputindex, int value)
      throw (NoSuchFilterOutputException, TypeException);
    void setRealOutput  (string inputname, double value)
      throw (NoSuchFilterOutputException, TypeException);
    void setRealOutput  (int    inputindex, double value)
      throw (NoSuchFilterOutputException, TypeException);
    void setBoolOutput  (string inputname, bool value)
      throw (NoSuchFilterOutputException, TypeException);
    void setBoolOutput  (int    inputindex, bool value)
      throw (NoSuchFilterOutputException, TypeException);
    void setStringOutput(string inputname, string value)
      throw (NoSuchFilterOutputException, TypeException);
    void setStringOutput(int    inputindex, string value)
      throw (NoSuchFilterOutputException, TypeException);
    //@}
    
  private:
    /// Buffer holding the output of the filter. 
    RecordBuffer * filter_outputbuffer;
  
    /// The type descriptor for the input stream.
    RecordType * filter_streamtype;
    
    /// The stream attached to the input of the filter.
    RecordStream * filter_stream;
  
    /// Set to true once a filter has been attached to a stream.
    bool filter_isattached;  
    
    /// The input specification - a null terminiated array provided by a subclass.
    filterinputstruct  * filter_input;
    /// The output specification - a null terminated array provided by a subclass.
    filteroutputstruct * filter_output;
  
    int filter_numinputs;
    int filter_numoutputs;
  
    /// Array holding details of connections.
    int ** filter_connections;
  
    /// Array holding details of the number of connections each input has.
    int *  filter_inputcardinality;
    
  };
}

#endif
