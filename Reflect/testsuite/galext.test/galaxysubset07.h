#ifndef RecordStream_h
#define RecordStream_h

#include <iostream>
#include <RecordBuffer.h>

namespace BIE {

  //+ CLASS RecordStream
  //! RecordStream:  An abstract class that defines basic operations
  //! like type queries and buffer lookup for streams of records (either input
  //! or output).
  class RecordStream {
  
  public:
    /// Virtual destructor.
    virtual ~RecordStream();
    
    //+ METHOD bool hasValue
    //!  Returns true if all fields in the stream have a value.
    bool hasValue();

    //+ METHOD TypedBuffer* getFieldBuffer int
    //! Get a reference to a particular field's buffer.
    TypedBuffer * getFieldBuffer(int fieldindex) 
      throw (NoSuchFieldException);

    //+ METHOD TypedBuffer* getFieldBuffer string
    //! Get a reference to a particular field's buffer.
    TypedBuffer * getFieldBuffer(string fieldname) 
      throw (NoSuchFieldException);

    //+ METHOD RecordBuffer* getBuffer
    //! Returns the record buffer for this data stream.
    RecordBuffer * getBuffer();
    
    //+ METHOD int getFieldIndex string
    //! Get the index of a field from a name.
    int getFieldIndex(string fieldname) throw (NoSuchFieldException);
    
    //+ METHOD string getFieldName int
    //! Get the name of a field from an index.
    string getFieldName(int fieldindex) throw (NoSuchFieldException);

    //+ METHOD bool isValidFieldIndex int
    //! Returns true if the specified index is within the range of allowed indices
    bool isValidFieldIndex(int fieldindex);

    //+ METHOD bool isValidFieldName string
    //! Returns true if the specified name is a name of a field in the stream
    bool isValidFieldName(string fieldname);

    //+ METHOD BasicType* getFieldType int
    //! Returns the type a field specified by index.
    BasicType * getFieldType(int fieldindex) throw (NoSuchFieldException);

    //+ METHOD BasicType* getFieldType string
    //! Returns the type of a field specified by name  
    BasicType * getFieldType(string fieldname)throw (NoSuchFieldException);

    //+ METHOD int numFields
    //! Gives the number of fields in this stream.
    int numFields();
    
    //+ METHOD RecordType* getType 
    //! Returns the record type descriptor for this stream
    RecordType * getType(); 
    
    //+ METHOD string toString
    //! Returns a string representation of the type
    virtual string toString() = 0;
   
  protected:
  
    /// The record buffer holding the most recently pulled values.
    RecordBuffer * rs_buffer;
  };
}
#endif
