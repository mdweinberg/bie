#ifndef RecordStream_Ascii_h
#define RecordStream_Ascii_h

#include <fstream>
#include <RecordInputStream.h>
#include <RecordOutputStream.h>
#include <AsciiLexer.h>
#include <errno.h>
#include <limits.h>

namespace BIE {

  //+ CLASS RecordInputStream_Ascii SUPER RecordInputStream
  //!
  //! Reads record input streams from ascii files.  The format of these 
  //! files is as follows:
  //!  - Each record is specified on its own line.  
  //!  - Field values are separated by either a space or a tab
  //!  - Strings are enclosed in double quotes.  Escape sequences are used to
  //!    include a newline or double quote in a string: \n represents a newline,
  //!    \" represents a double quote.  \\ is used for a backslash, and \t is
  //!    available for tab.
  //!    For example:  "Name: \"Galaxy\"\n"
  //!  - Boolean values are specified using either '1' or '0'.
  //!  - Integers are specified in base ten, and can optionally use a sign (+ or -)
  //!    Conversion is performed using the strtol library function.  Values in the
  //!    file which cause overflow or underflow will result in a FileFormatException.
  //!  - Reals are specified in fixed decimal or scientific form.  Both NaN and 
  //!    InF (case insensitive and optionally signed) are permitted values.
  //!    As with integers, values in the file which cause overflow or underflow 
  //!    will result in a FileFormatException.  Conversion (including any rounding) 
  //!    is performed using the library function strtod.
  //!  - Comments are formatted in a script like fashion using '#' - everything
  //!    on a line after a # is a comment.
  //!
  //! Where files include a type descriptor, it is expected at the very beginning 
  //! of the file in the following format:
  //!
  //! <type tag> ::= <field> [field]* <newline>
  //! <field>    ::= <type> <name> <newline>
  //! <type>     ::= string | int | real | bool  (In upper or lower case) 
  //! <name>     ::= a string in the format described above.
  //!
  //! Example:
  //! string "hole name"
  //! int    "hole number"
  //! real   "average score"
  //! bool   "double green"
  //!
  //! "Road" 17 4.567 0
  //! "Bobby Jones" 10 3.98 1
  //! "Hole 0'Cross" 13 4.12 1
  /// @see RecordOutputStream_Ascii
  class RecordInputStream_Ascii : public RecordInputStream{
    
  public:
    //+ CONSTR RecordType* string
    //! Open the file with the specified name, and create an input stream
    //! using the record type provided.
    RecordInputStream_Ascii (RecordType * type, string filename)
        throw (FileException);
    
    //+ CONSTR RecordType* istream*
    //! Create an input stream using the type provided by the caller and
    //! the (already open) input stream.
    RecordInputStream_Ascii(RecordType * type, istream * asciistream);
  
    //+ CONSTR string
    //! Open the file with the specified name, and extract the type information 
    //! from a tag at the beginning of the file.
    RecordInputStream_Ascii (string filename)
        throw (FileException, FileFormatException, NameClashException);
    
    //+ CONSTR RecordType* istream*
    //! Create an input stream using the (already open) input stream.
    //! and extract the type information from the current point in the stream.
    RecordInputStream_Ascii(istream * asciistream) 
      throw (FileFormatException, NameClashException);
  
    /**
    * Destructor: Closes the file stream if it was opened by this class
    * and close has not been called already.  Deletes all dynamically 
    * created objects.
    */
    ~RecordInputStream_Ascii();
    
    /// Reads next record from file and puts the values in the record buffer.
    /// Returns true if the operation was successful.
    bool nextRecord() throw (FileFormatException, TypeException);
    
    //+ METHOD bool eos
    //! Returns true if the end of the ascii file/stream has been reached.
    bool eos();
    
    //+ METHOD bool error
    //! Returns true if an error occured while reading this file/stream.
    bool error();
    
    //+ METHOD string toString
    //! Returns a string representation of the stream.
    string toString();
  
  private:
    /// This reads the meta data describing the type of file contents.
    RecordType * readMetaData() throw (FileFormatException, NameClashException);

    /// Method used to extract values from tokens, and set the appropriate field
    void setFieldValue (int fieldindex, int lexcode, const char * text)
    throw (FileFormatException);
  
    /// Common stream initialization.
    void initialize();
    
    /// The lexer object which reads the input file.
    AsciiLexer * risa_lexer;
    
    /// Flag to remember if we previously reached end of file.
    bool risa_eof;
    /// Flag to remember if we previously experienced an error.
    bool risa_error;  
  };
  
  //+ CLASS RecordOutputStream_Ascii SUPER RecordOutputStream
  //! A record output stream that writes to an ascii file.  The files are 
  //! written in the same format read by the RecordInputStream_Ascii class.
  /// @see RecordInputStream_Ascii
  class RecordOutputStream_Ascii :  public RecordOutputStream {
  
  public:
    //+ CONSTR RecordOutputStream* string bool
    //! Constructs an output stream which writes all records pushed by the
    //! specified stream into a file with the given name.  If the final argument
    //! field is set to true, then a type-tag is written at the beginning of the
    //! file.
    RecordOutputStream_Ascii 
    (RecordOutputStream * stream, string filename, bool writemetadata)
        throw (FileException, StreamInheritanceException);
    
    //+ CONSTR int string bool
    //! Constructs an output stream which writes all records pushed by the
    //! stream specified by ID into a file. If the final argument
    //! field is set to true, then a type-tag is written at the beginning of the
    //! file.
    RecordOutputStream_Ascii::RecordOutputStream_Ascii 
    (int streamindex, string filename, bool writemetadata)
    throw (FileException, StreamInheritanceException, InvalidStreamIDException);

    //+ CONSTR int ofstream* bool
    //! Constructs a new record output stream which writes all records pushed by 
    //! the record stream (specified by ID) to specified output stream (possibly a 
    //! file or network connection).  If the final argument
    //! field is set to true, then a type-tag is written to the stream.
    RecordOutputStream_Ascii
    (RecordOutputStream * stream, ostream * asciistream, bool writemetadata)
      throw (StreamInheritanceException);
      
    //+ CONSTR RecordOutputStream* ofstream* bool
    //! Constructs a new record output stream which writes all records pushed to 
    //! the specified record stream to specified output stream (possibly a 
    //! file or network connection).  If the final argument
    //! field is set to true, then a type-tag is written to the stream.
    RecordOutputStream_Ascii::RecordOutputStream_Ascii
    (int streamindex, ostream * asciistream, bool writemetadata)
    throw (StreamInheritanceException, InvalidStreamIDException);

    /// Closes the output file if it was opened by this class.
    ~RecordOutputStream_Ascii();
  
    /// Writes out the current record to the file.  
    virtual void pushRecord() throw (NoValueException, EndofStreamException);
    
    //+ METHOD string toString
    //! Returns a string representation of the stream.
    string toString();
   
  private:
    /// Common initialization for new objects.
    void initialize (RecordOutputStream * stream, bool writemetadata)
      throw (StreamInheritanceException);
  
    /// Writes out the meta data that describes the type of a file's contents.
    void writeMetaData();
  
    /// Converts the special characters:  newline, tab, double quote and 
    /// backslash to their escaped ascii format: '\n', \", \t and \\.
    void escapeString(string & str);
    
    /// Replaces all occurances of a substring with another substring.
    void replaceAll(string &str,const char * original, const char * replacewith);
  
    /// Flag saying whether we opened the stream or not.
    bool rosa_streamismine;
    
    /// The output file stream.
    ostream * rosa_outputstream;
  };
}

#endif
