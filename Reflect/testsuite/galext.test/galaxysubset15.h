
#ifndef KdTessellation_h
#define KdTessellation_h

#include <string>
#include <algorithm>
#include <vector>
#include <map>

#include <FullTessellation.h>
#include <RecordInputStream.h>

namespace BIE {

  /**
     Rectangular tessellation using a kd-tree.  The tessellation space is
     partioned so that each bin contains approximately the same number of 
     points.  
  */
  //+ CLASS KdTessellation SUPER FullTessellation
  class KdTessellation : public FullTessellation
  {
  public:
    //+ CONSTR Tile* RecordInputStream* int double
    KdTessellation(Tile *t,RecordInputStream* ris,int pointsperbin,double samplingpct);

    //+ CONSTR Tile* RecordInputStream* int double double double double double
    KdTessellation(Tile *t, RecordInputStream * ris, int pointsperbin, double samplingpct,
		   double minx, double miny, double maxx, double maxy);

    ~KdTessellation();
    
    /// Returns the tile id of the tree root.
    vector<int> GetRootTiles();

    /// Returns the root node.
    vector<Node*> GetRootNodes();
    
  private:
    /// Percent of dataset to sample to build the tessellation
    double pcttosample;
    /// Maximum number of points per tile
    int maxptspertile;
    /// The tessellation tree
    Node* treeroot;
    /// Tile factory
    Tile* tilefactory;
    /// Dimensions of tessellated area.
    double maxx, minx, maxy, miny;
    
    /// The sampled data points used in the creation of the tree.
    vector<twodcoords> sampledata;
    
    /// Perform tessellation of the space
    void tessellate(Node**, int, int, int, int, int, vector<int>&, double, 
		    double, double, double);

    /// Common initialization routine used by both constructors.
    void initialize(Tile *t, RecordInputStream * ris, int pointsperbin, 
         double samplingpct,double minx, double miny, double maxx, double maxy);  
  };
}

#endif
