#ifndef ContainerTessellation_h
#define ContainerTessellation_h

#include <FullTessellation.h>
#include <clivector.h>

namespace BIE {

  //+ CLASS ContainerTessellation SUPER FullTessellation
  //! Container tessellation for holding disjoint tessellations.  A tessellation
  //! is created by combining existing tessellations, which can be either
  //! normal tessellations like QuadTree and KDTree, or other container
  //! tessellations.  The root tiles of a container tessellation are the root
  //! tiles of the tessellations in the container.  This means there will 
  //! always be multiple root tiles in a container tessellation.
  //!
  //! \see FullTessellation
  class ContainerTessellation : public FullTessellation
  {
  public:
    //+ CONSTR clivectortess*
    //! Creates a container tessellation from the vector of existing
    //! tessellations passed in.  These tessellations should be disjoint --
    //! if they are not an exception will be thrown.
    ContainerTessellation(clivectortess* tessellations);

    /// Destructor.
    virtual ~ContainerTessellation() {};
    
    vector<int> GetRootTiles() { return _roottiles; }
    vector<Node*> GetRootNodes() { return _rootnodes; }

  private:
    /// List of root nodes in the tessellation tree.
    vector<Node*> _rootnodes;
    /// List of root nodes in the tessellation tree.
    vector<int>   _roottiles;
  };
}

#endif
