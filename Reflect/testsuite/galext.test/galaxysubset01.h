
#ifndef BinaryTessellation_h
#define BinaryTessellation_h

#include <string>
#include <map>

#include <FullTessellation.h>

namespace BIE {
  
  //+ CLASS BinaryTessellation SUPER FullTessellation
  //! Recursive, hierarchical tessellation of space into equally
  //! sized bins.  This is equivalent to QuadGrid when descending
  //! two levels at a time.
  class BinaryTessellation : public FullTessellation 
  {
  public:
    //+ CONSTR Tile* double double double double int
    //! Creates a new binary tessellation.  
    //! Arguments:
    //! 1. Tile Factory.
    //! 2. Minimum x coordinate of tessellation.
    //! 3. Maximum x coordinate of tessellation.
    //! 4. Minimum y coordinate of tessellation.
    //! 5. Maximum y coordinate of tessellation.
    //! 6. Depth of tessellation - number of x-y splits.
    BinaryTessellation(Tile * factory, double minx, double maxx, 
                       double miny, double maxy, int depth);
    
    /// Destructor.
    ~BinaryTessellation();

    /// Returns the single root tile.
    vector<int> GetRootTiles();

    /// Returns the single root node in the tessellation tree.
    vector<Node*> GetRootNodes();

    //+ STATICMETHOD void staticmethod int double
    //! Method added to test static generation.
    static staticmethod(int, double);

  private:
    /// Bounding variables.
    double Xmin, Xmax, Ymin, Ymax;
    /// The depth of the tessellation.
    int maxdepth;
    /// Number of data points, number of data dimensions, number of tiles
    int Ndata, Ndim_data;
    /// Tessellation tree
    Node * treeroot;
    /// Tile factory
    Tile * tilefactory;

    /// Perform tessellation of the space
    void tessellate(Node**, int, int, int, vector<int>&, double, double,
		    double, double);
  };
}

#endif
