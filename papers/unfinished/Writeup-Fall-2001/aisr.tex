\documentclass[12pt]{article}
\usepackage{times}
\usepackage{mathptm}
\usepackage{graphicx}
\usepackage{picins}
\usepackage{subfigure}

% page layout style parameters
\setlength{\textwidth     }{6.5in}
\setlength{\oddsidemargin }{0.0in}
\setlength{\evensidemargin}{0.0in}
\setlength{\topmargin     }{0.0in}
\setlength{\headheight    }{0.0in}
\setlength{\headsep       }{0.0in}
\setlength{\textheight    }{9.0in}
\setlength{\footskip      }{0.5in}

\pagestyle{plain}

\begin{document}
\thispagestyle{empty}

\begin{center}
\Large\bf Introducing The UMass Bayesian Inference Engine
\end{center}

\noindent
The UMass Bayesian Inference Engine (BIE) is a research product of the
Astronomy and Computer Science Departments of the University of
Massachusetts at Amherst. It is a result of the UMass Galaxy project
led by Martin Weinberg. The project has two primary goals: to produce
the BIE as a broadly useful tool, and to demonstrate the BIE's utility
by applying it to infer the structure of the Milky Way galaxy from
2MASS and other datasets. This brief document introduces the BIE, its
goals, structure, and results to date, in outline form. For further
information see {\tt www.astro.umass.edu/~weinberg/BIE} or email {\tt
  weinberg@astro.umass.edu}.

\section{BIE overview}
\begin{itemize}
\item The BIE provides general framework for Bayesian inference,
  tailored to astronomical and earth-science survey data.  Its primary
  uses are
  \begin{itemize}
  \item {\em Modeling:} parameter determination over terabyte-scale
    data sets
  \item {\em Data mining:} general non-parametric characterization
  \end{itemize}
\item It can incorporate multiple survey datasets simultaneously
\item Inference by Bayesian Monte Carlo Markov Chain (MCMC).
\item We use directed sampling and a new approach---{\em hierarchical
    empirical priors}---that efficiently explores and limits
  high-dimensional parameter space
\item Currently being used to infer Galactic structure from star
  counts but can be used for any scientific dataset to compare with
  predicted distributions.  The system is ideal for {\em mapped} data
  (spatial/temporal attributes) for example:
  \begin{itemize}
  \item Galactic ``spectral'' data cubes from astronomical radio
    surveys
  \item Multi-wavelength observations of nearby external galaxies
  \item Earth Observation System (EOS) data sets and combination
    with existing geographical and geophysical surveys
  \end{itemize}
\end{itemize}

\section{BIE Top Level Components / Structure}
\noindent
Each of these aspects has its own more detailed section below.

\begin{itemize}
\item Computational Engine
\item Command Line Interface
\item Graphical User Interface
\item Visualization Tool
\item Data Handling
\item Software Technology
\end{itemize}

\section{Computation Engine}

\begin{itemize}
\item Monte Carlo Markov Chain (MCMC) iteration
  \begin{itemize}
  \item General facility for Markov Chain testing and development
  \item Provision for extensible convergence testing
  \item Easy to add new chain generation algorithms and convergence
    models (e.g. convergence acceleration schemes such jump diffusion,
    multiple chains)
  \end{itemize}
\item Astronomical data sets require line-of-site model integration
  \begin{itemize}
  \item Can substitute a variety of quadrature/cubature methods
  \item Controllable/adaptive caching/interpolation/recomputation is
    under active development
  \end{itemize}
\item An ``Empirical Bayes'' hierarchical priors approach, leading to
      progressive refinement of spatial/temporal detail (see
      Figure~\ref{fig:stats}b)
  \begin{itemize}
  \item Uniform or non-uniform progression to finer granularity
  \item Extensible control of the refinement
  \item Extensible tessellation techniques
  \end{itemize}
\item Data spatially associated with tiles (spatial regions) in a hierarchy
      (see Figure~\ref{fig:stats}a)
  \begin{itemize}
  \item Data sets processed to distributions in general/extensible manner
  \item Supports multiple distributions, and multi-dimensional distributions
  \item Can easily add new distribution representations (such as basis sets)
  \end{itemize}
\item Persistence subsystem (in design; not yet implemented)
  \begin{itemize}
  \item Save intermediate/processed data sets, by name and with full account of
        how they were derived from inputs
  \item Maintain and check dependences of data sets on one another
  \item Save/restore under user control for checkpointing and what-if scenarios
  \end{itemize}
\end{itemize}

\begin{figure}
  \subfigure[Hierarchical tessellation]{\vbox{
      \hbox{\includegraphics[width=2.5in]{figure_1a}}
      \vspace{0.5in}
      \hbox{\includegraphics[width=2.5in]{figure_1b}}
      \vspace{0.5in}
      }}
  \subfigure[MCMC flowchart]{\includegraphics[height=7.0in]{flow1n.eps}}
  \caption{Statistical computation}
  \label{fig:stats}
\end{figure}

\section{Command Line Interpreter}
\begin{itemize}
\item Controls engine via a simple ASCII line-oriented interface (see
      sample session fragment in Figure~\ref{fig:CLI-session})
\item Support scripts, comments
\item Allows one to: get/set global variables, create new named variables
      (similar to a ``shell''), an invoke functions, particularly constructors
\item Available functions include support to: start/stop engine execution,
      connect I/O streams, define data filters, etc.
\item Part of the same OS process as the engine
\item Includes command line history, line editing, etc.
\item Type-checks variable accesses and function calls
\end{itemize}

\begin{figure}
\begin{verbatim}
#
# Choose the Bayesian prior
#
set prior = new InitialMixturePriorOrdered(nmix,1.0,"prior.first.level")
#
# Choose type of spatial tile
#
set tile = new SquareTile()
#
# Choose the input data
#
set ris = new RecordInputStream_Ascii("galaxy.data")
#
# Choose the type of spatial integration
#
set intgr = new AdaptiveLegeIntegration(dx, dy)
#
# Choose the covergence method
#
set convrg = new SubsampleConverge()
#
# Choose the tessellation
#
set tess = new QuadGrid(tile, 4.5728, 4.6775, 0.052360, 0.087266, tessdepth)
#
# Populate the tile with data (no choice here . . .)
#
set dis = new FullDistribution(ris,hist,tess)
#
#
# Choose the simulation method
#
set sim = new #RunMultilevelSimulationMPI(nsteps,L,minmc,maxT,width,nmix,ndim,stat,tile,hist,mod,prior,intgr,convrg,tess,dis)
# 
# Tell simulation to parallelize on lines of sight
#
sim->IntegrationGranularity()
#
# Run with out user interaction (e.g. batch mode). Stand back!
#
sim->Run()
#
\end{verbatim}
\caption{A sample CLI session fragment}
\label{fig:CLI-session}
\end{figure}

\section{Graphical User Interface (GUI)}
\begin{itemize}
\item Offers menu-oriented access to CLI features (see
      Figure~\ref{fig:GUI-screenshot})
\item Can run remotely
\end{itemize}

\begin{figure}
\includegraphics[width=6.5in]{GUIscript0.eps}
\caption{A sample GUI screen}
\label{fig:GUI-screenshot}
\end{figure}

\section{Visualization Tool}
\begin{itemize}
\item Offers a graphical control panel (see Figure~\ref{fig:visualizer-control})
\item Can be spawned via CLI/GUI
\item Allow one to monitor progress of MCMC computation, such as:
  \begin{itemize}
  \item Model parameters
  \item Posterior distribution
  \item Convergence diagnostics
  \end{itemize}
\item Connects to engine via a general TCP connection data-stream mechanism
\item Open-ended/extensible approach
\end{itemize}

\begin{figure}
\includegraphics[width=6.5in]{controls}
\caption{Visualizer control panel.  Monitors simulation output in real
  time and allows user to plot the run of any parameter and view
  marginals of posterior distribution for any two parameters.  See
  Fig. \protect{\ref{fig:visualizer-example}} for examples.
  }
\label{fig:visualizer-control}
\end{figure}

\section{Data Handling}
\begin{itemize}
\item Accepts record sequences in an extensible variety of formats
  \begin{itemize}
  \item Easy to extend to popular data exchange formats
  \end{itemize}
\item Record fields are named and typed
\item Streams of records can be filtered in general ways, for input or
    output, with filters and stream connections type-checked
\item Supports underlying byte streams connected to files, pipes, network
    sockets, etc.
\end{itemize}

\section{Software Technology}
\begin{itemize}
\item Built in C++ (hope to redo in Java if performance adequate)
\item Targeted to Intel/Linux platform; should port to any Un*x platform
\item We use CVS version management, autoconf, automake
\item Multiprocessor (Beowulf) support with MPI
\item Doxygen documentation tools
\item DejaGNU regression testing
\end{itemize}

\section{Results to date}
\begin{itemize}
\item Synthetic galaxy models
  \begin{itemize}
  \item Successful tests with many galactic disks with different
    properties
  \item[$\Rightarrow$] Hierarchical empirical prior scheme robustly
    converges to true solution for a high-dimensional model.  \newline
    Simulation with a standard weakly informative prior does {\em not}
    converge!
  \end{itemize}
\item 2MASS star count analysis (in progress)
  \begin{itemize}
    \item Extinction, physical density distribution, and population
      properties provide independent information
    \item[$\Rightarrow$] Will allow full-sky investigation of
      large-scale structural properties of Milky Way
  \end{itemize}
\end{itemize}

\begin{figure}
\subfigure[Galactic scale length (kiloparsecs)]%
{\includegraphics[width=3.25in]{xyplot}}
\subfigure[Posterior probability for scale length and height]%
{\includegraphics[width=3.25in]{contour}}
\caption{Visualizer example: plot of parameter galactic scale length and scale
  heights for a single galactic disk component.  Shown are scale
  length function of Markov chain state number (a) and marginalized
  posterior probability (b) with confidence level contours.}
\label{fig:visualizer-example}
\end{figure}

\end{document}
