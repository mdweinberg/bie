\documentclass[12pt]{article}
\usepackage{times}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{picins}

\fancyhead[L]{\thepage}
\fancyhead[C]{}
\fancyhead[R]{\bfseries BIE tutorial}
\fancyfoot[L]{From: Martin D. Weinberg}
\fancyfoot[C]{}
\fancyfoot[R]{}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
\addtolength{\headheight}{\baselineskip}

\newcommand{\btheta}{{\bf\theta}}

\begin{document}

\title{A BIE tutorial using the simple splat model}

\author{Martin D. Weinberg}

\date{}

\maketitle

\section*{Abstract}

\begin{narrower}
  I present a short walk through an Bayesian parametric inference,
  illustrating the choice priors, proposal function, convergence and
  multilevel simulation.
\end{narrower}

\section{MCMC basics}

A \emph{Markov Chain} is a mathematical model for a stochastic system
which may have continuous or discrete states.  The transition between
states is defined by a transition probability.  If the current state
in a Markov chain only depends on the previous state, a first-order
Markov chain, the transition probability is:
\[
P(x_n|x_{n-1}, x_{n-2}, \ldots, x_0) = P(x_n|x_{n-1}).
\]
This is called the \emph{Markov property}.  A process with the Markov
property is usually called a Markov process, and may be described as
\emph{Markovian}.

A Markov Chain is often denoted by $\{\Omega, \nu, T\}$ for the state
space $\Omega$, the initial distribution $\pi$ and the transition
probability $T$.


\piccaption{\label{fig:monaco} Monte Carlo Casino in Monaco}
\parpic[sr]{
  \begin{minipage}{0.3\textwidth}
    \includegraphics[width=\textwidth]{images/mc_casino.eps}
  \end{minipage}
}%
\emph{Monte Carlo} is a town in Monaco.  It was made infamous to
mathematical physicists by Enrico Fermi allusion to the resort
prinicipality when describing randomness as ``rolling dice''.

Markov Chain  Monte Carlo  (MCMC) is a  general purpose  technique for
sampling a probability distribution in a space of arbitrary dimension
using random numbers (e.g. dice).  Because the tecnique works in
high-dimensional spaces where direct sampling methods are infeasible,
the method is widely used for Maximum Likelihood or Bayesian inference.
A Markov Chain is designed to have the desired distribution $\pi(x)$
as its stationary or \emph{invariant}  probability distribution.

For example, in Bayesian inference, we begqin with a distribution of
observation data, call the set $Y$ with $y$ being an invidivudal
realization.  Assume a model that has $p$ parameters (and possibly latent
or ignorable variables) $\theta_j$.  We will refer to the vector of
parameters as $\btheta=(\theta_1,\theta_2,\ldots,\theta_p)$. The
probability of the data given the model, the \emph{likelihood} is
$L(y|\btheta)$ and the probability of the parameters in the
absense of data, the \emph{prior probability} is $\pi_0(\btheta)$.
Bayesian inference is based on the computation of the \emph{posterior
  probability} given by:
\[
\pi(\btheta|y) = {\pi_0(\btheta) L(y|\btheta)\over
    \int d\btheta\,\pi_0(\btheta) L(y|\btheta)}
\]
or in works, the posterior is proportional to the product of the prior
and likelihood.  A typical example might be as follows: suppose you
want to estimate the mean of a distribution which you know to be
normal with unit variance:
\[
\{y_1,\ldots,y_n\} \sim N(\theta, 1)
\]
You suspect that the mean is in the vicinity of $[-1,1]$ but you are
not sure.  So you pick a broad prior distribution:
\[
\pi_0(\theta) = {1\over 2(1+\theta^2)^{3/2}}
\]
and therefore the posterior distribution is:
\begin{eqnarray*}
\pi(\theta|y) &\propto& \prod_{i=1}^n 
\exp\left\{-{(y_i-\theta)^2\over 2}\right\} \times {1\over
    2(1+\theta^2)^{3/2}} \\
  &\propto&
  \exp\left\{-{\sum_{i=1}^n (y_i-\theta)^2\over 2}\right\} \times {1\over
      2(1+\theta^2)^{3/2}} \\
  &\propto&
  \exp\left\{-{n({\bar y}-\theta)^2\over 2}\right\} \times {1\over
      2(1+\theta^2)^{3/2}} \\
\end{eqnarray*}
where ${\bar y} = n^{-1}\sum_{i=1}^n y_i$.  This expression has a
number of interesting properties:
\begin{enumerate}
  \item For small $n$, the likelihood term and prior term have
    comparable weight in determining the posterior distribution
    $\pi$.  However as $n$ becomes large, the posterior becomes
    peaked about ${\bar y}$.
  \item The posterior mean, $E(\theta|y)$, follows from integrating
    the first moment of $\pi(\theta|y)$.
  \item Similarly, the posterior variance, $\hbox{var}(\theta|y)$,
    follows from integrating the first and second moments of
    $\pi(\theta|y)$.
  \item The credible interval $(l, h)$ for $\theta$ such that
    $P(l<\theta<h|y)=0.95$ requires knowledge of the entire
    distribution $\pi(\theta|y)$.
\end{enumerate}
In this case, the necessary integrals are straightforward, however,
for likelihood functions for realistic problems and in many
dimensions, the necessary integrals will be impossible by
straightforward techniques.

However, if we can draw samples from the posterior distribution:
\[
\left\{\theta_1,\theta_2,\ldots,\theta_N\right\} \sim \pi(\theta|y)
\]
then we can estimate any integral over $\theta$ as follows:
\[
E_\pi\left[f(\theta)\right] \approx {1\over N}\sum_{j=1}^N f(\theta_j)
\]
where the error term for this approximation is proportional to
$N^{-1/2}$ from the central limit theorem.  More generally, one knows
that 
\[
{1\over N}\sum_{j=1}^N f(\theta_j) \rightarrow E_\pi\left[f(\theta)\right]
\]
according to the Law of Large Numbers.  This is the central idea
behind \emph{Monte Carlo Integration}.

Sampling from $\pi$, even though the function is only implicitly
defined, is the main use of Markov chains.  A Markov chain is
generated by sampling based on the current state and a transition
probability $p(x|y)$ as follows:
\[
\theta_{j+1} \sim p(\theta|\theta_j), j=1,2\,\ldots
\]
where $\theta_{j+1}$ only depends on $\theta_j$ and not on other
previously sampled values $\theta_0\,\theta_1,\ldots,\theta_{j-1}$.
This is called a first-order Markov chain.

Markov chains, under some conditions have a number of important and
practically useful properties:
\begin{enumerate}
\item If the any set of states can be reached from any other state in
  a finite number of transistions, the distribution is said to be
  \emph{irreducible}.  
\item As $N\rightarrow\infty$, the Markov chain may converge to a
  stationary distribution.  If the stationary distribution exists,
  this distribution is unique if the chain is \emph{irreducible}.
\item A Markov chain taking only a finite number of values is
  \emph{aperiodic} if the greatest common divisor of return times to
  any particular state is 1.  This definition can be extended for a
  continuous state space.
\item If a Markov chain has a stationary distribution $\pi$ and is
  aperiodic and irreducible, then
  \begin{eqnarray*}
  \left\langle h_N\right\rangle &=& {1\over N}\sum_{j=1}^N
  h(\btheta_j) \\
  &\rightarrow& E_\pi\left[h(\btheta)\right]\quad\hbox{as}\quad{N\rightarrow\infty}
  \end{eqnarray*}
\item For these same chains, if the variance is finite,
  \[
  \sigma^2_h = \hbox{var}_\pi\left[h(\btheta)\right] < \infty
  \]
  then the Central Limit Theorem applies and the convergence is
  geometric.
\end{enumerate}

Metropolis et al. (1953) and Hastings (1970) devised an algorithm for
using a Markov chain to sample from a stationary distribution $\pi$ as
follows:

\begin{enumerate}
\item Sample $\theta_\ast \sim q(\btheta_\ast|\btheta_t)$.  $\theta_\ast$ is the
  candidate state and the distribution $q$ is the proposal
  distribution.
\item With the probability
  \[
  \alpha(\btheta_t,\btheta_\ast) = \min\left\{1,
    {\pi(\btheta_\ast) q(\btheta_t|\btheta_\ast)\over \pi(\btheta_t)
      q(\btheta_\ast|\btheta_t)}\right\}
    \]
    set
    \[
    \btheta_{t+1} = \btheta_\ast \quad \hbox{[accept]},
    \]
    or otherwise
    \[
    \btheta_{t+1} = \btheta_t \quad \hbox{[reject]}.
    \]
\end{enumerate}
This is the \emph{Metropolis-Hastings} (MH) algorithm and it has a
number of improtant and interesting features.  First, we do not
require that the proposal distribution be normalized because the
normalization constant factors out in the numerator and denominator of
the probability $\alpha$.  This makes the MH algorithm ideal for
sampling the Bayes posterior since computation of the normalization is
often infeasible.  The proposal distribution is usually chosen to
allow easy sampling.  Clearly the support of $q$ should be the support
of $\pi$.  Of course, one could choose $q$ to be $\pi$ but if we can
not sample directly from $\pi$, the MH algorithm can not be applied.
As long as weak conditions apply to $\pi$, the MH-generated Markov
chain has all of the desireable properties.

\section{Approaches to implementing MCMC}

The MH algorithm is generic and, therefore, there are a number of
choices that must be made before executing an MCMC simulation.  Each
of which affects the quality and performance of the simulation and we
will describe each below.  Each of these same decisions have parallels
in designing a BIE run.

\subsection{Choice of the proposal distribution, $q$}

Metropolis et al. (1953) suggested the use of a symmetric function,
\[
q(\btheta_\ast|\btheta_t) = q(\btheta_t|\btheta_\ast),
\]
and all of the built-in proposal distribution in BIE are of this
type.  For example, the original random-walk Metropolis algorithm
assumes 
\[
q(\btheta_\ast|\btheta_t) = q(|\btheta_t-\btheta_\ast|).
\]
In other words, the proposal depends on your current location but not
your absolute location in parameter space.  For this case the we have
\[
\alpha(\btheta_t,\btheta_\ast) = \min\left\{1,
  {\pi(\btheta_\ast) \over \pi(\btheta_t)}\right\}
\]
The BIE implements a number of proposal distributions coupled to the
specification of the prior distribution as described in the following
table:
\par
\begin{tabular}{l|l}
  Prior distribution & Proposal distribution \\ \hline
  Uniform 				& Heavyside \\
  Gaussian 				& Heavyside \\
  Gaussian (with range limits)		& Gaussian \\
  Exponential 				& Gaussian \\
  Exponential (with range limits) 	& Gaussian \\
  Weibull 				& Gaussian \\
  Weibull (with range limits) 		& Gaussian \\
\end{tabular}
\par
These choices could be changed in BIE by deriving a new Prior class
and could include non-symmetric proposal distributions.

Another sometimes used class of proposal function is the
\emph{Independence Sampler} which has the form:
\[
q(\btheta_\ast|\btheta_t) \equiv q(\btheta_\ast).
\]
Although they perform well under some circumstances they are less
general and require heavy tails to make sure that the convergence of
the Markov chain remains geometric.  These are not currently
implemented in the BIE but there is no intrinsic reason other than
their lack of generality.

\subsection{Chain convergence}

Early iteration of the chain, say $\btheta_1,\ldots,\btheta_m$, are
correlated with their starting value $\btheta_0$.  This is called the
\emph{burn-in} period.   After the chain is burnt-in, it should be
sampling from the stationary distribution, the desired distribution.
We use the chain beyond burn in to estimate quantities of interest via
Monte Carlo integration:
\[
\left\langle h \right\rangle = {1\over N-m} \sum_{j=m+1}^N
h(\btheta_j).
\]
Algorithms for estimating the burn in length $m$ are called
\emph{converge diagnostics}.

There are a wide variety of convergence diagnostics available.
One should use, at least, the obvious graphical ones and time-series
analyses, e.g. (1) plot the chain for each parameter of interest and
(2) determine the auto-correlation functions.

Gelman and Rubin (1992) proposed monitoring the convergence of MCMC
output using two or more parallel chains run with starting values that
are overdispersed relative to the posterior distribution. Convergence
is diagnosed when the chains have \emph{forgotten} their initial values,
and the output from all chains is indistinguishable.  This is done through
a comparison of within-chain and between-chain variances,
similar to a classical analysis of variance.

There are two ways to estimate the variance of the stationary
distribution: the mean of the empirical variance within each chain,
$W$, and the empirical variance from all chains combined, which can be
expressed as
\[
{\hat\sigma}^2 = (N-1)B/N + W/N
\]
where $B$ is the empirical between-chain variance.

If the chains have converged, then both estimates are unbiased.
Otherwise the first method will underestimate the variance, since the
individual chains have not had time to range all over the stationary
distribution, and the second method will overestimate the variance,
since the starting points were chosen to be overdispersed.  This
convergence diagnostic is based on the assumption that the target
distribution is normal. A Bayesian credible interval can be
constructed using a Student t-distribution whose mean is the sample
mean of all chains combined, ${\hat\mu}$, and variance ${\hat V} =
{\hat\sigma}^2 + B/(MN)$ where $M$ is the number of chains, and
degrees of freedom $d$ for the t-distribution may be estimated by the
method of moments $d = 2{\hat V}/{\hat{\hbox{var}}}({\hat V})$.  Use of the
t-distribution accounts for the fact that the mean and variance of the
posterior distribution are estimated.  The convergence diagnostic
itself is
\[
R=\sqrt{(d+3) {\hat V}\over (d+1)W}
\]
Values substantially above 1 indicate lack of convergence. If the
chains have not converged, Bayesian credible intervals based on the
t-distribution are too wide, and have the potential to shrink by this
factor if the MCMC run is continued.  The multivariate a version of
Gelman and Rubin's diagnostic was proposed by Brooks and Gelman
(1997).

This diagnostic, although commonly used, is not directly implemented
in the BIE (although it could be; project to be done).  Rather the
main converge diagnostic uses the subsampling algorithm proposed by S.
G. Giakoumatos and I. D. Vrontos and P. Dellaportas and D. N. Politis
(1999).  The method is based on the use of subsampling for the
construction of confidence regions from asymptotically stationary time
series as developed in Politis, Romano, and Wolf. The subsampling
diagnostic is capable of gauging at what point the chain has
\emph{forgotten} its starting points, as well as to indicate how many
points are needed to estimate the parameters of interest according to
the desired accuracy.  It is based on the convergence of the
intrachain variance at the rate predicted by the central limit
theorem.  The practical advantage is that one only uses a single
chain.

A related criteria is the method of \emph{batching}.  The standard
error for some Monte Carlo integral $h_N$ is
$\sqrt{\hbox{var}_\pi(\langle h_N\rangle}$ and for large $N$ may be
estimated as
\[
\hbox{std}(\langle h_N\rangle)^2 \approx
{\sigma_h^2\over N}\left[1+2\sum_{t=1}^{N-1} \rho_t(h)\right]
\]
where $\rho_j$ is the lag-t auto-correlation in the chain
$h(\btheta)$.  If $\rho_t \approx \epsilon^t$ then this can be
written:
\[
\hbox{std}(\langle h_N\rangle)^2 \approx
{\sigma_h^2\over N}{1+\epsilon\over 1-\epsilon}.
\]
Intuitively the standard error must be larger than the root variance
with respect to the stationary distribution because of the
autocorrelation induced by the MH algorithm.

We can then divide the burnt-in chain into $p$ equal lenght subchains
and compute mean for each, $\mu_p$.  Assuming that the
$\mu_1,\ldots\,\mu_p$ are uncorrelated we can estimate
\[
\hbox{std}(\langle \btheta_{N-M}\rangle)^2 \approx
{1\over p(p-1)}\sum_{j=1}^p (\mu_j - {\bar\mu})^2
\]


\section{BIE basics}

The BIE is object-oriented.  Object-oriented programming is a modular
approach to computer software design. Each module, or object, combines
data and procedures that act on the data.  For a scientist or
mathematician, the relationship between objects are designed to
reflect (and enforce) the relationships between the aspects of one's
physical system or theory.

A group of objects that have properties, operations, and behaviors in
common is called a class.  Classes can {\em inherit} each other's
underlying structure and interface.  The BIE is written in C++, a
generalization of the C language invented during the late 1980s and
1990s to facilitate object-oriented programming.

For an example of a class definition, the BIE code has a {\em
  distribution} class.  This class defines some of the basic
properties that one might desire of any distribution, such as partial
distribution function (PDF) evalutions, domain limits, mean and
variance, etc.:

\begin{verbatim}
class Distribution
{

public:
  
  /// Force runtime binding of inherited destructor
  virtual ~Distribution() {};

  /// Object factory (clone)
  virtual Distribution* New();
  
  /// Differential distribution function P(x)
  virtual double PDF(vector<double>&);

  /// Log of differential distribution function P(x)
  virtual double logPDF(vector<double>&);

  /// Cumulative distribution function P(>x).  
  /// Throws exception if not appropriate.
  virtual double CDF(vector<double> &); 

  /// Lower bound on distribution (in each dimension)
  virtual vector<double> lower(void);

  /// Upper bound on distribution (in each dimension)
  virtual vector<double> upper(void);
  
  /// Return mean of distribution (mulitvariate)
  virtual vector<double> Mean(void);

  /// Return standard deviation of distribution (mulitvariate)
  virtual vector<double> StdDev(void);

  /// Return specifided momemnt of distribution (mulitvariate)
  virtual vector<double> Moments(int);
  
  /// Return random variate from distribution
  virtual vector<double> Sample(void);
  
  /// Override default width
  virtual void setWidth(double x) {};
  
};
\end{verbatim}

This idea can be generalized by a class that includes the ideas
necessary to generate a distribution from a sample of data as follows:
\begin{verbatim}
class SampleDistribution : public Distribution 
{

public:
    
  /// Force runtime binding of inherited destructor
  virtual ~SampleDistribution();

  /// Cumulative distribution function P(>x).  
  /// Throws exception if not appropriate.
  virtual double CDF(vector<double> &);

  /// Clone (factory) function
  virtual SampleDistribution* New();

  /** Accumlulate data:
      Value or state may be dummy in some applications.
      returns true if data is used in distribution
  */
  virtual bool AccumData(double value, vector<double>& state);

  /**
     Accumlulate data from a data point held in a record buffer
     and increment data set size.
  */
  virtual bool AccumData(double v, RecordBuffer* datapoint);

  /// Compute distribution estimate as needed
  virtual void ComputeDistribution() {};
  
  /** @name Iterators
      Need this for iterating through data list.  Can be either
      bins, points, coefficients, etc.
  */
  //@{
  virtual int numberData();
  virtual double getValue(int i);
  virtual int getdim(int i);
  //@}

  /// Returns the record type describing the fields used by a distribution.
  virtual RecordType * getRecordType();

  /// Returns the total number of data points used in distribution
  virtual int getDataSetSize() { return datasetsize; }

};
\end{verbatim}
All of the features of a {\tt Distribution} are included in the {\tt
  SampleDistribution} now including functions such as {\tt AccumData}
(to build the distribution from the data).


\section{A simple example}

\section{Varying the prior}

\subsection{Flat priors}

\subsection{Gaussian priors}

\section{Determining convergence}

\section{Multiple level simulations}


\end{document}

