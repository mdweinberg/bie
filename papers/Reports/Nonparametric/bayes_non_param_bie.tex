\documentclass[12pt,twoside]{article}
                                % Don't waste so much paper!
\usepackage[height=9.0in,letterpaper]{geometry}

\usepackage{times}
\usepackage{mathptmx}

\usepackage{amsmath,amssymb}    % for math
\usepackage{subfigure,graphicx} % for figures
\usepackage{natbib}		% bibliography
\usepackage{appendix,fancyhdr}  % nice formatting
\usepackage{longtable}          % for the class list


\usepackage{hyperref}
% \hypersetup{dvips,colorlinks=true,allcolors=blue}
% \hypersetup{dvips}
\hypersetup{
  dvips,
  bookmarks=true,         % show bookmarks bar?
  unicode=false,          % non-Latin characters in Acrobat’s bookmarks
  pdftoolbar=true,        % show Acrobat’s toolbar?
  pdfmenubar=true,        % show Acrobat’s menu?
  pdffitwindow=false,     % window fit to page when opened
  pdfstartview={FitH},    % fits the width of the page to the window
  pdftitle={My title},    % title
  pdfauthor={Author},     % author
  pdfsubject={Subject},   % subject of the document
  pdfcreator={Creator},   % creator of the document
  pdfproducer={Producer}, % producer of the document
  pdfkeywords={keyword1} {key2} {key3}, % list of keywords
  pdfnewwindow=true,      % links in new window
  colorlinks=false,       % false: boxed links; true: colored links
  linkcolor=red,          % color of internal links (change box color with linkbordercolor)
  citecolor=green,        % color of links to bibliography
  filecolor=magenta,      % color of file links
  urlcolor=cyan           % color of external links
}
\usepackage{hypcap}
\usepackage{caption}

%% Add a trick to get around the figure float stuff

\makeatletter                   % Change the catagory code of @
\newenvironment{inlinetable}{%
  \def\@captype{table}%
  \noindent\begin{minipage}{0.999\linewidth}\begin{center}\footnotesize}
    {\end{center}\end{minipage}\smallskip}

\newenvironment{inlinefigure}{%
  \def\@captype{figure}%
  \noindent\begin{minipage}{0.999\linewidth}\begin{center}}
    {\end{center}\end{minipage}\smallskip}
\makeatother                    % Revert to protected

%% For convenience . . . 

\newcommand{\bx}{{\bf x}}
\newcommand{\bt}{{\bf\theta}}
\newcommand{\bp}{{\bf\pi}}
\newcommand{\barm}{{\bar m}}
\newcommand{\erf}{\mathop{\rm erf}\nolimits}

% \lta and \gta produce > and < signs with twiddle underneath
\def\spose#1{\hbox to 0pt{#1\hss}}
\newcommand{\lta}{\mathrel{\spose{\lower 3pt\hbox{$\mathchar"218$}}
    \raise 2.0pt\hbox{$\mathchar"13C$}}}
\newcommand{\gta}{\mathrel{\spose{\lower 3pt\hbox{$\mathchar"218$}}
    \raise 2.0pt\hbox{$\mathchar"13E$}}}

\let\lesssim=\lta
\let\gtrsim=\gta

\newcommand{\msun}{{\rm\,M_\odot}}
\newcommand{\AU}{\,\hbox{\rm AU}}
\newcommand{\kms}{{\rm\,km\,s^{-1}}}
\newcommand{\pc}{\,\hbox{\rm pc}}
\newcommand{\kpc}{{\rm\,kpc}}
\newcommand{\Myr}{\,\hbox{\rm Myr}}
\newcommand{\Byr}{\,\hbox{\rm Byr}}
\newcommand{\Gyr}{\,\hbox{\rm Gyr}}
\newcommand{\gyr}{\,\hbox{\rm Gyr}}
% \newcommand{\mod}{\mathop{\rm mod}\nolimits}
\newcommand{\sgn}{\mathop{\rm sgn}\nolimits}
\newcommand{\ext}{{\hbox{\ninerm ext}}}
\newcommand{\resp}{{\hbox{\ninerm resp}}}
\mathchardef\star="313F
\newcommand{\bth}{{\bf\theta}}
\newcommand{\bdata}{{\bf D}}
\newcommand{\bdth}{{\Delta{\bf\theta}}}
% \newcommand{\Re}{\mathop{\it Re}\nolimits}
% \newcommand{\Im}{\mathop{\it Im}\nolimits}
\newcommand{\Ei}{\mathop{\rm Ei}\nolimits}

\newcommand{\apj}  {ApJ}
\newcommand{\apjl} {ApJL}
\newcommand{\apjs} {ApJS}
\newcommand{\aj}   {AJ}
\newcommand{\mnras}{MNRAS}

\newcommand{\model}{{\cal M}}
\newcommand{\data} {{\bf D}}
\newcommand{\param}{\boldsymbol{\theta}}

%%% 
%%% End definitions
%%% 

\pagestyle{fancy}

\setlength{\headheight}{15pt}
\pagestyle{fancy}

\fancyhf{}
\fancyhead[LE,RO]{\thepage}
\fancyhead[RE]{\textit{\nouppercase{\leftmark}}}
\fancyhead[LO]{\textit{\nouppercase{\rightmark}}}
 
\begin{document}

\title{Notes on nonparametric functional inference using the BIE}

\author{Martin D. Weinberg\\
  Department of Astronomy\\
  University of Massachusetts, Amherst, MA, USA 01003;\\
  {\it weinberg@astro.umass.edu}
}

\date{\today}

\maketitle

\begin{abstract}
  I describe the implementation and testing of the Bernstein
  polynomial-based nonparametric functional inference in the BIE.  The
  method behaves stably and appears to be insensitive to polynomial
  order $n$.  This suggests that dimension switching may be less
  critical for applications to galaxy evolution models. As suggested
  in the previous notes, a fixed basis with $n={\cal O}(10)$ should
  provide enough information on the functional trends without
  requiring dimensional switching.  Heuristically, the resolution of
  the functional variation in the unit interval is roughly $1/n$.

  Specific enhancements of the BIE include: 1) a \emph{BernsteinPoly}
  class which gives model implementers callable function based on
  coefficients in the BIE state variable; 2) implementing of
  \emph{blocking} which allows multiple algorithms to be used for
  different partitioned parts of the MCMC state vector.  In
  particular, the non-parametric parts using Bernstein bases and the
  parametric parts are likely to have different features and only be
  weakly correlated; 3) an implementation of the \emph{Slice Sampler},
  a non-Metropolis-Hastings algorithm with an new extension that
  allows all previous posterior evaluations to be reused.
\end{abstract}

\tableofcontents

\section{Introduction}
\label{sec:intro}

I have updated the BIE \citep{Weinberg:2013} to produce non-parametric
functional approximations using the Bernstein basis polynomials as
described in the previous set of notes. The new test version of the
BIE may be checked out from
\href{https://bitbucket.org/mdweinberg/bie.git}{Bitbucket} in the
usual way; you will have to switch to the {\tt Nonparametric} branch
after cloning. The main goal of this implementation is a new
\emph{helper} class that provides a functional approximation, i.e. a
density function or rate function, that may be called by the user to
generate a new variate.

The domain of this functional approximation is the unit interval and
the integral of the approximating function is $1$.  Clearly, the
interval may scaled and/or offset by a simple linear transformation
and the amplitude of the function may be similarly scaled.  The
overall amplitude is likely to be an unknown and included as part of
the inference.  Also, the unit interval may be scaled non-linearly to
the semi-infinite interval.  For example, if $y\in(0,\infty)$, we may
map to the unit interval with the function
\[
x = \frac{y^2}{1+y^2}
\]
whose inverse is
\[
y = \sqrt(\frac{x}{1-x}).
\]
and Jacobian
\[
\frac{dx}{dy} = \frac{2y}{(1+y^2)^2}.
\]
I have not included transformations of this sort in the BIE
implementation; these choices of the most sensible transformation are
best made by the model implementer.

Secondly, using one or more of these functional approximations within
a larger parametric inference suggests that the BIE needs ways to
identify \emph{blocks} of variables.  I describe the first section
below (\S\ref{sec:blocking}.

And finally, I have implemented a non-Metropolis-Hastings MCMC sampler
known as the \emph{slice sampler} \citep{Neal:2003} that should be a
good choice for sampling the parameter blocks generated by the
Bernstein polynomial-based inference (see \S\ref{sec:slice}).  This
method does not have tunable parameters so shares some advantage with
the differential evolution algorithm.  Moreover, the BIE
implementation reuses all intermediate posterior probability
evaluations and \emph{learns} where the density is located in the
parameter hypercube using a tree structure.  This same tree structure
may be used, ex post facto, along with the posterior sample to obtain
normalization (i.e. marginal likelihood).

\section{Blocking within the BIE}
\label{sec:blocking}

The original version of the BIE implements two types of states: a
simple or \emph{standard} state equivalent to a simple array of
parameter values; and a \emph{mixture} or \emph{extended mixture
  model} that describes $M$ linearly weighted components from a single
model family, possibly extended by a set of common parameters.

To incorporate more general state specifications, and the functional
approximations in particular, I have augmented the state definition by
including user-defined blocks of variables.  These blocks are indexed
by arrays describing the initial and final positions in the overall
state vector.  The variable within each block may be imbued with
additional properties.  In the case of Bernstein polynomials, we want
the coefficients to be positive definite and sum to unity.  Such
conditions may be enumerated and enforced by the {\tt State} and {\tt
  Chain} classes.

In the current version of BIE then, blocking is selected by calling
the appropriate construction in the same way that a mixture is
selected by calling the appropriate variant of the {\tt StateInfo}
constructor.  So, the original implementation remains for backward
compatibility.  However, it seems easier in the long run to rewrite
{\tt State} so that blocking is always active; the old behavior is
then a simple block and the mixture models fit into this structure as
a particular case of blocking.

Practically, this new functionality allows a model designer to pass
the state to the nonparametric function, and the function class
automatically chooses the correct block of variables for computing the
functional value.

\section{Block updating}

Although we most often use our MCMC algorithms to update the entire
parameter vector at once as a sample from the target distribution, we
may split a multivariate vector into groups called \emph{blocks}, and
each block in the is sampled separately. A block may contain one or
more parameters.  Having implemented state blocking in the BIE, I
easily implemented \emph{block updating} as a container of simulations.

One expects block updating to improve sampling efficiency when the the
parameters within each block are strongly correlated and the
parameters in different blocks are only weakly correlated.  In
essence, this algorithm replaces sampling from high-dimensional space
of dimension $d$ with successive sampling from lower-dimensional
product spaces with dimension $d_i$ with $\sum d_i = d$.  This
strategy enables the use of a different MCMC algorithm for each
block. As an extreme case, one may ignore correlations altogether and
treat each parameter as a single block.  However, for strongly
correlated parameters, ignoring these correlations of parameters
between blocks, may yield poor convergence.

The latest version of BIE implements the {\tt BlockSampler} simulation
class.  The constructor takes a vector of simulations that are used
sequentially for each block to sample a new state at each step.

\section{The BernsteinPoly class}

A new helper class, {\tt BernsteinPoly}, provides Bernstein polynomial
approximations given a {\tt State} vector for coefficients and
one-dimensional argument.  The constructor takes a single argument:
the order $n$ of the polynomial basis.  A suite of member functions
provides evaluations for a vector of coefficients or a state instance
and an independent $x\in[0,1]$.  The basis is computed by recursion
relation for computational efficiency.

In a break from standard usage, each element of the basis is
normalized so that the the vector of normalized weights yields a
unit-normalized probability density.  The evaluation for the previous
$x$ value is cached for reuse.  In addition, the user may request the
value of a specific basis function or the cumulative distribution
function for the current approximation.  The computation of the
integral of the Bernstein basis is based on the analytic mathematical
relation and may not be computationally optimal.


\section{The Slice Sampler}
\label{sec:slice}

Here, I describe the implementation the \emph{Slice Sampler} algorithm
by Radford Neal following \citet{Neal:2003}.  Unlike the generic slice
sampler implementations, this one \emph{learns} about the structure of
the a priori unknown target distribution by using every evaluation of
the likelihood function to characterize the posterior.
Simultaneously, the slice sampler produces a valid posterior sample.
\citet{Neal:2003} describes a general framework for using auxiliary
information but, to my knowledge, this is the first implementation of
a sequentially self-improving algorithm.

\subsection{Introduction}
The Slice Sampler exploits the geometric observation from the
``rejection'' method that one can sample uniformly from the region
under the graph of its probability density function, $g(x)$.  That is,
if we divide the area under the curve into Riemann rectangles of width
$(dx)$ and count up the number of tiles of area $(dx)^2$ in each
rectangle, the frequency of the variate $x$ will be proportional to
the number of tiles in the rectangle containing $x$.  This is
equivalent to the rejection method, which samples the rectangle
defined by the range of the independent coordinate.  The slice sampler
uses the probabilistic equivalence of the tiles at height $y<g(x)$ In
short, the basic algorithm is as follows:
\begin{enumerate}
\item Choose a starting value $x_i$ for which the density $g(x_i)>0$.
\item Sample a $y$ value uniformly between $0$ and $g(x_i)$.
\item The value of $y$ defines a set of points with
$g(x)>g(x_i)$, i.e. in one dimension: the area under the
density and above the horizontal line the $y$ position.
\item Now, sample a point $(x,y)$ from the line segments within the curve.
\item Repeat from step 2 using the new $x$ value.
\end{enumerate}
\href{http://en.wikipedia.org/wiki/Slice_sampling}{Wikipedia} provides
a readable introduction to this if \citet{Neal:2003} is too much too
start.


The performance of this algorithm is limited by the efficiency in
finding the bounds of the horizontal slice.  Neal's (2003) paper
presents a broad range of implementation ideas.  Here, we implement a
form of the ``crumb'' idea (Neal, 2003, Section 5.2) that allows the
definition of an auxiliary sampling distribution.  The \emph{crumb} is
a variate from this auxiliary distribution that guides the selection
of the new bounded interval.  We propose a new state from the
distribution of states that could have generated that crumb. If the
proposal is in the slice, we accept the proposal as the new
state. Otherwise, we draw further crumbs and proposals until a
proposal is in the slice. 

The auxiliary \emph{crumb} distribution is constructed by caching all
previous posterior samples in an orthogonal recursive bisection tree
(ORBTree).  The ORBTree is, essentially, a kd-tree whose bisection
dimension is chosen cyclically rather than by the properties of the
points in the leaves.  This approach is produces a spatial cells that
whose axis ratios are never smaller than 0.5.  When used in parallel
(the default), each process runs a separate chain.  All posterior
probability evaluations from each process are pooled and added to the
ORB tree on each process at the end of the step.  Neal shows that the
detailed balance is ensured if the crumb distribution does not depend
on which value of $x$ is chosen for allowed value of $y$ (an
equivalance class, if you like).  This condition is clearly obeyed by
the new algorithm.

By the end of the simulation, the ORB tree may have accumulated more
than order of magnitude more states than the posterior simulation.
Since this implementation uses the same tree class as the marginal
likelihood computation, I could easily enhance the {\tt
  MarginalLikelihood} class to use the already constructed ORB tree
its computation; I have not yet changed the code but I plan to do so.

The slice sampler in the current BIE implementation proceeds one
dimension at time, cyclically, to choose a new sample.  Let the
sampling dimension be $i$.  The proposed range of the set $\{x |
g(x|x_{-i})>y\}$.  An estimate of the initial bounds is determined by
the ORBTree and refined using Neal's ``step-out'' algorithm.  I have
been able to efficiently compute the density along the line
\emph{shot} through the ORB tree parallel to a coordinate direction by
caching the boundaries of each cell; this is not typically done for
traditional kd-tree algorithms and requires only additional memory but
no additional computational complexity.

\subsection{Parameters}
There are two parameters that govern the algorithm:
\begin{enumerate}
\item The ORBTree density estimator is populated with {\tt ninit}
  states sampled from the prior distribution before the first use of
  the algorithm.  The tree is updated by all future samples.  If {\tt
    ninit=1}, the first step uses the standard ``step-out'' algorithm
  but learns about the distribution for every step thereafter.  A
  large value of {\tt ninit} will improve overall sampling efficiency
  with the tradeoff of cpu time required for likelihood evaluations.
  Spending something like 10\% of the total evaluations on prior
  learning seems like good common sense.
\item The implementation uses a linear cut through the ORBTree to
  estimate the range and a multiplicative fraction of this width given
  by {\tt wfrac} is used to define the step-out interval.  A very
  small value of {\tt wfrac} may have the consequence of requiring
  many likelihood evalutions for each step to find the interval
  containing the set $\{x | g(x|x_{-i})>y\}$.  Conversely, a very large
  value of {\tt wfrac} may find the interval containing the set with a
  small number of trys, but the range of the set may be much smaller
  than the bounding range, again, leading to many likelihood
  evaluations per step.  However, as the ORB tree provides an
  improving estimate of the ideal initial interval, the influence of
  {\tt wfrac} on efficiency decreases with sample size.  A choice of
  {\tt wfrac=0.2} seems like a good compromise.
\end{enumerate}

The fourth parameter is string value for the file containing the
chain-independent random variate parameters.  The first line is the
distribution type followed by the width of the symmetric sampling
distribution for the weight parameters.  Each successive line
describes specifies sampling distribution for each parameter in the
mixture model and contains four values: the symmetric sampling
distribution, the width, and the upper and lower limit for the
parameter.  The available symmetric distributions are listed in the
{\tt enum Distrib}.

\subsection{Control methods}
There are two obvious possibilities for arranging the computation for
a computational cluster:
\begin{enumerate}
\item The parallel likelihood classes should be used with \emph{serial
    control} only.  The parallel likelihood classes {\tt
    LikelihoodComputationMPI} and {\tt Likelihood\-Com\-pu\-ta\-tion\-MPITP}
  assume that each process will enter the likelihood computation code
  and only the root node will execute the slice sampler.
\item A serial likelihood class should be used with \emph{parallel
    control} only.  A serial likelihood computation is only entered by
  the process which calls it.  In this case, each process maintains
  its own chain.  All states computed by each process are shared with
  all other processes  (i.e. all processes learn from the mistakes of
  other) and the independent posterior samples from each process are
  added to the fiducial posterior sample.
\end{enumerate}


\section{Examples: Bernstein polynomial fitting using the slice
  sampler}
\label{sec:examples}

\begin{figure}
  \mbox{
    \begin{minipage}{0.48\textwidth}
      \includegraphics[width=\textwidth]{quartic_400.png}
      \caption{400 points draw from the quartic distribution.
        \label{fig:quartic_points}}
    \end{minipage}
    \hspace{0.04\textwidth}
    \begin{minipage}{0.48\textwidth}
      \includegraphics[width=\textwidth]{powerlaw_400.png}
      \caption{400 points draw from the power-law distribution with
        exponent $\beta=4$. \label{fig:powerlaw_points}}
    \end{minipage}
  }
\end{figure}

I have created a simple user likelihood, {\tt BernsteinTest}, that
constructs an instance of {\tt BernsteinPoly} based on the dimension
of the State (that is, the order is the rank of the state minus one).
This class is only directly applicable for simple tests, but provides
and example of creating and using a {\tt BernsteinPoly}.  Point data
is simulated from one of three analytic test distributions over the
unit interval:
\begin{enumerate}
\item A quartic polynomial which has the probability density
  \[
  P(x) = 30x^2(1 - x)^2
  \]
  and probability
  \[
  P(<x) = x^3(6x^2 - 15x + 10).
  \]
  See Figure \ref{fig:quartic_points} for a graphical example of this
  distribution.
\item A simple power law density
  \[
  P(x) = (1+b)x^b
  \]
  and probability
  \[
  P(<x) = x^{1+b}
  \]
  with $b>-1$ so that the probability exists.  See Figure
  \ref{fig:powerlaw_points} for a graphical example of this
  distribution.
\item A two-component Gaussian density
  \[
  P(x) =
  \sum_{i=1}^2w_i\frac{e^{-(x-c_i)^2/2\sigma_i^2}}{\sqrt{2\pi\sigma_i^2}}
  \frac{1}{\Phi(1;c_i,\sigma_i^2) - \Phi(0;c_i,\sigma_i^2)}
  \]
  with $\sum_i w_i=1$ and probability
  \[
  P(<x) = \sum_{i=1}^2w_i\frac{\Phi(x; c_i,\sigma_i^2) - \Phi(0;c_i,\sigma_i^2)}
  {\Phi(1;c_i,\sigma_i^2) - \Phi(0;c_i,\sigma_i^2)}
  \]
  where
  \[
  \Phi(x; c, \sigma^2) = 0.5\left[1 + \erf\left((x -
      c)/\sqrt{2\sigma^2}\right)\right].
  \]
  The function $\Phi(x)$ denotes the cumulative distribution function
  of the normal distribution with mean $c$ and variance $\sigma^2$.
\end{enumerate}
Graphical displays of samples with $N=400$ points from the first two
distributions is shown in Figures \ref{fig:quartic_points} and
\ref{fig:powerlaw_points}, respectively.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17b_100.png}
  \caption{The full-posterior distribution of functions for polynomial
    order $n=16$ and $N=100$ points is sampled and plotted.  The
    shaded red curve shows the envelope enclosing between 10\% and
    90\% of the curves at each $x$ value with the median embeded. The
    best fit function in the MISE sense and the exact density is also
    shown for comparison. The peak of the posterior functions are
    biased toward larger values of $x$.  \label{fig:q_16_100}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17b_400.png}
  \caption{As in Fig. \protect{\ref{fig:q_16_100}} but for a larger
    sample size $N=400$.  Note the reduced bias in the central
    peak. \label{fig:q_16_400}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17b_1000.png}
  \caption{As in Fig. \protect{\ref{fig:q_16_100}} but for a
    larger sample size $N=1000$. Note the \emph{continued} bias
    reduction in the central peak. \label{fig:q_16_1000}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17b_2000.png}
  \caption{As in Fig. \protect{\ref{fig:q_16_100}} but for a
    larger sample size $N=2000$. The bias is nearly gone.
    \label{fig:q_16_2000}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17c_100.png}
  \caption{As in Fig. \protect{\ref{fig:q_16_100}} but for a
    lower-order polynomial, $n=6$ and $N=100$.  As expected a
    fewer points are needed to suppress the bias for a lower order
    approximating function. \label{fig:q_6_100}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17d_400.png}
  \caption{As in Fig. \protect{\ref{fig:q_16_100}} but for
    a 400 point power-law distributed data with exponent
    $\beta=4$. \label{fig:p_6_400}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17e_400.png}
  \caption{As in Fig. \protect{\ref{fig:p_6_400}} but with order
    $n=16$. \label{fig:p_16_400}}
\end{figure}
  
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17g_400.png}
  \caption{As in Fig. \protect{\ref{fig:q_16_100}} but for a 400-point
    sample from the two-component Gaussian distributed data with $c_1=1/4,
    c_2=3/4, \sigma_1^2 = \sigma_2^2=0.0225, w_1=0.7, w_2=0.3$.
    \label{fig:g_16_400}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{fig17g_1000.png}
  \caption{As in Fig. \protect{\ref{fig:g_16_400}} but with 1000 points.
    \label{fig:g_16_1000}}
\end{figure}
  
\subsection{Bias}
\label{sec:bias}

The Bernstein polynomial functional estimator is biased (as expected).
This is clearly seen in the comparison of Figures \ref{fig:q_16_100},
\ref{fig:q_16_400}, \ref{fig:q_16_1000}, and \ref{fig:q_16_2000} which
show the inferred densities from quartic-distributed data for $N=100,
400$, $1000$ and $2000$ points, respectively for $n=16$.  The bias
decreases with increasing information is negligible by $N=2000$.  

Of course, for use galaxy formation models, the information content
provided by the data is not so simply measured as in this simple case.
Still, recalling that luminosity function bins summarize considerable
independent datums, when combined with the appropriate likelihood
function, the effective information dimension is likely to be at least
as large 1000 and probably larger.

For fixed $N$, the bias increases with increasing order $n$, and vice
versa (see Fig. \ref{fig:q_6_100} for an example of $n=6$, $N=100$).
The basis for $n=4$ contains the quartic function and the bias for
the inference using this order vanishes.

The power-law model is well fit by the Bernstein basis by design (that
is, my suggestion of using this polynomial family is based on the
astronomical motivation of power laws).  The bias trends are two-fold:
for small values of $N$, the low $x$ is heavier than the exact value
and the peak at $x\rightarrow1$ tends to be over fit.  Both trends
decrease with increasing $N$.

Trends in the bias for the two-component model is are two-fold.
Similar to the quartic model, the peaks are slightly offset inwards
and outwards; although this trend can be reversed by changing the
locations of the centers, $c_i$.  The estimator appears to bias the
weaker component to higher amplitude (cf. Fig. \ref{fig:g_16_400}).
This effect decreases as $N$ increases, and for the example here, it
is negligible by $N=1000$ (cf. Fig. \ref{fig:g_16_1000}).

\subsection{The dependence on order $n$}

To summarize all tests, increasing order affects the bias owing to
less information per basis function for a fixed sample size $N$, as
described in \S\ref{sec:bias}.  However, for modest order size $n$,
the overall \emph{qualitative} trends with increasing order are weak.
That is, a serious misidentification of unimodal for a multlimodal
distribution is unlikely.  Moreover, since each basis $B_{n,k}(x)$
function peaks at $x=k/n$, the relative coefficient of the coefficient
identifies the location of power in domain $[0,1]$, much like a
wavelet basis or window function.

\section{Arbitrary dimensionality}

As described in the first set of notes, the best-explored application
of nonparametric inference is clustering of points using the latent
variable mechanism.   \emph{Latent} or \emph{unobserved} variables
describe inferred features of the data.  In the case of clustering,
the identification of a data point with a cluster is a latent
variable.  Dirichlet priors, in principle, allow for an arbitrary
number of cluster components.  Monte-Carlo algorithms for sampling
from the posterior distribution rely on special properties
(e.g. conjugacy) of the prior and likelihood distributions.

In our case, we are not modeling simple points but rather
inferring a non-parametric functional form based on a complex,
indirect, relationship between the likelihood and the unknown
function.  This forces us into more general, and less efficiently
sampled, dimension switching algorithms.

The obvious algorithm is the reversible-jump sampler \citep{Green:95},
already implemented in the BIE.  This sampler proposes swaps between
parameter spaces with different dimensionality with carefully
constructed transition probabilities that maintain the detailed
balance condition.  Because the indices of the Bernstein basis
contains information about their locale (recall the peak of $B_{n, k}$
is $k/n$), we can construct a dimension switch algorithm that roughly
preserves the shape of the function on dimension switch by linear
mapping the coefficients of order $n$ to order $n+1$ according the
even split of the unit interval largest from $n$ intervals to $n+1$
intervals.  I have not yet explored this implementation.

\bibliographystyle{plainnat}
\bibliography{master}
\label{sec:ref}

\end{document}

