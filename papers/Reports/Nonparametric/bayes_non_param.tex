\documentclass[12pt]{article}
                                % Don't waste so much paper!
\usepackage[height=9.0in,letterpaper]{geometry}

\usepackage{times}
\usepackage{mathptmx}

\usepackage{amsmath,amssymb}    % for math
\usepackage{subfigure,graphicx} % for figures
\usepackage{natbib}		% bibliography
\usepackage{appendix,fancyhdr}  % nice formatting
\usepackage{longtable}          % for the class list


\usepackage{hyperref}
% \hypersetup{dvips,colorlinks=true,allcolors=blue}
% \hypersetup{dvips}
\hypersetup{
  dvips,
  bookmarks=true,         % show bookmarks bar?
  unicode=false,          % non-Latin characters in Acrobat’s bookmarks
  pdftoolbar=true,        % show Acrobat’s toolbar?
  pdfmenubar=true,        % show Acrobat’s menu?
  pdffitwindow=false,     % window fit to page when opened
  pdfstartview={FitH},    % fits the width of the page to the window
  pdftitle={My title},    % title
  pdfauthor={Author},     % author
  pdfsubject={Subject},   % subject of the document
  pdfcreator={Creator},   % creator of the document
  pdfproducer={Producer}, % producer of the document
  pdfkeywords={keyword1} {key2} {key3}, % list of keywords
  pdfnewwindow=true,      % links in new window
  colorlinks=false,       % false: boxed links; true: colored links
  linkcolor=red,          % color of internal links (change box color with linkbordercolor)
  citecolor=green,        % color of links to bibliography
  filecolor=magenta,      % color of file links
  urlcolor=cyan           % color of external links
}
\usepackage{hypcap}
\usepackage{caption}

%% Add a trick to get around the figure float stuff

\makeatletter                   % Change the catagory code of @
\newenvironment{inlinetable}{%
  \def\@captype{table}%
  \noindent\begin{minipage}{0.999\linewidth}\begin{center}\footnotesize}
    {\end{center}\end{minipage}\smallskip}

\newenvironment{inlinefigure}{%
  \def\@captype{figure}%
  \noindent\begin{minipage}{0.999\linewidth}\begin{center}}
    {\end{center}\end{minipage}\smallskip}
\makeatother                    % Revert to protected

%% For convenience . . . 

\newcommand{\bx}{{\bf x}}
\newcommand{\bt}{{\bf\theta}}
\newcommand{\bp}{{\bf\pi}}
\newcommand{\barm}{{\bar m}}

% \lta and \gta produce > and < signs with twiddle underneath
\def\spose#1{\hbox to 0pt{#1\hss}}
\newcommand{\lta}{\mathrel{\spose{\lower 3pt\hbox{$\mathchar"218$}}
    \raise 2.0pt\hbox{$\mathchar"13C$}}}
\newcommand{\gta}{\mathrel{\spose{\lower 3pt\hbox{$\mathchar"218$}}
    \raise 2.0pt\hbox{$\mathchar"13E$}}}

\let\lesssim=\lta
\let\gtrsim=\gta

\newcommand{\msun}{{\rm\,M_\odot}}
\newcommand{\AU}{\,\hbox{\rm AU}}
\newcommand{\kms}{{\rm\,km\,s^{-1}}}
\newcommand{\pc}{\,\hbox{\rm pc}}
\newcommand{\kpc}{{\rm\,kpc}}
\newcommand{\Myr}{\,\hbox{\rm Myr}}
\newcommand{\Byr}{\,\hbox{\rm Byr}}
\newcommand{\Gyr}{\,\hbox{\rm Gyr}}
\newcommand{\gyr}{\,\hbox{\rm Gyr}}
% \newcommand{\mod}{\mathop{\rm mod}\nolimits}
\newcommand{\sgn}{\mathop{\rm sgn}\nolimits}
\newcommand{\ext}{{\hbox{\ninerm ext}}}
\newcommand{\resp}{{\hbox{\ninerm resp}}}
\mathchardef\star="313F
\newcommand{\bth}{{\bf\theta}}
\newcommand{\bdata}{{\bf D}}
\newcommand{\bdth}{{\Delta{\bf\theta}}}
% \newcommand{\Re}{\mathop{\it Re}\nolimits}
% \newcommand{\Im}{\mathop{\it Im}\nolimits}
\newcommand{\Ei}{\mathop{\rm Ei}\nolimits}

\newcommand{\apj}  {ApJ}
\newcommand{\apjl} {ApJL}
\newcommand{\apjs} {ApJS}
\newcommand{\aj}   {AJ}
\newcommand{\mnras}{MNRAS}

\newcommand{\model}{{\cal M}}
\newcommand{\data} {{\bf D}}
\newcommand{\param}{\boldsymbol{\theta}}

%%% 
%%% End definitions
%%% 

\pagestyle{fancy}

\setlength{\headheight}{15pt}
\pagestyle{fancy}

% \renewcommand{\chaptermark}[1]{\markboth{#1}{}}
% \renewcommand{\sectionmark}[1]{\markright{#1}{}}
 
\fancyhf{}
\fancyhead[LE,RO]{\thepage}
\fancyhead[RE]{\textit{\nouppercase{\leftmark}}}
\fancyhead[LO]{\textit{\nouppercase{\rightmark}}}
 
% \shorttitle{Nonparametric functional inference}
% \shortauthors{Weinberg}

\begin{document}

\title{Notes on Bayesian nonparametric functional inference for
  galaxy formation models}

\author{Martin D. Weinberg\\
  Department of Astronomy\\
  University of Massachusetts, Amherst, MA, USA 01003;\\
  {\it weinberg@astro.umass.edu}
}

\date{\today}

\maketitle

\tableofcontents

\section{Introduction}
\label{sec:intro}

These notes summarize my current progress to date on specifying a
procedure for inferring an unknown process using a random prior in
function space.  It is an interesting thing to contemplate, and has
many possible applications.  In our case, this process might be the
run of star formation with mass.

Of course, we are not alone.  Bayesian nonparametrics is \emph{hot}
field and, after being told by our latest IIS NSF panel that I had not
cited enough of the literature, I spent several weeks reading two
graduate-level math monographs \citep[e.g.][]{Ghosh:Ramamoorthi:2003}
and multiple review articles \citep[e.g.][among others]{Orbanz:2014,
  Ghosal.vanderVaart:2013} and research papers \citep{Ferguson:1973,
  Lavine:1994, MacEachern:1998, Neal:2000, Ghosal:2001, Jain:2004,
  Walker:2007} on what appeared to be relevant investigations.  I had
some hope that some new approach might present itself.  However,
ironically, my proposal is not very different than what I proposed to
the NSF last year.  At least now I understand some more of the theory
and practice in this field.

In these notes, I will describe \emph{very} briefly the basic trends
in Bayesian nonparametrics that are most closely related to our
problem.  The most common use of Bayesian nonparametrics is in density
estimation and cluster analysis, with and without latent variables for
classification.  In all of these problems, the models describe data
directly.

In our problem, the nonparametric function is indirect in the sense
that the random process does not describe data but rather is part of
hierarchical random process (a semi-analytic model or empirical model)
that produces data indirectly.  Therefore, the problem is
significantly different than the standard applications, so that we are
plowing new ground.  Also, because of its indirect relation to the
data, nearly all of the elegant ``tricks'' invented for determining
the posterior distributions for nonparametric models do not work for
us which makes our inference more computationally challenging (as
usual).

\section{Background}
\label{sec:background}

A nonparametric model is a model on an infinite dimensional parameter
space. The parameter space represents the set of all possible
solutions for the inference--for example, the set of smooth functions
in nonlinear regression, or of all probability densities in a density
estimation problem. A Bayesian nonparametric model defines a prior
distribution on such an infinite dimensional space, and the
traditional prior assumptions (e.g. ``the parameter is likely to be
small'') are replaced by structural assumptions (``the function is
likely to be smooth'').  Of course in practice, computation does not
involve an infinite number of parameters; rather, the parameters
describing the nonparametric specification do not have independent
interpretation but are only used to compose the function that they
represent.  I will make this explicit in \S\ref{sec:bernstein}.

So why adopt the nonparametric Bayesian approach for inference?
Nonparametric (and semiparametric\footnote{A \emph{semiparametric}
  model has both parametric and nonparametric components.  So
  Zhankui's empirical model is truly a semiparametric model.}) models
can avoid the arbitrary and possibly unverifiable assumptions inherent
in parametric models.  We have spoken together about this at length
(i.e. why do we assume that everything is described by power laws?) so
I will spare you further discussion.

\subsection{Overall motivation}

In the Bayesian paradigm, a prior distribution is assigned to all
unknown quantities (i.e. parameters) for one's model.  In the Bayesian
{\em nonparametric} paradigm, one abstracts the prior specification
further and considers priors on some space of functions.  In this
sense, a nonparametric inference has an infinite number of parameters,
in principle, and a large number of parameters, in practice.  In
either case, an inference is made from the ``posterior distribution'',
the conditional distribution of all parameters given the data. A model
completely specifies the conditional distribution of all observable
given all unobserved quantities, or parameters, while a prior
distribution specifies the distribution of all unobservables.  The
posterior distribution involves an inversion of the order of
conditioning.  Although it is a difficult read, much of the literature
in the journals and monographs are measure-theoretic proofs
demonstrating that posterior existence is guaranteed under mild
conditions on the relevant spaces.

\subsection{Some details}

Parametric models make restrictive assumptions about the data
generating mechanism, which may cause serious bias in inference. In
the Bayesian framework a parametric model assumption can be viewed as
an extremely strong prior opinion. I believe that this may be true for
the galaxy formation problem, as you know.  A parametric model
specification assumes that the data $X$ given the model parameters
$\theta$ according to some defined distribution
$p_\theta$. Mathematically, one may write this as: $X|\theta \sim
p_\theta$, for $\theta\in\Theta \subset {\cal R}^d$, with a prior
$\theta\sim\pi$.\footnote{Recall, $\sim$ is the standard notation for
  ``distributed as''.  This is why I try to get astronomers to not use
  $\sim$ to be ``crudely and approximately equal to''.}  A
nonparametric model adds an additional layer by assuming that
$p_\theta$ is an element in a space of functions $\Pi$.  In the
nonparametric framework, this looks like: $X|p \sim p$, for $p \in P$
with $P$ a large set of densities equipped with a prior $p \sim \Pi$.
Mathematically, the parametric model puts all of weight on one
specific density in $P$: $\Pi\{p_\theta : \theta\in\Theta\} = 1$.
Thus parametric modeling is equivalent to insisting on a prior that
assigns probability one to a thin subset of all densities.  This is a
very strong prior opinion indeed.

To some extent the nonparametric Bayesian approach also solves the
problem of partial specification that plagued us in Yu Lu's
project. Often a model is specified incompletely, without describing
every detail of the data generating mechanism.  Our often discussed
problem of not knowing the underlying error distribution for observed
data can be solved nonparametrically.  For example, given some data of
unknown provenance, we may assume that the errors are uncorrelated and
centered (not always true) with known variance but unknown
distribution, a parametric Bayesian approach cannot proceed
further. However, a nonparametric Bayes approach can put a prior on
the space of densities with mean zero and consider that as a prior on
the error distribution. More generally, incomplete model assumptions
may be complemented by general assumptions involving
infinite-dimensional parameters in order to build a complete model,
which a nonparametric Bayesian approach can equip with
infinite-dimensional priors.

\subsection{State of the art}
\label{sec:sota}

The literature on this subject seems to concentrate on Dirichlet
process mixture models, largely because the math is beautiful,
tractable, and the sampling algorithms are computationally efficient.

The Dirichlet process (hereafter DP) is a family of nonparametric
Bayesian models which are commonly used for density estimation,
semiparametric modeling and model selection and model averaging. The
DPs are nonparametric in a sense that they have infinite number of
parameters. Since they are treated in a Bayesian approach we are able
to construct large models with infinite parameters which we integrate
out to avoid overfitting. It can be shown that DPs can be represented
in different ways all of which are mathematically equivalent. Few
common ways to represent a DP is with the Blackwell-MacQueen
\citep{Blackwell.MacQueen:1973} urn scheme, the stick-breaking
construction and the Chinese Restaurant Process \citep{Pitman:1995}.

The parameters of the DP have a Dirichlet distribution (from which the
process gets its name).  In short, the Dirichlet distribution is the
multidimensional generalization of a Beta distribution with the
density:
\begin{equation}
P(\theta_1,\theta_2,\ldots, \theta_m) = 
\frac{\Gamma(\sum_k\alpha_k)}{\prod_k\Gamma(\alpha_k)}
\prod^{m}_{k=1}\theta_k^{\alpha_k-1}
\label{eq:dirichlet}
\end{equation}
where $\theta_k\in[0,1]$ and $\sum_{k=1}^m \theta_k = 1$ and
$\Gamma(\cdot)$ is the usual complete Gamma function.  The sample
space for $\Theta$, therefore, is the $m-1$ dimensional simplex.

One says that a distribution $G$ is a Dirichlet \emph{process} with $G
\sim DP(\alpha, G_0 )$ where $G_0$ is a base distribution, and
$\alpha$ is a positive scaling parameter.  The random distribution $G$
is
\begin{equation}
G\left(\theta\right)=\sum_{k=1}^{m} w_{k}\delta_{\theta_{k}}\left(\theta\right)
\label{eq:randomG}
\end{equation}
where $w_{k}$ are defined by a recursive scheme that repeatedly
samples from a $\mathrm{Beta}\left(1,\alpha\right)$
distribution\footnote{There is a more formation definition of the GP
  but we do not need it here.}.  The simplest way to understand the
$w_k$ is the stick-breaking construction.  Constructively, begin with
a stick of length 1.  Break off a piece whose size is follows the Beta
distribution above.  This is $w_1$.  Then, apply the same prodecure to
the remaining stick (which has size $1-w_1$), and so on.  In this way,
$G$ is a random probability measure that has the same support as the
base measure $G_0$.  The Dirichlet process, then, is a distribution
over distributions.  Wikipedia has a nice, albeit very concise,
description with references.

A standard application application of DPs is in density
estimation. Suppose we are interested in modeling the density from
which a given set of observations is drawn. To avoid limiting
ourselves to any parametric class, we may again use a nonparametric
prior over all densities. Here again DPs are a popular.  However note
from equation (\ref{eq:randomG}) that distributions drawn from a DP
are discrete, thus do not have smooth densities. The solution is to
smooth out draws from the DP with a kernel.  Let $G \sim DP(\alpha,
G_0)$ and ) and let $f(x|\theta)$ be a family of densities (kernels)
indexed by $\theta$ (i.e. $f(\cdot|\cdot)$ might be a Gaussian of
fixed width and mean of $\theta$).  We use the following as our
nonparametric density of $x$:
\begin{equation}
p(x) = \int f(x|\theta)G(\theta)\,d\theta.
\label{eq:DPMM}
\end{equation}
This construction is called a DP mixture model (hereafter DPMM).
There are a large number of extensions and algorithms that implement
DPs and DPMMs.

\subsection{Implementation and usage}
\label{sec:bernstein}

Many papers use DPMMs to perform clustering analyses over sets of
data. With DPMMs we construct a single mixture model in which the
number of mixture components is formally infinite. This means that
DPMM does not require us to define from the beginning the number of
clusters (which in this case it is infinite) and it allows us to adapt
the number of active clusters as we feed more data to our model over
time.

After spending multiple weeks reading, I learned that nearly all
applications use DPMMs to represent the process in equation
(\ref{eq:DPMM}) where the density of the observed data is $p(x)$.  In
more detail, suppose one has a data set $\{x_i\}, i=1,\ldots,n$ where
the order of the $x_i$ is unimportant.  The likelihood function may be
written $L(\{x_i\} | \theta) = \prod_{i=1}^nf(x_i|\theta)$ where
$f(x|\theta)$ is the kernel from equation (\ref{eq:DPMM}).  By Bayes
theorem, we have the posterior:
\begin{equation}
  p(G|\{x_i\}) = \frac{\prod_{i=1}^nf(x_i|\theta)G(\theta)}{p(\{x_i\})}
\end{equation}
with posterior predictions of the form:
\begin{equation}
  p(y|\{x_i\}) = \int g(y|G) p(G|\{x_i\}) dG
\end{equation}
where $G$ is the prior for the random functions as previously
described.  By using kernels which are conjugate to the DP, there are
many clever algorithms for sampling from the posterior (using Gibbs
sampling) and even in the non-conjugate case there are a few clever
algorithms that rely on an intermediate `layer' of latent variables
that identify individual datums with particular discrete components of
the prior $G$.

\section{Application to galaxy formation}

\subsection{Particulars of this problem}

Because the random process of interest to us is embedded in our galaxy
formation model rather than generating the data directly, our problem
does not fit into any of the standard categories described in the
literature, alas.  We want to infer the function form of processes
which are related to the data \emph{indirectly}.  The main difference:
our likelihood function is a function of the function of interest.
Therefore, all of the nice implementation work in the literature which
assumes that the nonparametric function models the data itself does
not apply.

Specifically, the SAM or empirical-model likelihood function is a
functional of the nonparametric function for our inference.  Most of
the clever nonparametric algorithms assumes that the data can be used
to condition the sampling or intermediate sampling distribution, akin
to approaches used in machine learning.  This feature leads to Gibbs
sampling algorithms that assume conjugate likelihood
functions---efficient because all samples are accepted---and simple
easy-to-sample Metropolis-Hastings algorithms for non-conjugate
likelihood functions \citep[e.g.][]{Neal:2000}.


\subsection{Bernstein polynomials}

However, some the machinery built by the mathematicians along the way
may be used still.  In particular, in a very nice set of papers
\citet{Petrone:1999,Petrone:1999b} and \citet{Petrone.Wasserman:2002}
showed that at DPMM with Bernstein polynomial `kernels' are consistent
estimators for functions in the finite interval.  This has been
extended to multivariate polynomials since.  Of course the unit
interval may be scaled to any finite interval of interest.

\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{bern_10.png}
  \caption{The Bernstein polynomial basis of order $n=10$. \label{fig:bp}}
\end{figure}

The Bernstein polynomial of degree $n$ is constructed from a basis
basis of polynomials
\begin{equation}
  B_{k,n}(x) = {n \choose k} x^{k} \left( 1 - x \right)^{n - k}, \quad
  k = 0, \ldots, n.
    \label{eq:bbasis}
\end{equation}
where ${n \choose k}$ is the usual binomial coefficient.  See Figure
\ref{fig:bp} for a graphical example of this basis.  An Bernstein
polynomial is then
\begin{equation}
    B_n(x) = \sum_{k=0}^{n} \beta_{k,n} B_{k,n}(x)
    \label{eq:bern}
\end{equation}
which is at most order $n$ (\emph{at most} because some number of
$\beta_{k,n}$ could be zero-valued).  Although these polynomials have
many elegant properties, the basis is \emph{not} orthogonal.  However,
it is clear that if $\beta_{k,n}\ge0$ with at least one
$\beta_{k,n}>0$, then $B_n>0$.  And most importantly, for any
continuous function $f(x)$, it can be proven that
\begin{equation}
    \lim_{n \to \infty}{ B_n(f)(x) } = f(x)
\end{equation}
uniformly on the interval $[0, 1]$; that is, the rate of convergence
does not depend on the value of $x$. 

For our purposes, this means that we can use $B_n$ to represent our
physical rate processes without worrying about difficult to impose
constraints on positivity and with some assurance that we will not be
missing nay important features as long as $n$ is sufficiently large.

There are other functional families that we could explore if these
prove troublesome but these have a long history in the field, so I
advocate these to start.

\section{Method}

\subsection{Specifying the mixture model}

The prior distribution for our application has four separate terms:
\begin{itemize}
\item The prior on the order of the Bernstein polynomial: $p(n)$.
  This could be any sensible distribution, e.g. Poisson, where ${\bar
    n}$ might chosen to prefer low- rather than high-order
  polynomials.  Let $\theta_1 = n$.
\item The Dirichlet distribution for the Bernstein polynomial
  coefficients.  These coefficients are vector of order $n$; let
  $\theta_2=\{\beta_{0,n},\ldots,\beta_{n-1,n}\}$ with $\beta_{n,n} =
  1 -\sum_{k=0}^{n-1}\beta_{k,n}$.
\item There is an overall amplitude, $A_p$, for the Bernstein polynomial
  describing the physical process: $\theta_3 = A_p$.
\item The rest of the prior distribution for the parametric part of
  the SAM or empirical model is lumped into the vector $\theta_4$.
\end{itemize}
Altogether, then, the parameter vector for this model is $\theta =
\{\theta_1,\theta_2,\theta_3,\theta_4\}$. The nonparametric
specification, $\theta_2$, has rank $n$ which is itself is the random
variable $\theta_1$ (rank 1).  The value for $\theta_3$ is typically
already part of our models (i.e. part of $\theta_4$) and I have
separated it out for clarity's sake only.

Although this nonparametric specification increases the dimensionality
of the Bayesian simulation, I would guess that the parameters
specifying the DPMM for the Bernstein polynomials should not be the
bottleneck in MCMC mixing.  To see this, let us treat $n$ and the
$\beta_{k,n}$ separately.

\subsection{Multiple-block Metropolis-Hastings}
\label{sec:blocking}

Because the update of $\beta_{k,n}$ is a constrained process, I am
guessing (and hoping) that nonparametric part of the M-H simulation
will not require an elaborate tempering algorithm.  On the other hand,
mixing is likely to remain a problem for the rest of the model.  This
suggests that we break the simulation into two parts: one for the
nonparametric piece $\{\theta_1, \theta_2\}$ (or possibly multiple
pieces if one specifies multiple nonparametric functions ) and one for
the parametric remainder $\{\theta_3, \theta_4\}$.

For the general MCMC simulation, one can group different variables of
the vector $\theta \sim p(\theta|\cdot)$ into blocks.  Although we did
not explore this approach for our SAM computations, this has been
shown to increase efficiency in large spaces, especially if the
components in each block are strongly correlated.  This will clearly
be the case for our $\theta_2$.  So, we want to partition $\theta$
into $\theta = \{\theta_1,\ldots, \theta_r\}$; in the model specified
above, we have $r=4$.

To do this, we define: $\theta_{-k}$ to be all the blocks excluding
$\theta_k$.  (In our example, $k=2$.) Then we define $p(\theta_k,
\theta_{−k})$ to be the joint density of $\theta$ regardless of where
$\theta_k$ appears in the list of blocks.  Now, within Block $k$, we
propose a transition $\theta_k\rightarrow\phi_k$.  We define the
proposal transition probability $q_k(\theta_k, \phi_k|\theta_{−k})$
for this move.  Our Metropolis-Hastings update probability then
becomes
\begin{equation}
  Q_k(\theta_k,\phi_k|\theta_{-k}) = \min\left\{
    \frac{p(\phi_k,\theta_{-k}) q_k(\phi_k,\theta_k|\theta_{-k})}%
    {p(\theta_k,\theta_{-k}) q_k(\theta_k,\phi_k|\theta_{-k})}
    \right\}.
\end{equation}
That is, $Q_k$ is the probability of transitioning from $\theta_k$ to
$\phi_k$ within the block $k$.  The update of $\theta_k$ is done once
per cycle.  It is straightforward to show that this algorithm obeys
detailed balance.

\subsubsection{Stick breaking}
\label{sec:DPupdate}

I can think of a wide variety of possible proposals for updating the
$\beta_{k,n}$:
\begin{itemize}
\item At each step, pick a particular $\beta_{k,n}$ at random and
  propose a new $\beta^\prime_{k,n}$ according to some
  $q(\beta,\beta^\prime)$ and renormalize the remaining values so that
  $\sum_{k=1}^n\beta^\prime_{k,n} = 1$.
\item At each step, cycle through each $\beta_{k,n}$, $k=1,\ldots,n$,
  renormalizing at the end.
\item Another possibility is to update each component in $\beta_{k,n}$
  in $\theta_2$ while holding the remaining values $\beta_{j\not=k,n}$
  fixed.  This is equivalent to blocking singleton terms within
  $\theta_2$ block.  Intuitively, I favor this approach because it
  will more \emph{gently} probes the directions that may improve the
  overall likelihood and therefore will probably have the best mixing.
  Of course, this comes at the price of additional likelihood
  evaluations.
\end{itemize}
And, etc., there are many possible similar variations. This may
require some experimentation but the simple change at each step is
mostly like be the more effective, controllable choice.  As long as we
define a transition probability $q(\beta_{k,n},\beta^\prime_{k,n})$
each of these updates takes the standard Metropolis-Hastings form.

\subsubsection{Order changing}
\label{sec:orderchange}

One option is not to change the order $n$ at all; that is, assume that
$n$ is some large value and rely on the consistency properties of the
Bernstein polynomials.  This is not as crazy as it sounds since we
really do not care about the nonparametric parameters but only the
functions they represent.  I would guess that $n=\cal{O}(10)$ would be
a good place to start.

Alternatively, we can propose to increase or decrease the order at
some rate using a reversible-jump algorithm.  This is straightforward
and already built into the BIE.  To summarize the strategy: one
proposes to either add or remove a component (that is, increase or
decrease the value $n$ by 1).  This may be done by breaking the
smallest stick or joining the two smallest sticks respectively.  I
believe that this preserves reversibility, and the prior probabilities
are easily computed.  Moreover, the new Bernstein polynomial will
resemble the original one so, hopefully, mixing will not be
jeopardized.

\subsection{Summary: inferring the posterior}

I propose the following the MCMC sampling algorithm.

\noindent
{\bf First step:}
\begin{enumerate}
\item Choose $\theta_1=n$ from the prior for the polynomial order $p(n)$.
\item Generate $\theta_2=\{\beta_{k,n}\}$ from the Dirichlet
  distribution.
\item Choose the amplitude $\theta_3=A_p$ and the other parametric
  parameters $\theta_4$ from their respective priors.
\end{enumerate}

\noindent
{\bf Subsequent steps:}
\begin{enumerate}
\item Choose whether to propose a change in $n$ according to some
  constant frequency (could be every step). If no change is proposed
  skip to Item (3).
\item Apply the order-changing algorithm described in
  \S\ref{sec:orderchange} by choosing $n\rightarrow n-1$ or
  $n\rightarrow n+1$ and either join or break sticks to get a new
  Bernstein polynomial.  Use the Reversible-Jump algorithm to accept
  or reject this proposal.  Skip to Item (4).
\item Apply the simple Metropolis-Hastings update to the $\theta_2$
  block following one of the strategies outlined in \S\ref{sec:DPupdate}
\item Apply one of the standard MCMC algorithms to the remaining
  blocks $\{\theta_3, \theta_4\}$.  This might be either straight
  Differential Evolution (DE) or its tempered variant, as we have used
  in the past.
\end{enumerate}

\subsection{Interpreting the posterior}

Just to reiterate an important point here: generally, one should not
attempt to assign meaning to the inferred values of parameters
describing the Bernstein polynomial $B_n$.  Rather it is the posterior
distribution of the functions in the function space itself that are
meaningful.  Therefore, in practice, the posterior analysis might be a
graphical study of the $B_n(x)$ and a description of its variance as
function of $x$.  For example, if all $B_n(x)$ in the posterior are
double-humped, we will have learned something of significance.  On the
other hand if 50\% are multimodal and 50\% are unimodal, we will have
learned that our data does not constrain our physical process.

\section{Next steps}

If this sounds interesting, we need to start somewhere.  I believe
that it would be straightforward for Zhankui to include the Bernstein
polynomial instead of the usual power law (eq. \ref{eq:bern}) using
the basis from equation (\ref{eq:bbasis}) with weights determined by
stick breaking (see \S\ref{sec:sota}) with the Dirichlet distribution
(eq. \ref{eq:dirichlet}) as the prior.  The simplest test would fix
the order $n$ ($n=10$ say) and try it.  I could easily imagine that
one could have a simple implementation up and running with a day of
work.  I plan to come up with some simplified tests myself (i.e. a toy
empirical model) and try it in the BIE.  Zhankui, if you have any
suggestions for such a simple toy model, please let me know.

For the more general problem, I have no idea whether one can use the
nested sampler for the general problem with a non-trivial prior on
$n$.  It seems unlikely, as I understand the approach, especially if
one wants the full random prior DP which has arbitrary dimension.  The
fixed dimension model seems well-defined with respect to the nested
sampler but I do not know how well the current algorithms will handle
the large number dimensions without some sort of blocking close
coupling within the natural parameter blocks.

Regarding MCMC sampling, the slice sampler
\citep{Neal:2003,Walker:2007} has been used with good success for
non-conjugate DPMMs.  The slice sampler is a non-Metropolis-Hastings
scheme that that adapts to characteristics of the distribution being
sampled by generalizing the idea of random variate generation from the
cumulative distribution function.  Recall that one can sample from a
distribution by sampling uniformly from the region under the plot of
its density function. A Markov chain that converges to this uniform
distribution can be constructed by alternating uniform sampling in the
vertical direction with uniform sampling from the horizontal ``slice''
defined by the current vertical position, or more generally, with some
update that leaves the uniform distribution over this slice
invariant. Such ``slice sampling'' methods are easily implemented for
univariate distributions, and can be used to sample from a
multivariate distribution by updating each variable in turn.  I could
imagine implementing and trying the slice sampler for the block $k=2$
as described in \S\ref{sec:blocking} and DE for the parametric
components.

The Bernstein polynomials have straightforward extensions to multiple
dimensions. However, the number of required coefficients to get a good
representation of the underlying function may blow up the number of
dimensions into an unfeasible realm.  However, two and possibly three,
may still be practical.

Finally, please send questions and comments.  In particular, I am
willing to take on the project of implementing this in the BIE, and
certainly will at some point.  The priority will depend on your
feedback and interest in this method.

\bibliographystyle{plainnat}
\bibliography{master}
\label{sec:ref}

\end{document}

