
#include <CVectorM.h>

// mixed-type operations. I do these by casting to the more
// general type and calling that class's operator.


// dot product

Complex operator*(const VectorM &v, const CVectorM &c)
	{CVectorM tmp(v); return tmp*c;}
Complex operator*(const CVectorM &c, const VectorM &v)
	{CVectorM tmp(v); return tmp*c;}


// vector addition


CVectorM operator+(const VectorM &v, const CVectorM &c)
	{CVectorM tmp(v); return tmp+c;}

CVectorM operator+(const CVectorM &c, const VectorM &v)
	{CVectorM tmp(v); return tmp+c;}

CVectorM operator-(const VectorM &v, const CVectorM &c)
	{CVectorM tmp(v); return tmp - c;}

CVectorM operator-(const CVectorM &c, const VectorM &v)
	{CVectorM tmp(v); return c - tmp;}


// matrix-vector multiplication

/*
CVectorM operator*(const CMatrixM &c, const VectorM &v)
	{CVectorM tmp(v); return c*tmp;}
	*/

CVectorM operator*(const MatrixM &m, const CVectorM &c)
	{CMatrixM tmp(m); return tmp*c;}

CVectorM operator*(const CVectorM &c, const MatrixM &m)
	{CMatrixM tmp(m); return c*tmp;}

CVectorM operator*(const VectorM &v, const CMatrixM &c)
	{CVectorM tmp(v); return tmp*c;}


// matrix-matrix multiplication

CMatrixM operator*(const CMatrixM &c, const MatrixM &m)
	{CMatrixM tmp(m); return c*tmp;}

CMatrixM operator*(const MatrixM &m, const CMatrixM &c)
	{CMatrixM tmp(m); return tmp*c;}


// matrix addition

CMatrixM operator+(const MatrixM &m, const CMatrixM &c)
	{CMatrixM tmp(m); return tmp+c;}

CMatrixM operator+(const CMatrixM &c, const MatrixM &m)
	{CMatrixM tmp(m); return tmp+c;}

CMatrixM operator-(const MatrixM &m, const CMatrixM &c)
	{CMatrixM tmp(m); return tmp-c;}

CMatrixM operator-(const CMatrixM &c, const MatrixM &m)
	{CMatrixM tmp(m); return c-tmp;}


// multiplication by scalars


CMatrixM operator*(const MatrixM &m, const Complex &c)
	{CMatrixM tmp(m); return tmp*c;}

CMatrixM operator*(const Complex &c, const MatrixM &m)
	{CMatrixM tmp(m); return tmp*c;}

CVectorM operator*(const VectorM &v, const Complex &c)
	{CVectorM tmp(v); return tmp*c;}

CVectorM operator*(const Complex &c, const VectorM &v)
	{CVectorM tmp(v); return tmp*c;}

CMatrixM operator/(const MatrixM &m, const Complex &c)
	{CMatrixM tmp(m); return tmp/c;}

CVectorM operator/(const VectorM &v, const Complex &c)
	{CVectorM tmp(v); return tmp/c;}


	
