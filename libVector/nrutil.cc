#include <cstdlib>
#include <iostream>

using namespace std;

#include <numerical.h>

void nrerror(const char *error_text)
{
  cerr << "Numerical Recipes run-time error...\n";
  cerr << error_text << endl;
  cerr << "...now exiting to system...\n";
  exit(1);
}

double *nr_vector(int nl, int nh)
{
  double *v = 0;
    
  try {
    v = new double [nh-nl+1];
  } 
  catch (...) {
    nrerror("allocation failure in nr_vector()");
  }
  return v-nl;
}

int *nr_ivector(int nl, int nh)
{
  int *v = 0;

  try {
    v = new int [nh-nl+1];
  }
  catch (...) {
    nrerror("allocation failure in nr_ivector()");
  }
  return v-nl;
}

double *dvector(int nl, int nh)
{
  double *v = 0;

  try {
    v = new double [nh-nl+1];
  }
  catch (...) {
    nrerror("allocation failure in dvector()");
  }
  return v-nl;
}



double **nr_matrix(int nrl, int nrh, int ncl, int nch)
{
  double **m = 0;

  try {
    m = new double* [nrh-nrl+1];
  }
  catch (...) {
    nrerror("allocation failure 1 in nr_matrix()");
  }
  m -= nrl;

  for(int i=nrl; i<=nrh; i++) {
    try {
      m[i] = new double [nch-ncl+1];
    }
    catch (...) {
      nrerror("allocation failure 2 in nr_matrix()");
    }
    m[i] -= ncl;
  }
  return m;
}

double **dmatrix(int nrl, int nrh, int ncl, int nch)
{
  double **m = 0;

  try {
    m = new double* [nrh-nrl+1];
  }
  catch (...) {
    nrerror("allocation failure 1 in dmatrix()");
  }
  m -= nrl;

  for (int i=nrl; i<=nrh; i++) {
    try {
      m[i] = new double [nch-ncl+1];
    }
    catch (...) {
      nrerror("allocation failure 2 in dmatrix()");
    }
    m[i] -= ncl;
  }
  return m;
}

int **imatrix( int nrl, int nrh, int ncl, int nch)
{
  int **m = 0;

  try {
    m = new int* [nrh-nrl+1];
  }
  catch (...) {
    nrerror("allocation failure 1 in imatrix()");
  }
  m -= nrl;

  for (int i=nrl; i<=nrh; i++) {
    try {
      m[i] = new int [nch-ncl+1];
    }
    catch (...) {
      nrerror("allocation failure 2 in imatrix()");
    }
    m[i] -= ncl;
  }
  return m;
}



double **nr_submatrix(double **a, int oldrl, int oldrh, 
	int oldcl, int oldch, int newrl, int newcl)
{
  double **m = 0;

  try {
    m= new double* [oldrh-oldrl+1];
  }
  catch (...) {
    nrerror("allocation failure in submatrix()");
  }
  m -= newrl;

  for(int i=oldrl, j=newrl; i<=oldrh; i++, j++) m[j] = a[i] + oldcl - newcl;

  return m;
}



void free_nr_vector(double *v, int nl, int nh)
{
  delete [] (v+nl);
}

void free_nr_ivector(int *v, int nl, int nh)
{
  delete [] (v+nl);
}

void free_dvector(double *v, int nl, int nh)
{
  delete [] (v+nl);
}



void free_nr_matrix(double **m, int nrl, int nrh, int ncl, int nch)
{
  for (int i=nrh; i>=nrl; i--) delete [] (m[i]+ncl);
  delete [] (m+nrl);
}

void free_dmatrix(double **m, int nrl, int nrh, int ncl, int nch)
{
  for (int i=nrh; i>=nrl; i--) delete [] (m[i]+ncl);
  delete [] (m+nrl);
}

void free_imatrix(int **m, int nrl, int nrh, int ncl, int nch)
{
  for (int i=nrh; i>=nrl; i--) delete [] (m[i]+ncl);
  delete [] (m+nrl);
}



void free_nr_submatrix(double **b, int nrl, int nrh, int ncl, int nch)
{
  delete [] (b+nrl);
}



double **convert_nr_matrix(double *a, int nrl, int nrh, int ncl, int nch)
{
  double **m = 0;
  int nrow = nrh - nrl + 1;
  int ncol = nch - ncl + 1;

  try {
    m = new double* [nrow];
  }
  catch (...) {
    nrerror("allocation failure in convert_matrix()");
  }
  m -= nrl;

  for(int i=0, j=nrl; i<=nrow-1; i++, j++) m[j] = a + ncol*i - ncl;
  return m;
}



void free_convert_nr_matrix(double **b, int nrl, int nrh, int ncl, int nch)
{
  delete [] (b+nrl);
}

