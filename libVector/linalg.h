
int lu_decomp(MatrixM& a, int* indx, double& d);
int linear_solve(MatrixM& a, VectorM& b, VectorM& x);
void lu_backsub(MatrixM& a, int* indx, VectorM& b);
int inverse(MatrixM& a, MatrixM& b);
void improve(MatrixM& a, MatrixM& lud, int* indx, VectorM& b, VectorM& x);
double determinant(MatrixM& a, bool debug=false);
double lu_determinant(MatrixM& a, double& d);
MatrixM sub_matrix(MatrixM& in, 
		   int ibeg, int iend, int jbeg, int jend, 
		   int ioff=0, int joff=0);

void embed_matrix(MatrixM& to, MatrixM& from, int rbeg, int cbeg);
void cholesky_decomp(MatrixM& a, VectorM& p);
