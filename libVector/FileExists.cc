#include <string>
#include <sys/stat.h>

//
// *** Check for file existence ***
// We could make this throwable
//
bool FileExists(const std::string& filename) 
{
  bool ret;
  int status;
  struct stat info;

  // Get the file attributes
  status = stat(filename.c_str(), &info);
  if(status == 0) {
    // Successful! The file MUST exist . . .
    ret = true;
  } else {
    // We could check for, and report, other issues here . . .
    ret = false;
  }
  
  return ret;
}
