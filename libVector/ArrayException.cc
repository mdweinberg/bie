#include <iostream>
#include <sstream>

using namespace std;

#include <ArrayException.h>

string ArrayException::getErrorMessage()
{
  // This combines the error name, error message, and the throw locations.
  // errormessage << ends;  removed for conversion to stringstream
  // char * buffer = errormessage.str();
  //
  string buffer = errormessage.str();

  ostringstream wholemessage;
  wholemessage << exceptionname << ": " << buffer << endl;
  wholemessage << "Thrown from " << sourcefilename << ":" << sourcelinenumber;

  return wholemessage.str();
}

ArrayException::ArrayException
(string exceptionname, string message, 
 string cname, string sourcefilename, int sourcelinenumber)
{
  errormessage.str("");
  this->sourcefilename   = sourcefilename;
  this->sourcelinenumber = sourcelinenumber; 
  this->exceptionname = exceptionname;
  errormessage << message;
}

ArrayException::ArrayException(string cname,
			       string sourcefilename, int sourcelinenumber)
{
  errormessage.str("");
  errormessage << "In <" << cname << ">: ";
  this->sourcefilename   = sourcefilename;
  this->sourcelinenumber = sourcelinenumber; 
}

ArrayException::ArrayException(const ArrayException& e)
{
  exceptionname = e.exceptionname;
  errormessage.str(e.errormessage.str());
  this->sourcefilename   = e.sourcefilename;
  this->sourcelinenumber = e.sourcelinenumber; 
}

BoundsException::BoundsException
(string error, string cname, string sourcefilename, int sourcelinenumber)
  : ArrayException(cname, sourcefilename, sourcelinenumber)
{
  exceptionname = "Bounds Exception";
  errormessage << error;
}

RangeException::RangeException
(int v, int l, int h, string type,
 string cname, string sourcefilename, int sourcelinenumber)
  : ArrayException(cname, sourcefilename, sourcelinenumber)
{
  exceptionname = "Range Exception";
  errormessage << "The index value (" << v << ") for " << type 
	       << " must be in the range [" << l << ", " << h << "].";
}

OperationException::OperationException
(string op, string cname, string sourcefilename, int sourcelinenumber)
  : ArrayException(cname, sourcefilename, sourcelinenumber)
{
  exceptionname = "Operation Exception";
  errormessage << "Bad operation " << op;
}

AllocationException::AllocationException
(int size, string type, 
 string cname, string sourcefilename, int sourcelinenumber)
  : ArrayException(cname, sourcefilename, sourcelinenumber)
{
  exceptionname = "Allocation Exception";
  errormessage << "Error allocating " << size << " elements of type "
	       << type;
}

