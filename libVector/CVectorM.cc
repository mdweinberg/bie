#include <cmath>
#include <cstdlib>
#include <cassert>
#include <string>
#include <iomanip>

#include <CVectorM.h>

const string CVectorM::cname = "class CVector";
const string CMatrixM::cname = "class CMatrix";

/**
   Default constructor; make a null vector.
*/
CVectorM::CVectorM()
{
  low=0;
  high=0;
  elements = NULL;
}


CVectorM::CVectorM(int l, int h)
{
  low=0;
  high=0;
  elements = NULL;
  setsize(l, h);
}

CVectorM::CVectorM(int l, int h, double *v)
{
  low=0;
  high=0;
  elements = NULL;
  setsize(l, h);
  
  for (int i=low; i<=high; i++) elements[i] = v[i];
}

CVectorM::CVectorM(int l, int h, Complex *v)
{
  low=0;
  high=0;
  elements = NULL;
  setsize(l, h);
  
  for (int i=low; i<=high; i++) elements[i] = v[i];
}


/*
  Copy constructor; create a new vector which is a copy of another.
*/
CVectorM::CVectorM(const CVectorM &v)
{
  low=0;
  high=0;
  elements = NULL;
  setsize(v.low, v.high);
  
  for (int i=low; i<=high; i++) elements[i] = v.elements[i];
}

CVectorM::CVectorM(const VectorM &v)
{
  low=0;
  high=0;
  elements = NULL;
  setsize(v.low, v.high);
  
  for (int i=low; i<=high; i++) elements[i] = v.elements[i];
}




/*
  Destructor. Free elements if it exists.
*/

CVectorM::~CVectorM()
{
  delete [] (elements+low);
  //	if ((elements+low) != NULL) delete [] (elements+low);
  //	else 
  //	{
  //		puts("WARNING: destructor called for NULL CVectorM elements");
  //		puts("---- usually caused by not using a declared variable");
  //	}
}



/*
  Assignment operator for CVectorM; must be defined as a reference
  so that it can be used on lhs. Compatibility checking is performed;
  the destination vector is allocated if its elements are undefined.
*/

CVectorM &CVectorM::operator=(const CVectorM &v)
{
  // Allow assignment of null vector
  //
  if (v.elements==NULL) {
    elements = NULL;
    low = high = 0;
    return *this;
  }
  
  if (low!=v.low && high!=v.high 
      && (high != 0 || (elements+low) != NULL))
    throw OperationException("=", cname, __FILE__, __LINE__);

  if (high == 0 && low==0 && elements == NULL) setsize(v.low, v.high);
  
  for (int i=low; i<=high; i++) elements[i] = v.elements[i];
  
  return *this;
}



Complex &CVectorM::operator[](int i) const
{
  if (i<low || i>high)
    throw RangeException( i, low, high, "array index", 
			 cname, __FILE__, __LINE__);
  
  return elements[i];
}

void CVectorM::setsize(int l, int h)
{
  // do we need to resize at all?
  //
  if (l==low && h==high) return;
  
  // is the requested size positive? 
  //
  if (h<l) throw BoundsException("invalid size <0", cname, __FILE__, __LINE__);
  
  // delete the old elements if they already exist 
  //
  delete [] (elements + low);
  
  // set the new size 
  //
  low = l;
  high = h;
  
  // allocate the new elements, and offset the pointer 
  //
  elements = new Complex[high-low+1];
  if (elements == NULL) 
    throw AllocationException( high-low+1, "elements", cname, __FILE__, __LINE__);

  elements -= low;
}


void CVectorM::zero(void)
{
  for (int i=low; i<=high; i++) elements[i] = 0.0;
}

Complex *CVectorM::array(int l, int h)
{
  return elements+low-l;
}

Complex *CVectorM::array_copy(int l, int h) const
{
  Complex *ptr = new Complex[h-l+1];
  assert(ptr);
  ptr -= l;
  
  for (int i=low; i<=high; i++) {
    ptr[i] = elements[i];
  }
  return ptr+low-l;
}

CVectorM CVectorM::operator-(void)
{
  CVectorM tmp(low, high);
  
  for (int i=low; i<=high; i++) tmp.elements[i] = -elements[i];
  
  return tmp;
}


CVectorM operator+(const CVectorM &v1, const CVectorM &v2)
{
  if (v1.low != v2.low || v1.high != v2.high)
    throw OperationException("+", "friend (CVectorM, CVectorM)", 
			     __FILE__, __LINE__);
  
  CVectorM tmp(v1.low, v1.high);
  
  for (int i=v1.low; i<=v1.high; i++)
    tmp.elements[i] = v1.elements[i] + v2.elements[i];
  
  return tmp;
}

CVectorM operator-(const CVectorM &v1, const CVectorM &v2)
{
  if (v1.low != v2.low || v1.high != v2.high)
  throw OperationException("-", "friend (CVectorM, CVectorM)",
			   __FILE__, __LINE__);
  
  CVectorM tmp(v1.low, v1.high);
  
  for (int i=v1.low; i<=v1.high; i++)
    tmp.elements[i] = v1.elements[i] - v2.elements[i];
  
  return tmp;
}

CVectorM operator*(const Complex &a, const CVectorM &v)
{
  CVectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = a * v.elements[i];
  
  return tmp;
}

CVectorM operator*(const CVectorM &v, const Complex &a)
{
  CVectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = a * v.elements[i];
  
  return tmp;
}

CVectorM operator/(const CVectorM &v, const Complex &a)
{
  CVectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = v.elements[i]/a;
  
  return tmp;
}


CVectorM operator&(const CVectorM &v1, const VectorM &v2)
{
  if (v1.high != v2.gethigh() || v1.low != v2.getlow())
    throw OperationException("&", "friend (CVectorM, Vector)", 
			     __FILE__, __LINE__);
  
  CVectorM tmp(v1.low, v1.high);
  for (int i=v1.low; i<=v1.high; i++)
    tmp[i] = v1[i]*v2[i];
  
  return tmp;
}


CVectorM operator&(const VectorM &v1, const CVectorM &v2)
{
  if (v1.gethigh() != v2.high || v1.getlow() != v2.low)
    throw OperationException("&", "friend (Vector, CVectorM)", 
			     __FILE__, __LINE__);
  
  CVectorM tmp(v2.low, v2.high);
  for (int i=v2.low; i<=v2.high; i++)
    tmp[i] = v1[i]*v2[i];
  
  return tmp;
}


CVectorM operator*(double a, const CVectorM &v)
{
  CVectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = a * v.elements[i];
  
  return tmp;
}

CVectorM operator*(const CVectorM &v, double a)
{
  CVectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = a * v.elements[i];
  
  return tmp;
}

CVectorM operator/(const CVectorM &v, double a)
{
  CVectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = v.elements[i]/a;
  
  return tmp;
}


CVectorM &CVectorM::operator+=(const CVectorM &v)
{
  if (low != v.low || high != v.high)
    throw OperationException("+=", cname, __FILE__, __LINE__);
  
  for (int i=low; i<=high; i++) elements[i] += v[i];
  
  return *this;
}	

CVectorM &CVectorM::operator-=(const CVectorM &v)
{
  if (low != v.low || high != v.high)
    throw OperationException("-=", cname, __FILE__, __LINE__);
  
  for (int i=low; i<=high; i++) elements[i] -= v[i];
  
  return *this;
}	

CVectorM &CVectorM::operator*=(double a)
{
  for (int i=low; i<=high; i++) elements[i] *= a;

  return *this;
}

CVectorM &CVectorM::operator/=(double a)
{
  for (int i=low; i<=high; i++) elements[i] /= a;
  
  return *this;
}

CVectorM &CVectorM::operator*=(const Complex &a)
{
  for (int i=low; i<=high; i++) elements[i] *= a;
  
  return *this;
}

CVectorM &CVectorM::operator/=(const Complex &a)
{
  for (int i=low; i<=high; i++) elements[i] /= a;
  
  return *this;
}



Complex operator*(const CVectorM &v1, const CVectorM &v2)
{
  Complex tmp;
  
  if (v1.low != v2.low || v1.high != v2.high)
    throw OperationException("*", "friend (CVectorM, CVectorM)", 
			     __FILE__, __LINE__);
  
  tmp = 0.0;
  for (int i=v1.low; i<=v1.high; i++) {
    tmp += v1.elements[i] * v2.elements[i];
  }

  return tmp;
}



void CVectorM::print(ostream& out)
{
  int oldprecision = out.precision();
  
  for (int i=low; i<=high; i++){
    out << std::setprecision(12) << elements[i].real() 
	<< "+ " << elements[i].imag() << " ";
  }
  out << std::endl;
  out.precision(oldprecision);
}

void CVectorM::binwrite(ostream& out)
{
  out.write((char*)&low, sizeof(int));
  out.write((char*)&high, sizeof(int));
  out.write((char*)(elements+low), (high-low+1)*sizeof(Complex));
}

CVectorM CVectorM_binread(istream& in)
{
  int low, high;
  
  in.read((char*)&low, sizeof(int));
  in.read((char*)&high, sizeof(int));
  
  CVectorM tmp(low, high);
  
  in.read((char*)(tmp.elements+low), (high-low+1)*sizeof(Complex));
  
  return tmp;
}


VectorM CVectorM::Re(void)
{
  VectorM tmp(low, high);
  
  for (int i=low; i<=high; i++) tmp[i] = elements[i].real();
  
  return tmp;
}	

VectorM CVectorM::Im(void)
{
  VectorM tmp(low, high);
  
  for (int i=low; i<=high; i++) tmp[i] = elements[i].imag();
  
  return tmp;
}	


CVectorM CVectorM::Conjg(void)
{
  static Complex I(0.0, 1.0);
  CVectorM tmp(low, high);
  
  for (int i=low; i<=high; i++) 
      tmp.elements[i] = elements[i].real() - I * elements[i].imag();
  
  return tmp;
}	


CVectorM Conjg(const CVectorM &c)
{
  CVectorM t(c);
  return t.Conjg();
}

VectorM Re(const CVectorM &c)
{
  CVectorM t(c);
  return t.Re();
}

VectorM Im(const CVectorM &c)
{
  CVectorM t(c);
  return t.Im();
}






CMatrixM::CMatrixM()
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = NULL;
}


CMatrixM::CMatrixM(int rl, int rh, int cl, int ch)
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = NULL;
  setsize(rl, rh, cl, ch);
}


CMatrixM::CMatrixM(int rl, int rh, int cl, int ch, Complex **array)
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = NULL;
  setsize(rl, rh, cl, ch);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] = array[i][j];
  }
}

CMatrixM::CMatrixM(int rl, int rh, int cl, int ch, double **array)
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = NULL;
  setsize(rl, rh, cl, ch);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] = array[i][j];
  }
}

CMatrixM::CMatrixM(const CMatrixM &m)
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = NULL;
  setsize(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] = m.rows[i].elements[j];
  }

}	


CMatrixM::CMatrixM(const MatrixM &m)
{
  rlow  = 0;
  rhigh = 0;
  clow  = 0;
  chigh = 0;
  rows  = NULL;
  setsize(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] = m.rows[i].elements[j];
  }

}	


CMatrixM::~CMatrixM(void)
{
  rows += rlow;

  delete [] rows;
}


void CMatrixM::setsize(int rl, int rh, int cl, int ch)
{
  // do we need to resize at all? 
  //
  if (rl==rlow && cl==clow && rh==rhigh && cl==chigh) return;
  
  // is the new size positive? 
  //
  if (rh<rl || ch<cl)
  throw BoundsException("invalid size <0", cname, __FILE__, __LINE__);  
  
  
  // delete old storage if it exists 
  //
  delete [] (rows+rlow);
  
  // set the new size 
  //
  rlow = rl;
  rhigh = rh;
  clow = cl;
  chigh = ch;
  
  // allocate the array of rows 
  //
  rows = new CVectorM[rhigh+1-rlow];
  if (rows==NULL) 
    throw AllocationException( rhigh-rlow+1, "rows", cname, __FILE__, __LINE__);

  rows -= rlow;
  
  // create the individual rows 
  //
  for (int i=rlow; i<=rhigh; i++) rows[i].setsize(clow, chigh);
}



CVectorM CMatrixM::fastcol(int j) const
{
  CVectorM tmp(rlow, rhigh);
  for (int i=rlow; i<=rhigh; i++) tmp[i] = rows[i].elements[j];
  
  return tmp;
}

CVectorM CMatrixM::col(int j)
{
  if (j<clow || j>chigh)
    throw RangeException( j, clow, chigh, "column index", 
			 cname, __FILE__, __LINE__);
  
  CVectorM tmp = fastcol(j);
  
  return tmp;
}



CVectorM &CMatrixM::row(int i)
{
  if (i<rlow || i>rhigh)
    throw RangeException( i, rlow, rhigh, "row index", 
			 cname, __FILE__, __LINE__);
  
  return rows[i];
}

CVectorM &CMatrixM::fastrow(int i) const
{
  return rows[i];
}



void CMatrixM::fastsetrow(int i, const CVectorM &v)
{
  rows[i] = v;
}

void CMatrixM::setrow(int i, const CVectorM &v)
{
  if (v.low != clow || v.high != chigh) 
    throw BoundsException("row-vector size mismatch",
			  cname, __FILE__, __LINE__);

  if (i<rlow || i>rhigh)
    throw RangeException( i, rlow, rhigh, "row index", 
			 cname, __FILE__, __LINE__);

  rows[i] = v;
}

void CMatrixM::fastsetcol(int j, const CVectorM &v)
{
  for (int i=rlow; i<=rhigh; i++)
    rows[i].elements[j] = v.elements[i];
}

void CMatrixM::setcol(int j, const CVectorM &v)
{
  if (v.low != rlow || v.high != rhigh) 
    throw BoundsException("column-vector size mismatch",
			  cname, __FILE__, __LINE__);

  if (j<clow || j>chigh)
    throw RangeException( j, clow, chigh, "column index",
			 cname, __FILE__, __LINE__);
  
  fastsetcol(j, v);
}

void CMatrixM::zero(void)
{
  for (int i=rlow; i<=rhigh; i++) rows[i].zero();
}


CVectorM &CMatrixM::operator[](int i) const
{
  if (i<rlow || i>rhigh)
    throw RangeException( i, rlow, rhigh, "row index",
			 cname, __FILE__, __LINE__);
  
  return rows[i];
}

CMatrixM &CMatrixM::operator=(const CMatrixM &m)
{
  // Allow assignment of null matrix
  if (m.rows==NULL) {
    rows = NULL;
    rlow = rhigh = clow = chigh = 0;
    return *this;
  }
  
  if ((rows+rlow)==NULL) 
      setsize(m.rlow, m.rhigh, m.clow, m.chigh);
  
  if (m.rlow!=rlow || m.rhigh!=rhigh || m.clow!=clow || m.chigh!=chigh) 
    throw OperationException("=", cname, __FILE__, __LINE__);
  
  for (int i=rlow; i<=rhigh; i++) 
    fastsetrow(i, m.rows[i]);
  
  return *this;
}


CMatrixM CMatrixM::operator-(void)
{
  CMatrixM tmp(rlow, rhigh, clow, chigh);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) 
      tmp.rows[i].elements[j] = -rows[i].elements[j];
  }

  return tmp;
}


CMatrixM operator+(const CMatrixM &m1, const CMatrixM &m2)
{
  if (m1.rlow!=m2.rlow || m1.rhigh!=m2.rhigh || 
      m1.clow!=m2.clow || m1.chigh!=m2.chigh) 
    throw OperationException("+", "friend (CMatrixM, CMatrixM)", 
			     __FILE__, __LINE__);
  
  CMatrixM tmp(m1.rlow, m1.rhigh, m1.clow, m1.chigh);
  
  for (int i=m1.rlow; i<=m1.rhigh; i++) {
    for (int j=m1.clow; j<=m1.chigh; j++) {
      tmp.rows[i].elements[j] = m1.rows[i].elements[j]
	+ m2.rows[i].elements[j];
    }
  }
  
  return tmp;
}

CMatrixM operator-(const CMatrixM &m1, const CMatrixM &m2)
{
  if (m1.rlow!=m2.rlow || m1.rhigh!=m2.rhigh || 
      m1.clow!=m2.clow || m1.chigh!=m2.chigh) 
    throw OperationException("-", "friend (CMatrixM, CMatrixM)", 
			     __FILE__, __LINE__);
  
  CMatrixM tmp(m1.rlow, m1.rhigh, m1.clow, m1.chigh);
  
  for (int i=m1.rlow; i<=m1.rhigh; i++) {
    for (int j=m1.clow; j<=m1.chigh; j++) {
      tmp.rows[i].elements[j] = m1.rows[i].elements[j]
	- m2.rows[i].elements[j];
    }
  }
  
  return tmp;
}



CMatrixM operator*(const CMatrixM &m, double a)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i].elements[j] = m.rows[i].elements[j] * a;
    }
  }

  return tmp;
}


CMatrixM operator*(double a, const CMatrixM &m)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i].elements[j] = m.rows[i].elements[j] * a;
    }
  }

  return tmp;
}


CMatrixM operator+(const CMatrixM &m, double a)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i][j] += a;
    }
  }

  return tmp;
}


CMatrixM operator+(double a, const CMatrixM &m)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++)  {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i][j] += a;
    }
  }

  return tmp;
}


CMatrixM operator-(const CMatrixM &m, double a)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i][j] -= a;
    }
  }

  return tmp;
}


CMatrixM operator-(double a, const CMatrixM &m)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp[i][j] = a - m[i][j];
    }
  }

  return tmp;
}



CMatrixM operator/(const CMatrixM &m, double a)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i].elements[j] = m.rows[i].elements[j] / a;
    }
  }

  return tmp;
}


CMatrixM &CMatrixM::operator*=(double a)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) {
      rows[i].elements[j] *= a;
    }
  }
  return *this;
}

CMatrixM &CMatrixM::operator/=(double a)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) {
      rows[i].elements[j] /= a;
    }
  }
  return *this;
}



CMatrixM operator*(const CMatrixM &m, const Complex &a)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i].elements[j] = m.rows[i].elements[j] * a;
    }
  }

  return tmp;
}


CMatrixM operator*(const Complex &a, const CMatrixM &m)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i].elements[j] = m.rows[i].elements[j] * a;
    }
  }

  return tmp;
}




CMatrixM operator/(const CMatrixM &m, const Complex &a)
{
  CMatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.rows[i].elements[j] = m.rows[i].elements[j] / a;
    }
  }

  return tmp;
}


CMatrixM &CMatrixM::operator*=(const Complex &a)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) {
      rows[i].elements[j] *= a;
    }
  }

  return *this;
}

CMatrixM &CMatrixM::operator/=(const Complex &a)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) {
      rows[i].elements[j] /= a;
    }
  }

  return *this;
}


CMatrixM &CMatrixM::operator+=(const CMatrixM &m)
{
  if (m.rlow!=rlow || m.rhigh!=rhigh || m.clow!=clow || m.chigh!=chigh) 
    throw OperationException("+=", cname, __FILE__, __LINE__);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) {
      rows[i].elements[j] += m.rows[i].elements[j];
    }
  }

  return *this;
}


CMatrixM &CMatrixM::operator-=(const CMatrixM &m)
{
  if (m.rlow!=rlow || m.rhigh!=rhigh || m.clow!=clow || m.chigh!=chigh) 
    throw OperationException("-=", cname, __FILE__, __LINE__);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) {
      rows[i].elements[j] -= m.rows[i].elements[j];
    }
  }

  return *this;
}


CVectorM operator*(const CMatrixM &m, const CVectorM &v)
{
  if (m.clow != v.low || m.chigh != v.high)
    throw OperationException("*", "friend (CMatrixM, CVectorM)", 
			     __FILE__, __LINE__);
  
  CVectorM tmp(m.rlow, m.rhigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    tmp.elements[i] = 0.0;
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.elements[i] += 
	m.rows[i].elements[j]*v.elements[j];
    }
  }

  return tmp;
}

CVectorM operator*(const CMatrixM &m, const VectorM &v)
{
  if (m.clow != v.low || m.chigh != v.high)
    throw OperationException("*", "friend (CMatrixM, Vector)", 
			     __FILE__, __LINE__);
  
  CVectorM tmp(m.rlow, m.rhigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    tmp.elements[i] = 0.0;
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.elements[i] += 
	m.rows[i].elements[j]*v.elements[j];
    }
  }

  return tmp;
}

CVectorM operator*(const CVectorM &v, const CMatrixM &m)
{
  if (m.rlow != v.low || m.rhigh != v.high)
    throw OperationException("*", "friend (CVectorM, CMatrixM)", 
			     __FILE__, __LINE__);

  
  CVectorM tmp(m.clow, m.chigh);
  
  for (int j=m.clow; j<=m.chigh; j++) {
    tmp.elements[j] = 0.0;
    for (int i=m.rlow; i<=m.rhigh; i++) {
      tmp.elements[j] +=
	m.rows[i].elements[j]*v.elements[i];
    }
  }

  return tmp;
}


CMatrixM operator*(const CMatrixM &m1, const CMatrixM &m2)
{
  if (m1.clow != m2.rlow || m1.chigh != m2.rhigh)
    throw OperationException("*", "friend (CMatrixM, CMatrixM)", 
			     __FILE__, __LINE__);
  
  CMatrixM tmp(m1.rlow, m1.rhigh, m2.clow, m2.chigh);
  
  for (int i=m1.rlow; i<=m1.rhigh; i++) {
    for (int j=m2.clow; j<=m2.chigh; j++) {
      tmp.rows[i].elements[j] = 0.0;
      for (int k=m1.clow; k<=m1.chigh; k++) {
	tmp.rows[i].elements[j] += m1.rows[i].elements[k]
	  *m2.rows[k].elements[j];
      }
    }
  }

  return tmp;
}


CMatrixM CMatrixM::Conjg(void) const
{
  CMatrixM tmp(rlow, rhigh, clow, chigh);
  
  for (int i=rlow; i<=rhigh; i++) 
      tmp.rows[i] = rows[i].Conjg();

  return tmp;
}

CMatrixM CMatrixM::Transpose(void) const
{
  CMatrixM t(clow, chigh, rlow, rhigh);
  
  t.rlow = clow;
  t.rhigh = chigh;
  t.chigh = rhigh;
  t.clow = rlow;
  
  for (int i=t.rlow; i<=t.rhigh; i++)
    t.fastsetrow(i, fastcol(i));
  
  return t;
}

Complex CMatrixM::Trace(void) const
{
  Complex t;
  
  if (rlow != clow || rhigh != chigh) 
    throw BoundsException("cannot take trace of non-square matrix",
			  cname, __FILE__, __LINE__);
  
  t = 0.0;
  for (int i=rlow; i<=rhigh; i++) t += rows[i][i];
  
  return t;
}



MatrixM CMatrixM::Re(void) const
{
  MatrixM tmp(rlow, rhigh, clow, chigh);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) 
      tmp.rows[i].elements[j] = rows[i].elements[j].real();
  }

  return tmp;
}	


MatrixM CMatrixM::Im(void) const
{
  MatrixM tmp(rlow, rhigh, clow, chigh);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) 
      tmp.rows[i].elements[j] = rows[i].elements[j].imag();
  }
  
  return tmp;
}	


MatrixM Re(const CMatrixM &c)
{
  return c.Re();
}

MatrixM Im(const CMatrixM &c)
{
  return c.Im();
}
CMatrixM Conjg(const CMatrixM &c)
{
  return c.Conjg();
}

CMatrixM Transpose(const CMatrixM &c)
{
  return c.Transpose();
}
CMatrixM Adjoint(const CMatrixM &c)
{
  return c.Conjg().Transpose();
}

Complex Trace(const CMatrixM &c)
{
  return c.Trace();
}

void CMatrixM::print(ostream& out)
{
  for (int i=rlow; i<=rhigh; i++) rows[i].print(out);
}

void CMatrixM::binwrite(ostream& out)
{
  out.write((char*)&rlow, sizeof(int));
  out.write((char*)&rhigh, sizeof(int));
  out.write((char*)&clow, sizeof(int));
  out.write((char*)&chigh, sizeof(int));
  
  for (int i=rlow; i<=rhigh; i++) {
    rows[i].binwrite(out);
  }
}


CMatrixM CMatrixM_binread(istream& in)
{
  
  int rlow, rhigh, clow, chigh;
  
  in.read((char*)&rlow, sizeof(int));
  in.read((char*)&rhigh, sizeof(int));
  in.read((char*)&clow, sizeof(int));
  in.read((char*)&chigh, sizeof(int));
  
  CMatrixM tmp(rlow, rhigh, clow, chigh);
  
  for (int i=rlow; i<=rhigh; i++) {
    tmp.rows[i] = CVectorM_binread(in);
  }
  
  return tmp;
}
