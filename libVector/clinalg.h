int lu_decomp(CMatrixM& a, int* indx, double& d);
int linear_solve(CMatrixM& a, CVectorM& b, CVectorM& x);
void lu_backsub(CMatrixM& a, int* indx, CVectorM& b);
int inverse(CMatrixM& a, CMatrixM& b);
Complex determinant(CMatrixM& a);
Complex lu_determinant(CMatrixM& a, double& d);
CMatrixM sub_matrix(CMatrixM& in, 
		   int ibeg, int iend, int jbeg, int jend, 
		   int ioff=0, int joff=0);

void embed_matrix(CMatrixM& to, CMatrixM& from, int rbeg, int cbeg);
void embed_matrix(MatrixM& to, MatrixM& from, int rbeg, int cbeg);
void embed_matrix(CMatrixM& to, MatrixM& from, int rbeg, int cbeg);
