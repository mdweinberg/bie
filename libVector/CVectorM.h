// This is really -*- C++ -*-

#ifndef _C_VectorM_h
#define _C_VectorM_h

#include <iostream>
#include <fstream>

using namespace std;

#include <VectorM.h>

class VectorM;
class MatrixM;
class CMatrixM;

//! Complex vector class
class CVectorM
{
  friend class MatrixM;
  friend class VectorM;
  friend class CMatrixM;

protected:
  int low, high;
  Complex *elements;

public:

  //! Class name
  static const string cname;

  //@{
  //! constructors
  CVectorM(void);
  CVectorM(int, int);
  CVectorM(int, int, double *);
  CVectorM(int, int, Complex *);
  CVectorM(const CVectorM &);
  CVectorM(const VectorM &);
  //@}
  
  //! the destructor 
  ~CVectorM(void);
  
  //@{
  //! assignment, access, resizing 

  void setsize(int, int);
  
  Complex &operator[](int) const;	// safe access 
  CVectorM &operator=(const CVectorM &);
  
  void zero(void);
  bool isNull()       const  { return elements ? false : true; }
  int getlength(void) const {return high-low+1;}
  int getlow(void)    const {return low;}
  int gethigh(void)   const {return high;}
  //@}
  
  //@{
  /// make an array 
  Complex *array(int l, int h);
  Complex *array_copy(int l, int h) const;
  //@}
  
  //@{
  /// unary plus and minus 
  CVectorM operator+(void) {return *this;}
  CVectorM operator-(void);
  //@}
  
  //@{
  /// VectorM addition and subtraction 
  CVectorM &operator+=(const CVectorM &);
  CVectorM &operator-=(const CVectorM &);
  CVectorM &operator+=(const VectorM &v)
  {*this += v; return *this;}
  //			{CVectorM c(v); *this += c; return *this;}
  CVectorM &operator-=(const VectorM &v)
  {*this -= v; return *this;}
  //			{CVectorM c(v); *this -= c; return *this;}
  friend CVectorM operator+(const CVectorM &, const CVectorM &);
  friend CVectorM operator-(const CVectorM &, const CVectorM &);
  //@}
  
  /// dot product 
  friend Complex operator*(const CVectorM &, const CVectorM &);
  
  //@{
  /// combine product 
  friend CVectorM operator&(const CVectorM&, const VectorM &);
  friend CVectorM operator&(const VectorM&, const CVectorM &);
  //@}
  
  //@{
  /// operations with scalars 
  CVectorM &operator*=(const Complex &);
  CVectorM &operator/=(const Complex &);
  friend CVectorM operator*(const CVectorM &, const Complex &);
  friend CVectorM operator*(const Complex &, const CVectorM &);
  friend CVectorM operator/(const CVectorM &, const Complex &);
  
  CVectorM &operator*=(double);
  CVectorM &operator/=(double);
  friend CVectorM operator*(const CVectorM &, double);
  friend CVectorM operator*(double, const CVectorM &);
  friend CVectorM operator/(const CVectorM &, double);
  //@}
  
  
  //@{
  /// operations with matrices 
  friend CVectorM operator*(const CMatrixM &, const CVectorM &);
  friend CVectorM operator*(const CVectorM &, const CMatrixM &);
  friend CVectorM operator*(const CMatrixM &, const VectorM &);
  //@}
  
  //@{
  /// matrix-matrix operations 
  friend CMatrixM operator+(const CMatrixM &, const CMatrixM &);
  friend CMatrixM operator-(const CMatrixM &, const CMatrixM &);
  friend CMatrixM operator*(const CMatrixM &, const CMatrixM &);
  //@}
  
  //@{
  /// scalar-matrix operations 
  friend CMatrixM operator*(const CMatrixM &, const Complex &);
  friend CMatrixM operator/(const CMatrixM &, const Complex &);
  friend CMatrixM operator*(const Complex &, const CMatrixM &);
  friend CMatrixM operator*(const CMatrixM &, double);
  friend CMatrixM operator/(const CMatrixM &, double);
  friend CMatrixM operator*(double, const CMatrixM &);
  //@}
  
  //@{
  /// Mixed operations

  //	        friend CVectorM operator+(const VectorM &v, const CVectorM &c);
  friend CVectorM operator+(const CVectorM &c, const VectorM &v);
  friend CVectorM operator-(const VectorM &v, const CVectorM &c);
  friend CVectorM operator-(const CVectorM &c, const VectorM &v);
  
  //	        friend CVectorM operator*(const CMatrixM &c, const VectorM &v);
  friend CVectorM operator*(const MatrixM &m, const CVectorM &c);
  friend CVectorM operator*(const CVectorM &c, const MatrixM &m);
  friend CVectorM operator*(const VectorM &v, const CMatrixM &c);
  
  friend CVectorM operator/(const VectorM &v, const Complex &c);
  friend CVectorM operator*(const VectorM &v, const Complex &c);
  friend CVectorM operator*(const Complex &c, const VectorM &v);
  //@}
  
  //@{
  /// miscellaneous 
  VectorM Re(void);
  VectorM Im(void);
  CVectorM Conjg(void);
  //@}
  
  //@{
  /// IO 
  void print(ostream&);
  void binwrite(ostream&);
  friend CVectorM CVectorM_binread(istream&);
  //@}
};


//! Complex matrix class
class CMatrixM
{
  friend class CVectorM;
  friend class MatrixM;
  friend class VectorM;

protected:
  int rlow, rhigh;	// low and high row indices 
  int clow, chigh;	// low and high column indices 
  CVectorM *rows;
  
  // fast (but unsafe) access functions 
  
  CVectorM fastcol(int) const;
  CVectorM &fastrow(int) const;
  void fastsetrow(int, const CVectorM &);
  void fastsetcol(int, const CVectorM &);
  
public:
  /// Class name
  static const string cname;
  
  //@{
  /// constructors and the destructor
  CMatrixM(void);
  CMatrixM(int, int, int, int);
  CMatrixM(int, int, int, int, double **);
  CMatrixM(int, int, int, int, Complex **);
  CMatrixM(const CMatrixM &);
  CMatrixM(const MatrixM &);
  ~CMatrixM(void);
  //@}

  //@{
  /// assignment and access
  CMatrixM &operator=(const CMatrixM &);
  CVectorM &operator[](int) const;
  
  inline bool isNull(void)  const {return rows ? false : true; }
  inline int getnrows(void) const {return rhigh-rlow+1;}
  inline int getncols(void) const {return chigh-clow+1;}
  inline int getrlow(void)  const {return rlow;}
  inline int getclow(void)  const {return clow;}
  inline int getrhigh(void) const {return rhigh;}
  inline int getchigh(void) const {return chigh;}
  //@}
  
  //@{
  /// sizing 
  void setsize(int, int, int, int);
  void zero(void);
  //@}
  
  //@{
  /// row and column access 
  CVectorM col(int);
  CVectorM &row(int);
  void setrow(int, const CVectorM &);
  void setcol(int, const CVectorM &);
  //@}

  //@{
  /// unary plus and minus 
  CMatrixM operator-(void);
  CMatrixM operator+(void) {return *this;}
  //@}
  
  //@{
  /// operations with matrices 
  friend CMatrixM operator+(const CMatrixM &, const CMatrixM &);
  friend CMatrixM operator-(const CMatrixM &, const CMatrixM &);
  friend CMatrixM operator*(const CMatrixM &, const CMatrixM &);
  CMatrixM &operator+=(const CMatrixM &);
  CMatrixM &operator-=(const CMatrixM &);
  CMatrixM &operator+=(MatrixM &m) 
  {CMatrixM c(m); *this += c; return *this;}
  CMatrixM &operator-=(MatrixM &m) 
  {CMatrixM c(m); *this -= c; return *this;}
  //@}
  
  //@{
  /// operations with vectors 
  friend CVectorM operator*(const CMatrixM &, const CVectorM &);
  friend CVectorM operator*(const CMatrixM &, const VectorM &);
  friend CVectorM operator*(const CVectorM &, const CMatrixM &);
  //@}
  
  //@{
  /// operations with scalars 
  friend CMatrixM operator*(const CMatrixM &, double);
  friend CMatrixM operator*(double, const CMatrixM &);
  friend CMatrixM operator+(const CMatrixM &, double);
  friend CMatrixM operator+(double, const CMatrixM &);
  friend CMatrixM operator-(const CMatrixM &, double);
  friend CMatrixM operator-(double, const CMatrixM &);
  friend CMatrixM operator/(const CMatrixM &, double);
  CMatrixM &operator*=(double);
  CMatrixM &operator/=(double);
  
  friend CMatrixM operator*(const CMatrixM &, const Complex &);
  friend CMatrixM operator*(const Complex &, const CMatrixM &);
  friend CMatrixM operator/(const CMatrixM &, const Complex &);
  CMatrixM &operator*=(const Complex &);
  CMatrixM &operator/=(const Complex &);
  //@}
  
  
  //@{
  /// Mixed operations
  friend CMatrixM operator*(const CMatrixM &c, const MatrixM &m);
  friend CMatrixM operator*(const MatrixM &m, const CMatrixM &c);
  
  friend CMatrixM operator+(const MatrixM &m, const CMatrixM &c);
  friend CMatrixM operator+(const CMatrixM &c, const MatrixM &m);
  friend CMatrixM operator-(const MatrixM &m, const CMatrixM &c);
  friend CMatrixM operator-(const CMatrixM &c, const MatrixM &m);
  
  friend CMatrixM operator*(const MatrixM &m, const Complex &c);
  friend CMatrixM operator*(const Complex &c, const MatrixM &m);
  friend CMatrixM operator/(const MatrixM &m, const Complex &c);
  //@}
  
  //@{
  /// IO 
  void print(ostream&);
  void binwrite(ostream&);
  friend CMatrixM CMatrixM_binread(istream&);
  //@}
  
  //@{
  /// Miscellaneous 
  MatrixM Re(void) const;
  MatrixM Im(void) const;
  CMatrixM Conjg(void) const;	
  CMatrixM Transpose(void) const;	
  CMatrixM Adjoint(void) const {return Transpose().Conjg();}
  Complex Trace(void) const;
  //@}
  
};

//@{
/// declarations for mixed-type operators
Complex operator*(const VectorM &v, const CVectorM &c);
Complex operator*(const CVectorM &c, const VectorM &v);
//@}


//@{
/// miscellaneous functions 
MatrixM  Re(const CMatrixM &c);
MatrixM  Im(const CMatrixM &c);
CMatrixM Conjg(const CMatrixM &c);
CMatrixM Adjoint(const CMatrixM &c);
CMatrixM Transpose(const CMatrixM &c);
Complex  Trace(const CMatrixM &c);

VectorM  Re(const CVectorM &c);
VectorM  Im(const CVectorM &c);
CVectorM Conjg(const CVectorM &c);
//@}

//@{
/// Non-intrusive version of serialize/save/load routine for CVectorM
/// and CMatrixM
namespace boost { 
  namespace serialization {
    template<class Archive>
    inline void save(Archive & ar,
		     const CVectorM &cv,
		     const unsigned int file_version) 
    {
      bool null = cv.isNull();
      int low   = cv.getlow();
      int high  = cv.gethigh();

      ar << BOOST_SERIALIZATION_NVP(null);
      ar << BOOST_SERIALIZATION_NVP(low);
      ar << BOOST_SERIALIZATION_NVP(high);

      if (!null) {
	char buf[128];
	for (int i=low; i<=high; i++) {
	  sprintf(buf, "item_%d", i);
	  ar << boost::serialization::make_nvp(buf, cv[i]);
	}
      }
    }

    template<class Archive>
    inline void load(Archive & ar,
		     CVectorM &cv,
		     const unsigned int file_version) 
    {
      bool null;
      int low, high;

      ar >> BOOST_SERIALIZATION_NVP(null);
      ar >> BOOST_SERIALIZATION_NVP(low);
      ar >> BOOST_SERIALIZATION_NVP(high);

      cv.setsize(low, high);

      if (!null) {
	char buf[128];
	for (int i=low; i<=high; i++) {
	  sprintf(buf, "item_%d", i);
	  ar >> boost::serialization::make_nvp(buf, cv[i]);
	}
      }
    }

    template<class Archive>
    inline void serialize(Archive & ar,
			  CVectorM &cv,
			  const unsigned int file_version) {
      boost::serialization::split_free(ar, cv, file_version);
    }

    template<class Archive>
    inline void save(Archive & ar,
		     const CMatrixM &cm,
		     const unsigned int file_version) 
    {
      bool null = cm.isNull();
      int rlow  = cm.getrlow();
      int rhigh = cm.getrhigh();
      int clow  = cm.getclow();
      int chigh = cm.getchigh();

      ar << BOOST_SERIALIZATION_NVP(null);
      ar << BOOST_SERIALIZATION_NVP(rlow);
      ar << BOOST_SERIALIZATION_NVP(rhigh);
      ar << BOOST_SERIALIZATION_NVP(clow);
      ar << BOOST_SERIALIZATION_NVP(chigh);

      // CMatrixM is just a collection of CVectorM/s
      if (!null) {
	char buf[128];
	for (int i=rlow; i<=rhigh; i++) {
	  sprintf(buf, "item_%d", i);
	  ar << boost::serialization::make_nvp(buf, cm[i]);
	}
      }
    }

    template<class Archive>
    inline void load(Archive & ar,
		     CMatrixM &cm,
		     const unsigned int file_version) 
    {
      bool null;
      int rlow, rhigh, clow, chigh;
      
      ar >> BOOST_SERIALIZATION_NVP(null);
      ar >> BOOST_SERIALIZATION_NVP(rlow);
      ar >> BOOST_SERIALIZATION_NVP(rhigh);
      ar >> BOOST_SERIALIZATION_NVP(clow);
      ar >> BOOST_SERIALIZATION_NVP(chigh);

      cm.setsize(rlow, rhigh, clow, chigh);

      if (!null) {
	char buf[128];
	for (int i=rlow; i<=rhigh; i++) {
	  sprintf(buf, "item_%d", i);
	  ar >> boost::serialization::make_nvp(buf, cm[i]);
	}
      }
    }
    
    template<class Archive>
    inline void serialize(Archive & ar,
			  CMatrixM &cm,
			  const unsigned int file_version) 
    {
      boost::serialization::split_free(ar, cm, file_version);
    }

  } // namespace serialization
} // namespace boost
//@}

#endif
