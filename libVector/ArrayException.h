// -*- C++ -*-

#ifndef _Array_Exception_h
#define _Array_Exception_h

#include <sstream>

/** Defines an Error Handler base class to handle exceptions in the Vector
    and Matrix classes
 */
class ArrayException 
{
 public:
  //! Generic exception
  ArrayException(std::string exceptionname, std::string message,
		 std::string cname, std::string sourcefilename, 
		 int sourcelinenumber);

  //! Copy constructor
  ArrayException(const ArrayException& e);

  //! Destructor.
  virtual ~ArrayException() {}

  //! Returns an error message suitable for printing to the user.
  std::string getErrorMessage();
    
  protected:

  //! Protected so it is only called by properly implemented classes.
  ArrayException(std::string cname, std::string sourcefile, int linenumber);

  //! Friendly name of the exception.
  std::string exceptionname;

  //! Source class
  std::string cname;

  //! Error message describing the error in more detail.
  std::ostringstream errormessage;

  //! Source file where throw occured.
  std::string sourcefilename;
  
  //! Line number of throw.
  int sourcelinenumber;
};

class BoundsException : public ArrayException 
{  
 public:
  BoundsException(std::string error, std::string cname,
		  std::string sourcefilename, int sourcelinenumber);
};

class RangeException : public ArrayException 
{  
 public:
  RangeException(int v, int l, int h, std::string type, std::string cname, 
		 std::string sourcefilename, int sourcelinenumber);
};

class OperationException : public ArrayException 
{  
 public:
  OperationException(std::string op, std::string cname, 
		     std::string sourcefilename, int sourcelinenumber);
};

class AllocationException : public ArrayException 
{  
 public:
  AllocationException(int size, std::string type, std::string cname, 
		      std::string sourcefilename, int sourcelinenumber);
};


#endif
