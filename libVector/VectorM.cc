#include <cmath>
#include <cassert>
#include <cstdlib>
#include <iomanip>
#include <string>

#include <VectorM.h>

const string VectorM::cname      = "class VectorM";
const string MatrixM::cname      = "class MatrixM";
const string Three_Vector::cname = "class Three_Vector";

/**
   Default constructor; make a null vector.
*/
VectorM::VectorM()
{
  low=0;
  high=0;
  elements = 0;
}


/**
   Make a null vector.indexed in the range  [l, h]
*/
VectorM::VectorM(int l, int h)
{
  low=0;
  high=0;
  elements = 0;
  setsize(l, h);
}

/**
   Make a null vector.indexed in the range [l, h]
   from a c-style pointer to doubles
*/
VectorM::VectorM(int l, int h, double *v)
{
  low=0;
  high=0;
  elements = 0;
  setsize(l, h);
  
  for (int i=low; i<=high; i++) elements[i] = v[i];
}

/**
   Copy constructor; create a new vector which is a copy of another.
*/
VectorM::VectorM(const VectorM &v)
{
  // Allow consruction of null vector
  if (v.elements==0) {

    elements = 0;
    low = high = 0;

  } else {
    
    low=0;
    high=0;
    elements = 0;
    setsize(v.low, v.high);
    
    for (int i=low; i<=high; i++) elements[i] = v.elements[i];
  }
}

/**
   Destructor. Free elements if it exists.
*/
VectorM::~VectorM()
{
  delete [] (elements+low);
}


/**
   conversion operator (Three_Vector)
*/
VectorM::operator Three_Vector(void)
{
  if (low!=1 || high != 3)
    throw BoundsException("VectorM->Three_Vector conversion error",
			  cname, __FILE__, __LINE__);
  
  for (int i=1; i<=3; i++) (*this)[i] = elements[i];
  
  return *this;
}


/**
   Assignment operator for VectorM; must be defined as a reference
   so that it can be used on lhs. Compatibility checking is performed;
   the destination vector is allocated if its elements are undefined.
*/
VectorM &VectorM::operator=(const VectorM &v)
{
  //
  // Allow assignment of null vector
  //
  if (v.elements==0) {
    elements = 0;
    low = high = 0;
    return *this;
  }
  
  if (low!=v.low && high!=v.high && (high != 0 || (elements+low) != 0))
    throw OperationException("=", cname, __FILE__, __LINE__);
  
  if (high == 0 && low==0 && elements == 0)
    {
      setsize(v.low, v.high);
    }
  
  for (int i=low; i<=high; i++) elements[i] = v.elements[i];
  
  return *this;
}


/**
   The classic array index operator
*/
double &VectorM::operator[](int i) const
{
  if (i<low || i>high)
    throw RangeException(i, low, high, "array index", 
			 cname, __FILE__, __LINE__);
  
  return elements[i];
}


/**
   Set or change the internal storage size of a vector
 */
void VectorM::setsize(int l, int h)
{
  // do we need to resize at all?
  //
  if (l==low && h==high) return;
  
  
  // is the requested size positive?
  //
  if (h<l) throw BoundsException("invalid size <0", cname, __FILE__, __LINE__);
  
  
  
  // delete the old elements if they already exist
  
  delete [] (elements+low);
  
  
  
  // set the new size
  
  low = l;
  high = h;
  
  
  
  // allocate the new elements, and offset the pointer 
  
  elements = new double[high-low+1];
  if (elements == 0) 
    throw AllocationException( high-low+1, "elements", cname, __FILE__, __LINE__);
  elements -= low;
}

/**
   Load the vector values from a c-style array of doubles
*/
void VectorM::load(double *array)
{
  for (int i=low; i<=high; i++) elements[i] = array[i];
}


/**
   Sets all values to zero
*/
void VectorM::zero(void)
{
  for (int i=low; i<=high; i++) elements[i] = 0.0;
}

/**
   Return a pointer the internal vector storage
*/
double *VectorM::array(int l, int h)
{
  return elements+low-l;
}

/**
   Return a pointer to a copy of the vector storage
*/
double *VectorM::array_copy(int l, int h) const
{
  double *ptr = new double[h-l+1];
  assert(ptr);
  ptr-=l;
  
  for (int i=low; i<=high; i++) ptr[i] = elements[i];
  return ptr;
}


/**
   Destroy the copy of the vector storage
*/
void destroy_VectorM_array(double *p, int l, int h)
{
  delete [] (p+l);
}


VectorM VectorM::operator-(void)
{
  VectorM tmp(low, high);
  
  for (int i=low; i<=high; i++) tmp.elements[i] = -elements[i];
  
  return tmp;
}


VectorM operator+(const VectorM &v1, const VectorM &v2)
{
  if (v1.low != v2.low || v1.high != v2.high) 
    throw OperationException("+", "friend (VectorM, VectorM)", 
			     __FILE__, __LINE__);
  
  VectorM tmp(v1.low, v1.high);
  
  for (int i=v1.low; i<=v1.high; i++) {
    tmp.elements[i] = v1.elements[i] + v2.elements[i];
  }
  
  return tmp;
}

VectorM operator-(const VectorM &v1, const VectorM &v2)
{
  if (v1.low != v2.low || v1.high != v2.high) 
    throw OperationException("-", "friend (VectorM, VectorM)", 
			     __FILE__, __LINE__);
  
  VectorM tmp(v1.low, v1.high);
  
  for (int i=v1.low; i<=v1.high; i++) {
    tmp.elements[i] = v1.elements[i] - v2.elements[i];
  }
  
  return tmp;
}

VectorM operator^(const VectorM &v1, const VectorM &v2)
{
  if (v1.high != 3 || v2.high != 3)
    throw OperationException("^", "friend (VectorM, VectorM)", 
			     __FILE__, __LINE__);
  
  VectorM tmp(1, 3);
  
  tmp[1] = v1[2]*v2[3] - v1[3]*v2[2];
  tmp[2] = v1[3]*v2[1] - v1[1]*v2[3];
  tmp[3] = v1[1]*v2[2] - v1[2]*v2[1];
  
  return tmp;
}

VectorM operator&(const VectorM &v1, const VectorM &v2)
{
  if (v1.high != v2.high || v1.low != v2.low)
    throw OperationException("&", "friend (VectorM, VectorM)", 
			     __FILE__, __LINE__);
  
  VectorM tmp(v1.low, v1.high);
  for (int i=v1.low; i<=v1.high; i++)
    tmp[i] = v1[i]*v2[i];
  
  return tmp;
}

VectorM operator*(double a, const VectorM &v)
{
  VectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = a * v.elements[i];
  
  return tmp;
}

VectorM operator*(const VectorM &v, double a)
{
  VectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = a * v.elements[i];
  
  return tmp;
}

VectorM operator/(const VectorM &v, double a)
{
  VectorM tmp(v.low, v.high);
  for (int i=v.low; i<=v.high; i++) tmp.elements[i] = v.elements[i]/a;
  
  return tmp;
}

VectorM &VectorM::operator+=(double a)
{
  for (int i=low; i<=high; i++) elements[i] += a;
  
  return *this;
}

VectorM &VectorM::operator-=(double a)
{
  for (int i=low; i<=high; i++) elements[i] -= a;
  
  return *this;
}


VectorM &VectorM::operator+=(const VectorM &v)
{
  if (low != v.low || high != v.high) 
    throw OperationException("+=", cname, __FILE__, __LINE__);
  
  for (int i=low; i<=high; i++) elements[i] += v[i];
  
  return *this;
}	

VectorM &VectorM::operator-=(const VectorM &v)
{
  if (low != v.low || high != v.high)
    throw OperationException("-=", cname, __FILE__, __LINE__);
  
  for (int i=low; i<=high; i++) elements[i] -= v[i];
  
  return *this;
}	

VectorM &VectorM::operator*=(double a)
{
  for (int i=low; i<=high; i++) elements[i] *= a;
  
  return *this;
}

VectorM &VectorM::operator/=(double a)
{
  for (int i=low; i<=high; i++) elements[i] /= a;
  
  return *this;
}



double operator*(const VectorM &v1, const VectorM &v2)
{
  double tmp;
  
  if (v1.low != v2.low || v1.high != v2.high) 
    throw OperationException("*=", "friend (VectorM, VectorM)", 
			     __FILE__, __LINE__);
  
  tmp = 0.0;
  for (int i=v1.low; i<=v1.high; i++)
    {
      tmp += v1.elements[i] * v2.elements[i];
    }
  return tmp;
}



void VectorM::print(ostream& out)
{
  int oldprecision = out.precision();
  
  for (int i=low; i<=high; i++)
    out << setprecision(12) << elements[i] << " ";
  out << endl;
  out.precision(oldprecision);
}

void VectorM::binwrite(ostream& out)
{
  out.write((char*)&low, sizeof(int));
  out.write((char*)&high, sizeof(int));
  out.write((char*)(elements+low), (high-low+1)*sizeof(double));
}

VectorM VectorM_binread(istream& in)
{
  int low, high;
  
  in.read((char*)&low, sizeof(int));
  in.read((char*)&high, sizeof(int));
  
  VectorM tmp(low, high);
  
  in.read((char*)(tmp.elements+low), (high-low+1)*sizeof(double));
  
  return tmp;
}


MatrixM::MatrixM()
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = 0;
}


MatrixM::MatrixM(int rl, int rh, int cl, int ch)
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = 0;
  setsize(rl, rh, cl, ch);
}


MatrixM::MatrixM(int rl, int rh, int cl, int ch, double **array)
{
  rlow=0;
  rhigh=0;
  clow=0;
  chigh=0;
  rows = 0;
  setsize(rl, rh, cl, ch);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] = array[i][j];
  }

}


MatrixM::MatrixM(const MatrixM &m)
{
  // Allow construction of null matrix
  if (m.rows==0) {
    rows = 0;
    rlow = rhigh = clow = chigh = 0;
  }
  else {
    
    rlow=0;
    rhigh=0;
    clow=0;
    chigh=0;
    rows = 0;
    setsize(m.rlow, m.rhigh, m.clow, m.chigh);
    
    for (int i=rlow; i<=rhigh; i++) {
      for (int j=clow; j<=chigh; j++)
	rows[i].elements[j] = m.rows[i].elements[j];
    }
  }
}	


MatrixM::~MatrixM(void)
{
  delete [] (rows+rlow);
}

void MatrixM::setsize(int rl, int rh, int cl, int ch)
{
  // do we need to resize at all?
  //
  if (rl==rlow && cl==clow && rh==rhigh && cl==chigh) return;
  
  
  // is the new size positive?
  //
  if (rh<rl || ch<cl) 
    throw BoundsException("invalid size <0", cname, __FILE__, __LINE__);
  
  
  // delete old storage if it exists
  //
  delete [] (rows+rlow);
  
  
  
  // set the new size
  //
  rlow = rl;
  rhigh = rh;
  clow = cl;
  chigh = ch;
  
  
  // allocate the array of rows
  //
  rows = new VectorM[rhigh+1-rlow];
  if (rows==0) 
    throw AllocationException( rhigh-rlow+1, "rows", cname, __FILE__, __LINE__);
  
  rows -= rlow;
  
  
  // create the individual rows
  //
  for (int i=rlow; i<=rhigh; i++) rows[i].setsize(clow, chigh);
}




VectorM MatrixM::fastcol(int j)
{
  VectorM tmp(rlow, rhigh);
  for (int i=rlow; i<=rhigh; i++) tmp[i] = rows[i].elements[j];
  
  return tmp;
}

VectorM MatrixM::col(int j)
{
  if (j<clow || j>chigh) 
    throw RangeException( j, clow, chigh, "column index",
			 cname, __FILE__, __LINE__);
  
  VectorM tmp = fastcol(j);
  
  return tmp;
}



VectorM &MatrixM::row(int i)
{
  if (i<rlow || i>rhigh) 
    throw RangeException( i, rlow, rhigh, "row index",
			 cname, __FILE__, __LINE__);
  
  return rows[i];
}

VectorM &MatrixM::fastrow(int i)
{
  return rows[i];
}



void MatrixM::fastsetrow(int i, const VectorM &v)
{
  rows[i] = v;
}

void MatrixM::setrow(int i, VectorM &v)
{
  if (v.low != clow || v.high != chigh) 
    throw BoundsException("row-vector size mismatch",
			  cname, __FILE__, __LINE__);
  
  if (i<rlow || i>rhigh) 
    throw RangeException( i, rlow, rhigh, "row index", 
			 cname, __FILE__, __LINE__);
  
  rows[i] = v;
}

void MatrixM::fastsetcol(int j, VectorM &v)
{
  for (int i=rlow; i<=rhigh; i++)
    rows[i].elements[j] = v.elements[i];
}

void MatrixM::setcol(int j, VectorM &v)
{
  if (v.low != rlow || v.high != rhigh) 
    throw BoundsException("column-vector size mismatch",
			  cname, __FILE__, __LINE__);
  if (j<clow || j>chigh) 
    throw RangeException( j, clow, chigh, "column index",
			 cname, __FILE__, __LINE__);
  
  fastsetcol(j, v);
}

void MatrixM::zero(void)
{
  for (int i=rlow; i<=rhigh; i++) rows[i].zero();
}


double **MatrixM::array(int rl, int rh, int cl, int ch)
{
  double **ptr;
  
  ptr = new double*[rh-rl+1];
  if (ptr == 0) 
    throw AllocationException( rh-rl+1, "array", cname, __FILE__, __LINE__);
  
  ptr -= rl;
  
  for (int i=rl; i<=rh; i++) ptr[i] = rows[i].array(cl, ch);
  
  return ptr;
}

double **MatrixM::array_copy(int rl, int rh, int cl, int ch) const
{
  double **ptr;
  
  ptr = new double*[rh-rl+1];
  if (ptr == 0) 
    throw AllocationException( rh-rl+1, "array", cname, __FILE__, __LINE__);
  
  ptr -= rl;
  
  for (int i=rl; i<=rh; i++) ptr[i] = rows[i].array_copy(cl, ch);
  
  return ptr;
}

void destroy_MatrixM_array(double **ptr, int rl, int rh, int cl, int ch)
{
  for (int i=rl; i<=rh; i++) destroy_VectorM_array(ptr[i], cl, ch);
  
  delete [] (ptr + rl);
}



VectorM &MatrixM::operator[](int i) const
{
  if (i<rlow || i>rhigh) 
    throw RangeException( i, rlow, rhigh, "row index",
			 cname, __FILE__, __LINE__);
  
  return rows[i];
}

MatrixM &MatrixM::operator=(const MatrixM &m)
{
  // Allow assignment of null matrix
  //
  if (m.rows==0) {
    rows = 0;
    rlow = rhigh = clow = chigh = 0;
    return *this;
  }
  
  if ((rows+rlow)==0) 
    {
      setsize(m.rlow, m.rhigh, m.clow, m.chigh);
    }
  
  if (m.rlow!=rlow || m.rhigh!=rhigh || m.clow!=clow || m.chigh!=chigh) 
    throw OperationException("=", cname, __FILE__, __LINE__);
  
  for (int i=rlow; i<=rhigh; i++) 
    {
      fastsetrow(i, m.rows[i]);
    }
  return *this;
}

MatrixM MatrixM::operator-(void)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++) 
      rows[i].elements[j] = -rows[i].elements[j];
  }
  
  return *this;
}

MatrixM operator+(const MatrixM &m1, const MatrixM &m2)
{
  if (m1.rlow!=m2.rlow || m1.rhigh!=m2.rhigh || 
      m1.clow!=m2.clow || m1.chigh!=m2.chigh) 
    throw OperationException("+", "friend (MatrixM, MatrixM)", 
			     __FILE__, __LINE__);
  
  MatrixM tmp(m1.rlow, m1.rhigh, m1.clow, m1.chigh);
  
  for (int i=m1.rlow; i<=m1.rhigh; i++) {
    for (int j=m1.clow; j<=m1.chigh; j++)
      tmp.rows[i].elements[j] = m1.rows[i].elements[j]
	+ m2.rows[i].elements[j];
  }
  
  return tmp;
}

MatrixM operator-(const MatrixM &m1, const MatrixM &m2)
{
  if (m1.rlow!=m2.rlow || m1.rhigh!=m2.rhigh || 
      m1.clow!=m2.clow || m1.chigh!=m2.chigh) 
    throw OperationException("-", "friend (MatrixM, MatrixM)", 
			     __FILE__, __LINE__);
  
  MatrixM tmp(m1.rlow, m1.rhigh, m1.clow, m1.chigh);
  
  for (int i=m1.rlow; i<=m1.rhigh; i++) {
    for (int j=m1.clow; j<=m1.chigh; j++)
      tmp.rows[i].elements[j] = m1.rows[i].elements[j]
	- m2.rows[i].elements[j];
  }
  
  return tmp;
}



MatrixM operator*(const MatrixM &m, double a)
{
  MatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++)
      tmp.rows[i].elements[j] = m.rows[i].elements[j] * a;
  }

  return tmp;
}


MatrixM operator*(double a, const MatrixM &m)
{
  MatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++)
      tmp.rows[i].elements[j] = m.rows[i].elements[j] * a;
  }

  return tmp;
}




MatrixM operator/(const MatrixM &m, double a)
{
  MatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++)
      tmp.rows[i].elements[j] = m.rows[i].elements[j] / a;
  }

  return tmp;
}


MatrixM operator+(const MatrixM &m, double a)
{
  MatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++)
      tmp.rows[i][j] += a;
  }

  return tmp;
}

MatrixM operator-(const MatrixM &m, double a)
{
  MatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++)
      tmp.rows[i][j] -= a;
  }

  return tmp;
}


MatrixM operator+(double a, const MatrixM &m)
{
  MatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    for (int j=m.clow; j<=m.chigh; j++)
      tmp.rows[i][j] += a;
  }

  return tmp;
}

MatrixM operator-(double a, const MatrixM &m)
{
  MatrixM tmp(m.rlow, m.rhigh, m.clow, m.chigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++)  {
    for (int j=m.clow; j<=m.chigh; j++)
      tmp.rows[i][j] -= a;
  }

  return tmp;
}


MatrixM &MatrixM::operator*=(double a)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] *= a;
  }

  return *this;
}

MatrixM &MatrixM::operator/=(double a)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] /= a;
  }

  return *this;
}

MatrixM &MatrixM::operator+=(double a)
{
  for (int i=rlow; i<=rhigh; i++)  {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] += a;
  }

  return *this;
}

MatrixM &MatrixM::operator-=(double a)
{
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] -= a;
  }

  return *this;
}


MatrixM &MatrixM::operator+=(const MatrixM &m)
{
  if (m.rlow!=rlow || m.rhigh!=rhigh || m.clow!=clow || m.chigh!=chigh) 
    throw OperationException("+=", cname, __FILE__, __LINE__);
  
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] += m.rows[i].elements[j];
  }
  return *this;
}


MatrixM &MatrixM::operator-=(const MatrixM &m)
{
  if (m.rlow!=rlow || m.rhigh!=rhigh || m.clow!=clow || m.chigh!=chigh) 
    throw OperationException("-=", cname, __FILE__, __LINE__);
  
  for (int i=rlow; i<=rhigh; i++) {
    for (int j=clow; j<=chigh; j++)
      rows[i].elements[j] -= m.rows[i].elements[j];
  }

  return *this;
}


double operator^(const MatrixM &m1, const MatrixM &m2)
{
  if (m1.getrlow()!=m2.getrlow() || m1.getrhigh()!=m2.getrhigh() || 
      m1.getclow()!=m2.getclow() || m1.getchigh()!=m2.getchigh()) 
    throw OperationException("^", "friend (MatrixM, MatrixM)", 
			     __FILE__, __LINE__);
  
  double tmp = 0.0;
  
  for (int i=m1.rlow; i<=m1.rhigh; i++)  {
    for (int j=m1.clow; j<=m1.chigh; j++)
      tmp +=  m1[i][j]*m2[i][j];
  }
  
  return tmp;
}

Three_Vector operator*(const Three_Vector &v, const MatrixM &m)
{
  if (m.clow != 1 || m.chigh != 3 || m.rlow != 1 || m.rhigh != 3) 
    throw OperationException("*", "friend (Three_Vector, MatrixM)", 
			     __FILE__, __LINE__);
  
  Three_Vector tmp;
  
  for (int j=1; j<=3; j++) {
    tmp.x[j-1] = 0.0;
    for (int i=1; i<=3; i++)
      tmp.x[j-1] += m.rows[i].elements[j]*v.x[j-1];
  }
  return tmp;
}


Three_Vector operator*(const MatrixM &m, const Three_Vector &v)
{
  if (m.clow != 1 || m.chigh != 3 || m.rlow != 1 || m.rhigh != 3) 
    throw OperationException("*", "friend (MatrixM, Three_Vector)", 
			     __FILE__, __LINE__);
  
  Three_Vector tmp;
  
  for (int i=1; i<=3; i++) {
    tmp.x[i-1] = 0.0;
    for (int j=1; j<=3; j++)
      tmp.x[i-1] += m.rows[i].elements[j]*v.x[j-1];
  }

  return tmp;
}

VectorM operator*(const MatrixM &m, const VectorM &v)
{
  if (m.clow != v.low || m.chigh != v.high) 
    throw OperationException("*", "friend (MatrixM, VectorM)", 
			     __FILE__, __LINE__);
  
  VectorM tmp(m.rlow, m.rhigh);
  
  for (int i=m.rlow; i<=m.rhigh; i++) {
    tmp.elements[i] = 0.0;
    for (int j=m.clow; j<=m.chigh; j++) {
      tmp.elements[i] += 
	m.rows[i].elements[j]*v.elements[j];
    }
  }
  return tmp;
}

VectorM operator*(const VectorM &v, const MatrixM &m)
{
  if (m.rlow != v.low || m.rhigh != v.high) 
    throw OperationException("*", "friend (VectorM, MatrixM)", 
			     __FILE__, __LINE__);
  
  VectorM tmp(m.clow, m.chigh);
  
  for (int j=m.clow; j<=m.chigh; j++) {
    tmp.elements[j] = 0.0;
    for (int i=m.rlow; i<=m.rhigh; i++)
      tmp.elements[j] +=
	m.rows[i].elements[j]*v.elements[i];
  }

  return tmp;
}


MatrixM operator*(const MatrixM &m1, const MatrixM &m2)
{
  if (m1.clow != m2.rlow || m1.chigh != m2.rhigh)
    throw OperationException("*", "friend (MatrixM, MatrixM)", 
			     __FILE__, __LINE__);
  
  MatrixM tmp(m1.rlow, m1.rhigh, m2.clow, m2.chigh);
  
  for (int i=m1.rlow; i<=m1.rhigh; i++) {
    for (int j=m2.clow; j<=m2.chigh; j++) {
      
      tmp.rows[i].elements[j] = 0.0;

      for (int k=m1.clow; k<=m1.chigh; k++) {
	tmp.rows[i].elements[j] += m1.rows[i].elements[k]
	  *m2.rows[k].elements[j];
      }
    }
  }
  return tmp;
  
}

MatrixM MatrixM::Transpose(void)
{
  MatrixM t(clow, chigh, rlow, rhigh);
  
  for (int i=t.rlow; i<=t.rhigh; i++)
    t.fastsetrow(i, fastcol(i));
  
  return t;
}

double MatrixM::Trace(void)
{
  double t=0.0;
  
  for (int i=rlow; i<=rhigh; i++) t += rows[i][i];
  return t;
}

double Trace(MatrixM &m)
{
  return m.Trace();
}

MatrixM Transpose(MatrixM &m)
{
  return m.Transpose();
}

void MatrixM::print(ostream& out)
{
  for (int i=rlow; i<=rhigh; i++) rows[i].print(out);
}


void MatrixM::binwrite(ostream& out)
{
  out.write((char*)&rlow, sizeof(int));
  out.write((char*)&rhigh, sizeof(int));
  out.write((char*)&clow, sizeof(int));
  out.write((char*)&chigh, sizeof(int));
  
  for (int i=rlow; i<=rhigh; i++)
    rows[i].binwrite(out);
}


MatrixM MatrixM_binread(istream& in)
{
  
  int rlow, rhigh, clow, chigh;
  
  in.read((char*)&rlow, sizeof(int));
  in.read((char*)&rhigh, sizeof(int));
  in.read((char*)&clow, sizeof(int));
  in.read((char*)&chigh, sizeof(int));
  
  MatrixM tmp(rlow, rhigh, clow, chigh);
  
  for (int i=rlow; i<=rhigh; i++)
    tmp.rows[i] = VectorM_binread(in);
  
  return tmp;
}


inline double fabs(VectorM& v) {return sqrt(v*v);}

