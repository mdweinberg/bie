#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cmath>

using namespace std;

void AsciiProgressBar(ostream& out, 	// I/O stream
		      const int count,	// The current count value
		      const int total,	// Total number of counts possible
		      const int incr,	// Update after every incr counts
		      const bool val=false)	// Print the count value
{
  if (count % incr == 0) {

				// Resolution is 2%, bar is 50 char long
    int pcent = (count * 100) / total;
    pcent = pcent / 2;

				// Head
    ostringstream output;
    output << "\r" << right << setw(3) << pcent*2  << "% [";

				// Completed
    for (int i=0; i<=pcent; i++)
      output << "=";
    output << ">";
				// Not yet complete
    for (int i=pcent; i<50; i++)
      output << "_";
				// Tail
    output << "]";

    if (val) output << " " << setw(floor(log(total)/log(10.0))+1)
		    << count << "/" << total;
				// Spit to output stream
    out << output.str() << flush;
  }
}

/*

// Example . . .

int main(int argc, char** argv)
{
  cout << "Console Ascii Bar" << endl;
  for (int x = 1; x <= 100; x++) {
    AsciiProgressBar(cout, x, 100, 1, true);
    usleep(50000);
  }
}
 
*/
