#include <cmath>

//______________________________________________________________________________

static double GamCf(double a, double x)
{
  // Computation of the incomplete gamma function P(a,x)
  // via its continued fraction representation.
  //
  // The algorithm is based on the formulas and code from Numerical
  // Recipes 2nd ed. p. 210-212 (W.H.Press et al.).
  //
  
  int itmax    = 100;      // Maximum number of iterations
  double eps   = 3.e-7;    // Relative accuracy
  double fpmin = 1.e-30;   // Smallest double value allowed here

  if (a <= 0 || x <= 0) return 0;

  double gln = lgamma(a);
  double b   = x+1-a;
  double c   = 1/fpmin;
  double d   = 1/b;
  double h   = d;
  double an,del;
  for (int i=1; i<=itmax; i++) {
    an = double(-i)*(double(i)-a);
    b += 2;
    d  = an*d+b;
    if (fabs(d) < fpmin) d = fpmin;
    c = b+an/c;
    if (fabs(c) < fpmin) c = fpmin;
    d   = 1/d;
    del = d*c;
    h   = h*del;
    if (fabs(del-1) < eps) break;
    //if (i==itmax) cout << "*GamCf(a,x)* a too large or itmax too small" << endl;
  }
  double v = exp(-x+a*log(x)-gln)*h;
  return (1-v);
}

//______________________________________________________________________________

static double GamSer(double a,double x)
{
   // Computation of the incomplete gamma function P(a,x)
   // via its series representation.
   //
   // The algorithm is based on the formulas and code from Numerical
   // Recipes 2nd ed., pp. 210-212 (W.H.Press et al.).
   //

   int itmax  = 100;   // Maximum number of iterations
   double eps = 3.e-7; // Relative accuracy

   if (a <= 0 || x <= 0) return 0;

   double gln = lgamma(a);
   double ap  = a;
   double sum = 1/a;
   double del = sum;
   for (int n=1; n<=itmax; n++) {
      ap  += 1;
      del  = del*x/ap;
      sum += del;
      if (fabs(del) < fabs(sum*eps)) break;
      //if (n==itmax) cout << "*GamSer(a,x)* a too large or itmax too small" << endl;
   }
   double v = sum*exp(-x+a*log(x)-gln);
   return v;
}


//______________________________________________________________________________

double gamma_p(double a, double x)
{
  // Computation of the incomplete gamma function P(a,x)
  //
  // The algorithm is based on the formulas and code from Numerical
  // Recipes 2nd ed. pp. 210-212 (W.H.Press et al.).
  //
  
  if (a <= 0 || x <= 0) return 0;

  if (x < (a+1)) return GamSer(a,x);
  else           return GamCf (a,x);
}

//_______________________________________________________________________
