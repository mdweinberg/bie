// This is really -*- C++ -*-

#ifndef _VectorM_h
#define _VectorM_h

#include <cmath>
#include <iostream>
#include <fstream>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/nvp.hpp>

#include <ArrayException.h>

using namespace std;

typedef complex<double> Complex;

class Three_Vector;
class CVectorM;
class CMatrixM;
class MatrixM;


//! Real-valued vector class
class VectorM
{
  friend class CMatrixM;
  friend class CVectorM;
  friend class MatrixM;
  
 protected:
  int low, high;
  double *elements;

 public:

  /// Class name
  static const string cname;

  //@{
  /// Constructors
  VectorM(void);
  VectorM(int, int);
  VectorM(int, int, double *);
  VectorM(const VectorM &);
  //@}
  
  /// The destructor
  ~VectorM(void);

  /// conversion
  operator Three_Vector(void);
  
  //@{
  /// assignment, access, resizing
  void setsize(int, int);
  double &operator[](int) const; // safe access
  
  VectorM &operator=(const VectorM &);
  void load(double *);
  void zero(void);

  bool isNull()       const {return elements ? false : true;}
  int getlength(void) const {return high-low+1;}
  int getlow(void)    const {return low;}
  int gethigh(void)   const {return high;}
  //@}
  
  //@{
  /// make C-type array
  double *array(int l, int h);
  double *array_copy(int l, int h) const;
  friend void destroy_VectorM_array(double *, int, int);
  //@}

  //@{
  /// unary plus and minus
  VectorM operator+() {return *this;}
  VectorM operator-();
  //@}
  
  //@{
  /// Vector addition and subtraction
  VectorM &operator+=(const VectorM &);
  VectorM &operator-=(const VectorM &);
  friend VectorM operator+(const VectorM &, const VectorM &);
  friend VectorM operator-(const VectorM &, const VectorM &);
  //@}
  
  //@{
  /// dot product
  friend double operator*(const VectorM &, const VectorM &);
  friend VectorM operator^(const VectorM &, const VectorM &);
  //@}
  
  /// combine product
  friend VectorM operator&(const VectorM&, const VectorM &);
  
  //@{
  /// Operations with scalars
  VectorM &operator*=(double);
  VectorM &operator/=(double);
  VectorM &operator+=(double);
  VectorM &operator-=(double);
  friend VectorM operator*(const VectorM &, double);
  friend VectorM operator*(double, const VectorM &);
  friend VectorM operator/(const VectorM &, double);
  //@}
  
  /// Magnitude of vector
  friend double fabs(VectorM& v);
  
  //@{
  /// operations with matrices
  friend VectorM operator*(const MatrixM &, const VectorM &);
  friend VectorM operator*(const VectorM &, const MatrixM &);
  friend MatrixM operator+(const MatrixM &, const MatrixM &);
  friend MatrixM operator-(const MatrixM &, const MatrixM &);
  friend MatrixM operator*(const MatrixM &, const MatrixM &);
  friend MatrixM operator*(const MatrixM &, double);
  friend MatrixM operator/(const MatrixM &, double);
  friend MatrixM operator*(double, const MatrixM &);
  friend CVectorM operator*(const CMatrixM &a, const VectorM &v);
  friend Three_Vector operator*(const MatrixM &, const Three_Vector &);
  friend Three_Vector operator*(const Three_Vector &, const MatrixM &);
  //@}
 
  //@{
  /// IO
  void print(ostream&);
  void binwrite(ostream&);
  friend VectorM VectorM_binread(istream&);
  //@}
  
  /// Sort the elements
  void Sort(void);
};

//! Real valued matrix
class MatrixM
{
  friend class VectorM;
  friend class Three_Vector;
  friend class CMatrixM;
  friend class CVectorM;
  
 protected:
  int rlow, rhigh;	// low and high row indices
  int clow, chigh;	// low and high column indices
  VectorM *rows;
  
  // fast (but unsafe) access functions */
  //
  VectorM fastcol(int);
  VectorM &fastrow(int);
  void fastsetrow(int, const VectorM &);
  void fastsetcol(int, VectorM &);
  
 public:

  /// Class name
  static const string cname;
  
  //@{
  /// Constructors and the destructor
  MatrixM(void);
  MatrixM(int, int, int, int);
  MatrixM(int, int, int, int, double **);
  MatrixM(const MatrixM &);
  ~MatrixM(void);
  //@}
  
  //@{
  /// assignment and access
  MatrixM &operator=(const MatrixM &);
  VectorM &operator[](int) const;
  
  inline bool isNull()      const {return rows ? false : true;}
  inline int getnrows(void) const {return rhigh-rlow+1;}
  inline int getncols(void) const {return chigh-clow+1;}
  inline int getrlow(void)  const {return rlow;}
  inline int getclow(void)  const {return clow;}
  inline int getrhigh(void) const {return rhigh;}
  inline int getchigh(void) const {return chigh;}
  //@}
  
  //@{
  // Sizing
  void setsize(int, int, int, int);
  void zero(void);
  void load(double **);
  //@}
  
  //@{
  /// row and column access
  VectorM col(int);
  VectorM &row(int);
  void setrow(int, VectorM &);
  void setcol(int, VectorM &);
  //@}
  
  //@{
  /// Make a C-type double pointer
  double **array(int, int, int, int);
  double **array_copy(int, int, int, int) const;
  friend void destroy_MatrixM_array(double **, int, int, int, int);
  //@}
  
  //@{
  /// operations with matrices
  friend MatrixM operator+(const MatrixM &, const MatrixM &);
  friend MatrixM operator-(const MatrixM &, const MatrixM &);
  friend MatrixM operator*(const MatrixM &, const MatrixM &);
  friend double operator^(const MatrixM &, const MatrixM &);
  MatrixM &operator+=(const MatrixM &);
  MatrixM &operator-=(const MatrixM &);
  //@}
  
  //@{
  /// unary plus and minus
  MatrixM operator+(void) {return *this;}
  MatrixM operator-(void);
  //@}

  
  //@{
  /// Operations with vectors
  friend VectorM operator*(const MatrixM &, const VectorM &);
  friend VectorM operator*(const VectorM &, const MatrixM &);
  friend Three_Vector operator*(const MatrixM &, const Three_Vector &);
  friend Three_Vector operator*(const Three_Vector &, const MatrixM &);
  //@}
  
  //@{
  /// operations with scalars
  friend MatrixM operator*(const MatrixM &, double);
  friend MatrixM operator*(double, const MatrixM &);
  friend MatrixM operator/(const MatrixM &, double);
  friend MatrixM operator+(const MatrixM &, double);
  friend MatrixM operator+(double, const MatrixM &);
  friend MatrixM operator-(const MatrixM &, double);
  friend MatrixM operator-(double, const MatrixM &);
  MatrixM &operator*=(double);
  MatrixM &operator/=(double);
  MatrixM &operator+=(double);
  MatrixM &operator-=(double);
  //@}
  
  //@{
  /// IO
  void print(ostream&);
  void binwrite(ostream&);
  friend MatrixM MatrixM_binread(istream&);
  //@}
  
  //@{
  /// Linear algebra
  MatrixM Transpose(void);	
  double Trace(void);
  VectorM Symmetric_Eigenvalues(MatrixM &);
  VectorM Symmetric_Eigenvalues_GHQL(MatrixM &);
  MatrixM Inverse(void);
  //@}
  
};



//! Optimized real-valued 3-vector
class Three_Vector
{
  friend class VectorM;
  friend class MatrixM;

 protected:
  double x[3];
  static int nlive;

 public:

  /// Class name
  static const string cname;

  //@{
  /// Constructors
  Three_Vector(void)
    {nlive++;}
  Three_Vector(double xx, double yy, double zz)
    {x[0] = xx; x[1] = yy; x[2] = zz;}
  Three_Vector(const Three_Vector &v)
    {nlive++; x[0]=v.x[0]; x[1]=v.x[1]; x[2]=v.x[2];}
  ~Three_Vector(void)
    {nlive--;}
  //@}
  
  /// conversion
  operator VectorM(void);
  
  /// Element access
  double  operator[](int i) const { return x[i-1]; }
  double& operator[](int i)       { return x[i-1]; }
  

  /// assignment operator
  Three_Vector &operator=(const Three_Vector &v)	
    {x[0]=v.x[0]; x[1]=v.x[1]; x[2]=v.x[2]; return *this;}
  
  //@{
  /// reflexive arithmetic operators
  Three_Vector &operator+=(const Three_Vector &v)
    {x[0]+=v.x[0]; x[1]+=v.x[1]; x[2]+=v.x[2]; return *this;}
  Three_Vector &operator-=(const Three_Vector &v)
    {x[0]-=v.x[0]; x[1]-=v.x[1]; x[2]-=v.x[2]; return *this;}
  Three_Vector &operator*=(double a)
    {x[0]*=a; x[1]*=a; x[2]*=a; return *this;}
  Three_Vector &operator/=(double a)
    {x[0]/=a; x[1]/=a; x[2]/=a; return *this;}
  //@}
  
  //@{
  /// unary plus and minus
  Three_Vector operator+(void) {return *this;}
  Three_Vector operator-(void) 
  { 
    x[0] = -x[0]; 
    x[1] = -x[1]; 
    x[2] = -x[2]; 
    return *this;
  }
  //@}
  
  //@{
  /// binary operations with Three_Vectors
  friend Three_Vector operator+(const Three_Vector &, const Three_Vector &);
  friend Three_Vector operator-(const Three_Vector &, const Three_Vector &);
  friend Three_Vector operator^(const Three_Vector &, const Three_Vector &);
  friend Three_Vector Cross(const Three_Vector &, const Three_Vector &);
  //@}
  
  //@{
  /// Binary operations with doubles
  friend Three_Vector operator*(const Three_Vector &, double);
  friend Three_Vector operator*(double, const Three_Vector &);
  friend Three_Vector operator/(const Three_Vector &, double);
  //@}
  
  //@{
  /// Binary operations with VectorMs
  friend Three_Vector operator+(const VectorM &, const Three_Vector &);
  friend Three_Vector operator-(const VectorM &, const Three_Vector &);
  friend Three_Vector operator^(const VectorM &, const Three_Vector &);
  friend Three_Vector operator+(const Three_Vector &, const VectorM &);
  friend Three_Vector operator-(const Three_Vector &, const VectorM &);
  friend Three_Vector operator^(const Three_Vector &, const VectorM &);
  
  friend Three_Vector operator*(const MatrixM &, const Three_Vector &);
  friend Three_Vector operator*(const Three_Vector &, const MatrixM &);
  //@}
  
  /// Scalar product
  friend double operator*(const Three_Vector &v1, const Three_Vector &v2);
  
  /// Zero all the elements
  void zero(void)		
  {x[0]=0.0; x[1]=0.0; x[2]=0.0;}
  
  //@{
  /// IO
  void binwrite(ostream&);
  void binread(istream&);
  void print(ostream&);
  static void count(void)
  {cerr << "number of live three_vectors " << nlive << endl;}
  //@}
  

};

//@{
/// Linear algebra and helper functions
MatrixM Transpose(MatrixM &m);
double Trace(MatrixM &m);
VectorM Symmetric_Eigenvalues(MatrixM &m, MatrixM &ev);
VectorM Symmetric_Eigenvalues_GHQL(MatrixM &m, MatrixM &ev);
void jacobi(double **, int, double *, double **, int *);
void eigsrt(double *, double **, int);
void SVD(MatrixM A, MatrixM &U, MatrixM &V, VectorM &Z);
void destroy_VectorM_array(double *, int, int);
void destroy_MatrixM_array(double **, int, int, int, int);
//@}

//@{
/// Non-intrusive version of serialize/save/load routine for VectorM
/// and MatrixM classes
namespace boost { 
  namespace serialization {
    template<class Archive>
    inline void save(Archive & ar,
		     const VectorM &v,
		     const unsigned int /* file_version */) {
      
      // need to save status, low, high, and doubles
      //
      bool null = v.isNull();
      int low   = v.getlow();
      int high  = v.gethigh();
      
      ar << BOOST_SERIALIZATION_NVP(null);
      ar << BOOST_SERIALIZATION_NVP(low);
      ar << BOOST_SERIALIZATION_NVP(high);
      
      if (!null) {
	char buf[128];
	for (int i=low;i<=high;i++) {
	  sprintf(buf, "item_%d", i);
	  ar << boost::serialization::make_nvp(buf, v[i]);
	}
      }
      
    }
    
    template<class Archive>
      inline void load(Archive & ar,
		       VectorM &v,
		       const unsigned int /* file_version */) {
      bool null;
      int low, high;
      
      ar >> BOOST_SERIALIZATION_NVP(null);
      ar >> BOOST_SERIALIZATION_NVP(low);
      ar >> BOOST_SERIALIZATION_NVP(high);
      
      v.setsize(low, high);
      
      if (!null) {
	char buf[128];
	for (int i=low; i<=high; i++) {
	  sprintf(buf, "item_%d", i);
	  ar >> boost::serialization::make_nvp(buf, v[i]);
	}
      }
    }
    
    template<class Archive>
      inline void serialize(Archive & ar,
			    VectorM &v,
			    const unsigned int file_version) 
    {
      boost::serialization::split_free(ar, v, file_version);
    }
    

    template<class Archive>
    inline void save(Archive & ar,
		     const Three_Vector &v,
		     const unsigned int /* file_version */) 
    {
      double x = v[1];
      double y = v[2];
      double z = v[3];

      ar << BOOST_SERIALIZATION_NVP(x);
      ar << BOOST_SERIALIZATION_NVP(y);
      ar << BOOST_SERIALIZATION_NVP(z);
    }


    template<class Archive>
      inline void load(Archive & ar,
		       Three_Vector &v,
		       const unsigned int /* file_version */) 
    {
      double x, y, z;

      ar >> BOOST_SERIALIZATION_NVP(x);
      ar >> BOOST_SERIALIZATION_NVP(y);
      ar >> BOOST_SERIALIZATION_NVP(z);
      
      v[1] = x;
      v[2] = y;
      v[3] = z;
    }

    template<class Archive>
      inline void serialize(Archive & ar,
			    Three_Vector &v,
			    const unsigned int file_version) {
      boost::serialization::split_free(ar, v, file_version);
    }

    template<class Archive>
    inline void save(Archive & ar,
		       const MatrixM &m,
		     const unsigned int /* file_version */) 
    {
      // need to save statuc, rlow, rhigh, clow, chigh and rows (VectorM*)
      //
      bool null = m.isNull();
      int rlow  = m.getrlow();
      int rhigh = m.getrhigh();
      int clow  = m.getclow();
      int chigh = m.getchigh();
      
      ar << BOOST_SERIALIZATION_NVP(null);
      ar << BOOST_SERIALIZATION_NVP(rlow);
      ar << BOOST_SERIALIZATION_NVP(rhigh);
      ar << BOOST_SERIALIZATION_NVP(clow);
      ar << BOOST_SERIALIZATION_NVP(chigh);
      
      // MatrixM is just a collection of VectorM/s
      //
      if (!null) {
	char buf[128];
	for (int i=rlow;i<=rhigh;i++) {
	  sprintf(buf, "item_%d", i);
	  ar << boost::serialization::make_nvp(buf, m[i]);
	}
      }
    }
    
    template<class Archive>
      inline void load(Archive & ar,
		       MatrixM &m,
		       const unsigned int /* file_version */) {
      bool null;
      int rlow, rhigh, clow, chigh;
      
      ar >> BOOST_SERIALIZATION_NVP(null);
      ar >> BOOST_SERIALIZATION_NVP(rlow);
      ar >> BOOST_SERIALIZATION_NVP(rhigh);
      ar >> BOOST_SERIALIZATION_NVP(clow);
      ar >> BOOST_SERIALIZATION_NVP(chigh);
      
      m.setsize(rlow, rhigh, clow, chigh);
      
      if (!null) {
	char buf[128];
	for (int i=rlow;i<=rhigh;i++) {
	  sprintf(buf, "item_%d", i);
	  ar >> boost::serialization::make_nvp(buf, m[i]);
	}
      }
    }
    
    template<class Archive>
      inline void serialize(Archive & ar,
			    MatrixM &m,
			    const unsigned int file_version) {
      boost::serialization::split_free(ar, m, file_version);
    }

  } // namespace serialization
} // namespace boost
//@}

#endif
