#include "bieTags.h"
#include "EngineConfig.h"
#include "gvariable.h"

std::string engine_type_str[3] = { std::string("BOOST_TEXT"),
                                   std::string("BOOST_XML"),
                                   std::string("BOOST_BINARY") };
