// This is really -*- C++ -*-

#ifndef TESTCHECKPOINTMANAGER_H
#define TESTCHECKPOINTMANAGER_H

#include <iostream>
#include <ctime>

#include "TestSaveManager.h"
#include "CheckpointManager.h"

/** This checkpoint manager is for testing only and is used by test
    routines */
class TestCheckpointManager : public CheckpointManager 
{
 public: 

  //! Construct with the Save Manager
  TestCheckpointManager(TestSaveManager &tsm);

  //! Destructor
  ~TestCheckpointManager();

  //! Dump a checkpoint to ostream @param out
  void checkpoint(std::ostream &out);

  //! Take a checkpoint every @param iterations MCMC steps
  void setInterval(uint64 iterations);

  //! Take a checkpoint every @param seconds by the wall clock
  void setTimer(time_t seconds);

  //! Take a checkpoint right now!
  void setCheckpointNow();

 private:
  TestSaveManager *tsm;
  
  // when count == 0, take a checkpoint and reset it to interval
  bool itr_on;
  uint64 itr_interval;
  uint64 itr_count;

  // when deadline has passed, take a checkpoint and reset the timer
  bool timer_on;
  time_t timer_interval;
  time_t timer_deadline; // FIXME: time.h

  // take a checkpoint if this is true and set it to false
  bool now;
};
#endif
