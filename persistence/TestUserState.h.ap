// This is really -*- C++ -*-

#ifndef TestUserState_h
#define TestUserState_h

#include <string>

#include "UserState.h"
#include "StateHistory_Test.h"
#include "StateMetaInfo_Test.h"
#include "StateTransClosure_Test.h"

#include "VectorM.h"
#include "CVectorM.h"

@include_persistence

//! Manage state inforation of a user-defined persistant object
class @persistent(TestUserState) : public @super(UserState) 
{

public:

  /**
   Constructs a test user state that holds state information:
   <ul>
   <li> Meta information (username of archiver, hostname archive was written
   on, etc);
   <li> A blank history object (no history outside of CLI), and;
   <li> The transitive closure of a collection of objects.
   </ul>
  */
  TestUserState(string state_name, uint32 version, bool saving);

  /**
  * Constructor used when reading.
  */

  /// Add objects to the state
  //@{
  //! A serializable object
  void addObject(Serializable* obj);
  //! A VectorM type
  void addObject(VectorM* obj);
  //! A MatrixM type
  void addObject(MatrixM* obj);
  //! A Three-Vector type
  void addObject(Three_Vector* obj);
  //! A complex-valued VectorM type
  void addObject(CVectorM* obj);
  //! A complex-valued MatrixM type
  void addObject(CMatrixM* obj);
  //@}
  
  /**
  * Returns the history object for this state.
  */
  StateHistory & getHistory();
  
  /** 
  * Returns the meta info object for this state.
  */
  StateMetaInfo & getMetaInfo();
  
  /**
  * Returns the transitive closure object for this state.
  */
  StateTransClosure_Test & getTransClosure();

private:

  /// This is a dummy, empty object, not used in test states.
  StateHistory_Test      * @autopersist(tus_history);
  
  /// Holds meta information
  StateMetaInfo_Test     * @autopersist(tus_metainfo);
  
  /// Holds the roots of a transitive closure - this is where archiving
  /// and restoring begin seriously.
  StateTransClosure_Test * @autopersist(tus_transclosure);

@persistent_end
};

#endif
