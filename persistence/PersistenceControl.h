// This is really -*- C++ -*-

#ifndef PersistenceControl_h
#define PersistenceControl_h

#include <vector>
#include <string>

#include "EngineConfig.h"
#include "CLISaveManager.h"
#include "Backend.h"

class CLIUserState;
class StateHistory_CLI;
class StateMetaInfo_CLI;

/**
* PersistenceControl
* 
* This is the interface used by CLI to save, restore, and query states.
*/
class PersistenceControl 
{

public:

  /**
  * Called by CLI when the "psave <comment>" command is issued.
  */
  static void psave(string comment, ostream & console);

  /**
   * Called by CLI when the "pnewsession <session>" command is issued.
   */
  static void pnewsession(string name, ostream & console);

  /**
   * Called by CLI when the "pnewsession <session> <archive_type>"
   * command is issued.
   */
  static void pnewsession(string name, string archive_type, ostream & console);

  /**
  * Called by CLI when the "psave" command is called with no state name
  * specified.
  */
  static void psave(ostream & console);

  /**
  * Called by CLI when the prestore command is called with only a statename,
  * indicating that the most recent version of the state should be restored.
  */
  static void prestore (string statename, ostream & console);

  /**
  * Called by CLI when the prestore command is called with a statename and
  * a version number.
  */
  static void prestore (string statename, uint32 version, ostream & console);
  
  /**
  * Called by CLI when the phistory command is called.  This lists the
  * commands used to reach the specified state on the CLI console.
  */
  static void phistory(string statename, int32 version, ostream & console);

  /**
  * Called by CLI when the pmetainfo command is called.  This lists the
  * metadata for the specified  version.
  */
  static void pmetainfo (string statename, uint32 version, ostream & console);

  /**
  * Called by CLI when the pmetainfo command is called.  This lists the
  * matadate for ALL versions.
  */
  static void pmetainfo (string statename, ostream & console);

  /**
  * Called by CLI when the plist command is called.  This prints a list
  * of states out to the console in alphabetical order.
  */
  static void plist(ostream & console);

  /**
  * Called by CLI when the pversions command is called.  This prints a 
  * of available versions, along with the date they were created.
  */
  static void pversions(string statename, ostream & console);

  //! Turn checkpointing on
  static void ckon(ostream & console);

  //! Turn checkpointing off
  static void ckoff(ostream & console);

  //! Toggle the checkpointing state (on-->off and vice versa)
  static void cktoggle(ostream & console);

  //! Set the checkpointing interval in MCMC steps
  static void ckinterval(int interval, ostream & console);

  //! Set the checkpointing interval in seconds
  static void cktimer(int timer, ostream & console);

  //! Take a checkpoint manually now
  static void ckmanual(ostream & console);

  //! Clean-up (that is delete) the checkpointing helper instances
  static void ckcleanup();

private:

  static uint32 getLatest(string sessionname);

  //! CLI User State
  static CLIUserState * userstate;

  //! static Backend * backend;
  static CLISaveManager * savemanager;
};

#endif
