#include <mpi.h>
#include <iomanip>
#include "CLICheckpointManager.h"

// For clivector classes
#include "Tessellation.h"
#include "Distribution.h"

BIE_CLASS_EXPORT_IMPLEMENT(CLICheckpointManager)

namespace BIE {
  extern bool mpi_used;
  extern int myid;
}

CLICheckpointManager::CLICheckpointManager(CLISaveManager &tsmin) : 
  CheckpointManager(tsmin) 
{
  tsm = &tsmin;

  is_on = false;

  itr_interval = 0;
  itr_count = 0;

  timer_interval = 0;
  timer_deadline = 0;

  now = false;
}

CLICheckpointManager::~CLICheckpointManager() {
  /* nothing to do */
}

void CLICheckpointManager::checkpoint(std::ostream &out) {
  bool takeit = false;

  if (BIE::myid != 0) {
    short flag = takeit;
    MPI_Bcast(&flag, 1, MPI_SHORT, 0, MPI_COMM_WORLD);
    if (flag) takeit = true;
    else takeit = false;

    if (takeit) {
      // trigger a checkpoint
      try {
	tsm->setComment("automated checkpoint");
	tsm->save();
      } catch (BoostSerializationException * e) {
      }
    }
    return;
  }

  if (is_on && itr_interval > 0) {
    itr_count--;
  }

  out << "- Checkpoint -----------------------------"
      << "--------------------------------------" << endl;
  out << setw(16) << "on: " << ((is_on==true)?"yes":"no") << endl;
  if (itr_interval > 0) {
    out << setw(16) << "interval: " << itr_count << " out of " << itr_interval;
    out << " left" << endl;
  }
  
  if (timer_interval > 0) {
    time_t t = timer_deadline - time(NULL);
    out << setw(16) << "time: ";
    if (timer_deadline == 0) {
      out << timer_interval << " seconds left until next checkpoint." << endl;
    } else if (t<=0) {
      out << "deadline passed. (" << t << ")" << endl;
    } else {
      out << t << " seconds left until next checkpoint." << endl;
    }
  }
  
  if (is_on) {
    out << setw(16) << "now: " << ((now==true)?"true":"false") << endl;
  }
  
  // out << itr_count << " " << time(NULL) << " " << now << endl;

  // check every criteria and make sure to reset
  if (is_on && itr_interval > 0 && itr_count == 0) {
    itr_count = itr_interval;
    takeit = true;
  }
  if (is_on && timer_interval > 0 && time(NULL) > timer_deadline) {
    if (timer_deadline != 0) {
      takeit = true;
    }
    timer_deadline = time(NULL) + timer_interval;
  }
  if (now) {
    now = false;
    takeit = true;
  }

  if (BIE::mpi_used) {
    short flag = takeit;
    MPI_Bcast(&flag, 1, MPI_SHORT, 0, MPI_COMM_WORLD);
    if (flag) takeit = true;
    else takeit = false;
  }
  
  if (takeit) {
    // trigger a checkpoint
    out << setw(8) << "" << "CHECKPOINTING." << endl;
    try {
      tsm->setComment("automated checkpoint");
      tsm->save();
    } catch (BoostSerializationException * e) {
      out << "####### BoostSerializationException thrown! ########" << endl;
      out << "Printing persistence stack trace..." << endl;
      e->print(out);
      out << "####################################################" << endl;
    }
  }
  out << "-----------------------------------------------"
      << "-------------------- Checkpoint -" << endl;
}

void CLICheckpointManager::on() {
  is_on = true;
}

void CLICheckpointManager::off() {
  is_on = false;
}

void CLICheckpointManager::toggle() {
  is_on = !is_on;
}

void CLICheckpointManager::setInterval(uint64 iterations) {
  itr_interval = iterations;
  itr_count = itr_interval;
}

void CLICheckpointManager::setTimer(time_t seconds) {
  timer_interval = seconds;
  //  timer_deadline = time(NULL) + timer_interval;
}

void CLICheckpointManager::setCheckpointNow() {
  now = true;
}

bool CLICheckpointManager::getOnOff() {
  return is_on;
}
