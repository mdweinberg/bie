// This is really -*- C++ -*-

#ifndef LoadManager_h
#define LoadManager_h

#include "EngineConfig.h"
#include "UserState.h"
#include "Backend.h"
#include "BoostLoadEngine.h"

/**
   LoadManager: Deals with nitty-gritty bits of persistence mechanism.
   <ul>
   <li> file management - which file to store/restore objects to/from
   <li> DB management - PersistenceDB is used to keep track of versions of stored Objects
   </ul>
 */
class LoadManager 
{
 public:
  //! Constructor
  LoadManager(UserState * userstate, enum EngineType t, enum BackendType bt);

  //! Destructor
  virtual ~LoadManager();

  /** The member function that does the work restoring from the
      serialized archive */
  virtual void load() = 0;

 private:
  UserState *userState;
  Backend *backend;
  BoostLoadEngine *loadEngine;
};
#endif
