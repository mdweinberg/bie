// This is really -*- C++ -*-

#ifndef CLISaveManager_h
#define CLISaveManager_h

#include <fstream>

#include "EngineConfig.h"
#include "CLIUserState.h"
#include "BoostSaveEngine.h"
#include "Backend.h"
#include "FileBackend.h"
#include "SaveManager.h"

/**
   CLISaveManager: Deals with nitty-gritty bits of persistence mechanism.
   <ul>
   <li> file management - which file to store/restore objects to/from
   <li> DB management - PersistenceDB is used to keep track of versions of stored Objects
   </ul>
   Actual serialization of the object itself is done using SaveEngine
*/
class CLISaveManager : public SaveManager 
{
public:
  //! Constructor
  CLISaveManager(CLIUserState *userState, 
		 enum EngineType et, enum BackendType bt);

  //! Destructor
  ~CLISaveManager();

  //! The member function that does the serializaton work
  void save();

  //! The backend instance
  Backend * getBackend();

  //! Set the metainfo comment string
  void setComment(const string& comment) 
  {
    userState->getMetaInfo().setComment(comment);
  }

 private:
  CLIUserState * userState;
  Backend * backend;
  // Refer SaveEngine.h for the reason why this is BoostSaveEngine
  // instead of SaveEngine
  BoostSaveEngine * saveEngine;
  enum EngineType et;
  enum BackendType bt;

 protected:
  //! Null constructor
  CLISaveManager() { saveEngine = NULL; }

 private:
  friend class boost::serialization::access;
  template<class Archive>
    void save(Archive & ar, const unsigned int version) const {
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(SaveManager);
    try {
      ar << BOOST_SERIALIZATION_NVP(userState);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }                                                         
    ar << BOOST_SERIALIZATION_NVP(et);
    ar << BOOST_SERIALIZATION_NVP(bt);
  }

  template<class Archive>
    void load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(SaveManager);
    try {
      ar >> BOOST_SERIALIZATION_NVP(userState);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }                                                         
    ar >> BOOST_SERIALIZATION_NVP(et);
    ar >> BOOST_SERIALIZATION_NVP(bt);

    switch(bt) {
    case BACKEND_FILE:
      this->backend = new FileBackend();
      break;
    default:
      throw ArchiveException("No such BackendType.",__FILE__,__LINE__);
    }

    switch(et) {
    case BOOST_TEXT:
    case BOOST_XML:
    case BOOST_BINARY:
      // this->saveEngine = new
      // BoostSaveEngine(backend->getOutputStream(userState), et);
      this->saveEngine = new BoostSaveEngine();
      break;
    default:
      throw ArchiveException("No such EngineType.", __FILE__, __LINE__);
    }
  }

  BOOST_SERIALIZATION_SPLIT_MEMBER();
};

BIE_CLASS_TYPE_INFO(CLISaveManager)

#endif
