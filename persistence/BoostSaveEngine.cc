#include <iostream>
#include <fstream>

#include "BIEmpi.h"

#include "EngineConfig.h"
#include "PersistenceException.h"
#include "BoostSaveEngine.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

BIE_CLASS_EXPORT_IMPLEMENT(BoostSaveEngine)

using namespace std;

BoostSaveEngine::BoostSaveEngine()
{
  arB = NULL;
  arT = NULL;
  arX = NULL;
  o   = NULL;
}

void BoostSaveEngine::setOutputStream(ostream& o, EngineType t) 
{
  // Clean up from previous call
  //
  if (arB) { delete arB; arB = 0; }
  if (arT) { delete arT; arT = 0; }
  if (arX) { delete arX; arX = 0; }
  if (myid != 0) {
    if (this->o) delete this->o;
  }

  // Only the master writes to the actual file
  //
  if (myid == 0) {
    this->o = &o;
  } else {
    // Slaves write to the null device
    this->o = new ofstream("/dev/null");
  }

  switch(t) {
  case BOOST_TEXT:
    arT = new boost::archive::text_oarchive(*(this->o));
    if (myid == 0) cout << "Using text_oarchive"    << endl;
    break;
  case BOOST_XML:
    arX = new boost::archive::xml_oarchive(*(this->o));
    if (myid == 0) cout << "Using xml_oarchive" << endl;
    break;
  case BOOST_BINARY:
    arB = new boost::archive::binary_oarchive(*(this->o));
    if (myid == 0) cout << "Using binary_oarchive"  << endl;
    break;
  default:
    throw NoSuchArchiveTypeException("",__FILE__,__LINE__);
  }
  cout.flush();
}

BoostSaveEngine::~BoostSaveEngine() 
{
  if (arB) delete arB;
  if (arT) delete arT;
  if (arX) delete arX;
  if (myid != 0) {
    if (o) delete o;
  }
}
