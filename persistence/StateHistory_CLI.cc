#include "StateHistory_CLI.h"
#include <readline/history.h>

BIE_CLASS_EXPORT_IMPLEMENT(StateHistory_CLI)

StateHistory_CLI::StateHistory_CLI()
{
}

void StateHistory_CLI::capture()
{
  sh_history.clear();
  
  // Use readline history to get the commands used in this
  // session, and write them into the history vector.
  //
  unsigned int entryindex = 1;
  HIST_ENTRY * histentry = history_get(entryindex);
  
  while (histentry != NULL)
  {
    sh_history.push_back(histentry->line);
    entryindex++;
    histentry = history_get(entryindex);
  }
}
  
void StateHistory_CLI::restore()
{
  // Restore the readline history.
  //
  clear_history();

  for (unsigned int commandindex = 0; commandindex < sh_history.size(); commandindex++)
    { 
    add_history (sh_history[commandindex].c_str()); 
  }
}
