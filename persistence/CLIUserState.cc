#include "StateHistory_CLI.h"
#include "StateMetaInfo_CLI.h"
#include "StateTransClosure_CLI.h"

#include "CLICheckpointManager.h"
#include "CLIUserState.h"

// For clivector classes
#include "Tessellation.h"
#include "Distribution.h"

BIE_CLASS_EXPORT_IMPLEMENT(CLIUserState)

CLIUserState::CLIUserState(string state_name, uint32 version, bool saving)
{
  setSession(state_name);
  setVersion(version);

  if (saving) {
    this->cus_history      = new StateHistory_CLI();
    this->cus_metainfo     = new StateMetaInfo_CLI();
    this->cus_transclosure = new StateTransClosure_CLI();
  } else {
    this->cus_history      = 0;
    this->cus_metainfo     = 0;
    this->cus_transclosure = 0;
  }
}

CLIUserState::~CLIUserState()
{
  delete cus_history;
  delete cus_metainfo;
  delete cus_transclosure;
}

StateHistory & CLIUserState::getHistory() 
{ return *cus_history; }
  
StateMetaInfo & CLIUserState::getMetaInfo() 
{ return *cus_metainfo; }
  
StateTransClosure & CLIUserState::getTransClosure() 
{ return *cus_transclosure; }
