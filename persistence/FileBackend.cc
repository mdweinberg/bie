#include <boost/version.hpp>
#include "FileBackend.h"

#include <sstream>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "bieTags.h"
#include "BIEmpi.h"
#include "EngineConfig.h"
#include "PersistenceException.h"

using namespace std;
using namespace boost::filesystem;

namespace BIE {
  extern int myid;
}

/**
 * FileBackend: Implements file-backed backend.
 *              Sessions are stored in the following format
 *              <dir_prefix>/<session_name>/<version>/<files...>
 *
 * boost::filesystem used extensively for all file/directory operation
 * for portability
 */

FileBackend::FileBackend() 
{
  // dir_prefix defaults to current directory
  //
  char buf[1024];
  if (getcwd(buf, sizeof(buf)) != NULL) {
    dir_prefix = path(string(buf)) / PERSISTENCE_DIR;
  } else {
    throw ArchiveException("Couldn't initialize file backend.",__FILE__,__LINE__);
  }

  if (!exists(dir_prefix)) {
    create_directory(dir_prefix);
  } else if (!is_directory(dir_prefix)) {
    throw ArchiveException("Persistence directory name already in use!",__FILE__,__LINE__);
  }

  istream = new boost::filesystem::ifstream();
  ostream = new boost::filesystem::ofstream();
}

FileBackend::FileBackend(char *prefix): dir_prefix(path(prefix) / string(PERSISTENCE_DIR)) 
{
  if (!exists(dir_prefix)) {
    create_directory(dir_prefix);
  } else if (!is_directory(dir_prefix)) {
    throw ArchiveException("Persistence directory name already in use!",__FILE__,__LINE__);
  }
  
  istream = new boost::filesystem::ifstream();
  ostream = new boost::filesystem::ofstream();
}

FileBackend::~FileBackend() 
{
  ostream->flush();
  delete ostream;
  delete istream;
}

void FileBackend::listSessions(vector<string>& list) 
{
  list.clear();
  if (exists(dir_prefix) && is_directory(dir_prefix)) {
    // default ctor gives you an end-of-items iterator
    for (directory_iterator itr(dir_prefix); 
	 itr != directory_iterator(); ++itr) {
      if (is_directory(*itr)) {
#if BOOST_VERSION <= 103500
	list.push_back(itr->leaf());
#elif BOOST_VERSION >= 104600
	list.push_back(itr->path().filename().string());
#else
	list.push_back(itr->path().leaf());
#endif
      }
    }
  }
}

bool FileBackend::listVersions(vector<uint32>& list, const string& session) 
{
  list.clear();

  if (!exists(dir_prefix / session)) {
    return false;
  }

  // default ctor gives you an end-of-items iterator
  for (directory_iterator itr(dir_prefix / session);
       itr != directory_iterator(); ++itr) {
    if(is_directory(*itr)) {
      int i;
#if BOOST_VERSION <= 103500
      istringstream st(itr->leaf());
#elif BOOST_VERSION >= 104600
      istringstream st(itr->path().filename().string());
#else
      istringstream st(itr->path().leaf());
#endif
      st >> i;
      list.push_back(i);
    }
  }
  return true;
}

istream& FileBackend::getInputStream(const UserState* userState,
                                     const string& filename) {
  return getInputStream(userState->getSession(), userState->getVersion(), filename);
}

istream& FileBackend::getInputStream(const string& session, uint32 version,
                                     const string& filename) 
{
  stringstream s;
  s << version;
  string version_str = s.str();
  path p = dir_prefix / session / version_str / filename;
  if (!exists(p)) {
    std::string msg = "<" + session + "/" + version_str + "> does not exist.";
    throw new ArchiveException(msg ,__FILE__, __LINE__);
  } else {
    istream->close();
    istream->open(p, istream::in);
  }
  return *istream;
}

ostream& FileBackend::getOutputStream(const UserState* userState,
                                      const string& filename) {
  return getOutputStream(userState->getSession(), userState->getVersion(), filename);
}

ostream& FileBackend::getOutputStream(const string& session, uint32 version,
                                      const string& filename) 
{
  if (myid != 0) {

    ostream->open("/dev/null", ostream::out);

  } else {

    stringstream s;
    s << version;
    string version_str = s.str();
    path p = dir_prefix / session;

    // make sure the directory structure exists
    //
    if (!exists(p)) {

      create_directory(dir_prefix / session);

    } else if (!is_directory(dir_prefix / session)) {
      throw ArchiveException("Session directory name already in use!",
                             __FILE__,__LINE__);
    }
    
    // check the version; do not overwrite
    //
    p = p / version_str;
    if (!exists(p)) {
      create_directory(dir_prefix / session / version_str);
    } else if (!is_directory(dir_prefix / session / version_str)) {
      throw ArchiveException("Version directory name already in use!",
                             __FILE__,__LINE__);
    }
    
    p = p  / filename;
    ostream->flush();
    ostream->close();
    
    ostream->open(p, ostream::out);
  }
  
  return *ostream;
} 

