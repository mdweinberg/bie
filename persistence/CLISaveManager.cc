#include "Serializable.h"

#include "gvariable.h"
#include "CLICheckpointManager.h"
#include "CLISaveManager.h"
#include "FileBackend.h"
#include "PersistenceException.h"
#include "PersistenceControl.h"

#include "StateTransClosure_Test.h"
#include "CLIUserState.h"

// For clivector classes
#include "Tessellation.h"
#include "Distribution.h"

CLISaveManager::CLISaveManager(CLIUserState *userState, 
			       enum EngineType et, enum BackendType bt) 
{
  this->userState = userState;
  this->et = et;
  this->bt = bt;

  switch(bt) {
  case BACKEND_FILE:
    this->backend = new FileBackend();
    break;
  default:
    throw ArchiveException("No such BackendType.",__FILE__,__LINE__);
  }

  switch(et) {
  case BOOST_TEXT:
  case BOOST_XML:
  case BOOST_BINARY:
    this->saveEngine = new BoostSaveEngine();
    break;
  default:
    throw ArchiveException("No such EngineType.", __FILE__, __LINE__);
  }
}

CLISaveManager::~CLISaveManager() 
{
  delete saveEngine;
  delete backend;
}

Backend* CLISaveManager::getBackend() 
{
  return this->backend;
}

void CLISaveManager::save() 
{
  userState->getHistory().capture();
  userState->getMetaInfo().capture();
  userState->getTransClosure().capture();
  userState->newVersion();
  saveEngine->setOutputStream
    (
     backend->getOutputStream(userState, "UserState.ser"),
     this->et
     );
  saveEngine->save(userState);

  // Save meta information
  //
  ostream& meta_stream = backend->getOutputStream(userState, "config");
  meta_stream << engine_type_str[this->et] << endl;
  meta_stream.flush();

  // Clear any metainfo comment
  //
  userState->getMetaInfo().clearComment();
}
