#include "TestUserState.h"

BIE_CLASS_EXPORT_IMPLEMENT(TestUserState)

#include "StateHistory_Test.h"
#include "StateMetaInfo_Test.h"
#include "StateTransClosure_Test.h"

TestUserState::TestUserState
(string state_name, uint32 version, bool saving)
{
  setSession(state_name);
  setVersion(version);

  if (saving){
    this->tus_history      = new StateHistory_Test();
    this->tus_metainfo     = new StateMetaInfo_Test();
    this->tus_transclosure = new StateTransClosure_Test();
  } else {
    this->tus_history      = 0;
    this->tus_metainfo     = 0;
    this->tus_transclosure = 0;
 }
}

void TestUserState::addObject(Serializable* obj) {
  this->tus_transclosure->objectlist.push_back(obj);
}

void TestUserState::addObject(VectorM* obj) {
  this->tus_transclosure->v = obj;
}

void TestUserState::addObject(MatrixM* obj) {
  this->tus_transclosure->m = obj;
}

void TestUserState::addObject(Three_Vector* obj) {
  this->tus_transclosure->tv = obj;
}

void TestUserState::addObject(CVectorM* obj) {
  this->tus_transclosure->cv = obj;
}

void TestUserState::addObject(CMatrixM* obj) {
  this->tus_transclosure->cm = obj;
}

StateHistory& TestUserState::getHistory() 
{ return *tus_history; }
  
StateMetaInfo& TestUserState::getMetaInfo()
{ return *tus_metainfo; }

StateTransClosure_Test& TestUserState::getTransClosure()
{ return *tus_transclosure; }

