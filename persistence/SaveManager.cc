
#include "gvariable.h"
#include "SaveManager.h"
#include "FileBackend.h"
#include "PersistenceException.h"
#include "PersistenceControl.h"

BIE_CLASS_EXPORT_IMPLEMENT(SaveManager)

#include "StateTransClosure_Test.h"
#include "TestUserState.h"

SaveManager::SaveManager(UserState *userState, enum EngineType et, enum BackendType bt) {
  this->userState = userState;

  switch(bt) {
  case BACKEND_FILE:
    this->backend = new FileBackend();
    break;
  default:
    throw ArchiveException("No such BackendType.",__FILE__,__LINE__);
  }

  switch(et) {
  case BOOST_TEXT:
  case BOOST_XML:
  case BOOST_BINARY:
    this->saveEngine = new BoostSaveEngine(backend->getOutputStream(userState), et);
    break;
  default:
    throw ArchiveException("No such EngineType.", __FILE__, __LINE__);
  }
}

SaveManager::~SaveManager() 
{
  delete saveEngine;
  delete backend;
}

void SaveManager::save() {
  userState->getHistory().capture();
  userState->getMetaInfo().capture();
  saveEngine->save(userState);
}
