#include <ctime>
#include <iostream>
#include <iomanip>

#include <BIEmpi.h>
#include "StateMetaInfo_CLI.h"
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#include "PersistenceException.h"

BIE_CLASS_EXPORT_IMPLEMENT(StateMetaInfo_CLI)

StateMetaInfo_CLI::StateMetaInfo_CLI()
{
  asm("");
}
  
void StateMetaInfo_CLI::capture()
{
  // Get details of the time.
  time_t timer = time(NULL);
  localtime_r(&timer,&meta_tm);
  
  // Gets the name of the user driving the archiving pass.
  struct passwd * passwd = getpwuid(getuid());
  meta_username = passwd->pw_name;

  // Archive the hostname  
  u_int32_t hostnamebufferlen = 1024;
  char hostnamebuffer[hostnamebufferlen];

  if (gethostname(hostnamebuffer, hostnamebufferlen))
  { throw ArchiveException("Could not get hostname.", __FILE__,__LINE__); }

  meta_hostname = hostnamebuffer;
}

void StateMetaInfo_CLI::restore() 
{
  if (myid) return;

  cout << setw(40) << setfill('-') << '-' << setfill(' ') << endl;
  cout << setw(20) << left << "Creator"   << " : " << meta_username     << endl;
  cout << setw(20) << left << "Hostname"  << " : " << meta_hostname     << endl;
  cout << setw(20) << left << "Date/Time" << " : " << asctime(&meta_tm);
  if (meta_comment.size())
    cout << setw(20) << left << "Comment" << " : " << meta_comment      << endl;
  cout << setw(40) << setfill('-') << '-' << setfill(' ') << endl;
}
