#include <errno.h>
#include <iomanip>
#include <algorithm>

#include <time.h>

#include "config.h"
#include "gvariable.h"
#include "PersistenceControl.h"
#include "PersistenceException.h"
#include "CLIUserState.h"
#include "StateHistory_CLI.h"
#include "StateMetaInfo_CLI.h"
#include "CLISaveManager.h"
#include "CLILoadManager.h"
#include "CLICheckpointManager.h"
#include "gvariable.h"
#include "FileBackend.h"
#include "BIEMutex.h"

using namespace BIE;
using namespace std;

CLIUserState * PersistenceControl::userstate = NULL;
CLISaveManager * PersistenceControl::savemanager = NULL;
Backend * backend = new FileBackend();
bool need_new_session = true;

bool check_session(ostream & console) 
{
  if (need_new_session) {
    if (myid == 0) {
      console << "Warning:" << endl;
      console << "  Not in an active session." << endl;
      console << "  Create a new session using pnewsession command." << endl;
    }
    return true;
  } else {
    return false;
  }
}    

void PersistenceControl::pnewsession(string name, ostream& console) 
{
  pnewsession(name, persistType, console);
}

void PersistenceControl::pnewsession(string name, string archive_type,
                                     ostream& console) 
{
  EngineType etype = BOOST_BINARY;
  char skip_ckm = 0;
  if (myid == 0) {
    // check archive_type
    if (archive_type == "TEXT" || archive_type == "text") {
      etype = BOOST_TEXT;
    } else if (archive_type == "XML" || archive_type == "xml") {
      etype = BOOST_XML;
    } else if (archive_type == "BINARY" || archive_type == "binary"
               || archive_type == "BIN" || archive_type == "bin") {
      etype = BOOST_BINARY;
    } else {
      console << "Warning:\n";
      console << "Unrecognized archive type : " << archive_type << "\n";
      console << "Resorting to default type : BINARY" << endl;
    }
    vector<string> list;
    backend->listSessions(list);
    if (find(list.begin(), list.end(), name) != list.end()) {
      console << "Error: Specified session name already exists." << endl;
      skip_ckm = 1;
    }
  }

  // Share session existence with all nodes
  //
  MPI_Bcast(&skip_ckm, 1, MPI_CHAR, 0, MPI_COMM_WORLD);
  if (skip_ckm) return;

  // Make a new checkpoint manager
  //
  userstate = new CLIUserState(name, 0, true);
  savemanager = new CLISaveManager(userstate, etype, BACKEND_FILE);
  ckm = new CLICheckpointManager(*savemanager);
  need_new_session = false;
  if (myid == 0) 
    console << "Using " << engine_type_str[etype] << " archiver." << endl;
}

void PersistenceControl::psave(ostream & console) 
{
  if (check_session(console)) return;
  try {
    savemanager->save();
  } catch (BoostSerializationException * e) {
    console << "####### BoostSerializationException thrown! ########\n";
    console << "Printing persistence stack trace...\n";
    e->print(console);
    console << "####################################################\n";
    console << "Warning: Something went wrong.. Maybe some classes  \n";
    console << "         aren't persistence-ready?" << endl;
  }
}

void PersistenceControl::psave(string comment, ostream & console) 
{
  if (check_session(console)) return;
  try {
    cout << "Saving with comment: " << comment << endl;
    savemanager->setComment(comment);
    savemanager->save();
  } catch (BoostSerializationException * e) {
    console << "####### BoostSerializationException thrown! ########\n";
    console << "Printing persistence stack trace...\n";
    e->print(console);
    console << "####################################################\n";
    console << "Warning: Something went wrong.. Maybe some classes  \n";
    console << "         aren't persistence-ready?" << endl;
  }
}

void PersistenceControl::prestore(string sessionname, ostream & console) 
{
  // restore the latest archive
  prestore(sessionname, getLatest(sessionname), console);
}


void PersistenceControl::prestore(string sessionname, uint32 version, 
                                  ostream & console) 
{
  if (mpi_used) {
    MPI_Barrier(MPI_COMM_WORLD);
    time_t t = time(0);
    if (myid==0) cout << "Entering prestore ==> " << ctime(&t) << flush;
  }

  if (userstate != NULL) {
    delete userstate;
  }

  try {

    userstate = new CLIUserState(sessionname, version, false);
    userstate->setVersion(version);

    CLILoadManager loadManager(&userstate, BACKEND_FILE);

    try {
      loadManager.load();
    } catch (BoostSerializationException * e) {
      console << "####### BoostSerializationException thrown! ########\n";
      console << "Printing persistence stack trace...\n";
      e->print(console);
      console << "####################################################\n";
      console << "Warning: Something went wrong.. Maybe some classes  \n";
      console << "         aren't persistence-ready?" << endl;
    }
    
    if (myid == 0) {
#if PARANOID_MODE > 0
      console << "Note:" << endl
	      << "You have compiled BIE with paranoid-mode=yes and will\n"
	      << "have to start a new persistent session using pnewsession\n" 
	      << "before saving any new checkpoints" << endl;
#else
      console << "Note:" << endl
	      << "Consider starting a new persistent session using pnewsession.\n" 
	      << "Otherwise, any new checkpoint/psave save sets will overwrite\n" 
	      << "existing sets in the current session, " << sessionname 
	      << ", beginning at version " << version+1 << endl;
#endif
    }

    // If paranoid-mode is set, the persistence control must not be
    // allowed to write to the current session.
    //
#if PARANOID_MODE > 0
    need_new_session = true;
#else
    need_new_session = false;
#endif

    // Restore the internal savemanager from the saved checkpoint manager
    //
    savemanager = ckm->getCSM();
  }
  catch (ArchiveException* e) {
    if (myid==0)
      console << "####### ArchiveException thrown! ########" << std::endl
	      << e->getErrorMessage()                        << std::endl
	      << "#########################################" << std::endl;
  }

  if (mpi_used) {
    MPI_Barrier(MPI_COMM_WORLD);
    time_t t = time(0);
    if (myid==0) cout << "Exiting prestore ==> " << ctime(&t) << flush;
  }
  simulationInProgress = 0;
}

void PersistenceControl::pmetainfo(string sessionname, uint32 version, 
				   ostream & console)
{
  if (mpi_used) MPI_Barrier(MPI_COMM_WORLD);

  if (userstate != NULL) {
    delete userstate;
  }

  userstate = new CLIUserState(sessionname, version, false);
  userstate->setVersion(version);
  CLILoadManager loadManager(&userstate, BACKEND_FILE);
  try {
    loadManager.load_meta();
  } catch (BoostSerializationException * e) {
    console << "####### BoostSerializationException thrown! ########\n";
    console << "Printing persistence stack trace...\n";
    e->print(console);
    console << "####################################################\n";
    console << "Warning: Something went wrong.. Maybe some classes  \n";
    console << "         aren't persistence-ready?" << endl;
  }

  if (myid==0)
    console << setw(10) << left << userstate->getVersion()
	    << setw(40) << left << userstate->getMetaInfo().getComment()
	    << setw(20) << left 
	    << asctime(userstate->getMetaInfo().getCreationTime());
  
  if (mpi_used) MPI_Barrier(MPI_COMM_WORLD);
  simulationInProgress = 0;
}

void PersistenceControl::pmetainfo(string sessionname, ostream & console) 
{
  vector<uint32> list;
  if (!backend->listVersions(list, sessionname)) {
    console << "No such session found." << endl;
  } else {
    sort(list.begin(), list.end());

    for (vector<uint32>::iterator it=list.begin(); it!=list.end(); it++)
      pmetainfo(sessionname, *it, console);
  }
}



void PersistenceControl::phistory(string sessionname, int32 version, ostream & console) 
{
}

void PersistenceControl::pversions(string session, ostream & console) 
{
  if (myid) return;

  vector<uint32> list;
  if (!backend->listVersions(list, session)) {
    console << "No such session found." << endl;
  } else {
    sort(list.begin(), list.end());
    console << *(list.begin()) << " ~ " << *(list.rbegin()) << endl;
  }
}

void PersistenceControl::plist(ostream & console) 
{
  if (myid) return;

  vector<string> list;
  backend->listSessions(list);
  sort(list.begin(), list.end());
  vector<string>::const_iterator itr;
  for (itr = list.begin(); itr != list.end(); itr++) {
    console << *itr << endl;
  }
}

uint32 PersistenceControl::getLatest(string session) 
{
  vector<uint32> list;
  if(!backend->listVersions(list, session)) {
    // new session, starts with version number 1
    return 1;
  } else {
    sort(list.begin(), list.end());
    return *(--list.end());
  }
}

// CHECKPOINT stuff
void PersistenceControl::ckon(ostream & console) 
{
  if (check_session(console)) return;
  ckm->on();
  if (myid == 0)
    console << "Checkpointing: " << (ckm->getOnOff()==true?"On":"Off") << endl;
}

void PersistenceControl::ckoff(ostream & console) 
{
  ckm->off();
  if (myid == 0)
    console << "Checkpointing: " << (ckm->getOnOff()==true?"On":"Off") << endl;
}

void PersistenceControl::cktoggle(ostream & console) 
{
  if (check_session(console)) return;
  ckm->toggle();
  if (myid == 0)
    console << "Checkpointing: " << (ckm->getOnOff()==true?"On":"Off") << endl;
}

void PersistenceControl::ckinterval(int interval, ostream & console) 
{
  if (check_session(console)) return;
  ckm->setInterval(interval);
  if (myid == 0) {
    if (interval) {
      console << "Checkpointing every " << interval << " intervals." << endl;
    } else {
      console << "Checkpointing interval disabled." << endl;
    }
  }
}

void PersistenceControl::cktimer(int timer, ostream & console) 
{
  if (check_session(console)) return;
  ckm->setTimer(timer);
  if (myid == 0) {
    if (timer) {
      console << "Checkpointing every " << timer << " seconds." << endl;
    } else {
      console << "Checkpointing timer disabled." << endl;
    }
  }
}

void PersistenceControl::ckmanual(ostream & console) 
{
  if (check_session(console)) return;
  ckm->setCheckpointNow();
  if (myid == 0)
    console << "Checkpointing at next opportunity." << endl;
}

void PersistenceControl::ckcleanup() 
{
  delete userstate;
  delete savemanager;
  delete ckm;
}

