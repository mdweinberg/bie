#include "PersistenceException.h"

Persistence_NameInUseException::Persistence_NameInUseException
(string nameinuse, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "State Name In Use Exception";
  errormessage << "The name " << nameinuse << " is already being used.";
}

PersistenceDirException::PersistenceDirException
(string dir, int errnumber, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Persistence Directory Exception";
  
  errormessage << "Error creating or accessing directory: " << dir << ": ";
  errormessage << strerror(errnumber);
}

NoSuchUserStateException::NoSuchUserStateException
(string statename, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such User State Exception";
  errormessage << statename;
}

NoSuchUserStateVersionException::NoSuchUserStateVersionException
(string statename, uint32 stateversion, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such User State Version Exception";

  errormessage << "There is no version " << stateversion << " of " << statename;
}

ArchiveFileOpenException::ArchiveFileOpenException
(string filename, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Archive File Open Exception";
  errormessage << "Error opening: " << filename;
}

ArchiveException::ArchiveException
(string message, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Archive Exception";
  errormessage << message;
}

ArchiveException::ArchiveException
(string message, int errnumber, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Archive Exception";
  errormessage << message << strerror(errnumber);
}

NoSuchArchiveTypeException::NoSuchArchiveTypeException
(string message, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Archive Type Exception";
  errormessage << "Such archive type is not known";
}

NoSuchSessionException::NoSuchSessionException
(string session, string sourcefilename, int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber) {
  exceptionname = "No Such Session Exception";
  errormessage << "No Such Session exists: " << session;
}

NoSuchVersionException::NoSuchVersionException
(uint32 ver, string sourcefilename, int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber) {
  exceptionname = "No Such Version Exception";
  errormessage << "No Such Version exists: " << ver;
}

BoostSerializationException::BoostSerializationException(string msg,
                                                         string sourcefilename, 
                                                         int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber) {
  exceptionname = "Boost Serialization Exception";
  errormessage << "Exception occurred within boost::serialization library.";
  this->cause = NULL;
  this->msg = msg;
}

BoostSerializationException::BoostSerializationException(string msg,
                                                         string extra_msg,
                                                         string sourcefilename, 
                                                         int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber) {
  exceptionname = "Boost Serialization Exception";
  errormessage << "Exception occurred within boost::serialization library.";
  this->cause = NULL;
  this->msg = msg;
  this->extra_msg = extra_msg;
}

BoostSerializationException::BoostSerializationException(BoostSerializationException* cause,
                                                         string sourcefilename,
                                                         int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber) {
  exceptionname = "Boost Serialization Exception";
  errormessage << "Exception occurred within boost::serialization library.";
  this->cause = cause;
}

BoostSerializationException::BoostSerializationException(BoostSerializationException* cause,
                                                         string extra_msg,
                                                         string sourcefilename,
                                                         int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber) {
  exceptionname = "Boost Serialization Exception";
  errormessage << "Exception occurred within boost::serialization library.";
  this->cause = cause;
  this->extra_msg = extra_msg;
}

BoostSerializationException::~BoostSerializationException() {
  delete cause;
}

void BoostSerializationException::print(std::ostream& o) {
  o << sourcefilename << ":" << sourcelinenumber;
  if (extra_msg.length() > 0) {
    o << " : " << extra_msg;
  }
  o << endl;
  if (cause) {
    o << "--> ";
    cause->print(o);
  } else {
    o << this->msg << endl;
  }
}
