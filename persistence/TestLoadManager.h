// This is realy -*- C++ -*-

#ifndef TestLoadManager_h
#define TestLoadManager_h

#include "EngineConfig.h"
#include "TestUserState.h"
#include "Backend.h"
#include "BoostLoadEngine.h"

/**
   TestLoadManager: Deals with nitty-gritty bits of persistence mechanism.
   <ol>
   <li> file management - which file to store/restore objects to/from
   <li> DB management - PersistenceDB is used to keep track of versions of stored Objects
   </ol>
*/
class TestLoadManager 
{
public:
  //! Constructor
  TestLoadManager(TestUserState ** userstate, 
		  const std::string& session, const uint32 version,
		  enum EngineType t, enum BackendType bt);
  //! Destructor
  ~TestLoadManager();

  //! Requests that an archive be loaded
  void load();

 private:
  TestUserState **userState;
  Backend *backend;
  BoostLoadEngine *loadEngine;
};

#endif
