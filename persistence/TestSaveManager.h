// This is really -*- C++ -*-

#ifndef TestSaveManager_h
#define TestSaveManager_h

#include <fstream>

#include "EngineConfig.h"
#include "TestUserState.h"
#include "BoostSaveEngine.h"
#include "Backend.h"
#include "SaveManager.h"

/**
 TestSaveManager: Deals with nitty-gritty bits of persistence mechanism.
 <ol>
 <li> File management - which file to store/restore objects to/from
 <li> DB management - PersistenceDB is used to keep track of versions of stored Objects
 </ol>
  Actual serialization of the object itself is done using SaveEngine
*/
class TestSaveManager : public SaveManager 
{
 public:
  //! Construtor
  TestSaveManager(TestUserState *userState, 
		  enum EngineType et, enum BackendType bt);
  //! Destructor
  ~TestSaveManager();

  //! Request that the userState be saved
  void save();

 private:
  TestUserState *userState;
  Backend *backend;
  BoostSaveEngine *saveEngine;
  enum EngineType et;
  std::ostream *out;
};

#endif
