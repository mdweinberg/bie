#include "Serializable.h"

#include "gvariable.h"
#include "TestLoadManager.h"
#include "FileBackend.h"
#include "PersistenceException.h"
#include "PersistenceControl.h"

#include "StateTransClosure_Test.h"
#include "TestUserState.h"

TestLoadManager::TestLoadManager
(TestUserState **userState, const std::string& session, const uint32 version,
 enum EngineType et, enum BackendType bt) 
{
  this->userState = userState;

  switch(bt) {
  case BACKEND_FILE:
    this->backend = new FileBackend();
    break;
  default:
    throw NoSuchArchiveTypeException("",__FILE__,__LINE__);
  }

  switch(et) {
  case BOOST_TEXT:
  case BOOST_XML:
  case BOOST_BINARY:
    this->loadEngine = new BoostLoadEngine();
    break;
  default:
    throw NoSuchArchiveTypeException("", __FILE__, __LINE__);
  }

  loadEngine->setInputStream
    (backend->getInputStream(session, version, "UserState.ser"), et);
}

TestLoadManager::~TestLoadManager() 
{
  delete loadEngine;
  delete backend;
}

void TestLoadManager::load() 
{
  loadEngine->load(userState);
  (*userState)-> getHistory().restore();
  (*userState)->getMetaInfo().restore();
}
