// This is really -*- C++ -*-

#ifndef StateTransClosure_CLI_h
#define StateTransClosure_CLI_h

#include "AData.h"
#include "StateTransClosure.h"
#include "gvariable.h"

#include <unordered_map>
#include <boost/serialization/unordered_map.hpp>

/**
 StateTransClosure_CLI

 This will write a transitive closure of the objects from CLI using
 global and symbol table variables as the persistent roots.

*/
class StateTransClosure_CLI : public StateTransClosure 
{
  
public: 
  /// Empty constructor
  StateTransClosure_CLI() {}
 
  /// Empty destructor.
  virtual ~StateTransClosure_CLI () {}

  /** capture() puts global and local variables from the CLI table into
      respective containers here, ready to be serialized */
  void capture();

  /** restore() does exactly the opposite. It expects the variables to
   be loaded into the containers, and loads them back into the
   respective CLI table */
  void restore();

private:
  // The following two maps variable name to the corresponding Serializable* object
  std::unordered_map<std::string, AData*> globals;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    try {
      ar & BOOST_SERIALIZATION_NVP(globals);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION
    }
    try {
      ar & BOOST_SERIALIZATION_NVP(st);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION
    }
    try {
      ar & BOOST_SERIALIZATION_NVP(ckm);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION
    }  
  }
};

BIE_CLASS_TYPE_INFO(StateTransClosure_CLI)

#endif
