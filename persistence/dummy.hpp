#include <fstream>

#include <boost/archive/polymorphic_text_iarchive.hpp>
#include <boost/archive/polymorphic_binary_iarchive.hpp>
#include <boost/archive/polymorphic_xml_iarchive.hpp>
#include <boost/archive/polymorphic_text_oarchive.hpp>
#include <boost/archive/polymorphic_binary_oarchive.hpp>
#include <boost/archive/polymorphic_xml_oarchive.hpp>

#include <boost/serialization/vector.hpp>

void dummy(std::vector<Serializable*> &s)
{
  {
    // Make an archive
    //
    std::ofstream ofs("test.dat");
    boost::archive::polymorphic_binary_oarchive oa(ofs);

    // Store the pointers in the archive
    //
    oa << BOOST_SERIALIZATION_NVP(s);
    
  }

  {
    // Open the archive
    //
    std::ifstream ifs("test.dat");
    boost::archive::polymorphic_binary_iarchive ia(ifs);
    
    // Restore the objects from the archive
    //
    ia >> BOOST_SERIALIZATION_NVP(s);
  }
}


