/* This is where the registartion code for manually created persistent
   classes, i.e. ones where serialize() is written manually, are
   added. Don't forget to add corresponding headers to
   persistence/AllPersistenceHeaders_manual.h
*/
ar->register_type(static_cast<BIE::Serializable*>(NULL));
ar->register_type(static_cast<AData*>(NULL));
ar->register_type(static_cast<SymTab*>(NULL));
ar->register_type(static_cast<StateTransClosure_CLI*>(NULL));
ar->register_type(static_cast<Complex*>(NULL));
ar->register_type(static_cast<KComplex*>(NULL));
ar->register_type(static_cast<CLISaveManager*>(NULL));
ar->register_type(static_cast<Vector*>(NULL));
ar->register_type(static_cast<Three_Vector*>(NULL));
ar->register_type(static_cast<Matrix*>(NULL));
ar->register_type(static_cast<CVector*>(NULL));
ar->register_type(static_cast<CMatrix*>(NULL));
ar->register_type(static_cast<BIE::BIEACG*>(NULL));
