#include "AData.h"
#include "GlobalTable.h"
#include "CLICheckpointManager.h"
#include "gvariable.h"

#include "Tessellation.h"
#include "Distribution.h"

#include "StateTransClosure_CLI.h"
BIE_CLASS_EXPORT_IMPLEMENT(StateTransClosure_CLI)

void StateTransClosure_CLI::capture() 
{
  // Globals
  globals.clear();

  vector<string> global_names = GlobalTable::get_all_variable_names();
  
  for (auto v : global_names) {
    AData * value = new AData();
    GlobalTable::get_variable(v.c_str(), value);
    globals[v.c_str()] = value;
  }
  
  // Variables : these are stored directly by storing SymTab.
}

void StateTransClosure_CLI::restore() 
{
  //* Globals

  // globals is restored, now we have to put them in GlobalTable
  
  for (auto v : globals) {
    // dereferencing the iterator for hash_map gives you an object of type
    // std::pair<Key,Data>. With that you can access the Key as
    // v.first and the Data as v.second
    GlobalTable::set_variable(v.first.c_str(), v.second);
  }

  // Variables : Again, these are loaded directly by loading SymTab
}

