// This is really -*- C++ -*-

#ifndef PersistenceException_H
#define PersistenceException_H

#include <cc++/config.h>

#include "BIEException.h"

using namespace BIE;

/// Exception used to complain about a name already being used.
class Persistence_NameInUseException : public BIEException 
{
 public:
  /// Describes error, name involved in clash, and location of throw.
  Persistence_NameInUseException(string nameinuse, string sourcefilename, int sourcelinenumber);
};

/// Exception used when we cannot create a sub directory in the 
/// persistence data dir.
class PersistenceDirException : public BIEException 
{
 public:
  /// Reports name of sub directory, errno error, and location of throw.
  PersistenceDirException
    (string dir, int errnumber, string sourcefilename, int sourcelinenumber);
};

//! Can not open requested archive file
class ArchiveFileOpenException : public BIEException 
{
 public:
  //! Constructor
  ArchiveFileOpenException
    (string filename, string sourcefilename, int sourcelinenumber);
};

//! Requested archive does not exist
class ArchiveException : public BIEException 
{
 public:
  //! Constructor
  ArchiveException
    (string errormessage, string sourcefilename, int sourcelinenumber);
  
  //! Constructor
  ArchiveException
    (string errormessage, int errnumber, string sourcefilename, int sourcelinenumber);
};

/// Exception used when trying to restore an object that does not exist.
class NoSuchUserStateException : public BIEException 
{
 public:
  /// Reports name of state that doesn't exist, location of throw.
  NoSuchUserStateException
    (string statename, string sourcefilename, int sourcelinenumber);
};

/// Exception used when trying to restore an object version that does not exist.
class NoSuchUserStateVersionException : public BIEException {
 public:
  /// Reports name of State that doesn't exist, location of throw.
  NoSuchUserStateVersionException
    (string statename, uint32 stateversion, string sourcefilename, int sourcelinenumber);
};

/// Exception used when trying to instantiate an unknown archive type
class NoSuchArchiveTypeException : public BIEException 
{
 public:
  /// Pass along a message about the archive type that doesn't exist, location of throw.
  NoSuchArchiveTypeException(string errormessage, string sourcefilename, int sourceline);
};

//! User has requested a session that does not exist
class NoSuchSessionException : public BIEException 
{
 public:
  //! Pass along session name and the location of throw.
  NoSuchSessionException(string session, string sourcefilename, int sourceline);
};

//! The archive does not correpsond to known version
class NoSuchVersionException : public BIEException 
{
 public:
  //! Pass along the unknown version and location of the throw
  NoSuchVersionException(uint32 ver, string sourcefilename, int sourceline);
};

//! Exception thrown when the serialization fails
class BoostSerializationException : public BIEException 
{
 public:
  //! for the initial instance for wrapping boost::archive archive_exception
  BoostSerializationException(string msg, string sourcefilename, int sourceline);
  //! Include an extra message along with the default message and location
  BoostSerializationException(string msg, string extra_msg,
                              string sourcefilename, int sourceline);
  //! Pass along the exception itself and the throw locdation
  BoostSerializationException(BoostSerializationException* cause,
                              string sourcefilename, int sourceline);
  //! Pass along the exception itself, a message, and the throw locdation
  BoostSerializationException(BoostSerializationException* cause, string extra_msg,
                              string sourcefilename, int sourceline);
  
  //! Destructor
  ~BoostSerializationException();
  
  //! Print the serialization traceback
  void print(std::ostream& o);
  
 private:
  BoostSerializationException* cause;
  string msg;
  string extra_msg;
};
#endif
