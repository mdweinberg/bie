// This is really -*- C++ -*-

#ifndef FileBackend_h
#define FileBackend_h

#include "gvariable.h"
#include "Backend.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

/**
   FileBackend: Implements file-backed backend.  
   <ul>
   <li> Sessions are stored in the following format
   \<dir_prefix\>/\<session_name\>/\<version\>/\<files...\>
   <li> Note: Uses boost::filesystem in order to access directory/files in
        OS-independent, portable way
   </ul>
 */
class FileBackend : virtual public Backend 
{
 public:
  //! Null constructor
  FileBackend();
  //! Constructor with directory prefix (see note above)
  FileBackend(char *dir_prefix);
  //! Destructor
  ~FileBackend();

  //! List available sessions
  void listSessions(std::vector<std::string>& list);

  //! List available versions for a particular session
  bool listVersions(std::vector<uint32>& list, const std::string& session);

  //! Get an input stream for a given user state and filename
  std::istream& getInputStream(const UserState* userState,
                               const std::string& filename);

  //! Get an input stream for a desired session, version and filename
  std::istream& getInputStream(const std::string& session, uint32 version,
                               const std::string& filename);

  //! Get an output stream for a given user state and filename
  std::ostream& getOutputStream(const UserState* userState,
                                const std::string& filename);

  //! Get an output stream for a desired session, version and filename
  std::ostream& getOutputStream(const std::string& session, uint32 version,
                                const std::string& filename);

 private:
  bool find(const boost::filesystem::path& dir_path,     // in this directory
            const std::string& file_name,                // search for this name
            bool dir);                                   // search for dir?

  boost::filesystem::path dir_prefix;

  boost::filesystem::ifstream *istream;
  boost::filesystem::ofstream *ostream;
};
#endif
