#include "Serializable.h"

#include "gvariable.h"
#include "CLICheckpointManager.h"
#include "CLILoadManager.h"
#include "FileBackend.h"
#include "PersistenceException.h"
#include "PersistenceControl.h"

#include "StateTransClosure_Test.h"
#include "CLIUserState.h"

// For clivector classes
#include "Tessellation.h"
#include "Distribution.h"

CLILoadManager::CLILoadManager(CLIUserState **userState, enum BackendType bt) 
{
  this->userState = userState;

  switch(bt) {
  case BACKEND_FILE:
    this->backend = new FileBackend();
    break;
  default:
    throw ArchiveException("No such BackendType.",__FILE__,__LINE__);
  }

  // find out the type from meta info
  istream& meta_stream = backend->getInputStream(*userState, "config");
  string engine_type;
  meta_stream >> engine_type;
  if (engine_type == "BOOST_TEXT") {
    this->et = BOOST_TEXT;
  } else if (engine_type == "BOOST_XML") {
    this->et = BOOST_XML;
  } else if (engine_type == "BOOST_BINARY") {
    this->et = BOOST_BINARY;
  } else {
    throw ArchiveException("No such EngineType.", __FILE__, __LINE__);
  }

  switch(this->et) {
  case BOOST_TEXT:
  case BOOST_XML:
  case BOOST_BINARY:
    this->loadEngine = new BoostLoadEngine();
    break;
  default:
    throw ArchiveException("No such EngineType.", __FILE__, __LINE__);
  }
}

CLILoadManager::~CLILoadManager() 
{
  delete loadEngine;
  delete backend;
}

void CLILoadManager::load() 
{
  loadEngine->setInputStream
    (backend->getInputStream(*userState, "UserState.ser"), this->et);
  loadEngine->load(userState);
  (*userState)->getHistory().restore();
  (*userState)->getMetaInfo().restore();
  (*userState)->getTransClosure().restore();
}

void CLILoadManager::load_meta() 
{
  loadEngine->setInputStream
    (backend->getInputStream(*userState, "UserState.ser"), this->et);
  loadEngine->load(userState);
}
