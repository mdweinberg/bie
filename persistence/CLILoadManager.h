// This is really -*- C++ -*-

#ifndef CLILoadManager_h
#define CLILoadManager_h

#include <fstream>

#include "EngineConfig.h"
#include "CLIUserState.h"
#include "BoostLoadEngine.h"
#include "Backend.h"

/**
   CLILoadManager: Deals with nitty-gritty bits of persistence mechanism.
   <ul>
   <li> file management - which file to store/restore objects to/from
   <li> DB management - PersistenceDB is used to keep track of versions of stored Objects
   </ul>
   Actual serialization of the object itself is done using LoadEngine
*/
class CLILoadManager {
 public:
  //! Consructor
  CLILoadManager(CLIUserState **userState, enum BackendType bt);

  //! Destructor
  ~CLILoadManager();

  //! The member responsible for restoring the serialized archive
  void load();
  
  /** The member responsible for restoring the metadata from the
      serialized archive */
  void load_meta();
  
 private:
  CLIUserState **userState;
  Backend *backend;
  enum EngineType et;
  enum BackendType bt;
  // Refer LoadEngine.h for the reason why this is BoostLoadEngine
  // instead of LoadEngine
  BoostLoadEngine *loadEngine;
};
#endif
