#include <iomanip>

#include "TestCheckpointManager.h"

// class TestCheckpointManager : public CheckpointManager {
//  public:
//   TestCheckpointManager();
//   ~TestCheckpointManager();

//   void checkpoint();
//   void setTimer(uint64 milliseconds);
//   void setInterval(uint32 iterations);
//   void setCheckpointNow();
// };

TestCheckpointManager::TestCheckpointManager(TestSaveManager &tsmin) : CheckpointManager(tsmin) {
  tsm = &tsmin;

  itr_on = false;
  itr_interval = 0;
  itr_count = 0;

  timer_on = false;
  timer_interval = 0;
  timer_deadline = 0;

  now = false;
}

TestCheckpointManager::~TestCheckpointManager() {
  /* nothing to do */
}

void TestCheckpointManager::checkpoint(std::ostream &out) {
  if (itr_on) {
    itr_count--;
  }

  out << setfill('-') << setw(79) << "" << setfill(' ') << endl;
  out << setw(16) << "itr_on: " << ((itr_on==0)?"no":"yes") << endl;
  if (itr_on) {
    out << setw(16) << " " << itr_count << " out of " << itr_interval;
    out << "left" << endl;
  }

  out << setw(16) << "timer_on: " << ((timer_on==0)?"no":"yes") << endl;
  if (timer_on) {
    out << setw(16) << " " << timer_deadline - time(NULL);
    out << " seconds left until next checkpoint." << endl;
  }

  out << setw(16) << "now: " << ((now==0)?"false":"true") << endl;

  // out << itr_count << " " << time(NULL) << " " << now << endl;

  bool takeit = false;
  // check every criteria and make sure to reset
  if (itr_on && itr_count == 0) {
    itr_count = itr_interval;
    takeit = true;
  }
  if (timer_on && time(NULL) > timer_deadline) {
    takeit = true;
    timer_deadline = time(NULL) + timer_interval;
  }
  if (now) {
    now = false;
    takeit = true;
  }
      
  if (takeit) {
    // trigger a checkpoint
    out << setw(8) << "" << "CHECKPOINTING." << endl;
    tsm->save();
  }
  out << setfill('-') << setw(79) << "" << setfill(' ') << endl;
}

void TestCheckpointManager::setInterval(uint64 iterations) {
  itr_on = true;
  itr_interval = iterations;
  itr_count = itr_interval;
}

void TestCheckpointManager::setTimer(time_t seconds) {
  timer_on = true;
  timer_interval = seconds;
  timer_deadline = time(NULL) + timer_interval;
}

void TestCheckpointManager::setCheckpointNow() {
  now = true;
}
