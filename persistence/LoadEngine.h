// This is really -*- C++ -*-

#ifndef LoadEngine_h
#define LoadEngine_h

#include <iostream>
#include "Serializable.h"

//! Abstract class for loading an archive
class LoadEngine 
{
 public:
  //! Constructor
  LoadEngine();
  //! Destructor
  virtual ~LoadEngine();
  /** This needs to be overridded by a derived class, of course, since
      it can't be virtual
  */
  template<class T> 
    void load(T** obj) {};
};

#endif
