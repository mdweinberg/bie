#include "TestSaveManager.h"
#include "FileBackend.h"
#include "StateTransClosure_Test.h"

TestSaveManager::TestSaveManager(TestUserState *userState,
				 enum EngineType et, enum BackendType bt) 
{
  this->userState = userState;
  this->et = et;

  switch(bt) {
  case BACKEND_FILE:
    this->backend = new FileBackend();
    break;
  default:
    throw ArchiveException("No such BackendType.",__FILE__,__LINE__);
  }

  switch(et) {
  case BOOST_TEXT:
  case BOOST_XML:
  case BOOST_BINARY:
    this->saveEngine = new BoostSaveEngine();
    break;
  default:
    throw ArchiveException("No such EngineType.", __FILE__, __LINE__);
  }

  userState->newVersion();

  saveEngine->setOutputStream
    (backend->getOutputStream(userState, "UserState.ser"), this->et);
}

TestSaveManager::~TestSaveManager() 
{
  delete saveEngine;
  delete backend;
}

void TestSaveManager::save() 
{
  // userState->setVersion(userState->getVersion()+1);
  userState->getHistory().capture();
  userState->getMetaInfo().capture();
  saveEngine->save(userState);
}
