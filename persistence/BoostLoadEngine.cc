#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "EngineConfig.h"
#include "PersistenceException.h"
#include "BoostLoadEngine.h"


using namespace std;

BoostLoadEngine::BoostLoadEngine() 
{
  arB = NULL;
  arT = NULL;
  arX = NULL;
}

void BoostLoadEngine::setInputStream(istream& i, EngineType t) 
{
  if (arB) delete arB;
  if (arT) delete arT;
  if (arX) delete arX;
  
  switch(t) {
  case BOOST_TEXT:
    arT = new boost::archive::text_iarchive(i);
    break;
  case BOOST_XML:
    arX = new boost::archive::xml_iarchive(i);
    break;
  case BOOST_BINARY:
    arB = new boost::archive::binary_iarchive(i);
    break;
  default:
    throw NoSuchArchiveTypeException("",__FILE__,__LINE__);
  }
}

BoostLoadEngine::~BoostLoadEngine() 
{
  delete arB;
  delete arT;
  delete arX;
}
