// This is really -*- C++ -*-

#ifndef Backend_h
#define Backend_h

#include <string>
#include <cc++/config.h>

#include <boost/serialization/vector.hpp>

#include "UserState.h"

/**
 Backend: Virtual class for different types of backends
 <ul>
 <li> File - stores different versions in different files
 <li> Subversion - Uses subversion
 </ul>
*/
class Backend {
public:
  //! Constructor
  Backend() {};
  //! Destructor
  virtual ~Backend() {};

/*   virtual void setSession(const std::string session) = 0; */
/*   virtual std::string getSession() = 0; */
/*   virtual uint32 makeNewVersion() = 0; */
/*   virtual void setVersion(uint32 ver) = 0; */
/*   virtual uint32 getVersion() = 0; */

  //! Return a list of the available sessions
  virtual void listSessions(std::vector<std::string>& list) = 0;

  //! Return a list of versions for a session
  virtual bool listVersions(std::vector<uint32>& list,
                            const std::string& session) = 0;

  //! Get an input stream for a given user state and filename
  virtual std::istream& getInputStream(const UserState* userState,
                                       const std::string& filename) = 0;

  //! Get an input stream for a desired session, version and filename
  virtual std::istream& getInputStream(const std::string& session,
                                       uint32 version,
                                       const std::string& filename) = 0;

  //! Get an output stream for a given user state and filename
  virtual std::ostream& getOutputStream(const UserState* userState,
                                        const std::string& filename) = 0;

  //! Get an output stream for a desired session, version and filename
  virtual std::ostream& getOutputStream(const std::string& session,
                                        uint32 version,
                                        const std::string& filename) = 0;
};
#endif
