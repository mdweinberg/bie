#ifndef EngineConfig_h
#define EngineConfig_h

#include <cc++/config.h>
#include <string>

enum EngineType { BOOST_TEXT = 0, BOOST_XML, BOOST_BINARY };
extern std::string engine_type_str[3];
enum BackendType { BACKEND_FILE /*, BACKEND_SVN */ };
extern const char* persistence_dir;

#endif
