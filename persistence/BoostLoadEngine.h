#ifndef BoostLoadEngine_h
#define BoostLoadEngine_h

#include "EngineConfig.h"
#include "LoadEngine.h"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>

//! A load engine using the boost serialization backend
class BoostLoadEngine : public LoadEngine 
{
 public:
  //! Constructor
  BoostLoadEngine();

  //! Destructor
  ~BoostLoadEngine();

  //! Assign the input stream for the archive
  void setInputStream(std::istream& i, enum EngineType t);
  
  //! The load function which will do all of the work
  template <class T> 
    void load(T ** obj) 
    {
      if (arB) {
	(*arB) >> BOOST_SERIALIZATION_NVP(*obj);
	delete arB; arB = 0;
      }
      if (arT) {
	(*arT) >> BOOST_SERIALIZATION_NVP(*obj);
	delete arT; arT = 0;
      }
      if (arX) {
	(*arX) >> BOOST_SERIALIZATION_NVP(*obj);
	delete arX; arX = 0;
      }
    }

 private:
  boost::archive::binary_iarchive* arB;
  boost::archive::text_iarchive*   arT;
  boost::archive::xml_iarchive*    arX;
};

#endif
