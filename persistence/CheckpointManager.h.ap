// -*- C++ -*-

#ifndef CHECKPOINTMANAGER_H
#define CHECKPOINTMANAGER_H

#include <iostream>
#include <ctime>

#include "SaveManager.h"

@include_persistence

//! Class to manage checkpoint calls
class @persistent_root(CheckpointManager) 
{
 public:
  //! Construct with the SaveManager
  CheckpointManager(SaveManager &sm) {}
  //! Destructor
  virtual ~CheckpointManager() {};
  //! Dump a checkpoint to the ostream "out"
  virtual void checkpoint(std::ostream &out) = 0;
  //! Set checkpoint interval in MCMC step iterations
  virtual void setInterval(uint64 iterations) = 0;
  //! Set checkpoint interval in seconds
  virtual void setTimer(time_t seconds) = 0;
  //! Take a checkpoint now!
  virtual void setCheckpointNow() = 0;

  @persistent_end
};
#endif
