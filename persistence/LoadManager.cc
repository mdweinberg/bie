// #include "AllPersistentHeaders.h"

#include "Serializable.h"

#include "gvariable.h"
#include "LoadManager.h"
#include "FileBackend.h"
#include "PersistenceException.h"
#include "PersistenceControl.h"

#include "StateTransClosure_Test.h"
#include "TestUserState.h"

LoadManager::LoadManager(UserState *userState, enum EngineType et, enum BackendType bt) {
  this->userState = userState;

  switch(bt) {
  case BACKEND_FILE:
    this->backend = new FileBackend();
    break;
  default:
    throw ArchiveException("No such BackendType.",__FILE__,__LINE__);
  }

  switch(et) {
  case BOOST_TEXT:
  case BOOST_XML:
  case BOOST_BINARY:
    this->loadEngine = new BoostLoadEngine(backend->getInputStream(userState), et);
    break;
  default:
    throw ArchiveException("No such EngineType.", __FILE__, __LINE__);
  }
}

LoadManager::~LoadManager() {
  delete loadEngine;
  delete backend;
}

