#include "StateMetaInfo_Test.h"

BIE_CLASS_EXPORT_IMPLEMENT(StateMetaInfo_Test)

StateMetaInfo_Test::StateMetaInfo_Test()
{
}
  
void StateMetaInfo_Test::capture() {
  /// Make everything consistent so that we can use regression testing.
  memset(&meta_tm,0,sizeof(meta_tm));
  meta_username = "magic";
  meta_hostname = "magic2";
}

void StateMetaInfo_Test::restore() {
  //  memset(&meta_tm,0,sizeof(meta_tm));
  //meta_username = "loaded username";
  //meta_hostname = "loaded hostname";
}
