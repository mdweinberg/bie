#include <map>
#include <string>
#include <boost/assign/list_of.hpp>

#include "structs.h"

//
// Initializes default registry for default fitpars parameters
//
ModelRegistry::ModelRegistry()
{

  //------------------------------------------------------------
  // SINGLE
  //------------------------------------------------------------

  {
    const std::string stanza("Single");
    const std::map<int, double> vals = boost::assign::map_list_of (4, 1.0);
    const std::set<int> fixd;
    const std::map<int, std::string> labs = 
      boost::assign::map_list_of (0, "X") (1, "Y") (2, "mag") (3, "r_half") 
      (4, "axis_ratio") (5, "Sersic_n") (6, "pos_ang");
    
    create(7, stanza, vals, labs, fixd);
  }


  //------------------------------------------------------------
  // BULGEDISK
  //------------------------------------------------------------
  {
    const std::string stanza("BulgeDisk");
    const std::map<int, double> vals = boost::assign::map_list_of (7, 1.0);
    const std::set<int> fixd;
    const std::map<int, std::string> labs = boost::assign::map_list_of 
      (0, "X") (1, "Y") (2, "mag") (3, "B/T") (4, "r_half")
      (5, "R_d/R_b") (6, "Sersic_n") (7, "bulge_axis_rat")
      (8, "disk_axis_rat") (9, "bulge_pos_ang") (10, "disk_pos_ang")
      (11, "sky_val");

    create(12, stanza, vals, labs, fixd);
  }
  //------------------------------------------------------------
  // PSF
  //------------------------------------------------------------
  {
    const std::string stanza("PSF");
    const std::map<int, double> vals;
    const std::map<int, std::string> labs = 
      boost::assign::map_list_of (0, "X") (1,"Y") (2, "mag");
    const std::set<int> fixd;

    create(3, stanza, vals, labs, fixd);
  }

  //------------------------------------------------------------
  // SKY
  //------------------------------------------------------------
  {
    const std::string stanza("Sky");
    const std::map<int, double> vals;
    const std::map<int, std::string> labs = 
      boost::assign::map_list_of (0, "sky") (1, "dSky/dx") (2, "dSky/dy");
    const int fixed[] = {1, 2};
    const std::set<int> fixd (fixed, fixed+2);

    create(3, stanza, vals, labs, fixd);
  }

  //------------------------------------------------------------
}
