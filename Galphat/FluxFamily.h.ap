// -*- C++ -*-
// Define the abstract interface for a general flux model


#ifndef _FluxFamily_h
#define _FluxFamily_h

#include <vector>

// Yay for boost!!
//
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>

// For integrating flux in a pixel
//
#include <QuadTreeIntegrate.H>

#include <FluxInterp.h>
#include <BIEmpi.h>

namespace pt = boost::property_tree;

using namespace std;

@include_persistence

namespace BIE { 

  namespace Galphat {
    
    /** 
	@brief mapping class
    */
    class @persistent_root(Mapping)
    {
    public:
      std::string @autopersist(ID);
      
      double @ap(xMin), @ap(xMax), @ap(Rmin), @ap(Rmax), @ap(P);
      
      //! Constructor
      Mapping() : ID("Mapping"), 
	xMin(0.0), xMax(1.0), Rmin(0.0), Rmax(1.0), P(0.0) {}
      
      //! Mapping factory
      static boost::shared_ptr<Mapping>
	create(std::string name, double param, double rmin, double rmax);

      //! Destructor
      ~Mapping() {}

      //! Map radius (R) to unit interval (x)
      virtual double xi(double R) = 0;
      
      //! Map unit interval (x) to radius (R)
      virtual double invxi(double x) = 0;
      
      //! Jacobian: dR/dx
      virtual double invxi2(double x) = 0;
      
      //! Minimum value in unit interval
      virtual double xmin() { return xMin; }
      
      //! Maxamum value in unit interval
      virtual double xmax() { return xMax; }
      
      //! Set parameter
      virtual void setP(double p) { P = p; }
      
      //! Get parameter
      virtual double getP() { return P; }
      
      //! Minimum radius
      virtual double rmin() { return Rmin; }
      
      //! Maximum radius
      virtual double rmax() { return Rmax; }
      
      //! Write mapping to cache
      virtual void writeCache(std::ostream& out) 
      {
	std::shared_ptr<char> cbuf(new char [20]);
	strncpy(cbuf.get(), ID.c_str(), 20);
	
	out.write((const char*)cbuf.get(), 20);
	out.write((const char*)&xMin,      sizeof(double));
	out.write((const char*)&xMax,      sizeof(double));
	out.write((const char*)&Rmin,      sizeof(double));
	out.write((const char*)&Rmax,      sizeof(double));
	out.write((const char*)&P,         sizeof(double));
      }
      
      //! Read mapping from cache
      virtual bool badCache(std::istream& in)
      {
	const double TOL = 1.0e-8;
	double xmin, xmax, rmin, rmax, pp;
	
	std::shared_ptr<char> cbuf(new char [20]);
	in.read(cbuf.get(), 20);
	
	if (ID.compare(cbuf.get()) != 0) {
	  std::cerr << "Mapping: found <" << cbuf 
		    << "> but expected <" << ID << ">" 
		    << std::endl;
	  return true;
	}
	
	in.read((char*)&xmin, sizeof(double));
	in.read((char*)&xmax, sizeof(double));
	in.read((char*)&rmin, sizeof(double));
	in.read((char*)&rmax, sizeof(double));
	in.read((char*)&pp,   sizeof(double));
	
	if (in.bad()) {
	  std::cerr << "Mapping: parameter mismatch"
		    << std::endl;
	  return true;
	}
	
	bool bad = false;
	if (fabs(xmin - xMin) > TOL) bad = true;
	if (fabs(xmax - xMax) > TOL) bad = true;
	if (fabs(rmin - Rmin) > TOL) bad = true;
	if (fabs(rmax - Rmax) > TOL) bad = true;
	if (fabs(pp   - P   ) > TOL) bad = true;
	
	return bad;
      }
      
      @persistent_end
    };
    
    /**
       Power law: 
       \f{eqnarray*}
          x     &=& [R^a - R_s^a] / [R_m^a - R_s^a] = [R^a - f_1]/f_2 \\
	  f_1   &=& R_s^a \\
	  f_2   &=& R_m^a - R_s^a \\
          R     &=& (f_1 + x f_2)^{1/a} \\
          dR/dx &=& f2/a (f_1 + x f_2)^{1/a-1}
       \f}

       For \f$R_s=0, R_m=1: f_1 = 0, f_2 = 1\f$
       \f{eqnarray*}
          x     &=& R^a \\
	  R     &=& x^{1/a} \\
	  dR/dx &=& x^{(1/a-1)/a}
       \f}
    */
    class @persistent(PowerMap): public @super(Mapping)
    {
    private:
      double @autopersist(f1), @autopersist(f2);
      
    public:
      
      //! Constructor
      PowerMap(double expon, double rmin=0.0, double rmax=1.0)
	{
	  ID    = "PowerMap";
	  Rmin  = rmin;
	  Rmax  = rmax;
	  P     = expon;
	  f1    = pow(Rmin, P);
	  f2    = pow(Rmax, P) - f1;
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	return (pow(R, P) - f1)/f2;
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	return pow(f1 + f2*x, 1.0/P);
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	return f2/P*pow(f1 + f2*x, (1.0-P)/P);
      }
      
      @persistent_end
    };
    
    class @persistent(LogMap) : public @super(Mapping)
    {
    private:
      double @autopersist(gam);
      
    public:
      
      //! Constructor
      LogMap(double rscl, double rm)
	{
	  ID    = "LogMap";
	  Rmax  = rm;
	  P     = rscl;
	  gam   = 1.0/log(1.0 + Rmax/P);
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	R = std::max<double>(R, 0.0 );
	R = std::min<double>(R, Rmax);
	return gam*log(1.0 + R/P);
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, 1.0);
	return P*(exp(x/gam) - 1.0);
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, 1.0);
	return P*exp(x/gam)/gam;
      }
      
      @persistent_end
    };
    
    
    class @persistent(ExpMap) : public @super(Mapping)
    {
    private:
      
      double @autopersist(bet), @autopersist(gam);
      
    public:
      
      //! Constructor
      ExpMap(double rs, double rm)
	{
	  ID   = "ExpMap";
	  P    = rs;
	  Rmax = rm;
	  bet  = exp(-Rmax/P);
	  gam  = 1.0 - bet;
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	R = std::max<double>(R, 0.0 );
	R = std::min<double>(R, Rmax);
	return (1.0 - exp(-R/P))/gam;
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, 1.0);
	return std::min<double>(-P*log(1.0 - x*gam), Rmax);
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, 1.0);
	return P*gam/(1.0 - x*gam);
      }
      
      @persistent_end
    };
    
    class @persistent(ExpPowMap) : public @super(Mapping)
    {
    private:
      
      double @autopersist(bet), @autopersist(gam);
      
    public:
      
      //! Constructor
      ExpPowMap(double expon, double rm)
	{
	  ID   = "ExpPowMap";
	  Rmax = rm;
	  P    = 1.0/expon;
	  bet  = exp(-pow(Rmax, P));
	  gam  = 1.0 - bet;
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	R = std::max<double>(R, 0.0 );
	R = std::min<double>(R, Rmax);
	return (1.0 - exp(-pow(R, P)))/gam;
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, 1.0);
	return std::min<double>(pow(-log(1.0 - x*gam), 1.0/P), Rmax);
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, 1.0);
	double f = std::min<double>(-log(1.0 - x*gam), pow(Rmax, P));
	return pow(f, 1.0/P - 1.0) * gam/(1.0 - x*gam);
      }
      
      @persistent_end
    };
    
    
    class @persistent(R1Map) : public @super(Mapping)
    {
    private:
      
      double @autopersist(xmx);
      
    public:
      
      //! Constructor
      R1Map(double rm)
	{
	  ID   = "R1Map";
	  Rmax = rm;
	  xmx  = Rmax/sqrt(1.0 + Rmax*Rmax);
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	R = std::max<double>(R, 0.0 );
	R = std::min<double>(R, Rmax);
	return R/sqrt(1.0 + R*R);
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return x/sqrt(1.0 - x*x);
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return pow(1.0 - x*x, -1.5);
      }
      
      @persistent_end
    };
    
    
    class @persistent(R2Map) : public @super(Mapping)
    {
    private:
      
      double @autopersist(xmx);
      
    public:
      
      //! Constructor
      R2Map(double rm)
	{
	  ID   = "R2Map";
	  Rmax = rm;
	  xmx  = Rmax*Rmax/(1.0 + Rmax*Rmax);
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	R = std::max<double>(R, 0.0);
	return R*R/(1.0 + R*R);
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return sqrt(x/(1.0 - x));
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return 0.5*pow(x,-0.5)*pow(1.0 - x, -1.5);
      }
      
      @persistent_end
    };
    
    class @persistent(RA1Map) : public @super(Mapping)
    {
    private:
      
      double @autopersist(xmx);
      
    public:
      
      //! Constructor
      RA1Map(double expon, double rm)
	{
	  ID   = "RA1Map";
	  Rmax = rm;
	  P    = expon;
	  xmx = pow(Rmax, P)/(1.0 + pow(Rmax, P));
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	R = std::max<double>(R, 0.0);
	double q = pow(R, P);
	return q/(1.0 + q);
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return pow(x/(1.0 - x), 1.0/P);
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return pow(x/(1.0 - x), 1.0/P)/(P*x*(1.0 - x));
      }
      
      @persistent_end
    };
    
    class @persistent(RA2Map) : public @super(Mapping)
    {
    private:
      
      double @autopersist(xmx);
      
    public:
      
      //! Constructor
      RA2Map(double p, double rm)
	{
	  ID   = "RA2Map";
	  Rmax = rm;
	  P    = p;
	  xmx  = pow(Rmax, P)/sqrt(1.0 + pow(Rmax, 2*P));
	}
      
      //! Map radius (R) to unit interval (x)
      double xi(double R)
      {
	R = std::max<double>(R, 0.0);
	double q = pow(R, P);
	return q/sqrt(1.0 + q*q);
      }
      
      //! Map unit interval (x) to radius (R)
      double invxi(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return pow(x/sqrt(1.0 - x*x), 1.0/P);
      }
      
      //! Jacobian: dR/dx
      double invxi2(double x)
      {
	x = std::max<double>(x, 0.0);
	x = std::min<double>(x, xmx);
	return pow(x/sqrt(1.0 - x*x), 1.0/P)/(P*x*(1.0 - x*x));
      }
      
      @persistent_end
    };
    
    typedef boost::shared_ptr<Mapping> mPtr;
    
    //! Mapping factory
    mPtr createMap(std::string name, double param, double rmin, double rmax);
    
    //! For integrating flux over a pixel
    class @persistent_root(DVfunctor2d)
    {
    public:
      
      DVfunctor2d() {}
      
      virtual std::vector<double> operator()(double, double) = 0;
      
      @persistent_end
    };
    
    class @persistent(FluxFamily) : public @super(DVfunctor2d)
    {
    protected:
      std::string @ap(ID);

      bool @ap(inCons);

      std::vector<double> @ap(N);
      double @ap(r0), @ap(q0), @ap(x0), @ap(y0), @ap(phi0), @ap(p0);
      
      virtual void compute_norm() {}
      
      //! Normalization from integration
      bool @autopersist(normI);

      //! Normalize all higher order terms to zero
      bool @autopersist(normZ);

    public:
      
      //@{
      //! Currently valid model types
      static std::set<std::string> valid_families;
      static std::set<std::string>::iterator vf_end;
      //@}
      
      //! Factory constructor
      static boost::shared_ptr<FluxFamily> factory(pt::ptree& tree);
      
      //! Initialize based on input parameters
      virtual void Initialize() = 0;
      
      //! Set parameters
      virtual void set(const std::vector<double>& P, double P0=1.0,
		       double scale=1.0, double ratio=1.0,
		       double xcen=0.0, double ycen=0.0, double phi=0.0)
      {
	N      = P;
	p0     = P0;
	r0     = scale;
	q0     = ratio;
	x0     = xcen;
	y0     = ycen;
	phi0   = phi;
	
	if (!inCons) Initialize();
      }
      
      //! Default constructor
      FluxFamily() : normI(false), normZ(false) {}

      //! Destructor
      virtual ~FluxFamily() {}
      
      //! Evaluates the surface density for the model as a function of radius
      virtual double density(double r) = 0;
      
      //! Evaluates the surface density for the model for a given coordinate x, y
      virtual std::vector<double> operator()(double x, double y) = 0;
      
      /** The surface density integrated over the with lower right and upper left 
	  corners (a0, b0) and (a1, b1)
      */
      virtual vector<double> pixel(double a0, double a1, double b0, double b1, 
				   double eps=1.0e-4, int maxlev=10);
      
      //! Returns the normalization
      virtual double getNorm() { return 1.0; }
      
      //! Get flux in a pixel using the flux interpolation class
      virtual double getPixel(double X0, double X1, double Y0, double Y1) = 0;

      //! Get maximum radius in flux grid
      virtual double Rmax() { return 1.0; }

      @persistent_end
	
    };
    
    class @persistent(FluxFamilyOneDim) : public @super(FluxFamily)
    {
      friend FluxInterpOneDim;

    protected:
      
      //! Flux interpolation tables
      FluxInterpOneDimPtr @ap(flux);
      
      //! Compute flux normalization for this model specification
      virtual void compute_norm() {}
      
    public:
      
      //! Default constructor
      FluxFamilyOneDim() { inCons = false; }
      
      //! Initialize based on input parameters
      virtual void Initialize() { if (flux) flux->set(N); }
      
      //! Destructor
      virtual ~FluxFamilyOneDim() {}
      
      //! Evaluates the surface density for the model as a function of radius
      virtual double density(double r) = 0;
      
      //! Evaluates the surface density for the model for a given coordinate x, y
      virtual std::vector<double> operator()(double x, double y) = 0;
      
      //! Get flux in a pixel using the flux interpolation class
      double getPixel(double X0, double X1, double Y0, double Y1) {
	return flux->getPixel(X0, X1, Y0, Y1);
      }

      //! Get maximum radius in flux grid
      double Rmax() { return flux->Rmax; }

      @persistent_end
	
    };
    
    class @persistent(FluxFamilyBasis) : public @super(FluxFamily)
    {
      friend FluxInterpBasis;

    protected:
      
      //! Flux interpolation tables
      FluxInterpBasisPtr @ap(flux);
      
      //! Vector of parameter values
      std::vector<double> @ap(pvec);

      //! Mapping for image plane to unit interval
      mPtr @autopersist(mp);

      //! Get the normalization for a particular coefficient setting
      virtual void setNorm();
      
      //! Set the coefficients
      virtual void setCoefs(const std::vector<double>& v);
      
      //! For integrating a pixel
      virtual double integrand(double x) = 0;

      //! Half-light radius
      double @no_ap(r_half);

      //! Normalization factor
      double @no_ap(sigma0);

      //! Current parameter value
      double @no_ap(P);

    public:
      
      //! Default constructor
      FluxFamilyBasis() { inCons = false; }
      
      //! Destructor
      virtual ~FluxFamilyBasis() {}
      
      //! Set flux interpolation parameters
      virtual void Initialize() {
	if (!mp.get()) {
	  throw GalphatBadCode("FluxFamilyBasis", "Initialize",
			       "Mapping is uninstantiated",
			       __FILE__, __LINE__);
	}
	if (flux) {
	  flux->set(N);
	}
      }

      //! Get flux in a pixel using the flux interpolation class
      double getPixel(double X0, double X1, double Y0, double Y1) 
      {
	return flux->getPixel(X0, X1, Y0, Y1);
      }

      @persistent_end


    };
    
    BIE_CLASS_ABSTRACT(BIE::Galphat::Mapping)
    
    typedef boost::shared_ptr<FluxFamily>       FluxFamPtr;
    typedef boost::shared_ptr<FluxFamilyOneDim> FluxFamOneDimPtr;
    typedef boost::shared_ptr<FluxFamilyBasis>  FluxFamBasisPtr;
    
  } // namespace Galphat

} // namespace BIE

#endif

