//=============================================================================
// Bernstein polynomimal class
//=============================================================================

#include <values.h>

#include <map>
#include <cmath>
#include <cfloat>
#include <vector>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <utility>
#include <algorithm>

#include <BernPoly.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::BernPoly)

using namespace BIE::Galphat;

BernPoly::BernPoly()
{
  n = 0;
				// Force evaluation
  lastX = NAN;
				// No block defined
  bIndx = -1;
}

BernPoly::BernPoly(int order)
{
				// Sanity check
  n = std::max<size_t>(0, order);
				// Allocate return vector
  poly.resize(n+1, 0);
				// Force evaluation
  lastX = NAN;
				// No block defined
  bIndx = -1;
}

BernPoly::BernPoly(int order, int j)
{
				// Sanity check
  n = std::max<size_t>(0, order);
				// Allocate return vector
  poly.resize(n+1, 0);
				// Force evaluation
  lastX = NAN;
				// Block index
  bIndx = j;
}

const std::vector<double>& BernPoly::operator()(double x)
{
				// If we already evaluated the poly basis, 
  if (x == lastX) return poly;	// reuse it!

  lastX = x;			// Recompute

  if (n == 0) {
				// Special case for n=0 (constant order)
    poly[0] = 1.0;

  } else {

    poly[0] = 1.0 - x;		// Build using recursion relations
    poly[1] = x;		// from order n=1
 
    for (size_t i=2; i<=n; i++) {
      poly[i] = x * poly[i-1];
      for (size_t j=i-1; j>=1; j--) {
        poly[j] = x * poly[j-1] + (1.0 - x) * poly[j];
      }
      poly[0] = ( 1.0 - x ) * poly[0];
    }
  }
				// Normalization
  for (size_t i=0; i<=n; i++) poly[i] *= 1.0 + n;

  return poly;
}

double BernPoly::operator()(unsigned k, double x)
{
  if (k>n)        return 0.0;
  if (x != lastX) operator()(x);

  return poly[k];
}

double BernPoly::operator()(std::vector<double>& v, double x)
{
  // Vector rank sanity checking
  //
  if (v.size() != n+1) {
      std::ostringstream sout;
      sout << "BernPoly vector coefficient dimension mismatch; "
	   << "need <" << n+1 << "> but vector has rank <" << v.size() << ">";
      throw DimNotMatchException(sout.str(), n+1);
  }
    
  if (x != lastX) operator()(x);
  
  double ret = 0.0;
  for (size_t k=0; k<=n; k++) ret += v[k] * poly[k];
    
  return ret;
}


double BernPoly::integral(int k, double x)
{
  double ret = 0.0;
  for (int order=n+1; order<=k+1; order++) {
    BernPoly b(order);
    ret += b(k+1, x)/(1.0 + order);
  }
  ret *= (1.0 + n)/(1.0 + k);

  return ret;
}
