#include <stdio.h>
#include "fitsio.h"

void printerror(int status)
{
  //---------------------------------------------------
  // Print out cfitsio error messages and exit program
  //---------------------------------------------------


  if (status)
    {
      // print error report
      fits_report_error(stderr, status);

      // terminate the program, returning error status
      exit( status );
    }
    return;
}

