#include <values.h>		// For DBL_MAX

#include "BulgeDisk.h"
#include "GalphatLikelihoodFunction.h"

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::BulgeDisk)

using namespace BIE::Galphat;

BulgeDisk::BulgeDisk(pt::ptree::value_type& vt) : normDebug(false), sanityVal(0.0)
{
  initialize(vt);

  // Look for optional features
  //
  boost::optional<pt::ptree&>
    features = vt.second.get_child_optional("features");

  if (features) {
    // Enable reporting of image normalization for debugging
    normDebug = vt.second.get<bool>("features.normDebug", false);

    // Set norm sanity reporting limit (zero would report all)
    sanityVal = vt.second.get<double>("features.sanityVal", 0.05);

    if (normDebug and myid==0) {
      std::cout << "Galphat::BulgeDisk: norm debugging ON with tol="
		<< sanityVal << std::endl;
    }
  }

  if (xarray.num_elements() > 0) return;

  //
  // Generation and caching of implicit solution grid
  //
  
  nfluxn       = 10;
  nboverd      = 150;
  nalpha       = 10;
  nbeta        = 10;
  
  min_fluxn    = 0.1;
  min_boverd   = 0.01;
  min_alpha    = 0.3;
  min_beta     = 1.0;
  
  max_fluxn    = 11.0;
  max_boverd   = 15.0;
  max_alpha    = 3.0;
  max_beta     = 20.0;
  
  del_fluxn =   (max_fluxn   - min_fluxn )/(nfluxn  - 1);
  del_boverd  = (max_boverd  - min_boverd)/(nboverd - 1);
  del_alpha   = (max_alpha   - min_alpha )/(nalpha  - 1);
  del_beta    = (max_beta    - min_beta  )/(nbeta   - 1);
  
  xarray.resize(boost::extents[nfluxn][nboverd][nalpha][nbeta]);
  
  for (int i=0; i<nfluxn; i++) {

    double fluxn = min_fluxn + del_fluxn*i;

    for (int j=0; j<nboverd; j++) {

      double bd = min_boverd + del_boverd*j;

      for (int k=0; k<nalpha; k++) {

	double alpha = min_alpha + del_alpha*k;

	for (int l=0; l<nbeta; l++) {

	  double beta = min_beta + del_beta*l;

	  xarray[i][j][k][l] = solve_implicit(fluxn, bd, alpha, beta);
	}  
      }
    }
  }

}

void BulgeDisk::assignFlux(BIE::GalphatLikelihoodFunction* lf, int lev, 
			   inpars_ptr input, image_ptr img)
{
  setup(input, img);

  int indx1, indy1, indz1, indu1;
  int indx2, indy2, indz2, indu2;
      
  double xcent, ycent, fluxb, fluxd, rhbulge, rhdisk;
  double nbulge, qbulge, qdisk, phibulge, phidisk;
  double rhalf, rd_over_rb, bovert, boverd, ndisk;   
  double phi2,totcnt;
  
  int i0  = indexMap[0];
  int i1  = indexMap[1];
  int i2  = indexMap[2];
  int i3  = indexMap[3];
  int i4  = indexMap[4];
  int i5  = indexMap[5];
  int i6  = indexMap[6];
  int i7  = indexMap[7];
  int i8  = indexMap[8];
  int i9  = indexMap[9];
  int i10 = indexMap[10];


  //------------------------------
  // Center choice
  //------------------------------

  if      (ia[i0] == -1) xcent = d[i0];
  else if (ia[i0] ==  2) xcent = xoffset + a[i0];
  else                   xcent = a[i0];

  if      (ia[i1] == -1) ycent = d[i1];
  else if (ia[i1] ==  2) ycent = yoffset + a[i1];
  else                   ycent = a[i1];

  //------------------------------
  // B/T choice
  //------------------------------

  if (ia[i3] == -1) {		// Fixed: value is B/T
    bovert = d[i3];
    boverd = bovert/(1.0 - bovert);
  } else if (ia[i3] < 2) {	// Variable: value is B/T
    bovert = a[i3];
    boverd = bovert/(1.0 - bovert);
  } else {			// Variable: value is log(B/D)
    boverd = pow(10.0, a[i3]);
    bovert = boverd/(1.0 + boverd);
  }
      
  //------------------------------
  // Magnitude choice
  //------------------------------

  if (ia[i2] == -1) {
				// Fixed
				//
    totcnt = pow(10.0, (img->magzpt - d[i2])/2.5);
    fluxb  = totcnt * bovert; 
    fluxd  = totcnt * (1.0 - bovert);       

  } else if (ia[i2] > 1) {

    //------------------------------------------------------------
    // This offset is due to the distribution of mtotal following
    // Weibull with lambda=10.1, k=9.5, which peaks around 10.0.
    // And this offset need to be substracted from magi, which is
    // roughly measurement of total magnitude.  Therefore the
    // a[2] is distributed around zero with asymmetric shape
    // and magi should be estimated a priori and added.
    //------------------------------------------------------------

    double moffset = 1.0;
    totcnt = pow(10.0, (img->magzpt - (a[i2] - moffset + img->magi))/2.5);
    fluxb  = totcnt * bovert;  
    fluxd  = totcnt * (1.0 - bovert);  
  } else {
    totcnt = pow(10.0, (img->magzpt - a[i2])/2.5);
    fluxb  = totcnt * bovert; 
    fluxd  = totcnt * (1.0 - bovert);       
  }


  //------------------------------
  // Half-light radius
  //------------------------------

  if (ia[i4] == -1)
    rhalf = d[i4];		// Fixed
  else if (ia[i4] == 2)
    rhalf = rscale * (a[i4]);	// Relative
  else
    rhalf = a[i4];		// None

  //------------------------------
  // Disk/bulge ratio
  //------------------------------

  if (ia[i5] == -1)		// Fixed
    rd_over_rb = d[i5];
  else
    rd_over_rb = a[i5];

  //------------------------------
  // Sersic index for bulge
  //------------------------------

  if (ia[i6] == -1) nbulge = d[i6]; // Fixed
  else if (ia[i6] == 2) nbulge = noffset + a[i6];
  else nbulge = a[i6];     
  
  ndisk = 1.0;
      
  //------------------------------
  // Bulge axis ratio
  //------------------------------

  if (ia[i7] == -1)		// Fixed
    qbulge = d[i7];
  else if (ia[i7] == 2) 
    qbulge = qoffset + a[i7];
  else 
    qbulge = a[i7];
  
  //------------------------------
  // Disk axis ratio
  //------------------------------

  if (ia[i8] == -1)		// Fixed
    qdisk = d[i8];
  if (ia[i8] == 2) 
    qdisk  = qoffset + a[i8];
  else 
    qdisk = a[i8];
      
      
  //------------------------------------------------------------
  // Wrap angle if theta is greater than 90 or smaller than -90
  // GALPHAT angle definition -90 ~ 90
  //                         |
  //                  +      |     +
  //                         |
  //              ------------------------
  //                         |
  //                  -      |     -
  //                         |
  //
  // here phi is passed into rotation function as radian
  //------------------------------------------------------------

  if (ia[i9] == -1)		// Fixed
    phibulge = d[i9];
  else if (ia[i9] == 2) 	// Relative
    phibulge = paoffset + a[i9];
  else 				// None
    phibulge = a[i9];

  phi2 = phibulge;
  if(phi2 >  0.5*M_PI) phibulge = phi2 - M_PI;
  if(phi2 < -0.5*M_PI) phibulge = phi2 + M_PI;
      
      
  if (ia[i10] == -1) {		// Fixed
    phidisk = d[i10];
  } else if (ia[i10] > 2) {	// Relative to bulge
    phi2    = phibulge + a[i10];
    phidisk = phi2;
    if (phi2 >  0.5*M_PI) phidisk = phi2 - M_PI;
    if (phi2 < -0.5*M_PI) phidisk = phi2 + M_PI;
  } else if (ia[i10] == 2) {	// Relative
    phi2    = paoffset + a[i10];
    phidisk = phi2; 
    if (phi2 >  0.5*M_PI) phidisk = phi2 - M_PI;
    if (phi2 < -0.5*M_PI) phidisk = phi2 + M_PI;
  } else if (ia[i10] == -2) {	// Tied to bulge
    phidisk = phibulge;
  } else {			// None
    phidisk = a[i10];
    if (a[i10] >  0.5*M_PI) phidisk = a[i10] - M_PI;
    if (a[i10] < -0.5*M_PI) phidisk = a[i10] + M_PI;
  }
  
  //------------------------------------------------------------
  // Coordinate scaling (fixed to unity for now)
  //------------------------------------------------------------
  double rs = 1.0;

  //------------------------------------------------------------
  // interpolate the table
  //------------------------------------------------------------
  double alpha, beta, fluxn = nbulge;

  alpha = a[i4];
  beta  = rd_over_rb;
  indx1 = static_cast<int>( floor((fluxn  - min_fluxn )/del_fluxn  ));
  indy1 = static_cast<int>( floor((boverd - min_boverd)/del_boverd ));
  indz1 = static_cast<int>( floor((alpha  - min_alpha )/del_alpha  ));
  indu1 = static_cast<int>( floor((beta   - min_beta  )/del_beta   ));
      
  indx2 = indx1 + 1;
  indy2 = indy1 + 1;
  indz2 = indz1 + 1;
  indu2 = indu1 + 1;
      
  static    bool firstime = true;
  static    int  errorCount = 0;
  constexpr int  maxErrorCount = 10;

  std::ostringstream message;
  bool offGrid = false;

  if (indx1 < 0 || indx2 >= nfluxn) {
    offGrid = true;
    message << "Sersic index: "          << fluxn 
	    << " is out of bounds ["     << min_fluxn
	    << ", " << max_fluxn << "]"  << endl;
  }

  if (indy1 < 0 || indy2 >= nboverd) {
    offGrid = true;
    message << "B/D index: "             << boverd
	    << " is out of bounds ["     << min_boverd
	    << ", " << max_boverd << "]" << endl;
  }

  if (indz1 < 0 || indz2 >= nalpha) {
    offGrid = true;
    message << "r_half index: "          << alpha
	    << " is out of bounds ["     << min_alpha
	    << ", " << max_alpha << "]"  << endl;
  }

  if (indu1 < 0 || indu2 >= nbeta) {
    offGrid = true;
    message << "r_b/r_d index: "         << beta
	    << " is out of bounds ["     << min_beta
	    << ", " << max_beta << "]"   << endl;
  }

  double Fxyzu = 1.0;

  if (!offGrid) {

    double qx = (fluxn  - (min_fluxn  + del_fluxn  * indx1)) / del_fluxn;
    double qy = (boverd - (min_boverd + del_boverd * indy1)) / del_boverd;
    double qz = (alpha  - (min_alpha  + del_alpha  * indz1)) / del_alpha;
    double qu = (beta   - (min_beta   + del_beta   * indu1)) / del_beta;
      
    double px = 1.0 - qx;
    double py = 1.0 - qy;
    double pz = 1.0 - qz;
    double pu = 1.0 - qu;
  
    double Fx111 = 
      px*xarray[indx1][indy1][indz1][indu1] + 
      qx*xarray[indx2][indy1][indz1][indu1];
    
    double Fx211 = 
      px*xarray[indx1][indy2][indz1][indu1] + 
      qx*xarray[indx2][indy2][indz1][indu1];
  
    double Fx121 = 
      px*xarray[indx1][indy1][indz2][indu1] + 
      qx*xarray[indx2][indy1][indz2][indu1];

    double Fx221 = 
      px*xarray[indx1][indy2][indz2][indu1] + 
      qx*xarray[indx2][indy2][indz2][indu1];
      
    double Fxy11 = py*Fx111 + qy*Fx211;
    double Fxy21 = py*Fx121 + qy*Fx221;
    double Fxyz1 = pz*Fxy11 + qz*Fxy21;
      
    double Fx112=
      px*xarray[indx1][indy1][indz1][indu2] + 
      qx*xarray[indx2][indy1][indz1][indu2];
  
    double Fx212=
      px*xarray[indx1][indy2][indz1][indu2] + 
      qx*xarray[indx2][indy2][indz1][indu2];

    double Fx122=
      px*xarray[indx1][indy1][indz2][indu2] + 
      qx*xarray[indx2][indy1][indz2][indu2];

    double Fx222=
      px*xarray[indx1][indy2][indz2][indu2] + 
      qx*xarray[indx2][indy2][indz2][indu2];
      
    double Fxy12 = py*Fx112 + qy*Fx212;
    double Fxy22 = py*Fx122 + qy*Fx222;
    double Fxyz2 = pz*Fxy12 + qz*Fxy22;
    
    Fxyzu = pu*Fxyz1 + qu*Fxyz2;

  } else {

    Fxyzu = solve_implicit(fluxn, boverd, alpha, beta);

    if (firstime) {
      std::cout << "Galphat: parameters out of bounds . . . " << std::endl
		<< "consider check your prior specification for consistency with the implicit parameter grid in BulgeDisk.cc" << std::endl;
      firstime = false;
    }
    if (errorCount++ < maxErrorCount) {
      std::cout << "Galphat: " << message.str() << std::endl;
    }
    if (errorCount == maxErrorCount) {
      std::cout << "Galphat: further error messages will be suppressed"
		<< std::endl;
    }
  }

  rhbulge = rscale / Fxyzu;
  rhdisk  = rhbulge * rd_over_rb;
      
  double valbulge, valdisk, x, y;
      
  std::vector<double> nb(1, nbulge), nd(1, ndisk);

  setParam(nb, rs, rhbulge, qbulge);

  for (int j=0; j<Numy; j++) {
    y = Ymin + dy*j;

    for (int i=0; i<Numx; i++) {
      x = Xmin + dx*i;

      valbulge = intgrlFlux(x-xcent, x+dx-xcent, y-ycent, y+dy-ycent);
	
      lf->inbulge[level][j*Numx+i] = valbulge;
    }
  }
      
  setParam(nd, rs, rhdisk, qdisk);

  for (int j=0; j<Numy; j++) {
    y = Ymin + dy*j;

    for (int i=0; i<Numx; i++) {
      x = Xmin + dx*i;

      valdisk = intgrlFlux(x-xcent, x+dx-xcent, y-ycent, y+dy-ycent);
      
      lf->indisk [level][j*Numx+i] = valdisk;
    }
  }
      
  bool logscl = nbulge > lf->ncrit;

  // Rotate and PSF convolve the bulge model
  //
  lf->ProcessImage(lf->inbulge[level], lf->fluxbulge[level], numx, numy, Numx, Numy,
		   Xmin, Ymin, phibulge, xcent, ycent, input, logscl);

  // Rotate and PSF convolve the disk model
  //
  lf->ProcessImage(lf->indisk[level], lf->fluxdisk[level], numx, numy, Numx, Numy,
		   Xmin, Ymin, phidisk, xcent, ycent, input, false);
  //                                                         ^
  //                                                         |
  // No log scaling for disk---------------------------------+
  //

  if (normDebug) {
    double bNorm = 0.0, dNorm = 0.0;

    for (auto v : lf->fluxbulge[level]) bNorm += v;
    for (auto v : lf->fluxdisk [level]) dNorm += v;
    
    if (fabs(bNorm-1.0) > sanityVal and	fabs(dNorm-1.0) > sanityVal) {
      std::cout << "[" << std::setw(3) << myid
		<< "]: Galphat::BulgeDisk model norm ";
      for (auto v : a)
	std::cout << "| " << std::right
		  << std::setw( 2) << v.first << "->"
		  << std::setw(12) << v.second;
      std::cout << " :: " << bNorm << ", " << dNorm << std::endl;
    }
  }


  //------------------------------------------------------------
  // Sum up after rotation     
  //------------------------------------------------------------
  for (int j=0; j<numy; j++) {

    for (int i=0;i<numx; i++) {

      lf->mdl[level][j*numx+i] = 
	fluxb * lf->fluxbulge[level][j*numx+i] + 
	fluxd * lf->fluxdisk [level][j*numx+i];
    }

  }
      
  //------------------------------------------------------------
  // Sanity check and final flux assignment
  //------------------------------------------------------------
  for (int j=0; j<numy; j++) {
    for (int i=0; i<numx; i++) {

      if (std::isnan(lf->mdl[level][j*numx+i])) {

	std::cout << "Model(BuldeDisk) pixel is NaN: "
		  << i << " " << j 
		  << " " << lf->mdl[level][j*numx+i] << std::endl
		  << "Parameters: " 
		  << " " << a[i0]  << " " << a[i1]  << " " << a[i2] 
		  << " " << a[i3]  << " " << a[i4]  << " " << a[i5]  
		  << " " << a[i6]  << " " << a[i7]  << " " << a[i8] 
		  << " " << a[i9]  << " " << a[i10]
		  << std::endl;
      } else {
	lf->totalflux[level][j*numx+i] += lf->mdl[level][j*numx+i];
      }
    }
  }
  
}

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_sf.h>

struct _f_params 
{ 
  double sersicn;
  double bd;
  double alpha;
  double beta;
};

double _impl_func(double x, void *params)
{
  struct _f_params *p = (struct _f_params *)params;

  double bd      = p->bd;
  double sersicn = p->sersicn;
  double alpha   = p->alpha;
  double beta    = p->beta;

  double bn = 0.0;
  
  if (sersicn>0.36) { 
    bn = 2.0*sersicn - 1.0/3.0 +
      ((((131./1148175. - 2194697./30690717750./sersicn)/sersicn) + 46.0/25515.)/sersicn + 4./405.)/sersicn;
  }
  else {
    bn = 0.01945 - 0.8902*sersicn + 10.95*sersicn*sersicn -
      19.67*sersicn*sersicn*sersicn + 13.43*sersicn*sersicn*sersicn*sersicn;
  }
  
  double b1 = 1.678;
 
  double xn = bn*pow((alpha*x),1./sersicn);
  double x1 = b1*alpha/beta*x;
  
  double term1 = gsl_sf_gamma_inc_P (2.0*sersicn, xn);  
  double term2 = gsl_sf_gamma_inc_P (2.0, x1);  

  return term1*bd + term2 - 0.5*(bd+1.0);
}

double BulgeDisk::solve_implicit(double n, double bd, double alpha, double beta)
{

  int status;
  int iter = 0, max_iter = 100;
  const gsl_root_fsolver_type *T;
  gsl_root_fsolver *s;

  double r = 0.0;
  double x_lo = 0.0001, x_hi = 1000.0;
  gsl_function F;

  struct _f_params params = {n, bd, alpha, beta};

  F.function = &_impl_func;
  F.params   = &params;

  T = gsl_root_fsolver_brent;
//  T = gsl_root_fsolver_bisection;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_lo, x_hi);

  do
   {
     iter++;

     status = gsl_root_fsolver_iterate (s);
     r = gsl_root_fsolver_root (s);
     x_lo = gsl_root_fsolver_x_lower (s);
     x_hi = gsl_root_fsolver_x_upper (s);
     status = gsl_root_test_interval (x_lo, x_hi, 1.0e-6, 1.0e-5);

   } while (status == GSL_CONTINUE && iter < max_iter);

   gsl_root_fsolver_free (s);

   return r;
}

