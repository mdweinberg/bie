// This is really -*- C++ -*-

#ifndef GalphatLikelihoodFunction_h
#define GalphatLikelihoodFunction_h

#include <iomanip>
#include <vector>
#include <cmath>
#include "FluxFamily.h"
#include "FluxInterp.h"
#include <gaussQ.h>
#include <Timer.h>
#include <fftw3.h>

#include <Serializable.h>
#include <LikelihoodFunction.h>
#include "GalphatParam.h"
#include <string>
#include <BIEconfig.h>
#include <fstream>
#include <iostream>

@include_persistence

namespace BIE
{
  namespace Galphat
  {
    class Single;
    class BulgeDisk;
    class Point;
    class PSF;
    class Sky;
  }
}

namespace BIE {
  
  /// GALPHAT likelihood function
  //+ CLICLASS GalphatLikelihoodFunction SUPER LikelihoodFunction
  class @persistent(GalphatLikelihoodFunction) : public @super(LikelihoodFunction)
  {
    // Needed for model-specific callbacks (do we need to make more
    // data public?)
    friend class Galphat::Single;
    friend class Galphat::BulgeDisk;
    friend class Galphat::Point;
    friend class Galphat::PSF;
    friend class Galphat::Sky;
    
  protected:
    // Needs to be a pointer rather than a shared pointer since it's
    // passed from the BIE parser
    GalphatParam* @ap(obj);
    
    int    @ap(Level);
    
    double @ap(psfr);
    double @ap(ncrit);
    double @ap(temperature);   
    
    std::string @ap(infile);
    
    bool     @ap(imDebug);
    unsigned @ap(nfDebug);
    
    std::map<int, fftw_plan> xImg, kImg, Rotx, invRotx, Roty, invRoty;
    
    typedef std::vector<double>  Dvector;
    typedef std::complex<double> Complex;
    typedef std::vector<Complex> Cvector;
    typedef boost::shared_ptr<Cvector> cv_ptr;
    
    std::map<int, Cvector> ftmdl;
    std::map<int, Cvector> ftmdlmargin;
    std::map<int, Cvector> Fkx;
    std::map<int, Cvector> Gkx;
    std::map<int, Cvector> Fky;
    std::map<int, Cvector> Gky;
    
    std::map<int, Dvector> mdl;
    std::map<int, Dvector> mdlmargin;
    std::map<int, Dvector> xCol;
    std::map<int, Dvector> yRow;
    
    std::map<int, Dvector> totalflux;
    std::map<int, Dvector> in;
    std::map<int, Dvector> inbulge;
    std::map<int, Dvector> indisk;
    std::map<int, Dvector> flux;
    std::map<int, Dvector> fluxbulge;
    std::map<int, Dvector> fluxdisk;
    std::map<int, Dvector> modelpsf;
    
    
    std::map<int, Galphat::image_ptr> mask;
    std::map<int, Galphat::image_ptr> mdlmask;
    
    std::map<int, Galphat::image_ptr> model; 
    std::map<int, Galphat::image_ptr> d;
    
    Galphat::image_ptr     @ap(psf);
    Galphat::inpars_ptr    @ap(input);
    
    bool   @ap(no_psf);
    bool   @ap(use_bilinear);

    bool   @ap(debug_flux_on);
    bool   @ap(debug_flux_solo);
    string @ap(debug_flux_name);
    int    @ap(debug_flux_strid);
    int    @ap(debug_flux_total);
    int    @ap(debug_flux_count);
    int    @ap(debug_flux_index);
    
    bool   @ap(debug_flux_gof);
    string @ap(debug_flux_xcen);
    string @ap(debug_flux_ycen);
    string @ap(debug_flux_rad);
    string @ap(debug_flux_pa);
    string @ap(debug_flux_q);
    double @ap(debug_flux_rfac);

    std::vector<double> @no_ap(debug_flux_param);

    void debugFlux (Galphat::image_ptr model, Galphat::image_ptr data,
		    Galphat::image_ptr mask);
    
    void computeGOF(Galphat::image_ptr model, Galphat::image_ptr data,
		    Galphat::image_ptr mask);

  public:
    
    //+ CLICONSTR
    //! Null constructor; creates an uninitialized instance
    GalphatLikelihoodFunction();
    
    //+ CLICONSTR GalphatParam* int
    //+ CLICONSTR GalphatParam*
    
    //! The main constructor that initializes internal variables.
    //! @param pmap is an instance of the Galphat parameter helper class
    //! @param level is the aggregation level (use 1 for no aggregation)
    GalphatLikelihoodFunction(GalphatParam* pmap, int level=1);
    
    
    //+ CLIMETHOD void setNcrit int
    
    //! Used to switch between linear and log flux scaling in the
    //! GalphatModel routines that use Sersic indicies.  This is way
    //! too model specific for this class but I'm leaving it here for
    //! backward compatibility.
    void setNcrit (double n){ ncrit = n; }
    
    //+ CLIMETHOD void setTemp double
    //! For powering up . . . 
    void setTemp (double t) { temperature = t; }
    
    //+ CLIMETHOD void imageDebug bool
    //! Print out images with negative flux values for diagnosis
    void imageDebug (bool tf) { imDebug = tf; }
    
    //+ CLIMETHOD void noPSF
    //! Suppress PSF convolution (for testing; obviously, astronomical
    //! images would always have some PSF)
    void noPSF() { no_psf = true; }

    //+ CLIMETHOD void use3Shear
    //! Use the 3-shear FFT-based image rotation algorithm (this is
    //! the default)
    void use3Shear () { use_bilinear = false; }

    //+ CLIMETHOD void useBilinear
    //! Use the bilinear image rotation algorithm (for testing)
    void useBilinear () { use_bilinear = true; }

    //+ CLIMETHOD void setRotation string
    //! Set to "3-shear" or "bilinear" rotation
    void setRotation (string s)
    {
      if (s[0] == 'b' or s[0] == 'B')
	use_bilinear = true;
      else if (s[0] == '3')
	use_bilinear = false;
      else if (myid==0) {
	std::cout << "!!! Rotation key words are: \"3-shear\" and \"bilinear\"." << std::endl
		  << "!!! Assuming \"3-shear\"." << std::endl;
	use_bilinear = false;
      }
    }
    
    //+ CLIMETHOD void sampleImages int int
    //! Print out model and sample images in FITS format for comparison
    //! See Galphat/user/README.residual for additional details.
    //! @param stride is the spacing in steps between images
    //! @param total  is the total number of images to be computed
    void sampleImages (int stride, int total)
    {
      debug_flux_on    = true;
      debug_flux_solo  = false;
      debug_flux_strid = stride;
      debug_flux_total = total;
      debug_flux_count = 0;
      debug_flux_index = 0;
    }
    
    //+ CLIMETHOD void singleImage string
    //! Print out model and sample images in FITS format for
    //! comparison.  See Galphat/user/README.residual for additional
    //! details
    void singleImage (string prefix)
    {
      debug_flux_on    = true;
      debug_flux_solo  = true;
      debug_flux_name  = prefix;
      debug_flux_strid = 1;
      debug_flux_total = 1;
      debug_flux_count = 0;
      debug_flux_index = 0;
    }
    
    //+ CLIMETHOD void gofStats clivectord* string string string string string
    //+ CLIMETHOD void gofStats clivectord* string string string string string double

    //! Print out goodness-of-fit statistics: chi^2 per dof and KL
    //! divergence for comparison.  See Galphat/user/README.residual
    //! for additional details
    void gofStats (std::vector<double> vec,
		   std::string Xcenter, std::string Ycenter,
		   std::string PA, std::string Q, std::string Radius,
		   double radius_factor=1.0)
    {
      debug_flux_gof   = true;
      debug_flux_xcen  = Xcenter;
      debug_flux_ycen  = Ycenter;
      debug_flux_pa    = PA;
      debug_flux_q     = Q;
      debug_flux_rad   = Radius;
      debug_flux_rfac  = radius_factor;
      debug_flux_param = vec;
    }

    void gofStats (clivectord* vec,
		   string Xcenter, std::string Ycenter,
		   std::string PA, std::string Q, std::string Radius,
		   double radius_factor=1.0)
    {
      debug_flux_gof   = true;
      debug_flux_xcen  = Xcenter;
      debug_flux_ycen  = Ycenter;
      debug_flux_pa    = PA;
      debug_flux_q     = Q;
      debug_flux_rad   = Radius;
      debug_flux_rfac  = radius_factor;
      debug_flux_param = (*vec)();
    }

    //! destructor
    ~GalphatLikelihoodFunction();
    
    //! Setup parameters and input images
    //! level1 --> lowest level, original image
    //! level2 --> second level
    //! level3 --> third level
    //! level4 --> highest level
    void Setup (Galphat::image_ptr data, Galphat::image_ptr psf, Galphat::image_ptr badpix);
    
    //! This is likelihood function
    //! note that UserPreLikeProb is higer resolution image
    double LikeProb (std::vector<double> &z, SampleDistribution* sd, 
		     double norm, Tile *t, State *sta, int indx);
    
    //! State to Galfit param
    void UpdateGalphatVector(State *s);
    
    //! Label parameters. User should give correct form of input file
    //! to have correct param name.
    virtual const std::string ParameterDescription(int i);
    
    //! make mini image
    Galphat::image_ptr ToMini (Galphat::image_ptr img, int npix, Galphat::inpars_ptr input);
    
    //! convolve model
    void MakeModel(Galphat::image_ptr img, Galphat::inpars_ptr input, double mag_b, double Re_b);
    
    //! Do FFT of PSF
    cv_ptr FFTedPSF(Galphat::inpars_ptr input, double angle);
    
    //! Check PSF size
    void psfcheck_bie (Galphat::image_ptr psf);
    
    //! Rotate and convolve image with PSF
    void ProcessImage(std::vector<double>& in, std::vector<double>& out, 
		      int numx, int numy, int Numx, int Numy,
		      double Xmin, double Xmax,
		      double phi, double x0, double y0, 
		      Galphat::inpars_ptr input_ptr, bool logscl);

    //! Do rotation using shear algorithm
    void RotateImage(std::vector<double>& in, std::vector<double>& out, 
		     int numx, int numy, int Numx, int Numy,
		     double Xmin, double Ymin,
		     double phi, double x0, double y0, 
		     Galphat::inpars_ptr input_ptr);
    
    //! Do Shift
    void ShiftImage(std::vector<double>& im, 
		    double delx, double dely, int numx, int numy);
    
    //! calculate likelihood
    double LikelihoodCalc (std::vector<float>& inmask, 
			   std::vector<float>& tmpmask, 
			   std::vector<float>& indata, 
			   std::vector<float>& my_model, 
			   std::vector<long> & naxes, float gain, 
			   float ncomb, float rdnoise);
    

    //! Rotation class using 3-shear algorithm
    class ShearRotateReal : public ShearRotate<float>
    {
    private:
      
      double value;
      
    public: 
      //! Constructor
      ShearRotateReal (double value) : 
	ShearRotate<float> (NULL), value(value) {}
      
      //! Destructor
      virtual ~ShearRotateReal() {}
      
      //! Background value generator.  This could use a random generator
      //! to produce a sky value.
      float backVal() { return value; }
    };
    
    //! Rotation class using bilinear algorithm
    class InterpRotateReal : public InterpRotate<float>
    {
    private:
      
      double value;
      
    public: 
      //! Constructor
      InterpRotateReal (double value) : value(value) {}
      
      //! Destructor
      virtual ~InterpRotateReal() {}
      
      //! Background value generator.  This could use a random generator
      //! to produce a sky value.
      float backVal() { return value; }
    };
    
    
  private:
    
    //! Model image debugging
    void WriteFITS(std::vector<long>&  naxes,
		   std::vector<float>& image,
		   unsigned long       badpix);
    
    //! Model image debugging (model and image)
    void WriteFITS2(std::vector<long>&  naxes,
		    std::vector<float>& im1,
		    std::vector<float>& im2,
		    unsigned            nindex);
    
    //! Remake FFT and level maps on deserialization. They contain no
    //! state information.
    template<class Archive>
      void post_load(Archive &ar, const unsigned int file_version) 
    {
      Setup(obj->getData(), obj->getPsf(), obj->getBadpix());
    }
    
    @persistent_end_split
  };
  
} // namespace BIE

#endif

