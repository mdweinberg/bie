#include "GalphatLikelihoodFunction.h"
#include "Models.H"

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::Mapping)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::PowerMap)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::ExpMap)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::LogMap)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::R1Map)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::R2Map)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::RA1Map)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::RA2Map)

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::DVfunctor2d)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::FluxFamily)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::FluxFamilyOneDim)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::FluxFamilyBasis)

using namespace BIE::Galphat;

// For debugging only!
//
static bool verbose_debug = false;

//
// Initialize currently valid flux families (pre C++11 initialization
// must be done in two steps; this can be changed eventually)
//
const std::string family_t[] =  
  {
    "SERSIC", 
    "GAUSS", 
    "BERNSTEIN",
    "CHEBYSHEV",
    "POWER",  // This and following two are not yet implemented
    "BULGE", 
    "DISK"
  };

std::set<std::string> FluxFamily::valid_families
(family_t, family_t + sizeof(family_t)/sizeof(family_t[0]));

std::set<std::string>::iterator 
FluxFamily::vf_end(FluxFamily::valid_families.end());


FluxFamPtr FluxFamily::factory(pt::ptree& tree)
{
  //
  // Find the flux model
  //
  std::string mod;
  try {
    mod = tree.get<std::string>("model.value");
  } catch (pt::ptree_error &error) {
    std::string msg("Error finding string tag <model.value>");
    std::cerr << msg << std::endl;
    throw GalphatBadParameter("FluxFamily", "factory", 
			      msg, __FILE__, __LINE__);
  }
  
  //------------------------------------------------------    
  // Check tabtype
  //------------------------------------------------------    
  if (valid_families.find(mod) == vf_end) {
    std::ostringstream sout;
    sout << "Model <" << mod << "> is not defined";
    throw GalphatBadParameter("FluxFamily", "factory", sout.str(),
			      __FILE__, __LINE__);
  }
  
  FluxFamPtr model;

  if (mod.compare("SERSIC") == 0) {
    model = SersicPtr(new Sersic(tree));
  }
  else if (mod.compare("GAUSS") == 0) {
    model = GaussPtr(new Gauss(tree));
  } 
  else if (mod.compare("BERNSTEIN") == 0) {
    model = BernImgPtr(new BernImg(tree));
  } 
  else if (mod.compare("CHEBYSHEV") == 0) {
    model = ChebImgPtr(new ChebImg(tree));
  } 
  else {
    std::ostringstream sout;
    sout << "no such model <" << mod << ">, quitting";
    throw GalphatBadParameter("tableinfo", "initialize", sout.str(),
			      __FILE__, __LINE__);
  }

  return model;
}

vector<double> FluxFamily::pixel
(double a0, double a1, double b0, double b1, 
 double eps, int maxlev)
{
  vector<double> ret;

  QuadTreeIntegrate<FluxFamily> integrate(this);

  ret = integrate.Integral(a0, a1, b0, b1, 1, eps, maxlev);
  ret.push_back(integrate.Error());
  ret.push_back(integrate.NumEvals());
  return ret;
}

void FluxFamilyBasis::setCoefs(const std::vector<double>& v)
{
  if (v.size() != N.size()+1) {
    std::cerr << "ImageBasis: coefficient dimension mismatch" 
	      << std::endl;

    if (v.size() < N.size()+1) {
      for (size_t i=0; i<v.size()-1; i++) 
	N[i] = v[i];
      for (size_t i=v.size()-1; i<N.size(); i++) 
	N[i] = 0.0;
    } else {
      for (size_t i=0; i<N.size(); i++) 
	N[i] = v[i];
    }
  } else {
    N = std::vector<double>(v.begin(), v.begin()+N.size());
  }

  P = v.back();

  double sum = 0.0;
  for (auto  v : N) sum += fabs(v);
  for (auto &v : N) v = fabs(v)/sum;

  setNorm();
}


void FluxFamilyBasis::setNorm()
{
  // Find bounding scale values
  //
  std::vector<double>::iterator beg = pvec.begin(), end = pvec.end();
  std::vector<double>::iterator lb = std::lower_bound (beg, end, P);
  unsigned sz = pvec.size(), lo, hi;

  if (lb == end) {
    lo = sz - 2;
    hi = sz - 1;
  } else if (lb == beg) {
    lo = 0;
    hi = 1;
  } else {
    lo = lb - beg - 1;
    hi = lb - beg;
  }

  double Rlo = pvec[lo], Rhi = pvec[hi];
  double Ars = (Rhi - P)/(Rhi - Rlo);
  double Brs = (P - Rlo)/(Rhi - Rlo);

  size_t dim    = flux->slices[0].norm[0].size();
  size_t norder = flux->norder;

  std::vector<double> cum(dim, 0.0);
  for (size_t i=0; i<dim; i++) {
    for (unsigned n=0; n<norder; n++) {
      if (not normZ or n==0)
	cum[i] += N[n]*(Ars*flux->slices[lo].norm[n][i] + Brs*flux->slices[hi].norm[n][i]);
    }
  }

  std::vector<double>::iterator it = std::lower_bound(cum.begin(), cum.end(),
						      0.5*cum.back());
  if (verbose_debug)
    std::cout << "Test [" 
	      << *(it-1)/cum.back() << ",  " << *it/cum.back() 
	      << "]" << std::endl;
    
  size_t indx1 = it - cum.begin();
  
  double dx = flux->dx;
  double x2 = mp->xmin()+dx*indx1, x1 = mp->xmin()+dx*(indx1-1);
  double r1 = mp->invxi(x2);
  double r2 = mp->invxi(x1);
  
  if (verbose_debug) {
    std::cout << "I [" << mp->xmin() << ", " << mp->xmax() << "]" << std::endl;
    std::cout << "X [" << x1   << ", " << x2   << "]" << std::endl;
    std::cout << "R [" << r1   << ", " << r2   << "]" << std::endl;
  }
    
  r_half = (r1 * ( *it - 0.5*cum.back() ) + r2 * (0.5*cum.back() - *(it-1))) /
    ( *it - *(it-1) );
  
  if (verbose_debug)
    std::cout << "R_half = " << r_half   << std::endl;
  
  sigma0 = 1.0/cum.back();
}


boost::shared_ptr<Mapping>
Mapping::create(std::string name, double A, double rm, double Rm)
{
  // Instantiate the mapping
  //
  if (name.compare("ExpPowMap") == 0)  return mPtr(new ExpPowMap(A, Rm));
  if (name.compare("ExpMap")    == 0)  return mPtr(new ExpMap(A, Rm));
  if (name.compare("PowerMap")  == 0)  return mPtr(new PowerMap(A, rm, Rm));
  if (name.compare("R1Map")     == 0)  return mPtr(new R1Map(Rm));
  if (name.compare("R2Map")     == 0)  return mPtr(new R2Map(Rm));
  if (name.compare("RA1Map")     == 0) return mPtr(new RA1Map(A, Rm));
  if (name.compare("RA2Map")    == 0)  return mPtr(new RA2Map(A, Rm));

  // Default: linear mapping
  //
  return mPtr(new PowerMap(1.0, 0.0, Rm));
}
