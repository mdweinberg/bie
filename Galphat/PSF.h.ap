// -*- C++ -*-

// GALPHAT model class

#ifndef PSF_h
#define PSF_h

#include "GalphatModel.h"

@include_persistence

namespace BIE {

  namespace Galphat {
    
    class @persistent(PSF) : public @super(GalphatModel)
    {
    public:

      //! Return desired field names
      virtual std::map<std::string, int> getFieldNames()
      {
	const std::map<std::string, int> ret = 
	  {
	    {"X center", 0},
	    {"Y center", 1},
	    {"PSF mag",  2}
	  };
	
	return ret;
      }
      
    public:
      
      //! Constructor
      PSF(pt::ptree::value_type& vt) { initialize(vt); }
      
      //! Flux return callback
      virtual void assignFlux(GalphatLikelihoodFunction* lf, int lev, 
			      inpars_ptr input, image_ptr img);
      
      @persistent_end
    };
    
    typedef boost::shared_ptr<struct GalphatModel> Gmodel_ptr;

  } // namespace Galphat

} // namespace BIE
  
#endif
  
  
