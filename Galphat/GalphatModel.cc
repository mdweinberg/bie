
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>

#include <iomanip>

#include <boost/lexical_cast.hpp>

#include "GalphatParam.h"
#include "Models.H"

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::GalphatModel)

using namespace BIE::Galphat;

//
// Initialize currently valid fitpars model types (pre C++11
// initialization must be done in two steps; this can be changed
// eventually)
//
const std::string model_t[] = 
  {
    "Single", 
    "BulgeDisk", 
    "Point",
    "PSF", 
    "Sky"
  };

//
// Helper function to parse a vector value from a boost property_tree
//
template <typename T>
std::vector<T> as_vector(pt::ptree const& pt, pt::ptree::key_type const& key)
{
    std::vector<T> r;
    for (auto& item : pt.get_child(key))
        r.push_back(item.second.get_value<T>());
    return r;
}

//
// The set of all models for checking input parameters
//
std::set<std::string> 
GalphatModel::valid_models
(model_t, model_t + sizeof(model_t)/sizeof(model_t[0]));

//
// End-of-list iterator for model set
//
std::set<std::string>::iterator 
GalphatModel::vm_end (GalphatModel::valid_models.end());

//
//  GalphatModel "reflection" factory
//

Gmodel_ptr GalphatModel::factory(pt::ptree::value_type& v)
{
  //------------------------------------------------------    
  // Check model type
  //------------------------------------------------------    
  if (valid_models.find(v.first) == GalphatModel::vm_end) {
    std::ostringstream sout;
    sout << "Error reading value from <model type> stanza; type <" 
	 << v.first << "> is not defined";
    throw GalphatBadParameter("GalphatModel", "factory", sout.str(), 
			      __FILE__, __LINE__);
  }

  //------------------------------------------------------    
  // Call the constructor
  //------------------------------------------------------    
  Gmodel_ptr ret;
  if      (v.first.compare("Single"   ) == 0)  ret = Gmodel_ptr(new Single   (v));
  else if (v.first.compare("BulgeDisk") == 0)  ret = Gmodel_ptr(new BulgeDisk(v));
  else if (v.first.compare("Point"    ) == 0)  ret = Gmodel_ptr(new Point    (v));
  else if (v.first.compare("PSF"      ) == 0)  ret = Gmodel_ptr(new PSF      (v));
  else if (v.first.compare("Sky"      ) == 0)  ret = Gmodel_ptr(new Sky      (v));
  else {
    std::ostringstream sout;
    sout << "Error constructing new model of type "
	 << v.first << "> string not identified";
    throw GalphatBadParameter("GalphatModel", "factory", sout.str(), 
			      __FILE__, __LINE__);
  }

  //------------------------------------------------------    
  // Assign parameters from config stanza
  //------------------------------------------------------    
  ret->objtype = v.first;

  return ret;
}

void GalphatModel::initialize(pt::ptree::value_type& vt)
{
  objtype = vt.first;

  // Iterate over stanza
  int indx = 0;
  pt::ptree::const_iterator it;
  foreach_(pt::ptree::value_type const&v, vt.second.get_child("") ) {
    
    if (v.first.compare("description") == 0) {
      // continue, ignored
    } else if (v.first.compare("_comment") == 0) {
      // continue, ignored
    } else if (v.first.compare("tables") == 0) {
      // Read the flux model specification and cached tables
      //
      pt::ptree subtree = v.second;
      createFluxTables(subtree);
    } else if (v.first.compare("vector") == 0) {
      // Look for a vector definition
      //
      try {
	// The initial index
	//
	int beg = vt.second.get<int>(v.first+".first");

	// The final index
	//
	int lst = vt.second.get<int>(v.first+".last");

	// Make the vector map
	//
	vectorMap.clear();
	for (int i=beg; i<=lst; i++) vectorMap.push_back(i);

	// Look for initial values
	//
	std::vector<double> val;
	size_t numb = lst - beg + 1;
	
	if ( v.second.count("values") ) {
	  for (auto z : as_vector<double>(v.second, "values")) {
	    val.push_back(z);
	  }

	  if (val.size() != numb) {
	    std::ostringstream sout;
	    sout << "Dimension mismatch between count in stanza <" << v.first 
		 << "> and expected number: " << numb;
	    throw GalphatBadParameter("GalphatModel", "initialize", sout.str(), 
				      __FILE__, __LINE__);
	  }
	} else {
	  val.resize(numb, 1.0/numb); // Make a default unit-normed vector
	}

	for (int i=beg; i<=lst; i++) {
	  a[i] = val[i-beg];
	  d[i] = val[i-beg];
	}

	std::string lab = vt.second.get<std::string>(v.first+".label", "a");

	for (int i=beg; i<=lst; i++) {
	  std::ostringstream sout;
	  sout << lab << "(" << i - beg + 1 << ")";
	  parname[i] = sout.str();
	}

      } catch (pt::ptree_error &error) {
	std::ostringstream sout;
	sout << "Could not find [first, last] in stanza <" << v.first << ">";
	throw GalphatBadParameter("GalphatModel", "initialize", sout.str(), 
				  __FILE__, __LINE__);
      }

    } else if (v.first.compare("features") == 0) {
      // continue
    } else {
      // Everything else is assumed to be data stanzas
      //
      try {
	indx = vt.second.get<int>(v.first+".index");
      } catch (pt::ptree_error &error) {
	std::ostringstream sout;
	sout << "No index for stanza <" << v.first << ">";
	throw GalphatBadParameter("GalphatModel", "initialize", sout.str(), 
				  __FILE__, __LINE__);
      }

      parname[indx] = v.first;

      try {
	a[indx] = vt.second.get<float>(v.first+".value");
	d[indx] = a[indx];	// Cache default value
      } catch (pt::ptree_error &error) {
	std::ostringstream sout;
	sout << "No value for stanza <" << v.first << ">";
	throw GalphatBadParameter("fitpars", "initialize", sout.str(), 
				  __FILE__, __LINE__);
      }
      try {
	std::string field = vt.second.get<std::string>(v.first+".ctrl");
	ia[indx] = GalphatParam::setControl(objtype, field);
      } catch (pt::ptree_error &error) {
	ia[indx] = 0;		// Global default
      }
    }
  }

  findIndices();
}
  
void GalphatModel::createFluxTables(pt::ptree& tree)
{
  //                                                                            
  // Next find the model image table files                                      
  //                                                                            
  foreach_(pt::ptree::value_type& v, tree.get_child("") )
    {
      int j;
      try {
        j = boost::lexical_cast<int>(v.first);
      } catch ( boost::bad_lexical_cast const& ) {
        std::ostringstream error;
        error << "Error converting integer string tag <" << v.first
              << "> to integer in image table stanza";
        std::cerr << error.str() << std::endl;
        throw GalphatBadParameter("GalphatModel", "create_FluxTables",
                               error.str(), __FILE__, __LINE__);
      }

      fluxtab[j] = FluxFamily::factory(v.second);
    }
}

void GalphatModel::setup(inpars_ptr input, image_ptr img)
{
  aggpix     = input->aggfact;
  level      = input->level;
  rscale     = input->rscale;
  noffset    = input->noffset;
  qoffset    = input->qoffset;
  paoffset   = input->paoffset;
  xoffset    = input->xoffset;
  yoffset    = input->yoffset;
  bgoffset   = input->bgoffset;
  
  xoff       = 0.5*(aggpix-1)/aggpix;
  yoff       = 0.5*(aggpix-1)/aggpix;
  
  numx       = img->naxes[0];
  numy       = img->naxes[1];

  margx      = round<int, double>(0.25*numx);
  margy      = round<int, double>(0.25*numy);

  Numx       = numx+2*margx;
  Numy       = numy+2*margy;

  xmin       = img->imgsect[0];
  xmax       = img->imgsect[1];
  ymin       = img->imgsect[2];
  ymax       = img->imgsect[3];

  xmin       = xmin - 1;
  ymin       = ymin - 1;

  Xmin       = xmin - margx*aggpix;
  Ymin       = ymin - margy*aggpix;

  dx         = (xmax - xmin)/img->naxes[0];
  dy         = (ymax - ymin)/img->naxes[1];
}

double GalphatModel::intgrlFlux
(double x0, double x1, double y0, double y1)
{
  double retval = 0.0;		// Default return value
  double A      = r0;
  double B      = r0*q0;

  if (q0 > 1.0) {
    A = r0/q0;
    B = r0;
  }
  
  double sx0 = x0/A;
  double sy0 = y0/B;
  double sx1 = x1/A;
  double sy1 = y1/B;
  
  double distx0 = fabs(sx0);
  double disty0 = fabs(sy0);
  double distx1 = fabs(sx1);
  double disty1 = fabs(sy1);
  
  if (fluxtab.begin()->first>0) {
    // Assume a hierarchy of multiresolution grid if index > 0
    FluxFamPtr last = fluxtab.rbegin()->second;
    for (auto v : fluxtab) {
      double Rmax = v.second->Rmax();
      if (v.second == last) {
	retval = v.second->getPixel(x0, x1, y0, y1);
	break;
      }
      if (distx0 < Rmax &&  disty0 < Rmax &&
	  distx1 < Rmax &&  disty1 < Rmax  ) {
	retval = v.second->getPixel(x0, x1, y0, y1);
	break;
      }
    }
    
  } else {
    // Assume a single grid
    retval = fluxtab[0]->getPixel(x0, x1, y0, y1);
  }
  
  return retval;
}


void GalphatModel::findIndices()
{
  // Assign parameter rank by fields
  std::map<std::string, int> fnames = getFieldNames();

  if (fnames.size() == 0) return;

  // Invert the parname map
  std::map<std::string, int> tmpMap;
  for (auto v : parname) tmpMap[v.second] = v.first;


  for (auto v : fnames) {
    // Look for field name in parameter list
    //
    auto f = tmpMap.find(v.first);
    //
    // Field name not known!
    //
    if (f == tmpMap.end()) {
      fieldNameError(v.first, tmpMap, fnames);
    }

    //
    // Save location of field name in parameter list
    //
    indexMap[v.second] = f->second;
  }
}

void GalphatModel::fieldNameError(const std::string& field,
				  const std::map<std::string, int>& map,
				  const std::map<std::string, int>& fnm)
{
  if (myid==0) {
    std::cout << std::string(40, '-')  << std::endl
	      << "Field parsing error" << std::endl
	      << std::string(40, '-')  << std::endl
	      << std::endl << "From parameter stanza:" 
	      << std::endl << std::endl << std::left
	      << std::setw(20) << "Field" << std::setw(6) << "Value"
	      << std::endl
	      << std::setw(20) << "-----" << std::setw(6) << "-----"
	      << std::endl;
    for (auto x : map)
      std::cout << std::setw(20) << x.first << std::setw(6) << x.second
		<< std::endl;

    std::cout << std::endl << "Model expects:" << std::endl << std::endl
	      << std::setw(20) << "Field" << std::setw(6) << "Index"
	      << std::endl
	      << std::setw(20) << "-----" << std::setw(6) << "-----"
	      << std::endl;
    for (auto x : fnm)
      std::cout << std::setw(20) << x.first << std::setw(6) << x.second
		<< std::endl;
    std::cout << std::endl;
  }
  
  throw GalphatBadParameter("GalphatModel", "findIndices", 
			    "Could not find <" + field + ">",
			    __FILE__, __LINE__);
}
