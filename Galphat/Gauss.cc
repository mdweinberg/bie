#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

#include <Gauss.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::Gauss)

using namespace BIE::Galphat;

Gauss::Gauss(pt::ptree& tree) 
{
  ID   = "Gauss";
}

std::vector<double> Gauss::pixel(double a0, double a1, double b0, double b1, 
				 double eps, int maxlev)
{
  std::vector<double> ret;

  QuadTreeIntegrate<Gauss> integrate(this);

  ret = integrate.Integral(a0, a1, b0, b1, 1, eps, maxlev);
  ret.push_back(integrate.Error());
  ret.push_back(integrate.NumEvals());
  return ret;
}

