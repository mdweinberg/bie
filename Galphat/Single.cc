#include "Single.h"
#include "GalphatLikelihoodFunction.h"

#include <boost/function.hpp>

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::Single)

using namespace BIE::Galphat;

double cabsfftw(fftw_complex z) { return sqrt(z[0]*z[0] + z[1]*z[1]); }
    
Single::Single(pt::ptree::value_type& vt) : normDebug(false), sanityVal(0.0)
{
  initialize(vt);

  // Look for optional features
  //
  boost::optional<pt::ptree&>
    features = vt.second.get_child_optional("features");

  if (features) {
    // Enable reporting of image normalization for debugging
    normDebug = vt.second.get<bool>("features.normDebug", false);

    // Set norm sanity reporting limit (zero would report all)
    sanityVal = vt.second.get<double>("features.sanityVal", 0.05);

    if (normDebug and myid==0) {
      std::cout << "Galphat::Single: norm debugging ON with tol="
		<< sanityVal << std::endl;
    }
  }
}

void Single::assignFlux(BIE::GalphatLikelihoodFunction* lf, int lev, 
			inpars_ptr input, image_ptr img)
{
  //
  // Algorithm ID for log file 
  //
  if (myid==0) {
    static bool firstime = true;
    if (firstime) {
      std::cout << "**Galphat is using "
		<< (lf->use_bilinear ? "BILINEAR" : "3-SHEAR")
		<< " image rotation" << std::endl;
      firstime = false;
    }
  }

  //
  // Parameter initializaton callback for model
  //
  setup(input, img);

  int i0 = indexMap[0];
  int i1 = indexMap[1];
  int i2 = indexMap[2];
  int i3 = indexMap[3];
  int i4 = indexMap[4];
  int i5 = indexMap[5];
  int i6 = indexMap[6];

  //------------------------------------------------------------
  // Galaxy center
  // 
  double x0, y0;
				// Fixed
  if      (ia[i0] == -1) x0 = d[i0];
  else if (ia[i0] ==  2) x0 = xoffset + a[i0];
  else                   x0 = a[i0];
  
  if      (ia[i1] == -1) y0 = d[i1];
  else if (ia[i1] ==  2) y0 = yoffset + a[i1];
  else                   y0 = a[i1];

  //------------------------------------------------------------
  // Magnitude offset
  // 
  double totcnt;
  if (ia[i2] == -1) {		// Fixed
    totcnt = pow(10.0, (img->magzpt - d[i2])/2.5);
  } else if (ia[i2] > 1) {

    //------------------------------------------------------------
    // This offset is due to the distribution of mtotal following
    // Weibull with lambda=10.1, k=9.5, which peaks around 10.0.
    // And this offset need to be substracted from magi, which is
    // roughly measurement of total magnitude.  Therefore the
    // fp->a[2] is distributed around zero with asymmetric shape
    // and magi should be estimated a priori and added.
    //------------------------------------------------------------

    double moffset = 1.0;
    totcnt = pow(10.0, (img->magzpt - (a[i2] - moffset + img->magi))/2.5);
  } else {
    totcnt = pow(10.0, (img->magzpt - a[i2])/2.5);
  }
  
  //------------------------------------------------------------
  // Radial scaling (e.g. R_e for Sersic model)
  // 
  float re;
  if (ia[i3] == -1)		// Fixed
    re = d[i3];
  else if (ia[i3] == 2) 
    re = rscale * a[i3];
  else 
    re = a[i3];
      
  //------------------------------------------------------------
  // Model shape parameters
  // 
  size_t nsz = vectorMap.size();
  std::vector<double> n;
  double rs = 1.0;

  if (nsz) {
    n.resize(nsz);
    for (size_t i=0; i<nsz; i++) {
      int j = vectorMap[i];
      if (ia[j] == -1)		// Fixed
	n[i] = d[j];
      else if (ia[j] == 2) 
	n[i] = noffset + a[j];
      else 
	n[i] = a[j];
    }

    /*

    // Coordinate scaling
    int i7 = indexMap[7];

    if (ia[i7] == -1)		// Fixed
      rs = d[i7];
    else if (ia[i7] == 2) 	// Relative to re
      rs = a[i3] * a[i7];
    else 			// No scaling
      rs = a[i7];

    */
    
  } else {
    n.resize(1);
    if (ia[i4] == -1) 		// Fixed
      n[0] = d[i4];
    if (ia[i4] == 2) 
      n[0] = noffset + a[i4];
    else 
      n[0] = a[i4];
  } 

  //------------------------------------------------------------
  // Axis ratio
  // 
  double ba, q;
  if (ia[i5] == -1)		// Fixed
    ba = d[i5];
  else if (ia[i5] > 1) 		// Relative
    ba = qoffset + a[i5];
  else 
    ba = a[i5];
  
  q = ba;
      
  //------------------------------------------------------------
  // Wrap angle if theta is greater than 90 or smaller than -90
  // angle definition -90 ~ 90
  //                         |
  //                  +      |     +
  //                         |
  //              ------------------------
  //                         |
  //                  -      |     -
  //                         |
  //
  // Note: all position angle variables are in radians not in
  // degrees!!
  //------------------------------------------------------------
      
      
  //------------------------------------------------------------
  // Position angle
  // 
  double phi, phi2;

  if (ia[i6] == -1)		// Fixed
    phi = d[i6];
  else if (ia[i6] == 2) 	// Relative
    phi = paoffset + a[i6];
  else				// None
    phi = a[i6];

  phi2 = phi;

  if (phi2 >  0.5*M_PI) phi = phi2 - M_PI;
  if (phi2 < -0.5*M_PI) phi = phi2 + M_PI;
      

  //------------------------------------------------------------
  // Initialize model
  //------------------------------------------------------------

  setParam(n, rs, re, q);

  bool onedim = fluxtab.begin()->first > 0 ? true : false;
  bool logscl = onedim and n[0] > lf->ncrit;

  //------------------------------------------------------------
  // Get model flux
  //------------------------------------------------------------

  double x, y;
      
  for (int j=0; j<Numy; j++) {
    y = Ymin + dy*j;
    for (int i=0; i<Numx; i++) {
      x = Xmin + dx*i;
      lf->in[level][j*Numx+i] = intgrlFlux(x-x0, x+dx-x0, y-y0, y+dy-y0);
    }
  }


  lf->ProcessImage(lf->in[level], lf->flux[level], numx, numy, Numx, Numy,
		   Xmin, Ymin, phi, x0, y0, input, logscl);


  if (normDebug) {
    double tNorm = 0.0;
    for (auto v : lf->flux[level]) tNorm += v;

    if (fabs(tNorm - 1.0) > sanityVal) {
      std::cout << "[" << std::setw(3) << myid
		<< "]: Galphat::Single model norm ";
      for (auto v : a)
	std::cout << "| " << std::right
		  << std::setw( 2) << v.first << "->"
		  << std::setw(12) << v.second;
      std::cout << " :: " << tNorm << std::endl;
    }
  }

  for (int j=0; j<numy; j++) {

    for (int i=0; i<numx; i++) {
				// Sanity check . . .
      if (std::isnan(lf->mdl[level][j*numx+i])) {
	std::cout << "Model(Single) pixel is NaN: "
		  << i << " " << j
		  << " " << lf->mdl[level][j*numx+i] << std::endl
		  << "Parameters:" 
		  << " " << a[i0]
		  << " " << a[i1]
		  << " " << a[i2]
		  << " " << a[i3]
		  << " " << a[i4]
		  << " " << a[i5]
		  << " " << a[i6]
		  << std::endl;
	if (nsz) {
	  std::cout << "Vector:";
	  for (size_t i=0; i<nsz; i++)
	    std::cout << " " << a[vectorMap[i]];
	  std::cout << std::endl;
	}
      } else {
	lf->totalflux[level][j*numx+i] += totcnt * lf->flux[level][j*numx+i];
      }
    }
  }

}

void Single::findIndices()
{
  GalphatModel::findIndices();
  bool missing = true;

  // Find parameter slot for OneDim family only
  //
  if ( vectorMap.size() == 0 ) {
    for (auto v : parname) {
      // For backwards compatibility . . . 
      //
      if (v.second.compare("Sersic")==0) {
	indexMap[4] = v.first;
	missing = false;
      }
      //
      // I'd prefer to use the name Shape instead, since the
      // underlying flux model may not be Sersic
      //
      else if (v.second.compare("Shape")==0) {
	indexMap[4] = v.first;
	missing = false;
      }
    }

    //
    // One of these must be specified
    //
    if (missing) {
      const std::string msg("Could not find <Sersic> or <Shape> parameters "
			    "required for a single-parameter model family");

      throw GalphatBadParameter("Single", "findIndices", msg,
				__FILE__, __LINE__);
    }

  } else {

    // Add basis-only parameters

    /*

    //
    // Look for Param
    //
    for (auto v : parname) {
      if (v.second.compare("Param")==0) {
	indexMap[7] = v.first;
	missing = false;
      }
    }
    
    //
    // The Param parameter must be defined
    //
    if (missing) {
      const std::string msg("Could not find <Param> parameter "
			    "required for a vector-parameter model family");

      throw GalphatBadParameter("Single", "findIndices", msg,
				__FILE__, __LINE__);
      
    }

    */
    
  }
}
