#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>

#include <iomanip>

#include "FluxInterp.h"
#include "GalphatModel.h"
#include "GalphatParam.h"

#include "Models.H"
#include "Acquire.H"

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::GalphatParam)

using namespace BIE;

// Control structure for mnemonic parameter handling
GalphatParam::dbControl GalphatParam::paramControl;

// MPI variables

extern int  BIE::myid;
extern int  BIE::numprocs;
extern bool BIE::mpi_used;

//
// Constructor
//
GalphatParam::GalphatParam(const std::string& galphat_input)
{
  infile = galphat_input;
  initializeControl();
  Setup();
}

// Destructor
GalphatParam::~GalphatParam()
{
}

//
// Initialize control structure mnemonics
//
void GalphatParam::initializeControl()
{
  if (paramControl.size()) return;

  /** Notes:
      o All entries should define the pair ("Fixed", -1)

      o Using boost::assign::map_list_of to do initialization of the
        std::map<> containers.  This can be done wity {} initializers
        when everybody is using C++-11.  I could have written this is
        a CPP switch now but too visually messy.
  */

  //------------------------------------------------------------
  // Single
  //------------------------------------------------------------

  const modelControl Single = 
    boost::assign::map_list_of ("Multiplicative", 2) ("Additive", 2) ("Scaled", 2) 
    ("Weibull flux", 2) ("Fixed", -1) ;
  
  paramControl["Single"] = Single;

  //------------------------------------------------------------
  // BulgeDisk
  //------------------------------------------------------------
  modelControl BulgeDisk = boost::assign::map_list_of ("Additive", 2) 
    ("Multiplicative", 2) ("Scaled", 2) ("Linear", 0) ("Logarithmic", 2) 
    ("Weibull flux", 2) ("Offset from bulge", 3) ("Fixed", -1) ("Fixed to bulge", -2);

  paramControl["BulgeDisk"] = BulgeDisk;

  //------------------------------------------------------------
  // Point
  //------------------------------------------------------------
  modelControl Point = 
    boost::assign::map_list_of ("Additive", 1) ("Multiplicative", 2) 
    ("Weibull flux", 2) ("Fixed", -1);

  paramControl["Point"] = Point;

  //------------------------------------------------------------
  // Sky
  //------------------------------------------------------------
  modelControl Sky = 
    boost::assign::map_list_of ("Additive", 1) ("Multiplicative", 2) 
    ("Fixed", -1);

  paramControl["Sky"] = Sky;

  //------------------------------------------------------------
  // PSF
  //------------------------------------------------------------
  modelControl PSF = boost::assign::map_list_of ("Fixed", -1);
  
  paramControl["PSF"] = PSF;

  //------------------------------------------------------------
}

int GalphatParam::setControl(const std::string& model, const std::string& type)
{
  dbControl::iterator p = paramControl.find(model);
  if (p == paramControl.end()) {
    std::ostringstream sout;
    sout << "no such model named <" << model << ">";
    throw GalphatBadParameter("GalphatParam", "setControl", sout.str(), 
			      __FILE__, __LINE__);
  }

  int ret = 0;

  modelControl::iterator q = p->second.find(type);
  if (q != p->second.end()) ret = q->second;

  return ret;
}

// functions for accessing members
std::string GalphatParam::getFname()
{
  return infile;
}

Galphat::image_ptr GalphatParam::getData()
{
  return data;
}

Galphat::image_ptr GalphatParam::getBadpix()
{
  return badpix;
}

Galphat::image_ptr GalphatParam::getPsf()

{
  return psf;
}

Galphat::Gmodel_ptr GalphatParam::getModelsBegin()
{
  Galphat::Gmodel_ptr ret;
  if (models.size()) {
    curFP = models.begin();
    ret = *curFP;
  }
  return ret;
}

Galphat::Gmodel_ptr GalphatParam::getModelsNext()
{
  Galphat::Gmodel_ptr ret;
  if (++curFP != models.end()) ret = *curFP;
  return ret;
}

Galphat::inpars_ptr GalphatParam::getInput(int indx)
{
  Galphat::inpars_ptr ret;
  if (input.find(indx) != input.end()) ret = input[indx];
  return ret;
}

Galphat::inpars_ptr GalphatParam::getInputBegin()
{
  Galphat::inpars_ptr ret;
  if (input.size()) {
    curIN = input.begin();
    ret = curIN->second;
  }
  return ret;
}

Galphat::inpars_ptr GalphatParam::getInputNext()
{
  Galphat::inpars_ptr ret;
  if (++curIN != input.end()) ret = curIN->second;
  return ret;
}

void GalphatParam::Setup()
{
  if (myid==0) {
    std::cout << std::endl
	      << "starting.... GALaxy PHotometric ATtributes: GALPHAT" 
	      << std::endl
	      << "This is GALPHAT version " << GALPHAT_VERSION 
	      << std::endl;
  }
  
  int returnval = 0;
  
  std::ifstream in(infile.c_str());
  if (!in) {
    std::string error("can not open configuration file");
    throw GalphatBadConfig(infile, "GalphatParam", "Setup()", error, 
			   __FILE__, __LINE__);
  }
  read_json(in, props);

  //
  // Instantiate the image pointers
  //
  data    = Galphat::image_ptr (new struct Galphat::image);
  badpix  = Galphat::image_ptr (new struct Galphat::image);
  psf     = Galphat::image_ptr (new struct Galphat::image);

  //
  // Begin intializing
  //

  ptree_parse();

  //
  // Point to the base-level input parameter stanza
  //
  Galphat::inpars_ptr iptr = input[1];
  
  /************************************************************************\
   * Read fits files and store flux data into data, psf, badpix structure  *
   * Please note that image, badpix can be aggregated but PSF is not.      *
   * When we do PSF convolution, the convolution should be done for        *
   * original pixel scale.                                                 *
   * If aggfact is greator than 10*FWHM, No PSF convolution                *
  \************************************************************************/

  if ((returnval = data->readFITS()) == 1) {
    if (myid==0) {
      std::cout << "!!! WARNING -- No input image fits file was found" << std::endl
		<< "!!! Zero flux image will be generated" << std::endl;
    }

    int xmin = iptr->imgsect[0];
    int xmax = iptr->imgsect[1];
    int ymin = iptr->imgsect[2];
    int ymax = iptr->imgsect[3];

    for (int i=0; i<4; i++) data->imgsect[i] = iptr->imgsect[i];

    data->naxes[0] = xmax - xmin + 1;
    data->naxes[1] = ymax - ymin + 1;

    data->z.resize(data->naxes[1]*data->naxes[0]);
    for (int j=ymin; j <= ymax; ++j) for (int i=xmin; i <= xmax; ++i) {
      int ij = (j-1)*data->naxes[0]+(i-1);
      data->z[ij] = 0.0;
    }
  }
  if (data->gain < 0.0)     data->gain     = iptr->gain;
  if (data->ncombine < 0.0) data->ncombine = iptr->ncombine;
  if (data->rdnoise < 0.0)  data->rdnoise  = iptr->rdnoise;

  if (psf->readFITS() == 1) {
    if (myid==0) {
      std::cout << "!!! WARNING -- No PSF image fits file was found" << std::endl;
      if (iptr->shape>0.0)
	std::cout << "!!! WARNING -- Using analytic 2MASS-type PSF "
		  << "with shape parameter: " << iptr->shape << std::endl;
	
      else
	std::cout << "!!! WARNING -- No PSF will be used" << std::endl;
    }
    psf->name =  "none";
  } 

  if (badpix->readFITS() == 1            ||  
      badpix->naxes[0] != data->naxes[0] ||
      badpix->naxes[1] != data->naxes[1]) { 

    if (badpix->err==0) {
      if (myid==0) {
	std::cout << "!!! WARNING -- The pixel mask is not the same size as the data" 
		  << std::endl;
      }
    } else {
      badpix->naxes[0] = data->naxes[0];
      badpix->naxes[1] = data->naxes[1];
      if (badpix->readBADPIX() == 1) {
	if (myid==0) {
	  std:: cout << "-- No masking image used" << std::endl;
	}
	badpix->name = "none";
      }
    }
  }

  for (int i=0; i<4; i++) data  ->imgsect[i] = iptr->imgsect[i];
  for (int i=0; i<4; i++) badpix->imgsect[i] = iptr->imgsect[i];
}

double solve_shape(float sh, float fwhm);
struct my_f_params { float a;
  float b;
};  
double myfunc(double n, void *params);

int totnobj=0;
int totlevel=0;

void GalphatParam::ptree_parse()
{
  //
  // Object counters
  //

  totnobj  = 0;
  totlevel = 0;
  
  //
  // First, find the control stanza
  //
  foreach_(pt::ptree::value_type& v, props.get_child("parameters.control")) 
    {
      int indx;
      try {
	indx = boost::lexical_cast<int>(v.first);
      } catch ( boost::bad_lexical_cast const& ) {
	std::ostringstream error;
	error << "Error converting integer string tag <" << v.first
	      << "> to integer in control stanza";
	std::cerr << error.str() << std::endl;
	throw GalphatBadConfig(infile, "GalphatParam", "ptree_parse", error.str(), 
			       __FILE__, __LINE__);
      }

      input[indx] = Galphat::inpars_ptr(new Galphat::inpars);
      input[indx]->initialize(v.second);
      totlevel++;
    }

  if (totlevel==0) {
    std::string error("No valid stanzas found within control!");
    std::cerr << error << std::endl;
    throw GalphatBadConfig(infile, "GalphatParam", "ptree_parse", error, 
			   __FILE__, __LINE__);
  }

  //
  // Parse the model stanzas
  //
  foreach_(pt::ptree::value_type& v, props.get_child("parameters.models") ) 
    {
      if (v.first.compare("description") == 0) {
	// ignore
      } else if (v.first.compare("_comment") == 0) {
	// ignore
      } else {
	// Assume that this is a model type, otherwise throw exception
	Galphat::Gmodel_ptr newobj = Galphat::GalphatModel::factory(v);
	models.push_back(newobj);
	totnobj += 1;
      }
    }

  data->name   = input[1]->data;
  psf->name    = input[1]->psf;
  badpix->name = input[1]->badpix;

  data->magzpt = input[1]->magzpt;
  data->adjzpt = input[1]->adjzpt;
  data->magi   = input[1]->magi;

  return;
}

double solve_shape(float sh, float fwhm)
{
  
  int status;
  int iter = 0, max_iter = 100;
  const gsl_root_fsolver_type *T;
  gsl_root_fsolver *s;
  
  double r=0.0;
  double x_lo = 0.01, x_hi = 4.0;
  gsl_function F;
  
  struct my_f_params params = {sh,fwhm};  
  
  F.function = &myfunc;
  F.params=&params;
  
  T = gsl_root_fsolver_brent;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_lo, x_hi);
  
  do
    {
      iter++;
      status = gsl_root_fsolver_iterate (s);
      r = gsl_root_fsolver_root (s);
      x_lo = gsl_root_fsolver_x_lower (s);
      x_hi = gsl_root_fsolver_x_upper (s);
      status = gsl_root_test_interval (x_lo, x_hi, 0, 0.001);
      
      if (status == GSL_SUCCESS)
        printf ("Converged:\n");
      
    } while (status == GSL_CONTINUE && iter < max_iter);
  
  gsl_root_fsolver_free (s);
  
  return r;    
  
}

double myfunc(double n, void *params)
{
  // Using John Lucey's analysis
  // For K band, if |seesh-1.1|<0.001 FWHM=2.980 else FWHM=2.2355*seesh+0.7699
  
  struct my_f_params *p = (struct my_f_params *)params;
  double sh =  p->a;
  double FWHM = p->b;  
  
  if (fabs(FWHM-0.0) < 0.0001) {
    
    /*Version1:*/
    //  if(abs(sh-1.1)<0.001) FWHM=2.980;
    //  else FWHM=2.2355*sh+0.7699;
    
    /*Version2:*/
    FWHM=3.13*sh-0.46;
    
  }
  
  return 2.0/n*pow(log(2.0),n)-FWHM/sh;
}


void Galphat::inpars::initialize(pt::ptree& tree) 
{
  try {
    level = tree.get<int>("level.value");
  } catch (pt::ptree_error &error) {
    level = Acquire::get<int>("Level");
    tree.put("level.desc", "data aggregation level");
    tree.put("level.value", level);
  }
  //------------------------------------------------------
  try {
    data = tree.get<std::string>("image.value");
  } catch (pt::ptree_error &error) {
    data = Acquire::get<std::string>("Image");
    tree.put("image.desc", "Input data, FITS file");
    tree.put("image.value", data);
  }
  //------------------------------------------------------
  try {
    psf = tree.get<std::string>("psf.value");
  } catch (pt::ptree_error &error) {
    psf = Acquire::get<std::string>("PSF? (leave blank to use Sersic profile)");
    if (psf.size()) {
      tree.put("psf.desc", "PSF image, FITS file");
      tree.put("psf.value", data);
    }
  }
  //------------------------------------------------------
  try {
    badpix = tree.get<std::string>("mask.value");
  } catch (pt::ptree_error &error) {
    badpix = Acquire::get<std::string>("Bad pixel mask");
    tree.put("badpix.desc", "Bad pixel mask, FITS file");
    tree.put("badpix.value", data);
  }
  //------------------------------------------------------
  try {
    std::string buf = tree.get<std::string>("region.value");
    std::istringstream sin(buf);
    for (int i=0; i<4; i++) {
      sin >> imgsect[i];
    }
  } catch (pt::ptree_error &error) {
    imgsect = Acquire::get<int>("Image region", 4);
    std::ostringstream sbuf;
    for (int i=0; i<4; i++) {
      if (i<3) sbuf << " ";
      sbuf << imgsect[i];
    }
    tree.put("region.desc", "Image region to fit");
    tree.put("region.value", sbuf.str());
  }
  //------------------------------------------------------
  try {
    magzpt = tree.get<float>("zeropt.value");
  } catch (pt::ptree_error &error) {
    magzpt = Acquire::get<float>("Zero pt magnitude");
    tree.put("zeropt.desc", "Photometric zero point");
    tree.put("zeropt.value", magzpt);
  }
  try {
    adjzpt = tree.get<bool>("adjzpt.value");
  } catch (pt::ptree_error &error) {
    /*
    adjzpt = Acquire::get<bool>("Zero pt adjust");
    tree.put("adjexp.desc", "Adjust zero pt for exposure");
    tree.put("adjexp.value", adjzpt);
    */
    adjzpt = false;
  }
  //------------------------------------------------------
  try {
    aggfact = tree.get<int>("aggregate.value");
  } catch (pt::ptree_error &error) {
    aggfact = Acquire::get<float>("Image aggregation factor");
    tree.put("aggregate.desc", "Image aggregation factor");
    tree.put("aggregate.value", aggfact);
  }
  //------------------------------------------------------
  try {
    shape = tree.get<float>("shape.value");
  } catch (pt::ptree_error &error) {
    if (psf.size()==0) {
      shape = Acquire::get<float>("PSF seeing shape");
      tree.put("shape.desc", "PSF seeing shape");
      tree.put("shape.value", shape);
    } else {
      shape = 1.0;
    }
  }
  //------------------------------------------------------
  try {
    fwhm = tree.get<float>("fwhm.value");
    double FWHM = fwhm;
    if( fabs(fwhm)<0.0001) FWHM = 3.13*shape - 0.46;
    psf_r = FWHM/2.0/sqrt(log(2.0)/0.6933997);
    psf_n = 0.5;
  } catch (pt::ptree_error &error) {
    if (psf.size()==0) {
      fwhm = Acquire::get<float>("PSF FWHM");
      tree.put("fwhm.desc", "FWHM psf radius in pixels");
      tree.put("fwhm.value", fwhm);
    } else {
      fwhm = 1.0;
    }
  }
  //------------------------------------------------------
  try {
    psf_n = tree.get<float>("psf_n.value");
  } catch (pt::ptree_error &error) {
    if (psf.size()==0) {
      psf_n = Acquire::get<float>("PSF Sersic index");
      tree.put("psf_n.desc", "PSF Sersic index");
      tree.put("psf_n.value", psf_n);
    } else {
      psf_n = 2;
    }
  }
  //------------------------------------------------------
  try {
    psf_r = tree.get<float>("psf_r.value");
  } catch (pt::ptree_error &error) {
    if (psf.size()==0) {
      psf_r = Acquire::get<float>("PSF effective radius");
      tree.put("psf_r.desc", "PSF effective radius");
      tree.put("psf_r.value", psf_r);
    } else {
      psf_r = 1.0;
    }
  }
  //------------------------------------------------------
  try {
    gain = tree.get<float>("gain.value");
  } catch (pt::ptree_error &error) {
    gain = Acquire::get<float>("Gain [e-/ADU]");
    tree.put("gain.desc", "Gain [e-/ADU]");
    tree.put("gain.value", gain);
  }
  //------------------------------------------------------
  try {
    ncombine = tree.get<float>("coadd.value");
  } catch (pt::ptree_error &error) {
    if (myid==0) {
      std::cout << "Ncombine value not provided: assuming 1" 
		<< std::endl;
    }
    ncombine = 1.0;
    tree.put("coadd.desc", "Number of coadded frames");
    tree.put("coadd.value", ncombine);
  }
  //------------------------------------------------------
  try {
    rdnoise = tree.get<float>("rdnoise.value");
  } catch (pt::ptree_error &error) {
    if (myid==0) {
      std::cout << "Readout noise not provided, ignoring"
		<< std::endl;
    }
    rdnoise = 0;
  }
  //------------------------------------------------------
  try {
    magi = tree.get<float>("magi.value");
  } catch (pt::ptree_error &error) {
    magi = Acquire::get<float>("Fiducial magnitude");
    tree.put("magi.desc", "fiducial magnitude (i.e. catalog estimate)");
    tree.put("magi.value", magi);
  }
  //------------------------------------------------------
  try {
    rscale = tree.get<float>("rscale.value");
  } catch (pt::ptree_error &error) {
    rscale = Acquire::get<float>("Scale length");
    tree.put("rscale.desc", "Scale length");
    tree.put("rscale.value", rscale);
  }
  //------------------------------------------------------
  try {
    xoffset = tree.get<float>("xoffset.value");
  } catch (pt::ptree_error &error) {
    xoffset = Acquire::get<float>("X center offset in pixels");
    tree.put("xoffset.desc", "X center offset in pixels");
    tree.put("xoffset.value", xoffset);
  }
  //------------------------------------------------------
  try {
    yoffset = tree.get<float>("yoffset.value");
  } catch (pt::ptree_error &error) {
    yoffset = Acquire::get<float>("Y center offset in pixels");
    tree.put("yoffset.desc", "Y center offset in pixels");
    tree.put("yoffset.value", yoffset);
  }
  //------------------------------------------------------
  try {
    qoffset = tree.get<float>("qoffset.value");
  } catch (pt::ptree_error &error) {
    qoffset = Acquire::get<float>("Axis ratio offset");
    tree.put("qoffset.desc", "Axis ratio offset");
    tree.put("qoffset.value", qoffset);
  }
  //------------------------------------------------------
  try {
    paoffset = tree.get<float>("paoffset.value");
  } catch (pt::ptree_error &error) {
    paoffset = tree.get<float>("Position angle offset (radians)");
    tree.put("paoffset.desc", "Position angle offset (radians)");
    tree.put("paoffset.value", paoffset);
  }
  //------------------------------------------------------
  try {
    noffset = tree.get<float>("noffset.value");
  } catch (pt::ptree_error &error) {
    noffset = Acquire::get<float>("Shape index offset");
    tree.put("noffset.desc", "Shape index offset");
    tree.put("noffset.value", noffset);
  }
  //------------------------------------------------------
  try {
    bgoffset = tree.get<float>("bgoffset.value");
  } catch (pt::ptree_error &error) {
    bgoffset = Acquire::get<float>("Background value offset");
    tree.put("bgoffset.desc", "Background value offset");
    tree.put("bgoffset.value", bgoffset);
  }
  //------------------------------------------------------
}
