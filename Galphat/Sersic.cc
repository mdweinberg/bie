#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

#include <Sersic.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::Sersic)

using namespace BIE::Galphat;

Sersic::Sersic(pt::ptree& tree) 
{
  ID   = "Sersic";
  flux = FluxInterpOneDimPtr(new FluxInterpOneDim(tree, this));
}


Sersic::Sersic(double n, double r, double x, double y, double q, double phi)
{
  N    = std::vector<double>(1, n);
  r0   = r;
  x0   = x;
  y0   = y;
  q0   = q;
  phi0 = phi;
}

std::vector<double> Sersic::pixel(double a0, double a1, double b0, double b1,
				  double eps, int maxlev)
{
  std::vector<double> ret;

  QuadTreeIntegrate<Sersic> integrate(this);
  
  ret = integrate.Integral(a0, a1, b0, b1, 1, eps, maxlev);
  ret.push_back(integrate.Error());
  ret.push_back(integrate.NumEvals());
  return ret;
}
