#include "Acquire.H"

template <> 
std::string BIE::Galphat::Acquire::get<std::string>(const std::string& query)
{
  std::string value;
  if (myid==0) {
    std::cout << query << "? ";
    std::cin  >> value;
  }
  if (mpi_used) {
    unsigned sz = value.size();
    MPI_Bcast(&sz, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    char *buf = new char[sz];
    if (myid==0) strncpy(buf, value.c_str(), sz);
    MPI_Bcast(buf, sz, MPI_CHAR, 0, MPI_COMM_WORLD);
    if (myid) value = std::string(buf, sz);
    delete [] buf;
  }
  return value;
}

