#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

#include <gsl/gsl_integration.h>

#include <QuadTreeIntegrate.H>
#include <BernImg.h>
#include <BIEmpi.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::BernImg)

using namespace BIE::Galphat;

const bool verbose = true;

BernImg::BernImg(pt::ptree& tree) 
{
  ID = "BernImg";

  //
  // Find the flux model
  //
  try {
    norder = tree.get<int>("order.value");
  } catch (pt::ptree_error &error) {
    std::string msg("Error finding string tag <order.value>");
    if (myid==0) std::cerr << msg << std::endl;
    throw GalphatBadParameter("BernImg", "constructor", 
			      msg, __FILE__, __LINE__);
  }
  
  try {
    rs_min = tree.get<double>  ("scale.min");
    rs_max = tree.get<double>  ("scale.max");
    rs_num = tree.get<unsigned>("scale.num");
  } catch (pt::ptree_error &error) {
    if (myid==0) {
      std::string msg("Error finding string tag <scale.min>, <scale.max>, or <scale.num>, setting scale=0.1");
      std::cerr << msg << std::endl;
    }
    rs_min = rs_max = 0.1;
    rs_num = 1;
  }
  
  try {
    rmax = tree.get<double>("rmax.value");
  } catch (pt::ptree_error &error) {
    if (myid==0) {
      std::string msg("Error finding string tag <rmax.value>, setting rmax=1.0");
      std::cerr << msg << std::endl;
    }
    rmax = 1.0;
  }

  boost::optional<std::string> ov = tree.get_optional<std::string>("scale.map");
  std::string mtype = (ov ? ov.get() : "");

  double lmin = log(rs_min), lmax = log(rs_max), drs = 0.0;
  if (rs_num > 1) drs = (lmax - lmin)/(rs_num-1);
  for (unsigned t=0; t<rs_num; t++)  pvec.push_back(exp(lmin + drs*t));

  poly   = BernPolyPtr(new BernPoly(norder-1));
  P      = rs_min;		// Set a default value

  // Instantiate the mapping
  //
  mp = Mapping::create(mtype, P, 0.0, rmax);
  
  inCons = true;
  flux   = FluxInterpBasisPtr(new FluxInterpBasis(tree, this)); // 
  inCons = false;
}

std::vector<double> BernImg::pixel(double a0, double a1, double b0, double b1, 
				   double eps, int maxlev)
{
  std::vector<double> ret;

  QuadTreeIntegrate<BernImg> integrate(this);

  ret = integrate.Integral(a0, a1, b0, b1, 1, eps, maxlev);
  ret.push_back(integrate.Error());
  ret.push_back(integrate.NumEvals());

  return ret;
}


