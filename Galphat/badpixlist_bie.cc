#include <cstdio>
#include "structs.h"

#define STRLEN 80

using namespace BIE::Galphat;

int badpixlist_bie (image_ptr badpix)
{
  FILE *filename;
    
  float x, y;
  char string[STRLEN];

  badpix->z.resize(badpix->naxes[2]*badpix->naxes[1]);

  for (int iy = 0; iy < badpix->naxes[2]; iy++) {
    for (int ix = 0; ix < badpix->naxes[1]; ix++) {
      badpix->z[iy*badpix->naxes[1]+ix] = 0.;
    };
  };
  
  if ((filename = fopen (badpix->name.c_str(), "r")) == (FILE *)0)
    return (1);

  while (fscanf (filename, " %[^\n]", string) != EOF) {
    if (strncmp (string, "#", 1) != 0) {
      sscanf (string, " %f %f", &x, &y);
      int ix = (unsigned long) x;
      int iy = (unsigned long) y;
      if ( ix >= 0 && ix < badpix->naxes[1] && iy >= 0 && 
	   iy < badpix->naxes[2] )
	badpix->z[iy*badpix->naxes[1]+ix] = 1.;
    };
  };
  fclose (filename);
  return (0);
}
