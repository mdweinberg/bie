#include <cstring>
#include <cstdlib>
#include <cmath>
#include <Distribution.h>
#include <Tile.h>
#include "GalphatLikelihoodFunction.h"
#include "Models.H"
#include <gfunction.h>
#include <BIEmpi.h>
#include "Gauss.h"

#include <boost/shared_ptr.hpp>

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::GalphatLikelihoodFunction)

using namespace std;
using namespace BIE;
using namespace BIE::Galphat;

#define BILINEAR

//
// Null constructor
//
GalphatLikelihoodFunction::GalphatLikelihoodFunction() : LikelihoodFunction()
{
  no_psf          = false;
  use_bilinear    = false;
  debug_flux_on   = false;
  debug_flux_solo = false;
  debug_flux_gof  = false;
  debug_flux_rfac = 1.0;
  obj             = 0;
}

//
// Useful constructor
//
GalphatLikelihoodFunction::GalphatLikelihoodFunction
(GalphatParam* pmap, int level) : LikelihoodFunction()
{       
  temperature = 1.0;
  
  // PSF suppress is off by default
  no_psf          = false;

  // 3-shear rotation by default
  use_bilinear    = false;

  // Model image debugging
  debug_flux_on   = false;
  debug_flux_solo = false;
  debug_flux_gof  = false;
  debug_flux_rfac = 1.0;
  imDebug = false;
  nfDebug = 0;

  // This is the class calculating multilevel likelihood function from
  // orignal and poor gridded image data

  Level   = level;

  // Switch between linear and log flux interpolation.  Value in
  // original implementation was 3, but this seemed a bit to high for
  // some applications.

  ncrit   = 2.0;		
  
  obj     = pmap;		// Would probably be better to use a
				// smart pointer here . . .
  
  infile  = obj->getFname();
  psf     = obj->getPsf();
  input   = obj->getInput(1); 

  int xbeg = input->imgsect[0];
  int xend = input->imgsect[1]; 
  int ybeg = input->imgsect[2];
  int yend = input->imgsect[3]; 

  image_ptr data   = obj->getData();
  image_ptr badpix = obj->getBadpix();
  
  // PSF radius in unit of Re. 2*Re is the size of PSF image. You can
  // change this and compile again.  If 2*Re is greater than image size
  // it will be adjusted according to the image size.
  
  double psfr = 20.0;		// Used in default generation below
				// Original value: psfr=50.0
  
  // Use the 2MASS-style Sersic psf if no other PSF is specified

  if (psf->name.compare("none") == 0 && input->shape > 0.0) {

    cout << "Generating 2MASS psf using shape parameter....." << endl;
    psf->naxes[0] = xend - xbeg + 1;
    psf->naxes[1] = yend - ybeg + 1;
    psf->z.resize(psf->naxes[1]*psf->naxes[0]);
    for (int j=0; j<psf->naxes[1]; j++) 
      for (int i=0; i<psf->naxes[0]; i++) 
	psf->z[j*psf->naxes[0]+i] = 0.0;
    
    double n0 = input->psf_n;
    double r0 = input->psf_r;

    cout << "2MASS psf parameter: "      << endl;
    cout << "n0: " << n0 <<" r0: " << r0 << endl;
    
    double dx = 1.0;
    double dy = 1.0;

    //
    // Choose the center
    //
    double x, y, x0, y0;

    if ((psf->naxes[0]%2) != 0) x0 = psf->naxes[0]/2.0;
    else                        x0 = psf->naxes[0]/2.0 + 0.5;

    if ((psf->naxes[1]%2) != 0) y0 = psf->naxes[1]/2.0;
    else                        y0 = psf->naxes[1]/2.0 + 0.5;
    
    double xmax, xmin,  ymax, ymin;
    int psfsizex, psfsizey, xstart, ystart;
    
    if (2.*psfr*r0 > psf->naxes[0] || 2.*psfr*r0 > psf->naxes[1]) {
      xmin     = 0.0;
      ymin     = 0.0;
      xmax     = psf->naxes[0];
      ymax     = psf->naxes[1];
      psfsizex = static_cast<int>(xmax - xmin);
      psfsizey = static_cast<int>(ymax - ymin);
      xstart   = 0;
      ystart   = 0;
    } else {
      xmin = x0  - ceil(psfr*r0) - 0.5;
      ymin = y0  - ceil(psfr*r0) - 0.5;
      xmax = x0  + ceil(psfr*r0);
      ymax = y0  + ceil(psfr*r0);
      psfsizex = static_cast<int>(xmax - xmin);
      psfsizey = static_cast<int>(ymax - ymin);
      xstart   = psf->naxes[0]/2 - ceil(psfr*r0);
      ystart   = psf->naxes[1]/2 - ceil(psfr*r0);
    }
    vector<double> val;        
    
    Sersic twomasspsf(n0, r0, x0, y0, 1.0, 0.0);
    
    for (int j=0; j<psfsizey; j++) {
      y = ymin + dy*j;
      for (int i=0; i<psfsizex; i++) {
	x = xmin + dx*i;
	val = twomasspsf.pixel(x, x+dx, y, y+dy, 1.0e-4, 8);
	psf->z[(j+ystart)*psf->naxes[0]+i+xstart] = static_cast<float>(val[0]);
      }
    }
    
  }        
  
  psfcheck_bie(psf);
  
  this->Setup(data, psf, badpix);
  
}

void GalphatLikelihoodFunction::Setup(image_ptr data, image_ptr psf,
				      image_ptr badpix)
{
  for (Gmodel_ptr 
	 modl=obj->getModelsBegin(); modl.get()!=0; modl=obj->getModelsNext()) 
    {
      modl->begin(data, psf, badpix);
    }


  for (inpars_ptr inp=obj->getInputBegin(); inp.get()!=0; inp=obj->getInputNext()) {
    
    int lev  = inp->level;
    int npix = inp->aggfact;

    /*-----------------------------------------------------------------	\
      |  First cut the section of the image that we want to fit from     |
      |  the original data/sigma images into new "mini-images."  In      |
      |  general the mini-images will be larger than the convolution     |
      |  region.  The mini-images are regions the user specified         |
      |  to fit with no convolution padding added.                       |
      \-----------------------------------------------------------------*/
    
    d       [lev] = this->ToMini (data,   npix, inp);
    mask    [lev] = this->ToMini (badpix, npix, inp);
    mdlmask [lev] = this->ToMini (badpix, npix, inp);
    model   [lev] = image_ptr(new struct image);
    
    
    int abnormal; 
    //  extra masking of data pixel with NaN or Inf
    for (int j=0; j<mask[lev]->naxes[1]; j++) {
      for (int i=0; i<mask[lev]->naxes[0]; i++) {
	abnormal = isfinite(d[lev]->z[j*mask[lev]->naxes[0]+i]);
	if (abnormal == 0) mask[lev]->z[j*mask[lev]->naxes[0]+i] = 1.0;
      }
    }
    
    model[lev]->imgsect    = d[lev]->imgsect;

    model[lev]->naxes[0]   = d[lev]->naxes[0]; 
    model[lev]->naxes[1]   = d[lev]->naxes[1];
    model[lev]->magzpt     = d[lev]->magzpt;
    model[lev]->magi       = d[lev]->magi;
    model[lev]->muzpt      = d[lev]->muzpt;
    model[lev]->gain       = d[lev]->gain;
    model[lev]->rdnoise    = d[lev]->rdnoise;
    model[lev]->ncombine   = d[lev]->ncombine;

    model[lev]->z.resize(model[lev]->naxes[1]*model[lev]->naxes[0]);
    
    int nfrqx        = model[lev]->naxes[0]/2+1;

    int margx        = round<int, double>(0.25*model[lev]->naxes[0]);
    int margy        = round<int, double>(0.25*model[lev]->naxes[1]);
    int Numx         = model[lev]->naxes[0] + 2*margx;
    int Numy         = model[lev]->naxes[1] + 2*margy;
    
    int Nfrqx        = Numx/2+1;
    int Nfrqy        = Numy/2+1;

    in         [lev] .resize(Numy*Numx);
    inbulge    [lev] .resize(Numy*Numx);
    indisk     [lev] .resize(Numy*Numx);
    flux       [lev] .resize(Numy*Numx); 
    fluxbulge  [lev] .resize(Numy*Numx);
    fluxdisk   [lev] .resize(Numy*Numx);
    totalflux  [lev] .resize(Numy*Numx);
    
    mdl        [lev] .resize(model[lev]->naxes[1]*model[lev]->naxes[0]);
    mdlmargin  [lev] .resize(Numy*Numx);

    ftmdl      [lev] .resize(model[lev]->naxes[1]*nfrqx);
    ftmdlmargin[lev] .resize(Numy*Nfrqx);

    xCol       [lev] .resize(Numx);
    yRow       [lev] .resize(Numy);
    
    Fkx        [lev] .resize(Nfrqx);
    Gkx        [lev] .resize(Nfrqx);
    Fky        [lev] .resize(Nfrqy);
    Gky        [lev] .resize(Nfrqy);
    
    kImg       [lev] = fftw_plan_dft_r2c_2d(Numy, Numx, &mdlmargin[lev][0], reinterpret_cast<fftw_complex*>(&ftmdlmargin[lev][0]), FFTW_MEASURE);
    xImg       [lev] = fftw_plan_dft_c2r_2d(Numy, Numx, reinterpret_cast<fftw_complex*>(&ftmdlmargin[lev][0]), &mdlmargin[lev][0], FFTW_MEASURE);

    Rotx       [lev] = fftw_plan_dft_r2c_1d(Numx, &xCol[lev][0], reinterpret_cast<fftw_complex*>(&Fkx[lev][0]), FFTW_MEASURE);
    Roty       [lev] = fftw_plan_dft_r2c_1d(Numy, &yRow[lev][0], reinterpret_cast<fftw_complex*>(&Fky[lev][0]), FFTW_MEASURE);

    invRotx    [lev] = fftw_plan_dft_c2r_1d(Numx, reinterpret_cast<fftw_complex*>(&Gkx[lev][0]), &xCol[lev][0], FFTW_MEASURE);
    invRoty    [lev] = fftw_plan_dft_c2r_1d(Numy, reinterpret_cast<fftw_complex*>(&Gky[lev][0]), &yRow[lev][0], FFTW_MEASURE);
  }
  
}

void GalphatLikelihoodFunction::psfcheck_bie (image_ptr tmppsf)
{
  return;			// Ignore this for now

  int i, j, xoff=0, yoff=0;
  long new_naxes[3];
  std::vector<float> newimg;
  
  if ((tmppsf->naxes[0] % 2) != 0) {
    new_naxes[0] = tmppsf->naxes[0] + 1;
    xoff = 1;
  } else
    new_naxes[0] = tmppsf->naxes[0];
  
  if ((tmppsf->naxes[1] % 2) != 0) {
    new_naxes[1] = tmppsf->naxes[1] + 1;
    yoff = 1;
  } else
    new_naxes[1] = tmppsf->naxes[1];
  
  if (xoff == 1 || yoff == 1) {
    newimg.resize(new_naxes[1]*new_naxes[0]);
    for (j=0; j < new_naxes[1]; j++) {
      for (i=0; i < new_naxes[0]; i++) {
	
	if ( (i==0 && xoff == 1) || (j==0 && yoff == 1 ) )
	  newimg[j*new_naxes[0]+i] = 0.;
	else
	  newimg[j*new_naxes[0]+i] = tmppsf->z[(j-yoff)*(tmppsf->naxes[0])+i-xoff];
      };
    };
    
    tmppsf->naxes[0] = new_naxes[0];
    tmppsf->naxes[1] = new_naxes[1];
    tmppsf->z = newimg;
  };
}


const bool ALT_PSF = false;	// For testing image embedding
const bool PSF_TST = false;	// For PSF testing

GalphatLikelihoodFunction::cv_ptr
GalphatLikelihoodFunction::FFTedPSF(inpars_ptr input_ptr, double angle)
{ 
  // Get image size
  //
  // int aggpix = input_ptr->aggfact;
  // int level  = input_ptr->level;
  
  int xbeg   = input_ptr->imgsect[0];
  int xend   = input_ptr->imgsect[1]; 
  int ybeg   = input_ptr->imgsect[2];
  int yend   = input_ptr->imgsect[3]; 

  long nxaxis, nyaxis;
  nxaxis = xend - xbeg + 1;
  nyaxis = yend - ybeg + 1;
  
  //
  // Zero padding
  //
  int margx = round<int, double>(0.25*nxaxis);
  int margy = round<int, double>(0.25*nyaxis);
  int Numx  = nxaxis + 2*margx;
  int Numy  = nyaxis + 2*margy;
  
  int psfx = psf->Size().first;
  int psfy = psf->Size().second;
  
  
  if (no_psf) {
    //
    // Return the exact DFT of a delta function
    //
    int nfrq  = Numx/2 + 1;
    return cv_ptr(new Cvector(Numy*nfrq, 1.0));
  }
  else if (ALT_PSF) {
    
    //
    // Get PSF normalization
    //
    double sum = 0.0;
    for (int j=0; j < psfy; j++)
      for (int i=0; i < psfx; i++) 
	sum += (*psf)(i, j);
    
    //
    // Making PSF with same size of image data
    //
    
    int xcen  = psfx/2;
    int ycen  = psfy/2;
    int nfrq  = Numx/2 + 1;
    int halfx = std::min<int>(xcen, Numx/2);
    int halfy = std::min<int>(ycen, Numy/2);
  
    // Working image vector
    std::vector<double> imgw(Numx * Numy, 0.0);

    // Embed the image

    // Quad 1
    //
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	imgw[j*Numx + i] = (*psf)(xcen+i, ycen+j);
      }
    }

    // Quad 2
    //
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	imgw[j*Numx + (Numx-1-i)] = (*psf)(xcen-1-i, ycen+j);
      }
    }

    // Quad 3
    //
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	imgw[(Numy-1-j)*Numx + (Numx-1-i)] = (*psf)(xcen-1-i, ycen-1-j);
      }
    }
    
    // Quad 4
    //
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	imgw[(Numy-1-j)*Numx + i] = (*psf)(xcen+i, ycen-1-j);
      }
    }
    
    // Allocate the return vector
    //
    cv_ptr ftpsf(new Cvector(Numy*nfrq));

    // Initialize FFTW
    //
    fftw_plan p = fftw_plan_dft_r2c_2d(Numy, Numx, &imgw[0], reinterpret_cast<fftw_complex*>(&(*ftpsf)[0]), FFTW_ESTIMATE);
    
    // Perform the FFT
    //
    fftw_execute(p);
    fftw_destroy_plan(p);
  
    return ftpsf;
    
  } else {

    image_ptr psfimg;

    if (use_bilinear) psfimg = psf;
    else {
      // Get rotated PSF image
      //
#ifdef BILINEAR
      InterpRotateReal rot(0.0);
#else
      ShearRotateReal rot(0.0);
#endif
      if (PSF_TST) {
	static bool firstime = true;
	if (firstime) {
	  WriteFITS(psf->naxes, psf->z, 0);
	  firstime = false;
	}
      }

      psfimg = boost::dynamic_pointer_cast<image>(rot.Rotate (psf, -angle));

      if (PSF_TST) {
	static bool firstime = true;
	if (firstime) {
	  WriteFITS(psfimg->naxes, psfimg->z, 0);
	  firstime = false;
	}
      }
    }

    int aggpix = input_ptr->aggfact;
    // int level  = input_ptr->level;
  
    int xbeg   = input_ptr->imgsect[0];
    int xend   = input_ptr->imgsect[1]; 
    int ybeg   = input_ptr->imgsect[2];
    int yend   = input_ptr->imgsect[3]; 
    
    long nxaxis, nyaxis;
    nxaxis = xend - xbeg + 1;
    nyaxis = yend - ybeg + 1;

    //
    // Zero padding
    //
    int margx = round<int, double>(0.25*nxaxis);
    int margy = round<int, double>(0.25*nyaxis);
    int Numx  = nxaxis + 2*margx;
    int Numy  = nyaxis + 2*margy;
  
    //
    // Get normalization
    //
    float sum=0.0;
    for (int j=0; j < psfimg->naxes[1]; j++)
      for (int i=0; i < psfimg->naxes[0]; i++) 
	sum += psfimg->z[j*psfimg->naxes[0]+i];
    
    // PSF image shift for compensating aggregation
    // if 2 by 2 aggregation, image shifted by 0.5 pixel
    // if 3 by 3 aggregation, image shifted by 1.0 pixel

    double dx0 = 0.5*(aggpix-1); 
    double dy0 = 0.5*(aggpix-1); 
    
    int xnum = static_cast<int>(psfimg->naxes[0]);
    int ynum = static_cast<int>(psfimg->naxes[1]);
  
    //  +--- For debugging PSF embedding
    //  |
    //  v
    if (PSF_TST) {
      static bool firstime = true;
      if (firstime) {
	WriteFITS(psfimg->naxes, psfimg->z, 0);
	firstime = false;
      }
    }

    //
    // Making new PSF (mypsf) with same size as original PSF
    //
    image_ptr mypsf = image_ptr(new image);
    
    mypsf->naxes   = psf->naxes;
    mypsf->imgsect = psf->imgsect;
    mypsf->z       = psf->z;

    if (aggpix > 1) {
      std::vector<double> temp(xnum*ynum);
      std::copy(mypsf->z.begin(), mypsf->z.end(), temp.begin());
      ShiftImage(temp, dx0, dy0, xnum, ynum);
      std::copy(temp.begin(), temp.end(), mypsf->z.begin());
    }

    if (PSF_TST) {
      static bool firstime = true;
      if (firstime) {
	WriteFITS(mypsf->naxes, mypsf->z, 0);
	firstime = false;
      }
    }

    //
    // PSF image Aggregation 
    //
    image_ptr psfim = this->ToMini(mypsf, aggpix, input_ptr);
    
    int xcen   = mypsf->naxes[0]/2;
    int ycen   = mypsf->naxes[1]/2;
    int nfrq   = Numx/2 + 1;
    int halfx  = std::min<int>(xcen, Numx/2);
    int halfy  = std::min<int>(ycen, Numy/2);
    
    fftw_plan p;

    std::vector<double> img(Numy*Numx);
    cv_ptr ftimg(new Cvector(Numy*nfrq));
    
    p = fftw_plan_dft_r2c_2d(Numy, Numx, &img[0], reinterpret_cast<fftw_complex*>(&(*ftimg)[0]), FFTW_ESTIMATE);
    
    
    // 0 Padding PSF to size of image
    
    //---------------------------------------------
    // Before FFT, Distribute PSF to each quadrant
    //---------------------------------------------
    
    // corner 1 top right of orig to bottom left of new
    
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	img[j*Numx + i] = (*mypsf)(xcen+i, ycen+j);
      }
    }

    // corner 2 bottom left pf orig to top right of new
    
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	img[j*Numx + (Numx-1-i)] = (*mypsf)(xcen-1-i, ycen+j);
      }
    }

    // corner 3 bottom right of orig to top left of new 
  
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	img[(Numy-1-j)*Numx + (Numx-1-i)] = (*mypsf)(xcen-1-i, ycen-1-j);
      }
    }

    // corner 4 top left of orig to bottom right of new 
    
    for (int j=0; j<halfy; j++) {
      for (int i=0; i<halfx; i++) {
	img[(Numy-1-j)*Numx + i] = (*mypsf)(xcen+i, ycen-1-j);
      }
    }
    
    //  +--- For debugging PSF embedding
    //  |
    //  v
    if (PSF_TST) {
      static bool firstime = true;
      if (firstime) {
	std::vector<long> naxes = {Numx, Numy};
	std::vector<float> temp(Numx*Numy);
	std::copy(img.begin(), img.end(), temp.begin());
	WriteFITS(naxes, temp, 0);
	firstime = false;
      }
    }

    fftw_execute(p);
    fftw_destroy_plan(p);
  
    return ftimg;
  }
}

GalphatLikelihoodFunction::~GalphatLikelihoodFunction()
{
  // Clean up from FFTW
  //
  for (auto & v : xImg)    fftw_destroy_plan(v.second);
  for (auto & v : kImg)    fftw_destroy_plan(v.second);
  for (auto & v : Rotx)    fftw_destroy_plan(v.second);
  for (auto & v : Roty)    fftw_destroy_plan(v.second);
  for (auto & v : invRotx) fftw_destroy_plan(v.second);
  for (auto & v : invRoty) fftw_destroy_plan(v.second);
}

//
// The only variable where care about here is <State* s>
//
double GalphatLikelihoodFunction::LikeProb
(vector<double> &z, 
 SampleDistribution* sd, double norm, Tile *t, State *sta, int indx)
{ 
  
  double val   = 0.0;
  double Re_b  = 1.0;
  double mag_b = 1.0;

  this->UpdateGalphatVector(sta);

  inpars_ptr tmp_input = obj->getInput(Level-current_level);
  
  if (Level > 1) {
    
    int mylevel=1;
    
    mylevel = Level - current_level;

    MakeModel(model[mylevel], tmp_input, mag_b, Re_b);
    debugFlux(model[mylevel], d[mylevel], mask[mylevel]);

    val = this->LikelihoodCalc(mask[mylevel]->z, 
			       mdlmask[mylevel]->z, 
			       d[mylevel]->z, 
			       model[mylevel]->z, 
			       model[mylevel]->naxes, 
			       d[mylevel]->gain, 
			       d[mylevel]->ncombine, 
			       d[mylevel]->rdnoise);
  } else if (Level == 1) {

    MakeModel(model[1], tmp_input, mag_b, Re_b);
    debugFlux(model[1], d[1], mask[1]);
    

    val = this->LikelihoodCalc(mask[1]->z, 
			       mdlmask[1]->z, 
			       d[1]->z, 
			       model[1]->z,
			       model[1]->naxes, 
			       d[1]->gain, 
			       d[1]->ncombine, 
			       d[1]->rdnoise);
  }
  
  
  return (val);
}


void GalphatLikelihoodFunction::MakeModel 
(image_ptr img, inpars_ptr input, double mag_b, double Re_b)
{

  int level    = input->level;
  int numx     = img->naxes[0];
  int numy     = img->naxes[1];

  Timer one(true), two(true), three(true);
  
  for (int j=0; j<numy; j++) {
    for (int i=0; i<numx; i++) {
      totalflux[level][j*numx+i]  = 0.0;
      mdlmask[level]->z[j*numx+i] = 0.0;
    }
  }  

  /** Change in logic from Ilsang's original implementation:
      o x0common, y0common are set from model types "Single" and "BulgeDisk"
        if ia[0], ia[1] are >= 0
      o a[0] and a[1] are set from x0common, y0common if ia[0], ia[1] are < 0
      o Therefore, the first GalphatModel stanza must have ia[0], ia[1]>=0 or 
        no stanzas can have ia[0], ia[1] >= 0
  */
  
  vector<double> retV;

  for (Gmodel_ptr 
	 modl=obj->getModelsBegin(); modl.get()!=0; modl=obj->getModelsNext()) 
    {
      modl->setup(input, img);
      modl->assignFlux(this, level, input, img);
    }

  for (int j=0; j<numy; j++) {
    for (int i=0; i<numx; i++) {
      if (totalflux[level][j*numx+i]<0.0 || 
	  std::isnan(totalflux[level][j*numx+i])) mdlmask[level]->z[j*numx+i] = 1.0;
      img->z[j*numx+i] = static_cast<float>(totalflux[level][j*numx+i]);
    }
  }
  
}

void GalphatLikelihoodFunction::ShiftImage
(std::vector<double>& im, double delx, double dely, int numx, int numy)
{
  double m     = 0.0;
  double alpha = 0.0;
  double beta  = 0.0;

  std::vector<double> imgx(numy*numx);
  std::vector<double> imgy(numy*numx);
  
  int nfrqx = numx/2+1;
  int nfrqy = numy/2+1;
  
  std::vector<double> xcol(numx), yrow(numy);
  Cvector fkx(nfrqx), gkx(nfrqx), fky(nfrqy), gky(nfrqy);

  fftw_plan Sftx, Sfty, invSftx, invSfty;

  Sftx    = fftw_plan_dft_r2c_1d(numx, &xcol[0], reinterpret_cast<fftw_complex*>(&fkx[0]), FFTW_ESTIMATE);
  Sfty    = fftw_plan_dft_r2c_1d(numy, &yrow[0], reinterpret_cast<fftw_complex*>(&fky[0]), FFTW_ESTIMATE);
  invSftx = fftw_plan_dft_c2r_1d(numx, reinterpret_cast<fftw_complex*>(&gkx[0]), &xcol[0], FFTW_ESTIMATE);
  invSfty = fftw_plan_dft_c2r_1d(numy, reinterpret_cast<fftw_complex*>(&gky[0]), &yrow[0], FFTW_ESTIMATE);

  imgx = im;
  
  Complex endp(cos(-M_PI*m), sin(-M_PI*m));

  for (int j=0; j<numy; j++) {
    m = delx;
    memcpy(&xcol[0], &imgx[j*numx], sizeof(double)*numx);
    
    fftw_execute(Sftx);

    alpha = 2.*sin(-1.*M_PI*m/numx)*sin(-1.*M_PI*m/numx);
    beta = sin(-2.*M_PI*m/numx);
    
    Complex kk(1.0, 0.0), ab(1.0-alpha, beta);

    gkx[0] = fkx[0].real() * kk / (double)numx;

    for (int i=1; i<numx/2+1; i++) {
      kk *= ab;
      gkx[i] = fkx[i] * kk / (double)numx;
    }
    if (numx%2 == 0) {
      gkx[numx/2] = fkx[numx/2] * endp / (double)numx;
    }

    fftw_execute(invSftx);
    memcpy(&imgx[j*numx], &xcol[0], sizeof(double)*numx);
  }

  imgy = imgx;
  
  for (int i=0;i<numx;i++) {
    m = dely;
    memcpy(&yrow[0], &imgy[i*numy],sizeof(double)*numy);
    
    fftw_execute(Sfty);

    alpha = 2.*sin(-1.*M_PI*m/numy)*sin(-1.*M_PI*m/numy);
    beta  = sin(-2.*M_PI*m/numy);
    
    Complex kk(1.0, 0.0), ab(1.0-alpha, beta);

    gky[0] = fky[0].real() * kk / (double)numy;
    
    for (int j=1; j<numy/2+1; j++) {
      kk *= ab;
      gky[j] = fky[j] * kk / (double)numy;
    }
    if(numy%2 == 0) {
      gky[numy/2] = fky[numy/2] * endp / (double)numy;
    }

    fftw_execute(invSfty);
    memcpy(&imgy[i*numy], &yrow[0], sizeof(double)*numy);
  }
  
  im = imgy;

  fftw_destroy_plan(Sftx);
  fftw_destroy_plan(Sfty);
  fftw_destroy_plan(invSftx);
  fftw_destroy_plan(invSfty);
}


void GalphatLikelihoodFunction::ProcessImage
(std::vector<double>& in, std::vector<double>& out,
 int numx, int numy, int Numx, int Numy, 
 double Xmin, double Ymin, double phi, double x0, double y0,
 inpars_ptr input, bool logscl)
{
  const double base = 1e-30;

  double scale = 1.0/(Numx * Numy);

  if (use_bilinear) {
				// Make an image ptr
    image_ptr tImg(new image);
				// Set to the padded image size
    tImg->reset(Numx, Numy);

    //------------------------------------------------------------
    // Scale image values before rotation
    //------------------------------------------------------------

    for (int j=0; j<Numy; j++) {

      for (int i=0; i<Numx; i++) {

	double val = in[j*Numx+i];
	
	if (logscl) {		// Make sure flux > 0 for log scaling
	  if ( val <= 0.0 ) val = base;
	  (*tImg)(i, j) = log(val);
	} else {
	  (*tImg)(i, j) = val;
	}
      }
    }
    
    //------------------------------------------------------------
    // Perform image rotation
    //------------------------------------------------------------

    double backgr = logscl ? log(base) : base;
    InterpRotateReal rot(backgr);

    int margx = round<int, double>(0.25*numx);
    int margy = round<int, double>(0.25*numy);

    tImg = boost::dynamic_pointer_cast<image>
      (rot.Rotate (tImg, phi, x0+margx, y0+margy));

    //------------------------------------------------------------
    // Scale up after rotation
    //------------------------------------------------------------

				// Set to true for model image debugging
				// Set to false for production
    const bool iDebug = false;

    for (int j=0; j<Numy; j++) {
      for (int i=0;i<Numx; i++) {
	if (logscl) {
	  mdlmargin[Level][j*Numx+i] = exp((*tImg)(i, j));
	} else {
	  mdlmargin[Level][j*Numx+i] = (*tImg)(i, j);
	}
	if (iDebug) {
	  if (std::isnan(mdlmargin[Level][j*Numx+i]))
	    (*tImg)(i, j) = 0.0;
	  else
	    (*tImg)(i, j) = mdlmargin[Level][j*Numx+i];
	}
      }
    }
    
    if (iDebug) {
      std::ostringstream sout; sout << "testme1_" << myid << ".fits";
      tImg->writeFITS(sout.str());
    }
    
    //------------------------------------------------------------
    // Get PSF in Fourier space
    //------------------------------------------------------------

    cv_ptr fftpsf = FFTedPSF(input, 0.0);

    //------------------------------------------------------------
    // Do FFT of image
    //------------------------------------------------------------

				// Compute image FFT
    fftw_execute(kImg[Level]);
      
    for (int j=0; j<Numy; j++) {
      for (int i=0; i<Numx/2+1; i++) {
	int ij = j*(Numx/2+1) + i;
	ftmdlmargin[Level][ij] *= (*fftpsf)[ij] * scale;
      }
    }
    
    fftw_execute(xImg[Level]);
      
    //------------------------------------------------------------
    // Trim and copy back to model array
    //------------------------------------------------------------

    bool bad = false;

    for (int j=margy; j<numy+margy; j++) {
      for (int i=margx; i<numx+margx; i++) {
	int IJ = j*Numx + i;
	int ij = (j-margy)*numx+(i-margx);
	out[ij] = mdlmargin[Level][IJ]; 
	if (std::isnan(mdlmargin[Level][IJ])) {
	  bad = true;
	}
      }
    }

    if (iDebug and bad) {
      double minPix =  1.0e30;
      double maxPix = -1.0e30;

      for (int j=0; j<Numy; j++) {
	for (int i=0; i<Numx; i++) {
	  if (std::isnan(mdlmargin[Level][j*Numx + i])) 
	    (*tImg)(i, j) = 0.0;
	  else
	    (*tImg)(i, j) = mdlmargin[Level][j*Numx + i]; 

	  minPix = std::min<double>(minPix, (*tImg)(i, j));
	  maxPix = std::max<double>(maxPix, (*tImg)(i, j));
	}
      }

      std::ostringstream sout; sout << "testme2_" << myid << ".fits";
      tImg->writeFITS(sout.str());
    }

  } // END: bilinear rotation variant
  else {

    memcpy(&mdlmargin[Level][0], &in[0], sizeof(double)*Numy*Numx);

    //------------------------------------------------------------
    // Get rotated PSF
    //------------------------------------------------------------

    cv_ptr fftpsf = FFTedPSF(input, phi);

    //------------------------------------------------------------
    // Do FFT
    //------------------------------------------------------------

    fftw_execute(kImg[Level]);
      
    for (int j=0; j<Numy; j++) {
      for (int i=0; i<Numx/2+1; i++) {
	int ij = j*(Numx/2+1) + i;
	ftmdlmargin[Level][ij] *= (*fftpsf)[ij] * scale;
      }
    }
    
    fftw_execute(xImg[Level]);
      
    for (int j=0; j<Numy; j++) {
      for (int i=0;i<Numx; i++) {
	double val = mdlmargin[Level][j*Numx+i];

	if (logscl) {
				// Make sure flux > 0 for log scaling
	  if( val <= 0.0 ) val = base;
	  mdlmargin[Level][j*Numx+i] = log(val);
	} else {
	  mdlmargin[Level][j*Numx+i] = val;
	}
      }
    }
    
    RotateImage(mdlmargin[Level], mdl[Level],
		numx, numy, Numx, Numy, 
		Xmin, Ymin, phi, x0, y0, input);

    //------------------------------------------------------------
    // scale up after rotation
    //------------------------------------------------------------
    
    for (int j=0; j<numy; j++) {
      for (int i=0; i<numx; i++) {
	if (logscl) {
	  out[j*numx+i] = exp(mdl[Level][j*numx+i]);  
	} else {
	  out[j*numx+i] = mdl[Level][j*numx+i];
	}
      }
    }

  } // END: 3-shear variant
}

void GalphatLikelihoodFunction::RotateImage
(std::vector<double>& in, std::vector<double>& out,
 int numx, int numy, int Numx, int Numy, 
 double Xmin, double Ymin, double phi, double x0, double y0, 
 inpars_ptr input)
{
  double a = -1.*tan(phi/2.);
  double b =  sin(phi);
  
  int aggpix = input->aggfact;
  int level  = input->level;
  
  // Pixel center
  double xoff, yoff;
  xoff = 0.5 + (aggpix-1)/2.0;
  yoff = 0.5 + (aggpix-1)/2.0;
  
  // For zero padding
  int margx = round<int, double>(0.25*numx);
  int margy = round<int, double>(0.25*numy);
  
  std::vector<double> gx(Numy*Numx);
  std::vector<double> gy(Numx*Numy);
  
  double alpha, beta;
  double x, y, m;
  
  int m0;
  
  // Note::Xmin and Ymin should be a mid-point! in either aggregate or
  // original image

  Xmin = Xmin + xoff;
  Ymin = Ymin + yoff;

  //
  // X shear
  //
  for (int j=0;j<Numy;j++) {
    int Npix = Numx;
    int Nfrq = Npix/2+1;

    y  = Ymin + j*aggpix;
    m  = a*(y-y0)/aggpix;
    m0 = round<int, double>(m);
    
    memcpy(&xCol[level][0], &in[j*Npix], sizeof(double)*Npix);
    
    fftw_execute(Rotx[level]);

    alpha = 2.0*sin(-1.0*M_PI*m/Npix)*sin(-1.0*M_PI*m/Npix);
    beta  = sin(-2.*M_PI*m/Npix);
    
    Complex kk(1.0, 0.0), ab(1.0 - alpha, beta);

    Gkx[Level][0] = Fkx[level][0].real() * kk / (double)Npix;
    
    for (int i=1; i<Nfrq; i++) {
      kk *= ab;
      Gkx[level][i] = Fkx[level][i] * kk / (double)Npix;
    }

    if(Npix%2 == 0) {
      Gkx[level][Npix/2] = Fkx[level][Npix/2] * Complex(cos(-M_PI*m0), sin(-M_PI*m0)) / (double)Npix;
    }
    
    fftw_execute(invRotx[level]);
    memcpy(&gx[j*Npix], &xCol[level][0], sizeof(double)*Npix);
  } 
  
  for (int i=0;i<Numx;i++) 
    for (int j=0; j<Numy; j++) gy[i*Numy+j] = gx[j*Numx+i];

  // Y shear
  
  for (int i=0;i<Numx;i++) {
    int Npix = Numy;
    int Nfrq = Npix/2+1;

    x  = Xmin+i*aggpix;
    m  = b*(x-x0)/aggpix;
    m0 = round<int, double>(m);
    memcpy(&yRow[level][0],&gy[i*Npix],sizeof(double)*Npix);
    
    fftw_execute(Roty[level]);

    alpha = 2.0*sin(-1.*M_PI*m/Npix)*sin(-1.*M_PI*m/Npix);
    beta  = sin(-2.*M_PI*m/Npix);
    
    Complex kk(1.0, 0.0), ab(1.0 - alpha, beta);

    Gky[level][0] = Fky[level][0].real() * kk / (double)Npix;
    
    for (int j=1; j<Nfrq; j++) {
      kk *= ab;
      Gky[level][j] = Fky[level][j] * kk / (double)Npix;
    }
    if(Npix%2 == 0) {
      Gky[level][Npix/2] = Fky[level][Npix/2] * Complex(cos(-M_PI*m0), sin(-M_PI*m0))/ (double)Npix;
    }
    
    fftw_execute(invRoty[level]);
    memcpy(&gy[i*Npix], &yRow[level][0], sizeof(double)*Npix);
  } 
  
  for (int j=0; j<Numy; j++) 
    for (int i=0;i<Numx;i++) 
      gx[j*Numx+i]=gy[i*Numy+j];
  
  //
  // X shear again
  //
  for (int j=0; j<Numy;j ++) {
    int Npix=Numx;
    int Nfrq = Npix/2+1;
    y = Ymin+j*aggpix;

    m  = a*(y-y0)/aggpix;
    m0 = round<int, double>(m);
    memcpy(&xCol[level][0],&gx[j*Npix],sizeof(double)*Npix);
    
    fftw_execute(Rotx[level]);

    alpha = 2.0*sin(-1.0*M_PI*m/Npix)*sin(-1.0*M_PI*m/Npix);
    beta  = sin(-2.0*M_PI*m/Npix);

    Complex kk(1.0, 0.0), ab(1.0 - alpha, beta);
    
    Gkx[level][0] = Fkx[level][0].real() * kk / (double)Npix;
    
    for (int i=1; i<Nfrq; i++) {
      kk *= ab;
      Gkx[level][i] = Fkx[level][i] * kk / (double)Npix;
    }

    if(Npix%2 == 0) {
      Gkx[level][Npix/2] = Fkx[level][Npix/2] * Complex(cos(-M_PI*m0), sin(-M_PI*m0))/ (double)Npix;
    }
    
    fftw_execute(invRotx[level]);
    memcpy(&in[j*Npix], &xCol[level][0], sizeof(double)*Npix);
  } 
  
  //
  // trim and copy to *out
  //
  for (int j=margy; j<numy+margy; j++) {
    for (int i=margx; i<numx+margx; i++) {
      int IJ = j*Numx + i;
      int ij = (j-margy)*numx+(i-margx);
      out[ij] = in[IJ]; 
    }
  }

  for (int j=0; j<margy; j++) {
    for (int i=0; i<numx; i++) {
      int IJ = (j+margy)*Numx + (i+margx);
      int ij = j*numx + i;
      out[ij] = in[IJ]; 
    }
  }
}

void GalphatLikelihoodFunction::UpdateGalphatVector(State *s)
{
  for (Gmodel_ptr mdl=obj->getModelsBegin(); mdl.get()!=0; mdl=obj->getModelsNext()) {
    for (auto &a : mdl->a) {
      int i = a.first;
      if (mdl->ia[i] >= 0) {
	a.second = static_cast<float>((*s)[i]);
      }
    }
  }
}

/******************************************************************************/

image_ptr GalphatLikelihoodFunction::ToMini 
(image_ptr img, int npix, inpars_ptr input)
{
  int xmin, xmax, ymin, ymax;
  image_ptr new_img  = image_ptr(new struct image);
  image_ptr new_img2 = image_ptr(new struct image);

  if (new_img.get() == 0 || new_img2.get() == 0) {
    cout << "Error in allocating memory for mini-image...." << endl;
    exit (1);
  };
  
  new_img->exptime   = img->exptime;
  new_img->magzpt    = img->magzpt;
  new_img->magi      = img->magi;
  new_img->muzpt     = img->muzpt;
  new_img->dp[0]     = img->dp[0];
  new_img->dp[1]     = img->dp[1];
  new_img->gain      = img->gain;
  new_img->rdnoise   = img->rdnoise;
  new_img->ncombine  = img->ncombine;
  new_img->err       = img->err;
  
  new_img2->exptime  = img->exptime;
  new_img2->magzpt   = img->magzpt;
  new_img2->magi     = img->magi;
  new_img2->muzpt    = img->muzpt;
  new_img2->dp[0]    = img->dp[0];
  new_img2->dp[1]    = img->dp[1];
  new_img2->gain     = img->gain;
  new_img2->rdnoise  = img->rdnoise;
  new_img2->ncombine = img->ncombine;
  new_img2->err      = img->err;
  
  xmin = img->imgsect[0];
  xmax = img->imgsect[1];
  ymin = img->imgsect[2];
  ymax = img->imgsect[3];

  for (int i=0; i<4; i++) 
    new_img2->imgsect[i] = new_img->imgsect[i] = img->imgsect[i];

  new_img->naxes[0]  = xmax - xmin + 1;
  new_img->naxes[1]  = ymax - ymin + 1;
  
  new_img2->naxes[0]  = new_img->naxes[0]/npix;
  new_img2->naxes[1]  = new_img->naxes[1]/npix;
  

  new_img->z.resize(new_img->naxes[1]*new_img->naxes[0]);
  
  for (int j=ymin; j <= ymax; ++j){
    for (int i=xmin; i <= xmax; ++i)
      new_img->z[(j-ymin)*new_img->naxes[0]+i-xmin] = img->z[(j-1)*img->naxes[0]+i-1];
  }
  
  if (npix>1) {
    new_img2->z.resize(new_img2->naxes[1]*new_img2->naxes[0]);
    for (int j=0; j < new_img2->naxes[1]; ++j) {    
      for (int i=0; i < new_img2->naxes[0]; ++i) {
	new_img2->z[j*new_img2->naxes[0]+i]=0.0;
	for (int k=0; k < npix; ++k) 
	  for (int m=0; m < npix; ++m) 
	    new_img2->z[j*new_img2->naxes[0]+i] += 
	      new_img->z[(npix*j+k)*new_img->naxes[0]+(npix*i+m)];	
      }
    }
    return new_img2;
  } else {
    return new_img;
  }
}

/*****************************************************************************/
double GalphatLikelihoodFunction::LikelihoodCalc 
(std::vector<float>&inmask, std::vector<float>&tmpmask, 
 std::vector<float>&indata, std::vector<float>&my_model, 
 std::vector<long>& naxes, float gain, float ncomb, float rdnoise)
{
  double like  = 0.0;
  double fact1 = 0.0;
  double fact2 = 0.0;
  
  unsigned long mdl_badpix_cnt  = 0;
  unsigned long data_badpix_cnt = 0;
  
  for (int iy=0; iy<naxes[1]; iy++) {
    for (int ix=0; ix<naxes[0]; ix++) {
      if (my_model[iy*naxes[0]+ix] < 0.0 || tmpmask[iy*naxes[0]+ix] != 0.0) {
	cout << "Oops, negative or NAN flux value in model:  "      << endl
	     << "  ** Model flux: " << my_model[iy*naxes[0]+ix]     << endl
	     << "  ** Xindex: "     << ix                           << endl
	     << "  ** Yindex: "     << iy                           << endl; 
	mdl_badpix_cnt++;
      }
      if (inmask[iy*naxes[0]+ix] != 0) {
	data_badpix_cnt++;
      }
      
      if (inmask[iy*naxes[0]+ix] == 0.0 && tmpmask[iy*naxes[0]+ix] == 0.0) { 
	
	fact1 -= 0.5*gain*gain*
	  (indata[iy*naxes[0]+ix] - my_model[iy*naxes[0]+ix])*
	  (indata[iy*naxes[0]+ix] - my_model[iy*naxes[0]+ix])/
	  ((my_model[iy*naxes[0]+ix]*gain + rdnoise*rdnoise)/ncomb);

	fact2 -= log(sqrt(2.0*M_PI*
			  (my_model[iy*naxes[0]+ix]*gain + rdnoise*rdnoise)
			  /ncomb) );
      };
    };
  };
  
  // Debugging

  if (imDebug && mdl_badpix_cnt) WriteFITS(naxes, my_model, mdl_badpix_cnt);

   // Compute final likelhood value

   like = fact1 + fact2;

   // TRY Likelihood smooth by powering up

   like=like/temperature;

   //    cout << "log fact1 " << fact1 << endl;
   //    cout << "log fact2 " << fact2 << endl;
   //    cout << "log fact3 " << fact3 << endl;
   //    cout << "total log likelihood " << like << endl;
   //    cout << "chi2/DOF " << -1.*fact1/dof << endl;
   //    cout << "fraction of bad (NAN or negative) pixel " << frac << endl;
   //    cout << "gain, ncomb, rdnoise " << gain <<" "<< ncomb << " " << rdnoise << endl;

   return (like);
 }


//
// Used to label output
//
const std::string 
GalphatLikelihoodFunction::ParameterDescription(int i)
{
  for (Gmodel_ptr 
	 mdl=obj->getModelsBegin(); mdl.get()!=0; mdl=obj->getModelsNext()) {
    
    for (auto v : mdl->parname) {
				// Found an index match, return string
      if (v.first == i) return v.second;
    }
  }
  
  std::ostringstream sout;	// No match, return a generic label
  sout << "Comp(" << i+1 << ")";

  return sout.str();
}


void GalphatLikelihoodFunction::WriteFITS(std::vector<long>& naxes,
					  std::vector<float>& image,
					  unsigned long badpix)
{
  std::ostringstream fname;
  fname << nametag << ".galphat.n" << myid << "_" << nfDebug++ << ".fits";
  
  fitsfile  *ofptr;
  long      onaxis = 2;
  int       bitpix = FLOAT_IMG;
  int       status = 0;

  if (fits_create_file(&ofptr, fname.str().c_str(), &status)) 
    cout << "Error when creating file <" << fname.str() << ">" << endl;
  
  if (fits_create_img (ofptr, bitpix, onaxis, &naxes[0], &status)) 
    cout << "Error when creating img" << endl;
  
  //! for C-language interface
  
  char c1[300], c2[300];
  strncpy(c1, "NBADPIX", 300);
  strncpy(c2, "Number of bad pixels", 300);

  if (fits_update_key(ofptr, TLONG, c1, &badpix, c2, &status))
    cout << "Error updating <NBADPIX> key" << endl;

  unsigned cnt = 1;

  for (Gmodel_ptr 
	 mdl=obj->getModelsBegin(); mdl.get()!=0; mdl=obj->getModelsNext()) {

    for (size_t i=0; i<mdl->a.size(); i++) {

      if (mdl->ia[i] >= 0) {

	std::ostringstream name;
	name << mdl->objtype << "_" << cnt << "_" << i;

	strncpy(c1, name.str().c_str(), 300);
	strncpy(c2, mdl->parname[i].c_str(), 300);

	if (fits_update_key(ofptr, TFLOAT, c1, &mdl->a[i], c2, &status))
	  cout << "Error updating <" << mdl->parname[i].c_str() << "> key" 
	       << endl;
      }
    }
  }

  if (fits_write_img(ofptr, TFLOAT, 1, naxes[0]*naxes[1], &image[0], &status)) 
    cout << "Error when writing" << endl;
  
  fits_close_file(ofptr, &status);
  fits_report_error(stderr, status);

  // Done!
}


void GalphatLikelihoodFunction::WriteFITS2(std::vector<long>& naxes,
					   std::vector<float>& im1,
					   std::vector<float>& im2,
					   unsigned nindex)
{
  std::ostringstream fname;
  if (debug_flux_solo) {
    if (myid) return;
    fname << debug_flux_name << "." << nametag << ".cube.fits";
  } else {
    fname << nametag << ".cube.n" << myid << "_" << nindex << ".fits";
  }
  
  fitsfile  *ofptr;

  int     status = 0;
  long      dims = 3;
  long    images = 4;
  long    naxis1 = naxes[0];
  long    naxis2 = naxes[1];
  long numdim[3] = {naxis1, naxis2, images};
  long fpixel[3] = {1, 1, 1};
  long    impix  = naxis1 * naxis2;
  long    numpix = impix * images;
  
  if (fits_create_file(&ofptr, fname.str().c_str(), &status)) 
    cout << "GalphatLikelihoodFunction::WriteFITS2: "
	 << "Error when creating file <" << fname.str() << ">" << endl;
  
  std::vector<float> z(numpix, 0.0);
  
  for (int j=0; j<naxis2; j++) {
    for (int i=0; i<naxis1; i++) {
      int ij = j*naxis1 + i;
      z[ij + impix*0] = im1[ij];
      z[ij + impix*1] = im2[ij];
      z[ij + impix*2] = im2[ij] - im1[ij];
      z[ij + impix*3] = (im2[ij] - im1[ij])/im2[ij];
    }
  }

  int bitpix = -32;

  if (fits_create_img(ofptr, bitpix, dims, numdim, &status))
    cout << "GalphatLikelihoodFunction::WriteFITS2: "
	 << "Error when creating image" << endl;

  static char comments[4][68] = {
    "Image #1 is the computed model image",
    "Image #2 is the input data image for comparison",
    "Image #3 is the data image with the computed model subtracted",
    "Image #4 is the model subtracted divided by the data image"
  };
    
  for (int i=0; i<4; i++) {
    if (fits_write_comment(ofptr, comments[i], &status)) 
      cout << "GalphatLikelihoodFunction::WriteFITS2: "
	   << "Error writing new comment #" << i+1 << endl;
  }
  
  if (fits_write_history (ofptr, "Created by BIE's GALPHAT", &status) )
    cout << "GalphatLikelihoodFunction::WriteFITS2: "
	 << "Error writing history" << endl;
  
  if (fits_write_pix (ofptr, TFLOAT, fpixel, numpix, &z[0], &status))
    cout << "GalphatLikelihoodFunction::WriteFITS2: "
	 << "Error when creating data cube" << endl;
  
  fits_close_file(ofptr, &status);
  fits_report_error(stderr, status);

  // Done!
}


void GalphatLikelihoodFunction::debugFlux
(image_ptr model, image_ptr data, image_ptr mask)
{
  // ------------------------------------------------
  // Print first <total> images with a stride <strid>
  // ------------------------------------------------
  if (debug_flux_on) {

    // Check if we want to compute an image at this iteration?
    //
    bool this_one = 
      (debug_flux_index < debug_flux_total)
      and
      (debug_flux_count % debug_flux_strid == 0);

    // Compute if flagged by singleImage() or sampleImages()
    //
    if (debug_flux_solo or this_one) {
      WriteFITS2(model->naxes, model->z, data->z, debug_flux_index++);
      if (debug_flux_gof) computeGOF(model, data, mask);
    }

    // Increment image counter
    //
    debug_flux_count++;

    // Reset single image flag
    //
    debug_flux_solo = false;
  }
}

void GalphatLikelihoodFunction::computeGOF
(image_ptr model, image_ptr data, image_ptr mask)
{
  if (myid) return;

  // Image dimensions
  //
  long naxis1 = model->naxes[0];
  long naxis2 = model->naxes[1];

  // Database of model parameters
  //
  std::map<std::string, std::pair<int, std::string> > found {
    {debug_flux_xcen, {-1, "x-center"}},
    {debug_flux_ycen, {-1, "y-center"}},
    {debug_flux_pa,   {-1, "position angle"}},
    {debug_flux_q,    {-1, "axis ratio"}},
    {debug_flux_rad,  {-1, "radius"}}
  };
  

  // Look for the named fields in model's field list
  //
  for (Gmodel_ptr mdl=obj->getModelsBegin(); mdl.get()!=0; mdl=obj->getModelsNext()) {
    
    std::map<std::string, int> fields = mdl->getFieldNames();

    for (auto & v : found) {
      if (v.second.first < 0.0) {
	auto it = fields.find(v.first);
	if (it != fields.end()) v.second.first = it->second;
      }
    }
  }
  

  // Look for image center
  //
  double xoffset=0, yoffset=0, paoffset=0, qoffset=0, rscale=1.0;
  for (inpars_ptr inp=obj->getInputBegin(); inp.get()!=0; inp=obj->getInputNext()) {
    if (Level  == inp->level) {
      xoffset  = inp->xoffset;
      yoffset  = inp->yoffset;
      paoffset = inp->paoffset;
      qoffset  = inp->qoffset;
      rscale   = inp->rscale;
    }
  }
    
  // Make sure all fields have been found
  //
  bool good = true;
  for (auto v : found) {
    if (v.second.first<0) {
      good = false;
      std::cout << "GalphatLikelihoodFunction::computeGOF: "
		<< "could not find " << v.second.second << " "
		<< "named <" << v.first << ">" << std::endl;
    }
  }

  if (not good) return;

  double xcen = debug_flux_param[found[debug_flux_xcen].first] + xoffset;
  double ycen = debug_flux_param[found[debug_flux_ycen].first] + yoffset;
  double pa   = debug_flux_param[found[debug_flux_pa]  .first] + paoffset;
  double q    = debug_flux_param[found[debug_flux_q]   .first] + qoffset;
  double radf = debug_flux_param[found[debug_flux_rad] .first] * rscale * debug_flux_rfac;
  
  // Normalization pass
  //
  double normPapt = 0.0, normQapt = 0.0;
  double normPtot = 0.0, normQtot = 0.0;

  for (int j=0; j<naxis2; j++) {

    for (int i=0; i<naxis1; i++) {

      int ij = j*naxis1 + i;

      if (static_cast<int>(mask->z[ij]) == 0) {

	double y = static_cast<double>(j) - ycen;
	double x = static_cast<double>(i) - xcen;

	double xp =  x*cos(pa) + y*sin(pa);
	double yp = -x*sin(pa) + y*cos(pa);

	double dist = std::sqrt(xp*xp + yp*yp/q);
	
	if (dist < radf) {
	  normPapt += data->z[ij];
	  normQapt += model->z[ij];
	}

	normPtot += data->z[ij];
	normQtot += model->z[ij];
	
      } // good pixel

    } // x axis

  } // y axis


  double chi2 = 0.0;
  double DKL2 = 0.0, DKL = 0.0, dof = 0.0;
  double Entr = 0.0, Entr2 = 0.0;
  double Fapt = normQapt/normPapt, Ftot = normQtot/normPtot;

  for (int j=0; j<naxis2; j++) {

    for (int i=0; i<naxis1; i++) {

      int ij = j*naxis1 + i;

      if (static_cast<int>(mask->z[ij]) == 0) {

	double y = static_cast<double>(j) - ycen;
	double x = static_cast<double>(i) - xcen;

	double xp =  x*cos(pa) + y*sin(pa);
	double yp = -x*sin(pa) + y*cos(pa);

	double dist = std::sqrt(xp*xp + yp*yp/q);
	
	if (dist < radf) {
	  chi2   += (data->z[ij] - model->z[ij])*(data->z[ij] - model->z[ij])/model->z[ij];
	  DKL2   += data->z[ij]/normPapt * log(data->z[ij]/model->z[ij]*Fapt);
	  Entr2  -= data->z[ij]/normPapt * log(data->z[ij]/normPapt);
	  dof++;
	}

	DKL   += data->z[ij]/normPtot * log(data->z[ij]/model->z[ij]*Ftot);
	Entr  -= data->z[ij]/normPtot * log(data->z[ij]/normPtot);
	
      } // good pixel

    } // x axis

  } // y axis

  if (dof>0) chi2 /= dof;
  
  //
  // Shannon entropy is S = -sum[p ln_2 (p)] but we have computed
  // sum[f ln_2 (f)] where p = f/sum(f).  S = -sum[f/sum(f)
  // ln_2(f/sum(f))] = -sum[f ln_2(f)]/sum(f) + sum[f
  // ln_2(sum(f))]/sum(f) = -sum[f ln_2(f)]/sum(f) + ln_2(sum(f))

  std::cout << std::endl << std::string(32, '-') << std::endl
	    << "Goodness-of-fit parameters"
	    << std::endl << std::string(32, '-') << std::endl
	    << "  Chi^2/dof  = " << chi2 << std::endl
	    << "        dof  = " << dof  << std::endl
	    << "  Encls Entr = " << Entr2/ log(2.0) << std::endl
	    << "  Encls D_KL = " << DKL2 / log(2.0) << std::endl
	    << "  Total Entr = " << Entr / log(2.0) << std::endl
	    << "  Total D_KL = " << DKL  / log(2.0) << std::endl
	    << std::string(32, '-') << std::endl
	    << std::endl;
}
