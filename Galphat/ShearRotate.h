// -*- C++ -*-

#ifndef _SHEAR_ROTATE_H_
#define _SHEAR_ROTATE_H_

#include <cmath>

#include <boost/shared_ptr.hpp>

#include "Serializable.h"

namespace BIE {

  /**
     Abstract image class.

     This virtual members of this class need to be implemented by any
     image type used by ShearRotate.
  */
  template <class Pixel>
  class ImageType : public Serializable
  {
    
  public:
    
    //! Image size
    typedef std::pair<int, int> Isiz;
    
    //! Share pointer to image
    typedef boost::shared_ptr<ImageType> pPtr;
    
    //! Access and assignment operator
    virtual Pixel& operator()(int x, int y) = 0;
    
    //! Factory constructor, blank image with new size
    virtual pPtr factory(Isiz s) = 0;
    
    //! Copy constructor
    virtual pPtr copy() = 0;
    
    //! Get size
    virtual Isiz Size() = 0;
    
  private:
    
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
      try {                                                         
	ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);            
	BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;                     
      }                                                             
    }
    
  };
  
  /** Rotate image using the three-shear algorithm
      
      The each pixel is expected to be of type Pixel which must have
      the standard arithmetic operators +, -, and multiplication by a
      scalar.
      
      The image type must be derived from ImageType
  */
  template <class Pixel>
  class ShearRotate
  {
  public:
    
    //! Byte type for progress in percent
    typedef unsigned char byte;
    
    //! Defines size of image in array (Footron order)
    typedef std::pair<int, int> Isiz;
    
    //! Pointer to pixel array
    typedef boost::shared_ptr< ImageType<Pixel> > pPtr;
    
    //! Callback function to provide progress information.  Returning
    //! false aborts the rotation
    typedef bool (*ProgressCallBack)(byte bPercentComplete);
    
    //! Constructor with provided callback, if desired.
    ShearRotate (ProgressCallBack callback = NULL) : _callback (callback) {}
    
    //! Destructor for derived classes
    virtual ~ShearRotate() {}
    
    /**
       @brief Perform the rotation by transforming the image by an
       exact rotation of 90, 180, or 270 degrees and performing the
       rotation in [-45, 45] degrees

       @return Point to new destination image
       
       @param source  Pointer to source image
       @param angle   Rotation angle
    */
    pPtr Rotate (pPtr source, double angle);
    
  protected:
    
    //! Background pixel generator
    virtual Pixel backVal() = 0;
    
  private:
    
    ProgressCallBack _callback;
    
    /**
       @brief Skews a row horizontally (with filtered weights).
       @usage Limited to 45 degree skewing only. Filters two adjacent pixels.
       @return None
       
       @param source  Image to skew
       @param target  Destination image 
       @param uRow    Row index
       @param iOffset Skew offset
       @param weight  Relative weight of right pixel
    */
    void HorizSkew (pPtr source, pPtr target, int uRow, int iOffset, 
		    double weight);
    
    
    /**
       @brief Skews a column vertically (with filtered weights).
       @usage Limited to 45 degree skewing only. Filters two adjacent pixels.
       @return None
       
       @param source  Image to skew
       @param target  Destination image 
       @param uRow    Column index
       @param iOffset Skew offset
       @param weight Relative weight of right pixel
    */
    void VertSkew ( pPtr source, pPtr target, int uCol, int iOffset, 
		    double weight);
    
    /**
       @brief  Rotates an image by 90 degrees
       @usage  Exact rotation, no weights
       @return Pointer to new rotated pixel array
       
       @param source  Image to skew
    */
    pPtr Rotate90  (pPtr source);
    
    /**
       @brief  Rotates an image by 180 degrees
       @usage  Exact rotation, no weights
       @return Pointer to new rotated pixel array
       
       @param source    Image to skew
    */
    pPtr Rotate180 (pPtr source);
    
    /**
       @brief  Rotates an image by 270 degrees
       @usage  Exact rotation, no weights
       @return Pointer to new rotated pixel array
       
       @param source    Image to skew
    */
    pPtr Rotate270 (pPtr source);
    
    /**
       @brief  Rotates an image in the range -45 <= angle <= 45
       @return Pointer to new rotated pixel array
       
       @param source    Image to rotate
       @param angle     Rotation angle in degrees
       @param flipI     Operation on intermediate image for progress report only)
    */
    pPtr Rotate45  (pPtr source, double angle, bool flipI);
  };  // ShearRotate
  
  
  template <class Pixel>
  void ShearRotate<Pixel>::HorizSkew 
  (pPtr source, pPtr target, int uRow, int iOffset, double weight)
  {
    int i;
    
    // Fill gap left of skew with background
    for (i = 0; i < iOffset; i++) (*target)(i, uRow) = backVal();
    
    Isiz sINP = source->Size();
    Isiz sOUT = target->Size();
    
    Pixel pxlLastRem = backVal();
    
    for (i = 0; i < sINP.first; i++) {
      // Loop through row pixels
      Pixel pxlINP = (*source)(i, uRow);
      
      // Calculate weights
      Pixel pxlRem = static_cast<Pixel>( static_cast<double>(pxlINP) * weight  );
      
      // Update remainder on source
      pxlINP = Pixel (pxlINP - ( pxlRem - pxlLastRem) );
      
      // Check boundries 
      if ((i + iOffset >= 0) && (i + iOffset < sOUT.first))
	(*target)(i + iOffset, uRow) = pxlINP;
      
      // Save remainder for next pixel in scan
      pxlLastRem = pxlRem;
    }
    
    // Go to rightmost point of skew
    i += iOffset;  
    
    // If still in image bounds, add the remainder
    if (i < sOUT.first) (*target)(i, uRow) = pxlLastRem;
    
    // Clear to the right of the skewed line with background
    while (++i < sOUT.first) (*target)(i, uRow) = backVal();
  }   
  
  
  template <class Pixel>
  void ShearRotate<Pixel>::VertSkew 
  (pPtr source, pPtr target, int uCol, int iOffset, double weight)
  {
    int i, iYPos=0;
    
    for (i = 0; i < iOffset; i++) (*target)(uCol, i) = backVal();
    
    Pixel pxlLastRem = backVal();
    
    Isiz sINP = source->Size();
    Isiz sOUT = target->Size();
    
    for (i = 0; i < sINP.second; i++) {
      
      // Loop through column pixels
      Pixel pxlINP = (*source)(uCol, i);
      
      iYPos = i + iOffset;
      
      // Calculate weights
      Pixel pxlRem = 
	static_cast<Pixel>( static_cast<double>( pxlINP * weight ) );
      
      // Update remainder on source
      pxlINP = static_cast<Pixel>( pxlINP - (pxlRem - pxlLastRem) );
      
      // Check boundaries
      if ((iYPos >= 0) && (iYPos < sOUT.second)) 
	(*target)(uCol, iYPos) = pxlINP;
      
      // Save remainder for next pixel in scan
      pxlLastRem = pxlRem;
    }
    
    // Go to bottom point of skew
    i = iYPos;  
    
    // If still in image bounds, add remainder
    if (i < sOUT.second) (*target)(uCol, i) = pxlLastRem;
    
    // Clear below skewed line with background
    while (++i < sOUT.second) (*target)(uCol, i) = backVal();
  }   
  
  
  template <class Pixel>
  typename ShearRotate<Pixel>::pPtr ShearRotate<Pixel>::Rotate90 (pPtr source)
  {
    Isiz sINP(source->Size());
    Isiz sNew(sINP.second, sINP.first);
    
    pPtr target = source->factory(sNew);
    if (target.get() == 0) return pPtr();
    
    for (int uY = 0; uY < sINP.second; uY++) {
      
      for (int uX = 0; uX < sINP.first; uX++) {
	(*target)(uY, sNew.second - uX - 1) = (*source)(uX, uY);
      }
      
      if (_callback) {
	// Report progress
	double factor = static_cast<double>(uY) / source->Size().second;
	
	// Cancel rotation?
	if (!_callback (static_cast<byte>(factor * 50.0))) return pPtr();
      }
    }
    
    return target;
  }   
  
  template <class Pixel>
  typename ShearRotate<Pixel>::pPtr ShearRotate<Pixel>::Rotate180 (pPtr source)
  {
    pPtr target = source->copy();
    
    if (target.get() == 0) return pPtr();
    
    Isiz sINP(source->Size());
    Isiz sNew(target->Size());
    
    for (int uY = 0; uY < source->Size().second; uY++) {
      
      for (int uX = 0; uX < sINP.first; uX++) {
	(*target)(sNew.first-uX-1, sNew.second-uY-1) = (*source)(uX, uY);
      }
      
      if (_callback) {
	// Report progress
	double factor = static_cast<double> (uY) / source->Size().second;
	
	// Cancel rotation?
	if (!_callback (static_cast<byte>( factor * 50.0))) return pPtr();
      }
    }
    
    return target;
  }
  
  
  template <class Pixel>
  typename ShearRotate<Pixel>::pPtr ShearRotate<Pixel>::Rotate270 (pPtr source)
  {
    Isiz sINP(source->Size());
    Isiz sNew(sINP.second, sINP.first);
    
    pPtr target = source->factory(sNew);
    if (target.get() == 0) return pPtr();
    
    for (int uY = 0; uY < sINP.second; uY++) {
      for (int uX = 0; uX < sINP.first; uX++) {
	(*target)(sNew.first-uY-1, uX) = (*source)(uX, uY);
      }
      
      if (_callback) {
	// Report progress
	double factor = static_cast<double>(uY) / sINP.second;
	
	// Cancel rotation?
	if (!_callback (static_cast<byte>(factor * 50.0))) return pPtr();
      }
    }
    return target;
  }   
  
  
  template <class Pixel>
  typename ShearRotate<Pixel>::pPtr ShearRotate<Pixel>::Rotate45  
  (pPtr source, double angle, bool flipI)
  {
    Isiz sINP(source->Size());
    
    // Do the special case of no rotation
    if (angle == 0.0) return source;
    
    double angRad   = angle * M_PI / 180.0; // Angle in radians
    double SinE     = sin (angRad);
    double Tan      = tan (angRad / 2.0);
    
    // Calc first shear (horizontal) destination image dimensions 
    
    int sAngle = static_cast<int>(sINP.first);
    sAngle += static_cast<int>(static_cast<double>(sINP.second) * fabs(Tan));
    
    Isiz sNew1(sAngle, sINP.second);
    
    //------------------------------------------------------------
    // Perform 1st shear (horizontal)
    //------------------------------------------------------------
    
    // Create temporary pixel array for 1st shear
    //
    pPtr target1 = source->factory(sNew1);
    
    // Error in allocation?
    if (target1.get() == 0) return pPtr();
    
    for (int u = 0; u < sNew1.second; u++) {
      double dShear;
      
      if (Tan >= 0.0)
	// Positive angle
	dShear = (static_cast<double>(u) + 0.5) * Tan;
      else
	// Negative angle
	dShear = (static_cast<double>(u - sNew1.second) + 0.5) * Tan;
      
      int iShear = floor(dShear);
      HorizSkew ( source, target1, u, iShear, 
		  dShear - static_cast<double>(iShear) );
      
      if (_callback) {
	// Report progress
	double factor = static_cast<double>(u) / sNew1.second;
	byte bProgress = flipI ? 50 + 
	  static_cast<byte>( factor * 16.66667) : 
	  static_cast<byte>( factor * 33.33333);
	
	// Cancel rotation?
	if (!_callback (bProgress))  return pPtr();
      }
    }
    
    //------------------------------------------------------------
    // Perform 2nd shear (vertical)
    //------------------------------------------------------------
    
    // Calc 2nd shear (vertical) destination image dimensions
    typename ImageType<Pixel>::Isiz sNew2(sNew1);
    
    sNew2.second = static_cast<int>
      ( floor(static_cast<double>(sINP.first) * fabs (SinE) + 
	      static_cast<double>(sINP.second) * cos (angRad) ) );

    // Create image for 2nd shear
    pPtr target2 = source->factory(sNew2);
    if (target2.get() == 0) return pPtr();
    
    double dOffset;     // Variable skew offset
    if (SinE > 0.0)
      // Positive angle
      dOffset = static_cast<double>(sINP.first-1) * SinE;
    else
      // Negative angle
      dOffset = -SinE * static_cast<double>(sINP.first - sNew2.first);
    
    for (int u = 0; u < sNew2.first; u++, dOffset -= SinE)  {
      int iShear = floor(dOffset);
      VertSkew (target1, target2, u, iShear, dOffset - static_cast<double>(iShear));
      
      if (_callback) {
	// Report progress
	double factor = static_cast<double>(u) / sNew2.second;
	byte bProgress = flipI ? 
	  66 + byte( factor * 16.6666667) :
	  33 + byte( factor * 33.3333333) ;
	
	// Cancel rotation?
	if (!_callback (bProgress)) return pPtr();
      }
    }
    
    //------------------------------------------------------------
    // Perform 3rd shear (horizontal)
    //------------------------------------------------------------
    
    // Calc 3rd shear (horizontal) destination image dimensions
    
    typename ImageType<Pixel>::Isiz sNew3(sNew2);
    
    sNew3.first = static_cast<int>
      (floor( static_cast<double>(sINP.second) * fabs (SinE) + 
	      static_cast<double>(sINP.first) * cos (angRad) ) );
    
    // Create image for 3rd shear
    pPtr target3 = source->factory(sNew3);
    
    if (target3.get() == 0) return pPtr();
    
    if (SinE >= 0.0)
      // Positive angle
      dOffset = static_cast<double>(sINP.first - 1) * SinE * (-Tan);
    else 
      // Negative angle
      dOffset = Tan * ( static_cast<double>(sINP.first - 1) * (-SinE) + 
			 static_cast<double>(1 - sNew3.second) );
    
    for (int u = 0; u < int(sNew3.second); u++, dOffset += Tan) {
      int iShear = floor(dOffset);
      HorizSkew ( target2, target3, u, iShear, dOffset - double (iShear) );
      
      if (_callback) {
	// Report progress
	double factor = static_cast<double>(u)/sNew3.second;
	byte bProgress = flipI ? 
	  83 + byte(factor * 16.666667) :
	  66 + byte(factor * 33.333333) ;
	
	// Cancel rotation?
	if (!_callback (bProgress)) return pPtr();
      }
    }
    
    // Return result of 3rd shear
    return target3;      
  }
  
  template <class Pixel>
  typename ShearRotate<Pixel>::pPtr ShearRotate<Pixel>::Rotate 
  (pPtr source, double angle)
  {
    pPtr pTmp = source;
    Isiz sINP = source->Size();
    Isiz sTmp = sINP;
    
    if (source.get() == 0) return pPtr();
    
    // Modulo 360 degrees
    while (angle >= 360.0) angle -= 360.0;
    while (angle < 0.0)    angle += 360.0;
    
    
    // Angle in (45.0 .. 135.0] 
    if ((angle > 45.0) && (angle <= 135.0)) {
      // Rotate image by 90 degrees into temporary image, so it
      // requires only an extra rotation angle of -45.0 .. +45.0 to
      // complete rotation.
      pTmp   = Rotate90 (source);
      angle -= 90.0;
    }
    // Angle in (135.0 .. 225.0] 
    else if ((angle > 135.0) && (angle <= 225.0)) {
      // Rotate image by 180 degrees into temporary image, so it
      // requires only an extra rotation angle of -45.0 .. +45.0 to
      // complete rotation.
      pTmp   = Rotate180 (source);
      angle -= 180.0;
    }
    // Angle in (225.0 .. 315.0] 
    else if ((angle > 225.0) && (angle <= 315.0)) {
      // Rotate image by 270 degrees into temporary image, so it
      // requires only an extra rotation angle of -45.0 .. +45.0 to
      // complete rotation.
      pTmp   = Rotate270 (source);
      angle -= 270.0;
    }
    
    // Rotation angle is now in (-45.0 .. +45.0]
    
    // Failed to allocate middle image
    if (pTmp.get() == 0) return pPtr();
    
    return Rotate45 (pTmp, angle, source != pTmp);
  }   
  


  /** Rotate image using bilinear interpolation

      The each pixel is expected to be of type Pixel which must have
      the standard arithmetic operators +, -, and multiplication by a
      scalar.

      The image type must be derived from ImageType
  */
  template <class Pixel>
  class InterpRotate
  {
  public:

    //! Defines size of image in array (Footron order)
    typedef std::pair<int, int> Isiz;

    //! Pointer to pixel array
    typedef boost::shared_ptr< ImageType<Pixel> > pPtr;

    //! Constructor (no callback implemented)
    InterpRotate () {}

    //! Destructor for derived classes
    virtual ~InterpRotate() {}

    /**
       @brief Perform the rotation
       @return Point to new destination image

       @param source  Pointer to source image
       @param angle   Rotation angle
       @param x       x center in pixels
       @param y       y center in pixels
       @param offset  assume that centers arezero-based offsets
    */
    pPtr Rotate (pPtr source, double angle, double x, double y,
		 bool offset=false);

    //! Rotate about geometric center (same behavior as 3-shear Rotate)
    pPtr Rotate (pPtr source, double angle)
    {
      return Rotate(source, angle, 0.0, 0.0, true);
    }
  
  protected:

    //! Background pixel generator
    virtual Pixel backVal() = 0;

    //! Pin offgrid pixels to nearest boundary
    const bool boundary = true;

  };  // InterpRotate


  template <class Pixel>
  typename InterpRotate<Pixel>::pPtr InterpRotate<Pixel>::Rotate
  (pPtr source, double angle, double cX, double cY, bool offset)
  {
    pPtr pTmp = source;
    Isiz sINP = source->Size();
    Isiz sTmp = sINP;

    if (source.get() == 0) return pPtr();

    // Allocate target pixel array
    //
    pPtr target = source->factory(sINP);
    
    // For convenience
    int width  = sINP.first;
    int height = sINP.second;

    // Image center
    if (offset) {
      cX += (double)  (width-1)/2.0f;
      cY += (double) (height-1)/2.0f;
    }

    // For debugging . . . 
    double minVal =  1.0e30, minPix =  1.0e30;
    double maxVal = -1.0e30, maxPix = -1.0e30;

    for (int i=0; i < height; i++) {
      for (int j=0; j < width; j++) {
	double val = (*source)(j, i);
	minPix = std::min<double>(minPix, val);
	maxPix = std::max<double>(maxPix, val);
      }
    }

    // Begin the loop
    for (int i=0; i < height; i++) {
      
      // get center y coordinate
      double relY = cY - (double)i;
      
      for (int j=0; j < width; j++) {
	
	// get center x coordinate
	double relX = (double)j - cX;
	
	// Rotation transformation
	//
	double xPrime = relX*cos(angle) + relY*sin(angle);
	double yPrime = -1 * relX*sin(angle) + relY*cos(angle);
	
	// Re-center pixel
	//
	xPrime += cX;
	yPrime += cY;

	// The four nearest pixels on the source image
	//
	int q12x = std::floor(xPrime);
	int q12y = std::floor(yPrime);

	if (boundary) {
	  q12x = std::max<int>(0, q12x);
	  q12y = std::max<int>(0, q12y);

	  q12x = std::min<int>(width-1, q12x);
	  q12y = std::min<int>(height-1, q12y);
	}

	int q22x = std::ceil(xPrime);
	int q22y = q12y;

	if (boundary) {
	  q22x = std::min<int>(width-1, q22x);
	  q22x = std::max<int>(0, q22x);
	}

	int q11x = q12x;
	int q11y = std::ceil(yPrime);

	if (boundary) {
	  q11y = std::min<int>(height-1, q11y);
	  q11y = std::max<int>(0, q11y);
	}

	int q21x = q22x;
	int q21y = q11y;

	// Get nearest neighbor pixels.  Fill in offgrid pixels with
	// background value.
      
	Pixel q11 = backVal();
	Pixel q12 = backVal();
	Pixel q21 = backVal();
	Pixel q22 = backVal();

	if (boundary or (q11x>=0 and q11x<width and q11y>=0 and q11y<height))
	  q11 = (*source)(q11x, q11y);

	if (boundary or (q12x>=0 and q12x<width and q12y>=0 and q12y<height))
	  q12 = (*source)(q12x, q12y);

	if (boundary or (q21x>=0 and q21x<width and q21y>=0 and q21y<height))
	  q21 = (*source)(q21x, q21y);

	if (boundary or (q22x>=0 and q22x<width and q22y>=0 and q22y<height))
	  q22 = (*source)(q22x, q22y);
      
	// Interpolation factors
	//
	double f1, f2, f3, f4;
	  
	// Special case check to avoid "divide by zero"
	if ( q21x == q11x ) {
	  f1 = 1; 
	  f2 = 0;
	} else {
	  f1 = (((double)q21x - (double)xPrime)/((double)q21x - (double)q11x));
	  f2 = (((double)xPrime - (double)q11x)/((double)q21x - (double)q11x));
	}

	double R1 = f1 * (double)q11 + f2*(double)q21;
	double R2 = f1 * (double)q12 + f2*(double)q22;

	// Special case check to avoid "divide by zero"
	if (q12y == q11y) {
	  f3 = 1;
	  f4 = 0;
	} else {
	  f3 = ((double)q12y - yPrime)/((double)q12y - (double)q11y);
	  f4 = (yPrime - (double)q11y)/((double)q12y - (double)q11y);
	}

	// Calculate the final pixel value
	Pixel final = (Pixel)((f3 * R1) + (f4 * R2));

	// Assign to target array
	(*target)(j, height-i-1) = final;

	minVal = std::min<double>(minVal, (double)final);
	maxVal = std::max<double>(maxVal, (double)final);
	if (std::isnan(final) or std::isinf(final)) {
	  std::cout << "Crazy pixel" << std::endl;
	}
      }
    }
    
    return target;
  };

} // namespace BIE

BIE_CLASS_ABSTRACT   (BIE::ImageType<float>)
BIE_CLASS_EXPORT_KEY (BIE::ImageType<float>)  

#endif
