//=============================================================================
// Chebyshev polynomimal class
//=============================================================================

#include <values.h>

#include <map>
#include <cmath>
#include <cfloat>
#include <vector>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <utility>
#include <algorithm>

#include <ChebPoly.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::ChebPoly)

using namespace BIE::Galphat;

ChebPoly::ChebPoly()
{
  n = 0;
				// Force evaluation
  lastX = NAN;
				// No block defined
  bIndx = -1;
}

ChebPoly::ChebPoly(int order)
{
				// Sanity check
  n = std::max<size_t>(0, order);
				// Allocate return vector
  poly.resize(n+1, 0);
				// Force evaluation
  lastX = NAN;
				// No block defined
  bIndx = -1;
}

ChebPoly::ChebPoly(int order, int j)
{
				// Sanity check
  n = std::max<size_t>(0, order);
				// Allocate return vector
  poly.resize(n+1, 0);
				// Force evaluation
  lastX = NAN;
				// Block index
  bIndx = j;
}

const std::vector<double>& ChebPoly::operator()(double x)
{
				// If we already evaluated the poly basis, 
  if (x == lastX) return poly;	// reuse it!

  lastX = x;			// Recompute

				// Special case for n=0 (constant order)
  if (n >= 0)
    poly[0] = 1.0;

				// Special case for n=1 (constant order)
  if (n >= 1)
    poly[1] = x;

  for (size_t i=2; i<=n; i++) {
    poly[i] = 2.0*x*poly[i-1] - poly[i-2];
  }

  return poly;
}

double ChebPoly::operator()(unsigned k, double x)
{
  if (k>n)        return 0.0;
  if (x != lastX) operator()(x);

  return poly[k];
}

double ChebPoly::operator()(std::vector<double>& v, double x)
{
  // Vector rank sanity checking
  //
  if (v.size() != n+1) {
      std::ostringstream sout;
      sout << "ChebPoly vector coefficient dimension mismatch; "
	   << "need <" << n+1 << "> but vector has rank <" << v.size() << ">";
      throw DimNotMatchException(sout.str(), n+1);
  }
    
  if (x != lastX) operator()(x);
  
  double ret = 0.0;
  for (size_t k=0; k<=n; k++) ret += v[k] * poly[k];
    
  return ret;
}

