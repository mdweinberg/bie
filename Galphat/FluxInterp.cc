#include <Timer.h>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>

#include <gvariable.h>		// For nametag
#include <FluxInterp.h>
#include <FluxFamily.h>
#include <Acquire.H>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::FluxInterp)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::FluxInterpOneDim)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::FluxInterpBasis)

using namespace BIE::Galphat;

// For debugging only!
//
static bool         verbose_debug = true;
static bool           exact_debug = false;

// Static data
//
bool    FluxInterpOneDim::OffGrid = true;

/* Higher accuracy
double       FluxInterp::eps      = 1.0e-12;
int          FluxInterp::maxlev   = 10;
*/

double       FluxInterp::eps      = 1.0e-6;
int          FluxInterp::maxlev   = 8;


FluxInterpOneDim::FluxInterpOneDim(pt::ptree& tree, FluxFamilyOneDim* ptr)
{
  model = ptr;

  //------------------------------------------------------
  try {
    cache_file = tree.get<std::string>("cache.value");
  } catch (pt::ptree_error &error) {
    cache_file = Acquire::get<std::string>("Cache file");
    tree.put("cache.desc", "Image cube cache file");
    tree.put("cache.value", cache_file);
  }
  //------------------------------------------------------
  try {
    Npix = tree.get<int>("size.value");
  } catch (pt::ptree_error &error) {
    Npix = Acquire::get<int>("Number of model image pixels per side");
    tree.put("size.desc", "Number of pixes per side");
    tree.put("size.value", Npix);
  }
  //------------------------------------------------------
  try {
    Nnum = tree.get<int>("slices.value");
  } catch (pt::ptree_error &error) {
    Nnum = Acquire::get<int>("Number of model shape slices");
    tree.put("nshape.desc", "Number of shape sliices");
    tree.put("nshape.value", Nnum);
  }
  //------------------------------------------------------
  try {
    Rmax = tree.get<double>("rscale.value");
  } catch (pt::ptree_error &error) {
    Rmax = Acquire::get<double>("Radial extent of model images");
    tree.put("rscale.desc", "Radial extent of model images");
    tree.put("rscale.value", Rmax);
  }
  //------------------------------------------------------
  try {
    Nmin = tree.get<double>("nmin.value");
  } catch (pt::ptree_error &error) {
    Nmin = Acquire::get<double>("Minimum value of shape parameter");
    tree.put("nmin.desc", "Minimum value of shape parameter");
    tree.put("nmin.value", Nmin);
  }
  //------------------------------------------------------
  try {
    Nmax = tree.get<double>("nmax.value");
  } catch (pt::ptree_error &error) {
    Nmax = Acquire::get<double>("Maximum value of shape parameter");
    tree.put("nmax.desc", "Maximum value of shape parameter");
    tree.put("nmax.value", Nmax);
  }
  //------------------------------------------------------
  try {
    Central = tree.get<double>("central.value");
  } catch (pt::ptree_error &error) {
    Central = 0.1;
    tree.put("central.desc", "Pixels by quadrature inside this radius in r_e");
    tree.put("central.value", Central);
  }
  //------------------------------------------------------
  try {
    logI = tree.get<bool>("logindex.value");
    logI = false;		// Not in use currently
  } catch (pt::ptree_error &error) {
    logI = false;
    tree.put("logindex.desc", "Index is logarithmically spaced");
    tree.put("logindex.value", logI);
  }
  //------------------------------------------------------
  
  dn = (Nmax - Nmin)/(Nnum-1);
  
  // Make sure that npix is even!
  if ((Npix/2)*2 != Npix) Npix++;
  
  Nhalf = Npix/2;
  
  if (myid==0) {
    std::cout << "Grid parameters:" << std::endl
	      << Rmax << " " << Nmin << " " << Nmax << " " 
	      << Npix << " " << Nnum << std::endl;
  }
  
  dr = 2.0*Rmax/Npix;
  
  offgrid = ongrid = 0;
  
  ComputeGrid();

  // For debugging only.  Set to false for production
  //                             |
  //  +--------------------------+
  //  v
  if (false) {  
    dumpGridFITS(cache_file + "_grid_dump.fits");
  }
}


double FluxInterpOneDim::getPixel(double x0, double x1, double y0, double y1)
{
  //                                                                            
  // Compute pixel scalings                                                     
  //                                                                            
  
  double q = model->q0;
  double A = model->r0;
  double B = A * q;
  double C = 1.0;
  double D = q;
  
  if (q > 1.0) {
    q  = 1.0/q;
    A *= q;
    B /= q;
    C  = q;
    D  = 1.0;
  }
  
  //
  // Compute central pixel by direct integration
  //
  if (Central>0.0) {
    double rdist = 0.5*sqrt((x0+x1)*(x0+x1)/A/A + (y0+y1)*(y0+y1)/B/B );
    if (rdist<Central || (x0*x1 < 0 && y0*y1 < 0)) {
      vector<double> retV = model->pixel(x0, x1, y0, y1, eps, maxlev);
      return retV[0];
    }
  }

  
  double distx0 = fabs(x0/A);
  double disty0 = fabs(y0/B);
  double distx1 = fabs(x1/A);
  double disty1 = fabs(y1/B);
				// If the rotated pixel is off the table,
				// compute the pixel by direct integration
  if (OffGrid) {

    if (distx0>=Rmax || disty0>=Rmax || distx1>=Rmax || disty1>=Rmax) { 
      // the following is faster than integration 

      double R2 = (x0 + x1)*(x0 + x1)/C/C + (y0 + y1)*(y0 + y1)/D/D;
      double fdensity = fabs((x0-x1)*(y0-y1)) * model->density(0.5*sqrt(R2))/q;

      offgrid++;
      return fdensity;
    }
    ongrid++;
  }


  double ret1;
  double ret2;
  double ret3;
  double ret4;

  int    indxmin0, indymin0, indxmax0, indymax0;
  int    indxmin1, indymin1, indxmax1, indymax1;
  double aX0, bX0, aY0, bY0;
  double aX1, bX1, aY1, bY1;

  indxmin0 = static_cast<int>(floor((x0/A+Rmax)/dr));
  indymin0 = static_cast<int>(floor((y0/B+Rmax)/dr));
  indxmax0 = static_cast<int>(floor((x1/A+Rmax)/dr));
  indymax0 = static_cast<int>(floor((y1/B+Rmax)/dr));

  indxmin1 = indxmin0 + 1;
  indymin1 = indymin0 + 1;
  indxmax1 = indxmax0 + 1;
  indymax1 = indymax0 + 1;

  aX0 = (-Rmax+dr*indxmin1 - x0/A)/dr;
  bX0 = 1.0 - aX0;

  aY0 = (-Rmax+dr*indymin1 - y0/B)/dr;
  bY0 = 1.0 - aY0;

  aX1 = (-Rmax+dr*indxmax1 - x1/A)/dr;
  bX1 = 1.0 - aX1;

  aY1 = (-Rmax+dr*indymax1 - y1/B)/dr;
  bY1 = 1.0 - aY1;

  double dn = (Nmax - Nmin)/(Nnum - 1);
  int indn0;
  double a, b;
  
  if (logI) {
    double nlog = log(coef[0]);
    indn0 = static_cast<int>(floor( (nlog - Nmin)/dn ));
    indn0 = std::max<int>(indn0, 0);
    indn0 = std::min<int>(indn0, Nnum-2);
    a = (Nmin + dn*(indn0+1) - nlog)/dn;
    b = 1.0 - a;
  } else {
    indn0 = static_cast<int>(floor( (coef[0] - Nmin)/dn ));
    indn0 = std::max<int>(indn0, 0);
    indn0 = std::min<int>(indn0, Nnum-2);
    a = (Nmin + dn*(indn0+1) - coef[0])/dn;
    b = 1.0 - a;
  }

  int indn1 = indn0 + 1;
				// For x0,y0
  ret1 = 
    a*(aX0*aY0*Grid(indn0,      indxmin0, indymin0) +
       aX0*bY0*Grid(indn0,	indxmin0, indymin1) +
       bX0*aY0*Grid(indn0,	indxmin1, indymin0) +
       bX0*bY0*Grid(indn0,	indxmin1, indymin1)) +
    b*(aX0*aY0*Grid(indn1,	indxmin0, indymin0) +
       aX0*bY0*Grid(indn1,	indxmin0, indymin1) +
       bX0*aY0*Grid(indn1,	indxmin1, indymin0) +
       bX0*bY0*Grid(indn1,	indxmin1, indymin1));
  
				// For x1,y1	    
  ret2 = 
    a*(aX1*aY1*Grid(indn0,	indxmax0, indymax0) +
       aX1*bY1*Grid(indn0,	indxmax0, indymax1) +
       bX1*aY1*Grid(indn0,	indxmax1, indymax0) +
       bX1*bY1*Grid(indn0,	indxmax1, indymax1)) +
    b*(aX1*aY1*Grid(indn1,	indxmax0, indymax0) +
       aX1*bY1*Grid(indn1,	indxmax0, indymax1) +
       bX1*aY1*Grid(indn1,	indxmax1, indymax0) +
       bX1*bY1*Grid(indn1,	indxmax1, indymax1));
  
				// For x0,y1	    
  ret3 = 
    a*(aX0*aY1*Grid(indn0,	indxmin0, indymax0) +
       aX0*bY1*Grid(indn0,	indxmin0, indymax1) +
       bX0*aY1*Grid(indn0,	indxmin1, indymax0) +
       bX0*bY1*Grid(indn0,	indxmin1, indymax1)) +
    b*(aX0*aY1*Grid(indn1,	indxmin0, indymax0) +
       aX0*bY1*Grid(indn1,	indxmin0, indymax1) +
       bX0*aY1*Grid(indn1,	indxmin1, indymax0) +
       bX0*bY1*Grid(indn1,	indxmin1, indymax1));
    
				// For x1,y0
  ret4 = 
    a*(aX1*aY0*Grid(indn0,	indxmax0, indymin0) +
       aX1*bY0*Grid(indn0,	indxmax0, indymin1) +
       bX1*aY0*Grid(indn0,	indxmax1, indymin0) +
       bX1*bY0*Grid(indn0,	indxmax1, indymin1)) +
    b*(aX1*aY0*Grid(indn1,	indxmax0, indymin0) +
       aX1*bY0*Grid(indn1,	indxmax0, indymin1) +
       bX1*aY0*Grid(indn1,	indxmax1, indymin0) +
       bX1*bY0*Grid(indn1,	indxmax1, indymin1));
  
  return ret1 + ret2 - ret3 - ret4;
}

void FluxInterpOneDim::ComputeGrid()
{
  // Attempt to read cache
  if (read_cache()) return;

  Ngrid.clear();
  grid.clear();

  std::map<int, Rmatrix> work;
  for (int n=0; n<Nnum; n++) {
    std::vector<double> N(1, Nmin + dn*n);
    Ngrid.push_back(Nmin + dn*n);
    grid.push_back(ComputeMatrix(N));
  }
  
  // Write cache (if desired)
  write_cache();
}

vector< vector<Real> > FluxInterpOneDim::ComputeMatrix(const std::vector<double>& n)
{
  Rmatrix mat(Nhalf+1);
  for (int i=0; i<=Nhalf; i++) mat[i].resize(Nhalf+1, 0.0);
  
  vector<double> ret;
  double x, y;
  
  model->set(n);		// Set model parameters

  int count = 0;

  for (int i=0; i<Nhalf; i++) {
    for (int j=0; j<Nhalf; j++) {
      if (count % numprocs == myid) {
	x = -Rmax + dr*i;
	y = -Rmax + dr*j;
	ret = model->pixel(x, x+dr, y, y+dr, eps, maxlev);
	mat[i+1][j+1] = ret[0];
      }
      count++;
    }
  }
  
  //
  // Share pixels with all nodes
  //

  for (int i=0; i<=Nhalf; i++) {
    if (sizeof(Real)==4)
      MPI_Allreduce(MPI_IN_PLACE, &mat[i][0], Nhalf+1,
		    MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
    else if (sizeof(Real)==8)
      MPI_Allreduce(MPI_IN_PLACE, &mat[i][0], Nhalf+1,
		    MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  }
  
  //
  // Perform PSF smoothing here
  //
  
  // ...
  
  //
  // Cumulate in x-dimension
  //
  for (int j=0; j<=Nhalf; j++)
    for (int i=1; i<=Nhalf; i++) mat[i][j] += mat[i-1][j];
  
  //
  // Cumulate in y-dimension
  //
  for (int j=1; j<=Nhalf; j++)
    for (int i=0; i<=Nhalf; i++) mat[i][j] += mat[i][j-1];
  
  return mat;
}


void FluxInterpOneDim::write_cache()
{
  if (myid) return;
  if (cache_file.size()==0) return;
  ofstream out(cache_file.c_str());
  if (!out) {
    std::cerr << "Error writing cache file <" << cache_file 
	      << ">, continuing without caching . . ." << endl;
    return;
  }
  
  out.write((const char*)&Nmin,  sizeof(double));
  out.write((const char*)&Nmax,  sizeof(double));
  out.write((const char*)&Rmax,  sizeof(double));
  out.write((const char*)&Nnum,  sizeof(int));
  out.write((const char*)&Npix,  sizeof(int));
  
  int sze = sizeof(Real);
  out.write((const char*)&sze, sizeof(int));
  
  for (int n=0; n<Nnum; n++) {
    out.write((const char*)&Ngrid[n], sizeof(double));
    for (int i=0; i<=Nhalf; i++)
      out.write((const char*)&grid[n][i][0], (1+Nhalf)*sizeof(Real));
  }
}

bool FluxInterpOneDim::read_cache()
{
  if (cache_file.size()==0) return false;
  ifstream in(cache_file.c_str());
  if (!in) {
    if (myid==0) {
      std::cerr << "Error opening cache file <" << cache_file 
		<< ">" << std::endl
		<< "I will compute tables now and cache the new file" 
		<< std::endl;
    }
    return false;
  }
  
  bool okay = true;
  double nmin, nmax, rmax;
  int nnum, npix;
  
  in.read((char*)&nmin, sizeof(double));
  if (fabs(nmin-Nmin)>1.0e-06) okay = false;
  
  in.read((char*)&nmax, sizeof(double));
  if (fabs(nmax-Nmax)>1.0e-6) okay = false;

  in.read((char*)&rmax, sizeof(double));
  if (fabs(rmax-Rmax)>1.0e-6) okay = false;

  in.read((char*)&nnum, sizeof(int));
  if (nnum != Nnum) okay = false;
  
  in.read((char*)&npix, sizeof(int));
  if (npix != Npix) okay = false;
  
  if (!okay) {
    if (myid==0) {
      std::cerr << "Parameters do not match cache file <" << cache_file 
		<< ">" << std::endl
		<< "I will compute tables now and cache the new file" 
		<< std::endl;
    }
    return false;
  }
  
  int sze;
  in.read((char*)&sze, sizeof(int));
  if (sze != sizeof(Real)) {
    if (myid==0) {
      std::cerr << "Data type mismatch in cache file <" 
		<< cache_file << ">" << std::endl
		<< "I will compute tables now and cache the new file" 
		<< std::endl;
    }
    return false;
  }
  
  
  double val;
  vector< vector<Real> > mat(Nhalf+1);
  for (int j=0; j<=Nhalf; j++) mat[j].resize(Nhalf+1);
  
  for (int n=0; n<Nnum; n++) {
    in.read((char*)&val, sizeof(double));
    Ngrid.push_back(val);
    
    for (int i=0; i<=Nhalf; i++)
      in.read((char*)&mat[i][0], (1+Nhalf)*sizeof(Real));
    
    grid.push_back(mat);
  }
  
  if (in.good()) {
    if (myid==0) {
      std::cerr << "Cache file <" << cache_file 
		<< "> successfully read" << std::endl;
    }
    return true;
  }

  if (myid==0) {
    std::cerr << "Error reading grids from cache file <" 
	      << cache_file << ">" << std::endl
	      << "I will compute tables now and cache the new file" 
	      << std::endl;
  }
  
  return false;
}

void FluxInterpOneDim::dumpGridFITS(std::string fname)
{
  if (myid) return;
  
  fitsfile  *ofptr;

  int     status = 0;
  long      dims = 3;
  long    images = Nnum;
  long    naxis1 = Nhalf;
  long    naxis2 = Nhalf;
  long numdim[3] = {naxis1, naxis2, images};
  long fpixel[3] = {1, 1, 1};
  long    impix  = naxis1 * naxis2;
  long    numpix = impix * images;
  
  if (fits_create_file(&ofptr, fname.c_str(), &status)) 
    cout << "FluxInterpOneDim::dumpGridFITS: "
	 << "Error when creating file <" << fname << ">" << endl;
  
  std::vector<float> z(numpix, 0.0);
  
  for (int n=0; n<Nnum; n++) {
    for (int j=0; j<naxis2; j++) {
      for (int i=0; i<naxis1; i++) {
	int ij = j*naxis1 + i;
	z[ij + impix*n] = 
	  grid[n][i+1][j+1] + grid[n][i][j] - grid[n][i+1][j] - grid[n][i][j+1];
      }
    }
  }

  int bitpix = -32;

  if (fits_create_img(ofptr, bitpix, dims, numdim, &status))
    cout << "FluxInterpOneDim::dumpGridFITS: "
	 << "Error when creating image" << endl;

  if (fits_write_history (ofptr, "Created by BIE's GALPHAT", &status) )
    cout << "FluxInterpOneDim::dumpGridFITS: "
	 << "Error writing history" << endl;
  
  if (fits_write_pix (ofptr, TFLOAT, fpixel, numpix, &z[0], &status))
    cout << "FluxInterpOneDim::dumpGridFITS: "
	 << "Error when creating data cube" << endl;
  
  fits_close_file(ofptr, &status);
  fits_report_error(stderr, status);

  // Done!
}

double FluxInterpOneDim::Grid1(int n, int i, int j)
{
  if (i>Nhalf)			// Addition ordering for truncation error?
    return grid[n][Nhalf][j] + (grid[n][Nhalf][j] - grid[n][Npix-i][j]);
  else
    return grid[n][i][j];
}

double FluxInterpOneDim::Grid(int n, int i, int j)
{
  double val;
  if (j>Nhalf) {
    val = Grid1(n, i, Nhalf);
    return val + (val - Grid1(n, i, Npix-j));
  }
  else
    return Grid1(n, i, j);
}


FluxInterpBasis::FluxInterpBasis(pt::ptree& tree, FluxFamilyBasis* ptr)
{
  model = ptr;

  //------------------------------------------------------
  try {
    cache_file = tree.get<std::string>("cache.value");
  } catch (pt::ptree_error &error) {
    cache_file = Acquire::get<std::string>("Cache file");
    tree.put("cache.desc", "Basis cache file");
    tree.put("cache.value", cache_file);
  }
  //------------------------------------------------------
  try {
    ngrid = tree.get<int>("size.value");
  } catch (pt::ptree_error &error) {
    ngrid = Acquire::get<int>("Grid size");
    tree.put("size.desc", "Number of grid points per side");
    tree.put("size.value", ngrid);
  }
  //------------------------------------------------------
  try {
    norder = tree.get<int>("order.value");
  } catch (pt::ptree_error &error) {
    norder = Acquire::get<int>("Number of basis functions");
    tree.put("norder.desc", "Number of basis functions");
    tree.put("norder.value", norder);
  }
  
  computeGrid();
}

#include <gsl/gsl_integration.h>

//
// GSL function wrapper
//
class gsl_function_pp : public gsl_function
{
private:

  std::function<double(double)> _func;

  static double invoke(double x, void *params) 
  {
    return static_cast<gsl_function_pp*>(params)->_func(x);
  }

public:
  
  gsl_function_pp(std::function<double(double)> const& func) : _func(func)
  {
    function = &gsl_function_pp::invoke;
    params   = this;
  }    
  
};


// Compute half-light radius
double FluxInterpBasis::half_norm(size_t n, size_t j)
{  
  size_t csz = coef.size();
  std::vector<double> ret(csz);

  double half_max = 0.5*(*(slices[n].norm[j].rbegin()));
  std::vector<double>::iterator xBeg = slices[n].norm[j].begin(), xEnd = slices[n].norm[j].end();
  std::vector<double>::iterator flb = std::lower_bound (xBeg, xEnd, half_max);
  size_t nsz = slices[n].norm[j].size(), xlo, xhi;

  if (flb == xEnd) {
    xlo = nsz - 2;
    xhi = nsz - 1;
  } else if (flb == xBeg) {
    xlo = 0;
    xhi = 1;
  } else {
    xlo = flb - xBeg - 1;
    xhi = flb - xBeg;
  }
  
  double dnrm = slices[n].norm[j][xhi] - slices[n].norm[j][xlo];
  return model->mp->xmin() + dx*( (half_max - slices[n].norm[j][xlo])/dnrm + xlo );
}

void FluxInterpBasis::initialize()
{
  double sum = 0.0;
  if (not model->normZ) {
    for (auto  v : coef) { sum += v;   } // Sum weights
    for (auto& v : coef) { v   /= sum; } // Normalize
  }

  // Find bounding scale values
  //
  double P = model->P;
  std::vector<double>::iterator beg = model->pvec.begin(), end = model->pvec.end();
  std::vector<double>::iterator lb = std::lower_bound (beg, end, P);
  sz = model->pvec.size();

  if (sz==1) {
    lo = hi = 0;

    // Compute half-light radius
    for (size_t j=0; j<X12.size(); j++) X12[j] = half_norm(0, j);

  } else {
    if (lb == end) {
      lo = sz - 2;
      hi = sz - 1;
    } else if (lb == beg) {
      lo = 0;
      hi = 1;
    } else {
      lo = lb - beg - 1;
      hi = lb - beg;
    }

    double Rlo = model->pvec[lo];
    double Rhi = model->pvec[hi];

    Ars = (Rhi - P)/(Rhi - Rlo);
    Brs = (P - Rlo)/(Rhi - Rlo);

    // Compute half-light radius
    for (size_t j=0; j<X12.size(); j++)
      X12[j] = Ars * half_norm(Rlo, j) + Brs * half_norm(Rhi, j);

  }

  if (exact_debug) {
    // Incorporate basis normalization
    for (size_t n=0; n<coef.size(); n++) {
      if (sz==1)
	coef[n] /= slices[lo].norm[n].back();
      else
	coef[n] /= Ars*slices[lo].norm[n].back() + Brs*slices[hi].norm[n].back();
    }
  }
}

void FluxInterpBasis::computeNorm()
{
  std::vector<double> saveCoef = coef;
  
  gsl_function_pp Fp( std::bind(&BIE::Galphat::FluxFamilyBasis::integrand, 
				&(*model), std::placeholders::_1) );

  gsl_function *F = static_cast<gsl_function*>(&Fp);
  
  gsl_integration_workspace *  w = gsl_integration_workspace_alloc(1000);
      
  model->sigma0 = 1.0;

  int cnt = 0;

  for (auto & S : slices) {

    model->P = S.P;


    for (unsigned n=0; n<norder; n++) {

      if (cnt++ % numprocs == myid) {

	coef    = std::vector<double>(norder, 0.0);
	coef[n] = 1.0;
	
	//         +--------- Coefficients
	//         |     +--- Parameter
	//         |     |
	//         V     V
	model->set(coef, S.P);

	const size_t num = 10000;
	double xmin = model->mp->xmin(), xmax = model->mp->xmax();
	dx = (xmax - xmin)/num;
	
	const double atol = 0.0;
	const double ftol = 1.0e-8;
	double maxError   = 0.0;
	
	std::vector<double> cum;
	cum.push_back(0.0);
	for (size_t i=0; i<num; i++) {
	  double x1 = xmin + dx*i;
	  double x2 = xmin + dx*(i+1);
	  double result, error;
	  gsl_integration_qags (F, x1, x2, atol, ftol, 1000, w, &result, &error);
	  if ( (!model->normZ or n==0) and result < 0.0) {
	    std::cout << "Crazy" << std::endl;
	  }
	  maxError = std::max<double>(maxError, error);
	  result *= 2.0*M_PI;
	  cum.push_back(result+cum.back());
	}
	
	S.norm[n] = cum;
      
	if (std::isnan(cum.back())) {
	  std::cout << "Norm is NaN, order" << std::setw(4) 
		    << n << ", P=" << S.P
		    << ": " << maxError << std::endl;
	}

	if (verbose_debug and myid==0)
	  std::cout << "Max error, order" << std::setw(4) 
		    << n << ", P=" << S.P
		    << ": " << maxError << std::endl;
      }
    }
  }

  gsl_integration_workspace_free (w);

  coef = saveCoef;
}


void FluxInterpBasis::computeGrid()
{
  // Attempt to read cache
  if (read_cache()) {
    if (myid==0 and verbose_debug) dumpBasis(nametag + ".basis");
    return;
  }

  slices.clear();

  // Initialize the image grid
  //
  for (auto rs : model->pvec)
    slices.push_back(ImageSlice(rs, norder, ngrid));

  // Compute the normalizations
  //
  computeNorm();

  std::vector<double> saveCoef = coef;

  // Allocate storage for half-light vector
  //
  X12.resize(coef.size());

  ds = (model->mp->xmax() - model->mp->xmin())/ngrid;

  // Now compute the grid
  //
  int cnt = 0;

  //
  // model->normI==true means normalize from the normalization integral
  //
  // model->normZ==true means that all but n=0 term will be normalized
  // to have net zero flux
  //

  for (auto & S : slices)  {

    for (unsigned n=0; n<norder; n++) {

      coef    = std::vector<double>(norder, 0.0);
      coef[n] = 1.0;
      
      model->set(coef, S.P);

      double x, y, nrm = 1.0;
      if (model->normI) nrm /= S.norm[n].back();

      for (size_t i=1; i<=ngrid; i++) {
	x = model->mp->xmin() + ds*i;

	for (size_t j=1; j<=ngrid; j++) {
	  y = model->mp->xmin() + ds*j;
	  
	  if (cnt++ % numprocs == myid) {
	    
	    double x0 = model->mp->invxi(x-ds);
	    double x1 = model->mp->invxi(x);
	    double y0 = model->mp->invxi(y-ds);
	    double y1 = model->mp->invxi(y);
	    
	    S.imgs[n][i][j] =
	      nrm * model->pixel(x0, x1, y0, y1, eps, maxlev)[0];

				// Debug check
	    if (std::isnan(S.imgs[n][i][j])) {
	      std::cout << "Node " << myid << " NaN for Order=" << n
			<< ", pixel [i, j]=[" << i << ", " << j << "], "
			<< ", [x0, x1, y0, y1]=[" << x0 << ", " << x1
			<< ", " << y0 << ", " << y1 << "]"
			<< std::endl;
	    }
	  }
	}
      }
    }
  }

  double norm0 = 1.0;		// For quasi-orthogonal basis

  cnt = 0;

  MPI_Barrier(MPI_COMM_WORLD);
  
  for (auto & S : slices)  {
    
    for (unsigned n=0; n<norder; n++) {

      // Get the node that has the norm
      int nid = cnt++ % numprocs;

      // Get the size (largest vector will carry that full data)
      unsigned nsz = S.norm[n].size();
      MPI_Bcast(&nsz, 1, MPI_UNSIGNED, nid, MPI_COMM_WORLD);
	  
      // Resize if necessary
      S.norm[n].resize(nsz, 0.0);
      MPI_Bcast(&S.norm[n][0], nsz, MPI_DOUBLE, nid, MPI_COMM_WORLD);

      // Share computations with all nodes by summing matrix values
      for (size_t i=1; i<=ngrid; i++) {
	if (sizeof(Real)==4)
	  MPI_Allreduce(MPI_IN_PLACE, &S.imgs[n][i][0], ngrid+1,
			MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
	else if (sizeof(Real)==8)
	  MPI_Allreduce(MPI_IN_PLACE, &S.imgs[n][i][0], ngrid+1,
			MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	else {
	  std::ostringstream sout;
	  sout << "Wrong data type of size=" << sizeof(Real)
	       << ", should be float or double";
	  throw BIE::InternalError(sout.str(), __FILE__, __LINE__);
	}
      }

      // Sum columns
      for (size_t i=0; i<=ngrid; i++) {
	for (size_t j=1; j<=ngrid; j++) {
	  S.imgs[n][i][j] += S.imgs[n][i][j-1];
	}
      }
      
      // Sum rows
      for (size_t i=1; i<=ngrid; i++) {
	for (size_t j=0; j<=ngrid; j++) {
	  S.imgs[n][i][j] += S.imgs[n][i-1][j];
	}
      }
      
      if (std::isnan(S.imgs[n][ngrid][ngrid])) {
	std::cout << "NaN cum, order " << std::setw(3) << n 
		  << ", p row sum=" << 4.0*S.imgs[n][ngrid-1][ngrid]
		  << ", p col sum=" << 4.0*S.imgs[n][ngrid][ngrid-1]
		  << std::endl;
      }

      if (!model->normI) {
	double norm1 = 4.0*S.imgs[n][ngrid][ngrid];
	if (n==0) norm0 = norm1;
	// Apply normalization
	for (size_t i=0; i<=ngrid; i++) {
	  for (size_t j=1; j<=ngrid; j++) {
	    if (model->normZ and n>0)
	      S.imgs[n][i][j] = (S.imgs[n][i][j] - norm1)/norm0;
	    else
	      S.imgs[n][i][j] /= norm1;
	  }
	}
      }

      if (verbose_debug and myid==0) {
	std::cout << "Order " << std::setw(3) << n 
		  << ", sum=" << 4.0*S.imgs[n][ngrid][ngrid] << std::endl;
      }

    }
  }
  
  write_cache();

  if (verbose_debug and myid==0) dumpBasis(nametag + ".basis");
}

bool FluxInterpBasis::read_cache()
{
  std::ifstream in(cache_file.c_str());

  if (in) {
    if (model->mp->badCache(in)) {
      if (myid==0) {
	std::cout << "Error reading mapping parameters from cache file <" 
		  << cache_file << ">" << std::endl;
      }
      return false;
    }

    unsigned nslc, nnum, npix;
    in.read((char *)&nslc, sizeof(unsigned));
    in.read((char *)&nnum, sizeof(unsigned));
    in.read((char *)&npix, sizeof(unsigned));

    if (nnum != norder) {
      std::cout << "Basis order mismatch: cache has " << nnum 
		<< " but you specified " << norder << std::endl;
      return false;
    }

    if (npix != ngrid) {
      std::cout << "Pixel dimension mismatch: cache has " << npix
		<< " but you specified " << ngrid << std::endl;
      return false;
    }

    in.read((char *)&ds, sizeof(double));
    in.read((char *)&dx, sizeof(double));

    shared_ptr<char> cbuf = shared_ptr<char>(new char [20]);
    in.read(cbuf.get(), 20);

    if (model->ID.compare(cbuf.get()) != 0) {
      std::cout << "FluxInterpBasis found <" << cbuf 
		<< "> but expected <" << model->ID << ">" 
		<< std::endl;
      return false;
    }

    if (model->pvec.size() != nslc) {
      std::cout << "FluxInterpBasis found <" << cbuf 
		<< "> but with <" << nslc
		<< "> slices rather than the expected <"
		<< model->pvec.size() << ">" 
		<< std::endl;
      return false;
    }

    // Initialize the image grid
    //
    slices.clear();
    for (auto rs : model->pvec)
      slices.push_back(ImageSlice(rs, nnum, npix));

    // Read the image grid cache
    //
    for (auto & S : slices) {
      S.read_cache(in);
      if (myid==0 and S.norm.size() != norder) {
	std::cout << "ImageSlice found with P=" << S.P
		  << " but with order <" << S.norm.size()
		  << "> rather than the expected <"
		  << norder << ">" << std::endl;
      }
    }

    if (in.bad())
      return false;
    else
      return true;

  } else {
    return false;
  }
}

void FluxInterpBasis::write_cache()
{
  if (myid) return;
  if (cache_file.size()==0) return;

  std::ofstream out(cache_file.c_str());

  if (out) {
    unsigned nslice = slices.size();

    model->mp->writeCache(out);

    out.write((const char *)&nslice, sizeof(unsigned));
    out.write((const char *)&norder, sizeof(unsigned));
    out.write((const char *)&ngrid,  sizeof(unsigned));
    out.write((const char *)&ds,     sizeof(double));
    out.write((const char *)&dx,     sizeof(double));

				// Using shared_ptr for automatic
				// garbage collection
    shared_ptr<char> cbuf = shared_ptr<char>(new char [20]);
    strncpy(cbuf.get(), model->ID.c_str(), 20);
    out.write((const char*)cbuf.get(), 20);

    // Write the grid
    //
    for (auto & S : slices) S.write_cache(out);
  }
}

void ImageSlice::write_cache(std::ostream & out)
{
  unsigned nordr = norm.size();
  unsigned gsize = imgs[0].size();
  unsigned ncum  = norm[0].size();

  out.write((const char *)&P,     sizeof(double));
  out.write((const char *)&nordr, sizeof(unsigned));
  out.write((const char *)&gsize, sizeof(unsigned));
  out.write((const char *)&ncum,  sizeof(unsigned));

  for (unsigned n=0; n<nordr; n++) {
    out.write((const char *)&norm[n][0], ncum*sizeof(double));
  }      

  for (unsigned n=0; n<nordr; n++) {
    for (unsigned i=0; i<gsize; i++) {
      out.write((const char *)&imgs[n][i][0], gsize*sizeof(Real));
    }
  }
}

void ImageSlice::read_cache(std::istream & in)
{
  unsigned nordr, gsize, ncum;

  in.read((char *)&P,     sizeof(double));
  in.read((char *)&nordr, sizeof(unsigned));
  in.read((char *)&gsize, sizeof(unsigned));
  in.read((char *)&ncum,  sizeof(unsigned));

  norm.resize(nordr);
  for (unsigned n=0; n<nordr; n++) {
    norm[n].resize(ncum);
    in.read((char *)&norm[n][0], ncum*sizeof(double));
  }      

  imgs.resize(nordr);
  for (unsigned n=0; n<nordr; n++) {
    imgs[n].resize(gsize);
    for (unsigned i=0; i<gsize; i++) {
      imgs[n][i].resize(gsize);
      in.read((char *)&imgs[n][i][0], gsize*sizeof(Real));
    }
  }
}

double FluxInterpBasis::getPixel
(double x0, double x1, double y0, double y1)
{
  if (x0*x1<0.0 and y0*y1<0.0) {
    return			    // Pixel contains origin:
      getPixel0(0.0, x1, 0.0, y1) + // Quad 1
      getPixel0(x0, 0.0, 0.0, y1) + // Quad 2
      getPixel0(x0, 0.0, y0, 0.0) + // Quad 3
      getPixel0(0.0, x1, y0, 0.0) ; // Quad 4
  } else if (x0*x1<0.0) {
    return			   // Pixel intersected by y axis:
      getPixel0(0.0, x1, y0, y1) + // Quad 1 or Quad 4
      getPixel0(x0, 0.0, y0, y1) ; // Quad 2 or Quad 3
  } else if (y0*y1<0.0) {
    return			   // Pixel intersected by x axis:
      getPixel0(x0, x1, y0, 0.0) + // Quad 1 or Quad 2
      getPixel0(x0, x1, 0.0, y1) ; // Quad 3 or Quad 4
  } else {
    return			 // No axis intersection:
      getPixel0(x0, x1, y0, y1); // All in one quadrant
  }
}

double FluxInterpBasis::getPixel0(double x0, double x1, double y0, double y1)
{
  double q = model->q0;
  double R = model->r0;
  double A = R;
  double B = R * q;
  
  if (q > 1.0) {
    q = 1.0 / q;
    A = R * q;
    B = R;
  }
  
  // Quadrants are symmetric at this point
  //
  x0 = fabs(x0/A);
  x1 = fabs(x1/A);
  y0 = fabs(y0/B);
  y1 = fabs(y1/B);

  if (x0 > x1) {
    double tt = x0;
    x0 = x1;
    x1 = tt;
  }

  if (y0 > y1) {
    double tt = y0;
    y0 = y1;
    y1 = tt;
  }

  // Return zero flux if pixel is beyond map
  //
  double Rmax = model->mp->rmax();
  if (x0>=Rmax or y0>=Rmax) return 0.0;

  x1 = std::min<double>(x1, Rmax);
  y1 = std::min<double>(y1, Rmax);

  if (exact_debug) {
    return model->pixel(x0, x1, y0, y1, eps, maxlev)[0];
  }

  // Transform to unit interval
  //
  x0 = model->mp->xi(x0);
  x1 = model->mp->xi(x1);
  y0 = model->mp->xi(y0);
  y1 = model->mp->xi(y1);

  // Limit pixel boundardies to map
  //
  x0 = std::max<double>(model->mp->xmin(), x0);
  y0 = std::max<double>(model->mp->xmin(), y0);
  x1 = std::min<double>(model->mp->xmax(), x1);
  y1 = std::min<double>(model->mp->xmax(), y1);

  // The pixel boundaries
  //
  std::array<std::array<double, 2>, 4> pts;

  pts[0] = {x0, y0};		// Origin
  pts[1] = {x1, y0};		// Lower right
  pts[2] = {x1, y1};		// Diagonal corner
  pts[3] = {x0, y1};		// Upper left

  // Will contain cumulative flux map at the boundaries
  //
  std::array<double, 4> values = {0, 0, 0, 0};

  // Linear interpolation coefficients
  //
  std::array<std::array<double, 2>, 2> F;

  // Indices at the 4 boundary points
  //
  std::array<std::array<size_t, 2>, 2> I, J;

  for (size_t k=0; k<4; k++) {

    // Compute values, vertex by vertex
    //
    size_t ix = (pts[k][0] - model->mp->xmin())/ds;
    size_t iy = (pts[k][1] - model->mp->xmin())/ds;

    ix = std::min<size_t>(ix, ngrid-1);
    iy = std::min<size_t>(iy, ngrid-1);

    // Assign the four points from the grid in counterclockwise order
    // around the vertex
    //
    I[0][0] = ix;
    I[0][1] = ix;
    I[1][0] = ix + 1;
    I[1][1] = ix + 1;

    J[0][0] = iy;
    J[0][1] = iy + 1;
    J[1][0] = iy;
    J[1][1] = iy + 1;

    // The interpolation coefficients in each direction
    //
    double X0 = (model->mp->xmin() + ds*(ix+1) - pts[k][0])/ds;
    double X1 = (pts[k][0] - model->mp->xmin() - ds*(ix+0))/ds;
    double Y0 = (model->mp->xmin() + ds*(iy+1) - pts[k][1])/ds;
    double Y1 = (pts[k][1] - model->mp->xmin() - ds*(iy+0))/ds;
    
    // The full bilinear interpolation coefficients
    //
    F[0][0] = X0*Y0;
    F[0][1] = X0*Y1;
    F[1][1] = X1*Y1;
    F[1][0] = X1*Y0;

    // Test
    double test = 0.0;
    bool nega = false;
    for (size_t i=0; i<2; i++) {
      for (size_t j=0; j<2; j++) {
	test += F[i][j];
	if (F[i][j] < -1.0e-6) nega = true;
      }
    }

    if (fabs(test - 1.0) > 1.0e-8 or nega) {
      std::cout << "Range error: ";
      for (size_t i=0; i<2; i++) {
	for (size_t j=0; j<2; j++) {
	  std::cout << std::setw(18) << F[i][j];
	}
      }
      std::cout << std::endl;
    }

    // Do the bilinear interpolation for each point defining the pixel
    //
    for (size_t i=0; i<2; i++) {
      for (size_t j=0; j<2; j++) {
	double val = 0.0;
	for (unsigned n=0; n<norder; n++) {
	  if (sz == 1) 
	    val += coef[n] * slices[lo].imgs[n][I[i][j]][J[i][j]];
	  else
	    val += coef[n] * (Ars*slices[lo].imgs[n][I[i][j]][J[i][j]] +
			      Brs*slices[hi].imgs[n][I[i][j]][J[i][j]] );
	}
	values[k] += F[i][j] * val;

      }
    }
  }

  double val = values[0] - values[1] + values[2] - values[3];

  return val;
}


void FluxInterpBasis::dumpBasis(const std::string& fname)
{

  unsigned cnt = 0;
  
  for (auto & S : slices) {

    fitsfile  *ofptr;

    int     status = 0;
    long      dims = 3;
    long    images = norder;
    long    naxis1 = ngrid + 1;
    long    naxis2 = ngrid + 1;
    long numdim[3] = {naxis1, naxis2, images};
    long fpixel[3] = {1, 1, 1};
    long    impix  = naxis1 * naxis2;
    long    numpix = impix * images;
  
    std::stringstream fs;
    fs << fname << "." << cnt++ << ".fits";

    if (fits_create_file(&ofptr, fs.str().c_str(), &status)) 
      cout << "FluxInterpBasis::dumpBasis: "
	   << "Error when creating file <" << fs.str() << ">" << endl;
    
    std::vector<float> z(numpix, 0.0);
    
    for (int j=0; j<naxis2; j++) {
      for (int i=0; i<naxis1; i++) {
	int ij = j*naxis1 + i;
	
	for (unsigned n=0; n<norder; n++)
	  z[ij + impix*n] = S.imgs[n][i][j];
      }
    }

    int bitpix = -32;

    if (fits_create_img(ofptr, bitpix, dims, numdim, &status))
      cout << "FluxInterpBasis::dumpBasis: "
	   << "Error when creating image" << endl;
    
    // (char*) casts to make the compiler happy
    char* com = (char*)"Basis parameter value";
    if (fits_write_key(ofptr, TDOUBLE, (char*)"Param", &S.P, com, &status) )
      cout << "FluxInterp::dumpBasis: "
	   << "Error setting Param key word" << endl;

    char comments[3][68] = {
      "Each image in the stack is the cumulative distribution",
      "for each basis in the first quadrant.", ""
    };

    std::string com3("Basis name: " + model->ID);
    strncpy(comments[2], com3.c_str(), std::min<size_t>(68, com3.size()));

    for (int i=0; i<3; i++) {
      if (fits_write_comment(ofptr, comments[i], &status)) 
	cout << "FluxInterp::dumpBasis: "
	     << "Error writing new comment #" << i+1 << endl;
    }
    
    if (fits_write_history (ofptr, "Created by FluxInterp", &status) )
      cout << "FluxInterp::dumpBasis: "
	   << "Error writing history" << endl;
    
    if (fits_write_pix (ofptr, TFLOAT, fpixel, numpix, &z[0], &status) )
      cout << "FluxInterp::dumpBasis: "
	   << "Error when creating data cube" << endl;
    
    fits_close_file(ofptr, &status);
    fits_report_error(stderr, status);
  }

  // Done!
}

