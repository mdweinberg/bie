#include "PSF.h"
#include "GalphatLikelihoodFunction.h"

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::PSF)

using namespace BIE::Galphat;

void PSF::assignFlux(GalphatLikelihoodFunction* lf, int lev, 
		     inpars_ptr input, image_ptr img)
{
  setup(input, img);

  int i0 = indexMap[0];
  int i1 = indexMap[1];
  int i2 = indexMap[2];

  double xcent, ycent;

  if      (ia[i0] == -1) xcent = d[i0];
  else if (ia[i0] ==  2) xcent = xoffset + a[i0];
  else                   xcent = a[i0];
  
  if      (ia[i1] == -1) ycent = d[i1];
  else if (ia[i1] ==  2) ycent = yoffset + a[i1];
  else                   ycent = a[i1];


  double mag      = a[i2];
  double imgxcent = (xmax+xmin)/2.0;
  double imgycent = (ymax+ymin)/2.0;
  double xshft    = xcent-imgxcent;
  double yshft    = ycent-imgycent;
  double totcnt   = pow (10., (img->magzpt - mag)/ 2.5);

  lf->ShiftImage(lf->modelpsf[level], xshft, yshft, numx, numy);
  
  for (int j=0; j<numy; j++) {
    for (int i=0; i<numx; i++) {
      lf->totalflux[level][j*numx+i] += 
	lf->modelpsf[level][j*numx+i] * totcnt;
    }
  }
}

