#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <random>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_sf.h>

using namespace std;

std::pair<double, int> solve_x(double n, double bd, double alpha, double beta);

struct my_f_params { double sersicn;
                     double bd;
                     double alpha;
                     double beta;
                   };

double myfunc(double rh, void *params);

int main(int argc, char** argv)
{

  std::string output;
  double sersicn, mean_bt, disp_bt;
  double min_rat, mean_rat, disp_rat;
  double min_alpha, disp_alpha;
  int    nsample;

  po::options_description desc("Monte Carlo a distribution of values from the r-half relation for testing");

  desc.add_options()
    ("help,h",       "this help message")
    ("sersic,n",     po::value<double>(&sersicn)->default_value(4.0), "Bulge Sersic index")
    ("output,o",     po::value<string>(&output)->default_value("testtab.dat"), "output data file name")
    ("BTmean",       po::value<double>(&mean_bt)->default_value(0.2), "B/T mean value")
    ("BTdisp",       po::value<double>(&disp_bt)->default_value(0.25), "B/T stddev")
    ("BDmin",        po::value<double>(&min_rat)->default_value(0.005),  "R_b/R_d minimum value")
    ("BDmean",       po::value<double>(&mean_rat)->default_value(0.25), "R_b/R_d mean value")
    ("BDdisp",       po::value<double>(&disp_rat)->default_value(0.15), "R_b/R_d stddev value")
    ("Amin",         po::value<double>(&min_alpha)->default_value(0.1), "R_h/R_o minimum value")
    ("Adisp",        po::value<double>(&disp_alpha)->default_value(0.25), "R_h/R_o stddev value")
    ("sample,N",     po::value<int>(&nsample)->default_value(10000), "Number of samples");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << endl;
    return 1;
  }


  //
  // Engines 
  //
  std::random_device rd;
  std::mt19937 gen(rd());
  
  // Distribtuions
  //
  std::normal_distribution<> distU(log(mean_bt), disp_bt);
  std::normal_distribution<> distN(mean_rat, disp_rat);
  std::normal_distribution<> distA(1.0, disp_alpha);

  //std::uniform_real_distribution<double> distU(0, 1);
  //std::normal_distribution<> dist(2, 2);
  //std::student_t_distribution<> dist(5);
  //std::poisson_distribution<> dist(2);
  //std::extreme_value_distribution<> dist(0,2);
  
  std::ofstream out(output.c_str());

  if (!out) {
    std::cerr << "Error opening file <" << output << ">" << std::endl;
    return -1;
  }
  
  for (int i=0; i<nsample; i++) {

    double bt    = exp(distU(gen));
    double alpha = std::max<double>(min_alpha, distA(gen));

    double rbrd  = std::max<double>(min_rat, distN(gen));
    double beta  = 1.0/rbrd;

    double bd    = bt/(1.0 - bt);

    std::pair<double, int> ans = solve_x(sersicn, bd, alpha, beta);

    out << std::setw(15) << bt
	<< std::setw(18) << alpha
	<< std::setw(18) << beta
	<< std::setw(18) << ans.first
	<< std::endl;

  }

  return 0;
}

std::pair<double, int> solve_x(double n, double bd, double alpha, double beta)
{

  int status;
  int iter = 0, max_iter = 100;
  const gsl_root_fsolver_type *T;
  gsl_root_fsolver *s;

  double r=0.0;
  double x_lo = 0.0001, x_hi = 1000.0;
  gsl_function F;

  struct my_f_params params = {n,bd,alpha,beta};

  F.function = &myfunc;
  F.params=&params;

  T = gsl_root_fsolver_brent;
//  T = gsl_root_fsolver_bisection;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_lo, x_hi);

  do
   {
     iter++;

     status = gsl_root_fsolver_iterate (s);
     r = gsl_root_fsolver_root (s);
     x_lo = gsl_root_fsolver_x_lower (s);
     x_hi = gsl_root_fsolver_x_upper (s);
     status = gsl_root_test_interval (x_lo, x_hi, 1.0e-6, 1.0e-5);

   } while (status == GSL_CONTINUE && iter < max_iter);

   gsl_root_fsolver_free (s);

   return std::pair<double, int>(r, iter);
}

double myfunc(double x, void *params)
{
  struct my_f_params *p = (struct my_f_params *)params;

  double bd      = p->bd;
  double sersicn = p->sersicn;
  double alpha   = p->alpha;
  double beta    = p->beta;

  double bn = 0.0;
  
  if (sersicn>0.36) { 
    bn = 2.0*sersicn - 1.0/3.0 +
      ((((131./1148175. - 2194697./30690717750./sersicn)/sersicn) + 46.0/25515.)/sersicn + 4./405.)/sersicn;
  }
  else {
    bn = 0.01945 - 0.8902*sersicn + 10.95*sersicn*sersicn -
      19.67*sersicn*sersicn*sersicn + 13.43*sersicn*sersicn*sersicn*sersicn;
  }
  
  double b1 = 1.678;
 
  double xn = bn*pow((alpha*x),1./sersicn);
  double x1 = b1*alpha/beta*x;
  
  double term1 = gsl_sf_gamma_inc_P (2.0*sersicn, xn);  
  double term2 = gsl_sf_gamma_inc_P (2.0, x1);  

  return term1*bd + term2 - 0.5*(bd+1.0);
}
