#!/usr/bin/python

#================================================================
# Template: one-dimensional GALPHAT model script using
#           Differential Evolution algorithm
#================================================================

import BIE

BIE.BIEutil.init()

#================================================================
# Set the run ID string
#================================================================

BIE.cvar.nametag  = "run1"

#================================================================
# Set the archive type
#================================================================

# XML is nice if you want to examine the archive but leads to files > 1GB
# BIE.BIEutil.setArchiveType("XML")

# BINARY is much more efficient
BIE.BIEutil.setArchiveType("BINARY")

BIE.BIEutil.getArchiveType()

#================================================================
# Simulation setup
#================================================================

ndim    =  8
L       =  1
nsteps  =  40000
nburn   =  4000
width   =  0.1
mchains =  16
minmc   =  6

#================================================================
# Setup state information
#================================================================

si = BIE.State.StateInfo(ndim)

#================================================================
# Setup GALPHAT prior
#================================================================
pvec  = BIE.PyVector.VectorDist(ndim)

disc  = BIE.Distribution.DiscUnifDist(0, 1)
unif1 = BIE.Distribution.UniformDist(0.09, 0.99)
norm1 = BIE.Distribution.NormalDist(0.0, 3.0, -6.0, 6.0)
norm2 = BIE.Distribution.NormalDist(1.0, 0.000004, 0.9, 1.1)
norm3 = BIE.Distribution.NormalDist(0.0, 0.069, -0.785398, 0.785398)
norm4 = BIE.Distribution.NormalDist(4.0, 1.0, 0.6, 9.9)
weib1 = BIE.Distribution.WeibullDist(1.01, 9.5, 0.5, 1.3)
weib2 = BIE.Distribution.WeibullDist(1.21, 2.5, 0.5, 2.0)
#                    X centroid
pvec[0] = norm1
#                    Y centroid
pvec[1] = norm1
#                    mag
pvec[2] = weib1
#                    rhalf
pvec[3] = weib2
#                    Sersic n
pvec[4] = norm4
#                    axis ratio
pvec[5] = unif1
#                    position angle
pvec[6] = norm3
#                    sky background 
pvec[7] = norm2

prior = BIE.Prior.Prior(si,pvec)
sstat = BIE.Ensemble.EnsembleDisc(si)
#================================================================
# Convergence test
#================================================================

convrg = BIE.Converge.GelmanRubinConverge(-2000, sstat, "0")

convrg.setAlpha(0.05)
convrg.setNgood(4000)
convrg.setNskip(2000)
convrg.setNoutlier(2000)
convrg.setPoffset(-30.0)
convrg.setMaxout(6)
convrg.setRhatMax(1.3)
#================================================================
# Metropolis-Hastings
#================================================================

mca = BIE.MCAlgorithm.MetropolisHastings()

#============================================================
# DE randomization
#============================================================

uc  = BIE.Distribution.UniformDist(0.01)
uc2 = BIE.Distribution.UniformDist(0.0005)
eps = BIE.PyVector.VectorDist(ndim, uc)
eps[7] = uc2

#================================================================
# Differential Evolution setup
#================================================================
like    = BIE.Like.LikelihoodComputationSerial()
sim     = BIE.Simulation.DifferentialEvolution.WithVL(si, mchains, eps, convrg, prior, like, mca)

sim.SetLinearMapping(True)
sim.SetJumpFreq(10)
sim.SetControl(1)
sim.NewGamma(0.06)
# sim.EnableLogging()

#================================================================
# GALPHAT setup
# ** You might need to change "fname" and "outfile"**
#================================================================

fname = "input.galphat"
pmap = BIE.Galphat.GalphatParam(fname)
BIE.cvar.outfile = "run1.posterior"

#================================================================
# Run GALPHAT
#================================================================
fct = BIE.Galphat.GalphatLikelihoodFunction(pmap, L)
# Assign column labels
si.labelFromLike(fct)
# Register the user likelihood
sim.SetUserLikelihood(fct)
current_level = 0
run = BIE.Simulation.RunOneSimulation(nsteps, width, sstat, prior, sim)
run.Run()

#================================================================
# Serialize current state for later reuse
#================================================================
BIE.BIEutil.psave('galphat1')

#================================================================
# Print covariance
#================================================================

sstat.PrintDiag()

#================================================================
# Save image for extremal probability values
#================================================================

#---------------------------->Explore one (or small number) of states

solo  = BIE.Simulation.PosteriorProb(si, prior, like)
solo.SetUserLikelihood(fct)

#---------------------------->Compute the ML image

bestL = sstat.getMaxLikeStateVector()
fct.singleImage("ML")
fct.gof(bestL, "X center", "Y center", "PA", "q", "R_e", 1.0)
solo.RunVec(bestL)

#---------------------------->Compute the MAP image

bestP = sstat.getMaxProbStateVector()
fct.singleImage("MAP")
fct.gof(bestP, "X center", "Y center", "PA", "q", "R_e", 1.0)
solo.RunVec(bestP) 

#---------------------------->Compute the marginal likelihood

marg = BIE.MarginalLikelihood.MarginalLikelihood(sstat, sim)
marg.useImportance(1000, 1000)
marg.setOutfile("marglike.run1")
marg.setTreeType("TNT")
marg.compute()

BIE.BIEutil.quit()
