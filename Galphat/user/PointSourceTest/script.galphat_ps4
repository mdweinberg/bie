#================================================================
# Template: one-dimensional GALPHAT model script using
#           Differential Evolution algorithm
#================================================================
# Set a new persistence session
#================================================================
pnewsession galphat_persist_ps4
cktoggle
ckinterval 10000
#
#================================================================
# Set the run ID string
#================================================================
set nametag  = "run1ps4"
#
#================================================================
# Simulation setup
#================================================================
set L = 1
set ndim = 9
set minmc = 6
set nsteps = 40000
set nburn = 4000
set width = 0.1
set maxT = 32.0
#
#================================================================
# Setup state information
#================================================================
set si = new StateInfo(ndim)
#================================================================
# Setup GALPHAT prior
#================================================================
set pvec  = new clivectordist(ndim)
set disc  = new DiscUnifDist(0, 1)
set unif1 = new UniformDist(0.09, 0.99)
set norm1 = new NormalDist(0.0, 3.0, -6.0, 6.0)
set norm2 = new NormalDist(1.0, 0.000004, 0.9, 1.1)
set norm3 = new NormalDist(0.0, 0.069, -0.785398, 0.785398)
set norm4 = new NormalDist(4.0, 1.0, 0.6, 9.9)
set weib1 = new WeibullDist(1.01, 9.5, 0.5, 1.3)
set weib2 = new WeibullDist(1.21, 2.5, 0.5, 2.0)
set norm5 = new NormalDist(0.0, 5.0);
#                    X centroid
pvec->setval(0, norm1)
#                    Y centroid
pvec->setval(1, norm1)
#                    mag
pvec->setval(2, weib1)
#                    rhalf
pvec->setval(3, weib2)
#                    Sersic n
pvec->setval(4, norm4)
#                    axis ratio
pvec->setval(5, unif1)
#                    position angle
pvec->setval(6, norm3)
#                    ps magnitude
pvec->setval(7, norm5)
#                    sky background 
pvec->setval(8, norm2)
#
set prior = new Prior(si,pvec)
set sstat = new EnsembleDisc(si)
#================================================================
# Convergence test
#================================================================
set convrg = new GelmanRubinConverge(-2000, sstat, "0")
    convrg->setAlpha(0.05)
    convrg->setNgood(4000)
    convrg->setNskip(2000)
    convrg->setNoutlier(2000)
    convrg->setPoffset(-30.0)
    convrg->setMaxout(16)
    convrg->setRhatMax(1.3)
#================================================================
# Metropolis-Hastings
#================================================================
set mca = new MetropolisHastings()
set mvec  = new clivectord(ndim)
    mvec->setval(0,0.01)
    mvec->setval(1,0.01)
    mvec->setval(2,0.01)
    mvec->setval(3,0.01)
    mvec->setval(4,0.05)
    mvec->setval(5,0.01)
    mvec->setval(6,0.01)
    mvec->setval(7,0.01)
    mvec->setval(8,0.0005)
set mhwidth = new MHWidthOne(si, mvec)
#============================================================
# DE randomization
#============================================================
#
set uc = new UniformDist(0.01)
set uc2 = new UniformDist(0.0005)
set eps = new clivectordist(ndim, uc)
    eps->setval(8, uc2) 
#================================================================
# Differential Evolution setup
#================================================================
set like = new LikelihoodComputationSerial()
set mchains = 64
set sim = new DifferentialEvolution(si, mchains, eps, convrg, prior, like, mca)
    sim->SetLinearMapping(1)
    sim->SetJumpFreq(10)
    sim->SetControl(1)
    sim->NewGamma(0.06)
    sim->EnableLogging()
#================================================================
# GALPHAT setup
# ** You might need to change "fname" and "outfile"**
#================================================================
set fname = "input.galphat_ps4"
set pmap = new GalphatParam(fname)
set outfile = "run1ps4.posterior"
#================================================================
# Run GALPHAT
#================================================================
set fct = new GalphatLikelihoodFunction(pmap, L)
# Assign column labels
    si->labelFromLike(fct)
# Register the user likelihood
    sim->SetUserLikelihood(fct)
set current_level = 0
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#================================================================
# Serialize current state for later reuse
#================================================================
psave
#
#================================================================
# Print covariance
#================================================================
#
sstat->PrintDiag()
#
#================================================================
# Save image for extremal probability values
#================================================================
#
#---------------------------->Explore one (or small number) of states
#
set solo  = new PosteriorProb(si, prior, like)
    solo->SetUserLikelihood(fct)
#
#---------------------------->Compute the ML image
#
set bestL = sstat->getMaxLikeState()
    fct->singleImage("ML")
    solo->Run(bestL)
#
#---------------------------->Compute the MAP image
#
set bestP = sstat->getMaxProbState()
    fct->singleImage("MAP")
    solo->Run(bestP) 
#
#---------------------------->Compute the marginal likelihood
#
set marg = new MarginalLikelihood(sstat, sim)
    marg->useImportance(1000, 10000)
    marg->setOutfile("marglike.run1ps4")
    marg->setTreeType("TNT")
    marg->compute()
#
exit


