{
    "parameters":
    {
        "_comment": "A simple test for a one-dimensional profile",
	"_comment": "I use _comment for comments by convention",
	"_comment": ["In JSON, all strings must be surrounded by",
		     "double quotes, and name tags are strings.",
		     "String values must be surrount in double quotes",
		     "but integer or float types do not need quotes."],
        "author": "Martin Weinberg",
        "date":
        {
            "day": 15,
            "month": "January",
            "year": 2015
        },
        "control":
	{
	    "1": {
		"description": ["GALPHAT control parameters for Level 1.",
				"\"decimate=1\" turns off pixel aggregation.",
				"sshape is the 2MASS seeing shape parameter",
				"to determine PSF FWHM and not used if the",
				"PSF image is specified."],
		"level":
		{
                    "desc": "data aggregation level",
                    "value": 1
		},
		"image":
		{
                    "desc": "Input data, FITS file",
                    "value": "ps_single_20.fits"
		},
		"psf":
		{
                    "desc": "PSF image, FITS file",
                    "value": "ps_single_psf.fits"
		},
		"mask":
		{
                    "desc": "Bad pixel mask",
                    "value": "none"
		},
		"region":
		{
                    "desc": "Image region to fit",
                    "value": "1 142 1 142"
		},
		"zeropt":
		{
                    "desc": "Photometric zero point",
                    "value": -10.8
		},
		"adjzpt":
		{
                    "desc": "Adjust zero pt for exposure",
                    "value": true
		},
		"aggregate":
		{
                    "desc": "Image aggregration factor",
                    "value": 1
		},
		"shape":
		{
                    "desc": "seeing shape",
                    "value": 1.100
		},
		"gain":
		{
                    "desc": "gain, e\/ADU",
                    "value": 8.0
		},
		"coadd":
		{
                    "desc": "Number of coadded images",
                    "value": 1
		},
		"xoffset":
		{
                    "desc": "origin offset in x direction",
                    "value": 71.5
		},
		"yoffset":
		{
                    "desc": "origin offset in y direction",
                    "value": 71.5
		},
		"magi":
		{
                    "desc": "fiducial magitude (i.e. catalog estimate)",
                    "value": -23.03773
		},
		"rscale":
		{
                    "desc": "r scale",
                    "value": 4.95
		},
		"noffset":
		{
                    "desc": "offset in n",
                    "value": 4.3
		},
		"qoffset":
		{
                    "desc": "offset in axis ratio",
                    "value": 0.7
		},
		"paoffset":
		{
                    "desc": "offset in position anagle",
                    "value": -0.78539816
		},
		"bgoffset":
		{
                    "desc": "offset in background",
                    "value": 50.0
		}
            },
            "2":
            {
		"description": ["GALPHAT control parameters for Level 2 for",
				"pixel aggregation. It is safe to make this",
				"same as the level 1 EXCEPT for value for the",
				"level value."],
		"level":
		{
                    "desc": "data aggregation level",
                    "value": 2
		},
		"image":
		{
                    "desc": "Input data, FITS file",
                    "value": "ps_single_20.fits"
		},
		"psf":
		{
                    "desc": "PSF image, FITS file",
                    "value": "ps_single_psf.fits"
		},
		"mask":
		{
                    "desc": "Bad pixel mask",
                    "value": "none"
		},
		"region":
		{
                    "desc": "Image reagion to fit",
                    "value": "1 142 1 142"
		},
		"zeropt":
		{
                    "desc": "Photometric zero point",
                    "value": -10.8
		},
		"aggregate":
		{
                    "desc": "Aggregation factor",
                    "value": 1
		},
		"sshape":
		{
                    "desc": "seeing shape",
                    "value": 1.100
		},
		"gain":
		{
                    "desc": "gain, e\/ADU",
                    "value": 8.0
		},
		"coadd":
		{
                    "desc": "Number of coadded images",
                    "value": 1
		},
		"xoffset":
		{
                    "desc": "origin offset in x direction",
                    "value": 71.5
		},
		"yoffset":
		{
                    "desc": "origin offset in y direction",
                    "value": 71.5
		},
		"magi":
		{
                    "desc": "fiducial mag",
                    "value": -23.03773
		},
		"rscale":
		{
                    "desc": "r scale",
                    "value": 4.95
		},
		"noffset":
		{
                    "desc": "offset in n",
                    "value": 4.3
		},
		"qoffset":
		{
                    "desc": "offset in axis ratio",
                    "value": 0.7
		},
		"paoffset":
		{
                    "desc": "offset in position anagle",
                    "value": -0.78539816
		},
		"bgoffset":
		{
                    "desc": "offset in background",
                    "value": 50.0
		}
            }
	},
        "models":
        {
            "description": ["Initial guess for GALPHAT: id = id counter,",
			    "name = parameter name to print out,",
			    "desc = parameter description,",
			    "ctrl = parameter transform type",
			    "(i.e. Additive, Multiplicative, Scaled, etc.)"],
            "_comment": ["The \"model type\" is a required entity. All other",
			 "entities will be registered and their tags used as",
			 "string labels. "],
	    "Single":
	    {
		"X center":
		{
                    "desc": "X center rel to lower corner",
                    "index": 0,
                    "ctrl": "Additive",
                    "value": 71.5
		},
		"Y center":
		{
                    "desc": "Y center rel to lower corner",
                    "index": 1,
                    "ctrl": "Additive",
                    "value": 71.5
		},
		"Mag":
		{
                    "desc": "total magnitude relative to fiducial value, magi",
                    "index": 2,
                    "ctrl": "Weibull flux",
                    "value": 1.0
		},
		"R_e":
		{
                    "desc": "Effective radius scaled by the fiducial radius, rscale",
                    "index": 3,
                    "ctrl": "Scaled",
                    "value": 1.0
		},
		"Sersic":
		{
                    "label": "Sersic n",
                    "desc": "Sersic index",
                    "index": 4,
                    "ctrl": "None",
                    "value": 3.0
		},
		"q":
		{
                    "desc": "axis ratio b\/a",
                    "index": 5,
                    "ctrl": "None",
                    "value": 0.5
		},
		"PA":
		{
                    "desc": "position angle relative to pa offset",
                    "index": 6,
                    "ctrl": "Additive",
                    "value": -0.785398
		},
		"tables":
		{
		    "1":
		    {
			"description": ["CAUTION: for Sersic table, fine table",
					"(smaller maximum size) should be first.",
					"Otherwise, you will use coarse resolution",
					"table for generating model. Values should",
					"not be changed with caution."],
			"model":
			{
			    "desc": "Model type",
			    "value": "SERSIC"
			},
			"cache":
			{
			    "desc": "Image cube cache file",
			    "value": "p2000n60r10.cache"
			},
			"size":
			{
			    "desc": "Number of pixels per side",
			    "value": "2000"
			},
			"slices":
			{
			    "desc": "Number of shape slices",
			    "value": 60
			},
			"rscale":
			{
			    "desc": "Extent of table in radial scale",
			    "value": 10.0
			},
			"nmin":
			{
			    "desc": "Minimum value of shape parameter",
			    "value": 0.5
			},
			"nmax":
			{
			    "desc": "Maximum value of shape parameter",
			    "value": 12.0
			}
		    },
		    "2":		
		    {
			"model":
			{
			    "desc": "Flux model type",
			    "value": "SERSIC"
			},
			"cache":
			{
			    "desc": "Image cube name and path",
			    "value": "p1500n60r100.cache"
			},
			"size":
			{
			    "desc": "Number of pixels per side",
			    "value": 1500
			},
			"slices":
			{
			    "desc": "Number of shape slices",
			    "value": 60
			},
			"rscale":
			{
			    "desc": "Extent of table in radial scale",
			    "value": 100.0
			},
			"nmin":
			{
			    "desc": "Minimum value of shape parameter",
			    "value": 0.5
			},
			"nmax":
			{
			    "desc": "Maximum value of shape parameter",
			    "value": 12.0
			}
		    }
		}
	    },
	    "Point":
	    {
		"X center":
		{
                    "desc": "X center rel to lower corner",
                    "index": 0,
                    "ctrl": "Additive",
                    "value": 0.0
		},
		"Y center":
		{
                    "desc": "Y center rel to lower corner",
                    "index": 1,
                    "ctrl": "Additive",
                    "value": 0.0
		},
		"PS mag":
		{
		    "desc":  "point-source magnitude",
		    "index": 7,
		    "ctrl": "Additive",
                    "value": -20.0
		}
	    },
	    "Sky":
	    {
		"Sky value":
		{
		    "desc": "sky value in ADU counts relative to S",
		    "index": 8,
		    "ctrl": "Multiplicative",
		    "value": 50.0
		}
	    }
	}
    }
}
    
