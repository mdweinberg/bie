#!/usr/bin/python

#================================================================
# Template: one-dimensional GALPHAT model script using
#           Differential Evolution algorithm.  Tests
#           persistent save by deserializing the archive
#           and performing operations on the saved state.
#================================================================

import BIE

BIE.BIEutil.init()

#================================================================
# Deserialize current state
#================================================================
# BIE.BIEutil.setArchiveType('XML') # For testing
BIE.BIEutil.setArchiveType('BINARY') # For efficiency
BIE.BIEutil.getArchiveType()
BIE.BIEutil.prestore('galphat1')

#================================================================
# Print covariance
#================================================================

sstat.PrintDiag()

#================================================================
# Save image for extremal probability values
#================================================================

#---------------------------->Explore one (or small number) of states

solo  = BIE.Simulation.PosteriorProb(si, prior, like)
solo.SetUserLikelihood(fct)

#---------------------------->Compute the ML image

bestL = sstat.getMaxLikeStateVector()
fct.singleImage("ML")
fct.gof(bestL, "X center", "Y center", "PA", "q", "R_e", 1.0)
solo.RunVec(bestL)

#---------------------------->Compute the MAP image

bestP = sstat.getMaxProbStateVector()
fct.singleImage("MAP")
fct.gof(bestP, "X center", "Y center", "PA", "q", "R_e", 1.0)
solo.RunVec(bestP) 

#---------------------------->Compute the marginal likelihood

for m in [500, 1000, 2000]:
    for n in [500, 1000, 2000, 4000, 8000, 16000, 32000]:
        marg = BIE.MarginalLikelihood.MarginalLikelihood(sstat, sim)
        marg.useImportance(m, n)
        marg.setOutfile("marglike.run1")
        marg.setTreeType("TNT")
        marg.compute()
        marg = BIE.MarginalLikelihood.MarginalLikelihood(sstat, sim)
        marg.useImportance(m, n)
        marg.setOutfile("marglike.run1")
        marg.setTreeType("ORB")
        marg.compute()

BIE.BIEutil.quit()
