#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_sf.h>

using namespace std;

std::pair<double, int> solve_x(double n, double bd, double alpha, double beta);

struct my_f_params { double sersicn;
                     double bd;
                     double alpha;
                     double beta;
                   };

double myfunc(double rh, void *params);

int main(int argc, char** argv)
{

  int nalpha;
  int nbeta;
  int nbd;
  int nsersicn;
 
  std::string output;

  po::options_description desc("Make the r_half interpolation table");

  desc.add_options()
    ("help,h",       "this help message")
    ("output,o",     po::value<string>(&output)->default_value("xyzu.table"), "output table name")
    ("nbd,r",        po::value<int>(&nbd)->default_value(100), "B/D grid size")
    ("nalpha,a",     po::value<int>(&nalpha)->default_value(10), "R_half/R_0 grid size")
    ("nbeta,b",      po::value<int>(&nbeta)->default_value(10), "R_d/R_b grid size")
    ("nsersicn,n",   po::value<int>(&nsersicn)->default_value(2.0), "Sersic index grid size");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << endl;
    return 1;
  }

  double sersicn;
  double bd;
  double bt;
  double alpha;
  double beta;

  double min_sersicn = 0.1;
  double min_bd      = 0.01;
  double min_alpha   = 0.3;
  double min_beta    = 1.0;

  double max_sersicn = 11.0;
  double max_bd      = 10.0;
  double max_alpha   = 3.0;
  double max_beta    = 20.0;

  double del_sersicn = (max_sersicn - min_sersicn)/(nsersicn-1);
  double del_bd      = (max_bd      - min_bd     )/(nbd-1);
  double del_alpha   = (max_alpha   - min_alpha  )/(nalpha-1);
  double del_beta    = (max_beta    - min_beta   )/(nbeta-1);
 
  std::ofstream out(output.c_str());

  if (!out) {
    std::cerr << "Error opening file <" << output << ">" << std::endl;
    return  -1;
  }
  
  out << std::setw(10) << nsersicn
      << std::setw(10) << nbd
      << std::setw(10) << nalpha
      << std::setw(10) << nbeta
      << std::endl;
  
  out << std::setw(18) << min_sersicn
      << std::setw(18) << min_bd
      << std::setw(18) << min_alpha
      << std::setw(18) << min_beta
      << std::endl;
  
  out << std::setw(18) << max_sersicn
      << std::setw(18) << max_bd
      << std::setw(18) << max_alpha
      << std::setw(18) << max_beta
      << std::endl;

  unsigned long mean = 0, count = 0;
  
  for (int i=0; i<nsersicn; i++) {

    double sersicn = min_sersicn + del_sersicn*i;

    for (int j=0; j<nbd; j++) {

      double bd = min_bd + del_bd*j;

      for (int k=0; k<nalpha; k++) {

	double alpha = min_alpha + del_alpha*k;

	for (int l=0; l<nbeta; l++) {

	  double beta = min_beta + del_beta*l;

	  std::pair<double, int> ans = solve_x(sersicn, bd, alpha, beta);

	  out << ans.first << endl;
	  mean += ans.second;
	  count++;
	}  
      }
    }
  }

  std::cout << "Mean iteration count = " << static_cast<double>(mean)/count
	    << std::endl;

  return 0;
}

std::pair<double, int> solve_x(double n, double bd, double alpha, double beta)
{

  int status;
  int iter = 0, max_iter = 100;
  const gsl_root_fsolver_type *T;
  gsl_root_fsolver *s;

  double r=0.0;
  double x_lo = 0.0001, x_hi = 1000.0;
  gsl_function F;

  struct my_f_params params = {n,bd,alpha,beta};

  F.function = &myfunc;
  F.params=&params;

  T = gsl_root_fsolver_brent;
//  T = gsl_root_fsolver_bisection;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_lo, x_hi);

  do
   {
     iter++;

     status = gsl_root_fsolver_iterate (s);
     r = gsl_root_fsolver_root (s);
     x_lo = gsl_root_fsolver_x_lower (s);
     x_hi = gsl_root_fsolver_x_upper (s);
     status = gsl_root_test_interval (x_lo, x_hi, 1.0e-6, 1.0e-5);

   } while (status == GSL_CONTINUE && iter < max_iter);

   gsl_root_fsolver_free (s);

   return std::pair<double, int>(r, iter);
}

double myfunc(double x, void *params)
{
  struct my_f_params *p = (struct my_f_params *)params;

  double bd      = p->bd;
  double sersicn = p->sersicn;
  double alpha   = p->alpha;
  double beta    = p->beta;

  double bn = 0.0;
  
  if(sersicn>0.36) { 
    bn = 2.0*sersicn - 1.0/3.0 +
      ((((131./1148175. - 2194697./30690717750./sersicn)/sersicn) + 46.0/25515.)/sersicn + 4./405.)/sersicn;
  }
  else {
    bn = 0.01945 - 0.8902*sersicn + 10.95*sersicn*sersicn -
      19.67*sersicn*sersicn*sersicn + 13.43*sersicn*sersicn*sersicn*sersicn;
  }
  
  double b1 = 1.678;
 
  double xn = bn*pow((alpha*x),1./sersicn);
  double x1 = b1*alpha/beta*x;
  
  double term1 = gsl_sf_gamma_inc_P (2.0*sersicn, xn);  
  double term2 = gsl_sf_gamma_inc_P (2.0, x1);  

  return term1*bd + term2 - 0.5*(bd+1.0);
}
