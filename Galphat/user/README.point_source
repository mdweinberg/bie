Overview and Usage
------------------

The new "Point" model is a point source registered to center of
another component (e.g. "Single" or "BulgeDisk").  A "Point" is added
by incling a "Point" stanza in the config file, just as for "Single"
or "Sky". Currently, the "Point" model has three model fields:

"X center"    the x-position of another component center
"Y center"    the y-position of another component center
"PS mag"      the point source magnitude

Note that the "index" fields for "X center" and "Y center" refer to
the BIE parameter vector values are shared with the fiducial
component.  This model could be trivially generalized to have offsets
from "X center" and "Y center" at the expense of two more parameters.

Implementation
--------------

The "Point" model is implemented by using the Fourier shift theorem to
generate a point source at an arbitrary location in x and y.  A phase
ramp allows a shift to fractional pixel values with only very small
aliasing as long as the PSF is resolved.

Tests and Examples
------------------

The PointSourceTest directory contains data, scripts, and input config
files that I used to test the "Point" model and provides examples of
its usage.

There are two main galaxy images:

(1) a n=2 Sersic profile with r_0=4.95 pixels, q=0.7, pa=45deg with
magnitude -23.

(2) a n=4 Sersic profile (same parameters otherwise).

(3) a n=1 Sersic profile (same parameters otherwise).

The AGN-like point sources have magnitudes -25, -23, -20, -19, -18,
-15.  I computed all of these for the n=2 model but not for the n=1
and n=4 models.


The various scripts labeled sequential from ps2--ps11 run Galphat for
these models.  There is a corresponding set, nopsN (where N is an
integer) that fits a single Sersic model with no point source.  The
scripts compute the marginal likelhood after the chain has converged.
This is just an illustration of how one might do the computation.  I
prefer to use the "prestore" function to read in the binary persistent
store for the simulation and compute the marginal likelihood with
increasing numbers of samples to test the rate of convergence.

For example, a serialized computation in the directory pdir called
galphat_persist_ps3 might be recalled and used to compute the marginal
likelihood as follows:

prestore galphat_persist_ps3

# I am assuming that the Simulation instance was named "sim" and that
# the Ensemble instance was named "sstat".  These variable names
# depend on your script, of course.

set marg = new MarginalLikelihood(sstat, sim)
    marg->useImportance(1000, 2000)
    marg->setTreeType("TNT")
    marg->setOutfile("marglike.ps3")
    marg->compute()

set marg = new MarginalLikelihood(sstat, sim)
    marg->useImportance(1000, 4000)
    marg->setTreeType("TNT")
    marg->setOutfile("marglike.ps3")
    marg->compute()

set marg = new MarginalLikelihood(sstat, sim)
    marg->useImportance(1000, 8000)
    marg->setTreeType("TNT")
    marg->setOutfile("marglike.ps3")
    marg->compute()

The results will be written to the file "marglike.ps3".  Each
successive test will be appended to the file.  If you do not call
setOutfile(), the results go to the standard output.


Test results
------------

A summary of tests used to verify and explore the properties of the
model is as follows.

For the n=2 Sersic profiles, Galphat accurately recovers the
point-source magnitude for M>=-20 and strongly rejects the hypothesis
of a Sersic model sans point source.  For M=-19, the point-source
model is preferred with posterior odds of 10:1.  As M increases,
Galphat becomes biased towards larger point-source M than the true
value.  For M=-19, Galphat recovers M approx. -17.5. For M=-18,
Galphat recovers M=-13.5 and the Bayes factor prefers the pure Sersic
model with posterior odds of 1000:1.

For the n=4 Sersic profiles, the bias toward larger point-source M is
larger and the tendency to prefer a pure Sersic in the presence of a
point source is stronger.  For example, the M=-19 test infers M
approx. -14.5 and prefers the single Sersic model with odds 10000:1
(in other words, the wrong decision).  For the M=-20 model, a magitude
of -18 is recovered, a bias of 2 magnitudes.  However, the hypothesis
of a central point source is still preferred with odds of 4000:1 (the
correction decision).  A larger bias with larger Sersic index is
expected, since the largest index models have high concentration.

I expected the bias to decrease a bit for n=1, over those for n=2, and
I checked M=-18 and M=-19 to test this.  For M=-18 and M=-19 AGN
models, the recovered M is -17.1 and -18.7, respectively.  In both
cases, the posterior odds strongly favor the AGN model: 40000:1 and
10^{52}:1 for the M=-18 and M=-19, respectively.  So the AGN is easier
to detect as the concentration of the galaxy decreases (lower Sersic
n) as expected.

In most of these cases, the only clear covariance is between galaxy
Sersic index and AGN magnitude, and this, only for higher magnitude
(i.e. fainter) AGN.
