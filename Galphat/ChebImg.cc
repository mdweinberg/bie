#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

#include <gsl/gsl_integration.h>

#include <QuadTreeIntegrate.H>
#include <ChebImg.h>
#include <BIEmpi.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::ChebImg)

using namespace BIE::Galphat;

const bool verbose = true;

ChebImg::ChebImg(pt::ptree& tree) 
{
  ID     = "ChebImg";
  normZ  = true;

  //
  // Find the flux model
  //
  try {
    norder = tree.get<int>("order.value");
  } catch (pt::ptree_error &error) {
    std::string msg("Error finding string tag <order.value>");
    if (myid==0) std::cerr << msg << std::endl;
    throw GalphatBadParameter("ChebImg", "constructor", 
			      msg, __FILE__, __LINE__);
  }
  
  try {
    n_min = tree.get<double>  ("expon.min");
    n_max = tree.get<double>  ("expon.max");
    s0    = tree.get<double>  ("expon.scl");
    n_num = tree.get<unsigned>("expon.num");
  } catch (pt::ptree_error &error) {
    if (myid==0) {
      std::string msg("Galphat::ChebImg: "
		      "error finding string tag <expon.min>, <expon.max>, <expon.scl> or <expon.num>");
      std::cerr << msg << std::endl;
    }
    s0    = 0.1;
    n_min = n_max = 1.0;
    n_num = 1;
    if (myid==0) {
      std::cerr << "Galphat::ChebImg: setting s0=" << s0
		<< ", n_min=" << n_min
		<< ", n_max=" << n_max
		<< ", n_num=" << n_num
		<< ", this is probably not what you want"
		<< std::endl;
    }
  }
  
  try {
    rm = tree.get<double>("rmax.value");
  } catch (pt::ptree_error &error) {
    if (myid==0) {
      std::string msg("Error finding string tag <rmax.value>, setting rmax=1.0");
      std::cerr << msg << std::endl;
    }
    rm = 1.0;
  }

  double dn = 0.0;
  if (n_num > 1) dn = (n_max - n_min)/(n_num-1);
  for (unsigned t=0; t<n_num; t++) pvec.push_back(n_min + dn*t);

  poly   = ChebPolyPtr(new ChebPoly(norder-1));
  P      = n_min;		// Set a default value
  mp     = mPtr(new ExpMap(s0, rm));
  inCons = true;
  flux   = FluxInterpBasisPtr(new FluxInterpBasis(tree, this)); // 
  inCons = false;
}

std::vector<double> ChebImg::pixel(double a0, double a1, double b0, double b1, 
				   double eps, int maxlev)
{
  std::vector<double> ret;

  QuadTreeIntegrate<ChebImg> integrate(this);

  ret = integrate.Integral(a0, a1, b0, b1, 1, eps, maxlev);
  ret.push_back(integrate.Error());
  ret.push_back(integrate.NumEvals());

  return ret;
}


