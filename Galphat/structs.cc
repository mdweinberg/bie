#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "fitsio.h"
#include "structs.h"
#include "debug.h"

#include <boost/filesystem.hpp>

int printerror (int status);

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::inpars)

/**
   Read a FITS image and determine the minimum and maximum pixel values
*/
int BIE::Galphat::image::readFITS ()
{
  fitsfile *fptr;
  int status, nfound, anynull=0, errstat=0;
  long fpixel, npix, naxes[2];
  char comment[FLEN_COMMENT];
  
  float nullval;
    
  status = 0;

  if (fits_open_file(&fptr, name.c_str(), READONLY, &status)) {
    err = 1;
    return (1);
  } else
    err = 0;
  
  // Read the image keywords to determine size, gain, rdnoise, etc...
  
  char NAXIS   [] = "NAXIS";
  char GAIN    [] = "GAIN";
  char CCDGAIN [] = "CCDGAIN";
  char ATODGAIN[] = "ATODGAIN";
  char RDNOISE [] = "RDNOISE";
  char NCOMBINE[] = "NCOMBINE";
  char EXPTIME [] = "EXPTIME";

  if (fits_read_keys_lng(fptr, NAXIS, 1, 2, naxes, &nfound, &status))
    printerror( status );

  this->naxes[0] = naxes[0];
  this->naxes[1] = naxes[1];

  if (fits_read_key(fptr, TFLOAT, GAIN, &gain, comment, &status)){
    status=0;
    if (fits_read_key(fptr, TFLOAT, CCDGAIN, &gain, comment, &status)){
      status=0;
      if (fits_read_key(fptr, TFLOAT, ATODGAIN, &gain, comment, &status)) {
	gain = -1.;
	status = 0;
      };
    };
  };

  if (fits_read_key(fptr, TFLOAT, RDNOISE, &rdnoise, comment, &status)) {
    rdnoise  = -1;
    status   = 0;
  };
  if (fits_read_key(fptr, TFLOAT, NCOMBINE, &ncombine, comment, &status)) {
    ncombine = -1.;
    status   = 0;
  };
  if (fits_read_key(fptr, TFLOAT, EXPTIME, &exptime, comment, &status)) {
    exptime  = 1.;
    status   = 0;
    errstat  = 2;      // Return the fact the exposure time is missing
  } else {
    if (adjzpt) {
      magzpt  += 2.5 * log10 (exptime); // adjust the mag
      muzpt   += 2.5 * log10 (exptime); // zeropoints
    }
  };
  
  // number of pixels in the image
  npix = naxes[0] * naxes[1];  
  
  fpixel     = 1;
  nullval    = 0;       // don't check for null values in the image
  
  // Create one-offset vector and matrix.  Notice the needed "&" in
  // passing the pointer to the address which then points to a newly
  // created address space.
  
  int numpix = naxes[1] * naxes[0];
  z.resize(numpix);

  // Note that because fits_read_img needs zero offset vectors, one is
  // added to flux pointer.
  
  if ( fits_read_img(fptr, TFLOAT, fpixel, npix, &nullval,
		     &z[0], &anynull, &status) ) {
    printerror( status );
  }
  
  if ( fits_close_file(fptr, &status) )
    printerror( status );
  
  return (errstat);   // Normal return
}

/**
   Write a FITS image
*/
int BIE::Galphat::image::writeFITS (const std::string& wname)
{
  fitsfile *fptr;
  int status=0, errstat=0;
  long fpixel=1, naxis=2, badpix=0;

  boost::filesystem::path path = boost::filesystem::current_path();
  path /= wname;
  boost::filesystem::remove(path);

  if (fits_create_file(&fptr, path.string().c_str(), &status))
    printerror( status );
  
  if (fits_create_img(fptr, FLOAT_IMG, naxis, &naxes[0], &status))
    printerror( status );

  // Read the image keywords to determine size, gain, rdnoise, etc...
  
  char GAIN    [300] = "GAIN";
  char RDNOISE [300] = "RDNOISE";
  char NCOMBINE[300] = "NCOMBINE";
  char EXPTIME [300] = "EXPTIME";
  char BADPIX  [300] = "BADPIX";

  char GAIN_com    [300] = "gain factor";
  char RDNOISE_com [300] = "read noise value";
  char NCOMBINE_com[300] = "number of combined images";
  char EXPTIME_com [300] = "exposure time in seconds";
  char BADPIX_com  [300] = "number of bad pixels";

  char *gain_str = static_cast<char*>(GAIN);
  char *rdno_str = static_cast<char*>(RDNOISE);
  char *ncom_str = static_cast<char*>(NCOMBINE);
  char *expt_str = static_cast<char*>(EXPTIME);
  char *badp_str = static_cast<char*>(BADPIX);

  char *gain_com = static_cast<char*>(GAIN_com);
  char *rdno_com = static_cast<char*>(RDNOISE_com);
  char *ncom_com = static_cast<char*>(NCOMBINE_com);
  char *expt_com = static_cast<char*>(EXPTIME_com);
  char *badp_com = static_cast<char*>(BADPIX_com);


  this->naxes[0] = naxes[0];
  this->naxes[1] = naxes[1];

  if (fits_update_key(fptr, TFLOAT, gain_str, &gain, gain_com,
		      &status))
    printerror( status );

  if (fits_update_key(fptr, TFLOAT, rdno_str, &rdnoise, rdno_com,
		      &status))
    printerror( status );

  if (fits_update_key(fptr, TFLOAT, ncom_str, &ncombine, ncom_com,
		      &status))
    printerror( status );

  if (fits_update_key(fptr, TFLOAT, expt_str, &exptime, expt_com,
		      &status))
    printerror( status );

  if (fits_update_key(fptr, TFLOAT, badp_str, &badpix, badp_com,
		      &status))
    printerror( status );

  
  int numpix = naxes[1] * naxes[0];

  if ( fits_write_img(fptr, TFLOAT, fpixel, numpix, &z[0], &status) )
    printerror( status );
  
  if ( fits_close_file(fptr, &status) )
    printerror( status );
  
  fits_report_error(stderr, status);

  if (0) {
    std::ofstream test("test_fits.img");
    for (int j=0; j<naxes[1]; j++) {
      for (int i=0; i<naxes[0]; i++) {
	test << std::setw( 8) << i
	     << std::setw( 8) << j
	     << std::setw(16) << z[j*naxes[0]+i]
	     << std::endl;
      }
      test << std::endl;
    }
  }

  return errstat;   // Normal return
}


#define STRLEN 80

int BIE::Galphat::image::readBADPIX()
{
  FILE *filename;
    
  float x, y;
  char string[STRLEN];

  z.resize(naxes[1]*naxes[0]);

  for (int iy = 0; iy < naxes[1]; iy++) {
    for (int ix = 0; ix < naxes[0]; ix++) {
      z[iy*naxes[0]+ix] = 0.;
    };
  };
  
  if ((filename = fopen (name.c_str(), "r")) == (FILE *)0)
    return (1);

  while (fscanf (filename, " %[^\n]", string) != EOF) {
    if (strncmp (string, "#", 1) != 0) {
      sscanf (string, " %f %f", &x, &y);
      int ix = (unsigned long) x;
      int iy = (unsigned long) y;
      if ( ix >= 0 && ix < naxes[0] && iy >= 0 && 
	   iy < naxes[1] )
	z[iy*naxes[0]+ix] = 1.;
    };
  };
  fclose (filename);
  return (0);
}
