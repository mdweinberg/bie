## Makefile for the Galphat directory of BIE
##
## Copyright (C) 2005 Martin Weinberg
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

include $(top_srcdir)/persistence/Makefile.autopersist.am
AUTOPERSIST_FILES = GalphatParam.h GalphatLikelihoodFunction.h		\
	FluxInterp.h FluxFamily.h Sersic.h BernImg.h ChebImg.h		\
	Gauss.h structs.h GalphatModel.h Single.h BulgeDisk.h Point.h	\
	PSF.h Sky.h BernPoly.h ChebPoly.h

AUTOMAKE_OPTIONS = dejagnu
RUNTESTDEFAULTFLAGS = --tool alltests --srcdir $(srcdir)/testsuite

## Process this file with automake to produce Makefile.in
lib_LTLIBRARIES = libGalphat.la

## Include flags.  It does not matter that some of these locations might not exist.
AM_CPPFLAGS = -I$(top_srcdir)/include -I$(top_srcdir)/libVector \
           -I$(top_srcdir)/libsrnd -I$(top_srcdir)/libcutils \
	   -I$(top_srcdir)/Galphat -I$(top_srcdir)/persistence @INCLUDES_ALL@ \
	    @INCLUDES_CCXX@ @bie_mpi_include@


libGalphat_la_SOURCES = printerr.cc Sersic.cc BernImg.cc ChebImg.cc	\
	Gauss.cc FluxFamily.cc structs.cc FluxInterp.cc			\
	GalphatParam.cc GalphatModel.cc Single.cc BulgeDisk.cc		\
	GalphatLikelihoodFunction.cc PSF.cc Sky.cc Point.cc		\
	BernPoly.cc ChebPoly.cc Acquire.cc


noinst_HEADERS = const.h debug.h fitsio.h longnam.h structs.h		\
	GalphatParam.h GalphatLikelihoodFunction.h			\
	QuadTreeIntegrate.H FluxFamily.h Sersic.h BernImg.h ChebImg.h	\
	Gauss.h FluxInterp.h GalphatModel.h Single.h BulgeDisk.h	\
	PSF.h Sky.h Point.h

## Libraries to link in.
LIBS = -lgsl -lgslcblas -lfftw3 -lm -lnetcdf -lreadline -ltermcap	\
-lcfitsio @LIBADD_CCXX@ -lpthread -lz -ldb -lssl -lcrypto -lstdc++	\
-L./.libs

## Kludge . . . 
## Adding libs to prevent linking problems in the cli, need to fix this

AM_CXXFLAGS = -std=c++11 -Wall -Wno-non-virtual-dtor -fPIC		\
-D_G_NO_NRV @bie_cxxflags@ @bie_debug_flags@ -lgsl -lgslcblas -lfftw3	\
-lcfitsio
