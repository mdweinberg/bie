#include "Sky.h"
#include "GalphatLikelihoodFunction.h"

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::Sky)

using namespace BIE::Galphat;

void Sky::findIndices()
{
  GalphatModel::findIndices();

  for (auto v : parname) {
    if (std::string("X sky gradient").compare(v.second) == 0) {
      indexMap[1] = v.first;
      use_x_slope = true;
    } 
    if (std::string("Y sky gradient").compare(v.second) == 0) {
      indexMap[2] = v.first;
      use_x_slope = true;
    } 
  }
}


void Sky::assignFlux(GalphatLikelihoodFunction* lf, int lev, 
		     inpars_ptr input, image_ptr img)
{
  setup(input, img);

  int indx = indexMap[0];

  double sky;
  if (ia[indx] < 0)
    sky = d[indx];
  else if (ia[indx] > 1) 
    sky = bgoffset * a[indx];
  else if (ia[indx] > 0)
    sky = bgoffset + a[indx];
  else
    sky = a[indx];

  double dskydx = 0.0;
  double dskydy = 0.0;

  if (use_x_slope) dskydx = a[indexMap[1]];
  if (use_y_slope) dskydy = a[indexMap[2]];

  double xcent  = (xmax + xmin)/2.0, x;
  double ycent  = (ymax + ymin)/2.0, y;
      
  for (int j=0; j<numy; j++) {
    y = Ymin + dy*j;
    for (int i=0; i<numx; i++) {
      x = Xmin + dx*i;

      double val = 
	sky*dx*dy + (x-xcent)*dskydx + (y-ycent)*dskydy;

      if (val < 0.0) {
	std::cout << "Model(Sky) pixel is <0: " << i << " " << j 
		  << " " << sky << std::endl;
	std::cout << "Value: " << val << " dx=" << dx << " dy=" << dy
		  << " sky=" << " " << a[0];
	if (use_x_slope) std::cout << " dSky/dx=" << a[1];
	if (use_y_slope) std::cout << " dSky/dy=" << a[2];
	std::cout << std::endl;
				   
      } else {
	lf->totalflux[level][j*numx+i] += val;
      }

    }
  }
}

