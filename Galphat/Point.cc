#include "GalphatLikelihoodFunction.h"
#include "Point.h"

#include <boost/optional.hpp>

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::Galphat::Point)

using namespace BIE::Galphat;

Point::Point(pt::ptree::value_type& vt) : flexible(false)
{
  initialize(vt);

  // Look for additional parameters
  //
  boost::optional<pt::ptree&> features = vt.second.get_child_optional("features");

  if (features) {

    foreach_(pt::ptree::value_type const&v, *features) {
    
      if (v.first.compare("flexible") == 0) {
	try {
	  flexible = vt.second.get<bool>("features.flexible.value");
	} catch (pt::ptree_error &error) {
	  std::ostringstream sout;
	  sout << "No value field stanza <" << v.first << ">";
	  throw GalphatBadParameter("Point", "Point", sout.str(), 
				    __FILE__, __LINE__);
	}
      }
    }
  }
}


image_ptr Point::AggregatePSF(int npix)
{
  // No aggregation
  //
  if (npix==1) return psf;

  // Otherwise, aggregate . . . 
  //
  image_ptr new_img  = image_ptr(new struct image);
  
  if (new_img.get() == 0) {
    cout << "Error in allocating memory for aggregation...." << endl;
    exit (1);
  };
  
  int xmin = psf->imgsect[0];
  int xmax = psf->imgsect[1];
  int ymin = psf->imgsect[2];
  int ymax = psf->imgsect[3];

  for (int i=0; i<4; i++) new_img->imgsect[i] = psf->imgsect[i];

  new_img->naxes[0]  = psf->naxes[0]/npix;
  new_img->naxes[1]  = psf->naxes[1]/npix;
  
  new_img->z.resize(new_img->naxes[1]*new_img->naxes[0]);
  
  for (int j=ymin; j<=ymax; j++) {
    for (int i=xmin; i<=xmax; i++)
      new_img->z[(j-ymin)*new_img->naxes[0]+i-xmin] = psf->z[(j-1)*psf->naxes[0]+i-1];
  }
  
  for (int j=0; j < new_img->naxes[1]; j++) {    
    for (int i=0; i < new_img->naxes[0]; i++) {
      new_img->z[j*new_img->naxes[0]+i] = 0.0;
      for (int k=0; k<npix; ++k) 
	for (int m=0; m<npix; ++m) 
	  new_img->z[j*new_img->naxes[0]+i] += 
	    psf->z[(npix*j+k)*psf->naxes[0]+(npix*i+m)];
    }
  }
  
  return new_img;
}


void Point::levelSetup(int Numx, int Numy, int lev, int npix)
{
  if (levs.find(lev) == levs.end()) {

    levs.insert(lev);

    Nx[lev] = Numx;
    Ny[lev] = Numy;

    image_ptr img = AggregatePSF(npix);

    //
    // Get image size
    //
    int imgx = img->Size().first;
    int imgy = img->Size().second;
    
    //
    // Get normalization of IMG
    //
    double sum = 0.0;
    for (int j=0; j < imgy; j++)
      for (int i=0; i < imgx; i++) 
	sum += (*img)(i, j);
    
    //
    // Making a new pixel map with same size of image data
    //
    int xoffset, yoffset;
    image_ptr newpsf = image_ptr(new image);
  
    newpsf->naxes[0] = Numx;
    newpsf->naxes[1] = Numy;
    
    newpsf->imgsect[0] = 1;
    newpsf->imgsect[1] = Numx;
    newpsf->imgsect[2] = 1;
    newpsf->imgsect[3] = Numy;
    
    newpsf->z.resize(newpsf->naxes[1]*newpsf->naxes[0]);

    for (int j=0; j<Numy; j++) 
      for (int i=0; i<Numx; i++) {
	(*newpsf)(i, j) = 0.0;
      }
    
    if (imgx > Numx || imgy > Numy) {
      xoffset = round<int, double>(0.5*(imgx - Numx));
      yoffset = round<int, double>(0.5*(imgy - Numy));
      for (int j=0; j<Numy; j++) {
	for (int i=0; i<Numx; i++) {
	  (*newpsf)(i, j) = (*img)(i+xoffset, j+yoffset);
	}
      }
    } else {
      xoffset = round<int, double>(0.5*(Numx - imgx));
      yoffset = round<int, double>(0.5*(Numy - imgy));
      for (int j=0; j<imgy; j++) 
	for (int i=0; i<imgx; i++) {
	  (*newpsf)(i+xoffset, j+yoffset) = (*img)(i, j)/sum;
	}
    }
  
    int mxbeg = 0, mybeg = 0;
    int pxbeg = 0, pybeg = 0;
    
    int numx  = imgx;
    int numy  = imgy;
    int nfrq  = Numx/2+1;
    
    if (Numx > imgx) {
      mxbeg = round<int, double>(0.5*(Numx - imgx));
    } else {
      pxbeg = round<int, double>(0.5*(imgx - Numx));
      numx = Numx;
    }
    
    if (Numy > imgy) {
      mybeg = round<int, double>(0.5*(Numy - imgy));
    } else {
      pybeg = round<int, double>(0.5*(imgy - Numy));
      numy = Numy;
    }

    image_ptr modimg = image_ptr(new image);
  
    modimg->naxes[0] = Numx;
    modimg->naxes[1] = Numy;
    
    modimg->imgsect[0] = 1;
    modimg->imgsect[1] = Numx;
    modimg->imgsect[2] = 1;
    modimg->imgsect[3] = Numy;

    modimg->z.resize(modimg->naxes[1]*modimg->naxes[0]);

    // Embed the image
    //
    for (int j=0; j<numy; j++) {
      for (int i=0; i<numx; i++) {
	(*modimg)(i+mxbeg, j+mybeg) = (*img)(i+pxbeg, j+pybeg);
      }
    }
    
    ftpsf1[lev] .resize(Numy*nfrq);
    ftpsf2[lev] .resize(Numy*nfrq);

    imgwk1[lev] .resize(Numy*Numx);
    imgwk2[lev] .resize(Numy*Numx);
    
    planF[lev]  = fftw_plan_dft_r2c_2d(Numy, Numx, &imgwk1[lev][0], reinterpret_cast<fftw_complex*>(&ftpsf1[lev][0]), FFTW_ESTIMATE);
    planI[lev]  = fftw_plan_dft_c2r_2d(Numy, Numx, reinterpret_cast<fftw_complex*>(&ftpsf2[lev][0]), &imgwk2[lev][0], FFTW_ESTIMATE);
    
    // Zero the vector
    //
    std::fill(ftpsf1[lev].begin(), ftpsf1[lev].end(), 0.0);
    
    // Copy img to imgwk
    //
    for (int j=0; j<Numy; j++) {
      for (int i=0; i<Numx; i++) {
	imgwk1[lev][j*Numx+i] = (*modimg)(i, j);
      }
    }
  
    // DFT of the img
    //
    fftw_execute(planF[lev]);
  }
}


image_ptr Point::ShiftCenter(int lev, double x, double y)
{
  typedef std::complex<double> Complex;

  Numx = Nx[lev];
  Numy = Ny[lev];

  // Shift the DFT
  
  double xphase = x*2.0*M_PI/Numx;
  double yphase = y*2.0*M_PI/Numy;

  Complex I(0, 1), one(1, 0);
  Complex facx = exp(-I*xphase);
  Complex facy = exp(-I*yphase);
  Complex facX, facY;

  double scale = 1.0/Numx/Numy;

  int nfrq = Numx/2+1;

  facY = one;
  for (int j=0; j<Numy/2; j++) {
    facX = one;
    for (int i=0; i<nfrq; i++) {
      int ij = j*nfrq + i;

      ftpsf2[lev][ij] = ftpsf1[lev][ij] * facX * facY * scale;

      facX *= facx;
    }
    facY *= facy;
  }

  facY = one/facy;
  for (int j=Numy-1; j>=Numy/2; j--) {
    facX = one;
    for (int i=0; i<nfrq; i++) {
      int ij = j*nfrq + i;

      ftpsf2[lev][ij] = ftpsf1[lev][ij] * facX * facY * scale;

      facX *= facx;
    }
    facY /= facy;
  }


  // Inverse DFT to get the shifted image

  fftw_execute(planI[lev]);

  image_ptr modimg = image_ptr(new image);
  
  modimg->naxes[0] = Numx;
  modimg->naxes[1] = Numy;
    
  modimg->imgsect[0] = 1;
  modimg->imgsect[1] = Numx;
  modimg->imgsect[2] = 1;
  modimg->imgsect[3] = Numy;

  modimg->z.resize(modimg->naxes[1]*modimg->naxes[0]);

  for (int j=0; j<Numy; j++) {
    for (int i=0; i<Numx; i++) {
      (*modimg)(i, j) = imgwk2[lev][j*Numx+i];
    }
  }

  return modimg;
}


Point::~Point()
{
  for (auto v : planF ) fftw_destroy_plan(v.second);
  for (auto v : planI ) fftw_destroy_plan(v.second);
}

void Point::assignFlux(GalphatLikelihoodFunction* lf, int lev, 
		       inpars_ptr inp, image_ptr img)
{
  int numx = img->Size().first;
  int numy = img->Size().second;
  int npix = inp->aggfact;

  levelSetup(numx, numy, lev, npix);

  setup(inp, img);

  double x0, x1, mag;

  int indx0 = indexMap[0];
  int indx1 = indexMap[1];
  int indxM = indexMap[2];

  //----------------------------------------
  // X center
  //----------------------------------------

  if (ia[indx0]<0)
    x0 = d[indx0];		// Fixed
  else if (ia[indx0]==1)
    x0 = d[indx0] + a[indx0];	// Additive
  else if (ia[indx0]==2)
    x0 = d[indx0] * a[indx0];	// Multiplicative
  else
    x0 = a[indx0];		// Default

  //----------------------------------------
  // Y center
  //----------------------------------------

  if (ia[indx1]<0)
    x1 = d[indx1];
  else if (ia[indx1]==1)
    x1 = d[indx1] + a[indx1];	// Additive
  else if (ia[indx1]==2)
    x1 = d[indx1] * a[indx1];	// Multiplicative
  else
    x1 = a[indx1];		// Default

  //----------------------------------------
  // Mag
  //----------------------------------------

  const double moffset = 1.0;

  if (ia[indxM]<0)		// Fixed
    mag = d[indxM];
  else if (ia[indxM]==1)	// Additive
    mag = d[indxM] + a[indxM];
  else if (ia[indxM]==2)	// Weibull flux
    mag = a[indxM] - moffset + d[indxM];
  else				// Default
    mag = a[indxM];


  //----------------------------------------
  // Flexible center
  //----------------------------------------

  if (flexible) {

    int indx3 = indexMap[3];
    int indx4 = indexMap[4];

    if (indx3 >= 0) {
				// Update X center
      if (ia[indx3]<0)
	x0 += d[indx3];		// Fixed
      else if (ia[indx3]==1)
	x0 += d[indx3] + a[indx3]; // Additive
      else if (ia[indx3]==2)
	x0 += d[indx3] * a[indx3]; // Multiplicative
      else
	x0 += a[indx3];		// Default
    }

    if (indx4 >= 0) {
				// Update Y center
      if (ia[indx4]<0)
	x1 += d[indx4];		// Fixed
      else if (ia[indx4]==1)
	x1 += d[indx4] + a[indx4]; // Additive
      else if (ia[indx4]==2)
	x1 += d[indx4] * a[indx4]; // Multiplicative
      else
	x1 += a[indx4];		// Default
    }
  }

  //----------------------------------------

  double totcnt = pow (10.0, (img->magzpt - mag)/ 2.5);

  image_ptr shft = ShiftCenter(lev, x0, x1);

  for (int j=0; j<numy; j++) {
    for (int i=0; i<numx; i++) {
      lf->totalflux[level][j*numx+i] += (*shft)(i, j) * totcnt;
    }
  }

}
