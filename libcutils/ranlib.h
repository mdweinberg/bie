/* Prototypes for all user accessible RANLIB routines */

#ifdef __cplusplus

extern "C" void advnst(long k);
extern "C" float genbet(float aa,float bb);
extern "C" float genchi(float df);
extern "C" float genexp(float av);
extern "C" float genf(float dfn, float dfd);
extern "C" float gengam(float a,float r);
extern "C" void genmn(float *parm,float *x,float *work);
extern "C" void genmul(long n,float *p,long ncat,long *ix);
extern "C" float gennch(float df,float xnonc);
extern "C" float gennf(float dfn, float dfd, float xnonc);
extern "C" float gennor(float av,float sd);
extern "C" void genprm(long *iarray,int larray);
extern "C" float genunf(float low,float high);
extern "C" void getsd(long *iseed1,long *iseed2);
extern "C" void gscgn(long getset,long *g);
extern "C" long ignbin(long n,float pp);
extern "C" long ignnbn(long n,float p);
extern "C" long ignlgi(void);
extern "C" long ignpoi(float mu);
extern "C" long ignuin(long low,long high);
extern "C" void initgn(long isdtyp);
extern "C" long mltmod(long a,long s,long m);
extern "C" void phrtsd(char* phrase,long* seed1,long* seed2);
extern "C" float ranf(void);
extern "C" void setall(long iseed1,long iseed2);
extern "C" void setant(long qvalue);
extern "C" void setgmn(float *meanv,float *covm,long p,float *parm);
extern "C" void setsd(long iseed1,long iseed2);
extern "C" float sexpo(void);
extern "C" float sgamma(float a);
extern "C" float snorm(void);

#else


extern void advnst(long k);
extern float genbet(float aa,float bb);
extern float genchi(float df);
extern float genexp(float av);
extern float genf(float dfn, float dfd);
extern float gengam(float a,float r);
extern void genmn(float *parm,float *x,float *work);
extern void genmul(long n,float *p,long ncat,long *ix);
extern float gennch(float df,float xnonc);
extern float gennf(float dfn, float dfd, float xnonc);
extern float gennor(float av,float sd);
extern void genprm(long *iarray,int larray);
extern float genunf(float low,float high);
extern void getsd(long *iseed1,long *iseed2);
extern void gscgn(long getset,long *g);
extern long ignbin(long n,float pp);
extern long ignnbn(long n,float p);
extern long ignlgi(void);
extern long ignpoi(float mu);
extern long ignuin(long low,long high);
extern void initgn(long isdtyp);
extern long mltmod(long a,long s,long m);
extern void phrtsd(char* phrase,long* seed1,long* seed2);
extern float ranf(void);
extern void setall(long iseed1,long iseed2);
extern void setant(long qvalue);
extern void setgmn(float *meanv,float *covm,long p,float *parm);
extern void setsd(long iseed1,long iseed2);
extern float sexpo(void);
extern float sgamma(float a);
extern float snorm(void);

#endif
