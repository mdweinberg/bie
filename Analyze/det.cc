using namespace std;

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>

namespace ublas = boost::numeric::ublas;

int determinant_sign(const ublas::permutation_matrix<std::size_t>& pm)
{
  int pm_sign=1;
  std::size_t size = pm.size();
  for (std::size_t i = 0; i < size; ++i)
    // swap_rows would swap a pair of rows here, so we change sign
    if (i != pm(i)) pm_sign *= -1.0;
  return pm_sign;
}

double determinant(ublas::matrix<double>& m) 
{
  ublas::permutation_matrix<std::size_t> pm(m.size1());
  double det = 1.0;
  if (ublas::lu_factorize(m, pm)) {
    det = 0.0;
  } else {
    // multiply by elements on diagonal
    for(unsigned i = 0; i < m.size1(); i++) det *= m(i, i);
    det = det * determinant_sign( pm );
  }
  return det;
}

