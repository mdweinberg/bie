//----------------------------------------------------------------------
//
//  Compute a Bernstein polynomial from the coefficients given on the
//  command line or in a file
//
//----------------------------------------------------------------------

using namespace std;

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cfloat>
#include <vector>

#include <boost/program_options.hpp>

#include <BernsteinPoly.h>
#include "config.h"

namespace po = boost::program_options;

int
main(int argc, char** argv)
{
  //--------------------------------------------------
  // Parameters
  //--------------------------------------------------
  std::string infile, outfile;
  unsigned number, dim;
  
  typedef std::vector<double> DV;

  //--------------------------------------------------
  // Declare the supported options.
  //--------------------------------------------------

  po::options_description desc("Available options");
  desc.add_options()
    ("help,h",		"Produce help message")
    ("version,v",	"Display version info and exit")
    ("number,n",	po::value<unsigned>(&number)->default_value(400),
     "number of points in the output plot")
    ("order,m",	po::value<unsigned>(&dim)->default_value(0),
     "order of Bernstein polynomial")
    ("coefficients,c",	po::value<DV>(),
     "m+1 polynomial coefficients")
    ("input-file,i",	po::value<string>(&infile),
     "File containing dimension and coefficients (instead of on command line)")
    ("output-file,o",	po::value<string>(&outfile)->default_value("bern.dat"),
     "Output data file name")
    ;

  po::positional_options_description p;
  p.add("coefficients", -1);


  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(desc).
	      positional(p).run(), vm);
    po::notify(vm);    
  } catch(boost::program_options::error& e){
    std::cerr << "Invalid_option_value exception thrown parsing config file:"
	      << std::endl << e.what() << std::endl;
    return 2;
  } catch(std::exception e){
    std::cerr <<"Exception thrown parsing config file:" 
	      << std::endl << e.what() << std::endl;
    return 2;
  }

  if (vm.count("help")) {
    std::cout << std::setfill('-') << setw(76) << '-' 
	      << std::endl << std::setfill(' ')
	      << "Compute Bernstein polynomial from the dimensionality and "
	      << "coefficients." << std::endl
	      << std::setfill('-') << setw(76) << '-' 
	      << std::endl << std::setfill(' ')
	      << "Usage: " << argv[0] << " [options]" << std::endl
	      << std::setfill('-') << setw(76) << '-' 
	      << std::endl << std::setfill(' ')
	      << desc << std::endl;
    return 1;
  }

  if (vm.count("version")) {
    std::cout << argv[0] << " " << PACKAGE_VERSION 
	      << " version " << VERSION << std::endl;
    return 1;
  }

  DV coef;
  if (vm.count("coefficients")) {
    coef = vm["coefficients"].as<DV>();
  }

  if (infile.size() == 0) {
    std::cout << "Order=" << dim << std::endl;
    for (size_t i=0; i<coef.size(); i++) {
      std::cout << "==> coef[" << i << "] = " << coef[i] << std::endl;
    }    
    std::cout << std::endl;

    if (coef.size() != dim+1) {
      std::cout << "Need " << dim+1 << " coefficients for order=" << dim
		<< std::endl;
      return 3;
    }
    
  } else {
    // Try to open coefficient file
    std::ifstream in(infile.c_str());
    if (in) {
      in >> dim;
      while (in.good()) {
	double v;
	in >> v;
	if (in.good()) coef.push_back(v);
      }

      if (coef.size() != dim+1) {
	std::cout << "Need " << dim+1 << " coefficients for order=" << dim
		  << std::endl;
	return 3;
      }
    } else {
      std::cout << "Could not open coefficient file: <" << infile << ">"
		<< std::endl;
      return 4;
    }
  }

  BernsteinPoly bern(dim);

  double dx = 1.0/(number - 1);

  std::ofstream out(outfile.c_str());

  if (out.good()) {
    for (size_t i=0; i<number; i++) {
      double x = dx*i;
      double y = bern(coef, x);
      out << std::setw(16) << x
	  << std::setw(16) << y
	  << std::endl;
    }
  } else {
      std::cout << "Could not open output file: <" << outfile << ">"
		<< std::endl;
      return 6;
  }

  return 0;
}
