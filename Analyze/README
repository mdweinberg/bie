-*- text -*-

This is a test routine for an MCMC convergence diagnostic based on the
convergence diagnostic by Gelman and Rubin (1992).

Gelman and Rubin (1992) propose a general approach to monitoring
convergence of MCMC output in which two or more parallel chains are
run, with starting values that are over dispersed relative to the
posterior distribution. Convergence is diagnosed when the chains have
"forgotten" their initial values, and the output from all chains is
indistinguishable. The diagnostic is applied to a single variable from
the chain. It is based a comparison of within-chain and between-chain
variances, and is similar to a classical analysis of variance.

Here we use a generalization by:

   	Giakoumatos, Vrontos, Dellaportas and Politis, "An MCMC 
	Convergence Diagnostic using Subsampling" (1999), preprint 
	to appear in to in Journal of Computational and Graphical 
	Statistics. 

They propose a bootstrap analysis of a single chain to diagnose
asymptotic normality and determine the burn in period.  To do this
one adopts a consistent statistic for variables in the chain.  Here I
use the mean but some quantile might be a good choice.  The behavior
of the deviations of subsamples is then examined.  A linear
correlation analysis in 1/sqrt(N) and the maximum deviation determines
the burn in period.  The confidence regions in each parameter can be
determined and used to stop the chain.  For our
purposes---characterizing the posterior distribution---some
dimenensionally dependent number of points past burn in might be more
appropriate.

M. Weinberg 02/13/00


testcovar and logcovar codes do analysis of the covariance matrix
and generate random samples with the same covariance as the data
for testing the sample generate algorithm

M. Weinberg 09/30/00
