
#ifndef Regression_H
#define Regression_H

#include <cmath>
#include <vector>
#include <utility>

/**
   Compute linear regression for a vector tuple
 */
class Regression
{
private:
  Regression( const Regression &rhs );

public:
  typedef std::pair<double, double> tuple;

  //! Consructor
  Regression() {}

  //! Destructor
  virtual ~Regression() {}

  /**
     Linear regression info for each pair

    A regression line, defined by the equation y' = a + bx, where a and
    b are constants.  The constant a is the y intercept when x == 0.
    The constant b is the slope of the line.

  */
  class lineInfo
  {
  private:
    /** the slope of the line (e.g., b in y' = a + bx) */
    double slope_;
    /** the y-intercept (e.g., a, when x == 0 in y' = a + bx) */
    double intercept_;
    /** standard deviation of the points around the line */
    double stddev_;
    /** Regression error */
    double slopeError_;
    /** correlation between the x points and the y points */
    double cor_;
  public:
    /** constructor */
    lineInfo()
    {
      slope_ = 0;
      intercept_ = 0;
      cor_ = 0;
      stddev_;
      slopeError_ = 0;
    }
    ~lineInfo() {}
    /** copy constructor */
    lineInfo( const lineInfo &rhs )
    {
      slope_ = rhs.slope_;
      intercept_ = rhs.intercept_;
      cor_ = rhs.cor_;
      stddev_ = rhs.stddev_;
      slopeError_ = rhs.slopeError_;
    }
    
    double slope() { return slope_; }
    void slope( double s ) { slope_ = s; }

    double intercept() { return intercept_; }
    void intercept( double a ) { intercept_ = a; }

    double stddev() { return stddev_; }
    void stddev( double s ) { stddev_ = s; }

    double corr() { return cor_; }
    void corr( double c ) { cor_ = c; }

    double slopeErr() { return slopeError_; }
    void slopeErr(double e) { slopeError_ = e; }
  }; // class lineInfo

  double meanX( std::vector<tuple> &data ) const;
  double meanY( std::vector<tuple> &data ) const;

  /**
     Calculate the linear regression on a set of N points,
     {Xi, Yi}.
   */
  void compute( std::vector<tuple>& data, 
		lineInfo& info ) const;

}; // Regression

#endif
