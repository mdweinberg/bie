//----------------------------------------------------------------------
//  Autocorrelation function from state log file
//----------------------------------------------------------------------

using namespace std;

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cfloat>
#include <vector>
#include <deque>

#include <unistd.h>

#include <StateFile.h>
#include <AutoCorr.H>

string inputfile("multilevel.dat");
string outputtag("autocorr");
string cachefile(".cache.evidence");

void header(ostream& out, StateFile& sf, vector<AutoCorr::acorrInfo>& info);
void shortform(ostream& out, StateFile& sf, vector<AutoCorr::acorrInfo>& info);

int
main(int argc, char** argv)
{
  //--------------------------------------------------
  // Global variables
  //--------------------------------------------------

				// Use this level
  int level     = 0;
				// Number of states to skip (-1->use last half)
  int skip      = -1;
				// Data stride 
				// (e.g. strd=2 means skip every other point)
  int strd      =  1;
				// Number of states to keep (-1->use all states)
  int keep      = 1000000;
				// Read and write cache file
  bool cache    = true;
				// Produce summary only
  bool summary = false;

  //--------------------------------------------------
  // Parse command line
  //--------------------------------------------------

  int c;
  while (1) {
    c = getopt (argc, argv, "i:o:C:l:s:j:k:L:Szh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'l': level   = atoi(optarg);      break;
      case 's': skip    = atoi(optarg);      break;
      case 'j': strd    = atoi(optarg);      break;
      case 'k': keep    = atoi(optarg);      break;
      case 'S': summary = true;              break;
      case 'z': cache   = false;             break;
      case 'i': inputfile.erase(); inputfile = optarg; break;
      case 'o': outputtag.erase(); outputtag = optarg; break;
      case 'C': cachefile.erase(); cachefile = optarg; break;
      case 'h': 
      case '?': 
	string msg = "usage: " + string(argv[0]) + " [options]\n";
	msg += "\t-l int\t\tLevel to analyze (default: 0)\n";
	msg += "\t-s int\t\tStates to skip (first half if neg, default)\n";
	msg += "\t-j int\t\tSpacing of states or frequency (default: 1, use all)\n";
	msg += "\t-k int\t\tStates to keep (use all if neg, default: 1,000,000)\n";
	msg += "\t-L float\t\tStop when autocorr is below this value (default: -1e20)\n";
	msg += "\t-S \t\tSummary output only\n";
	msg += "\t-z \t\tIgnore cache file\n";
	msg += "\t-i string\tinput file\n";
	msg += "\t-o string\toutput file tag\n";
	msg += "\t-C string\tcache the posterior sample to this file\n";
	cerr << msg; exit(-1);
      }
  }

  //--------------------------------------------------
  // Open log file
  //--------------------------------------------------

  ofstream output, cellpt;

  //--------------------------------------------------
  // Get StateFile
  //--------------------------------------------------

  string cache_file, limit_file;
  if (cache) cache_file = cachefile;

  bool ignore = false;
  bool quiet  = true;

  StateFile sf(inputfile, cache_file, limit_file,
	       level, skip, strd, keep, ignore, quiet);


  //--------------------------------------------------
  // Sanity check
  //--------------------------------------------------

  if (sf.prob.size()<3) {
    cout << "There are two or fewer points, exiting . . ." << endl;
    return(-1);
  }

  //--------------------------------------------------
  // Dump first half
  //--------------------------------------------------

  if (skip<0) {
    long cp = sf.prob.size()/2 - sf.popped/2;
    if (cp>0) sf.prob.erase(sf.prob.begin(), sf.prob.begin() + cp);
  }
  

  //--------------------------------------------------
  // Compute the autocorrelation
  //--------------------------------------------------

  const size_t ldef[] = {0, 
			 1,    2,    3,    5,
			 10,   20,   30,   50, 
			 100,  200,  300,  500, 
			 1000, 2000, 3000, 5000};

  const size_t ndef = sizeof(ldef)/sizeof(size_t);

  vector<size_t> lags;  
  for (size_t n=0; n<ndef; n++) {
    if (sf.prob.size()>ldef[n]) lags.push_back(ldef[n]);
  }

  vector<AutoCorr::acorrInfo> info;
  AutoCorr ac(lags);

  ac.ACF(sf.prob, info);

  if (summary) shortform(cout, sf, info);
  else {

    header(cout, sf, info);

    for (size_t l=0; l<lags.size(); l++) {
      cout << setw(8) << lags[l];
      for (size_t i=0; i<info.size(); i++)
	cout << setw(18) << info[i].points()[l];
      cout << endl;
    }
  }

  return (0);
}

void header(ostream& out, StateFile& sf, vector<AutoCorr::acorrInfo>& info)
{
  size_t dim = sf.prob[0].point.size();

  // Horizontal rule
  out << left << setfill('-') << setw(8) << "#";
  for (size_t n=0; n<dim; n++) out << setw(18) << left << "+";
  out << endl << setfill(' ');
  
  // Column labels
  out << left << setw(8) << "# lag ";
  for (size_t n=0; n<dim; n++) {
    string fld = "| " + sf.label(n);
    out << setw(18) << left << fld;
  }
  out << endl;
  
  // Column numbers
  out << left << setfill(' ') << setw(8) << "# [1]";
  for (size_t n=0; n<dim; n++) {
    ostringstream fld;
    fld << "| [" << n+2 << "]";
    out << setw(18) << left << fld.str();
  }
  out << endl;

  // Horizontal rule
  out << left << setfill('-') << setw(8) << "#";
  for (size_t n=0; n<dim; n++) out << setw(18) << left << "+";
  out << endl << setfill(' ');

  // Print ACL info
  cout << setw(8) << "# ACL";
  for (size_t i=0; i<info.size(); i++) {
    ostringstream sout;
    sout << "| " << setprecision(1) << fixed << info[i].length();
    cout << setw(18) << sout.str();
  }
  cout << endl;

  // Print slope info
  cout << setw(8) << "# Expon";
  for (size_t i=0; i<info.size(); i++) {
    ostringstream sout;
    sout << "| " << setprecision(1) << fixed << info[i].slope();
    cout << setw(18) << sout.str();
  }
  cout << endl;
  
  // Horizontal rule
  cout << left << setfill('-') << setw(8) << "#-";
  for (size_t n=0; n<dim; n++) cout << setw(18) << left << "+";
  cout << endl << setfill(' ');


}

void shortform(ostream& out, StateFile& sf, vector<AutoCorr::acorrInfo>& info)
{
  const size_t wid = 18;
  size_t dim = sf.prob[0].point.size();
  
  // Horizontal rule
  out << left << setfill('-') << setw(wid) << "-"
      << setw(wid) << left << "-"
      << setw(wid) << left << "-"
      << endl << setfill(' ');

  out << left << setw(wid) << "Label"
      << setw(wid) << "ACL" << setw(wid) << "Expon" << endl;

  // Horizontal rule
  out << left << setfill('-') << setw(wid) << "-"
      << setw(wid) << left << "-"
      << setw(wid) << left << "-"
      << endl << setfill(' ');

  for (size_t n=0; n<dim; n++) {
    out << setw(wid) << sf.label(n)
	<< setw(wid) << info[n].length()
	<< setw(wid) << info[n].slope()
	<< endl;
  }
  
  // Horizontal rule
  out << left << setfill('-') << setw(wid) << "-"
      << setw(wid) << left << "-"
      << setw(wid) << left << "-"
      << endl << setfill(' ');
}

