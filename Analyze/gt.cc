// Need global table to be defined for resolving symbols in BIE libraries

#include <GlobalTable.h>
#include <VectorM.h>
#include <TestUserState.h>

// This forces the library to load to resolve a symbol, yuck.
VectorM _xyx;

// This forces the library to load to resolve a symbol, yuck.
TestUserState _user("test.tmp", 0, true);

std::vector<global_table_struct> global_variables = {
  {NULL,'0',"",NULL}
};


