//----------------------------------------------------------------------
//
//  Compute the Bernstein polynomial implied by a posterior distribution
//
//----------------------------------------------------------------------

using namespace std;

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cfloat>
#include <vector>
#include <list>
#include <map>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>

#include <BernsteinPoly.h>
#include "config.h"

namespace po = boost::program_options;

int
main(int argc, char** argv)
{
  //--------------------------------------------------
  // Parameters
  //--------------------------------------------------
  std::string infile, outfile;
  unsigned    spos, fpos, number, skip, type;
  double      qmin, qmax, beta;

  std::vector<double> comps;
  
  //--------------------------------------------------
  // Declare the supported options.
  //--------------------------------------------------

  po::options_description desc("Available options");
  desc.add_options()
    ("help,h",		"Produce help message")
    ("version,v",	"Display version info and exit")
    ("number,n",	po::value<unsigned>(&number)->default_value(100),
     "number of points in the output plot")
    ("skip,k",		po::value<unsigned>(&skip)->default_value(0),
     "number of leading states to skip")
    ("start,1",		po::value<unsigned>(&spos)->default_value(0),
     "index of first Bernstein coefficient")
    ("finish,2",	po::value<unsigned>(&fpos)->default_value(INT_MAX),
     "index of findal Bernstein coefficient")
    ("min,q",		po::value<double>(&qmin)->default_value(10),
     "inner quantile")
    ("max,Q",		po::value<double>(&qmax)->default_value(90),
     "outer quantile")
    ("type,t",		po::value<unsigned>(&type)->default_value(0),
     "data type: 0=quartic, 1=powerlaw, 2=mixture")
    ("exponent,b",	po::value<double>(&beta)->default_value(4.0),
     "power-law exponent")
    ("comps",		po::value<std::vector<double> >(&comps)->multitoken(),
     "mixture: c1, s1, w1, c2, s2, w2")
    ("input-file,i",	po::value<string>(&infile),
     "File containing the posterior distribution file")
    ("output-file,o",	po::value<string>(&outfile)->default_value("bernplot.dat"),
     "Output data file name")
    ;

  po::variables_map vm;

  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);    
  } catch(boost::program_options::error& e){
    std::cerr << "Invalid_option_value exception thrown parsing config file:"
	      << std::endl << e.what() << std::endl;
    return 2;
  } catch(std::exception e){
    std::cerr <<"Exception thrown parsing config file:" 
	      << std::endl << e.what() << std::endl;
    return 2;
  }

  if (vm.count("help")) {
    std::cout << std::setfill('-') << setw(76) << '-' 
	      << std::endl << std::setfill(' ')
	      << "Compute Bernstein polynomial from the dimensionality and "
	      << "coefficients." << std::endl
	      << std::setfill('-') << setw(76) << '-' 
	      << std::endl << std::setfill(' ')
	      << "Usage: " << argv[0] << " [options]" << std::endl
	      << std::setfill('-') << setw(76) << '-' 
	      << std::endl << std::setfill(' ')
	      << desc << std::endl;
    return 1;
  }

  if (vm.count("version")) {
    std::cout << argv[0] << " " << PACKAGE_VERSION 
	      << " version " << VERSION << std::endl;
    return 1;
  }


  if (fpos == spos) {
    std::cout << "You must specify fpos>ipos" << std::endl;
    return 3;
  }

  std::vector< std::vector<double> > c;

  if (type==2) {
    if (comps.size()!=6) {
      std::cout << "You must specify center, width and weight for 2 components or six parameters in total" << std::endl;
      return 4;
    }

    std::vector<double> v(5);

    v[0] = comps[0];
    v[1] = comps[1]*comps[1];
    v[2] = comps[2]/(comps[2]+comps[5]);
    v[3] = 0.5*erf((1.0 - v[0])/sqrt(2.0*v[1]));
    v[4] = 0.5*erf((0.0 - v[0])/sqrt(2.0*v[1]));
    c.push_back(v);

    v[0] = comps[3];
    v[1] = comps[4]*comps[4];
    v[2] = comps[5]/(comps[2]+comps[5]);
    v[3] = 0.5*erf((1.0 - v[0])/sqrt(2.0*v[1]));
    v[4] = 0.5*erf((0.0 - v[0])/sqrt(2.0*v[1]));
    c.push_back(v);
  }

  typedef boost::shared_ptr< std::vector<double> > vPtr;
  std::list<vPtr> data;

  // Try to open coefficient file
  //
  std::ifstream in(infile.c_str());
  const size_t lbufsz = 4096;
  char line[lbufsz];

  unsigned dim   = 0;
  unsigned count = 0;

  while (in.good()) {

    in.getline(line, lbufsz);

    if (std::string(line).find("Iter") == std::string::npos &&
	std::string(line).find("#")    == std::string::npos ) {

      if (count++ >= skip) {

	std::istringstream sin(line);
	unsigned dumI;
	double   dumF;
	
	sin >> dumI;		// Level
	sin >> dumI;		// Iter
	sin >> dumF;		// Prob
	sin >> dumF;		// Like
	sin >> dumF;		// Prior

	unsigned cnt = 0;
	vPtr p(new std::vector<double>());
	while (sin.good()) {
	  sin >> dumF;
	  if (cnt>=spos && cnt<fpos)
	    p->push_back(dumF);
	  cnt++;
	}
	if (dim==0  ) dim = cnt;	// Set the dimension, first time
	if (cnt==dim) data.push_back(p);
      }
    }
  }
      
  std::cout << "Data size=" << data.size() << " out of a total " << count 
	    << ", Dim=" << dim 
	    << ", Order=" << data.back()->size() - 1
	    << std::endl;

  if (dim != data.back()->size()) {
    std::cout << "First/last dimension mismatch" 
	      << ", first=" << dim
	      << ", last="  << data.back()->size()
	      << std::endl;
    return 1;
  }

  if (data.size()<2) {
    std::cout << "Need more states, found " << data.size()
	      << " . . . quitting" << std::endl;
    return 2;
  }

  //
  // Set up tabulation at each x grid point
  //
  typedef std::set<double> vSet;
  std::vector<vSet> quant(number);

  //
  // Best fit
  //    
  std::map<double, vPtr> good;

  BernsteinPoly bern(dim-1);

  double dx = 1.0/number;
  for (std::list<vPtr>::iterator i=data.begin(); i!=data.end(); i++)  {
    
    double norm = 0.0;
    for (unsigned j=0; j<number; j++) {
      double x = (0.5 + j)*dx;
      double y = bern(**i, x);
				// Accumulate for quantile values at
				// this grid point
      quant[j].insert(y);
				// Compute the MISE
      switch(type) {
      case 2:
	{
	  double f = 0.0;
	  for (int i=0; i<2; i++) {
	    double z = x - c[i][0];
	    f += c[i][2]*exp(-0.5*z*z/c[i][1])/sqrt(2.0*M_PI*c[i][1]) /
	      (c[i][3] - c[i][4]);
	  }
	  norm += (f - y)*(f - y);
	}
	break;
      case 1:
	{
	  double f = pow(x, beta)*(1.0 + beta);
	  norm += (f - y)*(f - y);
	}
	break;
      case 0:
      default:
	{
	  double f = 30.0*x*x*(1.0 - x)*(1.0 - x);
	  norm += (f - y)*(f - y);
	}
      }
    }
    good[sqrt(norm/number)] = *i;	// Store the poly
  }
  
  std::ofstream out(outfile.c_str());

  if (out) {
    
    const unsigned nfield = 7;
    std::ostringstream header;
    header << std::setfill('-') << "# ";
    for (unsigned n=0; n<nfield; n++)
      header << '+' << std::setw(13) << '-';
    header << std::endl;

    std::ostringstream labels;
    labels << "# ";
    labels << "|" << std::setw(13) << " x "
	   << "|" << std::setw(13) << " low "
	   << "|" << std::setw(13) << " median "
	   << "|" << std::setw(13) << " high "
	   << "|" << std::setw(13) << " best "
	   << "|" << std::setw(13) << " middle "
	   << "|" << std::setw(13) << " exact "
	   << std::endl;

    std::ostringstream numbs;
    numbs << "# ";
    for (unsigned n=0; n<nfield; n++) {
      std::ostringstream num;
      num   << " [" << n+1 << "] ";
      numbs << '|' << std::setw(13) << num.str();
    }
    numbs << std::endl;


    out << header.str() << labels.str() << numbs.str() << header.str();

    for (unsigned j=0; j<number; j++) {
				// The x mesh point
      double x      = (0.5 + j)*dx;
				// Compute the low quantile
      vSet::iterator it = quant[j].begin();
      for (size_t n=0; n<floor(quant[j].size()*0.01*qmin); n++, it++) {}
      double low    = *it;
				// Compute the median quantile
      it = quant[j].begin();
      for (size_t n=0; n<floor(quant[j].size()*0.5); n++, it++) {}
      double median = *it;
				// Compute the high quantile
      it = quant[j].begin();
      for (size_t n=0; n<floor(quant[j].size()*0.01*qmax); n++, it++) {}
      double high   = *it;
				// Compute the best fit values
      std::map<double, vPtr>::iterator jt = good.begin();
      double best   = bern(*(jt->second), x);
      for (size_t n=0; n<floor(good.size()*0.5); n++, jt++) {}
      double middle = bern(*(jt->second), x);

				// Compute the exact value
      double exact = 0.0;
      switch (type) {
      case 2:
	{
	  for (int i=0; i<2; i++) {
	    double z = x - c[i][0];
	    exact += c[i][2]*exp(-0.5*z*z/c[i][1])/sqrt(2.0*M_PI*c[i][1]) /
	      (c[i][3] - c[i][4]);
	  }
	}
	break;
      case 1:
	exact = pow(x, beta)*(1.0 + beta);
	break;
      case 0:
      default:
	exact = 30.0*x*x*(1.0 - x)*(1.0 - x);
	break;
      }

				// Print the values to the file
      out << "  "
	  << std::setw(14) << x
	  << std::setw(14) << low
	  << std::setw(14) << median
	  << std::setw(14) << high
	  << std::setw(14) << best
	  << std::setw(14) << middle
	  << std::setw(14) << exact
	  << std::endl;
    }
  } else {
    std::cout << "Could not open <" << outfile << ">" << std::endl;
  }

  double maxNorm = good.rbegin()->first;
  for (size_t j=1; j<10; j++) {
    std::map<double, vPtr>::iterator jt = good.lower_bound(0.1*j*maxNorm);

    std::cout << std::setw(8) << 0.1*j << std::setw(16) << jt->first
	      << std::endl;
  }
}
