#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <deque>
#include <cmath>
#include <cfloat>

using namespace std;

double GR(deque< vector< vector<double> > >& data,
	  vector< unsigned char >& mask,
	  unsigned endpt, unsigned psize, 
	  bool verbose, bool debug)
{

  if (verbose) {
				// Separator
    cout << setfill('-') << setw(72) << "-" << endl
	 << "End point=" << endpt << "  Count=" << psize << endl
	 << setw(72) << "-" << endl << setfill(' ');
  }
  


  //***************************************************
  //  Begin computation
  //***************************************************
  
				// Number of entries (steps)
  unsigned Nsteps = psize;
				// Number of chains
  unsigned Mchain = data[0].size();
                                // Number of values per chain 
  unsigned Nparam = data[0][0].size();


  unsigned begpt;
  if (endpt+1<psize) begpt = 0;
  else begpt = endpt+1-psize;
				// 
				// Gelman & Rubin quantities
				// 
  vector<double> B(Nparam, 0.0), W(Nparam, 0.0), meanS(Nparam, 0.0);
  vector<double> Sig2Hat(Nparam, 0.0), rHat(Nparam, 0.0);

				// 
				// Count non-aberrant chains
				// 
  unsigned Mgood = 0;
  for (unsigned n=0; n<mask.size(); n++) 
    if (mask[n]==0) Mgood++;

				// 
				// Set up vectors for in-chain analysis
				// 
  vector< vector<double> > meanCh(Mchain), varCh(Mchain);
  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    meanCh[i]  = vector<double>(Nparam, 0.0);
    varCh[i]   = vector<double>(Nparam, 0.0);
  }
				// 
				// Compute means and variance of each chain
				//
  for (unsigned n=begpt; n<=endpt; n++) {
    for (unsigned i=0; i<Mchain; i++) {
      if (mask[i]) continue;
      for (unsigned j=0; j<Nparam; j++) {
	meanCh[i][j]  += data[n][i][j];
	varCh [i][j]  += data[n][i][j]*data[n][i][j];
      }
    }
  }

  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    for (unsigned j=0; j<Nparam; j++) {
      meanCh[i][j] /= Nsteps;
      varCh[i][j] = 
        (varCh[i][j] - meanCh[i][j]*meanCh[i][j]*Nsteps)/(Nsteps-1);
      meanS[j] += meanCh[i][j]/Mgood;
    }
  }
                                // 
                                // Compute the Gelman & Rubin quantities
                                // 
  
  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    for (unsigned j=0; j<Nparam; j++) {
      B[j] += 
        (meanCh[i][j] - meanS[j])*(meanCh[i][j] - meanS[j]) *
        Nsteps/(Mgood-1);
      W[j] += varCh[i][j]/Mgood;
    }
  }
    
  
  for (unsigned j=0; j<Nparam; j++) {
    if (W[j]>0.0) {
      Sig2Hat[j] = (W[j]*(Nsteps-1) + B[j])/Nsteps;
      // Gelman & Brooks 1998 "correction"
      rHat[j] = sqrt(Sig2Hat[j]/W[j]*(Mgood+1)/Mgood - 
		     static_cast<double>(Nsteps-1)/(Mgood*Nsteps) );
    }
  }
  
  
  if (verbose && debug) {
    cout << endl 
	 << "Per chain data, N=" << Nsteps
	 << " M=" << Mgood << "/" << Mchain
	 << ": " << endl << endl
	 << setw(4) << left << "#";
    for (unsigned j=0; j<Nparam; j++) {
      ostringstream out1, out2;
      out1 << "Mean [" << j << "]";
      out2 << "Var [" << j << "]";
      cout << setw(15) << left << out1.str()
	   << setw(15) << left << out2.str();
    }
    cout << endl
	 << setw(4) << left << "-";
    for (unsigned j=0; j<Nparam; j++) {
      ostringstream out1, out2;
      out1 << "Mean [" << j << "]";
      out2 << "Var [" << j << "]";
      cout << setw(15) << left << "---------"
	   << setw(15) << left << "---------";
    }
    cout << endl;

    for (unsigned i=0; i<Mchain; i++) {
      cout << setw(4) << i;
      if (mask[i])
	for (unsigned j=0; j<Nparam; j++) {
	  cout << setw(15) << left << "*******"
	       << setw(15) << left << "*******";
	}
      else
	for (unsigned j=0; j<Nparam; j++) {
	  cout << setw(15) << left << meanCh[i][j]
	       << setw(15) << left << varCh[i][j];
	}
      cout << endl;
    }
    cout << endl << setw(4) << "**";
    for (unsigned j=0; j<Nparam; j++) {
      cout << setw(15) << left << meanS[j]
	   << setw(15) << left << B[j]/Nsteps;
    }
    cout << endl;
  }
      

  if (verbose) cout << endl 
		    << "Convergence summary, N=" << Nsteps
		    << " M=" << Mgood << "/" << Mchain
		    << ": " << endl << endl
		    << setw(4) << left << "#"
		    << setw(15) << left << "Mean"
		    << setw(15) << left << "Between"
		    << setw(15) << left << "Within"
		    << setw(15) << left << "Total"
		    << setw(15) << left << "Rhat"
		    << endl
		    << setw(4) << left << "-"
		    << setw(15) << left << "----"
		    << setw(15) << left << "-------"
		    << setw(15) << left << "------"
		    << setw(15) << left << "-----"
		    << setw(15) << left << "----"
		    << endl;
  
  double minParam = DBL_MAX, maxParam = 0.0;
  for (unsigned j=0; j<Nparam; j++) {

    if (verbose) cout << setw(4)  << j
		      << setw(15) << meanS[j]
		      << setw(15) << B[j]/Nsteps
		      << setw(15) << W[j]
		      << setw(15) << Sig2Hat[j]
		      << setw(15) << rHat[j]
		      << endl;

    minParam = min<double>(rHat[j], minParam);
    maxParam = max<double>(rHat[j], maxParam);
  }

  if (verbose) {
    cout << endl
	 << "Min, Max = " << minParam << ", " << maxParam << endl;
  }

  return maxParam;
}

double GR(deque< vector< vector<double> > >& data,
	  vector< unsigned char >& mask,
	  unsigned endpt, unsigned psize, 
	  int T, int N1, int N2, bool verbose, bool debug)
{

  if (verbose) {
				// Separator
    cout << setfill('-') << setw(72) << "-" << endl
	 << "End point=" << endpt << "  Count=" << psize << endl
	 << setw(72) << "-" << endl << setfill(' ');
  }
  


  //***************************************************
  //  Begin computation
  //***************************************************
  
				// Number of chains
  unsigned Mchain = data[0].size();

				// Number of values per chain 
  vector<unsigned> Nparam(2);
  Nparam[0] = T + N1;
  Nparam[1] = T + N2;
				// Map to parameter vector for each component
  vector< vector<unsigned> > MAP(2);
  for (unsigned m=0; m<2; m++) {
    for (int i=0; i<T; i++) MAP[m].push_back(1 + i);
  }
  for (int i=0; i<N1; i++) MAP[0].push_back(1 + T + i);
  for (int i=0; i<N2; i++) MAP[1].push_back(1 + T + N1 + i);

  unsigned begpt;
  if (endpt+1<psize) begpt = 0;
  else begpt = endpt+1-psize;
				// 
				// Gelman & Rubin quantities
				// 
  // For each component
  vector< vector<double> > B(2), W(2), meanS(2);
  vector< vector<double> > Sig2Hat(2), rHat(2);

  for (unsigned m=0; m<2; m++) {
    vector<double> tmp(Nparam[m], 0.0);
    B[m]       = tmp;
    W[m]       = tmp;
    meanS[m]   = tmp;
    Sig2Hat[m] = tmp;
    rHat[m]    = tmp;

  }
                                // 
                                // Set up vectors for in-chain analysis
                                // 
  vector< vector< vector<double> > > meanCh(2), varCh(2);
  vector< vector<unsigned> > Nmodel(2);

				// For indicator variable
  vector<double> meanCh_ind(Mchain, 0), varCh_ind(Mchain, 0);
  unsigned Nsteps = endpt - begpt + 1;

  for (unsigned m=0; m<2; m++) {
    meanCh[m] = vector< vector<double> >(Mchain);
    varCh [m] = vector< vector<double> >(Mchain);
    Nmodel[m] = vector<unsigned>(Mchain, 0);
    for (unsigned i=0; i<Mchain; i++) {
      if (mask[i]) continue;
      meanCh[m][i] = vector<double>(Nparam[m], 0.0);
      varCh [m][i] = vector<double>(Nparam[m], 0.0);
    }
  } 
				// 
				// Compute means and variance of each chain
				//
  for (unsigned n=begpt; n<=endpt; n++) {
    for (unsigned i=0; i<Mchain; i++) {
      if (mask[i]) continue;
      unsigned m = static_cast<unsigned>(floor(data[n][i][0]+0.01));
      meanCh_ind[i] += m;
      varCh_ind[i]  += m*m;
      Nmodel[m][i]++;
      for (unsigned j=0; j<Nparam[m]; j++) {
        meanCh[m][i][j]  += data[n][i][MAP[m][j]];
        varCh [m][i][j]  += data[n][i][MAP[m][j]]*data[n][i][MAP[m][j]];
      }
    }
  }

				// Count good chains per model
  vector<unsigned> Ngood(2, 0);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<Mchain; i++)
      if (Nmodel[m][i]) Ngood[m]++;
  }

				// For indicator variable
  unsigned Mgood_ind = 0;
  for (unsigned i=0; i<Mchain; i++) { if (!mask[i]) Mgood_ind++; }
  double meanS_ind = 0.0;

  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    unsigned Ntot = 0;
    for (unsigned m=0; m<2; m++) {
      Ntot += Nmodel[m][i];
      if (Nmodel[m][i]) {
	for (unsigned j=0; j<Nparam[m]; j++) {
	  meanCh[m][i][j] /= Nmodel[m][i];
	  if (Nmodel[m][i]>1)
	    varCh[m][i][j] = 
	      (varCh[m][i][j] - meanCh[m][i][j]*meanCh[m][i][j]*Nmodel[m][i])
	      /(Nmodel[m][i]-1);
	  else
	    varCh[m][i][j] = 0.0;

	  meanS[m][j] += meanCh[m][i][j]/Ngood[m];
	}
      }
    }

    if (Ntot) meanCh_ind[i] /= Ntot;
    if (Ntot>1)
      varCh_ind[i] = (varCh_ind[i] - meanCh_ind[i]*meanCh_ind[i]*Ntot)/(Ntot-1);
    else
      varCh_ind[i] = 0.0;
    
    meanS_ind += meanCh_ind[i]/Mgood_ind;
  }

				// Compute the Gelman & Rubin quantities
				// 
  vector<unsigned> Wcnt(2, 0), Wvar(2, 0);
  
  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	if (Ngood[m]>1) {
	  double del = meanCh[m][i][j] - meanS[m][j];
	  B[m][j] += del*del * Nmodel[m][i]/(Ngood[m]-1);
	}
	if (Ngood[m]) W[m][j] += varCh[m][i][j]/Ngood[m];
	Wcnt[m] += Nmodel[m][i];
	Wvar[m] += Nmodel[m][i] * Nmodel[m][i];
      }
    }
  }
  
  for (unsigned m=0; m<2; m++) 
    if (Ngood[m]) {
      Wcnt[m] /= Ngood[m];
      if (Ngood[m]>1) {
	Wvar[m] = (Wvar[m] - Wcnt[m]*Wcnt[m]*Ngood[m])/(Ngood[m]-1);
      } else {
	Wvar[m] = 0.0;
      }
    }

  for (unsigned m=0; m<2; m++) {
    for (unsigned j=0; j<Nparam[m]; j++) {
      if (Wcnt[m]>0) {
	Sig2Hat[m][j] = (W[m][j]*(Wcnt[m]-1) + B[m][j])/Wcnt[m];
	// Gelman & Brooks 1998 "correction"
	rHat[m][j] = sqrt( Sig2Hat[m][j]/W[m][j]*(Ngood[m]+1)/Ngood[m] -
			   static_cast<double>(Wcnt[m]-1)/(Ngood[m]*Wcnt[m]) );
      }
    }
  }
  
  
  double B_ind = 0.0, W_ind = 0.0;
  for (unsigned i=0; i<Mchain; i++) {
    B_ind += 
      (meanCh_ind[i] - meanS_ind)*(meanCh_ind[i] - meanS_ind)*Nsteps /
      (Mgood_ind-1);
    W_ind += varCh_ind[i]/Mgood_ind;
  }

  double Sig2Hat_ind = (W_ind*(Nsteps - 1) + B_ind)/Nsteps;

  // Gelman & Brooks 1998 "correction"
  double rHat_ind = 1.0;
  if (W_ind>1.0e-12) 
    rHat_ind = sqrt(Sig2Hat_ind/W_ind*(Mgood_ind+1)/Mgood_ind -
		    static_cast<double>(Nsteps-1)/(Mgood_ind*Nsteps));
  
  vector<unsigned> Tsteps(2, 0);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<Mchain; i++) Tsteps[m] += Nmodel[m][i];
  }

  if (verbose && debug) {

    cout << endl 
	 << "Per chain data, N=[" << Tsteps[0] << ", " << Tsteps[1]
	 << "] M=[" << Ngood[0] << ", " << Ngood[1] << "]/" << Mchain
	 << ": " << endl << endl
	 << setw(4) << left << "#";

    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	ostringstream out1, out2;
	out1 << "Mean [" << m << ", " << j << "]";
	out2 << "Var [" << m << ", " << j << "]";
	cout << setw(15) << left << out1.str()
	     << setw(15) << left << out2.str();
      }
    }
    cout << endl
	 << setw(4) << left << "-";
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	ostringstream out1, out2;
	out1 << "Mean [" << m << ", " << j << "]";
	out2 << "Var [" << m << ", " << j << "]";
	cout << setw(15) << left << "---------"
	     << setw(15) << left << "---------";
      }
      cout << endl;
    }
    for (unsigned i=0; i<Mchain; i++) {
      cout << setw(4) << i;
      for (unsigned m=0; m<2; m++) {
	if (mask[i])
	  for (unsigned j=0; j<Nparam[m]; j++) {
	    cout << setw(15) << left << "*******"
		 << setw(15) << left << "*******";
	  }
	else
	  for (unsigned j=0; j<Nparam[m]; j++) {
	    cout << setw(15) << left << meanCh[m][i][j]
		 << setw(15) << left << varCh[m][i][j];
	  }
      }
      cout << endl;
    }
    cout << endl << setw(4) << "**";
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	cout << setw(15) << left << meanS[m][j]
	     << setw(15) << left << B[m][j]/Wcnt[m];
      }
    }
    cout << endl;
  }
      

  if (verbose) 
    cout << endl 
	 << "Convergence summary" << endl
	 << "-------------------" << endl
	 << " N=[" << Tsteps[0] << ", " << Tsteps[1] << "]"
	 << " M=[" << Ngood[0]  << ", " << Ngood[1] << "]/" << Mchain << endl
	 << " Count=[" << Wcnt[0]  << ", " << Wcnt[1]  << "]"
	 << " Sigma=[" << floor(sqrt(Wvar[0])+0.5)  
	 << ", " << floor(sqrt(Wvar[1])+0.5)  << "]"
	 << endl << endl
	 << setw(4)  << left << "Mod"
	 << setw(4)  << left << "#"
	 << setw(15) << left << "Mean"
	 << setw(15) << left << "Between"
	 << setw(15) << left << "Within"
	 << setw(15) << left << "Total"
	 << setw(15) << left << "Rhat"
	 << endl
	 << setw(4)  << left << "---"
	 << setw(4)  << left << "---"
	 << setw(15) << left << "----"
	 << setw(15) << left << "-------"
	 << setw(15) << left << "------"
	 << setw(15) << left << "-----"
	 << setw(15) << left << "----"
	 << endl;
  
  double minParam = DBL_MAX, maxParam = 0.0;
  for (unsigned m=0; m<2; m++) {
    for (unsigned j=0; j<Nparam[m]; j++) {

      if (verbose) cout << setw(4)  << m
			<< setw(4)  << j
			<< setw(15) << meanS[m][j]
			<< setw(15) << B[m][j]/Wcnt[m]
			<< setw(15) << W[m][j]
			<< setw(15) << Sig2Hat[m][j]
			<< setw(15) << rHat[m][j]
			<< endl;
      
      minParam = min<double>(rHat[m][j], minParam);
      maxParam = max<double>(rHat[m][j], maxParam);
    }

  }

  if (verbose) cout << setw(8)  << "Ind *"
		    << setw(15) << meanS_ind
		    << setw(15) << B_ind/Nsteps
		    << setw(15) << W_ind
		    << setw(15) << Sig2Hat_ind
		    << setw(15) << rHat_ind
		    << endl;
  
  minParam = min<double>(rHat_ind, minParam);
  maxParam = max<double>(rHat_ind, maxParam);
  

  if (verbose) {
    cout << endl
	 << "Min, Max = " << minParam << ", " << maxParam << endl;
  }

  return maxParam;
}
