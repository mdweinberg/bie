
#include <Regression.H>
#include <AutoCorr.H>

/**
   Calculate the autocorrelation function
 */
void AutoCorr::ACF( std::deque<element>const &v,
		    std::vector<acorrInfo> &info )
{
				// Number of records
  size_t N   = v.size();
				// Size of each point
  size_t dim = v[0].point.size();
				// Make new info list
  info.clear();
  for (size_t i=0; i<dim; i++) info.push_back(acorrInfo(N));

  // The deltaM array will contain the deviation from the mean.
  // for each point
  std::vector< std::vector<double> > deltaM(N);
  for (size_t i=0; i<N; i++)  deltaM[i] = std::vector<double>(dim);
  
  // Calculate the mean and compute deltaM
  std::vector<double> mean(dim, 0.0);
  for (size_t i=0; i<N; i++) {
    for (size_t j=0; j<dim; j++) {
      mean[j]     += v[i].point[j];
      deltaM[i][j] = v[i].point[j];
    }
  }

  for (size_t j=0; j<dim; j++) mean[j] /= static_cast<double>(N);

  // Compute the values for the deltaM array.  Also calculate the
  // denominator for the autocorrelation function.
  std::vector<double> denom(dim, 0.0);
  for (size_t i=0; i<N; i++) {
    for (size_t j=0; j<dim; j++) {
      deltaM[i][j] -= mean[j];
      denom[j]     += deltaM[i][j] * deltaM[i][j];
    }
  }

  for (size_t j=0; j<dim; j++) denom[j] /= static_cast<double>(N);
  

  // Calculate a std::vector of values which will make up
  // the autocorrelation function.
  for (size_t j=0; j<dim; j++) {
    // Compute one dimension at a time
    for (size_t l=0; l<lags_.size(); l++) {
      double corr = 0.0;
      if (N>lags_[l]) {
	size_t n = N - lags_[l];
	double sum = 0.0;
	for (size_t i=0; i<n; i++)
	  sum += deltaM[i][j] * deltaM[i+lags_[l]][j];
	corr = sum / (denom[j] * n);
      }
      info[j].points().push_back(corr);
    }
  }
  
  // calculate the log/log slope of the autocorrelation
  Slope( info );

  // calculate the autocorrelation length
  Length( info );

} // ACF

/**
   Calculate the slope the autocorrelation curve.  
*/
void AutoCorr::Slope( std::vector<acorrInfo> &info )
{
  const size_t dim = info.size();
  for (size_t j=0; j<dim; j++) {
    const size_t len = info[j].points().size();
    if (len > 1) {
      Regression::lineInfo Info;
      Regression lfit;
      std::vector<Regression::tuple> Pnts;
      for (size_t i = 0; i < len; i++) {
	double x = lags_[i]+1;
	double y = info[j].points()[i];
				// Break at zero crossing
	if (y>0.0)
	  Pnts.push_back( Regression::tuple(log(x),log(y)) );
	else
	  break;
      }
      lfit.compute    ( Pnts, Info      );
      info[j].slope   ( Info.slope()    );
      info[j].slopeErr( Info.slopeErr() );
    }
  }
} // Slope


/**
   Calculate the autocorrelation length
*/
void AutoCorr::Length( std::vector<acorrInfo> &info )
{
  const size_t dim = info.size();
  for (size_t j=0; j<dim; j++) {
    const size_t len = info[j].points().size();
    if (len > 0) {
      std::vector<Regression::tuple> Pnts;
      double y1 = info[j].points()[0], y0;
      double target = y1/exp(1.0), l;
      for (size_t i = 1; i < len; i++) {
	l = lags_[i];
	y0 = y1;
	y1 = info[j].points()[i];
	if (y1<target) {
	  l = (log(lags_[i-1]+1)*(y0 - target) - 
	       log(lags_[i  ]+1)*(y1 - target) ) / ( y0 - y1 );
	  l = std::max<double>(exp(l)-1, 0);
	  break;
	}
      }
      info[j].length( l );
    }
  }
} // Length


