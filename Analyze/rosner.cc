#include "OutlierMask.H"
#include <set>
#include <map>

struct diagelem 
{
  int l;
  double R;
  double L;
};


/*
  Rosner's 1983 generalized ESD many-outlier procedure.
  [Technometrics, 25, 165]

  As in the Grubbs' test (Grubbs 1969, Stefansky 1972), an extreme
  studentized deviate (ESD) is used to detect outliers in a univariate
  data set. It is based on the assumption of normality.  This should
  be the case for converged MCMC calc.
*/

bool Rosner::compute(std::vector< std::vector<double> >& prb,
		     std::vector< std::vector<double> >& z,
		     double alpha, int maxoutlier, 
		     double poffset, bool verbose)
{
  unsigned n = z.size();
  unsigned m = z.front().size();
				// The returned outlier mask
  mask = std::vector<unsigned char>(n, 0);
  if (alpha <= 0.0) return true;

				// Compute the maximum posterior
				// probability for computing the final
				// rejection offset criterion

  double maxLP = -1.0e20;
  for (unsigned i=0; i<n; i++) maxLP = std::max<double>(maxLP, prb[i][0]);

  
				// Compute the Rosner statistic for 
				// the possible outliers count
  int k = n/2;		        // <-- can have as many as half the chains
				//     be outliers before we give up
  if (k<2) return false;
  
  std::vector<double> lambda, R;
  for (int j=0; j<k; j++) {
				// Compute the critical values from
				// Rosner 2-sided test to find
				// outliers equation (2.5)
    double pcrit = 1.0 - (alpha*0.5/(n-j));
    double t = inv_student_t_1sided(pcrit, n-j-2);
    double gmax = t*(n-j-1)/sqrt(((t*t + n - j - 2))*(n -j));
    lambda.push_back(gmax);
  }

				// Will contain the "bad" masks for each
				// variable in the parameter vector
  std::vector< std::vector<unsigned char> > table;

				// Will keep track of the chains
				// marked as "bad"
  std::set<int> outliers;

				// For verbose output . . .
  diagelem de;
  std::map<int, diagelem> diag;
  
  //
  // Do each variable separately
  //
  for (unsigned q=0; q<m; q++) {
				// p will be the sorted list of
				// (value, index) pairs
    std::vector< std::pair<double, int> > p;
    for (unsigned i=0; i<n; i++)
      p.push_back(std::pair<double, int>(z[i][q], i));
  
    std::vector<double> R;
    std::vector<int> extrema;

    double mean, var, val;
    unsigned sz;

    for (int j=0; j<k; j++) {
      mean = var = 0.0;		// Compute mean and variance 
      sz = p.size();
      for (unsigned i=0; i<sz; i++) {
	val = z[p[i].second][q];
	mean += val/sz;
	var  += val*val/(sz-1);
      }
      var -= mean*mean*sz/(sz-1);
      
      if (var<1.0e-18) var = 1.0e-18;

      for (unsigned i=0; i<sz; i++) p[i].first = fabs(z[p[i].second][q]-mean);
      sort(p.begin(), p.end());

				// The supremum value
      R.push_back( p.back().first / sqrt(var) );
      extrema.push_back( p.back().second );
      p.pop_back();
    }
				// Apply the test
    std::vector<unsigned char> mask(n, 0);
    for (int j=static_cast<int>(R.size())-1; j>=0; j--) {
      if (R[j] > lambda[j]) {
	if (verbose) {
	  de.l = j+1;
	  de.R = R[j];
	  de.L = lambda[j];
	  diag[q] = de;
	}
	for (int l=0; l<=j; l++) {
	  mask[extrema[l]] = 1;
	  if (outliers.find(extrema[l]) != outliers.end())
	    outliers.insert(extrema[l]);
	}
	break;
      }
    }
    table.push_back(mask);
  }

  // Make the mask from the joint table

  for (unsigned l=0; l<table.size(); l++) {
    for (unsigned m=0; m<n; m++) {
      if (table[l][m]) mask[m] = 1;
    }
  }

  // Compute the number of outliers excluding those within the
  // posterior probability tolerance

  int cnt = 0;
  for (unsigned m=0; m<n; m++) {
    if (mask[m]) {
      if (prb[m][0]+poffset>maxLP) mask[m] = 0;
      else cnt++;
    }
  }


  if (verbose && cnt>0) {
    std::ostringstream ostr;
    ostr << "Found " << cnt << " outlier chain";
    if (cnt>1) ostr << "s";
    std::cout << std::setw(7) << " " << std::setw(50) << std::setfill('_') << '_' << std::endl 
	      << std::setfill(' ') << std::setw(7) << " " << ostr.str() << std::endl << std::endl
	      << std::left << std::setw(7) << " " << std::setw(5) << "j" << std::setw(5) << "#"
	      << std::setw(16) << "R-value" << std::setw(16) << "C-value" << std::endl
	      << std::left << std::setw(7) << " " << std::setw(5) << "---" << std::setw(5) << "---"
	      << std::setw(16) << "-------" << std::setw(16) << "-------" << std::endl;
    for (auto d : diag)
      std::cout << std::setw(7) << " " << std::setw(5) << d.first
		<< std::setw(5)  << d.second.l
		<< std::setw(16) << d.second.R
		<< std::setw(16) << d.second.L
		<< std::endl;
    std::cout << std::endl;
    std::cout << std::setw(7) << " " << std::setw(50) << std::setfill('_') << '_' << std::endl
	      << std::setfill(' ') << std::setw(7) << " " << std::left 
	      << std::setw(10) << "j" << "is the index in the vector" << std::endl;
    std::cout << std::setw(7) << " " << std::left << std::setw(10) << "#" 
	      << "is the number of outlier chains" << std::endl;
    std::cout << std::setw(7) << " " << std::left << std::setw(10) << "R-value" 
	      << "is the ESD value" << std::endl;
    std::cout << std::setw(7) << " " << std::left << std::setw(10) << "C-value" 
	      << "is the critical value for alpha=" << alpha << std::endl << std::endl;
  }

  if (cnt > maxoutlier) {
    // Warning for the naive . . .
    std::cout << "****** Too many aberrant chains!  ******" << std::endl
	      << "****** Will continue sans masking ******" << std::endl
	      << std::setw(72) << std::setfill('-') << "-"  << std::endl
	      << std::setfill(' ');

    return false;
  }
  
				// Set the new mask
  if (verbose) {
    if (cnt==0) std::cout << "No outliers" << std::endl;
    else {
      std::cout << "Outlier mask: ";
      for (unsigned i=0; i<n; i++) 
	if (mask[i]==0) std::cout << std::setw(2) << 0;
	else            std::cout << std::setw(2) << 1;
      std::cout << std::endl;
    }
    std::cout << std::setw(72) << std::setfill('-') << "-" << std::endl
	      << std::setfill(' ');
  }

  return true;
}

bool Rosner::compute(std::vector< std::vector<double> >& prb,
		     std::vector< std::vector<double> >& z,
		     double alpha, int maxoutlier, 
		     int T, int N1, int N2,
		     double poffset, bool verbose)
{
  unsigned n = z.size();
				// The returned outlier mask
  mask = std::vector<unsigned char>(n, 0);
  if (alpha <= 0.0) return true;

				// Number of values per chain 
  std::vector<unsigned> Nparam(2), Nsteps(2, 0);
  Nparam[0] = T + N1;
  Nparam[1] = T + N2;

				// Make the parameter map for the two
				// models
  std::vector< std::vector<unsigned> > MAP(2);
  for (unsigned m=0; m<2; m++) {
    for (int i=0; i<T; i++) MAP[m].push_back(1 + i);
  }
  for (int i=0; i<N1; i++) MAP[0].push_back(1 + T + i);
  for (int i=0; i<N2; i++) MAP[1].push_back(1 + T + N1 + i);


				// Compute the maximum posterior
				// probability for computing the final
				// rejection offset criterion

  double maxLP = -1.0e20;
  for (unsigned i=0; i<n; i++) maxLP = std::max<double>(maxLP, prb[i][0]);

  
				// Compute the Rosner statistic for 
				// the possible outliers count
  int k = n/2;		        // <-- can have as many as half the chains
				//     be outliers before we give up
  if (k<2) return false;
  
  std::vector<double> lambda, R;
  for (int j=0; j<k; j++) {
				// Compute the critical values from
				// Rosner 2-sided test to find
				// outliers equation (2.5)
    double pcrit = 1.0 - (alpha*0.5/(n-j));
    double t = inv_student_t_1sided(pcrit, n-j-2);
    double gmax = t*(n-j-1)/sqrt(((t*t + n - j - 2))*(n -j));
    lambda.push_back(gmax);
  }

				// Will contain the "bad" masks for each
				// variable in the parameter vector
  std::vector< std::vector<unsigned char> > table;

				// Will keep track of the chains
				// marked as "bad"
  std::set<int> outliers;

				// For verbose output . . .
  diagelem de;
  std::map<int, diagelem> diag;
  std::map<int, diagelem>::iterator it;

  //
  // Do each variable separately for each model
  //
				// Model loop
  for (unsigned m=0; m<2; m++) {
				// Variable loop
    for (unsigned q=0; q<Nparam[m]; q++) {
				// p will be the sorted list of
				// (value, index) pairs
      std::vector< std::pair<double, int> > p;
      for (unsigned i=0; i<n; i++)
	if (fabs(z[i][0] - m) < 1.0e-10)
	  p.push_back(std::pair<double, int>(z[i][MAP[m][q]], i));
      
      if (p.size() == 0) continue;

      std::vector<double> R;
      std::vector<int> extrema;

      double mean, var, val;
      unsigned sz;

      for (int j=0; j<k; j++) {
	if ( (sz = p.size()) < 2) break;
	mean = var = 0.0;		// Compute mean and variance 
	for (unsigned i=0; i<sz; i++) {
	  val = z[p[i].second][MAP[m][q]];
	  mean += val/sz;
	  var  += val*val/(sz-1);
	}
	var -= mean*mean*sz/(sz-1);
	
	if (var<1.0e-18) var = 1.0e-18;

	for (unsigned i=0; i<sz; i++) 
	  p[i].first = fabs(z[p[i].second][MAP[m][q]]-mean);
	sort(p.begin(), p.end());

				// The supremum value
	R.push_back( p.back().first / sqrt(var) );
	extrema.push_back( p.back().second );
	p.pop_back();
      }

				// Apply the test
      std::vector<unsigned char> mask(n, 0);
      for (int j=static_cast<int>(R.size())-1; j>=0; j--) {
	if (R[j] > lambda[j]) {
	  if (verbose) {
	    de.l = j+1;
	    de.R = R[j];
	    de.L = lambda[j];
	    diag[q] = de;
	  }
	  for (int l=0; l<=j; l++) {
	    mask[extrema[l]] = 1;
	    if (outliers.find(extrema[l]) != outliers.end())
	      outliers.insert(extrema[l]);
	  }
	  break;
	}
      }
      table.push_back(mask);
    }
  }

  // Make the mask from the joint table

  for (unsigned l=0; l<table.size(); l++) {
    for (unsigned m=0; m<n; m++) {
      if (table[l][m]) mask[m] = 1;
    }
  }

  // Compute the number of outliers excluding those within the
  // posterior probability tolerance

  int cnt = 0;
  for (unsigned m=0; m<n; m++) {
    if (mask[m]) {
      if (prb[m][0]+poffset>maxLP) mask[m] = 0;
      else cnt++;
    }
  }


  if (verbose && cnt>0) {
    std::ostringstream ostr;
    ostr << "Found " << cnt << " outlier chain";
    if (cnt>1) ostr << "s";
    std::cout << std::setw(7) << " " << std::setw(50) << std::setfill('_') << '_' << std::endl 
	      << std::setfill(' ') << std::setw(7) << " " << ostr.str() << std::endl << std::endl
	      << std::left << std::setw(7) << " " << std::setw(5) << "j" << std::setw(5) << "#"
	      << std::setw(16) << "R-value" << std::setw(16) << "C-value" << std::endl
	      << std::left << std::setw(7) << " " << std::setw(5) << "---" << std::setw(5) << "---"
	      << std::setw(16) << "-------" << std::setw(16) << "-------" << std::endl;
    for (auto d : diag)
      std::cout << std::setw(7) << " " << std::setw(5) << d.first
		<< std::setw(5)  << d.second.l
		<< std::setw(16) << d.second.R
		<< std::setw(16) << d.second.L
		<< std::endl;
    std::cout << std::endl;
    std::cout << std::setw(7) << " " << std::setw(50) << std::setfill('_') << '_' << std::endl
	      << std::setfill(' ') << std::setw(7) << " " << std::left 
	      << std::setw(10) << "j" << "is the index in the vector" << std::endl;
    std::cout << std::setw(7) << " " << std::left << std::setw(10) << "#" 
	      << "is the number of outlier chains" << std::endl;
    std::cout << std::setw(7) << " " << std::left << std::setw(10) << "R-value" 
	      << "is the ESD value" << std::endl;
    std::cout << std::setw(7) << " " << std::left << std::setw(10) << "C-value" 
	      << "is the critical value for alpha=" << alpha << std::endl << std::endl;
  }

  if (cnt > maxoutlier) {
				// Warning for the naive . . .
    std::cout << "****** Too many aberrant chains!  ******" << std::endl
	      << "****** Will continue sans masking ******" << std::endl
	      << std::setw(72) << std::setfill('-') << "-" << std::endl << std::setfill(' ');
    
    return false;
  }
  
				// Set the new mask
  if (verbose) {
    if (cnt==0) std::cout << "No outliers" << std::endl;
    else {
      std::cout << "Outlier mask: ";
      for (unsigned i=0; i<n; i++) 
	if (mask[i]==0) std::cout << std::setw(2) << 0;
	else            std::cout << std::setw(2) << 1;
      std::cout << std::endl;
    }
    std::cout << std::setw(72) << std::setfill('-') << "-" << std::endl
	      << std::setfill(' ');
  }

  return true;
}

