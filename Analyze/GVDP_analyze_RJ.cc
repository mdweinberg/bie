// -*- C++ -*-

/* 
   Reads in a multilevel output file and analyzes convergence
   and provides graphical output

   Implementation of algorithm proposed by:

   	Giakoumatos, Vrontos, Dellaportas and Politis, "An MCMC 
	Convergence Diagnostic using Subsampling" (1999), preprint 
	to appear in to in Journal of Computational and Graphical 
	Statistics. 

   This generalizes Gelman and Rubin (1992).
*/


#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cmath>

#include <unistd.h>
#include <getopt.h>

using namespace std;


string inputfile("multilevel.dat");
string outputtag("gvdp");

int
main(int argc, char** argv)
{
  /***************************************************
    Global variables
   ***************************************************/
				// Use this level
  int level = 0;
				// Number of subintervals
  int nint = 0;
				// Number in each linear correlation test
  int ngrp = 0;
				// Number of common variables
  int T = 0;
				// Number of Model 1 variables
  int N1 = 1;
				// Number of Model 2 variables
  int N2 = 1;
				// Quantile
  double alpha = 0.05;
				// Tolerance for COD
  double rtol = 0.99;
				// Scale coordinates
  bool scale = true;

  /***************************************************
    Parse command line
   ***************************************************/

  int c;
  while (1) {
    c = getopt (argc, argv, "i:o:l:n:g:q:r:T:1:2:A");
    if (c == -1) break;
     
    switch (c)
      {
      case 'l': level    = atoi(optarg); break;
      case 'n': nint     = atoi(optarg); break;
      case 'g': ngrp     = atoi(optarg); break;
      case 'T': T        = atoi(optarg); break;
      case '1': N1       = atoi(optarg); break;
      case '2': N2       = atoi(optarg); break;
      case 'q': alpha    = atof(optarg); break;
      case 'r': rtol     = atof(optarg); break;
      case 'i': inputfile.erase(); inputfile = optarg; break;
      case 'o': outputtag.erase(); outputtag = optarg; break;
      case '?': 
	string msg = "usage: " + string(argv[0]) + " [options]\n";
	msg += "\t-l int\t\tlevel to analyze\n";
	msg += "\t-n int\t\tnumber of partitions\n";
	msg += "\t-g int\t\tnumber per correlation test\n";
	msg += "\t-T int\t\tnumber of common parameters\n";
	msg += "\t-1 int\t\tnumber of Model 1 parameters\n";
	msg += "\t-2 int\t\tnumber of Model 2 parameters\n";
	msg += "\t-q float\t\tquartile\n";
	msg += "\t-r float\t\tlimit for COD\n";
	msg += "\t-i string\t\tinput file\n";
	msg += "\t-o string\t\toutput file tag\n";
	msg += "\t-A \t\tuse absolute range (no scaling)\n";
	cerr << msg; exit(-1);
      }
  }

  /***************************************************
    Read in multilevel data
   **************************************************/

  ifstream in(inputfile.c_str());
  if (!in) {
    cerr << argv[0] << ": error opening <" << inputfile << ">\n";
    exit(-1);
  }

				// Make a vector type (used to define a vector
				// of vectors in STL)
  typedef vector<double> dvec;
				// Make a vector of vectors to hold parameters
  vector<dvec> data;
				// Make a vector to hold posterior probability
  vector<double> prob, like;

  const int linesize=2048;
  char line[linesize];
  int l, m;
  double v;

				// Read in (and discard) header string
  in.getline(line, linesize);

				// Read in the lines at the desired level
  while (in) {
    in.getline(line, linesize);
    if (!in) continue;

    istringstream istr(line);	// check level
    istr >> l;
    if (l!=level) continue;
    istr >> l;			// dump counter

    istr >> v;			// read prob level
    prob.push_back(v);
    istr >> v;			// read likelihood
    like.push_back(v);
    istr >> v;			// dump prior
    istr >> m;			// dump number
    if (m==1) istr >> v;	// dump weight

    vector<double> tmp;		// read parameter vector
    while (istr) {
      istr >> v;
      if (!istr) break;
      tmp.push_back(v);
    }
    data.push_back(tmp);
  }

  cout << "Size of prob:    " << prob.size() << endl;
  cout << "Size of data:    " << data.size() << endl;
  if (data.size() > 0)
    cout << "Size of data[0]: " << data[0].size() << endl;
  else
    exit(0);

  /***************************************************
    Initialize output files
   **************************************************/
  string outfile;

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".maxdev";
  ofstream out1(outfile.c_str());
  if (!out1) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".corr";
  ofstream out2(outfile.c_str());
  if (!out2) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".bounds";
  ofstream out3(outfile.c_str());
  if (!out3) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  /***************************************************
    Begin computation
   **************************************************/

  int N = prob.size();
  int q = data[0].size();

  // Sanity check
  if (q != T + N1 + N2 + 1) {
    cerr << argv[0] << ": dimension mismatch\n";
    exit(-1);
  }

  vector<int> Nparam(2), Nc(2, 0);
  Nparam[0] = T + N1;
  Nparam[1] = T + N2;
  vector< vector<int> > MAP(2);
  for (int m=0; m<2; m++) {
    for (int t=0; t<T; t++) MAP[m].push_back(1+t);
  }
  for (int n=0; n<Nparam[0]; n++) MAP[m].push_back(1+T+n);
  for (int n=0; n<Nparam[1]; n++) MAP[m].push_back(1+T+Nparam[0]+n);

				// Set up for correlation analysis

  vector<double> xx, yy, uu;
  vector<int> nburn(3, -1);


				// Total mean and variance
  vector< vector<double> > tmean(2);
  vector< vector<double> > tvar(2);
  vector< vector<unsigned> > IS(2);
  double Imean = 0.0, Ivar = 0.0;
  
  for (int m=0; m<2; m++) {
    tmean[m] = vector<double>(Nparam[m], 0);
    tvar [m] = vector<double>(Nparam[m], 0);
  }

  for (int i=0; i<N; i++) {
    Imean += data[i][0];
    Ivar  += data[i][0] * data[i][0];
    int m = static_cast<int>(floor(data[i][0]+0.01));
    Nc[m]++;
    IS[m].push_back(i);
    for (int k=0; k<Nparam[m]; k++) {
      tmean[m][k] += data[i][MAP[m][k]];
      tvar [m][k] += data[i][MAP[m][k]] * data[i][MAP[m][k]];
    }
  }

  Imean /= N;
  if (N>1) 
    Ivar = (Ivar - Imean*Imean*N)/(N-1);
  else
    Ivar = 0.0;

  for (int m=0; m<2; m++) {
    for (int k=0; k<Nparam[m]; k++) {
      if (Nc[m]) {
	tmean[m][k] /= Nc[m];
	tvar [m][k] = (tvar[m][k] - tmean[m][k]*tmean[m][k]*Nc[m]);
	if (Nc[m]>1) tvar[m][k] /= Nc[m]-1;
      }
    }
  }

  //
  // Do each subspace followed by the indicator variable
  //
  for (int m=0; m<2; m++) {

    cout << endl 
	 << "Maximum deviations for Model " << m << ": " << endl << endl;


    if (!nint) nint = (int)sqrt( static_cast<float>(Nc[m]));
    if (!ngrp) ngrp = (int)sqrt( static_cast<float>(nint));

				// Step 1: choose subsamples
    for (int j=1; j<=nint; j++) {
      int Nj = j*Nc[m]/nint;
      int bj = (int)(sqrt(static_cast<float>(Nj)));
      int Bj = Nj - bj + 1;

				// Step 2: compute total statistic
      vector<double> mean(Nparam[m], 0.0);
      vector<double> var (Nparam[m], 0.0);
      for (int i=(j-1)*Nc[m]/nint; i<Nj; i++) {
	int I = IS[m][i];
	for (int k=0; k<Nparam[m]; k++) mean[k] += data[I][k];
	for (int k=0; k<Nparam[m]; k++) var[k]  += data[I][k] * data[I][k];
    }
				// Mean
      for (int k=0; k<Nparam[m]; k++) 
	mean[k] /= Nj;
				// Variance
      for (int k=0; k<Nparam[m]; k++) 
	var[k] = (var[k] - mean[k]*mean[k]*Nj)/(Nj-1);


				// Step 3: compute subsample statistic
      vector<dvec> smean(Bj);
      for (int i=0; i<Bj; i++) {
	smean[i] = vector<double>(Nparam[m], 0.0);
	for (int i2=0; i2<bj; i2++)
	  for (int k=0; k<Nparam[m]; k++) smean[i][k] += data[IS[m][i+i2]][k];
	for (int k=0; k<Nparam[m]; k++) smean[i][k] /= bj;
      }
				// Step 4: compute maximum deviation

      vector<double> deviation(Bj, 0.0);
      double testdif;
      for (int i=0; i<Bj; i++) {
	for (int k=0; k<Nparam[m]; k++) {
	  if (scale)
	    testdif = sqrt(static_cast<double>(bj))*fabs(smean[i][k] - mean[k])/sqrt(var[k]);
	  else
	    testdif = sqrt(static_cast<double>(bj))*fabs(smean[i][k] - mean[k]);
	  if (testdif > deviation[i]) deviation[i] = testdif;
	}
      }
      
				// Step 5: sort and find quartile
      sort(deviation.begin(), deviation.end());
      double value = deviation[(int)((1.0 - alpha)*Bj+1.0)];


				// Step 6: output range
      double range = 2.0*value/sqrt(static_cast<double>(Nj));
      cout << setw(15) << Nj
	   << setw(15) << 1.0/sqrt(static_cast<double>(Nj))
	   << setw(15) << range
	   << endl;
      
      out1 << setw(15) << Nj
	   << setw(15) << 1.0/sqrt(static_cast<double>(Nj))
	   << setw(15) << range
	   << endl;
      
				// Cache result vectors for correlation
				// analysis
      xx.push_back(1.0/sqrt(static_cast<double>(Nj)));
      yy.push_back(range);
      uu.push_back(bj);

    }

    cout << endl;
    out1 << endl;

    //**************************************************
    // Correlation analysis
    //**************************************************
    
    cout << endl 
	 << "Coefficient of deviation: " << endl << endl;
    
    double R2;
    int nvecs = xx.size();
    int nk = nvecs - ngrp + 1;
    for (int i=0; i<nk; i++) {
      double sxx=0.0, sxy=0.0, syy=0.0, wght;
      double sx=0.0, sy=0.0, sum=0.0;
      
      for (int j=0; j<ngrp; j++) {
	wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
	sum += wght;
	sx += wght*xx[i+j];
	sy += wght*yy[i+j];
      }
      
      sx /= sum;
      sy /= sum;
      
      for (int j=0; j<ngrp; j++) {
	wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
	sxy += wght*(xx[i+j] - sx)*(yy[i+j] - sy);
	sxx += wght*(xx[i+j] - sx)*(xx[i+j] - sx);
	syy += wght*(yy[i+j] - sy)*(yy[i+j] - sy);
      }
      
      R2 = sxy*sxy/(sxx*syy);
      
				// Print out number and coefficient
				// of determination, R2

      cout << setw(15) << 1.0/(xx[i]*xx[i])
	   << setw(15) << R2
	   << endl;
      
      out2 << setw(15) << 1.0/(xx[i]*xx[i])
	   << setw(15) << R2
	   << endl;
      
      // Set burn in period based on COD
      if (R2>rtol && nburn[m]<0) nburn[m] = (int)( 1.0/(xx[i]*xx[i]) );
      
    }
  }

  
  {
    cout << endl 
	 << "Maximum deviations for indicator: " << endl << endl;


    if (!nint) nint = (int)sqrt( static_cast<float>(N));
    if (!ngrp) ngrp = (int)sqrt( static_cast<float>(nint));

				// Step 1: choose subsamples
    for (int j=1; j<=nint; j++) {
      int Nj = j*N/nint;
      int bj = (int)(sqrt(static_cast<float>(Nj)));
      int Bj = Nj - bj + 1;

				// Step 2: compute total statistic
      double mean = 0.0;
      double var  = 0.0;
      for (int i=(j-1)*N/nint; i<Nj; i++) {
	mean += data[i][0];
	var  += data[i][0] + data[i][0];
      }
				// Mean
      mean /= Nj;
				// Variance
      if (Nj>1)
	var = (var - mean*mean*Nj)/(Nj-1);
      else
	var = 0.0;


				// Step 3: compute subsample statistic
      vector<double> smean(Bj, 0);
      for (int i=0; i<Bj; i++) {
	for (int i2=0; i2<bj; i2++) smean[i] += data[i+i2][0];
	smean[i] /= bj;
      }
				// Step 4: compute maximum deviation

      vector<double> deviation(Bj, 0.0);
      double testdif;
      for (int i=0; i<Bj; i++) {
	if (scale)
	  testdif = sqrt(static_cast<double>(bj))*fabs(smean[i] - mean)/sqrt(var);
	else
	  testdif = sqrt(static_cast<double>(bj))*fabs(smean[i] - mean);
	if (testdif > deviation[i]) deviation[i] = testdif;
      }
      
				// Step 5: sort and find quartile
      sort(deviation.begin(), deviation.end());
      double value = deviation[(int)((1.0 - alpha)*Bj+1.0)];


				// Step 6: output range
      double range = 2.0*value/sqrt(static_cast<double>(Nj));
      cout << setw(15) << Nj
	   << setw(15) << 1.0/sqrt(static_cast<double>(Nj))
	   << setw(15) << range
	   << endl;
      
      out1 << setw(15) << Nj
	   << setw(15) << 1.0/sqrt(static_cast<double>(Nj))
	   << setw(15) << range
	   << endl;
      
				// Cache result vectors for correlation
				// analysis
      xx.push_back(1.0/sqrt(static_cast<double>(Nj)));
      yy.push_back(range);
      uu.push_back(bj);
    }

    cout << endl;
    out1 << endl;

    //**************************************************
    // Correlation analysis
    //**************************************************
    
    cout << endl 
	 << "Coefficient of deviation: " << endl << endl;
    
    double R2;
    int nvecs = xx.size();
    int nk = nvecs - ngrp + 1;
    for (int i=0; i<nk; i++) {
      double sxx=0.0, sxy=0.0, syy=0.0, wght;
      double sx=0.0, sy=0.0, sum=0.0;
      
      for (int j=0; j<ngrp; j++) {
	wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
	sum += wght;
	sx += wght*xx[i+j];
	sy += wght*yy[i+j];
      }
      
      sx /= sum;
      sy /= sum;
      
      for (int j=0; j<ngrp; j++) {
	wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
	sxy += wght*(xx[i+j] - sx)*(yy[i+j] - sy);
	sxx += wght*(xx[i+j] - sx)*(xx[i+j] - sx);
	syy += wght*(yy[i+j] - sy)*(yy[i+j] - sy);
      }
      
      R2 = sxy*sxy/(sxx*syy);
      
				// Print out number and coefficient
				// of determination, R2

      cout << setw(15) << 1.0/(xx[i]*xx[i])
	   << setw(15) << R2
	   << endl;
      
      out2 << setw(15) << 1.0/(xx[i]*xx[i])
	   << setw(15) << R2
	   << endl;
      
      // Set burn in period based on COD
      if (R2>rtol && nburn[2]<0) nburn[2] = (int)( 1.0/(xx[i]*xx[i]) );
      
    }
  }

  //**************************************************
  // Check to see if all spaces converged
  //**************************************************

  bool converge = true;
  for (int n=1; n<3; n++) if (nburn[n] < 0) converge = false;

  //**************************************************
  // Mode and quantiles after burn in
  //**************************************************

  if (converge) {
				// Locate the max burn in
    int worst = 0;
    int wval  = nburn[worst];
    for (int n=1; n<3; n++) {
      if (wval < nburn[n]) {
	worst = n;
	wval  = nburn[worst];
      }
    }

    vector<dvec> qparm(q+1);

    cout << endl 
	 << "Post burn in bounds [N=" << worst << "]: " << endl << endl;

    for (int i=worst; i<N; i++) {
      for (int k=0; k<q; k++) qparm[k].push_back(data[i][k]);
      qparm[q].push_back(prob[i]);
    }

    int beg = (int)( (N-worst+1)*alpha );
    int mid = (int)( (N-worst+1)*0.5 );
    int end = (int)( (N-worst+1)*(1.0 - alpha) );

    for (int k=0; k<=q; k++) {
      sort(qparm[k].begin(), qparm[k].end());
      cout 
	<< setw(15) << qparm[k][beg]
	<< setw(15) << qparm[k][mid]
	<< setw(15) << qparm[k][end]
	<< endl;

      out3
	<< setw(15) << qparm[k][beg]
	<< setw(15) << qparm[k][mid]
	<< setw(15) << qparm[k][end]
	<< endl;
    }
  } else {
    if (nburn[0]<0) cout << "Model 0 subspace did not converge" << endl;
    if (nburn[1]<0) cout << "Model 1 subspace did not converge" << endl;
    if (nburn[2]<0) cout << "Indicator did not converge" << endl;
  }
  
}

/*

  Notes:

  Coefficient of Determination: R Squared 
  ---------------------------------------

  COD = R^2

  where R is the Correlation Coefficient. 

  The coefficient of determination indicates how much of the total
  variation in the dependent variable can be accounted for by the
  regression function.  For example, a COD if .70 implies that 70% of
  the variation in y is accounted for by the Regression Equation. Most
  statisticians consider a COD of .7 or higher for a reasonable model.

*/
