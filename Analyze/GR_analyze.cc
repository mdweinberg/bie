// -*- C++ -*-

/* 
   Reads in a multilevel output file and analyzes convergence
   using Gelman and Rubin (1992) with outlier detection and
   marginal likelihood computation
*/


#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <memory>
#include <cfloat>
#include <string>
#include <vector>
#include <random>
#include <array>
#include <cmath>
#include <deque>
#include <map>

#include <unistd.h>

// Verbose printing for debugging in ML bootstrap routine
//
bool dbg_verbose = false;

// Convenient data types
typedef std::vector< std::vector<double> > chainType;
typedef std::deque<chainType> mcmcType;
typedef std::pair<int, int> Ielem;
typedef std::pair<double, Ielem> Pmap_value;
typedef std::vector<Pmap_value> Pmap;
typedef Pmap::iterator iterType;
typedef std::reverse_iterator<iterType> RiterType;
typedef std::array<double, 3> BSret;

// Outlier mask classes
//
#include "OutlierMask.H"

// Routines
//

double GR(mcmcType& data, std:: vector< unsigned char >& mask,
	  unsigned endpt, unsigned psize, 
	  bool verbose, bool debug);

/// For RJTwo models:

double GR(mcmcType& data, std::vector< unsigned char >& mask,
	  unsigned endpt, unsigned psize, 
	  int T, int N1, int N2,
	  bool verbose, bool debug);

std::vector<double>
MixingFraction(mcmcType& data,
	       std::vector< unsigned char >& mask,
	       unsigned endpt, unsigned psize, bool verbose);


BSret bootstrap(const mcmcType& prob, const mcmcType& data,
		const std::vector<unsigned char>& mask,
		int nconverge, unsigned sampN, double probD)
{
  BSret ret = {0, 0, 0};

  if (sampN<=1) return ret;

  // For debugging
  //
  std::string header(4+10+6*18, '-');

  if (dbg_verbose)
    std::cout << header << std::endl << std::left
	      << std::setw( 4) << "#"	    // 1
	      << std::setw(10) << "Nbox"    // 2
	      << std::setw(18) << "Mean"    // 3
	      << std::setw(18) << "P_min"   // 4
	      << std::setw(18) << "P_max"   // 5
	      << std::setw(18) << "Volume"  // 6
	      << std::setw(18) << "ML"	    // 7
	      << std::setw(18) << "log(ML)" // 8
	      << std::endl
	      << std::setw( 4) << "---"
	      << std::setw(10) << "------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::endl;

  // Flatten multiple chain data
  //
  std::vector<double> prob1;
  std::vector< std::vector<double> > data1;
  unsigned ndata0 = data.size();
  unsigned nchain = mask.size();
  std::vector<unsigned> partn;

  for (unsigned k=nconverge; k<ndata0; k++) {
    for (unsigned j=0; j<nchain; j++) {
      if (mask[j]) continue;
      prob1.push_back(prob[k][j][0]);
      data1.push_back(data[k][j]);
      partn.push_back(partn.size());
    }
  }

  unsigned dsize  = data1.size();
  unsigned rsize  = data1[0].size();

  // Generate random index selection: resampling with replacement
  //
  std::random_device rd;	// Use random pool for seed
  std::mt19937 gen(rd());
  std::uniform_int_distribution<unsigned> dis(0, dsize-1);
 
  std::vector<double> trials(sampN);

  // Get maximum a posterior value
  //
  ret[2] = -DBL_MAX;
  for (auto v : prob1) ret[2] = std::max<double>(ret[2], v);

  // Begin bootstrap statistic loop
  //
  typedef std::pair<double, int> Dvalue;
  typedef std::vector<Dvalue>    Dtype;

  for (unsigned n=0; n<sampN; n++) {

    // Map ranked by posterior probability
    //
    Dtype llist;
    for (unsigned k=0; k<dsize; k++) {
      unsigned j = dis(gen);
      llist.push_back(Dvalue(prob1[j], j));
    }

    // Compute the bounding box
    //
    std::vector<double> pminT(rsize, DBL_MAX), pmaxT(rsize, -DBL_MAX);
    Dvalue maxP(-DBL_MAX, 0);
    for (auto v : llist) {
      size_t j = v.second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data1[j][k];
	pminT[k] = std::min<double>(pminT[k], val);
	pmaxT[k] = std::max<double>(pmaxT[k], val);
      }
      if (maxP.first < v.first) maxP = v;
    }
    
    // Compute a scaling length
    //
    std::vector<double> scale(rsize);
    double initvol = 1.0;
    for (unsigned k=0; k<rsize; k++) {
      scale[k] = pmaxT[k] - pminT[k];
      initvol *= scale[k];
    }

    // Number of states in this resample
    //
    size_t nstates = llist.size();

    if (nstates != dsize) {
      std::cout << "Resampled data size mismatch" << std::endl;
    }

    //--------------------------------------------------
    // Compute the L1 distance
    //--------------------------------------------------
    
    // Use the MAP as the center
    //
    std::vector<double> center(rsize);
    for (unsigned k=0; k<rsize; k++) center[k] = data1[maxP.second][k];
    
    // Create map ranked by L1 distance from MAP value
    //
    Dtype expfct;
    for (auto v : llist) {
      int     j = v.second;
      double L1 = 0.0;
      for (unsigned k=0; k<rsize; k++) {
	L1 = std::max<double>(L1, fabs(data1[j][k] - center[k])/scale[k]);
      }
      expfct.push_back(Dvalue(L1, j));
    }
  
    Dtype::iterator it = expfct.begin(), jt = expfct.end(), kt;
    Dtype::iterator mt = it;
    size_t maxStates = std::min<size_t>(nstates/10, 10000);

    for (size_t n=0; n<maxStates and mt!=jt; n++) mt++;
    
    // Use partial sort to reduce the cost of a full sort or a multimap
    //
    std::partial_sort(it, mt, jt);

    for (kt=it; kt!=mt; kt++) {
      int k = kt->second;
      if (maxP.first - prob1[k] > probD) break;
    }

    size_t nmode = std::distance(it, kt);

    // Find enclosing box and mean posterior value
    //
    std::vector<double> pmin(rsize, DBL_MAX), pmax(rsize, -DBL_MAX);
    double mean = 0.0;
    for (Dtype::iterator qt=it; qt!=kt; qt++) {
      size_t j = qt->second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data1[j][k];
	pmin[k] = std::min<double>(pmin[k], val);
	pmax[k] = std::max<double>(pmax[k], val);
      }
      mean += exp(prob1[j] - ret[2]);
    }
    
    // Compute the MC integral
    //
    mean /= nmode;
    double volume = 1.0;
    for (unsigned k=0; k<rsize; k++) volume *= pmax[k] - pmin[k];
    
    trials[n] = mean * volume * nstates/nmode;

    if (dbg_verbose)
      std::cout << std::setw( 4) << n
		<< std::setw(10) << nmode
		<< std::setw(18) << mean
		<< std::setw(18) << prob1[kt->second] - ret[2]
		<< std::setw(18) << prob1[jt->second] - ret[2]
		<< std::setw(18) << volume
		<< std::setw(18) << trials[n]
		<< std::setw(18) << log(trials[n])
		<< std::endl;
  }

  if (dbg_verbose)
    std::cout << header << std::endl
	      << "Peak prob = " << ret[2] << std::endl
	      << header << std::endl;

  // Final results
  //
  for (auto v : trials) ret[0] += v/sampN;
  for (auto v : trials) ret[1] += (v - ret[0])*(v - ret[0])/(sampN-1);

  return ret;
}

std::string inputfile("multilevel.dat");
std::string outputfile("new.chain");

/*
  Parse one line of the state log file

  Return true if level matches current level and populate data vectors

  Return false if level does not match and do not populate data vectors
*/
bool parse_line(char *line, int level, int& iter, std::vector<double>& prob,
		unsigned& m, std::vector<double>& parm, bool mixture)
{
  double v;
  int l;

  std::istringstream istr(line); // check level
  istr >> l;
  if (l!=level) return false;

  istr >> iter;			// dump iteration counter

  prob.erase(prob.begin(), prob.end());

  istr >> v;			// read probability
  prob.push_back(v);
  istr >> v;			// read likeliehood
  prob.push_back(v);
  istr >> v;			// read prior
  prob.push_back(v);
  if (mixture) istr >> m;	// read number
  else m = 1;
				// read parameter vector
  parm.erase(parm.begin(), parm.end());
  while (istr) {
    istr >> v;
    if (!istr) break;
    parm.push_back(v);
  }

  return true;
}

//
// Parse the header into field labels.  Look for "Number" after
// the "Prior" field as a sign of a mixture
//
std::vector<std::string> parseHeader(char *line)
{
  std::vector<std::string> labs;
  char *p = line;
  while (*p!='\0') {
    if (*p=='"') {
      labs.push_back(std::string(""));
      while (*(++p)!='"') labs.back().push_back(*p);
    }
    ++p;
  }

  return labs;
}

bool isMixture(const std::vector<std::string>& labs)
{
  bool ret = false;
  for (unsigned n=0; n<labs.size(); n++) {
    if (labs[n].compare("Prior")==0) {
      if (labs[n+1].compare("Number")==0) ret = true;
      break;
    }
  }
  
  return ret;
}

int
main(int argc, char** argv)
{
  /***************************************************
    Global variables
   ***************************************************/
				// Use this level
  int level       = 0;
				// Number of states in ensemble before testing
  int maxit       = 500;
				// Number of steps between tests
  int nskip       = 100;
				// Confidence interval for two-sided 
  double alpha    = 0.05;	// Grubbs test

				// Maximum offset for accepting
  double poffset  = -30.0;	// outlier in addition to Grubbs test


  int maxout      = 6;		// Maximum number of outliers

				// For RJTwo models
				// ----------------
  int T           = -1;		// # of common parameters
  int N1          =  1;		// # of Model 1 parameters
  int N2          =  1;		// # of Model 2 parameters
  
  double    probD = -1.0;       // Marginal likelihood prob offset target
  unsigned  sampN =  0;		// Number of bootstrap samples

  double MaxR     = 1.2;	// GR statistic threshold

  bool dump       = false;	// Dump converged, burnt-in chain

				// Retain last convergence test sample
  bool retain     = true;	// in converged output

  bool oldML      = false;	// Provide message to user about '-e' flag

  bool skipGR     = false;	// Skip convergence analysis

  enum MaskType {grubbs, rosner};
  MaskType maskType = grubbs;

  /***************************************************
    Parse command line
  ***************************************************/

  int c;
  while (1) {
    c = getopt (argc, argv, "i:o:l:m:n:N:a:r:T:1:2:M:S:defsRGh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'l': level    = atoi(optarg); break;
      case 'N': maxit    = atoi(optarg); break;
      case 'n': nskip    = atoi(optarg); break;
      case 'm': maxout   = atoi(optarg); break;
      case 'a': alpha    = atof(optarg); break;
      case 'r': MaxR     = atof(optarg); break;
      case 'd': dump     = true;         break;
      case 'f': retain   = false;        break;
      case 'T': T        = atoi(optarg); break;
      case '1': N1       = atoi(optarg); break;
      case '2': N2       = atoi(optarg); break;
      case 'M': probD    = atof(optarg); break;
      case 'S': sampN    = atoi(optarg); break;
      case 'e': oldML    = true;         break;
      case 'i': inputfile.erase();  inputfile  = optarg; break;
      case 'o': outputfile.erase(); outputfile = optarg; break;
      case 's': skipGR   = true;         break;
      case 'R': maskType = rosner;       break;
      case 'G': maskType = grubbs;       break;
      case 'h':
	std::cerr << std::endl << "**** GRanalyze *****"
		  << std::endl << std::endl <<
	  "\tComputes the Gelman-Rubin statistic for multiple-chain\n"
	  "\talgorithms such as DifferentialEvolution and ParallelChains.\n\n"
          "\tOutlier chains are dropped automatically using the Grubbs'\n"
	  "\tstatistic (-G) or the Rosner algorithm (-R) with the provided\n"
	  "\tconfidence value (-a value).  If neither is specificed, you\n"
	  "\tget the Grubbs algorithm by default.  This has been the most\n"
	  "\textensively tested and is recommended. Mixing fractions are\n"
	  "\talso computed.  Poor mixing within chains will cause Gelman-\n"
	  "\tRubin to fail, allowing unconverged chains to be diagnosed.\n\n"
	  "\tThe converged chain may be dumped by specifying the -d flag\n"
	  "\tinto the file specified by the -o flag (the output file is\n"
	  "\t'new.chain' by default).\n\n"
	  "\tExample:\n"
	  "\t\tGRanalyze 0 -N 1000 -n 1000 -m 16 -i bie.posterior -d -r 1.3\n\n"
	  "\tThe marginal likelhood may be estimated using the algorithm\n"
	  "\tdescribed in arXiv:1301.3156 by specifying a positive offset\n"
	  "\tvalue with the -M flag.  The offset computes a sub box in\n"
	  "\tparameter space determined by L1 distance from the MAP value.\n"
	  "\tA bootstrap tvariance estimate will be computed if you specify\n"
	  "\ta postive tvalue for sampN with the -S flag. This is expen-\n"
	  "\tsive but provides some estimate of the error in ML for the\n"
	  "\tspecified value of the probabilaity offset. The convergence\n"
	  "\ttest may be skipped by specifying the -s flag.\n\n"
	  "\tExample:\n"
	  "\t\tGRanalyze -s -i new.chain -M 6 -S 100\n\n";
      case '?': 
	std::string msg = "usage: " + std::string(argv[0]) + " [options]\n\n\twhere options are:\n\n";
	msg += "\t-l int   \t\tlevel to analyze (default: 0)\n";
	msg += "\t-N int   \t\tnumber of states in each analysis (default: 500)\n";
	msg += "\t-n int   \t\tanalysis interval (default: 100)\n";
	msg += "\t-m int   \t\tmaximum number of allowed outliers (default: 6)\n";
	msg += "\t-T int   \t\tFor reversible jump models: # of common parameters (default: -1 means not reversible jump)\n";
	msg += "\t-1 int   \t\tFor reversible jump models: # of Model 1 parameters (default: 1)\n";
	msg += "\t-2 int   \t\tFor reversible jump models: # of Model 2 parameters (default: 1)\n";
	msg += "\t-a float \t\tGrubbs' or Rosner test confidence (default: 0.05)\n";
	msg += "\t-r float \t\tupper limit for GR statistic (default: 1.2)\n";
	msg += "\t-M float \t\tCompute Marginal Likelihood with box trim for delta P < value\n";
	msg += "\t-S int   \t\tNumber of bootstrap samples (default: 0)\n";
	msg += "\t-i string\t\tinput file (default: " + inputfile + ")\n";
	msg += "\t-d       \t\tdump converged chain with outliers removed\n";
	msg += "\t-o string\t\toutput file (default: " + outputfile + ")\n";
	msg += "\t-s       \t\tassume that state file is converged\n";
	msg += "\t-f       \t\tDO NOT dump tested steps with converged chain\n";
	std::cerr << msg; exit(-1);
      }
  }

  // Check for deprecated flag
  if (oldML) {
    std::cout << std::string(72, '-') << std::endl
	      << "The '-e' flag is gone.  To compute a marginal likelihood estimate from" << std::endl
	      << "the converged chain, use the '-M m' flag instead, where the offset" << std::endl
	      << "probability value from the MAP value used to compute a sub box.  A good staring" << std::endl
	      << "value is m=1000." << std::endl
	      << std::string(72, '-') << std::endl;
    exit(-1);
  }

  std::shared_ptr<OutlierMask> outlier;
  if (maskType == grubbs)
    outlier = std::shared_ptr<OutlierMask>(new Grubbs);
  else 
    outlier = std::shared_ptr<OutlierMask>(new Rosner);

  /***************************************************
    Read in multilevel data
   **************************************************/

  std::ifstream in(inputfile.c_str());
  if (!in) {
    std::cerr << argv[0] << ": error opening <" << inputfile << std::endl;
    exit(-1);
  }

  const int linesize=2048;
  char line[linesize], header[linesize];
  int iter, last=-1;
  unsigned m;
  std::vector<double> prb, parm;

				// Read in (and save) header string
  in.getline(header, linesize);
				// Sanity check
  if (std::string(header).find("Probability") == std::string::npos) {
    std::cerr << "Expect the first line to be a field header, but found:" << std::endl
	      << header << std::endl;
    exit(-1);
  }

  std::vector<std::string> labs = parseHeader(header);
  bool mixture = isMixture(labs);

  mcmcType prob, data;
  std::deque< std::vector<unsigned> > numb;

				// Read in the lines at the desired level
  bool first = true;

  std::vector<unsigned> mvec;
  std::vector< std::vector<double> > prec, drec;

  while (in) {
    in.getline(line, linesize);
    if (!in) break;
    while (parse_line(line, level, iter, prb, m, parm, mixture)) {
      if (first || last!=iter) {
	if (mvec.size()) {
	  prob.push_back(prec);
	  numb.push_back(mvec);
	  data.push_back(drec);

	  prec.erase(prec.begin(), prec.end());
	  mvec.erase(mvec.begin(), mvec.end());
	  drec.erase(drec.begin(), drec.end());
	}
	last = iter;
	first = false;
      }
	
      prec.push_back(prb);
      mvec.push_back(m);
      drec.push_back(parm);

      in.getline(line, linesize);
      if (!in) break;
    }
    if (!first) break;
  }

  unsigned psize, dsize, rsize;

  if (prob.size() < 2 || data.size() < 2) {
    std::cout << "No states at Level " << level << std::endl;
    exit(-1);
  }

  std::cout << "=========================" << std::endl
	    << "====  Data file info ====" << std::endl
	    << "=========================" << std::endl
	    << "Size of prob:       " << prob.size() << std::endl
	    << "Size of data:       " << (dsize=data.size()) << std::endl
	    << "Size of chain set:  " << (psize=data.front().size()) << std::endl
	    << "Size of param vec:  " << (rsize=data.front()[0].size()) << std::endl;
  
  for (unsigned n=1; n<data.size(); n++) {
    if (data[n].size() != psize) {
      std::cout << "Record " << n << " has size=" << data[n].size()
		<< std::endl;
    }
  }

  int nconverge=-1;
  std::vector<unsigned char> mask;

  if (skipGR) {

    nconverge = 0;
    
    std::cout << std::endl;

  } else {

    unsigned nend=std::max<unsigned>(maxit, nskip), count=0, ssize=maxit;

    while (nend<dsize) {

      count++;

      if (maxit==0) ssize = nend/2;

      std::ostringstream ostr;
      if (maxit)
	ostr << "==== Interval #" << count << ": [" 
	     << std::max<int>(0, (int)nend-(int)maxit) << ", " << nend << "] ";
      else
	ostr << "==== Interval #" << count << ": [" 
	     << std::max<unsigned>(0, (int)nend-(int)ssize) << ", " << nend << "] ";
      std::cout << std::setw(72) << std::setfill('=') << "=" << std::endl
		<< std::setw(72) << std::left << ostr.str()  << std::endl
		<< std::setw(72) << std::setfill('=') << "=" << std::endl 
		<< std::setfill(' ');


      bool ret;
      if (T>=0)
	ret = outlier->compute(prob[nend], data[nend], 
			       alpha, maxout, T, N1, N2, poffset, true);
      else
	ret = outlier->compute(prob[nend], data[nend], 
			       alpha, maxout, poffset, true);

      mask = (*outlier)();

      if (ret) {

	std::vector<double> frac =
	  MixingFraction(data, mask, nend, ssize, false);
	double wmix=2.0;
	unsigned wnum=-1;
	for (unsigned n=0; n<frac.size(); n++) {
	  if (mask[n]==1) continue;
	  if (frac[n]<wmix) {
	    wmix = frac[n];
	    wnum = n;
	  }
	}
	
	std::cout << "Worst non-masked mixing fraction in Chain #" << wnum
		  << " frac=" << wmix << std::endl;
	
	double val;
	if (T>=0) 
	  val = GR(data, mask, nend, ssize, T, N1, N2, true, false);
	else
	  val = GR(data, mask, nend, ssize, true, false);

	if (val < MaxR) {
	  if (retain)
	    nconverge = std::max<int>(0, nend-ssize);
	  else
	    nconverge = nend;
	  break;
	}
      }

      nend += nskip;
    }

    std::cout << std::endl;

    if (nconverge>=0)
      std::cout << "Converged at n=" << nconverge << std::endl;
    else
      std::cout << "No convergence" << std::endl;
  }
    

  if (dump && nconverge>=0) {

    unsigned nchain = mask.size();
    unsigned nmask = 0;
    std::vector<unsigned char>::iterator ip;
    for (ip=mask.begin(); ip!=mask.end(); ip++) {
      if (*ip == 1) nmask++;
    }

    std::ofstream out(outputfile.c_str());
    out.precision(12);
    if (out) {
      std::cout << "Dumping converged states to file <"
		<< outputfile << "> with " << nmask << " chains removed" 
		<< std::endl;
      
      out << header << std::endl;
      for (unsigned i=nconverge; i<dsize; i++) {
	for (unsigned j=0; j<nchain; j++) {
	  if (mask[j]) continue;
	  out << std::setw(6) << level
	      << std::setw(7) << i;
	  for (int k=0; k<3; k++)
	    out << std::setw(20) << prob[i][j][k];
	  if (mixture) out << std::setw(10) << numb[i][j];
	  for (unsigned k=0; k<rsize; k++)
	    out << std::setw(20) << data[i][j][k];
	  out << std::endl;
	}
      }
    } else {
      std::cout << "Could not open dump file <" << outputfile << ">"
		<< std::endl;
    }
  }

  if (probD>0.0 && nconverge<0) {
    std::cout << std::endl
	      << "Skipping marginal likelihood computation due because of no convergence" << std::endl;
  }
  
  if (probD>0.0 && nconverge>=0) {

    std::cout << std::endl
	      << std::string(30+2*18, '-') << std::endl
	      << "START MARGINAL LIKELIHOOD COMPUTATION" << std::endl
	      << std::string(30+2*18, '-') << std::endl;
    
    unsigned nchain = mask.size();
    unsigned nmask  = 0;
    std::vector<unsigned char>::iterator ip;
    for (ip=mask.begin(); ip!=mask.end(); ip++) {
      if (*ip == 1) nmask++;
    }
    
    // Map ranked by posterior probability
    //
    Pmap llist;
    for (unsigned i=nconverge; i<dsize; i++) {
      for (unsigned j=0; j<nchain; j++) {
	if (mask[j]) continue;
	llist.push_back(Pmap_value(prob[i][j][0], Ielem(i, j)));
      }
    }

    // Compute the bounding box
    //
    std::vector<double> pminT(rsize, DBL_MAX), pmaxT(rsize, -DBL_MAX);
    Pmap_value maxP(-DBL_MAX, Ielem(0, 0));
    for (auto v : llist) {
      size_t k1 = v.second.first;
      size_t k2 = v.second.second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data[k1][k2][k];
	pminT[k] = std::min<double>(pminT[k], val);
	pmaxT[k] = std::max<double>(pmaxT[k], val);
      }
      if (maxP.first < v.first) maxP = v;
    }

    // Compute a scaling length
    //
    std::vector<double> scale(rsize);
    double initvol = 1.0;
    for (unsigned k=0; k<rsize; k++) {
      scale[k] = pmaxT[k] - pminT[k];
      initvol *= scale[k];
    }

    //--------------------------------------------------
    // Compute the L1 distance
    //--------------------------------------------------
    
    // Use the MAP as the center
    //
    std::vector<double> center(rsize);
    {
      size_t c1 = maxP.second.first;
      size_t c2 = maxP.second.second;
      for (unsigned k=0; k<rsize; k++) center[k] = data[c1][c2][k];
    }
    
    // Create map ranked by L1 distance from MAP value
    //
    Pmap expfct;
    for (auto v : llist) {
      size_t k1 = v.second.first;
      size_t k2 = v.second.second;
      double L1 = 0.0;
      for (unsigned k=0; k<rsize; k++) {
	L1 = std::max<double>(L1, fabs(data[k1][k2][k] - center[k])/scale[k]);
      }
      expfct.push_back(Pmap_value(L1, v.second));
    }
  
    size_t nstates = llist.size();
    size_t maxStates = std::min<size_t>(nstates/10, 10000);

    Pmap::iterator it = expfct.begin(), jt = expfct.end(), kt;
    Pmap::iterator mt = it;

    for (size_t n=0; n<maxStates and mt!=jt; n++) mt++;
    
    // Use partial sort to reduce the cost of a full sort or a multimap
    //
    std::partial_sort(it, mt, jt);

    for (kt=it; kt!=mt; kt++) {
      int k1 = kt->second.first;
      int k2 = kt->second.second;
      if (maxP.first - prob[k1][k2][0] > probD) break;
    }

    size_t nsubbox = std::distance(it, kt);

				// Initial point in list
    size_t c1 = it->second.first;
    size_t c2 = it->second.second;
    
				// Final point in sub box list
    size_t d1 = kt->second.first;
    size_t d2 = kt->second.second;

    double Prmax = prob[c1][c2][0];
    double Prmin = prob[d1][d2][0];
 
    std::cout << "Computing marginal likelhood with " << nmask 
	      << " chains removed" << std::endl
	      << "Using " << nstates << " states in total" << std::endl
	      << "Using " << nsubbox << " states in the sub box" << std::endl
	      << "[Min : Max] prob in sub box: ["
	      << Prmin << " : " << Prmax << "]" << std::endl;

    // Find enclosing box and mean posterior value
    //
    std::vector<double> pmin(rsize, DBL_MAX), pmax(rsize, -DBL_MAX);
    double mean = 0.0;
    for (iterType qt=it; qt!=kt; qt++) {
      size_t k1 = qt->second.first;
      size_t k2 = qt->second.second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data[k1][k2][k];
	pmin[k] = std::min<double>(pmin[k], val);
	pmax[k] = std::max<double>(pmax[k], val);
      }
      mean += exp(prob[k1][k2][0] - Prmax);
    }
    
    std::cout << std::string(24+6*16, '-') << std::endl
	      << "Box stats" << std::endl
	      << std::string(24+6*16, '-') << std::endl
	      << std::left
	      << std::setw(24) << "Label"
	      << std::setw(16) << "Peak"
	      << std::setw(16) << "Sub min"
	      << std::setw(16) << "Sub max"
	      << std::setw(16) << "Tot min"
	      << std::setw(16) << "Tot max"
	      << std::setw(16) << "Ratio"
	      << std::endl
	      << std::setw(24) << "------------";

    for (size_t k=0; k<6; k++) std::cout << std::setw(16) << "-------";
    std::cout << std::endl;

    for (unsigned k=0; k<rsize; k++) {
      std::cout << std::left
		<< std::setw(24) << labs[k+5]
		<< std::setw(16) << center[k]
		<< std::setw(16) << pmin[k]
		<< std::setw(16) << pmax[k]
		<< std::setw(16) << pminT[k]
		<< std::setw(16) << pmaxT[k]
		<< std::setw(16) << (pmax[k] - pmin[k])/(pmaxT[k] - pminT[k])
		<< std::endl;
    }
    std::cout << std::endl;

    // Compute the MC integral
    //
    mean /= nsubbox;
    double volume = 1.0;
    for (unsigned k=0; k<rsize; k++) volume *= pmax[k] - pmin[k];
    
    BSret bs = bootstrap(prob, data, mask, nconverge, sampN, probD);

    std::cout << std::string(30+2*18, '-') << std::endl
	      << "MARGINAL LIKELIHOOD COMPUTATION SUMMARY" << std::endl
	      << std::string(30+2*18, '-') << std::endl
	      << std::setw(30) << "Quantity"
	      << std::setw(18) << "Value"
	      << std::setw(18) << "log(Value)"
	      << std::endl
	      << std::string(30+2*18, '-') << std::endl
	      << std::setw(30) << "Mean prob in sub box"
	      << std::setw(18) << mean
	      << std::setw(18) << log(mean) + Prmax
	      << std::endl
	      << std::setw(30) << "Volume of initial box"
	      << std::setw(18) << initvol
	      << std::setw(18) << log(initvol)
	      << std::endl
	      << std::setw(30) << "Volume of sub box"
	      << std::setw(18) << volume
	      << std::setw(18) << log(volume)
	      << std::endl
	      << std::setw(30) << "Count ratio"
	      << std::setw(18) << static_cast<double>(nstates)/nsubbox
	      << std::setw(18) << log(nstates) - log(nsubbox)
	      << std::endl
	      << std::setw(30) << "Sub/initial volume ratio"
	      << std::setw(18) << volume/initvol
	      << std::setw(18) << log(volume/initvol)
	      << std::endl;

    if (sampN>1) {
      std::cout << std::setw(30) << "Mean"
		<< std::setw(18) << bs[0]
		<< std::setw(18) << log(bs[0])
		<< std::endl
		<< std::setw(30) << "Variance"
		<< std::setw(18) << sqrt(bs[1])
		<< std::setw(18) << log(sqrt(bs[1]))
		<< std::endl
		<< std::setw(30) << "Ratio"
		<< std::setw(18) << sqrt(bs[1])/bs[0]
		<< std::setw(18) << log(sqrt(bs[1])/bs[0])
		<< std::endl
		<< std::setw(30) << "Bootstrap error [neg/pos]"
		<< std::setw(18) << log(bs[0] - sqrt(bs[1])) + bs[2]
		<< std::setw(18) << log(bs[0] + sqrt(bs[1])) + bs[2]
		<< std::endl;
    }
    
    // Upscale by count ratio
    //
    mean *= volume * nstates/nsubbox;
    
    std::cout << std::string(30+2*18, '-') << std::endl
	      << std::setw(30) << "**Marginal likelihood**"
	      << std::setw(18) << mean * exp(Prmax)
	      << std::setw(18) << log(mean) + Prmax
	      << std::endl << std::string(30+2*18, '-') << std::endl;
  }

  return 0;
}

