// -*- C++ -*-

/* 
   Reads in a multilevel output file and computes covariance matrix
   for each level and tests generation of normal variates from this
   distribution
*/

#define NOCHOL


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;


#include <unistd.h>

#include <../libsrnd/ACG.h>
#include <../libsrnd/Normal.h>

#include <../libVector/VectorM.h>

string inputfile("multilevel.dat");
string outputtag("covar");

int
main(int argc, char** argv)
{
  /***************************************************
    Global variables
   ***************************************************/

				// Use this level
  int level = 0;
  int nfactor = 1000;

  /***************************************************
    Parse command line
   ***************************************************/

  int c;
  while (1) {
    c = getopt (argc, argv, "i:o:l:n:");
    if (c == -1) break;
     
    switch (c)
      {
      case 'l': level = atoi(optarg); break;
      case 'n': nfactor = atoi(optarg); break;
      case 'i': inputfile.erase(); inputfile = optarg; break;
      case 'o': outputtag.erase(); outputtag = optarg; break;
      case '?': 
	string msg = "usage: " + string(argv[0]) + " [options]\n";
	msg += "\t-l int\t\tlevel to analyze\n";
	msg += "\t-n int\t\tincrease sample size by factor\n";
	msg += "\t-i string\t\tinput file\n";
	msg += "\t-o string\t\toutput file tag\n";
	cerr << msg; exit(-1);
      }
  }

  /***************************************************
    Read in multilevel data
   **************************************************/

  ifstream in(inputfile.c_str());
  if (!in) {
    cerr << argv[0] << ": error opening <" << inputfile << ">\n";
    exit(-1);
  }

				// Make a vector type (used to define a vector
				// of vectors in STL)
  typedef vector<double> dvec;
				// Make a vector of vectors to hold parameters
  vector<dvec> data;
				// Make a vector to hold posterior probability
  vector<double> prob;

  const int linesize=2048;
  char line[linesize];
  int l;
  double v;
				// Read in (and discard) header string
  in.getline(line, linesize);


				// Read in the lines at the desired level
  while (in) {
    in.getline(line, linesize);
    if (!in) continue;

    istringstream istr(line);	// check level
    istr >> l;
    if (l!=level) continue;
    istr >> l;			// dump counter

    istr >> v;			// read prob level
    prob.push_back(v);

    vector<double> tmp;		// read parameter vector
    while (istr) {
      istr >> v;
      if (!istr) break;
      tmp.push_back(v);
    }
    data.push_back(tmp);
  }

  cout << "Size of prob:    " << prob.size() << endl;
  cout << "Size of data:    " << data.size() << endl;
  if (data.size() > 0)
    cout << "Size of data[0]: " << data[0].size() << endl;
  else
    exit(0);

  /***************************************************
    Initialize output files
   **************************************************/
  string outfile;

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".covar";
  ofstream out1(outfile.c_str());
  if (!out1) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".eigen";
  ofstream out2(outfile.c_str());
  if (!out2) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  /***************************************************
    Begin computation
   **************************************************/

  int N = prob.size();
  int q = data[0].size();

				// Set up storage for convarianice matrix
  vector<double> mean(q, 0.0);
  vector<dvec> covar(q, mean);

  for (int i=0; i<N; i++) {
    for (int k=0; k<q; k++) {
      mean[k] += data[i][k];
      for (int l=0; l<q; l++) covar[k][l] += data[i][k] * data[i][l];
    }
  }

  for (int k=0; k<q; k++) mean[k] /= N;

  for (int k=0; k<q; k++) 
    for (int l=0; l<q; l++) 
      covar[k][l] = (covar[k][l] - mean[k]*mean[l]*N)/(N-1);
  
  /***************************************************
    Output
   **************************************************/
  
  out1 << "Level " << level << endl << endl;
  out1 << setw(5) << "n" << setw(15) 
       << "Mean" << setw(15) << "Stdev:" << endl << endl;
  for (int k=0; k<q; k++) 
    out1 << setw(5) << k 
	 << setw(15) << mean[k] 
	 << setw(15) << sqrt(covar[k][k])
	 << endl;

  out1 << endl;

  out1 << "Covar:" << endl << endl;
  out1 << setw(5) << "n\\n";
  for (int k=0; k<q; k++) 
    out1 << setw(15) << k;
  out1 << endl;

  for (int k=0; k<q; k++) {
    out1 << setw(5) << k;
    for (int l=0; l<q; l++) 
      out1 << setw(15) << covar[k][l];
    out1 << endl;
  }
  out1 << endl;

  /***************************************************
    Cholesky decomposition of the covariance matrix
   **************************************************/
  
  MatrixM Mcovar(1, q, 1, q);

  for (int k=0; k<q; k++) 
    for (int l=0; l<q; l++)
      Mcovar[k+1][l+1] = covar[k][l];

#ifndef NOCHOL

  double sum;

  MatrixM Mlower(1, q, 1, q);
  Mlower.zero();

  for (int k=0; k<q; k++) {
    for (int l=k; l<q; l++) {
      sum = Mcovar[k+1][l+1];
      for (int m=k-1; m>=0; m--) 
	sum -= Mlower[k+1][m+1]*Mlower[l+1][m+1];

      if (k==l) {
	if (sum <= 0.0) {
	  cerr << "Mcovar is non pos dev?\n";
	  exit(-1);
	}
	Mlower[k+1][k+1] = sqrt(sum);
      } else
	Mlower[l+1][k+1] = sum/Mlower[k+1][k+1];
    }
  }

#endif  

  /***************************************************
    Generate data
   **************************************************/
  
  VectorM Vrandom(1, q), Vdata;
  ACG gen(11, 20);
  Normal unit(0.0, 1.0, &gen);

  int N2 = N*nfactor;

#ifndef NOCHOL

  for (int k=0; k<q; k++) 
    for (int l=0; l<q; l++) 
      covar[k][l] = 0.0;

  for (int i=0; i<N2; i++) {

    for (int l=0; l<q; l++) Vrandom[l+1] = unit();

    Vdata = Mlower*Vrandom;

    for (int l=0; l<q; l++) Vdata[l+1] += mean[l];

    for (int k=0; k<q; k++)
      for (int l=0; l<q; l++) covar[k][l] += Vdata[k+1] * Vdata[l+1];
  }

  for (int k=0; k<q; k++) 
    for (int l=0; l<q; l++) 
      covar[k][l] = (covar[k][l] - mean[k]*mean[l]*N2)/(N2-1);
  
  /***************************************************
    Output
   **************************************************/
  
  out2 << "Covar orig:" << endl << endl;
  out2 << setw(5) << "n\\n";
  for (int k=0; k<q; k++) 
    out2 << setw(15) << k;
  out2 << endl;

  for (int k=0; k<q; k++) {
    out2 << setw(5) << k;
    for (int l=0; l<q; l++) 
      out2 << setw(15) << Mcovar[k+1][l+1];
    out2 << endl;
  }
  out2 << endl;

  out2 << "Covar:" << endl << endl;
  out2 << setw(5) << "n\\n";
  for (int k=0; k<q; k++) 
    out2 << setw(15) << k;
  out2 << endl;

  for (int k=0; k<q; k++) {
    out2 << setw(5) << k;
    for (int l=0; l<q; l++) 
      out2 << setw(15) << covar[k][l];
    out2 << endl;
  }
  out2 << endl;

  out2 << "Relative difference in percent:" << endl << endl;
  out2 << setw(5) << "n\\n";
  for (int k=0; k<q; k++) 
    out2 << setw(15) << k;
  out2 << endl;

  for (int k=0; k<q; k++) {
    out2 << setw(5) << k;
    for (int l=0; l<q; l++) 
      out2 << setw(15) << 100.0*(covar[k][l] - Mcovar[k+1][l+1])/Mcovar[k+1][l+1];
    out2 << endl;
  }
  out2 << endl;

#endif

  /***************************************************
    Eigenfunctions of the covariance matrix
   **************************************************/
  
  MatrixM Meigen(1, q, 1, q);
  VectorM Veigen = Mcovar.Symmetric_Eigenvalues_GHQL(Meigen);

  MatrixM Mtrans = Meigen.Transpose();
  VectorM Vmean(1, q);
  for (int l=0; l<q; l++) Vmean[l+1] = mean[l];

  Vmean = Mtrans * Vmean;

  /***************************************************
    Generate data
   **************************************************/
  
  for (int k=0; k<q; k++) {
    mean[k] = 0.0;
    for (int l=0; l<q; l++) 
      covar[k][l] = 0.0;
  }

  for (int i=0; i<N2; i++) {

    for (int l=0; l<q; l++) Vrandom[l+1] = Vmean[l+1] +
			      unit()*sqrt(fabs(Veigen[l+1]));

    Vdata = Meigen * Vrandom;

    // for (int l=0; l<q; l++) Vdata[l+1] += mean[l];
    for (int l=0; l<q; l++) mean[l] += Vdata[l+1];

    for (int k=0; k<q; k++)
      for (int l=0; l<q; l++) covar[k][l] += Vdata[k+1] * Vdata[l+1];
  }

  for (int k=0; k<q; k++)  mean[k] /= N2;

  for (int k=0; k<q; k++) 
    for (int l=0; l<q; l++) 
      covar[k][l] = (covar[k][l] - mean[k]*mean[l]*N2)/(N2-1);
  
  /***************************************************
    Output
   **************************************************/
  
  out2 << "Eigentransform-based covar:" << endl << endl;
  out2 << setw(5) << "n\\n";
  for (int k=0; k<q; k++) 
    out2 << setw(15) << k;
  out2 << endl;

  for (int k=0; k<q; k++) {
    out2 << setw(5) << k;
    for (int l=0; l<q; l++) 
      out2 << setw(15) << covar[k][l];
    out2 << endl;
  }
  out2 << endl;

  out2 << "Relative difference in percent:" << endl << endl;
  out2 << setw(5) << "n\\n";
  for (int k=0; k<q; k++) 
    out2 << setw(15) << k;
  out2 << endl;

  for (int k=0; k<q; k++) {
    out2 << setw(5) << k;
    for (int l=0; l<q; l++) 
      out2 << setw(15) << 100.0*(covar[k][l] - Mcovar[k+1][l+1])/Mcovar[k+1][l+1];
    out2 << endl;
  }
  out2 << endl;

}




