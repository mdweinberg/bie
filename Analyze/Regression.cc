#include <Regression.H>

using namespace std;

/**
   Calculate the mean of the x values in a vector of point
   objects.

*/
double Regression::meanX( vector<Regression::tuple> &data ) const
{
  double mean = 0.0, sum = 0.0;

  const size_t N = data.size();
  for (size_t i=0; i<N; i++) {
    sum =  data[i].first;
  }
  mean = sum / N;
  return mean;
} // meanX

/**
   Calculate the mean of the y values in a vector of point
   objects.

 */
double Regression::meanY( vector<Regression::tuple> &data ) const
{
  const size_t N = data.size();
  double mean = 0.0, sum = 0.0;

  for (size_t i=0; i<N; i++) {
    sum += data[i].second;
    }
  return mean/N;
} // meanY



/**
  Calculate the linear regression coefficients, a and b, along with
  the standard regression error (e.g., the error of b), the standard
  deviation of the points and the correlation.

  A regression line is described by the equation y' = a + bx.  The
  coefficients a and b are returned in a lineInfo object, along
  with the other values.

  Formally, linear regression of a set of {x,y} points is described in
  terms of independent and dependent variables.  The array x contains
  the independent variable, which is exactly known.  The array y
  contains the dependent variable, which is "measured".  The y values
  are assumed to be random values, dependent on the x values.

 */
void Regression::compute( vector<Regression::tuple>& data,
			  lineInfo& info ) const
{
  const size_t N = data.size();
  if (N > 1) {

    double muX = meanX( data );
    double muY = meanY( data );

    //     N-1
    //     ---
    //     \   (Xi - meanX)(Yi - meanY)
    //     /__ 
    //     i=0
    // b = -----------------------------
    //     N-1
    //     ---             2
    //     \   (Xi - meanX)
    //     /__ 
    //     i=0
    //

    double Sx   = 0;
    double SSx  = 0;
    double SSxx = 0;
    double Sy   = 0;
    double Sxy  = 0;
    double SSy  = 0;
    double SSxy = 0;
    double SSyy = 0;
    
    for (size_t i=0; i<N; i++) {
      Sx  += data[i].first;
      SSx += data[i].first * data[i].first;
      Sy  += data[i].second;
      Sxy += data[i].first  * data[i].second;
      SSy += data[i].second * data[i].second;
      
      double subX = data[i].first  - muX;
      double subY = data[i].second - muY;
      SSxx += subX * subX;
      SSyy += subY * subY;
      SSxy += subX * subY;
    }

    // slope
    double b = SSxy/SSxx;

    // intercept
    double a = muY - b*muX;

    // standard deviation of the points
    double stddevPoints = std::sqrt( (SSyy - b * SSxy)/(N-2) );
				
    // Error of the slope
    double bError = stddevPoints/std::sqrt(SSxx);

    double numer = Sxy*N - Sx*Sy;
    double denom = (SSx*N - Sx*Sx)*(SSy*N - Sy*Sy);
    double r2    = (numer * numer) / denom;
    double signB = (b < 0) ? -1.0 : 1.0;
    double r     = signB * std::sqrt( r2 );
    
    info.corr( r );
    info.stddev( stddevPoints );
    info.slopeErr( bError );
    info.slope( b );
    info.intercept( a );

  } // if N > 0

} // lineInfo
