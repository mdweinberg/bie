// This is -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cmath>

using namespace std;

//
// Structure for passing results
//
struct EvidenceReturn 
{
  double Emin;
  double Emax;
  double Eavg;
  double Ehmn;

  vector<double> L, Pl, Ph, E;
};

/**
   -------------
   New Algorithm 
   -------------

   Input:

   o vector<dvect> object containing the likelihood and
     (not-necessarily normalized) posterior probability (pairs) for
     the sample

   o the difference between adjacent values of log(L)
     must be smaller than the threshold value

   o return information about prior measure if prior=true

   Output:

   EvidenceReturn is a structure containing the results of Algorithm 1
   (Eavg), its lower (Emin) and upper (Emax) bounds, and the results
   of the harmonic mean approximation (Ehmn), and optionally, the
   prior measure computation.
   
 */
extern EvidenceReturn evidenceM(vector<double>& like, double threshold, 
			       bool prior=false);

extern EvidenceReturn evidenceN(vector<double>& like, unsigned subsample,
				bool prior=false);

extern EvidenceReturn evidenceO(vector<double>& like, 
				double threshold, double alpha,
				bool prior=false);

