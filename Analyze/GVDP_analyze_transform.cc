// -*- C++ -*-

/* 
   Reads in a multilevel output file and analyzes convergence
   and provides graphical output

   Implementation of algorithm proposed by:

   	Giakoumatos, Vrontos, Dellaportas and Politis, "An MCMC 
	Convergence Diagnostic using Subsampling" (1999), preprint 
	to appear in to in Journal of Computational and Graphical 
	Statistics. 

   This generalizes Gelman and Rubin (1992).
*/


#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

#include <math.h>

#include <unistd.h>
#include <stdlib.h>

#include <VectorM.h>

string inputfile("multilevel.dat");
string outputtag("gvdp");

int
main(int argc, char** argv)
{
  /***************************************************
    Global variables
   ***************************************************/
				// Use this level
  int level = 0;
				// Number of subintervals
  int nint = 10;
				// Number in each linear correlation test
  int ngrp = 10;
				// Quantile
  double alpha = 0.05;
				// Tolerance for COD
  double rtol = 0.99;
				// Scale coordinates
  bool scale = true;

  /***************************************************
    Parse command line
   ***************************************************/

  int c;
  while (1) {
    c = getopt (argc, argv, "i:o:l:n:g:q:r:A");
    if (c == -1) break;
     
    switch (c)
      {
      case 'l': level = atoi(optarg); break;
      case 'n': nint = atoi(optarg); break;
      case 'g': ngrp = atoi(optarg); break;
      case 'q': alpha = atof(optarg); break;
      case 'r': rtol = atof(optarg); break;
      case 'i': inputfile.erase(); inputfile = optarg; break;
      case 'o': outputtag.erase(); outputtag = optarg; break;
      case '?': 
	string msg = "usage: " + string(argv[0]) + " [options]\n";
	msg += "\t-l int\t\tlevel to analyze\n";
	msg += "\t-n int\t\tnumber of partitions\n";
	msg += "\t-g int\t\tnumber per correlation test\n";
	msg += "\t-q float\t\tquartile\n";
	msg += "\t-r float\t\tlimit for COD\n";
	msg += "\t-i string\t\tinput file\n";
	msg += "\t-o string\t\toutput file tag\n";
	msg += "\t-A \t\tuse absolute range (no scaling)\n";
	cerr << msg; exit(-1);
      }
  }

  /***************************************************
    Read in multilevel data
   **************************************************/

  ifstream in(inputfile.c_str());
  if (!in) {
    cerr << argv[0] << ": error opening <" << inputfile << ">\n";
    exit(-1);
  }

				// Make a vector type (used to define a vector
				// of vectors in STL)
  typedef vector<double> dvec;
				// Make a vector of vectors to hold parameters
  vector<dvec> data, ndata;
				// Make a vector to hold posterior probability
  vector<double> prob;

  const int linesize=2048;
  char line[linesize];
  int l, m;
  double v;

				// Read in (and discard) header string
  in.getline(line, linesize);

				// Read in the lines at the desired level
  while (in) {
    in.getline(line, linesize);
    if (!in) continue;

    istringstream istr(line);	// check level
    istr >> l;
    if (l!=level) continue;
    istr >> l;			// dump counter

    istr >> v;			// read prob level
    prob.push_back(v);
    istr >> v;			// dump likelihood
    istr >> v;			// dump prior
    istr >> m;			// dump number
    if (m==1) istr >> v;	// dump weight

    vector<double> tmp;		// read parameter vector
    while (istr) {
      istr >> v;
      if (!istr) break;
      tmp.push_back(v);
    }
    data.push_back(tmp);
  }

  cout << "Size of prob:    " << prob.size() << endl;
  cout << "Size of data:    " << data.size() << endl;
  if (data.size() > 0)
    cout << "Size of data[0]: " << data[0].size() << endl;
  else
    exit(0);

  /***************************************************
    Initialize output files
   **************************************************/
  string outfile;

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".maxdev";
  ofstream out1(outfile.c_str());
  if (!out1) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".corr";
  ofstream out2(outfile.c_str());
  if (!out2) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".bounds";
  ofstream out3(outfile.c_str());
  if (!out3) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  /***************************************************
    Compute covariance						      
   **************************************************/

  int N = prob.size();
  int q = data[0].size();

				// Set up storage for convarianice matrix
  vector<double> mean(q, 0.0);
  vector<dvec> covar(q, mean);

  for (int i=0; i<N; i++) {
    for (int k=0; k<q; k++) {
      mean[k] += data[i][k];
      for (int l=0; l<q; l++) covar[k][l] += data[i][k] * data[i][l];
    }
  }

  for (int k=0; k<q; k++) mean[k] /= N;

  for (int k=0; k<q; k++) 
    for (int l=0; l<q; l++) 
      covar[k][l] = (covar[k][l] - mean[k]*mean[l]*N)/(N-1);
  
  /***************************************************
    Cholesky decomposition of the covariance matrix
   **************************************************/
  
  MatrixM Mcovar(1, q, 1, q);

  for (int k=0; k<q; k++) 
    for (int l=0; l<q; l++)
      Mcovar[k+1][l+1] = covar[k][l];

  MatrixM Meigen(1, q, 1, q);
  VectorM Veigen = Mcovar.Symmetric_Eigenvalues_GHQL(Meigen);

  MatrixM Mtrans = Meigen.Transpose();


  VectorM Vtrans(1, q);
  vector <double> tmp(q);
  for (int i=0; i<N; i++) {
    for (int k=0; k<q; k++) Vtrans[k+1] = data[i][k];
    Vtrans = Mtrans * Vtrans;
    for (int k=0; k<q; k++) tmp[k] = Vtrans[k+1];
    ndata.push_back(tmp);
  }


  /***************************************************
    Begin computation
   **************************************************/

				// Set up for correlation analysis

  vector<double> xx, yy, uu;

				// Total mean and variance
  vector<double> tmean(q, 0.0);
  vector<double> tvar(q, 0.0);
  for (int i=0; i<N; i++) {
    for (int k=0; k<q; k++) tmean[k] += ndata[i][k];
    for (int k=0; k<q; k++) tvar[k] += ndata[i][k] * ndata[i][k];
  }
  for (int k=0; k<q; k++) tmean[k] /= N;
  for (int k=0; k<q; k++) tvar[k] = (tvar[k] - tmean[k]*tmean[k]*N)/(N-1);


  cout << endl 
       << "Maximum deviations: " << endl << endl;

				// Step 1: choose subsamples
  for (int j=1; j<=nint; j++) {
    int Nj = j*N/nint;
    int bj = (int)(sqrt( static_cast<float>(Nj)));
    int Bj = Nj - bj + 1;

				// Step 2: compute total statistic
    vector<double> mean(q, 0.0);
    vector<double> var(q, 0.0);
    for (int i=(j-1)*N/nint; i<Nj; i++) {
      for (int k=0; k<q; k++) mean[k] += ndata[i][k];
      for (int k=0; k<q; k++) var[k] += ndata[i][k] * ndata[i][k];
    }
				// Mean
    for (int k=0; k<q; k++) mean[k] /= Nj;
				// Variance
    for (int k=0; k<q; k++) var[k] = (var[k] - mean[k]*mean[k]*Nj)/(Nj-1);


    
				// Step 3: compute subsample statistic
    vector<dvec> smean(Bj);
    for (int i=0; i<Bj; i++) {
      smean[i] = vector<double>(q, 0.0);
      for (int i2=0; i2<bj; i2++)
	for (int k=0; k<q; k++) smean[i][k] += ndata[i+i2][k];
      for (int k=0; k<q; k++) smean[i][k] /= bj;
    }
				// Step 4: compute maximum deviation

    vector<double> deviation(Bj, 0.0);
    double testdif;
    for (int i=0; i<Bj; i++) {
      for (int k=0; k<q; k++) {
	if (scale)
	  testdif = sqrt(static_cast<float>(bj))*fabs(smean[i][k] - mean[k])/sqrt(tvar[k]);
	else
	  testdif = sqrt(static_cast<float>(bj))*fabs(smean[i][k] - mean[k]);
	if (testdif > deviation[i]) deviation[i] = testdif;
      }
    }
    
				// Step 5: sort and find quartile
    sort(deviation.begin(), deviation.end());
    double value = deviation[(int)((1.0 - alpha)*Bj+1.0)];


				// Step 6: output range
    double range = 2.0*value/sqrt(static_cast<double>(Nj));
    cout << setw(15) << Nj
	 << setw(15) << 1.0/sqrt(static_cast<double>(Nj))
	 << setw(15) << range
	 << endl;

    out1 << setw(15) << Nj
	 << setw(15) << 1.0/sqrt(static_cast<double>(Nj))
	 << setw(15) << range
	 << endl;

				// Cache result vectors for correlation
				// analysis
    xx.push_back(1.0/sqrt(static_cast<double>(Nj)));
    yy.push_back(range);
    uu.push_back(bj);

  }

  cout << endl;
  out1 << endl;

  /***************************************************
    Correlation analysis
   **************************************************/

  cout << endl 
       << "Coefficient of deviation: " << endl << endl;

  double R2;
  int nvecs = xx.size();
  int nk = nvecs - ngrp + 1;
  int nburn = -1;
  for (int i=0; i<nk; i++) {
    double sxx=0.0, sxy=0.0, syy=0.0, wght;
    double sx=0.0, sy=0.0, sum=0.0;
    
    for (int j=0; j<ngrp; j++) {
      wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
      sum += wght;
      sx += wght*xx[i+j];
      sy += wght*yy[i+j];
    }

    sx /= sum;
    sy /= sum;

    for (int j=0; j<ngrp; j++) {
      wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
      sxy += wght*(xx[i+j] - sx)*(yy[i+j] - sy);
      sxx += wght*(xx[i+j] - sx)*(xx[i+j] - sx);
      syy += wght*(yy[i+j] - sy)*(yy[i+j] - sy);
    }
      
    R2 = sxy*sxy/(sxx*syy);
      
				// Print out number and coefficient
				// of determination, R2

    cout << setw(15) << 1.0/(xx[i]*xx[i])
	 << setw(15) << R2
	 << endl;

    out2 << setw(15) << 1.0/(xx[i]*xx[i])
	 << setw(15) << R2
	 << endl;

				// Set burn in period based on COD
    if (R2>rtol && nburn<0) nburn = (int)( 1.0/(xx[i]*xx[i]) );

  }

  /***************************************************
    Mode and quantiles after burn in
   **************************************************/

  if (nburn<0)
    cout << "No convergence!\n";
  else {
    vector<dvec> qparm(q+1);

    cout << endl 
	 << "Post burn in bounds [N=" << nburn << "]: " << endl << endl;

    for (int i=nburn; i<N; i++) {
      for (int k=0; k<q; k++) qparm[k].push_back(data[i][k]);
      qparm[q].push_back(prob[i]);
    }

    int beg = (int)( (N-nburn+1)*alpha );
    int mid = (int)( (N-nburn+1)*0.5 );
    int end = (int)( (N-nburn+1)*(1.0 - alpha) );

    for (int k=0; k<=q; k++) {
      sort(qparm[k].begin(), qparm[k].end());
      cout 
	<< setw(15) << qparm[k][beg]
	<< setw(15) << qparm[k][mid]
	<< setw(15) << qparm[k][end]
	<< endl;

      out3
	<< setw(15) << qparm[k][beg]
	<< setw(15) << qparm[k][mid]
	<< setw(15) << qparm[k][end]
	<< endl;
    }
  }
}

/*

  Notes:

  Coefficient of Determination: R Squared 
  ---------------------------------------

  COD = R^2

  where R is the Correlation Coefficient. 

  The coefficient of determination indicates how much of the total
  variation in the dependent variable can be accounted for by the
  regression function.  For example, a COD if .70 implies that 70% of
  the variation in y is accounted for by the Regression Equation. Most
  statisticians consider a COD of .7 or higher for a reasonable model.

*/
