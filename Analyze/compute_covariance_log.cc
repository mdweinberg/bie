// -*- C++ -*-

/* 
   Reads in a multilevel output file and computes covariance matrix
   for each level
*/


#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <stdlib.h>


string inputfile("multilevel.dat");
string outputtag("covar");

int
main(int argc, char** argv)
{
  /***************************************************
    Global variables
   ***************************************************/

				// Use this level
  int level = 0;

  bool one_pass = true;

  /***************************************************
    Parse command line
   ***************************************************/

  int c;
  while (1) {
    c = getopt (argc, argv, "i:o:l:12");
    if (c == -1) break;
     
    switch (c)
      {
      case 'l': level = atoi(optarg); break;
      case 'i': inputfile.erase(); inputfile = optarg; break;
      case 'o': outputtag.erase(); outputtag = optarg; break;
      case '1': one_pass = true;  break;
      case '2': one_pass = false; break;
      case '?': 
	string msg = "usage: " + string(argv[0]) + " [options]\n";
	msg += "\t-l int\t\tlevel to analyze\n";
	msg += "\t-i string\t\tinput file\n";
	msg += "\t-o string\t\toutput file tag\n";
	msg += "\t-1 \t\tuse one-pass convariance algorithm\n";
	msg += "\t-2 \t\tuse two-pass convariance algorithm\n";
	cerr << msg; exit(-1);
      }
  }

  /***************************************************
    Read in multilevel data
   **************************************************/

  ifstream in(inputfile.c_str());
  if (!in) {
    cerr << argv[0] << ": error opening <" << inputfile << ">\n";
    exit(-1);
  }

				// Make a vector type (used to define a vector
				// of vectors in STL)
  typedef vector<double> dvec;
				// Make a vector of vectors to hold parameters
  vector<dvec> data;
				// Make a vector to hold posterior probability
  vector<dvec> prob;

  const int linesize=2048;
  char line[linesize];
  int l;
  double v;

				// Read in (and discard) header string
  in.getline(line, linesize);

				// Read in the lines at the desired level
  while (in) {
    in.getline(line, linesize);
    if (!in) continue;

    istringstream istr(line);	// check level
    istr >> l;
    if (l!=level) continue;
    istr >> l;			// dump counter

    dvec pv(3);
    istr >> pv[0];		// read probability value
    istr >> pv[1];		// read likelihood value
    istr >> pv[2];		// read prior prob value

    prob.push_back(pv);

    vector<double> tmp;		// read parameter vector
    while (istr) {
      istr >> v;
      if (!istr) break;
      tmp.push_back(v);
    }
    data.push_back(tmp);
  }

  cout << "Size of prob:    " << prob.size() << endl;
  cout << "Size of data:    " << data.size() << endl;
  if (data.size() > 0)
    cout << "Size of data[0]: " << data[0].size() << endl;
  else
    exit(0);

  /***************************************************
    Initialize output files
   **************************************************/
  string outfile;

  outfile.erase(outfile.begin(), outfile.end());
  outfile = outputtag + ".covar";
  ofstream out1(outfile.c_str());
  if (!out1) {
    cerr << argv[0] << ": error opening <" << outfile << ">\n";
    exit(-1);
  }

  /***************************************************
    Begin computation
   **************************************************/

  int N = prob.size();
  int q = data[0].size();

				// Set up storage for convarianice matrix
  dvec           mean (q, 0.0), pavg(3, 0.0), pvar(3, 0.0);
  vector<dvec>   covar(q, mean);

  if (one_pass) {

    for (int i=0; i<N; i++) {
      for (int k=0; k<3; k++) {
	pavg[k] += prob[i][k];
	pvar[k] += prob[i][k] * prob[i][k];
      }
      for (int k=0; k<q; k++) {
	mean[k] += data[i][k];
	for (int l=0; l<q; l++) covar[k][l] += data[i][k] * data[i][l];
      }
    }

    for (int k=0; k<3; k++) {
      pavg[k] /= N;
      pvar[k] = (pvar[k] - pavg[k]*pavg[k]*N)/(N-1);
    }

    for (int k=0; k<q; k++) mean [k] /= N;
    for (int k=0; k<q; k++) {
      for (int l=0; l<q; l++) 
	covar[k][l] = (covar[k][l] - mean[k]*mean[l]*N)/(N-1);
    }

  } else {

    for (int i=0; i<N; i++) {
      for (int k=0; k<3; k++) pavg[k] += prob[i][k];
      for (int k=0; k<q; k++) mean[k] += data[i][k];
    }

    for (int k=0; k<3; k++) pavg[k] /= N;
    for (int k=0; k<q; k++) mean[k] /= N;

    for (int i=0; i<N; i++) {
      for (int k=0; k<3; k++) {
	double diff = prob[i][k] - pavg[k];
	pvar[k] += diff*diff;
      }
      for (int k=0; k<q; k++) {
	double diff1 = data[i][k] - mean[k];
	for (int l=0; l<q; l++) {
	  double diff2 = data[i][l] - mean[k];
	  covar[k][l] += diff1 * diff2;
	}
      }
    }

    for (int k=0; k<3; k++) pvar[k] /= N-1;

    for (int k=0; k<q; k++)
      for (int l=0; l<q; l++) covar[k][l] /= N-1;
    
  }

    
  /***************************************************
    Output
   **************************************************/
  
  out1 << "Level " << level << endl << endl;
  out1 << setw(5) << "n" << setw(15) 
       << "Mean" << setw(15) << "Stdev:" << endl << endl;
  for (int k=0; k<q; k++) 
    out1 << setw(5) << k 
	 << setw(15) << mean[k] 
	 << setw(15) << sqrt(covar[k][k])
	 << endl;

  out1 << endl;
  
  const std::vector<std::string> labs = {"Prob", "Like", "Prio"};
  for (int k=0; k<3; k++)
    out1 << setw(5) << labs[k]
	 << setw(15) << pavg[k] 
	 << setw(15) << sqrt(pvar[k])
	 << endl;

  out1 << endl;

  out1 << "Covar:" << endl << endl;
  out1 << setw(5) << "n\\n";
  for (int k=0; k<q; k++) 
    out1 << setw(15) << k;
  out1 << endl;

  for (int k=0; k<q; k++) {
    out1 << setw(5) << k;
    for (int l=0; l<q; l++) 
      out1 << setw(15) << covar[k][l];
    out1 << endl;
  }
  cout << endl;

}
