//----------------------------------------------------------------------
//  Bayesian evidence computation using KD-/ORB-tree/2^n-tree
//  quadrature cells 
//
//  [Computes bootstrap or batch confidence limits]
//
//----------------------------------------------------------------------

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cfloat>
#include <vector>
#include <deque>

//----------------------------------------------------------------------

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/program_options.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/random.hpp>

namespace po    = boost::program_options;
namespace ublas = boost::numeric::ublas;

//----------------------------------------------------------------------

#include <mpi.h>

#include <VectorM.h>
#include <linalg.h>
#include <AsciiProgressBar.h>
#include <StateFileCL.H>
#include <KDTree.h>
#include <MLData.h>
#include <TwoNTree.h>
#include <TNTree.h>
#include <VTA.h>

#include "config.h"
#ifdef FPEDEBUG
#include <fenv.h>
#include "fpetrap.h"
#endif

//----------------------------------------------------------------------

string inputfile("multilevel.dat");
string cachefile(".cache.data");

double volumeTrim(size_t& beg, size_t& end, std::deque<BIE::element>& prob, 
		  double edgeL, int maxiter);
double volumeTrim(size_t& beg, size_t& end, std::deque<BIE::element>& prob,
		  int inner, bool debug);

//----------------------------------------------------------------------

int
main(int argc, char** argv)
{
#ifdef FPEDEBUG
  set_fpu_handler();
  feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW ); 
#endif

  //--------------------------------------------------
  // MPI initialization
  //--------------------------------------------------

  int numprocs, myid;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  
  //--------------------------------------------------
  // Parameters
  //--------------------------------------------------
  int level, skip, strd, nboot, keep, ssamp, cut, nlev, SCALE;
  int dim1, dim2, itmax, inner;
  unsigned ignr, Lkts;
  double qeps1, qeps2, ssfac, thold, loffs, edgeL, SCEXP, ffactor;
  double trimV;
  bool debug, range, uniq, logM, cache, diag, dump, measr;
  bool batch, fixbox, fullV, fillV, geomV, NLA, chainlog;
  bool fullT, ignore;
  string ttype, rangeF, outputtag, outfile;

  //--------------------------------------------------
  // Declare the supported options.
  //--------------------------------------------------

  po::options_description desc("Available options");
  desc.add_options()
    ("help,h",										"Produce help message")
    ("version,v",									"Display version info and exit")
    ("level,l", 		po::value<int>(&level)->default_value(0),		"Level to analyze")
    ("nboot,b", 		po::value<int>(&nboot)->default_value(400),		"Number of bootstrap samples")
    ("subsample,n", 		po::value<int>(&ssamp)->default_value(-1),		"Subsample size (-1 means use fraction")
    ("skip,s", 			po::value<int>(&skip)->default_value(-1),		"States to skip (first half if neg)")
    ("fraction,S", 		po::value<double>(&ssfac)->default_value(1.0),		 "Subsample fraction")
    ("stride,j", 		po::value<int>(&strd)->default_value(1),		"Spacing of states or frequency (e.g. 1: use all)")
    ("tree-depth,J", 		po::value<int>(&nlev)->default_value(14),		"Number of levels for 2^n tree")
    ("keep,k", 			po::value<int>(&keep)->default_value(-1),		"States to keep (use all if neg)")
    ("bucket-count,c", 		po::value<int>(&cut)->default_value(32), 		"Number of points per bucket, or number of nearest neighbors for nn-tree")
    ("dim1,1", 			po::value<int>(&dim1)->default_value(0),
     "Dimesion 1 for gnuplot tessellation graphic")
    ("dim2,2", 			po::value<int>(&dim2)->default_value(1),
     "Dimesion 2 for gnuplot tessellation graphic")
    ("knots,N", 		po::value<unsigned>(&Lkts)->default_value(1000),	"Number of Lebesgue integration knots")
    ("tree-error,e", 		po::value<double>(&qeps1)->default_value(0.1586552),	"Quantile for error in tree integral")
    ("bootstrap-error,E", 	po::value<double>(&qeps2)->default_value(0.025),	"Quantile for bootstrap error")
    ("ignore",	 		po::value<unsigned>(&ignr)->default_value(100),		"Number of states to skip before thresholding")
    ("threshold,t", 		po::value<double>(&thold)->default_value(0.1), 		"Likelihood difference threshold")
    ("offset,m", 		po::value<double>(&loffs)->default_value(16.0),		"Likelihood offset from maximum")
    ("transform,T", 		po::value<int>(&SCALE)->default_value(0),		"Transform the parameter space to the double unit interval")
    ("exponent,V", 		po::value<double>(&SCEXP)->default_value(1.0),		"Transformation exponent for PolyScale and TransScale")
    ("remove-edges,Z", 		po::value<double>(&edgeL)->default_value(1),		"Trim the edges of the sample to achieve a smaller, higher prob sample")
    ("new-trim", 		po::value<double>(&trimV)->default_value(1),		"Trim the edges of the sample to achieve a smaller, higher prob sample")
    ("inner",			po::value<int>(&inner)->default_value(0),		"Trim the edges to the sample by number to acheive a higher prob sample")
    ("volume-iter,I",		po::value<int>(&itmax)->default_value(24),		"Number of iterations for edge removal")
    ("tree-type,K", 		po::value<string>(&ttype)->default_value("2^n"),	"The tree type (2^n, KD, or ORB)")
    ("dump,r", 			po::value<bool>(&dump)->default_value(false),		"Dump list of all samples")
    ("diag,d", 			po::value<bool>(&diag)->default_value(false),		"Diagnostic output for 2^n tree cells and volume")
    ("chain-log",		po::value<bool>(&chainlog)->default_value(false),	"Read BIE chain logs")
    ("log,L", 		po::value<bool>(&logM)->default_value(true),		"Use log rather than linear scaling for Lebesgue integral summation")
    ("measure,M", 		po::value<bool>(&measr)->default_value(false),		"Print mesure diagnostics")
    ("range,R", 		po::value<bool>(&range)->default_value(true),		"Use range (true) or dispersion (false) width for selecting the kd-tree pivot")
    ("full,F", 		po::value<bool>(&fullV)->default_value(false),			"Use the full cell volume rather than volume enclosing the points")
    ("fill,f", 		po::value<bool>(&fillV)->default_value(false),			"Use the filling factor heuristic to select full cell volume or volume enclosing the points")
    ("complete,P", 		po::value<bool>(&fullT)->default_value(false),			"Fill the 2^N tree with volume using minimum P from parent cells")
    ("fill-factor", 		po::value<double>(&ffactor)->default_value(0.5),	"Filling factor for heuristic volume switch")
    ("geomean,G", 		po::value<bool>(&geomV)->default_value(false),		"Use the gemetric mean of the full cell volume and the volume enclosing the points")
    ("unique,U", 		po::value<bool>(&uniq)->default_value(true),		"Suppress unique state accumulation for VTA")
    ("debug-output,x", 		po::value<bool>(&debug)->default_value(false),		"Verbose debugging output")
    ("use-cache", 		po::value<bool>(&cache)->default_value(true),		"Use the binary cache file")
    ("compute-box,X", 		po::value<bool>(&fixbox)->default_value(false),		"Compute the data range from the full data sample")
    ("batch,C", 		po::value<bool>(&batch)->default_value(false),		"Use batch rather than bootstrap")
    ("NLA", 			po::value<bool>(&NLA)->default_value(false),		"Compute the marginal likelihood using the NLA")
    ("range-file", 		po::value<string>(&rangeF),				"File containing min max range for each dimension")
    ("output-file,O", 		po::value<string>(&outfile),				"Log file for marginal likelihood output")
    ("mix-ignore", 		po::value<bool>(&ignore)->default_value(true),		"Ignore a mixture weight for independence")
    ("zero-edge,z",									"Use zero as the smallest probability value if the parent is an edge cell")
    ("quiet,q",										"Suppress statefile diagnostic output")
    ("input,i", 		po::value<string>(&inputfile)->default_value("bie.statelog"),	"Input file")
    ("output,o", 		po::value<string>(&outputtag)->default_value("marglike"),	"Output prefix")
    ;

  po::variables_map vm;

  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);    
  } catch(boost::program_options::error& e){
    std::cerr << "Invalid_option_value exception thrown parsing config file:"
	      << std::endl << e.what() << std::endl;
    MPI_Finalize();
    return 2;
  } catch(std::exception e){
    std::cerr <<"Exception thrown parsing config file:" 
	      << std::endl << e.what() << std::endl;
    MPI_Finalize();
    return 2;
  }

  if (vm.count("help")) {
    if (myid==0) {
      std::cout << std::setfill('-') << setw(76) << '-' 
		<< std::endl << std::setfill(' ')
		<< "Bayesian marginal likelihood computation using the NLA "
		<< "or VTA algorithms." << std::endl
		<< "The VTA uses kd-tree, ORB-tree or 2^n-tree"
		<< "quadrature cells." << std::endl
		<< "[This version computes bootstrap or batch confidence limits]" 
		<< std::endl
		<< std::setfill('-') << setw(76) << '-' 
		<< std::endl << std::setfill(' ')
		<< "Usage: " << argv[0] << " [options]" << std::endl
		<< std::setfill('-') << setw(76) << '-' 
		<< std::endl << std::setfill(' ')
		<< desc << std::endl;
    }
    MPI_Finalize();
    return 1;
  }

  if (trimV < 1.0 && fullT) {
    std::cerr << setw(80) << setfill('*') << '*' << std::endl;
    std::cerr << "Using the new trimming algorithm with cell completion is probably a bad idea." << std::endl;
    std::cerr << "The two algorithms interfere with each other, but here you go . . . " << std::endl;
    std::cerr << setw(80) << setfill('*') << '*' << std::endl << setfill(' ');
  }

  if (vm.count("version")) {
    if (myid==0) std::cout << argv[0] << " " << PACKAGE_VERSION 
			   << " version " << VERSION << std::endl;
    MPI_Finalize();
    return 1;
  }

  bool zedge = false;
  if (vm.count("zero-edge")) zedge = true;

  bool quiet = false;
  if (vm.count("quiet") or outfile.size()>0) quiet = true;
  
  nboot = std::max<int>(nboot,1); // Sanity

  //--------------------------------------------------
  // Begin
  //--------------------------------------------------

  bool KD  = false;
  bool ORB = false;
  bool TNT = true;
  
  if (chainlog) batch = false;

  if (ttype.compare("kd") == 0 || ttype.compare("KD") == 0) {
    KD  = true;
    TNT = false;
  } else if (ttype.compare("orb") == 0 || ttype.compare("ORB") == 0) {
    ORB = true;
    TNT = false;
  }
  
  if (myid==0 && !quiet) {
    if (KD)
      cout << "[Using KD";
    else if (ORB)
      cout << "[Using ORB";
    else
      cout << "[Using 2^n";

    if (batch)
      cout << " tree with batch confidence limits, MPI enabled";
    else
      cout << " tree with bootstrap confidence limits, MPI enabled";

    switch (SCALE) {
    case 1:
      cout << ", polynomial scaling with exponent=" << SCEXP;
      break;
    case 2:
      cout << ", exponential scaling";
      break;
    case 3:
      cout << ", smoothed pulse scaling with exponent=" << SCEXP;
      break;
    case 0:
    default:
      cout << ", no scaling";
      break;
    }
    
    cout << "]" << endl;
  }

  BSPTree ::full_volume = fullV;
  BSPTree ::fill_volume = fillV;
  BSPTree ::geom_volume = geomV;
  BSPTree ::fill_factor = ffactor;
  BSPTree ::lower_quant = qeps1;
  BSPTree ::upper_quant = 1.0 - qeps1;
  BSPTree ::dim1        = dim1;
  BSPTree ::dim2        = dim2;
  KDTree  ::use_range   = range;
  TwoNTree::diag        = diag;
  TwoNTree::full        = fullT;
  TwoNTree::zedge       = zedge;

  //--------------------------------------------------
  // Get StateFile
  //--------------------------------------------------

  string cache_file;
  if (cache) cache_file = cachefile + "@" + inputfile;

  bool sf_quiet = quiet;	// Suppress statefile diagnostics from
  if (myid>0) sf_quiet = true;	// all but root node

  boost::shared_ptr<StateFile> sf;
    
  if (chainlog)
    sf = boost::shared_ptr<StateFile>(new StateFileCL(inputfile, cache_file, rangeF,
						      level, skip, strd, keep, ignore, sf_quiet));
  else
    sf = boost::shared_ptr<StateFile>(new StateFile(inputfile, cache_file, rangeF,
						    level, skip, strd, keep, ignore, sf_quiet));
  //--------------------------------------------------
  // Compute enclosing data range
  //--------------------------------------------------

  unsigned dim = sf->prob.front().point.size();

  if (rangeF.size()>0) {

    VTA::lowerBound = std::vector<double>(dim);
    VTA::upperBound = std::vector<double>(dim);

    ifstream in(rangeF.c_str());
    if (in) {
      for (unsigned d=0; d<dim; d++) {
	in >> VTA::lowerBound[d];
	in >> VTA::upperBound[d];
	if (!in) break;
      }
    } 
    if (!in) {
      if (myid==0)
	std::cerr << "Error reading <" << rangeF << ">" << std::endl;
      MPI_Finalize();
      return 1;
    }

  } else if (fixbox) {

    VTA::lowerBound = std::vector<double>(dim,  DBL_MAX);
    VTA::upperBound = std::vector<double>(dim, -DBL_MAX);

    std::deque<BIE::element>::iterator it=sf->prob.begin();
    while (it!=sf->prob.end()) {
      for (unsigned i=0; i<dim; i++) {
	VTA::lowerBound[i] = 
	  std::min<double>(VTA::lowerBound[i], it->point[i]);
	VTA::upperBound[i] = 
	  std::max<double>(VTA::upperBound[i], it->point[i]);
      }
      it++;
    }
  }

  //--------------------------------------------------
  // Sanity check
  //--------------------------------------------------

  if (sf->prob.size()<3) {
    cout << "There are two or fewer points, exiting . . ." << endl;
    MPI_Finalize();
    return(-1);
  }

  //--------------------------------------------------
  // Dump first half
  //--------------------------------------------------

  if (skip<0) {
    long cp = sf->prob.size()/2 - sf->popped/2;
    if (cp>0) sf->prob.erase(sf->prob.begin(), sf->prob.begin() + cp);
  }

  //--------------------------------------------------
  // Bootstrap loop
  //--------------------------------------------------

  vector<unsigned> numerr(2, 0);

  VTA::useMPI = true;
  VTA::treMPI = true;
  VTA::useORB = ORB;
  VTA::useTNT = TNT;
  VTA::TNTlev = nlev;

  VTA vt(SCALE, uniq, SCEXP), nl(SCALE, uniq, SCEXP);

  vt.setFile(outputtag);
  vt.Measure(measr);
  vt.setDBG (debug);
  nl.setDBG (debug);
				// Boost random # stuff for resampling
  boost::mt19937 rng (11+myid);
  boost::uniform_int<> ns(0, sf->prob.size()-1);
  boost::variate_generator<boost::mt19937, boost::uniform_int<> > nsamp(rng, ns);
  
  unsigned fileCtr = 0;

  if (batch) {
				// Assign number batches based on ssamp
    if (ssamp>0 && sf->prob.size()/ssamp>0)
      nboot = min<int>(sf->prob.size()/ssamp, nboot);
    else			// Assign ssamp based on the number of batches
      ssamp = sf->prob.size()/nboot;
  }
				// This structure will hold the
				// intermediate results per process
  MLData ml(qeps1, qeps2, sf->temps);
  if (inner>0) ml.subSample(true, inner, 0);

  ml.nsample = ssamp;
  if (!batch && ssamp<=0)
    ml.nsample = min<unsigned>(floor(ssfac*sf->prob.size()+0.5), sf->prob.size());

				// So we can see the progress . . .
				// 
  if (myid==0 && !quiet) AsciiProgressBar(cout, 0, nboot, 2);

				// This is the main bootstrap sampling loop
				// 
  for (int nb=0; nb<nboot; nb++) {
    
    if ( nb % numprocs != myid) continue;
    
    std::deque<BIE::element> prob;
				
    if (batch) {		// Partition
      for (unsigned i=0; i<ml.nsample; i++) 
	prob.push_back(sf->prob[ml.nsample*nb+i]);
    } else {			// Compute sample with replacment
      for (unsigned i=0; i<ml.nsample; i++) 
	prob.push_back(sf->prob[nsamp()]);
    }
    

    //--------------------------------------------------
    // Compute the harmonic mean approximation
    //--------------------------------------------------

    double l1st=prob.back().pval[1];

    typedef pair<double, double> dpair;
    typedef map<double, dpair>   kappaMap;

    kappaMap::iterator kit;
    kappaMap kappa;

    for (unsigned t=0; t<prob.size(); t++) {
      if ( (kit=kappa.find(prob[t].beta)) == kappa.end()) {
	kappa[prob[t].beta] = 
	  dpair(exp((1.0 - prob[t].beta)*prob[t].pval[2] + 
		    prob[t].beta*(l1st-prob[t].pval[1])), 1);
      } else {
	kit->second.first  += exp((1.0 - kit->first)*prob[t].pval[2] + 
				  kit->first*(l1st-prob[t].pval[1]));
	kit->second.second += 1;
      }
    }
    
    double Nt = 0.0;
    double Hm = 0.0;
    for (kit=kappa.begin(); kit!=kappa.end(); kit++) {
      double N = kit->second.second;
      Nt += N;
      Hm += N*N * exp(kit->first*l1st) / kit->second.first;
    }
    Hm = log(Hm/Nt);


    //--------------------------------------------------
    // Compute Laplace approximation
    //--------------------------------------------------

    double Lm=0.0;

				// Sort the list by posterior value
    sort(prob.begin(), prob.end(), compare_posterior);

    double fac = 1.0/prob.size();

    MatrixM disp(0, dim-1, 0, dim-1);
    VectorM mean(0, dim-1);

    disp.zero();
    mean.zero();

    std::deque<BIE::element>::iterator itp;
    for (itp=prob.begin(); itp!=prob.end(); itp++) {
      for (unsigned i=0; i<dim; i++) mean[i] += itp->point[i] * fac;
    }

    for (itp=prob.begin(); itp!=prob.end(); itp++) {
      for (unsigned i=0; i<dim; i++) {
	for (unsigned j=0; j<dim; j++) 
	  disp[i][j] += 
	    (itp->point[i] - mean[i]) *
	    (itp->point[j] - mean[j]) * fac;
      }
    }
    
    //--------------------------------------------------
    // Trim the points for VTA
    //--------------------------------------------------

    size_t beg_count, fin_count;

    double Vfac = 1.0;

    if (inner>0) Vfac = volumeTrim(beg_count, fin_count, prob, inner, debug);
    else         Vfac = volumeTrim(beg_count, fin_count, prob, edgeL, itmax);

    double lVfac = log(Vfac);
    
    double Vfac_vt  = Vfac;
    double lVfac_vt = lVfac;

    double Vfac_nl  = Vfac;
    double lVfac_nl = lVfac;

    std::sort(prob.begin(), prob.end(), compare_likelihood);
    double Lrat = prob.back().pval[1] - prob.front().pval[1];

    try {
      vt.compute(cut, Lkts, logM, trimV, prob.begin(), prob.end());
      if (trimV<1.0) {
	Vfac_vt  = 1.0/vt.getFraction();
	lVfac_vt = log(Vfac_vt);
      }
    }
    catch (string error) {
      if (myid==0) {
	numerr[0]++;
	if (numerr[0]<=10)
	  cout << endl << "VTA [#" << numerr[0] << "]: " 
	       << error << endl;
	if (numerr[0]==10)
	  cout << "Suppressing any further messages about this" << endl;
      }
    }


    if (debug) 
      cout << endl
	   << "Volume trim: [ini=" << beg_count << ", fin=" << fin_count
	   << ", fac=" << Vfac_vt << ", loff=" << lVfac_vt << "]"
	   << endl << endl;


    double Ht, Lt;
    std::vector<double> L, X;

    if (NLA) {

      //--------------------------------------------------
      // Compute unnormalized posterior value
      //--------------------------------------------------

				// Sort the list by likelihood value
				//
      std::sort(prob.begin(), prob.end(), compare_likelihood);
	
      //--------------------------------------------------
      // Look for threshold value
      //--------------------------------------------------
    
      std::deque<BIE::element>::reverse_iterator rit0, rit1;
      unsigned ncnt=0;
      
      rit0 = prob.rbegin();
      ++(rit1 = rit0);
      double lmax = rit0->pval[1];
      for (; rit1!=prob.rend(); rit1++, rit0++) {
	if (ncnt>ignr) {
	  if (rit0->pval[1] - rit1->pval[1] > thold) break;
	  if (lmax - rit1->pval[1] > loffs) break;
	}
	ncnt++;
      }
      
      if (ncnt<1) {			// Need at least one interval . . .
	cerr << "Fewer than 2 points for threshold=" << thold 
	     << " and L offset=" << loffs << endl;
	MPI_Finalize();
	return -1;
      }
      
      //--------------------------------------------------
      // Compute prior measure
      //--------------------------------------------------
      
      std::deque<BIE::element>::iterator it=rit0.base();
      
      //--------------------------------------------------
      // Compute the integrated prior for trimmed dist
      //--------------------------------------------------
      try {
	nl.compute(cut, Lkts, logM, trimV, it, prob.end());
	if (trimV<1.0) {
	  Vfac_nl  = 1.0/nl.getFraction();
	  lVfac_nl = log(Vfac_nl);
	}
      }
      catch (string error) {
	if (myid==0) {
	  numerr[1]++;
	  if (numerr[1]<=10)
	    cout << endl << "Trimmed NLA [#" << numerr[1] << "]: " 
		 << error << endl;
	  if (numerr[1]==10)
	    cout << "Suppressing any further messages about this" << endl;
	}
      }
      

      //--------------------------------------------------
      // Normalization for NLA
      //--------------------------------------------------
      
      unsigned x = 1;
      
      for (; it!=prob.end(); it++) {
	X.push_back(x++);
	L.push_back(it->pval[1]);
      }
      
      double norm = X.back();
      for (unsigned t=0; t<X.size(); t++) X[t] = X[t]/norm;
    
      //--------------------------------------------------
      // Compute Laplace approximation for trimmed dist
      //--------------------------------------------------
      
				// Trim the list to values below threshold
      prob.erase(prob.begin(), rit0.base());

      fac = 1.0/prob.size();


      MatrixM disp(0, dim-1, 0, dim-1);
      VectorM mean(0, dim-1);

      disp.zero();
      mean.zero();
      
      std::deque<BIE::element>::iterator itp;
      for (itp=prob.begin(); itp!=prob.end(); itp++) {
	for (unsigned i=0; i<dim; i++) mean[i] += itp->point[i] * fac;
      }
      
      for (itp=prob.begin(); itp!=prob.end(); itp++) {
	for (unsigned i=0; i<dim; i++) {
	  for (unsigned j=0; j<dim; j++) 
	    disp[i][j] += 
	      (itp->point[i] - mean[i]) *
	      (itp->point[j] - mean[j]) * fac;
	}
      }
      
      Lt = 0.5*dim*log(2.0*M_PI) + 
	0.5*log(fabs(determinant(disp))) + prob.back().pval[0];


      //--------------------------------------------------
      // Compute harmonic mean for trimmed sample
      //--------------------------------------------------
      
      kappa.erase(kappa.begin(), kappa.end());
      lmax = prob.back().pval[1];
      
      for (unsigned t=0; t<prob.size(); t++) {
	if ( (kit=kappa.find(prob[t].beta)) == kappa.end()) {
	  kappa[prob[t].beta] = 
	    dpair(exp((1.0 - prob[t].beta)*prob[t].pval[2] + 
		      prob[t].beta*(lmax-prob[t].pval[1])), 1);
	} else {
	  kit->second.first  += exp((1.0 - prob[t].beta)*prob[t].pval[2] + 
				    prob[t].beta*(lmax-prob[t].pval[1]));
	  kit->second.second += 1;
	}
      }
      
      Nt = 0.0;
      Ht = 0.0;
      for (kit=kappa.begin(); kit!=kappa.end(); kit++) {
	double N = kit->second.second;
	Nt += N;
	Ht += N*N * exp(kit->first*l1st) / kit->second.first;
      }
      Ht = log(Nt/Nt);
    }
    
    //--------------------------------------------------
    // Results
    //--------------------------------------------------

    if (NLA) {

      ofstream out;
      if (measr && outputtag.size()) {
	ostringstream file;
	file << outputtag << ".NLA.";
	if (numprocs>1) file << myid << ".";
	file << fileCtr;

	out.open(file.str().c_str());
	out << "# " << right << setw(14) << "Y "
	    << "| " << setw(14) << "Measr [mean] "
	    << "| " << setw(14) << "Measr [lower] "
	    << "| " << setw(14) << "Measr [upper] "
	    << "| " << setw(14) << "K(<Y) [mean] "
	    << "| " << setw(14) << "K(<Y) [lower] "
	    << "| " << setw(14) << "K(<Y) [upper] "
	    << endl << setfill('-')
	    << "# " << setw(14) << '-'
	    << "| " << setw(14) << '-'
	    << "| " << setw(14) << '-'
	    << "| " << setw(14) << '-'
	    << "| " << setw(14) << '-'
	    << "| " << setw(14) << '-'
	    << "| " << setw(14) << '-'
	    << endl << setfill(' ');
      }

      double Lmax = L.back();
      double Ml=1.0, Mh=1.0, Mm;
      for (unsigned t=L.size()-1; t>0; t--) {
	Ml += exp(Lmax - L[t-1]) * (1.0 - exp(L[t-1] - L[t])) * X[t  ];
	Mh += exp(Lmax - L[t-1]) * (1.0 - exp(L[t-1] - L[t])) * X[t-1];
	if (out)  out << setw(16) << Lmax - 0.5*(L[t-1] + L[t])
		      << setw(16) << 0.5*(X[t-1] + X[t])
		      << setw(16) << X[t-1]
		      << setw(16) << X[t]
		      << setw(16) << 0.5*(Ml + Mh)
		      << setw(16) << Ml
		      << setw(16) << Mh
		      << endl;
      }
      
      Mm = Lmax - log(0.5*(Ml + Mh));
      Ml = Lmax - log(Ml);
      Mh = Lmax - log(Mh);
    
				// Store the results
      for (unsigned l=0; l<2; l++) {
	ml.AlNL1[l].push_back(Mm + nl.reslt[l][2] + lVfac_nl);
	ml.AlNL2[l].push_back(Mm + nl.Rmean[l] + lVfac_nl   );
	ml.Prio1[l].push_back(nl.reslt[l][2]);
	ml.Prio2[l].push_back(nl.Rmean[l]);
      }

      ml.Htrim.push_back(Ht + lVfac_nl);
      ml.Ltrim.push_back(Lt + lVfac_nl);

      ml.Used. push_back(L.size());
    }

    for (unsigned l=0; l<2; l++) {
      ml.AlVT1[l].push_back(vt.reslt[l][0] + lVfac_vt);
      ml.Pr1Lo[l].push_back(vt.lower[l][0] + lVfac_vt);
      ml.Pr1Hi[l].push_back(vt.upper[l][0] + lVfac_vt);
      ml.Pmean[l].push_back(vt.vmean[l][0] + lVfac_vt);
      ml.Volm1[l].push_back(vt.reslt[l][3]);

      ml.AlVT2[l].push_back(vt.Lmean  [l] + lVfac_vt);
      ml.Pr2Lo[l].push_back(vt.Llower [l] + lVfac_vt);
      ml.Pr2Hi[l].push_back(vt.Lupper [l] + lVfac_vt);
      ml.Volm2[l].push_back(vt.Yvolume[l]);
    }
      
    ml.VolEP.push_back(vt.Xvolume);
    ml.Vrat .push_back(Vfac);
    ml.Lrat .push_back(Lrat);
    ml.Nmean.push_back(0.0);
    ml.Hmean.push_back(Hm + lVfac_vt);
    ml.Lplce.push_back(Lm + lVfac_vt);

    ml.Size. push_back(prob.size());

    if (myid==0 && !quiet) AsciiProgressBar(cout, nb, nboot, 2);
  }
  
  if (myid==0 && !quiet) {
    AsciiProgressBar(cout, nboot, nboot, 2);
    cout << endl;
    cout << "Gathering data . . . " << flush;
  }

  ml.share_data();

  if (myid==0) {
    if (!quiet) cout << "done" << endl;

    //
    // Dump sample and stats
    //
    if (dump) ml.dump_sample(outputtag);

    ml.print_stats(keep, cut, thold, loffs, skip, outfile);
  }

  if (TNT) TwoNTree::diagDump(MPI_COMM_WORLD, nlev);

  MPI_Finalize();
  
  return (0);
}


double volumeTrim(size_t& beg, size_t& end,
		  std::deque<BIE::element>& prob, double edgeL, int maxiter)
{
				// No trimming for edgeL=1 (full
				// volume)
  if (edgeL>=1.0) return 1.0;

  beg = prob.size();
  size_t dim = prob.front().point.size(), i;

				// Compute the enclosing hypercube
  vector<double> dmin(dim,  DBL_MAX);
  vector<double> dmax(dim, -DBL_MAX);

  for (std::deque<BIE::element>::iterator it=prob.begin(); it!=prob.end(); it++) {
    for (i=0; i<dim; i++) {
      dmin[i] = min<double>(dmin[i], it->point[i]);
      dmax[i] = max<double>(dmax[i], it->point[i]);
    }
  }

  std::deque<BIE::element> newp;

  if (maxiter) {
    
    vector<double> dwid;
    for (i=0; i<dim; i++) dwid.push_back(dmax[i] - dmin[i]);
    
    double dfracL = 0.0, fracL = 0.0;
    double dfracH = 0.5, fracH = 1.0;
    double dfrac;

    for (int n=0; n<maxiter; n++) {

      dfrac = 0.5*(dfracL + dfracH);
      unsigned cnt = 0;

      for (std::deque<BIE::element>::iterator it=prob.begin(); it!=prob.end(); it++) {
	for (i=0; i<dim; i++) {
	  if (it->point[i] < dmin[i]+dfrac*dwid[i] || 
	      it->point[i] > dmax[i]-dfrac*dwid[i]) break;
	}
	if (i==dim) cnt++;
      }

      double frac = static_cast<double>(cnt)/beg;
      if (frac > edgeL) {
	dfracL = dfrac;
	fracL  = frac;
      } else {
	dfracH = dfrac;
	fracH  = frac;
      }

      if (fabs(fracH - fracL) < 1.5/beg) break;

    }
    
				// Make a new list of inner volume
				// subsample
    dfrac = 0.5*(dfracL + dfracH);
    for (std::deque<BIE::element>::iterator it=prob.begin(); it!=prob.end(); it++) {
      for (i=0; i<dim; i++) {
	if (it->point[i] < dmin[i]+dfrac*dwid[i] || 
	    it->point[i] > dmax[i]-dfrac*dwid[i]) break;
      }
      if (i==dim) newp.push_back(*it);
    }
    
  } else {
				// Find the new cube side lengths by
				// scaling the original values to
				// obtain the desired volume fraction
    double dfrac = 1.0 - pow(edgeL, 1.0/dim);
    for (i=0; i<dim; i++) {
      double delt = 0.5*dfrac*(dmax[i] - dmin[i]);
      dmin[i] += delt;
      dmax[i] -= delt;
    }
				// Make a new list of inner volume
				// subsample
    std::deque<BIE::element> newp;
    for (std::deque<BIE::element>::iterator it=prob.begin(); it!=prob.end(); it++) {
      for (i=0; i<dim; i++) {
	if (it->point[i] < dmin[i] || 
	    it->point[i] > dmax[i]) break;
      }
      if (i==dim) newp.push_back(*it);
    }
  }

  beg = prob.size();
  end = newp.size();
				// Returned the trimmed data in the
				// original deque
  prob = newp;

				// The normalization correction
  return static_cast<double>(beg)/end;
}

double volumeTrim(size_t& beg, size_t& end, std::deque<BIE::element>& prob,
		  int inner, bool debug)
{
  //--------------------------------------------------
  // Compute enclosing data range
  //--------------------------------------------------

  unsigned dim = prob.front().point.size();

  std::vector<double> lowerBound0(dim,  DBL_MAX);
  std::vector<double> upperBound0(dim, -DBL_MAX);

  std::vector<double> lowerBound1(dim,  DBL_MAX);
  std::vector<double> upperBound1(dim, -DBL_MAX);

  std::vector<double> scale0(dim), scale1(dim);

  for (auto v : prob) {
    for (unsigned i=0; i<dim; i++) {
      lowerBound0[i] = std::min<double>(lowerBound0[i], v.point[i]);
      upperBound0[i] = std::max<double>(upperBound0[i], v.point[i]);
    }
  }

  double initvol = 1.0;
  for (unsigned i=0; i<dim; i++) {
    scale0[i] = upperBound0[i] - lowerBound0[i];
    initvol *= scale0[i];
  }

  //--------------------------------------------------
  // Sort the list by posterior value
  //--------------------------------------------------

  std::sort(prob.begin(), prob.end(), compare_posterior);

  //--------------------------------------------------
  // Compute the L1 distance
  //--------------------------------------------------

  std::vector<double> center = prob.back().point;

  typedef std::pair<double, size_t> iKey;
  std::vector<iKey> expfct0, expfct1;

  {
    size_t i = 0;
    for (auto v : prob) {
      double L1 = 0.0;
      for (unsigned j=0; j<dim; j++) {
	L1 = std::max<double>(L1, fabs(v.point[j] - center[j])/scale0[j]);
      }
      expfct0.push_back(iKey(L1, i++));
    }
  }

  //--------------------------------------------------
  // Find the position
  //--------------------------------------------------

  std::sort(expfct0.begin(), expfct0.end());

  // Strip leading zero entries (probably due to stuck chain)
  //
  std::vector<iKey>::iterator vK = expfct0.begin();
  while (vK != expfct0.end()) {
    if (vK->first==0.0) vK++;
    else break;
  }
  if (vK != expfct0.end()) expfct0.erase(expfct0.begin(), vK);
    
  const double minfrac = 0.5;

  unsigned partn = 
    std::min<unsigned>
    (inner, static_cast<unsigned>(floor(minfrac*expfct0.size()+0.5)));
		       

  //--------------------------------------------------
  // Compute the variance
  //--------------------------------------------------
  
  std::vector<double> var(dim, 0.0);
  for (unsigned i=0; i<partn; i++) {
    size_t k = expfct0[i].second;
    for (unsigned j=0; j<dim; j++) {
      double tmp = prob[k].point[j] - center[j];
      var[j] += tmp*tmp;
    }
  }
	
  // New scale
  //
  for (unsigned j=0; j<dim; j++) scale1[j] = sqrt(var[j]/partn);
  
  
  //--------------------------------------------------
  // Recompute the new scaled L1 distance
  //--------------------------------------------------

  {
    size_t i = 0;
    for (auto v : prob) {
      double L1 = 0.0;
      for (unsigned j=0; j<dim; j++) {
	L1 = std::max<double>(L1, fabs(v.point[j] - center[j])/scale1[j]);
      }
      expfct1.push_back(iKey(L1, i++));
    }
  }
  
  //--------------------------------------------------
  // Resort and find the position in the list
  //--------------------------------------------------

  std::sort(expfct1.begin(), expfct1.end());

  partn = std::min<unsigned>
    (inner, static_cast<unsigned>(floor(minfrac*expfct1.size()+0.5)));

  // Compute the bounds from the new sorted vector of parameters
  //
  for (unsigned i=0; i<partn; i++) {
    size_t k  = expfct1[i].second;
    for (unsigned j=0; j<dim; j++) {
      double val = prob[k].point[j];
      lowerBound1[j] = std::min<double>(lowerBound1[j], val);
      upperBound1[j] = std::max<double>(upperBound1[j], val);
    }
  }
  
  double volume = 1.0;
  for (unsigned j=0; j<dim; j++) volume *= scale1[j];

  std::deque<element> newp;

  beg = prob.size();

  double minLike = DBL_MAX, maxLike = -DBL_MAX;

  for (auto v : prob) {
    bool okay = true;
    for (unsigned j=0; j<dim; j++) {
      double z = v.point[j];
      if (z < lowerBound1[j] or z > upperBound1[j]) okay = false;
    }
    if (okay) {
      minLike = std::min<double>(minLike, v.pval[1]);
      maxLike = std::max<double>(maxLike, v.pval[1]);
      newp.push_back(v);
    }
  }

  prob = newp;

  end = prob.size();


  if (myid==0 and debug) {

    double likeRat = maxLike - minLike;

    //--------------------------------------------------
    // Print the box
    //--------------------------------------------------

    std::cout << right
	      << std::setw(5)  << "Dim"
	      << std::setw(18) << "Left"
	      << std::setw(18) << "Center"
	      << std::setw(18) << "Right"
	      << std::setw(18) << "Scale"
	      << std::endl
	      << std::setw(5)  << "----"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::endl;
    
    
    for (unsigned j=0; j<dim; j++)
      std::cout << std::setw(5) << j+1
		<< std::setw(18) << lowerBound1[j]
		<< std::setw(18) << center[j]
		<< std::setw(18) << upperBound1[j]
		<< std::setw(18) << scale1[j]
		<< std::endl;
    
    std::cout << std::endl
	      << std::string(5+18*4, '-')         << std::endl
	      << "Init size : " << prob.size()    << std::endl
	      << "Final size: " << expfct1.size() << std::endl
	      << "Init vol  : " << initvol        << std::endl
	      << "Final vol : " << volume         << std::endl
	      << "Min like  : " << minLike        << std::endl
	      << "Max like  : " << maxLike        << std::endl
	      << "Like ratio: " << likeRat        << std::endl
	      << std::string(5+18*4, '-')         << std::endl;
  }
    
  return static_cast<double>(beg)/end;
}

