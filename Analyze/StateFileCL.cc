
#include <StateFileCL.H>

#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <set>
				// For file stat
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;


void StateFileCL::ReadData()
{
  vector<double> minV, maxV;
  if (limit.size()) {
    if (!quiet) {
      cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
      cout << "Reading the limit file <" << limit << "> . . . " << flush;
    }


    ifstream fin(limit.c_str());
    if (!fin) {
      cerr << "StateFileCL: error opening <" << limit << ">\n";
      exit(-1);
    }

    while (fin.good()) {
      double x, y;
      fin >> x;
      fin >> y;
      if (fin.good()) {
	minV.push_back(x);
	maxV.push_back(y);
      }
    }
    if (!quiet) {
      cout << "dim=" << minV.size() << ", done" << endl;
    }
  }

  const unsigned filemax = 512;
  std::set<double> betas;
  cnt = 0;
  for (unsigned fileno=0; fileno<filemax; fileno++) {

    ostringstream name;
    name << file << "." << fileno;

    ifstream in(name.str().c_str());
    if (!in) break;

    if (!quiet) {
      if (fileno==0)
	cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
      cout << "Reading the input file <" << name.str() << "> . . . " << flush;
    }

    const int linesize=2048;
    char line[linesize];
    vector<double> pvals(4, 1.0);
    double dd;
    bool firsttime = true;

    int indx, ptype;
    unsigned ntot;
    double beta, factor, wght;

				// Read in and parse header string
    // Indx
    {
      in.getline(line, linesize);
      istringstream sin(line);
      sin >> indx;
    }
    // Beta
    {
      in.getline(line, linesize);
      istringstream sin(line);
      sin >> beta;
    }
    // Factor
    {
      in.getline(line, linesize);
      istringstream sin(line);
      sin >> factor;
    }
    // Weight
    {
      in.getline(line, linesize);
      istringstream sin(line);
      sin >> wght;
    }
    // Ptype
    {
      in.getline(line, linesize);
      istringstream sin(line);
      sin >> ptype;
    }
    // Ntot
    {
      in.getline(line, linesize);
      istringstream sin(line);
      sin >> ntot;
    }
    
    betas.insert(beta);

    int l;
    int icnt = 0;
    while (in) {
      in.getline(line, linesize);
      if (!in) continue;
    
      if (icnt++<skip) continue;
      if (icnt % strd) continue;
      
      istringstream istr(line);
      element elem;
      
      istr >> l;		// dump counter
      istr >> pvals[0];		// read prob level
      istr >> pvals[1];		// read likelihood
      istr >> pvals[2];		// read prior
      
				// correct by beta factor
      pvals[0] /= beta;

      elem.pval   = pvals;
      elem.weight = 1.0;
      elem.beta   = beta;
      
      ndim = 0;
      
      while (istr.good()) {
	istr >> dd;
	elem.point.push_back(dd);
	ndim++;
      }
      
      bool use = true;
      if (static_cast<size_t>(ndim) == minV.size()) {
	for (int i=0; i<ndim; i++) {
	  if (elem.point[i] < minV[i] || elem.point[i] > maxV[i]) use = false;
	}
      }

      if (use) {
	prob.push_back(elem);
	if (keep>0 && static_cast<int>(prob.size())>keep) {
	  prob.pop_front();
	  popped++;
	}
      }
    
      if (firsttime) {
	ndim0 = ndim;
	firsttime = false;
      } else {
	if (ndim!=ndim0) {
	  cout << "Dimension count inconsistent! [" << ndim << " != " << ndim0
	       << "] line=" << icnt << endl;
	}
      }
      
    }

    if (!quiet) {
      cout << "done" << endl;
    }

    cnt += icnt;
  }
  
  if (!prob.size()) {
    cout << "No states!" << endl;
    exit(-1);
  }

  if (!quiet) cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');

  temps = betas.size();
}

