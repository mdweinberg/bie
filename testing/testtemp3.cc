#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

using namespace std;

#include <BIEmpi.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <TemperedSimulation.h>
#include <LikelihoodComputationMPI.h>
#include <CountConverge.h>
#include <MappedGrid.h>
#include <Histogram1D.h>
#include <Model.h>
#include <InitialMixturePrior.h>
#include <SimpleStat.h>
#include <EnsembleStat.h>
#include <SimpleGalaxyModel.h>
#include <AdaptiveLegeIntegration.h>
#include <QuadGrid.h>
#include <DataTree.h>
#include <RecordStream_Ascii.h>
#include <SquareTile.h>
#include <MetropolisHastings.h>


using namespace BIE;

string datafile("data.dat");

/**
   @name testtemp3: main
   Does full simulation at one level of resolution (so far).  Parallel
   MPI implementation.
*/
int
main(int argc, char** argv)
{
  bool mstat   = false;
  int minmc    = 2;
  double dX    = 0.2;
  double dY    = 0.2;
  double maxT  = 1.0;
  long seed    = 11;
  int NICE     = -1;
  int nmix     = 2;		// Two component mixture
  double alpha = 1.0;		// Dirichlet shape
  int mpi_type = 0;		// Tile-type granularity

  // Initialize mpi
  //
  mpi_init(argc, argv);

  BIE::mpi_used = 1;

#ifdef DEBUG
  sleep(20);
#endif

  int c;
  while (1) {
    c = getopt (argc, argv, "s:m:M:X:Y:t:a:d:f:P:g:Sxh");
    if (c == -1) break;
     
    switch (c)
      {
      case 's': seed = atoi(optarg); break;
      case 'm': minmc = atoi(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'X': dX = atof(optarg); break;
      case 'Y': dY = atof(optarg); break;
      case 't': maxT = atof(optarg); break;
      case 'a': alpha = atof(optarg); break;
      case 'd': datafile.erase(); datafile = optarg; break;
      case 'f': homedir.erase(); homedir = optarg; break;
      case 'P': NICE = atoi(optarg); break;
      case 'S': mstat = true; break;
      case 'x': TemperedSimulation::use_tempering = false; break;
      case 'g': mpi_type = 1; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-s I\t\twhere I is the pseudorandom number seed\n";
	msg += "\t-m N\t\tminimum number of temperature states\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-X x\t\taverage knot spacing in x-direction\n";
	msg += "\t-Y x\t\taverage knot spacing in y-direction\n";
	msg += "\t-t x\t\tmaximum temperature for tempering\n";
	msg += "\t-a x\t\tDirichlet shape parameter\n";
	msg += "\t-d file\t\tdata file\n";
	msg += "\t-t T\t\tmaximum temperature for simulated tempering\n";
	msg += "\t-W x\t\tscale factor for proposal width from variance\n";
	msg += "\t-U x\t\tpower law scaling for galactic latitude (x<1.0)\n";
	msg += "\t-k N\t\tnumber of states used to do PCA\n";
	msg += "\t-R p\t\treport only if percent support larger than p\n";
	msg += "\t-p file\t\tprior parameter file\n";
	msg += "\t-d file\t\tworking directory\n";
	msg += "\t-g\t\tuse integration-level granularity (default is tile)\n";
	msg += "\t-x\t\tturn off tempering\n";
	cerr << msg; exit(-1);
      }
  }

  // Prefix root data directory
  //
  datafile = homedir + "/" + datafile;

  // Set priority on nodes
  // 
  if (NICE>0) setpriority(PRIO_PROCESS, 0, NICE);


  // Initialize base random number generator
  //
  BIEgen = new BIEACG(seed, 20);

  // Hold instantiation of classes define the simuation
  //
  SimpleGalaxyModel *model;
  QuadGrid *grid;
  RecordInputStream_Ascii *ris;
  DataTree* dist;
  Histogram1D *histo;
  Integration *intgr;
  SquareTile *tile;
  StateInfo *si;

  int ndim = 2;			// Two model parameters

				// State metadata
  si    = new StateInfo(nmix, ndim);

				// Galaxy model
  model = new SimpleGalaxyModel(ndim, nmix);

				// Integration method
  intgr = new AdaptiveLegeIntegration(dX, dY);

				// Factor for 1D histogram in each tile
  histo = new Histogram1D(6.0, 15.0, 1.0, "mag1");



				// Rectangular grid tessellation
  tile = new SquareTile();
  
  ris = new RecordInputStream_Ascii(datafile);
#ifdef CRAZY
  grid = new QuadGrid(tile, 1.0, 1.05, 0.1, 0.11, 6);
#else
  grid = new QuadGrid(tile, -M_PI, M_PI, -1.0, 1.0, 6);
#endif
  dist = new DataTree(ris, histo, grid);

  cout << "---- Tessellation diagnostic" << endl
       << "      Number of data points in bounds:  " 
       << dist->Total() << endl
       << "      Number of data points out of bounds: "
       << dist->Offgrid() << endl 
       << "---- Done" << endl 
       << flush;

  // Dump out means and standard devs for each tile
  //
#ifdef DEBUG
  dist->Reset();
  while(!(dist->IsDone())) {
    cout 
      << setw(12) << dist->CurrentTile()->X(0.5, 0.5)
      << setw(12) << dist->CurrentTile()->Y(0.5, 0.5)
      << setw(12) << (dist->CurrentItem()->Mean())[0]
      << setw(12) << (dist->CurrentItem()->StdDev())[0]
      << endl;

    dist->Next();
  }
#endif

  // Define prior
  //
  UniformDist unif1(0.5, 8.0);
  UniformDist unif2(100.0, 1200.0);
  clivectordist vdist(2);
  vdist()[0] = &unif1;
  vdist()[1] = &unif2;
  InitialMixturePrior* prior = new InitialMixturePrior(si, alpha, &vdist);

  // Simple sample statistic
  //
  EnsembleStat* sstat = new EnsembleStat();

  // Define convergence method
  //
  const int nsteps = 400;
  CountConverge *converge = new CountConverge(nsteps, sstat);

  // Define parameters for TemperedSimulation
  //
  vector<double> mhw;
  mhw.push_back(0.02);
  mhw.push_back(0.2);
  mhw.push_back(10.0);
  MHWidthOne mhwidth(si, &mhw);

  // Ok, now instantiate the simulation
  //
  LikelihoodComputationMPI likelihoodComputationMPI(si);
  MetropolisHastings mca;

  cout << "Process " << myid << ": contructing testsim . . . " 
       << endl << flush;

  TemperedSimulation testsim(si, minmc, maxT, &mhwidth,
			     dist, model, intgr, converge, prior,
			     &likelihoodComputationMPI, &mca);
  //                            ^
  // defining classes           |
  // ---------------------------|

  // Put the all nodes *but* Node 0 into slave mode
  //

  cout << "Process " << myid << ": done" << endl << flush;

  likelihoodComputationMPI.Granularity(mpi_type);

  vector<double> winit;
  vector< vector<double> > pinit(nmix);

  for (int i=0; i<nmix; i++) {
    winit.push_back(1.0/nmix);
#ifdef CRAZY
    pinit[i].push_back(0.1);
    pinit[i].push_back(20.0);
#else
    pinit[i].push_back(2.5);
    pinit[i].push_back(200.0);
#endif
  }

  if (myid) {
    cout << "Process " << myid << ": enslaved" << endl << flush;
    likelihoodComputationMPI.startThread();
  }
  else {
    cout << "Process " << myid << ": continuing\n";
  }


  //
  // Simulation will be controlled by Node 0
  //
  try {
    testsim.NewState(nmix, winit, pinit);
  }
  catch (ImpossibleStateException e) {
    if (myid==0) 
      cout << "Process " << myid << ": screwy state, this is very bad"
	   << endl;
#ifndef CRAZY
    exit(-1);
#endif
  }


  if (myid==0) cout << "Initial value: " << testsim.GetValue() << endl;

  for (int i=0; i<nsteps; i++) {

    //
    // Do the work
    //

    testsim.OneStep();

    //
    // Master node prints diagnostic output
    //
    if (myid==0) {

      cout << "Step " << i << ": value=" << testsim.GetValue() <<  endl;

      testsim.ReportState();

      if (mstat) {
	vector<double> ret = testsim.GetMixstat();
	cout << endl;
	cout << "  P(accept):";
	for (int ik=0; ik<(int)ret.size(); ik++) cout << " " << ret[ik];
	cout << endl;
      }

      cout << endl;
    }

  }

  //
  // Stop the worker threads
  //
  
  likelihoodComputationMPI.free_workers();
  if (myid) likelihoodComputationMPI.stopThread();

  //
  // All done
  //
  MPI_Finalize();

  return 0;
}
