/**
   For testing persistence of EnsembleDisc and EnsembleKD
*/

#include <cstdlib>
#include <cmath>

#include <iostream>
#include <iomanip>
#include <typeinfo>

#include <unistd.h>

#include <config.h>
#if PERSISTENCE_ENABLED
#include <TestUserState.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#endif

using namespace std;

#define IS_MAIN

#include <BIEconfig.h>
#include <EnsembleDisc.h>
#include <EnsembleKD.h>

using namespace BIE;

struct Record {
  int level, iter;
  double prob, like, prior;
  int M;
  vector<double> wght;
  vector<double> parm;
} record;

vector< vector<Record> > levels;
vector< int > levels_mix;
vector< int > levels_dim;

/**
   Test routine for data accumulation
*/
int
main(int argc, char** argv)
{
  unsigned nbeg = 0;		// First burnt-in state
  unsigned nlev = 0;		// Desired level
				// Input data file
  string infile = "test.statelog";
  bool   scaled = true;		// Scaled distance for state separation
  
  while (1) {
    int c = getopt(argc, argv, "f:n:l:S:h");
    if (c == -1) break;
    
    switch (c) {
    case 'f':
      infile = string(optarg);
      break;
    case 'n':
      nbeg = atoi(optarg);
      break;
    case 'l':
      nlev = atoi(optarg);
      break;
    case 'S':
      scaled = atoi(optarg) ? true : false;
      break;
    case 'h':
      cerr << "Usage: " << argv[0] << " [arguments]" << endl;
      return 0;
    }
  }
  
  // ==================================================
  // Remove old test files
  // ==================================================

  system("rm -rf pdir/testPersistenceEDS");

  
  // ==================================================
  // Parse BIE posterior state file
  // ==================================================
  
  ifstream fin(infile.c_str());
  if (!fin) {
    cerr << "Error opening <" << infile << ">" << endl;
    exit(-1);
  }
  
  const int linelen = 4096;
  char linebuf[linelen];
  
  fin.getline(linebuf, linelen);
  string s(linebuf);
  
  vector<string> labels;
  size_t first, second, loc = 0;
  while (1) {
    first  = s.find('"', loc);
    if (first == string::npos) break;
    second = s.find('"', first+1);
    labels.push_back(s.substr(first+1, second-first-1));
    loc = second+1;
  }
  
  cout << "# labels: " << labels.size() << endl;
  
  bool mixture = false;
  if (labels[5] == string("Number")) mixture = true;

  // ==================================================
  // Parse rest of file
  // ==================================================
  
  vector<Record> curlev;
  unsigned nmix=0, ndim=0;
  int lastl = -1;
  
  while (fin) {
    fin.getline(linebuf, linelen);
    istringstream sin(linebuf);
    
    sin >> record.level;
    sin >> record.iter;
    sin >> record.prob;
    sin >> record.like;
    sin >> record.prior;
    if (mixture) sin >> record.M;
    else record.M = 0;
    
    // Only on first time . . .
    if (lastl==-1) lastl = record.level;
    
    // Compute the various ranks and initialize
    // vectors
    if (mixture) {
      nmix = record.M;
      ndim = (labels.size()-6-nmix)/nmix;
      record.wght = vector<double>(nmix);
      record.parm = vector<double>(nmix*ndim);
    
      // Parse the input line
      for (unsigned i=0; i<nmix; i++) 
	sin >> record.wght[i];
      for (unsigned i=0; i<ndim*nmix; i++) 
	sin >> record.parm[i];
    } else {
      nmix = record.M;
      ndim = labels.size()-5;
      record.parm = vector<double>(ndim);
      // Parse the input line
      for (unsigned i=0; i<ndim; i++) 
	sin >> record.parm[i];
    }
    
    // Next level?
    if (lastl != record.level) {
      levels.push_back(curlev);
      levels_mix.push_back(nmix);
      levels_dim.push_back(ndim);
      curlev.erase(curlev.begin(), curlev.end());
    }
    // Save the completed record
    curlev.push_back(record);
    lastl = record.level;
  }
  // The remaining data is the final level
  if (curlev.size()) {
    levels.push_back(curlev);
    levels_mix.push_back(nmix);
    levels_dim.push_back(ndim);
  }
  
  // ==================================================
  // Stats and sanity check
  // ==================================================
  
  cout << setw(8) << "Levels" << setw(8) << "Counts" << endl;
  cout << setw(8) << "------" << setw(8) << "------" << endl;
  for (unsigned i=0; i<levels.size(); i++)
    cout << setw(8) << i << setw(8) << levels[i].size() << endl;
  
  if (nlev >= levels.size() || nlev<0) {
    ostringstream msg;
    msg << "Request level <" << nlev << "> does not exist!" << endl;
    exit(-1);
  }
  
  if (nbeg >= levels[nlev].size() || nbeg<0) {
    ostringstream msg;
    msg << "Request start point  <" << nbeg << "> is out of bounds!" << endl;
    exit(-1);
  }
  
  nmix = levels_mix[nlev];
  ndim = levels_dim[nlev];
  
  StateInfo *si;
  if (mixture)
    si = new StateInfo(nmix, ndim);
  else
    si = new StateInfo(ndim);

  EnsembleDisc::scaled = scaled;
  EnsembleDisc *sstat1 = new EnsembleDisc(si);
  EnsembleKD   *sstat2 = new EnsembleKD  (si);
  
  
  // ==================================================
  // MAIN LOOP
  // ==================================================
  
  State state(si);
  
  vector<double> dv(3, 1.0);
  for (unsigned i=nbeg; i<levels[nlev].size(); i++) {
    
    if (mixture) {
      for (unsigned j=0; j<nmix; j++) 
	state[j] = levels[nlev][i].wght[j];
      for (unsigned j=0; j<nmix*ndim; j++) 
	state[j+nmix] = levels[nlev][i].parm[j];
    } else {
      for (unsigned j=0; j<ndim; j++) 
	state[j] = levels[nlev][i].parm[j];
    }      
    
    sstat1->AccumData(dv, state);
    sstat2->AccumData(dv, state);
  }
  
  cout << "Class: " << typeid(*sstat1).name() << endl;
  sstat1->ComputeDistribution();
  sstat1->PrintDiag(cout);

  if (mixture) state = sstat1->Mean(nmix);
  else         state = sstat1->Mean(1);
  std::cout << "Initial Disc logPDF(mean)=" << sstat1->logPDF(state) << std::endl;
  
  cout << "Class: " << typeid(*sstat2).name() << endl;
  sstat2->ComputeDistribution();
  sstat2->PrintDiag(cout);

  if (mixture) state = sstat2->Mean(nmix);
  else         state = sstat2->Mean(1);
  std::cout << "Initial KD  logPDF(mean)=" << sstat2->logPDF(state) << std::endl;


  TestUserState saveState("testPersistenceEDS", 0, true);
  saveState.addObject(sstat1);
  saveState.addObject(sstat2);

  try {
    TestSaveManager testSaveManager(&saveState, BOOST_BINARY, BACKEND_FILE);
    testSaveManager.save();
  } catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }
  
  // Delete the ensembles
  //
  delete sstat1;
  delete sstat2;

  // Restore the State
  //
  TestUserState *restored;

  try {
    TestLoadManager testLoadManager(&restored, "testPersistenceEDS", 1, 
				    BOOST_BINARY, BACKEND_FILE);
    testLoadManager.load();
  } catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on LOAD! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }
  
  vector<Serializable*> objectlist = restored->getTransClosure().objectlist;
  
  sstat1 = dynamic_cast<EnsembleDisc*>(objectlist[0]);
  sstat2 = dynamic_cast<EnsembleKD*>  (objectlist[1]);
  
  std::cout << "Restored Disc logPDF(mean)=" << sstat1->logPDF(state) << std::endl;
  sstat1->PrintDiag(cout);
  std::cout << "Restored KD   logPDF(mean)=" << sstat2->logPDF(state) << std::endl;
  sstat2->PrintDiag(cout);

  TestUserState saveState2("testPersistenceEDS", 1, true);
  saveState2.addObject(sstat1);
  saveState2.addObject(sstat2);

  try {
    TestSaveManager testSaveManager2(&saveState2, BOOST_BINARY, BACKEND_FILE);
    testSaveManager2.save();
  } catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }
  
  delete sstat1;
  delete sstat2;
  
  return 0;
}
