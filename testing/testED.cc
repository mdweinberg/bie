/**
   For testing EnsembleDisc . . .
*/

#include <iostream>
#include <iomanip>
#include <typeinfo>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <EnsembleDisc.h>
#include <ACG.h>
#include <Uniform.h>

#include <DummyModel.h>

using namespace BIE;

string priorfile("prior.dat");
string datafile("data.dat");

/**
   Test routine for data accumulation
*/
int
main(int argc, char** argv)
{
  int nmix = 2;
  int ndim = 2;
  int nstates = 1000;
  int nsamples = 20;
  int nimage = 0;
  double xy = 2.0;

  while (1) {
    int c = getopt(argc, argv, "M:N:n:s:i:x:h");
    if (c == -1) break;

    switch (c) {
    case 'M':
      nmix = atoi(optarg);
      break;
    case 'N':
      ndim = atoi(optarg);
      break;
    case 'n':
      nstates = atoi(optarg);
      break;
    case 's':
      nsamples = atoi(optarg);
      break;
    case 'i':
      nimage = atoi(optarg);
      break;
    case 'x':
      xy = atof(optarg);
      break;
    case 'h':
      cerr << "Usage: " << argv[0] << " [arguments]" << endl;
      return 0;
    }
  }

  BIEgen = new BIEACG(11, 20);
  Uniform *unit = new Uniform(0.0, 1.0, BIEgen);
  StateInfo *si = new StateInfo(nmix, ndim);
  EnsembleDisc *sstat = new EnsembleDisc(si);

  cout << "Class: " << typeid(*sstat).name() << endl;

  // ==================================================
  // MAIN LOOP
  // ==================================================
  
  int dim = si->Ntot;

  State state(si);

  for (int i=0; i<nstates; i++) {

    double sum = 0.0;
      
    for (int j=0; j<dim;  j++) state[j] = (*unit)();
    for (int j=0; j<nmix; j++) sum += state[j];
    for (int j=0; j<nmix; j++) state[j] /= sum;
    
    vector<double> dv(3, 1.0);
    sstat->AccumData(dv, state);
    sstat->AccumData(dv, state);
    sstat->AccumData(dv, state);
  }

  sstat->ComputeDistribution();
  sstat->PrintDiag(cout);

  int M = nmix;
  cout << endl << "Sampling:" << endl;
  for (int i=0; i<nsamples; i++) {
    vector<double> pt = sstat->Sample(M);
    State st(si, pt);
    cout << setw(3) << i
	 << setw(15) << exp(sstat->logPDF(st))
	 << setw(3) << M;
    for (int j=0; j<M; j++) cout << setw(15) << pt[j];
    cout << endl;
  }

  if (nimage > 1 && nmix==2 && ndim==2) {

    ofstream ogpl("data.gpl");

    double dxy = 2.0*xy/(nimage-1);
    vector<double> pt(7), ptdef(7, 0.5);
    State st;

    ptdef[0] = 2;

    for (int j=0; j<nimage; j++) {

      for (int i=0; i<nimage; i++) {
	
	pt = ptdef;
	pt[1] = -xy + dxy*i;
	pt[2] = -xy + dxy*j;

	st = pt;

	ogpl << setw(18) << pt[1]
	     << setw(18) << pt[2]
	     << setw(18) << exp(sstat->logPDF(st));

	pt = ptdef;
	pt[3] = -xy + dxy*i;
	pt[4] = -xy + dxy*j;

	st = pt;

	ogpl << setw(18) << pt[3]
	     << setw(18) << pt[4]
	     << setw(18) << exp(sstat->logPDF(st));

	pt = ptdef;
	pt[5] = -xy + dxy*i;
	pt[6] = -xy + dxy*j;

	st = pt;

	ogpl << setw(18) << pt[5]
	     << setw(18) << pt[6]
	     << setw(18) << exp(sstat->logPDF(st))
	     << endl;
      }
      ogpl << endl;
    }
  }

  delete sstat;
  delete BIEgen;
  delete unit;

  return 0;
}

