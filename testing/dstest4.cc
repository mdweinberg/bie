#include <string>
#include <vector>
#include <sstream>
using namespace std;

#include <BasicType.h>
#include <TypedBuffer.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStream.h>
#include <RecordInputStream.h>
#include <RecordStream_Ascii.h>
#include <RecordStream_Binary.h>
#include <RecordOutputStream.h>
#include <RecordStreamFilter.h>
#include <BinaryFilters.h>
#include <UnaryFilters.h>
#include <SetFilters.h>
#include <BIEException.h>

using namespace BIE;

int main ()
{
  /*****************************************************************************
  * Filter test.
  *****************************************************************************/
  try 
  {
    RecordInputStream * risa = new RecordInputStream_Ascii("tagged.dat");
    cout << risa->eos() << "  " << risa->error() << endl;
  
    RecordStreamFilter * filter = new ScaleFilter(risa, 5);
  
    cout << filter->toString();
    filter->connect("fieldone", "x");
    cout << filter->toString();
    filter->renameOutputField("scaled", "alistair's new value");
  
    cout << filter->toString();
  
    cout << "filter->connect(f, x): ";
    try { filter->connect("f", "x"); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
    cout << "filter->connect(0, x): ";
    try { filter->connect(0, "x"); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
    cout << "filter->connect(1, 0): ";
    try { filter->connect(1, 0); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
    cout << "filter->connect(1, 23): ";
    try { filter->connect(1, 23); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
    cout << "filter->connect(1, q): ";
    try { filter->connect(0, "q"); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
    risa = risa->filterWith(2, filter);
    cout << filter->toString();
  
    cout << "filter->connect(fieldone, x):";
    try { filter->connect("fieldone", "x"); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
   
    cout << risa->getBuffer()->toString() << endl;
    cout << "******Average FILTER******************" << endl;
  
    filter = new AverageFilter(risa);
    cout << "Before connecting\n" <<  filter->toString();
    filter->connect(1, 1);
    cout << "after connecting 1 to 1\n" << filter->toString();
    filter->connect(2, 1);
    cout << "After connecting 2 to 1\n" << filter->toString();
  
    filter->disconnect(1, 1);
    cout << "after disconnecting 1 from 1\n" << filter->toString();
    filter->disconnect(2, 1);
    cout << "after disconnecting 2 from 1\n" << filter->toString();
  
    filter->connect(1, 1);
    cout << filter->toString();
    filter->connect(2, 1);
    cout << "After reconnecting both\n" << filter->toString();
  
    filter->disconnect(1);
    cout << "after disconnecting all in set\n" << filter->toString();
  
    filter->connect(1, 1);
    cout << filter->toString();
    filter->connect(2, 1);
    cout << filter->toString();
    
    filter->connect(2, 1);
    cout << "After attemping duplicate\n" << filter->toString();
  
    risa = risa->filterWith(2, filter);
    cout << risa->getBuffer()->toString() << endl;
  
    try { risa = risa->filterWith(2, filter); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
    filter = new AverageFilter(risa);
    cout << "Before connecting\n" <<  filter->toString();
    filter->connect(1, 1);
    cout << "after connecting 1 to 1\n" << filter->toString();
    filter->connect(3, 1);
    cout << "After connecting 2 to 1\n" << filter->toString();
    filter->connect(2, 1);
    cout << "After connecting 2 to 1\n" << filter->toString();
  
    try { risa = risa->filterWith(2, filter); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
    filter->disconnect(1, 1);
    cout << "after disconnecting 1 from 1\n" << filter->toString();
    filter->disconnect(2, 1);
    cout << "after disconnecting 1 from 1\n" << filter->toString();
  
    try { risa = risa->filterWith(2, filter); }
    catch (BIEException e) { cout << e.getErrorMessage() << endl; }
    
    cout << "****Addition Filter******************\n";
    cout << risa->getType()->toString() << endl;
    filter = new AdditionFilter(risa);
    cout << "Before connecting: \n" << filter->toString();
    filter->connect(2, 1);
    cout << "After connecting 2 to 1\n" << filter->toString();
    filter->disconnect(1);
    cout << "After disconnecting all into 1\n" << filter->toString();
    filter->connect(2, 1);
    cout << "After reconnecting 2 to 1\n" << filter->toString();
  
    filter->connect(3, "y");
    cout << filter->toString();
    risa = risa->filterWith(7, filter);
    cout << risa->getBuffer()->toString() << endl;
  
    cout << "****Subtraction Filter******************\n";
    cout << risa->getType()->toString() << endl;
    filter = new SubtractionFilter(risa);
    cout << "Before connecting: \n" << filter->toString();
    filter->connect(2, 1);
    cout << "After connecting 2 to 1\n" << filter->toString();
    filter->connect(3, "y");
    cout << "After connecting 3 to y\n" << filter->toString();
    risa = risa->filterWith(7, filter);
    cout << risa->getBuffer()->toString() << endl;
  
    cout << "****Division Filter******************\n";
    cout << risa->getType()->toString() << endl;
    filter = new DivisionFilter(risa);
    cout << "Before connecting: \n" << filter->toString();
    filter->connect(2, 1);
    cout << "After connecting 2 to 1\n" << filter->toString();
    filter->connect(3, 2);
    cout << "After connecting 3 to 2\n" << filter->toString();
    risa = risa->filterWith(7, filter);
    cout << risa->getBuffer()->toString() << endl;
  
    cout << "****Power Filter******************\n";
    cout << risa->getType()->toString() << endl;
    filter = new PowerFilter(risa);
    cout << "Before connecting: \n" << filter->toString();
    filter->connect(2, 1);
    cout << "After connecting 2 to 1\n" << filter->toString();
    filter->connect(3, "exponent");
    cout << "After connecting 3 to exponent\n" << filter->toString();
    risa = risa->filterWith(7, filter);
    cout << risa->getBuffer()->toString() << endl;
  
    cout << " **** LogFilter ***** " << endl;
    cout << risa->getType()->toString() << endl;
    filter = new LogFilter(risa);
    cout << "Before connecting: \n" << filter->toString();
    filter->connect("fieldone", "x");
    cout << "After connecting fieldone to x\n" << filter->toString();
    risa = risa->filterWith(8, filter);
    cout << risa->getBuffer()->toString() << endl;
    
    cout << " **** LogBaseNFilter ***** " << endl;
    cout << risa->getType()->toString() << endl;
    filter = new LogBaseNFilter(risa, 10);
    cout << "Before connecting: \n" << filter->toString();
    filter->connect("fieldone", "x");
    cout << "After connecting fieldone to x\n" << filter->toString();
    risa = risa->filterWith(8, filter);
    cout << risa->getBuffer()->toString() << endl;
  
    
    cout << risa->nextRecord() << endl;
    cout << risa->getBuffer()->toString() << endl;
    cout << risa->nextRecord() << endl;
    cout << risa->getBuffer()->toString() << endl;
    cout << risa->nextRecord() << endl;
    cout << risa->getBuffer()->toString() << endl;
    
    cout << filter->toString();
  }
  catch (BIEException e)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << e.getErrorMessage() << endl;
  }
  catch (...)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << "not a bie exception" << endl;
  }
}
