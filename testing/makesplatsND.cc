// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <ACG.h>
#include <Normal.h>

string outname("splat.data");

int 
main(int argc, char** argv)
{
  int M = 1;
  int Ndim = 1;
  long iseed = 11;
  
  int c;
  while (1) {
    c = getopt (argc, argv, "M:i:f:d:");
    if (c == -1) break;
     
    switch (c)
      {
      case 'M': M = atoi(optarg); break;
      case 'f': outname = optarg; break;
      case 'i': iseed = atoi(optarg); break;
      case 'd': Ndim = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-f filename\t\toutput filename (default: " +outname+ ")\n";
	msg += "\t-M n\t\tnumber of components\n";
	msg += "\t-i x\t\tseed for random number generator\n";
	msg += "\t-d x\t\tnumber of data dimensions\n";
	cerr << msg; exit(-1);
      }
  }

  ACG gen(iseed, 20);
  Normal unit(0.0, 1.0, &gen);
  Normal ddat(5.0, 1.0, &gen);

  ofstream out(outname.c_str());
  if (!out) {
    cerr << "Error opening <" + outname + ">\n";
    exit(-1);
  }

  vector<int> number;
  vector<double> xx, yy, sx, sy;
  double x, y, dx, dy;
  int n, sum = 0;

				// Get parameters from user
  for (int m=0; m<M; m++) {
    cout << "Component " << m << ": n, x0, y0, width_x, width_y? ";
    cin >> n;
    cin >> x;
    cin >> y;
    cin >> dx;
    cin >> dy;

    number.push_back(n);
    sum += n;

    xx.push_back(x);
    yy.push_back(y);
    sx.push_back(fabs(dx));
    sy.push_back(fabs(dy));
  }

				// Write data file

  // Recent versions of DataSet and DataSetBuffer determine these dynamically
  // out << setw(15) << sum << endl;
  // out << setw(15) << Ndim << endl;

  for (int m=0; m<M; m++) {
    
    for (int i=0; i<number[m]; i++) {

      out 
	<< setw(15) << xx[m] + sx[m]*unit()
	<< setw(15) << yy[m] + sy[m]*unit();

      for (int j=0; j<Ndim; j++)
	out << setw(15) << ddat();
      
      out << endl;
    }

  }
				// Write parameters
  
  out << "#     Parameters" << endl;
  for (int m=0; m<M; m++) {
    out << "#---- Component #" << m << endl;
    out << "#     N=" << number[m]  << endl;
    out << "#     X=" << xx[m]  << endl;
    out << "#     Y=" << yy[m]  << endl;
    out << "#    SX=" << sx[m]  << endl;
    out << "#    SY=" << sy[m]  << endl;
  }

}
  
