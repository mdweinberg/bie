// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <MHWidthOne.h>
#include <TemperedSimulation.h>
#include <LikelihoodComputationSerial.h>
#include <CountConverge.h>
#include <MappedGrid.h>
#include <Histogram1D.h>
#include <Model.h>
#include <InitialMixturePrior.h>
#include <SimpleStat.h>
#include <SimpleGalaxyModel.h>
#include <AdaptiveLegeIntegration.h>
#include <QuadGrid.h>
#include <DataTree.h>
#include <RecordStream_Ascii.h>
#include <MuSquareTile.h>
#include <MetropolisHastings.h>
#include <EnsembleStat.h>

using namespace BIE;

string datafile("data.dat");

/**
   @name testtemp2: main
   Does full simulation at one level of resolution (so far).  Single
   process (non-MPI) implementation.
*/
int
main(int argc, char** argv)
{
  bool mstat   = false;
  int minmc    = 2;
  double dX    = 0.2;
  double dY    = 0.2;
  double maxT  = 1.0;
  int NICE     = -1;
  int nmix     = 2;		// Two component mixture
  double alpha = 1.0;		// Dirichlet shape


  int c;
  while (1) {
    c = getopt (argc, argv, "m:M:X:Y:t:a:d:f:O:P:Sxh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'm': minmc = atoi(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'X': dX = atof(optarg); break;
      case 'Y': dX = atof(optarg); break;
      case 't': maxT = atof(optarg); break;
      case 'a': alpha = atof(optarg); break;
      case 'd': datafile.erase(); datafile = optarg; break;
      case 'f': homedir.erase(); homedir = optarg; break;
      case 'O': outfile.erase(); outfile = optarg; break;
      case 'P': NICE = atoi(optarg); break;
      case 'S': mstat = true; break;
      case 'x': TemperedSimulation::use_tempering = false; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-m N\t\tminimum number of temperature states\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-X x\t\tmean integration knots in x-direction\n";
	msg += "\t-Y x\t\tmean integration knots in y-direction\n";
	msg += "\t-t x\t\tmaximum temperature for tempering\n";
	msg += "\t-a x\t\tDirichlet shape parameter\n";
	msg += "\t-d file\t\tdata file\n";
	msg += "\t-p file\t\tprior parameter file\n";
	msg += "\t-O file\t\tlog file\n";
	msg += "\t-i file\t\tinit file\n";
	msg += "\t-P i\t\tsets the nice level\n";
	msg += "\t-S\t\tmixture statistics from tempering\n";
	msg += "\t-x\t\tturn off tempering\n";
	cerr << msg; exit(-1);
      }
  }

  // Prefix root data directory
  //
  datafile  = homedir + "/" + datafile;
  
  // Set priority on nodes
  // 
  if (NICE>0) setpriority(PRIO_PROCESS, 0, NICE);


  // Initialize base random number generator
  //
  BIEgen = new BIEACG(11, 20);

  // Hold instantiation of classes define the simuation
  //
  SimpleGalaxyModel *model;
  QuadGrid *grid;
  RecordInputStream_Ascii *ris;
  DataTree* dist;
  Histogram1D *histo;
  Integration *intgr;
  SquareTile *tile;
  StateInfo *si;

  int ndim = 2;			// Two model parameters

				// The state metadata
  si = new StateInfo(nmix, ndim);

				// Galaxy model
  model = new SimpleGalaxyModel(ndim, nmix);

				// Integration method
  intgr = new AdaptiveLegeIntegration(dX, dY);

				// Factor for 1D histogram in each tile
  histo = new Histogram1D(6.0, 15.0, 1.0, "mag1");
  

				// Rectangular grid tessellation
  tile = new SquareTile();

  ris = new RecordInputStream_Ascii(datafile);

#ifdef CRAZY
  grid = new QuadGrid(tile, 1.0, 1.05, 0.1, 0.11, 6);
#else
  grid = new QuadGrid(tile, -M_PI, M_PI, -1.0, 1.0, 6);
#endif
  dist = new DataTree(ris, histo, grid);

  cout << "---- Tessellation diagnostic" << endl
       << "      Number of data points in bounds:  " 
       << dist->Total() << endl
       << "      Number of data points out of bounds: "
       << dist->Offgrid() << endl 
       << "---- Done" << endl 
       << flush;

  // Dump out means and standard devs for each tile
  //
  dist->Reset();
  while(!(dist->IsDone())) {
    cout 
      << setw(12) << dist->CurrentTile()->X(0.5, 0.5)
      << setw(12) << dist->CurrentTile()->Y(0.5, 0.5)
      << setw(12) << (dist->CurrentItem()->Mean())[0]
      << setw(12) << (dist->CurrentItem()->StdDev())[0]
      << endl;

    dist->Next();
  }

  // Define prior
  //
  UniformDist unif1(0.5, 8.0);
  UniformDist unif2(100.0, 1200.0);
  clivectordist vdist(2);
  vdist()[0] = &unif1;
  vdist()[1] = &unif2;
  InitialMixturePrior* prior = new InitialMixturePrior(si, alpha, &vdist);

  // Simple sample statistic
  //
  EnsembleStat* sstat = new EnsembleStat(si);

  // Define convergence method
  //
  CountConverge *converge = new CountConverge(100, sstat);

  // Define parameters for TemperedSimulation
  //
  vector<double> mhw;
  mhw.push_back(0.02);
  mhw.push_back(0.2);
  mhw.push_back(10.0);

  MHWidthOne mhwidth(si, &mhw);

  // Ok, now instantiate the simulation
  //
  LikelihoodComputationSerial likelihoodComputationSerial;

  MetropolisHastings mca;

  TemperedSimulation testsim(si, minmc, maxT, &mhwidth,
			     dist, model, intgr, converge, prior,
			     &likelihoodComputationSerial, &mca);
  //                         ^
  // defining classes        |
  // ------------------------|

  vector<double> winit;
  vector< vector<double> > pinit(nmix);

  for (int i=0; i<nmix; i++) {
    winit.push_back(1.0/nmix);
#ifdef CRAZY
    pinit[i].push_back(0.1);
    pinit[i].push_back(20.0);
#else
    pinit[i].push_back(3.5);
    pinit[i].push_back(350.0);
#endif
  }
  
  State s(si);
  s.setState(nmix, winit, pinit);

  try {
    testsim.NewState(s);
  }
  catch (ImpossibleStateException e) {
    cout << "Screwy state, this is very bad"
	 << endl;
#ifndef CRAZY
    exit(-1);
#endif
  }

  cout << "Initial value: " << testsim.GetValue() << endl;

  const int nsteps = 10;

  for (int i=0; i<nsteps; i++) {
    testsim.OneStep();
    cout << "Step " << i << ": value=" << testsim.GetValue() <<  endl;

    testsim.ReportState();

    if (mstat) {
      vector<double> ret = testsim.GetMixstat();
      cout << endl;
      cout << "  P(accept):";
      for (int ik=0; ik<(int)ret.size(); ik++) cout << " " << ret[ik];
      cout << endl;
    }
    
    cout << endl;
  }

}

