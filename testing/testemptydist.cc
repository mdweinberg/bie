#define IS_MAIN
#include <DataTree.h>
#include <OneBin.h>
#include <Distribution.h>
#include <iostream>
#include <SquareTile.h>
#include <QuadGrid.h>
#include <RecordStream_Ascii.h>
#include <BasicType.h>
#include <Node.h>
#include <EmptyDataTree.h>

using namespace BIE;

/*******************************************************************************
* This tests the DataTree class with different tile distribution
* classes.
*******************************************************************************/
void maintest();
void dumpbinneddist(BinnedDistribution * binneddist);

void printarray(vector<double> array)
{
  if (array.size() == 0)
  {
    cout << "{ empty vector }" << endl;
  }
  else
  {
    cout << "{" << array[0];
  
    for (int arrayindex = 1; arrayindex < (int) array.size(); arrayindex++)
    {
      cout << ", " << array[arrayindex];
    }
    
    cout << "}" << endl;
  }
}

int main ()
{
  cout << "\n\n\n"<< "************* EmptyDist test *************** " << endl;
  maintest();
  cout << "\n\n\n" << "************* Test finished *********" << endl;
}


void maintest()
{
  /*****************************************************************************
  * Build the DataTree
  *****************************************************************************/
  BaseDataTree * empty = new EmptyDataTree();
  
  cout << "Total() returns: " << empty->Total() << endl;
  cout << "OffGrid() returns: " << empty->Offgrid() << endl;
  
  cout << "GetTile(-1): " << empty->GetTile(-1) << endl;
  cout << "GetTile(0): " << empty->GetTile(0) << endl;
  cout << "GetTile(1): " << empty->GetTile(1) << endl;
  cout << "GetTile(10000000): " << empty->GetTile(10000000) << endl;

  cout << "Number of tiles in frontier: " << empty->NumberItems() << endl;

  empty->Reset();  

  for (; ! empty->IsDone(); empty->Next())
  {
    cout << "Current Tile ID: "  << empty->CurrentTile()->GetNode()->ID() << endl;  
    empty->CurrentTile()->printTile(cout);

    dumpbinneddist((BinnedDistribution*)empty->CurrentItem());
  }
  
  dumpbinneddist((BinnedDistribution*)empty->First());
  dumpbinneddist((BinnedDistribution*)empty->Last());
  
  try {
    cout << "getDistribution(-1): " << empty->getDistribution(-1) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "getDistribution(0): " << empty->getDistribution(0) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "getDistribution(1): " << empty->getDistribution(1) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "getDistribution(10000000): " << empty->getDistribution(10000000) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }
}


void dumpbinneddist(BinnedDistribution * binneddist)
{
  cout << "DUMPING BINNED DIST ************************************" << endl;
  cout << "numberData returns: " << binneddist->numberData() << endl; 
  cout << "getValue(-1) returns: " << binneddist->getValue(-1) << endl;
  cout << "getValue(0) returns: " << binneddist->getValue(0) << endl;
  cout << "getValue(1) returns: " << binneddist->getValue(1) << endl;
  cout << "getValue(10000000) returns: " << binneddist->getValue(10000000) << endl;

  cout << "getLow(-1) returns : " << endl;
  printarray(binneddist->getLow(-1)); 
  cout << "getLow(0) returns : " << endl; 
  printarray(binneddist->getLow(0)); 
  cout << "getLow(1) returns : " << endl; 
  printarray(binneddist->getLow(1)); 
  cout << "getLow(10000000) returns : " << endl; 
  printarray(binneddist->getLow(10000000)); 
  
  cout << "getHigh(-1) returns : " << endl; 
  printarray(binneddist->getHigh(-1)); 
  cout << "getHigh(0) returns : " << endl; 
  printarray(binneddist->getHigh(0)); 
  cout << "getHigh(1) returns : " << endl; 
  printarray(binneddist->getHigh(1)); 
  cout << "getHigh(10000000) returns : " << endl; 
  printarray(binneddist->getHigh(10000000)); 

  cout << "Moments(0) returns : " << endl; 
  printarray(binneddist->Moments(0)); 
  cout << "Moments(1) returns : " << endl; 
  printarray(binneddist->Moments(1)); 
  cout << "Moments(2) returns : " << endl; 
  printarray(binneddist->Moments(2)); 
  cout << "Moments(3) returns : " << endl; 
  printarray(binneddist->Moments(3)); 


  cout << "getDim(-1) returns : " << binneddist->getdim(-1) << endl; 
  cout << "getDim(0) returns : " << binneddist->getdim(0) << endl; 
  cout << "getDim(1) returns : " << binneddist->getdim(1) << endl; 
  cout << "getDim(10000000) returns : " << binneddist->getdim(10000000) << endl; 
  
  cout << "Type: \n" << binneddist->getRecordType()->toString() << endl;
 
  cout << "getDataSetSize() returns: " << binneddist->getDataSetSize() << endl;
  
  cout << "lower: ";
  printarray(binneddist->lower());
  cout << "upper: ";
  printarray(binneddist->upper());
  cout << "Mean: ";
  printarray(binneddist->Mean());
  cout << "StdDev: ";
  printarray(binneddist->StdDev());
}
