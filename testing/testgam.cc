#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <getopt.h>
#include <ACG.h>
#include <Gamma.h>

int
main(int argc, char** argv)
{
  int c;
  int iseed=11;
  int N=10000;
  double alpha=0.5;

  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
      {
	{"alpha", 1, 0, 0},
	{"seed", 1, 0, 0},
	{"Number", 1, 0, 0},
	{0, 0, 0, 0}
      };

      c = getopt_long (argc, argv, "a:s:N:",
		       long_options, &option_index);
      if (c == -1)
	break;
      
      switch (c)
	{
	case 0:
	  if (long_options[option_index].name == string("alpha")) {
	    if (optarg) alpha = atof(optarg);
	    else {
	      cerr << "Need value alpha (float)\n";
	      exit(-1);
	    }
	  }
	  if (long_options[option_index].name == string("seed")) {
	    if (optarg) iseed = atoi(optarg);
	    else {
	      cerr << "Need value seed (int)\n";
	      exit(-1);
	    }
	  }
	  if (long_options[option_index].name == string("Number")) {
	    if (optarg) N = atoi(optarg);
	    else {
	      cerr << "Need value Number (int)\n";
	      exit(-1);
	    }
	  }
	  break;
	  
	case 'a':
	  alpha = atof(optarg);
	  break;
	  
	case 's':
	  iseed = atoi(optarg);
	  break;
	  
	case 'N':
	  N = atoi(optarg);
	  break;
	  
	case '?':
	  break;
	  
	default:
	  cerr << "?? getopt returned character code " << oct <<  c << dec << "\n";
	}
    }
  
  ACG gen(iseed, 20);
  Gamma gamma(alpha, &gen);

  for (int i=0; i<N; i++)
    cout << setw(15) << gamma() << endl;

  exit (0);
}

