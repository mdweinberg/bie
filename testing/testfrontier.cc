
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <fstream>
using namespace std;

#include <bieTags.h>
#include <MappedGrid.h>
#include <KdTessellation.h>
#include <QuadGrid.h>
#include <ContainerTessellation.h>
#include <BinaryTessellation.h>
#include <SquareTile.h>
#include <Histogram1D.h>
#include <DataTree.h>
#include <DummyModel.h>
#include <vector>
#include <RecordStream_Ascii.h>
#include <Frontier.h>
#include <FrontierExpansionHeuristic.h>
#include <Node.h>

#if PERSISTENCE_ENABLED
#include <TestUserState.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#endif

using namespace BIE;

void printFrontier(vector<int>,ostream & outputstream);
void mappedgridtests();
void quadgridtests();
void binarytesstests();
void kdtesstests();
void containertesstests();
void commontests(Tessellation *,string);
void runtests(Tessellation * tess, fstream &);

/*******************************************************************************
* This runs several tests on frontier operations using all tessellation types.
*******************************************************************************/
int main()
{
  try 
  { 
    system("rm -rf " PERSISTENCE_DIR);
    
    /*****************************************************************************
    * Tests of frontier operations with Mapped Grid Tessellation.
    *****************************************************************************/
    cout << "\n\n\n"<< "************* Mapped Grid tests *************** " << endl;
    mappedgridtests();
    
    /*****************************************************************************
    * Tests of frontier operations with Quad Grid Tessellation.
    *****************************************************************************/
    cout << "\n\n\n" << "************* Quad Grid tests *************** " << endl;
    quadgridtests();
  
    /*****************************************************************************
    * Tests of frontier operations with Binary Tessellation.
    *****************************************************************************/
    cout << "\n\n\n" << "************* Binary tessellation tests ***** " << endl;
    binarytesstests();
    
    /*****************************************************************************
    * Tests of frontier operations with Kd Tessellation.
    *****************************************************************************/
    cout << "\n\n\n" << "************* Kd tessellation tests ***** " << endl;
    kdtesstests();
    
    /*****************************************************************************
    * Tests of frontier operations with container tessellation.
    *****************************************************************************/
    cout << "\n\n\n" << "************* Container tess tests ***** " << endl;
    containertesstests();
   
    cout << "\n\n\n" << "************* Test finished *********" << endl;
  }
  catch (BIEException e)
  { cout << "Caught this: " << e.getErrorMessage() << endl; }
  
}
  
void printFrontier(vector<int> frontier_value, ostream & outputstream) 
{
    vector <int>::iterator     it;

    outputstream << "---- new frontier is: ("; 
    for (it=frontier_value.begin();it!=frontier_value.end();it++)
        outputstream << (*it) << ",";
    outputstream << ")" << endl;
}

/*******************************************************************************
* MAPPED GRID.
*******************************************************************************/
void mappedgridtests()
{
  /*****************************************************************************
  * Constructor tests - inputing bad values to test outcome.
  *****************************************************************************/
  SquareTile * tile = new SquareTile();
  MappedGrid * mg;
  
  try { mg = new MappedGrid(tile, 0, -1, -1.0, 1.0, 10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { mg = new MappedGrid(tile, 0, 0, -1.0, 1.0, 10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { mg = new MappedGrid(tile, 0, 1, 0, -1, 10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { mg = new MappedGrid(tile, 0, 1, 0,  0, 10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { mg = new MappedGrid(tile, 0, 1, 0,  1, 0, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { mg = new MappedGrid(tile, 0, 1, 0,  1, 10, 0); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}

  /*****************************************************************************
  * Common tests.
  *****************************************************************************/
  MappedGrid *mappedgrid = new MappedGrid(tile, -M_PI, M_PI, -1.0, 1.0, 10, 10);
  commontests(mappedgrid,"mappedgrid");
  
  /*****************************************************************************
  * Destructor tests.
  *****************************************************************************/
  delete mappedgrid;
}

/*******************************************************************************
* QUADGRID
*******************************************************************************/
void quadgridtests()
{
  /*****************************************************************************
  * Constructor tests - inputing bad values to test outcome.
  *****************************************************************************/
  SquareTile * tile = new SquareTile();
  QuadGrid * qg;
  
  try { qg = new QuadGrid(tile, 0, -1, -1.0, 1.0, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { qg = new QuadGrid(tile, 0,  0, -1.0, 1.0, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { qg = new QuadGrid(tile, 0, 1, 0, -1, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { qg = new QuadGrid(tile, 0, 1, 0, 0, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { qg = new QuadGrid(tile, 0, 1, 0, 1, -1); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}

  /*****************************************************************************
  * Common tests.
  *****************************************************************************/
  qg = new QuadGrid(tile, -2.56, 2.56, -2.56, 2.56, 3);
  commontests(qg, "quadgrid");
  
  delete qg;
  
  //qg = new QuadGrid(tile, -10,10,-10,10, 10);
  //commontests(qg);

  /*****************************************************************************
  * Destructor tests
  *****************************************************************************/
  //delete qg;
}

/*******************************************************************************
* BINARY TESS TESTS.
*******************************************************************************/
void binarytesstests()
{
  /*****************************************************************************
  * Constructor tests - inputing bad values to test outcome.
  *****************************************************************************/
  Tile * tile = new SquareTile();
  BinaryTessellation * bt;
  
  try { bt = new BinaryTessellation(tile, 0, -1, -1.0, 1.0, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl; }
  try { bt = new BinaryTessellation(tile, 0,  0, -1.0, 1.0, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl; }
  try { bt = new BinaryTessellation(tile, 0, 1, 0, -1, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl; }
  try { bt = new BinaryTessellation(tile, 0, 1, 0, 0, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl; }
  try { bt = new BinaryTessellation(tile, 0, 1, 0, 1, -1); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl; }

  /*****************************************************************************
  * Common tests.
  *****************************************************************************/
  bt = new BinaryTessellation(tile, -2.56, 2.56, -2.56, 2.56, 6);
  commontests(bt, "binarytess");

  /*****************************************************************************
  * Destructor tests
  *****************************************************************************/
  delete bt;
}

void kdtesstests()
{
  /*****************************************************************************
  * Constructor tests - inputing bad values to test outcome.
  *****************************************************************************/
  RecordInputStream * inputstream = new RecordInputStream_Ascii("data.dat");
  SquareTile * tile = new SquareTile();
  KdTessellation * kd;
  
  try { kd = new KdTessellation(tile, inputstream, -1, 1.0, -10, 10, -10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { kd = new KdTessellation(tile, inputstream, 100, 0, -10, 10, -10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { kd = new KdTessellation(tile, inputstream, 100, -1, -10, 10, -10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { kd = new KdTessellation(tile, inputstream, 100, 2.0, -10, 10, -10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { kd = new KdTessellation(tile, inputstream, 100, 0.5, 10, 10, -10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { kd = new KdTessellation(tile, inputstream, 100, 0.5, 10, -10, -10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { kd = new KdTessellation(tile, inputstream, 100, 0.5, -10, 10, 10, 10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}
  try { kd = new KdTessellation(tile, inputstream, 100, 0.5, -10, 10, 10, -10); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl;}

  /*****************************************************************************
  * Common tests.
  *****************************************************************************/
  inputstream = new RecordInputStream_Ascii("data.dat");
  kd = new KdTessellation(tile, inputstream, 100, 1.0, -10, 10, -10, 10);
  commontests(kd, "kdtess");

  /*****************************************************************************
  * Destructor tests
  *****************************************************************************/
  delete kd;
}

/*******************************************************************************
* CONTAINER TESS TESTS.
*******************************************************************************/
void containertesstests()
{
  /*****************************************************************************
  * Constructor tests - inputing bad values to test outcome.
  *****************************************************************************/
  ContainerTessellation * ct;
  Tile * tile = new SquareTile();
  clivectortess tessl;

  tessl().push_back(new MappedGrid(tile, -M_PI, M_PI, -3.0, -1.1, 10, 10));
  tessl().push_back(new MappedGrid(tile, -M_PI, M_PI, -3.0, -1.1, 10, 10));
  try { ct = new ContainerTessellation(&tessl); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl; }

  tessl().clear();
  try { ct = new ContainerTessellation(&tessl); }
  catch (BIEException & e) { cout << e.getErrorMessage() << endl; }

  /*****************************************************************************
  * Common tests.
  *****************************************************************************/
  tessl().push_back(new QuadGrid(tile, -2.56, 2.56, -2.56, 2.56, 3));
  tessl().push_back(new MappedGrid(tile, 2.56, 4.56, 2.56, 4.56, 10, 10));
  ct = new ContainerTessellation(&tessl);
  tessl().clear();
  tessl().push_back(ct);
  ct = new ContainerTessellation(&tessl);

  commontests(ct,"conttest");

  /*****************************************************************************
  * Destructor tests
  *****************************************************************************/
  delete ct;
}

void commontests(Tessellation * tess, string name)
{
  
  string filename = name + "_actual";
  
  fstream actualoutput;
  actualoutput.open(filename.c_str(), fstream::out);
 
#if PERSISTENCE_ENABLED
  TestUserState testState("testfrontier", 1, true);
  testState.addObject(tess);
  TestSaveManager testSaveManager(&testState, BOOST_XML, BACKEND_FILE);
  testSaveManager.save();
#endif

  cout << "Running tests with normally constructed tess.\n" << endl;
  runtests(tess,actualoutput);
  actualoutput.close();

#if PERSISTENCE_ENABLED
  TestUserState *loadState;
  TestLoadManager testLoadManager(&loadState, "testfrontier", 1,
				  BOOST_XML, BACKEND_FILE);
  testLoadManager.load();

  Serializable* loadedObj = *(loadState->getTransClosure().objectlist.begin());
  filename = name + "_restored";
  fstream restoredoutput;
  restoredoutput.open(filename.c_str(), fstream::out);
  cout << "Running tests with reconstructed tess.\n" << endl;
  runtests(tess, restoredoutput);
  restoredoutput.close();
#endif
}

/*******************************************************************************
* Tests common to all tessellations.
*******************************************************************************/
void runtests(Tessellation * tess, fstream & outputstream)
{
  try {
    outputstream << "----- Starting common tests\n\n";
    
    /*****************************************************************************
    * Print the entire tessellation tree.
    *****************************************************************************/
    tess->PrintPreOrder(outputstream);
    
    Frontier * frontier = new Frontier(tess);
    printFrontier(frontier->ExportFrontier(),outputstream);
      
    /*****************************************************************************
    * Mutation tests
    *****************************************************************************/
    outputstream << "\n\n--Contracting one level. " << endl;
    frontier->UpDownLevels(-1);
    printFrontier(frontier->ExportFrontier(),outputstream);
    
    outputstream << "\n\n---Expanding one level. " << endl;
    frontier->UpDownLevels(1);
    printFrontier(frontier->ExportFrontier(),outputstream);
  
    /*****************************************************************************
    * RetractToTopLevel tests.
    *****************************************************************************/
    outputstream << " Retract to top level " << endl;
    frontier->RetractToTopLevel();
    printFrontier(frontier->ExportFrontier(),outputstream);
    
    /*****************************************************************************
    * Increase resolution tests.
    *****************************************************************************/
    outputstream << "\n\n---Increasing resolution by 2 levels (always increase heuristic)" << endl;
    AlwaysIncreaseResolution * heuristic = new AlwaysIncreaseResolution();
    
    frontier->IncreaseResolution(heuristic, 2);
    printFrontier(frontier->ExportFrontier(),outputstream);
    
    /*****************************************************************************
    * Selection tests
    *****************************************************************************/
    outputstream << "---Selecting 1,2,3,4,5,6,7,8,9 in frontier" << endl;
    vector<int> afewnodes;
    for(int i = 1; i <= 9; i++)
    { afewnodes.push_back(i); }
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier(),outputstream);
    
    afewnodes.clear();
  
    outputstream << "---Selecting 10,14,18,22 in frontier" << endl;
    afewnodes.push_back(10);
    afewnodes.push_back(14);
    afewnodes.push_back(18);
    afewnodes.push_back(22);
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier(),outputstream);
    
    afewnodes.clear();
    outputstream << "---Trying to selecting -1, 100000, 2,3,4 in frontier" << endl;
    afewnodes.push_back(-1);
    afewnodes.push_back(100000);
    afewnodes.push_back(2);
    afewnodes.push_back(3);
    afewnodes.push_back(4);
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier(),outputstream);
  
    /*****************************************************************************
    * FindInFrontier tests.
    *****************************************************************************/
    outputstream << "Found (0.0, 0.0) ? " << (bool) frontier->Find(0,0) << endl;
    outputstream << "Found (-3, -0.9) ? " << (bool) frontier->Find(-3,-0.9) << endl;
    outputstream << "Found (10, 9) ? " << (bool) frontier->Find(10,9) << endl;
    
    /*****************************************************************************
    * FindAll tests.
    *****************************************************************************/
    vector<int> found;
    
    tess->FindAll(0,0, found); 
    outputstream << "FindAll (0,0) size :  " << found.size() << endl;
    tess->FindAll(-3,-0.9, found); 
    outputstream << "FindAll (-3,-0.9) size:  " << found.size() << endl;
    tess->FindAll(10,9, found); 
    outputstream << "FindAll (10,9) size: " << found.size() << endl;
  
    /*****************************************************************************
    * Get Tile tests
    *****************************************************************************/
    outputstream << "Printing tiles 0, 1 " << endl;
    tess->GetTile(0)->printTile(outputstream);
    tess->GetTile(1)->printTile(outputstream);
    
    outputstream << "GetTile(-1):   " << tess->GetTile(-1) << endl;
    
    /*****************************************************************************
    * Is valid tests.
    *****************************************************************************/
    outputstream << "Is valid ID : -1 : " << tess->IsValidTileID(-1) << endl;
    outputstream << "Is valid ID : 0 : " << tess->IsValidTileID(0) << endl;
    outputstream << "Is valid ID : 99 : " << tess->IsValidTileID(99) << endl;
    outputstream << "Is valid ID : 100 : " << tess->IsValidTileID(100) << endl;
    outputstream << "Is valid ID : 10000 : " << tess->IsValidTileID(10000) << endl;
    
    /*****************************************************************************
    * Number tiles test.
    *****************************************************************************/
    outputstream << "Number of tiles: " << tess->NumberTiles() << endl;
    
    /*****************************************************************************
    * GetRootTiles test
    *****************************************************************************/
    vector<Node*> rootnodes = tess->GetRootNodes();
    
    outputstream << "Printing root nodes: " << endl;
    for (int i = 0; i < (int) rootnodes.size(); i++)
    { outputstream << rootnodes[i]->ID() << ", "; }
    outputstream << endl;
    
    /*****************************************************************************
    * GetRootNodes test
    *****************************************************************************/
    outputstream << "Printing root tiles:" << endl;
    vector<int> roottiles = tess->GetRootTiles();
    
    for (int i = 0; i < (int) roottiles.size(); i++)
    { outputstream << roottiles[i] << ","; } 
    outputstream << endl;
    
    /*****************************************************************************
    * Loop method tests.
    *****************************************************************************/
    frontier->Reset();
    outputstream << "Looping through all tiles in frontier" << endl;
    while(! (frontier->IsDone()))
    {
      outputstream << "ID: " << frontier->CurrentItem() << "  ";
      frontier->Next();
    }
    outputstream << "First and last in frontier: " << endl;
    outputstream << "first " << frontier->First() << endl;
    outputstream << "last " << frontier->Last() << endl;
        
    /*****************************************************************************
    * Some additional mutation tests.
    *****************************************************************************/
    outputstream << "\nSome additional mutation tests\n";
    
    for (int i = 1; i <= 10; i++)
    {
      frontier->UpDownLevels(1);
      outputstream << "\nAfter expanding " << i << " times." << endl;
      printFrontier(frontier->ExportFrontier(),outputstream);  
    }
    
    for (int i = 1; i <= 10; i++)
    {
      frontier->UpDownLevels(-1);
      outputstream << "\nAfter contracting " << i << " times." << endl;
      printFrontier(frontier->ExportFrontier(),outputstream);  
    }
    
    /*****************************************************************************
    * Heuristic tests.
    *****************************************************************************/
    RecordInputStream * ris = new RecordInputStream_Ascii("data.dat");
    Histogram1D * hist      = new Histogram1D(0,20,20,"mag1");
    
    DataTree * dist = new DataTree(ris,hist,tess);
    
    FrontierExpansionHeuristic * h = new DataPointCountHeuristic(dist,50);
    frontier = dist->GetDefaultFrontier();
    
    for (int i = 1; i <= 10; i++)
    {
      outputstream << "\nPoints per tile : " << endl;
      for (dist->Reset(); ! dist->IsDone(); dist->Next())
      {
        outputstream << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	outputstream << dist->CurrentItem()->getDataSetSize() << "), ";
      }

      frontier->IncreaseResolution(h);
      outputstream << "\nAfter expanding with  DataPointCountHeuristic " << i << " times." << endl;
      printFrontier(frontier->ExportFrontier(),outputstream); 
    }
    
    h = new KSDistanceHeuristic(dist,0.1);
    frontier->RetractToTopLevel();
    
    for (int i = 1; i <= 10; i++)
    {
      outputstream << "\nPoints per tile : " << endl;
      for (dist->Reset(); ! dist->IsDone(); dist->Next())
      {
        outputstream << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	outputstream << dist->CurrentItem()->getDataSetSize() << "), ";
      }

      frontier->IncreaseResolution(h);
      outputstream << "\nAfter expanding with KSDistanceHeuristic " << i << " times." << endl;
      printFrontier(frontier->ExportFrontier(),outputstream); 
    }
          
    outputstream << "Finished COMMON tests\n";
  }
  catch (BIEException & e)
  { cerr << e.getErrorMessage() << endl; }
}
