#include <string>
#include <vector>
#include <sstream>
using namespace std;

#include <BasicType.h>
#include <TypedBuffer.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStream.h>
#include <RecordInputStream.h>
#include <RecordStream_Ascii.h>
#include <RecordStream_NetCDF.h>
#include <RecordOutputStream.h>
#include <RecordStreamFilter.h>
#include <BinaryFilters.h>
#include <UnaryFilters.h>
#include <SetFilters.h>
#include <BIEException.h>

using namespace BIE;

int main ()
{
  /*****************************************************************************
  * Filter test.
  *****************************************************************************/
  try 
  {
    RecordInputStream * risa = new RecordInputStream_Ascii("../examples/Galaxy/data/galaxy.data.3");
    // RecordInputStream * risa = new RecordInputStream_NetCDF("test2.cdf");
    cout << risa->eos() << "  " << risa->error() << endl;
  
    RecordStreamFilter * filter = new MuFilter(risa, 0.67);
  
    filter->connect("b", "x");
    filter->renameOutputField("mu", "y");
  
    risa = risa->filterWith(3, filter);
   
    cout << "This filter is: " << filter->toString();

    cout << risa->getBuffer()->toString() << endl;

    cout << risa->nextRecord() << endl;
    cout << risa->getBuffer()->toString() << endl;
    cout << risa->nextRecord() << endl;
    cout << risa->getBuffer()->toString() << endl;
    cout << risa->nextRecord() << endl;
    cout << risa->getBuffer()->toString() << endl;
    
    cout << filter->toString();

  }
  catch (BIEException e)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << e.getErrorMessage() << endl;
  }
  catch (...)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << "not a bie exception" << endl;
  }

  return 0;
}
