/**
   For testing Marginal Likelihood class
*/

#include <iostream>
#include <iomanip>
#include <typeinfo>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>

#include <ACG.h>
#include <BIEconfig.h>
#include <gvariable.h>
#include <Uniform.h>

#include <BIEmpi.h>
#include <EnsembleDisc.h>
#include <MetropolisHastings.h>
#include <LikelihoodComputationSerial.h>
#include <MultiDimGaussMixture.h>
#include <DifferentialEvolution.h>
#include <MarginalLikelihood.h>
#include <GelmanRubinConverge.h>
#include <RunOneSimulation.h>

#include <boost/program_options.hpp>

namespace po = boost::program_options;
using namespace BIE;

/**
   Test routine for data accumulation
*/
int
main(int argc, char** argv)
{
  //
  // Initialize mpi
  //
  mpi_init(argc, argv);

  BIE::mpi_used = 1;

  //
  // User variables
  //
  bool use_vta=false, use_two=false, use_close=false, use_full=true;
  bool batch=false, dump=false;
  int ndim, nsteps, nboot, strd, bucket, ssamp, ngood, inner, blobs, keep;
  double sigma2, frac, comp0, comp1, comp2, delta, margin, trim;
  string outfile, comment, ttype, prefix;
  unsigned Ns;

  //--------------------------------------------------
  // Declare the supported options.
  //--------------------------------------------------

  po::options_description desc("Available options");
  desc.add_options()
    ("help,h",	"Produce help message")
    ("version,v",										"Display version info and exit")
    ("VTA,V",	"compute the marginal likelihood using the original VTA method")
    ("TWO,t", 	"create a two-blob distribution")
    ("dump,D", 	"dump the intermediate marginal likelihood evaluations")
    ("batch,B", "use batch mode rather than sampling with replacment")
    ("close,c",	"make overlapping blobs")
    ("enclosed,e",
     		"used enclosed rather than full volume")
    ("ndim,d",			po::value<int>(&ndim)->default_value(2),			"Number of dimensions")
    ("nsteps,n", 		po::value<int>(&nsteps)->default_value(400000),			"Number of MCMC steps")
    ("ngood,g", 		po::value<int>(&ngood)->default_value(100000),			"Number of MCMC steps")
    ("sample,M", 		po::value<int>(&ssamp)->default_value(-1),			"Sample size for bootstrap or random sample")
    ("nboot,b", 		po::value<int>(&nboot)->default_value(64),			"Number of bootstrap samples")
    ("bucket,m", 		po::value<int>(&bucket)->default_value(1),			"Number of points per bucket")
    ("inner,r", 		po::value<int>(&inner)->default_value(1000),			"Number of states in posterior chain used to define importance region")
    ("keep,k", 			po::value<int>(&keep)->default_value(400000),			"Number of states to retain in FIFO queue per processor")
    ("fraction,f", 		po::value<double>(&frac)->default_value(0.01),			"Fraction of posterior chain used to define importance region")
    ("central,0", 		po::value<double>(&comp0)->default_value(0.5),			"Central blob position")
    ("offset1,1", 		po::value<double>(&comp1)->default_value(0.3),			"Offset from center for distant blobs")
    ("offset2,2", 		po::value<double>(&comp2)->default_value(0.1),			"Offset from center for close blobs")
    ("weight,w", 		po::value<double>(&delta)->default_value(0.1),			"Weight bias for blobs")
    ("sigma2,s", 		po::value<double>(&sigma2)->default_value(0.01),		"Variance per dimension for each normal component")
    ("stride,j", 		po::value<int>(&strd)->default_value(1),			"Spacing of states or frequency (e.g. 1: use all)")
    ("resample,R", 		po::value<unsigned>(&Ns)->default_value(100000),		"Size of new (re)sample")
    ("blobs", 			po::value<int>(&blobs)->default_value(0),			"Use random blob model with specifed number (e.g. 0: no blobs)")
    ("margin", 			po::value<double>(&margin)->default_value(5.0),			"Number of widths for margin offset from the unit hypercube")
    ("trim", 			po::value<double>(&trim)->default_value(-1.0),			"Volume trimming fraction for VTA")
    ("output,o", 		po::value<string>(&outfile)->default_value(""),			"Output log file name (not used if not set)")
    ("prefix,P", 		po::value<string>(&prefix)->default_value("marglike"),		"Prefix for debugging files")
    ("tree-type,T", 		po::value<string>(&ttype)->default_value("ORB"),			"Tree type, one of KD, ORB, TNT")
    ("comment", 		po::value<string>(&comment)->default_value(""),			"Add a comment to the log file for this evaluation")
    ;

  po::variables_map vm;

  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);    
  } catch(boost::program_options::error& e){
    std::cerr << "Invalid_option_value exception thrown parsing config file:"
	      << std::endl << e.what() << std::endl;
    MPI_Finalize();
    return 2;
  } catch(std::exception e){
    std::cerr <<"Exception thrown parsing config file:" 
	      << std::endl << e.what() << std::endl;
    MPI_Finalize();
    return 2;
  }

  if (vm.count("VTA"))        use_vta = true;

  if (vm.count("TWO"))        use_two = true;

  if (vm.count("close"))    use_close = true;

  if (vm.count("enclosed"))  use_full = false;

  if (vm.count("batch"))        batch = true;

  if (vm.count("dump"))          dump = true;

  if (vm.count("help")) {
    if (myid==0) {
      std::cout << std::setfill('-') << setw(76) << '-' 
		<< std::endl << std::setfill(' ')
		<< "Bayesian marginal likelihood computation using the resampled "
		<< "VTA algorithms." << std::endl
		<< "The VTA uses kd-tree, ORB-tree or 2^n-tree"
		<< "quadrature cells." << std::endl
		<< "[This version computes bootstrap or batch confidence limits]" 
		<< std::endl
		<< std::setfill('-') << setw(76) << '-' 
		<< std::endl << std::setfill(' ')
		<< "Usage: " << argv[0] << " [options]" << std::endl
		<< std::setfill('-') << setw(76) << '-' 
		<< std::endl << std::setfill(' ')
		<< desc << std::endl;
    }
    MPI_Finalize();
    return 1;
  }

  BIEgen              = new BIEACG(11, 20);
  StateInfo *si       = new StateInfo(ndim);
  EnsembleDisc *sstat = new EnsembleDisc(si);

  sstat->setNKeep(keep*numprocs);

  if (myid==0) cout << "Class: " << typeid(*sstat).name() << endl;

  CauchyDist uc(0.001);
  clivectordist eps(ndim, &uc);

  //
  // State metadata
  //
  clivectors labs(ndim);
  for (int n=0; n<ndim; n++) {
    ostringstream sout;
    sout << "Pos" << n+1;
    labs.setval(n, sout.str());
  }
  si->labelAll(&labs);

  //
  // Minimum number of chains
  //
  int mchains  = 32;
  double width = 0.1;

  //
  // Prior
  //
  boost::shared_ptr<UniformDist> unif;
  if (use_two || blobs)
    unif = boost::shared_ptr<UniformDist>(new UniformDist(0.0, 1.0));
  else
    unif = boost::shared_ptr<UniformDist>(new UniformDist(-0.5, 0.5));

  clivectordist dist(ndim, unif.get());
  Prior prior(si, &dist);

  //
  // Gelman-Ruben convergence scheme
  //
  GelmanRubinConverge convrg(1000, sstat, "run9.0");
  convrg.setAlpha(0.05);
  convrg.setNgood(ngood);
  convrg.setNskip(1000);
  convrg.setNoutlier(1000);
  convrg.setPoffset(-30.0);
  convrg.setMaxout(2);

  //
  // Metropolis-Hastings Monte Carlo algorithm
  //
  MCAlgorithm *mca = new MetropolisHastings();

  //
  // Differential evolution
  //
  LikelihoodComputation *like = new LikelihoodComputationSerial();
  DifferentialEvolution de(si, mchains, &eps, &convrg, &prior, like, mca);

  de.SetLinearMapping(1);
  de.SetJumpFreq(10);
  de.SetControl(1);
  de.NewGamma(0.1);

  Simulation *sim = &de;

  //
  // Simple Gaussian model
  //
  MultiDimGaussMixture gauss(sigma2);
  if (blobs) {
    std::vector<double> wghts(ndim);
    if (myid==0) {
      Uniform unit(0.0, 1.0, BIEgen);
      for (int i=0; i<blobs; i++) wghts[i] = unit();
      double sum = 0.0;
      for (int i=0; i<blobs; i++) sum += wghts[i];
      for (int i=0; i<blobs; i++) wghts[i] /= sum;
    }

    MPI_Bcast(&wghts[0], ndim, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    clivectord v(ndim);

    for (int i=0; i<blobs; i++) {

      if (myid==0) {
	double offset = margin*sqrt(sigma2);
	Uniform unif(0.5-offset, 0.5+offset, BIEgen);
	for (int i=0; i<ndim; i++) v()[i] = unif();
      }

      MPI_Bcast(&v()[0], ndim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
      
      gauss.addComponent(wghts[i], &v);
    }
  } else if (use_two) {
    clivectord v(ndim, comp0);
    if (use_close) {
      v()[0] += comp2;
      v()[1] += comp2;
    } else {
      v()[0] += comp1;
      v()[1] += comp1;
    }
    gauss.addComponent(0.5 + delta, &v);
    if (use_close) {
      v()[0] -= 2.0*comp2;
      v()[1] -= 2.0*comp2;
    } else {
      v()[0] -= 2.0*comp1;
      v()[1] -= 2.0*comp1;
    }
    gauss.addComponent(0.5 - delta, &v);
  }

  sim->SetUserLikelihood(&gauss);

  //
  // Define simulation output file
  //
  if (outfile.size()) {
    ostringstream sout;
    sout << outfile << ".statelog";
    BIE::outfile = sout.str();
  }

  //
  // Run the simulation
  //
  RunOneSimulation run(nsteps, width, sstat, &prior, sim);

  run.Run();

  //
  // Communicate and tabulate
  //
  sstat->ComputeDistribution();

  //
  // Compute the marginal likelihood
  //
  Ensemble *ens = sstat;
  MarginalLikelihood *marglike = new MarginalLikelihood(ens, sim);

  marglike->setNBoot(nboot);
  marglike->setBatch(batch);
  marglike->setPrefix(prefix);
  marglike->setTreeType(ttype);
  marglike->setSubsample(ssamp);
  marglike->setBucketCount(bucket);
  marglike->setFullVolume(use_full);

  if (dump)
    marglike->setDump();

  if (outfile.size()) {
    marglike->setOutfile(outfile);
    marglike->setComment(comment);
  }

  if (use_vta) {
    marglike->useVTA();
    if (trim>0.0) marglike->setTrimVolume(trim);
  } else {
    if (inner>0)
      marglike->useImportance(inner, Ns);
    else
      marglike->useImportance(frac, Ns);
    marglike->setSampleFrac(1.0/nboot);
  }

  marglike->compute();

  //
  // Close down MPI
  //
  MPI_Finalize();

  //
  // Delete instances
  //
  delete sstat;
  delete BIEgen;
  delete si;
  delete mca;
  delete like;
  delete marglike;

  return 0;
}

