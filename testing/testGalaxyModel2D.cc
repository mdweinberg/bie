// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#define IS_MAIN
#define USE_ND

#include <HistogramND.h>
#ifdef USE_ND
#include <GalaxyModelND.h>
#include <GalaxyModelNDCached.h>
#else
#include <GalaxyModelTwoD.h>
#include <GalaxyModelTwoDCached.h>
#endif

using namespace BIE;

/**
   @name testGalaxyModel2D: main
   Check out of GalaxyModel (two flux case)
*/
int
main(int argc, char** argv)
{
  const double onedeg = M_PI/180.0;

  double L = 30.0;
  double B = 5.0;

  bool cached = true;

				// Maximum line-of-sight distance
  double RMAX = 100.0;
				// Integration knots
  int NUM = 200;
				// Default init file
  string initfile = "init.dat.galaxy";
  int nmix = 2;			// Two component mixture


  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "L:B:R:M:i:n:xh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'L': L = atof(optarg); break;
      case 'B': B = atof(optarg); break;
      case 'R': RMAX = atof(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'n': NUM = atoi(optarg); break;
      case 'i': initfile.erase(); initfile = optarg; break;
      case 'x': cached = false; break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-L L\t\tlongitude in degrees\n";
	msg += "\t-B B\t\tlatitude in degrees\n";
	msg += "\t-R R\t\tlline-of-sight integration maximum\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-n n\t\tnumber of integration knots\n";
	msg += "\t-i file\t\tname of initialization file\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  L *= onedeg;
  B *= onedeg;

  // Set global parameters
  //

#ifdef USE_ND
  GalaxyModelND::RMAX = RMAX;
  GalaxyModelND::NUM = NUM;
#else
  GalaxyModelTwoD::RMAX = RMAX;
  GalaxyModelTwoD::NUM = NUM;
#endif

  // Hold instantiation of classes define the simuation
  //
#ifdef USE_ND
  GalaxyModelND *model;
#else
  GalaxyModelTwoD *model;
#endif
  HistogramND *histo;

  int ndim = 2;			// Two model parameters

				// Factor for 1D histogram in each tile
  clivectord lo_b(2, 6.0), hi_b(2, 16.0), w_b(2, 1.0);

  clivectors names;
  names().push_back("namedattribute");
  names().push_back("namedattribute2");
  
  histo = new HistogramND(&lo_b, &hi_b, &w_b, &names);

				// Galaxy model
  if (cached)
#ifdef USE_ND
    model = new GalaxyModelNDCached(ndim, nmix, histo);
#else
    model = new GalaxyModelTwoDCached(ndim, nmix, histo);
#endif
  else
#ifdef USE_ND
    model = new GalaxyModelND(ndim, nmix, histo);
#else
    model = new GalaxyModelTwoD(ndim, nmix, histo);
#endif

    vector<double> winit(nmix);
  vector< vector<double> > pinit(nmix);

  ifstream initf(initfile.c_str());

  for (int i=0; i<nmix; i++) {
    initf >> winit[i];
    pinit[i] = vector<double>(ndim);
    for (int j=0; j<ndim; j++) initf >> pinit[i][j];
  }
  
  model->Initialize(winit, pinit);
  double norm = model->NormEval(L, B, histo);

  cout << " Norm = " << norm << endl;

  model->ResetCache();

  vector<double> ans = model->Evaluate(L, B, histo);

  for (int i=0; i<histo->numberData(); i++) {
    cout
      << setw(15) << histo->getLow(i)[0]
      << setw(15) << histo->getHigh(i)[0]
      << setw(15) << histo->getLow(i)[1]
      << setw(15) << histo->getHigh(i)[1]
      << setw(15) << ans[i]/norm
      << endl;
  }

}


