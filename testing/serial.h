#ifndef _serial_h
#define _serial_h

#include <boost/version.hpp>
#include <boost/archive/archive_exception.hpp>

#ifdef POLYMORPHIC
#include <boost/archive/polymorphic_xml_oarchive.hpp>
#include <boost/archive/polymorphic_xml_iarchive.hpp>
#include <boost/archive/impl/archive_serializer_map.ipp>
#else
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#endif

#include <boost/serialization/export.hpp>
#include <boost/serialization/serialization.hpp>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>

#endif
