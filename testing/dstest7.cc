#include <string>
#include <vector>
#include <strstream>
#include <BasicType.h>

#include <ArrayType.h>

#include <TypedBuffer.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStream.h>
#include <RecordInputStream.h>
#include <RecordStream_Ascii.h>
#include <RecordStream_Binary.h>
#include <RecordOutputStream.h>
#include <RecordStreamFilter.h>
#include <BinaryFilters.h>
#include <UnaryFilters.h>
#include <SetFilters.h>
#include <BIEException.h>

using namespace BIE;

int main ()
{
  /*****************************************************************************
  * Array Filter test.
  *****************************************************************************/
  //  try 
  //  {
    ArrayType *atr = new ArrayType(BasicType::Real, 4);

    RecordInputStream* ris = new RecordInputStream_Ascii("dstest7.dat");
    cout << ris->getBuffer()->getType()->toString() << endl;

    RecordType *rrt = new RecordType();
    RecordType *xrrt = rrt->insertField(1, "xarray", atr);
    RecordType *xyrrt = xrrt->insertField(2, "yarray", atr);
    RecordType *dxyrrt = xyrrt->insertField(3, "darray", atr);
    RecordType *mdxyrrt = dxyrrt->insertField(4, "r2", BasicType::Real);
    RecordOutputStream* ros = new RecordOutputStream(mdxyrrt);
    RecordOutputStream* aros = new RecordOutputStream_Ascii(ros, "dstest7.out", true);
    cout << ros->getBuffer()->getType()->toString() << endl;
    cout << aros->getBuffer()->getType()->toString() << endl;

    RecordStreamFilter* rsf = new SubtractionFilter(ris);
    cout << rsf->toString();
    // stream inp#1 plumped to filter inp#1,... 
    rsf->connect(1, 1);
    rsf->connect(2, 2);

    // field#2 is the output of filter
    RecordInputStream *nrisa = ris->filterWith(3, rsf);
    cout << rsf->toString();
    cout << nrisa->getBuffer()->toString() << endl;

    RecordStreamFilter* ssf = new MaxFilter(nrisa);
    cout << ssf->toString();
    ssf->connect(3,1);
    cout << ssf->toString();

    RecordInputStream *snrisa = nrisa->filterWith(4, ssf);
    cout << ssf->toString();
    cout << snrisa->getBuffer()->toString() << endl;


    while(!snrisa->eos()) {
      bool done = snrisa->nextRecord();
      if (!done) break;
      cout << snrisa->getBuffer()->toString();

      vector<double> xdbl = snrisa->getBuffer()->getRealArrayValue(1);
      vector<double> ydbl = snrisa->getBuffer()->getRealArrayValue(2);
      vector<double> sdbl = snrisa->getBuffer()->getRealArrayValue(3);
      double md = snrisa->getBuffer()->getRealValue(4);
      ros->setRealArrayValue(1,xdbl);
      ros->setRealArrayValue(2,ydbl);
      ros->setRealArrayValue(3,sdbl);
      ros->setRealValue(4,md);
      ros->pushRecord();
    }

    /*
  }
  catch (...)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << "not a bie exception" << endl;
  }
    */
}
