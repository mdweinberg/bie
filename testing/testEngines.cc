/*******************************************************************************
* This is a collection of tests that checks error checking and
* sanity saving requirements are enforced when creating archives, together with
* tests of generally tricky transitive closures.
*******************************************************************************/
#include <iostream>
#include <gvariable.h>
#include <config.h>

#include <Serializable.h>

#include <EngineConfig.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <BIEException.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#include <FileBackend.h>

#include <Node.h>
#include <SquareTile.h>
#include <AData.h>
#include <Frontier.h>
#include <NormalDist.h>
#include <Dirichlet.h>
#include <WeibullDist.h>

using namespace BIE;

/** This is for simple save/load testing. */
int main()
{
  Node* node = new Node(new SquareTile(1.0f,2.0f,3.0f,4.0f),10,10,0);
  Tile* tile = new SquareTile(1.0f,2.0f,3.0f,4.0f);
  Node node2(tile, 10,10,0);
  AData* adata = new AData(node, "Node");
  NormalDist * dist1 = new NormalDist(0.1, 0.1);
  vector<double> a(3, 1.0);
  Dirichlet * dist2 = new Dirichlet(&a);
  WeibullDist * dist3 = new WeibullDist();
  StateInfo * si = new StateInfo(3);
  State * s = new State(si, a);

  // Pack the state for saving
  //
  TestUserState saveState("testEngines1", 10, true);

  saveState.addObject(node);
  saveState.addObject(adata);
  saveState.addObject(dist1);
  saveState.addObject(dist2);
  saveState.addObject(dist3);
  saveState.addObject(si);
  saveState.addObject(s);

  try {
    TestSaveManager testSaveManager(&saveState, BOOST_TEXT, BACKEND_FILE);
    testSaveManager.save();
  } catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }

  // Some time later
  //
  TestUserState *loadState;
  try {
    TestLoadManager testLoadManager(&loadState, "testEngines1", 11, 
				    BOOST_TEXT, BACKEND_FILE);
    testLoadManager.load();
  } catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on LOAD! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }

  cout << "Try to resave state . . ." << endl;

  // save the loaded stuff to check for correctness
  //
  try {
    TestSaveManager testSaveManager2(loadState, BOOST_TEXT, BACKEND_FILE);
    testSaveManager2.save();
  }  catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }
  
}
