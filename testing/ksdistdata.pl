#!/usr/bin/perl -w
#*******************************************************************************
#* Distribution data generator for KS distance tests.
#*******************************************************************************

#*******************************************************************************
#* Lines that turn on warnings, strict pragmas.
#*******************************************************************************
use strict;
use diagnostics;
use Normal;

sub normal($$$);

my $USAGE = "USAGE: ksdistdata.pl (normal mean stddev)|(uniform min max)".
            " #instances";

if (@ARGV != 4) { die $USAGE;}

my $disttype = $ARGV[0];

print "Real \"parameter\"\n\n";

if ($disttype eq "normal")
{
  my $mean      = $ARGV[1];
  my $stddev    = $ARGV[2];
  my $instances = $ARGV[3];

  if ($stddev < 0 || $instances < 0) 
  { die "Bad value for sdtdev or instances.\n"; }
  
  print "# Normal dist, mean $mean, stddev $stddev, instances $instances\n";

  my $distribution = new Normal;
  $distribution->mu($mean);
  $distribution->sigma($stddev);

  for (my $i=0; $i<$instances; $i++)
  { 
    my ($inst) = $distribution->rand();
    print "$inst\n"; 
  }
}
elsif ($disttype eq "uniform")
{
  my $min       = $ARGV[1];
  my $max       = $ARGV[2];
  my $instances = $ARGV[3];

  if ($min > $max) { die "Min must be smaller than max.\n"; }
  
  if ($instances < 0)  { die "Bad value for instances.\n"; }

  print "# Uniform dist, min $min, max $max, instances $instances\n";

  for (my $i=0; $i<$instances; $i++)
  { print "" . (rand() * ($max-$min)) + $min . "\n"; }
}
else
{ die $USAGE; }
