#ifndef _A_h
#define _A_h

#include <serial.h>

class A
{
public:
  float x, y, z;
  A();

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_NVP(x);
    ar & BOOST_SERIALIZATION_NVP(y);
    ar & BOOST_SERIALIZATION_NVP(z);
  }
};

BOOST_CLASS_EXPORT_KEY(A)

#endif
