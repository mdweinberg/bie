#!/bin/bash

exec_name=$_

if [ ! $1 ]; then
    echo "Usage: "$exec_name" <number of processes>"
    exit -1
fi
j=`echo $1 | sed 's/[0-9]*//g'`
if [ "$j" != "" ]; then
    echo "Usage: "$exec_name" <number of processes>"
    exit -1
fi

echo "removing files from previous test session.."
rm -rf pdir/testBIEACG slave.output* testBIEACGmpi_values.*
echo "running testBIEACGmpi with "$1" processes"
mpirun -np $1 ./testBIEACGmpi
echo "diffing..."
diff testBIEACGmpi_values.before testBIEACGmpi_values.after
if [ "$?" -eq "0" ]; then
    echo "Test : SUCCESS"
else
    echo "Test : FAILED"
fi
