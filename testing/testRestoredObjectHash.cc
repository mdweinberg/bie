#include <iostream>
#include <string>
#include <RestoredObjectHash.h>
#include <UPO.h>

class A : public UPO {
private:
  string name;
  uint32 version;
public:
  A(string name, uint32 version)
  { this->name = name; this->version = version; }
  void printid() { cout << name << ", " << version << endl; }
};

int main()
{
  RestoredObjectHash h(16);
  
  const char * names [] = {"test", "alistair", "martin", "al", "herbie"};
  uint32 versionmin = 1;
  uint32 versionmax = 10;
  
  for (unsigned int nameindex = 0; nameindex < sizeof(names)/sizeof(char*); nameindex ++)
  {
    for (uint32 version = versionmin; version < versionmax; version++)
    {
      h.addRestored(new A(names[nameindex], version), names[nameindex], version);
      cout << "Added " << names[nameindex] << ", " << version << endl;
    }
  }
  
  for (unsigned int nameindex = 0; nameindex < sizeof(names)/sizeof(char*); nameindex ++)
  {
    for (uint32 version = versionmin; version < versionmax; version++)
    {
      cout << names[nameindex] << ", " << version << endl;
      cout << "In hash? : " << h.alreadyRestored(names[nameindex], version) << endl;

      UPO * result = h.getRestored(names[nameindex], version);
      ((A*) ((void *)result))->printid();
    }
  }
  
  for (unsigned int nameindex = 0; nameindex < sizeof(names)/sizeof(char*); nameindex ++)
  {
    for (uint32 version = versionmax; version < versionmax+10; version++)
    {
      cout << names[nameindex] << ", " << version << endl;
      cout << "In hash? : " << h.alreadyRestored(names[nameindex], version) << endl;
      cout << "Result of getRestored: " <<
               h.getRestored(names[nameindex], version) << endl;
    }
  }

  
}
