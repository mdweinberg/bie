#include <bieTags.h>
#include <Dirichlet.h>
#include <NormalDist.h>
#include <UniformDist.h>
#include <WeibullDist.h>
#include <InverseGammaDist.h>
#include <gvariable.h>
#include <math.h>

#if PERSISTENCE_ENABLED
#include <TestUserState.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#endif

using namespace BIE;

void compareandprint(double a, double b)
{
  cout << fabs(a - b) << " | " << a << " | " << b << endl;
}

void comparedists(Distribution * a, Distribution * b)
{
  compareandprint(a->lower()[0],  b->lower()[0]);
  compareandprint(a->upper()[0],  b->upper()[0]);
  compareandprint(a->Mean()[0],   b->Mean()[0]);
  compareandprint(a->StdDev()[0], b->StdDev()[0]);

  for (int i = 0; i < 10; i++)
  { compareandprint(a->Sample()[0], b->Sample()[0]); }
}

int main()
{
#if PERSISTENCE_ENABLED
  try
  {
    // Make the persistence directory something unique to us
    // to avoid interence with other tests.
    system("rm -rf " PERSISTENCE_DIR);
  
    // Initialize the random number generator.
    BIEgen = new BIEACG(11, 25);
    
    // Create some distributions.
    UniformDist * u       = new UniformDist(-2,3);
    NormalDist * g        = new NormalDist(4, 4);
    WeibullDist * w       = new WeibullDist(4, 6);
    InverseGammaDist * q  = new InverseGammaDist(1.2, 3.5);
    vector<double> a(4, 0.45);
    Dirichlet   * d       = new Dirichlet(&a);
    
    TestUserState saveState("testAnalytic", 0, true);

    saveState.addObject(BIEgen);
    saveState.addObject(u);
    saveState.addObject(g);
    saveState.addObject(w);
    saveState.addObject(q);
    saveState.addObject(d);

    {
      TestSaveManager testSaveManager(&saveState, BOOST_XML, BACKEND_FILE);
      testSaveManager.save();
    }
    
    // Restore the State
    TestUserState *restored;

    {
      TestLoadManager testLoadManager(&restored, "testAnalytic", 1,
				      BOOST_XML, BACKEND_FILE);
      testLoadManager.load();
    }
    
    vector<Serializable*> objectlist = restored->getTransClosure().objectlist;
    //
    // RTTI to ID class type
    //
    for (unsigned i=0; i<objectlist.size(); i++) {
      if (typeid(UniformDist) == typeid(objectlist[i])) {
	cout << i << " is UniformDist index" << endl;
      }
      if (typeid(NormalDist) == typeid(objectlist[i])) {
	cout << i << " is NormalDist index" << endl;
      }
      if (typeid(WeibullDist) == typeid(objectlist[i])) {
	cout << i << " is WeibullDist index" << endl;
      }
      if (typeid(InverseGammaDist) == typeid(objectlist[i])) {
	cout << i << " is InverseGammaDist index" << endl;
      }
      if (typeid(Dirichlet) == typeid(objectlist[i])) {
	cout << i << " is Dirichlet index" << endl;
      }
    }

    // Compare the restored distributions to the dists we saved.
    cout << "UNIFORM DIST" << endl;
    comparedists(u, dynamic_cast<Distribution*>(objectlist[1]));
    cout << "GAUSSIAN DIST" << endl;
    comparedists(g, dynamic_cast<Distribution*>(objectlist[2]));
    cout << "WEIBULL DIST" << endl;
    comparedists(w, dynamic_cast<Distribution*>(objectlist[3])); 
    cout << "INVERSEGAMMA DIST" << endl;
    comparedists(q, dynamic_cast<Distribution*>(objectlist[4])); 
    cout << "DIRICHLET DIST" << endl;
    comparedists(d, dynamic_cast<Distribution*>(objectlist[5]));
  }
  catch (BIEException e)
  { cerr << e.getErrorMessage() << endl; }

#else

cout << "This test is only appropriate when persistence is enabled" << endl;

#endif  
}
