// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>

using namespace std;

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#define IS_MAIN

#include <GalaxyModelOneD.h>
#include <GalaxyModelOneDCached.h>
#include <GalaxyModelOneDHashed.h>

using namespace BIE;

/**
   @name testGalaxyModel: main
   Check out of GalaxyModel
*/
int
main(int argc, char** argv)
{
  const double onedeg = M_PI/180.0;

  double L = 30.0;
  double B = 5.0;

  int type = 2;

				// Maximum line-of-sight distance
  double RMAX = 100.0;
				// Integration knots
  int NUM = 200;
				// Default init file
  string initfile = "init.dat.galaxy";
  int nmix = 2;			// Two component mixture


  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'

  int c;
  while (1) {
    c = getopt (argc, argv, "L:B:R:M:i:n:t:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'L': L = atof(optarg); break;
      case 'B': B = atof(optarg); break;
      case 'R': RMAX = atof(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'n': NUM = atoi(optarg); break;
      case 'i': initfile.erase(); initfile = optarg; break;
      case 't': type = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-L L\t\tlongitude in degrees\n";
	msg += "\t-B B\t\tlatitude in degrees\n";
	msg += "\t-R R\t\tlline-of-sight integration maximum\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-n n\t\tnumber of integration knots\n";
	msg += "\t-i file\t\tname of initialization file\n";
	msg += "\t-t type\t\t1,2 or 3\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  L *= onedeg;
  B *= onedeg;

  // Set global parameters
  //

  // Hold instantiation of classes define the simuation
  //
  Model *model;
  Histogram1D *histo;

  int ndim = 2;			// Two model parameters

				// Factor for 1D histogram in each tile
  histo = new Histogram1D(6.0, 15.0, 1.0, "attribute");

				// Galaxy model
  switch (type) {
  case 0:
    GalaxyModelOneD::RMAX = RMAX;
    GalaxyModelOneD::NUM = NUM;
    model = new GalaxyModelOneD(ndim, nmix, histo);
    break;
  case 1:
    GalaxyModelOneDCached::RMAX = RMAX;
    GalaxyModelOneDCached::NUM = NUM;
    model = new GalaxyModelOneDCached(ndim, nmix, histo);
    break;
  case 2:
  default:
    GalaxyModelOneDHashed::RMAX = RMAX;
    GalaxyModelOneDHashed::NUM = NUM;
    model = new GalaxyModelOneDHashed(ndim, nmix, histo);
    break;
  }

  vector<double> winit(nmix);
  vector< vector<double> > pinit(nmix);

  ifstream initf(initfile.c_str());

  for (int i=0; i<nmix; i++) {
    initf >> winit[i];
    pinit[i] = vector<double>(ndim);
    for (int j=0; j<ndim; j++) initf >> pinit[i][j];
  }
  
  double norm;

  switch (type) {
  case 0:
    ((GalaxyModelOneD*)model)->Initialize(winit, pinit);
    norm = ((GalaxyModelOneD*)model)->NormEval(L, B, histo);
    break;
  case 1:
    ((GalaxyModelOneDCached*)model)->Initialize(winit, pinit);
    norm = ((GalaxyModelOneDCached*)model)->NormEval(L, B, histo);
    break;
  case 2:
  default:
    ((GalaxyModelOneDHashed*)model)->Initialize(winit, pinit);
    norm = ((GalaxyModelOneDHashed*)model)->NormEval(L, B, histo);
    break;
  }

  cout << " Norm = " << norm << endl;

  model->ResetCache();

  vector<double> ans1 = model->Evaluate(L, B, histo);
  vector<double> ans = model->Evaluate(L+0.1, B+0.1, histo);


  for (int i=0; i<histo->numberData(); i++) {
    cout
      << setw(15) << histo->getLow(i)[0]
      << setw(15) << histo->getHigh(i)[0]
      << setw(15) << ans[i]/norm
      << endl;
  }

}


