/*******************************************************************************
* This is a collection of tests that checks error checking and
* sanity saving requirements are enforced when creating archives, together with
* tests of generally tricky transitive closures.
*******************************************************************************/
#include <iostream>
#include <bieTags.h>
#include <gvariable.h>
#include <config.h>

#include <EngineConfig.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <BIEException.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#include <FileBackend.h>

#include <VectorM.h>
#include <CVectorM.h>

#include <Serializable.h>

// For clivector
#include <Tessellation.h>
#include <Distribution.h>

using namespace BIE;

/* This is for simple save/load testing. */
int main() 
{
  system("rm -rf " PERSISTENCE_DIR);

  TestUserState saveState("testPersistence_Vector", 10, true);

  // VectorM test
  VectorM v(3, 10);
  for (unsigned int i=3;i<=10;i++) {
    v[i] = i + 100.12; // just to make it double
  }
  saveState.addObject(&v);

  // MatrixM test
  MatrixM m(3, 8, 4, 10);
  for (unsigned int i=3;i<=8;i++) {
    for (unsigned int j=4;j<=10;j++) {
      m[i][j] = i + j + 100.12; // just to make it double
    }
  }
  saveState.addObject(&m);

  // Three_Vector test
  Three_Vector tv(111.11, 222.22, 333.33);
  saveState.addObject(&tv);

  // CVectorM test
  CVectorM cv(5, 9);
  for (unsigned int i=5; i<=9; i++) {
    cv[i] = std::complex<double>(i + 10.0, i + 100.0);
  }
  saveState.addObject(&cv);

  // CMatrixM test
  CMatrixM cm(2, 5, 3, 6);
  for (unsigned int i=2; i<=5; i++) {
    for (unsigned int j=3; j<=6; j++) {
      cm[i][j] = std::complex<double>(i + j + 10.0, i + j + 100.0);
    }
  }
  saveState.addObject(&cm);

  TestSaveManager testSaveManager(&saveState, BOOST_TEXT, BACKEND_FILE);
  testSaveManager.save();

  // Some time later
  
  TestUserState *loadState;
  TestLoadManager testLoadManager(&loadState, "testPersistence_Vector", 11,
				  BOOST_TEXT, BACKEND_FILE);
  testLoadManager.load();

  // save the loaded stuff to check for correctness
  TestSaveManager testSaveManager2(loadState, BOOST_TEXT, BACKEND_FILE);
  testSaveManager2.save();
}

