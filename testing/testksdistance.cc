#include <stdlib.h>
#include <iostream>
using namespace std;

#include <KSDistance.h>
#include <Histogram1D.h>
#include <HistogramND.h>
#include <RecordStream_Ascii.h>
#include <BasicType.h>

using namespace BIE;

int
main()
{
  
  KSDistance * ksd = new KSDistance();

  Histogram1D *  hist1 = new Histogram1D(-1, 1, 0.2, "fieldname");
  Histogram1D *  hist2 = new Histogram1D(-1, 1, 0.2, "othername");

  vector<SampleDistribution*> dists;

  /*****************************************************************************
  * First check that dodgy parameters are picked up.
  *****************************************************************************/
  try {
    cout << ksd->ksdistance(dists);
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  dists.push_back(hist1);

  try {
    cout << ksd->ksdistance(dists);
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  dists.push_back(hist2);

  try {
    cout << ksd->ksdistance(dists);
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  clivectord low    (3, -1.0);
  clivectord high   (3, 1.0);
  clivectord widths (3, 0.2);

  RecordType * multitype = new RecordType();
  multitype = multitype->insertField(1, "fieldone",   BasicType::Real);
  multitype = multitype->insertField(2, "fieldtwo",   BasicType::Real);
  multitype = multitype->insertField(3, "fieldthree", BasicType::Real);
  
  HistogramND * histnd = new HistogramND(&low, &high, &widths, multitype);

  dists[1] = histnd;
  
  try {
    cout << ksd->ksdistance(dists);
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  /*****************************************************************************
  * KS distance and statistic tests.
  *****************************************************************************/
  cout << "\n\nTests of distance measure and significance statistic." << endl;
  
  /*****************************************************************************
  * test 1
  *****************************************************************************/
  cout << "\n\nTest one - zero distance (identical dists), same binning" << endl;

  Histogram1D *  pophist1 = new Histogram1D(-1, 1, 0.2, "parameter");
  Histogram1D *  pophist2 = new Histogram1D(-1, 1, 0.2, "parameter");

  RecordInputStream * ris1 = new RecordInputStream_Ascii("kstest1.dat");

  while (ris1->nextRecord())
  {
    pophist1->AccumData(1.0, ris1->getBuffer());
    pophist2->AccumData(1.0, ris1->getBuffer());
  }  
  
  dists.clear();
  dists.push_back(pophist1);
  dists.push_back(pophist2);
  
  cout << "Distance between identical dists: " << ksd->ksdistance(dists)<< endl;
  cout << "Significance : " << ksd->kssignificance(pophist1, pophist2) << endl;
  
  /*****************************************************************************
  * test 2
  *****************************************************************************/
  cout << "\n\nTest two - dists drawn from same distribution (100 instances), same binning.\n";
  pophist1 = new Histogram1D(-1, 1, 0.2, "parameter");
  pophist2 = new Histogram1D(-1, 1, 0.2, "parameter");

  ris1 = new RecordInputStream_Ascii("kstest2.dat");
  RecordInputStream * ris2 = new RecordInputStream_Ascii("kstest3.dat");

  while (ris1->nextRecord())
  { pophist1->AccumData(1.0, ris1->getBuffer()); }  
  
  while (ris2->nextRecord())
  { pophist2->AccumData(1.0, ris2->getBuffer()); }  

  dists.clear();
  dists.push_back(pophist1);
  dists.push_back(pophist2);
  
  cout << "Distance between dists: " << ksd->ksdistance(dists)<< endl;
  cout << "Significance : " << ksd->kssignificance(pophist1, pophist2)<< endl;

  /*****************************************************************************
  * test 3
  *****************************************************************************/
  cout << "\n\nTest two - dists drawn from same distribution (10000 instances), same binning.\n";
  pophist1 = new Histogram1D(-1, 1, 0.2, "parameter");
  pophist2 = new Histogram1D(-1, 1, 0.2, "parameter");

  ris1 = new RecordInputStream_Ascii("kstest4.dat");
  ris2 = new RecordInputStream_Ascii("kstest5.dat");

  while (ris1->nextRecord())
  { pophist1->AccumData(1.0, ris1->getBuffer()); }  
  
  while (ris2->nextRecord())
  { pophist2->AccumData(1.0, ris2->getBuffer()); }  

  dists.clear();
  dists.push_back(pophist1);
  dists.push_back(pophist2);
  
  cout << "Distance between dists: " << ksd->ksdistance(dists)<< endl;
  cout << "Significance : " << ksd->kssignificance(pophist1, pophist2)<< endl;

  /*****************************************************************************
  * test 4
  *****************************************************************************/
  cout << "\n\nTest 3 - dists drawn from same distribution (10000 instances), different binning.\n";
  pophist1 = new Histogram1D(-1, 1, 0.01, "parameter");
  pophist2 = new Histogram1D(-1, 1, 0.02, "parameter");

  ris1 = new RecordInputStream_Ascii("kstest4.dat");
  ris2 = new RecordInputStream_Ascii("kstest5.dat");

  while (ris1->nextRecord())
  { pophist1->AccumData(1.0, ris1->getBuffer()); }  
  
  while (ris2->nextRecord())
  { pophist2->AccumData(1.0, ris2->getBuffer()); }  

  dists.clear();
  dists.push_back(pophist1);
  dists.push_back(pophist2);
  
  cout << "Distance between dists: " << ksd->ksdistance(dists)<< endl;
  cout << "Significance : " << ksd->kssignificance(pophist1, pophist2)<< endl;

  /*****************************************************************************
  * test 5
  *****************************************************************************/
  cout << "\n\nTest 4 - different dists (100 instances), same binning.\n";
  pophist1 = new Histogram1D(-1, 1, 0.2, "parameter");
  pophist2 = new Histogram1D(-1, 1, 0.2, "parameter");

  ris1 = new RecordInputStream_Ascii("kstest6.dat");
  ris2 = new RecordInputStream_Ascii("kstest7.dat");

  while (ris1->nextRecord())
  { pophist1->AccumData(1.0, ris1->getBuffer()); }  
  
  while (ris2->nextRecord())
  { pophist2->AccumData(1.0, ris2->getBuffer()); }  

  dists.clear();
  dists.push_back(pophist1);
  dists.push_back(pophist2);
  
  cout << "Distance between dists: " << ksd->ksdistance(dists)<< endl;
  cout << "Significance : " << ksd->kssignificance(pophist1, pophist2)<< endl;

  /*****************************************************************************
  * test 6
  *****************************************************************************/
  cout << "\n\nTest 5 - different dists (10000 instances), same binning.\n";
  pophist1 = new Histogram1D(-1, 1, 0.2, "parameter");
  pophist2 = new Histogram1D(-1, 1, 0.2, "parameter");

  ris1 = new RecordInputStream_Ascii("kstest8.dat");
  ris2 = new RecordInputStream_Ascii("kstest9.dat");

  while (ris1->nextRecord())
  { pophist1->AccumData(1.0, ris1->getBuffer()); }  
  
  while (ris2->nextRecord())
  { pophist2->AccumData(1.0, ris2->getBuffer()); }  

  dists.clear();
  dists.push_back(pophist1);
  dists.push_back(pophist2);
  
  cout << "Distance between dists: " << ksd->ksdistance(dists)<< endl;
  cout << "Significance : " << ksd->kssignificance(pophist1, pophist2)<< endl;

  /*****************************************************************************
  * test 7
  *****************************************************************************/
  cout << "\n\nTest 6 - different dists (10000 instances), different binning.\n";
  pophist1 = new Histogram1D(-1, 1, 0.2, "parameter");
  pophist2 = new Histogram1D(-1, 1, 0.15, "parameter");

  ris1 = new RecordInputStream_Ascii("kstest8.dat");
  ris2 = new RecordInputStream_Ascii("kstest9.dat");

  while (ris1->nextRecord())
  { pophist1->AccumData(1.0, ris1->getBuffer()); }  
  
  while (ris2->nextRecord())
  { pophist2->AccumData(1.0, ris2->getBuffer()); }  

  dists.clear();
  dists.push_back(pophist1);
  dists.push_back(pophist2);
  
  cout << "Distance between dists: " << ksd->ksdistance(dists)<< endl;
  cout << "Significance : " << ksd->kssignificance(pophist1, pophist2)<< endl;
  
}


