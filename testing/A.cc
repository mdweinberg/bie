#include <A.h>

#if BOOST_VERSION >= 104200
BOOST_CLASS_EXPORT_IMPLEMENT(A)
#else
BOOST_CLASS_EXPORT(A)
#endif

A::A()
{
  x = static_cast<float>(rand())/RAND_MAX;
  y = static_cast<float>(rand())/RAND_MAX;
  z = static_cast<float>(rand())/RAND_MAX;
}
