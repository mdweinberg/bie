#include <string>
#include <vector>
#include <sstream>
using namespace std;

#include <RecordBuffer.h>
#include <TypedBuffer.h>
#include <BasicType.h>
#include <RecordType.h>
#include <BIEException.h>

using namespace BIE;

int main ()
{
  /*****************************************************************************
  * Typed Buffer and Basic Type tests.
  *****************************************************************************/
  TypedBuffer * s = TypedBuffer::createNew(BasicType::String);
  TypedBuffer * i = TypedBuffer::createNew(BasicType::Int);
  TypedBuffer * r = TypedBuffer::createNew(BasicType::Real);
  TypedBuffer * b = TypedBuffer::createNew(BasicType::Bool);
  
  cout << "Statements getting values before one is available (4 exceptions)\n";
  
  try {
    cout << s->getStringValue();
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  try {
    cout << i->getIntValue();
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  try {
    cout << r->getRealValue();
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  try {
    cout << b->getBoolValue();
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "\nStatements setting TypedBuffer values.\n";
  s->setStringValue("Hello");
  i->setIntValue(2);
  r->setRealValue(3.2);
  b->setBoolValue(1);

  cout << "\nThe values are now:\n";
  cout << s->toString() << "\n";
  cout << i->toString() << "\n";
  cout << r->toString() << "\n";
  cout << b->toString() << "\n";
  
  cout << "\nTests on getvalue routines for String type\n";
  try {
    cout << s->getStringValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << s->getIntValue()  << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << s->getRealValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << s->getBoolValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "\nTests on getvalue routines for Int type\n";
  try {
    cout << i->getStringValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << i->getIntValue()  << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << i->getRealValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << i->getBoolValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "\nTests on getvalue routines for Real type\n";
  try {
    cout << r->getStringValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << r->getIntValue()  << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << r->getRealValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << r->getBoolValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "\nTests on getvalue routines for Bool type\n";
  try {
    cout << b->getStringValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << b->getIntValue()  << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << b->getRealValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    cout << b->getBoolValue() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}


  cout << "\nTests on SETvalue routines for String type\n";
  try {
    s->setStringValue("hello");
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    s->setIntValue(123456789) ;
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    s->setRealValue(3.14);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    s->setBoolValue(false);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "\nTests on setvalue routines for Int type\n";
  try {
    i->setStringValue("hello");
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    i->setIntValue(123456789) ;
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    i->setRealValue(3.14);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    i->setBoolValue(false);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "\nTests on setvalue routines for Real type\n";
  try {
    r->setStringValue("hello");
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    r->setIntValue(123456789) ;
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    r->setRealValue(3.14);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    r->setBoolValue(false);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "\nTests on setvalue routines for Bool type\n";
  try {
    b->setStringValue("hello");
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    b->setIntValue(123456789) ;
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    b->setRealValue(3.14);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  try {
    b->setBoolValue(false);
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << s->getType()->toString() << endl;
  cout << i->getType()->toString() << endl;
  cout << r->getType()->toString() << endl;
  cout << b->getType()->toString() << endl;

  try {
    BasicType fake("cat");
    TypedBuffer * badtype = TypedBuffer::createNew(&fake);
    cout << "XXXXXXXX\n" << badtype->toString() << "\n";
  } catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "\n*********************  Record Type tests ***********\n";

  /*****************************************************************************
  * Record Type tests.
  *****************************************************************************/
  RecordType * rt = new RecordType();
  cout << "Record type before insertions: " << rt->toString() << "\n";
  
  rt = rt->insertField(1, "a", BasicType::String);
  cout << "insert(1, a, string): \n"<< rt->toString() << "\n";

  rt = rt->insertField(1, "b", BasicType::Real);
  cout << "insert(1, b, real): \n"<< rt->toString() << "\n";

  rt = rt->insertField(3, "c", BasicType::Int);
  cout << "insert(3, c, int): \n"<< rt->toString() << "\n";

  rt = rt->insertField(4, "d", BasicType::Bool);
  cout << "insert(4, d, bool): \n"<< rt->toString() << "\n";
  
  rt = rt->deleteField("c");
  cout << "delete(c): \n" << rt->toString() << "\n";

  rt = rt->deleteField("b");
  cout << "delete(b): \n"<< rt->toString() << "\n";

  rt = rt->deleteField(1);
  cout << "delete(1): \n"<< rt->toString() << "\n";
  
  rt = rt->insertField(1, "a", BasicType::String);
  cout << "insert(1, a, string): \n"<< rt->toString() << "\n";

  rt = rt->insertField(3, "b", BasicType::Real);
  cout << "insert(3, b, real): \n"<< rt->toString() << "\n";

  rt = rt->insertField(2, "c", BasicType::Int);
  cout << "insert(2, c, int): \n"<< rt->toString() << "\n";

  rt = rt->deleteRange(1,3);
  cout << "deleteRange(1, 3): \n"<< rt->toString() << "\n";

  RecordType * rt1 = new RecordType();
  rt1 = rt1->insertField(1, "aa", BasicType::String);
  rt1 = rt1->insertField(2, "bb", BasicType::Real);
  rt1 = rt1->insertField(3, "cc", BasicType::Int);
  cout << "record rt1 is :\n" << rt1->toString() << "\n";
  
  rt = rt->insertRecord(1, rt1);
  cout << "insertRecord(1, rt1): \n"<< rt->toString() << "\n";
  
  cout << "insertField(0, ...) : ";
  try {rt = rt->insertField(0, "aaaa", BasicType::Real);}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "insertField(10, ...): ";
  try {rt = rt->insertField(10, "aaaa", BasicType::Real);}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "insertRecord(0, ...) : ";
  try {rt = rt->insertRecord(0, rt1);}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "insertRecord(10, ...): ";
  try {rt = rt->insertRecord(10, rt1);}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "insert(1, aa) :... ";
  try {rt = rt->insertField(1, "aa", BasicType::Real);}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "deleteRange(2,1): .... ";
  try {rt = rt->deleteRange(2, 1) ;}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  rt = rt->renameField("aa", "Alistair");
  cout << "\nrenameField(aa, Alistair): \n" << rt->toString() << "\n";

  rt = rt->renameField(3, "Dundas");
  cout << "renameField(3, Dundas): \n" << rt->toString() << "\n";
    
  cout << "getFieldIndex(Dundas): " << rt->getFieldIndex("Dundas") << "\n";
  cout << "getFieldName(3): " << rt->getFieldName(3) << "\n";

  RecordType * rt2 = new RecordType(rt);
  cout << "\nrt is :\n" << rt->toString() << "\n";
  cout << "\nrt2 is:\n" << rt2->toString() << "\n";
    
  cout << "rt equals rt2 :" << rt->equals(rt2) << "\n";
  cout << "rt2 subset of rt :" << rt->isSubset(rt2) << "\n";
  cout << "rt2 prefix of rt:" << rt->isPrefix(rt2) << "\n";
  
  rt2 = rt2->deleteField(rt2->numFields());
  rt2 = rt2->deleteField(rt2->numFields());
  cout << "\nrt2 is now:\n" << rt2->toString() << "\n";
  
  cout << "rt equals rt2 :" << rt->equals(rt2) << "\n";
  cout << "rt2 subset of rt :" << rt->isSubset(rt2) << "\n";
  cout << "rt2 prefix of rt:" << rt->isPrefix(rt2) << "\n";
  
  rt2 = rt2->deleteField(1);
  cout << "\nrt2 is now:\n" << rt2->toString() << "\n";

  cout << "rt equals rt2 :" << rt->equals(rt2) << "\n";
  cout << "rt2 subset of rt :" << rt->isSubset(rt2) << "\n";
  cout << "rt2 prefix of rt:" << rt->isPrefix(rt2) << "\n";
  
  rt2 = rt2->insertField(1, "hdhdhdh", BasicType::Int);
  cout << "\nrt2 is now:\n" << rt2->toString() << "\n";

  cout << "rt equals rt2 :" << rt->equals(rt2) << "\n";
  cout << "rt2 subset of rt :" << rt->isSubset(rt2) << "\n";
  cout << "rt2 prefix of rt:" << rt->isPrefix(rt2) << "\n";

  RecordType * rt3 = new RecordType();
  rt3 = rt3->insertField(1, "aa", BasicType::String);
  rt3 = rt3->insertField(2, "bb", BasicType::Real);
  rt3 = rt3->insertField(3, "cc", BasicType::Int);
  
  cout << "\nRecord rt3 is :\n" << rt3->toString() << "\n";
  
  rt3 = rt3->moveField("cc", 1);
  cout << "moveField(cc, 1): \n" << rt3->toString() << "\n";  
  rt3 = rt3->moveField("aa", 1);
  cout << "moveField(aa, 1): \n" << rt3->toString() << "\n";
  rt3 = rt3->moveField("aa", 3);
  cout << "moveField(aa, 3): \n" << rt3->toString() << "\n";
  rt3 = rt3->moveField(1, 3);
  cout << "moveField(1, 3): \n" << rt3->toString() << "\n";  
  rt3 = rt3->moveField(2, 3);
  cout << "moveField(2, 3): \n" << rt3->toString() << "\n";
  rt3 = rt3->moveField(3, 1);
  cout << "moveField(3, 1): \n" << rt3->toString() << "\n";
  
  vector<string> select;
  select.insert(select.begin(),"aa");
  select.insert(select.begin(),"cc");

  rt3 = rt3->selectFields(&select);
  cout << "selectbyname({cc,aa}): \n" << rt3->toString() << "\n";  
  
  vector<int> indexselect;
  indexselect.insert(indexselect.begin(),2);
  indexselect.insert(indexselect.begin(),2);

  cout << "selectbyindex({2,2}): ";
  try { rt3->selectFields(&indexselect);}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}


  indexselect.insert(indexselect.begin(),10);
  indexselect.insert(indexselect.begin(),1);

  cout << "selectbyindex({10,1}): ";
  try { rt3->selectFields(&indexselect);}
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  /*****************************************************************************
  * Tests on empty record types.
  *****************************************************************************/
  RecordType * emptytype = new RecordType();
  
  cout << "\nTests on empty type: \n";
  
  cout << "deletefield(a field):"; 
  try { emptytype->deleteField("a field"); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "deletefield(3): ";
  try { emptytype->deleteField(3); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "deleteRange(1,2): "; 
  try { emptytype->deleteRange(1,2); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "moveField(1,2): ";
  try { emptytype->moveField(1,2); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "getFieldIndex(a field): ";
  try { emptytype->getFieldIndex("a field"); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "getFieldName(1): ";
  try { emptytype->getFieldName(1); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "Is valid (2): " << emptytype->isValidFieldIndex(2) << "\n";
  cout << "Is valid (0): " << emptytype->isValidFieldIndex(0) << "\n";
  cout << "Num fields: " << emptytype->numFields() << "\n\n";

  rt = rt->deleteField(4);
  rt = rt->insertField(4, "paradox", BasicType::Bool);
  
  RecordBuffer * rb = new RecordBuffer(rt);
  cout << rb->toString() << "\n"; 
  
  cout << "getStringValue(1) : "; 
  try { rb->getStringValue(1); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "getRealValue(2) : ";
  try { rb->getRealValue(2); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}
  
  cout << "getIntValue(3) : ";
  try { rb->getIntValue(3); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "getBoolValue(4) : ";
  try { rb->getBoolValue(4); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  rb->setStringValue("Alistair", "student");
  rb->setRealValue("bb", 3.14);
  rb->setIntValue("Dundas", 123456789);
  rb->setBoolValue("paradox", true);
  
  cout << "After setting some values:\n" << rb->toString() << "\n"; 

  rb->setStringValue(1, "grad");
  rb->setRealValue(2, 123.4567000004);
  rb->setIntValue(3, 111000111);
  rb->setBoolValue(4, 1==2);

  cout << "After setting some other values:\n" << rb->toString() << "\n"; 

  rb = rb->moveField(1,4);
  cout << "moveField(1,4): \n" << rb->toString() << "\n";
  
  rb = rb->moveField("Alistair", 3);
  cout << "moveField(Alistair,3): \n" << rb->toString() << "\n";

  rb = rb->renameField("paradox", "question");
  cout << "renameField(paradox question): \n" << rb->toString() << "\n";
  
  rb = rb->renameField(3, "golfer");
  cout << "renameField(3, golfer): \n" << rb->toString() << "\n";

  rb = rb->deleteField("golfer");
  cout << "deleteField(golfer): \n" << rb->toString() << "\n";

  rb = rb->insertField(3,"golfer", BasicType::String);
  cout << "insertField(3, golfer): \n" << rb->toString() << "\n";

  rb->setStringValue("golfer", "radical");
  
  cout << "insertRecord(the same buffer): ";
  try { rb->insertRecord(1, rb); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "insertRecord(0, ): ";
  try { rb->insertRecord(0, rb); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "insertRecord(6, ): ";
  try { rb->insertRecord(rb->numFields()+2, rb); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "insertField(0, ): ";
  try { rb->insertField(0, "galaxy", BasicType::Int); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "insertField(6, ): ";
  try { rb->insertField(rb->numFields()+2, "galaxy", BasicType::Int); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "renameField(0, zero): ";
  try { rb->renameField(0, "zero"); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "renameField(1, golfer): ";
  try { rb->renameField(1, "golfer"); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  cout << "getFieldType(0, zero): ";
  try { rb->getFieldType(0); }
  catch (BIEException e) {cout << e.getErrorMessage() << endl;}

  RecordBuffer * rb2 = new RecordBuffer(rt3);
  cout << "rt2 buffer is:\n" << rb2->toString() << "\n";  

  rb = rb->insertRecord(2, rb2); 
  cout << "insertRecord(2,rb2): \n" << rb->toString() << "\n";

  rb = rb->selectFields((rb->getType())->deleteField(1));
  cout << "selectFields(all except first): \n" << rb->toString() << "\n";
  
  vector<string> fields;
  fields.insert(fields.begin(), "Dundas");
  fields.insert(fields.begin(), "golfer");

  rb = rb->selectFields(&fields);
  cout << "selectFields(golfer, Dundas): \n" << rb->toString() << "\n";
  
  rb = rb->insertField(1, "New Field", BasicType::String); 
  cout << "insertField(1, New Field, String): \n" << rb->toString() << "\n";

  cout << "num fields :" <<  rb->numFields() << "\n";
  cout << "hasValue(1) :" <<  rb->hasValue(1) << "\n";
  cout << "hasValue(2) :" <<  rb->hasValue(2) << "\n";
  cout << "hasValue(Dundas) :" <<  rb->hasValue("Dundas") << "\n";
  cout << "hasValue(New Field) :" <<  rb->hasValue("New Field") << "\n";
  cout << "hasValue(all fields) : " <<  rb->hasValue() << "\n";

  rb->setStringValue("New Field", "Cherry Hill");
  cout << "setStringValue(New Field, Cherry Hill):\n" << rb->toString() << "\n";
    
  cout << "hasValue(all fields) : " <<  rb->hasValue() << "\n";

  
  cout << "Record type rte\n" << rt->toString() << "\n";
  RecordType * rte = new RecordType(rt);
  cout << "Equals?? : " << rte->equals(rt) << endl;
  cout << "Equals?? : " << rte->orderedEquals(rt) << endl;

  rte = rte->moveField(1,2);
  cout << "Record type rte\n" << rte->toString() << "\n";
  cout << "Equals?? : " << rte->equals(rt) << endl;
  cout << "Equals?? : " << rte->orderedEquals(rt) << endl;
}
