#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

#define IS_MAIN
#include <MappedGrid.h>
#include <KdTessellation.h>
#include <Distribution.h>
#include <PointTessellation.h>
#include <QuadGrid.h>
#include <ContainerTessellation.h>
#include <BinaryTessellation.h>
#include <SquareTile.h>
#include <DataTree.h>
#include <DummyModel.h>
#include <vector>
#include <RecordStream_Ascii.h>
#include <Frontier.h>
#include <FrontierExpansionHeuristic.h>
#include <Node.h>

using namespace BIE;

void pointTessellationTests();
void printFrontier(vector<int>);
void commontests(Tessellation * tess);

/*******************************************************************************
 * This runs several tests on the Point tesselation
 *******************************************************************************/
int main()
{
  cout << "Starting up\n";
  pointTessellationTests();
}

void pointTessellationTests()
{
  RecordInputStream * inputstream = new RecordInputStream_Ascii("7pts.dat");
  SquareTile * tile = new SquareTile();
  PointTessellation * ptTess=0;
  
  cout << "About to construct PointTesselation\n";
  cout.flush();
  
  try { ptTess = new PointTessellation(tile, inputstream); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl;}
  
  
  cout << "Finished construction!\n";
  cout.flush();
  
  ptTess->PrintPreOrder(cout);
  
  
  vector<int> found;
  
  ptTess->FindAll(1.1, 1.2, found); 
  cout << "FindAll (1.1, 1.2) size :  " << found.size() << endl;
  
  ptTess->FindAll(4.1, 4.0, found); 
  cout << "FindAll (4.1, 4.0) size :  " << found.size() << endl;
  
  ptTess->FindAll(4.1, 4.2, found); 
  cout << "FindAll (4.1, 4.2) size :  " << found.size() << endl;
  
  ptTess->FindAll(7.1, 7.2, found); 
  cout << "FindAll (7.1, 7.2) size :  " << found.size() << endl;
  
  // aah put in test for other constructor
  
  
  /*****************************************************************************
   * Selection tests
   *****************************************************************************/
  
  
  RecordInputStream * ris = new RecordInputStream_Ascii("7pts.dat");
  PointDistribution * dis = new PointDistribution("namedattribute");
  
  DataTree * dist = new DataTree(ris, dis, ptTess);
  
  cout << "\nTotal points in distribution = " << dist->Total() << "\n";
  cout << "Points off grid - not covered by tessellation = " << dist->Offgrid() << "\n";
  
  
  Frontier * frontier = dist->GetDefaultFrontier();
  
  cout << "---Selecting 1,2,3,4,5,6,7,8,9 in frontier" << endl;
  vector<int> afewnodes;
  for(int i = 0; i <= 6; i++)
    { afewnodes.push_back(i); }
  frontier->Set(afewnodes);
  printFrontier(frontier->ExportFrontier());
  
  afewnodes.clear();
  
  
  
  //dist->SetDefaultFrontier(frontier); // why do we need this?
  //    frontier = dist->GetDefaultFrontier();
  
  for (int i = 1; i <= 5; i++)
    {
      cout << "\nPoints per tile : " << endl;
      for (dist->Reset(); ! dist->IsDone(); dist->Next())
	{
	  cout << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	  cout << dist->CurrentItem()->getDataSetSize() << "), ";
	}
      
      frontier->UpDownLevels(-1);
      cout << "\nAfter contracting with UpDownLevels(-1)" << i << " times." << endl;
      printFrontier(frontier->ExportFrontier()); 
    }
  
  
  
  
  /*****************************************************************************
   * Common tests.
   *****************************************************************************/
  inputstream = new RecordInputStream_Ascii("data.dat");
  ptTess = new PointTessellation(tile, inputstream, -10, 10, -10, 10);
  commontests(ptTess);
  
  /*****************************************************************************
   * Destructor tests
   *****************************************************************************/
  delete ptTess;
  
}

void printFrontier(vector<int> frontier_value) 
{
  vector <int>::iterator it;
  
  cout << "---- new frontier is: ("; 
  for (it=frontier_value.begin();it!=frontier_value.end();it++) {
    if (it!=frontier_value.begin()) cout << ", ";
    cout << (*it);
  }
  cout << ")" << endl;
}

/*******************************************************************************
 * Tests common to all tessellations.
 *******************************************************************************/
void commontests(Tessellation * tess)
{
  try {
    cout << "\nCreated tessellation, starting common tests\n\n";
    
    /*****************************************************************************
     * Print the entire tessellation tree.
     *****************************************************************************/
    tess->PrintPreOrder(cout);
    
    Frontier * frontier = new Frontier(tess);
    printFrontier(frontier->ExportFrontier());
    
    /*****************************************************************************
     * Mutation tests
     *****************************************************************************/
    cout << "\n\n--Contracting one level. " << endl;
    frontier->UpDownLevels(-1);
    printFrontier(frontier->ExportFrontier());
    
    for (int i=0; i<4; i++) {
      cout << "\n\n---Expanding two levels. " << endl;
      frontier->UpDownLevels(2);
      printFrontier(frontier->ExportFrontier());
    }
    
    /*****************************************************************************
     * RetractToTopLevel tests.
     *****************************************************************************/
    cout << " Retract to top level " << endl;
    frontier->RetractToTopLevel();
    printFrontier(frontier->ExportFrontier());
    
    /*****************************************************************************
     * Increase resolution tests.
     *****************************************************************************/
    cout << "\n\n---Increasing resolution by 2 levels (always increase heuristic)" << endl;
    AlwaysIncreaseResolution * heuristic = new AlwaysIncreaseResolution();
    
    frontier->IncreaseResolution(heuristic, 2);
    printFrontier(frontier->ExportFrontier());
    
    /*****************************************************************************
     * Selection tests
     *****************************************************************************/
    cout << "---Selecting 1,2,3,4,5,6,7,8,9 in frontier" << endl;
    vector<int> afewnodes;
    for(int i = 1; i <= 9; i++)
      { afewnodes.push_back(i); }
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier());
    
    afewnodes.clear();
    
    cout << "---Selecting 10,14,18,22 in frontier" << endl;
    afewnodes.push_back(10);
    afewnodes.push_back(14);
    afewnodes.push_back(18);
    afewnodes.push_back(22);
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier());
    
    afewnodes.clear();
    cout << "---Trying to selecting -1, 100000, 2,3,4 in frontier" << endl;
    afewnodes.push_back(-1);
    afewnodes.push_back(100000);
    afewnodes.push_back(2);
    afewnodes.push_back(3);
    afewnodes.push_back(4);
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier());
    
    /*****************************************************************************
     * FindInFrontier tests.
     *****************************************************************************/
    cout << "Found (0.0, 0.0) ? " << (bool) frontier->Find(0,0) << endl;
    cout << "Found (-3, -0.9) ? " << (bool) frontier->Find(-3,-0.9) << endl;
    cout << "Found (10, 9) ? " << (bool) frontier->Find(10,9) << endl;
    
    /*****************************************************************************
     * FindAll tests.
     *****************************************************************************/
    vector<int> found;
    
    tess->FindAll(0,0, found); 
    cout << "FindAll (0,0) size :  " << found.size() << endl;
    tess->FindAll(-3,-0.9, found); 
    cout << "FindAll (-3,-0.9) size:  " << found.size() << endl;
    tess->FindAll(10,9, found); 
    cout << "FindAll (10,9) size: " << found.size() << endl;
    
    /*****************************************************************************
     * Get Tile tests
     *****************************************************************************/
    cout << "Printing tiles 0, 1 " << endl;
    tess->GetTile(0)->printTile(cout);
    tess->GetTile(1)->printTile(cout);
    
    cout << "GetTile(-1):   " << tess->GetTile(-1) << endl;
    
    /*****************************************************************************
     * Is valid tests.
     *****************************************************************************/
    cout << "Is valid ID : -1 : " << tess->IsValidTileID(-1) << endl;
    cout << "Is valid ID : 0 : " << tess->IsValidTileID(0) << endl;
    cout << "Is valid ID : 99 : " << tess->IsValidTileID(99) << endl;
    cout << "Is valid ID : 100 : " << tess->IsValidTileID(100) << endl;
    cout << "Is valid ID : 10000 : " << tess->IsValidTileID(10000) << endl;
    cout << "Is valid ID : 100000 : " << tess->IsValidTileID(100000) << endl;
    
    /*****************************************************************************
     * Number tiles test.
     *****************************************************************************/
    cout << "Number of tiles: " << tess->NumberTiles() << endl;
    
    /*****************************************************************************
     * GetRootTiles test
     *****************************************************************************/
    vector<Node*> rootnodes = tess->GetRootNodes();
    
    cout << "Printing root nodes: " << endl;
    for (int i = 0; i < (int) rootnodes.size(); i++)
      { cout << rootnodes[i]->ID() << ", "; }
    cout << endl;
    
    /*****************************************************************************
     * GetRootNodes test
     *****************************************************************************/
    cout << "Printing root tiles:" << endl;
    vector<int> roottiles = tess->GetRootTiles();
    
    for (int i = 0; i < (int) roottiles.size(); i++)
      { cout << roottiles[i] << ","; } 
    cout << endl;
    
    /*****************************************************************************
     * Loop method tests.
     *****************************************************************************/
    frontier->Reset();
    cout << "Looping through all tiles in frontier" << endl;
    while(! (frontier->IsDone()))
      {
	cout << "ID: " << frontier->CurrentItem() << "  ";
	frontier->Next();
      }
    cout << "First and last in frontier: " << endl;
    cout << "first " << frontier->First() << endl;
    cout << "last " << frontier->Last() << endl;
    
    /*****************************************************************************
     * Some additional mutation tests.
     *****************************************************************************/
    cout << "\nSome additional mutation tests\n";
    
    for (int i = 1; i <= 20; i++)
      {
	frontier->UpDownLevels(1);
	cout << "\nAfter expanding " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier());  
      }
    
    for (int i = 1; i <= 20; i++)
      {
	frontier->UpDownLevels(-1);
	cout << "\nAfter contracting " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier());  
      }
    
    /*****************************************************************************
     * Heuristic tests.
     *****************************************************************************/
    RecordInputStream * ris = new RecordInputStream_Ascii("data.dat");
    PointDistribution * dis = new PointDistribution("mag1");
    
    DataTree * dist = new DataTree(ris, dis, tess);
    
    cout << "\nTotal points in distribution = " << dist->Total() << "\n";
    cout << "Points off grid - not covered by tessellation = " << dist->Offgrid() << "\n";
    
    FrontierExpansionHeuristic * h = new DataPointCountHeuristic(dist,50);
    frontier = dist->GetDefaultFrontier();
    
    for (int i = 1; i <= 10; i++)
      {
	cout << "\nPoints per tile : " << endl;
	for (dist->Reset(); ! dist->IsDone(); dist->Next())
	  {
	    cout << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	    cout << dist->CurrentItem()->getDataSetSize() << "), ";
	  }
	
	frontier->IncreaseResolution(h);
	cout << "\nAfter expanding with  DataPointCountHeuristic " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier()); 
      }
    
    h = new KSDistanceHeuristic(dist,0.1);
    frontier->RetractToTopLevel();
    
    for (int i = 1; i <= 10; i++)
      {
	cout << "\nPoints per tile : " << endl;
	for (dist->Reset(); ! dist->IsDone(); dist->Next())
	  {
	    cout << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	    cout << dist->CurrentItem()->getDataSetSize() << "), ";
	  }
	
	frontier->IncreaseResolution(h);
	cout << "\nAfter expanding with KSDistanceHeuristic " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier()); 
      }
    
    cout << "Finished COMMON tests\n";
  }
  catch (BIEException e)
    { cerr << e.getErrorMessage() << endl; }
}
