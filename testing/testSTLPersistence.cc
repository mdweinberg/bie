/*******************************************************************************
* This is a collection of tests that checks the persistence library
* can successfully save and restore a variety of STL container classes.
*******************************************************************************/

#include <iostream>
#include <config.h>
#include <fstream>

#include <bieTags.h>
#include <Serializable.h>
#include <gvariable.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#include <SquareTile.h>

// For clivector
#include <Tessellation.h>
#include <Distribution.h>

using namespace std;

/*******************************************************************************
* Superclass for all test classes in this regression test.
*******************************************************************************/
class TestClass : public Serializable 
{

public:
  TestClass() {}
  virtual ~TestClass() {}
  virtual void fillWithData() = 0;
  virtual void print(ostream & out) = 0;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
  }
};

BOOST_CLASS_EXPORT(TestClass)

/*******************************************************************************
* Superclass for all test classes in this regression test.
* [This test class simply maintains an integer value]
*******************************************************************************/
class A : public Serializable {
public:
  A() {}
  A(int i) { this->i = i; }
  int getValue() { return this->i; }
private:
  int i;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
    ar & BOOST_SERIALIZATION_NVP(i);
  }
};

BOOST_CLASS_EXPORT(A)

/*******************************************************************************
* Test class for several kinds of vector objects.
*******************************************************************************/
class VectorTest : public TestClass {

  typedef vector<double>  dvector;
  typedef vector<dvector> ddvector;

public:
  VectorTest() {}
  ~VectorTest() {}
  void fillWithData();
  void print(ostream & out);

private:
  vector<uint32>  v1;
  vector<string>  v2;
  vector<bool>    v4;
  vector< vector<bool>* > v5;
  vector<dvector> v6;
  vector<A*>      v7;
  vector<ddvector> v8, v8T;
  vector<uint32>  *vp1;
  vector<string>  *vp2;
  vector<bool>    *vp4;
  vector< vector<bool>* > *vp5;
  vector<dvector> *vp6;
  vector<double>  *vp7;

  clivectord      vd;
  clivectord      *vpd;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(TestClass);
    ar & BOOST_SERIALIZATION_NVP(v1);
    ar & BOOST_SERIALIZATION_NVP(v2);
    ar & BOOST_SERIALIZATION_NVP(v4);
    ar & BOOST_SERIALIZATION_NVP(v5);
    ar & BOOST_SERIALIZATION_NVP(v6);
    ar & BOOST_SERIALIZATION_NVP(v7);
    ar & BOOST_SERIALIZATION_NVP(v8);
    ar & BOOST_SERIALIZATION_NVP(v8T);
    ar & BOOST_SERIALIZATION_NVP(vp1);
    ar & BOOST_SERIALIZATION_NVP(vp2);
    ar & BOOST_SERIALIZATION_NVP(vp4);
    ar & BOOST_SERIALIZATION_NVP(vp5);
    ar & BOOST_SERIALIZATION_NVP(vp6);
    ar & BOOST_SERIALIZATION_NVP(vp7);
    ar & BOOST_SERIALIZATION_NVP(vd);
    ar & BOOST_SERIALIZATION_NVP(vpd);
  }
};
BOOST_CLASS_EXPORT(VectorTest)

void VectorTest::fillWithData()
{
  string str = "cat_";
  vp1 = new vector<uint32>;
  vp2 = new vector<string>;
  vp4 = new vector<bool>;
  vp5 = new vector<vector<bool>*>;
  vp6 = new vector<dvector>;
  vp7 = new vector<double>;
  vpd = new clivectord();

  for (uint32 i = 0; i < 19; i++)
  {
    v1.push_back(i);
    v2.push_back(str);
    v4.push_back(i%2);
    v7.push_back(new A(i*2));
    vd().push_back(1.234*i);
    vd().push_back(1.234*i);
    
    vp1->push_back(i);
    vp2->push_back(str);
    vp4->push_back(i%2);
    vp7->push_back(i*3.14159);
    (*vpd)().push_back(5.678*i);
    
    (*vp5).push_back(new vector<bool>());
    v5.push_back(new vector<bool>());
    
    (*vp6).push_back(vector<double>());
    v6.push_back(vector<double>());
    
    for (int y = 0; y < 4; y++)
    {
      ((*vp5)[i])->push_back((y+i)%2);    
      v5[i]->push_back((y+i)%2);

      (*vp6)[i].push_back((y+i)*3.14159);    
      v6[i].push_back((y+i)*3.14159);
    }
    
    str = str + "a"; 
  }

  v8 = vector<ddvector>(3);
  for (int i=0; i<3; i++) {
    v8[i] = ddvector(3);
    for (int j=0; j<3; j++) {
      v8[i][j] = dvector(3);
      for (int k=0; k<3; k++) {
	v8[i][j][k] = (1.0+i)*(1.0+j)*(1.0+k);
      }
    }
  }

  const int v8Tsiz = 100;

  v8T = vector<ddvector>(v8Tsiz);
  for (int i=0; i<v8Tsiz; i++) {
    v8T[i] = ddvector(v8Tsiz);
    for (int j=0; j<v8Tsiz; j++) {
      v8T[i][j] = dvector(v8Tsiz);
      for (int k=0; k<v8Tsiz; k++) {
	v8T[i][j][k] = (1.0+i)*(1.0+j)*(1.0+k);
      }
    }
  }

}

void VectorTest::print(ostream & out)
{
  out << "v1: ";
  for (unsigned int i = 0; i < v1.size(); i++) out << v1[i] << ", ";
  out << endl;
 
  out << "v2: ";
  for (unsigned int i = 0; i < v2.size(); i++) out << v2[i] << ", ";
  out << endl;
  
  out << "v4: ";
  for (unsigned int i = 0; i < v4.size(); i++) out << v4[i] << ", ";
  out << endl;
  
  out << "v5: ";
  for (unsigned int j = 0; j < v5.size(); j++) {
    out << "    ";
    for (unsigned int i = 0; i < (v5[j])->size(); i++)
      out << (*(v5[j]))[i] << ", ";
    out << endl;
  }
  out << endl;

  out << "v6: ";
  for (unsigned int j = 0; j < v6.size(); j++) {
    out << "    ";
    for (unsigned int i = 0; i < v6[j].size(); i++)
      out << v6[j][i] << ", ";
    out << endl;
  }
  out << endl;

  out << "v7: ";
  for (unsigned int i = 0; i < v7.size(); i++)
    out << v7[i]->getValue() << ", ";
  out << endl;

  out << "vd: ";
  for (unsigned int i = 0; i < vd().size(); i++)
    out << vd()[i] << ", ";
  out << endl;

  out << "vpd: ";
  for (unsigned int i = 0; i < (*vpd)().size(); i++)
    out << (*vpd)()[i] << ", ";
  out << endl;

  out << "vp1: ";
  for (unsigned int i = 0; i < (*vp1).size(); i++)
    out << (*vp1)[i] << ", ";
  out << endl;
  
  out << "vp2: ";
  for (unsigned int i = 0; i < (*vp2).size(); i++)
    out << (*vp2)[i] << ", ";
  out << endl;
  
  out << "vp4: ";
  for (unsigned int i = 0; i < (*vp4).size(); i++)
    out << (*vp4)[i] << ", ";
  out << endl;

  out << "vp5: ";
  for (unsigned int j = 0; j < (*vp5).size(); j++) {
    out << "     ";
    for (unsigned int i = 0; i < ((*vp5)[j])->size(); i++)
      out << (*((*vp5)[j]))[i] << ", ";
    out << endl;
  }
  out << endl;

  out << "vp6: ";
  for (unsigned int j = 0; j < (*vp6).size(); j++) {
    out << "     ";
    for (unsigned int i = 0; i < (*vp6)[j].size(); i++)
      out << (*vp6)[j][i] << ", ";
    out << endl;
  }
  out << endl;

  out << "vp7: ";
  for (unsigned int i = 0; i < (*vp7).size(); i++)
    out << (*vp7)[i] << ", ";
  out << endl;

  out << "v8: ";
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      for (int k=0; k<3; k++) out << setw(10) << v8[i][j][k];
      out << endl;
    }
    out << endl;
  }

  out << "v8T: ";
  for (unsigned i=0; i<v8T.size(); i++) {
    for (unsigned j=0; j<v8T[i].size(); j++) {
      for (unsigned k=0; k<v8T[i][j].size(); k++) out << setw(10) << v8T[i][j][k];
      out << endl;
    }
    out << endl;
  }

}


/*******************************************************************************
* Test class for several kinds of map objects.   
*******************************************************************************/
class MapTest : public TestClass 
{

public:
  MapTest() {}
  ~MapTest() {}
  void fillWithData();
  void print(ostream & out);
private:
  map<uint32,string> m1;
  map<string,bool>   m4;
  map<string,vector<bool> *>   m5;
  map<string,A*>     m6;
  map<uint32,string> *mp1;
  map<string,bool>   *mp4;
  map<string,map<uint32, string> *> *mp5;
  map<string,A*>     *mp6;
  map<int,   Tile*>  m7, m8;
  Tile* m9;
  map<int, vector< vector<double> > > m10;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(TestClass);
    ar & BOOST_SERIALIZATION_NVP(m1);
    ar & BOOST_SERIALIZATION_NVP(m4);
    ar & BOOST_SERIALIZATION_NVP(m5);
    ar & BOOST_SERIALIZATION_NVP(m6);
    ar & BOOST_SERIALIZATION_NVP(mp1);
    ar & BOOST_SERIALIZATION_NVP(mp4);
    ar & BOOST_SERIALIZATION_NVP(mp5);
    ar & BOOST_SERIALIZATION_NVP(mp6);
    ar & BOOST_SERIALIZATION_NVP(m7);
    ar & BOOST_SERIALIZATION_NVP(m8);
    ar & BOOST_SERIALIZATION_NVP(m9);
    ar & BOOST_SERIALIZATION_NVP(m10);
  }
};
BOOST_CLASS_EXPORT(MapTest)

void MapTest::fillWithData()
{
  string str = "cat_";

  mp1 = new map<uint32,string>;
  mp4 = new map<string,bool>;
  mp5 = new map<string,map<uint32,string>* >;
  mp6 = new map<string,A*>;
  
  for (uint32 i = 0; i < 19; i++)
  {
    m1[2*i] = str;
    m4[str] = i%3;

    m5[str] = new vector<bool>;
    
    for (uint32 j = 0; j < 10; j++)
    {
      (m5[str])->push_back((i+j)%4);
    }
    
    m6[str] = new A(i);
    
    (*mp1)[2*i] = str;
    (*mp4)[str] = i%3;

    (*mp5)[str] = new map<uint32,string>;
    
    for (uint32 j = 0; j < 10; j++)
    {
      (*(*mp5)[str])[1000*i + j] = str;
    }

    (*mp6)[str] = new A(i);

    str = str + "a"; 
    
    m7[i] = new SquareTile(-static_cast<double>(i+1),
			    static_cast<double>(i+1),
			   -static_cast<double>(2*(i+1)),
			    static_cast<double>(2*(i+1))
			   );
  }
  m9 = new SquareTile(-1.0, 1.0, -4.0, 4.0);

  for (int i=0; i<8; i+=2) {
    vector< vector<double> > ins1;
    for (int j=0; j<4; j++) {
      vector<double> ins2;
      for (int k=0; k<4; k++) ins2.push_back(i + j + k);
      ins1.push_back(ins2);
    }
    m10[i] = ins1;
  }
  m10[9] = vector< vector<double> >();
}

void MapTest::print(ostream & out)
{
  out << "m1: " << endl;
  for (map<uint32,string>::iterator it = m1.begin(); it != m1.end(); it++)
  { out << it->first << ": " << it->second << endl;  }

  out << "m4: " << endl;
  for (map<string,bool>::iterator it = m4.begin(); it != m4.end(); it++)
  { out << it->first << ": " << it->second << endl;  }

  out << "m5: " << endl;
  for (map<string,vector<bool> *>::iterator it = m5.begin(); it != m5.end(); it++)
  { 
    out << it->first << ": ";
    
    vector<bool> * vec = it->second;
    
    for (uint32 j = 0; j < vec->size(); j++)
    {
      out << (*vec)[j];
    }
    
    out << endl;
  }

  out << "m6: " << endl;
  for (map<string,A*>::iterator it = m6.begin(); it != m6.end(); it++)
  { out << it->first << ": " << (it->second)->getValue() << endl;  }

  out << "mp1: " << endl;
  for (map<uint32,string>::iterator it = mp1->begin(); it != mp1->end(); it++)
  { out << it->first << ": " << it->second << endl;  }

  out << "mp4: " << endl;
  for (map<string,bool>::iterator it = mp4->begin(); it != mp4->end(); it++)
  { out << it->first << ": " << it->second << endl;  }

  out << "mp5: " << endl;
  for (map<string,map<uint32,string> *>::iterator it = mp5->begin(); it != mp5->end(); it++)
  { 
    out << it->first << ": " << endl;
    
    map<uint32,string> * mapptr = it->second;

    for (map<uint32,string>::iterator it2 = mapptr->begin(); it2 != mapptr->end(); it2++)
    { out << "     " << it2->first << ": " << it2->second << endl;  }
        
    out << endl;
  }

  out << "mp6: " << endl;
  for (map<string,A*>::iterator it = mp6->begin(); it != mp6->end(); it++)
  { out << it->first << ": " << it->second->getValue() << endl;  }

  out << "m7: " << endl;

  double x0, x1, y0, y1;

  for (map<int,Tile*>::iterator it = m7.begin(); it != m7.end(); it++)
    { 
      dynamic_cast<SquareTile*>(it->second)->corners(x0, x1, y0, y1);
      out << it->first 
	  << ": [" << x0 << ", " << x1 
	  << "] [" << y0 << ", " << y1 << "]" << endl;  
    }
  out << "m8: tuple number=" << m8.size() << endl;

  dynamic_cast<SquareTile*>(m9)->corners(x0, x1, y0, y1);
  out << "m9: [" << x0 << ", " << x1 
      << "] [" << y0 << ", " << y1 << "]" << endl;  

  out << "m10: [";
  for (map<int, std::vector< std::vector<double> > >::iterator it = m10.begin();
       it != m10.end(); it++) 
    out << " " << it->first;
  out << "]" << std::endl;
}

/******************************************************************************
* These structures contain operators used when using STL string objects
* as hash keys.  stringhash is a hash function and stringequals is used to
* test whether strings are equal when hash keys match.
******************************************************************************/
struct stringhash
{

#define SCACHE_HASH_SIZE 257
  
  size_t operator () (string const & str) const
  {
    char *c = new char [str.size()+1];
    strcpy(c, str.c_str());

    // String hash djb2
    size_t hash = 5381;

    while (int i = *c++)
      hash = ((hash << 5) + hash) + i; // hash * 33 + c

    return hash;
  }
};

struct stringequals
{
  size_t operator () (string const & str1, string const & str2) const
  {
    return (str1 == str2);
  }
};

/*******************************************************************************
* Test class for several kinds of hash map objects.   
*******************************************************************************/
class HashMapTest : public TestClass {
public:
  HashMapTest() {}
  void fillWithData();
  void print(ostream & out);
  ~HashMapTest() {}
private:
  std::unordered_map<string,uint32,stringhash, stringequals> h1;
  std::unordered_map<uint32,string> h2;
  std::unordered_map<string,bool,stringhash,stringequals> h3;
  std::unordered_map<string,A*,stringhash,stringequals> h4;
  std::unordered_map<string, std::unordered_map<uint32,A*>* , stringhash, stringequals> h5;
  std::unordered_map<string,uint32,stringhash, stringequals> * hp1;
  std::unordered_map<uint32,string> * hp2;
  std::unordered_map<string,bool,stringhash,stringequals> * hp3;
  std::unordered_map<string,A*,stringhash,stringequals> * hp4;
  std::unordered_map<string, std::unordered_map<uint32,A*>* , stringhash, stringequals>* hp5;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(TestClass);
    ar & BOOST_SERIALIZATION_NVP(h1);
    ar & BOOST_SERIALIZATION_NVP(h2);
    ar & BOOST_SERIALIZATION_NVP(h3);
    ar & BOOST_SERIALIZATION_NVP(h4);
    ar & BOOST_SERIALIZATION_NVP(h5);
    ar & BOOST_SERIALIZATION_NVP(hp1);
    ar & BOOST_SERIALIZATION_NVP(hp2);
    ar & BOOST_SERIALIZATION_NVP(hp3);
    ar & BOOST_SERIALIZATION_NVP(hp4);
    ar & BOOST_SERIALIZATION_NVP(hp5);
  }
};
BOOST_CLASS_EXPORT(HashMapTest)

void HashMapTest::fillWithData()
{
  string str = "cat_";

  hp1 = new std::unordered_map<string,uint32,stringhash, stringequals>;
  hp2 = new std::unordered_map<uint32,string>;
  hp3 = new std::unordered_map<string,bool,stringhash,stringequals>;
  hp4 = new std::unordered_map<string,A*,stringhash,stringequals>;
  hp5 = new std::unordered_map<string, std::unordered_map<uint32,A*>* , stringhash, stringequals>;

  for (uint32 i = 100; i < 800; i = i + 100)
  {
    h1[str] = i;
    h2[i]   = str;
    h3[str] = i%200;
    h4[str] = new A(i);
    h5[str] = new std::unordered_map<uint32,A*>;

    for (uint32 j = 27; j < 37; j++)
    {
      (*(h5[str]))[j] = new A(j*2);
    }

    (*hp1)[str] = i;
    (*hp2)[i]   = str;
    (*hp3)[str] = i%200;
    (*hp4)[str] = new A(i);
    (*hp5)[str] = new std::unordered_map<uint32,A*>;

    for (uint32 j = 27; j < 37; j++)
    {
      (*((*hp5)[str]))[j] = new A(j*2);
    }

    str += "a";
  }
}

void HashMapTest::print(ostream & out)
{
  string str = "cat_";

  for (uint i = 100; i < 800; i = i + 100)
  {
    out << "h1[\"" << str << "\"] = " << h1[str] << endl;
    out << "h2[" << i   << "] = \"" << h2[i] << "\"" << endl; 
    out << "h3[\"" << str << "\"] = " << h3[str] << endl;
    out << "h4[\"" << str << "\"] = " << h4[str]->getValue() << endl;
    
    for (uint32 j = 27; j < 37; j++)
    {
      out << "h5[\"" << str << "\"][" << j << "] = " << (*(h5[str]))[j]->getValue() << endl;
    }

    out << "hp1[\"" << str << "\"] = " << (*hp1)[str] << endl;
    out << "hp2[" << i   << "] = \"" << (*hp2)[i] << "\"" << endl; 
    out << "hp3[\"" << str << "\"] = " << (*hp3)[str] << endl;
    out << "hp4[\"" << str << "\"] = " << (*hp4)[str]->getValue() << endl;
    
    for (uint32 j = 27; j < 37; j++)
    {
      out << "hp5[\"" << str << "\"][" << j << "] = " << (*((*hp5)[str]))[j]->getValue() << endl;
    }

    str += "a";
  }
}

void fill_save_run_run(TestClass * tc, string statename)
{
  try {
    tc->fillWithData();
   
    TestUserState teststate(statename, 0, true);
    teststate.addObject(tc);
    {
      TestSaveManager testSaveManager(&teststate, BOOST_TEXT, BACKEND_FILE);
      testSaveManager.save();
    }
  
    TestUserState *restored;
    {
      TestLoadManager testLoadManager(&restored, 
				      teststate.getSession(), 
				      teststate.getVersion(), 
				      BOOST_TEXT, BACKEND_FILE);
      testLoadManager.load();
    }

    Serializable *loadedObj = *(restored->getTransClosure().objectlist.begin());
    TestClass* restored_tc = dynamic_cast<TestClass*>(loadedObj);

    fstream actualoutput;
    string filename = statename + "_actual";
    actualoutput.open(filename.c_str(), fstream::out);
    tc->print(actualoutput);
    actualoutput.close();

    fstream restoredoutput;
    filename = statename + "_restored";
    restoredoutput.open(filename.c_str(), fstream::out);
    restored_tc->print(restoredoutput);
    restoredoutput.close();
  }
  catch (BIEException & e)
  { cout << "Caught Exception: \n" << e.getErrorMessage() << endl; }
}

int main()
{

  system("rm -rf " PERSISTENCE_DIR);

  try {
    TestClass * tc;
    
    tc = new VectorTest();
    fill_save_run_run(tc, "testSTLPersistence_vector");
    cout << "Finished persistence test for vector" << endl;

    tc = new MapTest();
    fill_save_run_run(tc, "testSTLPersistence_map");
    cout << "Finished persistence test for map." << endl;
    
    tc = new HashMapTest();
    fill_save_run_run(tc, "testSTLPersistence_hashmap");
    cout << "Finished persistence test for hashmap." << endl;
    
  }
  catch (BIEException & e)
  { 
    cout << "Caught Exception: \n" << e.getErrorMessage() << endl; 
  }

}

