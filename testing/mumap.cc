
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <cstring>

using namespace std;

#include <unistd.h>

int
main (int argc, char** argv)
{

  double MUI = 0.0;
  double MUF = 0.67;

  // Parse command line
  int c;
  while (1) {
    c = getopt (argc, argv, "m:M:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'm': MUI = atof(optarg); break;
      case 'M': MUF = atof(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-m mu\t\tmapping exponent (input)\n";
	msg += "\t-M mu\t\tmapping exponent (output)\n";
	msg += "\t-h\t\tthis help message\n";
	cerr << msg; exit(-1);
      }
  }

  //

  int Number;
  int Nattrib;

  cin >> Number;
  cin >> Nattrib;

  cout << setw(15) << Number << endl;
  cout << setw(15) << Nattrib << endl;
  
  double l, x, v;

  for (int i=0; i<Number; i++) {
    cin >> l;
    cin >> x;
    if (MUI != 0.0)
      x = copysign(1.0, x) * asin(pow(fabs(x), 1.0/(1.0 - MUI)));

    x = pow(fabs(sin(x)), 1.0 - MUF) * copysign(1.0, x);

    cout << setw(15) << l;
    cout << setw(15) << x;
    
    for (int j=0; j<Nattrib; j++) {
      cin >> v;
      cout << setw(15) << v;
    }
    cout << endl;
  }

  char *line = new char [256];

  while (cin) {
    cin.getline(line, 256);
    if (strlen(line)) cout << line << endl;
  }

  delete [] line;

  return 0;
}
