/*******************************************************************************
* This is a collection of tests that checks error checking and sanity
* saving requirements are enforced when creating archives, together
* with tests of generally tricky transitive closures.
*******************************************************************************/
#include <iostream>
#include <bieTags.h>
#include <gvariable.h>
#include <config.h>

#include <EngineConfig.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <BIEException.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#include <FileBackend.h>

#include <VectorM.h>
#include <CVectorM.h>

#include <Serializable.h>

// For clivector
#include <Tessellation.h>
#include <Distribution.h>

using namespace BIE;

/* This is for simple save/load testing. */
int main() 
{
  system("rm -rf " PERSISTENCE_DIR);

  string statename = "testPersistence_clivector";

  TestUserState saveState(statename, 10, true);

  // clivectord

  clivectord vd(14);
  for (unsigned int i=0;i<14;i++) {
    vd()[i] = i + 100.12;
  }
  saveState.addObject(&vd);

  // clivectori

  clivectori vi(14);
  for (unsigned int i=0;i<14;i++) {
    vi()[i] = 2*i + 18;
  }
  saveState.addObject(&vi);

  TestSaveManager testSaveManager(&saveState, BOOST_XML, BACKEND_FILE);
  testSaveManager.save();

  // Some time later
  
  TestUserState *loadState;
  TestLoadManager testLoadManager(&loadState, statename, 11,
				  BOOST_XML, BACKEND_FILE);
  testLoadManager.load();

  vector<Serializable*> p = loadState->getTransClosure().objectlist;

  clivectord *vd2 = dynamic_cast<clivectord*>(p[0]);
  clivectori *vi2 = dynamic_cast<clivectori*>(p[1]);
  
  for (unsigned int i=0; i<14; i++)
    cout << setw(8) << vd()[i] << setw(8) << (*vd2)()[i] 
	 << setw(8) << vi()[i] << setw(8) << (*vi2)()[i] 
	 << endl;

  // save the loaded stuff to check for correctness
  TestSaveManager testSaveManager2(loadState, BOOST_XML, BACKEND_FILE);
  testSaveManager2.save();
}

