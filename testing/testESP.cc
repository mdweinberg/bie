/**
   For leak testing EnsembleStat & Chain
*/


#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

#include <stdlib.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <EnsembleStat.h>
#include <Chain.h>
#include <ACG.h>
#include <Uniform.h>
#include <BIEmpi.h>

#include <DummyModel.h>

using namespace BIE;

string priorfile("prior.dat");
string datafile("data.dat");

/**
   Test routine for data accumulation
*/
int
main(int argc, char** argv)
{
  const int nmix = 2;
  const int ndim = 2;
  const int nstates = 100;
  const int nensembles = 100;
  const int ntimes = 5;

  // ==================================================
  // MPI init
  // ==================================================

  mpi_init(argc, argv);

  BIE::mpi_used = 1;

  BIEgen = new BIEACG(11+myid, 20);
  Uniform *unit = new Uniform(0.0, 1.0, BIEgen);
  
  // ==================================================
  // MAIN LOOP
  // ==================================================
  
  StateInfo *si = new StateInfo(nmix, ndim);
  int dim = si->Ntot;

  vector<double> state(dim);
  vector<Chain> particles;

  for (int n=0; n<ntimes; n++) {

    state[0] = nmix;

    for (int k=0; k<nensembles; k++) {

#ifdef ES
      EnsembleStat *sstat = new EnsembleStat(nmix, ndim);
#endif

      particles.clear();

      for (int i=1; i<nstates; i++) {

	double sum = 0.0;
      
	for (int j=0; j<dim;   j++) state[j] = (*unit)();
	for (int j=0; j<nmix; j++) sum += state[j];
	for (int j=0; j<nmix; j++) state[j] /= sum;
      
	State s(si, state);
	particles.push_back(Chain(s));

#ifdef ES
	sstat->AccumData(1.0, s);
#endif
      }

#ifdef ES
      sstat->ComputeDistribution();
      sstat->PrintDiag(cout);

      delete sstat;
#endif
    }

    for (unsigned i=0; i<particles.size(); i++) {
      particles[i].BroadcastChain();
    }

  }
  
  // ==================================================
  // Anal retentive delete
  // ==================================================

  particles.clear();		// Delete Chain instances by 
				// forcing destructor calls

  delete BIEgen;
  delete unit;
  delete si;

  // ==================================================
  // ALL DONE with MPI
  // ==================================================

  MPI_Finalize();

  return 0;
}

