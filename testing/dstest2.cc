#include <string>
#include <vector>
#include <sstream>
using namespace std;

#include <BasicType.h>
#include <TypedBuffer.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStream.h>
#include <RecordInputStream.h>
#include <RecordStream_Ascii.h>
#include <RecordStream_Binary.h>
#include <RecordOutputStream.h>
#include <RecordStreamFilter.h>
#include <BinaryFilters.h>
#include <UnaryFilters.h>
#include <SetFilters.h>
#include <BIEException.h>

using namespace BIE;

int main ()
{
  RecordType * rt = new RecordType();
  rt = rt->insertField(1, "b", BasicType::Real);
  rt = rt->insertField(2, "a", BasicType::String);
  rt = rt->insertField(3, "c", BasicType::Int);
  rt = rt->insertField(4, "d", BasicType::Bool);  
  RecordInputStream * risa;
  
  try {
    risa = new RecordInputStream_Ascii(rt, "asdasdasd.dat");
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
  risa = new RecordInputStream_Ascii(rt, "streamtest.dat");

  try {
  cout << "Result of nextRecord is: " << risa->nextRecord() << "\n";
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  cout << risa->getBuffer()->toString() << "\n";
  
  RecordInputStream * risa2 = risa->deleteField(1);   
  cout << "deleteField(1) : \n" << risa2->getBuffer()->toString() << "\n";

  RecordInputStream * risa3 = risa2->renameField("c", "x"); 
  cout << "renameField(c, x) : \n" << risa3->getBuffer()->toString() << "\n";

  RecordInputStream * risa4 = risa3->renameField(3, "y"); 
  cout << "renameField(3, y) : \n" << risa4->getBuffer()->toString() << "\n";

  try {
    cout << "Calling next record:" << risa4->nextRecord() << "\n";
  }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  
  cout << "risa : \n" << risa->getBuffer()->toString() << "\n";
  cout << "risa2: \n" << risa2->getBuffer()->toString() << "\n";
  cout << "risa3 : \n" << risa3->getBuffer()->toString() << "\n";
  cout << "risa4 : \n" << risa4->getBuffer()->toString() << "\n";

  RecordType * rt2 = new RecordType();
  rt2 = rt2->insertField(1, "aa", BasicType::String);
  rt2 = rt2->insertField(1, "bb", BasicType::Real);
  rt2 = rt2->insertField(3, "cc", BasicType::Int);
  rt2 = rt2->insertField(4, "dd", BasicType::Bool);  

  RecordInputStream * risa5 = new RecordInputStream_Ascii(rt2, "streamtest.dat");

  RecordInputStream * risa6 = risa4->joinWithStream(2, risa5);
     
  try {
   cout << "Calling next record on risa6:" << risa6->nextRecord() << "\n";
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }  
  
  cout << "risa : \n" << risa->getBuffer()->toString() << "\n";
  cout << "risa2: \n" << risa2->getBuffer()->toString() << "\n";
  cout << "risa3 : \n" << risa3->getBuffer()->toString() << "\n";
  cout << "risa4 : \n" << risa4->getBuffer()->toString() << "\n";
  cout << "risa5 : \n" << risa5->getBuffer()->toString() << "\n";
  cout << "risa6 : \n" << risa6->getBuffer()->toString() << "\n";

  try {
    cout << "Calling next record:" << risa6->nextRecord() << "\n";
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  cout << "risa : \n" << risa->getBuffer()->toString() << "\n";
  cout << "risa2: \n" << risa2->getBuffer()->toString() << "\n";
  cout << "risa3 : \n" << risa3->getBuffer()->toString() << "\n";
  cout << "risa4 : \n" << risa4->getBuffer()->toString() << "\n";
  cout << "risa5 : \n" << risa5->getBuffer()->toString() << "\n";
  cout << "risa6 : \n" << risa6->getBuffer()->toString() << "\n";

  try {
    cout << "Calling next record:" << risa6->nextRecord() << "\n";
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  cout << "risa : \n" << risa->getBuffer()->toString() << "\n";
  cout << "risa2: \n" << risa2->getBuffer()->toString() << "\n";
  cout << "risa3 : \n" << risa3->getBuffer()->toString() << "\n";
  cout << "risa4 : \n" << risa4->getBuffer()->toString() << "\n";
  cout << "risa5 : \n" << risa5->getBuffer()->toString() << "\n";
  cout << "risa6 : \n" << risa6->getBuffer()->toString() << "\n";

  try {
    cout << "Calling next record:" << risa6->nextRecord() << "\n";
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  cout << "risa : \n" << risa->getBuffer()->toString() << "\n";
  cout << "risa2: \n" << risa2->getBuffer()->toString() << "\n";
  cout << "risa3 : \n" << risa3->getBuffer()->toString() << "\n";
  cout << "risa4 : \n" << risa4->getBuffer()->toString() << "\n";
  cout << "risa5 : \n" << risa5->getBuffer()->toString() << "\n";
  cout << "risa6 : \n" << risa6->getBuffer()->toString() << "\n";

  RecordType * rt3 = rt2->moveField(3,4);
  
  RecordInputStream * newrisa = new RecordInputStream_Ascii(rt2, "streamtest.dat"); 
  RecordInputStream * risa7 = newrisa->selectFields(rt3); 
  risa7->nextRecord();
  cout << "selectFields with type:\n" << rt3->toString()  << "\n" ;
  cout << risa7->getBuffer()->toString() << "\n";

  RecordInputStream * risa8 = risa7->moveField("dd", 1); 
  cout << "moveField(dd, 1) : \n" << risa8->getBuffer()->toString() << "\n";

  try { risa8->getStringValue(1); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  try { risa8->getIntValue(1); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  try { risa8->getRealValue(1); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  cout << "Value of field one: " << risa8->getBoolValue(1) << "\n"; 


  try { risa8->getStringValue("dd"); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  try { risa8->getIntValue("dd"); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  try { risa8->getRealValue("dd"); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
  cout << "Value of field dd: " << risa8->getBoolValue("dd") << "\n"; 

  try {
    cout << "Calling next record:" << risa8->nextRecord() << "\n";
    }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "Calling next record:" << risa8->nextRecord() << "\n";
    }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "Calling next record:" << risa8->nextRecord() << "\n";
    }
  catch (BIEException e) { cout << e.getErrorMessage() << endl; }
}
