/*******************************************************************************
* This is a collection of tests that checks the persistence library
* can successfully save and restore a variety of STL container classes.
*******************************************************************************/

#include <iostream>
#include <config.h>
#include <fstream>

#include <bieTags.h>
#include <Serializable.h>
#include <gvariable.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>

#include <BIEACG.h>
#include <Uniform.h>
#include <gvariable.h>

using namespace std;

/*******************************************************************************
* Superclass for all test classes in this regression test.
*******************************************************************************/
class TestClass : public Serializable 
{

public:
  TestClass() {}
  virtual ~TestClass() {}
  virtual void fillWithData() = 0;
  virtual void print(ostream & out) = 0;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
  }
};

BOOST_CLASS_EXPORT(TestClass)

/*******************************************************************************
* Test class for several kinds of vector objects.
*******************************************************************************/
class ACGTest : public TestClass {
public:
  ACGTest() {}
  ~ACGTest() {}
  void fillWithData();
  void print(ostream & out);
  
private:
  BIEACG *gen;
  Uniform *unif;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(TestClass);
    ar & BOOST_SERIALIZATION_NVP(gen);
    ar & BOOST_SERIALIZATION_NVP(unif);
  }
};
BOOST_CLASS_EXPORT(ACGTest)

void ACGTest::fillWithData()
{
  // Initialize the random number generator.
  //
  gen = new BIEACG(11, 25);

  // Create a distribution
  //
  unif  = new Uniform(0, 1, gen);
}

void ACGTest::print(ostream & out)
{
  for (unsigned n=0; n<20; n++)
    out << setw(18) << (*unif)() << endl;
}


void fill_save_run_run(TestClass * tc, string statename)
{
  try {
    tc->fillWithData();
   
    TestUserState teststate(statename, 0, true);
    teststate.addObject(tc);
    {
      TestSaveManager testSaveManager(&teststate, BOOST_BINARY, BACKEND_FILE);
      testSaveManager.save();
    }
  
    TestUserState *restored;
    {
      TestLoadManager testLoadManager(&restored, 
				      teststate.getSession(), 
				      teststate.getVersion(), 
				      BOOST_BINARY, BACKEND_FILE);
      testLoadManager.load();
    }

    Serializable *loadedObj = *(restored->getTransClosure().objectlist.begin());
    TestClass* restored_tc = dynamic_cast<TestClass*>(loadedObj);

    fstream actualoutput;
    string filename = statename + "_actual";
    actualoutput.open(filename.c_str(), fstream::out);
    tc->print(actualoutput);
    actualoutput.close();

    fstream restoredoutput;
    filename = statename + "_restored";
    restoredoutput.open(filename.c_str(), fstream::out);
    restored_tc->print(restoredoutput);
    restoredoutput.close();
  }
  catch (BIEException & e)
  { cout << "Caught Exception: \n" << e.getErrorMessage() << endl; }
}

int main()
{

  system("rm -rf " PERSISTENCE_DIR);

  try {
    TestClass * tc;
    
    tc = new ACGTest();
    fill_save_run_run(tc, "testPersistence_BIEACG");
    cout << "Finished persistence test for BIEACG" << endl;

  }
  catch (BIEException & e)
  { cout << "Caught Exception: \n" << e.getErrorMessage() << endl; }

}

