#include <string>
#include <vector>
#include <strstream>
#include <BasicType.h>

#include <ArrayType.h>

#include <TypedBuffer.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStream.h>
#include <RecordInputStream.h>
#include <RecordStream_Ascii.h>
#include <RecordStream_Binary.h>
#include <RecordOutputStream.h>
#include <RecordStreamFilter.h>
#include <BinaryFilters.h>
#include <UnaryFilters.h>
#include <SetFilters.h>
#include <BIEException.h>

using namespace BIE;

int main ()
{
  /*****************************************************************************
  * Array Filter test.
  *****************************************************************************/
  try 
  {
    ArrayType *at = new ArrayType(BasicType::Int, 18);
    cout << at->toString() << endl;
    ArrayType *atr = new ArrayType(BasicType::Real, 4);

    //TypedBuffer *tb = new TypedBuffer_IntArray(at); 
    //cout << tb->toString();

    TypedBuffer *tb = TypedBuffer::createNew(at); 
    cout << tb->toString() << endl;

    RecordType *rt = new RecordType();
    RecordType *nrt = rt->insertField(1, "intarray", at);
    cout << nrt->toString() << endl;

    RecordType *rrt = new RecordType();
    RecordType *nrrt = rrt->insertField(1, "rarray", atr);

    RecordBuffer *rb = new RecordBuffer(nrt);
    cout << rb->toString();
    
/* dstest6.dat file
[int@4 "a"

11 12 13 14
21 22 23 24
31 32 33 34
41 42 43 44
51 52 53 54
61 62 63 64 
71 72 73 74
81 82 83 84
91 92 93 94
*/
//    RecordInputStream* ris = new RecordInputStream_Ascii("dstest6.dat");
    RecordInputStream* ris = new RecordInputStream_Ascii("dstest62.dat");
    cout << ris->getBuffer()->getType()->toString() << endl;

    RecordType *nrrt2 = nrrt->insertField(2, "r2", BasicType::Real);
    RecordType *nrrt3 = nrrt2->insertField(3, "r3array", atr);

    RecordOutputStream* ros = new RecordOutputStream(nrrt3);
    RecordOutputStream* aros = new RecordOutputStream_Ascii(ros, "dstest6.out", true);
    cout << ros->getBuffer()->getType()->toString() << endl;
    cout << aros->getBuffer()->getType()->toString() << endl;

    /*
    while(!ris->eos()) {
      cout << ris->nextRecord() << endl;
      cout << ris->getBuffer()->toString();

      vector<int> vint = ris->getBuffer()->getIntArrayValue(1);
      ros->setIntArrayValue(1,vint);
      ros->pushRecord();
    }
    */

    //    RecordStreamFilter* rsf = new AverageFilter(ris);
    RecordStreamFilter* rsf = new StatisticsFilter(ris);
    cout << rsf->toString();
    // stream inp#1 plumped to filter inp#1 
    rsf->connect(1, 1);
    // field#2 is the output of filter
    RecordInputStream *nrisa = ris->filterWith(2, rsf);
    cout << nrisa->getBuffer()->toString() << endl;

    RecordStreamFilter* ssf = new ScaleFilter(nrisa, 2.0);
    cout << ssf->toString();
    ssf->connect(1,1);
    RecordInputStream *snrisa = nrisa->filterWith(3, ssf);
    cout << snrisa->getBuffer()->toString() << endl;

    while(!snrisa->eos()) {
      bool done = snrisa->nextRecord();
      if (!done) break;
      cout << snrisa->getBuffer()->toString();

      vector<double> vdbl = snrisa->getBuffer()->getRealArrayValue(1);
      double r2 = snrisa->getBuffer()->getRealValue(2);
      vector<double> svdbl = snrisa->getBuffer()->getRealArrayValue(3);
      ros->setRealArrayValue(1,vdbl);
      ros->setRealValue(2,r2);
      ros->setRealArrayValue(3,svdbl);
      ros->pushRecord();
    }


  }
  catch (...)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << "not a bie exception" << endl;
  }

}
