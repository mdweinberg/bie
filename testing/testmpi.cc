//
//      Test routine demonstrating passing char string commands from
//      Node 0 to all other nodes
//
//	Compile command:
//      ---------------
//	hcp -I../include -I$(LAMHOME)/include -I$(LAMHOME)/include/mpi2c++ \
//            -g -static -o testmpi testmpi.cc ../src/BIEmpi.o -lmpi -lm
//
//      Execute command:
//      ---------------
//      mpirun -c 4 ./testmpi
//
//      Type string after "Input? " prompt. Nodes will print out command
//      string received.  Type "quit" to quit.
//

#include <unistd.h>
#include <stdlib.h>
#include <iostream.h>
#include <mpi.h>
#include <string>

#include <BIEmpi.h>


const int BUFSIZE = 64;		// Size of the character buffer
char buf[BUFSIZE];		// And the buffer itself

int
main(int argc, char *argv[])
{

				// Use BIEmpi stuff to initialize
  mpi_init(argc, argv);


				// If I am the boss, ask for input
  if (myid==0) {

    while (1) {
      cout << "Input? ";
      cin.getline(buf, BUFSIZE);
				// Bcast sends buf to all nodes
      MPI_Bcast(buf, BUFSIZE, MPI_CHAR, 0, MPI_COMM_WORLD);
      //                                ^
      //                                |
      // This says that the message is broadcast from Node 0
      //

				// Quit if input says "quit"
      if ((string)buf == "quit") break;

				// Wait a bit to give slaves a chance to
				// print . . .
      sleep(1);

    }

  }
  else {			// I am a slave . . . 

    while (1) {
				// Need a bcast here to receive from Node 0
      MPI_Bcast(buf, BUFSIZE, MPI_CHAR, 0, MPI_COMM_WORLD);
      
      cout << "Process " << myid << " was told: " << buf << endl;

				// Quit if input says "quit"
      if ((string)buf == "quit") break;
    }

  }
    
  cout << "Process " << myid << " is exiting" << endl;

  MPI_Finalize();
  return(0);
}
