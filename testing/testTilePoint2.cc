// This should have the same behavior as examples/Splats/parallel/script0

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

#include <sys/time.h>
#include <sys/resource.h>

using namespace std;

#include <ACG.h>

#define IS_MAIN

#include <TemperedSimulation.h>

#include <CountConverge.h>
#include <SubsampleConverge.h>
#include <MappedGrid.h>
#include <KdTessellation.h>
#include <QuadGrid.h>
#include <SquareTile.h>
#include <RecordStream_Ascii.h>
#include <DataTree.h>
#include <LikelihoodComputationMPITP.h>

#include <Model.h>
#include <InitialMixturePrior.h>
#include <PostMixturePrior.h>
#include <Histogram1D.h>
#include <EnsembleStat.h>
#include <GalaxyModelOneDHashed.h>

#include <AdaptiveLegeIntegration.h>
#include <LegeIntegration.h>
#include <FivePointIntegration.h>
#include <MetropolisHastings.h>

#include <RunOneSimulation.h>

enum TessType {kd_inf, kd, mapped, quad};
enum ConvType {counter, subsample};

using namespace BIE;

/**
   @name multilevel: main
   Does full simulation at multiple levels of resolution.  Parallel
   MPI implementation.
*/
int main(int argc, char** argv)
{
  // Hold instantiation of classes to define the simulation
  //
  GalaxyModelOneDHashed *model;
  SquareTile *tile;
  RecordInputStream_Ascii *ris;
  Histogram1D *histo;
  Integration *intgr;
  Tessellation *tess;
  DataTree *dist;
  LikelihoodComputationMPITP *like;
  MHWidth *mhwidth;
  TemperedSimulation *sim;
  RunOneSimulation *run;
  MetropolisHastings *mca;
  StateInfo *si;

  //
  // Initialize mpi
  //
  cout << "calling mpi_init\n";
  mpi_init(argc, argv);
  

  // Initialize base random number generator
  //
  // BIEgen = new ACG(seed, 20);
  cout << "create new ACG.\n";
  BIEgen = new BIEACG(11, 20);


  // Factor for 1D histogram in each tile (put everything in one bin)
  //
  cout << "create new Histogram1D.\n";
  histo = new Histogram1D(6.0, 15.0, 1.0, "mag");


  // Galaxy model
  unsigned nmix = 2;
  unsigned ndim = 2;
  cout << "create new Galaxy Model.\n";
  model = new GalaxyModelOneDHashed(ndim, nmix, histo);
  model->SetKnots(200);

  // State metadata
  cout << "create new StateInfo.\n";
  si = new StateInfo(nmix, ndim);

  // Define prior
  //
  cout << "create new InitialMixturePrior.\n";
  UniformDist unif1(0.5, 8.0);
  UniformDist unif2(100.0, 1200.0);
  clivectordist vdist(2);
  vdist()[0] = &unif1;
  vdist()[1] = &unif2;
  MixturePrior* prior = new InitialMixturePrior(si, 1.0, &vdist);


  // Simple sample statistic
  //
  // EnsembleStat* sstat = new EnsembleStat [L];
  cout << "create new EnsembleStat.\n";
  EnsembleStat* sstat = new EnsembleStat [6];

  // Rectangular grid tessellation
  //

  cout << "create new SquareTile.\n";
  tile = new SquareTile();

  // integration method
  cout << "create new AdaptiveLegeIntegration.\n";
  //intgr = new AdaptiveLegeIntegration(0.2, 0.1);
  intgr = new LegeIntegration(10, 10);

  //cout << "create new FivePointIntegration.\n";
  //intgr = new FivePointIntegration();

  // Convergence method
  //
  Converge *convrg = new SubsampleConverge(200, &sstat[0], "");

  // Tessellation
  //cout << "create new KdTessellation.\n";
  //tess = new KdTessellation(tile,ris,100,1.0,-1.0,2.0,-1.0,2.0);
  cout << "create new QuadGridTessellation.\n";
  tess = new QuadGrid(tile, -3.14159, 3.14159,-0.2, .2, 5);

  // distribution
  cout << "create new RecordInputStream_Ascii." << endl;
  ris = new RecordInputStream_Ascii("galaxy.data.2");

  cout << "create new Fulldistribution.\n";
  dist = new DataTree(ris,histo,tess);
  
  cout << "create new LikelihoodComputationMPITP.\n";
  like = new LikelihoodComputationMPITP(si);

  cout << "create new MetropolisHastings.\n";
  mca = new MetropolisHastings();

  cout << "crate new MHWidthOne.\n";
  vector<double> mvec(3, 0.1);
  mvec[2] = 10.0;
  mhwidth = new MHWidthOne(si, &mvec);

  cout << "create new TemperedSimulation.\n";
  sim = new TemperedSimulation(si, 6, 1024.0, mhwidth,
			       dist, model, intgr, convrg, prior, like, mca);

  // Simple RunOneSimulation
  cout << "create new RunOneSimulation.\n";
  run = new RunOneSimulation(2/*200*/, 0.1, sstat, prior, sim);
  run->Run();

  cout << "returned from RunSimulation\n";
  sleep(30); // change to a mutex!
  cout << "testTilePoint exiting\n";
}
