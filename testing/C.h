// -*- C++ -*-

#ifndef _C_h
#define _C_h

#include <iostream>
#include <vector>

#include <Serializable.h>

template <class T>
class vectortest : public Serializable
{
public:
  vectortest() {}
  vectortest(int n, T z) { v = std::vector<T>(n, z); }
  void print(std::ostream & out);
  
private:
  std::vector<T> v;

  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
    ar & BOOST_SERIALIZATION_NVP(v);
  }
};

template <class T>
void vectortest<T>::print(std::ostream & out)
{
  for (unsigned int i = 0; i < v.size(); i++)
    { out << v[i] << ", ";  }
  out << std::endl;
}

typedef vectortest<double> vectord;

class VectorTest : public vectord
{
public:
  VectorTest() {}
  VectorTest(int n, double z) : vectord(n, z) {}

private:

  friend class boost::serialization::access;

  template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(vectord);
  }
};


BOOST_CLASS_EXPORT_KEY(vectord)
BOOST_CLASS_EXPORT_KEY(VectorTest)

#endif
