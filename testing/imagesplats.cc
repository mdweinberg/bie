#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cmath>
#include <cassert>

using namespace std;

#include <unistd.h>

struct Stanza 
{
  unsigned level;
  unsigned step;
  double prob;
  double like;
  double prior;
  unsigned M;
  unsigned N;
  vector<double> w;
  vector< vector<double> > p;
};

vector<string> labels;
vector<Stanza> stanzas;

int main(int argc, char **argv)
{
  const int bufsize = 2048;
  char buffer[bufsize];

  unsigned M=0, N=0;
  double xmin = -1.0;
  double xmax =  2.0;
  double ymin = -1.0;
  double ymax =  2.0;
  int numx = 100;
  int numy = 100;
  string filename("tmp.dat");
  
  while (1) {

    int c = getopt(argc, argv, "x:X:y:Y:1:2:f:");
    if (c==-1) break;

    switch (c) {

    case 'x':
      xmin = atof(optarg);
      break;
      
    case 'X':
      xmax = atof(optarg);
      break;
      
    case 'y':
      ymin = atof(optarg);
      break;
      
    case 'Y':
      ymax = atof(optarg);
      break;
      
    case '1':
      numx = atoi(optarg);
      break;
      
    case '2':
      numy = atoi(optarg);
      break;
      
    case 'f':
      filename = string(optarg);
      break;

    default:
      cout << "No such argument: " << c << endl;
      cout << "Arguments: " << optind << endl;
      exit(-1);
    }
  }

  while (optind == argc) {
    cout << "No file name specified" << endl;
    exit(-1);
  }

  ifstream in(argv[optind]);

  cout << "Opening filename: " << filename;
  ofstream out(filename.c_str());
  if (out) cout << " . . . ok" << endl;
  else {
    cout << " . . . error" << endl;
    exit(-1);
  }

  while (in) {
    in.getline(buffer, bufsize);
    string line(buffer);

    if (line.find('"') != string::npos) {

      // This is a label record dump stanzas and start again

      labels.erase(labels.begin(), labels.end());
      stanzas.erase(stanzas.begin(), stanzas.end());

      // Break into words
      unsigned long pos = 0, pos1, pos2;
      while (pos != string::npos) {
	pos1 = pos;
	pos = pos2 = line.find('"', pos1+1);
	if (pos != string::npos) {
	  labels.push_back(line.substr(pos1+1, pos2-pos1-1));
	  pos = line.find('"', pos2+1);
	}
      }

      M = 0;
      while (labels[6+M].substr(0,1)=="W" && labels[6+M].substr(1,1)=="(") {
	M++;
      }
      N = (labels.size()-6-M)/M;
      assert ( (N+1)*M+6 == labels.size() );

    } else {			
      // Store the stanza
      Stanza stanza;
      istringstream ins(buffer);
      ins >> stanza.level;
      ins >> stanza.step;
      ins >> stanza.prob;
      ins >> stanza.like;
      ins >> stanza.prior;
      ins >> stanza.M;
      stanza.N = N;
      double val;
      for (unsigned m=0; m<M; m++) {
	ins >> val;
	if (m<stanza.M) stanza.w.push_back(val);
      }
      for (unsigned m=0; m<stanza.M; m++) {
	vector<double> pp;
	if (m<stanza.M) {
	  for (unsigned j=0; j<stanza.N; j++) {
	    ins >> val;
	    pp.push_back(val);
	  }
	}
	stanza.p.push_back(pp);
      }
      stanzas.push_back(stanza);
    }
  }

  cout << "Found: " << labels.size() << " fields" << endl;
  cout << "Found: " << stanzas.size() << " records" << endl;
  cout << endl;
  cout << "Maximum number of components: " << M << endl;
  cout << "Component dimension: " << N << endl;
  cout << "Labels:" << endl;
  for (unsigned i=0; i<N; i++) {
    cout << setw(5) << i << setw(20) 
	 << labels[6+M+i].substr(0,labels[6+M+i].find("(")) << endl;
  }
  cout << endl;

  // Scan through data
  vector<Stanza>::iterator p;
  unsigned curl = 0;
  unsigned last = 0;
  vector<int> cnt(M, 0);

  for (p=stanzas.begin(); p!=stanzas.end(); p++) {
    if (p->level != curl) {
      cout << "Level " << curl << " last step=" << last << endl;
      for (unsigned i=0; i<M; i++) {
	cout << setw(5) << i+1 << ": " << cnt[i] << endl;
	cnt[i] = 0;
      }
      curl = p->level;
      last = 0;
    }
    cnt[p->M-1]++;
    last = p->step;
  }
  cout << "Level " << curl << " last step=" << last << endl;
  cout << endl;

  // Get info from user

  unsigned level, step;
  double sx, sy;

  // Scan for state
  while (1) {
    cout << "level step? ";
    cin >> level;
    cin >> step;
    for (p=stanzas.begin(); p!=stanzas.end(); p++) {
      if (p->level == level && p->step == step) break;
    }
    if (p==stanzas.end())
      cout << "Coudn't find state!" << endl;
    else
      break;
  }

  cout << "State:" << endl;
  for (unsigned m=0; m<p->M; m++) {
    cout << setw(5) << m << ": ";
    cout << setw(15) << p->w[m];
    for (unsigned j=0; j<N; j++) cout << setw(15) << p->p[m][j];
    cout << endl;
  }
  cout << endl;

  if (N==2) {
    cout << "sx sy? ";
    cin >> sx;
    cin >> sy;
  }

  double dx = (xmax - xmin)/(numx-1);
  double dy = (ymax - ymin)/(numy-1);
  double x1, x2, y1, y2, val;

  for (int i=0; i<numx; i++) {
    x1 = xmin + dx*i;
    x2 = x1 + dx;
    for (int j=0; j<numy; j++) {
      y1 = ymin + dy*j;
      y2 = y1 + dy;

      val = 0.0;
      for (unsigned m=0; m<p->M; m++) {

	if (N==4) {
	  sx = p->p[m][2];
	  sy = p->p[m][3];
	}

	val += p->w[m] * 0.25 * 
	  ( erf((x2 - p->p[m][0])/sqrt(2.0*sx)) - 
	    erf((x1 - p->p[m][0])/sqrt(2.0*sx)) ) *
	  ( erf((y2 - p->p[m][1])/sqrt(2.0*sy)) - 
	    erf((y1 - p->p[m][1])/sqrt(2.0*sy)) ) ;
      }

      out << setw(18) << 0.5*(x1+x2) << setw(18) << 0.5*(y1+y2)
	   << setw(18) << val << endl;
    }
    out << endl;
  }

  return 0;
}
