#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>

#include <ACG.h>
#include <Uniform.h>
#include <Normal.h>
#include <BasicType.h>
#include <UnaryFilters.h>

#include <RecordStream_Ascii.h>

#define IS_MAIN
#define USE_ND

#include <BIEconfig.h>
#ifdef USE_ND
#include <GalaxyModelND.h>
#else
#include <GalaxyModelTwoD.h>
#endif
#include <HistogramND.h>

using namespace BIE;

const double onedeg = M_PI/180.0;
const double Log10  = log(10.0);

int    ISEED   = 11;
bool   MU      = false;
bool   REMOVE  = false;
bool   VERBOSE = false;
double MUEXP   = 0.67;
double R0      = 8.0;
double A       = 3.5;
double A1      = 20.0;
double Z1      = 100.0;
double H       = 350.0;
double AK      = 0.1;
double MMIN    = 6.0;
double MMAX    = 16.0;
double MWID    = 1.0;

int nparam, nflux;
vector<double> vx, vy, sx, sy, w;

class MakeStar
{
private:
  ACG* gen;
  Uniform* unit;
  Normal* var;
  double mmax;
  
public:

  static int MAXITER;
  static double TOL;

  MakeStar(double rmax)
  {
    gen = new ACG(ISEED);
    unit = new Uniform(0.0, 1.0, gen);
    var = new Normal(0.0, 1.0, gen);
    mmax = 1.0 - (1.0 + rmax/A)*exp(-rmax/A);
  }

  ~MakeStar()
  {
    delete gen;
    delete unit;
  }

  vector<double> realize()
  {

    vector<double> ret(4);

    double fac, del, rran = log(1.0 - mmax*(*unit)());
    double z = 1.0e-3*H*atanh(2.0*(*unit)() - 1.0);

    double r = -rran;
    for (int i=0; i < MAXITER; i++) {
      fac = 1.0 + r;
      del = fac*(log(fac) - r - rran)/r;
      r += del;
      if (fabs(del) < TOL) break;
    }
    r *= A;


    double phi = 2.0*M_PI*(*unit)();
    double x = r*cos(phi) + R0;
    double y = r*sin(phi);
    double s = sqrt(x*x + y*y + z*z);
    
    double L = atan2(y, x);
    double B = asin(z/s);

    double sinL = sin(L);
    double cosL = cos(L);
    double sinB = sin(B);
    double cosB = cos(B);

    double smaxr, smaxz, smax;
    if (fabs(cosB)<1.0e-6) 
      smaxr = 1.0e30;
    else
      smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));
    
    if (fabs(sinB)<1.0e-6) 
      smaxz = 1.0e30;
    else
      smaxz = Z1*1.0e-3/fabs(sinB);
    
    smax = min<double>(smaxr, smaxz);

				// Choose class
    double type = (*unit)();
    int itype = 0;
    for (int k=0; k<nparam; k++) {
      if (type < w[k]) break;
      itype++;
    }

    ret[0] = L;
    ret[1] = B;
    ret[2] = vx[itype] + sx[itype]*(*var)() + 5.0*log(s*1.0e3)/Log10 - 5.0 + AK*min<double>(s, smax);
    ret[3] = vy[itype] + sy[itype]*(*var)() + 5.0*log(s*1.0e3)/Log10 - 5.0 + AK*min<double>(s, smax);

    return ret;
  }

};

int MakeStar::MAXITER = 1000;
double MakeStar::TOL = 1.0e-10;

int 
main(int argc, char** argv)
{
  bool los_test    = false;
  double L         = 10.0;
  double B         =  5.0;
  double dL        = 0.5;
  double dB        = 0.5;
  double dLB       = 0.5;
  double maxR      = 25.0;
  int number       = 1000;
  string BASISDATA = "basisdata.2d";
  string OUTPUT    = "galaxy_ascii.dat";
  
  int c;
  while (1) {
    c = getopt (argc, argv, "M:A:B:S:H:Z:D:R:i:l:b:m:trs:o:vh");
    if (c == -1) break;
     
    switch (c)
      {
      case 'M': number = atoi(optarg); break;
      case 'A': A = atof(optarg); break;
      case 'H': H = atof(optarg); break;
      case 'B': A1 = atof(optarg); break;
      case 'Z': Z1 = atof(optarg); break;
      case 'i': ISEED = atoi(optarg); break;
      case 'l': L = atof(optarg); break;
      case 'b': B = atof(optarg); break;
      case 'D': dLB = atof(optarg); break;
      case 'R': maxR = atof(optarg); break;
      case 'm': MU=true; MUEXP = atof(optarg); break;
      case 'r': REMOVE=true; break;
      case 't': los_test = true; break;
      case 's': BASISDATA.erase(); BASISDATA = optarg; break;
      case 'o': OUTPUT.erase(); OUTPUT = optarg; break;
      case 'v': VERBOSE=true; break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-M n\t\tnumber of stars\n";
	msg += "\t-A z\t\texponential disk scale length\n";
	msg += "\t-H z\t\tdisk scale height\n";
	msg += "\t-B z\t\tsize of extinction disk\n";
	msg += "\t-Z z\t\textinction scale height\n";
	msg += "\t-0 z\t\tmean of standard candle distribution\n";
	msg += "\t-i x\t\tseed for random number generator\n";
	msg += "\t-l x\t\tlongitude in degrees\n";
	msg += "\t-b x\t\tlatitude in degrees\n";
	msg += "\t-D x\t\tlongitude, latitude window in degrees\n";
	msg += "\t-R x\t\tmaximum stellar galacocentric radius\n";
	msg += "\t-m x\t\tmu transform\n";
	msg += "\t-r\t\tremove \"b\" field\n";
	msg += "\t-v\t\tverbose diagnostics\n";
	msg += "\t-t x\t\ttest line-of-sight distribution\n";
	cerr << msg; exit(-1);
      }
  }

  // Read in parameters
  //
  ifstream in(BASISDATA.c_str());
  if (!in) {
    string msg = "could not open BASISDATA file: " + BASISDATA;
    cerr << msg << endl;
    exit(-1);
  }

  const int linesize = 512;
  char line[linesize];
  
  {
    in.getline(line, linesize);
    istringstream ins(line);
    ins >> nparam;
  }
  {
    in.getline(line, linesize);
    istringstream ins(line);
    ins >> nflux;
  }
  if (nflux!=2) {
    string msg = "wrong number of fluxes, must be 2";
    cerr << msg << endl;
    exit(-1);
  }
    
  for (int i=0; i<nparam; i++) {
    double xx, yy, sxx, syy, ww;
    in.getline(line, linesize);
    istringstream ins(line);
    ins >> xx;		vx.push_back(xx);
    ins >> yy;		vy.push_back(yy);
    ins >> sxx;		sx.push_back(sxx);
    ins >> syy;		sy.push_back(syy);
    ins >> ww;		w.push_back(ww);
  }
  
  for (int i=1; i<nparam; i++) w[i] += w[i-1];
  for (int i=0; i<nparam; i++) w[i] /= w[nparam-1];

				// Default line-of-sight integration knots

  if (los_test) {

    const int NUM1 = 10;
    LegeQuad intgr(NUM1);
    
    L  *= onedeg;
    B  *= onedeg;
    dL  = onedeg*dLB;
    dB  = onedeg*dLB;

    // Set global parameters
    //
#ifdef USE_ND
    GalaxyModelND::RMAX = maxR;
    GalaxyModelND::NUM = 200;
#else
    GalaxyModelTwoD::RMAX = maxR;
    GalaxyModelTwoD::NUM = 200;
#endif

    // Hold instantiation of classes define the simuation
    //
#ifdef USE_ND
    GalaxyModelND *model;
#else
    GalaxyModelTwoD *model;
#endif
    HistogramND *histo;

    int ndim = 2;		// Two model parameters

				// One model
    int nmix = 1;

				// Factor for 1D histogram in each tile
    clivectord lo_b(2, MMIN), hi_b(2, MMAX), w_b(2, MWID);

    clivectors names;
    names().push_back("namedattribute");
    names().push_back("namedattribute2");
    
    histo = new HistogramND(&lo_b, &hi_b, &w_b, &names);


				// Galaxy model
#ifdef USE_ND
    GalaxyModelND::BASISDATA = BASISDATA;
    model = new GalaxyModelND(ndim, nmix, histo);
#else
    model = new GalaxyModelTwoD(ndim, nmix, histo);
#endif

    vector<double> winit(nmix);
    vector< vector<double> > pinit(nmix);

    winit[0] = 1.0;
    pinit[0] = vector<double>(ndim);
    pinit[0][0] = A;
    pinit[0][1] = H;
  
    // Intregate over areal region
    //
    model->Initialize(winit, pinit);
    
    double norm = 0.0, fac;
    vector<double> theory;
    double xmax = asin(B+dB);
    double xmin = asin(B-dB);
    
    for (int ii=1; ii<=NUM1; ii++) {
      double LL = L + 2.0*dL*(intgr.knot(ii) - 0.5);
      for (int jj=1; jj<=NUM1; jj++) {
	double BB = sin(xmin + (xmax - xmin)*intgr.knot(jj));
	double norm1 = model->NormEval(LL, BB, histo);
	vector<double> theory1 = model->Evaluate(LL, BB, histo);

	if (theory.size() != theory1.size())
	  theory = vector<double>(theory1.size(), 0.0);

	fac = 2.0*dL*(xmax - xmin)*intgr.weight(ii)*intgr.weight(jj);
	for (int kk=0; kk<(int)theory.size(); kk++) 
	  theory[kk] += fac*theory1[kk];
	norm += fac*norm1;
      }
    }

    // Do simulation
    //
    MakeStar star(maxR);
    vector<double> ans;

    int icnt = 0;
    int ioff = 0;

    RecordBuffer * buffer = new RecordBuffer();
    buffer = buffer->insertField(1, "namedattribute", BasicType::Real);
    buffer = buffer->insertField(2, "namedattribute2", BasicType::Real);
    
    while (1) {

      ans = star.realize();

      if (fabs(ans[0] - L) <= dL  &&
	  fabs(ans[1] - B) <= dB  ) {
	
				// Off the lower end of grid
	if (ans[2] <  MMIN || ans[3] <  MMIN  || 
	    ans[2] >= MMAX || ans[3] >= MMAX  ) ioff++;

	else {

	  buffer->setRealValue(1, ans[2]);
	  buffer->setRealValue(2, ans[3]);
	  histo->AccumData(1.0, buffer);
	  icnt++;
	
	  if (icnt>number) break;

	  if ( icnt - (int)(icnt/100)*100 == 0 ) 
	    cout << "\rNumber so far: " << icnt << flush;
	}

      }

    }
      
    cout << endl << ioff << " points off the grid\n";

    for (int i=0; i<histo->numberData(); i++) {
      cout
	<< setw(15) << histo->getLow(i)[0]
	<< setw(15) << histo->getHigh(i)[0]
	<< setw(15) << histo->getLow(i)[1]
	<< setw(15) << histo->getHigh(i)[1]
	<< setw(15) << theory[i]/norm
	<< setw(15) << histo->getValue(i)/icnt
	<< setw(15) << sqrt(histo->getValue(i))/icnt
	<< endl;
    }
    
  } else {

    MakeStar star(maxR);
    vector<double> ans;

    RecordType * rt = new RecordType();
    rt = rt->insertField(1, "x", BasicType::Real);
    if (MU) rt = rt->insertField(2, "b", BasicType::Real);
    else    rt = rt->insertField(2, "y" , BasicType::Real);
    rt = rt->insertField(3, "mag1", BasicType::Real);
    rt = rt->insertField(4, "mag2", BasicType::Real);
    
    RecordOutputStream *ros0  = new RecordOutputStream(rt);
    RecordOutputStream *ros1;
  
    if (MU) {
      RecordStreamFilter * filter = new MuFilter(ros0, 0.67);
      filter->connect("b", "x");
      filter->renameOutputField("mu", "y");
      if (VERBOSE) cout << filter->toString() << endl;
      ros1 = ros0->filterWith(ros0->numFields()+1, filter);
      if (VERBOSE) cout << filter->toString() << endl;
      if (REMOVE) ros1 = ros1->deleteField("b");
      ros1 = ros1->moveField("y", 2);
    } else {
      ros1 = ros0;
    }
    
    RecordOutputStream *rosa = 
      new RecordOutputStream_Ascii(ros1, OUTPUT, true);

    if (VERBOSE) {
      cout.setf(ios::left);

      cout << setw(10) << setfill('-') << '-' 
	   << setw(20) << "ros0" << setfill(' ') << endl << endl
	   << ros0->toString() << endl;

      cout << setw(10) << setfill('-') << '-'
	   << setw(20) << "ros1" << setfill(' ') << endl << endl
	   << ros1->toString() << endl;

      cout << setw(10) << setfill('-') << '-'
	   << setw(20) << "rosa" << setfill(' ') << endl << endl
	   << rosa->toString() << endl;
      
    }

    for (int i=0; i<number; i++) {
      ans = star.realize();

      try {
	ros0->setRealValue(1, ans[0]);
	ros0->setRealValue(2, ans[1]);
	ros0->setRealValue(3, ans[2]);
	ros0->setRealValue(4, ans[3]);

      } catch (BIE::BIEException e) {
	cout << e.getErrorMessage() << endl; 

      } catch (...) {
	cout << "Non-BIE exception\n";

      }

      ros0->pushRecord();
    }

				// Save parameters
    ofstream out(OUTPUT.c_str(), ios::out | ios::app);
    if (out) {

      out.setf(ios::left);

      out << "#" << endl
	  << "# Parameters" << endl
	  << "# ----------" << endl
	  << setw(20) << "# ISEED=" << ISEED << endl
	  << setw(20) << "# R0="    << R0    << endl
	  << setw(20) << "# A="     << A     << endl
	  << setw(20) << "# A1="    << A1    << endl
	  << setw(20) << "# Z1="    << Z1    << endl
	  << setw(20) << "# H="     << H     << endl
	  << setw(20) << "# AK="    << AK    << endl
	  << setw(20) << "# MMIN "  << MMIN  << endl
	  << setw(20) << "# MMAX="  << MMAX  << endl
	  << setw(20) << "# MWID="  << MWID  << endl;
      
      ifstream basis(BASISDATA.c_str());
      const int linesize = 512;
      char line[linesize];

      basis.getline(line, linesize);

      if (basis) {
	out << "#" << endl
	    << "# Basis" << endl
	    << "# -----" << endl;
	while (!basis.eof()) {
	  out << "# " << line << endl;
	  basis.getline(line, linesize);
	}
      }

    }
      
    delete rt;
    delete ros0;
    if (MU) delete ros1;
    delete rosa;
  }

  return 0;
}
