/*******************************************************************************
* This is a collection of tests that checks the persistence library
* can successfully save and restore a variety of STL container classes.
*******************************************************************************/

#include <iostream>
#include <config.h>
#include <fstream>

#include <bieTags.h>
#include <Serializable.h>
#include <gvariable.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#include <clivector.h>
#include <UniformDist.h>
#include <NormalDist.h>
#include <Tile.h>
#include <SquareTile.h>

// For clivector
#include <Tessellation.h>
#include <Distribution.h>

using namespace std;

/*******************************************************************************
* Superclass for all test classes in this regression test.
* [This test class simply maintains an integer value]
*******************************************************************************/
class A : public Serializable {
public:
  A() {}
  A(int i) { this->i = i; }
  int getValue() { return this->i; }
private:
  int i;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
    ar & BOOST_SERIALIZATION_NVP(i);
  }
};

BOOST_CLASS_EXPORT(A)


/*******************************************************************************
* Test class for several kinds of vector objects.
*******************************************************************************/
class VectorTest : public Serializable {
public:
  VectorTest() {}
  ~VectorTest() {}
  void fillWithData();
  void print(ostream & out);
  
private:
  vector<string> v2;
  vector<double> v3;
  clivectord     d3;
  clivectordist  d4;
  A* aa;
  Tile* tile;
  UniformDist*  unif;
  NormalDist* norm;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
    ar & BOOST_SERIALIZATION_NVP(v2);
    ar & BOOST_SERIALIZATION_NVP(v3);
    ar & BOOST_SERIALIZATION_NVP(d3);
    ar & BOOST_SERIALIZATION_NVP(d4);
    ar & BOOST_SERIALIZATION_NVP(aa);
    ar & BOOST_SERIALIZATION_NVP(tile);
  }
};
BOOST_CLASS_EXPORT(VectorTest)

void VectorTest::fillWithData()
{
  string str = "cat_";

  unif = new UniformDist(-3.2, 4.8);
  norm = new NormalDist(0.1, 0.2);

  for (uint32 i = 0; i < 19; i++)
  {
    v2.push_back(str);
    v3.push_back(2.68*i);
    d3().push_back(3.14*i);
    if ( (i/2)*2 == i)
      d4().push_back(unif);
    else
      d4().push_back(norm);
    str = str + "a"; 
  }

  aa = new A(19);
  tile = new SquareTile(1.0,2.0,3.0,4.0);
}

void VectorTest::print(ostream & out)
{
  out << "v2: ";
  for (unsigned int i = 0; i < v2.size(); i++)
  { out << v2[i] << ", ";  }
  out << endl;

  out << "v3: ";
  for (unsigned int i = 0; i < v3.size(); i++)
  { out << v3[i] << ", ";  }
  out << endl;

  out << "d3: ";
  for (unsigned int i = 0; i < d3().size(); i++)
  { out << d3()[i] << ", ";  }
  out << endl;

  out << "d4: ";
  for (unsigned int i = 0; i < d4().size(); i++)
  { 
    vector<double> l = d4()[i]->Mean();
    vector<double> u = d4()[i]->StdDev();
    out << setw(3) << i << ": " 
	<< setw(6) << l[0] << ", "
	<< setw(6) << u[0] << endl;
  }
  out << endl;

  out << "aa: " << aa->getValue() << endl;

  double x0, x1, y0, y1;
  dynamic_cast<SquareTile*>(tile)->corners(x0, x1, y0, y1);
  out << "tile: [" << x0 << ", " << x1 << "]"
       << "[" << y0 << ", " << y1 << "]" << endl;

  out << endl;
}


int main(/* int numc, char **numv */)
{

  system("rm -rf " PERSISTENCE_DIR);

  try {
    VectorTest tc;
    
    tc.fillWithData();
   
    string statename("testPersistence_vector");

    TestUserState teststate(statename, 0, true);
    teststate.addObject(&tc);

    try {
      TestSaveManager testSaveManager(&teststate, BOOST_XML, BACKEND_FILE);
      testSaveManager.save();
    }
    catch (BIEException & e) {
      cout << "While SAVING, caught exception:" << endl
	   << e.getErrorMessage() << endl; 
    }
  
    TestUserState *restored;
    try {
      TestLoadManager testLoadManager(&restored, 
				      teststate.getSession(), 
				      teststate.getVersion(), 
				      BOOST_XML, BACKEND_FILE);
      testLoadManager.load();
    }
    catch (BIEException & e) {
      cout << "While LOADING, caught exception:" << endl
	   << e.getErrorMessage() << endl; 
    }

    Serializable *loadedObj = *(restored->getTransClosure().objectlist.begin());
    VectorTest* restored_tc = dynamic_cast<VectorTest*>(loadedObj);

    fstream actualoutput;
    string filename = statename + "_actual";
    actualoutput.open(filename.c_str(), fstream::out);
    tc.print(actualoutput);
    actualoutput.close();

    fstream restoredoutput;
    filename = statename + "_restored";
    restoredoutput.open(filename.c_str(), fstream::out);
    restored_tc->print(restoredoutput);
    restoredoutput.close();
  }
  catch (BIEException & e)
  { cout << "Caught Exception: \n" << e.getErrorMessage() << endl; }

}

