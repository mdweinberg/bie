
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
using namespace std;

#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <ACG.h>

#define IS_MAIN

#include <TemperedSimulation.h>

#include <CountConverge.h>
#include <SubsampleConverge.h>
#include <MappedGrid.h>
#include <KdTessellation.h>
#include <QuadGrid.h>
#include <SquareTile.h>
#include <RecordStream_Ascii.h>
#include <DataTree.h>
#include <LikelihoodComputationSerial.h>

#include <Model.h>
#include <InitialMixturePrior.h>
#include <PostMixturePrior.h>
#include <Histogram1D.h>
#include <EnsembleStat.h>
#include <SplatModel.h>
#include <LegeIntegration.h>
#include <MetropolisHastings.h>

#include <RunOneSimulation.h>

enum TessType {kd_inf, kd, mapped, quad};
enum ConvType {counter, subsample};

using namespace BIE;

/**
   @name multilevel: main
   Does full simulation at multiple levels of resolution.  Parallel
   MPI implementation.
*/
int main(void)
{
  // Hold instantiation of classes to define the simulation
  //
  SplatModel *model;
  SquareTile *tile;
  RecordInputStream_Ascii *ris;
  Histogram1D *histo;
  LegeIntegration *intgr;
  Tessellation *tess;
  DataTree *dist;
  LikelihoodComputationSerial *like;
  MetropolisHastings *mca;
  TemperedSimulation *sim;
  RunOneSimulation *run;
  StateInfo *si;

  // Initialize base random number generator
  //
  cout << "create new ACG.\n";
  BIEgen = new BIEACG(11, 20);


  // Gaussian splat model
  //
  int ndim = 2;
  int nmix = 2;
  cout << "create new SplatModel.\n";
  model = new SplatModel(ndim, nmix);

  // State metadata
  //
  si = new StateInfo(nmix, ndim);

  // Factor for 1D histogram in each tile (put everything in one bin)
  //
  cout << "create new Histogram1D.\n";
  histo = new Histogram1D(0.0, 2.0, 10.0, "mag1");

  // Define prior
  //
  cout << "create new InitialMixturePrior.\n";
  UniformDist unif(-1.0, 1.0);
  clivectordist vdist(ndim, &unif);
  Prior* prior = new InitialMixturePrior(si, 1.0, &vdist);


  // Simple sample statistic
  //
  cout << "create new EnsembleStat.\n";
  EnsembleStat* sstat = new EnsembleStat [4];

  // Rectangular grid tessellation
  //

  cout << "create new SquareTile.\n";
  tile = new SquareTile();

  // integration method
  //
  cout << "create new LegeIntegration.\n";
  intgr = new LegeIntegration(2, 2);

  // Convergence method
  //
  Converge *convrg = new CountConverge(100, sstat);

  // Tessellation

  cout << "create new RecordInputStream_Ascii." << endl;
  ris = new RecordInputStream_Ascii("data.dat");

  cout << "create new KdTessellation.\n";
  tess = new KdTessellation(tile,ris,100,1.0,-1.0,2.0,-1.0,2.0);
  //  tess = new QuadGrid(tile, -1.0,2.0,-1.0,2.0,4);

  // distribution
  cout << "create new RecordInputStream_Ascii." << endl;
  ris = new RecordInputStream_Ascii("data.dat");

  cout << "create new DataTree.\n";
  dist = new DataTree(ris,histo,tess);
  
  cout << "create a LikelihoodComputation.\n";
  like = new LikelihoodComputationSerial();

  cout << "create an MC algorithm.\n";
  mca = new MetropolisHastings();

  cout << "create new TemperedSimulation.\n";
  sim = new TemperedSimulation(si, 6, 128.0, 0,
			       dist, model, intgr, convrg, prior, like, mca);

  // Simple RunOneSimulation
  cout << "create new RunOneSimulation.\n";
  run = new RunOneSimulation(100, 0.2, sstat, prior, sim);

  return 0;
}
