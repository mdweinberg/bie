// This may look like C code, but it is really -*- C++ -*-


#ifndef DummyModel_h
#define DummyModel_h

#include <Model.h>

namespace BIE {
  
  /**
     Simple dummy model definition for testing
  */
  class DummyModel : public Model 
    {
    public:
      /// Constructor
      DummyModel() {};
      
      /// "Do nothing" initialization
      void Initialize(State& s) {};
      
      /// "Do nothing" evaluation
      double NormEval(double x, double y, SampleDistribution *d) {return 0.0;}
      
      /// "Do nothing" evaluation for binned distribution
      vector<double> EvaluateBinned(double x, double y,
				    BinnedDistribution* d) 
      {return vector<double>(1, 0.0);}

      /// "Do nothing" evaluation for point distribution
      vector<double> EvaluatePoint(double x, double y,
				   PointDistribution* d) 
      {return vector<double>(1, 0.0);}

      /// "Do nothing" parameter strings
      string ParameterDescription(int i) {
	return string("");
      }

      vector<string> ParameterLabels() {
	return vector<string>(1);
      }

      /// Return number of data attributes returned for this model
      int DataDimension() { return 0; }
    };
  
}

#endif
