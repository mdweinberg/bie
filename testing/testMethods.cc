#include <iostream>
#include <gvariable.h>
#include <config.h>

#include <MethodTable.h>

using namespace BIE;
using namespace std;

int main()
{
  MethodTable::initialize(methodstring);  
  
  MethodTable::print_graph();

  vector<string> mods = MethodTable::get_derived("Model");
  
  cout << "Found " << mods.size() << " models:";
  for (unsigned n=0; n<mods.size(); n++)
    cout << setw(4) << left << n << ": " << mods[n] << endl;

  return 0;
}
