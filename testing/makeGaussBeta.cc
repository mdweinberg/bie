// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <string>

using namespace std;

#include <unistd.h>

#include <gaussQ.h>

unsigned nx = 100;
unsigned ny = 100;
double Rmax = 2.0;
double cenx_g = 0.0;
double ceny_g = 0.0;
double varx_g = 0.03;
double vary_g = 0.03;
double cenx_b = 0.0;
double ceny_b = 0.0;
double varx_b = 0.15;
double vary_b = 0.15;
double alpha  = 4.0;
int    nint   = 6;

string outname("gaussbeta.data");
LegeQuad *lq = 0;

double Norm;

void normBeta()
{
  static bool first = true;
  if (!first) return;
  
  Norm = 2.0*M_PI * sqrt(varx_b*vary_b) *
    exp(lgamma(2.0)+lgamma(alpha+2.0)-lgamma(alpha+4.0));
  first = false;
}

double densBeta(double x, double y)
{
  normBeta();

  double r2 = 
    (x - cenx_b)* (x - cenx_b)/varx_b
    +
    (y - cenx_b)* (y - ceny_b)/vary_b;

  return 1.0/(Norm*pow(1.0 + sqrt(r2), alpha + 4.0));
}


double pixelBeta(double xmin, double xmax, double ymin, double ymax)
{
  double ans = 0.0, x, y;
  for (int i=1; i<=nint; i++) {
    x = xmin + (xmax - xmin)*lq->knot(i);
    for (int j=1; j<=nint; j++) {
      y = ymin + (ymax - ymin)*lq->knot(j);

      ans += lq->weight(i) * lq->weight(j) * densBeta(x, y);
    }
  }

  return ans*(xmax - xmin)*(ymax - ymin);
}

double pixelGauss(double xmin, double xmax, double ymin, double ymax)
{
  double ans = 
    0.5*(erf((xmax - cenx_g)/sqrt(2.0*varx_g)) - 
	 erf((xmin - cenx_g)/sqrt(2.0*varx_g)) )
    *
    0.5*(erf((ymax - ceny_g)/sqrt(2.0*vary_g)) - 
	 erf((ymin - ceny_g)/sqrt(2.0*vary_g)) );

  return ans;
}

int 
main(int argc, char** argv)
{
  int c;
  while (1) {
    c = getopt (argc, argv, "1:2:3:4:5:6:a:R:x:X:y:Y:n:f:");
    if (c == -1) break;
     
    switch (c)
      {
      case '1': nx      = atoi(optarg); break;
      case '2': ny      = atoi(optarg); break;
      case '3': varx_g  = atof(optarg); break;
      case '4': vary_g  = atof(optarg); break;
      case '5': varx_b  = atof(optarg); break;
      case '6': vary_b  = atof(optarg); break;
      case 'x': cenx_g  = atof(optarg); break;
      case 'y': ceny_g  = atof(optarg); break;
      case 'X': cenx_b  = atof(optarg); break;
      case 'Y': ceny_b  = atof(optarg); break;
      case 'R': Rmax    = atof(optarg); break;
      case 'a': alpha   = atof(optarg); break;
      case 'n': nint    = atoi(optarg); break;
      case 'f': outname = optarg; break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-1 nx\t\tnumber of x grid points\n";
	msg += "\t-2 ny\t\tnumber of y grid points\n";
	msg += "\t-3 sx\t\tx Gaussian variance\n";
	msg += "\t-4 sy\t\ty Gaussian variance\n";
	msg += "\t-5 sx\t\tx BetaR scale\n";
	msg += "\t-6 sy\t\ty BetaR scale\n";
	msg += "\t-x cx\t\tx Gaussian center\n";
	msg += "\t-y cy\t\ty Gaussian center\n";
	msg += "\t-X cx\t\tx BetaR center\n";
	msg += "\t-Y cy\t\ty BetaR center\n";
	msg += "\t-R r\t\thalf length of side on output\n";
	msg += "\t-a a\t\tBetaR shape parameter\n";
	msg += "\t-n n\t\tintegration knots\n";
	msg += "\t-f filename\t\toutput filename (default: " +outname+ ")\n";
	cerr << msg; exit(-1);
      }
  }

  lq = new LegeQuad(nint);

  ofstream out(outname.c_str());
  if (!out) {
    cerr << "Error opening <" + outname + ">\n";
    exit(-1);
  }

  double dX = 2.0*Rmax/(nx - 1), x;
  double dY = 2.0*Rmax/(ny - 1), y;

  for (unsigned i=0; i<nx; i++) {
    x = -Rmax + dX*i;

    for (unsigned j=0; j<ny; j++) {
      y = -Rmax + dY*j;

      out << setw(18) << x + 0.5*dX
	  << setw(18) << y + 0.5*dY
	  << setw(18) << pixelGauss(x, x+dX, y, y+dY)
	  << setw(18) << pixelBeta (x, x+dX, y, y+dY)
	  << endl;
    }
    out << endl;
  }

}
