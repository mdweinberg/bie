#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cmath>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <BIEconfig.h>
#include <gvariable.h>
#include <StateFile.h>

#include <ACG.h>
#include <DiscUnif.h>
#include <Normal.h>
#include <Poisson.h>
#include <VectorM.h>

using namespace BIE;
using namespace std;

void replicateData(vector<double>& weights, 
		   vector<double>& centers,
		   vector<double>& variance);

void la_svd(MatrixM& a, MatrixM& u, MatrixM& v, MatrixM& d);

static const int    number0 = 2;
static const double weights0  [] = {0.5,  0.5};
static const double centers0  [] = {0.2,  0.9};
static const double variance0 [] = {0.03, 0.03};

/** 
    Compute Posterior Predictive Checks from chain produced by BIE
    for binned data
*/

int nbins, Numb;
double xmin, xmax, dx;
bool use_cauchy = false;
vector<double> fdata;
NormalPtr  nrm;
PoissonPtr poi;

int main(int ac, char** av)
{
  int myid = 0;
  int skip;
  int level;
  int strd;
  int keep;
  int nsamp;
  bool cache   = false;
  bool ignore  = false;
  bool quiet   = false;
  bool verbose = false;
  bool cauchy_model = false;
  bool cauchy_data  = false;
  double eps;
  vector<string> files;
  string modelfile;
  string datafile;
  string cachefile;
  string limitfile;

  try {
    //
    // Declare the supported options.
    //
    po::options_description desc("Allowed options");
    desc.add_options()
      ("help,h",	"produce help message")
      ("cache,c",	"use cache file")
      ("quiet,q",	"quiet output mode")
      ("verbose,v",	"verbose output mode")
      ("cauchy-model",	"use Cauchy-distributed data for model")
      ("cauchy-data",	"use Cauchy-distributed data for data")
      ("cache-file,C", po::value<string>(&cachefile), "cache file name")
      ("level,l",      po::value<int>(&level)->default_value(0), "level")
      ("skip,s",       po::value<int>(&skip)->default_value(0), "number of states to skip")
      ("keep,k",       po::value<int>(&keep)->default_value(100000), "number of states to keep")
      ("stride,S",     po::value<int>(&strd)->default_value(1), "data stride")
      ("number",       po::value<int>(&Numb)->default_value(40000), "number of points in bins (for internal variance)")
      ("nbins,n",      po::value<int>(&nbins)->default_value(400), "number of bins")
      ("nsample,N",    po::value<int>(&nsamp)->default_value(2000), "number of posterior samples")
      ("xmin,x"  ,     po::value<double>(&xmin)->default_value(-1.0), "minimum x range")
      ("xmax,X"  ,     po::value<double>(&xmax)->default_value(2.0), "maximum x range")
      ("epsilon,e",    po::value<double>(&eps)->default_value(0.05), "tail p-value for truncation dispersion")
      ("input-file,f", po::value< vector<string> >(&files), "input file")
      ("model-file,F", po::value<string>(&modelfile), "model file")
      ("data-file,D", po::value<string>(&datafile), "data file")
      ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);    
  
    if (vm.count("help")) {
      cout << desc << endl;
      return 1;
    }
    
    if (vm.count("cache")) {
      cache = true;
    }

    if (vm.count("quiet")) {
      quiet = true;
    }

    if (vm.count("verbose")) {
      verbose = true;
    }

    if (vm.count("cauchy-data")) {
      cauchy_data = true;
    }

    if (vm.count("cauchy-model")) {
      cauchy_model = true;
    }

    if (vm.count("input-file")) {
      cout << "Input files are:";
      for (unsigned n=0; n<files.size(); n++) cout << " " << files[n];
      cout << "." << endl;
    } else {
      cout << "Input files were not set." << endl;
    }
  }
  catch (exception& e) {
    cerr << "error: " << e.what() << endl;
    return 1;
  }
  catch(...) {
    cerr << "Exception of unknown type!" << endl;
  }
  
  //--------------------------------------------------
  // Get StateFile
  //--------------------------------------------------

  StateFile sf(files[0], cachefile, limitfile, level, skip, strd, keep, ignore, quiet);

  //--------------------------------------------------
  // Compute enclosing data range
  //--------------------------------------------------

  int     nmix = sf.nmix;
  int     ndim = sf.ndim;
  unsigned num = sf.prob.size();
  unsigned dim = sf.prob.front().point.size();

  if (nmix>1) {
    ndim = (ndim - (nmix-1))/nmix;
  }

  ACG gen(11+myid, 20);
  DiscreteUniform samp(0, sf.prob.size()-1, &gen);
  poi = PoissonPtr(new Poisson(0.0, &gen));
  nrm = NormalPtr (new Normal (0.0, 1.0, &gen));

  if (verbose) {
    cout << " # states: " << num << endl;
    cout << "Dimension: " << dim << endl;
  }

  dx    = (xmax - xmin)/nbins;
  fdata = vector<double>(nbins, 0.0);

  int nmx0=0, nmxD=0;
  double xmin0, xmax0;
  vector<double> wght0, cen0, var0, fdata0;
  vector<double> wght(nmix), cen(nmix), var(nmix);

				// Use the provided data file
  if (datafile.size()) {
    ifstream in(datafile.c_str());
    if (in) {
      in >> nmxD;
      in >> xmin0;
      in >> xmax0;
      fdata0 = vector<double>(nmxD);
      for (int n=0; n<nmxD; n++) in >> fdata0[n];
    } else {
      cerr << "Could not open data file <" << datafile << ">" << endl;
      exit(-1);
    }
  }
				// Use the provided model file
  if (modelfile.size()) {
    ifstream in(modelfile.c_str());
    if (in) {
      in >> nmx0;
      wght0 = vector<double>(nmx0);
      cen0  = vector<double>(nmx0);
      var0  = vector<double>(nmx0);
      for (int n=0; n<nmx0; n++) {
	in >> wght0[n];
	in >> cen0 [n];
	in >> var0 [n];
      }
    } else {
      cerr << "Could not open model file <" << modelfile << ">" << endl;
      exit(-1);
    }
  } else {			// Otherwise, use the default model
    nmx0  = number0;
    wght0 = vector<double>(nmx0);
    cen0  = vector<double>(nmx0);
    var0  = vector<double>(nmx0);
    for (int n=0; n<nmx0; n++) {
      wght0[n] = weights0[n];
      cen0 [n] = centers0[n];
      var0 [n] = variance0[n];
    }
  }

  for (int n=0; n<nmx0; n++) {
    wght[n] = wght0[n];
    cen [n] = cen0 [n];
    var [n] = var0 [n];
  }

  use_cauchy = cauchy_model;

  MatrixM Y (0, nbins-1, 0, nsamp-1);
  VectorM Ym(0, nbins-1), Y0(0, nbins-1);
  Ym.zero();

  for (int n=0; n<nsamp; n++) {
    size_t icnt = 0;
    double sum  = 0.0;
    int indx = samp();
    for (size_t j=0; j<static_cast<size_t>(nmix); j++) {
      if (j!=sf.ignr) {
	wght[j] = sf.prob[indx].point[icnt++];
	sum += wght[j];
      }
    }
    wght[sf.ignr] = 1.0 - sum;

    for (int j=0; j<nmix; j++) {
      cen[j] = sf.prob[indx].point[nmix-1 + ndim*j];
      if (ndim>1) var[j] = sf.prob[indx].point[nmix-1 + ndim*j + 1];
    }
    replicateData(wght, cen, var);
    for (int j=0; j<nbins; j++) {
      Y [j][n] = fdata[j];
      Ym[j]   += fdata[j]/nsamp;
    }
  }

  for (int j=0; j<nbins; j++) {
    for (int n=0; n<nsamp; n++) {
      if (Ym[j]>0.0) Y[j][n] = ( Y[j][n] - Ym[j] )/Ym[j];
    }
  }

  use_cauchy = cauchy_data;

  for (int j=0; j<nmix; j++) {
    wght[j] = weights0 [j];
    cen[j]  = centers0 [j];
    var[j]  = variance0[j];
  }

  if (nmxD) {
    fdata = fdata0;
  } else {
    nmix = nmx0;
    replicateData(wght0, cen0, var0);
  }
  for (int i=0; i<nbins; i++) {
    if (Ym[i]>0.0) Y0[i] = (fdata[i] - Ym[i])/Ym[i];
    else if (fdata[i]>0.0) Y0[i] = 1.0e20;
  }
  
  //--------------------------------------------------
  // Compute Chi^2-like statistic
  //--------------------------------------------------

  double chi20 = 0.0;
  for (int i=0; i<nbins; i++) chi20 += Y0[i]*Y0[i];

  vector<double> distr_chi2;
  for (int j=0; j<nsamp; j++) {
    double X2 = 0.0;
    for (int i=0; i<nbins; i++) X2 += Y[i][j]*Y[i][j];
    distr_chi2.push_back(X2);
  }
  sort(distr_chi2.begin(), distr_chi2.end());

  cout << setw(40) << setfill('-') << '-' << endl;
  cout << "-- " << "Chi^2" << endl;
  cout << setw(40) << setfill('-') << '-' << endl;
  cout << setfill(' ');

  if (verbose) {
    for (unsigned i=0; i<distr_chi2.size(); i++)
      cout << setw(4) << i << setw(18) << distr_chi2[i] << endl;
    cout << endl << "chi20 = " << chi20 << endl;
  }

  vector<double>::iterator it = lower_bound(distr_chi2.begin(), 
					    distr_chi2.end  (), chi20);
  if (chi20 <= distr_chi2.front())
    cout << "p-value: 0.0" << endl;
  else if (chi20 >= distr_chi2.back())
    cout << "p-value: 1.0" << endl;
  else {
    // Rank in ordered array
    double ymin = &(*it) - &distr_chi2[0];
    double ymax = ymin + 1;
    // Bracketed values
    double xmin = *it, xmax = *(it+1);
    // Normalization
    double fact = 1.0/(xmax - xmin)/distr_chi2.size();
    
    cout << "p-value: " << setprecision(5)
	 << (ymin*(xmax - chi20) + ymax*(chi20 - xmin) )*fact
	 << endl;
  }
  
  //--------------------------------------------------
  // Compute posterior distribution of X^2
  //--------------------------------------------------

				// Compute SVD
  MatrixM U, V, S;
  try {
    la_svd(Y, U, V, S);
  }
  catch (string& error) {
    cerr << "Error: " << error << endl;
    return -1;
  }

  vector<double> ss;
  double sum = 0.0;
  int imax;

  for (int i=0; i<min<int>(nbins, nsamp); i++) {
    sum += S[i][i];
    ss.push_back(sum);
  }
  for (unsigned i=0; i<ss.size(); i++) ss[i] /= sum;
  for (imax=0;  imax<static_cast<int>(ss.size()); imax++) 
    if (ss[imax]>1.0 - eps) break;
  imax = min<int>(imax, ss.size()-1);

  cout << setw(40) << setfill('-') << '-' << endl;
  cout << "-- " << "X^2" << endl;
  cout << setw(40) << setfill('-') << '-' << endl;
  cout << setfill(' ');

  //--------------------------------------------------
  // Compute posterior distribution of X^2
  //--------------------------------------------------

  MatrixM Urt = U.Transpose();
  for (int i=imax+1; i<Urt.getrhigh(); i++)
    for (int j=imax+1; j<Urt.getrlow(); j++) Urt[i][j] = 0.0;

  MatrixM X  = Urt * Y;
  VectorM X0 = Urt * Y0;

  double X20 = 0.0;
  for (int i=0; i<=imax; i++) X20 += X0[i]*X0[i];

  vector<double> distr;
  for (int j=0; j<nsamp; j++) {
    double X2 = 0.0;
    for (int i=0; i<=imax; i++) X2 += X[i][j]*X[i][j];
    distr.push_back(X2);
  }
  sort(distr.begin(), distr.end());

  if (verbose) {
    for (unsigned i=0; i<distr.size(); i++)
      cout << setw(4) << i << setw(18) << distr[i] << endl;
    cout << endl << "X20 = " << X20 << endl;
  }

  it = lower_bound(distr.begin(), distr.end(), X20);
  if (X20 <= distr.front())
    cout << "p-value: 0.0" << endl
	 << "#-comps: " << imax << endl;
  else if (X20 >= distr.back())
    cout << "p-value: 1.0" << endl
	 << "#-comps: " << imax << endl;
  else {
    // Rank in ordered array
    double ymin = &(*it) - &distr[0];
    double ymax = ymin + 1;
    // Bracketed values
    double xmin = *it, xmax = *(it+1);
    // Normalization
    double fact = 1.0/(xmax - xmin)/distr.size();

    cout << "p-value: " << setprecision(5)
	 << (ymin*(xmax - X20) + ymax*(X20 - xmin) )*fact << endl
	 << "#-comps: " << imax << endl;
  }
  
  cout << setw(40) << setfill('-') << '-' << endl;
  cout << setfill(' ');

  //--------------------------------------------------
  // Done
  //--------------------------------------------------

  return 0;
}


void replicateData(vector<double>& weights, 
		   vector<double>& centers,
		   vector<double>& variance)
{
  double x1, x2;
  for (int i=0; i<nbins; i++) fdata[i] = 0.0;
  
  for (unsigned c=0; c<weights.size(); c++) {
    for (int i=0; i<nbins; i++) {
      x1 = xmin + dx*i;
      x2 = xmin + dx*(i+1);
      if (use_cauchy)
	fdata[i] += weights[c]*(  
				atan((x2 - centers[c])/sqrt(variance[c]) ) -
				atan((x1 - centers[c])/sqrt(variance[c]) )
				  ) * Numb / M_PI;
      else
	fdata[i] += weights[c]*(  
				erf( (x2 - centers[c])/sqrt(2.0*variance[c]) ) -
				erf( (x1 - centers[c])/sqrt(2.0*variance[c]) )
				  ) * 0.5 * Numb;
    }
  }

  for (int i=0; i<nbins; i++) {
    double z = round(fdata[i]);
    // Flip from Poisson to Normal when the mean is larger than 40
    if (fdata[i] < 40.0) fdata[i] = (*poi)(z);
    else                 fdata[i] = round((*nrm)(z, z));
  }
}

extern "C" int dgesvd_(char *jobu, char *jobvt, int *m, int *n, 
	double *a, int *lda, double *s, double *u, 
	int *ldu, double *vt, int *ldvt, double *work, 
	int *lwork, int *info);

void la_svd(MatrixM& a, MatrixM& u, MatrixM& vt, MatrixM& d)
{

// Make copies of a and b so as not to disturb their contents

  int m0 = a.getrlow();
  int n0 = a.getclow();
  int m  = a.getnrows();
  int n  = a.getncols();
				// Transform to LAPACK
  double *A    = new double [m*n];
  double *S    = new double [max<int>(n, m)];
  double *U    = new double [m*m];
  double *VT   = new double [n*n];

  int lwork    = max<int>(3*min<int>(m, n) + max<int>(m, n), 5*min<int>(m, n));
  double *WORK = new double [lwork];

  int INFO;
				// Repack in Lapack form (column major)
  for (int j=0; j<n; j++)
    for (int i=0; i<m; i++) A[i+j*m] = a[i+m0][j+n0];
  
				// Perform singular value decomposition
  char Ac[] = "A";
  dgesvd_(Ac, Ac, &m, &n, A, &m, S, U, &m, VT, &n, WORK, &lwork, &INFO);
  //      1   2   3   4   5  6   7  8  9   10  11  12    13      14

				// Transform back
  u .setsize(m0, m0+m-1, m0, m0+m-1);
  vt.setsize(n0, n0+n-1, n0, n0+n-1);
  d .setsize(m0, m0+m-1, n0, n0+n-1);
  d .zero();

  for (int j=0; j<m; j++)
    for (int i=0; i<m; i++) u[i+m0][j+m0] = U[i+j*n];

  for (int j=0; j<n; j++)
    for (int i=0; i<n; i++) vt[i+n0][j+n0] = VT[i+j*n];
  
  for (int i=0; i<min<int>(n, m); i++) d[i+m0][i+n0] = S[i];
  
  delete [] A;
  delete [] S;
  delete [] U;
  delete [] VT;
  delete [] WORK;

  if (INFO) {
    ostringstream mout;
    if (INFO<0) {
      mout << "Argument " << -INFO << " to DGESVD has an illegal value";
    } else {
      mout << INFO << " superdiagonal elements did not converge to zero";
    }
    throw mout.str();
  }
}
