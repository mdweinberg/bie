
#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

#include <stdlib.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <MHWidthOne.h>
#include <TemperedSimulation.h>
#include <LikelihoodComputationSerial.h>
#include <CountConverge.h>
#include <MappedGrid.h>
#include <QuadGrid.h>
#include <Histogram1D.h>
#include <Model.h>
#include <InitialMixturePrior.h>
#include <SimpleStat.h>
#include <LegeIntegration.h>
#include <SquareTile.h>
#include <MuSquareTile.h>
#include <RecordStream_Ascii.h>
#include <DataTree.h>
#include <MetropolisHastings.h>
#include <BIEdebug.h>
#include <EnsembleStat.h>

#include <DummyModel.h>

using namespace BIE;

/**
   Test routine for data accumulation
*/
int
main(int argc, char** argv)
{
  // Debug
  //
#ifdef DEBUG
  // system("sleep 20");
#endif

  BIEgen = new BIEACG(11, 20);

  // Initialize mpi
  //
  mpi_init(argc, argv);


  DummyModel *model;
  Tessellation *grid;
  Histogram1D *histo;
  LegeIntegration *intgr;
  SquareTile *tile;
  RecordInputStream *ris;
  DataTree *dist;

  int minmc = 6;                // minimum number of temperature states

				// Instantiate dummy model
  model = new DummyModel();

				// Integration (not used here)
  intgr = new LegeIntegration(2, 2);

				// Instantiate 1D histogram in each tile
  histo = new Histogram1D(0.0, 20.0, 0.5, "mag1");
  
				// Instantiate simple coordinate-line tiling
  // tile = new MuSquareTile();
  tile = new SquareTile();

  // grid = new MappedGrid(tile, -M_PI, M_PI, -1.0, 1.0, 10, 10);
  grid = new QuadGrid(tile, -M_PI, M_PI, -1.0, 1.0, 4);

  ris = new RecordInputStream_Ascii("data.dat");

  dist = new DataTree(ris, histo, grid);

  printFrontierTiles(dist->GetDefaultFrontier());
  cout << endl;

				// Dump out means and standard devs 
				// for each tile
  dist->Reset();
  while(!(dist->IsDone())) {
    cout 
      << setw(15) << dist->CurrentTile()->X(0.5, 0.5)
      << setw(15) << dist->CurrentTile()->Y(0.5, 0.5)
      << setw(15) << (dist->CurrentItem()->Mean())[0]
      << setw(15) << (dist->CurrentItem()->StdDev())[0]
      << endl;

    dist->Next();
  }

  // Define prior
  const int nmix = 2;
  const int ndim = 4;
  const double alpha = 1.0;
  StateInfo *si = new StateInfo(nmix, ndim);

  UniformDist unif(-1.0, 1.0);
  WeibullDist weib(0.1, 1.0, 1.0e-4, 10.0);
  clivectordist vdist(4, &unif);
  vdist()[2] = vdist()[3] = &weib;

  InitialMixturePrior* prior = new InitialMixturePrior(si, alpha, &vdist);

  // Simple sample statistic
  EnsembleStat* sstat = new EnsembleStat(si);

  // Define converge
  CountConverge *converge = new CountConverge(10, sstat);

  double maxT = 1000.0;
  double wdisp = 0.1;
  vector<double> mhw;
  for (int i=0; i<ndim+1; i++) mhw.push_back(wdisp);

  MHWidthOne mhwidth(si, &mhw);

  LikelihoodComputationSerial likelihoodComputationSerial;

  MetropolisHastings mca;


  TemperedSimulation testsim(si, minmc, maxT, &mhwidth,
			     dist, model, intgr, converge, prior,
			     &likelihoodComputationSerial, &mca);

}

