#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

using namespace std;

#include <BIEmpi.h>
#include <gvariable.h>
#include <BIEconfig.h>
#include <KDTree.h>
#include <TreePrior.h>

using namespace BIE;

/**
   @name testTreePrior: main
   Compute density distribution for a BIE-generated MCMC state log
*/
int
main(int argc, char** argv)
{
  int pos1     =  0;
  int pos2     =  1;
  int ndim     =  2;
  int ncell    =  32;
  int lev      =  0;
  double xmin  = -3.0;
  double xmax  =  3.0;
  double ymin  = -3.0;
  double ymax  =  3.0;
  double temp  =  1.0;
  int    numxy =  100;
  int    nsamp =  1000000;
  string mcmc  =  "run.statelog";
  string densf =  "density.dat";

				// No MPI needed here
  BIE::mpi_used = 0;

  while (1) {
    int c = getopt (argc, argv, "1:2:x:X:y:Y:T:d:n:c:l:f:o:h");
    if (c == -1) break;
     
    switch (c)
      {
      case '1': pos1  = atoi(optarg); break;
      case '2': pos2  = atoi(optarg); break;
      case 'x': xmin  = atof(optarg); break;
      case 'X': xmax  = atof(optarg); break;
      case 'y': ymin  = atof(optarg); break;
      case 'Y': ymax  = atof(optarg); break;
      case 'T': temp  = atof(optarg); break;
      case 'd': ndim  = atoi(optarg); break;
      case 'c': ncell = atoi(optarg); break;
      case 'l': lev   = atoi(optarg); break;
      case 'n': nsamp = atoi(optarg); break;
      case 'N': numxy = atoi(optarg); break;
      case 'f': mcmc  = optarg;       break;
      case 'o': densf = optarg;       break;
      case 'h':
      case '?': 
	string msg = "usage: "  + string(argv[0]) + " [options]\n";
	msg += "\t-1 float\t\tposition 1 in state vector (default: 0)\n";
	msg += "\t-2 float\t\tposition 2 in state vector (default: 1)\n";
	msg += "\t-x float\t\tminimum position 1 value (default: -3.0)\n";
	msg += "\t-X float\t\tmaximum position 1 value (default:  3.0)\n";
	msg += "\t-y float\t\tminimum position 2 value (default: -3.0)\n";
	msg += "\t-Y float\t\tmaximum position 2 value (default:  3.0)\n";
	msg += "\t-T float\t\ttemperature (default: 1.0)\n";
	msg += "\t-d int  \t\tdimensionality (default: 2)\n";
	msg += "\t-c int  \t\ttarget points per cell (default: 32)\n";
	msg += "\t-l int  \t\tmultiple resolution level (default: 0)\n";
	msg += "\t-n int  \t\tnumber of points to sample (default: 1000000)\n";
	msg += "\t-N int  \t\tsample points per dimension (default: 100)\n";
	msg += "\t-f file \t\tMCMC state file (default: run.statelog)\n";
	msg += "\t-o file \t\toutput density file (default: density.dat)\n";
	cerr << msg; exit(-1);
      }
  }

				// State metadata
  StateInfo si(ndim);
				// Initialize tree prior

  TreePrior prior(&si, mcmc, lev, ncell, -1, 1, 1000000);
  //                   *     *    *       *  *  *
  //                   |     |    |       |  |  |
  // o state file------+     |    |       |  |  |
  //                         |    |       |  |  |
  // o level-----------------+    |       |  |  |
  //                              |       |  |  |
  // o # per cell-----------------+       |  |  |
  //                                      |  |  |
  // o skip (-1->1/2 sample)--------------+  |  |
  //                                         |  |
  // o stride--------------------------------+  |
  //                                            |
  // o maximum number of states to keep---------+
  //

				// Set the temperature
  prior.setTemp(temp);
  
				// Resample and print densities
  ofstream out(densf.c_str());
  if (out) {
    State  s(&si, prior.Mean());

    double dx = (xmax - xmin)/(numxy-1);
    double dy = (ymax - ymin)/(numxy-1);

    vector< vector<unsigned> > histo(numxy);
    for (int i=0; i<numxy; i++) histo[i] = vector<unsigned>(numxy, 0);

    double norm1 = 0, norm2 = 0;
    unsigned total = 0;
    vector<double> xy;
    int n1, n2;
    for (int n=0; n<nsamp; n++) {
      xy = prior.Sample();
      if (xy[pos1] >= xmin && xy[pos1] < xmax &&
	  xy[pos2] >= ymin && xy[pos2] < ymax ) 
	{
	  n1 = max<int>(min<int>( floor((xy[pos1] - xmin)/dx), numxy-1), 0);
	  n2 = max<int>(min<int>( floor((xy[pos2] - ymin)/dy), numxy-1), 0);
	  histo[n1][n2]++;
	  total++;
	}
    }

    for (int i=0; i<numxy; i++) {
      s.vec(pos1) = xmin + dx*i;

      for (int j=0; j<numxy; j++) {
	s.vec(pos2) = ymin + dy*j;

	double u = prior.PDF(s), v;
	if (nsamp) v = static_cast<double>(histo[i][j])/(dx*dy*nsamp);

	out << setw(18) << s.vec(pos1)
	    << setw(18) << s.vec(pos2)
	    << setw(18) << u;
	if (nsamp) out << setw(18) << v << endl;

	norm1 += u*dx*dy;
	norm2 += v*dx*dy;
      }
      out << endl;
    }
    cout << "Norm = [" << norm1 << ", " << norm2 << "]" << endl;
  }

  return 0;
}
