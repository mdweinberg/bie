#include <iostream>
#include <fstream>

using namespace std;

#include <Timer.h>
#include <serial.h>
#include <A.h>
#include <B.h>

/*******************************************************************************
* Test class for several kinds of vector objects.
*******************************************************************************/

class VectorTest 
{
public:
  VectorTest() {}
  ~VectorTest() {}
  void fillWithData(int ns, int nd);
  void print(ostream & out);
  
private:
  vector<string> v2;
  vector< vector< vector<A> > > aa;
  vector< vector< vector<B> > > bb;
  vector<int> cc, dd;
  vector< vector<float> > ee;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_NVP(v2);
    ar & BOOST_SERIALIZATION_NVP(aa);
    ar & BOOST_SERIALIZATION_NVP(bb);
    ar & BOOST_SERIALIZATION_NVP(cc);
    ar & BOOST_SERIALIZATION_NVP(dd);
    ar & BOOST_SERIALIZATION_NVP(ee);
  }
};

BOOST_CLASS_EXPORT(VectorTest)

void VectorTest::fillWithData(int ns, int nd)
{
  string str = "cat_";

  for (int i = 0; i < ns; i++)
  {
    v2.push_back(str);
    str = str + "a"; 
  }

  aa = vector< vector< vector<A> > > (nd);
  bb = vector< vector< vector<B> > > (nd);
  for (int i=0; i<nd; i++) {
    aa[i] = vector< vector<A> > (nd);
    bb[i] = vector< vector<B> > (nd);
    ee.push_back(vector<float>(nd));
    for (int j=0; j<nd; j++) {
      aa[i][j] = vector<A>(nd);
      bb[i][j] = vector<B>(nd);
      for (int k=0; k<nd; k++) {
	cc.push_back(i*j*k*2);
	dd.push_back(i*j*k*3);
      }
      ee[i].push_back(3.14159*i*j);
    }
  }
}

void VectorTest::print(ostream & out)
{
  out << "v2: ";
  for (unsigned int i = 0; i < v2.size(); i++)
  { out << v2[i] << ", ";  }
  out << endl;

  out << "aa: ";
  for (unsigned i=0; i<aa.size(); i++) {
    for (unsigned j=0; j<aa[i].size(); j++) {
      out << setw(4) << i << setw(4) << j;
      for (unsigned k=0; k<aa[i][j].size(); k++) {
	out << setw(18) << aa[i][j][k].x;
	out << setw(18) << aa[i][j][k].y;
	out << setw(18) << aa[i][j][k].z;
      }
      out << endl;
    }
  }
  out << endl;

  out << "bb: ";
  for (unsigned i=0; i<bb.size(); i++) {
    for (unsigned j=0; j<bb[i].size(); j++) {
      out << setw(4) << i << setw(4) << j;
      for (unsigned k=0; k<bb[i][j].size(); k++) {
	out << setw(18) << bb[i][j][k].u[0];
	out << setw(18) << bb[i][j][k].u[1];
	out << setw(18) << bb[i][j][k].u[2];
      }
      out << endl;
    }
  }
  out << endl;

  out << "cc: ";
  for (unsigned i=0; i<cc.size(); i++) out << cc[i] << endl;
}


int main(int argc, char** argv)
{
  Timer timer(true);
  TimeElapsed t;
  vector<VectorTest*> tc0, tc1;
  string archivename("test_archive");
    
  int nv = atoi(argv[1]);
  int ns = atoi(argv[2]);
  int nd = atoi(argv[3]);

  for (int n=0; n<nv; n++) {
    tc0.push_back(new VectorTest);
    tc0.back()->fillWithData(ns, nd);
  }
   
  timer.start();
  try {
    ofstream out(archivename.c_str());
    unsigned ver = 1;
#ifdef POLYMORPHIC
    boost::archive::polymorphic_xml_oarchive ar(out, ver);
#else
    boost::archive::xml_oarchive ar(out, ver);
#endif
    ar << BOOST_SERIALIZATION_NVP(tc0);
  }
  catch (boost::archive::archive_exception & e) {
    cout << "While SAVING, caught exception:" << endl
	 << e.what() << endl; 
  }
  t = timer.stop();
  cout << "Save time=" << 1.0e-6*t.getTotalTime() << endl;
  
  timer.reset();
  timer.start();
  try {
    ifstream in(archivename.c_str());
    unsigned ver = 1;
#ifdef POLYMORPHIC
    boost::archive::polymorphic_xml_iarchive ar(in, ver);
#else
    boost::archive::xml_iarchive ar(in, ver);
#endif
    ar >> BOOST_SERIALIZATION_NVP(tc1);
  }
  catch (boost::archive::archive_exception & e) {
    cout << "While LOADING, caught exception:" << endl
	 << e.what() << endl; 
  }
  t = timer.stop();
  cout << "Load time=" << 1.0e-6*t.getTotalTime() << endl;
  
  /*
  fstream actualoutput;
  string filename = archivename + "_actual";
  actualoutput.open(filename.c_str(), fstream::out);
  tc0.print(actualoutput);
  actualoutput.close();

  fstream restoredoutput;
  filename = archivename + "_restored";
  restoredoutput.open(filename.c_str(), fstream::out);
  tc1.print(restoredoutput);
  restoredoutput.close();
  */
}
