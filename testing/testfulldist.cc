#define IS_MAIN
#include <DataTree.h>
#include <Histogram1D.h>
#include <HistogramND.h>
#include <OneBin.h>
#include <MultiDistribution.h>
#include <Distribution.h>
#include <iostream>
#include <SquareTile.h>
#include <QuadGrid.h>
#include <RecordStream_Ascii.h>
#include <BasicType.h>
#include <Node.h>

using namespace BIE;

/*******************************************************************************
* This tests the DataTree class with different tile distribution
* classes.
*******************************************************************************/
void MultiDistributiontest();
void OneBintest();
void HistogramNDtest();
void Histogram1Dtest();
void commontests(BinnedDistribution*, string);
void dumpbinneddist(BinnedDistribution * binneddist);

void printarray(vector<double> array)
{
  if (array.size() == 0)
  {
    cout << "{ empty vector }" << endl;
  }
  else
  {
    cout << "{" << array[0];
  
    for (int arrayindex = 1; arrayindex < (int) array.size(); arrayindex++)
    {
      cout << ", " << array[arrayindex];
    }
    
    cout << "}" << endl;
  }
}

int main ()
{
  cout << "\n\n\n"<< "************* One Bin tests *************** " << endl;
  OneBintest();

  cout << "\n\n\n"<< "************* Histrogram 1D tests ************ " << endl;
  Histogram1Dtest();

  cout << "\n\n\n"<< "************* Histogram ND tests ************ " << endl;
  HistogramNDtest();

  cout << "\n\n\n"<< "************* Multi Distribution tests ********" << endl;
  MultiDistributiontest();

  cout << "\n\n\n" << "************* Test finished *********" << endl;
}


void OneBintest()
{
  OneBin * onebin = new OneBin();
  commontests(onebin, "data.dat");
}

void Histogram1Dtest()
{
  /*****************************************************************************
  * Constructor tests.
  *****************************************************************************/
  Histogram1D * histogram;
  RecordType * rt;
  
  try {
    histogram = new Histogram1D(-5, -5, 20, "mag1");
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    histogram = new Histogram1D(-5, -10, 20, "mag1");
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    histogram = new Histogram1D(-5, -10, 0, "mag1");
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    histogram = new Histogram1D(-5, -10, -1, "mag1");
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  rt = new RecordType();
  rt = rt->insertField(1,"one",BasicType::Real);
  rt = rt->insertField(2,"two",BasicType::Real);
  
  try {
    histogram = new Histogram1D(-5, 5, 20, rt);
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  rt = rt->deleteField(2);
  
  try {
    histogram = new Histogram1D(-5, 5, 2, rt);
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }


  histogram = new Histogram1D(-5, 5, 2, "mag1");
  commontests(histogram, "data.dat");
}

void HistogramNDtest()
{


}

void MultiDistributiontest()
{


}


void commontests(BinnedDistribution * binneddist, string datafile)
{
  cout << "************ COMMON TESTS **************" << endl;
  
  /*****************************************************************************
  * What does the distribution look like without data?? 
  *****************************************************************************/
  dumpbinneddist(binneddist);
  
  /*****************************************************************************
  * Read the data.
  *****************************************************************************/
  RecordInputStream * ris = 0;

  try 
  {
    ris = new RecordInputStream_Ascii(datafile);
  } 
  catch (BIEException e) 
  {
    cout << e.getErrorMessage() << endl;
  }

  /*****************************************************************************
  * Try with Quad Grid first.
  *****************************************************************************/
  Tile * tile = new SquareTile();
  QuadGrid * quadgrid = new QuadGrid(tile, -3.14, 3.14, -3.14, 3.14, 2);

  /*****************************************************************************
  * Build the full distribution.
  *****************************************************************************/
  DataTree * fulldist = new DataTree(ris, binneddist, quadgrid);
  
  cout << "Total() returns: " << fulldist->Total() << endl;
  cout << "OffGrid() returns: " << fulldist->Offgrid() << endl;
  
  cout << "GetTile(-1): " << fulldist->GetTile(-1) << endl;
  cout << "GetTile(0): " << fulldist->GetTile(0) << endl;
  cout << "GetTile(1): " << fulldist->GetTile(1) << endl;
  cout << "GetTile(10000000): " << fulldist->GetTile(10000000) << endl;

  cout << "Number of tiles in frontier: " << fulldist->NumberItems() << endl;

  fulldist->Reset();  

  for (; ! fulldist->IsDone(); fulldist->Next())
  {
    cout << "Current Tile ID: "  << fulldist->CurrentTile()->GetNode()->ID() << endl;  
    fulldist->CurrentTile()->printTile(cout);

    dumpbinneddist((BinnedDistribution*)fulldist->CurrentItem());
  }
  
  dumpbinneddist((BinnedDistribution*)fulldist->First());
  dumpbinneddist((BinnedDistribution*)fulldist->Last());
  
  try {
    cout << "getDistribution(-1): " << fulldist->getDistribution(-1) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "getDistribution(0): " << fulldist->getDistribution(0) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "getDistribution(1): " << fulldist->getDistribution(1) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }

  try {
    cout << "getDistribution(10000000): " << fulldist->getDistribution(10000000) << endl;
  } catch (BIEException e) { cout << e.getErrorMessage() << endl; }
}


void dumpbinneddist(BinnedDistribution * binneddist)
{
  cout << "DUMPING BINNED DIST ************************************" << endl;
  cout << "numberData returns: " << binneddist->numberData() << endl; 
  cout << "getValue(-1) returns: " << binneddist->getValue(-1) << endl;
  cout << "getValue(0) returns: " << binneddist->getValue(0) << endl;
  cout << "getValue(1) returns: " << binneddist->getValue(1) << endl;
  cout << "getValue(10000000) returns: " << binneddist->getValue(10000000) << endl;

  cout << "getLow(-1) returns : " << endl;
  printarray(binneddist->getLow(-1)); 
  cout << "getLow(0) returns : " << endl; 
  printarray(binneddist->getLow(0)); 
  cout << "getLow(1) returns : " << endl; 
  printarray(binneddist->getLow(1)); 
  cout << "getLow(10000000) returns : " << endl; 
  printarray(binneddist->getLow(10000000)); 
  
  cout << "getHigh(-1) returns : " << endl; 
  printarray(binneddist->getHigh(-1)); 
  cout << "getHigh(0) returns : " << endl; 
  printarray(binneddist->getHigh(0)); 
  cout << "getHigh(1) returns : " << endl; 
  printarray(binneddist->getHigh(1)); 
  cout << "getHigh(10000000) returns : " << endl; 
  printarray(binneddist->getHigh(10000000)); 

  cout << "Moments(0) returns : " << endl; 
  printarray(binneddist->Moments(0)); 
  cout << "Moments(1) returns : " << endl; 
  printarray(binneddist->Moments(1)); 
  cout << "Moments(2) returns : " << endl; 
  printarray(binneddist->Moments(2)); 
  cout << "Moments(3) returns : " << endl; 
  printarray(binneddist->Moments(3)); 


  cout << "getDim(-1) returns : " << binneddist->getdim(-1) << endl; 
  cout << "getDim(0) returns : " << binneddist->getdim(0) << endl; 
  cout << "getDim(1) returns : " << binneddist->getdim(1) << endl; 
  cout << "getDim(10000000) returns : " << binneddist->getdim(10000000) << endl; 
  
  cout << "Type: \n" << binneddist->getRecordType()->toString() << endl;
 
  cout << "getDataSetSize() returns: " << binneddist->getDataSetSize() << endl;
  
  cout << "lower: ";
  printarray(binneddist->lower());
  cout << "upper: ";
  printarray(binneddist->upper());
  cout << "Mean: ";
  printarray(binneddist->Mean());
  cout << "StdDev: ";
  printarray(binneddist->StdDev());
  // cout << "Sample: ";
  // printarray(binneddist->Sample());
}
