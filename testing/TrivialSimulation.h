// This may look like C code, but it is really -*- C++ -*-


#ifndef TrivialSimulation_h
#define TrivialSimulation_h

namespace BIE {

  class TrivialSimulation : public Simulation
    {
    public:
      TrivialSimulation(StateInfo *si,
			DataTree* dist, Model* model, 
			Integration* intgr, Converge* converge, 
			Prior* prior, LikelihoodComputation* l,
			MCAlgorithm* mca, Simulation* last=NULL) :
	Simulation(si, dist, model, intgr, converge, prior, l, mca, NULL) {};
      
    private:
      void MCMethod();
    };
  
  void TrivialSimulation::MCMethod() {}
}  
  
#endif
