
#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

#include <stdlib.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <Simulation.h>
#include <CountConverge.h>
#include <MappedGrid.h>
#include <Histogram1D.h>
#include <Model.h>
#include <InitialMixturePrior.h>
#include <SimpleStat.h>
#include <LegeIntegration.h>
#include <DataTree.h>
#include <RecordStream_Ascii.h>
#include <SquareTile.h>
#include <KdTessellation.h>
#include <BIEdebug.h>
#include <EnsembleStat.h>

#include <DummyModel.h>
#include <TrivialSimulation.h>
#include <MetropolisHastings.h>
#include <LikelihoodComputationSerial.h>


using namespace BIE;

string priorfile("prior.dat");

/**
   @name testsimu: main
   Checks consistency of interfaces by generating data grid and
   associated classes needed by Simulation.  Most instantiated classes
   are dummies.
*/
int
main()
{
  DummyModel *model;
  Tessellation *tess;
  DataTree *dist;
  Histogram1D *hist;
  SquareTile* tile;
  RecordInputStream * ris;
  vector<string> dname, dtype;

				// Random number generator
  BIEgen = new BIEACG(11, 25);

				// Instantiate dummy model
  model = new DummyModel();

  hist = new Histogram1D(0.0, 20.0, 0.5, "mag1");

  tile = new SquareTile();
  ris = new RecordInputStream_Ascii("data.dat");
  tess = new KdTessellation(tile, ris, 100, 1.0);

  ris = new RecordInputStream_Ascii("data.dat");
  dist = new DataTree(ris, hist, tess);    
	
  const int ndim = 4;

  // StateInfo
  StateInfo *si = new StateInfo(ndim);


  // Define integration method
  Integration* intgr = new LegeIntegration(2, 2);

  // Define prior
  Distribution *unif = new UniformDist(-1.0, 1.0);
  vector<Distribution*> vdist(2, unif);
  Prior* prior = new Prior(si, vdist);

  // Simple sample statistic
  EnsembleStat* sstat = new EnsembleStat(si);

  // Define converge
  CountConverge *converge = new CountConverge(10, sstat);

  // Print frontier
  printFrontierTiles(dist->GetDefaultFrontier());

  // Likelihood compuation
  LikelihoodComputationSerial *l = new LikelihoodComputationSerial;

  // MC Algorithm
  MCAlgorithm *mca = new MetropolisHastings;

  TrivialSimulation testsim(si, dist, model, intgr, converge, prior, l, mca);

}


