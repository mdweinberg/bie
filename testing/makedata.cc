// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <gvariable.h>
#include <StdCandle2.h>

using namespace BIE;

int NUM=40;
int NTAB=100;
int M=0;
double ALPHA=0.0;
double BETA=0.0;
double R=8.0;
double A=3.5;
double A1=3.5;
double KLOW=0.0;
double KLIM=14.3;
double K0=-8.0;
double SIGK=0.25;
double Z=350.0;
double H=100.0;
double MU=0.67;
double BMAX=10.0;


int 
main(int argc, char** argv)
{
  double L=0, B=0;
  bool scaled = false;
  bool debug = false;
  long iseed = 11;
  
  int c;
  while (1) {
    c = getopt (argc, argv, 
		"n:N:a:b:A:B:R:K:E:0:S:H:Z:M:m:i:U:xd");
    if (c == -1) break;
     
    switch (c)
      {
      case 'n': NUM = atoi(optarg); break;
      case 'N': NTAB = atoi(optarg); break;
      case 'a': ALPHA = atof(optarg); break;
      case 'b': BETA = atof(optarg); break;
      case 'A': A = atof(optarg); break;
      case 'B': A1 = atof(optarg); break;
      case '0': K0 = atof(optarg); break;
      case 'K': KLIM = atof(optarg); break;
      case 'S': SIGK = atof(optarg); break;
      case 'H': H = atof(optarg); break;
      case 'Z': Z = atof(optarg); break;
      case 'M': M = atoi(optarg); break;
      case 'm': BMAX = atof(optarg); break;
      case 'i': iseed = atoi(optarg); break;
      case 'U': MU = atof(optarg); break;
      case 'x': scaled = true; break;
      case 'd': debug = true; break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-n N\t\twhere N is the number of integration knots\n";
	msg += "\t-N N\t\twhere N is the size of the interpolation tables\n";
	msg += "\t-a a\t\twhere a, b such that weighting is x^a (1-x)^b\n";
	msg += "\t-ab b\n";
	msg += "\t-A z\t\texponential disk scale length\n";
	msg += "\t-B z\t\textinction scale length\n";
	msg += "\t-0 z\t\tmean of standard candle distribution\n";
	msg += "\t-S z\t\tdispersion of standard candle distribution\n";
	msg += "\t-K z\t\tlimiting flux\n";
	msg += "\t-E z\t\textinction per kpc (does nothing so far)\n";
	msg += "\t-H z\t\tdisk scale height\n";
	msg += "\t-Z z\t\textinction scale height\n";
	msg += "\t-M n\t\tgenerate n samples\n";
	msg += "\t-U x\t\tscale factor for galactic latitude (MU)\n";
	msg += "\t-i x\t\tseed for random number generator\n";
	msg += "\t-x x\t\tplot in scaled coordinates\n";
	cerr << msg; exit(-1);
      }
  }

  BIEgen = new BIEACG(iseed, 20);

  const double onedeg = M_PI/180.0;
  L *= onedeg;
  B *= onedeg;

  StdCandle::NUM = NUM;
  StdCandle::NTAB = NTAB;
  StdCandle::ALPHA = ALPHA;
  StdCandle::BETA = BETA;

  StdCandle std(A1, H, K0, SIGK, KLOW, KLIM);

  if (M) {

    vector<double> star;

    // Header
    //

    cout << "real \"x\""    << endl;
    cout << "real \"y\""    << endl;
    cout << "real \"attr\"" << endl;
    cout << endl;

    cout.setf(ios::scientific);
    cout.precision(12);

    for (int i=0; i<M; i++) {
      star = std.realize(A, Z);
      if (debug) std.print_kfrac("testkdist.dat");
      cout 
	<< setw(20) << star[0]
	<< setw(20) << pow(fabs(sin(star[1])), 1.0-MU) * copysign(1.0, star[1])
	<< setw(20) << star[2]
	<< endl;
    }

				// Write parameters
    cout << "#     M=" << M    << endl;
    cout << "#     R=" << R    << endl;
    cout << "#     A=" << A    << endl;
    cout << "#    A1=" << A1   << endl;
    cout << "#  KLOW=" << KLOW << endl;
    cout << "#  KLIM=" << KLIM << endl;
    cout << "#    K0=" << K0   << endl;
    cout << "#  SIGK=" << SIGK << endl;
    cout << "#     H=" << H    << endl;
    cout << "#     Z=" << Z    << endl;
    cout << "#    MU=" << MU   << endl;

  } else {

    int numl = 200;
    int numb = 40;
    float z;
  
    if (scaled) {
      cout.write(reinterpret_cast<char*>(&numl), sizeof(int));
      cout.write(reinterpret_cast<char*>(&numb), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z=-180.0)), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z= 180.0)), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z=-BMAX)), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z= BMAX)), sizeof(int));

      double dl = 360.0/(numl-1);
      double dq = 2.0*BMAX/(numb-1);
      double q;

      for (int j=0; j<numb; j++) {
	for (int i=0; i<numl; i++) {
	  L = onedeg*(-180.0 + dl*i);
	  q = -1.0 + dq*j;
	  B = asin(pow(fabs(q), 1.0/(1.0 - MU))) * copysign(1.0, q);
	  
	  cout.write(reinterpret_cast<char*>(
                        &(z=std.dens(A, Z, L, B)*pow(fabs(q), MU/(1.0-MU)))), 
		     sizeof(float));
	}
      }
    } else {
      cout.write(reinterpret_cast<char*>(&numl), sizeof(int));
      cout.write(reinterpret_cast<char*>(&numb), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z=-180.0)), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z= 180.0)), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z=-BMAX)), sizeof(int));
      cout.write(reinterpret_cast<char*>(&(z= BMAX)), sizeof(int));

      double dl = 360.0/(numl-1);
      double db = 2.0*BMAX/(numb-1);

      for (int j=0; j<numb; j++) {
	for (int i=0; i<numl; i++) {
	  L = onedeg*(-180.0 + dl*i);
	  B = onedeg*(-BMAX + db*j);
	  
	  cout.write(reinterpret_cast<char*>(&(z=std.dens(A, Z, L, B))),
                     sizeof(float));
	}
      }
    }
  }

}
