// This is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include <cmath>
#include <cstdlib>

using namespace std;

#include <unistd.h>

#include <ACG.h>
#include <Uniform.h>
#include <Normal.h>
#include <BasicType.h>

#include <BIEconfig.h>
#include <GalaxyModelOneD.h>
#include <Histogram1D.h>
#include <HistogramND.h>
#include <RecordStream_Ascii.h>

using namespace BIE;

const double onedeg = M_PI/180.0;
const double Log10  = log(10.0);

//
// Model parameters
//
int ISEED    =  11;
double R0    =  8.0;
double A     =  3.5;
double K0    = -4.0;
double SIGK  =  0.25;
double A1    =  20.0;
double Z1    =  100.0;
double H     =  350.0;
double AK    =  0.1;

class MakeStar
{
private:
  ACG*     gen;
  Uniform* unit;
  Normal*  var;
  
public:

  static int MAXITER;
  static double TOL;

  MakeStar()
  {
    gen = new ACG(ISEED);
    unit = new Uniform(0.0, 1.0, gen);
    var = new Normal(0.0, SIGK*SIGK, gen);
  }

  ~MakeStar()
  {
    delete gen;
    delete unit;
  }

  vector<double> realize()
  {

    vector<double> ret(6);

    double fac, del, rran = log(1.0 - (*unit)());
    double z = 1.0e-3*H*atanh(2.0*(*unit)() - 1.0);

    double r = -rran;
    for (int i=0; i < MAXITER; i++) {
      fac = 1.0 + r;
      del = fac*(log(fac) - r - rran)/r;
      r += del;
      if (fabs(del) < TOL) break;
    }
    r *= A;


    double phi  = 2.0*M_PI*(*unit)();
    double x    = r*cos(phi) + R0;
    double y    = r*sin(phi);
    double s    = sqrt(x*x + y*y + z*z);
    
    double L    = atan2(y, x);
    double B    = asin(z/s);

    double sinL = sin(L);
    double cosL = cos(L);
    double sinB = sin(B);
    double cosB = cos(B);

    double smax;
    if (fabs(cosB)<1.0e-6) 
      smax = Z1*1.0e-3/fabs(sinB);
    else if (fabs(sinB)<1.0e-6) 
      smax = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));
    else
      smax = min<double>(
			 Z1*1.0e-3/fabs(sinB), 
			 R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL))
			 );

    ret[0] = L;
    ret[1] = B;
    ret[2] = K0 + (*var)() + 5.0*log(s*1.0e3)/Log10 - 5.0 + AK*min<double>(s, smax);
    ret[3] = x;
    ret[4] = y;
    ret[5] = z;

    return ret;
  }

};

int    MakeStar::MAXITER = 1000;
double MakeStar::TOL     = 1.0e-10;

int 
main(int argc, char** argv)
{
  bool los_test  = false;
  double L       = 10.0;
  double B       =  5.0;
  double dL      = 1.0;
  double dB      = 1.0;
  int number     = 1000;
  string OUTFILE = "galaxy_ascii.dat";
  
  while (1) {
    int c = getopt (argc, argv, "M:A:B:0:S:H:Z:K:i:l:b:to:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'M': number = atoi(optarg); break;
      case 'A': A = atof(optarg); break;
      case 'H': H = atof(optarg); break;
      case 'B': A1 = atof(optarg); break;
      case 'Z': Z1 = atof(optarg); break;
      case 'K': AK = atof(optarg); break;
      case '0': K0 = atof(optarg); break;
      case 'S': SIGK = atof(optarg); break;
      case 'i': ISEED = atoi(optarg); break;
      case 'l': L = atof(optarg); break;
      case 'b': B = atof(optarg); break;
      case 't': los_test = true; break;
      case 'o': OUTFILE.erase(); OUTFILE=optarg; break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-M n\t\tnumber of stars\n";
	msg += "\t-A z\t\texponential disk scale length\n";
	msg += "\t-H z\t\tdisk scale height\n";
	msg += "\t-B z\t\tsize of extinction disk\n";
	msg += "\t-Z z\t\textinction scale height\n";
	msg += "\t-K z\t\textinction coefficient\n";
	msg += "\t-0 z\t\tmean of standard candle distribution\n";
	msg += "\t-i x\t\tseed for random number generator\n";
	msg += "\t-l x\t\tlongitude in degrees\n";
	msg += "\t-b x\t\tlatitude in degrees\n";
	msg += "\t-t x\t\ttest line-of-sight distribution\n";
	msg += "\t-o s\t\toutput data file\n";
	cerr << msg; exit(-1);
      }
  }

  if (los_test) {

    L  *= onedeg;
    B  *= onedeg;
    dL *= onedeg;
    dB *= onedeg;

    // Set global parameters
    //
    GalaxyModelOneD::RMAX = 100.0;
    GalaxyModelOneD::NUM  = 200;

    // Hold instantiation of classes define the simuation
    //
    GalaxyModelOneD *model;
    Histogram1D *histo;
    HistogramND *histo1;

    int ndim = 2;		// Two model parameters

				// One model
    int nmix = 1;

				// Factor for 1D histogram in each tile
    histo = new Histogram1D(6.0, 15.0, 1.0, "namedattribute");// 

				// ND histogram for comparison

    clivectord blo(1,6.0), bhi(1,15.0), bwid(1,1.0);
    clivectors bnam(1, "namedattribute");
    histo1 = new HistogramND(&blo, &bhi, &bwid, &bnam);

				// Galaxy model
    model = new GalaxyModelOneD(ndim, nmix, histo);

    vector<double> winit(nmix);
    vector< vector<double> > pinit(nmix);

    winit[0] = 1.0;
    pinit[0] = vector<double>(ndim);
    pinit[0][0] = A;
    pinit[0][1] = H;
  
    model->Initialize(winit, pinit);
    // double norm = model->NormEval(L, B);
    vector<double> theory = model->Evaluate(L, B, histo);

				// Do simulation
    MakeStar star;
    vector<double> ans;

    int icnt = 0;
    int ioff = 0;

    RecordBuffer * buffer = new RecordBuffer();
    buffer = buffer->insertField(1, "namedattribute", BasicType::Real);
	  
    while (1) {
      ans = star.realize();
      if (fabs(ans[0] - L) <= dL  &&
	  fabs(ans[1] - B) <= dB  ) {

	
				// Off the lower end of grid
	if (ans[2] < 6.0) {
	  ioff++;
	} else {

	  buffer->setRealValue(1, ans[2]);
	  histo->AccumData(1.0, buffer);
	  histo1->AccumData(1.0, buffer);
	  icnt++;
	
	  if (icnt>number) break;

	  if ( icnt - (int)(icnt/100)*100 == 0 ) 
	    cout << "\rNumber so far: " << icnt << flush;
	}

      }

    }
      
    histo  -> ComputeDistribution();
    histo1 -> ComputeDistribution();

    cout << endl << ioff << " points off the grid\n";

    for (int i=0; i<histo->numberData(); i++) {
      cout
	<< setw(15) << histo->getLow(i)[0]
	<< setw(15) << histo->getHigh(i)[0]
	// << setw(15) << theory[i]/norm
	<< setw(15) << theory[i]
	<< setw(15) << histo->getValue(i)/icnt
	<< setw(15) << sqrt(histo->getValue(i))/icnt
	<< endl;
    }
    
    cout << "Number=" << histo->numberData() << endl;
    cout << "Mean="   << histo->Mean()[0]    << endl;
    cout << "StdDev=" << histo->StdDev()[0]  << endl;

    cout << endl;

    for (int i=0; i<histo1->numberData(); i++) {
      cout
	<< setw(15) << histo1->getLow(i)[0]
	<< setw(15) << histo1->getHigh(i)[0]
	// << setw(15) << theory[i]/norm
	<< setw(15) << theory[i]
	<< setw(15) << histo1->getValue(i)/icnt
	<< setw(15) << sqrt(histo1->getValue(i))/icnt
	<< endl;
    }
    
    cout << "Number=" << histo1->numberData() << endl;
    cout << "Mean="   << histo1->Mean()[0]    << endl;
    cout << "StdDev=" << histo1->StdDev()[0]  << endl;

  } else {

    MakeStar star;
    vector<double> ans;

    RecordType * rt = new RecordType();
    rt = rt->insertField(1, "x", BasicType::Real);
    rt = rt->insertField(2, "y", BasicType::Real);
    rt = rt->insertField(3, "mag1", BasicType::Real);
    rt = rt->insertField(4, "Xpos", BasicType::Real);
    rt = rt->insertField(5, "Ypos", BasicType::Real);
    rt = rt->insertField(6, "Zpos", BasicType::Real);
    
    RecordOutputStream *ros  = new RecordOutputStream(rt);
    RecordOutputStream *rosa = new RecordOutputStream_Ascii(ros, OUTFILE, true);
  
    for (int i=0; i<number; i++) {
      ans = star.realize();

      try {
	for (int k=0; k<6; k++) ros->setRealValue(1+k, ans[k]);
      } catch (BIE::BIEException e) {
	cout << e.getErrorMessage() << endl; 
      } catch (...) {
	cout << "Non-BIE exception\n";
      }
      rosa->pushRecord();
    }

    delete rt;
    delete ros;
    delete rosa;
  }

  return 0;
}
