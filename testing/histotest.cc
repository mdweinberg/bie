// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>

using namespace std;

#include <unistd.h>

#include <ACG.h>
#include <Uniform.h>
#include <Normal.h>
#include <BasicType.h>

#include <BIEconfig.h>
#include <Histogram1D.h>
#include <HistogramND.h>
#include <RecordStream_Ascii.h>

using namespace BIE;

int 
main(int argc, char** argv)
{
  int NUMBER=1000;

  int c;
  while (1) {
    c = getopt (argc, argv, "N:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'N': NUMBER = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-N n\t\tnumber of points\n";
	msg += "\t-S s\t\trandom number seed\n";
	cerr << msg; exit(-1);
      }
  }

  // Factor for 1D histogram in each tile
  Histogram1D *histo = new Histogram1D(0.0, 2.0, 1.0, "namedattribute");

  // ND histogram for comparison
  clivectord blo(1,0.0), bhi(1,2.0), bwid(1,1.0);
  clivectors bnam(1, "namedattribute");
  HistogramND *histo1 = new HistogramND(&blo, &bhi, &bwid, &bnam);

  RecordBuffer * buffer = new RecordBuffer();
  buffer = buffer->insertField(1, "namedattribute", BasicType::Real);
	  
  int icnt = 0;

  while (1) {
    
    buffer->setRealValue(1, 1.5);
    histo->AccumData(1.0, buffer);
    histo1->AccumData(1.0, buffer);
    icnt++;
    
    if (icnt>NUMBER) break;

    if ( icnt - (int)(icnt/100)*100 == 0 ) 
      cout << "\rNumber so far: " << icnt << flush;
  }
      
  cout << endl;
  cout << endl;

  histo  -> ComputeDistribution();
  histo1 -> ComputeDistribution();

  for (int i=0; i<histo->numberData(); i++) {
    cout
      << setw(15) << histo->getLow(i)[0]
      << setw(15) << histo->getHigh(i)[0]
      << setw(15) << histo->getValue(i)
      << setw(15) << histo->getValue(i)/icnt
      << setw(15) << sqrt(histo->getValue(i))/icnt
      << endl;
  }
    
  cout << "Number=" << histo->numberData() << endl;
  cout << "Mean="   << histo->Mean()[0]    << endl;
  cout << "StdDev=" << histo->StdDev()[0]  << endl;

  cout << endl;
  
  for (int i=0; i<histo1->numberData(); i++) {
    cout
      << setw(15) << histo1->getLow(i)[0]
      << setw(15) << histo1->getHigh(i)[0]
      << setw(15) << histo1->getValue(i)
      << setw(15) << histo1->getValue(i)/icnt
      << setw(15) << sqrt(histo1->getValue(i))/icnt
      << endl;
  }
    
  cout << "Number=" << histo1->numberData() << endl;
  cout << "Mean="   << histo1->Mean()[0]    << endl;
  cout << "StdDev=" << histo1->StdDev()[0]  << endl;

  return 0;
}
