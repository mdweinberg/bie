/*******************************************************************************
* can successfully save and restore a variety of STL container classes.
*******************************************************************************/

#include <iostream>
#include <config.h>
#include <fstream>

#if PERSISTENCE_ENABLED
#include <Serializable.h>
#include <gvariable.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>

#include <InitialMixturePrior.h>
#include <EnsembleStat.h>
#include <SubsampleConverge.h>
#include <PointIntegration.h>
#include <EmptyDataTree.h>
#include <LikelihoodComputationSerial.h>
#include <MetropolisHastings.h>
#include <MHWidthOne.h>
#include <ParallelChains.h>
#include <Chain.h>


using namespace std;

/*******************************************************************************
* Crash Dummy class
*******************************************************************************/
class CrashDummy : public Model {
public:
  CrashDummy() {}
  
  ~CrashDummy() {}

  void Initialize(State&) {}
  double NormEval(double x,double y, SampleDistribution *d) { return 0.0; }
  string ParameterDescription(int i) { return ""; }
  vector<string> ParameterLabels() { return vector<string>(1); }
  int DataDimension() { return 0; }
  
private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    this->pre_serialize(ar, version);
    try {                                                         
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Model);            
      ::boost::throw_exception(::boost::archive::archive_exception(::boost::archive::archive_exception::unregistered_class));
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    this->post_serialize(ar, version);
  }
};

BOOST_CLASS_EXPORT(CrashDummy)

int main()
{
  string statename = "testPersistenceStackTrace";

  int ndim = 1;
  int nmix = 2;
  int minmc = 1;
  int nsteps = 20000;
  double maxT = 256.0;
  StateInfo si(nmix, ndim);
  UniformDist unif(-0.2, 1.2);
  clivectordist vdist(1, &unif);
  InitialMixturePrior prior(&si, 1.0, &vdist);
  EnsembleStat sstat(&si);
  SubsampleConverge convrg(nsteps, &sstat, "0");
  PointIntegration intgr;
  EmptyDataTree dis;
  LikelihoodComputationSerial like;
  MetropolisHastings mca;
  CrashDummy crash;
  MHWidthOne mhwidth(&si, "testPersistenceStackTrace_methast.dat.2");
  ParallelChains sim(&si, minmc, maxT, &mhwidth, &dis, &crash, &intgr, &convrg, &prior, &like, &mca);
  
  TestUserState teststate(statename, 0, true);
  teststate.addObject(&sim);

  try {
    TestSaveManager testSaveManager(&teststate, BOOST_XML, BACKEND_FILE);
    testSaveManager.save();
  } catch (BoostSerializationException * e) {
    cout << "####### BoostSerializationException thrown! ########" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "####################################################" << endl;
    delete e;
  }
}

#else

int main()
{
  cout << "This test is only appropriate when persistence is enabled" << endl;
}

#endif
