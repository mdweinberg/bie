/******************************************************************************
* This is a collection of tests that checks error checking and
* sanity saving requirements are enforced when creating archives, together with
* tests of generally tricky transitive closures.
******************************************************************************/
#include <iostream>
#include <gvariable.h>
#include <config.h>

#include <EngineConfig.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <BIEException.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#include <FileBackend.h>

#include <Serializable.h>

#include <Node.h>
#include <SquareTile.h>
#include <AData.h>
#include <CliArgList.h>

/*
BOOST_CLASS_EXPORT(BIE::Node)
BOOST_CLASS_EXPORT(BIE::Tile)
BOOST_CLASS_EXPORT(BIE::SquareTile)
*/

using namespace BIE;

/* This is for simple save/load testing. */
int main() 
{
  TestUserState saveState("testPersistence_AData1", 10, true);

  // bool
  AData* adata01 = new AData(AData::bool_TYPE);
  adata01->set_bool(false);
  saveState.addObject(adata01);

  // uint32
  AData* adata02 = new AData(AData::uint32_TYPE);
  adata02->set_uint32(123);
  saveState.addObject(adata02);

  // int32
  AData* adata03 = new AData(AData::int32_TYPE);
  adata03->set_int32(-123);
  saveState.addObject(adata03);

  // float
  AData* adata04 = new AData(AData::float_TYPE);
  adata04->set_float(123.45f);
  saveState.addObject(adata04);

  // double
  AData* adata05 = new AData(AData::double_TYPE);
  adata05->set_double(9876.5431);
  saveState.addObject(adata05);

  // char*
  AData* adata06 = new AData(AData::charstar_TYPE);
  adata06->set_charstar("Magic word - adata06");
  saveState.addObject(adata06);

  // void*
  // Node* node = new Node(new SquareTile(1.0f,2.0f,3.0f,4.0f),10,10,0);
  Node* node = new MonoNode(new SquareTile(1.0f,2.0f,3.0f,4.0f),10,10);
  AData* adata07 = new AData(node, "Node");
  saveState.addObject(adata07);

  // variable (char*)
  AData* adata08 = new AData(AData::variable_TYPE);
  adata08->set_variable("Magic variable - adata08");
  saveState.addObject(adata08);

  // arrayelement (ArgList*)
  AData* adata09 = new AData(AData::arrayelement_TYPE);
  CliArgList* arglist01 = new CliArgList();
  AData* list01 = new AData(AData::bool_TYPE);
  list01->set_bool(true);
  AData* list02 = new AData(AData::uint32_TYPE);
  list02->set_uint32(456);
  AData* list03 = new AData(AData::charstar_TYPE);
  list03->set_charstar("Abracatabra - list03");
  arglist01->add_element(list01);
  arglist01->add_element(list02);
  arglist01->add_element(list03);
  adata09->set_arrayelement(arglist01);
  saveState.addObject(adata09);

  // arrayelementptr (CliArgList*)
  AData* adata10 = new AData(AData::arrayelementptr_TYPE);
  CliArgList* arglist02 = new CliArgList();
  AData* list11 = new AData(AData::bool_TYPE);
  list11->set_bool(true);
  AData* list12 = new AData(AData::uint32_TYPE);
  list12->set_uint32(456);
  AData* list13 = new AData(AData::charstar_TYPE);
  list13->set_charstar("Abracatabra - list13");
  arglist02->add_element(list11);
  arglist02->add_element(list12);
  arglist02->add_element(list13);
  adata10->set_arrayelementptr(arglist02);
  saveState.addObject(adata10);

  // variablearrayindex (char*)
  AData* adata11 = new AData(AData::variablearrayindex_TYPE);
  adata11->set_variablearrayindex("Magic variablearrayindex");
  saveState.addObject(adata11);

  // arrayindex (uint32)
  AData* adata12 = new AData(AData::arrayindex_TYPE);
  adata12->set_arrayindex(9876);
  saveState.addObject(adata12);

  try {
    // TestSaveManager testSaveManager(&saveState, BOOST_BINARY, BACKEND_FILE);
    TestSaveManager testSaveManager(&saveState, BOOST_XML, BACKEND_FILE);
    testSaveManager.save();
  } catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }

  // Some time later
  
  TestUserState *loadState;

  try {
    TestLoadManager testLoadManager(&loadState, "testPersistence_AData1", 11,
				    BOOST_XML,    BACKEND_FILE);
    //				    BOOST_BINARY, BACKEND_FILE);
    testLoadManager.load();
  } catch (BoostSerializationException * e) {
    cout << "#######################################################" << endl;
    cout << "##### BoostSerializationException thrown on LOAD! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#######################################################" << endl;
  }

  // save the loaded stuff to check for correctness
  // TestSaveManager testSaveManager2(loadState, BOOST_BINARY, BACKEND_FILE);
  TestSaveManager testSaveManager2(loadState, BOOST_XML, BACKEND_FILE);
  testSaveManager2.save();
}
