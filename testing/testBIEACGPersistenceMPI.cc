/*******************************************************************************
* This is a collection of tests that checks the persistence library
* can successfully save and restore a variety of STL container classes.
*******************************************************************************/

#include <iostream>
#include <fstream>

#include <config.h>

#include <bieTags.h>
#include <Serializable.h>
#include <gvariable.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>

#include <BIEmpi.h>
#include <BIEACG.h>
#include <Uniform.h>
#include <gvariable.h>

using namespace std;

/*******************************************************************************
* Superclass for all test classes in this regression test.
*******************************************************************************/
class TestClass : public Serializable 
{

public:
  TestClass() {}
  virtual ~TestClass() {}
  virtual void fillWithData() = 0;
  virtual void print(string & fname) = 0;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
  }
};

BOOST_CLASS_EXPORT(TestClass)

/*******************************************************************************
* Test class for several kinds of vector objects.
*******************************************************************************/
class ACGTest : public TestClass {
public:
  ACGTest() {}
  ~ACGTest() {}
  void fillWithData();
  void print(string & fname);
  
private:
  BIEACG *gen;
  Uniform *unif;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(TestClass);
    ar & BOOST_SERIALIZATION_NVP(gen);
    ar & BOOST_SERIALIZATION_NVP(unif);
  }
};
BOOST_CLASS_EXPORT(ACGTest)

void ACGTest::fillWithData()
{
  // Initialize the random number generator.
  //
  gen = new BIEACG(11+myid, 25);

  // Create a distribution
  //
  unif  = new Uniform(0, 1, gen);
}

void ACGTest::print(string & fname)
{
  for (int id=0; id<numprocs; id++) {
    if (myid==id) {
      ofstream out(fname.c_str(), ios::app);
      for (unsigned n=0; n<20; n++)
	out << setw(18) << (*unif)() << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
}


void fill_save_run_run(TestClass * tc, string statename)
{
  try {
    tc->fillWithData();
   
    TestUserState teststate(statename, 0, true);
    teststate.addObject(tc);
    {
      TestSaveManager testSaveManager(&teststate, BOOST_TEXT, BACKEND_FILE);
      testSaveManager.save();
    }
  
    TestUserState *restored;
    {
      TestLoadManager testLoadManager(&restored, 
				      teststate.getSession(), 
				      teststate.getVersion(), 
				      BOOST_TEXT, BACKEND_FILE);
      testLoadManager.load();
    }

    Serializable *loadedObj = *(restored->getTransClosure().objectlist.begin());
    TestClass* restored_tc = dynamic_cast<TestClass*>(loadedObj);

    string filename = statename + "_actual";
    tc->print(filename);

    filename = statename + "_restored";
    restored_tc->print(filename);
  }
  catch (BIEException & e)
    { cout << "Caught Exception on ID#" << myid << ": \n" 
	   << e.getErrorMessage() << endl; }
}

int main(int argc, char **argv)
{
  mpi_init(argc, argv);
  BIE::mpi_used = true;

  if (myid==0) system("rm -rf " PERSISTENCE_DIR);

  try {
    TestClass * tc;
    
    tc = new ACGTest();
    fill_save_run_run(tc, "testPersistence_BIEACG");
    cout << "Finished persistence test for BIEACG" << endl;

  }
  catch (BIEException & e)
  { cout << "Caught Exception: \n" << e.getErrorMessage() << endl; }

  MPI_Finalize();
}

