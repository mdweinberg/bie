// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <ACG.h>
#include <Normal.h>

string outname("gauss.data");

int 
main(int argc, char** argv)
{
  int M = 1;
  long iseed = 11;
  
  int c;
  while (1) {
    c = getopt (argc, argv, "M:i:f:");
    if (c == -1) break;
     
    switch (c)
      {
      case 'M': M = atoi(optarg); break;
      case 'f': outname = optarg; break;
      case 'i': iseed = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-f filename\t\toutput filename (default: " +outname+ ")\n";
	msg += "\t-M n\t\tnumber of components\n";
	msg += "\t-i x\t\tseed for random number generator\n";
	cerr << msg; exit(-1);
      }
  }

  ACG gen(iseed, 20);
  Normal unit(0.0, 1.0, &gen);

  ofstream out(outname.c_str());
  if (!out) {
    cerr << "Error opening <" + outname + ">\n";
    exit(-1);
  }

  vector<int> number;
  vector<double> xx, sx;
  double x, dx;
  int n, sum = 0;

				// Get parameters from user
  for (int m=0; m<M; m++) {
    cout << "Component " << m << ": n, x0, width_x? ";
    cin >> n;
    cin >> x;
    cin >> dx;

    number.push_back(n);
    sum += n;

    xx.push_back(x);
    sx.push_back(fabs(dx));
  }

				// Write data file


  out << "real \"x\"" << endl << endl;

  for (int m=0; m<M; m++) {

    for (int i=0; i<number[m]; i++)
    out 
      << setw(15) << xx[m] + sx[m]*unit()
      << endl;
  }

				// Write parameters
  
  out << "#     Parameters" << endl;
  for (int m=0; m<M; m++) {
    out << "#---- Component #" << m << endl;
    out << "#     N=" << number[m]  << endl;
    out << "#     X=" << xx[m]  << endl;
    out << "#    SX=" << sx[m]  << endl;
  }

}
