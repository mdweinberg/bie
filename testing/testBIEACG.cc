#include <cmath>

#include <config.h>
#if PERSISTENCE_ENABLED
#include <TestUserState.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#endif

#include <SquareTile.h>
#include <BIEACG.h>
#include <Uniform.h>
#include <gvariable.h>

using namespace BIE;

// BOOST_CLASS_EXPORT(SquareTile)

void printfile(ostream* out, Uniform* unif, unsigned num)
{
  for (unsigned n=0; n<num; n++)
    (*out) << setw(18) << (*unif)() << endl;
}


int main()
{
#if PERSISTENCE_ENABLED
  string adir("./pdir/testBIEACG");

  try
  {
    // Make the persistence directory something unique to us
    // to avoid interence with other tests.
    system("rm -rf ./pdir/testBIEACG");
  
    // Try a simple class
    //
    SquareTile *tile = new SquareTile(-1.0, 1.0, -1.0, 1.0);

    // Initialize the random number generator.
    //
    BIEgen = new BIEACG(11, 25);
    
    // Output name
    //
    string oname("testBIEACG_values.");

    // Create a distribution
    //
    Uniform * u  = new Uniform(0, 1, BIEgen);
    
    // Number of trials
    //
    const unsigned numtot = 20;

    // Fiducial output
    //
    string fname1 = oname + "before";
    ofstream out1(fname1.c_str());
    if (!out1) {
      cerr << "Error opening <" << fname1 << ">" << endl;
      exit(-1);
    }

    printfile(&out1, u, numtot);
    out1.close();

    delete BIEgen;
    BIEgen = new BIEACG(11, 25);

    delete u;
    u  = new Uniform(0, 1, BIEgen);
 
    // Test output
    //
    string fname2 = oname + "after";
    ofstream out2(fname2.c_str());
    if (!out2) {
      cerr << "Error opening <" << fname1 << ">" << endl;
      exit(-1);
    }

    printfile(&out2, u, numtot/2);

    try {
      TestUserState tus("testBIEACG", 0, true);

      tus.addObject(tile);
      tus.addObject(BIEgen);
      tus.addObject(u);

      TestSaveManager testSaveManager(&tus, BOOST_XML, BACKEND_FILE);

      testSaveManager.save();
    } catch (BoostSerializationException * e) {
      cout << "#######################################################" << endl;
      cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "#######################################################" << endl;
    }

    // Restore the State

    try {
      TestUserState *restored;
      TestLoadManager testLoadManager(&restored, "testBIEACG", 1,
				      BOOST_XML, BACKEND_FILE);

      testLoadManager.load();    

      vector<Serializable*> p = restored->getTransClosure().objectlist;

      tile =   static_cast<SquareTile*>(p[0]);
      BIEgen = static_cast<BIEACG*    >(p[1]);
      u =      static_cast<Uniform*   >(p[2]);
      
    } catch (BoostSerializationException * e) {
      cout << "#######################################################" << endl;
      cout << "##### BoostSerializationException thrown on LOAD! #####" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "#######################################################" << endl;
    }

    printfile(&out2, u, numtot - numtot/2);
  }
  catch (BIEException & e) {
    cerr << e.getErrorMessage() << endl; 
  }


#else

cout << "This test is only appropriate when persistence is enabled" << endl;

#endif  

 return 0;
}
