#include <string>
#include <vector>
#include <iomanip>
#include <sstream>
using namespace std;

#include <BasicType.h>
#include <TypedBuffer.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStream.h>
#include <RecordInputStream.h>
#include <RecordStream_Ascii.h>
#include <RecordStream_Binary.h>
#include <RecordOutputStream.h>
#include <RecordStreamFilter.h>
#include <BinaryFilters.h>
#include <UnaryFilters.h>
#include <SpecialFilters.h>
#include <SetFilters.h>
#include <BIEException.h>

using namespace BIE;

int main ()
{
  /*****************************************************************************
  * Filter test.
  *****************************************************************************/
  try 
  {
    RecordInputStream * risa1 = 
      new RecordInputStream_Ascii("test4.data");

    cout << risa1->eos() << "  " << risa1->error() << endl;
  
    RecordInputStream *risa2 = risa1->renameField(1, "x_orig");
    RecordInputStream *risa3 = risa2->renameField(2, "y_orig");

    RecordStreamFilter * filter = new RPhiFilter(risa3);
    filter->connect("x_orig", "x");
    filter->connect("y_orig", "y");

    cout << filter->toString() << endl;

    risa3 = risa3->filterWith(3, filter);
    RecordInputStream *risa4 = risa3->renameField("r", "x");
    RecordInputStream *risa5 = risa4->renameField("phi", "y");

    cout << "This filter is " << filter->toString() << endl;

    cout << risa5->getBuffer()->toString() << endl;

    cout << risa5->nextRecord() << endl;
    cout << risa5->getBuffer()->toString() << endl;
    cout << risa5->nextRecord() << endl;
    cout << risa5->getBuffer()->toString() << endl;
    cout << risa5->nextRecord() << endl;
    cout << risa5->getBuffer()->toString() << endl;
    
    cout << filter->toString();

    for (int i=0; i<1000; i++) {
      risa5->nextRecord();
      cout << setw(18) << risa5->getBuffer()->getRealValue("x")
	   << setw(18) << risa5->getBuffer()->getRealValue("y")
	   << endl;
    }
  }
  catch (BIEException e)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << e.getErrorMessage() << endl;
  }
  catch (...)
  {
    cerr << "&&&&&&&&&&&  UNEXPECTED EXCEPTION &&&&&&&&&&&&&" << endl;
    cerr << "not a bie exception" << endl;
  }
}
