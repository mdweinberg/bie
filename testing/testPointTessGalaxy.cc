#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>

using namespace std;

#include <MappedGrid.h>
#include <PointTessellation.h>
#include <DataTree.h>
#include <SquareTile.h>
#include <RecordStream_Ascii.h>
#include <Frontier.h>
#include <Node.h>

#if PERSISTENCE_ENABLED
#include <Serializable.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#endif


using namespace BIE;

void pointTessellationTests();
void printFrontier(vector<int>);
void commontests(Tessellation * tess);

bool verbose = false;

/******************************************************************************
 * This runs several tests on the Point tesselation using galaxy data
 ******************************************************************************/
int main()
{
  cout << "Starting up\n";
  pointTessellationTests();
}

bool list_compare(vector<int> l1, vector<int> l2)
{
  if (l1.size() != l2.size()) return false;

  sort(l1.begin(), l1.end());
  sort(l2.begin(), l2.end());

  bool ans = true;
  for (unsigned i=0; i<l1.size(); i++)
    if (l1[i] != l2[i]) ans = false;

  return ans;
}

void printDist(DataTree *dis)
{
  if (!verbose) return;

  Frontier *frontier = dis->GetDefaultFrontier();
  SampleDistribution *d;
  for (frontier->Reset(); !frontier->IsDone(); frontier->Next()) {
    d = dis->CurrentItem();
    cout << setw(5) << left << frontier->CurrentItem() << setw(60) 
	 << setfill('-') << '-' << setfill(' ') << right << endl;
    for (int i=0; i<d->numberData(); i++) {
      cout << "    " << setw(5) << i << setw(18) << d->getValue(i) << endl;
    }
  }
}

void pointTessellationTests()
{
  const size_t cwdsiz = 8192;
  char *cwdbuf = new char[cwdsiz];
  string iodata(getcwd(cwdbuf, cwdsiz));
  delete [] cwdbuf;
  
  iodata = string(iodata.begin(), iodata.begin() + iodata.find("testing"));
  iodata += "examples/Galaxy/data/galaxy.data.5r";

  RecordType *rt = new RecordType();
  rt = rt->insertField(1, "mag1", BasicType::Real);
  rt = rt->insertField(2, "mag2", BasicType::Real);

  PointDistribution *dist = new PointDistribution(rt);
  RecordInputStream_Ascii *ris1 = new RecordInputStream_Ascii(iodata);
  RecordInputStream_Ascii *ris2 = new RecordInputStream_Ascii(iodata);
  
  Tile *tile = new SquareTile();

  Tessellation *tess=0;

  cout << "About to construct PointTesselation\n";
  cout.flush();
  
  try {
    tess = new PointTessellation(tile, ris1, 
				 -3.14159, 3.14159, -1.5708, 1.5708);
  }
  catch (BIEException e) { 
    cout << e.getErrorMessage() << endl;
  }
  cout << "Finished construction!\n";
  cout.flush();

  DataTree *dis = new DataTree(ris2, dist, tess);

  Frontier *frontier = dis->GetDefaultFrontier();

  vector< vector<int> > down;

  bool success = true;

  frontier->RetractToTopLevel();
  cout << setw(60) << setfill('-') << '-' << endl;
  cout << "*** Number of tiles=" << frontier->Size() << endl;
  cout << setw(60) << setfill('-') << '-' << endl;
  printDist(dis);
  down.push_back(frontier->ExportFrontier());

  frontier->UpDownLevels(5);
  cout << setw(60) << setfill('-') << '-' << endl;
  cout << "*** Number of tiles=" << frontier->Size() << endl;
  cout << setw(60) << setfill('-') << '-' << endl;
  printDist(dis);
  cout << "Saving frontier . . ." << endl;
  down.push_back(frontier->ExportFrontier());

  for (int i=0; i<5; i++) {
    frontier->UpDownLevels(2);
    cout << setw(60) << setfill('-') << '-' << endl;
    cout << "*** Number of tiles=" << frontier->Size() << endl;
    cout << setw(60) << setfill('-') << '-' << endl;
    printDist(dis);
    cout << "Saving frontier . . ." << endl;
    down.push_back(frontier->ExportFrontier());
  }

  frontier->UpDownLevels(2);

  for (int i=0; i<6; i++) {
    frontier->UpDownLevels(-2);
    cout << setw(60) << setfill('-') << '-' << endl;
    cout << "*** Number of tiles=" << frontier->Size() << endl;
    cout << setw(60) << setfill('-') << '-' << endl;
    printDist(dis);
    if (list_compare(frontier->ExportFrontier(), down.back()))
      cout << "Agree with original!" << endl;
    else {
      cout << "MISMATCH with original" << endl;
      success = false;
    }
    down.pop_back();
  }

  frontier->UpDownLevels(-5);
  cout << setw(60) << setfill('-') << '-' << endl;
  cout << "*** Number of tiles=" << frontier->Size() << endl;
  cout << setw(60) << setfill('-') << '-' << endl;
  printDist(dis);
  if (list_compare(frontier->ExportFrontier(), down.back()))
    cout << "Agree with original!" << endl;
  else {
    cout << "MISMATCH with original" << endl;
    success = false;
  }

  cout << endl;
  cout << setw(60) << setfill('-') << '-' << endl;
  if (success) cout << "*** ALL TESTS PASSED ***" << endl;
  else cout << "*** TEST FAILUIRE ***" << endl;
  cout << setw(60) << setfill('-') << '-' << endl;
}
