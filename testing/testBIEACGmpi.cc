#include <BIEmpi.h>
#include <BIEACG.h>
#include <Uniform.h>
#include <gvariable.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cmath>

#if PERSISTENCE_ENABLED
#include <TestUserState.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#endif

using namespace BIE;

void printfile(string& fname, Uniform* unif, unsigned num)
{
  for (int id=0; id<numprocs; id++) {
    if (myid==id) {
     ofstream out(fname.c_str(), ios::app);
      for (unsigned n=0; n<num; n++)
	out << setw(18) << (*unif)() << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
}

int main(int argc, char **argv)
{
#if PERSISTENCE_ENABLED

  mpi_init(argc, argv);

  BIE::mpi_used = true;

  try
  {
    // Make the persistence directory something unique to us
    // to avoid interence with other tests.
    //
    if (myid==0) system("rm -rf ./pdir/testBIEACGmpi");
  
    // Initialize the random number generator.
    //
    BIEgen = new BIEACG(11+myid, 25);
    
    // Output name
    //
    string oname("testBIEACGmpi_values.");

    // Create a distribution
    //
    Uniform * u  = new Uniform(0, 1, BIEgen);
    
    // Number of trials
    //
    const unsigned numtot = 20;

    // Fiducial output
    //
    string fname1 = oname + "before";
    if (myid==0) {
      ostringstream ostr;
      ostr << "rm -rf " << fname1;
      system(ostr.str().c_str());
    }

    // Print the two pieces without a break
    //
    printfile(fname1, u, numtot/2);
    printfile(fname1, u, numtot-numtot/2);

    // New remake the RNGs and print the same
    // two pieces with a serialization in between

    delete BIEgen;
    BIEgen = new BIEACG(11+myid, 25);

    delete u;
    u  = new Uniform(0, 1, BIEgen);
 
    // Test output
    //
    string fname2 = oname + "after";
    if (myid==0) {
      ostringstream ostr;
      ostr << "rm -rf " << fname2;
      system(ostr.str().c_str());
    }

    printfile(fname2, u, numtot/2);

    TestUserState saveState("testBIEACG", 0, true);
    saveState.addObject(BIEgen);
    saveState.addObject(u);

    try {
      TestSaveManager testSaveManager(&saveState, BOOST_BINARY, BACKEND_FILE);   
      testSaveManager.save();
    } catch (BoostSerializationException * e) {
      cout << "ON SAVE, #" << myid << endl;
      cout << "####### BoostSerializationException thrown! ########" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "####################################################" << endl;
      throw e;
    } catch (ArchiveException * e) {
      cout << e->getErrorMessage() << endl;
      throw e;
    }

    MPI_Barrier(MPI_COMM_WORLD);
    
    // Restore the State
    TestUserState *restored;

    try {
      TestLoadManager testLoadManager(&restored, "testBIEACG", 1, 
				      BOOST_BINARY, BACKEND_FILE);
      testLoadManager.load();
    } catch (BoostSerializationException * e) {
      cout << "ON LOAD, #" << myid << endl;
      cout << "####### BoostSerializationException thrown! ########" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "####################################################" << endl;
      throw e;
    } catch (ArchiveException * e) {
      cout << e->getErrorMessage() << endl;
      throw e;
    }

    
    vector<Serializable*> objectlist = restored->getTransClosure().objectlist;

    BIEgen = dynamic_cast<BIEACG*>(objectlist[0]);
    u = dynamic_cast<Uniform*>(objectlist[1]);

    printfile(fname2, u, numtot - numtot/2);
  }
  catch (BIEException e)
    { cerr << e.getErrorMessage() << endl; }

  MPI_Finalize();

#else

cout << "This test is only appropriate when persistence is enabled" << endl;

#endif  
}
