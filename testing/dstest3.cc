#include <RecordStream_Binary.h>
#include <RecordStream_Ascii.h>
#include <RecordStream_NetCDF.h>
#include <BasicType.h>
#include <RecordType.h>
#include <BIEException.h>
#include <stdlib.h>

using namespace BIE;

void pushoutputwithstrings(RecordOutputStream * ros);
void pushoutputnostrings(RecordOutputStream * ros);
void inputtest(RecordInputStream * ris);

int main()
{
  try {
    system("rm -f dstest3_binary.dat");
    system("rm -f dstest3_ascii.dat");
    system("rm -f dstest3_netcdf.dat");
    
    RecordType * rt = new RecordType();
    rt = rt->insertField(1, "a", BasicType::String);
    rt = rt->insertField(2, "b", BasicType::Real);
    rt = rt->insertField(3, "c", BasicType::Int);
    rt = rt->insertField(4, "d", BasicType::Bool);  
    
    RecordOutputStream * ros  = new RecordOutputStream(rt);
  
    RecordOutputStream * rosb = new RecordOutputStream_Binary(ros, "dstest3_binary.dat", true);
    RecordOutputStream * rosa = new RecordOutputStream_Ascii(ros, "dstest3_ascii.dat", true);
  
    pushoutputwithstrings(ros);
  
    cout << "Pushed Values\n";
    ros->close();  
    delete rosa;
    delete rosb;
  
    cout << "Deleted output streams" << endl;
  
    cout << endl << "=============== BINARY INPUT STREAM TESTS ======" << endl;
    RecordInputStream *  risb  = new RecordInputStream_Binary("dstest3_binary.dat");
    inputtest(risb);
  
    cout << endl << "=============== ASCII INPUT STREAM TESTS ======" << endl;
    RecordInputStream *  risa  = new RecordInputStream_Ascii("dstest3_ascii.dat");
    inputtest(risa);
  
    cout << endl << "=============== NETCDF INPUT STREAM TESTS ======" << endl;

    rt = rt->deleteField(1);
    ros  = new RecordOutputStream(rt);
    RecordOutputStream * rosn = new RecordOutputStream_NetCDF(ros, "dstest3_netcdf.dat");
    pushoutputnostrings(ros);

    delete rosn;
    RecordInputStream *  risn  = new RecordInputStream_NetCDF("dstest3_netcdf.dat");
    inputtest(risn);
  
    cout << endl << "=============== DONE ======" << endl;
  }
  catch (BIEException e)
  { cerr << e.getErrorMessage() << endl; }

}

void pushoutputwithstrings(RecordOutputStream * ros)
{
  ros->setStringValue(1, "Old Ranfurly GC");
  ros->setRealValue(2, 66.9);
  ros->setIntValue(3, 75);
  ros->setBoolValue(4, true);

  ros->pushRecord();

  ros->setStringValue(1, "\n\n\n\t\\");
  ros->setRealValue(2, 3e+4);
  ros->setIntValue(3, -10101010);
  ros->setBoolValue(4, false);

  ros->pushRecord();

  ros->setStringValue(1, "");
  ros->setRealValue(2, 3.46478e-4);
  ros->setIntValue(3, +30984);
  ros->setBoolValue(4, false);

  ros->pushRecord();

  ros->setStringValue(1, "Old Ranfurly GC");
  ros->setRealValue(2, 66.9);
  ros->setIntValue(3, 75);
  ros->setBoolValue(4, true);

  ros->pushRecord();
  ros->pushRecord();

  cout << "Pushed Values\n";
  ros->close();  
}

void pushoutputnostrings(RecordOutputStream * ros)
{
  ros->setRealValue(1, 66.9);
  ros->setIntValue(2, 75);
  ros->setBoolValue(3, true);

  ros->pushRecord();

  ros->setRealValue(1, 3e+4);
  ros->setIntValue(2, -10101010);
  ros->setBoolValue(3, false);

  ros->pushRecord();

  ros->setRealValue(1, 3.46478e-4);
  ros->setIntValue(2, +30984);
  ros->setBoolValue(3, false);

  ros->pushRecord();

  ros->setRealValue(1, 66.9);
  ros->setIntValue(2, 75);
  ros->setBoolValue(3, true);

  ros->pushRecord();
  ros->pushRecord();

  cout << "Pushed Values\n";
  ros->close();  
}

void inputtest(RecordInputStream * ris)
{
  cout << ris->getBuffer()->getType()->toString() << endl;
  cout << ris->nextRecord() << endl;
  cout << ris->getBuffer()->toString();
  cout << ris->nextRecord() << endl;
  cout << ris->getBuffer()->toString();
  cout << ris->nextRecord() << endl;
  cout << ris->getBuffer()->toString();
  cout << ris->nextRecord() << endl;
  cout << ris->getBuffer()->toString();
  cout << ris->nextRecord() << endl;
  cout << ris->getBuffer()->toString();
  cout << ris->nextRecord() << endl;
  cout << ris->getBuffer()->toString();
  cout << ris->nextRecord() << endl;
  cout << ris->getBuffer()->toString();
}
