#include <unistd.h>
#include <getopt.h>

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

#include <BIEconfig.h>
#include <HistogramND.h>
#include <MultiDistribution.h>
#include <RecordStream_Ascii.h>
#include <BasicType.h>

#include <ACG.h>
#include <Uniform.h>
#include <vector>
#include <string>

using namespace BIE;

/**
   @name testmultidist: main
   Tests MultiDistribution implementation by reading in data 
   and exercising various methods.
*/
int
main(int argc, char **argv)
{
  int iseed=11;
  int nsample=200;
  int c;

  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
      {
	{"number", 1, 0, 0},
	{"seed", 1, 0, 0},
	{0, 0, 0, 0}
      };

      c = getopt_long (argc, argv, "n:s:",
		       long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 0:
	  printf ("option %s", long_options[option_index].name);
	  if (optarg)
	    printf (" with arg %s", optarg);
	  printf ("\n");
	  break;
	  

	case 'n':
	  nsample = atoi(optarg);
	  break;

	case 's':
	  iseed = atoi(optarg);
	  break;

	case '?':
	  break;

	default:
	  printf ("?? getopt returned character code 0%o ??\n", c);
	}
    }

  if (optind < argc)
    {
      printf ("non-option ARGV-elements: ");
      while (optind < argc)
	printf ("%s ", argv[optind++]);
      printf ("\n");
    }
  

  // Construct some histograms

  clivectord blo(2, 0.0);
  clivectord bhi(2, 20.0);
  clivectord bwid(2, 0.5);
  clivectors names;
  names().push_back("namedattribute");
  names().push_back("namedattribute2");

  HistogramND hist22(&blo, &bhi, &bwid, &names);

  bwid()[1] = 0.1;

  HistogramND hist11(&blo, &bhi, &bwid, &names);

  bwid()[0] = 0.2;
  bwid()[1] = 0.2;

  HistogramND hist21(&blo, &bhi, &bwid, &names);


  BinnedDistribution **dlist = new BinnedDistribution* [3];
  dlist[0] = &hist11;
  dlist[1] = &hist21;
  dlist[2] = &hist22;

  MultiDistribution mlist(3, dlist);

  // Now add some data

  vector<double> vlo;
  vector<double> vhi;

  vlo.push_back(3.0);
  vlo.push_back(10.0);
  vhi.push_back(4.0);
  vhi.push_back(11.0);

  ACG gen(iseed);
  Uniform ival1(vlo[0], vhi[0], &gen);
  Uniform ival2(vlo[1], vhi[1], &gen);

  RecordBuffer * buffer = new RecordBuffer();
  
  buffer = buffer->insertField(1, "x", BasicType::Real);
  buffer = buffer->insertField(2, "y", BasicType::Real);
  buffer = buffer->insertField(3, "namedattribute", BasicType::Real);
  buffer = buffer->insertField(4, "namedattribute2", BasicType::Real);
  
  for (int i=0; i<nsample; i++) {
    
    buffer->setRealValue(1, ival1());
    buffer->setRealValue(2, ival2());
    buffer->setRealValue(3, ival1());
    buffer->setRealValue(4, ival2());
    
    mlist.AccumData(1.0, buffer);
  }


  mlist.ComputeDistribution();

  // Now run some sample functions

  vector<double> mean0;
  for (int i=0; i<3; i++) {
    mean0.push_back(0.5*(vlo[0]+vhi[0]));
    mean0.push_back(0.5*(vlo[1]+vhi[1]));
  }

  vector<double> var0;
  for (int i=0; i<3; i++) {
    var0.push_back((pow(vhi[0],3.0) - pow(vlo[0],3.0))/(3.0*(vhi[0]-vlo[0]))
		   - mean0[0]*mean0[0]);
    var0.push_back((pow(vhi[1],3.0) - pow(vlo[1],3.0))/(3.0*(vhi[1]-vlo[1]))
		   - mean0[1]*mean0[1]);
  }

  cout.fill(' ');
  cout << "Mean:\n";
  cout << setw(5) << "Index" << "    " 
       << setw(15) <<  "Data" << "  " << setw(15) << "Expected" << "\n";
  cout.fill('-');
  cout << setw(5) << "|" << "    " 
       << setw(15) <<  "|" << "  " << setw(15) << "|" << "\n";
  cout.fill(' ');
  for (int j=0; j<(int)mlist.Mean().size(); j++) {
    cout << setw(5) << j+1 << "  ";
    cout << "  " << setw(15) << mlist.Mean()[j];
    cout << "  " << setw(15) << mean0[j] << endl;
  }
    
  cout << endl;
  cout << "StdDev:\n";
  cout << setw(5) << "Index" << "    " 
       << setw(15) <<  "Data" << "  " << setw(15) << "Expected" << "\n";
  cout.fill('-');
  cout << setw(5) << "|" << "    " 
       << setw(15) <<  "|" << "  " << setw(15) << "|" << "\n";
  cout.fill(' ');
  for (int j=0; j<(int)mlist.StdDev().size(); j++) {
    cout << setw(5) << j+1 << "  ";
    cout << "  " << setw(15) << mlist.StdDev()[j];
    cout << "  " << setw(15) << sqrt(var0[j]) << endl;
  }

  hist22.dumpraw("test.raw");

  return 0;
}



