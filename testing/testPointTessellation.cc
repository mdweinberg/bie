#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <unordered_map>

using namespace std;

#include <MappedGrid.h>
#include <KdTessellation.h>
#include <PointTessellation.h>
#include <QuadGrid.h>
#include <ContainerTessellation.h>
#include <BinaryTessellation.h>
#include <SquareTile.h>
#include <Histogram1D.h>
#include <DataTree.h>
#include <DummyModel.h>
#include <vector>
#include <RecordStream_Ascii.h>
#include <Frontier.h>
#include <FrontierExpansionHeuristic.h>
#include <Node.h>

#if PERSISTENCE_ENABLED
#include <Serializable.h>
#include <TestUserState.h>
#include <PersistenceControl.h>
#include <StateTransClosure_Test.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>
#endif


using namespace BIE;

void pointTessellationTests();
void printTree(DataTree * dist);
void printFrontier(vector<int>, Tessellation * tess=0);
void commonTests(Tessellation * tess);

/******************************************************************************
 * This runs several tests on the Point tesselation
 ******************************************************************************/
int main()
{
  cout << "Starting up" << endl;
  pointTessellationTests();
}

void pointTessellationTests()
{
  RecordInputStream * inputstream = new RecordInputStream_Ascii("7pts.dat");
  SquareTile * tile = new SquareTile();
  PointTessellation * ptTess=0;
  
  cout << "About to construct PointTesselation" << endl;
  cout.flush();
  
  try { ptTess = new PointTessellation(tile, inputstream); }
  catch (BIEException e) { cout << e.getErrorMessage() << endl;}
  
  
  cout << "Finished construction!" << endl;
  cout.flush();
  
  ptTess->PrintPreOrder(cout);
  
  
  vector<int> found;
  
  ptTess->FindAll(1.1, 1.2, found); 
  cout << "FindAll (1.1, 1.2) size :  " << found.size() << endl;
  
  ptTess->FindAll(4.1, 4.0, found); 
  cout << "FindAll (4.1, 4.0) size :  " << found.size() << endl;
  
  ptTess->FindAll(4.1, 4.2, found); 
  cout << "FindAll (4.1, 4.2) size :  " << found.size() << endl;
  
  ptTess->FindAll(7.1, 7.2, found); 
  cout << "FindAll (7.1, 7.2) size :  " << found.size() << endl;
  
  // aah put in test for other constructor
  
  
  /****************************************************************************
   * Selection tests
   ****************************************************************************/
  
  
  RecordInputStream * ris = new RecordInputStream_Ascii("7pts.dat");
  Histogram1D * hist      = new Histogram1D(0, 20, 20, "namedattribute");
  
  DataTree * dist = new DataTree(ris, hist, ptTess);
  
  cout << "\nTotal points in distribution = " << dist->Total() << "\n";
  cout << "Points off grid - not covered by tessellation = " << dist->Offgrid() << "\n";
  
  printTree(dist);
  
  Frontier * frontier = dist->GetDefaultFrontier();
  
  cout << "---Selecting 1,2,3,4,5,6,7,8,9 in frontier" << endl;
  vector<int> afewnodes;
  for(int i = 0; i <= 6; i++)
    { afewnodes.push_back(i); }
  frontier->Set(afewnodes);
  printFrontier(frontier->ExportFrontier(), ptTess);
  
  afewnodes.clear();
  
  for (int i = 1; i <= 5; i++)
    {
      cout << "\nPoints per tile : " << endl;
      for (dist->Reset(); ! dist->IsDone(); dist->Next())
	{
	  cout << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	  cout << dist->CurrentItem()->getDataSetSize() << "), ";
	}
      
      frontier->UpDownLevels(-1);
      cout << "\nAfter contracting with UpDownLevels(-1)" << i << " times." << endl;
      printFrontier(frontier->ExportFrontier(), ptTess); 
    }
  
  
  
  
  /****************************************************************************
   * Common tests.
   ****************************************************************************/
  inputstream = new RecordInputStream_Ascii("data.dat");
  ptTess = new PointTessellation(tile, inputstream, -10, 10, -10, 10);
  commonTests(ptTess);
  
  /****************************************************************************
   * Destructor tests
   ****************************************************************************/
  delete ptTess;
  
}

void printTree(DataTree * dist) 
{
  Frontier * frontier = dist->GetDefaultFrontier();
  frontier->RetractToTopLevel();
  vector<int> f = frontier->ExportFrontier();
  unsigned last=0, cur=f.size(), level=0;
  Tessellation *tess = dist->GetTessellation();

  while (last!=cur) {
    cout << "---- Level " << level++ << endl;
    printFrontier(f, tess);
    frontier->UpDownLevels(1);
    f = frontier->ExportFrontier();
    last = cur;
    cur = f.size();
  }

  cout << "---- Done!" << endl << endl;
}

void printFrontier(vector<int> frontier_value, Tessellation * tess) 
{
  vector <int>::iterator beg = frontier_value.begin();
  vector <int>::iterator end = frontier_value.end();
  vector <int>::iterator it;
  double x, y, dummy;
  
  cout << "---- new frontier is: ("; 
  if (!frontier_value.empty()) {
    for (it=beg; it!=end; it++) {
      if (tess) {
	if (it==beg) cout << endl;
	tess->GetTile(*it)->corners(x, dummy, y, dummy);
	cout << setw(8) << (*it) 
	     << "  ["  << setw(8) << right << x
	     << ", " << setw(8) << right << y << "]"
	     << endl;
      } else {
	if (it!=beg) cout << ", ";
	cout << (*it);
      }
    }
  }
  cout << ")" << endl;
}

/******************************************************************************
 * Tests common to all tessellations.
 ******************************************************************************/
void commonTests(Tessellation * tess)
{
  try {
    cout << "\nCreated tessellation, starting common tests\n\n";
    
    /**************************************************************************
     * Print the entire tessellation tree.
     **************************************************************************/
    tess->PrintPreOrder(cout);
    
    Frontier * frontier = new Frontier(tess);
    printFrontier(frontier->ExportFrontier(), tess);
    
    /**************************************************************************
     * Level population
     **************************************************************************/
    {
      frontier->RetractToTopLevel();
      unsigned last=1, cur=0, lev=0;
      while (1) {
	last = cur;
	cur  = frontier->Size();
	if (last == cur) break;
	cout << "--Level " << setw(3) << lev << ": " << cur << endl;
	frontier->UpDownLevels(1);
	lev++;
      }
    }

    /**************************************************************************
     * Mutation tests
     **************************************************************************/
    cout << "\n\n--Contracting one level. " << endl;
    frontier->UpDownLevels(-1);
    printFrontier(frontier->ExportFrontier(), tess);
    
    cout << "\n\n---Expanding one level. " << endl;
    frontier->UpDownLevels(1);
    printFrontier(frontier->ExportFrontier(), tess);
    
    cout << "\n\n---Expanding one level. " << endl;
    frontier->UpDownLevels(1);
    printFrontier(frontier->ExportFrontier(), tess);
    
    /**************************************************************************
     * RetractToTopLevel tests.
     **************************************************************************/
    cout << " Retract to top level " << endl;
    frontier->RetractToTopLevel();
    printFrontier(frontier->ExportFrontier(), tess);
    
    /**************************************************************************
     * Increase resolution tests.
     **************************************************************************/
    cout << "\n\n---Increasing resolution by 2 levels (always increase heuristic)" << endl;
    AlwaysIncreaseResolution * heuristic = new AlwaysIncreaseResolution();
    
    frontier->IncreaseResolution(heuristic, 2);
    printFrontier(frontier->ExportFrontier(), tess);
    
    /**************************************************************************
     * Selection tests
     **************************************************************************/
    cout << "---Selecting 1,2,3,4,5,6,7,8,9 in frontier" << endl;
    vector<int> afewnodes;
    for(int i = 1; i <= 9; i++)
      { afewnodes.push_back(i); }
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier());
    
    afewnodes.clear();
    
    cout << "---Selecting 10,14,18,22 in frontier" << endl;
    afewnodes.push_back(10);
    afewnodes.push_back(14);
    afewnodes.push_back(18);
    afewnodes.push_back(22);
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier());
    
    afewnodes.clear();
    cout << "---Trying to select -1, 100000, 2,3,4 as the frontier" << endl;
    afewnodes.push_back(-1);
    afewnodes.push_back(100000);
    afewnodes.push_back(2);
    afewnodes.push_back(3);
    afewnodes.push_back(4);
    
    frontier->Set(afewnodes);
    printFrontier(frontier->ExportFrontier());
    
    /**************************************************************************
     * FindInFrontier tests.
     **************************************************************************/
    cout << "Found (0.0, 0.0) ? " << (bool) frontier->Find(0,0) << endl;
    cout << "Found (-3, -0.9) ? " << (bool) frontier->Find(-3,-0.9) << endl;
    cout << "Found (10, 9) ? " << (bool) frontier->Find(10,9) << endl;
    
    /**************************************************************************
     * FindAll tests.
     **************************************************************************/
    vector<int> found;
    
    tess->FindAll(0,0, found); 
    cout << "FindAll (0,0) size :  " << found.size() << endl;
    tess->FindAll(-3,-0.9, found); 
    cout << "FindAll (-3,-0.9) size:  " << found.size() << endl;
    tess->FindAll(10,9, found); 
    cout << "FindAll (10,9) size: " << found.size() << endl;
    
    /**************************************************************************
     * Get Tile tests
     **************************************************************************/
    cout << "Printing tiles 0, 1 " << endl;
    tess->GetTile(0)->printTile(cout);
    tess->GetTile(1)->printTile(cout);
    
    cout << "GetTile(-1):   " << tess->GetTile(-1) << endl;
    
    /**************************************************************************
     * Is valid tests.
     **************************************************************************/
    cout << "Is valid ID : -1 : " << tess->IsValidTileID(-1) << endl;
    cout << "Is valid ID : 0 : " << tess->IsValidTileID(0) << endl;
    cout << "Is valid ID : 99 : " << tess->IsValidTileID(99) << endl;
    cout << "Is valid ID : 100 : " << tess->IsValidTileID(100) << endl;
    cout << "Is valid ID : 10000 : " << tess->IsValidTileID(10000) << endl;
    cout << "Is valid ID : 100000 : " << tess->IsValidTileID(100000) << endl;
    
    /**************************************************************************
     * Number tiles test.
     **************************************************************************/
    cout << "Number of tiles: " << tess->NumberTiles() << endl;
    
    /**************************************************************************
     * GetRootTiles test
     **************************************************************************/
    vector<Node*> rootnodes = tess->GetRootNodes();
    
    cout << "Printing root nodes: " << endl;
    for (int i = 0; i < (int) rootnodes.size(); i++)
      { cout << rootnodes[i]->ID() << ", "; }
    cout << endl;
    
    /**************************************************************************
     * GetRootNodes test
     **************************************************************************/
    cout << "Printing root tiles:" << endl;
    vector<int> roottiles = tess->GetRootTiles();
    
    for (int i = 0; i < (int) roottiles.size(); i++)
      { cout << roottiles[i] << ","; } 
    cout << endl;
    
    /**************************************************************************
     * Loop method tests.
     **************************************************************************/
    frontier->Reset();
    cout << "Looping through all tiles in frontier" << endl;
    while(! (frontier->IsDone()))
      {
	cout << "ID: " << frontier->CurrentItem() << "  ";
	frontier->Next();
      }
    cout << "First and last in frontier: " << endl;
    cout << "first " << frontier->First() << endl;
    cout << "last " << frontier->Last() << endl;
    
    /**************************************************************************
     * Some additional mutation tests.
     **************************************************************************/
    frontier->RetractToTopLevel();

    cout << "\nSome additional mutation tests\n";
    
    for (int i = 1; i <= 10; i++)
      {
	frontier->UpDownLevels(1);
	cout << "\nAfter expanding " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier());  
      }
    
    for (int i = 1; i <= 10; i++)
      {
	frontier->UpDownLevels(-1);
	cout << "\nAfter contracting " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier());  
      }
    
    /**************************************************************************
     * Heuristic tests.
     **************************************************************************/
    RecordInputStream * ris = new RecordInputStream_Ascii("data.dat");
    Histogram1D * hist      = new Histogram1D(0, 20, 20, "mag1");
    
    DataTree * dist = new DataTree(ris, hist, tess);
    
    cout << "\nTotal points in distribution = " << dist->Total() << "\n";
    cout << "Points off grid - not covered by tessellation = " << dist->Offgrid() << "\n";
    
    FrontierExpansionHeuristic * h = new DataPointCountHeuristic(dist,50);
    frontier = dist->GetDefaultFrontier();
    
    for (int i = 1; i <= 10; i++)
      {
	cout << "\nPoints per tile : " << endl;
	for (dist->Reset(); ! dist->IsDone(); dist->Next())
	  {
	    cout << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	    cout << dist->CurrentItem()->getDataSetSize() << "), ";
	  }
	
	frontier->IncreaseResolution(h);
	cout << "\nAfter expanding with  DataPointCountHeuristic " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier()); 
      }
    
    h = new KSDistanceHeuristic(dist,0.1);
    frontier->RetractToTopLevel();
    
    for (int i = 1; i <= 10; i++)
      {
	cout << "\nPoints per tile : " << endl;
	for (dist->Reset(); ! dist->IsDone(); dist->Next())
	  {
	    cout << "(" << dist->CurrentTile()->GetNode()->ID() << "=";
	    cout << dist->CurrentItem()->getDataSetSize() << "), ";
	  }
	
	frontier->IncreaseResolution(h);
	cout << "\nAfter expanding with KSDistanceHeuristic " << i << " times." << endl;
	printFrontier(frontier->ExportFrontier()); 
      }
    
    cout << "Finished COMMON tests\n";
  }
  catch (BIEException e)
    { cerr << e.getErrorMessage() << endl; }



  /****************************************************************************
   * Persistence tests.
   ****************************************************************************/

#if PERSISTENCE_ENABLED

  string statename("testPointTessellation");

  try {
    TestUserState teststate(statename, 0, true);
    teststate.addObject(tess);
    {
      TestSaveManager testSaveManager(&teststate, BOOST_TEXT, BACKEND_FILE);
      testSaveManager.save();
      cout << "Tessellation persisted: number=" << tess->NumberTiles() << endl;
    }
  
    TestUserState *restored;
    {
      TestLoadManager testLoadManager(&restored, 
				      teststate.getSession(), 
				      teststate.getVersion(), 
				      BOOST_TEXT, BACKEND_FILE);
      testLoadManager.load();
    }


    Serializable *loadedObj = *(restored->getTransClosure().objectlist.begin());
    Tessellation* restored_tess = dynamic_cast<Tessellation*>(loadedObj);

    cout << "Tessellation restored:  number=" 
	 << restored_tess->NumberTiles() << endl;

  }
  catch (BIEException & e){
    cout << "Caught Exception: \n" << e.getErrorMessage() << endl; 
  }

#endif

}
