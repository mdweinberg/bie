/**
   For testing EnsembleDisc . . .
*/

#include <iostream>
#include <iomanip>
#include <typeinfo>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>

#include <boost/regex.hpp>

#define IS_MAIN

#include <BIEconfig.h>
#include <EnsembleDisc.h>
#include <ACG.h>
#include <DiscUnif.h>

#include <DummyModel.h>

using namespace BIE;

string priorfile("prior.dat");
string datafile("data.dat");

struct Record {
  int level, iter;
  double prob, like, prior;
  int M;
  vector<double> wght;
  vector<double> parm;
} record;

vector< vector<Record> > levels;
vector< int > levels_mix;
vector< int > levels_dim;

/**
   Test routine for data accumulation
*/
int
main(int argc, char** argv)
{
  unsigned nbeg = 0;		// First burnt-in state
  unsigned nlev = 0;		// Desired level
  string infile = "data.dat";	// Input data file
				// ------------------------------------
				// All the rest for a probability slice
				// ------------------------------------
  unsigned ix = 1;		// Which x parameter
  unsigned jy = 2;		// Which y parameter
  unsigned ic = 0;		// Which component
  double x = 0.0;		// Minimum x value
  double X = 1.0;		// Maximum x value
  double y = 0.0;		// Minimum y value
  double Y = 1.0;		// Maximum y value
  unsigned nimage = 0;		// Number of grid points per dimension
  unsigned sample = 0;		// Sample points from input file
  bool scaled = true;		// Scaled distance for state separation

  while (1) {
    int c = getopt(argc, argv, "f:n:l:i:j:k:x:X:y:Y:M:S:s:h");
    if (c == -1) break;

    switch (c) {
    case 'f':
      infile = string(optarg);
      break;
    case 'n':
      nbeg = atoi(optarg);
      break;
    case 'l':
      nlev = atoi(optarg);
      break;
    case 'i':
      ix = atoi(optarg);
      break;
    case 'j':
      jy = atoi(optarg);
      break;
    case 'k':
      ic = atoi(optarg);
      break;
    case 'x':
      x = atof(optarg);
      break;
    case 'X':
      X = atof(optarg);
      break;
    case 'y':
      y = atof(optarg);
      break;
    case 'Y':
      Y = atof(optarg);
      break;
    case 'M':
      nimage = atoi(optarg);
      break;
    case 'S':
      scaled = atoi(optarg) ? true : false;
      break;
    case 's':
      sample = atoi(optarg);
      break;
    case 'h':
      cerr << "Usage: " << argv[0] << " [arguments]" << endl;
      return 0;
    }
  }

  // ==================================================
  // Parse BIE posterior state file
  // ==================================================

  ifstream fin(infile.c_str());
  if (!fin) {
    cerr << "Error opening <" << infile << ">" << endl;
    exit(-1);
  }

  const int linelen = 4096;
  char linebuf[linelen];

  fin.getline(linebuf, linelen);
  string s(linebuf);

  const boost::regex re("\"[a-zA-Z 0-9_()]*\"");
  boost::smatch what; 
  boost::sregex_iterator it(s.begin(), s.end(), re), end;
  vector<string> labels;
  for (; it!=end; it++) 
    {
      string t = (*it)[0];
      labels.push_back(t.substr(1, t.size()-2));
    }

  cout << "# labels: " << labels.size() << endl;

  // ==================================================
  // Parse rest of file
  // ==================================================

  vector<Record> curlev;
  unsigned nmix=0, ndim=0;
  int lastl = -1;

  while (fin) {
    fin.getline(linebuf, linelen);
    istringstream sin(linebuf);

    sin >> record.level;
    sin >> record.iter;
    sin >> record.prob;
    sin >> record.like;
    sin >> record.prior;
    sin >> record.M;

				// Only on first time . . .
    if (lastl==-1) lastl = record.level;

				// Compute the various ranks and initialize
				// vectors
    nmix = record.M;
    ndim = (labels.size()-6-nmix)/nmix;
    record.wght = vector<double>(nmix);
    record.parm = vector<double>(nmix*ndim);

				// Parse the input line
    for (unsigned i=0; i<nmix; i++) 
      sin >> record.wght[i];
    for (unsigned i=0; i<ndim*nmix; i++) 
      sin >> record.parm[i];

				// Next level?
    if (lastl != record.level) {
      levels.push_back(curlev);
      levels_mix.push_back(nmix);
      levels_dim.push_back(ndim);
      curlev.erase(curlev.begin(), curlev.end());
    }
				// Save the completed record
    curlev.push_back(record);
    lastl = record.level;
  }
				// The remaining data is the final level
  if (curlev.size()) {
    levels.push_back(curlev);
    levels_mix.push_back(nmix);
    levels_dim.push_back(ndim);
  }

  // ==================================================
  // Stats and sanity check
  // ==================================================

  cout << setw(8) << "Levels" << setw(8) << "Counts" << endl;
  cout << setw(8) << "------" << setw(8) << "------" << endl;
  for (unsigned i=0; i<levels.size(); i++)
    cout << setw(8) << i << setw(8) << levels[i].size() << endl;

  if (nlev >= levels.size() || nlev<0) {
    ostringstream msg;
    msg << "Request level <" << nlev << "> does not exist!" << endl;
    exit(-1);
  }

  if (nbeg >= levels[nlev].size() || nbeg<0) {
    ostringstream msg;
    msg << "Request start point  <" << nbeg << "> is out of bounds!" << endl;
    exit(-1);
  }

  nmix = levels_mix[nlev];
  ndim = levels_dim[nlev];

  StateInfo *si = new StateInfo(nmix, ndim);

  EnsembleDisc::scaled = scaled;
  EnsembleDisc *sstat = new EnsembleDisc(si);

  cout << "Class: " << typeid(*sstat).name() << endl;

  // ==================================================
  // MAIN LOOP
  // ==================================================
  
  State state(si);

  vector<double> dv(3, 1.0);
  for (unsigned i=nbeg; i<levels[nlev].size(); i++) {

    for (unsigned j=0; j<nmix; j++) 
      state[j] = levels[nlev][i].wght[j];
    for (unsigned j=0; j<nmix*ndim; j++) 
      state[j+nmix] = levels[nlev][i].parm[j];
    
    sstat->AccumData(dv, state);
  }

  sstat->ComputeDistribution();
  sstat->PrintDiag(cout);

  if (nimage>0) {

    ofstream ogpl("data.gpl");

    double dx = (X - x)/(nimage-1);
    double dy = (Y - y)/(nimage-1);
    vector<double> pt((ndim+1)*nmix), ptdef((ndim+1)*nmix);
    State st;

    double sum = 0.0;

    cout << "Weights: " << endl;
    for (unsigned i=0; i<nmix; i++) {
      cout << i << "? ";
      cin >> ptdef[i];
      sum += ptdef[i];
    }
    for (unsigned i=0; i<nmix; i++) ptdef[i] /= sum;

    cout << "Parameters: " << endl;
    for (unsigned i=0; i<nmix; i++) {
      cout << "Component #" << i << endl;
      vector<double> pp;
      for (unsigned j=0; j<ndim; j++) {
	cout << j << "? ";
	cin >> ptdef[nmix+i*ndim+j];
      }
    }

    for (unsigned j=0; j<nimage; j++) {

      for (unsigned i=0; i<nimage; i++) {
	
	pt = ptdef;
	pt[nmix+ndim*ic+ix] = x + dx*i;
	pt[nmix+ndim*ic+jy] = y + dy*j;

	st = pt;
	double val = sstat->logPDF(st);

	ogpl << setw(18) << pt[nmix+ndim*ic+ix]
	     << setw(18) << pt[nmix+ndim*ic+jy]
	     << setw(18) << val
	     << setw(18) << exp(val)
	     << endl;
      }
      ogpl << endl;
    }
  } else if (sample) {

    ofstream ogpl("data.out");

    ACG gen(11, 20);
    DiscreteUniform udisc(nbeg, levels[nlev].size()-1, &gen);

    int dim = si->Ndim;
    State st(si);

    for (unsigned i=0; i<sample; i++) {

      unsigned indx = (unsigned)floor(udisc());

      for (unsigned j=0; j<nmix; j++) 
	st[j] = levels[nlev][indx].wght[j];
      for (unsigned j=0; j<nmix*ndim; j++) 
	st[j+nmix] = levels[nlev][indx].parm[j];
    
      double val = sstat->logPDF(st);

      for (int j=0; j<dim; j++)	ogpl << setw(18) << st[j];
      ogpl << setw(18) << val
	   << setw(18) << exp(val)
	   << endl;
    }

  } else {
    
    bool more = true;
    cout << "More? ";
    cin >> more;

    vector<double> pt((ndim+1)*nmix);

    while (more) {
      State st;

      pt[0] = nmix;
      double sum = 0.0;

      cout << "Weights: " << endl;
      for (unsigned i=0; i<nmix; i++) {
	cout << i << "? ";
	cin >> pt[i];
	sum += pt[i];
      }
      for (unsigned i=0; i<nmix; i++) pt[i] /= sum;

      cout << "Parameters: " << endl;
      for (unsigned i=0; i<nmix; i++) {
	cout << "Component #" << i << endl;
	vector<double> pp;
	for (unsigned j=0; j<ndim; j++) {
	  cout << j << "? ";
	  cin >> pt[nmix+i*ndim+j];
	}
      }
      
      st = pt;
      cout << "Value=" << sstat->logPDF(st);

      cout << endl << "More? ";
      cin >> more;
    }
  }

  delete sstat;

  return 0;
}

