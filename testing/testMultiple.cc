//
// Test multiple inheritance with and STL member as one of the base
// classes (based on test_diamond.cpp from the BOOST example
// directory)
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

using namespace boost;

int in_service = 0; // number constructed so far
int save_count = 0; // used to detect when base class is saved multiple times
int load_count = 0; // used to detect when base class is loaded multiple times

class base 
{
public:
  base() : id("Base") {}
  base(std::string& ID) : id(ID) {}
  virtual ~base() {}

  template<class Archive>
  void save(Archive &ar, const unsigned int /* file_version */) const
  {
    std::cout << "Saving base\n";
    ar << BOOST_SERIALIZATION_NVP(id);
  }
  
  template<class Archive>
  void load(Archive &ar, const unsigned int /* file_version */)
  {
    std::cout << "Restoring base\n";
    ar >> BOOST_SERIALIZATION_NVP(id);
  }
  
  BOOST_SERIALIZATION_SPLIT_MEMBER()

private:
  std::string id;

};

class derived : public base, public std::vector<double>
{
private:
  static std::string myname;

public:
  derived() : base(myname), i(0) {}
  derived(int i) : base(myname), i(i)
  {
    m[i] = "text";
    push_back(3.14159*(in_service*4+1));
    push_back(3.14159*(in_service*4+2));
    push_back(3.14159*(in_service*4+3));
    push_back(3.14159*(in_service*4+4));
    ++in_service;
  }

  virtual ~derived() {};

  template<class Archive>
  void save(Archive &ar, const unsigned int /* file_version */) const
  {
    std::cout << "Saving derived\n";
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(base)
       << BOOST_SERIALIZATION_BASE_OBJECT_NVP(std::vector<double>)
       << BOOST_SERIALIZATION_NVP(i) << BOOST_SERIALIZATION_NVP(m);
    ++save_count;
  }
  
  template<class Archive>
  void load(Archive & ar, const unsigned int /* file_version */)
  {
    std::cout << "Restoring derived\n";
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(base)
       >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(std::vector<double>)
       >> BOOST_SERIALIZATION_NVP(i) >> BOOST_SERIALIZATION_NVP(m);
    ++load_count;
  }
  
  BOOST_SERIALIZATION_SPLIT_MEMBER()
  
  bool operator==(const derived& another) const 
  {
    bool vec = true;
    if (size() != another.size()) vec = false;
    else {
      for (unsigned i=0; i<size(); i++)
	if (std::fabs((*this)[i] - another[i]) > 1.0e-18) vec = false;
    }
    return i == another.i && m == another.m;
  }

private:
  int i;
  std::map<int, std::string> m;    
};

std::string derived::myname = "Derived";

BOOST_CLASS_TRACKING(base, track_always)

BOOST_CLASS_EXPORT(derived)

int
main(int argc, char** argv)
{
  const char * testfile = "testMultiple.test1";
    
  const derived b(3);   
  {
    std::ofstream ofs(testfile);
    boost::archive::text_oarchive oa(ofs);
    oa << boost::serialization::make_nvp("b", b);
  }

  derived b2;
  {
    std::ifstream ifs(testfile);
    boost::archive::text_iarchive ia(ifs);
    ia >> boost::serialization::make_nvp("b2", b2);
  }

  if (1 == save_count) {
    std::cout << "save count PASS" << std::endl;
  } else {
    std::cout << "save count FAIL" << std::endl;
  }
    
  if (1 == load_count) {
    std::cout << "load count PASS" << std::endl;
  } else {
    std::cout << "load count FAIL" << std::endl;
  }
    
  if (b2 == b) {
    std::cout << "object equality PASS" << std::endl;
  } else {
    std::cout << "object equality FAIL" << std::endl;
  }
    
  std::remove(testfile);

  // do the same test with pointers
  testfile = "testMultiple.test2";

  save_count = 0;
  load_count = 0;

  const derived* bp = new derived( 3 );
  {
    std::ofstream ofs(testfile);    
    boost::archive::text_oarchive oa(ofs);
    oa << BOOST_SERIALIZATION_NVP(bp);
  }

  derived* bp2;
  {
    std::ifstream ifs(testfile);
    boost::archive::text_iarchive ia(ifs);
    ia >> BOOST_SERIALIZATION_NVP(bp2);
  }
  
  if (1 == save_count) {
    std::cout << "save count PASS" << std::endl;
  } else {
    std::cout << "save count FAIL" << std::endl;
  }
    
  if (1 == load_count) {
    std::cout << "load count PASS" << std::endl;
  } else {
    std::cout << "load count FAIL" << std::endl;
  }
    
  if (*bp2 == *bp) {
    std::cout << "pointer equality PASS" << std::endl;
  } else {
    std::cout << "pointer equality FAIL" << std::endl;
  }
    
  for (unsigned i=0; i<bp->size(); i++)
    std::cout << std::setw(10) << (*bp )[i]
	      << std::setw(10) << (*bp2)[i]
	      << std::endl;

  std::remove(testfile);
  
  return EXIT_SUCCESS;
}
