#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <BIEconfig.h>
#include <gvariable.h>
#include <ErfDist.h>

using namespace BIE;

/** 
    Check ErfDist implementation
*/

int main(int ac, char** av)
{
  int trials, iseed;
  double xmin, xmax, a, b;
  std::string outfile;

  try {
    //
    // Declare the supported options.
    //
    po::options_description desc("Allowed options");
    desc.add_options()
      ("help,h",	"produce help message")
      ("trials,N",     po::value<int>(&trials)->default_value(10000), "number of random trials")
      ("xmin,x"  ,     po::value<double>(&xmin)->default_value(-1.0), "minimum x range")
      ("xmax,X"  ,     po::value<double>(&xmax)->default_value(2.0), "maximum x range")
      ("offset,a",     po::value<double>(&a)->default_value(0.8), "error function offset")
      ("width,b",      po::value<double>(&b)->default_value(-0.1), "error function width")
      ("seed,s",       po::value<int>(&iseed)->default_value(11), "randomm number seed")
      ("outfile,o",   po::value<std::string>(&outfile)->default_value("test.dat"), "file containing x, pdf, cdf, pdftrials")
      ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);
  
    if (vm.count("help")) {
      cout << desc << endl;
      return 1;
    }
  }
  catch (std::exception& e) {
    cerr << "error: " << e.what() << endl;
    return 1;
  }
  catch(...) {
    cerr << "Exception of unknown type!" << endl;
  }
  
  // The ErfDist instance
  BIEgen = new BIEACG(iseed, 20);

  BIE::ErfDist erfD(a, b, xmin, xmax);

  // Sample from ErfDist and generate histogram
  unsigned nbins = std::floor(sqrt(trials));
  const double eps = 0.1;

  double Xmin = xmin, Xmax = xmax;
  if (Xmin<0.0) Xmin *= 1.0 + eps;
  else          Xmin *= 1.0 - eps;
  if (Xmax<0.0) Xmax *= 1.0 - eps;
  else          Xmax *= 1.0 + eps;


  double dx = (Xmax - Xmin)/nbins;
  std::vector<double> fbins(nbins, 0.0);

  for (int n=0; n<trials; n++) {
    auto ret = erfD.Sample();
    auto ind = std::floor( (ret[0] - Xmin)/dx );
    if (ind>=0 and ind<nbins) fbins[ind] += 1.0/dx/trials;
  }


  // Write output file
  try {
    std::ofstream out(outfile);
    for (unsigned i=0; i<nbins; i++) {
      std::vector<double> x {Xmin + dx*(0.5 + i)};
      BIE::State s(x);
      out << std::setw(18) << x[0]
	  << std::setw(18) << erfD.PDF(s)
	  << std::setw(18) << erfD.CDF(s)
	  << std::setw(18) << fbins[i]
	  << std::endl;
    }
  }
  catch (std::exception & e) {
    std::cout << "Caught exception:" << e.what() << std::endl; 
  }

  return 0;
}
