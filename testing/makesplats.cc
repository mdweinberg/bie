
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

#include <unistd.h>

#include <ACG.h>
#include <Normal.h>

string outname("splat.data");
string imagenm;

int 
main(int argc, char** argv)
{
  int M = 1;
  long iseed = 11;
  
  int c;
  while (1) {
    c = getopt (argc, argv, "M:i:f:g:");
    if (c == -1) break;
     
    switch (c)
      {
      case 'M': M = atoi(optarg); break;
      case 'f': outname = optarg; break;
      case 'g': imagenm = optarg; break;
      case 'i': iseed = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-f filename\t\toutput filename (default: " +outname+ ")\n";
	msg += "\t-g filename\t\timage data file\n";
	msg += "\t-M n\t\tnumber of components\n";
	msg += "\t-i x\t\tseed for random number generator\n";
	cerr << msg; exit(-1);
      }
  }

  ACG gen(iseed, 20);
  Normal unit(0.0, 1.0, &gen);

  ofstream out(outname.c_str());
  if (!out) {
    cerr << "Error opening <" + outname + ">\n";
    exit(-1);
  }

  vector<int> number;
  vector<double> xx, yy, sx, sy;
  double x, y, dx, dy;
  int n, sum = 0;

				// Get parameters from user
  for (int m=0; m<M; m++) {
    cout << "Component " << m << ": n, x0, y0, width_x, width_y? ";
    cin >> n;
    cin >> x;
    cin >> y;
    cin >> dx;
    cin >> dy;

    number.push_back(n);
    sum += n;

    xx.push_back(x);
    yy.push_back(y);
    sx.push_back(fabs(dx));
    sy.push_back(fabs(dy));
  }

				// Write an image?
  ofstream img;
  double xmin, xmax, ymin, ymax;
  unsigned numx, numy, total=0;
  vector< vector<unsigned> > image;
  if (imagenm.size()) {
    cout << "Xmin Xmax Nx? ";
    cin >> xmin;
    cin >> xmax;
    cin >> numx;
    cout << "Ymin Ymax Ny? ";
    cin >> ymin;
    cin >> ymax;
    cin >> numy;
    image = vector< vector<unsigned> >(numx, vector<unsigned>(numy, 0));
    img.open(imagenm.c_str());
    if (!img) {
      cerr << "Error opening image file <" << imagenm << ">" << endl;
      exit(-1);
    }
  }
  // Write data file

  // ** Header
  out << "real \"x\"" << endl;
  out << "real \"y\"" << endl;
  out << "real \"attribute\"" << endl;
  out << endl;

  for (int m=0; m<M; m++) {

    for (int i=0; i<number[m]; i++) {
      x = xx[m] + sx[m]*unit();
      y = yy[m] + sy[m]*unit();
      out 
	<< setw(15) << xx[m] + sx[m]*unit()
	<< setw(15) << yy[m] + sy[m]*unit()
	<< setw(15) << 1.0
	<< endl;

      if (img) {
	if (x>=xmin && x<xmax && y>=ymin && y<ymax) {
	    size_t nx = static_cast<size_t>(floor((x-xmin)*numx/(xmax - xmin)));
	    size_t ny = static_cast<size_t>(floor((y-ymin)*numy/(ymax - ymin)));
	    image[nx][ny]++;
	    total++;
	  }
      }
    }
  }
				// Write parameters
    
  out << "#     Parameters" << endl;
  for (int m=0; m<M; m++) {
    out << "#---- Component #" << m << endl;
    out << "#     N=" << number[m]  << endl;
    out << "#     X=" << xx[m]  << endl;
    out << "#     Y=" << yy[m]  << endl;
    out << "#    SX=" << sx[m]  << endl;
    out << "#    SY=" << sy[m]  << endl;
  }

				// Write image file
  if (img) {
    for (unsigned i=0; i<numx; i++) {
      x = xmin + (xmax - xmin)*i/(numx-1);
      for (unsigned j=0; j<numy; j++) {
	y = ymin + (ymax - ymin)*j/(numy-1);
	img << setw(15) << x << setw(15) << y 
	    << setw(15) << static_cast<double>(image[i][j])/total << endl;
      }
      img << endl;
    }
  }
}
