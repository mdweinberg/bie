/*******************************************************************************
* can successfully save and restore the Chain class
*******************************************************************************/

#include <iostream>
#include <iomanip>
#include <fstream>

#include <config.h>

#if PERSISTENCE_ENABLED

#include <bieTags.h>
#include <TestSaveManager.h>
#include <TestLoadManager.h>

#include <Serializable.h>
#include <clivector.h>
#include <Tessellation.h>
#include <Distribution.h>
#include <Chain.h>

using namespace std;

void vc_print(vector<Chain>& v, ostream & out)
{
  out << "Chains: ";
  for (unsigned i=0; i<v.size(); i++) {
    out << " [" << v[i].Wght0(0)
	<< ", " << v[i].Wght0(1) << "] " << endl;
  }
  out << endl;
}

void vd_print(clivectord* v, ostream& out)
{
  for (unsigned i=0; i<(*v)().size(); i++) {
    if (i) out << ", ";
    out << setw(18) << (*v)()[i];
  }
  out << endl;
}

int main()
{
  system("rm -rf " PERSISTENCE_DIR);

  string statename = "testPersistenceChain";

  vector<Chain> tc, restored_tc;

  unsigned nmix = 2, ndim = 2;
  StateInfo *si = new StateInfo(nmix, ndim);

  try {
    for (unsigned i=0; i<19; i++) {
      Chain tmp(si);
    
      tmp.Wght0(0) = (double)i/19.0;
      tmp.Wght0(1) = 1.0 - (double)i/19.0;
      
      tc.push_back(tmp);
    }

    clivectord td(3, 3.14159);
   
    TestUserState teststate(statename, 0, true);
    for (unsigned i=0; i<19; i++) teststate.addObject(&tc[i]);
    teststate.addObject(&td);

    TestSaveManager testSaveManager(&teststate, BOOST_BINARY, BACKEND_FILE);

    try {
      testSaveManager.save();
    } catch (BoostSerializationException * e) {
      cout << "#######################################################" << endl;
      cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "#######################################################" << endl;
    }
    
    TestUserState *restored;

    try {
      TestLoadManager testLoadManager(&restored, statename, 1, BOOST_BINARY, BACKEND_FILE);
      testLoadManager.load();
    } catch (BoostSerializationException * e) {
      cout << "#######################################################" << endl;
      cout << "##### BoostSerializationException thrown on LOAD! #####" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "#######################################################" << endl;
    }

    vector<Serializable*>::iterator loadedObj = 
      restored->getTransClosure().objectlist.begin();

    for (unsigned i=0; i<19; i++) {
      Chain *tmp = dynamic_cast<Chain*>(*(loadedObj++));
      restored_tc.push_back(*tmp);
    }
    
    clivectord *restored_td = dynamic_cast<clivectord*>(*(loadedObj++));

    fstream actualoutput;
    string filename = statename + "_actual";
    actualoutput.open(filename.c_str(), fstream::out);
    vc_print(tc,  actualoutput);
    vd_print(&td, actualoutput);
    actualoutput.close();

    fstream restoredoutput;
    filename = statename + "_restored";
    restoredoutput.open(filename.c_str(), fstream::out);
    vc_print(restored_tc, restoredoutput);
    vd_print(restored_td, restoredoutput);
    restoredoutput.close();
  }
  catch (BIEException & e)
  { cout << "Caught Exception: \n" << e.getErrorMessage() << endl; }
}

#else

int main()
{
  std::cout << "This test is only appropriate when persistence is enabled" 
	    << std::endl;
}

#endif
