#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

#include <unistd.h>

#include <ACG.h>
#include <Normal.h>
#include <Uniform.h>

string outname("splatND.data");

int 
main(int argc, char** argv)
{
  const double xyrange = 0.1;
  int M = 1;
  int N = 2;
  long iseed = 11;
  
  int c;
  while (1) {
    c = getopt (argc, argv, "M:N:i:f:h");
    if (c == -1) break;
     
    switch (c)
      {
      case 'M': M = atoi(optarg); break;
      case 'N': N = atoi(optarg); break;
      case 'f': outname = optarg; break;
      case 'i': iseed = atoi(optarg); break;
      case 'h':
      case '?': 
	string msg = "usage: %s [options]\n";
	msg += "\t-f filename\t\toutput filename (default: " +outname+ ")\n";
	msg += "\t-N n\t\t\tnumber of dimensions\n";
	msg += "\t-M n\t\t\tnumber of components\n";
	msg += "\t-i x\t\t\tseed for random number generator\n";
	cerr << msg; exit(-1);
      }
  }

  ACG gen(iseed, 20);
  Normal unit(0.0, 1.0, &gen);
  Uniform xy(-xyrange, xyrange, &gen);

  ofstream out(outname.c_str());
  if (!out) {
    cerr << "Error opening <" + outname + ">\n";
    exit(-1);
  }

  vector<int> number;
  vector< vector<double> > center;
  vector<double> var, pos(N, 0.0);
  double v;
  int n, sum = 0;

				// Get parameters from user
  for (int m=0; m<M; m++) {
    cout << "Component " << m << ": n, width? ";
    cin >> n;
    cin >> v;

    cout << "Need " << N << " position values . . ." << endl;
    for (int i=0; i<N; i++) {
      cout << setw(3) << i+1 << "> ";
      cin >> pos[i];
    }

    number.push_back(n);
    center.push_back(pos);
    var.push_back(v*v);
    sum += n;
  }

				// Make and randomly order the data
  vector< vector<double> > recs;
  vector<double> att;

  for (int m=0; m<M; m++) {
    for (int i=0; i<number[m]; i++) {
      att = center[m];
      for (int n=0; n<N; n++) att[n] += sqrt(var[m])*unit();
      recs.push_back(att);
    }
  }
  std::random_shuffle(recs.begin(), recs.end());
  

				// Write data file

  // ** Header
  ostringstream sout;
  out << "  " << setw(18) << "real \"x\"" << endl;
  out << "  " << setw(18) << "real \"y\"" << endl;
  for (int n=0; n<N; n++) {
    sout.str("");
    sout << "real \"R(" << n+1 << ")\"";
    out  << "  " << setw(18) << sout.str() << endl;
  }
  out << endl;
  
  out.precision(10);

  for (unsigned i=0; i<recs.size(); i++) {
    out << setw(18) << xy() << setw(18) << xy();
    for (int n=0; n<N; n++) out << setw(18) << recs[i][n];
    out << endl;
  }
				// Write parameters
    
  out << "#     Parameters" << endl;
  for (int m=0; m<M; m++) {
    out << "#---- Component #" << m << endl;
    out << "#     N=" << number[m]  << endl;
    for (int n=0; n<N; n++)
      out << "#     Pos(" << n+1 << ")=" << center[m][n] << endl;
    out << "#     Var=" << var[m]  << endl;
  }
  
}
