// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <ACG.h>

#define IS_MAIN

#include <TemperedSimulation.h>

#include <BIEmpi.h>
#include <LikelihoodComputationMPI.h>

#include <CountConverge.h>
#include <SubsampleConverge.h>
#include <MappedGrid.h>
#include <KdTessellation.h>
#include <QuadGrid.h>
#include <SquareTile.h>
#include <RecordStream_Ascii.h>
#include <DataTree.h>

#include <Model.h>
#include <InitialMixturePrior.h>
#include <PostMixturePrior.h>
#include <Histogram1D.h>
#include <EnsembleStat.h>
#include <SplatModel.h>
#include <LegeIntegration.h>
#include <MetropolisHastings.h>
#include <BIEdebug.h>

enum TessType {kd_inf, kd, mapped, quad};
enum ConvType {counter, subsample};

using namespace BIE;

/** Files */
string datafile("splat.data");

/** Control functions 
    to modify behavior of running simulation 
    
    Status word in file ".control" is read after every
    iteration.
*/
//@{

/// If 1st bit is 1, stop simulation, compute statistics and decend one level
bool goto_next_level(void) 
{
  bool ans = false;
  
  // Read control file
  ifstream in(cntrlfile.c_str());
  if (in) {
    // Read the flag
    unsigned int flag;
    in >> flag;
    in.close();
    
    if (flag & 1) {
      ans = true;
      // Reset the flag (bit 1 zeroed)
      flag = flag & 0xFFFE;
      ofstream out(cntrlfile.c_str());
      out << flag << endl;
    }
  }
  
  return ans;
}

/// If 2nd bit is 1, turn off tempering for this level
bool stop_tempering(void)
{
  bool ans = false;
  
  // Read control file
  ifstream in(cntrlfile.c_str());
  if (in) {
    // Read the flag
    unsigned int flag;
    in >> flag;
    in.close();
    
    if (flag & 2) {
      ans = true;
      // Reset the flag (bit 2 zeroed)
      flag = flag & 0xFFFD;
      ofstream out(cntrlfile.c_str());
      out << flag << endl;
    }
  }
  
  return ans;
}

/// If 3rd bit is 1, clean up and finish
bool finish(void) 
{
  bool ans = false;
  
  // Read control file
  ifstream in(cntrlfile.c_str());
  if (in) {
    // Read the flag
    unsigned int flag;
    in >> flag;
    in.close();
    
    if (flag & 4) {
      ans = true;
      // Reset the flag (bits 1--3 zeroed)
      flag = flag & 0xFFF8;
      ofstream out(cntrlfile.c_str());
      out << flag << endl;
    }
  }
  
  return ans;
}
//@}

/**
   @name multilevel: main
   Does full simulation at multiple levels of resolution.  Parallel
   MPI implementation.
*/
int
main(int argc, char** argv)
{
  bool logfile = true;
  bool mstat = false;
  bool tempering = true;
  int nsteps = 100;
  int keep = 1000;
  int nxy0 = 4;
  int rate = 10;
  int L = 4;
  int minmc = 0;
  int ntab = 100;
  int NLB = 20;
  int nload = -1;
  double maxT=128.0;
  double wscale=0.2;
  long seed = 11;
  int NICE=-1;
  int nmix = 2;			// Two component mixture
  double alpha = 1.0;		// Dirichlet shape
  int mpi_type = 0;		// Tile granularity
  int ndim = 2;			// Two model parameters
  TessType TESSTYPE = quad;
  ConvType CONVTYPE = counter;
  
  
  // Initialize mpi
  //
  mpi_init(argc, argv);
  
#ifdef DEBUG
  sleep(20);
#endif
  
  // Invokes ctor `GetOpt (int argc, char **argv,
  //                       char *optstring);'
  
  int c;
  while (1) {
    c = getopt (argc, argv, 
		"n:m:N:M:L:I:B:d:f:r:t:a:O:P:oH:s:k:SW:q:t:wx4T:C:gh");
    if (c == -1) break;
    
    switch (c)
      {
      case 's': seed = atoi(optarg); break;
      case 'n': nsteps = atoi(optarg); break;
      case 'm': minmc = atoi(optarg); break;
      case 'N': nxy0 = atoi(optarg); break;
      case 'M': nmix = atoi(optarg); break;
      case 'L': L = atoi(optarg); break;
      case 'I': NLB = atoi(optarg); break;
      case 'B': nload = atoi(optarg); break;
      case 'r': rate = atoi(optarg); break;
      case 'q': ntab = atoi(optarg); break;
      case 't': maxT = atof(optarg); break;
      case 'a': alpha = atof(optarg); break;
      case 'W': wscale = atof(optarg); break;
      case 'd': datafile.erase(); datafile = optarg; break;
      case 'f': homedir.erase(); homedir = optarg; break;
      case 'O': outfile.erase(); outfile = optarg; break;
      case 'k': keep = atoi(optarg); break;
      case 'P': NICE = atoi(optarg); break;
      case 'S': mstat = true; break;
      case 'T': TESSTYPE = (TessType)atoi(optarg); break;
      case 'C': CONVTYPE = (ConvType)atoi(optarg); break;
      case 'o': logfile = false; break;
      case 'x': tempering = false; break;
      case 'g': mpi_type = 1; break;
      case '4': ndim = 4; break;
      case 'h':
      case '?': 
	string msg = "usage: " + string(argv[0]) + " [options]\n";
	msg += "Tests the two-dimensional Gaussian splat model which\n";
	msg += "has no line-of-sight integration.  Options are:\n";
	msg += "\t-s I\t\twhere I is the pseudorandom number seed\n";
	msg += "\t-n N\t\twhere N is the number of iterations\n";
	msg += "\t-m N\t\tminimum number of temperature states\n";
	msg += "\t-N N\t\twhere N is the number of bins in each dimension\n";
	msg += "\t-M M\t\twhere M is the number of potential components\n";
	msg += "\t-r N\t\treport state after every N iterations\n";
	msg += "\t-L N\t\tnumber of spatial resolution levels\n";
	msg += "\t-l N\t\tnumber of Metropolis-Hastings updates\n";
	msg += "\t-q N\t\tlength of extinction interpolation table\n";
	msg += "\t-t T\t\tmaximum temperature for simulated tempering\n";
	msg += "\t-W x\t\tscale factor for proposal width from variance\n";
	msg += "\t-k N\t\tnumber of states used to do PCA\n";
	msg += "\t-R p\t\treport only if percent support larger than p\n";
	msg += "\t-d file\t\tdata file\n";
	msg += "\t-O file\t\tlog file\n";
	msg += "\t-O file\t\tMetropolis-Hastings parameters\n";
	msg += "\t-C file\t\tposterior sample in SM 'ch' format\n";
	msg += "\t-o\t\tturn off log file\n";
	msg += "\t-e\t\tturn off posterior sampled estimate at lower levels\n";
	msg += "\t-4\t\tuse position and variances (4 parameters)\n";
	msg += "\t-g\t\tuse integration-level granularity (default is tile)\n";
	msg += "\t-T\t\tTessellation type (kd_inf, kd, mapped, quad)\n";
	cerr << msg; exit(-1);
      }
  }
  
  // Prefix root data directory
  //
  datafile  = homedir + "/" + datafile;
  
  // Set priority on nodes
  // 
  if (NICE>0) setpriority(PRIO_PROCESS, 0, NICE);
  
  
  // Initialize base random number generator
  //
  BIEgen = new BIEACG(seed, 20);
  
  // Hold instantiation of classes define the simuation
  //
  SplatModel*              model = 0;
  Tessellation*            grid  = 0;
  SquareTile*              tile  = 0;
  RecordInputStream_Ascii* ris   = 0;
  BaseDataTree*            dist  = 0;
  SampleDistribution*      histo = 0;
  LegeIntegration*         intgr = 0;
  
  int nlb;			// Integration grid
  int nxy = nxy0;		// Integration grid target
  
  // StateInfo
  //
  StateInfo *si = new StateInfo(nmix, ndim);

  // Gaussian splat model
  //
  model = new SplatModel(ndim, nmix);
  
  // Factor for 1D histogram in each tile (put everything in one bin)
  //
  histo = new Histogram1D(0.0, 2.0, 10.0, "attribute");
  
  // Define prior
  //
  UniformDist unif(-1.0, 1.0);
  WeibullDist weib(0.1, 1.0, 1.0e-4, 10.0);
  clivectordist vdist(ndim, &unif);
  if (ndim==4) vdist()[2] = vdist()[3] = &weib;
  MixturePrior* prior = new InitialMixturePrior(si, alpha, &vdist);
  
  // Simple sample statistic
  //
  EnsembleStat* sstat = new EnsembleStat [L];
  
  
  // Store  initial state
  //
  vector<double> winit(nmix);
  vector< vector<double> > pinit(nmix);
  for (int i=0; i<nmix; i++) pinit[i] = vector<double>(ndim);

  State s(si);
  s.setState(nmix, winit, pinit);
  
  for (int n=0; n<L; n++) {
    
    // Integration method
    //
    // Compute number of Gaussian pts
    // at this level; NLB is the number
    // of points desired per half-dimension
    //
    nlb = (int)(NLB/(nxy/2)) + 1;
    intgr = new LegeIntegration(nlb, nlb);
    
    // Rectangular grid tessellation
    //
    
    tile = new SquareTile();
    
    switch (TESSTYPE) {
    case kd_inf:
      ris = new RecordInputStream_Ascii(datafile);
      grid = new KdTessellation(tile, ris, 100, 1.0);
      break;
    case kd:
      ris = new RecordInputStream_Ascii(datafile);
      grid = new KdTessellation(tile, ris, 100, 1.0, -1.0, 2.0, -1.0, 2.0);
      break;
    case mapped:
      grid = new MappedGrid(tile, -1.0, 2.0, -1.0, 2.0, nxy, nxy);
      break;
    case quad:
      grid = new QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, L+1);
      break;
    default:
      cerr << "No such tessellation <" << (int)TESSTYPE << ">\n";
      exit(-1);
    }
    
    ris  = new RecordInputStream_Ascii(datafile);
    dist = new DataTree(ris, histo, grid);

    // Initialize the ensemble statistic container for this level
    //
    sstat[n].setDimensions(si);

    // Print for debugging
    //
    printFrontierTiles(dist->GetDefaultFrontier());
    cout << endl;
    
    // Dump out means and standard devs 
    // for each tile
    dist->Reset();
    while(!(dist->IsDone())) {
      cout 
	<< setw(15) << dist->CurrentTile()->X(0.5, 0.5)
	<< setw(15) << dist->CurrentTile()->Y(0.5, 0.5)
	<< setw(15) << dist->CurrentItem()->getValue(0)
	<< setw(15) << (dist->CurrentItem()->Mean())[0]
	<< setw(15) << (dist->CurrentItem()->StdDev())[0]
	<< endl;
      
      dist->Next();
    }
    cout << endl;
    


    // Define convergence method
    //
    Converge *converge;
    switch (CONVTYPE) {
    case counter:
      converge = new CountConverge(nsteps, &sstat[n]);
      break;
    case subsample:
      converge = new SubsampleConverge(nsteps, &sstat[n], "");
      break;
    default:
      cerr << "No such tessellation <" << (int)TESSTYPE << ">\n";
      exit(-1);
    }
    
    // Define parameters for TemperedSimulation
    //
    
    vector<double> mhw(ndim+1);
    MHWidth* mhwidth = 0;
    
    if (n==0) {
      mhw[0] = 0.005;
      mhw[1] = 0.005;
      mhw[2] = 0.005;
      if (ndim==4) {
	mhw[3] = 0.01;
	mhw[4] = 0.01;
      }
      mhwidth = new MHWidthOne(si, &mhw);
    }
    
    // Set up widths
    //
    if (n) {

      mhw = vector<double>(ndim+1, 0.0);
	
      sstat[n-1].ComputeDistribution();
      
      for (int k=0; k<nmix; k++)
	mhw[0] += sstat[n-1].covar[nmix-1][1+k][1+k];
	
      for (int k=0; k<nmix; k++) {
	for (int j=0; j<ndim; j++) 
	  mhw[1+j] += sstat[n-1].covar[nmix-1][1+nmix+ndim*k+j][1+nmix+ndim*k+j];
      }
      
      for (int j=0; j<ndim+1; j++) 
	mhw[j] = wscale*sqrt(mhw[j]/nmix);
      
      if (myid==0) {
	cout << endl << "Widths:" << endl;
	
	for (int i=0; i<ndim+1; i++)
	  cout << setw(5) << i << "> " << mhw[i] << endl;
	cout << endl;
      }

      delete mhwidth;
      mhwidth = new MHWidthOne(si, &mhw);
    }
      
    // Ok, now instantiate the simulation
    //

    LikelihoodComputationMPI likelihoodComputation(si);
    MetropolisHastings       mca;

    TemperedSimulation       testsim(si, minmc, maxT, mhwidth,
			             dist, model, intgr, converge, prior, 
				     &likelihoodComputation, &mca);
    //                               ^
    // defining classes              |
    // ------------------------------|
      
    // Put the all nodes *but* Node 0 into slave mode
    //
      
    likelihoodComputation.Granularity(mpi_type);
      
    if (myid) {
      cout << "Process " << myid << ": enslaved\n";
      likelihoodComputation.startThread();
    }

    cout << "Process " << myid << ": continuing\n";
      
    if (stop_tempering()) TemperedSimulation::use_tempering = false;
      
    if (n==0) {			// Default initialization
      for (int i=0; i<nmix; i++) {
	winit[i] = 1.0/nmix;
	pinit[i][0] = 0.5;
	pinit[i][1] = 0.5;
	if (ndim==4) {
	  pinit[i][2] = 0.1;
	  pinit[i][3] = 0.1;
	}
      }
    }
     
    s.setState(nmix, winit, pinit);
    testsim.NewState(s);
      
    if (myid==0) cout << "Initial value: " << testsim.GetValue() << endl;
      
    for (int i=0; i<nsteps; i++) {
      testsim.OneStep();
      
      double value = testsim.GetValue();
      State s = testsim.GetState();
      double mean, stdev, up, down;
      sstat[n].stats(10, mean, stdev);
      
      // Print diagnostics to screen
      
      if (myid==0) {

	testsim.GetLastCycle(up, down);
	cout << "Step " << setw(4) << i << ": [" << nxy << "X" << nxy
	     << "]  value=" << value 
	     << "  mean=" << mean << "  stdev=" << stdev << endl
	     << "           swaps=" << testsim.GetSwap() 
	     << "[" << testsim.GetAcceptance() << "]"
	     << "  up=" << up << "  down=" << down
	     << "  prob=" << exp(up-down)
	     <<  endl;
	
	testsim.ReportState();
	
	if (mstat) {
	  vector<double> ret = testsim.GetMixstat();
	  cout << endl;
	  cout << "  P(accept):";
	  for (int ik=0; ik<(int)ret.size(); ik++) cout << " " << ret[ik];
	  cout << endl;
	}
	
	cout << endl;
      }
      
      if (goto_next_level()) break;
	
      // Log state to file
      if (logfile) testsim.LogState(n, i, outfile);
      
      // Test Convergence
      if (testsim.Convergence()) break;
    }
      
    likelihoodComputation.free_workers();
      
    // Only need to replace this for the master
    // because the slaves never evaluate the prior
    MixturePrior* old_prior = prior;
    prior = new PostMixturePrior(old_prior, sstat+n);
    delete old_prior;
    
    // Clean up garbage from this level
    //
    delete intgr;
    delete grid;
    delete converge;
    
    // Double resolution for next level
    //
    nxy *= 2;
    
    if (finish()) break;
    
#ifdef DEBUG
    cerr << "Process " << myid << ": moving to next level . . . \n";
#endif
  } // Next level
    
  // All done
  //
  MPI_Finalize();
    
  // Anal retentative delete for leak analysis
  //
  delete dist;
  delete tile;
  delete [] sstat;
  delete prior;
  delete histo;
  delete model;
  delete BIEgen;
  
  return 0;
}
  
