#ifndef _B_h
#define _B_h

#include <vector>
#include <serial.h>

class B
{
public:
  std::vector<float> u;
  B() {}

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & BOOST_SERIALIZATION_NVP(u);
  }
};

BOOST_CLASS_EXPORT_KEY(B)

#endif
