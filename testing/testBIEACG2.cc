#include <cmath>

#include <config.h>

#include <Serializable.h>
#include <boost/serialization/tracking.hpp>
#include <boost/shared_ptr.hpp> 

#include <BIEACG.h>
#include <Uniform.h>
#include <gvariable.h>

using namespace BIE;

void printfile(ostream* out, Uniform* unif, unsigned num)
{
  for (unsigned n=0; n<num; n++)
    (*out) << setw(18) << (*unif)() << endl;
}

int main()
{
  try
  {
    // Make the persistence directory something unique to us
    // to avoid interence with other tests.
    system("rm -rf ./pdir/testBIEACG2");
  
    // Initialize the random number generator.
    //
    BIEgen = new BIEACG(11, 25);
    
    // Output name
    //
    string oname("testBIEACG_values.");

    // Create a distribution
    //
    Uniform * u  = new Uniform(0, 1, BIEgen);
    
    // Number of trials
    //
    const unsigned numtot = 20;

    // Fiducial output
    //
    string fname1 = oname + "before";
    ofstream out1(fname1.c_str());
    if (!out1) {
      cerr << "Error opening <" << fname1 << ">" << endl;
      exit(-1);
    }

    printfile(&out1, u, numtot);
    out1.close();

    delete BIEgen;
    BIEgen = new BIEACG(11, 25);

    delete u;
    u  = new Uniform(0, 1, BIEgen);
 
    // Test output
    //
    string fname2 = oname + "after";
    ofstream out2(fname2.c_str());
    if (!out2) {
      cerr << "Error opening <" << fname1 << ">" << endl;
      exit(-1);
    }

    printfile(&out2, u, numtot/2);

    try {
      ofstream ofs("archive.test");
      boost::archive::binary_oarchive oa(ofs);

      vector<Serializable *> p;
      p.push_back(BIEgen);
      p.push_back(u);
			       
      oa << BOOST_SERIALIZATION_NVP(p);

    } catch (BoostSerializationException * e) {
      cout << "#######################################################" << endl;
      cout << "##### BoostSerializationException thrown on SAVE! #####" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "#######################################################" << endl;
    }

    // Restore the State

    try {
      ifstream ifs("archive.test");
      boost::archive::binary_iarchive ia(ifs);

      vector<Serializable *> p;
      ia >> BOOST_SERIALIZATION_NVP(p);

      BIEgen = static_cast<BIEACG * >(p[0]);
      u      = static_cast<Uniform *>(p[1]);


    } catch (BoostSerializationException * e) {
      cout << "#######################################################" << endl;
      cout << "##### BoostSerializationException thrown on LOAD! #####" << endl;
      cout << "Printing persistence stack trace..." << endl;
      e->print(cout);
      cout << "#######################################################" << endl;
    }
    printfile(&out2, u, numtot - numtot/2);
  }
  catch (BIEException & e)
  { cerr << e.getErrorMessage() << endl; }

  return 0;
}
