#include <iostream>
#include <fstream>

using namespace std;

#include <C.h>


BIE_CLASS_EXPORT_IMPLEMENT(vectord)
BIE_CLASS_EXPORT_IMPLEMENT(VectorTest)

int main(int argc, char** argv)
{
  string archivename("test_archive");

  vector<Serializable*> instances, instances2;

  instances.push_back(new VectorTest(3, 3.14159));
  instances.push_back(new VectorTest(5, 1.23));

  try {
    ofstream out(archivename.c_str());
    unsigned ver = 1;
#ifdef POLYMORPHIC
    boost::archive::polymorphic_xml_oarchive ar(out, ver);
#else
    boost::archive::xml_oarchive ar(out, ver);
#endif
    ar << BOOST_SERIALIZATION_NVP(instances);
  }
  catch (boost::archive::archive_exception & e) {
    cout << "While SAVING, caught exception:" << endl
	 << e.what() << endl; 
  }

  try {
    ifstream in(archivename.c_str());
    unsigned ver = 1;
#ifdef POLYMORPHIC
    boost::archive::polymorphic_xml_iarchive ar(in, ver);
#else
    boost::archive::xml_iarchive ar(in, ver);
#endif
    ar >> BOOST_SERIALIZATION_NVP(instances2);
  }
  catch (boost::archive::archive_exception & e) {
    cout << "While LOADING, caught exception:" << endl
	 << e.what() << endl; 
  }
  
  fstream actualoutput;
  string filename = archivename + "_actual";
  actualoutput.open(filename.c_str(), fstream::out);
  for (unsigned i=0; i<instances.size(); i++) {
    VectorTest * z = dynamic_cast<VectorTest*>(instances[i]);
    z->print(actualoutput);
  }
  actualoutput.close();

  fstream restoredoutput;
  filename = archivename + "_restored";
  restoredoutput.open(filename.c_str(), fstream::out);
  for (unsigned i=0; i<instances2.size(); i++) {
    VectorTest * z = dynamic_cast<VectorTest*>(instances2[i]);
    z->print(restoredoutput);
  }
  restoredoutput.close();
}
