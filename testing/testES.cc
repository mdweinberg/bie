/**
   For leak testing EnsembleStat & Chain
*/


#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

#include <stdlib.h>

#define IS_MAIN

#include <BIEconfig.h>
#include <EnsembleStat.h>
#include <Chain.h>
#include <ACG.h>
#include <Uniform.h>

#include <DummyModel.h>

using namespace BIE;

string priorfile("prior.dat");
string datafile("data.dat");

/**
   Test routine for data accumulation
*/
int
main(int argc, char** argv)
{
  const unsigned nmix = 2;
  const unsigned ndim = 2;
  const int nstates = 100;
  const int nensembles = 100;

  BIEgen = new BIEACG(11, 20);
  Uniform *unit = new Uniform(0.0, 1.0, BIEgen);
  

  // ==================================================
  // MAIN LOOP
  // ==================================================
  
  StateInfo *si = new StateInfo(nmix, ndim);

  vector<double> state(si->Ntot);
  vector<Chain*> particles;

  for (int k=0; k<nensembles; k++) {

#ifdef ES
    EnsembleStat *sstat = new EnsembleStat(si);
#endif

    unsigned dim = si->Ntot;

    for (int i=1; i<nstates; i++) {

      double sum = 0.0;
      
      for (unsigned j=0; j<dim;  j++) state[j] = (*unit)();
      for (unsigned j=0; j<nmix; j++) sum += state[j];
      for (unsigned j=0; j<nmix; j++) state[j] /= sum;
      
      State s(si, state);
      Chain *chain = new Chain(si);

      chain->AssignState0(s);

      particles.push_back(chain);

#ifdef ES
      sstat->AccumData(1.0, s);
#endif
    }

#ifdef ES
    sstat->ComputeDistribution();
    sstat->PrintDiag(cout);

    delete sstat;
#endif
  }

  for (unsigned i=0; i<particles.size(); i++) {
    delete particles[i];
  }

  delete BIEgen;
  delete unit;

  return 0;
}

