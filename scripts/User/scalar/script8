#============================================================
#
# Example: one-dimensional PARALLEL CHAINS run
#
#============================================================
# Tag for output files
#============================================================
#
set nametag = "run8"
#
#============================================================
# Logfile name
#============================================================
#
set outfile = "run8.statelog"
#
#============================================================
# Model dimension
#============================================================
#
set ndim = 1
#
#============================================================
# Number of mixture components
#============================================================
#
set nmix = 2
#
#============================================================
# State metadata
#============================================================
#
set labs = new clivectors(1, "Pos(x)")
set si = new StateInfo(nmix, ndim)
    si->labelMixture(labs)
#
#============================================================
# Minimum number of temperature levels
#============================================================
#
set minmc = 16
#
#============================================================
# Maximum number of steps
#============================================================
#
set nsteps = 2000
#
#============================================================
# Proposal function scaling factor
#============================================================
#
set width = 0.01
#
#============================================================
# Maximum temperature (T=1 means no heating at all)
#============================================================
#
set maxT = 16.0
#
#============================================================
# Prior function
#============================================================
#
set unif  = new UniformDist(-0.2, 1.0)
set dist  = new clivectordist(1, unif)
set prior = new InitialMixturePrior(si, 1.0, dist)
#
#============================================================
# Define the M-H proposal function
#============================================================
#
set mca     = new MetropolisHastings()
set mvec    = new clivectord(2, 0.001)
set mhwidth = new MHWidthOne(si, mvec)
#
#============================================================
# Define the simulation
#============================================================
#
set sstat   = new EnsembleDisc(si)
set convrg  = new SubsampleConverge(nsteps, sstat, "run8.0")
set like    = new LikelihoodComputationSerial()
set sim     = new ParallelChains(si, minmc, maxT, mhwidth, convrg, prior, like, mca)
#   sim->SetAlgorithm(1)
#
#============================================================
# Define the model through the user-defined likelihood
#============================================================
#
set fct = new GaussTestLikelihoodFunctionMulti(9, 10000, 6)
    fct->SetDim(1)
    sim->SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#============================================================
# Compute distribribution for Level 0 and new prior for Level 1
#============================================================
#
sstat->statesToFile("run8.savestates.1", false)
sstat->ComputeDistribution(500)
sstat->statesToFile("run8.savestates.2", false)
set nprior = new PostMixturePrior(prior, sstat)
set nstat  = new EnsembleDisc(si)
set convrg = new SubsampleConverge(nsteps, sstat, "run8.1")
#
#============================================================
# New simulation for Level 1
#============================================================
#
set current_level = 1
set nsim = new ParallelChains(si, minmc, maxT, mhwidth, convrg, nprior, like, mca, sim)
    nsim->SetUserLikelihood(fct)
set width = 0.01
set run = new RunOneSimulation(nsteps, width, sstat, nprior, nsim)
    run->Run()
