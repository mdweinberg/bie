#============================================================
#
# Example: one-dimensional TEMPERED SIMULATION run
#          and REVERSIBLE JUMP
#
#============================================================
#
#============================================================
# Logfile name
#============================================================
#
set outfile = "run4.statelog"
#
#============================================================
# Number of dimensions (position and variance)
#============================================================
#
set ndim = 1
#
#============================================================
# TOTAL number of mixture components
#============================================================
#
set nmix = 12
#
#============================================================
# State metadata
#============================================================
#
set labs = new clivectors(1, "Pos(x)")
set si = new StateInfo(nmix, ndim)
#
#============================================================
# Minimum number of levels
#============================================================
#
set minmc = 16
#
#============================================================
# Number of steps
#============================================================
#
set nsteps = 10000
#
#============================================================
# Width scaling
#============================================================
#
set width = 0.1
#
#============================================================
# Maximum temperature
#============================================================
#
set maxT = 16.0
#
#============================================================
# Mixture prior
#============================================================
#
set unif = new UniformDist(-0.2, 1.2)
set pvec = new clivectordist(1, unif)
set prior = new InitialMixturePrior(si, 1.0, pvec)
set sstat = new EnsembleStat(si)
#
#============================================================
# Simply count states
#============================================================
#
set convrg = new CountConverge(nsteps, sstat)
#
#============================================================
# Reversible Jump Monte Carlo algorithm
#============================================================
#
set mca = new ReversibleJump(nmix, 1.0)
#
#============================================================
# Proposal function for Metropolis-Hastings steps
#============================================================
#
set mvec = new clivectord(2, 0.001)
set mhwidth = new MHWidthOne(si, mvec)
#
#============================================================
# Parallel chains algorithm
#============================================================
#
set like = new LikelihoodComputationSerial()
set sim = new TemperedSimulation(si, minmc, maxT, mhwidth, convrg, prior, like, mca)
#
#============================================================
# 1-d Gaussian model
#============================================================
#
set fct = new GaussTestLikelihoodFunction(150, 1000000)
    fct->SetDim(ndim)
    sim->SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
