#!/bin/bash

exec_name=$0

if [ $1 ]; then
    echo "Usage: "$exec_name
    exit -1
fi

echo "==========================================="
echo "This tests checkpointing of a simple test"
echo "simulation by comparing the results of the"
echo "chain output without checkpointing and"
echo "after saving and restoring"
echo "==========================================="
echo "removing files from previous test session.."
echo "==========================================="

rm -rf pdir/test8 bie.chainlog.* run8* script8*out

echo "==========================================="
echo "running script8"
echo "==========================================="

mpirun -np 1 bie -f script8 > script8.out

echo "==========================================="
echo "running script8part1"
echo "==========================================="

mpirun -np 1 bie -f script8part1 > script8part1.out

echo "==========================================="
echo "running script8part2"
echo "==========================================="

mpirun -np 1 bie -f script8part2 > script8part2.out

echo "==========================================="
echo "diffing..."
echo "==========================================="

diff run8.statelog run8p12.statelog 

if [ "$?" -eq "0" ]; then
    echo "Test : SUCCESS"
else
    echo "Test : FAILED"
fi
