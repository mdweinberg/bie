#!/bin/bash

exec_name=$0

if [ $1 ]; then
    echo "Usage: "$exec_name
    exit -1
fi

echo "==========================================="
echo "This tests checkpointing of a simple test"
echo "simulation by comparing the results of the"
echo "chain output without checkpointing and"
echo "after saving and restoring"
echo "==========================================="
echo "removing files from previous test session.."
echo "==========================================="

rm -rf pdir/test9 bie.chainlog.* run9* script9*out

echo "==========================================="
echo "running script9"
echo "==========================================="

mpirun -np 1 bie -f script9 > script9.out

echo "==========================================="
echo "running script9part1"
echo "==========================================="

mpirun -np 1 bie -f script9part1 > script9part1.out

echo "==========================================="
echo "running script9part2"
echo "==========================================="

mpirun -np 1 bie -f script9part2 > script9part2.out

echo "==========================================="
echo "diffing..."
echo "==========================================="

diff run9.statelog run9p12.statelog 

if [ "$?" -eq "0" ]; then
    echo "Test : SUCCESS"
else
    echo "Test : FAILED"
fi
