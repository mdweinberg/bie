#============================================================
#
# Example: two-dimensional TEMPERED SIMULATION run
#
#============================================================
#
#============================================================
# Logfile name
#============================================================
#
set outfile = "run0.statelog"
#
#============================================================
# Model dimension
#============================================================
#
set ndim = 2
#
#============================================================
# Number of mixture components
#============================================================
#
set nmix = 2
#
#============================================================
# State metainfo
#============================================================
#
set si = new StateInfo(ndim, nmix)
#
#============================================================
# Minimum number of temperature levels
#============================================================
#
set minmc = 20
#
#============================================================
# Maximum number of steps
#============================================================
#
set nsteps = 20000
#
#============================================================
# Additional scaling factor for proposal function
#============================================================
#
set width = 0.1
#
#============================================================
# Maximum temperature (T=1 means no heating at all)
#============================================================
#
set maxT = 16.0
#
#============================================================
# Prior function
#============================================================
#
set pvec  = new clivectordist(2)
set unif  = new UniformDist(0.0, 1.0)
set weib  = new WeibullDist(0.025, 1.0, 0.0001, 10.0)
pvec->setval(0, unif)
pvec->setval(1, weib)
set prior = new InitialMixturePrior(si, 1.0, pvec)
#
#============================================================
# Metropolis-Hastings width
#============================================================
#
set mca = new MetropolisHastings()
set mvec = new clivectord(3, 0.001)
mvec->setval(1, 0.01)
set mhwidth = new MHWidthOne(si, mvec)
#
#============================================================
# Define the simulation
#============================================================
#
set like   = new LikelihoodComputationSerial()
set sstat  = new EnsembleStat(si)
set convrg = new CountConverge(nsteps, sstat)
set sim    = new TemperedSimulation(si, minmc, maxT, mhwidth, convrg, prior, like, mca)
#
#============================================================
# Define the model through the user-defined likelihood
#============================================================
#
set fct = new GaussTestLikelihoodFunction()
sim->SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
run->Run()
#
