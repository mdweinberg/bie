#============================================================
#
# Example: 10d test using slice sampling
#
#============================================================
#
#============================================================
# Logfile name
#============================================================
#
set runtag = "run16"
set outfile = "run16.statelog"
#
#============================================================
# Number of dimensions
#============================================================
#
set ndim = 10
#
#============================================================
# State metadata
#============================================================
#
set si  = new StateInfo(ndim)
#
#============================================================
# Number of steps
#============================================================
#
set nsteps  = 2000000
#
#============================================================
# Width scaling (not used here)
#============================================================
#
set width = 0.1
#
#============================================================
# Prior
#============================================================
#
set vmin = -3.0
set vmax =  2.0
set unif0 = new UniformDist (vmin,  vmax)
set unif1 = new UniformDist (-30.0, 30.0)
set dist = new clivectordist(ndim, unif1)
    dist->setval(0, unif0)
set prior = new Prior(si, dist)
#
#============================================================
# Likelihood function
#============================================================
#
set fct = new FunnelTest(ndim, vmin, vmax)
#
#============================================================
# The nominal model
#============================================================
#
set fct = new FunnelTest(10, -3.0, 2.0)
#
#============================================================
# Parameter labels
#============================================================
#
si->labelFromLike(fct)
#
#============================================================
# Ensemble
#============================================================
#
set sstat = new EnsembleDisc(si)
#
#============================================================
# Gelman Rubin
#============================================================
#
# set convrg = new SubsampleConverge(nsteps, sstat, "0")
set convrg = new GelmanRubinConverge(500, sstat, "run6")
    convrg->setAlpha(0.05)
    convrg->setNskip(500)
    convrg->setNoutlier(500)
    convrg->setPoffset(-30.0)
    convrg->setMaxout(6)
#
#============================================================
# Monte Carlo algorithm
#============================================================
#
set mca = new StandardMC()
#
#============================================================
# Simple simulation
#============================================================
#
set ninit = 20000
set wfac  = 0.05
set like  = new LikelihoodComputationSerial()
set sim = new SliceSampler(ninit, wfac, si, convrg, prior, like, mca)
    sim->clampingOn()
    sim->SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#============================================================
# Compute distribribution and new prior
#============================================================
#
set discard = convrg->ConvergedIndex()
    print discard
    sstat->ComputeDistribution(discard)
    sstat->printWidth()
