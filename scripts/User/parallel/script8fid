#============================================================
#
# Example: two-dimensional PARALLEL CHAINS run
#
#============================================================
#
#============================================================
# Tag for output files
#============================================================
#
set nametag = "run8fid"
#
#============================================================
# Logfile name
#============================================================
#
set outfile = "run8fid.statelog"
#
#============================================================
# Model dimension
#============================================================
#
set ndim = 2
#
#============================================================
# Number of mixture components
#============================================================
#
set nmix = 2
#
#============================================================
# State metadata
#============================================================
#
set si = new StateInfo(nmix, ndim)
#
#============================================================
# Minimum number of temperature levels
#============================================================
#
set minmc = 16
#
#============================================================
# Maximum number of steps
#============================================================
#
set nsteps = 500
set nburn  = 1000
#
#============================================================
# Proposal function scaling factor
#============================================================
#
set width = 0.1
#
#============================================================
# Maximum temperature (T=1 means no heating at all)
#============================================================
#
set maxT = 16.0
#
#============================================================
# Prior function
#============================================================
#
set unif  = new UniformDist(0.0, 1.0)
set weib  = new WeibullDist(0.025, 1.0, 0.0001, 10.0)
set vdist = new clivectordist(2)
    vdist->setval(0, unif)
    vdist->setval(1, weib)
set alpha = 1.0
set prior = new InitialMixturePrior(si, alpha, vdist)
#
#============================================================
# The Ensemble (here discrete chain sampler)
#============================================================
#
// set sstat = new EnsembleDisc(si)
set sstat = new EnsembleStat(si)
#
#============================================================
# Convergence routine (comment one out)
#============================================================
#
set convrg = new SubsampleConverge(nsteps, sstat, "0")
# set convrg = new CountConverge(nsteps, sstat, nburn)
#
#============================================================
# Define the M-H proposal function
#============================================================
#
set mvec = new clivectord(3, 0.01)
    mvec->setval(0, 0.001)
set mhwidth = new MHWidthOne(si, mvec)
#
#============================================================
# Define the simulation method
#============================================================
#
set like = new LikelihoodComputationSerial()
set mca  = new MetropolisHastings()
set sim  = new ParallelChains(si, minmc, maxT, mhwidth, convrg, prior, like, mca)
#
# Set to scalar likelihood
    sim->SetControl(1)
#
#   sim->SetAlgorithm(1)
#
#============================================================
# Define the model through the user-defined likelihood
#============================================================
#
set fct = new GaussTestLikelihoodFunctionMulti(9, 10000, 6)
    fct->SetDim(2)
    sim->SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
set current_level = 0
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#============================================================
# Compute distribribution and new prior
#============================================================
#
sstat->ComputeDistribution()
sstat->printWidth()
set mhwidth = new MHWidthEns(si, sstat)
    mhwidth->printWidth()
set nprior = new PostMixturePrior(prior, sstat)
// set nstat = new EnsembleDisc(si)
set nstat = new EnsembleStat(si)
set convrg = new SubsampleConverge(nsteps, nstat, "1")
# set convrg = new CountConverge(nsteps, nstat, nburn)
#============================================================
# New simulation
#============================================================
#
set nsim = new ParallelChains(si, minmc, maxT, mhwidth, convrg, nprior, like, mca, sim)
    nsim->SetControl(1)
    nsim->SetUserLikelihood(fct)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
set current_level = 1
set width = 0.001
set run = new RunOneSimulation(nsteps, width, nstat, nprior, nsim)
    run->Run()
#============================================================
# Compute distribribution and new prior
#============================================================
#
nstat->ComputeDistribution()
nstat->printWidth()
set mhwidth = new MHWidthEns(si, nstat)
    mhwidth->printWidth()
set prior = new PostMixturePrior(nprior, nstat)
// set sstat = new EnsembleDisc(si)
set sstat = new EnsembleStat(si)
set convrg = new SubsampleConverge(nsteps, sstat, "2")
# set convrg = new CountConverge(nsteps, sstat, nburn)
#============================================================
# New simulation
#============================================================
#
set sim = new ParallelChains(si, minmc, maxT, mhwidth, convrg, prior, like, mca, nsim)
    sim->SetControl(1)
    sim->SetUserLikelihood(fct)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
set current_level = 2
set width = 0.001
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
run->Run()
#============================================================
# Compute distribribution and new prior
#============================================================
#
sstat->ComputeDistribution()
sstat->printWidth()
set mhwidth = new MHWidthEns(si, sstat)
    mhwidth->printWidth()
set nprior = new PostMixturePrior(prior, sstat)
// set nstat = new EnsembleDisc(si)
set nstat = new EnsembleStat(si)
set convrg = new SubsampleConverge(nsteps, nstat, "3")
# set convrg = new CountConverge(nsteps, nstat, nburn)
#============================================================
# New simulation
#============================================================
#
set nsim = new ParallelChains(si, minmc, maxT, mhwidth, convrg, nprior, like, mca, sim)
    nsim->SetControl(1)
    nsim->SetUserLikelihood(fct)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
set current_level = 3
set width = 0.001
set run = new RunOneSimulation(nsteps, width, nstat, nprior, nsim)
    run->Run()
