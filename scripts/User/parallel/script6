#============================================================
#
# Example: 4d test using slice sampling
#
#============================================================
#
#============================================================
# Logfile name
#============================================================
#
set runtag = "run6"
set outfile = "run6.statelog"
#
#============================================================
# Number of dimensions
#============================================================
#
set ndim = 8
#
#============================================================
# State metadata
#============================================================
#
set si  = new StateInfo(ndim)
#
#============================================================
# Parameter labels
#============================================================
#
set labs = new clivectors(8)
    labs->setval(0, "Pos1(x)")
    labs->setval(1, "Var1(x)")
    labs->setval(2, "Pos2(y)")
    labs->setval(3, "Var3(y)")
    labs->setval(4, "Pos3(y)")
    labs->setval(5, "Var3(y)")
    labs->setval(6, "Pos4(y)")
    labs->setval(7, "Var4(y)")
    si->labelAll(labs)
#
#============================================================
# Number of steps
#============================================================
#
set nsteps  = 2000000
#
#============================================================
# Width scaling (not used here)
#============================================================
#
set width = 0.1
#
#============================================================
# Prior
#============================================================
#
set norm = new NormalDist (0.0,  1.0, -1.0,   1.0 )
set weib = new WeibullDist(0.03, 1.0, 0.0001, 1.0)
set dist = new clivectordist(8, norm)
    dist->setval(1, weib)
    dist->setval(3, weib)
    dist->setval(5, weib)
    dist->setval(7, weib)
set prior = new Prior(si, dist)
#
#
#============================================================
# The nominal model
#============================================================
#
set cvec = new clivectord(4, 0.0)
    cvec->setval(1, 0.05)
    cvec->setval(2, 0.10)
    cvec->setval(3, 0.15)
set vvec = new clivectord(4, 0.03)
set fct = new GaussTestMultiD(1000, 1, cvec, vvec)
    fct->SetDim(2)
#
#============================================================
# Ensemble
#============================================================
#
set sstat = new EnsembleDisc(si)
#
#============================================================
# Gelman Rubin
#============================================================
#
# set convrg = new SubsampleConverge(nsteps, sstat, "0")
set convrg = new GelmanRubinConverge(500, sstat, "run6")
    convrg->setAlpha(0.05)
    convrg->setNskip(500)
    convrg->setNoutlier(500)
    convrg->setPoffset(-30.0)
    convrg->setMaxout(6)
#
#============================================================
# Monte Carlo algorithm
#============================================================
#
set mca = new StandardMC()
#
#============================================================
# Simple simulation
#============================================================
#
set ninit = 200000
set wfac  = 0.05
set like  = new LikelihoodComputationSerial()
set sim = new SliceSampler(ninit, wfac, si, convrg, prior, like, mca)
    sim->SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#============================================================
# Compute distribribution and new prior
#============================================================
#
set discard = convrg->ConvergedIndex()
    print discard
    sstat->ComputeDistribution(discard)
    sstat->printWidth()
