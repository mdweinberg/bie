#============================================================
#
# Example: The Han-Carlin hierarchical longititudinal model
#          [Model 1]
#
#============================================================
#
#============================================================
# Tag for output files
#============================================================
#
set nametag = "runHLM1"
#
#============================================================
# Logfile name
#============================================================
#
set outfile = "runHLM1.statelog"
#
#============================================================
# Define the model through the user-defined likelihood
# Data file and Model 1
#============================================================
#
set fct = new HLM("../data/ddIddClong_data.txt", 1)
#
#============================================================
# Number of components
# dimF is the number of fixed dimensions
# dimS is the number of patient (variable effect) dimensions
#============================================================
#
set dimF = fct->fixedDim()
set dimS = fct->subjectDim()
set ndim = fct->Dim()
#
print "Number of fixed effect dimensions:"
print dimF
print "Number of variable effect dimensions:"
print dimS
print "Total number of dimensions:"
print ndim
#
#============================================================
# State metadata
#============================================================
#
set si = new StateInfo(ndim)
#
#============================================================
# Minimum number of temperature levels
#============================================================
#
set minmc = 8
#
#============================================================
# Maximum number of steps
#============================================================
#
set nsteps = 100000
set nburn  = 10000
#
#============================================================
# Proposal function scaling factor
#============================================================
#
set width = 0.1
#
#============================================================
# Maximum temperature (T=1 means no heating at all)
#============================================================
#
set maxT = 16.0
#
#============================================================
# Prior function
#============================================================
#
# Variance component (#0)
#
set invg  = new InverseGammaDist(3.0, 0.005)
#
# Fixed effects (#1-#9)
#
set nrm = new NormalDist(0.0,  1.0);
set vdist = new clivectordist(dimF, nrm)
    vdist->setval(0, invg)
    set nrm = new NormalDist(10.0, 4.0);
    vdist->setval(1, nrm);
    set nrm = new NormalDist(0.0,  0.01)
    vdist->setval(4, nrm);
    set nrm = new NormalDist(-3.0, 1.0);
    vdist->setval(7, nrm);
#
# Variable effects (#>=10)
#
set diag = new clivectord(3, 1.5)
    diag->setval(0, 96.0)
set wish = new MultiNWishartDist(24.0, diag, dimS)
    vdist->append(wish)
#
# Instantiate the final prior
#
set prior = new Prior(si, vdist)
#
#============================================================
# The Ensemble (here discrete chain sampler)
#============================================================
#
set sstat = new EnsembleDisc(si)
#
#============================================================
# Convergence routine (comment one out)
#============================================================
#
set convrg = new CountConverge(nsteps, sstat, nburn)
#
#============================================================
# Define the M-H proposal function
#============================================================
#
set mvec = new clivectord(ndim, 0.04)
#    mvec->setval(0, 0.001)
set mhwidth = new MHWidthOne(si, mvec)
#
#============================================================
# Define the simulation method
#============================================================
#
set mca  = new MetropolisHastings()
set like = new LikelihoodComputationSerial()
set sim  = new ParallelChains(si, minmc, maxT, mhwidth, convrg, prior, like, mca)
#
# Set to scalar likelihood
    sim->SetControl(1)
#   sim->SetAlgorithm(1)
#
#============================================================
# Define the model through the user-defined likelihood
#============================================================
#
sim->SetUserLikelihood(fct)
#
#============================================================
# Run the simulation
#============================================================
#
set current_level = 4
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
run->Run()
