#!/bin/bash

exec_name=$_

if [ ! $1 ]; then
    echo "Usage: "$exec_name" <number of processes>"
    exit -1
fi
j=`echo $1 | sed 's/[0-9]*//g'`
if [ "$j" != "" ]; then
    echo "Usage: "$exec_name" <number of processes>"
    exit -1
fi

echo "==========================================="
echo "This tests checkpointing of a simple test"
echo "simulation by comparing the results of the"
echo "chain output without checkpointing and"
echo "after saving and restoring"
echo "==========================================="
echo "removing files from previous test session.."
echo "==========================================="
rm -rf pdir/test8 pdir/test8_2 slave.output* run8*statelog script8*out

echo "==========================================="
echo "running script8fid with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script8fid > script8fid.out

echo "==========================================="
echo "running script8part1 with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script8part1 > script8part1.out

echo "==========================================="
echo "running script8part2 with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script8part2 > script8part2.out

echo "==========================================="
echo "diffing..."
echo "==========================================="
diff run8fid.statelog run8p12.statelog

if [ "$?" -eq "0" ]; then
    echo "Test : SUCCESS"
else
    echo "Test : FAILED"
fi
