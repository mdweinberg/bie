#!/bin/bash

if [ ! $1 ]; then
    echo "Usage: "$0" <number of processes>"
    exit -1
fi
j=`echo $1 | sed 's/[0-9]*//g'`
if [ "$j" != "" ]; then
    echo "Usage: "$0" <number of processes>"
    exit -1
fi

echo "==========================================="
echo "This tests checkpointing of a simple test"
echo "simulation by comparing the results of the"
echo "chain output without checkpointing and"
echo "after saving and restoring"
echo "==========================================="
echo "removing files from previous test session.."
echo "==========================================="
rm -rf pdir/test9 pdir/test9_2 slave.output* run9* script9*out

echo "==========================================="
echo "running script9 with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script9 > script9.out

echo "==========================================="
echo "running script9part1 with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script9part1 > script9part1.out

echo "==========================================="
echo "running script9part2 with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script9part2 > script9part2.out

echo "==========================================="
echo "diffing..."
echo "==========================================="
diff run9.statelog run9p12.statelog

if [ "$?" -eq "0" ]; then
    echo "Test : SUCCESS"
else
    echo "Test : FAILED"
fi
