#!/bin/bash

exec_name=$_

if [ ! $1 ]; then
    echo "Usage: "$exec_name" <number of processes>"
    exit -1
fi
j=`echo $1 | sed 's/[0-9]*//g'`
if [ "$j" != "" ]; then
    echo "Usage: "$exec_name" <number of processes>"
    exit -1
fi

echo "==========================================="
echo "This tests checkpointing of a simple test"
echo "simulation by comparing the results of the"
echo "chain output without checkpointing and"
echo "after saving and restoring"
echo "==========================================="
echo "removing files from previous test session.."
echo "==========================================="
rm -rf pdir/run20 pdir/run20pt2 slave.output* run20*statelog script20*out

echo "==========================================="
echo "running script20fid with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script20fid > script20fid.out

echo "==========================================="
echo "running script20pt1 with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script20pt1 > script20pt1.out

echo "==========================================="
echo "running script8pt2 with "$1" processes"
echo "==========================================="
mpirun -np $1 bie -f script20pt2 > script20pt2.out

echo "==========================================="
echo "diffing..."
echo "==========================================="
diff run20.statelog run20pt12.statelog

if [ "$?" -eq "0" ]; then
    echo "Test : SUCCESS"
else
    echo "Test : FAILED"
fi
