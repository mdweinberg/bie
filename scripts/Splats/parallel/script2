#------------------------------------------------------------------------
# Serialization
#------------------------------------------------------------------------
#
pnewsession run2
#
#------------------------------------------------------------------------
# Prefix for diagnostic output files
#------------------------------------------------------------------------
#
set nametag = "run2"
#
#------------------------------------------------------------------------
# The chain output file
#------------------------------------------------------------------------
#
set outfile = "run2.statelog"
#
#------------------------------------------------------------------------
# The model dimension and number of components in the mixture
#------------------------------------------------------------------------
#
set ndim = 2
set nmix = 2
set si   = new StateInfo(nmix, ndim)
#
#------------------------------------------------------------------------
# The number of node levels in the tessellation
#------------------------------------------------------------------------
#
set tessdepth = 6
#
#------------------------------------------------------------------------
# The minimum number of temperature levels and the maximum temperature
# for the tempered states simulation
#------------------------------------------------------------------------
#
set minmc = 16
set maxT  = 16.00
#
#------------------------------------------------------------------------
# The target knot spacing for the Gauss-Legendre 2-d integrator
#------------------------------------------------------------------------
#
set dxy = 0.1
#
#------------------------------------------------------------------------
# The maximum number of steps per multiresolution level
#------------------------------------------------------------------------
#
set nsteps = 200
#
#------------------------------------------------------------------------
# Scale factor for the variance estimated widths at successive levels
#------------------------------------------------------------------------
#
set width = 0.1
#
#------------------------------------------------------------------------
# The MODEL
#------------------------------------------------------------------------
#
set mod = new SplatModel(ndim, nmix)
    si->labelFromModel(mod)
#
#------------------------------------------------------------------------
# The data distribution
#------------------------------------------------------------------------
#
set hist = new Histogram1D(0.0,10.0,10.0,"attribute")
#
#------------------------------------------------------------------------
# The initial prior distribution
#------------------------------------------------------------------------
#
set unif = new UniformDist(-0.5, 1.5)
set pvec = new clivectordist(2, unif)
#
set prior = new InitialMixturePrior(si, 1.0, pvec)
#
#------------------------------------------------------------------------
# The posterior distribution estimator
#------------------------------------------------------------------------
#
set sstat = new EnsembleStat(si)
#
#------------------------------------------------------------------------
# The tile type
#------------------------------------------------------------------------
#
set tile = new SquareTile()
#
#------------------------------------------------------------------------
# The integrator over x-y to generate estimates for the data distribution
#------------------------------------------------------------------------
#
set intgr = new AdaptiveLegeIntegration(dxy, dxy)
#
#------------------------------------------------------------------------
# Assess the convergce of the chain 
# (CountConverge simply counts to nsteps)
#------------------------------------------------------------------------
#
set convrg = new CountConverge(nsteps, sstat)
#
#------------------------------------------------------------------------
# The spatial tesselation type
#------------------------------------------------------------------------
#
set tess = new QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, tessdepth)
#
#------------------------------------------------------------------------
# Populate the tesselation with data distributions
#------------------------------------------------------------------------
#
set ris = new RecordInputStream_Ascii("../data/splat.data.2");
set dis = new DataTree(ris,hist,tess)
#
#------------------------------------------------------------------------
# The likelihood computation
#------------------------------------------------------------------------
#
set like = new LikelihoodComputationMPI(si)
#
#------------------------------------------------------------------------
# The Monte Carlo algorithm
#------------------------------------------------------------------------
#
set mca = new MetropolisHastings()
#
#------------------------------------------------------------------------
# The Metropolis-Hastings proposal function
#------------------------------------------------------------------------
#
set mvec  = new clivectord(3, 0.01)
mvec->setval(0, 0.002)
#
set mhwidth = new MHWidthOne(si, mvec)
#
#------------------------------------------------------------------------
# The simulation type
#------------------------------------------------------------------------
#
set frontier = dis->GetDefaultFrontier()
frontier->UpDownLevels(2)
set sim = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca)
#
#------------------------------------------------------------------------
# The multiresolution simulation driver
#------------------------------------------------------------------------
#
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
# like->IntegrationGranularity()
# run->Restart()
#------------------------------------------------------------------------
# Let the simulation begin!
#------------------------------------------------------------------------
#
run->Run()
psave
#
#------------------------------------------------------------------------
# Compute distribribution for Level 0 and new prior for Level 1
#------------------------------------------------------------------------
#
sstat->ComputeDistribution()
set mhwidth = new MHWidthEns(si, sstat)
mhwidth->printWidth()
set nprior = new PostMixturePrior(prior, sstat)
set nstat = new EnsembleStat(si)
set convrg = new CountConverge(nsteps, nstat)
set current_level = 1
frontier->UpDownLevels(1)
set nsim = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, nprior, like, mca, sim)
set width = 0.01
set run = new RunOneSimulation(nsteps, width, nstat, nprior, nsim)
#
#------------------------------------------------------------------------
# Let the simulation begin!
#------------------------------------------------------------------------
#
run->Run()
psave
#
#------------------------------------------------------------------------
# Compute distribribution for Level 1 and new prior for Level 2
#------------------------------------------------------------------------
#
nstat->ComputeDistribution()
set mhwidth = new MHWidthEns(si, nstat)
mhwidth->printWidth()
set prior = new PostMixturePrior(nprior, nstat)
set sstat = new EnsembleStat(si)
set convrg = new CountConverge(nsteps, sstat)
set current_level = 2
frontier->UpDownLevels(1)
set sim = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca, nsim)
set width = 0.01
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
#
#------------------------------------------------------------------------
# Let the simulation begin!
#------------------------------------------------------------------------
#
run->Run()
psave
