#------------------------------------------------------------------------
# Serialization
#------------------------------------------------------------------------
#
pnewsession run10
#
#------------------------------------------------------------------------
# Prefix for diagnostic output files
#------------------------------------------------------------------------
#
set nametag = "run10"
#
#------------------------------------------------------------------------
# The chain output file
#------------------------------------------------------------------------
#
set outfile = "run10.statelog"
#
#------------------------------------------------------------------------
# The model dimension and number of components in the mixture
#------------------------------------------------------------------------
#
set ndim = 2
set nmix = 10
set si = new StateInfo(nmix, ndim)
#
#------------------------------------------------------------------------
# The number of node levels in the tessellation
#------------------------------------------------------------------------
#
set tessdepth = 6
#
#------------------------------------------------------------------------
# The minimum number of temperature levels and the maximum temperature
# for the tempered states simulation
#------------------------------------------------------------------------
#
set minmc = 8
set maxT = 16.0
#
#------------------------------------------------------------------------
# The maximum number of steps per multiresolution level
#------------------------------------------------------------------------
#
set nsteps = 10000
#
#------------------------------------------------------------------------
# The target knot spacing for the Gauss-Legendre 2-d integrator
#------------------------------------------------------------------------
#
set dxy = 0.02
#
#------------------------------------------------------------------------
# Scale factor for the variance estimated widths at successive levels
#------------------------------------------------------------------------
#
set width = 0.1
#
#------------------------------------------------------------------------
# The MODEL
#------------------------------------------------------------------------
#
set mod = new SplatModel(ndim, nmix)
    si->labelFromModel(mod)
#
#------------------------------------------------------------------------
# The data distribution
#------------------------------------------------------------------------
#
set hist = new Histogram1D(0.0,10.0,10.0,"attribute")
#
#------------------------------------------------------------------------
# The initial prior distribution
#------------------------------------------------------------------------
#
set unif = new UniformDist(-0.5, 1.5)
set pvec = new clivectordist(2, unif)
set prior = new InitialMixturePrior(si, 1.0, pvec)
    prior->setMinMaxWeight(0.02, 0.98)
#
#------------------------------------------------------------------------
# The posterior distribution estimator
#------------------------------------------------------------------------
#
set sstat = new EnsembleStat(si)
#
#------------------------------------------------------------------------
# The tile type
#------------------------------------------------------------------------
#
set tile = new SquareTile()
#
#------------------------------------------------------------------------
# The integrator over x-y to generate estimates for the data distribution
#------------------------------------------------------------------------
#
set intgr = new AdaptiveLegeIntegration(dxy, dxy)
#
#------------------------------------------------------------------------
# Assess the convergce of the chain 
# (CountConverge simply counts to nsteps)
#------------------------------------------------------------------------
#
set convrg = new SubsampleConverge(nsteps, sstat, "")
    convrg->setNgood(400)
    convrg->setRcovTol(0.97)
#
#------------------------------------------------------------------------
# The spatial tesselation type
#------------------------------------------------------------------------
#
set tess = new QuadGrid(tile, -1.0, 2.0, -1.0, 2.0, tessdepth)
#
#------------------------------------------------------------------------
# Populate the tesselation with data distributions
#------------------------------------------------------------------------
#
set ris = new RecordInputStream_Ascii("../data/splat.data.2");
set dis = new DataTree(ris,hist,tess)
#
#------------------------------------------------------------------------
# The likelihood computation
#------------------------------------------------------------------------
#
set like = new LikelihoodComputationMPI(si)
#
#------------------------------------------------------------------------
# The Monte Carlo algorithm
#------------------------------------------------------------------------
#
set mca = new ReversibleJump(nmix, 1.0)
#
#------------------------------------------------------------------------
# The Metropolis-Hastings proposal function
#------------------------------------------------------------------------
#
set mvec = new clivectord(3, 0.01)
    mvec->setval(0, 0.002)
#
set mhwidth = new MHWidthOne(si, mvec)
#
#------------------------------------------------------------------------
# The simulation type
#------------------------------------------------------------------------
#
set frontier = dis->GetDefaultFrontier()
    frontier->UpDownLevels(3)
set sim = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca)
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#------------------------------------------------------------------------
# Compute distribribution for Level 0 and new prior for Level 1
#------------------------------------------------------------------------
#
psave
sstat->ComputeDistribution()
set nprior = new PostMixturePrior(prior, sstat)
set nstat = new EnsembleStat(si)
set convrg = new SubsampleConverge(nsteps, nstat, "")
    convrg->setNgood(400)
    convrg->setRcovTol(0.97)
set current_level = 1
frontier->UpDownLevels(1)
set nsim = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, nprior, like, mca, sim)
set width = 0.01
set run = new RunOneSimulation(nsteps, width, nstat, nprior, nsim)
    run->Run()
#
#------------------------------------------------------------------------
# Compute distribribution for Level 1 and new prior for Level 2
#------------------------------------------------------------------------
#
psave
nstat->ComputeDistribution()
set prior = new PostMixturePrior(nprior, nstat)
set sstat = new EnsembleStat(si)
set convrg = new SubsampleConverge(nsteps, sstat, "")
    convrg->setNgood(400)
    convrg->setRcovTol(0.97)
set current_level = 2
frontier->UpDownLevels(1)
set sim = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca, nsim)
set width = 0.01
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
psave
#------------------------------------------------------------------------
# Done
#------------------------------------------------------------------------
