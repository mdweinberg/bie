#==================================================
#
# Particle Filter test
#
#==================================================
# Persistence
#==================================================
#
pnewsession run3
cktimer 3600
ckon

#
#==================================================
# Log files
#==================================================
#
set nametag    = "run3"
set outfile    = "pfsim3t.statelog"
#
#==================================================
# Algorithmic parameters
#==================================================
#
set ndim       = 2
set nmix       = 2
set nsteps     = 0
set npart      = 2000
set bsize      = 20
#
#==================================================
# Model
#==================================================
#
set dist       = new PointDistribution("mag")
set mod        = new GalaxyModelOneD(ndim,nmix,dist)
    mod->SetKnots(200)
    mod->CacheLimit(bsize)
#
#==================================================
# State metadata
#==================================================
#
set si         = new StateInfo(nmix, ndim)
    si->labelFromModel(mod)    
#
#==================================================
# Prior distribution
#==================================================
#
set uni1       = new UniformDist(1.0, 8.0)
set uni2       = new UniformDist(100.0, 1200.0)
set pvec       = new clivectordist(2)
    pvec->setval(0, uni1)
    pvec->setval(1, uni2)
set prior      = new InitialMixturePrior(si, 1.0, pvec)
#
#==================================================
# Tessellation
#==================================================
#
set tile       = new PointTile()
set ris1       = new RecordInputStream_Ascii("../data/galaxy.data.4r")
set ris2       = new RecordInputStream_Ascii("../data/galaxy.data.4r")
set intgr      = new PointIntegration()
set tess       = new CursorTessellation(ris1, bsize, -3.14159, 3.14159, -1.5708, 1.5708)
set dis        = new DataTree(ris2, dist, tess)
#
#==================================================
# Set up for simulation
#==================================================
#
set sstat       = new EnsembleStat(si)
set convrg      = new CountConverge(nsteps, sstat)
set like        = new LikelihoodComputationSerial()
set mca         = new MetropolisHastings()
set mvec        = new clivectord(3)
    mvec->setval(0, 0.1)
    mvec->setval(1, 0.1)
    mvec->setval(2, 10.0)
set mhwidth     = new MHWidthOne(si, mvec)
set sim         = new Simulation(si, mhwidth, dis, mod, intgr, convrg, prior, like, mca)
set run         = new PFSimulation(nsteps, npart, sim, sstat)
    run->Nsample(3)
    run->Run()
