#==================================================
# Persistence
#==================================================
#
pnewsession run3
cktimer 3600
ckon
#
#==================================================
# Log files
#==================================================
#
set nametag    = "run3"
set outfile    = "run3.statelog"
#
#==================================================
# Algorithmic parameters
#==================================================
#
set minmc      = 8
set ndim       = 2
set nmix       = 2
set nsteps     = 1000
set width      = 0.1
set maxT       = 16.0
#
#==================================================
# Model
#==================================================
#
set dist       = new PointDistribution("attr")
set mod        = new GalaxyModelOneD(ndim,nmix,dist)
    mod->SetKnots(200)
#
#==================================================
# State metadata
#==================================================
#
set si         = new StateInfo(nmix, ndim)
    si->labelFromModel(mod)    
#
#==================================================
# Prior distribution
#==================================================
#
set uni1       = new UniformDist(1.0, 8.0)
set uni2       = new UniformDist(100.0, 1200.0)
set pvec       = new clivectordist(2)
    pvec->setval(0, uni1)
    pvec->setval(1, uni2)
set prior      = new InitialMixturePrior(si, 1.0, pvec)
#
#==================================================
# Tessellation
#==================================================
#
set sstat      = new EnsembleStat(si)
set tile       = new PointTile()
set ris1       = new RecordInputStream_Ascii("../data/galaxy.data.2")
set ris2       = new RecordInputStream_Ascii("../data/galaxy.data.2")
set intgr      = new PointIntegration()
set convrg     = new SubsampleConverge(nsteps, sstat, "")
set tess       = new PointTessellation(tile, ris1, -3.14159, 3.14159, -0.2, 0.2)
set dis        = new DataTree(ris2,dist,tess)
set frontier   = dis->GetDefaultFrontier()
    frontier->UpDownLevels(6)
#
#==================================================
# Set up for simulation
#==================================================
#
set like       = new LikelihoodComputationMPI(si)
set mca        = new MetropolisHastings()
set mvec       = new clivectord(3, 0.1)
    mvec->setval(2, 10.0)
set mhwidth    = new MHWidthOne(si, mvec)
set sim        = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca)
set run        = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
