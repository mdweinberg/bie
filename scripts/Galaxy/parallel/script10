#============================================================
# Set a new session
#============================================================
#
pnewsession run10
cktoggle
cktimer 100
#
#============================================================
# DE randomization file
#============================================================
#
set uc1 = new CauchyDist(0.0005)
set uc2 = new CauchyDist(0.002, -10.0, 10.0)
set uc3 = new UniformDist(0.002)
set eps = new clivectordist(3)
    eps->setval(0, uc1)
    eps->setval(1, uc2)
    eps->setval(2, uc3)
#
#============================================================
# Minimum number of chains
#============================================================
#
set mchains = 32
#
#============================================================
# Logfile name
#============================================================
#
set outfile = "run10.statelog"
#
#
#============================================================
# Control files and parameters
#============================================================
#
set ndim = 2
set nmix = 2
set nsteps = 80000
set bsize = 2000
#
#============================================================
# Width scaling
#============================================================
#
set width = 0.1
#
#============================================================
# Model
#============================================================
#
set rt = new RecordType
set rt = rt->insertField(1, "mag", real)
set dist = new PointDistribution(rt)
set mod = new GalaxyModelOneD(ndim, nmix, dist)
    mod->SetKnots(200)
    mod->CacheLimit(bsize)
set si  = new StateInfo(nmix, ndim)
    si->labelFromModel(mod)    
    si->setOrdering(0)
#
#============================================================
# Prior distribution
#============================================================
#
set uni1 = new UniformDist(0.2, 8.0)
set uni2 = new UniformDist(50.0, 2000.0)
set pvec = new clivectordist(2)
    pvec->setval(0, uni1)
    pvec->setval(1, uni2)
set prior = new InitialMixturePrior(si, 1.0, pvec)
#
#============================================================
# Posterior ensemble
#============================================================
#
set sstat = new EnsembleDisc(si)
#
#============================================================
# Data streams
#============================================================
#
set ris1 = new RecordInputStream_Ascii("../data/galaxy.data.4r")
set ris2 = new RecordInputStream_Ascii("../data/galaxy.data.4r")
#
#============================================================
# Dummy integration
#============================================================
#
set intgr = new PointIntegration()
#
#============================================================
# Gelman-Rubin
#============================================================
#
set convrg = new GelmanRubinConverge(500, sstat, "run10.0")
convrg->setAlpha(0.05)
convrg->setNskip(500)
convrg->setNoutlier(500)
convrg->setPoffset(-30.0)
convrg->setMaxout(6)
#
#============================================================
# A simple square tile
#============================================================
#
set tile = new SquareTile()
#
#============================================================
# Simple tessellation
#============================================================
#
set tess = new PointTessellation(tile, ris1, -3.14159, 3.14159, -1.5708, 1.5708)
set dis = new DataTree(ris2, dist, tess)
set like = new LikelihoodComputationMPI(si)
#
#============================================================
# Metropolis-Hastings Monte Carlo algorithm
#============================================================
#
set mca = new MetropolisHastings()
#
#============================================================
# Set the frontier
#============================================================
#
set frontier = dis->GetDefaultFrontier()
    frontier->RetractToTopLevel()
    frontier->UpDownLevels(8)
    frontier->printSize()
#
#============================================================
# Differential evolution
#============================================================
#
set sim = new DifferentialEvolution(si, mchains, eps, dis, mod, intgr, convrg, prior, like, mca)
    sim->SetLinearMapping(1)
    sim->SetJumpFreq(10)
    sim->SetControl(0)
    sim->NewGamma(0.75)
#
#============================================================
# Run the simulation
#============================================================
#
set current_level = 0
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#============================================================
# Next level:
# Compute distribribution and new prior
#============================================================
#
set discard = convrg->ConvergedIndex()
print discard
sstat->ComputeDistribution(discard)
sstat->printWidth()
set nprior = new PostMixturePrior(prior, sstat)
set nstat = new EnsembleDisc(si)
set convrg = new GelmanRubinConverge(500, nstat, "run10.1")
#
#============================================================
# Set the frontier
#============================================================
#
frontier->UpDownLevels(2)
frontier->printSize()
#
#============================================================
# New simulation
#============================================================
#
set nsim = new DifferentialEvolution(si, mchains, eps, dis, mod, intgr, convrg, nprior, like, mca, sim)
    nsim->SetLinearMapping(1)
    nsim->SetJumpFreq(10)
    nsim->SetControl(0)
    nsim->NewGamma(0.75)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
set current_level = 1
set width = 0.01
set run = new RunOneSimulation(nsteps, width, nstat, nprior, nsim)
    run->Run()
#============================================================
# Compute distribribution and new prior
#============================================================
set discard = convrg->ConvergedIndex()
print discard
nstat->ComputeDistribution(discard)
nstat->printWidth()
set prior = new PostMixturePrior(prior, nstat)
set sstat = new EnsembleDisc(si)
set convrg = new GelmanRubinConverge(500, sstat, "run10.2")
#
#============================================================
# Set the frontier
#============================================================
#
frontier->UpDownLevels(2)
frontier->printSize()
#
#============================================================
# New simulation
#============================================================
#
set sim = new DifferentialEvolution(si, mchains, eps, dis, mod, intgr, convrg, prior, like, mca, nsim)
    sim->SetLinearMapping(1)
    sim->SetJumpFreq(10)
    sim->SetControl(0)
    sim->NewGamma(0.75)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
set current_level = 2
set width = 0.01
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#============================================================
# Next level:
# Compute distribribution and new prior
#============================================================
#
set discard = convrg->ConvergedIndex()
print discard
sstat->ComputeDistribution(discard)
sstat->printWidth()
set nprior = new PostMixturePrior(prior, sstat)
set nstat = new EnsembleDisc(si)
set convrg = new GelmanRubinConverge(500, nstat, "run10.3")
#
#============================================================
# Set the frontier
#============================================================
#
frontier->UpDownLevels(2)
frontier->printSize()
#
#============================================================
# New simulation
#============================================================
#
set nsim = new DifferentialEvolution(si, mchains, eps, dis, mod, intgr, convrg, nprior, like, mca, sim)
    nsim->SetLinearMapping(1)
    nsim->SetJumpFreq(10)
    nsim->SetControl(0)
    nsim->NewGamma(0.75)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
set current_level = 3
set width = 0.01
set run = new RunOneSimulation(nsteps, width, nstat, nprior, nsim)
    run->Run()
#============================================================
# Compute distribribution and new prior
#============================================================
set discard = convrg->ConvergedIndex()
print discard
nstat->ComputeDistribution(discard)
nstat->printWidth()
set prior = new PostMixturePrior(prior, nstat)
set sstat = new EnsembleDisc(si)
set convrg = new GelmanRubinConverge(500, sstat, "run10.4")
#
#============================================================
# Set the frontier
#============================================================
#
frontier->UpDownLevels(2)
frontier->printSize()
#
#============================================================
# New simulation
#============================================================
#
set sim = new DifferentialEvolution(si, mchains, eps, dis, mod, intgr, convrg, prior, like, mca, nsim)
    sim->SetLinearMapping(1)
    sim->SetJumpFreq(10)
    sim->SetControl(0)
    sim->NewGamma(0.75)
#
#============================================================
# The multiresolution simulation driver
#============================================================
#
set current_level = 4
set width = 0.01
set run = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#
#============================================================
# Done!
#============================================================
#
