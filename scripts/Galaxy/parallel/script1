#==================================================
# Persistence
#==================================================
#
pnewsession run1
cktimer 3600
ckon
#
#==================================================
# Log files
#==================================================
#
set nametag    = "run1"
set outfile    = "run1.statelog"
#
#==================================================
# Algorithmic parameters
#==================================================
#
set minmc      = 16
set ndim       = 2
set nmix       = 2
set nxy        = 5
set dx         = 0.05
set dy         = 0.005
set nsteps     = 1200
set width      = 0.1
set maxT       = 32.0
#
#==================================================
# Model
#==================================================
#
set hist       = new Histogram1D(6.0,15.0,1.0,"attr")
set mod        = new GalaxyModelOneD(ndim,nmix,hist)
    mod->SetKnots(200)
#
#==================================================
# State metadata
#==================================================
#
set si         = new StateInfo(nmix, ndim)
    si->labelFromModel(mod)    
#
#==================================================
# Prior distribution
#==================================================
#
set uni1       = new UniformDist(1.0, 8.0)
set uni2       = new UniformDist(100.0, 1200.0)
set pvec       = new clivectordist(2)
    pvec->setval(0, uni1)
    pvec->setval(1, uni2)
set prior      = new InitialMixturePrior(si, 1.0, pvec)
#
#==================================================
# Tessellation
#==================================================
#
set tile       = new SquareTile()
set ris        = new RecordInputStream_Ascii("../data/galaxy.data.2")
set intgr      = new AdaptiveLegeIntegration(dx, dy)
set tess       = new QuadGrid(tile, -3.14159, 3.14159, -0.1, 0.1, nxy)
set dis        = new DataTree(ris,hist,tess)
set frontier   = dis->GetDefaultFrontier()
    frontier->UpDownLevels(2)
#
#==================================================
# Set up for simulation
#==================================================
#
set sstat      = new EnsembleStat(si)
set convrg     = new SubsampleConverge(nsteps, sstat, "")
set like       = new LikelihoodComputationMPI(si)
# like->IntegrationGranularity()
set mca        = new MetropolisHastings()
set mvec       = new clivectord(3, 0.1)
    mvec->setval(2, 10.0)
set mhwidth    = new MHWidthOne(si, mvec)
set sim        = new TemperedSimulation(si, minmc, maxT, mhwidth, dis, mod, intgr, convrg, prior, like, mca)
set run        = new RunOneSimulation(nsteps, width, sstat, prior, sim)
    run->Run()
#   run->RunThread()
# interactive
