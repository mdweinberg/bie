#ifndef _DATAFIELDBOX_H
#define _DATAFIELDBOX_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

#include <gtkmm.h>
#include <Spinners.h>

// Classes and structures for saving simulation output stream

struct DataRecord 
{
  vector<double> data;
};

class DataSet
{
private:

  unsigned ndim;

public:

  vector<double> datamin;
  vector<double> datamax;

  vector<DataRecord> data;

  DataSet() {
    ndim = 0;
  }

  DataSet(int Ndim) {
    ndim = Ndim;
    datamin = vector<double>(ndim,  1.0e30);
    datamax = vector<double>(ndim, -1.0e30);
  }
    
  DataSet(const DataSet &v)
  {
    ndim = v.ndim;
    datamin = v.datamin;
    datamax = v.datamax;
    data = v.data;
  }

  void operator+=(DataRecord& r) {
    for (unsigned i=0; i<ndim; i++) {
      datamin[i] = min<double>(r.data[i], datamin[i]);
      datamax[i] = max<double>(r.data[i], datamax[i]);
    }

    data.push_back(r);
  }

};


class SQLFieldBox : public Gtk::Frame
{
public:

  SQLFieldBox(std::string file);
  virtual ~SQLFieldBox();

  void addMenus(Gtk::HBox&, Gtk::HBox&);
  void fieldNumber(int);
  void setXRanges(double xmin, double xmax);
  void setYRanges(double ymin, double ymax);
  void getRanges(double& xmin, double& xmax, double& ymin, double& ymax);

  Gtk::HBox &get_x_menu() {return m_HBox_X;}
  Gtk::HBox &get_y_menu() {return m_HBox_Y;}


  void getData(std::vector<double>& X, std::vector<double>& Y,
	       std::string& xlabel, std::string& ylabel);

  void Reread() { parse_data_file(); }

 protected:

  DataSet Data;
  std::vector<std::string> fieldNames;
  
  DataRecord datarecord;
  std::string filename;

  Gtk::Menu m_Menu_X, m_Menu_Y;
  Gtk::HBox m_HBox_X, m_HBox_Y;

  void parse_data_file();
  void make_menus();

  int xpos, ypos;
  void on_menu_position_x(int i);
  void on_menu_position_y(int i);

  bool x_reread_range, y_reread_range;

  void on_reread_xrange();
  void on_reread_yrange();

  // Child widgets:

  Gtk::Frame m_Frame_fields;
  Gtk::Frame m_Frame_xaxis, m_Frame_yaxis;
  Gtk::Frame m_Frame_ranges, m_Frame_xrange, m_Frame_yrange;
  Gtk::Frame m_Frame_type;
  Gtk::VBox m_VBox_main, m_VBox_xranges, m_VBox_yranges;
  Gtk::HBox m_HBox_fields;
  Gtk::HBox m_HBox_ranges, m_HBox_xrange, m_HBox_yrange;

  Spinners my_spinners_x_min, my_spinners_y_min;
  Spinners my_spinners_x_max, my_spinners_y_max;

  Gtk::CheckButton m_Button_x_reread;
  Gtk::CheckButton m_Button_y_reread;
};

#endif //_DATAFIELDBOX_H
