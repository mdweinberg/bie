#ifndef _RENDERBOX_H
#define _RENDERBOX_H

#include <gtkmm.h>

class RenderBox : public Gtk::Frame
{
public:
  RenderBox();
  virtual ~RenderBox();

  int Contours() { return nContours; }
  void SetLevelLimits(double, double);
  void GetLevelValues(double& x, double& y) 
    { x = low; y = high; }

  bool Log() { return logarithm; }
  bool ReverseColors() { return reverse; }
  bool BlackAndWhite() { return blackwhite; }
  bool Threshold() { return threshold; }
  bool ApplyToField() { return applyfield; }
  bool RainbowMap() { return rainbowmap; }
  bool CDF() { return cdf; }

protected:
  //Signal handlers:
  virtual void on_log_toggled();
  virtual void on_reverse_toggled();
  virtual void on_blackwhite_toggled();
  virtual void on_threshold_toggled();
  virtual void on_applytofield_toggled();
  virtual void on_cdf_toggled();
  virtual void on_cmap_toggled();
  virtual void on_adjustment_contour_changed();
  virtual void on_adjustment_low_changed();
  virtual void on_adjustment_high_changed();

  // Child widgets:
  Gtk::VBox m_VBox_Top, m_VBox_levels;
  Gtk::HBox m_HBox_contour, m_HBox_buttons1, m_HBox_buttons2;
  Gtk::HBox m_HBox_low, m_HBox_high;

  Gtk::Adjustment m_adjustment_contour;
  Gtk::Adjustment m_adjustment_low, m_adjustment_high;

  Gtk::HScale m_Scale_contour, m_Scale_low, m_Scale_high;

  Gtk::CheckButton m_LogButton, m_ReverseButton, m_BlackWhiteButton;
  Gtk::CheckButton m_ThreshButton, m_ApplyFieldButton, m_CDFButton;
  Gtk::CheckButton m_RainbowButton;

  Gtk::Frame m_Frame_levels;

  bool logarithm, reverse, blackwhite, threshold, applyfield, cdf, rainbowmap;
  int nContours;
  double low, high;

};

#endif // _RENDERBOX_H
