
#ifndef _Denest2d_h
#define _Denest2d_h

using namespace std;

#include <vector>

enum OutputType {GNUPLOT, SM, VTK};

class Denest2d
{
 protected:

  double dX, dY, dkX, dkY;
  double xmin, xmax, ymin, ymax;
  int N1, N2;
  double H1, H2;
  double cost, sint;

  double F;
  int normVH;
  int Sflg;

  vector<double> X, Y;
  bool computed;

  vector<double> Prob, CumDF;
  bool computed_cdf;

  double pedestal;
  bool use_log;

  void readdata2d(int, int);
  void kernel2d(int, int, double**);
  void normal2d(int, int);
  double norm2d(int n1, int n2);

  int output_type;
  string outputname;

  int n1, n2;
  double **a, *w;
  int *ip;

  static double TINY;

 public:

  /*
  static const unsigned CDF_PRINT = 1;
  static const unsigned CDF_FILE = 2;
  static const unsigned NORM_ROWS = 1;
  static const unsigned NORM_COLUMNS = 2;
  */

  static string default_outputname;

  Denest2d(double xmin, double xmax, double ymin, double ymax,
	   vector<double>& x, vector<double>& y);


  ~Denest2d();

  void newData(double xmin, double xmax, double ymin, double ymax,
	       vector<double>& x, vector<double>& y);

  void set_output_type(int t) { 
    if (t == GNUPLOT)  output_type = t;
    else if (t == SM)  output_type = t;
    else if (t == VTK) output_type = t;
    else {
      cerr << "Illegal output_type\n";
      exit(-1);
    }
  }

  void set_log(bool x) { use_log = x; }
  void set_sflags(int flag) { Sflg = flag; }
  void set_nflags(int flag) { normVH = flag; }
  void set_N1(int n1) { N1 = n1; }
  void set_N2(int n2) { N2 = n2; }
  void set_F(double f) { F = f; }

  void set_pedestal(double x) { pedestal = x; }
  double get_pedestal() { return pedestal; }

  void compute(void);
  void output(ostream& out);
  void compute_cdf(void);
  void output_cdf(void);
  void get_cdf(vector<double>& v, vector<double>& cdf);

};

#endif
