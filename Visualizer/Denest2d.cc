/*
   Performs two-dimension kernel smoothing using fast (FFT) binning and
   and smoothing
  
   Computes optimal smoothing shape (based on MISE) assuming
   multivariate Gaussian distribution and kernel.  The kernel shape is
   chosen by diagonalizing the covariance matrix.  The automatically
   chosen smoothing length may be scaled using command line options.
   In particular, if the density is multimodal, the covariance
   estimate is likely to be larger than ideal.
  
   In this version, rotation based on covariance is turned off.
   Consistent with Wand & Jones, 1993, JASA, 88, 520, this seems
   to make matters worse.  The smoothing parameters H1 & H2 are
   still chosen in the diagonal basis but rotated back to original
   coordinates before application.

   MDWeinberg 03/05/99 
*/

using namespace std;

#include <stdlib.h>
#include <math.h>

#include <iostream>		// IOStream
#include <fstream>
#include <iomanip>

#include <string>		// from STL
#include <algorithm>
#include <vector>

				// FFT stuff
#include <cpp_alloc.h>

void cdft2d(int, int, int, double **, int *, double *);

#include <Denest2d.h>

				// ------------------------------------------
				// Global functions
				// ------------------------------------------

string Denest2d::default_outputname = "Denest2d";
double Denest2d::TINY = 1.0e-10;

Denest2d::Denest2d(double Xmin, double Xmax, double Ymin, double Ymax,
		   vector<double>& x, vector<double>& y)
{
  xmin = Xmin;
  xmax = Xmax;
  ymin = Ymin;
  ymax = Ymax;

  X = x;
  Y = y;

  N1 = 9;			// Number for x-grid
  N2 = 9;			// Number for y-grid

  F = 1.0;
  normVH = 0;
  Sflg = 0;
  computed = false;
  computed_cdf = false;

  use_log = false;
  pedestal = -8.0;

  outputname = default_outputname;
  output_type = 1;
}
  
void Denest2d::newData(double Xmin, double Xmax, 
		       double Ymin, double Ymax,
		       vector<double>& x, vector<double>& y)
{
  xmin = Xmin;
  xmax = Xmax;
  ymin = Ymin;
  ymax = Ymax;

  X = x;
  Y = y;

  computed = false;
  computed_cdf = false;
}


void Denest2d::compute(void)
{
  int n10 = 2 << (N1-1);
  int n20 = 2 << (N2-1);
				// zero padding for data (factor of two)
  n1 = n10/2;
  n2 = n20/2;

  dX = (xmax - xmin)/(n1-1);
  dY = (ymax - ymin)/(n2-1);
  dkX = 1.0/(n1*dX);
  dkY = 1.0/(n2*dY);

  a = alloc_2d_double(n10, 2*n20);
  int n = max<int>(n10, n20);
  ip = alloc_1d_int(2 + (int) sqrt(n + 0.5));
  n = max<int>(n10 / 2, n20 / 2) + max<int>(n10, n20);
  w = alloc_1d_double(n);

  ip[0] = 0;

				// ------------------------------------------
				// Begin computation
				// ------------------------------------------


				// Read in data
				// Store in real part
  readdata2d(n1, n2);

				// Compute kernel
				// Store in imaginary part
  kernel2d(n1, n2, a);

				// Do FFT
  cdft2d(n10, 2*n20, 1, a, ip, w);


				// Convolution
  for (int j1=0; j1<n10; j1++)
    for (int j2=0; j2<n20; j2++) {
      a[j1][2*j2] *= a[j1][2*j2+1];
      a[j1][2*j2+1] = 0.0;	// reduce noise for inverse
    }


				// Inverse to get grid
  cdft2d(n10, 2*n20, -1, a, ip, w);

				// Normalize on rows or columns
  if (normVH) normal2d(n1, n2);

  computed = true;

}

Denest2d::~Denest2d()
{
  free_1d_double(w);
  free_1d_int(ip);
  free_2d_double(a, n1);
}
				// ------------------------------------------
				// Read two columns of numbers from file
				// and compute bandwidth estimate by diag.
				// of covariance matrix
				// ------------------------------------------

void Denest2d::readdata2d(int n1, int n2)
{
  int indx1, indx2;
  double x, y;
  double A[2][2];
  double xm, ym, sx, sy, sxy;
  int n;

  n = 0;
  xm = ym = sx = sy = sxy = 0.0;

  for (int j1=0; j1<2*n1; j1++)
    for (int j2=0; j2<4*n2; j2++) a[j1][j2] = 0.0;

  for (unsigned  ii=0; ii<X.size(); ii++) {

    x = X[ii];
    y = Y[ii];

    if (x<xmin || x>xmax || y<ymin || y>ymax) continue;
    
    indx1 = (int)( (x-xmin)/dX );
    indx2 = (int)( (y-ymin)/dY );

				// Compute weights for linear binning

    A[0][0] = (xmin + dX*(indx1+1) - x)*(ymin + dY*(indx2+1) - y);
    A[1][0] = (x - xmin - dX*indx1    )*(ymin + dY*(indx2+1) - y);
    A[0][1] = (xmin + dX*(indx1+1) - x)*(y - ymin - dY*indx2);
    A[1][1] = (x - xmin - dX*indx1    )*(y - ymin - dY*indx2);

				// Linear binning
    for (int j1=0; j1<2; j1++)
      for (int j2=0; j2<2; j2++)
	a[indx1+j1][2*(indx2+j2)] += A[j1][j2];

				// Compute moments
    xm += x;
    ym += y;
    sx += x*x;
    sxy += x*y;
    sy += y*y;
    n++;
  }

				// Compute covariance
  xm /= n;
  ym /= n;

  sx = sx/(n-1) - xm*xm*n/(n-1);
  sy = sy/(n-1) - ym*ym*n/(n-1);
  sxy = sxy/(n-1) - xm*ym*n/(n-1);

  double cos2t = 0.5*(sx-sy);
  double sin2t = sxy;
  double denom = sqrt(cos2t*cos2t + sin2t*sin2t);
  cos2t /= denom;
  sin2t /= denom;
  double cost2 = 0.5*(1.0 + cos2t);
  double sint2 = 0.5*(1.0 - cos2t);

  double snx = cost2*sx + sint2*sy + sin2t*sxy;
  double sny = sint2*sx + cost2*sy - sin2t*sxy;

  double theta = 0.5*atan2(sin2t, cos2t);
  cost = cos(theta);
  sint = sin(theta);

  if (snx>0.0)
    H1 = F*sqrt(snx)/pow(n, 1.0/6.0);
  else
    H1 = TINY;
  if (sny>0.0)
    H2 = F*sqrt(sny)/pow(n, 1.0/6.0);
  else
    H2 = TINY;

#ifdef DEBUG
  if (Sflg & 1)
    cerr << "Kernel shape: (" << H1 << ", " << H2 
	 << ")  Rotation: " << theta*180/M_PI << " degrees\n";
#endif
}


				// ------------------------------------------
				// Compute kernel (here a gaussian but could
				// be anything
				// ------------------------------------------

void Denest2d::kernel2d(int n1, int n2, double **b)
{
				// Rotate to original coordinates
  double h1 = fabs(H1*cost - H2*sint);
  double h2 = fabs(H1*sint + H2*cost);

				// 5 sigma point
  int max1 = (int)( 5.0*h1/dX ) + 1;
  int max2 = (int)( 5.0*h2/dY ) + 1;
  
				// Check for overrun of zero padding

  if (2*max1 > n1) {		// X-direction
#ifdef DEBUG
    cerr << "Kernel too fat in x-direction for padding . . . too few data!\n"
	 << "    You wanted H1=" << h1 << " but you are getting H1=";
#endif
    h1 = 0.5*dX*n1/5.0;
#ifdef DEBUG
    cerr << h1 << endl;
#endif
    max1 = (int)( 5.0*h1/dX );
  }

  if (2*max2 > n2) {		// Y-direction
#ifdef DEBUG
    cerr << "Kernel too fat in y-direction for padding . . . too few data!\n"
	 << "    You wanted H2=" << h2 << " but you are getting H2=";
#endif
    h2 = 0.5*dY*n2/5.0;
#ifdef DEBUG
    cerr << h2 << endl;
#endif
    max2 = (int)( 5.0*h2/dY );
  }


  double x, y, fac = 1.0/sqrt(2.0*M_PI*(h1*h1+h2*h2)), norm=0.0;

  for (int j1=0; j1<max1; j1++) {
    x = 0.5*dX*j1;

    for (int j2=0; j2<max2; j2++) {
      y = 0.5*dY*j2;

      b[j1][2*j2+1] = fac*exp(-0.5*(x*x/h1/h1 + y*y/h2/h2));
      
    }

    for (int j2=2*n2-1; j2>2*n2-max2; j2--) {
      y = 0.5*dY*(2*n2-j2);

      b[j1][2*j2+1] = fac*exp(-0.5*(x*x/h1/h1 + y*y/h2/h2));
    }

  }

  for (int j1=2*n1-1; j1>2*n1-max1; j1--) {
    x = 0.5*dX*(2*n1-j1);

    for (int j2=0; j2<max2; j2++) {
      y = 0.5*dY*j2;

      b[j1][2*j2+1] = fac*exp(-0.5*(x*x/h1/h1 + y*y/h2/h2));
    }

    for (int j2=2*n2-1; j2>2*n2-max2; j2--) {
      y = 0.5*dY*(2*n2-j2);

      b[j1][2*j2+1] = fac*exp(-0.5*(x*x/h1/h1 + y*y/h2/h2));
    }

  }

  for (int j1=0; j1<2*n1; j1++)
    for (int j2=0; j2<2*n2; j2++)
      norm += b[j1][2*j2+1];

  for (int j1=0; j1<2*n1; j1++)
    for (int j2=0; j2<2*n2; j2++)
      b[j1][2*j2+1] /= norm;

}


				// ------------------------------------------
				// Write output file
				// ------------------------------------------

struct Couple
{
  double x;
  double m;
};

class CoupleLess {
public:
  bool operator() (const Couple& p, const Couple& q) const
    {
      return p.x < q.x;
    }
};

double match;

class MassLoc {
public:

  bool operator() (const Couple& p) const
    {
      return p.m >= match;
    }
};


void Denest2d::output(ostream& out)
{
  double x, y;
  float z;
  double norm;

  if (!computed) compute();

  if (normVH) 
    norm = 1.0;
  else
    norm = norm2d(n1, n2);

  switch (output_type) {
  case 1:
    {
      out.write((char *)&n1, sizeof(int));
      out.write((char *)&n2, sizeof(int));
      out.write((char *)&(z=xmin), sizeof(float));
      out.write((char *)&(z=xmax), sizeof(float));
      out.write((char *)&(z=ymin), sizeof(float));
      out.write((char *)&(z=ymax), sizeof(float));

      for (int j2=0; j2<n2; j2++)
	for (int j1=0; j1<n1; j1++)
	  out.write((char *)&(z=a[j1][2*j2]/norm), sizeof(float));
    }
    break;

  case 2:
    {
      float z;

      out.write((char *)&n1, sizeof(int));
      out.write((char *)&n2, sizeof(int));
      for (int j1=0; j1<n1; j1++) {
	x =  xmin + dX*j1;
	for (int j2=0; j2<n2; j2++) {
	  y =  ymin + dY*j2;
	  out.write((char *)&(z=x), sizeof(float));
	  out.write((char *)&(z=y), sizeof(float));
	  out.write((char *)&(z=a[j1][2*j2]/norm), sizeof(float));
	}
      }
    }
    break;
    
  default:
  case 0:
    {
      for (int j1=0; j1<n1; j1++) {
	x =  xmin + dX*j1;
	for (int j2=0; j2<n2; j2++) {
	  y =  ymin + dY*j2;
	  out << setw(15) << x << setw(15) << y 
	      << setw(15) << a[j1][2*j2]/norm << endl;
	}
	out << endl;
      }
    }
    break;

  }

}


void Denest2d::compute_cdf(void)
{
  if (!computed) compute();

  double norm;
  if (normVH) 
    norm = 1.0;
  else
    norm = norm2d(n1, n2);

				// Build vectors of value, CDF pairs
  Couple t;
  vector<Couple> val;
  for (int j1=0; j1<n1; j1++) {
    for (int j2=0; j2<n2; j2++) {
      t.x = a[j1][2*j2]/norm;
      val.push_back(t);
    }
  }
    
				// Sort on value
  sort(val.begin(), val.end(), CoupleLess());

				// Make CDF
  for (unsigned i=0; i<val.size(); i++) {
    if (i==0) val[i].m = val[i].x*dX*dY;
    else val[i].m = val[i-1].m + val[i].x*dX*dY;
  }

				// Cum points for output
  const int numl = 17;
  double levels[numl] = {0.001, 0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5,
			 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 0.995, 0.999};
  
  Prob.erase(Prob.begin(), Prob.end());
  CumDF.erase(CumDF.begin(), CumDF.end());
  
  vector<Couple>::iterator where;

  for (int i=0; i<numl; i++) {
    match = levels[i] * val[val.size()-1].m;
    where = find_if(val.begin(), val.end(), MassLoc());
    Prob.push_back(levels[i]);
    CumDF.push_back(where->x);
  }
  
  computed_cdf = true;
}

void Denest2d::get_cdf(vector<double>& v, vector<double>& cdf)
{
  if (!computed_cdf) compute_cdf();
  v = Prob;
  cdf = CumDF;
}


void Denest2d::output_cdf(void)
{
  if (!computed_cdf) compute_cdf();

				// Open file if needed
  ofstream *out = NULL;
  if (Sflg & 2) {
    string fname = outputname + ".stats";
    out = new ofstream(fname.c_str());
    if (!out) {
      cerr << "Couldn't open <" << fname.c_str() << ">\n";
      Sflg &= 1;
    }
  }

  if (Sflg & 1)
    cerr << endl
	 << setw(10) << "Fraction"
	 << setw(14) << "Level(data)"
	 << endl
	 << setw(10) << "--------"
	 << setw(14) << "------------"
	 << endl;

  for (unsigned i=0; i<Prob.size(); i++) {
    if (Sflg & 1)
      cerr << setw(10) << Prob[i]
	   << setw(14) << CumDF[i]
	   << endl;
    if (Sflg & 2)
      *out << setw(10) << Prob[i]
	   << setw(14) << CumDF[i]
	   << endl;
  }

  if (Sflg & 2) {
    out->close();
    delete out;
  }

}


				// ------------------------------------------
				// Compute normalization
				// ------------------------------------------

				// By rows or columns

void Denest2d::normal2d(int n1, int n2)
{
  double norm, norm0 = norm2d(n1, n2);


  if (normVH==1) {		// rows
    for (int j1=0; j1<n1; j1++) {

      norm = 0.0;
      for (int j2=0; j2<n2; j2++) norm += a[j1][2*j2] * dX;

      if (norm>norm0*1.0e-8)
	for (int j2=0; j2<n2; j2++) a[j1][2*j2] /= norm;
      else
	for (int j2=0; j2<n2; j2++) a[j1][2*j2] = 0.0;

    }
  }
  if (normVH==2) {		// columns

    for (int j2=0; j2<n2; j2++) {

      norm = 0.0;
      for (int j1=0; j1<n1; j1++) norm += a[j1][2*j2] * dY;

      if (norm>norm0*1.0e-8)
	for (int j1=0; j1<n1; j1++) a[j1][2*j2] /= norm;
      else
	for (int j1=0; j1<n1; j1++) a[j1][2*j2] = 0.0;
    }
  }
  
}

				// Total

double Denest2d::norm2d(int n1, int n2)
{
  double norm = 0.0;

  for (int j1=0; j1<n1; j1++)
    for (int j2=0; j2<n2; j2++)
      norm += a[j1][2*j2] * dX*dY;
  
  return norm;
}

