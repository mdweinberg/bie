// This may look like C code, but it is really -*- C++ -*-

#ifndef _VtkDenest2d_h
#define _VtkDenest2d_h

using namespace std;

#include <Denest2d.h>

// For details, see:
// http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Factories_now_require_defines
// 
#include <vtkVersion.h>
#if VTK_MAJOR_VERSION==6 && VTK_MINOR_VERSION<2
#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#endif

#if VTK_MAJOR_VERSION>=4
#include <vtkRenderer.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkProperty.h>
#include <vtkProperty2D.h>
#include <vtkTransform.h>
#include <vtkMapper.h>
#include <vtkActor.h>
#include <vtkActor2D.h>
#include <vtkCamera.h>
#include <vtkLight.h>
#include <vtkRenderWindow.h>
#include <vtkPolyDataMapper.h>
#include <vtkStructuredGrid.h>
#include <vtkCleanPolyData.h>
#include <vtkFloatArray.h>
#include <vtkContourFilter.h>
#include <vtkOutlineFilter.h>
#include <vtkTubeFilter.h>
#include <vtkStructuredGridGeometryFilter.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNMWriter.h>
#include <vtkPoints.h>
#if VTK_MAJOR_VERSION>4 || (VTK_MAJOR_VERSION==4 && VTK_MINOR_VERSION>=2)
#include <vtkTextProperty.h>
#endif
#else
#include <vtkCommon.h>
#include <vtkGraphics.h>
#include <vtkImaging.h>
#include <vtkContrib.h>
#include <vtkXRenderWindow.h>
#include <vtkFloatScalars.h>
#include <vtkFloatPoints.h>
#endif

#include <vtkAxisActor2D.h>
#include <vtkCubeAxesActor2D.h>
#include <vtkScalarBarActor.h>
#include <vtkLookupTable.h>

class VtkDenest2d : public Denest2d
{
private:
  
  double* pscl;			// scale vector to keep box square
 
#if VTK_MAJOR_VERSION>=4
  vtkFloatArray *scalars0;
#else
  vtkScalars *scalars0;
#endif
  vtkPoints *points0;

#if VTK_MAJOR_VERSION>5 || (VTK_MAJOR_VERSION==4  && VTK_MINOR_VERSION>=4)
  double min_value, max_value;
#else
  float min_value, max_value;
#endif

				// Parameters

  bool thresholdC, threshold, preserve, cdf_contours;
  bool use_outline, use_height, use_labels, reverse_colors;
  bool black_and_white, no_pedestal, rainbow_map;

  int contours, num_ticks;
  double dolly;
#if ((VTK_MAJOR_VERSION==4) && (VTK_MINOR_VERSION>=4))
  double tube;
  double hscale;
  double tscale;
  double lo, hi;
#else
  float tube;
  float hscale;
  float tscale;
  float lo, hi;
#endif
  string xlab;
  string ylab;
  string tlab;

  bool loaded;

public:
  
  VtkDenest2d(double xmin, double xmax, double ymin, double ymax,
	      vector<double>& x, vector<double>& y);

  ~VtkDenest2d();

  void loadVtk();

  void plotVtk(vtkRenderer*);

  void newData(double xmin, double xmax, double ymin, double ymax,
	       vector<double>& x, vector<double>& y)
  {
    loaded = false;
    Denest2d::newData(xmin, xmax, ymin, ymax, x, y);
  }
  

				// Toggles
  void Threshold(bool x) { threshold = x; }
  void ThresholdC(bool x) { thresholdC = x; }
  void Preserve(bool x) { preserve = x; }
  void Tube(bool x) { tube = x; }
  void Outline(bool x) { use_outline = x; }
  void Height(bool x) { use_height = x; }
  void Labels(bool x) { use_labels = x; }
  void BlackAndWhite(bool x) { black_and_white = x; }
  void ReverseColors(bool x) { reverse_colors = x; }
  void RainbowMap(bool x) { rainbow_map = x; }
  void Pedestal(bool x) { no_pedestal = x; }
  void CDF(bool x) { cdf_contours = x; }

				// Setting values
  void SetLabels(string Xlab, string Ylab, string Tlab) 
  { 
    xlab = Xlab; 
    ylab = Ylab; 
    tlab = Tlab; 
  }

  void SetContours (int c) { contours = c;  }
  void SetNumticks (int c) { num_ticks = c; }

				// Get and set values
  void setMinMax(double vmin, double vmax) 
  {
    lo = vmin;
    hi = vmax;
  }

  void getMinMax(double& vmin, double& vmax) 
  {
    vmin = min_value;
    vmax = max_value; 
#ifdef DEBUG
    cout << "Min, max=" << min_value << ", " << max_value << endl;
#endif
  }

  double getMin() { return min_value; }

  double getMax() { return max_value; }

};

#endif
