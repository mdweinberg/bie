#include <gtkmm/main.h>
#include <DataViewer.h>
#include <black_cat.h>

int main(int argc, char *argv[])
{
  Gtk::Main main_instance(argc, argv);

  //
  // Init gtkglextmm.
  //
 
  Gtk::GL::init(argc, argv);

  //
  // Main application
  //

  int size = 400;
  string filename = "data.dat";

  if (argc>1) 
    filename = argv[1];
  else {
    cout << "Usage: " << argv[0] << " filename [size]\n";
    return 0;
  }
  if (argc>2) size = atoi(argv[2]);

#ifdef DEBUG
  cout << "Pixel size=" << size << endl;
#endif

  try {
    DataViewer window(size, filename);

    // Set icon
    const Glib::RefPtr< Gdk::Pixbuf > icon = 
      Gdk::Pixbuf::create_from_xpm_data(black_cat);
    
    window.set_icon(icon);

    // Start loop
    Gtk::Main::run(window);
  }
  catch (const char *message) {
    cerr << message << endl;
    return 0;
  }

  return 0;
}
