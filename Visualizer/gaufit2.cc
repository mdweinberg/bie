#include <math.h>
#include <vector>
#include <fftw3.h>

using namespace std;


//
//     Reference:
//
//     ALGORITHM AS 176  APPL. STATIST. (1982) VOL.31, NO.1
//     Modified using AS R50 (Appl. Statist. (1984))
//


int Density_estimate(vector<double>& dt, double dlo, double dhi, double window,
		     vector<double>& sm)
{

  const double big = 30.0;
  const int kftlo = 5;		// 32
  const int kfthi = 16;		// 65536

//
//     The constant BIG is set so that exp(-BIG) can be calculated
//     without causing underflow problems and can be considered = 0.
//
//     Initialize and check for valid parameter values.
//

  if (window <= 0.0) return 2;
  if (dlo >= dhi) return 3;

  int ndt = dt.size();
  int nft = sm.size();
  
  int ii = 1<<kftlo;
  int ok=0;

  for (int k = kftlo; k<=kfthi; k++) {
    if (ii == nft) {
      ok = 1;
      break;
    }
    ii = ii + ii;
  }
  if (!ok) return 1;		// Isn't a power of 2!

  double step = (dhi - dlo) / nft;
  double ainc = 1.0 / (ndt * step);
  int nft2 = nft / 2;
  double hw = window / step;
  double fac1 = 32.0 * pow(atan(1.0) * hw / nft, 2.0);


//
//     Discretize the data
//
  double dlo1 = dlo - step * 0.5; // dlo is the lower bin edge
  double wt, winc;
  int jj, kk;

  vector<double> ft(nft, 0.0);

  for (int i=0; i<ndt; i++) {

    wt = (dt[i] - dlo1) / step;
    jj = (int)floor(wt);
    if (jj < 0 || jj >= nft) continue;

    wt = wt - jj;
    winc = wt * ainc;
    kk = jj + 1;
    if (jj == nft) kk = 0;	// Period boundary conditions
    ft[jj] += ainc - winc;
    ft[kk] += winc;

  }

//
//     Transform to find FT.
//


  //
  // FFTW setup
  //
  fftw_complex *tft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nft);
  fftw_plan transform = fftw_plan_dft_r2c_1d(nft, &ft[0], tft, FFTW_ESTIMATE);
  fftw_plan inverse_transform = fftw_plan_dft_c2r_1d(nft, tft, &sm[0], FFTW_ESTIMATE);
  //

  fftw_execute(transform);

//
//     Find transform of density estimate.
//
  int jhi = (int)sqrt(big / fac1);
  double fac, bc, rjfac, rj = 0.0;

  int i = nft2;
  int j = jhi;
  int jmax = i<j ? i : j;

  for (j=1; j<=jmax; j++) {
    rj = rj + 1.0;
    rjfac = rj * rj * fac1;
    bc = 1.0;
    if (window > step)
      bc -= rjfac / (hw * hw * 6.0);
    fac = exp(-rjfac) / bc;
    tft[j][0] *= fac;
    tft[j][1] *= fac;
  }

//
//     Cope with underflow by setting tail of transform to zero.
//

  if (jhi > nft2) {
    tft[nft2][0] = exp(-fac1 * nft2*nft2) * ft[nft2];
    tft[nft2][1] = exp(-fac1 * nft2*nft2) * ft[nft2];
  }
  else {
    if (jhi < nft2) {
      for (j=jhi; j<=nft2; j++) {
	tft[j][0] = tft[j][1] = 0.0;
      }
    }
  }

//
//     Invert Fourier transform of SMOOTH to get estimate and eliminate
//     negative density values.
//

  fftw_execute(inverse_transform);

  for (j=0; j<nft; j++) {
    sm[j] /= nft;
    if (sm[j] < 0.0) sm[j] = 0.0;
  }

  //
  // FFTW cleanup
  //
  fftw_destroy_plan(transform);
  fftw_destroy_plan(inverse_transform);
  fftw_free(tft);
  
  return 0;
}
