int *alloc_1d_int(int n1);
void free_1d_int(int *i);
double *alloc_1d_double(int n1);
void free_1d_double(double *d);
int **alloc_2d_int(int n1, int n2);
void free_2d_int(int **ii, int n1);
double **alloc_2d_double(int n1, int n2);
void free_2d_double(double **dd, int n1);


int *alloc_1d_int(int n1)
{
  return new int [n1];
}


void free_1d_int(int *i)
{
  delete [] i;
}


double *alloc_1d_double(int n1)
{
  return new double [n1];
}


void free_1d_double(double *d)
{
  delete [] d;
}


int **alloc_2d_int(int n1, int n2)
{
    int **ii = new int* [n1];
    for (int j=0; j<n1; j++)
      ii[j] = new int [n2];

    return ii;
}


void free_2d_int(int **ii, int n1)
{
    for (int j=0; j<n1; j++)
      delete [] ii[j];
    delete [] ii;
}


double **alloc_2d_double(int n1, int n2)
{
    double **dd = new double* [n1];
    for (int j=0; j<n1;j++)
      dd[j] = new double [n2];

    return dd;
}


void free_2d_double(double **dd, int n1)
{
  for (int j=0; j<n1; j++)
    delete [] dd[j];
  delete [] dd;
}

