#ifndef _DATAVIEWER_H
#define _DATAVIEWER_H

#include <gtkmm.h>

#include <VtkGtkBox.h>
#include <XYPlot.h>
#include <VtkDenest2d.h>
#include <DataFieldBox.h>
#include <Spinners.h>
#include <DenseBox.h>
#include <RenderBox.h>

class DataViewer : public Gtk::Window
{
public:
  DataViewer(int size, string filename);
  virtual ~DataViewer();

protected:
  //Signal handlers:
  virtual void on_button_curve();
  virtual void on_button_marginal();
  virtual void on_button_contour();
  virtual void on_button_reread();
  virtual void on_button_jpeg();
  virtual void on_button_help();
  virtual void on_button_quit();

  // Pixels
  int psize;

  // Plots
  XYPlot *xyplot;
  VtkDenest2d *tdplot;

  // Flags
  bool screendump_ok0;
  bool screendump_ok1;

  //Child widgets:
  Gtk::VBox m_VBox;
  Gtk::Notebook m_Notebook;

  Gtk::HButtonBox m_ButtonBox;
  Gtk::Button m_Button_Curve;
  Gtk::Button m_Button_Marginal;
  Gtk::Button m_Button_Contour;
  Gtk::Button m_Button_Reread;
  Gtk::Button m_Button_Jpeg;
  Gtk::Button m_Button_Help;
  Gtk::Button m_Button_Quit;

  VtkGtkBox my_vtk_box0;
  VtkGtkBox my_vtk_box1;

  DataFieldBox my_fields;
  DenseBox my_dens;
  RenderBox my_render;
};

#endif //_DATAVIEWER_H

