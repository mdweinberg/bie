#include <unistd.h>
#include <gtk/gtk.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;


// Classes and structures for saving simulation output stream

struct DataRecord 
{
  unsigned iter;
  double prob;
  vector<double> data;
} datarecord;

class LevelRecord
{
private:

  unsigned level;
  unsigned ndim;

public:

  unsigned itmin;
  unsigned itmax;

  double probmin;
  double probmax;

  vector<double> datamin;
  vector<double> datamax;

  vector<DataRecord> data;

  LevelRecord(int Lev, int Ndim) {
    level = Lev;
    ndim = Ndim;
    itmin = 1000000000;
    itmax = 0;
    probmin =  1.0e30;
    probmax = -1.0e30;
    datamin = vector<double>(ndim,  1.0e30);
    datamax = vector<double>(ndim, -1.0e30);
  }
    
  LevelRecord(const LevelRecord &v)
  {
    level = v.level;
    ndim = v.ndim;
    itmin = v.itmin;
    itmax = v.itmax;
    datamin = v.datamin;
    datamax = v.datamax;
    data = v.data;
  }

  unsigned Level(void) { return level; }

  void operator+=(DataRecord& r) {
    itmin = min<unsigned>(r.iter, itmin);
    itmax = max<unsigned>(r.iter, itmax);
    probmin = min<double>(r.prob, probmin);
    probmax = max<double>(r.prob, probmax);
    for (unsigned i=0; i<ndim; i++) {
      datamin[i] = min<double>(r.data[i], datamin[i]);
      datamax[i] = max<double>(r.data[i], datamax[i]);
    }

    data.push_back(r);
  }

};

// Widgets

vector <LevelRecord> Data;

static gint button_press (GtkWidget *, GdkEvent *);
void x_menuitem_response (unsigned *);
void y_menuitem_response (unsigned *);
GtkWidget *xlabel, *ylabel;
vector<string> fieldNames;
GtkAdjustment *xadjmin, *xadjmax, *yadjmin, *yadjmax, *levadj;
GtkAdjustment *itadjmin, *itadjmax, *imadjmin, *imadjmax;
unsigned xcolumn, ycolumn;

void scale_set_default_values( GtkScale *scale )
{
    gtk_range_set_update_policy (GTK_RANGE (scale),
                                 GTK_UPDATE_CONTINUOUS);
    gtk_scale_set_digits (scale, 1);
    gtk_scale_set_value_pos (scale, GTK_POS_TOP);
    gtk_scale_set_draw_value (scale, TRUE);
}


GtkWidget *get_list(const char *header, GtkWidget **label, 
		    void func(unsigned *))
{
  GtkWidget *vbox, *menu, *menu_items, *frame, *button;

  /* A vbox to put a menu and a button in: */
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox);

  /* Init the menu-widget, and remember -- never
   * gtk_show_widget() the menu widget!! */
  menu = gtk_menu_new ();

  for (unsigned i = 0; i < fieldNames.size(); i++)
    {
      /* Create a new menu-item with a name... */
      menu_items = gtk_menu_item_new_with_label (fieldNames[i].c_str());

      /* ...and add it to the menu. */
      gtk_menu_append (GTK_MENU (menu), menu_items);
      
      /* Do something interesting when the menuitem is selected */
      gtk_signal_connect_object (GTK_OBJECT (menu_items), "activate",
				 GTK_SIGNAL_FUNC (func), 
				 (GtkObject *)(g_memdup(&i, sizeof(unsigned))));
      /* Show the widget */
      gtk_widget_show (menu_items);
    }


  *label = gtk_label_new (" ");
  gtk_widget_show(*label);

  frame = gtk_frame_new(header);
  // gtk_widget_set_usize(frame, 40, 50);
  gtk_container_border_width(GTK_CONTAINER(frame), 5);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (frame), *label);
  gtk_widget_show(frame);

  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show(vbox);

  /* Create a button to which to attach menu as a popup */
  button = gtk_button_new_with_label ("Choose field");
  gtk_signal_connect_object (GTK_OBJECT (button), "event",
			     GTK_SIGNAL_FUNC (button_press), 
			     GTK_OBJECT (menu));
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);
  
  return vbox;
}

/* Respond to a button-press by posting a menu passed in as widget.
 *
 * Note that the "widget" argument is the menu being posted, NOT
 * the button that was pressed.
 */

static gint button_press( GtkWidget *widget,
                          GdkEvent *event )
{

    if (event->type == GDK_BUTTON_PRESS) {
        GdkEventButton *bevent = (GdkEventButton *) event; 
        gtk_menu_popup (GTK_MENU (widget), NULL, NULL, NULL, NULL,
                        bevent->button, bevent->time);

        /* Tell calling code that we have handled this event; the buck
         * stops here. */
        return TRUE;
    }

    /* Tell calling code that we have not handled this event; pass it on. */
    return FALSE;
}


/* Print a string when a menu item is selected */

void update_iterlimits(void)
{
  itadjmin->value = Data[(int)levadj->value].itmin;
  itadjmin->lower = Data[(int)levadj->value].itmin;
  itadjmin->upper = Data[(int)levadj->value].itmax;
  itadjmax->value = Data[(int)levadj->value].itmax;
  itadjmax->lower = Data[(int)levadj->value].itmin;
  itadjmax->upper = Data[(int)levadj->value].itmax;

  gtk_signal_emit_by_name (GTK_OBJECT (itadjmin), "value_changed");
  gtk_signal_emit_by_name (GTK_OBJECT (itadjmax), "value_changed");
}

void update_xlimits(void)
{
  if (xcolumn==0) {

    xadjmin->value = Data[(int)levadj->value].probmin;
    xadjmax->value = Data[(int)levadj->value].probmax;

  } else {

    xadjmin->value = Data[(int)levadj->value].datamin[xcolumn-1];
    xadjmax->value = Data[(int)levadj->value].datamax[xcolumn-1];
  }

  gtk_signal_emit_by_name (GTK_OBJECT (xadjmin), "value_changed");
  gtk_signal_emit_by_name (GTK_OBJECT (xadjmax), "value_changed");
}

void update_ylimits(void)
{
  if (ycolumn==0) {

    cout << "Ymin=" << Data[(int)levadj->value].probmin << endl;
    cout << "Ymax=" << Data[(int)levadj->value].probmax << endl;

    yadjmin->value = Data[(int)levadj->value].probmin;
    yadjmax->value = Data[(int)levadj->value].probmax;
    
  } else {
    
    cout << "Ymin=" << Data[(int)levadj->value].datamin[ycolumn-1] << endl;
    cout << "Ymax=" << Data[(int)levadj->value].datamax[ycolumn-1] << endl;

    yadjmin->value = Data[(int)levadj->value].datamin[ycolumn-1];
    yadjmax->value = Data[(int)levadj->value].datamax[ycolumn-1];
    
  }
  gtk_signal_emit_by_name (GTK_OBJECT (yadjmin), "value_changed");
  gtk_signal_emit_by_name (GTK_OBJECT (yadjmax), "value_changed");
}


void x_menuitem_response( unsigned *i )
{
  xcolumn = *i;
  gtk_label_set_text (GTK_LABEL(xlabel), fieldNames[xcolumn].c_str());
  update_xlimits();
}

void y_menuitem_response( unsigned *i )
{
  ycolumn = *i;
  gtk_label_set_text (GTK_LABEL(ylabel), fieldNames[ycolumn].c_str());
  update_ylimits();
}


void compute_data( GtkWidget *widget,
		   gpointer   data )
{
  g_print ("Hello again - %s was pressed\n", (char *) data);
}

void make_plot( GtkWidget *widget,
		gpointer   data )
{
  g_print ("Hello again - %s was pressed\n", (char *) data);
}

GtkWidget *lvscale, *hvscale;

void cb_pixel_scale( GtkAdjustment *adj )
{
  cout << "Pixels set to: " << adj->value << endl;
}

void cb_low_val( GtkAdjustment *adj )
{
  cout << "Low value set to: " << adj->value << endl;
}

void cb_high_val( GtkAdjustment *adj )
{
  cout << "High value set to: " << adj->value << endl;
}

void bb_level_choose( unsigned *i )
{
  cout << "Level value: " << *i << endl;
  levadj->value = *i;
  gtk_signal_emit_by_name (GTK_OBJECT (levadj), "changed");
  update_xlimits();
  update_ylimits();
  update_iterlimits();
}

void cb_level_choose( GtkAdjustment *get )
{
  cout << "Level value: " << get->value << endl;
  gtk_signal_emit_by_name (GTK_OBJECT (get), "changed");
  update_xlimits();
  update_ylimits();
  update_iterlimits();
}

GtkWidget *cntrscale;

void cb_contour_number( GtkAdjustment *get )
{
  cout << "Contour number: " << get->value << endl;
  /* Now emit the "changed" signal to reconfigure all the widgets that
   * are attached to this adjustment */
  gtk_signal_emit_by_name (GTK_OBJECT (get), "changed");
}

void cb_use_contours( GtkToggleButton *button )
{
    /* Turn the value display on the scale widgets off or on depending
     *  on the state of the checkbutton */
  if (button->active) {
    gtk_widget_map(cntrscale);
  }
  else {
    gtk_widget_unmap(cntrscale);
  }
}

void cb_use_log( GtkToggleButton *button )
{
    /* Turn the value display on the scale widgets off or on depending
     *  on the state of the checkbutton */
  if (button->active) {
    cout << "Using log scaling\n";
  }
  else {
  }
}

/* Convenience functions */

GtkWidget *make_menu_item( gchar         *name,
                           GtkSignalFunc  callback,
                           gpointer       data )
{
    GtkWidget *item;
  
    item = gtk_menu_item_new_with_label (name);
    gtk_signal_connect (GTK_OBJECT (item), "activate",
                        callback, data);
    gtk_widget_show (item);

    return(item);
}

GtkWidget *level_scale(void)
{
  GtkWidget *hbox, *box2, *label, *scale;

  hbox = gtk_hbox_new (FALSE, 10); 
  gtk_container_set_border_width (GTK_CONTAINER(hbox), 10);
  
  /* Scrollbar */
  box2 = gtk_hbox_new(FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

  label = gtk_label_new ("Level:");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  
  levadj = 
    (GtkAdjustment *) gtk_adjustment_new (Data[0].Level(), Data[0].Level(), 
					  Data[Data.size()-1].Level(), 
					  1.0, 1.0, 0.0);
  gtk_signal_connect (GTK_OBJECT (levadj), "value_changed",
		      GTK_SIGNAL_FUNC (cb_level_choose), NULL);
  scale = gtk_hscale_new (GTK_ADJUSTMENT (levadj));
  gtk_scale_set_digits (GTK_SCALE (scale), 0);
  gtk_box_pack_start (GTK_BOX (box2), scale, TRUE, TRUE, 0);
  gtk_widget_show (scale);

  gtk_box_pack_start (GTK_BOX (hbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);

  return hbox;
}

GtkWidget *level_radio(void)
{
  GtkWidget *hbox, *box2, *label, *button;
  GSList *group;

  hbox = gtk_hbox_new (FALSE, 10); 
  gtk_container_set_border_width (GTK_CONTAINER(hbox), 10);
  
  // Label
  box2 = gtk_hbox_new(FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
  gtk_box_pack_start(GTK_BOX(hbox), box2, FALSE, FALSE, 0);

  label = gtk_label_new ("Level:");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  
  // Radio buttons
  
  box2 = gtk_hbox_new(FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
  gtk_box_pack_start(GTK_BOX(hbox), box2, FALSE, FALSE, 0);

  group = NULL;
  for (unsigned lev = Data[0].Level(); lev < Data.size(); lev++)
    {
      ostringstream digit;
      digit << lev;
      button = gtk_radio_button_new_with_label (group, digit.str().c_str());
      gtk_box_pack_start (GTK_BOX (box2), button, TRUE, TRUE, 0);
      gtk_widget_show (button);

      if (!group) 
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);

      group = gtk_radio_button_group (GTK_RADIO_BUTTON (button));

      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (bb_level_choose), 
				 (GtkObject *)(g_memdup(&lev, sizeof(unsigned))));
    }

  gtk_widget_show(hbox);

  levadj = 
    (GtkAdjustment *) gtk_adjustment_new (Data[0].Level(), Data[0].Level(), 
					  Data[Data.size()-1].Level(), 
					  1.0, 1.0, 0.0);
  return hbox;
}

GtkWidget *axis_ranges(void)
{
  GtkWidget *vbox, *hbox, *hbox2, *vbox2, *label, *frame, *spinner1;

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show(vbox);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER(hbox), 10);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

				// X axis

  hbox2 = gtk_hbox_new (FALSE, 0);

				// Xmin
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Min:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  xadjmin = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (xadjmin, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);
  
				// Xmax
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Max:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  xadjmax = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (xadjmax, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);


				// Put in frame
  frame = gtk_frame_new("X-axis");
  gtk_widget_set_usize(frame, 175, 75);
  gtk_container_border_width(GTK_CONTAINER(frame), 5);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (frame), hbox2);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);
  gtk_widget_show(hbox2);
  gtk_widget_show(frame);

				// Y axis

  hbox2 = gtk_hbox_new (FALSE, 0);

				// Ymin
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Min:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  yadjmin = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (yadjmin, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);
  
				// Ymax
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Max:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  yadjmax = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (yadjmax, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);


				// Put in frame
  frame = gtk_frame_new("Y-axis");
  gtk_widget_set_usize(frame, 175, 75);
  gtk_container_border_width(GTK_CONTAINER(frame), 5);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (frame), hbox2);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);
  gtk_widget_show(hbox2);
  gtk_widget_show(frame);


  gtk_widget_show(hbox);

  return vbox;
}

GtkWidget *plot_controls(void)
{
  GtkWidget *vbox, *box2, *box3, *button;
  GtkWidget *label, *scale, *frame;
  GtkAdjustment *adj;
  GtkObject *adj2;

  vbox = gtk_vbox_new (FALSE, 10);
  gtk_widget_show (vbox);

  /* A checkbutton to control whether the value is displayed or not */
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

  button = gtk_check_button_new_with_label("Contour levels");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_use_contours), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  /* Scrollbar */
  adj = (GtkAdjustment *) gtk_adjustment_new (10.0, 0.0, 100.0, 1.0, 1.0, 0.0);
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      GTK_SIGNAL_FUNC (cb_contour_number), NULL);
  cntrscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_scale_set_digits (GTK_SCALE (cntrscale), 0);
  gtk_box_pack_start (GTK_BOX (box2), cntrscale, TRUE, TRUE, 0);
  gtk_widget_show (cntrscale);

  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);


  /* A checkbutton to enable log scaling*/
  box2 = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

  button = gtk_check_button_new_with_label("Log scale");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), FALSE);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_use_log), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);

  /* An option menu to change the position of the value */
  box2 = gtk_hbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

  label = gtk_label_new ("Scale Value Position:");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  
  /* Set pixels */
  
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
  
  label = gtk_label_new ("Pixels: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  
  adj2 = gtk_adjustment_new (400.0, 0.0, 1000.0, 1.0, 1.0, 0.0);
  gtk_signal_connect (GTK_OBJECT (adj2), "value_changed",
		      GTK_SIGNAL_FUNC (cb_pixel_scale), NULL);
  scale = gtk_hscale_new (GTK_ADJUSTMENT (adj2));
  gtk_scale_set_digits (GTK_SCALE (scale), 0);
  gtk_box_pack_start (GTK_BOX (box2), scale, TRUE, TRUE, 0);
  gtk_widget_show (scale);
  
  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show (box2);
  
				// Contour level adjustments

  box2 = gtk_vbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);

  frame = gtk_frame_new("Level range");
  gtk_box_pack_start(GTK_BOX (box2), frame, TRUE, TRUE, 0);
  gtk_widget_show (box2);

  box3 = gtk_vbox_new(FALSE, 0);
  gtk_container_add (GTK_CONTAINER(frame), box3);
  gtk_widget_show(box3);

				// -- Low level
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(box2), 5);
  gtk_box_pack_start(GTK_BOX(box3), box2, TRUE, TRUE, 0);
  
  imadjmin = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, 0.0, 101.0, 0.1, 1.0, 1.0);
  
  label = gtk_label_new ("Low: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  lvscale = gtk_hscale_new (GTK_ADJUSTMENT (imadjmin));
  scale_set_default_values (GTK_SCALE (lvscale));
  gtk_box_pack_end (GTK_BOX (box2), lvscale, TRUE, TRUE, 0);
  gtk_widget_show (lvscale);
  gtk_widget_show (box2);
  
				// -- High level

  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 5);
  gtk_box_pack_start(GTK_BOX(box3), box2, TRUE, TRUE, 0);

  imadjmax = (GtkAdjustment *)
    gtk_adjustment_new (101.0, 0.0, 101.0, 0.1, 1.0, 1.0);

  label = gtk_label_new ("High: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  hvscale = gtk_hscale_new (GTK_ADJUSTMENT (imadjmax));
  scale_set_default_values (GTK_SCALE (hvscale));
  gtk_box_pack_end (GTK_BOX (box2), hvscale, TRUE, TRUE, 0);
  gtk_widget_show (hvscale);
  gtk_widget_show (box2);
  
				// Event handlers

  gtk_signal_connect (GTK_OBJECT (imadjmin), "value_changed",
		      GTK_SIGNAL_FUNC (cb_low_val), NULL);

  gtk_signal_connect (GTK_OBJECT (imadjmax), "value_changed",
		      GTK_SIGNAL_FUNC (cb_high_val), NULL);

  return vbox;
}

GtkWidget *loscale, *hiscale;

void cb_lower_limit( GtkAdjustment *get,
		     GtkAdjustment *set )
{
  set->lower = get->value;
  set->value = max<gfloat>(set->lower, set->value);
  gtk_signal_emit_by_name (GTK_OBJECT (set), "changed");
}

void cb_upper_limit( GtkAdjustment *get,
		     GtkAdjustment *set )
{
  set->upper = get->value;
  set->value = min<gfloat>(set->upper, set->value);
  gtk_signal_emit_by_name (GTK_OBJECT (set), "changed");
}

GtkWidget *iteration_window(void)
{
  GtkWidget *vbox, *vbox1, *vbox2, *hbox1, *hbox2, *label;

  vbox = gtk_vbox_new (FALSE, 10);
  gtk_widget_show (vbox);

  vbox1 = gtk_vbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (vbox1), 10);
  gtk_box_pack_start (GTK_BOX (vbox), vbox1, TRUE, TRUE, 0);
  gtk_widget_show (vbox1);

  vbox2 = gtk_hbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (vbox2), 10);
  gtk_box_pack_start (GTK_BOX (vbox), vbox2, TRUE, TRUE, 0);
  gtk_widget_show (vbox2);
  
  hbox1 = gtk_hbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (hbox1), 10);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox1, TRUE, TRUE, 0);
  gtk_widget_show (hbox1);

  hbox2 = gtk_hbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 10);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);
  gtk_widget_show (hbox2);
  
  itadjmin = (GtkAdjustment *) 
    gtk_adjustment_new (Data[(int)levadj->value].itmin, 
			Data[(int)levadj->value].itmin, 
			Data[(int)levadj->value].itmax,
			1.0, 1.0, 0.0);
  
  label = gtk_label_new ("Lower limit:");
  gtk_box_pack_start (GTK_BOX (hbox1), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  loscale = gtk_hscale_new (GTK_ADJUSTMENT (itadjmin));
  scale_set_default_values (GTK_SCALE (loscale));
  gtk_scale_set_digits (GTK_SCALE (loscale), 0);
  gtk_box_pack_end (GTK_BOX (hbox1), loscale, TRUE, TRUE, 0);
  gtk_widget_show (loscale);
  
  itadjmax = (GtkAdjustment* ) 
    gtk_adjustment_new (Data[(int)levadj->value].itmax, 
			Data[(int)levadj->value].itmin, 
			Data[(int)levadj->value].itmax,
			1.0, 1.0, 0.0);

  label = gtk_label_new ("Upper limit:");
  gtk_box_pack_start (GTK_BOX (hbox2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  hiscale = gtk_hscale_new (GTK_ADJUSTMENT (itadjmax));
  scale_set_default_values (GTK_SCALE (hiscale));
  gtk_scale_set_digits (GTK_SCALE (hiscale), 0);
  gtk_box_pack_end (GTK_BOX (hbox2), hiscale, TRUE, TRUE, 0);
  gtk_widget_show (hiscale);

  gtk_signal_connect (GTK_OBJECT (itadjmin), "value_changed",
		      GTK_SIGNAL_FUNC (cb_lower_limit), itadjmax);

  gtk_signal_connect (GTK_OBJECT (itadjmax), "value_changed",
		      GTK_SIGNAL_FUNC (cb_upper_limit), itadjmin);

  return vbox;
}


void parse_data_file(const char* filename)
{
  ifstream in(filename);
  if (!in) {
    cerr << "Couldn't open: " << filename << endl;
    throw "parse_data_file: ifstream error";
  }

  const int bufsize = 4096;
  char buf[bufsize];
  in.getline(buf, bufsize);
  if (!in) throw "parse_data_file: error reading header line";

  string buffer(buf);
  size_t s1, s2;
  while (1) {
    s1 = buffer.find("\"");
    if (s1 >= buffer.size()) break;
    s2 = buffer.find("\"", s1+1);
    if (s2 >= buffer.size()) break;

    string label = buffer.substr(s1+1, s2-s1-1);
    if (label.size() > 0) {
      if (label.compare(string("Level")) != 0 && label.compare(string("Iter")) != 0)
	fieldNames.push_back(label);
    }

    buffer.erase(0, s2+1);
  }

  bool first = true;
  unsigned level;
  LevelRecord *levelrecord = NULL;
  unsigned Ndim = fieldNames.size() - 1;
  datarecord.data = vector<double>(Ndim);

  while (in) {
    in >> level;

    if (first) {
      levelrecord = new LevelRecord(level, Ndim);
      first = false;
    }
    else if (level != levelrecord->Level()) {
      Data.push_back(*levelrecord);
      levelrecord = new LevelRecord(level, Ndim);
    }
    
    in >> datarecord.iter;
    in >> datarecord.prob;
    for (unsigned i=0; i<Ndim; i++) in >> datarecord.data[i];

    *levelrecord += datarecord;
  }  

  cout << "Data size: " << Data.size() << endl;
  for (unsigned i=0; i<Data.size(); i++) {
    cout << "Level: " << Data[i].Level() 
	 << " itmin=" << Data[i].itmin
	 << " itmax=" << Data[i].itmax
	 << endl;
  }
}

int main( int   argc, char *argv[] )
{
  GtkWidget *window;
  GtkWidget *frame;
  GtkWidget *hbox;
  GtkWidget *main_hbox;
  GtkWidget *vbox_left;
  GtkWidget *vbox;
  GtkWidget *button;

  /* Initialise GTK */
  gtk_init(&argc, &argv);

  if (argc != 2) {
    cout << "No filename specfied\n";
    exit(0);
  }

  try {
    parse_data_file(argv[1]);
  }
  catch (const char *message) {
    cerr << message << endl;
    return 0;
  }

  /* Make the main window */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (gtk_main_quit),
                      NULL);

  gtk_window_set_title (GTK_WINDOW (window), "Distribution display controls");


  /* Top level container */
  main_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (main_hbox), 10);
  gtk_container_add (GTK_CONTAINER (window), main_hbox);

  /* Left hand side container */
  vbox_left = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox_left), 10);
  gtk_container_add (GTK_CONTAINER (main_hbox), vbox_left);
  
  /* Top left frame */

  frame = gtk_frame_new ("Levels and fields");
  gtk_box_pack_start (GTK_BOX (vbox_left), frame, TRUE, TRUE, 0);
  
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);
  
  if (Data.size()>8)
    hbox = level_scale();
  else
    hbox = level_radio();

  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 5);
  
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
  gtk_container_add (GTK_CONTAINER (vbox), hbox);

  gtk_container_add(GTK_CONTAINER (hbox), 
		    get_list("X-axis", &xlabel, x_menuitem_response));
  gtk_container_add(GTK_CONTAINER (hbox), 
		    get_list("Y-axis", &ylabel, y_menuitem_response));

  /* Center left frame */

  frame = gtk_frame_new ("Axis ranges");
  gtk_box_pack_start (GTK_BOX (vbox_left), frame, TRUE, TRUE, 0);
  
  vbox = axis_ranges();
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  /* Bottom left frame */

  frame = gtk_frame_new ("Iteration window");
  gtk_box_pack_start (GTK_BOX (vbox_left), frame, TRUE, TRUE, 0);
  
  vbox = iteration_window();
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  /* Right-hand frame */

  frame = gtk_frame_new ("Plot controls");
  gtk_box_pack_start (GTK_BOX (main_hbox), frame, TRUE, TRUE, 0);
  
  vbox = plot_controls();
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  
  /* Button bar */
  
  hbox = gtk_hbox_new(FALSE, 0);
  
  /* Add execute button */

  button = gtk_button_new_with_label ("Compute");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (compute_data),
		      (gpointer)"Compute!");
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 5);

  /* Add plot button */

  button = gtk_button_new_with_label ("Plot");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (make_plot),
		      (gpointer)"Plot!");
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 5);

  /* Add quit button */

  button = gtk_button_new_with_label ("QUIT");
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (window));
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 5);


  gtk_box_pack_start (GTK_BOX (vbox_left), hbox, FALSE, FALSE, 5);

  /* Bring up the widget */

  gtk_widget_show_all (window);

  /* Enter the event loop */
  gtk_main ();
    
  return(0);
}

