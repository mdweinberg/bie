#include <stdlib.h>
#include <math.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <strstream>
#include <vector>
#include <string>

#include <VtkDenest2d.h>

				// Global variables
string outfile("plot.ppm");
int flip = 0;

VtkDenest2d::VtkDenest2d(double xmin, double xmax, double ymin, double ymax,
			 vector<double>& x, vector<double>& y) : 
  Denest2d(xmin, xmax, ymin, ymax, x, y)
{
  thresholdC      = false;
  threshold       = false;
  use_outline     = true;
  contours        = 0;
  cdf_contours    = false;
  use_height      = false;
  use_labels      = true;
  reverse_colors  = false;
  preserve        = false;
  black_and_white = false;
  rainbow_map     = false;
  no_pedestal     = true;
  num_ticks       = 5;
  tube            = -1.0;
  hscale          = 1.0;
  tscale          = 0.8;
  xlab            = "x-axis";
  ylab            = "y-axis";
  tlab            = "";

#if VTK_MAJOR_VERSION > 4 || (VTK_MAJOR_VERSION==4 && VTK_MINOR_VERSION>=4)
  pscl = new double [3];
#else
  pscl = new float [3];
#endif

#if VTK_MAJOR_VERSION>=6 && VTK_MINOR_VERSION>=3
  tscale = 1.0;
#endif

  loaded = false;
}


VtkDenest2d::~VtkDenest2d(void)
{
  delete [] pscl;
}

void VtkDenest2d::loadVtk(void)
{
  double norm;

  if (!computed) compute();

  if (normVH) 
    norm = 1.0;
  else
    norm = norm2d(n1, n2);

  pscl[0] = pscl[1] = pscl[2] = 1.0;
  if (!preserve) {
#if ((VTK_MAJOR_VERSION==4) && (VTK_MINOR_VERSION>=4))
    double ratio = 1.0;
#else
    float ratio = 1.0;
#endif
    if (ymax > ymin) ratio = (xmax - xmin)/(ymax - ymin);
    if (ratio > 1.0) pscl[1] = ratio;
    if (ratio < 1.0) pscl[0] = 1.0/ratio;
  }

  //
  // Make grid
  //

#if ((VTK_MAJOR_VERSION==4) && (VTK_MINOR_VERSION>=4))
  double xyz[3], value;
#else
  float xyz[3], value;
#endif  

#if VTK_MAJOR_VERSION>=4
  scalars0 = vtkFloatArray::New();
  points0 = vtkPoints::New();
  points0->SetDataTypeToFloat();
#else
  scalars0 = vtkFloatScalars::New();
  points0 = vtkFloatPoints::New();
#endif


  double x, dx = (xmax - xmin)/(n1-1);
  double y, dy = (ymax - ymin)/(n2-1);
  double z;

  int offset, jOffset;

  min_value =  1.0e20;
  max_value = -1.0e20;

  for (int j=0; j<n2; j++) {

    jOffset = j * n1;

    for (int i=0; i<n1; i++) {
    
      offset = i + jOffset;
      
      xyz[0] = x = xmin + dx*i;
      xyz[1] = y = ymin + dy*j;
      value =  z = a[i][2*j]/norm;

      if (use_log)
	value = log(fabs(value)+pow(10.0, pedestal))/log(10.0);

      if (use_height) {
	if (use_log)
	  xyz[2] = hscale*(value - pedestal);
	else
	  xyz[2] = hscale*value;
      }
      else
	xyz[2] = 0.0;
      
      points0->InsertPoint(offset, xyz);
#if VTK_MAJOR_VERSION>=4
      scalars0->InsertValue(offset, value);
#else
      scalars0->InsertScalar(offset, value);
#endif

      min_value = min<float>(min_value, value);
      max_value = max<float>(max_value, value);

    }
  }

  loaded = true;
}


void VtkDenest2d::plotVtk(vtkRenderer *ren)
{
  if (!loaded) loadVtk();

  vtkActor *planeActor;
  vtkPolyDataMapper *planeMapper;
  vtkStructuredGridGeometryFilter *plane;
  vtkLight *light;

  //
  // Initialize rendering
  //

  vtkStructuredGrid *planeSet;
  planeSet = vtkStructuredGrid::New();

#ifdef DEBUG
  cout << "Min value =" << min_value << endl;
  cout << "Max value =" << max_value << endl;
#endif

  planeSet->SetDimensions(n1, n2, 1);

  planeSet->SetPoints(points0);
  points0->Delete();
  planeSet->GetPointData()->SetScalars(scalars0);
  scalars0->Delete();

  plane = vtkStructuredGridGeometryFilter::New();
#if VTK_MAJOR_VERSION >= 6
  plane->SetInputData(planeSet);
#else
  plane->SetInput(planeSet);
#endif
  plane->SetExtent(0, n1-1, 0, n2-1, 0, 0);
  planeSet->Delete();
  
  vtkCleanPolyData *planeClean;
  planeClean = vtkCleanPolyData::New();
#if VTK_MAJOR_VERSION >= 6
     planeClean->SetInputConnection(plane->GetOutputPort());
#else
     planeClean->SetInput(plane->GetOutput());
#endif
     plane->Delete();

  planeMapper = vtkPolyDataMapper::New();
#if VTK_MAJOR_VERSION >= 6
    planeMapper->SetInputConnection(planeClean->GetOutputPort());
#else
    planeMapper->SetInput(planeClean->GetOutput());
#endif
     planeClean->Delete();
     if (thresholdC)
       planeMapper->SetScalarRange(lo, hi);
     else
       planeMapper->SetScalarRange(min_value, max_value);
     planeMapper->ScalarVisibilityOn();

     if (rainbow_map) {
       vtkLookupTable* lt = vtkLookupTable::New();
       if (reverse_colors) lt->SetHueRange(0.667, 0.0);
       lt->Build();
       lt->SetTableValue(0, 1.0, 1.0, 1.0, 0.0);
       planeMapper->SetLookupTable(lt);
       lt->Delete();
     }
     else if (black_and_white) {
       vtkLookupTable* lt = vtkLookupTable::New();

       lt->SetNumberOfTableValues(256);
       int num = 1<<8;		// Number of gray values
       float val, one=1.0, zero=0.0;
       for (int i=0; i<num; i++) {
	 if (reverse_colors)
	   val = (float)(num-1-i)/(num-1);
	 else
	   val = (float)i/(num-1);
	 lt->SetTableValue(i, val, val, val, val);
       }
       if (no_pedestal) {
	 if (reverse_colors)
	   lt->SetTableValue(0, one, one, one, one);
	 else
	   lt->SetTableValue(0, zero, zero, zero, zero);
       }
       lt->Build();
       planeMapper->SetLookupTable(lt);
       lt->Delete();
     } else {
       vtkLookupTable* lt = vtkLookupTable::New();
       lt->SetTableRange (0, 2000);
       lt->SetSaturationRange (0.0, 0.8);
       lt->SetValueRange (0.3, 1.0);
       if (reverse_colors) {
	 lt->SetHueRange(0.05, 0.8);
       } else {
	 lt->SetHueRange (0.8, 0.05);
       }
       lt->Build();
       planeMapper->SetLookupTable(lt);
       lt->Delete();
     }

  planeActor = vtkActor::New();
     planeActor->SetMapper(planeMapper);
     planeActor->SetScale(pscl);
     planeMapper->Delete();

  vtkActor *contourActor = NULL;
  vtkContourFilter *contour;
  vtkCleanPolyData *isoClean;
  vtkPolyDataMapper *contourMapper;
  vtkTubeFilter *isoTubes = NULL;

  if (contours || cdf_contours) {

    contour = vtkContourFilter::New();
#if VTK_MAJOR_VERSION >= 6
    contour->SetInputConnection(plane->GetOutputPort());
#else
    contour->SetInput((vtkDataSet *)plane->GetOutput());
#endif
    
    if (cdf_contours) {
      
      compute_cdf();

      float element;
      for (int i=0; i<(int)CumDF.size(); i++) {
	if (use_log)
	  element = log(CumDF[i])/log(10.0);
	else
	  element = CumDF[i];
	contour->SetValue(i, element);
      }
      
    } else {

#ifdef DEBUG
      cout << "Generating " << contours << " contours\n";
#endif
      float tol = 1.0e-7 * max<float>(fabs(min_value), fabs(max_value));
    
      if (threshold)
	contour->GenerateValues(contours, lo, hi);
      else
	contour->GenerateValues(contours, min_value+tol, max_value-tol);
    }
    
    if (tube>0) {

      isoClean = vtkCleanPolyData::New();
#if VTK_MAJOR_VERSION >= 6
      isoClean->SetInputConnection(contour->GetOutputPort());
#else
      isoClean->SetInput(contour->GetOutput());
#endif
      contour->Delete();
      
      isoTubes = vtkTubeFilter::New();
#if VTK_MAJOR_VERSION >= 6
      isoTubes->SetInputConnection(isoClean->GetOutputPort());
#else
      isoTubes->SetInput(isoClean->GetOutput());
#endif
      isoTubes->SetRadius((xmax-xmin)*tube);
      isoClean->Delete();
    }
  
    contourMapper = vtkPolyDataMapper::New();
    if (tube>0) {
#if VTK_MAJOR_VERSION >= 6
      contourMapper->SetInputConnection(isoTubes->GetOutputPort());
#else
      contourMapper->SetInput(isoTubes->GetOutput());
#endif
      isoTubes->Delete();
    }
    else {
#if VTK_MAJOR_VERSION >= 6
      contourMapper->SetInputConnection(contour->GetOutputPort());
#else
      contourMapper->SetInput(contour->GetOutput());
#endif
      contour->Delete();
    }
    contourMapper->ScalarVisibilityOff();
  
    contourActor = vtkActor::New();
    contourActor->SetMapper(contourMapper);
    contourActor->GetProperty()->SetColor(0, 0, 0);
    contourActor->SetScale(pscl);
  }

  // Create axes and ScalarBar

  vtkAxisActor2D *axisX = vtkAxisActor2D::New();
  vtkAxisActor2D *axisY = vtkAxisActor2D::New();

  axisX->GetPoint1Coordinate()->SetCoordinateSystem(VTK_WORLD);
  if (flip & 0x2)
    axisX->GetPoint1Coordinate()->SetValue(xmax*pscl[0], ymax*pscl[1]);
  else
    axisX->GetPoint1Coordinate()->SetValue(xmin*pscl[0], ymin*pscl[1]);
  axisX->GetPoint2Coordinate()->SetCoordinateSystem(VTK_WORLD);
  if (flip & 0x2)
    axisX->GetPoint2Coordinate()->SetValue(xmin*pscl[0], ymax*pscl[1]);
  else
    axisX->GetPoint2Coordinate()->SetValue(xmax*pscl[0], ymin*pscl[1]);
  axisX->SetTitle((char *)xlab.c_str());
  axisX->AxisVisibilityOn();
  if (!num_ticks) {
    axisX->TickVisibilityOff();
    axisX->LabelVisibilityOff();
  } else {
    axisX->TickVisibilityOn();
    axisX->LabelVisibilityOn();
  }
  axisX->GetProperty()->SetColor(0.0, 0.0, 0.0);
#if VTK_MAJOR_VERSION>4 || (VTK_MAJOR_VERSION==4 && VTK_MINOR_VERSION>=2)
  axisX->GetTitleTextProperty()->ShadowOn();
  axisX->GetLabelTextProperty()->ShadowOn();
#else
  axisX->ShadowOn();
#endif

  axisY->GetPoint1Coordinate()->SetCoordinateSystem(VTK_WORLD);
  if (flip & 0x1)
    axisY->GetPoint1Coordinate()->SetValue(xmax*pscl[0], ymin*pscl[1]);
  else
    axisY->GetPoint1Coordinate()->SetValue(xmin*pscl[0], ymax*pscl[1]);
  axisY->GetPoint2Coordinate()->SetCoordinateSystem(VTK_WORLD);
  if (flip & 0x1)
    axisY->GetPoint2Coordinate()->SetValue(xmax*pscl[0], ymax*pscl[1]);
  else
    axisY->GetPoint2Coordinate()->SetValue(xmin*pscl[0], ymin*pscl[1]);
  axisY->SetTitle((char *)ylab.c_str());
  axisY->AxisVisibilityOn();
  if (!num_ticks) {
    axisY->TickVisibilityOff();
    axisY->LabelVisibilityOff();
  }
  else {
    axisY->TickVisibilityOn();
    axisY->LabelVisibilityOn();
  }
  axisY->GetProperty()->SetColor(0.0, 0.0, 0.0);
#if VTK_MAJOR_VERSION>4 || (VTK_MAJOR_VERSION==4 && VTK_MINOR_VERSION>=2)
  axisY->GetTitleTextProperty()->ShadowOn();
  axisY->GetLabelTextProperty()->ShadowOn();
#else
  axisY->ShadowOn();
#endif

#if VTK_MAJOR_VERSION>4 || (VTK_MAJOR_VERSION==4 && VTK_MINOR_VERSION>=4)
  double inRange[2], outRange[2], interval;
#else
  float inRange[2], outRange[2], interval;
#endif
  int inNumTicks=num_ticks+1, outNumTicks;

  inRange[0] = xmin;
  inRange[1] = xmax;

#ifdef DEBUG
  cout << "[xmin, xmax]=[" << xmin << ", " << xmax << "]\n";
  cout << "[ymin, ymax]=[" << ymin << ", " << ymax << "]\n";
#endif

  axisX->ComputeRange(inRange, outRange, inNumTicks, outNumTicks, interval);
  axisX->SetNumberOfLabels(outNumTicks); 
  axisX->SetRange(outRange);
  // axisX->SetRange(inRange);
  axisX->AdjustLabelsOn();
  axisX->SetFontFactor(tscale);

  inRange[0] = ymax;
  inRange[1] = ymin;

  axisY->ComputeRange(inRange, outRange, inNumTicks, outNumTicks, interval);
  axisY->SetNumberOfLabels(outNumTicks); 
  axisY->SetRange(outRange);
  // axisY->SetRange(inRange);
  axisY->AdjustLabelsOn();
  axisY->SetFontFactor(tscale);


  // Create a scalar bar
  vtkScalarBarActor* scalarBar = vtkScalarBarActor::New();
     scalarBar->SetLookupTable(planeMapper->GetLookupTable());
     scalarBar->SetTitle((char *)tlab.c_str());
     scalarBar->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
     if (preserve && ymax-ymin<xmax-xmin) // Move scalar bar downward in window
       scalarBar->GetPositionCoordinate()->SetValue(0.18, 0.5 + 0.38*(ymax-ymin)/(xmax-xmin));
     else
       scalarBar->GetPositionCoordinate()->SetValue(0.18, 0.88);
     scalarBar->SetOrientationToHorizontal();
     scalarBar->SetWidth(0.64);
     scalarBar->SetHeight(0.1);
     scalarBar->GetProperty()->SetColor(0.0, 0.0, 0.0);
#if VTK_MAJOR_VERSION>4 || (VTK_MAJOR_VERSION==4 && VTK_MINOR_VERSION>=2)
     scalarBar->GetTitleTextProperty()->ShadowOn();
     scalarBar->GetLabelTextProperty()->ShadowOn();
#else
     scalarBar->ShadowOn();
#endif

  // Create outline

  vtkActor* outlineActor = vtkActor::New();
  vtkOutlineFilter* outline = vtkOutlineFilter::New();
#if VTK_MAJOR_VERSION >= 6
     outline->SetInputData(planeSet);
#else
     outline->SetInput(planeSet);
#endif
  vtkPolyDataMapper* outlineMapper = vtkPolyDataMapper::New();
#if VTK_MAJOR_VERSION >= 6
     outlineMapper->SetInputConnection(outline->GetOutputPort());
#else
     outlineMapper->SetInput(outline->GetOutput());
#endif
     outlineActor->SetMapper(outlineMapper);
     outlineActor->GetProperty()->SetColor(0.0, 0.0, 0.0);
     outlineActor->SetScale(pscl);
     outline->Delete();
     outlineMapper->Delete();

  //
  // Test flip
  //

  if (flip) {

#if VTK_MAJOR_VERSION>4 || (VTK_MAJOR_VERSION==4 && VTK_MINOR_VERSION>=4)
    double *cen = planeActor->GetCenter();
    double *pos = planeActor->GetPosition();
    
    double posP[3];
    double posM[3];
#else
    float *cen = planeActor->GetCenter();
    float *pos = planeActor->GetPosition();
    
    float posP[3];
    float posM[3];
#endif
    for (int k=0; k<3; k++) {
      posP[k] = cen[k] - pos[k];
      posM[k] = -posP[k];
    }

    vtkTransform *trans = vtkTransform::New();
       // trans->Translate(posP);
       trans->Translate(posP[0], posP[1], posP[2]);
       if (flip & 0x1)
	 trans->RotateX(180.0);
       if (flip & 0x2)
	 trans->RotateY(180.0);
       // trans->Translate(posM);
       trans->Translate(posM[0], posP[1], posP[2]);

#if VTK_MAJOR_VERSION>=4
    planeActor->SetUserMatrix(trans->GetMatrix());
    if (contours || cdf_contours) 
      contourActor->SetUserMatrix(trans->GetMatrix());
#else
    planeActor->SetUserMatrix(trans->GetMatrixPointer());
    if (contours || cdf_contours) 
      contourActor->SetUserMatrix(trans->GetMatrixPointer());
#endif

    trans->Delete();
  }


  //
  // Rendering
  //
  
  light = vtkLight::New();
  ren->AddLight(light);

  ren->AddActor(planeActor);
  ren->SetBackground(255, 255, 255);
  // ren->SetBackground(0.08, 0.49, 0.68);
  planeActor->Delete();

				// Contours
  if (contours || cdf_contours) {
    ren->AddActor(contourActor);
    contourActor->Delete();
  }

				// Outline
  if (use_outline) {
    ren->AddActor(outlineActor);
  }
  outlineActor->Delete();

				// Axes
  if (use_labels) {
    ren->AddActor(axisX);
    ren->AddActor(axisY);
  }
  axisX->Delete();
  axisY->Delete();

				// Scalar Bar
  if (tlab.length()>0) {
    ren->AddActor(scalarBar);
  }
  scalarBar->Delete();


				// This is needed on replotting.
				// I'm not sure why . . .
  ren->GetActiveCamera()->SetWindowCenter(0, 0);
  ren->ResetCamera();

  light->SetFocalPoint(ren->GetActiveCamera()->GetFocalPoint());
  light->SetPosition(ren->GetActiveCamera()->GetPosition());
  light->Delete();
}

