// This is really -*- C++ -*-

#ifndef _DATAFIELDBOX_H
#define _DATAFIELDBOX_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

#include <gtkmm.h>
#include <Spinners.h>
#include <DenseBox.h>

struct DataRecord 
{
  unsigned iter;
  double prob;
  std::vector<double> data;
};

class LevelRecord
{
private:

  unsigned level;
  unsigned ndim;

public:

  unsigned itmin;
  unsigned itmax;

  double probmin;
  double probmax;

  std::vector<double> datamin;
  std::vector<double> datamax;

  std::vector<DataRecord> data;

  LevelRecord(int Lev, int Ndim) {
    level = Lev;
    ndim = Ndim;
    itmin = 1000000000;
    itmax = 0;
    probmin =  1.0e30;
    probmax = -1.0e30;
    datamin = std::vector<double>(ndim,  1.0e30);
    datamax = std::vector<double>(ndim, -1.0e30);
  }
    
  LevelRecord(const LevelRecord &v)
  {
    level = v.level;
    ndim = v.ndim;
    itmin = v.itmin;
    itmax = v.itmax;
    probmin = v.probmin;
    probmax = v.probmax;
    datamin = v.datamin;
    datamax = v.datamax;
    data = v.data;
  }

  unsigned Level(void) { return level; }

  void operator+=(DataRecord& r) {
    itmin = min<unsigned>(r.iter, itmin);
    itmax = max<unsigned>(r.iter, itmax);
    probmin = min<double>(r.prob, probmin);
    probmax = max<double>(r.prob, probmax);
    for (unsigned i=0; i<ndim; i++) {
      datamin[i] = min<double>(r.data[i], datamin[i]);
      datamax[i] = max<double>(r.data[i], datamax[i]);
    }

    data.push_back(r);
  }

};


class DataFieldBox : public Gtk::Frame
{

public:

  DataFieldBox(std::string file);
  virtual ~DataFieldBox();

  void addMenus(Gtk::HBox&, Gtk::HBox&);
  void resetMenuRanges();
  void fieldNumber(int);
  void setXRanges(double xmin, double xmax) 
    { setXRanges(pair<double, double>(xmin, xmax)); }
  void setYRanges(double ymin, double ymax)
    { setYRanges(pair<double, double>(ymin, ymax)); }
  void setXLimits(double xmin, double xmax)
    { setXLimits(pair<double, double>(xmin, xmax)); }
  void setYLimits(double ymin, double ymax)
    { setYLimits(pair<double, double>(ymin, ymax)); }

  void getRanges(double& xmin, double& xmax, double& ymin, double& ymax);

  Gtk::HBox &get_x_menu() {return m_HBox_X;}
  Gtk::HBox &get_y_menu() {return m_HBox_Y;}


  void getData(std::vector<double>& X, std::vector<double>& Y,
	       std::string& xlabel, std::string& ylabel);

  void getData(std::vector<double>& X, std::vector<double>& Y,
	       std::string& xlabel, std::string& ylabel, 
	       int imin, int imax);

  void getData(std::vector<double>& Y, std::string& ylabel, 
	       int imin, int imax);

  int getLevel() { return curLevel; }

  void setDensCallback(DenseBox* densebox);

  int Levels() { return Data.size(); }

  void Reread() { parse_data_file(); }

 protected:

  const double quant1 = 0.10;
  const double quant2 = 0.05;

  DenseBox *_densebox;

  std::vector<LevelRecord> Data;
  std::vector<std::string> fieldNames;
  
  DataRecord datarecord;

  Gtk::Menu m_Menu_X, m_Menu_Y;
  Gtk::HBox m_HBox_X, m_HBox_Y;

  std::string filename;
  void parse_data_file();
  void make_menus();
  
  void on_checkbutton_xoff();
  void on_checkbutton_yoff();

  void on_checkbutton_xfree();
  void on_checkbutton_yfree();

  void on_checkbutton_xquant();
  void on_checkbutton_yquant();

  int xpos, ypos;
  void on_menu_position_x(int i);
  void on_menu_position_y(int i);
  pair<double, double> set_menu_position_x(int i);
  pair<double, double> set_menu_position_y(int i);

  double xminC, xmaxC, yminC, ymaxC;

  void setXRanges(pair<double, double> x);
  void setYRanges(pair<double, double> y);
  void setXLimits(pair<double, double> x);
  void setYLimits(pair<double, double> x);

  pair<double, double> getQuantile(int pos);

  // Levels

  int curLevel;
  void erase_level_buttons();
  void on_levelbutton(int i) 
    { 
#ifdef DEBUG      
      cout << "Level set to " << i << endl;
#endif
      curLevel = i; 
      on_menu_position_x(xpos);
      on_menu_position_y(ypos);
      // On initial call, _densebox may not be assigned
      if (_densebox) {
	_densebox->SetMinIterRange(Data[i].itmin);
	_densebox->SetMaxIterRange(Data[i].itmax);
      }
    }

  // Child widgets:

  Gtk::Frame m_Frame_levels, m_Frame_fields;
  Gtk::Frame m_Frame_xaxis, m_Frame_yaxis;
  Gtk::Frame m_Frame_ranges, m_Frame_xrange, m_Frame_yrange;
  Gtk::Frame m_Frame_type;
  Gtk::VBox m_VBox_main, m_VBox_xranges, m_VBox_yranges;
  Gtk::HBox m_HBox_levels, m_HBox_fields;
  Gtk::HBox m_HBox_ranges, m_HBox_xrange, m_HBox_yrange;

  Spinners my_spinners_x_min, my_spinners_y_min;
  Spinners my_spinners_x_max, my_spinners_y_max;

  Gtk::CheckButton m_xoffset_cb, m_yoffset_cb;
  bool xoffset, yoffset;

  Gtk::CheckButton m_xfree_cb, m_yfree_cb;
  bool xfree, yfree;

  Gtk::CheckButton m_xquant_cb, m_yquant_cb;
  bool xquant, yquant;
};

#endif //_DATAFIELDBOX_H
