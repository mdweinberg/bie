#ifndef _SPINNERS_H
#define _SPINNERS_H

#include <string>
#include <gtkmm.h>

class Spinners : public Gtk::Frame
{
public:
  Spinners();
  Spinners(std::string);
  virtual ~Spinners();

  void set_limits(double, double);
  void set_value(double);
  double get_value();
  void set_direction(int);

protected:
  int direction;

  // Signal handlers:
  virtual void on_checkbutton_snap();
  virtual void on_spinbutton_digits_changed();

  // Child widgets:
  Gtk::HBox m_HBox, m_HBox_Buttons;
  Gtk::VBox m_VBox_Main, m_VBox, m_VBox_Value, m_VBox_Digits;
  Gtk::Label m_Label_Value, m_Label_Digits, m_Label_ShowValue;
  Gtk::Adjustment m_adjustment_value, m_adjustment_digits;
  Gtk::SpinButton m_SpinButton_Value, m_SpinButton_Digits;
  Gtk::CheckButton m_CheckButton_Snap;
};

#endif //_SPINNERS_H
