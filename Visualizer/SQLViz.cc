#include <gtkmm/main.h>
#include <DataViewerSQL.h>

int main(int argc, char *argv[])
{
  Gtk::Main main_instance(argc, argv);

  //
  // Init gtkglextmm.
  //
 
  Gtk::GL::init(argc, argv);

  //
  // Main application
  //

  int size = 400;
  string filename = "data.dat";

  if (argc>1) 
    filename = argv[1];
  else {
    cout << "Usage: " << argv[0] << " filename [size]\n";
    return 0;
  }
  if (argc>2) size = atoi(argv[2]);

#ifdef DEBUG
  cout << "Pixel size=" << size << endl;
#endif

  DataViewerSQL window(size, filename);

  // Start loop
  Gtk::Main::run(window);

  return 0;
}
