using namespace std;

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>

#include <gtkmm.h>

#include <SQLFieldBox.h>
#include <labeledoptionmenu.h>

SQLFieldBox::SQLFieldBox(string file)
  :
  filename(file),
  xpos(0), ypos(0),
  m_Frame_fields("Fields"),
  m_Frame_xaxis("X-axis"),
  m_Frame_yaxis("Y-axis"),
  m_Frame_ranges("Axis ranges"),
  m_Frame_xrange("X-axis"),
  m_Frame_yrange("Y-axis"),
  m_VBox_main(false, 10),
  m_HBox_fields(false, 10),
  m_HBox_ranges(false, 10),
  my_spinners_x_min("X min"), my_spinners_y_min("Y min"),
  my_spinners_x_max("X max"), my_spinners_y_max("X max"),
  m_Button_x_reread("New X range on reread"),
  m_Button_y_reread("New Y range on reread")
{
  // Data initialization

  parse_data_file();
  make_menus();
  on_menu_position_x(0);
  on_menu_position_y(0);

  // Widget initialization

  m_VBox_main.set_border_width(10);
  add(m_VBox_main);
  
  m_VBox_main.pack_start(m_Frame_fields, Gtk::PACK_SHRINK);
  m_VBox_main.pack_start(m_Frame_ranges, Gtk::PACK_SHRINK);

  m_HBox_fields.set_border_width(5);
  m_Frame_fields.add(m_HBox_fields);
  m_HBox_fields.set_homogeneous(true);

  m_Frame_xaxis.set_size_request(200, 50);
  m_Frame_yaxis.set_size_request(200, 50);

  m_HBox_fields.pack_start(m_Frame_xaxis, Gtk::PACK_SHRINK);
  m_HBox_fields.pack_start(m_Frame_yaxis, Gtk::PACK_SHRINK);

  m_HBox_ranges.set_border_width(10);
  m_Frame_ranges.add(m_HBox_ranges);
  
  m_VBox_xranges.set_homogeneous(false);
  m_VBox_xranges.pack_start(my_spinners_x_min);
  m_VBox_xranges.pack_start(my_spinners_x_max);
  m_VBox_xranges.pack_start(m_Button_x_reread);
  m_Button_x_reread.signal_clicked().connect( sigc::mem_fun(*this, &SQLFieldBox::on_reread_xrange) );
  x_reread_range = false;


  m_VBox_yranges.set_homogeneous(false);
  m_VBox_yranges.pack_start(my_spinners_y_min);
  m_VBox_yranges.pack_start(my_spinners_y_max);
  m_VBox_yranges.pack_start(m_Button_y_reread);
  m_Button_y_reread.signal_clicked().connect( sigc::mem_fun(*this, &SQLFieldBox::on_reread_yrange) );
  y_reread_range = false;
  

  m_HBox_ranges.set_homogeneous(true);
  m_HBox_ranges.add(m_VBox_xranges);
  m_HBox_ranges.add(m_VBox_yranges);

  show_all_children();
}

SQLFieldBox::~SQLFieldBox()
{
}

void SQLFieldBox::addMenus(Gtk::HBox& x, Gtk::HBox& y)
{
  m_Frame_xaxis.add(x);
  m_Frame_yaxis.add(y);
}


void SQLFieldBox::setXRanges(double xmin, double xmax)
{
  double del = 0.5*(xmax - xmin);
  my_spinners_x_min.set_limits(xmin-del, xmax+del);
  my_spinners_x_min.set_value(xmin);

  my_spinners_x_max.set_limits(xmin-del, xmax+del);
  my_spinners_x_max.set_value(xmax);
}

void SQLFieldBox::setYRanges(double ymin, double ymax)
{
  double del = 0.5*(ymax - ymin);
  my_spinners_y_min.set_limits(ymin-del, ymax+del);
  my_spinners_y_min.set_value(ymin);

  my_spinners_y_max.set_limits(ymin-del, ymax+del);
  my_spinners_y_max.set_value(ymax);
}

void SQLFieldBox::getRanges(double& xmin, double& xmax, 
			     double& ymin, double& ymax)
{
  xmin = my_spinners_x_min.get_value();
  xmax = my_spinners_x_max.get_value();
  ymin = my_spinners_y_min.get_value();
  ymax = my_spinners_y_max.get_value();
}



void SQLFieldBox::parse_data_file()
{
  ifstream in;

  try {
    in.open(filename.c_str());
    if (!in) {
      cerr << "Couldn't open: " << filename << endl;
      throw "parse_data_file: ifstream error";
    }
  }
  catch (const char *message) {
    cerr << message << endl;
    return;
  }
  
  const int bufsize = 32768;
  char buf[bufsize];

  // First line
  in.getline(buf, bufsize);
  if (!in) throw "parse_data_file: error reading header line";

  // Look for leading comment lines
  while (buf[0] == '-') {
    in.getline(buf, bufsize);
    if (!in) throw "parse_data_file: error reading header line";
  }

  string buffer(buf);
  size_t s1;
  while (1) {			// Parse the header line
    s1 = buffer.find("|");

    string label;
    if (s1>=buffer.size())
      label = buffer;
    else
      label = buffer.substr(0, s1);

    if (label.size() > 0) {
				// Remove leading spaces
      while (1) {
	if (label[0] != ' ') break;
	label = label.substr(1, label.size()-1);
      }
				// Remove trailing spaces
      while (1) {
	if (label[label.size()-1] != ' ') break;
	label = label.substr(0, label.size()-1);
      }
      
      fieldNames.push_back(label);
    }

    if (s1>=buffer.size()) break;
    buffer.erase(0, s1+1);
  }

				// Throw out next line
  in.getline(buf, bufsize);
  if (!in) throw "parse_data_file: error reading header line (2)";
  
  int Ndim = fieldNames.size();
  Data = DataSet(Ndim);
  datarecord.data = vector<double>(Ndim);
  string delim;

  while (in) {
    in.getline(buf, bufsize);
    if (in.rdstate() & ios::eofbit) break;
    if (!in) {
      string message = "parse_data_file: error reading data";
      if (in.rdstate() & ios::failbit)  message += " [fail]";
      if (in.rdstate() & ios::badbit)  message += " [bad]";
      throw message.c_str();
    }

				// Look for SQL trailer
    if (buf[0] == '(') {
      cout << "Sql trailer: " << buf << endl;
      break;
    }

    istringstream istr(buf);

    for (int i=0; i<Ndim; i++) {
      istr >> datarecord.data[i];
      if (i != Ndim-1) istr >> delim;
    }

    Data += datarecord;
  }  

#ifdef DEBUG
  cout << "Data size: " << Data.data.size() << endl;
#endif

  if (x_reread_range) on_menu_position_x(xpos);
  if (y_reread_range) on_menu_position_y(ypos);

}


void SQLFieldBox::make_menus()
{

  using namespace Gtk::Menu_Helpers;
    
  MenuList& list_vpos_x = m_Menu_X.items();
  MenuList& list_vpos_y = m_Menu_Y.items();

  for (int i=0; i<(int)fieldNames.size(); i++) {
    
    Glib::ustring lab(fieldNames[i]);

    list_vpos_x.push_back(MenuElem(lab, sigc::bind( sigc::mem_fun(*this, &SQLFieldBox::on_menu_position_x), i)) );

    list_vpos_y.push_back(MenuElem(lab, sigc::bind( sigc::mem_fun(*this, &SQLFieldBox::on_menu_position_y), i)) );
  }

  m_HBox_X.pack_start( *Gtk::manage(new LabeledOptionMenu("Field:", m_Menu_X)), Gtk::PACK_EXPAND_WIDGET, 5 );
  
  m_HBox_Y.pack_start( *Gtk::manage(new LabeledOptionMenu("Field:", m_Menu_Y)), Gtk::PACK_EXPAND_WIDGET, 5 );

}


void SQLFieldBox::getData(vector<double>& X, vector<double>& Y,
			 string& xlabel, string& ylabel)
{
#ifdef DEBUG
  cout << "SQLFieldBox: getting data for xpos, ypos="
       << xpos << ", " << ypos << "   size=" << Data.data.size() << endl;
#endif

  X.erase(X.begin(), X.end());
  Y.erase(Y.begin(), Y.end());
  
  vector<DataRecord>::iterator i;
  for (i=Data.data.begin(); i!=Data.data.end(); i++) {
    X.push_back(i->data[xpos]);
    Y.push_back(i->data[ypos]);
  }

  xlabel = fieldNames[xpos]; 
  ylabel = fieldNames[ypos]; 
}


void SQLFieldBox::on_menu_position_x(int i)
{
  xpos = i;

  double xmin = Data.datamin[xpos];
  double xmax = Data.datamax[xpos];

  setXRanges(xmin, xmax);
}

void SQLFieldBox::on_menu_position_y(int i)
{ 
  ypos = i;

  double ymin = Data.datamin[ypos];
  double ymax = Data.datamax[ypos];

  setYRanges(ymin, ymax);
}


void SQLFieldBox::on_reread_xrange()
{
  if (m_Button_x_reread.get_active())
    x_reread_range = true;
  else
    x_reread_range = false;
}

void SQLFieldBox::on_reread_yrange()
{
  if (m_Button_y_reread.get_active())
    y_reread_range = true;
  else
    y_reread_range = false;
}

