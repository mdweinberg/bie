using namespace std;

#define INTERNAL_COUNT

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>

#include <gtkmm.h>

#include "DataFieldBox.h"
#include "labeledoptionmenu.h"

static double minval = -1.0e30;
static double maxval =  1.0e30;

DataFieldBox::DataFieldBox(string file)
  :
  _densebox(0),
  filename(file),
  xpos(0), 			// Set x default to Iter
  ypos(1),			// Set y default to Proability
  xminC(minval), xmaxC(maxval),
  yminC(minval), ymaxC(maxval),
  curLevel(0),
  m_Frame_levels("Levels"),
  m_Frame_fields("Fields"),
  m_Frame_xaxis("X-axis"),
  m_Frame_yaxis("Y-axis"),
  m_Frame_ranges("Axis ranges"),
  m_Frame_xrange("X-axis"),
  m_Frame_yrange("Y-axis"),
  m_VBox_main(false, 10),
  m_HBox_levels(false, 10),
  m_HBox_fields(false, 10),
  m_HBox_ranges(false, 10),
  my_spinners_x_min("X min"), my_spinners_y_min("Y min"),
  my_spinners_x_max("X max"), my_spinners_y_max("Y max"),
  m_xoffset_cb("Offset from last X"), m_yoffset_cb("Offset from last Y"),
  xoffset(false), yoffset(false),
  m_xfree_cb("Free range in X"), m_yfree_cb("Free range in Y"),
  xfree(false), yfree(false),
  m_xquant_cb("Use quantile in X"),   m_yquant_cb("Use quantile in Y"),
  xquant(false), yquant(false)
{

  // Data initialization

  parse_data_file();
  make_menus();

  // Widget initialization

  m_VBox_main.set_border_width(10);
  add(m_VBox_main);
  
  m_VBox_main.pack_start(m_Frame_levels, Gtk::PACK_SHRINK);
  m_VBox_main.pack_start(m_Frame_fields, Gtk::PACK_SHRINK);
  m_VBox_main.pack_start(m_Frame_ranges, Gtk::PACK_SHRINK);
  // m_VBox_main.pack_start(m_Frame_type, Gtk::PACK_SHRINK);

  m_HBox_levels.set_border_width(5);
  m_Frame_levels.add(m_HBox_levels);
  m_HBox_levels.set_homogeneous(true);

  m_HBox_fields.set_border_width(5);
  m_Frame_fields.add(m_HBox_fields);
  m_HBox_fields.set_homogeneous(true);

  m_Frame_xaxis.set_size_request(200, 50);
  m_Frame_yaxis.set_size_request(200, 50);

  m_HBox_fields.pack_start(m_Frame_xaxis, Gtk::PACK_SHRINK);
  m_HBox_fields.pack_start(m_Frame_yaxis, Gtk::PACK_SHRINK);

  m_HBox_ranges.set_border_width(10);
  m_Frame_ranges.add(m_HBox_ranges);
  
  m_VBox_xranges.set_homogeneous(false);
  m_VBox_xranges.pack_start(my_spinners_x_min);
  m_VBox_xranges.pack_start(my_spinners_x_max);

  m_VBox_xranges.pack_start(m_xoffset_cb);
  m_xoffset_cb.set_active(xoffset);
  m_xoffset_cb.signal_clicked().
    connect( sigc::mem_fun(*this, &DataFieldBox::on_checkbutton_xoff) );

  m_VBox_xranges.pack_start(m_xfree_cb);
  m_xfree_cb.set_active(xfree);
  m_xfree_cb.signal_clicked().
    connect( sigc::mem_fun(*this, &DataFieldBox::on_checkbutton_xfree) );

  m_VBox_xranges.pack_start(m_xquant_cb);
  m_xquant_cb.set_active(xquant);
  m_xquant_cb.signal_clicked().
    connect( sigc::mem_fun(*this, &DataFieldBox::on_checkbutton_xquant) );

  m_VBox_yranges.set_homogeneous(false);
  m_VBox_yranges.pack_start(my_spinners_y_min);
  m_VBox_yranges.pack_start(my_spinners_y_max);

  m_VBox_yranges.pack_start(m_yoffset_cb);
  m_yoffset_cb.set_active(yoffset);
  m_yoffset_cb.signal_clicked().
    connect( sigc::mem_fun(*this, &DataFieldBox::on_checkbutton_yoff) );

  m_VBox_yranges.pack_start(m_yfree_cb);
  m_yfree_cb.set_active(yfree);
  m_yfree_cb.signal_clicked().
    connect( sigc::mem_fun(*this, &DataFieldBox::on_checkbutton_yfree) );

  m_VBox_yranges.pack_start(m_yquant_cb);
  m_yquant_cb.set_active(yquant);
  m_yquant_cb.signal_clicked().
    connect( sigc::mem_fun(*this, &DataFieldBox::on_checkbutton_yquant) );

  m_HBox_ranges.set_homogeneous(true);
  m_HBox_ranges.add(m_VBox_xranges);
  m_HBox_ranges.add(m_VBox_yranges);

  show_all_children();
}

DataFieldBox::~DataFieldBox()
{
  erase_level_buttons();
}

void DataFieldBox::setDensCallback(DenseBox *p)
{
  _densebox = p;
  _densebox->SetMinIterRange(Data[curLevel].itmin);
  _densebox->SetMaxIterRange(Data[curLevel].itmax);
}

void DataFieldBox::addMenus(Gtk::HBox& x, Gtk::HBox& y)
{
  m_Frame_xaxis.add(x);
  m_Frame_yaxis.add(y);
}

void DataFieldBox::erase_level_buttons()
{
				// Remove the old widgets
				// 
  vector<Gtk::Widget*> buttons = m_HBox_levels.get_children();
  vector<Gtk::Widget*>::iterator bb;
  for (bb=buttons.begin(); bb!=buttons.end(); bb++) {
    m_HBox_levels.remove(*(*bb));
  }
}


void DataFieldBox::fieldNumber(int n)
{
				// Number of current buttons
  vector<Gtk::Widget*> buttons = m_HBox_levels.get_children();
  int nsize = buttons.size();

				// Make new buttons
				// if needed
  Gtk::RadioButton::Group group;
  Gtk::RadioButton *button;

  if (nsize) 
    group = reinterpret_cast<Gtk::RadioButton*>(buttons[0])->get_group();

  for (int i=nsize; i<n; i++) {
    ostringstream ochar;
    ochar << i;
    Glib::ustring digit(ochar.str().c_str());
    
    button = Gtk::manage(new Gtk::RadioButton(digit));
    button->signal_toggled().connect( sigc::bind( sigc::mem_fun(*this, &DataFieldBox::on_levelbutton), i) );
    
    if (i==0 && nsize) {
      group = button->get_group();
    }
    else 
      button->set_group(group);

    if (i==curLevel) button->set_active();

    m_HBox_levels.pack_start(*button);
  }

  show_all_children();
}

void DataFieldBox::setXRanges(pair<double, double> x)
{
  setXLimits(x);
  my_spinners_x_min.set_value(x.first);
  my_spinners_x_max.set_value(x.second);
}

void DataFieldBox::setXLimits(pair<double, double> x)
{
  my_spinners_x_min.set_limits(xminC, xmaxC);
  my_spinners_x_max.set_limits(xminC, xmaxC);

  if (xfree) {
    my_spinners_x_min.set_limits(min<double>(x.first,  xminC),
				 max<double>(x.second, xmaxC));
    my_spinners_x_max.set_limits(min<double>(x.first,  xminC),
				 max<double>(x.second, xmaxC));
  }
}

void DataFieldBox::setYRanges(pair<double, double> y)
{
  setYLimits(y);
  my_spinners_y_min.set_value(y.first);
  my_spinners_y_max.set_value(y.second);
}

void DataFieldBox::setYLimits(pair<double, double> y)
{
  my_spinners_y_min.set_limits(yminC, ymaxC);
  my_spinners_y_max.set_limits(yminC, ymaxC);

  if (yfree) {
    my_spinners_y_min.set_limits(min<double>(y.first,  yminC),
				 max<double>(y.second, ymaxC));
    my_spinners_y_max.set_limits(min<double>(y.first,  yminC),
				 max<double>(y.second, ymaxC));
  }
}

void DataFieldBox::getRanges(double& xmin, double& xmax, 
			     double& ymin, double& ymax)
{
  xmin = my_spinners_x_min.get_value();
  xmax = my_spinners_x_max.get_value();
  ymin = my_spinners_y_min.get_value();
  ymax = my_spinners_y_max.get_value();
}



void DataFieldBox::parse_data_file()
{
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "Couldn't open: " << filename << endl;
    throw "parse_data_file: ifstream error";
  }

  const int bufsize = 131072;
  char buf[bufsize];
  in.getline(buf, bufsize);
  if (!in) throw "parse_data_file: error reading header line";

  fieldNames.erase(fieldNames.begin(), fieldNames.end());
  Data.erase(Data.begin(), Data.end());

  string buffer(buf);
				// Sanity check
  if (buffer.find("Level") == string::npos) {
    cerr << "First line of <" << filename 
	 << "> does not appear to be a header" << endl;
    throw "parse_data_file: file format error";
  }

  size_t s1, s2;
  while (1) {
    s1 = buffer.find("\"");
    if (s1 >= buffer.size()) break;
    s2 = buffer.find("\"", s1+1);
    if (s2 >= buffer.size()) break;

    string label = buffer.substr(s1+1, s2-s1-1);
    if (label.size() > 0) {
      if (label.compare(string("Level")) != 0) fieldNames.push_back(label);
    }

    buffer.erase(0, s2+1);
  }

  bool first = true;
  unsigned level;
#ifdef INTERNAL_COUNT
  unsigned count=0;
#endif
  LevelRecord *levelrecord = NULL;
  unsigned Ndim = fieldNames.size() - 2;
  datarecord.data = vector<double>(Ndim);

  while (in.getline(buf, bufsize, '\n').good()) {
    istringstream sin(buf);

    sin >> level;

    if (first) {
      levelrecord = new LevelRecord(level, Ndim);
      first = false;
    }
    else if (level != levelrecord->Level()) {
      Data.push_back(*levelrecord);
      delete levelrecord;
      levelrecord = new LevelRecord(level, Ndim);
#ifdef INTERNAL_COUNT
      count = 0;
#endif
    }
    
    sin >> datarecord.iter;
#ifdef INTERNAL_COUNT
    datarecord.iter = count++;
#endif
    sin >> datarecord.prob;
    for (unsigned i=0; i<Ndim; i++) sin >> datarecord.data[i];

    *levelrecord += datarecord;
  }  
  
  if (levelrecord->data.size()>0) Data.push_back(*levelrecord);
  delete levelrecord;

#ifdef DEBUG
  cout << "Data size: " << Data.size() << endl;
  for (unsigned i=0; i<Data.size(); i++) {
    cout << "Level: " << Data[i].Level() 
	 << " itmin=" << Data[i].itmin
	 << " itmax=" << Data[i].itmax
	 << "  Pmin=" << Data[i].probmin
	 << "  Pmax=" << Data[i].probmax
	 << endl;
  }
#endif

  fieldNumber(Data.size());
}


void DataFieldBox::make_menus()
{

  using namespace Gtk::Menu_Helpers;
    
  MenuList& list_vpos_x = m_Menu_X.items();
  MenuList& list_vpos_y = m_Menu_Y.items();

  // Make the menu choices from the fieldNames vector
  //
  for (int i=0; i<(int)fieldNames.size(); i++) {
    
    Glib::ustring lab(fieldNames[i]);

    list_vpos_x.push_back(MenuElem(lab, sigc::bind( sigc::mem_fun(*this, &DataFieldBox::on_menu_position_x), i)) );

    list_vpos_y.push_back(MenuElem(lab, sigc::bind( sigc::mem_fun(*this, &DataFieldBox::on_menu_position_y), i)) );
  }

  // Set default values (Iter & Probability)
  //
  m_Menu_X.set_active(xpos);
  m_Menu_Y.set_active(ypos);

  on_menu_position_x(xpos);
  on_menu_position_y(ypos);

  // Add the menus to the panel
  //
  m_HBox_X.pack_start( *Gtk::manage(new LabeledOptionMenu("Field:", m_Menu_X)), Gtk::PACK_EXPAND_WIDGET, 5 );
  
  m_HBox_Y.pack_start( *Gtk::manage(new LabeledOptionMenu("Field:", m_Menu_Y)), Gtk::PACK_EXPAND_WIDGET, 5 );
}

pair<double, double> DataFieldBox::getQuantile(int pos)
{
  vector<double> tdata;
  for (vector<DataRecord>::iterator
	 i=Data[curLevel].data.begin(); i!=Data[curLevel].data.end(); i++)
    {
      switch (pos) {
      case 0:
	tdata.push_back(i->iter);
	break;
      case 1:
	tdata.push_back(i->prob);
	break;
      default:
	tdata.push_back(i->data[pos-2]);
      }
    }

  std::sort(tdata.begin(), tdata.end());
  
  size_t imin = static_cast<size_t>(floor(quant1*tdata.size()+0.5));
  size_t imax = static_cast<size_t>(floor((1.0-quant1)*tdata.size()-0.5));
  
  imin = std::max<size_t>(imin, 0);
  imax = std::min<size_t>(imax, tdata.size()-1);
  
  return pair<double, double>(tdata[imin], tdata[imax]);
}


void DataFieldBox::getData(vector<double>& X, vector<double>& Y,
			   string& xlabel, string& ylabel)
{
#ifdef DEBUG
  cout << "DataFieldBox: getting data for level, xpos, ypos="
       << curLevel << ", " << xpos << ", " << ypos << endl;
#endif

  X.erase(X.begin(), X.end());
  Y.erase(Y.begin(), Y.end());
  
  vector<DataRecord>::iterator i;
  for (i=Data[curLevel].data.begin(); i!=Data[curLevel].data.end(); i++) {
    if (xpos==0)
      X.push_back(i->iter);
    else if (xpos==1)
      X.push_back(i->prob);
    else
      X.push_back(i->data[xpos-2]);

    if (ypos==0)
      Y.push_back(i->iter);
    else if (ypos==1)
      Y.push_back(i->prob);
    else
      Y.push_back(i->data[ypos-2]);
  }

  double xmin =  1.0e30;
  double xmax = -1.0e30;
  double ymin =  1.0e30;
  double ymax = -1.0e30;
  double val;

  if (xoffset) val = X.back();
  vector<double>::iterator j;
  for (unsigned j=0; j<X.size(); ++j) {
    if (xoffset) X[j] -= val;
    xmin = min<double>(xmin, X[j]);
    xmax = max<double>(xmax, X[j]);
  }

  xminC = xmin;
  xmaxC = xmax;

  if (xoffset && !xfree) {
    if (my_spinners_x_min.get_value()<xmin ||
	my_spinners_x_min.get_value()<xmin )
      my_spinners_x_min.set_value(xmin);
      
    if (my_spinners_x_max.get_value()<xmin ||
	my_spinners_x_max.get_value()>xmax )
      my_spinners_x_max.set_value(xmax);
  }

  if (yoffset) val = Y.back();
  for (unsigned j=0; j<Y.size(); ++j) {
    if (yoffset) Y[j] -= val;
    ymin = min<double>(ymin, Y[j]);
    ymax = max<double>(ymax, Y[j]);
  }

  yminC = ymin;
  ymaxC = ymax;

  if (yoffset && !yfree) {
    if (my_spinners_y_min.get_value()<ymin ||
	my_spinners_y_min.get_value()>ymax )
      my_spinners_y_min.set_value(ymin);

    if (my_spinners_y_max.get_value()<ymin ||
	my_spinners_y_max.get_value()>ymax )
      my_spinners_y_max.set_value(ymax);
  }
  
  xlabel = fieldNames[xpos]; 
  ylabel = fieldNames[ypos]; 

  //
  // Sanity check for valid ranges
  //
				// Xvalues
  if (xmax < my_spinners_x_min.get_value() ||
      xmin > my_spinners_x_max.get_value()  )
    {
      my_spinners_x_min.set_limits(xmin, xmax);
      my_spinners_x_max.set_limits(xmin, xmax);
      my_spinners_x_min.set_value(xmin);
      my_spinners_x_max.set_value(xmax);
    }

				// Yvalues
  if (ymax < my_spinners_y_min.get_value() ||
      ymin > my_spinners_y_max.get_value()  )
    {
      my_spinners_y_min.set_limits(ymin, ymax);
      my_spinners_y_max.set_limits(ymin, ymax);
      my_spinners_y_min.set_value(ymin);
      my_spinners_y_max.set_value(ymax);
    }
}


void DataFieldBox::getData(vector<double>& X, vector<double>& Y,
			 string& xlabel, string& ylabel, int imin, int imax)
{
#ifdef DEBUG
  cout << "DataFieldBox: getting data for level, xpos, ypos="
       << curLevel << ", " << xpos << ", " << ypos << endl;
#endif

  X.erase(X.begin(), X.end());
  Y.erase(Y.begin(), Y.end());
  
  vector<DataRecord>::iterator i;
  int icnt = 0;
  for (i=Data[curLevel].data.begin(); i!=Data[curLevel].data.end(); i++) {

    icnt++;
    if (icnt < imin || icnt > imax) continue;

    if (xpos==0)
      X.push_back(i->iter);
    else if (xpos==1)
      X.push_back(i->prob);
    else
      X.push_back(i->data[xpos-2]);

    if (ypos==0)
      Y.push_back(i->iter);
    else if (ypos==1)
      Y.push_back(i->prob);
    else
      Y.push_back(i->data[ypos-2]);
  }

  double xmin =  1.0e30;
  double xmax = -1.0e30;
  double ymin =  1.0e30;
  double ymax = -1.0e30;
  double val;


  if (xoffset) val = X.back();
  vector<double>::iterator j;
  for (unsigned j=0; j<X.size(); ++j) {
    if (xoffset) X[j] -= val;
    xmin = min<double>(xmin, X[j]);
    xmax = max<double>(xmax, X[j]);
  }

  xminC = xmin;
  xmaxC = xmax;

  if (xoffset && !xfree) {
    if (my_spinners_x_min.get_value()<xmin ||
	my_spinners_x_min.get_value()<xmin )
      my_spinners_x_min.set_value(xmin);
    
    if (my_spinners_x_max.get_value()<xmin ||
	my_spinners_x_max.get_value()>xmax )
      my_spinners_x_max.set_value(xmax);
  }
  

  if (yoffset) val = Y.back();
    for (unsigned j=0; j<Y.size(); ++j) {
    if (yoffset) Y[j] -= val;
    ymin = min<double>(ymin, Y[j]);
    ymax = max<double>(ymax, Y[j]);
  }
  
  yminC = ymin;
  ymaxC = ymax;

  if (yoffset && !yfree) {
    if (my_spinners_y_min.get_value()<ymin ||
	my_spinners_y_min.get_value()>ymax )
      my_spinners_y_min.set_value(ymin);
    
    if (my_spinners_y_max.get_value()<ymin ||
	my_spinners_y_max.get_value()>ymax )
      my_spinners_y_max.set_value(ymax);
  }

  xlabel = fieldNames[xpos]; 
  ylabel = fieldNames[ypos]; 

  //
  // Sanity check for valid ranges
  //
				// Xvalues
  if (xmax < my_spinners_x_min.get_value() ||
      xmin > my_spinners_x_max.get_value()  )
    {
      my_spinners_x_min.set_limits(xmin, xmax);
      my_spinners_x_max.set_limits(xmin, xmax);
      my_spinners_x_min.set_value(xmin);
      my_spinners_x_max.set_value(xmax);
    }

				// Yvalues
  if (ymax < my_spinners_y_min.get_value() ||
      ymin > my_spinners_y_max.get_value()  )
    {
      my_spinners_y_min.set_limits(ymin, ymax);
      my_spinners_y_max.set_limits(ymin, ymax);
      my_spinners_y_min.set_value(ymin);
      my_spinners_y_max.set_value(ymax);
    }
}


// This one is for 1-dim marginalization
//
void DataFieldBox::getData(vector<double>& Y,
			   string& ylabel, int imin, int imax)
{
#ifdef DEBUG
  cout << "DataFieldBox: getting data for level, ypos="
       << curLevel << ", " << ypos << endl;
#endif

  Y.erase(Y.begin(), Y.end());
  
  vector<DataRecord>::iterator i;
  int icnt = 0;
  for (i=Data[curLevel].data.begin(); i!=Data[curLevel].data.end(); i++) {

    icnt++;
    if (icnt < imin || icnt > imax) continue;

    if (ypos==0)
      Y.push_back(i->iter);
    else if (ypos==1)
      Y.push_back(i->prob);
    else
      Y.push_back(i->data[ypos-2]);
  }

  if (yoffset) {
    double val = Y.back();
    vector<double>::iterator j;
    for (unsigned j=0; j<Y.size(); ++j) Y[j] -= val;
  }

  ylabel = fieldNames[ypos]; 
}


void DataFieldBox::resetMenuRanges()
{
  pair<double, double> x, y;
  if (xoffset) {
    x.first  = my_spinners_x_min.get_value();
    x.second = my_spinners_x_max.get_value();
  }
  else 
    x = set_menu_position_x(xpos);

  if (yoffset) {
    y.first  = my_spinners_y_min.get_value();
    y.second = my_spinners_y_max.get_value();
  }
  else 
    y = set_menu_position_y(ypos);

  if (!xfree) setXRanges(x);
  if (!yfree) setYRanges(y);
}


void DataFieldBox::on_menu_position_x(int i)
{
  pair<double, double> x = set_menu_position_x(i);
  xminC = x.first;
  xmaxC = x.second;

  xoffset = false;
  m_xoffset_cb.set_active(xoffset);

  xfree   = false;
  m_xfree_cb.set_active(xfree);

  xquant  = false;
  m_xquant_cb.set_active(xquant);

  setXRanges(x);
}

pair<double, double> DataFieldBox::set_menu_position_x(int i)
{
  xpos = i;

  pair<double, double> x;

  if (xpos==0) {
    x.first  = Data[curLevel].itmin;
    x.second = Data[curLevel].itmax;
  }
  else if (xpos==1) {
    x.first  = Data[curLevel].probmin;
    x.second = Data[curLevel].probmax;
  }
  else if (xquant) {
    x = getQuantile(xpos);
  }
  else {
    x.first  = Data[curLevel].datamin[xpos-2];
    x.second = Data[curLevel].datamax[xpos-2];
  }

  return x;
}

void DataFieldBox::on_menu_position_y(int i)
{
  pair<double, double> y = set_menu_position_y(i);
  yminC = y.first;
  ymaxC = y.second;

  yoffset = false;
  m_yoffset_cb.set_active(yoffset);

  yfree   = false;
  m_yfree_cb.set_active(yfree);
  
  yquant  = false;
  m_yquant_cb.set_active(yquant);
  
  setYRanges(y);
}

pair<double, double> DataFieldBox::set_menu_position_y(int i)
{ 
  ypos = i;

  pair<double, double> y;

  if (ypos==0) {
    y.first  = Data[curLevel].itmin;
    y.second = Data[curLevel].itmax;
  }
  else if (ypos==1) {
    y.first  = Data[curLevel].probmin;
    y.second = Data[curLevel].probmax;
  }
  else if (yquant) {
    y = getQuantile(ypos);
  } 
  else {
    y.first  = Data[curLevel].datamin[ypos-2];
    y.second = Data[curLevel].datamax[ypos-2];
  }

  return y;
}

void DataFieldBox::on_checkbutton_xoff()
{
  xoffset = m_xoffset_cb.get_active();

  pair<double, double> x = set_menu_position_x(xpos);
  xminC = x.first;
  xmaxC = x.second;
}

void DataFieldBox::on_checkbutton_yoff()
{
  yoffset = m_yoffset_cb.get_active();

  pair<double, double> y = set_menu_position_y(ypos);
  yminC = y.first;
  ymaxC = y.second;
}

void DataFieldBox::on_checkbutton_xfree()
{
  xfree = m_xfree_cb.get_active();
  setXLimits(minval, maxval);
}

void DataFieldBox::on_checkbutton_yfree()
{
  yfree = m_yfree_cb.get_active();
  setYLimits(minval, maxval);
}

void DataFieldBox::on_checkbutton_xquant()
{
  xquant = m_xquant_cb.get_active();
  if (xquant) {
    pair<double, double> x = getQuantile(xpos);
    setXRanges(x.first, x.second);
  }
    
}

void DataFieldBox::on_checkbutton_yquant()
{
  yquant = m_yquant_cb.get_active();
  if (yquant) {
    pair<double, double> y = getQuantile(ypos);
    setYRanges(y.first, y.second);
  }
}

