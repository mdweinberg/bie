#ifndef GTKMM_HELPWINDOW_H
#define GTKMM_HELPWINDOW_H

#include <gtkmm.h>

class HelpWindow : public Gtk::Window
{
 public:
  HelpWindow();
  virtual ~HelpWindow();

 protected:

  virtual void fill_buffers();
  
  //Signal handlers:
  virtual void on_button_quit();
  virtual void on_button_buffer1();
  virtual void on_button_buffer2();

  //Child widgets:
  Gtk::VBox m_VBox;

  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::TextView m_TextView;
  
  Glib::RefPtr<Gtk::TextBuffer> m_refTextBuffer1, m_refTextBuffer2;

  Gtk::HButtonBox m_ButtonBox;
  Gtk::Button m_Button_Quit, m_Button_Buffer1, m_Button_Buffer2;
};

#endif //GTKMM_HELPWINDOW_H
