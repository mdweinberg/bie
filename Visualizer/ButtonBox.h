#include <gtkmm.h>

class ButtonBox : public Gtk::Frame
{
public:
  ButtonBox();
  virtual ~ButtonBox();

protected:
  Gtk::Frame* create_button_box(bool horizontal, const Glib::ustring& title,
                                int spacing, Gtk::ButtonBoxStyle layout);

  //Member widgets:
  Gtk::Frame m_Frame_Horizontal, m_Frame_Vertical;
  Gtk::VBox m_VBox_Main, m_VBox;
  Gtk::HBox m_HBox;
};

