#include "config.h"
#include "HelpWindow.h"

HelpWindow::HelpWindow()
  : m_Button_Quit(Gtk::Stock::QUIT),
    m_Button_Buffer1("Basic usage"),
    m_Button_Buffer2("Control tabs")
{
  set_title("Help on VISUALIZE");
  set_border_width(5);
  set_default_size(400, 400);


  add(m_VBox);

  //Add the TreeView, inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.add(m_TextView);

  //Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  m_VBox.pack_start(m_ScrolledWindow);

  //Add buttons: 
  m_VBox.pack_start(m_ButtonBox, Gtk::PACK_SHRINK);

  m_ButtonBox.pack_start(m_Button_Buffer1, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Buffer2, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Quit, Gtk::PACK_SHRINK);
  m_ButtonBox.set_border_width(5);
  m_ButtonBox.set_spacing(5);
  m_ButtonBox.set_layout(Gtk::BUTTONBOX_END);

  
  //Connect signals:
  m_Button_Quit.signal_clicked().connect( sigc::mem_fun(*this, &HelpWindow::on_button_quit) );
  m_Button_Buffer1.signal_clicked().connect( sigc::mem_fun(*this, &HelpWindow::on_button_buffer1) );
  m_Button_Buffer2.signal_clicked().connect( sigc::mem_fun(*this, &HelpWindow::on_button_buffer2) );
  
  fill_buffers();
  on_button_buffer1();

  show_all_children();
}

char main_txt[] = 
"\n === VISUALIZE " VERSION" ===\n\n . . . for a quick look at BIE output\n\n\
\n\
Basic usage:\n\
\n\tvisualize bie_output.dat\n\n\
\n\
The clickable tabs across the top select different\n\
plots or controls.  The buttons along the bottom\n\
execute various actions.  This page describes the\n\
first three tabs and the actions.  Click on the\n\
\"Control tabs\" button at the bottom of this window\n\
to view help on the density and rendering controls.\n\
The \"Quit\" button will hide the help window.\n\
\n\
On startup, \"visualize\" reads the BIE posterior\n\
probabilty log file given on the command line and lists\n\
the fields and their ranges on the \"Fields\" tab.  From\n\
this tab, you can select which fields to plot, and\n\
adjust the ranges for each data dimension.\n\
\n\
After setting the data parameters in the \"Fields\"\n\
tab, you create a curve of x versus y values by\n\
clicking on the \"Curve\" button.  Or you can create a\n\
contour plot of the probability density marginalized\n\
over all variables but x and y using kernel density\n\
estimation by clicking on the \"Contour\" button.  You\n\
can compute a one-dimensional marginalized probability\n\
distribution by clicking on the \"Marginal\" button\n\
The y-axis selection is used to compute the independent\n\
variable for the distribution.  Because both curves and\n\
the marginal and the x-y plot share the same renderer,\n\
you must return to the \"Fields\" tab before executing\n\
\"Marginal\".  This actions loads the appropriate data.\n\
\n\
You may click on the \"Fields\" tab at any time to select\n\
different data values or parameters.  The ranges can be\n\
changed to limit the extent of the plot or zoom in on\n\
paritcular regions of parameter space.  The\n\
\"Truncate\" checkbox will truncate the data to the\n\
desired number of digits.  You may offset each variable\n\
by the last value at each level to help rescale your\n\
plots by clicking the checkbox; this is useful if the\n\
absolute value is very large.  The \"Free range\"\n\
checkbox removes the restriction to a variable's\n\
inclusive range.\n\
\n\
The \"Reread\" button refreshes the cached data from\n\
the command-line specified file.  This is useful if\n\
you monitoring the log file from a running simulation.\n\
\n\
The \"Jpeg\" button writes the currently visible plot to\n\
a JPEG-format image file.  The x-y plot image is called\n\
\"curve.jpg\" and the current density image is called\n\
\"contour.jpg\".\n\
\n\
M. Weinberg 2/11/07\n";

char config_txt[] = "\n === Using the control tabs ===\n\n\
Density controls:\n\
\n\
These adjustments control the properties of the kernel\n\
density estimation. The default kernel width is chosen by\n\
estimating the optimal value assuming an underlying\n\
Gaussian distribution.  The value can be adjusted by a\n\
multiplicative factor of this optimal value using the\n\
\"Kernel scale\" slider.  By default, the probability\n\
distribution is normalized over the entire area.  You\n\
can choose to normalize by rows and columns using the\n\
checkboxes.  You may compute the marginal cumulative\n\
probability rather than the marginal partial probability\n\
density when you click \"Marginal\" by selecting the\n\
\"Cumulative marginal\" checkbox.  Finally, you may\n\
limit the number range of iteration values using the\n\
sliders at the bottom of the page (e.g. to select only\n\
the states post burn-in).\n\
\n\
Render controls:\n\
\n\
These specify the properties of the rendered contour\n\
map.  You can select logarithmic spacing of the\n\
contour values, reverse the direction default color\n\
map, or select a black and white color map via checkbox.\n\
\n\
You may limit the contour ranges the \"Level range\"\n\
sliders and checking \"Threshold contours\".  You may\n\
apply this same range restriction to the color rendering\n\
by checking \"Apply to field\"\n\
\n\
The \"CDF contour\" checkbox chooses the following set\n\
of standard quantiles for contour lines: 0.001, 0.005,\n\
0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8,\n\
0.9, 0.95, 0.99, 0.995, 0.999.  The median is the nineth\n\
contour value\n\
\n\
M. Weinberg 2/11/07\n";

void HelpWindow::fill_buffers()
{
  Glib::ustring tag_name("not_editable");

  m_refTextBuffer1 = Gtk::TextBuffer::create();
  m_refTextBuffer1->set_text(main_txt);
  m_refTextBuffer1->create_tag(tag_name)->property_editable() = FALSE;
  m_refTextBuffer1->apply_tag_by_name(tag_name, 
				      m_refTextBuffer1->begin(),
				      m_refTextBuffer1->end()
				      );

  m_refTextBuffer2 = Gtk::TextBuffer::create();
  m_refTextBuffer2->set_text(config_txt);
  m_refTextBuffer2->create_tag(tag_name)->property_editable() = FALSE;
  m_refTextBuffer2->apply_tag_by_name(tag_name,
				      m_refTextBuffer2->begin(),
				      m_refTextBuffer2->end()
				      );
}

HelpWindow::~HelpWindow()
{
}

void HelpWindow::on_button_quit()
{
  hide();
}

void HelpWindow::on_button_buffer1()
{
  m_TextView.set_buffer(m_refTextBuffer1);
}

void HelpWindow::on_button_buffer2()
{
  m_TextView.set_buffer(m_refTextBuffer2);
}


