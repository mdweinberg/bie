#include <unistd.h>
#include <math.h>

#include <iostream>

#include <XYPlot.h>

int main()
{
  int dataSize = 100;
  vector<double> X(dataSize), Y(dataSize);
  for(int i=0; i<dataSize; i++)
    {
      X[i] = 0.01*i;
      Y[i] = 1.0e-4*i*i;
    }
  
  XYPlot plot(X, Y);
  string xlab("dog"), ylab("cat");
  plot.setXlabel(xlab);
  plot.setYlabel(ylab);

  plot.plotVtk();

  char c;
  cin >> c;

  return 0;
}

