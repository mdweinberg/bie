#include <vtkCylinderSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <vtkCamera.h>

void TestCylinder(vtkRenderer *ren)
{
  // This creates a polygonal cylinder model with eight circumferential facets.
  //
  vtkCylinderSource *cylinder = vtkCylinderSource::New();
  cylinder->SetResolution(8);

  // The mapper is responsible for pushing the geometry into the graphics
  // library. It may also do color mapping, if scalars or other attributes
  // are defined.
  //
  vtkPolyDataMapper *cylinderMapper = vtkPolyDataMapper::New();
#if VTK_MAJOR_VERSION >= 6
  cylinderMapper->SetInputConnection(cylinder->GetOutputPort());
#else
  cylinderMapper->SetInput(cylinder->GetOutput());
#endif

  // The actor is a grouping mechanism: besides the geometry (mapper), it
  // also has a property, transformation matrix, and/or texture map.
  // Here we set its color and rotate it -22.5 degrees.
  vtkActor *cylinderActor = vtkActor::New();
  cylinderActor->SetMapper(cylinderMapper);
  cylinderActor->GetProperty()->SetColor(1.0000, 0.3882, 0.2784);
  cylinderActor->RotateX(30.0);
  cylinderActor->RotateY(-45.0);

  // Create the graphics structure. The renderer renders into the 
  // render window. The render window interactor captures mouse events
  // and will perform appropriate camera or actor manipulation
  // depending on the nature of the events.
  //

  // Add the actors to the renderer, set the background and size
  //
  ren->AddActor(cylinderActor);
  ren->SetBackground(0.1, 0.2, 0.4);

  // We'll zoom in a little by accessing the camera and invoking a "Zoom"
  // method on it.
  ren->GetActiveCamera()->Zoom(1.5);
  
  // Exiting from here, we have to delete all the instances that
  // have been created.
  cylinder->Delete();
  cylinderMapper->Delete();
  cylinderActor->Delete();
}
