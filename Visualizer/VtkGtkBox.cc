//
//  Copyright (C) 2003-2008 Martin D. Weinberg <weinberg@astro.umass.edu>
//

#include "VtkGtkBox.h"

#include <gdk/gdkx.h>
#include <vtkWindowToImageFilter.h>
#include <vtkJPEGWriter.h>
#include <vtkCamera.h>
#include <vtkActor.h>
#include <vtkActorCollection.h>


using std::string;

//
// Print a configuration attribute.
//
void GLConfigUtil::print_gl_attrib(const Glib::RefPtr<const Gdk::GL::Config>& glconfig,
                                   const char* attrib_str,
                                   int attrib,
                                   bool is_boolean)
{
  int value;

  if (glconfig->get_attrib(attrib, value))
    {
      std::cout << attrib_str << " = ";
      if (is_boolean)
        std::cout << (value == true ? "true" : "false") << std::endl;
      else
        std::cout << value << std::endl;
    }
  else
    {
      std::cout << "*** Cannot get "
                << attrib_str
                << " attribute value\n";
    }
}

//
// Print configuration attributes.
//
void GLConfigUtil::examine_gl_attrib(const Glib::RefPtr<const Gdk::GL::Config>& glconfig)
{
  std::cout << "\nOpenGL visual configurations :\n\n";

  std::cout << "glconfig->is_rgba() = "
            << (glconfig->is_rgba() ? "true" : "false")
            << std::endl;
  std::cout << "glconfig->is_double_buffered() = "
            << (glconfig->is_double_buffered() ? "true" : "false")
            << std::endl;
  std::cout << "glconfig->is_stereo() = "
            << (glconfig->is_stereo() ? "true" : "false")
            << std::endl;
  std::cout << "glconfig->has_alpha() = "
            << (glconfig->has_alpha() ? "true" : "false")
            << std::endl;
  std::cout << "glconfig->has_depth_buffer() = "
            << (glconfig->has_depth_buffer() ? "true" : "false")
            << std::endl;
  std::cout << "glconfig->has_stencil_buffer() = "
            << (glconfig->has_stencil_buffer() ? "true" : "false")
            << std::endl;
  std::cout << "glconfig->has_accum_buffer() = "
            << (glconfig->has_accum_buffer() ? "true" : "false")
            << std::endl;

  std::cout << std::endl;

  print_gl_attrib(glconfig, "Gdk::GL::USE_GL",           Gdk::GL::USE_GL,           true);
  print_gl_attrib(glconfig, "Gdk::GL::BUFFER_SIZE",      Gdk::GL::BUFFER_SIZE,      false);
  print_gl_attrib(glconfig, "Gdk::GL::LEVEL",            Gdk::GL::LEVEL,            false);
  print_gl_attrib(glconfig, "Gdk::GL::RGBA",             Gdk::GL::RGBA,             true);
  print_gl_attrib(glconfig, "Gdk::GL::DOUBLEBUFFER",     Gdk::GL::DOUBLEBUFFER,     true);
  print_gl_attrib(glconfig, "Gdk::GL::STEREO",           Gdk::GL::STEREO,           true);
  print_gl_attrib(glconfig, "Gdk::GL::AUX_BUFFERS",      Gdk::GL::AUX_BUFFERS,      false);
  print_gl_attrib(glconfig, "Gdk::GL::RED_SIZE",         Gdk::GL::RED_SIZE,         false);
  print_gl_attrib(glconfig, "Gdk::GL::GREEN_SIZE",       Gdk::GL::GREEN_SIZE,       false);
  print_gl_attrib(glconfig, "Gdk::GL::BLUE_SIZE",        Gdk::GL::BLUE_SIZE,        false);
  print_gl_attrib(glconfig, "Gdk::GL::ALPHA_SIZE",       Gdk::GL::ALPHA_SIZE,       false);
  print_gl_attrib(glconfig, "Gdk::GL::DEPTH_SIZE",       Gdk::GL::DEPTH_SIZE,       false);
  print_gl_attrib(glconfig, "Gdk::GL::STENCIL_SIZE",     Gdk::GL::STENCIL_SIZE,     false);
  print_gl_attrib(glconfig, "Gdk::GL::ACCUM_RED_SIZE",   Gdk::GL::ACCUM_RED_SIZE,   false);
  print_gl_attrib(glconfig, "Gdk::GL::ACCUM_GREEN_SIZE", Gdk::GL::ACCUM_GREEN_SIZE, false);
  print_gl_attrib(glconfig, "Gdk::GL::ACCUM_BLUE_SIZE",  Gdk::GL::ACCUM_BLUE_SIZE,  false);
  print_gl_attrib(glconfig, "Gdk::GL::ACCUM_ALPHA_SIZE", Gdk::GL::ACCUM_ALPHA_SIZE, false);

  std::cout << std::endl;
}


VtkGtkBox::VtkGtkBox()
{
  //
  // Configure OpenGL-capable visual.
  //

  Glib::RefPtr<Gdk::GL::Config> glconfig;

  // Try double-buffered visual
  glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB    |
                                     Gdk::GL::MODE_DEPTH  |
                                     Gdk::GL::MODE_DOUBLE);
  if (!glconfig)
    {
      std::cerr << "*** Cannot find the double-buffered visual.\n"
                << "*** Trying single-buffered visual.\n";

      // Try single-buffered visual
      glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB   |
                                         Gdk::GL::MODE_DEPTH);
      if (!glconfig)
        {
          std::cerr << "*** Cannot find any OpenGL-capable visual.\n";
          std::exit(1);
        }
    }

#ifdef VERBOSE
  // print frame buffer attributes.
  GLConfigUtil::examine_gl_attrib(glconfig);
#endif

  //
  // Set OpenGL-capability to the widget.
  //

  set_gl_capability(glconfig);

  //
  // VTK initialization
  //
  ren = vtkRenderer::New();
  ren->LightFollowCameraOn();
  ren_win = vtkOpenGLRenderWindow::New();
  ren_win->AddRenderer(ren);  

}

VtkGtkBox::~VtkGtkBox()
{
  // Work around for some dependency between the VTK classes and
  // the GTK classes: don't delete the renderer and render window
  /*
    ren_win->Delete();
    ren->Delete();
  */
}

void VtkGtkBox::on_realize()
{
  // We need to call the base on_realize()
  Gtk::DrawingArea::on_realize();

  //
  // Attach VTK renderer to current OpenGL window
  //
  window = get_window()->gobj();
  wid = (void *)GDK_WINDOW_XWINDOW(window);

  ren_win->SetWindowId(wid);
}

bool VtkGtkBox::on_configure_event(GdkEventConfigure* event)
{
  //
  // Get GL::Window.
  //

  Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();

  //
  // GL calls.
  //

  // *** OpenGL BEGIN ***
  if (!glwindow->gl_begin(get_gl_context()))
    return false;

  glViewport(0, 0, get_width(), get_height());

  glwindow->gl_end();
  // *** OpenGL END ***

  // Set the size to agree with the GtkFrame
  //
  ren_win->SetSize(get_width(), get_height());
  ren->GetActiveCamera()->GetPosition(initial_position);

  return true;
}

bool VtkGtkBox::on_expose_event(GdkEventExpose* event)
{
  //
  // Get GL::Window.
  //

  Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();

  //
  // GL calls.
  //

  // *** OpenGL BEGIN ***
  if (!glwindow->gl_begin(get_gl_context()))
    return false;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glCallList(1);

  // Swap buffers.
  if (glwindow->is_double_buffered())
    glwindow->swap_buffers();
  else
    glFlush();

  glwindow->gl_end();
  // *** OpenGL END ***

  //
  // Render via VTK
  //
  ren_win->Render();

  return true;
}


vtkRenderer* VtkGtkBox::get_renderer() 
{
  ren_win->RemoveRenderer(ren);
  ren->Delete();
  ren = vtkRenderer::New();
  ren_win->AddRenderer(ren);

  return ren;
}

void VtkGtkBox::reset_view() 
{
  ren->GetActiveCamera()->SetWindowCenter(0, 0);
  ren->GetActiveCamera()->SetPosition(initial_position);
  ren->GetActiveCamera()->SetViewUp(0, 0, 1);
  ren->GetActiveCamera()->SetViewAngle(30.0);
  ren->ResetCamera();

  queue_draw();
}


void
VtkGtkBox::screenshot(const string& filename) 
{
  // would be more efficient with PNG format
  //
  vtkWindowToImageFilter* w2im = vtkWindowToImageFilter::New();
  vtkJPEGWriter* writer = vtkJPEGWriter::New();
  w2im->SetInput(ren_win);
  writer->SetQuality(88);
#if VTK_MAJOR_VERSION >= 6
  writer->SetInputConnection(w2im->GetOutputPort());
#else
  writer->SetInput(w2im->GetOutput());
#endif
  writer->SetFileName((filename + ".jpg").c_str());
  writer->Write();
  writer->Delete();  
  w2im->Delete();
}
