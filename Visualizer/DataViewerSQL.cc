#include <iostream>
#include <DataViewerSQL.h>

DataViewerSQL::DataViewerSQL(int size, string filename)
  : 
  screendump_ok(false),
  m_Button_Curve("Curve"), 
  m_Button_Contour("Contour"), 
  m_Button_Reread("Reread"), 
  m_Button_Jpeg("Jpeg"), 
  m_Button_Quit("Quit"),
  my_fields(filename),
  my_dens(filename)
{
  void TestCylinder(vtkRenderer *ren);

  my_fields.addMenus(my_fields.get_x_menu(), 
		     my_fields.get_y_menu());

  set_title("Data Visualizer");
  set_border_width(10);
  set_default_size(size, size);


  add(m_VBox);

  //
  // Add the Notebook, with the button underneath:
  //
  m_Notebook.set_border_width(10);
  m_VBox.pack_start(m_Notebook);
  m_VBox.pack_start(m_ButtonBox, Gtk::PACK_SHRINK);

  m_ButtonBox.pack_start(m_Button_Curve, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Contour, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Reread, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Jpeg, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Quit, Gtk::PACK_SHRINK);

  m_Button_Curve.signal_clicked().connect( sigc::mem_fun(*this, &DataViewerSQL::on_button_curve) );
  m_Button_Contour.signal_clicked().connect( sigc::mem_fun(*this, &DataViewerSQL::on_button_contour) );
  m_Button_Reread.signal_clicked().connect( sigc::mem_fun(*this, &DataViewerSQL::on_button_reread) );
  m_Button_Jpeg.signal_clicked().connect( sigc::mem_fun(*this, &DataViewerSQL::on_button_jpeg) );
  m_Button_Quit.signal_clicked().connect( sigc::mem_fun(*this, &DataViewerSQL::on_button_quit) );


  //
  // Add the Notebook pages:
  //
  m_Notebook.append_page(my_vtk_box0,  "X-Y plot");
  m_Notebook.append_page(my_vtk_box1,  "3D object");
  m_Notebook.append_page(my_fields,    "Fields");
  m_Notebook.append_page(my_dens,      "Density controls");
  m_Notebook.append_page(my_render,    "Render controls");

  show_all_children();

  //
  // Plots
  //

  int dataSize = 100;
  vector<double> X(dataSize), Y(dataSize);
  double xmin=1.0e30, xmax=-1.0e30;
  double ymin=1.0e30, ymax=-1.0e30;
  for(int i=0; i<dataSize; i++)
      {
	X[i] = 0.03*(i+1000.0);
	Y[i] = 1.0e-4*i*i;
	xmin = min<double>(X[i], xmin);
	ymin = min<double>(Y[i], ymin);
	xmax = max<double>(X[i], xmax);
	ymax = max<double>(Y[i], ymax);
      }
  
  xyplot = new XYPlot(X, Y);
  string xlab("dog"), ylab("cat");
  xyplot->setXlabel(xlab);
  xyplot->setYlabel(ylab);

  m_Notebook.set_current_page(0);
  xyplot->plotVtk(my_vtk_box0.get_renderer());

  tdplot = new VtkDenest2d(xmin, xmax, ymin, ymax, X, Y);

  m_Notebook.set_current_page(1);
  tdplot->SetLabels(xlab, ylab, string());
  tdplot->SetContours(10);
  tdplot->plotVtk(my_vtk_box1.get_renderer());

  // TestCylinder(my_vtk_box1.get_renderer());
}

DataViewerSQL::~DataViewerSQL()
{
}

void DataViewerSQL::on_button_quit()
{
  hide();
}

void DataViewerSQL::on_button_curve()
{
  vector<double> X, Y;
  string xlabel, ylabel;
  double xmin, xmax, ymin, ymax;
    
  my_fields.getData(X, Y, xlabel, ylabel);
  my_fields.getRanges(xmin, xmax, ymin, ymax);
    
  xyplot->newData(X, Y, ymin, ymax);
  xyplot->setXlabel(xlabel);
  xyplot->setYlabel(ylabel);
  xyplot->plotVtk(my_vtk_box0.get_renderer());

  my_vtk_box0.queue_draw();	// Force redrawing
  
				// Move to X-Y page
  m_Notebook.set_current_page(0);

				// Allow screendump
  screendump_ok = true;

}

void DataViewerSQL::on_button_contour()
{
  vector<double> X, Y;
  string xlabel, ylabel;
  double xmin, xmax, ymin, ymax;
    
  my_fields.getData(X, Y, xlabel, ylabel);
  my_fields.getRanges(xmin, xmax, ymin, ymax);

#ifdef DEBUG    
  cout << "Xmin Xmax, Ymin, Ymax: "
       << xmin << ", " << xmax << ", " << ymin << ", " << ymax
       << endl;
#endif // DEBUG

  tdplot->newData(xmin, xmax, ymin, ymax, X, Y);
  tdplot->set_F(my_dens.GetKernel());
  tdplot->set_pedestal(my_dens.GetPedestal());
  tdplot->set_log(my_render.Log());
  tdplot->set_nflags(my_dens.GetNorm());
  tdplot->loadVtk();
				// Get/Set contour levels
  double vmin, vmax;
  double low, high;

  my_render.GetLevelValues(vmin, vmax);
  tdplot->getMinMax(low, high);

#ifdef DEBUG
  cout << "Got values: "  << vmin << ", " << vmax << endl;
  cout << "Got limits: "  << low  << ", " << high << endl;
#endif

  my_render.SetLevelLimits(low, high);
  tdplot->SetLabels(xlabel, ylabel, string());
  tdplot->SetContours(my_render.Contours());
  tdplot->setMinMax(max<double>(low, vmin), min<double>(high, vmax));
  tdplot->Threshold(my_render.Threshold());
  tdplot->ThresholdC(my_render.ApplyToField());
  tdplot->BlackAndWhite(my_render.BlackAndWhite());
  tdplot->ReverseColors(my_render.ReverseColors());
  tdplot->ReverseColors(my_render.ReverseColors());
  tdplot->CDF(my_render.CDF());
  
				// Make the plot
  tdplot->plotVtk(my_vtk_box1.get_renderer());
  
  my_vtk_box1.queue_draw();	// Force redrawing

				// Move to 3D page
  m_Notebook.set_current_page(1);

				// Allow screendump
  screendump_ok = true;
}


void DataViewerSQL::on_button_reread()
{
  my_fields.Reread();
}

void DataViewerSQL::on_button_jpeg()
{
  // The visable plot will be dumped so it doesn't matter which one
  // we pick (I think . . .)

  if (screendump_ok) my_vtk_box0.screenshot("plot"); 

}
