#include <XYPlot.h>

extern string outfile;

XYPlot::XYPlot(vector<double>& x, vector<double>& y, double ymin, double ymax)
{
  X = x;
  Y = y;
  Ymin = ymin;
  Ymax = ymax;
  xlab = "x-axis";
  ylab = "y-axis";
}

void XYPlot::newData(vector<double>& x, vector<double>& y, double ymin, double ymax)
{
  X = x;
  Y = y;
  Ymin = ymin;
  Ymax = ymax;
  xlab = "x-axis";
  ylab = "y-axis";
}


void XYPlot::plotVtk(vtkRenderer *ren)
{
  // Create the rendering stuff

#if VTK_MAJOR_VERSION>=4
  vtkFloatArray *xCoords = vtkFloatArray::New();
  vtkFloatArray *yCoords = vtkFloatArray::New();
#else
  vtkScalars *xCoords = vtkScalars::New();
  xCoords->SetDataTypeToFloat();
  vtkScalars *yCoords = vtkScalars::New();
  yCoords->SetDataTypeToFloat();
#endif
  
  int dataSize = X.size();
  for(int i=0; i<dataSize; i++)
    {
#if VTK_MAJOR_VERSION>=4
      xCoords -> InsertValue(i, X[i]);
      yCoords -> InsertValue(i, Y[i]);
#else
      xCoords -> InsertScalar(i, X[i]);
      yCoords -> InsertScalar(i, Y[i]);
#endif
    }
  
  vtkXYPlotActor *xyplot = vtkXYPlotActor::New();
  xyplot -> SetXValues(dataSize);
  xyplot -> SetXValuesToValue();
  if (Xmin<Xmax) xyplot -> SetXRange(Xmin, Xmax);
  if (Ymin<Ymax) xyplot -> SetYRange(Ymin, Ymax);
  xyplot -> GetPositionCoordinate()->SetValue(0.06, 0.0, 0);
  xyplot -> GetPosition2Coordinate()->SetValue(0.94, 1.0, 0);
  xyplot -> SetXTitle(xlab.c_str());
  xyplot -> SetYTitle(ylab.c_str());
#if VTK_MAJOR_VERSION > 4 || (VTK_MAJOR_VERSION == 4 && VTK_MINOR_VERSION >= 2)
  xyplot -> GetTitleTextProperty()->BoldOn();
  xyplot -> GetTitleTextProperty()->ShadowOn();
#else
  xyplot -> BoldOn();
  xyplot -> ShadowOn();
#endif
  xyplot -> GetProperty()->SetLineWidth(2.0);

  vtkRectilinearGrid *curve  = vtkRectilinearGrid::New();
  curve -> SetDimensions(dataSize, 1, 1);
  curve -> SetXCoordinates(xCoords);
  curve -> GetPointData()->SetScalars(yCoords);
#if VTK_MAJOR_VERSION >= 6
  xyplot-> AddDataSetInput(curve);
#else
  xyplot-> AddInput(curve);
#endif
  xCoords -> Delete();
  yCoords -> Delete();
  curve -> Delete();

  ren -> AddActor2D(xyplot);
  ren -> SetBackground(0.08, 0.49, 0.68);

  xyplot -> Delete();
}

