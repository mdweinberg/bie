#include <iostream>
#include <algorithm>

using namespace std;

#include <DenseBox.h>
#include <labeledoptionmenu.h>

DenseBox::DenseBox(string fname)
:
  kernel(1.0), pedestal(-8), cumulate(false), 
  peglow(false), peghigh(true), peghalf(false), normVH(0),
  m_VBox_iter    (false, 10),
  m_VBox_text    (false, 10),
  m_HBox_kernel  (false, 10),
  m_HBox_buttons (false, 10),
  m_HBox_pedestal(false, 10),
  m_HBox_text    (false, 10),
  m_HBox_miniter (false, 10),
  m_HBox_maxiter (false, 10),

  m_Frame_smooth("Smoothing control"),
  m_Frame_file  ("Current file"),
  m_Frame_iter  ("Iteration limits"),

  // value, lower, upper, step_increment, page_increment, page_size
  // note that the page_size value only makes a difference for
  // scrollbar widgets, and the highest value you'll get is actually
  // (upper - page_size).
  //
  m_adjustment_kernel(1.0, 0.1, 5.0, 0.1),
  m_adjustment_pedestal(-8, -20, 20, 0.5, 1.0, 0.0),
  m_adjustment_miniter(0.0, 1.0, 100000.0, 0.0),
  m_adjustment_maxiter(100000.0, 1.0, 100000.0, 0.0),

  m_Scale_kernel(m_adjustment_kernel),
  m_Scale_miniter(m_adjustment_miniter),
  m_Scale_maxiter(m_adjustment_maxiter),
  
  // a checkbutton to control whether the value is displayed or not
  //
  m_RowButton("Normalize on rows", 0),
  m_ColumnButton("Normalize on columns", 0),
  m_CumulativeButton("Cumulative marginal", 0),
  m_PegLowButton("Set to minimum on reread", 0),
  m_PegHighButton("Set to maximum on reread", 0),
  m_PegHalfButton("Use upper 50% on reread", 0),

  m_Label_pedestal("Pedestal: "),
  m_Label_miniter("Min iteration: "),
  m_Label_maxiter("Max iteration: "),

  m_SpinButton_pedestal(m_adjustment_pedestal, 1.0, 2)
{
  add(m_VBox_Top);

  m_VBox_Top.pack_start(m_Frame_smooth);
  m_VBox_Top.pack_start(m_Frame_iter);
  m_VBox_Top.pack_start(m_Frame_file);

  m_Frame_smooth.add(m_VBox_smooth);
  m_Frame_smooth.set_border_width(10);

  m_VBox_smooth.pack_start(m_HBox_kernel);
  m_VBox_smooth.pack_start(m_HBox_buttons);
  m_VBox_smooth.pack_start(m_HBox_pedestal);

  // Smoothing kernel
  //
  m_HBox_kernel.pack_start(*Gtk::manage(new Gtk::Label("Kernel scale:", 0)),  Gtk::PACK_SHRINK);
  m_Scale_kernel.set_digits(1);
  m_adjustment_kernel.signal_value_changed().connect( sigc::mem_fun(*this, &DenseBox::on_adjustment_value_changed) );
  m_HBox_kernel.pack_start(m_Scale_kernel);

  // CheckButtons
  //
  m_RowButton.signal_toggled().connect( sigc::mem_fun(*this, &DenseBox::on_rowbutton_toggled) );
  m_HBox_buttons.pack_start(m_RowButton);

  m_ColumnButton.signal_toggled().connect( sigc::mem_fun(*this, &DenseBox::on_columnbutton_toggled) );
  m_HBox_buttons.pack_start(m_ColumnButton);
  
  m_CumulativeButton.signal_toggled().connect( sigc::mem_fun(*this, &DenseBox::on_cumulativebutton_toggled) );

  // Spinner
  //
  m_HBox_pedestal.pack_start(m_CumulativeButton);
  m_Label_pedestal.set_alignment(Gtk::ALIGN_LEFT);
  m_HBox_pedestal.pack_start(m_Label_pedestal, Gtk::PACK_SHRINK, 5);
  m_HBox_pedestal.pack_start(m_SpinButton_pedestal, Gtk::PACK_SHRINK, 5);
  m_SpinButton_pedestal.set_size_request(80, -1);
  m_adjustment_pedestal.signal_value_changed().connect( sigc::mem_fun(*this, &DenseBox::on_spinbutton_value_changed) );


  // File window
  //
  m_FileLabel.set_label(fname.c_str());
  m_HBox_text.pack_start(m_FileLabel);
  m_VBox_text.pack_start(m_HBox_text);
  m_Frame_file.add(m_VBox_text);
  m_Frame_file.set_border_width(10);

  // Iteration sliders
  //
  m_Frame_iter.add(m_VBox_iter);
  m_Frame_iter.set_border_width(10);

  m_HBox_miniter.set_border_width(5);
  // m_HBox_miniter.set_homogeneous(true);
  m_VBox_iter.add(m_HBox_miniter);

  m_HBox_miniter.pack_start(*Gtk::manage(new Gtk::Label("Min iteration:", 0)),  Gtk::PACK_SHRINK);
  m_Scale_miniter.set_digits(1);
  m_adjustment_miniter.signal_value_changed().connect( sigc::mem_fun(*this, &DenseBox::on_miniter_value_changed) );
  m_HBox_miniter.pack_start(m_Scale_miniter);

  m_HBox_maxiter.set_border_width(5);
  // m_HBox_maxiter.set_homogeneous(true);
  m_VBox_iter.add(m_HBox_maxiter);

  m_HBox_maxiter.pack_start(*Gtk::manage(new Gtk::Label("Max iteration:", 0)),  Gtk::PACK_SHRINK);
  m_Scale_maxiter.set_digits(1);
  m_adjustment_maxiter.signal_value_changed().connect( sigc::mem_fun(*this, &DenseBox::on_maxiter_value_changed) );

  m_HBox_maxiter.pack_start(m_Scale_maxiter);

  m_PegLowButton.signal_toggled().connect( sigc::mem_fun(*this, &DenseBox::on_peglowbutton_toggled) );
  m_PegHighButton.signal_toggled().connect( sigc::mem_fun(*this, &DenseBox::on_peghighbutton_toggled) );
  m_PegHalfButton.signal_toggled().connect( sigc::mem_fun(*this, &DenseBox::on_peghalfbutton_toggled) );

				// Let min iter remember its setting
  m_PegLowButton.set_active(false);
				// Peg max iter to maximum by default
  m_PegHighButton.set_active(true);
				// Peg to upper 50% of the chain;
				// this will desensitize the two above
  m_PegHalfButton.set_active(false); 

  m_VBox_iter.add(m_PegLowButton);
  m_VBox_iter.add(m_PegHighButton);
  m_VBox_iter.add(m_PegHalfButton);

  //

  show_all_children();
}

DenseBox::~DenseBox()
{
}

void DenseBox::SetMinIterRange(int imin)
{
  m_adjustment_miniter.set_lower(static_cast<double>(imin));
  m_adjustment_maxiter.set_lower(static_cast<double>(imin));

  if (peghalf) {
    m_adjustment_miniter.set_value(m_adjustment_maxiter.get_value()*0.5);
  } else if (peglow) {
    m_adjustment_miniter.set_value(static_cast<double>(imin));
  } else {
    m_adjustment_miniter.set_value
      (max<double>(m_adjustment_miniter.get_value(), static_cast<double>(imin)));
    m_adjustment_maxiter.set_value
      (max<double>(m_adjustment_maxiter.get_value(), static_cast<double>(imin)));
  }
}


void DenseBox::SetMaxIterRange(int imax)
{
  m_adjustment_miniter.set_upper(static_cast<double>(imax));
  m_adjustment_maxiter.set_upper(static_cast<double>(imax));

  if (peghalf) {
    m_adjustment_miniter.set_value(static_cast<double>(imax/2));
    m_adjustment_maxiter.set_value(static_cast<double>(imax));
  } else if (peghigh) {
    m_adjustment_maxiter.set_value(static_cast<double>(imax));
  } else {
    m_adjustment_miniter.set_value
      (min<double>(m_adjustment_miniter.get_value(), static_cast<double>(imax)));
    m_adjustment_maxiter.set_value
      (min<double>(m_adjustment_maxiter.get_value(), static_cast<double>(imax)));
  }
}

void DenseBox::on_rowbutton_toggled()
{
  if (m_RowButton.get_active()) {
    if (m_ColumnButton.get_active()) m_ColumnButton.set_active(false);
    normVH = 1;
  } else
    if (!m_ColumnButton.get_active()) normVH = 0;
}

void DenseBox::on_columnbutton_toggled()
{
  if (m_ColumnButton.get_active()) {
    if (m_RowButton.get_active()) m_RowButton.set_active(false);
    normVH = 2;
  } else
    if (!m_RowButton.get_active()) normVH = 0;
}

void DenseBox::on_cumulativebutton_toggled()
{
  if (m_CumulativeButton.get_active()) 
    cumulate = true;
  else
    cumulate = false;
}

void DenseBox::on_peglowbutton_toggled()
{
  if (m_PegLowButton.get_active()) 
    peglow = true;
  else
    peglow = false;
}

void DenseBox::on_peghighbutton_toggled()
{
  if (m_PegHighButton.get_active()) 
    peghigh = true;
  else
    peghigh = false;
}

void DenseBox::on_peghalfbutton_toggled()
{
  if (m_PegHalfButton.get_active()) {
    peghalf = true;
    m_PegLowButton. set_sensitive(false);
    m_PegHighButton.set_sensitive(false);
  } else {
    peghalf = false;
    m_PegLowButton. set_sensitive(true);
    m_PegHighButton.set_sensitive(true);
  }
}

void DenseBox::on_adjustment_value_changed()
{
  kernel   = m_adjustment_kernel.get_value();
}

void DenseBox::on_spinbutton_value_changed()
{
  pedestal = m_adjustment_pedestal.get_value();
}

void DenseBox::on_miniter_value_changed()
{
  miniter  = m_adjustment_miniter.get_value();
}

void DenseBox::on_maxiter_value_changed()
{
  maxiter  = m_adjustment_maxiter.get_value();
}

