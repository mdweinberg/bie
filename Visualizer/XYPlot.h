#ifndef _XYPlot_h
#define _XYPlot_h

#include <string>
#include <vector>

// For details, see:
// http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Factories_now_require_defines
// 
#include <vtkVersion.h>
#if VTK_MAJOR_VERSION==6 && VTK_MINOR_VERSION<2
#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)#include <vtkAutoInit.h>
#endif

#include <vtkRenderer.h>
#include <vtkXYPlotActor.h>
#include <vtkRectilinearGrid.h>

#if VTK_MAJOR_VERSION>=4
#include <vtkProperty2D.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkWindowToImageFilter.h>
#if VTK_MAJOR_VERSION<=4 && VTK_MINOR_VERSION<=2
#include <vtkScalars.h>
#else
#include <vtkTextProperty.h>
#endif
#endif

using namespace std;

class XYPlot {

protected:

  vector<double> X, Y;
  float Xmin, Xmax;
  float Ymin, Ymax;
  string xlab, ylab;

public:

  XYPlot(vector<double>& x, vector<double>& y, 
	 double ymin=1.0, double ymax=1.0);

  void newData(vector<double>& x, vector<double>& y, 
	       double ymin=1.0, double ymax=1.0);

  void plotVtk(vtkRenderer *ren);
  void setXlabel(string &s) { xlab = s; }
  void setYlabel(string &s) { ylab = s; }
  void setXrange(double xmin, double xmax) { Xmin=xmin; Xmax=xmax; }
  void setYrange(double ymin, double ymax) { Ymin=ymin; Ymax=ymax; }

};

#endif
