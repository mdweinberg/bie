#include <unistd.h>
#include <time.h>
#include <gtk/gtk.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>

#include <XYPlot.h>
#include <VtkDenest2d.h>

GtkWidget *plot_type(void);

// Classes and structures for saving simulation output stream

struct DataRecord 
{
  vector<double> data;
} datarecord;

class DataSet
{
private:

  unsigned ndim;

public:

  vector<double> datamin;
  vector<double> datamax;

  vector<DataRecord> data;

  DataSet() {
    ndim = 0;
  }

  DataSet(int Ndim) {
    ndim = Ndim;
    datamin = vector<double>(ndim,  1.0e30);
    datamax = vector<double>(ndim, -1.0e30);
  }
    
  DataSet(const DataSet &v)
  {
    ndim = v.ndim;
    datamin = v.datamin;
    datamax = v.datamax;
    data = v.data;
  }

  void operator+=(DataRecord& r) {
    for (unsigned i=0; i<ndim; i++) {
      datamin[i] = min<double>(r.data[i], datamin[i]);
      datamax[i] = max<double>(r.data[i], datamax[i]);
    }

    data.push_back(r);
  }

};

DataSet Data;

// Widgets ===================================================================

static gint button_press (GtkWidget *, GdkEvent *);
void x_menuitem_response (unsigned *);
void y_menuitem_response (unsigned *);
GtkWidget *xlabel, *ylabel;
vector<string> fieldNames;
GtkAdjustment *xadjmin, *xadjmax, *yadjmin, *yadjmax;
GtkAdjustment *itadjmin, *itadjmax, *imadjmin, *imadjmax;
unsigned xcolumn, ycolumn;

void cb_lower_limit( GtkAdjustment *get,
		     GtkAdjustment *set )
{
  set->lower = get->value;
  set->value = max<gfloat>(set->lower, set->value);
  gtk_signal_emit_by_name (GTK_OBJECT (set), "changed");
}

void cb_upper_limit( GtkAdjustment *get,
		     GtkAdjustment *set )
{
  set->upper = get->value;
  set->value = min<gfloat>(set->upper, set->value);
  gtk_signal_emit_by_name (GTK_OBJECT (set), "changed");
}

void scale_set_default_values( GtkScale *scale )
{
    gtk_range_set_update_policy (GTK_RANGE (scale),
                                 GTK_UPDATE_CONTINUOUS);
    gtk_scale_set_digits (scale, 1);
    gtk_scale_set_value_pos (scale, GTK_POS_TOP);
    gtk_scale_set_draw_value (scale, TRUE);
}


GtkWidget *get_list(const char *header, GtkWidget **label, 
		    void func(unsigned *))
{
  GtkWidget *vbox, *menu, *menu_items, *frame, *button;

  // A vbox to put a menu and a button in
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox);

  // Init the menu-widget, and remember -- never
  // gtk_show_widget() the menu widget!!
  menu = gtk_menu_new ();

  for (unsigned i = 0; i < fieldNames.size(); i++)
    {
      // Create a new menu-item with a name...
      menu_items = gtk_menu_item_new_with_label (fieldNames[i].c_str());

      // ...and add it to the menu.
      gtk_menu_append (GTK_MENU (menu), menu_items);
      
      gtk_signal_connect_object (GTK_OBJECT (menu_items), "activate",
				 GTK_SIGNAL_FUNC (func), 
				 (GtkObject *)(g_memdup(&i, sizeof(unsigned))));
      // Show the widget
      gtk_widget_show (menu_items);
    }


  *label = gtk_label_new (" ");
  gtk_widget_show(*label);

  frame = gtk_frame_new(header);
  // gtk_widget_set_usize(frame, 40, 50);
  gtk_container_border_width(GTK_CONTAINER(frame), 5);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (frame), *label);
  gtk_widget_show(frame);

  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show(vbox);

  // Create a button to which to attach menu as a popup
  button = gtk_button_new_with_label ("Choose field");
  gtk_signal_connect_object (GTK_OBJECT (button), "event",
			     GTK_SIGNAL_FUNC (button_press), 
			     GTK_OBJECT (menu));
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);
  
  return vbox;
}

GtkWidget *get_list2(const char *header, GtkWidget **label, 
		     void func(unsigned *))
{
  const int mmsiz = 18;

  GtkWidget *vbox, *menu, *submenu, *menu_items, *frame, *button;
  GtkWidget *menuitem;

  // A vbox to put a menu and a button in
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox);

  // Init the menu-widget, and remember -- never
  // gtk_show_widget() the menu widget!!
  menu = gtk_menu_new ();

  if (fieldNames.size() < (unsigned)mmsiz) {

    for (unsigned i = 0; i < fieldNames.size(); i++)
      {
	// Create a new menu-item with a name...
	menu_items = gtk_menu_item_new_with_label (fieldNames[i].c_str());
	
	// ...and add it to the menu.
	gtk_menu_append (GTK_MENU (menu), menu_items);
	
	gtk_signal_connect_object (GTK_OBJECT (menu_items), "activate",
				   GTK_SIGNAL_FUNC (func), 
				   (GtkObject *)(g_memdup(&i, sizeof(unsigned))));
	// Show the widget
	gtk_widget_show (menu_items);
      }

  } else {
    
    int nsub = fieldNames.size()/mmsiz;

    // Title
    menu_items = gtk_menu_item_new_with_label ("Index");
    gtk_menu_append (GTK_MENU (menu), menu_items);
    gtk_widget_show (menu_items);

    for (int j=0; j<=nsub; j++) {

      // Create a new menu-item with a name...
      ostrstream lab;
      lab << j*mmsiz+1 << "-->" 
	  << min<unsigned>(mmsiz*(j+1), fieldNames.size()) 
	  << '\0';

      menuitem = gtk_menu_item_new_with_label (lab.str());
	
      // Add it to the menu.
      gtk_menu_append (GTK_MENU (menu), menuitem);

      // A new submenu
      submenu = gtk_menu_new ();

      // Associate it with a new menu
      gtk_menu_item_set_submenu(GTK_MENU_ITEM (menuitem), submenu);
      
      // Show the widget
      gtk_widget_show (menuitem);

      for (unsigned i = mmsiz*j; i < min<unsigned>(mmsiz*(j+1), fieldNames.size()); 
	   i++)
	{
	  // Create a new menu-item with a name...
	  menu_items = gtk_menu_item_new_with_label (fieldNames[i].c_str());
	  
	  // ...and add it to the menu.
	  gtk_menu_append (GTK_MENU (submenu), menu_items);
	  
	  gtk_signal_connect_object (GTK_OBJECT (menu_items), "activate",
				     GTK_SIGNAL_FUNC (func), 
				     (GtkObject *)(g_memdup(&i, sizeof(unsigned))));
	  // Show the widget
	  gtk_widget_show (menu_items);
	}
    }
  }


  *label = gtk_label_new (" ");
  gtk_widget_show(*label);

  frame = gtk_frame_new(header);
  // gtk_widget_set_usize(frame, 40, 50);
  gtk_container_border_width(GTK_CONTAINER(frame), 5);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (frame), *label);
  gtk_widget_show(frame);

  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show(vbox);

  // Create a button to which to attach menu as a popup
  button = gtk_button_new_with_label ("Choose field");
  gtk_signal_connect_object (GTK_OBJECT (button), "event",
			     GTK_SIGNAL_FUNC (button_press), 
			     GTK_OBJECT (menu));
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);
  
  return vbox;
}

/* Respond to a button-press by posting a menu passed in as widget.
 *
 * Note that the "widget" argument is the menu being posted, NOT
 * the button that was pressed.
 */

static gint button_press( GtkWidget *widget,
                          GdkEvent *event )
{

    if (event->type == GDK_BUTTON_PRESS) {
        GdkEventButton *bevent = (GdkEventButton *) event; 
        gtk_menu_popup (GTK_MENU (widget), NULL, NULL, NULL, NULL,
                        bevent->button, bevent->time);

        // Tell calling code that we have handled this event; the buck
	// stops here.
        return TRUE;
    }

    // Tell calling code that we have not handled this event; pass it on.
    return FALSE;
}

bool xsnap = false, ysnap = false;
bool xlog = false, ylog = false;
bool xtickbig = false, ytickbig = false;
double xtickscl = 0.1, ytickscl = 0.1;
void update_xlimits(void);
void update_ylimits(void);

void set_xsnap( GtkToggleButton *button )
{
  if (button->active) {
    xsnap = true;
  }
  else {
    xsnap = false;
  }

  update_xlimits();

}

void set_xlog( GtkToggleButton *button )
{
  if (button->active) {
    xlog = true;
  }
  else {
    xlog = false;
  }

  update_xlimits();

}

void set_xtick( GtkToggleButton *button )
{
  if (button->active) {
    xtickbig = true;
    xtickscl = 1.0;
  }
  else {
    xtickbig = false;
    xtickscl = 0.1;
  }

  update_xlimits();

}

void set_ysnap( GtkToggleButton *button )
{
  if (button->active) {
    ysnap = true;
  }
  else {
    ysnap = false;
  }

  update_ylimits();

}

void set_ylog( GtkToggleButton *button )
{
  if (button->active) {
    ylog = true;
  }
  else {
    ylog = false;
  }

  update_ylimits();

}

void set_ytick( GtkToggleButton *button )
{
  if (button->active) {
    ytickbig = true;
    ytickscl = 1.0;
  }
  else {
    ytickbig = false;
    ytickscl = 0.1;
  }

  update_ylimits();

}

// Update spinner precision

void change_spin_digits( GtkAdjustment *adj,
			 GtkSpinButton *spin )
{
  if (fabs(adj->value) > 0) {
    int digits = min<int>(5,
			  max<int> ((int)(3.0 - log10(fabs(adj->value)) ), 0)
			  );
    gtk_spin_button_set_digits (spin, digits);
  }
}

// Update scale precision

void change_scale_digits( GtkAdjustment *adj,
			  GtkScale *scale )
{
  if (adj->upper > adj->lower) {
    int digits = 
      max<int> ((int)(3.0 - log10(adj->upper - adj->lower) ), 0);
    gtk_scale_set_digits (scale, digits);

  }
}

// Select plot type

bool lineplot = false;

void cb_line_plot( GtkToggleButton *button )
{
  if (button->active) {
    lineplot = true;
  }
  else {
    lineplot = false;
  }
}


GtkWidget *plot_type(void)
{
  GtkWidget *vbox, *hbox, *button;
  GSList *group = NULL;

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(vbox);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 10);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);

  button = gtk_radio_button_new_with_label(group, "line plot");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC(cb_line_plot), NULL);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  group = gtk_radio_button_group (GTK_RADIO_BUTTON (button));

  button = gtk_radio_button_new_with_label(group, "contour plot");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  group = gtk_radio_button_group (GTK_RADIO_BUTTON (button));

  return vbox;
}


// Print a string when a menu item is selected

void update_xlimits(void)
{
  if (xlog) {
    xadjmin->value = log10(fabs(Data.datamin[xcolumn]) + 1.0e-8);
    xadjmax->value = log10(fabs(Data.datamax[xcolumn]) + 1.0e-8);
  }
  else {
    xadjmin->value = Data.datamin[xcolumn];
    xadjmax->value = Data.datamax[xcolumn];
  }

  if (xsnap && xadjmax->value > xadjmin->value) {
    double big = max<double>(fabs(xadjmax->value), fabs(xadjmin->value));
    double factor = pow(10.0, floor(log10(big*xtickscl)));

    if (xadjmin->value > 0)
      xadjmin->value = floor(xadjmin->value/factor)*factor;
    else
      xadjmin->value = -floor(fabs(xadjmin->value)/factor+1)*factor;

    if (xadjmax->value > 0)
      xadjmax->value = floor(xadjmax->value/factor+1)*factor;
    else
      xadjmax->value = -floor(fabs(xadjmax->value)/factor)*factor;
  }

  gtk_signal_emit_by_name (GTK_OBJECT (xadjmin), "value_changed");
  gtk_signal_emit_by_name (GTK_OBJECT (xadjmax), "value_changed");
}

void update_ylimits(void)
{
  if (ylog) {
    yadjmin->value = log10(fabs(Data.datamin[ycolumn]) + 1.0e-8);
    yadjmax->value = log10(fabs(Data.datamax[ycolumn]) + 1.0e-8);
  }
  else {
    yadjmin->value = Data.datamin[ycolumn];
    yadjmax->value = Data.datamax[ycolumn];
  }

  if (ysnap && yadjmax->value > yadjmin->value) {
    double big = max<double>(fabs(yadjmax->value), fabs(yadjmin->value));
    double factor = pow(10.0, floor(log10(big*ytickscl)));

    if (yadjmin->value > 0)
      yadjmin->value = floor(yadjmin->value/factor)*factor;
    else
      yadjmin->value = -floor(fabs(yadjmin->value)/factor+1)*factor;

    if (yadjmax->value > 0)
      yadjmax->value = floor(yadjmax->value/factor+1)*factor;
    else
      yadjmax->value = -floor(fabs(yadjmax->value)/factor)*factor;
  }

  gtk_signal_emit_by_name (GTK_OBJECT (yadjmin), "value_changed");
  gtk_signal_emit_by_name (GTK_OBJECT (yadjmax), "value_changed");
}


void x_menuitem_response( unsigned *i )
{
  xcolumn = *i;
  gtk_label_set_text (GTK_LABEL(xlabel), fieldNames[xcolumn].c_str());
  update_xlimits();
}

void y_menuitem_response( unsigned *i )
{
  ycolumn = *i;
  gtk_label_set_text (GTK_LABEL(ylabel), fieldNames[ycolumn].c_str());
  update_ylimits();
}

VtkDenest2d *plotter = NULL;
XYPlot *xyplotter = NULL;
int contours = 10;		// Number of contours (changed by scale)
int pixels = 400;		// Number of pixels (changed by scale)
double scaleF = 0.5;
int normVH = 0;

bool colnorm = false;
bool rownorm = false;
bool logscale = false;
bool inversecolor = false;
bool blackandwhite = false;
bool threshold = false;
bool thresholdC = false;

GtkAdjustment *adjthresh;

void set_xyplot_state(void)
{
  if (xyplotter) {
    xyplotter->setXlabel(fieldNames[xcolumn]);
    xyplotter->setYlabel(fieldNames[ycolumn]);
    xyplotter->setPixels(pixels);
  }
}

void set_vtk_state(void)
{
  if (plotter) {
    plotter->set_log(logscale);
    plotter->set_nflags(normVH);
    plotter->set_pedestal(adjthresh->value);
    plotter->ReverseColors(inversecolor);
    plotter->BlackAndWhite(blackandwhite);
    plotter->Threshold(threshold);
    plotter->ThresholdC(thresholdC);

    string title;
    if (logscale) title = "Log Density";
    else title = "Density";
      
    plotter->SetLabels(fieldNames[xcolumn], 
		       fieldNames[ycolumn], 
		       title);
    plotter->SetPixels(pixels);
    plotter->SetContours(contours);
    double vmin = imadjmin->value;
    double vmax = imadjmax->value;
    plotter->setMinMax(vmin, vmax);
  }
}

void compute_data( GtkWidget *widget,
		   gpointer   data )
{
  vector<double> x, y;

				// Load the data

  for (unsigned i=0; i<Data.data.size(); i++) {

    // For line plots, only load x data within spinner range
    if (lineplot && 
	(Data.data[i].data[xcolumn] < xadjmin->value ||
	 Data.data[i].data[xcolumn] > xadjmax->value )
	) continue;
	 
    if (xlog)
      x.push_back(log10(fabs(Data.data[i].data[xcolumn])+1.0e-8));
    else
      x.push_back(Data.data[i].data[xcolumn]);

    if (ylog)
      y.push_back(log10(fabs(Data.data[i].data[ycolumn])+1.0e-8));
    else
      y.push_back(Data.data[i].data[ycolumn]);
  }
				// Compute the image

  delete plotter;
  plotter = NULL;
  
  delete xyplotter;
  xyplotter = NULL;
  
  if (lineplot) {

    xyplotter = new XYPlot(x, y, yadjmin->value, yadjmax->value);

    set_xyplot_state();

    xyplotter->plotVtk();

  } else {

    plotter = new VtkDenest2d(xadjmin->value, xadjmax->value,
			      yadjmin->value, yadjmax->value,
			      x, y);
  
    set_vtk_state();
    
    plotter->compute();

    plotter->plotVtk();

    double vmin, vmax;
    plotter->getMinMax(&vmin, &vmax);

    gfloat step_increment = 
      pow(10.0, (int)(log10(max<double>(fabs(vmin), fabs(vmax))) - 3.0));
    gfloat page_increment = step_increment*10.0;


    imadjmin->value = vmin;
    imadjmin->lower = vmin;
    imadjmin->upper = vmax;
    imadjmin->step_increment = step_increment;
    imadjmin->page_increment = page_increment;

    imadjmax->value = vmax;
    imadjmax->lower = vmin;
    imadjmax->upper = vmax;
    imadjmax->step_increment = step_increment;
    imadjmax->page_increment = page_increment;

    gtk_signal_emit_by_name (GTK_OBJECT (imadjmin), "value_changed");
    gtk_signal_emit_by_name (GTK_OBJECT (imadjmax), "value_changed");
    
  }

}

void make_plot( GtkWidget *widget,
		gpointer   data )
{

  if (lineplot) {

    if (!xyplotter) {
      compute_data(widget, data);
      return;
    }

    set_xyplot_state();

    xyplotter->plotVtk();


  } else {

    if (!plotter) {
      compute_data(widget, data);
      return;
    }

    set_vtk_state();

    plotter->plotVtk();
  }

}

void render(void)
{

  if (lineplot) {

    if (xyplotter) {
      xyplotter->Render();
    }

  } else {

    if (plotter) {
      plotter->Render();
    }

  }
}



struct timespec req, rem;

gint refresh( gpointer data )
{
  render();

  req.tv_sec = 0;
  req.tv_nsec = 50000000;
  nanosleep(&req, &rem);

  return 1;
}

GtkWidget *lvscale, *hvscale;

void cb_pixel_scale( GtkAdjustment *adj )
{
  pixels = (int)adj->value;
}

GtkWidget *cntrscale;

void cb_kernel_width( GtkAdjustment *get )
{
  scaleF = get->value;
  gtk_signal_emit_by_name (GTK_OBJECT (get), "changed");
}

void cb_contour_number( GtkAdjustment *get )
{
  contours = (int)get->value;
  gtk_signal_emit_by_name (GTK_OBJECT (get), "changed");
}

void cb_use_contours( GtkToggleButton *button )
{
  if (button->active) {
    gtk_widget_map(cntrscale);
  }
  else {
    gtk_widget_unmap(cntrscale);
  }
}

void cb_use_log( GtkToggleButton *button )
{
  if (button->active) {
    logscale = true;
  }
  else {
    logscale = false;
  }
}

void cb_row_norm( GtkToggleButton *button )
{
  if (button->active) {
    normVH = normVH | 0x1;
  }
  else {
    normVH = normVH & 0xE;
  }
}

void cb_col_norm( GtkToggleButton *button )
{
  if (button->active) {
    normVH = normVH | 0x2;
  }
  else {
    normVH = normVH & 0xD;
  }
}

void cb_reverse_colors( GtkToggleButton *button )
{
  if (button->active) {
    inversecolor = true;
  }
  else {
    inversecolor = false;
  }
}

void cb_blackandwhite( GtkToggleButton *button )
{
  if (button->active) {
    blackandwhite = true;
  }
  else {
    blackandwhite = false;
  }
}

void cb_threshold( GtkToggleButton *button )
{
  if (button->active) {
    threshold = true;
  }
  else {
    if (thresholdC) 
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), true);
    else
      threshold = false;
  }
}

void cb_threshold_contours( GtkToggleButton *button )
{
  if (button->active) {
    if (threshold)
      thresholdC = true;
    else
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), false);
  }
  else {
    thresholdC = false;
  }
}

// Convenience functions

GtkWidget *make_menu_item( gchar         *name,
                           GtkSignalFunc  callback,
                           gpointer       data )
{
    GtkWidget *item;
  
    item = gtk_menu_item_new_with_label (name);
    gtk_signal_connect (GTK_OBJECT (item), "activate",
                        callback, data);
    gtk_widget_show (item);

    return(item);
}

GtkWidget *axis_ranges(void)
{
  GtkWidget *vbox, *hbox, *hbox2, *vbox2, *hbox3, *label, *frame;
  GtkWidget *spinner1, *button;

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show(vbox);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER(hbox), 10);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

				// X axis

  hbox2 = gtk_hbox_new (FALSE, 0);

				// Xmin
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Min:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  xadjmin = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (xadjmin, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(xadjmin), "value_changed",
		     GTK_SIGNAL_FUNC(change_spin_digits),
		     (gpointer) spinner1);
  
				// Xmax
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Max:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  xadjmax = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (xadjmax, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(xadjmax), "value_changed",
		     GTK_SIGNAL_FUNC(change_spin_digits),
		     (gpointer) spinner1);

				// Snap
  vbox2 = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);
  gtk_widget_show(vbox2);

  hbox3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox3, TRUE, TRUE, 0);
  gtk_widget_show(hbox3);

  button = gtk_check_button_new_with_label ("Snap");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (set_xsnap), NULL);
  gtk_box_pack_start (GTK_BOX (hbox3), button, TRUE, TRUE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), xsnap);
  gtk_widget_show(button);

  button = gtk_check_button_new_with_label ("Big tick");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (set_xtick), NULL);
  gtk_box_pack_start (GTK_BOX (hbox3), button, TRUE, TRUE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), xtickbig);
  gtk_widget_show(button);

				// Put in frame
  frame = gtk_frame_new("X-axis");
  gtk_widget_set_usize(frame, 150, 75);
  gtk_container_border_width(GTK_CONTAINER(frame), 5);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (frame), vbox2);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);
  gtk_widget_show(hbox2);
  gtk_widget_show(frame);

				// Y axis

  hbox2 = gtk_hbox_new (FALSE, 0);

				// Ymin
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Min:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  yadjmin = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (yadjmin, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(yadjmin), "value_changed",
		     GTK_SIGNAL_FUNC(change_spin_digits),
		     (gpointer) spinner1);

  
				// Ymax
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, TRUE, TRUE, 5);
  
  label = gtk_label_new ("Max:");
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);
  
  yadjmax = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
  spinner1 = gtk_spin_button_new (yadjmax, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner1), TRUE);
  gtk_widget_set_usize (spinner1, 60, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), spinner1, FALSE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(yadjmax), "value_changed",
		     GTK_SIGNAL_FUNC(change_spin_digits),
		     (gpointer) spinner1);


				// Snap
  vbox2 = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);
  gtk_widget_show(vbox2);

  hbox3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox3, TRUE, TRUE, 0);
  gtk_widget_show(hbox3);

  button = gtk_check_button_new_with_label ("Snap");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (set_ysnap), NULL);
  gtk_box_pack_start (GTK_BOX (hbox3), button, TRUE, TRUE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), ysnap);
  gtk_widget_show(button);

  button = gtk_check_button_new_with_label ("Big tick");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (set_ytick), NULL);
  gtk_box_pack_start (GTK_BOX (hbox3), button, TRUE, TRUE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), ytickbig);
  gtk_widget_show(button);

				// Put in frame
  frame = gtk_frame_new("Y-axis");
  gtk_widget_set_usize(frame, 150, 75);
  gtk_container_border_width(GTK_CONTAINER(frame), 5);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (frame), vbox2);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);
  gtk_widget_show(hbox2);
  gtk_widget_show(frame);


  gtk_widget_show(hbox);

  return vbox;
}

GtkWidget *density_controls(void)
{
  GtkWidget *vbox, *box2, *button;
  GtkWidget *label, *scale, *spinner;
  GtkAdjustment *adj;

  vbox = gtk_vbox_new (FALSE, 10);
  gtk_widget_show (vbox);

  // Scrollbar
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

  label = gtk_label_new ("Kernel scale: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  adj = (GtkAdjustment *) gtk_adjustment_new (scaleF, 
					      0.1, 5.0, 0.1, 1.0, 0.0);
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      GTK_SIGNAL_FUNC (cb_kernel_width), NULL);
  scale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_scale_set_digits (GTK_SCALE (scale), 1);
  gtk_box_pack_start (GTK_BOX (box2), scale, TRUE, TRUE, 0);
  gtk_widget_show (scale);

  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);


  // Checkbuttons for boolean options
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

				// Log

  button = gtk_check_button_new_with_label("Normalize on rows");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), rownorm);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_row_norm), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  button = gtk_check_button_new_with_label("Normalize on columns");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), colnorm);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_col_norm), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

				// Pack buttons

  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);

  // Log scale controls
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
  
  label = gtk_label_new("Pedestal value: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (button);

  adjthresh = (GtkAdjustment *) 
    gtk_adjustment_new (-8.0, -16.0, -1.0, 0.5, 100.0, 0.0);
  spinner = gtk_spin_button_new (adjthresh, 1.0, 2);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner), TRUE);
  gtk_widget_set_usize (spinner, 60, 0);
  gtk_box_pack_start (GTK_BOX (box2), spinner, FALSE, TRUE, 0);
  
				// Pack it up
  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);


  return vbox;
}

GtkWidget *plot_controls(void)
{
  GtkWidget *vbox, *box2, *box3, *button;
  GtkWidget *label, *scale, *frame;
  GtkAdjustment *adj;
  GtkObject *adj2;

  vbox = gtk_vbox_new (FALSE, 10);
  gtk_widget_show (vbox);

  // Scrollbar
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

  label = gtk_label_new ("Contour levels: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  adj = (GtkAdjustment *) gtk_adjustment_new (contours, 
					      0.0, 100.0, 1.0, 1.0, 0.0);
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      GTK_SIGNAL_FUNC (cb_contour_number), NULL);
  cntrscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_scale_set_digits (GTK_SCALE (cntrscale), 0);
  gtk_box_pack_start (GTK_BOX (box2), cntrscale, TRUE, TRUE, 0);
  gtk_widget_show (cntrscale);

  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);


  // Checkbuttons for boolean options
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

				// Log

  button = gtk_check_button_new_with_label("Log scale");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), logscale);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_use_log), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  button = gtk_check_button_new_with_label("Reverse colors");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), inversecolor);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_reverse_colors), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  button = gtk_check_button_new_with_label("Black & white");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), blackandwhite);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_blackandwhite), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

				// Pack buttons

  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);

  // Checkbuttons for boolean options (2nd row)
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

				// Log

  button = gtk_check_button_new_with_label("Threshold contours");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), threshold);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_threshold), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  button = gtk_check_button_new_with_label("Apply to field");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), thresholdC);
  gtk_signal_connect (GTK_OBJECT (button), "toggled",
		      GTK_SIGNAL_FUNC(cb_threshold_contours), NULL);
  gtk_box_pack_start (GTK_BOX (box2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

				// Pack buttons

  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);

  // An option menu to change the position of the value
  box2 = gtk_hbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);

  label = gtk_label_new ("Scale Value Position:");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  
  // Set pixels
  
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
  
  label = gtk_label_new ("Pixels: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  
  adj2 = gtk_adjustment_new (pixels, 0.0, 1000.0, 1.0, 1.0, 0.0);
  gtk_signal_connect (GTK_OBJECT (adj2), "value_changed",
		      GTK_SIGNAL_FUNC (cb_pixel_scale), NULL);
  scale = gtk_hscale_new (GTK_ADJUSTMENT (adj2));
  gtk_scale_set_digits (GTK_SCALE (scale), 0);
  gtk_box_pack_start (GTK_BOX (box2), scale, TRUE, TRUE, 0);
  gtk_widget_show (scale);
  
  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);
  gtk_widget_show (box2);
  
				// Contour level adjustments

  box2 = gtk_vbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
  gtk_box_pack_start (GTK_BOX (vbox), box2, TRUE, TRUE, 0);

  frame = gtk_frame_new("Level range");
  gtk_box_pack_start(GTK_BOX (box2), frame, TRUE, TRUE, 0);
  gtk_widget_show (box2);

  box3 = gtk_vbox_new(FALSE, 0);
  gtk_container_add (GTK_CONTAINER(frame), box3);
  gtk_widget_show(box3);

				// -- Low level
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(box2), 5);
  gtk_box_pack_start(GTK_BOX(box3), box2, TRUE, TRUE, 0);
  
  imadjmin = (GtkAdjustment *) 
    gtk_adjustment_new (0.0, 0.0, 101.0, 0.1, 1.0, 0.0);
  
  label = gtk_label_new ("Low: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  lvscale = gtk_hscale_new (GTK_ADJUSTMENT (imadjmin));
  scale_set_default_values (GTK_SCALE (lvscale));
  gtk_box_pack_end (GTK_BOX (box2), lvscale, TRUE, TRUE, 0);
  gtk_widget_show (lvscale);
  gtk_widget_show (box2);
  
				// -- High level

  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box2), 5);
  gtk_box_pack_start(GTK_BOX(box3), box2, TRUE, TRUE, 0);

  imadjmax = (GtkAdjustment *)
    gtk_adjustment_new (101.0, 0.0, 101.0, 0.1, 1.0, 0.0);

  label = gtk_label_new ("High: ");
  gtk_box_pack_start (GTK_BOX (box2), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  hvscale = gtk_hscale_new (GTK_ADJUSTMENT (imadjmax));
  scale_set_default_values (GTK_SCALE (hvscale));
  gtk_box_pack_end (GTK_BOX (box2), hvscale, TRUE, TRUE, 0);
  gtk_widget_show (hvscale);
  gtk_widget_show (box2);
  
				// Event handlers

  gtk_signal_connect (GTK_OBJECT (imadjmin), "value_changed",
		      GTK_SIGNAL_FUNC (cb_lower_limit), imadjmax);

  gtk_signal_connect (GTK_OBJECT (imadjmax), "value_changed",
		      GTK_SIGNAL_FUNC (cb_upper_limit), imadjmin);

  gtk_signal_connect(GTK_OBJECT(imadjmin), "value_changed",
		     GTK_SIGNAL_FUNC(change_scale_digits),
		     (gpointer) lvscale);
  
  gtk_signal_connect(GTK_OBJECT(imadjmax), "value_changed",
		     GTK_SIGNAL_FUNC(change_scale_digits),
		     (gpointer) hvscale);

  return vbox;
}

//typedef _IO_istream_withassign istream_with_assign;

void parse_data_file(istream& in)
{
  const int bufsize = 32768;
  char buf[bufsize];

  // First line
  in.getline(buf, bufsize);
  if (!in) throw "parse_data_file: error reading header line";

  // Look for leading comment lines
  while (buf[0] == '-') {
    in.getline(buf, bufsize);
    if (!in) throw "parse_data_file: error reading header line";
  }

  string buffer(buf);
  size_t s1;
  while (1) {			// Parse the header line
    s1 = buffer.find("|");

    string label;
    if (s1>=buffer.size())
      label = buffer;
    else
      label = buffer.substr(0, s1);

    if (label.size() > 0) {
				// Remove leading spaces
      while (1) {
	if (label[0] != ' ') break;
	label = label.substr(1, label.size()-1);
      }
				// Remove trailing spaces
      while (1) {
	if (label[label.size()-1] != ' ') break;
	label = label.substr(0, label.size()-1);
      }
      
      fieldNames.push_back(label);
    }

    if (s1>=buffer.size()) break;
    buffer.erase(0, s1+1);
  }

				// Throw out next line
  in.getline(buf, bufsize);
  if (!in) throw "parse_data_file: error reading header line (2)";
  
  int Ndim = fieldNames.size();
  Data = DataSet(Ndim);
  datarecord.data = vector<double>(Ndim);
  string delim;

  while (in) {
    in.getline(buf, bufsize);
    if (in.rdstate() & ios::eofbit) break;
    if (!in) {
      string message = "parse_data_file: error reading data";
      if (in.rdstate() & ios::failbit)  message += " [fail]";
      if (in.rdstate() & ios::badbit)  message += " [bad]";
      throw message.c_str();
    }

				// Look for SQL trailer
    if (buf[0] == '(') {
      cout << "Sql trailer: " << buf << endl;
      break;
    }

    istrstream istr(buf);

    for (int i=0; i<Ndim; i++) {
      istr >> datarecord.data[i];
      if (i != Ndim-1) istr >> delim;
    }

    Data += datarecord;
  }  

  cout << "Data size: " << Data.data.size() << endl;
}

bool using_stdin = false;
string filename;

void reread_data()
{

  if (using_stdin) return;

  try {
    ifstream fin(filename.c_str());
    if (!fin) {
      cerr << "Couldn't open: " << filename << endl;
      throw "parse_data_file: ifstream error";
    }
    parse_data_file(fin);
  }
  catch (const char *message) {
    cerr << message << endl;
  }
  
}

int main( int   argc, char *argv[] )
{
  GtkWidget *window;
  GtkWidget *frame;
  GtkWidget *hbox, *hbox2;
  GtkWidget *main_hbox;
  GtkWidget *vbox_left;
  GtkWidget *vbox_right;
  GtkWidget *vbox, *vbox2;
  GtkWidget *button;

  // Initialise GTK
  gtk_init(&argc, &argv);

  try {
				// Read from stdin
    if (argc == 1) {
      parse_data_file(cin);
      using_stdin = true;
    }
    else if (argc == 2) {	// Open file
      filename = string(argv[1]);
      ifstream fin(filename.c_str());
      if (!fin) {
	cerr << "Couldn't open: " << filename << endl;
	throw "parse_data_file: ifstream error";
      }
      parse_data_file(fin);
    } else {			// Oops!
      throw "parse_data_file: invalid command line";
    }
  }
  catch (const char *message) {
    cerr << message << endl;
    return 0;
  }

  // Make the main window
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (gtk_main_quit),
                      NULL);

  gtk_window_set_title (GTK_WINDOW (window), "PostgreSQL table visualizer");


  // Top level container

  main_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (main_hbox), 10);
  gtk_container_add (GTK_CONTAINER (window), main_hbox);

  // Left hand side container

  vbox_left = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox_left), 10);
  gtk_container_add (GTK_CONTAINER (main_hbox), vbox_left);
  
  // Top left frame

  frame = gtk_frame_new ("Fields");
  gtk_box_pack_start (GTK_BOX (vbox_left), frame, TRUE, TRUE, 0);
  
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);
  
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
  gtk_container_add (GTK_CONTAINER (vbox), hbox);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (hbox), vbox2);
  gtk_container_add(GTK_CONTAINER (vbox2), 
		    get_list2("X-axis", &xlabel, x_menuitem_response));
  
  frame = gtk_frame_new("Options");
  gtk_box_pack_start (GTK_BOX(vbox2), frame, FALSE, FALSE, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), hbox2);

  button = gtk_check_button_new_with_label ("Log");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (set_xlog), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), button, TRUE, TRUE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), xlog);
  gtk_widget_show(button);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (hbox), vbox2);
  gtk_container_add(GTK_CONTAINER (vbox2), 
		    get_list2("Y-axis", &ylabel, y_menuitem_response));

  frame = gtk_frame_new("Options");
  gtk_box_pack_start (GTK_BOX (vbox2), frame, FALSE, FALSE, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), hbox2);

  button = gtk_check_button_new_with_label ("Log");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (set_ylog), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), button, TRUE, TRUE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), ylog);
  gtk_widget_show(button);

  // Center left frame

  frame = gtk_frame_new ("Axis ranges");
  gtk_box_pack_start (GTK_BOX (vbox_left), frame, TRUE, TRUE, 0);
  
  vbox = axis_ranges();
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);
  
  // Cener middle frame

  frame = gtk_frame_new ("Plot type");
  gtk_box_pack_start (GTK_BOX (vbox_left), frame, TRUE, TRUE, 0);
  
  vbox = plot_type();
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  // Right hand side container
  vbox_right = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox_right), 10);
  gtk_container_add (GTK_CONTAINER (main_hbox), vbox_right);

  // Upper right-hand frame

  frame = gtk_frame_new ("Density estimation controls");
  gtk_box_pack_start (GTK_BOX (vbox_right), frame, TRUE, TRUE, 0);
  
  vbox = density_controls();
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  
  /* Bottom right-hand frame */

  frame = gtk_frame_new ("Render controls");
  gtk_box_pack_start (GTK_BOX (vbox_right), frame, TRUE, TRUE, 0);
  
  vbox = plot_controls();
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  
  /* Button colors */
  GdkColor color;
  GtkStyle *style, *oldstyle;

  /* Button bar */
  
  hbox = gtk_hbox_new(FALSE, 0);
  
  /* Add execute button */

  button = gtk_button_new_with_label ("Compute");
  oldstyle = gtk_widget_get_default_style();
  style = gtk_style_copy(oldstyle);
  color.green = 65535;
  color.red = color.blue = 0;
  style->bg[GTK_STATE_NORMAL] = color;
  color.red = color.blue = 32768;
  style->bg[GTK_STATE_PRELIGHT] = color;
  gtk_widget_set_style(button, style);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (compute_data),
		      (gpointer)"Compute!");
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 5);

  /* Add Replot button */

  button = gtk_button_new_with_label ("Replot");
  oldstyle = gtk_widget_get_default_style();
  style = gtk_style_copy(oldstyle);
  color.red = color.green = 65535;
  color.blue = 0;
  style->bg[GTK_STATE_NORMAL] = color;
  color.blue = 32768;
  style->bg[GTK_STATE_PRELIGHT] = color;
  gtk_widget_set_style(button, style);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (make_plot),
		      (gpointer)"Plot!");
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 5);

  /* Add Reread button */

  button = gtk_button_new_with_label ("Reread");
  oldstyle = gtk_widget_get_default_style();
  style = gtk_style_copy(oldstyle);
  color.red = color.green = 65535;
  color.blue = 0;
  style->bg[GTK_STATE_NORMAL] = color;
  color.blue = 32768;
  style->bg[GTK_STATE_PRELIGHT] = color;
  gtk_widget_set_style(button, style);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (reread_data),
		      (gpointer)"Reread!");
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 5);

  /* Add quit button */

  button = gtk_button_new_with_label ("QUIT");
  oldstyle = gtk_widget_get_default_style();
  style = gtk_style_copy(oldstyle);
  color.red = 65535;
  color.green = color.blue = 0;
  style->bg[GTK_STATE_NORMAL] = color;
  color.green = color.blue = 32768;
  style->bg[GTK_STATE_PRELIGHT] = color;
  gtk_widget_set_style(button, style);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (window));
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 5);


  gtk_box_pack_start (GTK_BOX (vbox_left), hbox, FALSE, FALSE, 5);

  /* Bring up the widget */

  gtk_widget_show_all (window);

  /* Refresh on idle */
  gtk_idle_add(refresh, NULL);

  /* Enter the event loop */
  gtk_main ();
    
  return(0);
}

