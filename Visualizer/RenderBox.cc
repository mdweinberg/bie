#include <iostream>
#include <iomanip>

#include <RenderBox.h>
#include "labeledoptionmenu.h"
#include <iostream>

RenderBox::RenderBox()
:
  m_HBox_contour(false, 10),
  m_HBox_buttons1(false, 10),
  m_HBox_buttons2(false, 10),
  m_HBox_low(false, 10),
  m_HBox_high(false, 10),

  // value, lower, upper, step_increment, page_increment, page_size
  // note that the page_size value only makes a difference for
  // scrollbar widgets, and the highest value you'll get is actually
  // (upper - page_size).
  m_adjustment_contour(10., 0., 50., 1.),
  m_adjustment_low(-20, -20, 20, 0.5, 1.0, 0.0),
  m_adjustment_high(20, -20, 20, 0.5, 1.0, 0.0),

  m_Scale_contour(m_adjustment_contour),
  m_Scale_low(m_adjustment_low),
  m_Scale_high(m_adjustment_high),

  // a checkbutton to control whether the value is displayed or not
  m_LogButton("Log scale", 0),
  m_ReverseButton("Reverse colors", 0),
  m_BlackWhiteButton("Black and white", 0),
  m_ThreshButton("Threshold contours", 0),
  m_ApplyFieldButton("Apply to field", 0),
  m_CDFButton("CDF contours", 0),
  m_RainbowButton("Rainbow colors", 0),
  m_Frame_levels("Level range"),
  logarithm(false), reverse(false), blackwhite(false),
  threshold(false), applyfield(false), cdf(false), rainbowmap(false),
  nContours(10), low(-20.0), high(20.0)
{
  add(m_VBox_Top);

  m_VBox_Top.pack_start(m_HBox_contour, Gtk::PACK_SHRINK, 20);
  m_VBox_Top.pack_start(m_HBox_buttons1);
  m_VBox_Top.pack_start(m_HBox_buttons2);
  m_VBox_Top.pack_start(m_Frame_levels, Gtk::PACK_SHRINK, 20);

  m_HBox_contour.set_border_width(10);
  m_HBox_buttons1.set_border_width(10);
  m_HBox_buttons2.set_border_width(10);


  // Contour

  m_HBox_contour.pack_start(*Gtk::manage(new Gtk::Label("Contour levels:", 0)),  Gtk::PACK_SHRINK);
  m_Scale_contour.set_digits(0);
  // m_Scale_contour.set_size_request(200, 10);
  m_Scale_contour.set_draw_value();
  m_Scale_contour.set_update_policy(Gtk::UPDATE_CONTINUOUS);
  m_adjustment_contour.signal_value_changed().connect( sigc::mem_fun(*this, &RenderBox::on_adjustment_contour_changed) );
  m_HBox_contour.pack_start(m_Scale_contour);
  
  // CheckButtons
  m_LogButton.signal_toggled().connect( sigc::mem_fun(*this, &RenderBox::on_log_toggled) );
  m_HBox_buttons1.pack_start(m_LogButton);

  m_RainbowButton.signal_toggled().connect( sigc::mem_fun(*this, &RenderBox::on_cmap_toggled) );
  m_HBox_buttons1.pack_start(m_RainbowButton);

  m_BlackWhiteButton.signal_toggled().connect( sigc::mem_fun(*this, &RenderBox::on_blackwhite_toggled) );
  m_HBox_buttons1.pack_start(m_BlackWhiteButton);

  m_ReverseButton.signal_toggled().connect( sigc::mem_fun(*this, &RenderBox::on_reverse_toggled) );
  m_HBox_buttons1.pack_start(m_ReverseButton);

  m_ThreshButton.signal_toggled().connect( sigc::mem_fun(*this, &RenderBox::on_threshold_toggled) );
  m_HBox_buttons2.pack_start(m_ThreshButton);

  m_ApplyFieldButton.signal_toggled().connect( sigc::mem_fun(*this, &RenderBox::on_applytofield_toggled) );
  m_HBox_buttons2.pack_start(m_ApplyFieldButton);

  m_CDFButton.signal_toggled().connect( sigc::mem_fun(*this, &RenderBox::on_cdf_toggled) );
  m_CDFButton.set_active();	// Activate CDF contours by default
  m_HBox_buttons2.pack_start(m_CDFButton);

  // Range sliders

  m_Frame_levels.set_border_width(10);
  m_Frame_levels.add(m_VBox_levels);
  m_VBox_levels.pack_start(m_HBox_low);
  m_VBox_levels.pack_start(m_HBox_high);
  
  m_HBox_low.pack_start(*Gtk::manage(new Gtk::Label("Low:", 0)), Gtk::PACK_SHRINK);
  m_Scale_low.set_draw_value();
  m_Scale_low.set_update_policy(Gtk::UPDATE_CONTINUOUS);
  m_Scale_low.set_size_request(100, 60);
  m_Scale_low.set_digits(1);
  m_adjustment_low.signal_value_changed().connect( sigc::mem_fun(*this, &RenderBox::on_adjustment_low_changed) );
  m_HBox_low.pack_start(m_Scale_low);

  m_HBox_high.pack_start(*Gtk::manage(new Gtk::Label("High:", 0)), Gtk::PACK_SHRINK);

  m_Scale_high.set_draw_value();
  m_Scale_high.set_update_policy(Gtk::UPDATE_CONTINUOUS);
  m_Scale_high.set_size_request(100, 60);
  m_Scale_high.set_digits(1);
  m_adjustment_high.signal_value_changed().connect( sigc::mem_fun(*this, &RenderBox::on_adjustment_high_changed) );
  m_HBox_high.pack_start(m_Scale_high);

  show_all_children();
}

RenderBox::~RenderBox()
{
}

void RenderBox::on_log_toggled()
{
  if (logarithm)
    logarithm = false;
  else
    logarithm = true;
}

void RenderBox::on_reverse_toggled()
{
  if (reverse)
    reverse = false;
  else
    reverse = true;
}

void RenderBox::on_blackwhite_toggled()
{
  if (blackwhite)
    blackwhite = false;
  else
    blackwhite = true;
}

void RenderBox::on_cmap_toggled()
{
  if (rainbowmap)
    rainbowmap = false;
  else
    rainbowmap = true;
}

void RenderBox::on_threshold_toggled()
{
  if (threshold)
    threshold = false;
  else
    threshold = true;
}

void RenderBox::on_applytofield_toggled()
{
  if (applyfield)
    applyfield = false;
  else
    applyfield = true;
}

void RenderBox::on_cdf_toggled()
{
  if (cdf)
    cdf = false;
  else
    cdf = true;
}

void RenderBox::on_adjustment_contour_changed()
{
  nContours = (int)(m_adjustment_contour.get_value()+0.5);
}

void RenderBox::on_adjustment_low_changed()
{
  low = m_adjustment_low.get_value();
  high = m_adjustment_high.get_value();
  if (low>high) {
    m_adjustment_high.set_value(low);
  }
}

void RenderBox::on_adjustment_high_changed()
{
  low = m_adjustment_low.get_value();
  high = m_adjustment_high.get_value();
  if (low>high) {
    m_adjustment_low.set_value(high);
  }
}


void RenderBox::SetLevelLimits(double x, double y)
{
  m_adjustment_low.set_lower(x);
  m_adjustment_high.set_lower(x);
  m_adjustment_low.set_upper(y);
  m_adjustment_high.set_upper(y);
  m_adjustment_low.set_step_increment((y-x)/100.0);
  m_adjustment_high.set_step_increment((y-x)/100.0);

  if (low<x || low>y) {
    low = x;
    m_adjustment_low.set_value(low);
  }

  if (high<x || high>y) {
    high = y;
    m_adjustment_high.set_value(high);
  }

#ifdef DEBUG
  std::cout << "LOW Lower, Upper, Value, Incr: " 
	    << m_adjustment_low.get_lower()
	    << ", " << m_adjustment_low.get_upper() 
	    << ", " << m_adjustment_low.get_value() 
	    << ", " << m_adjustment_low.get_step_increment() 
	    << std::endl;

  std::cout << "HIGH Lower, Upper, Value: " 
	    << m_adjustment_high.get_lower()
	    << ", " << m_adjustment_high.get_upper()
	    << ", " << m_adjustment_high.get_value() 
	    << ", " << m_adjustment_high.get_step_increment() 
	    << std::endl;
#endif
}
