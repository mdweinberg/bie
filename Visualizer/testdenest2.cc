#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <cstdlib>

#include <string>		// from STL
#include <vector>

#include <unistd.h>
#include <getopt.h>

using namespace std;

#include <Denest2d.h>

void usage(char* prog, int verbose=0) {
  cerr << "\nusage: ... | "
       << prog
       << "[-oname -O -1x -2y -Fa -tm -S -F -h] -- xmin xmax ymin ymax\n\n";
  if (verbose) {
    cerr << "Parameters:\n"
	 << "-----------\n"
	 << "-o name    sets file prefix to \"name\"\n"
	 << "-O         uses stdout for output\n"
	 << "-1 x       use 2^x points in x-dimension FFT (default=9)\n" 
	 << "-2 y       use 2^y points in y-dimension FFT (default=9)\n" 
	 << "-F a       scale Gaussian smoothing parameter (default=1.0)\n" 
	 << "-S         print out cumulative distribution at selected points\n"
	 << "-F         print the cumulative statistics to a file\n"
	 << "-H         normalize on rows\n"
	 << "-V         normalize on columns\n"
	 << "-t m       m: 0=gnuplot, 1=sm, 2=vtk contour  (default=0)\n"
	 << "-h         prints this help\n"
	 << "\nThe values <xmin xmax ymin ymax> set the scaling limits\n"
	 << "for the grid and are required.\n"
	 << "\nThe default smoothing parameter is optimal if the true\n"
	 << "distribution is Gaussian.  In practice, \"a\" may need\n"
	 << "to be set smaller than one esp. if the distribution is\n"
	 << "multimodal with relatively similar amplitude peaks.\n"
	 << "\nThe routine expects pairs of numbers on the standard\n"
	 << "input.\n\n";
  }

  exit(-1);
}



int 
main (int argc, char** argv)
{
				// ------------------------------------------
				// Default parameters
				// ------------------------------------------

  string outputname = "newdata"; // Base name for output files
  int N1 = 9;
  int N2 = 9;
  int Sflg = 0;
  int normVH = 0;
  int output_type = 0;
  double F = 1.0;
  bool use_stdout = false;
				// ------------------------------------------
				// Command line parsing
				// ------------------------------------------

  
  int c;
  while (1)
    {

      c = getopt (argc, argv, "o:O1:2:a:f:t:SFhVH");
      if (c == -1) break;
  
      switch (c)
      {
      case 'o': outputname.erase(); outputname = optarg; break;
      case 'O': use_stdout = true; break;
      case '1': N1 = atoi(optarg); break;
      case '2': N2 = atoi(optarg); break;
      case 'f': F = atof(optarg); break;
      case 'S': Sflg |= 1; break;
      case 'F': Sflg |= 2; break;
      case 'H': normVH=1; break;
      case 'V': normVH=2; break;
      case 't': output_type = atoi(optarg); break;
      case 'h': usage(argv[0], 1); break;
      case '?': usage(argv[0]); break;
      }

    }


  if (argc - optind < 4) {
    usage(argv[0]);
    exit(-1);
  }

				// ------------------------------------------
				// Setup limits
				// ------------------------------------------

  double xmin = atof(argv[optind++]);
  double xmax = atof(argv[optind++]);
  double ymin = atof(argv[optind++]);
  double ymax = atof(argv[optind++]);

				// ------------------------------------------
				// Read in the vectors
				// ------------------------------------------
  vector<double> X, Y;
  double x, y;
  const int bufsize = 2048;
  char buf[bufsize];

  while(cin) {
    cin.getline(buf, bufsize);
    istringstream istr(buf);
    istr >> x;
    istr >> y;

    if (istr) {
      X.push_back(x);
      Y.push_back(y);
    }
  }

				// ------------------------------------------
				// Invoke the smoother
				// ------------------------------------------

  Denest2d smooth(xmin, xmax, ymin, ymax, X, Y);

  smooth.set_sflags(Sflg);
  smooth.set_nflags(normVH);
  smooth.set_N1(N1);
  smooth.set_N2(N2);
  smooth.set_F(F);
  smooth.set_output_type(output_type);

  if (use_stdout) smooth.output(cout);
  else {
    string fname = outputname + ".smooth";

    ofstream out(fname.c_str());
    if (!out) {
      cerr << "Error opening <" << fname << ">\n";
      exit(-1);
    }
    smooth.output(out);
  }

  return 0;
}
