// This is really -*- C++ -*-

#ifndef _DENSEBOX_H
#define _DENSEBOX_H

#include <math.h>
#include <gtkmm.h>

class DenseBox : public Gtk::Frame
{
public:
  DenseBox(string fname);
  virtual ~DenseBox();

  double GetKernel() { return kernel; }
  double GetPedestal() { return pedestal; }
  int GetMinIter() { return (int)rint(miniter); }
  int GetMaxIter() { return (int)rint(maxiter); }
  int GetNorm() { return normVH; }
  void SetMinIterRange(int imin);
  void SetMaxIterRange(int imax);
  bool Cumulate() { return cumulate; }

protected:

  double kernel, pedestal, miniter, maxiter;
  bool cumulate, peglow, peghigh, peghalf;
  int normVH;

  // Signal handlers:
  virtual void on_rowbutton_toggled();
  virtual void on_columnbutton_toggled();
  virtual void on_cumulativebutton_toggled();
  virtual void on_peglowbutton_toggled();
  virtual void on_peghighbutton_toggled();
  virtual void on_peghalfbutton_toggled();
  virtual void on_adjustment_value_changed();
  virtual void on_spinbutton_value_changed();
  virtual void on_miniter_value_changed();
  virtual void on_maxiter_value_changed();

  // Child widgets:

  Gtk::VBox m_VBox_Top, m_VBox_iter, m_VBox_smooth, m_VBox_text;
  Gtk::HBox m_HBox_kernel, m_HBox_buttons, m_HBox_pedestal, m_HBox_text;
  Gtk::HBox m_HBox_miniter, m_HBox_maxiter;
  Gtk::Frame m_Frame_smooth, m_Frame_file, m_Frame_iter;

  Gtk::Adjustment m_adjustment_kernel, m_adjustment_pedestal;
  Gtk::Adjustment m_adjustment_miniter, m_adjustment_maxiter;

  Gtk::HScale m_Scale_kernel, m_Scale_miniter, m_Scale_maxiter;

  Gtk::CheckButton m_RowButton, m_ColumnButton, m_CumulativeButton;
  Gtk::CheckButton m_PegLowButton, m_PegHighButton, m_PegHalfButton;

  Gtk::Label m_Label_pedestal, m_Label_miniter, m_Label_maxiter;

  Gtk::Label m_FileLabel;

  Gtk::SpinButton m_SpinButton_pedestal;
};

#endif // _DENSEBOX_H
