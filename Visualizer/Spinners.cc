#include "Spinners.h"
#include <iostream>
#include <math.h>
#include <stdio.h>

Spinners::Spinners(std::string name)
:
  direction(0),
  m_VBox_Main(false, 5),
  m_Label_Value(name),
  m_Label_Digits("Digits:"),
  m_adjustment_value(0.0, -10000.0, 10000.0, 0.01, 100.0, 0.0),
  m_adjustment_digits(2.0, 1.0, 7.0, 1.0, 1.0, 0.0),
  m_SpinButton_Value(m_adjustment_value, 1.0, 2),
  m_SpinButton_Digits(m_adjustment_digits),
  m_CheckButton_Snap("Truncate")
{
  m_VBox_Main.set_border_width(10);
  add(m_VBox_Main);

  m_VBox_Main.pack_start(m_VBox);

  m_VBox.set_border_width(5);

  m_VBox.pack_start(m_HBox, Gtk::PACK_EXPAND_WIDGET, 5);

  m_HBox.pack_start(m_VBox_Value, Gtk::PACK_EXPAND_WIDGET, 5);

  m_Label_Value.set_alignment(Gtk::ALIGN_LEFT);
  m_VBox_Value.pack_start(m_Label_Value);

  m_SpinButton_Value.set_wrap();
  m_SpinButton_Value.set_size_request(100, -1);
  m_VBox_Value.pack_start(m_SpinButton_Value);

  m_HBox.pack_start(m_VBox_Digits, Gtk::PACK_EXPAND_WIDGET, 5);

  m_Label_Digits.set_alignment(Gtk::ALIGN_LEFT);
  m_VBox_Digits.pack_start(m_Label_Digits);

  m_SpinButton_Digits.set_wrap();
  m_adjustment_digits.signal_value_changed().connect( sigc::mem_fun(*this, &Spinners::on_spinbutton_digits_changed) );

  m_VBox_Digits.pack_start(m_SpinButton_Digits);


  // CheckButtons
  m_VBox.pack_start(m_CheckButton_Snap);
  // m_CheckButton_Snap.set_active();
  m_CheckButton_Snap.signal_clicked().connect( sigc::mem_fun(*this, &Spinners::on_checkbutton_snap) );

  // Buttons
  m_VBox.pack_start (m_HBox_Buttons, Gtk::PACK_SHRINK, 5);

  show_all_children();
}

Spinners::Spinners()
:
  direction(0),
  m_VBox_Main(false, 5),
  m_Label_Value("Value: "),
  m_Label_Digits(""),
  m_adjustment_value(0.0, -10000.0, 10000.0, 0.01, 100.0, 0.0),
  m_adjustment_digits(2.0, 1.0, 5.0, 1.0, 1.0, 0.0),
  m_SpinButton_Value(m_adjustment_value, 1.0, 2),
  m_SpinButton_Digits(m_adjustment_digits),
  m_CheckButton_Snap("Truncate")
{
  m_VBox_Main.set_border_width(10);
  add(m_VBox_Main);

  m_VBox_Main.pack_start(m_VBox);

  m_VBox.set_border_width(5);

  m_VBox.pack_start(m_HBox, Gtk::PACK_EXPAND_WIDGET, 5);

  m_HBox.pack_start(m_VBox_Value, Gtk::PACK_EXPAND_WIDGET, 5);

  m_Label_Value.set_alignment(Gtk::ALIGN_LEFT);
  m_VBox_Value.pack_start(m_Label_Value);

  m_SpinButton_Value.set_wrap();
  m_SpinButton_Value.set_size_request(100, -1);
  m_VBox_Value.pack_start(m_SpinButton_Value);

  m_HBox.pack_start(m_VBox_Digits, Gtk::PACK_EXPAND_WIDGET, 5);

  m_Label_Digits.set_alignment(Gtk::ALIGN_LEFT);
  m_VBox_Digits.pack_start(m_Label_Digits);

  m_SpinButton_Digits.set_wrap();
  m_adjustment_digits.signal_value_changed().connect( sigc::mem_fun(*this, &Spinners::on_spinbutton_digits_changed) );

  m_VBox_Digits.pack_start(m_SpinButton_Digits);


  // CheckButtons
  m_VBox.pack_start(m_CheckButton_Snap);
  m_CheckButton_Snap.set_active();
  m_CheckButton_Snap.signal_clicked().connect( sigc::mem_fun(*this, &Spinners::on_checkbutton_snap) );

  // Buttons
  m_VBox.pack_start (m_HBox_Buttons, Gtk::PACK_SHRINK, 5);

  show_all_children();
}

Spinners::~Spinners()
{
}


void Spinners::set_direction(int dir)
{
  if (dir<0) 
    direction=-1;
  else if (dir>0)
    direction= 1;
  else
    direction=0;
}

void Spinners::on_checkbutton_snap()
{
  int digits =  m_SpinButton_Value.get_digits();
  double power = pow(10.0, digits);
  double value1, value = m_SpinButton_Value.get_value();
  
  if (direction>0) {
    value1 = ceil(fabs(value)*power)/power;
    
  } else if (direction<0) {
    value1 = floor(fabs(value)*power)/power;
    if (value<0) value1 *= -1.0;
  } else {
    value1 = round(fabs(value)*power)/power;
    if (value<0) value1 *= -1.0;
  }
  
  m_SpinButton_Value.set_value(value1);
}

void Spinners::on_spinbutton_digits_changed()
{
  m_SpinButton_Value.set_digits( m_SpinButton_Digits.get_value_as_int() );
  m_adjustment_value.set_step_increment( pow(10.0, -m_SpinButton_Digits.get_value()) );
}


void Spinners::set_limits(double min, double max)
{
  m_adjustment_value.set_lower(min);
  m_adjustment_value.set_upper(max);
}

void Spinners::set_value(double val)
{
  m_adjustment_value.set_value(val);
}

double Spinners::get_value()
{
  return m_adjustment_value.get_value();
}
