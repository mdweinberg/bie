//  Copyright (C) 2003, 2008 Martin D. Weinberg <weinberg@astro.umass.edu>
//
//  Modified to work as a generic vtk/gtk widget. Removed visu specific
//  things. Renamed from vtkglarea.cc to vtkgtkarea.cc to prevent confusion.

#ifndef __VTKGTKBOX_H__
#define __VTKGTKBOX_H__

#include <gtk/gtk.h>
#include <gtkmm.h>
#include <gtkglmm.h>

// For details, see:
// http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Factories_now_require_defines
// 
#include <vtkVersion.h>
#if VTK_MAJOR_VERSION<=6
#if VTK_MINOR_VERSION<2
#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#else
#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL);
VTK_MODULE_INIT(vtkRenderingFreeType);
#endif
#else
#if VTK_MAJOR_VERSION>=7
#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkRenderingFreeType);
#endif
#endif

#include <vtkOpenGLRenderWindow.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <string>

#undef VERSION
#include <GL/gl.h>
#include <GL/glu.h>


using std::string;

//
// OpenGL frame buffer configuration utilities.
//

struct GLConfigUtil
{
  static void print_gl_attrib(const Glib::RefPtr<const Gdk::GL::Config>& glconfig,
                              const char* attrib_str,
                              int attrib,
                              bool is_boolean);

  static void examine_gl_attrib(const Glib::RefPtr<const Gdk::GL::Config>& glconfig);
};


//
// Simple OpenGL scene.
//

class VtkGtkBox : public Gtk::DrawingArea,
		  public Gtk::GL::Widget<VtkGtkBox>
{
public:
  VtkGtkBox();
  virtual ~VtkGtkBox();

  void screenshot(const string& filename);
  void reset_view();  

  vtkRenderer* get_renderer();

protected:
  virtual void on_realize();
  virtual bool on_configure_event(GdkEventConfigure* event);
  virtual bool on_expose_event(GdkEventExpose* event);

  vtkRenderer *ren;
  vtkRenderWindow *ren_win;

  double initial_position[3];

  GdkWindow *window;
  void* wid;
};


#endif // __VTKGTKBOX_H__
