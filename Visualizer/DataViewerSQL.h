#ifndef _DATAVIEWERSQL_H
#define _DATAVIEWERSQL_H

#include <gtkmm.h>

#include <VtkGtkBox.h>
#include <XYPlot.h>
#include <VtkDenest2d.h>
#include <SQLFieldBox.h>
#include <Spinners.h>
#include <DenseBox.h>
#include <RenderBox.h>

class DataViewerSQL : public Gtk::Window
{
public:
  DataViewerSQL(int size, string filename);
  virtual ~DataViewerSQL();

protected:
  //Signal handlers:
  virtual void on_button_curve();
  virtual void on_button_contour();
  virtual void on_button_reread();
  virtual void on_button_jpeg();
  virtual void on_button_quit();

  //Pixels
  int psize;

  // Plots
  XYPlot *xyplot;
  VtkDenest2d *tdplot;
  bool screendump_ok;

  //Child widgets:
  Gtk::VBox m_VBox;
  Gtk::Notebook m_Notebook;

  Gtk::HButtonBox m_ButtonBox;
  Gtk::Button m_Button_Curve;
  Gtk::Button m_Button_Contour;
  Gtk::Button m_Button_Reread;
  Gtk::Button m_Button_Jpeg;
  Gtk::Button m_Button_Quit;

  VtkGtkBox my_vtk_box0;
  VtkGtkBox my_vtk_box1;

  SQLFieldBox my_fields;
  DenseBox my_dens;
  RenderBox my_render;
};

#endif //_DATAVIEWERSQL_H

