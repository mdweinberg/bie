#include <math.h>
#include <iostream>
#include <algorithm>

using namespace std;

#include "DataViewer.h"
#include "config.h"
#include "HelpWindow.h"

int Density_estimate(vector<double>& dt, double dlo, double dhi, double window,
		     vector<double>& sm);

DataViewer::DataViewer(int size, string filename)
  : 
  screendump_ok0(false),
  screendump_ok1(false),
  m_Button_Curve("Curve"), 
  m_Button_Marginal("Marginal"), 
  m_Button_Contour("Contour"), 
  m_Button_Reread("Reread"), 
  m_Button_Jpeg("Jpeg"),
  m_Button_Help(Gtk::Stock::HELP),
  m_Button_Quit(Gtk::Stock::QUIT),
  my_fields(filename),
  my_dens(filename)
{
  void TestCylinder(vtkRenderer *ren);

  my_fields.addMenus(my_fields.get_x_menu(), 
		     my_fields.get_y_menu());

  set_title("BIE Visualizer " VERSION);
  set_border_width(10);
  set_default_size(size, size);


  add(m_VBox);

  //
  // Add the Notebook, with the button underneath:
  //
  m_Notebook.set_border_width(10);
  m_VBox.pack_start(m_Notebook);
  m_VBox.pack_start(m_ButtonBox, Gtk::PACK_SHRINK);

  m_ButtonBox.pack_start(m_Button_Curve, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Marginal, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Contour, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Reread, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Jpeg, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Help, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Quit, Gtk::PACK_SHRINK);

  m_Button_Curve.signal_clicked().connect( sigc::mem_fun(*this, &DataViewer::on_button_curve) );
  m_Button_Marginal.signal_clicked().connect( sigc::mem_fun(*this, &DataViewer::on_button_marginal) );
  m_Button_Contour.signal_clicked().connect( sigc::mem_fun(*this, &DataViewer::on_button_contour) );
  m_Button_Reread.signal_clicked().connect( sigc::mem_fun(*this, &DataViewer::on_button_reread) );
  m_Button_Jpeg.signal_clicked().connect( sigc::mem_fun(*this, &DataViewer::on_button_jpeg) );
  m_Button_Help.signal_clicked().connect( sigc::mem_fun(*this, &DataViewer::on_button_help) );
  m_Button_Quit.signal_clicked().connect( sigc::mem_fun(*this, &DataViewer::on_button_quit) );


  //
  // Add the Notebook pages:
  //
  m_Notebook.append_page(my_vtk_box0,  "X-Y plot");
  m_Notebook.append_page(my_vtk_box1,  "3D object");
  m_Notebook.append_page(my_fields,    "Fields");
  m_Notebook.append_page(my_dens,      "Density controls");
  m_Notebook.append_page(my_render,    "Render controls");

  my_fields.setDensCallback(&my_dens);

  show_all();

  //
  // Plots
  //

  int dataSize = 100;
  vector<double> X(dataSize), Y(dataSize);
  double xmin=1.0e30, xmax=-1.0e30;
  double ymin=1.0e30, ymax=-1.0e30;
  for(int i=0; i<dataSize; i++)
      {
	X[i] = 0.03*(i+1000.0);
	Y[i] = 1.0e-4*i*i;
	xmin = min<double>(X[i], xmin);
	ymin = min<double>(Y[i], ymin);
	xmax = max<double>(X[i], xmax);
	ymax = max<double>(Y[i], ymax);
      }
  
  string xlab("dog"), ylab("cat");

  xyplot = new XYPlot(X, Y);
  xyplot->setXlabel(xlab);
  xyplot->setYlabel(ylab);
  xyplot->plotVtk(my_vtk_box0.get_renderer());

  tdplot = new VtkDenest2d(xmin, xmax, ymin, ymax, X, Y);
  tdplot->SetLabels(xlab, ylab, string());
  tdplot->SetContours(10);
  tdplot->plotVtk(my_vtk_box1.get_renderer());

  m_Notebook.set_current_page(1);
}

DataViewer::~DataViewer()
{
}

void DataViewer::on_button_quit()
{
  hide();
}

void DataViewer::on_button_curve()
{
  vector<double> X, Y;
  string xlabel, ylabel;
  double xmin, xmax, ymin, ymax;
    
  my_fields.getData(X, Y, xlabel, ylabel);
  my_fields.getRanges(xmin, xmax, ymin, ymax);
    
  xyplot->newData(X, Y);
  xyplot->setXlabel(xlabel);
  xyplot->setYlabel(ylabel);
  xyplot->setXrange(xmin, xmax);
  xyplot->setYrange(ymin, ymax);
  xyplot->plotVtk(my_vtk_box0.get_renderer());
  
  // Move to X-Y page
  m_Notebook.set_current_page(0);

  // Allow screendump
  screendump_ok0 = true;

}

bool less_than_pair(const pair<double, int>& x, const pair<double, int>& y)
{
  return x.first < y.first;
}

void DataViewer::on_button_marginal()
{
  const double LAP = 1.0;
  const int NOUT = 2<<10;

  vector<double> D, X(NOUT), Y(NOUT);
  typedef pair<double, int> DPair;
  vector<DPair> C(NOUT);
  string xlabel, ylabel;
  double xmin, xmax, ymin, ymax;
    
  // Get the data for the chosent field
  
  my_fields.getData(D, xlabel, 
		    my_dens.GetMinIter(), my_dens.GetMaxIter());

  ylabel = "dP/(" + xlabel + ")";

  // Need to do density estimation here

  xmin =  1.0e40;
  xmax = -1.0e40;
  double mean = 0.0,  var = 0.0;
  int num = D.size();

  for (int i=0; i<num; i++) {
    xmin = min<double>(xmin, D[i]);
    xmax = max<double>(xmax, D[i]);
    mean += D[i];
    var  += D[i] * D[i];
  }

  mean /= num;
  var = sqrt( fabs(var - num*mean*mean)/(num-1) );

  double win0 = 1.06*var/pow(num, 0.2);
  xmin = xmin - 3.0*win0*LAP;
  xmax = xmax + 3.0*win0*LAP;
  double window = win0 * my_dens.GetKernel();

  int iret = Density_estimate(D, xmin, xmax, window, Y);
  if (iret) {
    cerr << endl
	 << "denest: Density estimation failed [Return status=" << iret
	 << "]" << endl
	 << "denest: The most likely problem is bad input data."
	 << endl
	 << "denest: Have you selected a field with constant data?"
	 << endl;
    return;
  }

  // Compute abscissa and cumulate distribution
  //
  double dx = (xmax - xmin)/NOUT;
  ymin = ymax = 0.0;
  for (int i=0; i<NOUT; i++) {
    X[i] = xmin + dx*i;
    ymax = max<double>(ymax, Y[i]);
    if (i) 
      C[i].first = C[i-1].first + dx*Y[i];
    else
      C[i].first = dx*Y[i];

    C[i].second = i;
  }
  ymax *= 1.05;

  // Print out quantiles
  //
  double quantiles[] = {0.0013499, 0.022750, 0.15866, 0.5, 
			0.84134, 0.97725, 0.99865};

  string sigmas[] = {"-3", "-2", "-1", "median", "1", "2", "3"};

  // double quantiles[] = {0.005, 0.01, 0.05, 0.10, 0.25, 
  // 0.5, 0.75, 0.9, 0.95, 0.99, 0.995};

  const int N = sizeof(quantiles) / sizeof(double);

  vector<DPair>::iterator ub;
  DPair xx;
  int indx;

  cout << setw(70) << setfill('=') << "=" << setfill(' ') << endl;
  cout << setw(10) << "Field:"    << setw(10) << xlabel << endl;
  cout << setw(10) << "Level:"    << setw(10) << my_fields.getLevel() << endl;
  cout << setw(10) << "Iter_min:" << setw(10) << my_dens.GetMinIter() << endl;
  cout << setw(10) << "Iter_max:" << setw(10) << my_dens.GetMaxIter() << endl;
  cout << setw(10) << "Width:"    << setw(10) << window << endl;
  cout << endl
       << setw(20) << "# StdDev"
       << setw(20) << "Quantile"
       << setw(20) << xlabel 
       << endl
       << setw(20) << "--------------"
       << setw(20) << "--------------"
       << setw(20) << "--------------" 
       << endl;

  // Linearly interpolate to get quantiles
  //
  for (int i=0; i<N; i++) {
    xx.first = quantiles[i]*C.back().first;
    ub = upper_bound(C.begin(),C.end(), xx, less_than_pair);

				// Sanity check
    if (ub == C.end()) indx = C.size()-2;
    else               indx = ub->second-1;

				// Make sure interpolation
				// is on the grid
    indx = max<int>(indx, 0);
    indx = min<int>(indx, C.size()-2);

    double del = C[indx+1].first - C[indx].first;
    double w1 = (C[indx+1].first - xx.first)/del;
    double w2 = (xx.first - C[indx  ].first)/del;

    cout << setw(20) << sigmas[i]
	 << setw(20) << quantiles[i] 
	 << setw(20) << X[indx]*w1 + X[indx+1]*w2
	 << endl;
  }

  // Do we want to plot cumulate data?
  //
  if (my_dens.Cumulate()) {
    for (int i=0; i<NOUT; i++) Y[i] = C[i].first;
    ylabel = "P( < " + xlabel + ")";
    ymax = 1.0;
  }

  // Ok, make the plot
  //
  xyplot->newData(X, Y);
  xyplot->setXlabel(xlabel);
  xyplot->setYlabel(ylabel);
  xyplot->setXrange(xmin, xmax);
  xyplot->setYrange(ymin, ymax);
  xyplot->plotVtk(my_vtk_box0.get_renderer());
  
  // Move to X-Y page
  m_Notebook.set_current_page(0);

  // Allow screendump
  screendump_ok0 = true;
}

void DataViewer::on_button_contour()
{
  vector<double> X, Y;
  string xlabel, ylabel;
  double xmin, xmax, ymin, ymax;
    
  my_fields.getData(X, Y, xlabel, ylabel, 
		    my_dens.GetMinIter(), my_dens.GetMaxIter());

  my_fields.getRanges(xmin, xmax, ymin, ymax);

#ifdef DEBUG    
  cout << "Xmin Xmax, Ymin, Ymax: "
       << xmin << ", " << xmax << ", " << ymin << ", " << ymax
       << endl;
#endif
  tdplot->newData(xmin, xmax, ymin, ymax, X, Y);
  tdplot->set_F(my_dens.GetKernel());
  tdplot->set_pedestal(my_dens.GetPedestal());
  tdplot->set_log(my_render.Log());
  tdplot->set_nflags(my_dens.GetNorm());
  tdplot->loadVtk();
				// Get/Set contour levels
  double vmin, vmax;
  double low, high;

  my_render.GetLevelValues(vmin, vmax);
  tdplot->getMinMax(low, high);

#ifdef DEBUG
  cout << "Got values: "  << vmin << ", " << vmax << endl;
  cout << "Got limits: "  << low  << ", " << high << endl;
#endif

  my_render.SetLevelLimits(low, high);
  tdplot->SetLabels(xlabel, ylabel, string());
  tdplot->SetContours(my_render.Contours());
  tdplot->setMinMax(max<double>(low, vmin), min<double>(high, vmax));
  tdplot->Threshold(my_render.Threshold());
  tdplot->ThresholdC(my_render.ApplyToField());
  tdplot->BlackAndWhite(my_render.BlackAndWhite());
  tdplot->ReverseColors(my_render.ReverseColors());
  tdplot->RainbowMap(my_render.RainbowMap());
  tdplot->CDF(my_render.CDF());
  
				// Make the plot
  tdplot->plotVtk(my_vtk_box1.get_renderer());
  
				// Move to 3D page
  m_Notebook.set_current_page(1);

				// Allow screendump
  screendump_ok1 = true;
}


void DataViewer::on_button_reread()
{
  my_fields.Reread();
  my_fields.resetMenuRanges();
  my_fields.setDensCallback(&my_dens);
  // Move to Fields page
  m_Notebook.set_current_page(2);
}

void DataViewer::on_button_jpeg()
{
  switch (m_Notebook.get_current_page()) {
  case 0:
    if (screendump_ok0) {
      my_vtk_box0.screenshot("curve");
      cerr << "Wrote file <curve.jpg>" << endl << flush;
    }
    break;
  case 1:
    if (screendump_ok1) {
      my_vtk_box1.screenshot("contour");
      cerr << "Wrote file <contour.jpg>" << endl << flush;
    }
    break;
  }
}

void DataViewer::on_button_help()
{
  Gtk::Window *window = Gtk::manage(new HelpWindow);
  window->show_all();
}

