#!/bin/sh

if test ! -f install-sh ; then touch install-sh ; fi

MAKE=`which gnumake`
if test ! -x "$MAKE" ; then MAKE=`which gmake` ; fi
if test ! -x "$MAKE" ; then MAKE=`which make` ; fi
HAVE_GNU_MAKE=`$MAKE --version|grep -c "Free Software Foundation"`

if test "$HAVE_GNU_MAKE" != "1"; then
echo !!!! Warning: not tested with non Gnu-Make $MAKE
else
echo Found GNU Make at $MAKE ... good.
fi

MAKE_OPTIONS="CXXFLAGS=\"-O3 -fno-inline-small-functions\" CFLAGS=\"-O3\" -j2"

echo This script runs configure and make...
echo You did remember necessary arguments for configure, right?

if test ! -x `which aclocal`
then echo you need autoconf and automake to generate the Makefiles
fi
if test ! -x `which autoheader`
then echo you need autoconf and automake to generate the Makefiles
fi
if test ! -x `which libtoolize`
then echo you need libtool to generate the Makefiles
fi
if test ! -x `which automake`
then echo you need automake to generate the Makefiles
fi
if test ! -x `which autoconf`
then echo you need autoconf to generate the Makefiles
fi
aclocal -I m4
autoheader
libtoolize --force --copy
automake --add-missing --copy -Wno-portability
autoconf
./configure $*
eval $MAKE $MAKE_OPTIONS

echo ------------------------------------------
echo If your make completed successfully . . . 
echo finish with a \"make install\"
echo ------------------------------------------

