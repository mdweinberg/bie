#include <iomanip>

#include "gvariable.h"
#include "TestLoadManager.h"
#include "TessToolGPL.h"
#include "tesstoolMutex.h"
#include <BasicType.h>

using namespace BIE;

//! For sorting on X coordinate
bool compare_GPL_X(const GPL_record &a, const GPL_record &b) 
{
  return a.x < b.x;
}

//! For sorting on Y coordinate
bool compare_GPL_Y(const GPL_record &a, const GPL_record &b) 
{
  return a.y < b.y;
}

TessToolGPL::TessToolGPL()
{
  initialize();
  tterr << "TessTool persistence dir is " << _persistencedir << endl;
}

TessToolGPL::~TessToolGPL()
{
}


TessToolGPL::TessToolGPL(string pdir)
{
  _persistencedir = pdir;
  initialize();
}

void TessToolGPL::initialize()
{
  // Debug output file
  tterr.open("errfile.tt");
  if (tterr) cout << "Opened error file\n";

  _mpistrm=NULL;
  _initGPL = false;

  _tmpScalarNameString = string();
  _tmpScalarInfoNameString = string();
  
  _scalarNameChanged =true;
  _scalarInfoNameChanged =  true; 
  
  _scalarInfoNameInitialized = false;
  _scalarNameInitialized = false;
  
  _firstDataSet = true;
  _sampleNum = 0;

  _persistencedir = string(PERSISTENCE_DIR) + "/" + TESSELLATION_STORE;

}

void TessToolGPL::initGPL()
{
  if (!_initTessellation) {
    initializeTessellation();
  } else {
    tterr << "TessToolGPL:: tessellation already initialized" << endl;
  }
}

void TessToolGPL::initializeTessellation()
{
  int tries=3;
  int rv=1;
  struct stat s;
  string cmd = "ls -lR " + _persistencedir;

  tterr << system(cmd.c_str()) << endl;
  
  while(rv) {			// Polling loop: wait for 
				// stored tessellation to exist
    if (rv < 0) sleep(5);
    rv = stat(_persistencedir.c_str(), &s);
  }
  
  TestUserState *restored;

  while (tries-->0) {
    
    ++tesstoolMutex;

    try {
      
      try {
	string saved(TESSELLATION_STORE);
	TestLoadManager loadManager(&restored, saved, 1,
				    BOOST_BINARY, BACKEND_FILE);
	loadManager.load();
#ifdef DEBUG_TESSTOOL
	cout << "TessToolGPL: " << TESSELLATION_STORE << " loaded" << endl;
#endif
      } catch (BoostSerializationException * e) {
	cout << 
	  "################################################################" 
	     << endl <<
	  "##### BoostSerializationException thrown on Tesstool load! #####" 
	     << endl << "Printing persistence stack trace..." << endl;
	e->print(cout);
	cout <<
	  "################################################################" 
	     << endl;
      }
  
      vector<Serializable*> objectlist = restored->getTransClosure().objectlist;
  
      QuadGrid *rtess = dynamic_cast<QuadGrid*>(objectlist[0]);
      _tess = rtess;
      _tessType = TESS_QUADGRID;
      
      tterr << "restored tess" << endl;
      tterr << "Total number of tiles=" << rtess->NumberTiles() << endl;
      vector<int> rtiles = rtess->GetRootTiles();
      tterr << "Number of root tiles=" << rtiles.size() << endl;
      
      vector<int>::iterator ritr = rtiles.begin();
      while (ritr !=  rtiles.end()) {
	tterr << *ritr++ << endl; 
      }
      vector<Node*> rnodes = rtess->GetRootNodes();
      tterr << "Number of root nodes=" << rnodes.size() << endl;
      vector<Node*>::iterator rnitr = rnodes.begin();
      while (rnitr !=  rnodes.end()) {
	Node * rnode = (Node*) *rnitr;
	rnode->print(cout);
	rnode->print(rferr);
	rnitr++; 
      }
      tterr << "Restored Tree" << endl;
      rtess->PrintPreOrder(tterr);
      tterr << "----------------------------------------------------------" << endl;
    }
    catch (BIEException e)
      { 
	cerr << "Caught Exception: \n" << e.getErrorMessage() << endl; 
      }
    
    --tesstoolMutex;

    if (_tess != NULL) break;

  }
  if (_tess != NULL) {
    _initTessellation = true;
  } else { 
    tterr << "Could not initialize Tessellation" << endl;
    std::exit(1024);
  }
}

void TessToolGPL::createNewDataSet(ostream& out, int indx, 
				   bool ordered)
{

  int recordNum=0;
  int tileid=-1;
  double scalar;
  
  _scalarIndex = indx;

  rferr << "In TessToolGPL::createNewDataSet" << endl;
  
  curtype = _mpistrm->getFieldType(indx);

  double x1, x2, y1, y2;
  double ll[2] = { 1e20,  1e20};
  double ur[2] = {-1e20, -1e20};

  while(_mpistrm->nextRecord()) {
    tterr << "Record Num=" << recordNum << endl;
    tterr << _mpistrm->getBuffer()->toString() << endl;
    recordNum++;
    tileid = _mpistrm->getTileId();
    Tile * tile = _tess->GetTile(tileid);
    Node * node = tile->GetNode();
    scalar = getScalar();

    listTileData(node, scalar);
    
    node->GetTile()->corners(x1, x2, y1, y2);
    if (ll[0] >= x1 && ll[1] >= y1) {
      ll[0] = x1;
      ll[1] = y1;
    }
    if (ur[0] <= x2 && ur[1] <= y2) {
      ur[0] = x2;
      ur[1] = y2;
    }

  }

  outputData(out, ordered);
}


static int added=0;

void TessToolGPL::listTileData(Node *node, double sclr)
{
  double minx, maxx, miny, maxy;
  float scalar = sclr;
  
  if (node == NULL) return;

  Tile *tile = node->GetTile();

  tile->corners(minx, maxx, miny, maxy);

  tterr << " Adding Point " << added++ << " with id " << node->ID() << " ";
  tterr << "xmin[" << added << "]=" << minx << "; ";
  tterr << "xmax[" << added << "]=" << maxx << "; ";
  tterr << "ymin[" << added << "]=" << miny << "; ";
  tterr << "ymax[" << added << "]=" << maxy << "; ";
  tterr << "scalar=" << scalar;

  tterr << endl;

  record.x = 0.5*(minx + maxx);
  record.y = 0.5*(miny + maxy);
  record.s = scalar;

  cells.push_back(record);
}


void TessToolGPL::outputData(ostream& out, bool ordered)
{
  vector<GPL_record>::iterator i = cells.begin();

  if (ordered) {
    // Sort by x-axis;
    sort(cells.begin(), cells.end(), compare_GPL_X);

    double last = i->x;
    stanza.push_back(*(i++));

    while (i != cells.end()) {
				// New stanza?  Dump old one.
      if ( fabs(i->x - last) > 1.0e-16 ) { 
	sort(stanza.begin(), stanza.end(), compare_GPL_Y);
	vector<GPL_record>::iterator j = stanza.begin();
	for (;j != stanza.end(); j++)
	  out << setw(15) << j->x
	      << setw(15) << j->y
	      << setw(15) << j->s
	      << endl;
	out << endl;

	stanza.erase(stanza.begin(), stanza.end());
      }

      last = i->x;
      stanza.push_back(*(i++));
    }

    // Dump last stanza

    sort(stanza.begin(), stanza.end(), compare_GPL_Y);
    vector<GPL_record>::iterator j = stanza.begin();
    for (;j != stanza.end(); j++)
      out << setw(15) << j->x
	  << setw(15) << j->y
	  << setw(15) << j->s
	  << endl;
    
  } else {

    for(; i!= cells.end(); i++)
      out << setw(15) << i->x
	  << setw(15) << i->y
	  << setw(15) << i->s
	  << endl;
  }

}

double TessToolGPL::getScalar()
{
  double scalar;

  try {
    if (BasicType::Int->equals(curtype))
      scalar = _mpistrm->getIntValue(_scalarIndex);
    else
      scalar = _mpistrm->getRealValue(_scalarIndex);
  } catch (BIEException e) {
    tterr << "Error on index=" << _scalarIndex
	  << " field=" << _scalarNameString
	  << endl;
    try {
      scalar = _mpistrm->getIntValue(_scalarIndex);
    }
    catch (BIEException e) {
      tterr << "TessToolVTK::getScalar(): Invalid data type" << endl;
      scalar = 0.0;
    }
    tterr << "Found an <int> not a <real>"
	  << endl;
  }

  return scalar;
}

