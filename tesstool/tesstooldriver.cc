#include "TessToolVTK.h"
#include "gvariable.h"
#include "CliCommandThread.h"
#include "TessToolConsole.h"
#include "TessToolReceiver.h"
#include "tesstoolMutex.h"
#include "RecordInputStream_MPI.h"

#include "TessToolGUI.h"
#include "TessToolController.h"

#include <fstream>

using namespace BIE;

namespace TESSTOOL 
{
  bool work = true;
  bool viewer = false;
}

void batchDriver(int argc, char *argv[])
{
  int epochNum=0;
  // int numEpochs=100;
  
  //
  char *hname = argv[2];
  int port = atoi(argv[3]);
  string scriptfile(argv[4]);
  
  // start BIEInterface thread
  TessToolDataStream *rcvrthread;
  string ttname = string("TessTool");
  
  Semaphore *controllerVTK = new Semaphore(0);
  Semaphore *controllerCLI = new Semaphore(0);
  Semaphore *controllerRX = new Semaphore(0);
  
  rcvrthread = new TessToolReceiver (&argc, &argv, ttname);
  Semaphore *rcvrThreadSem = rcvrthread->getSemaphore();
  rcvrthread->setConsumerSemaphore(controllerRX);
  
  rferr << "tesstooldriver::before Start" << endl;
  cerr << "tesstooldriver::before Start" << endl;
  rcvrthread->start();
  rferr << "tesstooldriver::after Start" << endl;
  cerr << "tesstooldriver::after Start" << endl;
  
  CliCommandThread *clithread = new CliCommandThread(hname, port);
  
  CliOutputReceiveThread *clilogger = clithread->getConsoleLogger();
  if (clilogger == NULL) {
    cerr << "tesstooldriver::CliOutputReceiveThread NULL" << endl;
    exit(101);
  }
  ConsoleLogThread *consoleLog = new ConsoleLogThread(clilogger);
  consoleLog->start();
  clilogger->start();
  
  Semaphore *clisem = clithread->getSemaphore();
  clithread->setControllerSemaphore(controllerCLI);
  
  // read scriptfile and send to cli
  clithread->sendScript(scriptfile);
  
  cerr << "tesstooldriver:: main sent script" << endl;
  cerr << "tesstooldriver:: entering wait for ttr accept" << endl;
  controllerRX->wait();
  cerr << "tesstooldriver:: posting for ttr accept" << endl;
  rcvrThreadSem->post();
  cerr << "tesstooldriver:: waiting for ttr accept to complete" << endl;
  controllerRX->wait();
  
  
  // start cli command loop
  cerr << "tesstooldriver:: main starting CliCommandThread Run" << endl;
  clithread->start();
  cerr << "tesstooldriver:: main starting CliCommandThread After" << endl;
  
  // initialize gtk, vtk
  cerr << "tesstooldriver:: init VTK" << endl;
  TessToolVTK *vtkThread = new TessToolVTK();
  vtkThread->setProducerSemaphore(controllerVTK);
  vtkThread->start();
  
  
  cerr << "tesstooldriver:: main entering sampling loop" << endl;
  int sampleInterval=30;

  while (TESSTOOL::work) {

    cerr << "tesstooldriver:: Epoch num = " << epochNum << endl;
    
    clithread->sampleNext();
    clisem->post();
    
    controllerCLI->wait();
    rcvrthread->SynchronizeCommand();
    rcvrThreadSem->post();
    
    controllerRX->wait();
    RecordInputStream_MPI *mpistrm = 
      new RecordInputStream_MPI(rcvrthread->GetRecordType(), rcvrthread);

    // sender has written tessellation to disk
    //
    if (!vtkThread->isTessellationInitialized()) {
      vtkThread->getSemaphore()->post();
    }
    
    vtkThread->SetSourceStream(mpistrm);
    vtkThread->getSemaphore()->post();
    
    sleep(sampleInterval);
    epochNum++;
  }
  
  cerr << "tesstooldriver:: main exiting sampling loop" << endl;

  vtkThread->suspend();
  clilogger->suspend();
  consoleLog->suspend();
  rcvrthread->suspend();
  
  while (TESSTOOL::work) {sleep(1);}
  
}

void guiDriver(int argc, char *argv[])
{
  Semaphore _mysem(0);
  cerr << "====================" << endl;
  cerr << "= Work in Progress =" << endl;
  cerr << "====================" << endl;
  TessToolController * controller;
  controller = new TessToolController(argc, argv, &_mysem);
  controller->start();
  cerr << "Started Controller Thread " << endl;

  _mysem.wait();
}

int main(int argc, char *argv[])
{           
  rferr.open("errfile.rx");
  bool isBatch=false;
  
  int num_words=argc, count=1;
  // int rv;
  char *argi;
  while(--num_words>0) {
    cerr << argv[count] << endl;
    argi = argv[count];
    if ( argi[0] == '-' ) {
      switch(argi[1]) {
      case 'b' : 
	{
	  isBatch = true;
	  break;
	}
      case 'g' : 
	{
	  isBatch = false;
	  break;
	}
	
      }
    }
    count++;
  }
  
  // see if we are in batch mode
  if (isBatch) {
    batchDriver(argc, argv);
  } else {
    guiDriver(argc, argv);
  }
  
  rferr.close();

  return 0;
}

