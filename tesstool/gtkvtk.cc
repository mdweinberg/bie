
#include <cstdlib>
#include <cstring>
#include <string>

#include <gtk/gtk.h>

#include <gtk/gtkgl.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include <gdk/gdkx.h>

#include <X11/X.h>
#include <X11/keysym.h>
// #include <X11/Shell.h>

#include <string>
#include <sstream>

#include <gdk/gdkkeysyms.h>
#include "vtkGtkRenderWindowInteractor.h"
#include "interface.h"
#include "debug.h"

using namespace TESSTOOL;

void TESSTOOL::pop_message(const std::string &message) {
  
  GtkWidget *dialog, *label, *okay_button;
  GtkWindow *gtkwin;
  
  dialog = gtk_dialog_new();
  gtkwin = GTK_WINDOW(dialog);
  gtk_window_set_modal(gtkwin, 1);
  
  label = gtk_label_new (message.c_str());
  okay_button = gtk_button_new_with_label("Okay");
  
  gtk_signal_connect_object (GTK_OBJECT (okay_button), "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy), dialog);

  gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->action_area),
		     okay_button);
  
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		     label);

  gtk_widget_show_all (dialog);
}


void TESSTOOL::data_popup(double value, vtkIdType npts, vtkPoints *pts) 
{
  // Create the widgets
   
  GtkWidget *dialog = 
    gtk_dialog_new_with_buttons ("Picked data",
				 NULL,
				 GTK_DIALOG_DESTROY_WITH_PARENT,
				 GTK_STOCK_OK,
				 GTK_RESPONSE_NONE,
				 NULL);

  
  std::ostringstream sout;

  sout << endl;
  sout << TESSTOOL::scalarNameString << "=" << value << endl << endl;
  sout << "Vertices:";
  for (int i=0; i<npts; i++) {
    sout << endl;
    sout << "  [" << i << "]=";
    for (int j=0; j<3; j++) sout << " " << pts->GetPoint(i)[j];
  }
  sout << endl;
  cerr << "Length=" << sout.str().length() << endl;

  g_print(sout.str().c_str());

  GtkWidget *label = gtk_label_new (sout.str().c_str());

  // Ensure that the dialog box is destroyed when the user responds.
   
  g_signal_connect_swapped (dialog,
			    "response", 
			    G_CALLBACK (gtk_widget_destroy),
			    dialog);

  // Add the label, and show everything we've added to the dialog.

  gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		     label);

  gtk_widget_show_all (dialog);
  
}


void TESSTOOL::pop_fieldnames(std::vector<std::string>&nlist)
{
  
  // Create the widgets
   
  GtkWidget *dialog = 
    gtk_dialog_new_with_buttons ("Valid field names",
				 NULL,
				 GTK_DIALOG_DESTROY_WITH_PARENT,
				 GTK_STOCK_OK,
				 GTK_RESPONSE_NONE,
				 NULL);

  
  std::ostringstream sout;

  sout << "Valid field names:" << endl << endl;
  for (unsigned i=0; i<nlist.size(); i++)
    sout << "  [" << i+1 << "]=" << nlist[i] << endl;
  sout << endl;
  
  g_print(sout.str().c_str());

  GtkWidget *label = gtk_label_new (sout.str().c_str());

  // Ensure that the dialog box is destroyed when the user responds.
   
  g_signal_connect_swapped (dialog,
			    "response", 
			    G_CALLBACK (gtk_widget_destroy),
			    dialog);

  // Add the label, and show everything we've added to the dialog.

  gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		     label);

  gtk_widget_show_all (dialog);
  
}


GtkWidget* TESSTOOL::vtk_window;


// Called after Init GTK, gtk_main() called after this
int
TESSTOOL::gtkvtk (int argc, char **argv)
{
  /*
   * Init GtkGLExt.
   */
  
  gtk_gl_init (&argc, &argv);
  
  
  /*
   * Interactor
   */
  iren = vtkGtkRenderWindowInteractor::New();
  cerr << "IREN created!\n";
  
  /*
   * Top-level window.
   */
  vtk_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (vtk_window), "VTK in GTK");
  
   gtk_container_set_resize_mode (GTK_CONTAINER (vtk_window), GTK_RESIZE_IMMEDIATE);
  
  gtk_container_set_reallocate_redraws (GTK_CONTAINER (vtk_window), TRUE);
  
  gtk_widget_add_events(vtk_window, GDK_KEY_PRESS_MASK | GDK_KEY_RELEASE_MASK );
  
  g_signal_connect (G_OBJECT (vtk_window), "delete_event",
		    G_CALLBACK (gtk_main_quit), NULL);
  
  gtk_container_add (GTK_CONTAINER (vtk_window), iren->get_drawing_area());
  
  gtk_widget_set_size_request(iren->get_drawing_area(), 400,400);

  gtk_widget_show (iren->get_drawing_area());
  
  gtk_widget_show (vtk_window);
  
  return 0;
}

