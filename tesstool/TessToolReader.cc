#include <iostream>
#include <sstream>
#include <cstdlib>

#include "gvariable.h"
#include "TessToolReader.h"
#include "RecordStream_Ascii.h"
#include "MPICommunicationSession.h"
#include "bieTags.h"
#include "BasicType.h"
#include "tesstoolMutex.h"

using namespace BIE;
static int counter=0;

namespace TESSTOOL {
  extern bool stream_ok;
}

TessToolReader::TessToolReader() : TessToolDataStream()
{
  _bufSizeInWords = 0;
  filter_recordSize = 0;
  _dtsize = 0;
  _tsem = new Semaphore(0);
  _name = 0;
}


TessToolReader::~TessToolReader()
{
  delete [] _scratchDtypeBuffer;
  delete [] (int *)_currentBuffer;
  delete _iss;
  delete _ris;
  delete [] _name;
}


void TessToolReader::setFile(char* name) 
{
  delete [] _name;
  _name = new char [strlen(name)+1];
  strcpy(_name, name);
}

void TessToolReader::run()
{
  while(1) {
				// sleep on a semaphore
    cerr  << "TessToolReader::run(): starting wait on _tsem=" << _tsem << endl;
    rferr << "TessToolReader::run(): starting wait on _tsem=" << _tsem << endl;
    _tsem->wait();
    cerr  << "TessToolReader::run() returned from wait" << endl;
    rferr << "TessToolReader::run() returned from wait" << endl;

				// Parse the command
    if (parse_command()) break;
  }

}

void TessToolReader::Synchronize()
{
  counter++;
  rferr << "TessToolReader::Synchronize() entering " << counter << endl;
  cerr  << "TessToolReader::Synchronize() entering " << counter << endl;
  rferr << "TessToolReader::Synchronize() file=" << _name << endl;
  cerr  << "TessToolReader::Synchronize() file=" << _name << endl;

				// Open the new file
  _in.close();
  _in.open(_name);
  if (!_in) {
    ostringstream ostr;
    ostr << "cannot open file: " << _name;

    rferr << "TessToolReader::Synchronize(): " << ostr.str() << endl;
    cerr  << "TessToolReader::Synchronize(): " << ostr.str() << endl;

    TESSTOOL::stream_ok = false;

    notifyConsumer();

    return;
  }


  // Read the file header info
  //
  rferr << "TessToolReader::Synchronize() calling Recv for bufSize" << endl;

  int old_bufSizeInWords = _bufSizeInWords;
  int old_filter_recordSize = filter_recordSize;
  int old_dtsize = _dtsize;

  _in.read(reinterpret_cast<char *>(&_bufSizeInWords), sizeof(int));
  _in.read(reinterpret_cast<char *>(&filter_recordSize), sizeof(int));
  _in.read(reinterpret_cast<char *>(&_dtsize), sizeof(int));

  // Create the buffers
  //

  if (old_bufSizeInWords    != _bufSizeInWords    ||
      old_filter_recordSize != filter_recordSize ||
      old_dtsize            != _dtsize             )
    {
      if (!_first) {
	delete [] _scratchDtypeBuffer;
	delete [] (int *)_currentBuffer;
      }

      _scratchBufferSizeInWords = _dtsize;
      _scratchDtypeBuffer = new char [_scratchBufferSizeInWords * sizeof(int)];
      bzero(_scratchDtypeBuffer, _scratchBufferSizeInWords);
      
      _currentBuffer = (void *) new int[_bufSizeInWords];
      _currentBufferEnd = (void *)( (int *)_currentBuffer + _bufSizeInWords );

    }


  // Read the datatype buffer
  //
  _in.read(reinterpret_cast<char *>(_scratchDtypeBuffer), _dtsize);
  

  // Create the data buffer
  //
  _currentBuffer = (void *) new int[_bufSizeInWords];
  _currentBufferEnd = (void *)( (int *)_currentBuffer + _bufSizeInWords );


  // Construct the datatype
  //
  if (!_first) {
    delete _iss;
    delete _ris;
  }

  _str = string(_scratchDtypeBuffer);
  _iss = new istringstream(_str);
  _ris = new RecordInputStream_Ascii (_iss);
  _rtype = _ris->getType();

  rferr << "TessToolReader::Synchronize() numfields= " 
	<< _rtype->numFields() 
	<< " _rtype=" << _rtype->toString() << endl;

  _typeSizeInBytes = MPIStreamFilter::getRecordBufferSizeInBytes(_rtype);
  _numRecordsInBuffer = (_bufSizeInWords *sizeof(int))/_typeSizeInBytes;
  
  rferr << "_bufSizeInWords=" << _bufSizeInWords 
	<< " _typeSizeInBytes=" << _typeSizeInBytes << endl;


  _currentBufferPointer = _currentBufferEnd;

  notifyConsumer();

  rferr << "TessToolReader::Synchronize() exit" << endl << flush;
  cerr  << "TessToolReader::Synchronize() exit" << endl << flush;

  _first = false;
  TESSTOOL::stream_ok = true;
}


void TessToolReader::GetData()
{
  static unsigned long icur=0;
  int numRecordsReceived;
  
  _in.read(reinterpret_cast<char *>(&numRecordsReceived), 
	   sizeof(int));

  if (_in.eof()) {
    _currentBufferPointer = _currentBufferEnd = 0;
    return;
  }

  _in.read(reinterpret_cast<char *>(_currentBuffer), 
	   numRecordsReceived*filter_recordSize);

  _currentBufferPointer = _currentBuffer;

  char *offset = (char *)_currentBuffer + numRecordsReceived*filter_recordSize;

  _currentBufferEnd = (void *)offset;
  
  rferr << "TesstoolReader::GetData() reading=" << icur << ": "
	<< "numRecordsReceived=" << numRecordsReceived 
	<< " _currentBufferPointer=" << _currentBufferPointer
	<< " _currentBufferEnd=" << _currentBufferEnd
	<< endl; 
  
}

