#include "gvariable.h"
#include "CliCommandThread.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <cc++/thread.h>
#include <errno.h>
#include "cli_server.h"
#include "tesstoolMutex.h"
#include <sys/poll.h>

using namespace BIE;
using namespace ost;

CliCommandThread::CliCommandThread(string hostname, int port)
{
  ++tesstoolMutex;
  _hname = (char *) hostname.c_str();
  wrerr << "_hname=" << _hname << endl;
  _port = port;
  _loop = true;

  struct sockaddr_in server_sock_addr, client_sock_addr;

  _socketid = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
  if (_socketid < 0)
  {
    wrerr << "Problem creating listener socket \n";
    std::exit(-1);
  }
  _console_socketid = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
  if (_socketid < 0)
  {
    wrerr << "Problem creating console socket \n";
    std::exit(-1);
  }

  bzero((char *)&client_sock_addr, sizeof(client_sock_addr));
  client_sock_addr.sin_family = AF_INET;
  client_sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  // client_sock_addr.sin_port = htons(MY_PORT_ID);
  client_sock_addr.sin_port = htons(MY_PORT_ID+1);


  wrerr << "client: binding the local listening socket\n";
  if (bind(_socketid,(struct sockaddr *)&client_sock_addr,
	   sizeof(client_sock_addr)) < 0) {
    perror("CliCommandThread::Problem binding listener socket");
    ::close(_socketid);
    _socketid = 0;
    std::exit(-1);
  }

  struct hostent *server=gethostbyname(_hname);

  int *ptr = (int *) server->h_addr_list[0];
  wrerr << "hostent" <<endl;
  wrerr << server->h_name << endl;
  wrerr << server->h_addrtype << endl;
  wrerr << server->h_length << endl;
  wrerr << *(int *) ptr << endl;

  bzero((char *)&server_sock_addr, sizeof(server_sock_addr));
  server_sock_addr.sin_family = AF_INET;
  in_addr_t *sip = (in_addr_t *) server->h_addr_list[0];
  wrerr << "hname=" << _hname << " sip=" << sip << endl; // " h_addr=" << server->h_addr << endl;
  server_sock_addr.sin_addr.s_addr = *sip;
  server_sock_addr.sin_port = htons(_port);

  wrerr <<  "client: starting connect request\n";
  if(connect(_socketid, (struct sockaddr *)&server_sock_addr, sizeof(server_sock_addr)) < 0)
  {
    perror("client: error in connect");
    ::close(_socketid);
    ::close(_console_socketid);
    std::exit(-1);
  }

  if(connect(_console_socketid, (struct sockaddr *)&server_sock_addr, sizeof(server_sock_addr)) < 0)
  {
    wrerr <<  "client: error in connecterrno=" << errno << endl;
    ::close(_socketid);
    ::close(_console_socketid);
    std::exit(-1);
  }
  _consoleLogger = new CliOutputReceiveThread(_socketid, NULL);
  wrerr << "CliCommandThread:: _consoleLogger=" << _consoleLogger << endl;
  // fcntl(_socketid, F_SETFL, O_NONBLOCK);
  // fcntl(_socketid, F_SETFL, FNDELAY);

  _qsem = new Semaphore(0);
  _receiveBufferSize=_sendBufferSize=_cmdBufferSize=1<<12;
  _sendBuffer = new char[_sendBufferSize];
  _receiveBuffer = new char[_receiveBufferSize];
  _clicmd = new char[_cmdBufferSize];
  --tesstoolMutex;
}

CliCommandThread::~CliCommandThread()
{
  delete [] _sendBuffer;
  delete [] _receiveBuffer;
  ::close(_socketid);
}

/* this needs to be made more robust for large replies received from cli */
int CliCommandThread::readReply()
{
  int rv;
  wrerr << "CliCommandThread readReply enter" << endl; 
  bzero(_receiveBuffer, _receiveBufferSize);
  wrerr << "CliCommandThread readReply calling recv" << endl; 
  rv = recv(_socketid, _receiveBuffer, _receiveBufferSize,0);
  if (rv == -1) {
    wrerr << "error receiving data from cli server" << endl;
  }
  wrerr << "CliCommandThread readReply count=" << rv << endl << _receiveBuffer << endl; 
  return rv;
}

/* used by cli console o/p */
int CliCommandThread::readReply(int sockid)
{
  int rv=-2;
  pollfd pfd;
  short rev;
  wrerr << "CliCommandThread readReply(id) enter" << endl; 
  bzero(_receiveBuffer, _receiveBufferSize);
  wrerr << "CliCommandThread readReply(id) calling recv" << endl; 

  pfd.fd = sockid;
  pfd.events=POLLIN;
  rev = poll(&pfd, 1, 5);
  if (rev >0 ) {
    rv = recv(sockid, _receiveBuffer, _receiveBufferSize,0);
  } else {
    wrerr << "poll returned errno " << errno << " error receiving data from cli server" << endl;
    rv = -1;
  }
  if (rv == -1) {
    wrerr << "error receiving data from cli server" << endl;
  }
  wrerr << "CliCommandThread readReply(id) count=" << rv << endl << _receiveBuffer << endl; 
  // fcntl(_socketid, F_SETFL, fl);
  return rv;
}

int CliCommandThread::writeCommand()
{
  int msize = strlen(_sendBuffer)+1;
  int rv=0, count=0;
  char *tmp;
  bool addNewLine=false;
  char newlc = '\n', nullc='\0';

  int breakpt=0;
  while(breakpt) {};

  wrerr << "CliCommandThread::writeCommand() enter send " << _sendBuffer << " size=" << msize << endl;
  // check if the command has newline
  if (_sendBuffer[msize-2] != newlc) {
    if (msize+1 < _sendBufferSize) {
      _sendBuffer[msize-1] = newlc;
      _sendBuffer[msize+1] = nullc;
    } else {
      addNewLine=true;
    }
  }
  wrerr << "CliCommandThread::writeCommand() size= " << msize << " sendbuffer=" << _sendBuffer << endl;
  while(count < msize) {
    tmp = _sendBuffer + count;
    rv = send(_socketid, tmp, msize-count, 0);
    if (rv == -1 ) {
      wrerr << "error sending data to cli server" << endl;
    } else {
      count += rv;
    }
    wrerr << "CliCommandThread::writeCommand(char *) sent "  << " count=" << rv << endl;
  }
  // send the trailing newline
  if (addNewLine) {
    rv = send(_socketid, &newlc, 1, 0);
    if (rv == -1 ) {
      wrerr << "error sending data to cli server" << endl;
    } else {
      count += rv;
    }
  }

  wrerr << "CliCommandThread::writeCommand(char *) exit sent "  << " count=" << msize << endl;
  return rv;
}

int CliCommandThread::writeCommand(const string& msg)
{
  int msize = msg.size()+1;
  int rv, count=0;
  char *tmp;
  bool addNewLine=false;
  char newlc = '\n', nullc='\0';

  int breakpt=0;
  while(breakpt) {};

  wrerr << "CliCommandThread::writeCommand(char *) enter send " << msg << " size=" << msize << endl;
  bzero(_sendBuffer, _sendBufferSize);
  strcpy(_sendBuffer, msg.c_str());
  wrerr << "CliCommandThread::writeCommand(char *) copied into buffer " << _sendBuffer << endl;
  // check if the command has newline
  if (msg[msize-2] != newlc) {
    if (msize < _sendBufferSize) {
      _sendBuffer[msize-1] = newlc;
      _sendBuffer[msize+1] = nullc;
    } else {
      addNewLine=true;
    }
  }
  wrerr << "CliCommandThread::writeCommand(char *) size= " << msize << " sendbuffer=" << _sendBuffer << endl;
  while(count < msize) {
    tmp = _sendBuffer + count;
    rv = send(_socketid, tmp, msize-count, 0);
    if (rv == -1 ) {
      wrerr << "error sending data to cli server" << endl;
    } else {
      count += rv;
    }
    wrerr << "CliCommandThread::writeCommand(char *) sent "  << " count=" << rv << endl;
  }
  // send the trailing newline
  if (addNewLine) {
    rv = send(_socketid, &newlc, 1, 0);
    if (rv == -1 ) {
      wrerr << "error sending data to cli server" << endl;
    } else {
      count += rv;
    }
  }

  wrerr << "CliCommandThread::writeCommand(char *) exit sent "  << " count=" << msize << endl;
  return rv;
}

int CliCommandThread::writeCommand(istream *strm)
{
  
  int rv, msize, count=0;
  char *tmp;
  wrerr << "CliCommandThread::writeCommand(strm) enter" << endl;
  bzero(_sendBuffer, _sendBufferSize);
  /*
  while(1) {
    strm->getline(_sendBuffer, _sendBufferSize);
    if (strm->eof()) break;
    msize = strlen(_sendBuffer);
    wrerr << _sendBuffer << endl;
    count=0;
    while(count < msize) {
      tmp = _sendBuffer + count;
      wrerr << "CliCommandThread::writeCommand " << endl;
      rv = send(_socketid, tmp, msize-count, 0);
      if (rv == -1 ) {
	wrerr << "error sending data to cli server" << endl;
      } else {
	count += rv;
      }
    }
  }
  */
  // get length of file:
  strm->seekg (0, ios::end);
  int length = strm->tellg();
  strm->seekg (0, ios::beg);
  int left= length;

  while(left > 0) {
    // if (strm->eof()) break;
    strm->read(_sendBuffer, _sendBufferSize);
    left -= _sendBufferSize;
    msize = length;
    wrerr << _sendBuffer << endl;
    count=0;
    while(count < msize) {
      tmp = _sendBuffer + count;
      wrerr << "CliCommandThread::writeCommand(strm) " << endl;
      rv = send(_socketid, tmp, msize-count, 0);
      if (rv == -1 ) {
	wrerr << "CliCommandThread::writeCommand(strm) error sending data to cli server" << endl;
      } else {
	count += rv;
      }
    }
  }
 wrerr << "CliCommandThread::writeCommand(strm) return" << endl;
  return length;
}

/*
Semaphore *CliCommandThread::getSemaphore()
{
  return _qsem;
}
*/
void CliCommandThread::run()
{

  while(_loop)
    {
      // wait until we have a command for cli
      wrerr << "CliCommandThread Run starting wait "  << endl;
      rferr << "CliCommandThread Run starting wait " << endl;
      fprintf(stderr,"CliCommandThread Run starting wait\n");
      _qsem->wait();
      wrerr << "CliCommandThread Run returned from  wait" << endl;
      ++tesstoolMutex;
      wrerr << "CliCommandThread Run got Mutex" << endl;

      // check the command buffer
      int w1 = *(int *) _sendBuffer;
      if (w1 == -1) {
	break;
      } else {
	// send the command to cli
	char *cmd=_sendBuffer;
	int rsize;
	if(strncmp(cmd, "SampleNext()", 12)==0) {
	  // retrieve the TessToolSender name
	  wrerr << "CliCommandThread Run received command from driver " << cmd << endl;
	  //	  writeCommand("get TessToolSender\n");
	  //	  writeCommand("get LikelihoodComputation\n");
	  readPrompt();
	  writeCommand("get LikelihoodComputation");
	  while(1) {
	    rsize = readReply(_console_socketid);
	    if(rsize > 0) break;
	  }
	  int ssize = strlen(_receiveBuffer);
	  char *p, *tts = new char [ssize+1];

	  // need to parse and see if it is a single word
	  
	  p = tts;
	  for (int i=0; i<ssize; i++) {
	    if ( 
		(_receiveBuffer[i] >='0' && _receiveBuffer[i] <='9') ||
		(_receiveBuffer[i] >='a' && _receiveBuffer[i] <='z') ||
		(_receiveBuffer[i] >='A' && _receiveBuffer[i] <='Z') ||
		(_receiveBuffer[i] =='_'   )
		)
	      *(p++) = _receiveBuffer[i];
	  }
	  *p = '\0';

	  wrerr << "CliCommandThread Run received ttsender name " << tts << endl;
	  wrerr << "CliCommandThread Run received LikelihoodComputation name " << tts << endl;
	  bzero(_clicmd, sizeof(_clicmd));
	  //	  sprintf(_clicmd, "%s->SampleNext()\n", tts);
	  sprintf(_clicmd, "%s->SampleNext()", tts);
	  wrerr << "CliCommandThread Run sending command to cli " << _clicmd << endl;
	  readPrompt();
	  writeCommand(_clicmd);
	  wrerr << "CliCommandThread Run sent " << _clicmd << endl;
      	} else {
	  // just send the buffer contents
	  readPrompt();
	  writeCommand();
	  // command sender will read reply
	}
	// clear buffer
	rferr << "CliCommandThread sent " << _sendBuffer << endl;
	bzero(_sendBuffer, sizeof(_sendBuffer));
	notifyController();
      }
      --tesstoolMutex;
      wrerr << "CliCommandThread Run released Mutex" << endl;

    }
}

void CliCommandThread::sampleNext()
{
  sprintf(_sendBuffer, "SampleNext()\0");
}

void CliCommandThread::sendCommand(string& cmd)
{
  wrerr << "CliCommandThread::sendCommand " << cmd.c_str() << endl;
  sprintf(_sendBuffer, "%s", cmd.c_str());
}


void CliCommandThread::sendScript(const string& filename){
    // open input file
  wrerr << "CliCommandThread::sendScript " << filename << endl;
    ifstream file(filename.c_str());

    // file opened?
    if (! file) {
        // NO, abort program
        wrerr << "can't open input file \"" << filename << "\""
             << endl;
        std::exit(EXIT_FAILURE);
    }

    ++tesstoolMutex;
    // copy file contents to cli
    int file_length;
    file_length = writeCommand(&file);
    // handle echo. Is there a way to suppress it? set cin.tie to null ?
    wrerr << "file sent size=" << file_length << endl;
    /*
    int echo_length=0;
    wrerr << "echoing file sent start" << endl;
    while( echo_length < file_length) {
      echo_length += readReply();
    }
    wrerr << "echoing file sent end" << endl;
    */
    --tesstoolMutex;

    //    file.close();
    //    delete &file;
}

/* We expect a prompt, \n */
int CliCommandThread::readPrompt()
{
  int size;
  wrerr << "CliCommandThread::readPrompt() enter calling readreply" << endl;
  size = readReply(_console_socketid);
  return size;
}

istream *CliCommandThread::getReply()
{
  int size;
  wrerr << "CliCommandThread::getReply() enter calling readreply" << endl;
  size = readReply();
  istream *strm;
  if (size > 0) {
    strm = new istringstream(_receiveBuffer);
  } else {
    strm = NULL;
  }
  wrerr << "CliCommandThread::getReply() exit" << endl;
  return strm;
}

istream *CliCommandThread::getCommandReply()
{
  int size;
  wrerr << "CliCommandThread::getCommandReply() enter calling readreply" << endl;
  istream *strm;
  while(1) {
    size = readReply(_console_socketid);
    if (size > 0) {
      strm = new istringstream(_receiveBuffer);
      break;
    } else {
      strm = NULL;
    }
  }
  wrerr << "CliCommandThread::getCommandReply() exit" << endl;
  return strm;
}

CliOutputReceiveThread *CliCommandThread::getConsoleLogger() 
{
  wrerr << "CliCommandThread::getConsoleLogger: returning " << _consoleLogger << endl; 
  return _consoleLogger;
};
