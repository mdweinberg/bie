#include <gvariable.h>
#include <TessToolController.h>
#include <TessToolGUI.h>
#include <tesstoolMutex.h>
#include <RecordInputStream_MPI.h>

using namespace BIE;
using namespace TESSTOOL;

namespace TESSTOOL {
  bool stream_ok = true;
}

extern bool TESSTOOL::viewer;

TessToolController* BIE::TessToolController::_guiController = NULL;

TessToolController::TessToolController(int argc, char *argv[], 
				       Semaphore *mainsem)
{
  _argc = argc;
  _argv = argv;
  _stop = false;

				// Use to signal main process to finish
  _mainsem = mainsem;
  
				// Signal me to stop
  _working = new Semaphore(0);

  char *hname = argv[1];
  int port = atoi(argv[2]);
  
				// start BIEInterface thread
  string ttname = string("TessTool");
  
  _controllerVTK = new Semaphore(0);
  if (!viewer) _controllerCLI = new Semaphore(0);
  _controllerRX = new Semaphore(0);
  
  if (viewer)
    _rcvrthread = new TessToolReader ();
  else
    _rcvrthread = new TessToolReceiver (&argc, &argv, ttname);
  
  _rcvrThreadSem = _rcvrthread->getSemaphore();
  _rcvrthread->setConsumerSemaphore(_controllerRX);
  
  rferr << "TessToolController::before DataStream start" << endl;
  _rcvrthread->start();
  rferr << "TessToolController::after DataStream Start" << endl;
  
  if (!viewer) {
    _clithread = new CliCommandThread(hname, port);
    
    _clilogger = _clithread->getConsoleLogger();
    if (_clilogger == NULL) {
      rferr << "TessToolController::CliOutputReceiveThread NULL" << endl;
      std::exit(101);
    }
    _clilogger->start();
    
    _consoleLog = new ConsoleLogThread(_clilogger);
    _consoleLog->setController(this);
    _consoleLog->start();
    
    _clisem = _clithread->getSemaphore();
    _clithread->setControllerSemaphore(_controllerCLI);
  }
  
  _vtkThread = new TessToolVTK();
  
  rferr << "TessToolController: before GUI start" << endl;
  _guiController = this;
  _gui = new TessToolGUI(argc, argv, this);
  _gui->start();
  rferr << "TessToolController: after GUI start" << endl;
  
  _sentInitialScript = false;
  _startedCLICommandLoop = false;
  _startedVTK = false;
  _scriptFileName = 0;
  
  run();
}

TessToolController::~TessToolController()
{
  delete [] _scriptFileName;
}



void TessToolController::run() 
{

  _working->wait();

  // Shut everything down. Hopefully, the destructor for each
  // class will to this cleanly . . .

  rferr << "Deleting _rcvrthread" << endl;
  delete _rcvrthread;

  if (!viewer) {
    rferr << "Deleting _clithread" << endl;
    delete _clithread;
    rferr << "Deleting _clilogger" << endl;
    delete _clilogger;
  }

  rferr << "Deleting _vtkThread" << endl;
  delete _vtkThread;

  rferr << "Deleting _gui" << endl;
  delete _gui;

  ++tesstoolMutex;
  _mainsem->post();
  --tesstoolMutex;
}


bool TessToolController::isTessToolReceiverConnected()
{				// A little bit of runtime nastiness
				// to save a lot of redesign
  if (dynamic_cast<TessToolReceiver*>(_rcvrthread))
    return ((TessToolReceiver*)_rcvrthread)->isConnectedToTessToolSender();
  else
    return false;
}

void TessToolController::sendScriptFile(string scriptfile)
{
  rferr << "tesstoolController:: sending script to " << _clithread << endl;
  _clithread->sendScript(scriptfile);
  
  rferr << "TessToolController:: main sent script" << endl;
  rferr << "TessToolController:: entering wait for ttr accept" << endl;
  _controllerRX->wait();
  rferr << "TessToolController:: posting for ttr accept" << endl;
  ++tesstoolMutex;
  _rcvrThreadSem->post();
  --tesstoolMutex;
  rferr << "TessToolController:: waiting for ttr accept to complete" << endl;
  _controllerRX->wait();
  
  
  rferr << "TessToolController calling CliCommandThread::getReply() " << endl;
  _sentInitialScript = true;
}

void TessToolController::startCLICommandLoop() 
{
  // start cli command loop
  if (!_startedCLICommandLoop) {
    rferr << "TessToolController:: main starting CliCommandThread Run" << endl;
    _clithread->start();
    rferr << "TessToolController:: main starting CliCommandThread After" << endl;
    _startedCLICommandLoop = true;
  }
}

istream *TessToolController::sendCommand(string cmd) 
{
  if (!_startedCLICommandLoop) {
    startCLICommandLoop();
    _startedCLICommandLoop = true;
  }
  rferr << "TessToolController:: sendCommand calling sendCommand()" << endl;
  _clithread->sendCommand(cmd);
  ++tesstoolMutex;
  _clisem->post();
  --tesstoolMutex;
  
  // what about the CLI response? Needs to be sent back to the CLITextView.
  _controllerCLI->wait();
  rferr << "TessToolController:: sendCommand calling getCommandReply()" << endl;
  istream *rstrm = _clithread->getCommandReply();
  return rstrm;
}

void TessToolController::startVTK() 
{
  if (!_startedVTK) {
    rferr << "TessToolController:: init VTK" << endl;
    _vtkThread->setProducerSemaphore(_controllerVTK);
    _vtkThread->start();
    _startedVTK = true;
  }
}

static int epochNum=0;

void TessToolController::sampleNext()
{
  
  if (!viewer) {
    // start cli command loop
    if (!_startedCLICommandLoop) startCLICommandLoop();
    
    _clithread->sampleNext();
    ++tesstoolMutex;
    _clisem->post();
    --tesstoolMutex;
    
    _controllerCLI->wait();
  }
  
  _rcvrthread->SynchronizeCommand();
  ++tesstoolMutex;
  _rcvrThreadSem->post();
  --tesstoolMutex;
  
  _controllerRX->wait();
  
  if (!stream_ok) return;
  
  RecordType *rt = _rcvrthread->GetRecordType();
				// Poll for record type
  while(rt == NULL) {
    sleep(sample1); 
    rt = _rcvrthread->GetRecordType();
  }
  
  vector<string> namelist;
  for (int i=1; i<=rt->numFields(); i++)
    namelist.push_back(rt->getFieldName(i));
  
  TESSTOOL::rebuild_menu(namelist);
}

void TessToolController::visualize()
{
  if (!stream_ok) return;

  if (!_startedVTK) {
    startVTK();
    _startedVTK = true;
  }

  rferr << "TessToolController::visualize(): epoch num = " << epochNum << endl;
  rferr  << "TessToolController::visualize(): epoch num = " << epochNum << endl;

  RecordType *rt = _rcvrthread->GetRecordType();
  while(rt == NULL) {
    sleep(sample1); 
    _rcvrthread->GetRecordType();
  }
  
  RecordInputStream_MPI *mpistrm = new RecordInputStream_MPI(rt, _rcvrthread);
  
  // sender has written tessellation to disk
  //
  if (!_vtkThread->isTessellationInitialized()) {
    ++tesstoolMutex;
    _vtkThread->getSemaphore()->post();
    --tesstoolMutex;
  }
  
  _vtkThread->SetSourceStream(mpistrm);
  ++tesstoolMutex;
  _vtkThread->getSemaphore()->post();
  --tesstoolMutex;

  rferr << "TessToolController::visualize(): exiting" << endl;
  rferr  << "TessToolController::visualize(): exiting" << endl;
}

void TessToolController::updateBIEOutputView(char *buffer)
{
  _gui->updateBIEOutputView(buffer);
}

void TessToolController::updateVTK()
{
  _vtkThread->Render();
}

void TessToolController::setHueLow(float l)
{
  _vtkThread->setHueLow(l);
}

void TessToolController::setHueHigh(float l)
{
  _vtkThread->setHueHigh(l);
}

void TessToolController::setSaturationLow(float l)
{
  _vtkThread->setSaturationLow(l);
}

void TessToolController::setSaturationHigh(float l)
{
  _vtkThread->setSaturationHigh(l);
}

void TessToolController::setValueLow(float l)
{
  _vtkThread->setValueLow(l);
}

void TessToolController::setValueHigh(float l)
{
  _vtkThread->setValueHigh(l);
}

void TessToolController::setAlphaLow(float l)
{
  _vtkThread->setAlphaLow(l);
}

void TessToolController::setAlphaHigh(float l)
{
  _vtkThread->setAlphaHigh(l);
}

void TessToolController::setVTKWindow(int width, int height)
{
  _xwinWidth = width;
  _xwinHeight = height;
}

vtkGtkRenderWindowInteractor 
*TessToolController::getVTKRenderWindowInteractor()
{
  if (!_startedVTK) {
    return NULL;
  } else {
    return _vtkThread->getVTKRenderWindowInteractor();
  }
}

vtkRenderWindow *TessToolController::getVTKRenderWindow()
{
  if (!_startedVTK) {
    return NULL;
  } else {
    return _vtkThread->getVTKRenderWindow();
  }
}

vtkRenderer *TessToolController::getVTKRenderer()
{
  if (!_startedVTK) {
    return NULL;
  } else {
    return _vtkThread->getVTKRenderer();
  }
}

vtkCellPicker *TessToolController::getVTKCellPicker()
{
  if (!_startedVTK) {
    return NULL;
  } else {
    return _vtkThread->getVTKCellPicker();
  }
}


void TessToolController::initVTK()
{
  if (_startedVTK) {
    g_print("VTK already called\n");
  } else {
    startVTK();
    _startedVTK = true;
  }
}

void TessToolController::setScalarName(char *n)
{
  _vtkThread->setScalarName(n);
}

void TessToolController::setScalarInfoName(char *n)
{
  _vtkThread->setScalarInfoName(n);
}

void TessToolController::setScriptFileName(char *fn)
{
  delete [] _scriptFileName;
  _scriptFileName = new char [strlen(fn)+1];
  strcpy(_scriptFileName, fn);
}

char* TessToolController::getScriptFileName()
{
  return _scriptFileName;
}

void TessToolController::setWorkingDirName(char *fn)
{
  _vtkThread->setPersistenceDir(fn);
}

void TessToolController::setDataFileName(char *fn)
{
  if (dynamic_cast<TessToolReader*>(_rcvrthread))
    ((TessToolReader *)_rcvrthread)->setFile(fn);
}

char* TessToolController::getDataFileName()
{
  return _dataFileName;
}

void TessToolController::quitTessTool()
{
  rferr << "TestToolController::quitTessTool enter " << endl;
  ++tesstoolMutex;
  _working->post();
  --tesstoolMutex;
  rferr << "TestToolController::quitTessTool exit" << endl;
}
