#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <gtk/gtksignal.h>
#include <gdk/gdkkeysyms.h>

#include <sstream>

#include "gvariable.h"
#include "vtkGtkRenderWindowInteractor.h"
#include "TessToolGUI.h"
#include "callbacks.h"
#include "interface.h"

using namespace BIE;
using namespace TESSTOOL;
using namespace std;

namespace TESSTOOL {

  void
  on_new1_activate                       (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_open1_activate                      (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {

    GtkWidget *dialog = gtk_file_chooser_dialog_new 
      ("Open File",
       GTK_WINDOW(window1),
       GTK_FILE_CHOOSER_ACTION_OPEN,
       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
       GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
       NULL);

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
      {
	char *filename;

	filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

				// Check to see if file can be opened
	ifstream in(filename);
	
	if (in) {
	  if (viewer) {
	    g_print("Data File Name: %s\n", filename);
	    setDataFileName(filename);
	    gtk_entry_set_text(GTK_ENTRY(DataFileView), filename);
	  }  else {
	    g_print("Script Name: %s\n", filename);
	    setScriptFileName(filename);
	    gtk_entry_set_text(GTK_ENTRY(CLIScriptView), filename);
	  }
	} else {
	  
	  char message[] = "Enter new file name";

	  if (viewer) {
	    gtk_entry_set_text(GTK_ENTRY(DataFileView), message);
	  }  else {
	    gtk_entry_set_text(GTK_ENTRY(CLIScriptView), message);
	  }
	}

	g_free (filename);
      }

    gtk_widget_destroy (dialog);

  }
  
  
  void
  on_open2_activate                      (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {

    GtkWidget *dialog = gtk_file_chooser_dialog_new 
      ("Open Directory",
       GTK_WINDOW(window1),
       GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
       GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
       NULL);

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
      {
	char *filename;

	filename = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (dialog));

				// Check to see if file can be opened
	ifstream in(filename);
	
	if (in) {
	  g_print("Directory: %s\n", filename);
	  setWorkingDirName(filename);
	  gtk_entry_set_text(GTK_ENTRY(WorkingDirView), filename);
	} else {
	  
	  char message[] = "Enter new directory";

	  gtk_entry_set_text(GTK_ENTRY(WorkingDirView), message);
	}

	g_free (filename);
      }

    gtk_widget_destroy (dialog);

  }
  
  
  void
  on_save1_activate                      (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void store_filename (GtkWidget *widget, gpointer user_data)
  {
    GtkWidget *file_selector = GTK_WIDGET (user_data);
    const gchar *selected_filename 
      = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector));
    g_print ("Selected filename: %s\n", selected_filename);
    
    // find start and end of text
    GtkTextBuffer *buffer = 
      gtk_text_view_get_buffer(GTK_TEXT_VIEW(BIEOutputView));
	
    GtkTextIter start, end;
    gtk_text_buffer_get_bounds(buffer, &start, &end);
	
    // copy all text from screen to temp_buffer
    gchar *temp_buffer = gtk_text_buffer_get_slice
      ( GTK_TEXT_BUFFER(buffer), &start, &end, TRUE);

    // Write the temp buffer to a file
    ofstream ofile(selected_filename);
    if (ofile) {
      ofile << temp_buffer;
    }

    // free temp, close file and return
    g_free (temp_buffer);
    // g_free (selected_filename);
  }


  void
  on_save_as1_activate(GtkMenuItem *menuitem, gpointer user_data)
  {
    // Create the selector
   
    GtkWidget *file_selector = 
      gtk_file_selection_new ("Please select a file for output.");
  
    g_signal_connect (GTK_FILE_SELECTION (file_selector)->ok_button,
		      "clicked",
		      G_CALLBACK (store_filename),
		      file_selector);
   			   
    // Ensure that the dialog box is destroyed when the user clicks a button.
   
    g_signal_connect_swapped (GTK_FILE_SELECTION (file_selector)->ok_button,
			      "clicked",
			      G_CALLBACK (gtk_widget_destroy), 
			    file_selector);
    
    g_signal_connect_swapped (GTK_FILE_SELECTION (file_selector)->cancel_button,
			      "clicked",
			      G_CALLBACK (gtk_widget_destroy),
			      file_selector); 
   
    gtk_widget_show (file_selector);
  }

  
  void
  on_quit1_activate                      (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    g_print("Stop VTK fn\n");
    quitTessTool();
  }
  
  
  void
  on_cut1_activate                       (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_copy1_activate                      (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_paste1_activate                     (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_delete1_activate                    (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_about1_activate                     (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  gboolean
  hue_low                                (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    float hlow;
    g_print("Hue Low=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    hlow = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setHueLow(hlow);
    return FALSE;
  }
  
  
  gboolean
  hue_high                               (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    
    float hhigh;
    g_print("Hue High=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    hhigh = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setHueLow(hhigh);
    return FALSE;
  }
  
  
  gboolean
  saturation_low                         (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    float slow;
    g_print("Saturation Low=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    slow = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setSaturationLow(slow);
    return FALSE;
  }
  
  
  gboolean
  saturation_high                        (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    float shigh;
    g_print("Saturation High=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    shigh = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setSaturationLow(shigh);
    return FALSE;
  }
  
  
  gboolean
  alpha_low                              (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    float alow;
    g_print("Alpha Low=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    alow = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setAlphaLow(alow);
    return FALSE;
  }
  
  
  gboolean
  alpha_high                             (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    float ahigh;
    g_print("Alpha High=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    ahigh = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setAlphaLow(ahigh);
    return FALSE;
  }
  
  
  gboolean
  value_low                              (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    float vlow;
    g_print("Value Low=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    vlow = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setValueLow(vlow);
    return FALSE;
  }
  
  
  gboolean
  value_high                             (GtkWidget       *widget,
					  GdkEventFocus   *event,
					  gpointer         user_data)
  {
    float vhigh;
    g_print("Value High=%s\n", gtk_entry_get_text(GTK_ENTRY(widget)));
    vhigh = atof(gtk_entry_get_text(GTK_ENTRY(widget)));
    setValueLow(vhigh);
    return FALSE;
  }
  
  void
  call_vtk                               (GtkButton       *button,
					  gpointer         user_data)
  {
    g_print("Call VTK fn\n");
    visualize();
  }
  
  
  void
  stop_vtk                               (GtkButton       *button,
					  gpointer         user_data)
  {
    g_print("Stop VTK fn\n");
    quitTessTool();
  }
  
  
  void
  display_mpi_nodes                      (GtkButton       *button,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  request_data_from_mpi                  (GtkButton       *button,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  refresh_vtk                            (GtkButton       *button,
					  gpointer         user_data)
  {
    updateVTK();
  }
  
  gboolean 
  working_dir_changed  (GtkWidget *w, GdkEventKey *key, gpointer data)
  {
    GtkEditable *editable = GTK_EDITABLE(w);
    char *dirname = (char *)gtk_entry_get_text(GTK_ENTRY(editable));

    ifstream in(dirname);
    if (in) {
      g_print("Working directory name: %s\n", dirname);
      setWorkingDirName(dirname);
    } else {
      gtk_entry_set_text(GTK_ENTRY(WorkingDirView), "Enter new working dir name");
    }
    return FALSE;
  }
  
  
  gboolean 
  data_file_changed  (GtkWidget *w, GdkEventKey *key, gpointer data)
  {
    GtkEditable *editable = GTK_EDITABLE(w);
    char *filename = (char *)gtk_entry_get_text(GTK_ENTRY(editable));

    ifstream in(filename);
    if (in) {
      g_print("Data file name: %s\n", filename);
      setDataFileName(filename);
    } else {
      gtk_entry_set_text(GTK_ENTRY(DataFileView), "Enter new file name");
    }
    return FALSE;
  }
  
  
  gboolean 
  cli_script_changed  (GtkWidget *w, GdkEventKey *key, gpointer data)
  {
    GtkEditable *editable = GTK_EDITABLE(w);
    char *filename = (char *)gtk_entry_get_text(GTK_ENTRY(editable));

				// Check to see if the file can be opened
    ifstream in(filename);
    if (in) {
      g_print("Script Name: %s\n", filename);
      setScriptFileName(filename);
    } else {
      gtk_entry_set_text(GTK_ENTRY(CLIScriptView), "Enter new file name");
    }

    return FALSE;
  }
  
  
  static int prevLineNum=0;
  
  gboolean
  cli_command_input (GtkWidget *w, GdkEventKey *key, gpointer data)
  {
    rferr << "In cli_command_input!\n";

    GtkTextBuffer *buffer;
    GtkTextIter start, end;
    gchar *gcmd=NULL;
    int breakpt=0;
    int lineNum=0;
    int numReplyLines, replySize;
    char *replyBuffer;
    GtkTextView *textview = GTK_TEXT_VIEW (w);
    
    switch(key->keyval)
      {
      case GDK_Return : // case for detection of enter key 
	break;
      default:
	return FALSE;
      }
    
    g_print("cli_command_input: Command \n");
    
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
    if ( buffer == NULL ) return FALSE;
    lineNum = gtk_text_buffer_get_line_count(buffer);
    
    g_print("cli_command_input: prevLineNum=%d currLineNum=%d \n", prevLineNum, lineNum);
    if ( lineNum <= prevLineNum ) return FALSE;
    
    while(breakpt) {}
    //  gtk_text_buffer_get_iter_at_line_offset(buffer, &start, prevLineNum, 0);
    //  gtk_text_buffer_get_iter_at_line_offset(buffer, &end, prevLineNum+1, 0);
    gtk_text_buffer_get_iter_at_line_offset(buffer, &start, lineNum, 0);
    gtk_text_buffer_get_end_iter(buffer, &end);
    
    gcmd = gtk_text_buffer_get_text (buffer, &start, &end, 1);
    
    g_print("cli_command_input: gcmd=%s \n", gcmd);
    
    // add this into the textbuffer and set the number of lines appropriately
    replyBuffer = sendCommand(gcmd, &replySize, &numReplyLines);
    gtk_text_buffer_insert(buffer, &end, "\r", 1);
    //  gtk_text_buffer_insert(buffer, &end, replyBuffer, replySize);
    gtk_text_buffer_insert_at_cursor(buffer, replyBuffer, replySize);
    prevLineNum = gtk_text_buffer_get_line_count(buffer);
    return FALSE;
  }
  
  void
  update_bie(char *txt)
  {
    cerr << "In update_bie!\n";

    int replySize;
    GtkTextBuffer *buffer;
    GtkTextView *textview;
    char *replyBuffer=txt;
    
    textview = GTK_TEXT_VIEW (BIEOutputView);

    rferr << "update_bie_output(): txt=" << txt << endl;
    rferr << "update_bie_output(): textview=" << textview << endl;
    rferr << "update_bie_output(): iren=" << iren << endl;

    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
    replySize=strlen(txt);
    gtk_text_buffer_insert_at_cursor(buffer, replyBuffer, replySize);
    gtk_widget_queue_draw(GTK_WIDGET(BIEOutputView));
    /*
    while (gtk_events_pending ()) { 
      g_print("."); 
      gtk_main_iteration (); 
    } 
    */
    free(txt);
  }
  
  gboolean
  bie_output_event                       (GtkWidget       *widget,
					  GdkEvent        *event,
					  gpointer         user_data)
  {
    
    cerr << "In bie_output_event!\n";
    rferr << "In bie_output_event!\n";

    GtkTextBuffer *buffer;
    GtkTextView *textview = GTK_TEXT_VIEW (widget);
    int replySize;
    char *replyBuffer;
    
    g_print("bie_output_event: Command \n");
    // replySize=strlen(txt);
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
    gtk_text_buffer_insert_at_cursor(buffer, replyBuffer, replySize);
    return FALSE;
  }
  
  
  void
  help_about_galaxy                      (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_maximum_of_difference1_activate     (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_log_of_maximum_difference1_activate (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  display_mpi_node_status                (GtkButton       *button,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  GetData                                (GtkButton       *button,
					  gpointer         user_data)
  {
    sampleNext();
  }
  
  
  void
  ProcessData                            (GtkButton       *button,
					  gpointer         user_data)
  {
    processData();
  }
  
  
  void
  SetScalars                             (GtkButton       *button,
					  gpointer         user_data)
  {
    istringstream minS(gtk_entry_get_text(GTK_ENTRY(scalarMinEntry)));
    istringstream maxS(gtk_entry_get_text(GTK_ENTRY(scalarMaxEntry)));

    minS >> min_scalar;
    maxS >> max_scalar;

    cout << "SetScalars callback: min=" << TESSTOOL::min_scalar
	 << " max=" << TESSTOOL::max_scalar << endl;

    updateVTK();
  }
  
  
  void
  on_maximum_difference_between_model_and_data1_activate
  (GtkMenuItem     *menuitem,
   gpointer         user_data)
  {
    
  }
  
  
  void
  on_sum_of_differences1_activate        (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  maxDiffModelDataActivate               (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  logMaxDiffActivate                     (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  sumOfDifferencesActivate               (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  gboolean
  cliCommands_event                      (GtkWidget       *widget,
					  GdkEvent        *event,
					  gpointer         user_data)
  {
    
    return FALSE;
  }
  
  
  void
  ModelValueActivate                     (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  DataValueActivate                      (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  DifferencesBetweenModelAndDataActivate (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  nullOperatorActivate                   (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  sumOperatorActivate                    (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  differenceOperatorActivate             (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  minimumOperatorActivate                (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  maximumOperatorActivate                (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  logOperatorActivate                    (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    
  }
  
  /* Hand Added */
  void
  CLIScriptSend                           (GtkButton       *button,
					   gpointer         user_data)
  {
    
    char *filename = getScriptFileName();
    rferr << "CLIScriptSend: about to send" << endl << flush;
    sendScriptFile(filename);
    rferr << "CLIScriptSend: send completed" << endl << flush;
  }
  
  void
  on_ConnectVTKButton_clicked            (GtkButton       *button,
					  gpointer         user_data)
  {
    g_print("Init VTK Called\n");
    initVTK();
  }
  
  void
  on_scalar_menu                         (GtkMenuItem     *menuitem,
					  gpointer         user_data)
  {
    char *n = (char *)user_data;
    g_print("Scalar Name=%s\n", n);
    setScalarName(n);
  }
  
  
  void
  on_SendCLIScript_activate              (GtkButton       *button,
					  gpointer         user_data)
  {
    
  }
  
  
  void
  on_scalarByName_changed                (GtkEditable     *editable,
					  gpointer         user_data)
  {     
    char *n = (char *)gtk_entry_get_text(GTK_ENTRY(editable));
    g_print("Scalar Name=%s\n", n);
    setScalarName(n);
    
  }
  
  
  void
  on_scalarInfoByName_changed                      (GtkEditable     *editable,
						    gpointer         user_data)
  {
    
    char *n = (char *)gtk_entry_get_text(GTK_ENTRY(editable));
    g_print("Scalar Info Name=%s\n", n);
    setScalarInfoName(n);
    //  return FALSE;
  }
  
}


