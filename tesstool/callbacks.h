#ifndef _CALLBACKS_H
#define _CALLBACKS_H

#include <gtk/gtk.h>

namespace TESSTOOL {
  
  void on_new1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_open1_activate (GtkMenuItem *menuitem, gpointer user_data);

  void on_open2_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_save1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_save_as1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_quit1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_cut1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_copy1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_paste1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_delete1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_about1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  gboolean hue_low (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean hue_high (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean saturation_low (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean saturation_high (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean alpha_low (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean alpha_high (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean value_low (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean value_high (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  void call_vtk (GtkButton *button, gpointer user_data);
  
  void stop_vtk (GtkButton *button, gpointer user_data);
  
  void display_mpi_nodes (GtkButton *button, gpointer user_data);
  
  void request_data_from_mpi (GtkButton *button, gpointer user_data);
  
  void refresh_vtk (GtkButton *button, gpointer user_data);
  
  gboolean working_dir_changed (GtkWidget *w, GdkEventKey *key, gpointer data);

  gboolean data_file_changed (GtkWidget *w, GdkEventKey *key, gpointer data);

  gboolean cli_script_changed (GtkWidget *w, GdkEventKey *key, gpointer data);
  
  gboolean cli_command_input (GtkWidget *w, GdkEventKey *key, gpointer data);
  
  gboolean bie_output_event (GtkWidget *widget, GdkEvent *event, gpointer user_data);
  
  void help_about_galaxy (GtkMenuItem *menuitem, gpointer user_data);
  
  gboolean value_low (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean value_high (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean alpha_low (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  gboolean alpha_high (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
  
  void on_maximum_of_difference1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_log_of_maximum_difference1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void display_mpi_node_status (GtkButton *button, gpointer user_data);
  
  void ProcessData (GtkButton *button, gpointer user_data);

  void GetData (GtkButton *button, gpointer user_data);
  
  void SetScalars (GtkButton *button, gpointer user_data);
  
  void on_maximum_difference_between_model_and_data1_activate
    (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_log_of_maximum_difference1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_sum_of_differences1_activate (GtkMenuItem *menuitem, gpointer user_data);
  
  void maxDiffModelDataActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void logMaxDiffActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void sumOfDifferencesActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  gboolean cliCommands_event (GtkWidget *widget, GdkEvent *event, gpointer user_data);
  
  void ModelValueActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void DataValueActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void DifferencesBetweenModelAndDataActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void nullOperatorActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void sumOperatorActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void differenceOperatorActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void minimumOperatorActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void maximumOperatorActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void logOperatorActivate (GtkMenuItem *menuitem, gpointer user_data);
  
  void CLIScriptSend (GtkButton *button, gpointer user_data);
  
  void CLIScriptName (GtkButton *button, gpointer user_data);
  
  void on_ConnectVTKButton_clicked (GtkButton *button, gpointer user_data);
  
  void on_scalar_menu (GtkMenuItem *menuitem, gpointer user_data);
  
  void on_scalarByName_changed (GtkEditable *editable, gpointer user_data);
  
  void on_scalarInfoByName_changed (GtkEditable *editable, gpointer user_data);
  
  void update_bie(char *txt);
}

#endif
