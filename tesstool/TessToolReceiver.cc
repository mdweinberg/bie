#include <iostream>
#include <sstream>
#include <cstdlib>

#include "gvariable.h"
#include "TessToolReceiver.h"
#include "RecordStream_Ascii.h"
#include "MPICommunicationSession.h"
#include "bieTags.h"
#include "BasicType.h"
#include "tesstoolMutex.h"

using namespace BIE;
static int counter=0;

TessToolReceiver::TessToolReceiver(int *argc, char ***argv, string name)
  : TessToolDataStream()
{
  _name = (char *) name.c_str();
  _port = new char[MPI_MAX_PORT_NAME];

  _argc=argc;
  _argv=argv;

  _receivedBuffer=0;
  _bufSizeInWords=0;
  _numTMs=0;
  _rtype = 0;
  _scratchBufferSizeInWords = (1<<10);
  _scratchDtypeBuffer = new char [_scratchBufferSizeInWords * sizeof(int)];
  _tsem = new Semaphore(0);
  _mpidtype = 0;
}


TessToolReceiver::~TessToolReceiver()
{
  /*
  MPI_Unpublish_name(_name, MPI_INFO_NULL, _port); 
  err = MPI_Finalize();
  */
  delete [] _port;
  delete [] _request;
  delete [] _status;
  delete [] _flag;
  delete [] _completed;
  delete [] _scratchDtypeBuffer;
  // the receive buffers
  for(int i=0; i<_numTMs; i++) {
    delete [] (int *)_receivedBuffer[i];
  }
  delete [] (int *)_currentBuffer;
}

void TessToolReceiver::Detach()
{
  // cancel any pending recvs
  // disconnect communicator
}

void TessToolReceiver::run()
{
  int tessSize, err;
  int myRank, size, rv;

  counter++;
  rferr << "TessToolReceiver::Entering " << counter << endl << flush;

  ++tesstoolMutex;
  err = MPI_Init(_argc, _argv);

  MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
           
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank); // Get rank
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  rferr << "TessToolReceiver::myRank=" << myRank << endl << flush;
  --tesstoolMutex;

  if ( myRank == 0 ) {
    ++tesstoolMutex;
    MPI_Open_port(MPI_INFO_NULL, _port); 
    rferr << "TessToolReceiver::port name is: " << _port << endl << flush;

    MPI_Publish_name(_name, MPI_INFO_NULL, _port); 
    MPI_Comm_size(MPI_COMM_WORLD, &tessSize);

    rferr << "TesstoolReceiver::TessToolName=" << _name << endl << flush ;
    rferr << "TesstoolReceiver::TessToolSize=" << tessSize << endl << flush ;

    --tesstoolMutex;
    notifyConsumer();
    rferr << "TessToolReceiver::Entering wait" << endl;
    _tsem->wait();
    rferr << "TessToolReceiver::Entering accept" << endl;
    rv = MPI_Comm_accept(_port, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &_bieComm); 
    ++tesstoolMutex;

    rferr << "TesstoolReceiver:: accept rv=" << rv << endl << flush ;

    MPI_Comm_remote_size(_bieComm, &_bieSize);

    rferr << "TesstoolReceiver:: BIE size="<<  _bieSize << endl << flush ;

    rferr << "TesstoolReceiver::Finished accepts" << endl << flush ;

    --tesstoolMutex;
    notifyConsumer();
    _connectedToTessToolSender = true;

    // this will wait on a semaphore, posted by the gui
    // util someone wakes us up to do something
    while(1) {
				// sleep on a semaphore
      rferr << "TessToolReceiver Run starting wait on _tsem=" << _tsem << endl;
      _tsem->wait();
      rferr << "TessToolReceiver Run returned from wait" << endl;

				// Parse the command
      if (parse_command()) break;
    }

    MPI_Unpublish_name(_name, MPI_INFO_NULL, _port); 

  } else {
       rferr << "TesstoolReceiver::Rank(" <<  myRank << "):: \n" ;
  }       
  ++tesstoolMutex;
  err = MPI_Finalize();
  --tesstoolMutex;

}

void TessToolReceiver::Synchronize()
{
  // delete the structs from previous iteration
  int i;
  int old_bufSizeInWords, old_numTMs;
  old_bufSizeInWords = _bufSizeInWords;
  old_numTMs = _numTMs;
  RecordType *old_rtype = _rtype;
  MPI_Status status;

  rferr << "TessToolReceiver::Synchronize() enter" << endl << flush;

  // request other processes to sample computation 
  //
  MPI_Recv((void *)&_level,  1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&_step,   1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&_iter,   1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&_numTMs, 1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  rferr << "TessToolReceiver::Synchronize() numTMs=" << _numTMs << endl;
  if (!_numTMs > 0) {
    rferr << "Received number of tilemasters ! >0 " << endl;
  }

  _tmList = new int [_numTMs];
  for(i=0; i<_numTMs; i++) {
    rferr << " tmList[" << i << "]= " << _tmList[i] << endl << flush;
  }
  int count=_numTMs;
  rferr << "TessToolReceiver::Synchronize() calling Recv for tmList" << endl;
  MPI_Recv((void *)_tmList, count, MPI_INT, 0,BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  rferr << "TessToolReceiver::Synchronize() returned from Recv" << endl;

  for(i=0; i<_numTMs; i++) {
    rferr << " tmList[" << i << "]= " << _tmList[i] << endl << flush;
  }


  rferr << "TessToolReceiver::Synchronize() calling Recv for bufSize" << endl;

  MPI_Recv((void *)&filter_sessionId, 1, MPI_INT, 0,BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&filter_capacityInRecords, 1, MPI_INT, 0,BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&filter_recordSize, 1, MPI_INT, 0,BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  _bufSizeInWords = filter_capacityInRecords * filter_recordSize / sizeof(int);
  //
  rferr << "TessToolReceiver::Synchronize() bufSizeInWords=" << _bufSizeInWords << endl;

  if (_numTMs > old_numTMs ) {
    if (!_first) {
      delete [] _request;
      delete [] _status;
      delete [] _flag;
      delete [] _completed;
    }
    _request = new MPI_Request [_numTMs];
    _status = new MPI_Status [_numTMs];
    _flag = new int [_numTMs];
    _completed = new int [_numTMs];
  }

  // if the number of TMs has changed, or buffer size is different
  if(_first || _numTMs > old_numTMs || _bufSizeInWords > old_bufSizeInWords) {
    rferr << "Allocate buffers" << endl;

				// delete previous buffers ... 
    if ( !_first ) {
      for(i=0; i<old_numTMs; i++) {
	delete [] (int *) _receivedBuffer[i];
      }
      delete [] (int *) _currentBuffer;
    }
    _receivedBuffer = new void* [_numTMs];

				// allocate new buffers
    for(i=0; i<_numTMs; i++) {
      _receivedBuffer[i] = (void *) new int[_bufSizeInWords];
    }   
    _currentBuffer = (void *) new int[_bufSizeInWords];
    _currentBufferEnd = (void *)( (int *)_currentBuffer + _bufSizeInWords );
  }

				// Get datatype 
  int dtsize;
  MPI_Recv(&dtsize, 1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  rferr << "TessToolReceiver::Synchronize() dtsize=" << dtsize << endl;

  if(_scratchBufferSizeInWords < dtsize) {
    delete [] _scratchDtypeBuffer;
    _scratchBufferSizeInWords = dtsize;
    _scratchDtypeBuffer = new char [_scratchBufferSizeInWords * sizeof(int)];
  } 
  bzero(_scratchDtypeBuffer, _scratchBufferSizeInWords);
  MPI_Recv(_scratchDtypeBuffer, dtsize, MPI_BYTE, 0, BIE_DATATYPE_TAG, _bieComm, &status);
  rferr << "TessToolReceiver::Synchronize() dtype" << _scratchDtypeBuffer << endl;
				// construct datatype
				// receive
  if (!_first) {
    delete _ris;
    delete _iss;
  }

  _str = string(_scratchDtypeBuffer);
  _iss = new istringstream(_str);
  _ris = new RecordInputStream_Ascii (_iss);
  _rtype = _ris->getType();

  rferr << "TessToolReceiver::Synchronize(): numfields= " << endl
	<< _rtype->numFields() << endl
	<< " _rtype=" << _rtype->toString() << endl;

  _typeSizeInBytes = MPIStreamFilter::getRecordBufferSizeInBytes(_rtype);
  _numRecordsInBuffer = (_bufSizeInWords *sizeof(int))/_typeSizeInBytes;
  
  rferr << "TessToolReceiver::Synchronize(): _bufSizeInWords=" 
	<< _bufSizeInWords 
	<< " _typeSizeInBytes=" << _typeSizeInBytes << endl;

  if (_first) rferr << "This is the *first* time through" << endl;
  else rferr << "This is *NOT* the first time through, _mpidtype="
	     << _mpidtype << endl;

  if(_first || !_rtype->equals(old_rtype)) {
    rferr << "Calling convertRecordToDatatype" << endl;
    _mpidtype = MPICommunicationSession::convertRecordToDatatype(_rtype);
    rferr << "returned from convertRecordToDatatype " << _mpidtype << endl;
  }

				// check completion of sends
  _numCompleted=0;
  for(i=0; i<_numTMs; i++) {
    _completed[i] = 0;
    rferr << "setting completed " << i << endl << flush;
  }   
  
				// poll nodes to pull data
  for(i=0; i<_numTMs; i++) {
    rferr << "settingup Irecv for tm " << i << endl;
    rferr << &_receivedBuffer[i] << " " << _numRecordsInBuffer 
	  << " " << _mpidtype << " " 
	  <<  _tmList[i] << " "  << _bieComm << " " <<  &_request[i] << endl;

    MPI_Irecv(_receivedBuffer[i], _numRecordsInBuffer, _mpidtype, _tmList[i], 
	      BIE_DATA_TAG, _bieComm, &_request[i]);         
    rferr << "TesstoolReceiver::Irecv started from " << _tmList[i] 
	  << " :: to :: 0" << endl << flush;
  }
  _currentBufferPointer = _currentBufferEnd;
  notifyConsumer();

  rferr << "TessToolReceiver::Synchronize() exit" << endl << flush;

  _first = false;
}

void TessToolReceiver::GetData()
{
  static unsigned long icur=0;
  int i;
  
  rferr << "TesstoolReceiver::GetData _numCompleted=" << _numCompleted 
	<< " _numTMs=" << _numTMs << endl << flush;

  // pull data from the node
  //
  while(_numCompleted < _numTMs) {

    i = (icur++ % _numTMs);

    if (_completed[i]) continue;
      
    rferr << "TesstoolReceiver::GetData testing for data on=" << i
	  << " . . . " << flush;

    MPI_Test( &_request[i], &_flag[i], &_status[i] );

    if (_flag[i])
      rferr << "got some!" << endl << flush;
    else
      rferr << "no data." << endl << flush;

    if (_flag[i]) {		// Receive is completed . .  
				// How many did we get?
      int numRecordsReceived;
      MPI_Get_count( &_status[i], _mpidtype, &numRecordsReceived);

      rferr << "TesstoolReceiver::GetData i=" << i << " got "
	    << numRecordsReceived << " records" << endl << flush;
      
      // At end of stream check?
      if (numRecordsReceived==1) {
	bool atEnd = true;
	
	for (int j=0; j<filter_recordSize; j++)
	  if (static_cast<char*>( _receivedBuffer[i])[j] 
	      != BIE_ENDOFDATA_MARKER) atEnd = false;

	if (atEnd) {
	  _completed[i] = 1;
	  _numCompleted++;
	    
	  rferr <<"TesstoolReceiver::Recv complete from " 
		<< _tmList[i] << ":: to: " << 0 
		<< " tag:" <<  BIE_DATATYPE_TAG <<endl << flush;
	}
      }

      if (!_completed[i]) {	// Got some good data:
	  
				// Copy data to current buffer
	memcpy(_currentBuffer, _receivedBuffer[i], 
	       numRecordsReceived*filter_recordSize);

				// Set pointers
	_currentBufferPointer = _currentBuffer;

	_currentBufferEnd = (void *)((char *)_currentBuffer + 
				     numRecordsReceived*filter_recordSize);

	rferr << "numRecordsReceived=" << numRecordsReceived 
	      << " _currentBufferPointer=" << _currentBufferPointer
	      << " _currentBufferEnd=" << _currentBufferEnd
	      << endl; 

	// Get more data on this channel
	MPI_Irecv(_receivedBuffer[i], _numRecordsInBuffer, 
		  _mpidtype, _tmList[i], 
		  BIE_DATA_TAG, _bieComm, &_request[i]);         
	
	return;
      }

    }
    
  }   
				// No more data on any channel

  _currentBufferPointer = _currentBufferEnd = 0;

}

