#include <iostream>
#include <sstream>
#include <cstdlib>

#include "TessToolWriter.h"

#include <fstream>

using namespace std;
using namespace BIE;

namespace TESSTOOL 
{
  bool work = true;
  bool viewer = true;
}

ofstream wrerr, rferr;

int main(int argc, char *argv[])
{           
  // int counter = 0;

  wrerr.open("errfile.wx");
  
  string tag("ttcache");
  if (argc>1) tag = argv[1];

  wrerr << "tesstoolwriter::tag=" << tag << "\n";
  cerr << "tesstoolwriter::tag=" << tag << "\n";

				// MPI initialization
  int myRank, mySize;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank); // Get rank
  MPI_Comm_size(MPI_COMM_WORLD, &mySize);

  wrerr << "tesstoolwriter::myRank=" << myRank 
	<< " out of " << mySize 
	<< " comm=" << MPI_COMM_WORLD
	<< endl;
  cerr << "tesstoolwriter::myRank=" << myRank 
       << " out of " << mySize 
       << " comm=" << MPI_COMM_WORLD
       << endl;
  
  MPI_Comm parent;
  MPI_Comm_get_parent(&parent); 
  if (parent == MPI_COMM_NULL) {
    wrerr << "tesstoolwriter:: No parent!";
    cerr  << "tesstoolwriter:: No parent!";
    exit(-1);
  }

  int psize;
  MPI_Comm_remote_size(parent, &psize); 

  wrerr << "tesstoolwriter: parent size=" << psize 
	<< " comm=" << parent << endl;	

  cerr  << "tesstoolwriter: parent size=" << psize 
	<< " comm=" << parent << endl;
  
  if ( myRank == 0 ) {

    TessToolWriter *write = 0;
    int wcnt = 0;

    do {
      delete write;

      write = new TessToolWriter(tag, parent);

      wrerr << "tesstoolwriter: created writer [" << wcnt++ << "]"
	    << endl << flush;

    } while (write->CacheData());


  } else {
    wrerr << "tesstoolwriter: Rank(" <<  myRank << ")" << endl << flush;
  }       

  return MPI_Finalize();
}

