#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string>
#include <vector>
#include <sstream>
#include <iomanip>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "vtkGtkRenderWindowInteractor.h"
#include "TessToolGUI.h"
#include "callbacks.h"
#include "interface.h"
#include "debug.h"

GtkWidget* TESSTOOL::window1 = 0;
vtkGtkRenderWindowInteractor* TESSTOOL::iren = 0;
GtkWidget *TESSTOOL::BIEOutputView = 0;
GtkWidget *TESSTOOL::scalar_menu = 0;
GtkWidget *TESSTOOL::scalarMin = 0;
GtkWidget *TESSTOOL::scalarMax = 0;
GtkWidget *TESSTOOL::scalarMinEntry = 0;
GtkWidget *TESSTOOL::scalarMaxEntry = 0;
GtkWidget *TESSTOOL::CLIScriptView = 0;
GtkWidget *TESSTOOL::WorkingDirView = 0;
GtkWidget *TESSTOOL::DataFileView = 0;
double TESSTOOL::min_scalar;
double TESSTOOL::max_scalar;

namespace TESSTOOL {
  std::vector<GtkWidget*> menu_items;
}

// For debugging
TESSTOOL::create_scene *TESSTOOL::scene = 0;

void
TESSTOOL::rebuild_menu(std::vector<std::string>& list)
{
  // Remove all current items
  //
  while (menu_items.size()) {
    gtk_container_remove(GTK_CONTAINER(scalar_menu), menu_items.back());
    menu_items.pop_back();
  }

  // Add new items
  //
  for (unsigned i=0; i<list.size(); i++) {
    // Create a new menu-item with a name...
    GtkWidget* menu_item = gtk_menu_item_new_with_label (list[i].c_str());
    menu_items.push_back(menu_item);

    // ...and add it to the menu.
    gtk_menu_shell_append (GTK_MENU_SHELL (scalar_menu), menu_item);

    // Add the name
    g_signal_connect (G_OBJECT (menu_item), "activate",
			      G_CALLBACK (on_scalar_menu),
			      (gpointer) strdup (list[i].c_str()));
    // Look for fiducial item
    if (TESSTOOL::scalarNameString.compare(list[i]) == 0) {
      gtk_menu_shell_select_item (GTK_MENU_SHELL (scalar_menu), menu_item);
      std::ostringstream sout;
      sout << "Selected for default: " << list[i].c_str() << endl;
      g_print(sout.str().c_str());

    }

    // Show the widget
    gtk_widget_show (menu_item);
  }

}

void
TESSTOOL::set_scalars()
{
  ostringstream minS;
  ostringstream maxS;

  minS << "Min scalar: " << setprecision(3) << TESSTOOL::min_scalar;
  maxS << "Max scalar: " << setprecision(3) << TESSTOOL::max_scalar;
  
  cout << "call to set_scalars: min=" << TESSTOOL::min_scalar
       << " max=" << TESSTOOL::max_scalar << endl;

  gtk_label_set_text(GTK_LABEL(scalarMin), minS.str().c_str());
  gtk_label_set_text(GTK_LABEL(scalarMax), maxS.str().c_str());
}




GtkWidget*
TESSTOOL::create_window1 (void)
{
  // ----------------------------------------------------------------------
  // MAIN WINDOW
  // ----------------------------------------------------------------------

  GtkAccelGroup *accel_group = gtk_accel_group_new ();

				// Main window
  TESSTOOL::window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window1), _("BIE Visualizer"));
  
				// Main vbox container
  GtkWidget *vbox_window = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_window);
  gtk_container_add (GTK_CONTAINER (window1), vbox_window);

				// Menus
  GtkWidget *menubar1 = gtk_menu_bar_new ();
  gtk_widget_show (menubar1);
  gtk_box_pack_start (GTK_BOX (vbox_window), menubar1, FALSE, FALSE, 0);

  GtkWidget *menuitem1 = gtk_menu_item_new_with_mnemonic (_("_File"));
  gtk_widget_show (menuitem1);
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);

  GtkWidget *menu1 = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menu1);

  /*
  GtkWidget *new1 = gtk_image_menu_item_new_from_stock ("gtk-new", accel_group);
  gtk_widget_show (new1);
  gtk_container_add (GTK_CONTAINER (menu1), new1);
  */

  GtkWidget *open1 = 
    gtk_image_menu_item_new_from_stock ("gtk-open", accel_group);
  gtk_widget_show (open1);
  gtk_container_add (GTK_CONTAINER (menu1), open1);

  
  
  GtkWidget *open2 = 
    gtk_image_menu_item_new_with_label ("Choose directory");
  
  GtkWidget *stock = gtk_image_new_from_stock (GTK_STOCK_DIRECTORY, GTK_ICON_SIZE_MENU);
  gtk_widget_show (stock);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (open2), stock);
  gtk_widget_show (open2);
  gtk_container_add (GTK_CONTAINER (menu1), open2);
  
  /*
  GtkWidget *save1 = gtk_image_menu_item_new_from_stock ("gtk-save", accel_group);
  gtk_widget_show (save1);
  gtk_container_add (GTK_CONTAINER (menu1), save1);
  */

  GtkWidget *save_as1 = gtk_image_menu_item_new_from_stock ("gtk-save-as", accel_group);
  gtk_widget_show (save_as1);
  // gtk_container_add (GTK_CONTAINER (menu1), save_as1);

  GtkWidget *separatormenuitem1 = gtk_separator_menu_item_new ();
  gtk_widget_show (separatormenuitem1);
  gtk_container_add (GTK_CONTAINER (menu1), separatormenuitem1);
  gtk_widget_set_sensitive (separatormenuitem1, FALSE);

  GtkWidget *quit1 = gtk_image_menu_item_new_from_stock ("gtk-quit", accel_group);
  gtk_widget_show (quit1);
  gtk_container_add (GTK_CONTAINER (menu1), quit1);

  GtkWidget *menuitem2 = gtk_menu_item_new_with_mnemonic (_("_Edit"));
  gtk_widget_show (menuitem2);
  //  gtk_container_add (GTK_CONTAINER (menubar1), menuitem2);

  GtkWidget *menu2 = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem2), menu2);

  GtkWidget *cut1 = gtk_image_menu_item_new_from_stock ("gtk-cut", accel_group);
  gtk_widget_show (cut1);
  gtk_container_add (GTK_CONTAINER (menu2), cut1);

  GtkWidget *copy1 = gtk_image_menu_item_new_from_stock ("gtk-copy", accel_group);
  gtk_widget_show (copy1);
  gtk_container_add (GTK_CONTAINER (menu2), copy1);
  
  GtkWidget *paste1 = gtk_image_menu_item_new_from_stock ("gtk-paste", accel_group);
  gtk_widget_show (paste1);
  gtk_container_add (GTK_CONTAINER (menu2), paste1);

  GtkWidget *delete1 = gtk_image_menu_item_new_from_stock ("gtk-delete", accel_group);
  gtk_widget_show (delete1);
  gtk_container_add (GTK_CONTAINER (menu2), delete1);

  GtkWidget *menuitem3 = gtk_menu_item_new_with_mnemonic (_("_View"));
  gtk_widget_show (menuitem3);
  // gtk_container_add (GTK_CONTAINER (menubar1), menuitem3);

  GtkWidget *menu3 = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem3), menu3);

  GtkWidget *menuitem4 = gtk_menu_item_new_with_mnemonic (_("_Help"));
  gtk_widget_show (menuitem4);
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem4);

  GtkWidget *menu4 = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem4), menu4);

  GtkWidget *HelpAboutLabel = gtk_menu_item_new_with_mnemonic (_("_About"));
  gtk_widget_show (HelpAboutLabel);
  gtk_container_add (GTK_CONTAINER (menu4), HelpAboutLabel);


  GtkWidget *textview1 = 0;
  GtkWidget *CLIScriptSendLabel = 0;

  if (viewer) {
    // ----------------------------------------------------------------------
    // Viewer stanza [2]
    // ----------------------------------------------------------------------
    
    GtkWidget *vbox_viewer = gtk_vbox_new (FALSE, 0);
    gtk_widget_show (vbox_viewer);
    gtk_box_pack_start (GTK_BOX (vbox_window), vbox_viewer, TRUE, TRUE, 0);

    GtkWidget *hbox_viewer = gtk_hbox_new (FALSE, 0);
    gtk_widget_show (hbox_viewer);
    gtk_box_pack_start (GTK_BOX (vbox_viewer), hbox_viewer, TRUE, TRUE, 0);

    GtkWidget *vbox_datafile = gtk_vbox_new (FALSE, 0);
    gtk_widget_show (vbox_datafile);
    gtk_box_pack_start (GTK_BOX (hbox_viewer), vbox_datafile, TRUE, TRUE, 0);

    GtkWidget *DataFileLabel = gtk_label_new (_("Data File"));
    gtk_widget_show (DataFileLabel);
    gtk_box_pack_start (GTK_BOX (vbox_datafile), DataFileLabel, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (DataFileLabel), GTK_JUSTIFY_LEFT);
    
    TESSTOOL::DataFileView = gtk_entry_new ();
    gtk_entry_set_text(GTK_ENTRY(DataFileView), "data.file");
    gtk_widget_show (DataFileView);
    gtk_box_pack_start (GTK_BOX (vbox_datafile), DataFileView, FALSE, FALSE, 13);

    GtkWidget *WorkingDirLabel = gtk_label_new (_("Tesstool archive directory"));
    gtk_widget_show (WorkingDirLabel);
    gtk_box_pack_start (GTK_BOX (vbox_datafile), WorkingDirLabel, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (WorkingDirLabel), GTK_JUSTIFY_LEFT);
    
    TESSTOOL::WorkingDirView = gtk_entry_new ();
    gtk_entry_set_text(GTK_ENTRY(WorkingDirView), "tessStore");
    gtk_widget_show (WorkingDirView);
    gtk_box_pack_start (GTK_BOX (vbox_datafile), WorkingDirView, FALSE, FALSE, 13);

  } else {
    // ----------------------------------------------------------------------
    // CLI script stanza [2]
    // ----------------------------------------------------------------------

    GtkWidget *vbox_cli = gtk_vbox_new (FALSE, 0);
    gtk_widget_show (vbox_cli);
    gtk_box_pack_start (GTK_BOX (vbox_window), vbox_cli, TRUE, TRUE, 0);

    GtkWidget *hbox_cli = gtk_hbox_new (FALSE, 0);
    gtk_widget_show (hbox_cli);
    gtk_box_pack_start (GTK_BOX (vbox_cli), hbox_cli, TRUE, TRUE, 0);

    GtkWidget *vbox_script = gtk_vbox_new (FALSE, 0);
    gtk_widget_show (vbox_script);
    gtk_box_pack_start (GTK_BOX (hbox_cli), vbox_script, TRUE, TRUE, 0);

    GtkWidget *CLIScriptLabel = gtk_label_new (_("CLI Script"));
    gtk_widget_show (CLIScriptLabel);
    gtk_box_pack_start (GTK_BOX (vbox_script), CLIScriptLabel, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (CLIScriptLabel), GTK_JUSTIFY_LEFT);
    
    TESSTOOL::CLIScriptView = gtk_entry_new ();
    gtk_widget_show (CLIScriptView);
    gtk_box_pack_start (GTK_BOX (vbox_script), CLIScriptView, FALSE, FALSE, 13);

    CLIScriptSendLabel = gtk_button_new_with_mnemonic (_("Send CLI Script"));
    gtk_widget_show (CLIScriptSendLabel);
    gtk_box_pack_start (GTK_BOX (vbox_script), CLIScriptSendLabel, FALSE, FALSE, 0);

    GtkWidget *vbox_cli_com = gtk_vbox_new (FALSE, 0);
    gtk_widget_show (vbox_cli_com);
    gtk_box_pack_start (GTK_BOX (hbox_cli), vbox_cli_com, TRUE, TRUE, 0);
    
    GtkWidget *frame_cli = gtk_frame_new (_("CLI Commands"));
    gtk_container_set_border_width  (GTK_CONTAINER(frame_cli), 5);
    gtk_widget_set_size_request(frame_cli, 200, 100);
    gtk_widget_show (frame_cli);
    
    gtk_box_pack_start (GTK_BOX (vbox_cli_com), frame_cli, TRUE, TRUE, 0);
    GtkWidget *scrolledwindow_cli = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_show (scrolledwindow_cli);
    gtk_container_add (GTK_CONTAINER (frame_cli), scrolledwindow_cli);

    textview1 = gtk_text_view_new ();
    gtk_widget_show (textview1);
    gtk_container_add (GTK_CONTAINER (scrolledwindow_cli), textview1);
  }

  // ----------------------------------------------------------------------
  // Scalar stanza
  // ----------------------------------------------------------------------

  GtkWidget *hseparator_cli = gtk_hseparator_new ();
  gtk_widget_show (hseparator_cli);
  gtk_box_pack_start (GTK_BOX (vbox_window), hseparator_cli, TRUE, TRUE, 20);

  GtkWidget *vbox_scalar = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_scalar);
  gtk_box_pack_start (GTK_BOX (vbox_window), vbox_scalar, TRUE, TRUE, 0);

  GtkWidget *hbox_scalar = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox_scalar);
  gtk_box_pack_start (GTK_BOX (vbox_scalar), hbox_scalar, TRUE, TRUE, 0);


  GtkWidget *vbox2d_data = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox2d_data);
  gtk_box_pack_start (GTK_BOX (hbox_scalar), vbox2d_data, TRUE, TRUE, 0);

  GtkWidget *GetDataLabel;
  if (viewer)
    GetDataLabel = gtk_label_new (_("From data file"));
  else
    GetDataLabel = gtk_label_new (_("From simulation"));

  gtk_widget_show (GetDataLabel);
  gtk_label_set_justify (GTK_LABEL (GetDataLabel), GTK_JUSTIFY_LEFT);
  gtk_box_pack_start (GTK_BOX (vbox2d_data), GetDataLabel, TRUE, TRUE, 0);

  GtkWidget *GetDataButton;
  if (viewer)
    GetDataButton = gtk_button_new_with_mnemonic (_("Process Data"));
  else
    GetDataButton = gtk_button_new_with_mnemonic (_("Get Data"));

  gtk_widget_show (GetDataButton);
  gtk_box_pack_start (GTK_BOX (vbox2d_data), GetDataButton, FALSE, FALSE, 0);

  GtkWidget *vbox_scalar_left = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_scalar_left);
  gtk_box_pack_start (GTK_BOX (hbox_scalar), vbox_scalar_left, TRUE, TRUE, 0);

  GtkWidget *vbox_scalar_right = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_scalar_right);
  gtk_box_pack_start (GTK_BOX (hbox_scalar), vbox_scalar_right, TRUE, TRUE, 0);

  GtkWidget *SelectScalarLabel = gtk_label_new (_("Select Scalar"));
  gtk_widget_show (SelectScalarLabel);
  gtk_box_pack_start (GTK_BOX (vbox_scalar_right), SelectScalarLabel, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (SelectScalarLabel), GTK_JUSTIFY_LEFT);

  GtkWidget *scalaroOptionMenu = gtk_option_menu_new ();
  gtk_widget_show (scalaroOptionMenu);
  gtk_box_pack_start (GTK_BOX (vbox_scalar_right), scalaroOptionMenu, FALSE, FALSE, 0);

  TESSTOOL::scalar_menu = gtk_menu_new ();

  GtkWidget *DefValue = gtk_menu_item_new_with_mnemonic (_("-----"));
  gtk_widget_show (DefValue);
  gtk_container_add (GTK_CONTAINER (TESSTOOL::scalar_menu), DefValue);
  menu_items.push_back(DefValue);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (scalaroOptionMenu), scalar_menu);

  GtkWidget *vbox_scalar_menu = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_scalar_menu);
  gtk_box_pack_start (GTK_BOX (hbox_scalar), vbox_scalar_menu, TRUE, TRUE, 0);

  GtkWidget *ScalarLabel = gtk_label_new (_("Scalar Name"));
  gtk_widget_show (ScalarLabel);
  gtk_box_pack_start (GTK_BOX (vbox_scalar_menu), ScalarLabel, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (ScalarLabel), GTK_JUSTIFY_LEFT);

  GtkWidget *scalarByName = gtk_entry_new ();
  gtk_widget_show (scalarByName);
  gtk_box_pack_start (GTK_BOX (vbox_scalar_menu), scalarByName, FALSE, FALSE, 0);

  GtkWidget *vbox2d_viz = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox2d_viz);
  gtk_box_pack_start (GTK_BOX (hbox_scalar), vbox2d_viz, TRUE, TRUE, 0);

  GtkWidget *VisualizeLabel = gtk_label_new (_("To VTK"));
  gtk_widget_show (VisualizeLabel);
  gtk_box_pack_start (GTK_BOX (vbox2d_viz), VisualizeLabel, TRUE, TRUE, 0);

  GtkWidget *VisualizeButton = gtk_button_new_with_mnemonic (_("Visualize"));
  gtk_widget_show (VisualizeButton);
  gtk_box_pack_start (GTK_BOX (vbox2d_viz), VisualizeButton, FALSE, FALSE, 0);


  // ----------------------------------------------------------------------
  // Scalar min/max stanza
  // ----------------------------------------------------------------------

  GtkWidget *hseparator_stanza = gtk_hseparator_new ();
  gtk_widget_show (hseparator_stanza);
  gtk_box_pack_start (GTK_BOX (vbox_window), hseparator_stanza, TRUE, TRUE, 20);

  GtkWidget *hbox_minmax = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox_minmax);
  gtk_box_pack_start (GTK_BOX (vbox_window), hbox_minmax, TRUE, TRUE, 0);

  GtkWidget *vbox2d_minmax = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox2d_minmax);
  gtk_box_pack_start (GTK_BOX (hbox_minmax), vbox2d_minmax, TRUE, TRUE, 0);

  GtkWidget *SetScalarLabel = gtk_label_new (_("Min/Max"));
  gtk_widget_show (SetScalarLabel);
  gtk_box_pack_start (GTK_BOX (vbox2d_minmax), SetScalarLabel, FALSE, FALSE, 0);

  GtkWidget *SetScalarButton = gtk_button_new_with_mnemonic (_("Set"));
  gtk_widget_show (SetScalarButton);
  gtk_box_pack_start (GTK_BOX (vbox2d_minmax), SetScalarButton, FALSE, FALSE, 0);

  GtkWidget *vbox_min = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_min);
  gtk_box_pack_start (GTK_BOX (hbox_minmax), vbox_min, TRUE, TRUE, 0);

  TESSTOOL::scalarMin = gtk_label_new (_("Min scalar"));
  gtk_widget_show (scalarMin);
  gtk_box_pack_start (GTK_BOX (vbox_min), scalarMin, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (scalarMin), GTK_JUSTIFY_LEFT);

  TESSTOOL::scalarMinEntry = gtk_entry_new ();
  gtk_widget_show (scalarMinEntry);
  gtk_box_pack_start (GTK_BOX (vbox_min), scalarMinEntry, FALSE, FALSE, 0);

  GtkWidget* vbox_max = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_max);
  gtk_box_pack_start (GTK_BOX (hbox_minmax), vbox_max, TRUE, TRUE, 0);

  TESSTOOL::scalarMax = gtk_label_new (_("Max scalar"));
  gtk_widget_show (scalarMax);
  gtk_box_pack_start (GTK_BOX (vbox_max), scalarMax, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (scalarMax), GTK_JUSTIFY_LEFT);

  TESSTOOL::scalarMaxEntry = gtk_entry_new ();
  gtk_widget_show (scalarMaxEntry);
  gtk_box_pack_start (GTK_BOX (vbox_max), scalarMaxEntry, FALSE, FALSE, 0);

  // ----------------------------------------------------------------------
  // Color stanza
  // ----------------------------------------------------------------------

  GtkWidget *hseparator_color = gtk_hseparator_new ();
  gtk_widget_show (hseparator_color);
  gtk_box_pack_start (GTK_BOX (vbox_window), hseparator_color, TRUE, TRUE, 20);

  GtkWidget *vbox_color = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox_color);
  gtk_box_pack_start (GTK_BOX (vbox_window), vbox_color, TRUE, TRUE, 0);

  GtkWidget *table1 = gtk_table_new (4, 6, FALSE);
  gtk_widget_show (table1);
  gtk_box_pack_start (GTK_BOX (vbox_color), table1, TRUE, TRUE, 0);

  GtkWidget *HueLabel = gtk_label_new (_("Hue"));
  gtk_widget_show (HueLabel);
  gtk_table_attach (GTK_TABLE (table1), HueLabel, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (HueLabel), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (HueLabel), 0, 0.5);

  GtkWidget *SaturationLabel = gtk_label_new (_("Saturation"));
  gtk_widget_show (SaturationLabel);
  gtk_table_attach (GTK_TABLE (table1), SaturationLabel, 2, 3, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (SaturationLabel), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (SaturationLabel), 0, 0.5);

  GtkWidget *ValueLabel = gtk_label_new (_("Value"));
  gtk_widget_show (ValueLabel);
  gtk_table_attach (GTK_TABLE (table1), ValueLabel, 3, 4, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (ValueLabel), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (ValueLabel), 0, 0.5);

  GtkWidget *AlphaLabel = gtk_label_new (_("Alpha"));
  gtk_widget_show (AlphaLabel);
  gtk_table_attach (GTK_TABLE (table1), AlphaLabel, 4, 5, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (AlphaLabel), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (AlphaLabel), 0, 0.5);

  GtkWidget *label5 = gtk_label_new (_("Low"));
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table1), label5, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label5), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (label5), 0, 0.5);

  GtkWidget *label6 = gtk_label_new (_("High"));
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table1), label6, 0, 1, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label6), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (label6), 0, 0.5);

  GtkWidget *HueLowView = gtk_entry_new ();
  gtk_widget_show (HueLowView);
  gtk_table_attach (GTK_TABLE (table1), HueLowView, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (HueLowView, GTK_CAN_DEFAULT);
  gtk_entry_set_text (GTK_ENTRY (HueLowView), _("0.6666667"));

  GtkWidget *HueHighView = gtk_entry_new ();
  gtk_widget_show (HueHighView);
  gtk_table_attach (GTK_TABLE (table1), HueHighView, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (HueHighView, GTK_CAN_DEFAULT);
  gtk_entry_set_text (GTK_ENTRY (HueHighView), _("1.0"));

  GtkWidget *SaturationLowView = gtk_entry_new ();
  gtk_widget_show (SaturationLowView);
  gtk_table_attach (GTK_TABLE (table1), SaturationLowView, 2, 3, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (SaturationLowView, GTK_CAN_DEFAULT);
  gtk_entry_set_text (GTK_ENTRY (SaturationLowView), _("1.0"));

  GtkWidget *SaturationHighView = gtk_entry_new ();
  gtk_widget_show (SaturationHighView);
  gtk_table_attach (GTK_TABLE (table1), SaturationHighView, 2, 3, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (SaturationHighView, GTK_CAN_DEFAULT);
  gtk_entry_set_text (GTK_ENTRY (SaturationHighView), _("1.0"));

  GtkWidget *ValueLowView = gtk_entry_new ();
  gtk_widget_show (ValueLowView);
  gtk_table_attach (GTK_TABLE (table1), ValueLowView, 3, 4, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (ValueLowView, GTK_CAN_DEFAULT);
  gtk_entry_set_text (GTK_ENTRY (ValueLowView), _("1.0"));

  GtkWidget *ValueHighView = gtk_entry_new ();
  gtk_widget_show (ValueHighView);
  gtk_table_attach (GTK_TABLE (table1), ValueHighView, 3, 4, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (ValueHighView, GTK_CAN_DEFAULT);
  gtk_entry_set_text (GTK_ENTRY (ValueHighView), _("1.0"));

  GtkWidget *AlphaLowView = gtk_entry_new ();
  gtk_widget_show (AlphaLowView);
  gtk_table_attach (GTK_TABLE (table1), AlphaLowView, 4, 5, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_text (GTK_ENTRY (AlphaLowView), _("1.0"));

  GtkWidget *AlphaHighView = gtk_entry_new ();
  gtk_widget_show (AlphaHighView);
  gtk_table_attach (GTK_TABLE (table1), AlphaHighView, 4, 5, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_text (GTK_ENTRY (AlphaHighView), _("1.0"));

  /*
  GtkWidget *StopLabel = gtk_button_new_with_mnemonic (_("Stop"));
  gtk_widget_show (StopLabel);
  gtk_table_attach (GTK_TABLE (table1), StopLabel, 4, 5, 3, 4,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  */

  

  GtkWidget *NoLabel = gtk_label_new (_(" "));
  gtk_widget_show (NoLabel);
  gtk_table_attach (GTK_TABLE (table1), NoLabel, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (NoLabel), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (NoLabel), 0, 0.5);


  GtkWidget *NewMapButton = gtk_button_new_with_mnemonic (_("New Color Map"));
  gtk_widget_show (NewMapButton);
  gtk_table_attach (GTK_TABLE (table1), NewMapButton, 1, 2, 5, 6,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  if (!viewer) {
    GtkWidget *hseparator8 = gtk_hseparator_new ();
    gtk_widget_show (hseparator8);
    gtk_box_pack_start (GTK_BOX (vbox_window), hseparator8, TRUE, TRUE, 20);

    GtkWidget *vbox_oput = gtk_vbox_new (FALSE, 0);
    gtk_widget_show (vbox_oput);
    gtk_box_pack_start (GTK_BOX (vbox_window), vbox_oput, TRUE, TRUE, 0);

    GtkWidget *frame_oput = gtk_frame_new (_("BIE Output"));
    gtk_container_set_border_width  (GTK_CONTAINER(frame_oput), 5);
    gtk_widget_show (frame_oput);

    gtk_box_pack_start (GTK_BOX (vbox_oput), frame_oput, TRUE, TRUE, 0);
    GtkWidget *scrolledwindow_oput = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_set_size_request(scrolledwindow_oput, 400, 200);
    gtk_widget_show (scrolledwindow_oput);
    gtk_container_add (GTK_CONTAINER (frame_oput), scrolledwindow_oput);

    BIEOutputView = gtk_text_view_new ();
    gtk_widget_show (BIEOutputView);
    gtk_container_add (GTK_CONTAINER (scrolledwindow_oput), BIEOutputView);
  }

  /*
  g_signal_connect ((gpointer) new1, "activate",
                    G_CALLBACK (on_new1_activate),
                    NULL);
  */

  g_signal_connect ((gpointer) open1, "activate",
                    G_CALLBACK (on_open1_activate),
                    NULL);

  g_signal_connect ((gpointer) open2, "activate",
                    G_CALLBACK (on_open2_activate),
                    NULL);

  /*
  g_signal_connect ((gpointer) save1, "activate",
                    G_CALLBACK (on_save1_activate),
                    NULL);

  */

  g_signal_connect ((gpointer) save_as1, "activate",
                    G_CALLBACK (on_save_as1_activate),
                    NULL);

  g_signal_connect ((gpointer) quit1, "activate",
                    G_CALLBACK (on_quit1_activate),
                    NULL);

  g_signal_connect ((gpointer) cut1, "activate",
                    G_CALLBACK (on_cut1_activate),
                    NULL);

  g_signal_connect ((gpointer) copy1, "activate",
                    G_CALLBACK (on_copy1_activate),
                    NULL);

  g_signal_connect ((gpointer) paste1, "activate",
                    G_CALLBACK (on_paste1_activate),
                    NULL);

  g_signal_connect ((gpointer) delete1, "activate",
                    G_CALLBACK (on_delete1_activate),
                    NULL);

  g_signal_connect ((gpointer) HelpAboutLabel, "activate",
                    G_CALLBACK (help_about_galaxy),
                    NULL);

  if (viewer) {
    g_signal_connect ((gpointer) WorkingDirView, "focus_out_event",
		      G_CALLBACK (working_dir_changed),
		      NULL);

    g_signal_connect ((gpointer) DataFileView, "focus_out_event",
		      G_CALLBACK (data_file_changed),
		      NULL);
  } else {
    g_signal_connect ((gpointer) CLIScriptView, "focus_out_event",
		      G_CALLBACK (cli_script_changed),
		      NULL);

    g_signal_connect (GTK_TEXT_VIEW (textview1), "key-press-event",
		      G_CALLBACK (cli_command_input),
		      NULL);
  }

  g_signal_connect ((gpointer) scalarByName, "changed",
                    G_CALLBACK (on_scalarByName_changed),
                    NULL);

  if (viewer) {
    g_signal_connect ((gpointer) GetDataButton, "clicked",
		      G_CALLBACK (ProcessData),
		      NULL);
  } else {
    g_signal_connect ((gpointer) GetDataButton, "clicked",
		      G_CALLBACK (GetData),
		      NULL);
  }

  g_signal_connect ((gpointer) SetScalarButton, "clicked",
                    G_CALLBACK (SetScalars),
                    NULL);

  g_signal_connect ((gpointer) HueLowView, "focus_out_event",
                    G_CALLBACK (hue_low),
                    NULL);

  g_signal_connect ((gpointer) HueHighView, "focus_out_event",
                    G_CALLBACK (hue_high),
                    NULL);

  g_signal_connect ((gpointer) SaturationLowView, "focus_out_event",
                    G_CALLBACK (saturation_low),
                    NULL);

  g_signal_connect ((gpointer) SaturationHighView, "focus_out_event",
                    G_CALLBACK (saturation_high),
                    NULL);

  g_signal_connect ((gpointer) ValueLowView, "focus_out_event",
                    G_CALLBACK (value_low),
                    NULL);

  g_signal_connect ((gpointer) ValueHighView, "focus_out_event",
                    G_CALLBACK (value_high),
                    NULL);

  g_signal_connect ((gpointer) AlphaLowView, "focus_out_event",
                    G_CALLBACK (alpha_low),
                    NULL);

  g_signal_connect ((gpointer) AlphaHighView, "focus_out_event",
                    G_CALLBACK (alpha_high),
                    NULL);

  g_signal_connect ((gpointer) VisualizeButton, "clicked",
                    G_CALLBACK (call_vtk),
                    NULL);

  /*
  g_signal_connect ((gpointer) StopLabel, "clicked",
                    G_CALLBACK (stop_vtk),
                    NULL);
  */

  g_signal_connect ((gpointer) NewMapButton, "clicked",
                    G_CALLBACK (refresh_vtk),
                    NULL);

  if (!viewer) {
    g_signal_connect ((gpointer) CLIScriptSendLabel, "clicked",
		      G_CALLBACK (CLIScriptSend),
		      NULL);
  }

  gtk_widget_grab_default (HueLowView);

  gtk_window_add_accel_group (GTK_WINDOW (window1), accel_group);

  return window1;
}

