#include <cstdlib>		// For exit()

#include "TessToolController.h"
#include "TessToolGUI.h"
#include "interface.h"
#include "callbacks.h"

#include "vtkGtkRenderWindowInteractor.h"
#include <vtk/vtkInteractorStyleImage.h>

using namespace BIE;
using namespace TESSTOOL;

TessToolGUI* BIE::TessToolGUI::_gui = NULL;
int  BIE::TessToolGUI::_bufferSize = 1 << 12;
char *  BIE::TessToolGUI::_buffer = new char[_bufferSize];

TessToolGUI::TessToolGUI(int argc, char *argv[], TessToolController *controller)
{
  // allow only one instance
  //
  if (_gui == NULL) {
    _argc = argc;
    _argv = argv;
    _work = true;
    _controller = controller;
    
    _gui = this;
    scriptSent =  ttRxInitialized = ttVTKInitialized = 
      scalarSelected = scalarInfoSelected = dataStreamStarted =
      dataFetchRequested = dataVisualized =  false;
    
  } else {
    rferr << "TessToolGUI already exists" << endl;
    std::exit(1);
  }
}

TessToolGUI::~TessToolGUI()
{
  delete [] _buffer;
}

void TessToolGUI::run()
{
  rferr << "TessToolGUI: entered run method" << endl << flush;

  gtk_init(&_argc, &_argv); 

  // Make the GUI
  //
  window1 = create_window1();
  gtk_widget_show_all(window1);

  rferr << "TessToolGUI: created GUI window" << endl << flush;

  // Make the tessellation scene
  //
  gtkvtk(_argc, _argv);

  // Add scene to interactor
  //
  TESSTOOL::iren->GetRenderWindow()->
    AddRenderer(_controller->getVTK()->getVTKRenderer());
  
  // Image interactor (keeps camera normal)
  //
  vtkInteractorStyleImage *iimage = vtkInteractorStyleImage::New();
  TESSTOOL::iren->SetInteractorStyle(iimage);
  iimage->Delete();

  // Start the gtk loop
  //
  rferr << "TessToolGUI: about to start gtk loop" << endl << flush;
  gtk_main();
  rferr << "TessToolGUI: exited gtk loop" << endl << flush;
}


void TessToolGUI::updateBIEOutputView(char *txt)
{
  update_bie(txt);
}

namespace TESSTOOL {
  
  void setWorkingDirName(char *n)
  {
    BIE::TessToolController::_guiController->getVTK()->setPersistenceDir(n);
  }

  void setDataFileName(char *n)
  {
    BIE::TessToolController::_guiController->setDataFileName(n);
  }

  void processData()
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();

    BIE::TessToolController::_guiController->getGUI()->dataStreamStarted = true;
    BIE::TessToolController::_guiController->sampleNext();
    BIE::TessToolController::_guiController->getGUI()->dataFetchRequested = true;
    BIE::TessToolController::_guiController->getGUI()->dataVisualized = false;
  }

  void setHueLow(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setHueLow(l);
  }
  
  void setHueHigh(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setHueHigh(l);
  }
  
  void setSaturationLow(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
  }
  
  void setSaturationHigh(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setSaturationHigh(l);
  }
  
  void setValueLow(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setValueLow(l);
  }
  
  void setValueHigh(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setValueHigh(l);
  }
  
  void setAlphaLow(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setAlphaLow(l);
  }
  
  void setAlphaHigh(float l)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setAlphaHigh(l);
  }
  
  void setVTKWindow(int width, int height)
  {
    BIE::TessToolController::_guiController->setVTKWindow(width, height);
  }
  
  void initVTK()
  {
    BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized = true;
    BIE::TessToolController::_guiController->initVTK();
  }
  
  void *getVTKRenderWindow()
  {
    return (void *) BIE::TessToolController::_guiController->getVTKRenderWindow();
  }
  
  void *getVTKRenderer()
  {
    return (void *) BIE::TessToolController::_guiController->getVTKRenderer();
  }
  
  void *getVTKCellPicker()
  {
    return (void *) BIE::TessToolController::_guiController->getVTKCellPicker();
  }
  
  
  void setScalarName(char *n)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    
    BIE::TessToolController::_guiController->setScalarName(n);
    BIE::TessToolController::_guiController->getGUI()->scalarSelected = true;
  }
  
  void setScalarInfoName(char *n)
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    BIE::TessToolController::_guiController->setScalarInfoName(n);
    BIE::TessToolController::_guiController->getGUI()->scalarInfoSelected = true;
  }
  
  void setScriptFileName(char *fn)
  {
    BIE::TessToolController::_guiController->setScriptFileName(fn);
  }
  
  char *getScriptFileName()
  {
    return BIE::TessToolController::_guiController->getScriptFileName();
  }
  
  void sendScriptFile(char* filename)
  {
    string scriptfile(filename);
    rferr << "TestToolGUI::sendScriptFile:Sending " << scriptfile << " to "<< 
      BIE::TessToolController::_guiController << endl;
    BIE::TessToolController::_guiController->sendScriptFile(scriptfile);
    rferr << "TestToolGUI::sendScriptFile:Sent" << endl;
    BIE::TessToolController::_guiController->getGUI()->scriptSent = true;
  }
  
  char *sendCommand(char* cmd, int* replySize, int *replyLines)
  {
    int numLines=0, size = 0;
    string comm(cmd);
    rferr << "TestToolGUI::sendCommand:Sending " << comm << " to "<< 
      BIE::TessToolController::_guiController << endl;
    istream *reply = BIE::TessToolController::_guiController->sendCommand(comm);
    rferr << "TestToolGUI::sendCommand:Sent" << endl;

    // return reply string
    //
    bzero(BIE::TessToolGUI::_buffer, BIE::TessToolGUI::_bufferSize);
    if (reply) {
      while (!reply->eof()) {
	reply->getline(BIE::TessToolGUI::_buffer+size, BIE::TessToolGUI::_bufferSize);
	size = strlen(BIE::TessToolGUI::_buffer);
	numLines++;
      }
    } else {
      size = 0;
      BIE::TessToolGUI::_buffer[0] = '\0';
      //numLines++;
    }
    rferr << "TessToolGUI:sendCommand: reply is " << BIE::TessToolGUI::_buffer << endl;
    *replySize = size;
    *replyLines = numLines;
    return BIE::TessToolGUI::_buffer;
  }
  
  void startCLICommandLoop() 
  {
    BIE::TessToolController::_guiController->startCLICommandLoop();
  }  
  
  void startVTK() 
  {
    BIE::TessToolController::_guiController->startVTK();
  }
  
  void updateVTK() 
  {
    BIE::TessToolController::_guiController->updateVTK();
  }
  
  void sampleNext()
  {
    if(!BIE::TessToolController::_guiController->getGUI()->ttVTKInitialized)
      initVTK();
    
    if (TessToolController::_guiController->isTessToolReceiverConnected()) {
      BIE::TessToolController::_guiController->sampleNext();
      BIE::TessToolController::_guiController->getGUI()->dataFetchRequested = true;
      BIE::TessToolController::_guiController->getGUI()->dataVisualized = false;
    } else {
      pop_message("TessToolReceiver not attached to TessToolSender");
    }
  }
  
  void visualize()
  {
    if (TessToolController::_guiController->getGUI()->dataFetchRequested  && 
	BIE::TessToolController::_guiController->getGUI()->scalarSelected &&
	! TessToolController::_guiController->getGUI()->dataVisualized ) {
      BIE::TessToolController::_guiController->visualize();
      BIE::TessToolController::_guiController->getGUI()->dataVisualized = true;
      gtk_window_present(GTK_WINDOW(TESSTOOL::vtk_window));
    } else {

      if (!TessToolController::_guiController->getGUI()->dataFetchRequested ||
	  TessToolController::_guiController->getGUI()->dataVisualized ) {
	if (viewer)
	  pop_message("Process Data First");
	else
	  pop_message("Get Data First");
      }

      if (!TessToolController::_guiController->getGUI()->scalarSelected)
	pop_message("Select Scalar to Visualize First");
    }
  }
  
  void quitTessTool()
  {
#ifdef DEBUG_TESSTOOL
    rferr << "TestToolGUI::quitTessTool enter " << endl;
#endif
    BIE::TessToolController::_guiController->quitTessTool();
#ifdef DEBUG_TESSTOOL
    rferr << "TestToolGUI::quitTessTool exit" << endl;
#endif
  }
  
}
