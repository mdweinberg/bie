#include <iostream>
#include <fstream>
#include <iomanip>

#include <cc++/thread.h>
#include <TessToolReader.h>
#include <TessToolGPL.h>
#include <BasicType.h>

using namespace std;
using namespace BIE;

namespace TESSTOOL 
{
  bool work = true;
  bool viewer = false;
  extern bool stream_ok;
}

int main(int argc, char *argv[])
{           
  rferr.open("errfile.rx");
  
  Semaphore* _controllerRX = new Semaphore(0);
  
  TessToolReader *_rcvrthread = new TessToolReader ();
  
  Semaphore* _rcvrThreadSem = _rcvrthread->getSemaphore();
  _rcvrthread->setConsumerSemaphore(_controllerRX);
  
  rferr << "TessToolPlotter::before DataStream start" << endl;
  cerr  << "TessToolPlotter::before DataStream start" << endl;
  _rcvrthread->start();
  rferr << "TessToolPlotter::after DataStream Start" << endl;
  cerr  << "TessToolPlotter::after DataStream Start" << endl;
  
  TessToolGPL *_gpl = 0;
  
  vector<string> namelist;

  do {
    delete _gpl;
    _gpl = new TessToolGPL();

    //
    // Parse file
    //
    cout << "Reading directory . . . ";

    _rcvrthread->setFile(argv[1]);
    _rcvrthread->SynchronizeCommand();
    _rcvrThreadSem->post();
    _controllerRX->wait();

    cout << " done" << endl << flush;
    
    RecordType *rt = _rcvrthread->GetRecordType();

    if (TESSTOOL::stream_ok) {
  
      // Poll for record type
      while(rt == NULL) {
	sleep(1); 
	rt = _rcvrthread->GetRecordType();
      }
  
      for (int i=1; i<=rt->numFields(); i++) {
	namelist.push_back(rt->getFieldName(i));
	cout << setw(4) << i << ":  "
	     << setw(40) << left << rt->getFieldName(i) 
	     << rt->getFieldType(i)->toString()
	     << endl;
      }

      RecordInputStream_MPI *mpistrm = 
	new RecordInputStream_MPI(rt, _rcvrthread);
    
      _gpl->SetSourceStream(mpistrm);
    }


  } while (!TESSTOOL::stream_ok);

  //
  // Get tessellation
  //
  cout << "Reading tessellation . . . ";
  _gpl->initGPL();
  cout << "done" << endl << flush;

  //
  // Select scalar
  //    
  int scalar;
  cout << "Choose scalar? ";
  cin >> scalar;

  string outfile;
  cout << "Output file? ";
  cin >> outfile;

  ofstream out(outfile.c_str());

  cout << "Reading tessellation . . . ";
  _gpl->createNewDataSet(out, scalar);
  cout << "done" << endl << flush;

  out.close();


  cerr << "Deleting _rcvrthread" << endl;
  delete _rcvrthread;

  rferr << "tesstoolplotter: exiting" << endl;
  cerr  << "tesstoolplotter: exiting" << endl;

  cerr  << "tesstoolplotter: closing output stream . . . " << flush;
  rferr.close();
  cerr << "done" << endl;

  return 0;
}

