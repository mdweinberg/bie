#include "debug.h"

using namespace TESSTOOL;

// Creates a scene consisting of a sphere and a 3D block

create_scene::create_scene()
{
  // create sphere geometry
  sphere = vtkSphereSource::New();
  sphere->SetRadius(1.0);
  sphere->SetThetaResolution(50);
  sphere->SetPhiResolution(50);

  // map to graphics library
  sphere_map = vtkPolyDataMapper::New();
  sphere_map->SetInput(sphere->GetOutput());

  // actor coordinates geometry, properties, transformation
  sphere_actor = vtkActor::New();
  sphere_actor->SetMapper(sphere_map);
  sphere_actor->GetProperty()->SetColor(0,0,1); // sphere color blue

  // cube
  cube = vtkCubeSource::New();
  cube->SetXLength(1.5);
  cube->SetYLength(1.0);
  cube->SetZLength(0.5);

  // map to graphics library
  cube_map = vtkPolyDataMapper::New();
  cube_map->SetInput(cube->GetOutput());

  // actor coordinates geometry, properties, transformation
  cube_actor = vtkActor::New();
  cube_actor->SetMapper(cube_map);
  cube_actor->SetPosition(2.5,0,0);
  cube_actor->GetProperty()->SetColor(1,0,0); // cube color is red

  // a renderer and and place the objects in it render window
  ren1 = vtkRenderer::New();
  ren1->AddActor(sphere_actor);
  ren1->AddActor(cube_actor);
  ren1->SetBackground(1,1,1); // Background color white

  sphere->Delete();
  sphere_map->Delete();
  sphere_actor->Delete();
  cube->Delete();
  cube_map->Delete();
  cube_actor->Delete();
}

create_scene::~create_scene()
{
  ren1->Delete();
}
