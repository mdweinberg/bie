
#include <gvariable.h>
#include <TessToolVTK.h>
#include <tesstoolMutex.h>
#include <gvariable.h>
#include <interface.h>
#include <TestLoadManager.h>

#include <Node.h>
#include <QuadGrid.h>
#include <SquareTile.h>
#include <RecordInputStream_MPI.h>
#include <bieTags.h>

// using namespace BIE;

int TessToolVTK::default_height = 800;
int TessToolVTK::default_width  = 800;

vtkRenderer *create_scene();

string TESSTOOL::scalarNameString, TESSTOOL::scalarInfoNameString;

TessToolVTK::TessToolVTK()
{
  _XWinHeight = default_height;
  _XWinWidth = default_width;
  _persistencedir = string(PERSISTENCE_DIR) + "/" + TESSELLATION_STORE;
  initialize();
  tterr << "TessToolVTK persistence dir is " << _persistencedir << endl;
}

TessToolVTK::TessToolVTK(int w, int h)
{
  _XWinWidth = w;
  _XWinHeight = h;
  _persistencedir = string(PERSISTENCE_DIR) + "/" + TESSELLATION_STORE;
  initialize();
  tterr << "TessToolVTK persistence dir is " << _persistencedir << endl;
}

TessToolVTK::~TessToolVTK()
{
}


TessToolVTK::TessToolVTK(string pdir)
{
  _XWinHeight = default_height;
  _XWinWidth = default_width;
  _persistencedir = pdir;
  initialize();
}

TessToolVTK::TessToolVTK(string pdir, int w, int h)
{
  _XWinHeight = h;
  _XWinWidth = w;
  _persistencedir = pdir;
  initialize();
}


void TessToolVTK::initialize()
{
  // Debug output file
  tterr.open("errfile.tt");
  if (tterr) cout << "Opened error file\n";

  _vtksem = new Semaphore(0);
  _mpistrm=NULL;
  _tess=NULL;
  
  _initTessellation = false;
  _initVTK = false;

  _hueLow = 0.666667;
  _hueHigh = 1.0;
  _saturationLow = _saturationHigh = 1.0;
  _valueLow = _valueHigh = 1.0;
  _alphaLow = _alphaHigh = 1.0;
  
  _tmpScalarNameString = string();
  _tmpScalarInfoNameString = string();
  
  _scalarNameChanged =true;
  _scalarInfoNameChanged =  true; 
  
  _scalarInfoNameInitialized = false;
  _scalarNameInitialized = false;
  
  _firstDataSet = true;
  _sampleNum = 0;

  _renWin = NULL;


  // create a renderer

  _renderer = vtkRenderer::New();
  

  // create a picker

  vtkTextMapper *textMapper = vtkTextMapper::New();
  vtkTextProperty *textProp = textMapper->GetTextProperty();
  textProp->SetFontSize(12);
  textProp->BoldOn();
  textProp->SetColor(0, 0, 0);
  
  vtkActor2D *textActor = vtkActor2D::New();
  textActor->VisibilityOff();
  textActor->SetMapper(textMapper);
  
  _picker = vtkCellPicker::New();
  
  _annotatePick = vtkGtkCallback::New();
  _annotatePick->textMapper = textMapper;
  _annotatePick->textActor = textActor;
  
  _picker->AddObserver(vtkCallbackCommand::EndPickEvent, _annotatePick);
  
  _renderer->AddActor2D(textActor);
  _renderer->AddObserver(vtkCommand::EndPickEvent, _annotatePick);
  
  textMapper->Delete();
  textActor->Delete();
}

void TessToolVTK::initVTK()
{
  if (!_initTessellation) {
    initializeTessellation();
  } else {
    tterr << "TessToolVTK:: tessellation already initialized" << endl;
  }
  if (!_initVTK) {
    initializeVTK();
    tterr << "TessToolVTK:: Called initializeVTK() from initVTK" << endl;
  } else {
    tterr << "TessToolVTK:: Called VTK already initialized" << endl;
  }    
}

void TessToolVTK::initializeTessellation()
{
  // until tessellation is written
  _vtksem->wait();

  int tries=3;
  int rv=1;
  struct stat s;
  string cmd = "ls -lR " + _persistencedir;

  tterr << system(cmd.c_str()) << endl;
  
  while(rv) {			// Polling loop: wait for 
				// stored tessellation to exist
    if (rv < 0) sleep(5);
    rv = stat(_persistencedir.c_str(), &s);
  }
  
  TestUserState *restored;

  while (tries-->0) {

    ++tesstoolMutex;

    try {

      try {
	string saved(TESSELLATION_STORE);
	TestLoadManager loadManager(&restored, saved, 1,
				    BOOST_BINARY, BACKEND_FILE);
	loadManager.load();
#ifdef DEBUG_TESSTOOL
	cout << "TessToolVTK: " << TESSELLATION_STORE << " loaded" << endl;
#endif
      } catch (BoostSerializationException * e) {
	cout << 
	  "################################################################" 
	     << endl <<
	  "##### BoostSerializationException thrown on Tesstool load! #####" 
	     << endl << "Printing persistence stack trace..." << endl;
	e->print(cout);
	cout <<
	  "################################################################" 
	     << endl;
      }
  
      vector<Serializable*> objectlist = restored->getTransClosure().objectlist;
  
      QuadGrid *rtess = dynamic_cast<QuadGrid*>(objectlist[0]);
      
      _tess = rtess;
      _tessType = TESS_QUADGRID;
      
      tterr << "restored tess" << endl;
      tterr << "Total number of tiles=" << rtess->NumberTiles() << endl;
      vector<int> rtiles = rtess->GetRootTiles();
      tterr << "Number of root tiles=" << rtiles.size() << endl;
      
      vector<int>::iterator ritr = rtiles.begin();
      while (ritr !=  rtiles.end()) {
	tterr << *ritr++ << endl; 
      }
      vector<Node*> rnodes = rtess->GetRootNodes();
      tterr << "Number of root nodes=" << rnodes.size() << endl;
      vector<Node*>::iterator rnitr = rnodes.begin();
      while (rnitr !=  rnodes.end()) {
	Node * rnode = (Node*) *rnitr;
	rnode->print(cout);
	rnode->print(rferr);
	rnitr++; 
      }
      tterr << "Restored Tree" << endl;
      rtess->PrintPreOrder(cout);
      tterr << "----------------------------------------------------------" << endl;
    }
    catch (BIEException e)
      { 
	cerr << "Caught Exception: \n" << e.getErrorMessage() << endl; 
      }
    
    --tesstoolMutex;
    if (_tess != NULL) break;
    
  }
  if (_tess != NULL) {
    _initTessellation = true;
  } else { 
    tterr << "Could not initialize Tessellation" << endl;
    std::exit(1024);
  }
}

void TessToolVTK::createNewDataSet()
{

  int recordNum=0;
  int numTiles=-1;
  int tileid=-1;
  int offset =0;
  double scalar;
  
  rferr << "In TessToolVTK::createNewDataSet" << endl;
  
  updateScalarIndices();
  
  if(_firstDataSet) {
    _firstDataSet = false;
  } else {

    vtkCellArray* cells = _polydata->GetPolys();
    vtkPoints *points = _polydata->GetPoints();
    vtkDataArray *scalars = _polydata->GetCellData()->GetScalars();

    cells->Delete();
    points->Delete();
    scalars->Delete();

    _polydata->Initialize();
  }
  
  vtkCellArray* cells = vtkCellArray::New();
  vtkPoints *points = vtkPoints::New();
  vtkFloatArray *scalars = vtkFloatArray::New();
  scalars->SetNumberOfComponents(1);
  
  tterr << "setup the points" << endl;
  
  TESSTOOL::min_scalar =  1.0e30;
  TESSTOOL::max_scalar = -1.0e30;

  double x1, x2, y1, y2;
  double ll[2] = { 1e20,  1e20};
  double ur[2] = {-1e20, -1e20};

  while(_mpistrm->nextRecord()) {
    if(recordNum == 0) {
      numTiles = _mpistrm->getNumRecords();;
      scalars -> Allocate(numTiles);
      points  -> Allocate(4*numTiles);
      tterr << "Creating vtk Data source for #tiles " << numTiles << endl;
    }
    tterr << "Record Num=" << recordNum << endl;
    tterr << _mpistrm->getBuffer()->toString() << endl;
    recordNum++;
    tileid = _mpistrm->getTileId();
    Tile * tile = _tess->GetTile(tileid);
    Node * node = tile->GetNode();
    // node->print(cout);
    scalar = getScalar();
    offset = addTileToPoints(node, offset, points, cells, scalars, scalar);
    TESSTOOL::min_scalar = min<double>(TESSTOOL::min_scalar, scalar);
    TESSTOOL::max_scalar = max<double>(TESSTOOL::max_scalar, scalar);

    node->GetTile()->corners(x1, x2, y1, y2);
    if (ll[0] >= x1 && ll[1] >= y1) {
      ll[0] = x1;
      ll[1] = y1;
    }
    if (ur[0] <= x2 && ur[1] <= y2) {
      ur[0] = x2;
      ur[1] = y2;
    }

  }
    
  
  vtkIndent ind;

  tterr << "Setting points and scalars in grid, records=" << recordNum << endl;
  tterr << "Corners: [" << ll[0] << ", " << ll[1] << "] "
	<< " [" << ur[0] << ", " << ur[1] << "]" << endl;
  tterr << endl;
  tterr << "POINTS:" << endl;
  points  -> PrintSelf(tterr, ind);
  tterr << endl;
  tterr << "SCALARS:" << endl;
  scalars -> PrintSelf(tterr, ind);
  tterr << endl;
  
  _polydata->SetPoints(points);
  _polydata->SetPolys(cells);
  _polydata->GetCellData()->SetScalars(scalars);

  tterr << "We have " << _polydata->GetNumberOfCells() << " cells (polydata)" << endl;

  double bb[6];
  for (vtkIdType i=0; i< _polydata->GetNumberOfCells(); i++) {
    _polydata->GetCell(i)->GetBounds(bb);
    tterr << setw(4) << i;
    for (int j=0; j<4; j++) tterr << setw(8) << bb[j];
    tterr << setw(10) << scalars->GetValue(i) << endl;
  }

  TESSTOOL::set_scalars();
}

void TessToolVTK::initializeVTK()
{
  ++tesstoolMutex;

  // initialize indices of scalar in the MPI record stream
  //
  if ( ! _scalarNameInitialized )
    setScalarName(string("mean"));
  
  if ( ! _scalarInfoNameInitialized )
    setScalarInfoName(string("mean"));
  
  updateScalarIndices();
  
  // Create the Polygonal Data
  //
  _polydata = vtkPolyData::New();

  _lut =  vtkLookupTable::New();
  _lut->SetHueRange(0.6666667, 1);
  _lut->SetSaturationRange(1, 1);
  _lut->SetValueRange(1, 1);
  _lut->SetNumberOfTableValues(256);
  _lut->Build();
  
  _polymapper = vtkPolyDataMapper::New();
  _polymapper->SetInput(_polydata);
  _polymapper->ScalarVisibilityOn();
  _polymapper->SetLookupTable(_lut);
  _lut->Delete();

  _polymapper->UseLookupTableScalarRangeOn();
  _polymapper->SetScalarModeToUseCellData();
  
  // Create an actor.
  //
  vtkActor* polyactor = vtkActor::New();
  polyactor->SetMapper(_polymapper);
  
  // Create the usual rendering stuff
  //
  _renWin = TESSTOOL::iren->GetRenderWindow();
  _renWin->AddRenderer(_renderer);
  _renderer->Delete();
  _annotatePick->renWin = _renWin;
  TESSTOOL::iren->SetPicker(_picker);
  TESSTOOL::iren->UpdateSize(_XWinWidth, _XWinHeight);
  
  _renderer->AddActor(polyactor);
  _renderer->SetBackground(1,1,1);
  _renderer->GetActiveCamera()->Elevation(0.0);
  _renderer->GetActiveCamera()->Azimuth(0.0);
  _renderer->GetActiveCamera()->Zoom(0.50);
  _renderer->GetActiveCamera()->Dolly(0.25);
  
  _initVTK = true;
  --tesstoolMutex;
}

static int added=0;

int TessToolVTK::addTileToPoints(Node *node, int offset,
				 vtkPoints *points, 
				 vtkCellArray *cells, 
				 vtkFloatArray *scalars, double sclr)
{
  double minx, maxx, miny, maxy;
  float scalar = sclr;
  
  if (node == NULL) 
    { 
      return offset; 
    }
  else
    {
      Tile *tile = node->GetTile();

      tile->corners(minx, maxx, miny, maxy);

      tterr << " Adding Point " << added++ << " with id " << node->ID() << " ";
      tterr << "xmin[" << added << "]=" << minx << "; ";
      tterr << "xmax[" << added << "]=" << maxx << "; ";
      tterr << "ymin[" << added << "]=" << miny << "; ";
      tterr << "ymax[" << added << "]=" << maxy << "; ";
      tterr << "scalar=" << scalar;
      tterr << endl;
      
      //      node->print(tterr);
      
      points->InsertNextPoint(minx, miny, 0);
      points->InsertNextPoint(maxx, miny, 0);
      points->InsertNextPoint(maxx, maxy, 0);
      points->InsertNextPoint(minx, maxy, 0);
      
      cells->InsertNextCell(4);
      cells->InsertCellPoint(offset+0);
      cells->InsertCellPoint(offset+1);
      cells->InsertCellPoint(offset+2);
      cells->InsertCellPoint(offset+3);
      
      scalars->InsertNextTuple(&scalar);

      offset +=4;
      
      return offset;
    }
}


void TessToolVTK::run()
{
  // initialize tessellation display to wireframe
  // construct vtk data source
  // sleep on semaphore and update display when woken up

  while(1)
    {
      // wait until we have new data to display
      ++tesstoolMutex;
      tterr << "TessToolVTK:: Run got Mutex for post" << endl;
      _producer->post();
      --tesstoolMutex;
      tterr << "TessToolVTK:: Run released Mutex after post" << endl;
      tterr << "TessToolVTK:: Run starting wait" << endl;
      _vtksem->wait();
      tterr << "TessToolVTK:: Run returned from  wait" << endl;
      ++tesstoolMutex;
      tterr << "TessToolVTK:: Run got Mutex" << endl;
      if (!_initTessellation) {
        initializeTessellation();
        tterr << "TessToolVTK:: Called initializeTessellation() from run" << endl;
      }
      if (!_initVTK) {
        initializeVTK();
        tterr << "TessToolVTK:: Called initializeVTK() from run" << endl;
      }
      if (!_mpistrm) break;
      RenderNewData();
      --tesstoolMutex;
      tterr << "TessToolVTK:: Run released Mutex" << endl;
    }

  return;
}


void TessToolVTK::RenderNewData()
{
  createNewDataSet();
  Render();
}

void TessToolVTK::Render()
{
  updateScalarIndices();
  
  //
  // set the color table mapping
  //

				// Enforce the ranges
				// 
  _hueLow = min<float>(_hueLow, 1.0);
  _hueLow = max<float>(_hueLow, 0.0);

  _hueHigh = min<float>(_hueHigh, 1.0);
  _hueHigh = max<float>(_hueHigh, 0.0);

  _saturationLow = min<float>(_saturationLow, 1.0);
  _saturationLow = max<float>(_saturationLow, 0.0);

  _saturationHigh = min<float>(_saturationHigh, 1.0);
  _saturationHigh = max<float>(_saturationHigh, 0.0);

  _valueLow = min<float>(_valueLow, 1.0);
  _valueLow = max<float>(_valueLow, 0.0);

  _valueHigh = min<float>(_valueHigh, 1.0);
  _valueHigh = max<float>(_valueHigh, 0.0);

				// Set the current values
				//
  _lut->SetHueRange(_hueLow, _hueHigh);
  _lut->SetSaturationRange(_saturationLow, _saturationHigh);
  _lut->SetValueRange(_valueLow, _valueHigh);
  _lut->Build();

				// Update the scalar range
				//
  _polymapper->SetScalarRange(TESSTOOL::min_scalar, TESSTOOL::max_scalar);
  _polymapper->UseLookupTableScalarRangeOff();
  _polymapper->ScalarVisibilityOn();
  
  _annotatePick->textActor->VisibilityOff();
  
  double* _ans = _polymapper->GetScalarRange();
  tterr << "Min scalar=" << _ans[0] << "  Max scalar=" << _ans[1] << endl;
}


void TessToolVTK::setHueLow(float l)
{
  _hueLow = l;
}

void TessToolVTK::setHueHigh(float l)
{
  _hueHigh = l;
}

void TessToolVTK::setSaturationLow(float l)
{
  _saturationLow = l;
}

void TessToolVTK::setSaturationHigh(float l)
{
  _saturationHigh = l;
}

void TessToolVTK::setValueLow(float l)
{
  _valueLow = l;
}

void TessToolVTK::setValueHigh(float l)
{
  _valueHigh = l;
}

void TessToolVTK::setAlphaLow(float l)
{
  _alphaLow = l;
}

void TessToolVTK::setAlphaHigh(float l)
{
  _alphaHigh = l;
}

// char *n versions called from GUI
void TessToolVTK::setScalarName(char *n)
{
  string s(n);
  _tmpScalarNameString = s;
  setScalarName(s);
}

void TessToolVTK::setScalarInfoName(char *n)
{
  string s(n);
  _tmpScalarInfoNameString = s;
  setScalarInfoName(s);
}

void TessToolVTK::setScalarName(string s)
{
  _scalarNameString = s;
  _scalarNameChanged = true;
  _scalarNameInitialized = true;
  TESSTOOL::scalarNameString = s;
  tterr << "Set scalar name to: " << s << endl;
}

void TessToolVTK::setScalarInfoName(string s)
{
  _scalarInfoNameString = s;
  _scalarInfoNameChanged = true;
  _scalarInfoNameInitialized = true;
  TESSTOOL::scalarInfoNameString = s;
  tterr << "Set scalar info name to: " << s << endl;
}

void TessToolVTK::updateScalarIndices()
{
  if (_scalarNameChanged)
    tterr << "In updateScalarIndices: Scalar name has changed" << endl;
  else
    tterr << "In updateScalarIndices: NO change in scalar name" << endl;

  if (_scalarInfoNameChanged)
    tterr << "In updateScalarIndices: Scalar info name has changed" << endl;
  else
    tterr << "In updateScalarIndices: NO change in scalar info name" << endl;

  if (_scalarInfoNameChanged) {
    _scalarInfoIndex = getIndex(_scalarInfoNameString);
    _scalarInfoNameChanged = false;
  }
  if (_scalarNameChanged) {
    _scalarIndex = getIndex(_scalarNameString);
    _scalarNameChanged = false;
  }
}

int TessToolVTK::getIndex(string s)
{
  // lookup field with name Scalar, if absent, return the field after TileId
  int index;
  
  RecordType *rt = _mpistrm->getType();
  try {
    index = rt->getFieldIndex(s);
  } catch (BIEException e) {

    tterr << " Error setting scalar: " << s << endl;
    tterr << " Number of fields: " << rt->numFields() << endl;

    index = 3;
    _scalarNameString = rt->getFieldName(3);
    TESSTOOL::scalarNameString = _scalarNameString;
  }

  tterr << "TessToolVTK::getIndex(" << s << "):Scalar index=" << index << endl;
  return index;
}

double TessToolVTK::getScalar()
{
  double scalar;
  try {
    scalar = _mpistrm->getRealValue(_scalarIndex);
  } catch (BIEException e) {
    tterr << "Error on index=" << _scalarIndex
	  << " field=" << _scalarNameString
	  << endl;
    try {
      scalar = _mpistrm->getIntValue(_scalarIndex);
    }
    catch (BIEException e) {
      tterr << "TessToolVTK::getScalar(): Invalid data type" << endl;
      scalar = 0.0;
    }
  }

  return scalar;
}

double TessToolVTK::getScalarInfo()
{
  double scalar;
  try {
    scalar = _mpistrm->getRealValue(_scalarInfoIndex);
  } catch (NoSuchFieldException e) {
    try {
      scalar = _mpistrm->getIntValue(_scalarInfoIndex);
    }
    catch (BIEException e) {
      tterr << "TessToolVTK::getScalar(): Invalid data type" << endl;
      scalar = 0.0;
    }
  }

  return scalar;
}

void vtkBIECallback::Execute(vtkObject *caller, unsigned long eid, void *cdata)
{
  vtkCellPicker *picker = reinterpret_cast<vtkCellPicker*>(caller);
  tterr << "BIE callback: caller=" << caller << " " << eid << " " << cdata 
	<< " cellID=" << picker->GetCellId() << endl;

  if(picker->GetCellId() < 0) {
    textActor->VisibilityOff();
  } else {
				// Debug print the cell data
    picker->GetDataSet()->GetCell(picker->GetCellId())->Print(cerr);

#if (VTK_MAJOR_VERSION>4 || ((VTK_MAJOR_VERSION>=4) && (VTK_MINOR_VERSION>=4)))
    double *point;
    double *pos;
#else
    float *point;
    float *pos;
#endif
    point = picker->GetSelectionPoint();
    tterr << "Selection Point " << point[0] << " " << point[1]  << endl;
    
    pos = picker->GetPickPosition();
    
    tterr << "Pick Position " << pos[0] 
	  << " " << pos[1] 
	  << " " << pos[2] 
	  << endl;
    
    stringstream position(stringstream::out);
    position << pos[0] << " " << pos[1] << endl;
    
    double value;

    tterr << "String=" << position.str() << endl;
    tterr << "TextActor=" << textActor << endl;
    tterr << "Value=" 
	  << (value = picker->GetDataSet()->GetCellData()->
	      GetScalars()->GetComponent(picker->GetCellId(),0)) << endl;
    
    picker->GetDataSet()->GetCellData()->GetScalars()->Print(cerr);

    tterr << endl;
    for (vtkIdType i=0; i<picker->GetDataSet()->
	   GetCellData()->GetScalars()->GetNumberOfTuples(); i++) {
      tterr << setw(5) << i << setw(15) 
	    << picker->GetDataSet()->
	GetCellData()->GetScalars()->GetComponent(i, 0) << endl;
    }
    tterr << endl;

    vtkPoints *pts = 
      picker->GetDataSet()->GetCell(picker->GetCellId())->GetPoints();
    vtkIdType npts = pts->GetNumberOfPoints();
    for (int i=0; i<npts; i++) {
      tterr << "  [" << i << "]=";
      for (int j=0; j<3; j++) tterr << " " << pts->GetPoint(i)[j];
      tterr << endl;
    }

    TESSTOOL::data_popup(value, npts, pts);

    textMapper->SetInput(position.str().c_str());
    textActor->SetPosition(point[0], point[1]);
    textActor->VisibilityOn();
    renWin->Render();
  }
}

void vtkGtkCallback::Execute(vtkObject *caller, unsigned long eid, void *cdata)
{
  vtkCellPicker *pickr = reinterpret_cast<vtkCellPicker*>(caller);
  tterr << "GTK callback: caller=" << caller << " " << eid << " " << cdata 
	<< " cellID=" << pickr->GetCellId() << endl;

  if(pickr->GetCellId() < 0) {
    textActor->VisibilityOff();
  } else {
				// Debug print the cell data
    pickr->GetDataSet()->GetCell(pickr->GetCellId())->Print(tterr);

#if (VTK_MAJOR_VERSION>4 || ((VTK_MAJOR_VERSION>=4) && (VTK_MINOR_VERSION>=4)))
    double *point;
    double *pos;
#else
    float *point;
    float *pos;
#endif
    point = pickr->GetSelectionPoint();
    tterr << "Selection Point " << point[0] << " " << point[1]  << endl;
    
    pos = pickr->GetPickPosition();
    tterr << "Pick Position " << pos[0] << " " << pos[1] << " " << pos[2] << " " << endl;
    
    double value = pickr->GetDataSet()->GetCellData()->
      GetScalars()->GetComponent(pickr->GetCellId(),0);

    stringstream position(stringstream::out);
    position << "["
	     << setprecision(3) << pos[0] 
	     << ", " 
	     << setprecision(3) << pos[1] 
	     << "]=" << value << endl;
    
    tterr << "String=" << position.str() << endl;
    tterr << "TextActor=" << textActor << endl;
    tterr << "Value=" << value << endl;

    tterr << endl;
    vtkIdType number = pickr->GetDataSet()->
      GetCellData()->GetScalars()->GetNumberOfTuples();

    for (vtkIdType i=0; i<number ; i++) {
      tterr << setw(5) << i << setw(15) 
	    << pickr->GetDataSet()->GetCellData()->
	GetScalars()->GetComponent(i, 0) << endl;
    }
    tterr << endl;

    vtkPoints *pts = 
      pickr->GetDataSet()->GetCell(pickr->GetCellId())->GetPoints();
    
    vtkIdType npts = pts->GetNumberOfPoints();
    for (int i=0; i<npts; i++) {
      tterr << "  [" << i << "]=";
      for (int j=0; j<3; j++) tterr << " " << pts->GetPoint(i)[j];
      tterr << endl;
    }

    pickr->GetDataSet()->GetCellData()->GetScalars()->Print(tterr);

    TESSTOOL::data_popup(value, npts, pts);
    
    textMapper->SetInput(position.str().c_str());
    textActor->SetPosition(point[0], point[1]);
    textActor->VisibilityOn();
    renWin->Render();
  }
}


