## Makefile for the tesstool directory of galaxy.
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

## Process this file with automake to produce Makefile.in

lib_LTLIBRARIES = libTessTool.la

# Include flags.  It does not matter that some of these locations might not exist.
GTK2_INCLUDE = `PKG_CONFIG_PATH=@bie_gtk2_pkgconfig@ pkg-config --cflags  gtk+-2.0`
GTK2_LIBS = `PKG_CONFIG_PATH=@bie_gtk2_pkgconfig@ pkg-config --libs  gtk+-2.0`

GTKGL_INCLUDE = `PKG_CONFIG_PATH=@bie_gtk2_pkgconfig@ pkg-config --cflags gtkglext-1.0`
GTKGL_LIBS = `PKG_CONFIG_PATH=@bie_gtk2_pkgconfig@ pkg-config --libs gtkglext-1.0`

AM_CPPFLAGS = -I$(top_srcdir)/include -I$(top_srcdir)/libVector		\
-I$(top_srcdir)/libsrnd -I$(top_srcdir)/libcutils @INCLUDES_ALL@	\
@bie_mpi_include@ @bie_vtk_include@ -I$(top_srcdir)/persistence		\
@INCLUDES_CCXX@ $(GTK2_INCLUDE) $(GTKGL_INCLUDE)

libTessTool_la_SOURCES = TessToolReceiver.cc CliCommandThread.cc \
	CliOutputReceiveThread.cc TessToolVTK.cc tesstoolMutex.cc \
	TessToolConsole.cc TessToolGUI.cc callbacks.cc interface.cc \
	gtkvtk.cc vtkGtkRenderWindowInteractor.cc TessToolController.cc \
	TessToolReader.cc TessToolGPL.cc debug.cc

noinst_HEADERS = ../include/*.h ./*.h

AM_CXXFLAGS = -std=c++11 -Wall -Wno-deprecated -Wno-non-virtual-dtor -fPIC @bie_cxxflags@ @bie_debug_flags@ -D_REENTRANT 

bin_PROGRAMS = tesstooldriver tesstoolviewer tesstoolplotter tesstoolwriter 


# Libraries to link in.
TTLDFLAGS =	-L/usr/X11R6/lib -L./.libs @LDFLAGS_ALL@

TTLDADD =	@LDADD_ALL@ @bie_mpi_lib@ @bie_vtk_lib@ \
		-lTessTool @bie_vtk_ldlib@ \
		-lm -lnetcdf -lreadline -ltermcap @LIBADD_CCXX@ \
		-lpthread

tesstooldriver_SOURCES = tesstooldriver.cc
tesstooldriver_DEPENDENCIES = libTessTool.la
tesstooldriver_LDFLAGS = $(LDFLAGS) $(GTK2_LIBS) $(GTKGL_LIBS) $(TTLDFLAGS) 
tesstooldriver_LDADD = $(TTLDADD)

tesstoolviewer_SOURCES = tesstoolviewer.cc
tesstoolviewer_DEPENDENCIES = libTessTool.la
tesstoolviewer_LDFLAGS = $(LDFLAGS) $(GTK2_LIBS) $(GTKGL_LIBS) $(TTLDFLAGS) 
tesstoolviewer_LDADD = $(TTLDADD)


tesstoolplotter_SOURCES = tesstoolplotter.cc
tesstoolplotter_DEPENDENCIES = libTessTool.la
tesstoolplotter_LDFLAGS = $(LDFLAGS) $(GTK2_LIBS) $(GTKGL_LIBS) $(TTLDFLAGS) $(TTLDADD)

tesstoolwriter_SOURCES = tesstool_writer.cc TessToolWriter.cc
tesstoolwriter_DEPENDENCIES = libTessTool.la
tesstoolwriter_LDFLAGS = $(LDFLAGS) $(GTK2_LIBS) $(GTKGL_LIBS) $(TTLDFLAGS) $(TTLDADD)

## tesstool/Makefile.am ends here
