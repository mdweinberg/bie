#ifndef _DEBUG_H
#define _DEBUG_H

#include <vtk/vtkSphereSource.h>
#include <vtk/vtkCubeSource.h>
#include <vtk/vtkPolyDataMapper.h>
#include <vtk/vtkActor.h>
#include <vtk/vtkProperty.h>
#include <vtk/vtkRenderWindow.h>
#include <vtk/vtkRenderer.h>

namespace TESSTOOL {
  
  //! Make a silly scene for debugging the VTK engine
  class create_scene
    {
    private:
      vtkSphereSource *sphere;
      vtkPolyDataMapper *sphere_map;
      vtkActor *sphere_actor;
      vtkCubeSource *cube;
      vtkPolyDataMapper *cube_map;
      vtkActor *cube_actor;
      vtkRenderer *ren1;
    public:
      
      //! Constructor for the scene
      create_scene();
      //! Destructor for the scene
      ~create_scene();
      //! Return the VTK renderer for the scene
      vtkRenderer* get_renderer();
    };
  
}

#endif
