
#include "gvariable.h"
#include "RecordType.h"
#include "TessToolWriter.h"

using namespace std;
using namespace BIE;

int TessToolWriter::_instance = 0;

TessToolWriter::TessToolWriter(const string ofile, const MPI_Comm bieComm)
{
  _ofile = ofile;
  _bieComm = bieComm;
  _first=true;
  _tmList = 0;
  _request = 0;
  _status = 0;
  _flag = 0;
  _completed = 0;
  _numTMs = 0;
  _scratchBufferSizeInWords = 0;
  _scratchDtypeBuffer = 0;
  _currentBuffer = 0;

  ostringstream ostr;
#ifdef DEBUG_TESSTOOL
  ostr << "wwerr." << _instance++;
#else
  ostr << "/dev/null";
#endif
  wwerr.open(ostr.str().c_str());

  wwerr << "TessToolWriter::TessToolWriter() constructed"
	<< " comm=" << _bieComm	<< endl << flush;
}

TessToolWriter::~TessToolWriter()
{
  if (_numTMs) {
    delete [] _tmList;
    delete [] _request;
    delete [] _status;
    delete [] _flag;
    delete [] _completed;
    for (int i=0; i<_numTMs; i++) delete [] (int *)_receivedBuffer[i];
    delete [] _receivedBuffer;
    delete [] _scratchDtypeBuffer;
    delete [] (int *)_currentBuffer;
  }    
  wwerr.close();
}


bool TessToolWriter::CacheData()
{
  //
  // delete the structs from previous iteration
  //
  int i;
  int old_bufSizeInWords, old_numTMs;

  old_bufSizeInWords = _bufSizeInWords;
  old_numTMs = _numTMs;
  RecordType *old_rtype = _rtype;

  MPI_Status status;
  
  wwerr << "TessToolWriter::CacheData() entered" 
	<< " comm=" << _bieComm	<< endl << flush;

  // request other processes to sample computation 
  //
  MPI_Recv((void *)&_level, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&_step, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&_iter, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  MPI_Recv((void *)&_numTMs, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);

  wwerr << "TessToolWriter::CacheData() numTMs=" << _numTMs << endl;
  if (!_numTMs > 0) {
    wwerr << "Received number of tilemasters ! >0 " << endl;

    return false;
  }

  _tmList = new int [_numTMs];

  int count=_numTMs;

  wwerr << "TessToolWriter::CacheData() calling Recv for tmList" 
	<< endl << flush;

  MPI_Recv((void *)_tmList, count, MPI_INT, 0,
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  wwerr << "TessToolWriter::CacheData() returned from Recv" << endl << flush;

  for(i=0; i<_numTMs; i++) {
    wwerr << " tmList[" << i << "]= " << _tmList[i] << endl << flush;
  }

  wwerr << "TessToolWriter::CacheData() calling Recv for bufSize" 
	<< endl << flush;

  MPI_Recv((void *)&filter_sessionId, 1, MPI_INT, 0,
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);

  wwerr << "TessToolWriter::CacheData() got sessionID=" 
	<< filter_sessionId << endl << flush;

  MPI_Recv((void *)&filter_capacityInRecords, 1, MPI_INT, 0,
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);

  wwerr << "TessToolWriter::CacheData() got capacity=" 
	<< filter_capacityInRecords << endl << flush;

  MPI_Recv((void *)&filter_recordSize, 1, MPI_INT, 0,
	   BIE_SYNCHRONIZE_TAG, _bieComm, &status);

  wwerr << "TessToolWriter::CacheData() got record size=" 
	<< filter_recordSize << endl << flush;

  _bufSizeInWords = filter_capacityInRecords * filter_recordSize / sizeof(int);
  

  wwerr << "TessToolWriter::CacheData() bufSizeInWords=" 
	<< _bufSizeInWords << endl << flush;

  if (_numTMs > old_numTMs ) {
    if (!_first) {
      delete [] _request;
      delete [] _status;
      delete [] _flag;
      delete [] _completed;
    }
    _request = new MPI_Request [_numTMs];
    _status = new MPI_Status [_numTMs];
    _flag = new int [_numTMs];
    _completed = new int [_numTMs];
  }

  // if the number of TMs has changed, or buffer size is different
  //
  if (_first || _numTMs > old_numTMs || _bufSizeInWords > old_bufSizeInWords) {
    wwerr << "Allocate buffers" << endl << flush;

				// delete previous buffers ... 
    if ( !_first ) {
      for(i=0; i<old_numTMs; i++) {
	delete [] (int *) _receivedBuffer[i];
      }
      delete [] (int *) _currentBuffer;
    }
    _receivedBuffer = new void* [_numTMs];

				// allocate new buffers
    for(i=0; i<_numTMs; i++) {
      _receivedBuffer[i] = (void *) new int[_bufSizeInWords];
    }   
    _currentBuffer = (void *) new int[_bufSizeInWords];
  }

				// Get datatype 
  int dtsize;
  MPI_Recv(&dtsize, 1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _bieComm, &status);
  wwerr << "TessToolWriter::CacheData() dtsize=" << dtsize << endl << flush;

  if(_scratchBufferSizeInWords < dtsize) {
    delete [] _scratchDtypeBuffer;
    _scratchBufferSizeInWords = dtsize;
    _scratchDtypeBuffer = new char [_scratchBufferSizeInWords * sizeof(int)];
  } 

  bzero(_scratchDtypeBuffer, _scratchBufferSizeInWords);

  MPI_Recv(_scratchDtypeBuffer, dtsize, MPI_BYTE, 0, 
	   BIE_DATATYPE_TAG, _bieComm, &status);

  wwerr << "TessToolWriter::CacheData() dtype:"  << endl
	<< _scratchDtypeBuffer << endl << flush;

				// construct the datatype
  if (!_first) {
    delete _ris;
    delete _iss;
  }

  _str = string(_scratchDtypeBuffer);
  _iss = new istringstream(_str);
  _ris = new RecordInputStream_Ascii (_iss);
  _rtype = _ris->getType();

  wwerr << "TessToolWriter::CacheData() numfields= " << _rtype->numFields() 
	<< " _rtype=" << _rtype->toString() << endl << flush;

  _typeSizeInBytes = MPIStreamFilter::getRecordBufferSizeInBytes(_rtype);
  _numRecordsInBuffer = (_bufSizeInWords *sizeof(int))/_typeSizeInBytes;

  wwerr << "_bufSizeInWords=" << _bufSizeInWords 
	<< " _typeSizeInBytes=" << _typeSizeInBytes << endl << flush;

  if(_first || !_rtype->equals(old_rtype)) {
    wwerr << "Calling convertRecordToDatatype" << endl;
    _mpidtype = MPICommunicationSession::convertRecordToDatatype(_rtype);
    wwerr << "returned from convertRecordToDatatype " << _mpidtype 
	  << endl << flush;
  }

  //
  // Cache the output
  //

  ostringstream ostr;		// Make the output file name
  if (_level >= 0)
    ostr << _ofile << "." << _level << "_" << _step;
  else
    ostr << _ofile << "." << _iter;

				// Open the file
  ofstream cache(ostr.str().c_str());

  wwerr << "TessToolWriter::CacheData() file, " << ostr << ", is ";
  if (cache)  wwerr << "OK" << endl << flush;
  else wwerr << "BAD" << endl << flush;
	 
  _currentBuffer = (void *) new int[_bufSizeInWords];

  cache.write(reinterpret_cast<char *>(&_bufSizeInWords), sizeof(int));
  cache.write(reinterpret_cast<char *>(&filter_recordSize), sizeof(int));
  cache.write(reinterpret_cast<char *>(&dtsize), sizeof(int));
  cache.write(reinterpret_cast<char *>(_scratchDtypeBuffer), dtsize);
  //
  // set up for checking the completion of send
  //
  _numCompleted=0;
  for(i=0; i<_numTMs; i++) {
    _completed[i] = 0;
    wwerr << "setting completed " << i << endl << flush;
  }   
  
  //
  // poll nodes to pull data
  //
  for(i=0; i<_numTMs; i++) {
    wwerr << "setting up Irecv for tm " << i << endl << flush;

    wwerr << &_receivedBuffer[i] << " " << _numRecordsInBuffer 
	  << " " << _mpidtype << " " 
	  <<  _tmList[i] << " "  << _bieComm << " " <<  &_request[i] 
	  << endl << flush;

    MPI_Irecv(_receivedBuffer[i], _numRecordsInBuffer, _mpidtype, \
	      _tmList[i], BIE_DATA_TAG, _bieComm, &_request[i]);         
    wwerr << "TessToolWriter::Irecv started from " << _tmList[i] 
	  << " :: to :: 0" << endl << flush;
  }

  _first = false;

  unsigned long icur=0;

  // continue pull data from the node until done
  //
  while(_numCompleted < _numTMs) {

    i = (icur++ % _numTMs);

    if (_completed[i]) continue;
      
    wwerr << "TesstoolWriter::GetData testing for data on=" << i
	  << " . . . " << flush;

    MPI_Test( &_request[i], &_flag[i], &_status[i] );

    if (_flag[i])
      wwerr << "got some!" << endl << flush;
    else
      wwerr << "no data." << endl << flush;

    if (_flag[i]) {		// Receive is completed . .  
				// How many did we get?
      int numRecordsReceived;
      MPI_Get_count( &_status[i], _mpidtype, &numRecordsReceived);

      wwerr << "TesstoolWriter::GetData i=" << i << " got "
	    << numRecordsReceived << " records" << endl << flush;
      
      // At end of stream check?
      if (numRecordsReceived==1) {
	bool atEnd = true;
	
	for (int j=0; j<filter_recordSize; j++)
	  if (reinterpret_cast<char*>( _receivedBuffer[i])[j] 
	      != BIE_ENDOFDATA_MARKER) atEnd = false;

	if (atEnd) {
	  _completed[i] = 1;
	  _numCompleted++;
	    
	  wwerr <<"TesstoolWriter::Recv complete from " 
		<< _tmList[i] << ":: to: " << 0 
		<< " tag:" <<  BIE_DATATYPE_TAG <<"\n";
	}
      }

      if (!_completed[i]) {	// Got some good data:
				// Copy data to cache

	cache.write(reinterpret_cast<char *>(&numRecordsReceived), 
		    sizeof(int));
	cache.write(reinterpret_cast<char *>(_receivedBuffer[i]), 
		    numRecordsReceived * filter_recordSize);


	wwerr << "numRecordsReceived=" << numRecordsReceived << endl;
	
	// Get more data on this channel
	MPI_Irecv(_receivedBuffer[i], _numRecordsInBuffer, 
		  _mpidtype, _tmList[i], 
		  BIE_DATA_TAG, _bieComm, &_request[i]);         
	
      }

    }
    
  }   

  wwerr << "TessToolWriter::CacheData(): data writing completed" << endl;

				// No more data on any channel
  return true;
}

