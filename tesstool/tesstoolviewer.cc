#include <TessToolController.h>

#include <fstream>

using namespace BIE;

namespace TESSTOOL 
{
  bool work = true;
  bool viewer = true;
}

int main(int argc, char *argv[])
{           
  rferr.open("errfile.rx");
  
  Semaphore *_mainSem = new Semaphore(0);
  TessToolController * viewer = new TessToolController(argc, argv, _mainSem);

  rferr << "tesstoolviewer: starting controller" << endl;
  cerr  << "tesstoolviewer: starting controller" << endl;

  viewer->start();
  
  _mainSem->wait();

  rferr << "tesstoolviewer: exiting" << endl;
  cerr  << "tesstoolviewer: exiting" << endl;

  cerr  << "tesstoolviewer: closing output stream . . . " << flush;
  rferr.close();
  cerr << "done" << endl;

  return 0;
}

