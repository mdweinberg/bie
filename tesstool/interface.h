#ifndef _INTERFACE_H
#define _INTERFACE_H

#include <string>
#include <vector>
// #include <vtk/vtkIdType.h>
#include <vtk/vtkPoints.h>
#include "debug.h"

namespace TESSTOOL {
  void initializeGTK(int _argc, char *_argv[]);
  GtkWidget* create_window1 (void);
  void pop_message(const std::string& message);
  void data_popup(double value, vtkIdType npts, vtkPoints *pts);
  void initializeGTK(int _argc, char **_argv);
  void pop_fieldnames(std::vector<std::string>& nlist);
  void rebuild_menu(std::vector<std::string>& list);
  void set_scalars();
  int gtkvtk (int argc, char **argv);
  void setWorkingDirName(char *);
  void setDataFileName(char *);
  extern GtkWidget *vtk_window;
  extern GtkWidget *window1;
  extern vtkGtkRenderWindowInteractor *iren;
  extern GtkWidget *BIEOutputView;
  extern GtkWidget *scalar_menu;
  extern std::vector<GtkWidget*> menu_items;
  extern std::string scalarNameString;
  extern std::string scalarInfoNameString;
  extern create_scene *scene;
  extern GtkWidget* scalarMin;
  extern GtkWidget* scalarMax;
  extern GtkWidget* scalarMinEntry;
  extern GtkWidget* scalarMaxEntry;
  extern GtkWidget* CLIScriptView;
  extern GtkWidget* WorkingDirView;
  extern GtkWidget* DataFileView;
  extern double min_scalar, max_scalar;
  extern bool work;
  extern bool viewer;
}

#endif

