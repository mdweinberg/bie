#include <TessToolController.h>
#include <TessToolConsole.h>

// using namespace BIE;

ConsoleLogThread::ConsoleLogThread(CliOutputReceiveThread *rcvthrd) 
{
  _clircvthrd = rcvthrd;
  _console.open("console.log");
  _console << "Starting Log" << endl;
  _console.flush();
}

ConsoleLogThread::~ConsoleLogThread()
{
  _console.close();
}

void ConsoleLogThread::run() {
  const int blen=1<<12;
  char *lbuffer;
  int len;
  rferr << "TessToolConsole::Run(): Entering" << endl;
  Semaphore *_consumer = new Semaphore(0);
  _clircvthrd->setConsumerSemaphore(_consumer);
  work=true;
  Semaphore *_producer = _clircvthrd->getSemaphore();
  rferr << "TessToolConsole::Run(): Entering loop" << endl;

  // wait until the CliOutputReceiver thread starts up
  while (!_clircvthrd->isReady()) {}

  while(work) {
    rferr << "TessToolConsole::Run(): Telling producer" << endl;
    _producer->post();
    rferr << "TessToolConsole::Run(): Waiting on consumer" << endl;
    _consumer->wait();
    rferr << "TessToolConsole::Run(): Getting stream from producer" << endl;
    istream *fromcli = _clircvthrd->getReply();
    rferr << "TessToolConsole::Run(): received output from cli" << endl;
    if (fromcli && fromcli->good()) { 
      while (!fromcli->eof()) {
	rferr << "TessToolConsole::Run(): writing to console" << endl;
	lbuffer = (char *) malloc(blen);
	fromcli->getline(lbuffer, blen-1);
	len = strlen(lbuffer);
	lbuffer[len] = '\n';
	lbuffer[len+1] = '\0';
	rferr << "TessToolConsole::Run(): writing to console " << lbuffer << endl;
	_controller->updateBIEOutputView(lbuffer);
      }
      delete fromcli;
      _console.flush();
    } else {
      //lbuffer[0]='\0';
      //rferr << "TessToolConsole::Run(): writing to console " << lbuffer << endl;
      //_controller->updateBIEOutputView(lbuffer);
    }
  }
  _clircvthrd->setStop();
}

void ConsoleLogThread::setController(TessToolController *controller) 
{
   _controller = controller;
}
