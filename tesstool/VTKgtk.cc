#include "vtk/vtkPolyDataMapper.h"
#include "vtk/vtkPolyDataMapper.h"
#include "vtk/vtkActor.h"
#include "vtk/vtkRenderWindow.h"
#include "vtk/vtkXOpenGLRenderWindow.h"
#include "vtk/vtkRenderer.h"
#include "vtk/vtkRenderWindowInteractor.h"
#include "vtk/vtkStructuredGridGeometryFilter.h" 
#include "vtk/vtkStructuredGridReader.h" 
#include "vtk/vtkContourFilter.h"
#include "vtk/vtkPolyDataReader.h"
#include "vtk/vtkLookupTable.h"
#include "vtk/vtkCamera.h"
#include "vtk/vtkPointPicker.h"
#include "vtk/vtkCellPicker.h"
#include "vtk/vtkCallbackCommand.h"

#include <X11/X.h>
#include <X11/keysym.h>
#include "vtk/vtkXRenderWindowInteractor.h"
#include "vtk/vtkInteractorStyle.h"
#include "vtk/vtkXOpenGLRenderWindow.h"
#include "vtk/vtkCallbackCommand.h"

#include "vtk/vtkTextMapper.h"
#include "vtk/vtkTextProperty.h"
#include "vtk/vtkActor2D.h"
#include "vtk/vtkPoints.h"

#include "vtk/vtkActor.h"
#include <X11/Shell.h>

#include <sstream>

static int buttons_mask;
static double last_x, last_y;

extern "C" {
  void setVTKWindow(int, int);
  void *getVTKRenderWindow();
  void *getVTKRenderer();
  void *getVTKCellPicker();

  static bool initRenderWin=false;

  void initRendererWin(int wdt, int ht, int pos) {
    if (!initRenderWin) {
      // set the xid, ht, width in TessToolGui/TessToolController 
      // to be used in TessToolVTK constructor
      setVTKWindow(wdt, ht);
      initRenderWin=true;
    }
  }
  
  void redraw() 
  {
    vtkRenderWindow *renWin = static_cast<vtkRenderWindow*>(getVTKRenderWindow());
    if (renWin != NULL) {
      cerr << "redraw: Before Render " << renWin << 
	" WindowId=" << renWin->GetGenericWindowId() << endl;
      renWin->Render();
      cerr << "redraw: After Render WindowId=" << renWin->GetGenericWindowId() << endl;
    }
  }
  void rerender() 
  {
    vtkRenderWindow *renWin = static_cast<vtkRenderWindow*>(getVTKRenderWindow());
    cerr << "Enter rerender: " << endl;
    if (renWin != NULL)
      renWin->Render();
  }

  void key_press(int kcode)
  {
    vtkRenderer *ren = static_cast<vtkRenderer*>(getVTKRenderer());
    vtkCellPicker *picker = static_cast<vtkCellPicker*>(getVTKCellPicker());
    
    cerr << "key_press(" << kcode << ")" << endl;
    fprintf(stderr, "key_press(%d)\n", kcode);
    if (picker == NULL) fprintf(stderr, "picker is NULL!\n");
    // call picker like the interactor would setting pick position
    if (picker != NULL) {
      if (picker->Pick(last_x, last_y, 0, ren))
	fprintf(stderr, "picked someting\n");
    }
  }
  

  void button_press(int bm, double lx, double ly)
 {
    cerr << "button_press(" << bm << lx << " " << ly << ")" << endl;
    last_x = lx;
    last_y = ly;
    buttons_mask |= bm;
 }

  void button_release(int bm)
  {
    cerr << "button_release(" << bm << ")" << endl;
    buttons_mask &= bm;
  }

  int motion_notify(double x, double y, int s, int height, int width)
  {
    int redrw = 0;
    double dx = x - last_x;
    double dy = y - last_y;
    // vtkRenderWindow *renWin = static_cast<vtkRenderWindow*>(getVTKRenderWindow());
    vtkRenderer *ren = static_cast<vtkRenderer*>(getVTKRenderer());
    if (ren == NULL) return redrw;
    cerr << " Enter motion_notify(" << 
      x << " " << y << " " << s << " " << 
      height << " " << width <<  
      ")" << endl;
  last_x = x;
  last_y = y;
  bool shift;
  if ( s != 0 ) shift = true; else shift = false;
  bool b1 = buttons_mask == (1 << 1);
  bool b2 = buttons_mask == (1 << 2);
  bool b3 = buttons_mask == (1 << 3);

  if (b1 && !shift) {
    ren->GetActiveCamera()->Azimuth(-1.0 * dx);
    ren->GetActiveCamera()->Elevation(1.0 * dy);
    ren->ResetCameraClippingRange();
    redrw++;
  }
  else if ((b1 && shift) || b3) {
    double focal[3], camera[3], up[3], dir[3], ds[3];
    double a = 50;
    double dl = a * dx / width;
    double dh = a * dy / height;
    ren->GetActiveCamera()->GetDirectionOfProjection(dir);
    ren->GetActiveCamera()->GetViewUp(up);
    ren->GetActiveCamera()->GetPosition(camera);
    ren->GetActiveCamera()->GetFocalPoint(focal);
    ds[0] = dh * up[0] + dl * (up[1] * dir[2] - up[2] * dir[1]);
    ds[1] = dh * up[1] + dl * (up[2] * dir[0] - up[0] * dir[2]);
    ds[2] = dh * up[2] + dl * (up[0] * dir[1] - up[1] * dir[0]);

    focal[0] += ds[0];
    focal[1] += ds[1];
    focal[2] += ds[2];
    camera[0] += ds[0];
    camera[1] += ds[1];
    camera[2] += ds[2];
    ren->GetActiveCamera()->SetPosition(camera);
    ren->GetActiveCamera()->SetFocalPoint(focal);

    redrw++;
  }
  else if (b2) {
    if (!shift)
      ren->GetActiveCamera()->Roll(1.0 * dx);
    ren->GetActiveCamera()->Zoom(exp(0.01 * dy));
    redrw++;
  }
    cerr << " Leave motion_notify(" << redrw << ")" << endl;
    return redrw;
  }

  void scroll(int up)
  {
    vtkRenderer *ren = static_cast<vtkRenderer*>(getVTKRenderer());
    if (ren == NULL) return;
    cerr << "scroll(" << up << ")" << endl;
    if (up == 0) return;
    if (up < 0 ) {
      ren->GetActiveCamera()->Zoom(exp(0.2));
    } else {
      ren->GetActiveCamera()->Zoom(exp(-0.2));
    }
  }
}

