#include "gvariable.h"
#include "CliOutputReceiveThread.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <cc++/thread.h>
#include <errno.h>
#include "cli_server.h"

using namespace BIE;

CliOutputReceiveThread::CliOutputReceiveThread(int socketid, Semaphore *consumer)
{
  _hname = (char *) NULL;
  cerr << "_hname=" << _hname << endl;
  _port = -1;
  _socketid = socketid;
  initialize(consumer);
}

CliOutputReceiveThread::CliOutputReceiveThread(string hostname, int port, Semaphore *consumer)
{
  _hname = (char *) hostname.c_str();
  cerr << "_hname=" << _hname << endl;
  _port = port;

  struct sockaddr_in server_sock_addr, client_sock_addr;

  _socketid = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
  if (_socketid < 0)
  {
    cerr << "Problem creating listener socket \n";
    std::exit(-1);
  }
  cerr << "client: binding the local listening socket\n";
  if (bind(_socketid,(struct sockaddr *)&client_sock_addr,
	   sizeof(client_sock_addr)) < 0) {
    perror("CliOutputReceiveThread::Problem binding listener socket");
    ::close(_socketid);
    cerr << "CliOutputReceiveThread: retrying . . ." << endl;
    std::exit(-1);
  }

  struct hostent *server=gethostbyname(_hname);
  bzero((char *)&server_sock_addr, sizeof(server_sock_addr));
  server_sock_addr.sin_family = AF_INET;
  in_addr_t *sip = (in_addr_t *) server->h_addr_list[0];
  cerr << "hname=" << _hname << " sip=" << sip << " h_addr=" << server->h_addr << endl;
  server_sock_addr.sin_addr.s_addr = *sip;
  server_sock_addr.sin_port = htons(_port);

  cerr <<  "client: starting connect request\n";
  if(connect(_socketid, (struct sockaddr *)&server_sock_addr, sizeof(server_sock_addr)) < 0)
  {
    cerr <<  "client: error in connecterrno=" << errno << endl;
    ::close(_socketid);
    std::exit(-1);
  }

  initialize(consumer);
}

void CliOutputReceiveThread::initialize(Semaphore *consumer)
{
  _qsem = new Semaphore(0);
  _consumer = consumer;
  _receiveBufferSize=1<<12;
  _receiveBuffer = new char[_receiveBufferSize];
  _stop=false;
  _isReady=false;
  _size=0;
}

CliOutputReceiveThread::~CliOutputReceiveThread()
{
  delete [] _receiveBuffer;
  delete _qsem;
  ::close(_socketid);
}

void CliOutputReceiveThread::run()
{
  cerr << "CliOutputReceiveThread Run starting" << endl;
  _isReady = true;
  while(!_stop)
    {
      cerr << "CliOutputReceiveThread Run starting wait qsem" << endl;
      _qsem->wait();
      cerr << "CliOutputReceiveThread Run calling readReply" << endl;
      //while (!((_size = readReply())>0));
      _size = readReply();
      cerr << "CliOutputReceiveThread Run returned from  wait consumer" << endl;
      _consumer->post();
      // wait for consumption
    }
  _isReady = false;
}

Semaphore *CliOutputReceiveThread::getSemaphore()
{
  return _qsem;
}


int CliOutputReceiveThread::readReply()
{
  int rv;
  cerr << "CliOutputReceiveThread readReply enter" << endl; 
  bzero(_receiveBuffer, _receiveBufferSize);
  cerr << "CliOutputReceiveThread readReply calling recv" << endl; 
  rv = recv(_socketid, _receiveBuffer, _receiveBufferSize,0);
  if (rv == -1) {
    cerr << "error receiving data from cli server" << endl;
  }
  cerr << "CliOutputReceiveThread readReply count=" << rv << endl << _receiveBuffer << endl; 
  return rv;
}

istream *CliOutputReceiveThread::getReply()
{
  cerr << "CliOutputReceiveThread::getReply() enter calling readreply" << endl;
  istream *strm;
  if (_size > 0) {
    strm = new istringstream(_receiveBuffer);
  } else {
    strm = NULL;
  }
  cerr << "CliOutputReceiveThread::getReply() exit" << endl;
  return strm;
}
