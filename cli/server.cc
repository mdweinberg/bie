#include "cli_server.h"

using namespace std;

static int socketid, new_socketid, console_socketid;

void signal_handler(int sig) 
{
  close(socketid);
  close(new_socketid);
  close(console_socketid);
}

void signal_handler1(int sig) 
{
  close(socketid);
  close(new_socketid);
  close(console_socketid);
}

int
main(int argc, char *argv[]) 
{
  int pid;
  int status;

  int exec_arg_array_size;
  char **exec_arg_array;
  int i, argchars, carg;

  char progname[1024]={0};
  char *ptr = progname;
  int num_words=argc, count=1, rv;
  while(--num_words>0) {
    rv = sprintf(ptr, "%s ", argv[count++]);
    ptr += rv;
  }
  *ptr = '\n';
#ifdef DEBUG_TESSTOOL
  cerr << "Exec cmd=" << progname;
#endif

  if (signal(SIGINT,  signal_handler))
    perror("server: error setting signal (SIGINT)");

  if (signal(SIGPIPE, signal_handler1))
    perror("server: error setting signal (SIGPIPE)");

  input_redirector  i_redir;
  output_redirector o_std_redir;
  output_redirector o_redir;
  output_redirector console_redir;

  // Use mkfifo to create named pipes to communicate with the program
  // in this case, it is cli.
  //
  // mkfifo will give you the error if the file exist.  Everytime
  // server runs, it always creates named pipes when you start run
  // server again, always gives an error, unless you delete the files
  // by hand or in the program.

  remove(CLI_INPUT);
  remove(CLI_OUTPUT);
  remove(CLI_CONSOLE);

  if(mkfifo(CLI_INPUT, 0755)<0)
  {
    cerr << "server: mkfifo input error" << endl;
    perror("server: mkfifo input error");
    exit(-1);
  }
 
  if(mkfifo(CLI_OUTPUT, 0755)<0)
  {
    cerr << "server: mkfifo output error" << endl;
    perror("server: mkfifo output error");
    exit(-1);
  }

  if(mkfifo(CLI_CONSOLE, 0755)<0)
  {
    cerr << "server: mkfifo console error" << endl;
    perror("server: mkfifo console error");
    exit(-1);
  }
 
  //
  // create a socket to communicate with client
  //
  struct sockaddr_in server_sock_addr;
  struct sockaddr_in client_sock_addr;
  struct sockaddr_in client_console_sock_addr;
  socklen_t clientlen;
  socklen_t client_console_len;

#ifdef DEBUG_TESSTOOL
  cerr << "server: creating socket" << endl;
#endif
  socketid = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
  if (socketid < 0)
  {
    cerr << "Problem creating listener socket" << endl;
    exit(-1);
  }

  /*
  int optval = 1;
  int optlen = sizeof(int);
  if (setsockopt(socketid, IPPROTO_IP, SO_REUSEADDR, &optval, optlen) < 0) {
    perror("server: error in setsockopt");
  }
  */

#ifdef DEBUG_TESSTOOL
  cerr << "server: assigning the local listening socket" << endl;
#endif
  bzero((char *)&server_sock_addr, sizeof(server_sock_addr));
  server_sock_addr.sin_family = AF_INET;
  server_sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  server_sock_addr.sin_port = htons(MY_PORT_ID);

#ifdef DEBUG_TESSTOOL
  cerr << "server: binding the local listening socket" << endl;
#endif
  if (bind(socketid,(struct sockaddr *)&server_sock_addr,
	   sizeof(server_sock_addr)) < 0) {
    perror("server: problem binding listener socket");
    close(socketid);
    close(new_socketid);
    close(console_socketid);
    exit(-1);
  }

  //
  // start accepting connections
  //
#ifdef DEBUG_TESSTOOL
  cerr << "server: starting listening client request" << endl;
#endif
  if(listen(socketid, SOMAXCONN) < 0)
  {
    perror("server: error in listening");
    close(socketid);
    close(new_socketid);
    exit(-1);
  }

  wait_for_cli = true;

  // loop to accept the connectins, use child process to do the work
  while (true) 
  {
    // accept a connection
    clientlen = sizeof(client_sock_addr);
#ifdef DEBUG_TESSTOOL
    cerr << "server: accepting connection [1] . . . " << flush;
#endif
    new_socketid=accept(socketid,(struct sockaddr *)&client_sock_addr,&clientlen);
    if(new_socketid < 0)
    {
      if (errno != EINTR) // accept was interrupted
      {
        cerr << "error in accepting socket" << endl;
        close(socketid);
        close(new_socketid);
        exit(-1);
      }
      continue;  
    }

#ifdef DEBUG_TESSTOOL
    cerr << "done" << endl;
#endif

    // accept another connection for console_socketid
    client_console_len = sizeof(client_console_sock_addr);
#ifdef DEBUG_TESSTOOL
    cerr << "server: accepting connection [2] . . . " << flush;
#endif
    console_socketid=accept(socketid,(struct sockaddr *)&client_console_sock_addr,&client_console_len);
    if(console_socketid < 0)
    {
      if (errno != EINTR) // accept was interrupted
      {
        cerr << "error in accepting socket" << endl;
        close(socketid);
        close(new_socketid);
        exit(-1);
      }
      continue;  
    }

#ifdef DEBUG_TESSTOOL
    cerr << "done" << endl;
    cerr << "server: before fork" << endl;
#endif

    // fork a child process

    pid = fork();
    if (pid<0)
    {
      cerr << "server: fork error" << endl;
      close(socketid);
      close(new_socketid);
      exit(-1);
    }

    if (pid == 0) // child
    {
       // child process does all the reading and sending work
       // procedure to handle the client requst

       i_redir.Setup(new_socketid,CLI_INPUT);
       o_redir.Setup(CLI_OUTPUT,new_socketid);
       console_redir.Setup(CLI_CONSOLE,console_socketid);

       i_redir.start();
       o_redir.start();
       console_redir.start();

       cout.flush();
        
       while(true) {sleep(100);};
       // wait to be killed
       
       exit(0); 

     }
     else 
     {
       // call cli
       carg = 0;
       exec_arg_array_size = argc-1 + 7;
       exec_arg_array = new char* [exec_arg_array_size];
       for (i=1; i< argc; i++) {
	 argchars = strlen(argv[i]);
	 exec_arg_array[carg]  = new char [argchars+1];
	 strcpy(exec_arg_array[carg++], argv[i]);
       }

       exec_arg_array[carg] = new char [2+1];
       strcpy(exec_arg_array[carg++], "-c");
       exec_arg_array[carg] = new char [strlen(CLI_CONSOLE)+1];
       strcpy(exec_arg_array[carg++],   CLI_CONSOLE);

       exec_arg_array[carg] = new char [2+1];
       strcpy(exec_arg_array[carg++], "-f");
       exec_arg_array[carg] = new char [strlen(CLI_INPUT)+1];
       strcpy(exec_arg_array[carg++], CLI_INPUT);

       exec_arg_array[carg] = new char [2+1];
       strcpy(exec_arg_array[carg++], "-d");
       exec_arg_array[carg] = new char [strlen(CLI_OUTPUT)+1];
       strcpy(exec_arg_array[carg++], CLI_OUTPUT);

       exec_arg_array[carg]  = NULL;

#ifdef DEBUG_TESSTOOL
       cerr << "about to call=" << argv[1] << " ";
       for (int i=0; i<carg; i++) cerr << exec_arg_array[i] << " ";
       cerr << endl;
#endif

       execvp(argv[1], exec_arg_array);

#ifdef DEBUG_TESSTOOL
       perror("server: returned from system(cli)");
#endif
       wait_for_cli = false;
 
       kill(pid, SIGTERM);

       // kill the child (blocking i/o)

       close(new_socketid);
       close(console_socketid);

       wait((int *)&status);

       wait_for_cli = true;
       exit(0);
    }
  }
 
  // end, delete the pipe files.

  remove(CLI_INPUT);
  remove(CLI_OUTPUT);
  remove(CLI_CONSOLE);
}
