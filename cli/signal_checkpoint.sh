#!/bin/bash
exec_name=$_
if [ ! $1 ]; then
    echo "Usage: "$exec_name" <pid of the cli process>"
fi

kill -3 $1 &> /dev/null

if [ $? -ne 0 ]; then
    echo "Operation failed: maybe wrong pid?"
else
    echo "Checkpointing at next opportunity."
fi