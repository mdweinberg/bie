/*
 *
 *  Put a limit on address space
 *
 *
 */

#include <cstdlib>
#include <cstring>
#include <sys/time.h>
#include <sys/resource.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

void set_as_limit (long size)
{
#ifdef RLIMIT_AS
  struct rlimit rlim;
  int rc;

  getrlimit(RLIMIT_AS, &rlim);
  rlim.rlim_cur = size;
  rc = setrlimit (RLIMIT_AS, &rlim);
  if (rc) {
    ostringstream msg;
    msg << "set_as_limit: could not set RLIMIT_AS, rlim=" << size << "\n";
    perror (msg.str().c_str()), exit (-1);
  }

  getrlimit(RLIMIT_AS, &rlim);
  if (rlim.rlim_cur != (rlim_t)size)
    cout << "set_as_limit: current VM size limit is " << rlim.rlim_cur << "\n";
#else
  cerr << "no RLIMIT_AS defined in this OS version\n";
#endif
}


// Uses proc . . . and therefore will be OS dependent
long get_cur_vm(pid_t pid)
{
  ostringstream sout;
  sout << "/proc/" << pid << "/stat";

  string word;
  ifstream in(sout.str().c_str());  // ridiculous that streams don't take <string>s
  
  char line[2048];
  in.getline(line, 2048);

  int pid1;
  char comm[1024];
  char state;
  int ppid, pgrp, session, tty, tpgid;
  unsigned flags, minflt, cminflt, majflt, cmajflt;
  unsigned utime, stime, cutime, cstime;
  int counter, priority, timeout, itrealvalue;
  unsigned starttime;
  unsigned vsize;
  unsigned int  rss;                      /** Resident Set Size **/
  unsigned int  rlim;                     /** Current limit in bytes on the rss **/
  unsigned int  startcode;                /** The address above which program text can run **/
  unsigned int	endcode;                  /** The address below which program text can run **/
  unsigned int  startstack;               /** The address of the start of the stack **/
  unsigned int  kstkesp;                  /** The current value of ESP **/
  unsigned int  kstkeip;                  /** The current value of EIP **/
  int		signal;                   /** The bitmap of pending signals **/
  int           blocked; /** 30 **/       /** The bitmap of blocked signals **/
  int           sigignore;                /** The bitmap of ignored signals **/
  int           sigcatch;                 /** The bitmap of catched signals **/
  unsigned int  wchan;  /** 33 **/        /** (too long) **/

  /** pid **/
  sscanf (line, "%u", &pid1);
  char *s = strchr (line, '(') + 1;
  char *t = strchr (line, ')');
  strncpy (comm, s, t - s);
  comm [t - s] = '\0';
  
  sscanf (t + 2, "%c %d %d %d %d %d %u %u %u %u %u %d %d %d %d %d %d %u %u %d %u %u %u %u %u %u %u %u %d %d %d %d %u",
	  &state, &ppid, &pgrp, &session, &tty, &tpgid,
	  &flags, &minflt, &cminflt, &majflt, &cmajflt,
	  &utime, &stime, &cutime, &cstime,
	  &counter, &priority, &timeout, 
	  &itrealvalue, &starttime, &vsize, &rss,
	  &rlim, &startcode, &endcode, &startstack, &kstkesp,
	  &kstkeip, &signal, &blocked, &sigignore, &sigcatch, &wchan);

#ifdef DEBUG
  cout << "pid, command, rss=" << pid << ", " << comm << ", " << rss << "\n";
#endif

  return vsize;
}

