
The BIE CLI uses the GNU readline to implement command line editing
and history.

The most often used commands are:

        ctrl-b          -- moves the cursor back one space
        ctrl-f          -- moves the cursor forward one space
        ctrl-r          -- searches the command history
        ctrl-a          -- moves to the beginning of the line
        ctrl-e          -- move to the end of the line
        meta-b          -- moves back one word
        meta-f          -- move forward one word
        ctrl-] + c      -- move forward to the next instance of character c
        meta-ctrl-] + c -- move backwards to the next instance of c

The meta key is escape by default in Debian/GNU Linux.  See the GNU
readline manual for more details.

By default, the session history is only saved though "psave" and
"prestore" (see help persistence").  However, you may use the cli
config parameter "savehist" (default: 0) to specify the number of
commands to save to the file "histfile" (default: .bie_history).
See "help bierc" for more details about the config file.

