#!/usr/bin/wish
#

global sockid
global gd_sockid

set host localhost
set port 8000

set sockid [socket $host $port]
fconfigure  $sockid -blocking no -translation auto  

set drop_cmd 0
set list_box maindisplay 

proc readfilebyline {filename} {
    global classlist
    if { [file readable $filename] } {
	set fileid [open $filename "r"]
	#end of the file?
	while { [gets $fileid data] >= 0} {
	    #process data,
	    #break the line and if the first word is CLASS,
	    #then put it on the class list(used to construct the GUI)
	    if { [lindex $data 0] == "CLASS"} {
		#print out all the classes name
		#puts [lindex $data 1]
		lappend classlist [lindex $data 1]
	    }
	}
	close $fileid
    }
}

proc writefileintosocket {filename} { 
    global sockid
    if { [file readable $filename] } {
	set fileid [open $filename "r"]
	while { [gets $fileid data] >= 0} {
	    puts $sockid $data
	    flush $sockid
	}
	close $fileid
    }
}

#history key for command input
#history init
global history
set history {}
global step
set step 0

proc histmove {w move} {
    #use Up and Down key to go through history list
    #when there is no up or down, simple do nothing or beeps
    #to indicate this is the end.
    global inputcommand
    global history
    global step
    #how many command on the list
    set numberofcommand [llength $history]
    #the actually move will be numberofcommand+move

    #clear the old command
    $w delete 0 end
  
    set index [expr $numberofcommand+$move]

    if {$index<0} then {
	#beep to let the user know it reaches the begining of the list
	#stay as the first command on the list
	bell
	set inputcommand [lindex $history 0]
	set step [expr $step+1]
    } elseif {$index>[expr $numberofcommand-1]} {
	#beep to let the user know it reaches the end of the list
	#stay as the last command on the list
	bell
	set inputcommand [lindex $history end]
	set step [expr $step-1]
    } else {
	set inputcommand [lindex $history $index]
    }

    $w icursor end
}

proc sendcommand {} {
    global inputcommand
    global sockid
    global history
    global step
    global list_box
    #send the message to server through the socket
    puts $sockid $inputcommand 
    fconfigure  $sockid -blocking yes 
    flush $sockid

    #also set listboxclass = false
    set list_box maindisplay 

    #history management
    #use a simple list to store the command
    if {![string compare "" $inputcommand]} return
    
    #if inputcommand is 'exit', close the application
    if {$inputcommand == "exit" || $inputcommand == "quit"} {
	puts "user wants to exit"
	close $sockid
	exit
    }  
    
    lappend history $inputcommand
    
    set step 0

    #clear contents of widget
    .inputc delete 0 end
}

#get the filename from the user
global textedit
set textedit(last_directory) [pwd]

proc getfilename {} {
    #how do you access the file over a network?  This issue only
    #occurs when the client in on local and server in on network.
    global textedit
    
    set file_types {
	{"Simulation Scripts" {simu*}}
	{"Data Files" {.dat .data}}
	{"TCL Files" { .tcl .TCL .tk .TK}}
	{"Text Files" { .txt .TXT}}
	{"All files" *}
    }
    
    set filename [tk_getOpenFile -initialdir \
		      -$textedit(last_directory) \
		      -filetypes $file_types \
		      -title "Select a file to load" \
		      -parent .]
 
    #store last directory for loading next time
    set $textedit(last_directory) [file dirname $filename]
    return $filename
}

proc runscript {} {
    #get the script file name from the user
    #read the script into the socket.
    global list_box
    set filename [getfilename]
    if {$filename != ""} {
	global .tx.text
	writefileintosocket $filename 
	#disable the command input
	#.tx.text configure -state disabled
	set list_box maindisplay
    }
}

proc runMPIscript {} {
    #lanuch mpirun 
    puts "before invoke mpirun\n"
    #exec lamboot -v hosts
    exec mpirun -v -c 3 N ./cli -- -mpi -f simuclass_script8 -d output.txt& 
    puts "after invoke mpirun\n"
}

proc exitprogram {} {
    global sockid
    #also send the exit command to server
    puts $sockid "exit"
    close $sockid
    exit
}

proc listclasses {} {
    #send the command 'list classes' to server and get back the list
    #and put the list on a list box
    global sockid
    global drop_cmd
    global list_box
    set drop_cmd 2
    puts $sockid "list classes"
    flush $sockid
    set list_box classlist
}


proc clearmethodlist {} {
    .fmethod.list delete 0 end
}

proc sendcommandtoserver {} {
    global dlg
    global msg
    global sockid
    global dlgcommand
    global list_box
    puts $sockid $dlgcommand
    flush $sockid
    set list_box maindisplay
    destroy $msg
    destroy $dlg
}

proc clearcommand {} {
    global msg
    global dlgcommand
    #also need to clean up the previous command
    set dlgcommand ""  
    destroy $msg
}

proc methoddesc {} {
    global methodname
    global classname
    global sockid
    global list_box
    set tmpcmd "desc"
    lappend tmpcmd $classname $methodname
    puts $sockid $tmpcmd
    flush $sockid
    #puts $tmpcmd
    #set list_box maindisplay
    set list_box descdisplay
    #create a msg window for information
    #before doing anything, create an another dialog window
    global helpmsg
    set helpmsg .helpmsgdialog
    global createhelpmsgwindow
    if {[winfo exists $helpmsg]==0} {
	set createhelpmsgwindow 1 
	toplevel $helpmsg
	wm protocol $helpmsg WM_DELETE_WINDOW "destroy $helpmsg"
	wm title $helpmsg "description" 
	frame .helpmsgdialog.tx
	text .helpmsgdialog.tx.text -yscrollcommand ".helpmsgdialog.tx.scroll set"
	scrollbar .helpmsgdialog.tx.scroll -command ".helpmsgdialog.tx.text yview"
	pack .helpmsgdialog.tx.scroll -side right -fill y
	pack .helpmsgdialog.tx.text -side left
	pack .helpmsgdialog.tx -side top
	button .helpmsgdialog.bok -text "OK" -command "destroy $helpmsg" 
	pack .helpmsgdialog.bok  
    }
}

proc createobject {} {
    global dlg
    global varname
    global tmpvalue
    global methodargs
    global methodname
    global dlgcommand
    global drop_test_var
    #first reset the dlgcomand
    set dlgcommand ""
    #get the input from dialog window and form a command and send to server
    lappend dlgcommand set $varname = new $methodname
    lappend dlgcommand "("
    #puts $dlgcommand
    for {set i 2 } {$i<[expr [llength $methodargs]-1]} {incr i} {
	#puts [.createdialog.input$i get] 
	if {$i == [expr [llength $methodargs]-2]} {
	    #lappend dlgcommand [.createdialog.input$i get] 
	    lappend dlgcommand [.createdialog$methodname.n$i.ent get] 
	} else { 
	    #lappend dlgcommand [.createdialog.input$i get] "," 
	    lappend dlgcommand [.createdialog$methodname.n$i.ent get] "," 
	}
    }
    lappend dlgcommand ")"
    #puts $dlgcommand

    #before doing anything, create an another dialog window
    #to make sure the user want to do this
    global msg
    set msg .msgdialog
    if {[winfo exists $msg]==0} {
	toplevel $msg
	wm protocol $msg WM_DELETE_WINDOW "destroy $msg"
	wm title $msg "attention" 
	#add the text and edit 
	label .msgdialog.lmsg -text $dlgcommand
	pack .msgdialog.lmsg
	button .msgdialog.bok -text "OK" -command sendcommandtoserver 
	button .msgdialog.bcancel -text "Cancel" -command clearcommand 
	pack .msgdialog.bok .msgdialog.bcancel -side left 
	#destroy $dlg 
    }
}

#place value in global variable
proc DropListSetValue{entry variable} {
    upvar #0 $variable var

    set value [$entry get]
    if { $value != ""} {
	set var $value
    }
}


proc DropListCreate { basename text width height variable initial_value} {
    upvar #0 $variable var
    set var "$initial_value"
    
    #name of top-level widget to creates
    set top $basename.top

    #widgets to enter data
    frame $basename -bd 0
    label $basename.lbl -text $text -anchor e
    entry $basename.ent -width $width
    $basename.ent insert 0 "$initial_value"
    DropButton $basename.drop $basename.top $basename.ent
    button $basename.new -text "new" -command "createnew $basename.top"
    
    #bind $basename.ent <Return> \
	#  "DropListSetVal $basename.ent $variable"

    bind $basename.ent <Key-Escape> "wm withdraw $top"
 
    pack $basename.lbl -side left -fill y
    pack $basename.ent -side left -expand 1 -fill y
    pack $basename.drop -side left -fill y
    pack $basename.new -side left -fill y

    #drop-list is a top-level temporary window
    toplevel $top -cursor top_left_arrow
    wm overrideredirect $top 1
    wm withdraw $top

    #create list
    set frm $top.frame
    frame $frm -bd 4 -relief sunken

    listbox $frm.list -height $height -width $width -selectmode single \
	-yscrollcommand "$frm.scrollbar set"

    bind $frm.list <Key-Escape> "wm withdraw $top"

    #create scrollbar
    scrollbar $frm.scrollbar -command "$frm.list yview"
    
    pack $frm.scrollbar -side right -fill y
    pack $frm.list -side left
    pack $frm -side top

    bind $frm.list <ButtonRelease-1> "DropListClick $top $basename.ent $variable"
    pack $basename

    #return list widget so you can fill it
    return $frm.list
}

#return selected item for a single select list
proc list_selected {listname} {
    set indx [$listname curselection]
    if {$indx != ""} {
	set item [$listname get $indx]
	return $item
    } else {
	return ""
    }
}


#handle click on drop-list widget
proc DropListClick {basename entry variable} {
    catch {
	set selected [list_selected $basename.frame.list]

	if {$selected != ""} {
	    #put item into entry widget
	    $entry delete 0 end
	    $entry insert 0 "$selected"
	    DropListSetVal $entry $variable
	}
    }
    wm withdraw $basename
}

proc getlabelname {basename} {
    #puts "ask server for drop list"
    global methodargs
    #global sockid
    #global list_box
    global aslabel1
    global mylabel

    set aslabel1 $basename.drop 
    #puts $aslabel1
    set tmpl [expr [string length $aslabel1] -11]
    #puts $tmpl
    set aslabel [string range $aslabel1 $tmpl [expr $tmpl +1]]
    #puts $aslabel
    if {[string index $aslabel 0]=="n"} {
	set i [string index $aslabel 1]
    } else {
	set i $aslabel
    }
    #puts $i
    set mylabel [lindex $methodargs $i]
    #puts $mylabel
    return $mylabel
}

#makes drop list visible, create with DropListCreate
proc ShowDropList { basename associated_widget} {
    #before doing these, ask the server for list of instances created 
    global sockid
    global list_box
    global mylabel
    getlabelname $basename
    set printobj printobject
    #puts $mylabel
    lappend printobj $mylabel 
    puts $sockid $printobj
    flush $sockid
    set list_box droplist

    set x [winfo rootx $associated_widget]
    set y [winfo rooty $associated_widget]
    set y [expr $y + [winfo height $associated_widget]]
    
    wm geometry $basename "+$x+$y"

    wm deiconify $basename
    raise $basename

    #clear list first and then add the list from the server msgback 
    $basename.frame.list delete 0 end
    focus $basename.frame.list
}

#create a button with a drop dowm bitmap
proc DropButton {name toplevel entry} {
    button $name -image dnarrow \
	-command "ShowDropList $toplevel $entry"

    return $name
}

#bitmap data for down arrow bitmap
set dnarrow_data "
#define dnarrow2_width 18
#define dnarrow2_height 18
static unsigned char dnarrow2_bits[] = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xfc, 0xff, 0x00, 0xf8, 0x7f, 0x00,
  0xf8, 0x7f, 0x00, 0xf0, 0x3f, 0x00,
  0xf0, 0x3f, 0x00, 0xe0, 0x1f, 0x00,
  0xc0, 0x0f, 0x00, 0xc0, 0x0f, 0x00,
  0x80, 0x07, 0x00, 0x80, 0x07, 0x00,
  0x00, 0x03, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0xfc, 0xff, 0x00,
  0xfc, 0xff, 0x0, 0x00, 0x00,0x00}
"
image create bitmap dnarrow -data $dnarrow_data

set drop_test_var "Unset"
global drop_test_var

proc createnew {basename} {
    #get the label as class name and open a new create class dialogy window 
    global mylabel
    #puts $basename
    getlabelname $basename
    #puts $mylabel
    set tmp [expr [string length $mylabel]-2]
    if {[string index $mylabel [expr $tmp+1]] == "*"} {
	set mylabel1 [string range $mylabel 0 $tmp] 
    } else {
	set mylabel1 $mylabel
    }
    #puts $mylabel1
    #question, should I always open the first constructor or let the user chose?
    #createdialogwindow $mylabel1
    showmethforclassname $mylabel1
}


proc showprintall {} {
    global sockid
    global list_box
    puts $sockid "printall"
    flush $sockid
    set list_box maindisplay
}

proc initialdroplist {} {
    global methodargs
    global sockid
    global init
    set init true
    set list_box droplist
    for {set i 2 } {$i<[expr [llength $methodargs]-1]} {incr i} {
	set classlabel [lindex $methodargs $i]
	set printobj printobject
	lappend printobj $classlabel
	#puts $printobj
	puts $sockid $printobj
	flush $sockid
	#puts $list_box
    }
}

proc showhistory {} {
    global history
    #open a new window
    #lanuch a dialogy window for displaying command history
    set hdlg .historydialoy
    toplevel $hdlg
    wm protocol $hdlg WM_DELETE_WINDOW "destroy $hdlg"
    wm title $hdlg "Command History" 
    #use a list box for displaying 
    listbox .historydialoy.hlist -height 10 -width 40 \
	-selectmode single \
	-yscrollcommand ".historydialoy.hscrl set"
    scrollbar .historydialoy.hscrl -command ".historydialoy.hlist yview"
    pack .historydialoy.hscrl -side right -fill y
    pack .historydialoy.hlist -side left -fill x -fill y
    for {set i 0} {$i<[llength $history]} {incr i} {
	.historydialoy.hlist insert 0 [lindex $history $i]
    }
    #double click on the list, should get the command into command line window.
    #capture double click for select history command list  
    bind .historydialoy.hlist <Double-Button-1> {
	global inputcommand
	set commandname [list_selected .historydialoy.hlist]
	if {$commandname !=""} {
	    #put the commandname into inputcommand
	    set inputcommand $commandname
	    focus .inputc
	}
    }
}

proc setdefault {} {
    global list_box
    writefileintosocket "/home/weinberg/src/BIE/cli/.clirc"
    set list_box maindisplay
    global sockid
    puts $sockid "ignore interactive"
    flush $sockid
}

#extract the class name list from methods.out
#readfilebyline methods.out

label .l -text "welcome to BIE" -background yellow
pack .l

menu .menubar
. config -menu .menubar
menu .menubar.file
menu .menubar.help
#menu .menubar.optionst
#menu .menubar.format
.menubar add cascade -label "File" -menu .menubar.file
.menubar.file add command -label "Run Script" -command runscript
.menubar.file add command -label "History" -command showhistory
.menubar.file add command -label "Exit" -command exitprogram 
#menu .menubar.file -tearoff 0
#.menubar add cascade -label "Classes List" -menu .menubar.help
#for {set i 0} {$i<[llength $classlist]} {incr i} {
#  .menubar.help add command -label [lindex $classlist $i]
#  }
#menu .meunbar.help -tearoff 0

#listbox to display available classes
frame .fclass -relief groove -borderwidth 8 
label .fclass.l -text "Available Classes"
listbox .fclass.list -height 4 -yscrollcommand ".fclass.scrl set" -width 62
scrollbar .fclass.scrl -command ".fclass.list yview"
pack .fclass.scrl -side  right -fill y
pack .fclass.list -side right
pack .fclass.l 
pack .fclass 

proc showmethforclassname {classname} {
    global sockid
    global drop_cmd
    global list_box
    
    set list_box methodlist 
    set drop_cmd 1
    set tmpcmd list 
    lappend tmpcmd methods $classname 
    set cmd $tmpcmd 
    puts $sockid $cmd
    flush $sockid
    #clear list box for methods
    clearmethodlist 
}

#capture double click for class list  
bind .fclass.list <Double-Button-1> {
    #global sockid
    global classname
    set classname [list_selected .fclass.list]
    if {$classname !=""} {
	showmethforclassname $classname
    }  
}


#listbox to display available methods
frame .fmethod -relief groove -borderwidth 8 
label .fmethod.l -text "Available Methods" 
listbox .fmethod.list -height 4 -yscrollcommand ".fmethod.scrl set" -width 62
scrollbar .fmethod.scrl -command ".fmethod.list yview"
pack .fmethod.scrl -side  right -fill y
pack .fmethod.l .fmethod.list -side left 
pack .fmethod -fill x

proc createdialogwindow {methodname} {
    global methodargs
    global dlg
    set dlg .createdialog$methodname
    if {[winfo exists $dlg]==0} {
	toplevel $dlg
	wm protocol $dlg WM_DELETE_WINDOW "destroy $dlg"
	wm title $dlg [lindex $methodargs 0] 
	label .createdialog$methodname.lmname -text $methodname
	pack .createdialog$methodname.lmname
	label .createdialog$methodname.lvname -text "Name"
	#reset the varname everytime we open the createobject window
	set varname ""
	entry .createdialog$methodname.inputvname -width 15 -textvariable varname
	pack .createdialog$methodname.lvname .createdialog$methodname.inputvname  
	set tmpvalue "( )"
	for {set i 2 } {$i<[expr [llength $methodargs]-1]} {incr i} {
	    #instead of entry widgt, use drop down list for default value
	    set classlabel [lindex $methodargs $i]
	    set drop_test_var "Unset"
	    set initial_value ""
	    set createdlglist [DropListCreate .createdialog$methodname.n$i $classlabel 40 3 drop_test_var "$initial_value"] 
	    #can make label clickable to create the new object
	    #button .createdialog$methodname.new$i -text "new" -command createnew 
	    #pack .createdialog$methodname.new$i 
	} 
	#add buttons 
	button .createdialog$methodname.bok -text "OK" -command createobject 
	button .createdialog$methodname.bcancel -text "Cancel" -command "destroy $dlg"
	button .createdialog$methodname.bhelp -text "Desc" -command methoddesc 
	pack .createdialog$methodname.bok .createdialog$methodname.bcancel .createdialog$methodname.bhelp -side left 
    }
}


global gd_portid


proc openfiledisplay {} {
    global sockid
    global list_box
    set list_box newsocket
    
    #first ask cli for a new socket port
    puts $sockid "create socket"
    flush $sockid
    #then create a new socket.
    #all the information will go through this socket 
    #send the file name back to cli
    
    global gd_filename
    global gd_sockid
    global tmp_fileid
    global filename

    #get the file name from the user
    set filename [getfilename]
    #if {$filename != ""} 
    #send the request to the server and the server will send back to the client.
    #the client receives the data and creates a new temp file
    #after finishing, the client should delete this temp file.

    set getcommand "get"
    lappend getcommand $filename
    puts $getcommand
    #send the client request to the server through the new socket
    puts $gd_sockid $getcommand
    flush $gd_sockid
    #set gd_filename "cli_"
    #append gd_filename $filename 
    #puts $gd_filename
}    

proc invokegnuplot {} {
    global fakefilename
    
    #need to create a new gnuplot.test according to different file the user chose.
    #through the socket I just created.
    set gnuplotid [open gnuplot.test1 w]
    puts $gnuplotid "set term x11"
    set tmpstr "plot"
    #append tmpstr "'" $filename "'" "using 2:3" 
    append tmpstr "'" fakefilename "'" "using 2:3" 
    puts $gnuplotid $tmpstr
    puts $gnuplotid "pause -1"
    flush $gnuplotid 
    #lanuch gnuplot to display
    exec gnuplot gnuplot.test1 &
}

proc invokeVisualize {} {
    global filename
    puts $filename

    #lanuch gnuplot to display
    exec ~/BIE/Visualizer/visualize $filename &
}
proc pickOS {} { 
    #bring a new window for the user to type the fields.
    #send back the information to cli and lanuch gnu plot 

}

proc listAOS {} {
    global sockid
    global list_box
    set list_box streamlist

    #send the client request to the server through the new socket
    puts $sockid "list streams" 
    flush $sockid
    
    #also need to set a flag to display the return. 
    #open a window to display the message back from cli
    set aosdlg .listAOSdialog
    global createaoswindow 
    set createaoswindow 0
    if {[winfo exists $aosdlg]==0} {
	set createaoswindow 1
	toplevel $aosdlg
	wm protocol $aosdlg WM_DELETE_WINDOW "destroy $aosdlg"
	wm title $aosdlg "Select RecordOutputStream" 
	label .listAOSdialog.l -text "select a RecordOutputStream and fields:" 
	pack .listAOSdialog.l
	#listbox to display available classes
	frame .listAOSdialog.f -relief groove -borderwidth 8 
	listbox .listAOSdialog.f.list -height 4 -yscrollcommand ".listAOSdialog.f.scrl set" -width 62
	scrollbar .listAOSdialog.f.scrl -command ".listAOSdialog.f.list yview"
	pack .listAOSdialog.f.scrl -side  right -fill y
	pack .listAOSdialog.f.list -side right
	pack .listAOSdialog.f 
	#button .listAOSdialog.ok -text "OK" -command selectfields 
	button .listAOSdialog.cancel -text "Cancel" -command "destroy $aosdlg" 
	pack .listAOSdialog.cancel -side left 
	
	#capture double click for select a RecordOutputStream list  
	bind .listAOSdialog.f.list <Double-Button-1> {
	    global listindex
	    set listindex [list_selected .listAOSdialog.f.list]
	    set listindex [lindex $listindex 0]
	    set listindex [string index $listindex 1]
	    if {$listindex !=""} {
		selectfields 
	    }  
	}
    }
}

proc checkselect {} {
    global selectedfields

    if {$selectedfields!=""} {
	commitselect
    }  
}

proc commitselect {} {
    #before do anything, check if the user typed valid information.
    #open a new socket and send the selected fields information to cli
    #another way to do this is send the information through command
    #socket and cli will create a new socket and GUI too to receive
    #data and display them.
    global sockid
    global list_box
    global selectedfields
    global gd_sockid
    global listindex
    set list_box newsocket 

    set gd_sockid 0

    #ask cli for a new socket port
    puts $sockid "create socket"
    flush $sockid

    vwait gd_sockid

    #send the command through gd_sockid
    set command "select "
    #the user input should be append here 
    append command $listindex "," $selectedfields
    puts $command
    
    #send the client request to the server through the command socket
    puts $gd_sockid $command
    flush $gd_sockid
    #cli should be modified to accept not only "get" command but also "select"
    #command
}


proc selectfields {} {
    #let the user choses which fields to display
    #open a dialogy window for user input
    set sfdlg .selectfieldsdialog
    if {[winfo exists $sfdlg]==0} {
	toplevel $sfdlg
	wm protocol $sfdlg WM_DELETE_WINDOW "destroy $sfdlg"
	wm title $sfdlg "Select Fields" 
	label .selectfieldsdialog.l -text "Please input field names:" 
	pack .selectfieldsdialog.l
	entry .selectfieldsdialog.input -width 40 -textvariable selectedfields
	pack .selectfieldsdialog.input 
	button .selectfieldsdialog.ok -text "OK" -command checkselect 
	button .selectfieldsdialog.cancel -text "Cancel" -command "destroy $sfdlg" 
	pack .selectfieldsdialog.ok .selectfieldsdialog.cancel -side left 
    }
}


proc graphicaldisplaywindow {} {
    set gdwindow .gddialog
    if {[winfo exists $gdwindow]==0} {
	toplevel $gdwindow
	wm protocol $gdwindow WM_DELETE_WINDOW "destroy $gdwindow"
	wm title $gdwindow "graphical display" 
	button .gddialog.bopenfile -text "Open File" -command openfiledisplay
	button .gddialog.blists -text "Select outputStream" -command listAOS
	button .gddialog.bok -text "Exit" -command "destroy $gdwindow" 
	pack .gddialog.bopenfile .gddialog.blists .gddialog.bok -side left 
    }
}


#capture double click for method list  
bind .fmethod.list <Double-Button-1> {
    global methodargs
    set methodargs [list_selected .fmethod.list]
    #add the text and edit 
    set tmpname [lindex $methodargs 1]
    set methodname [string range $tmpname 0 [expr [string length $tmpname]-2]]
    #lanuch a dialogy window for create an object or method
    createdialogwindow $methodname
}

label .l2 -text "Please input the commands" 

#get the user input and send it to server
entry .inputc -width 40 -textvariable inputcommand

#capture Return/Enter key
bind .inputc <Key-Return> "sendcommand"
#capture Up key
bind .inputc <Key-Up> {
    set step [expr $step-1]
  histmove .inputc $step
}
bind .inputc <Key-Down> {
  set step [expr $step+1]
  histmove .inputc $step
}

pack .l2 .inputc -side top 

flush $sockid
label .l3 -text "Data received from cli: \n"
#display the message received from server
frame .tx
text .tx.text -yscrollcommand ".tx.scroll set"
scrollbar .tx.scroll -command ".tx.text yview"
#pack scrollbar first
pack .tx.scroll -side right -fill y
pack .tx.text -side left
pack .l3 .tx -side top

#get the message from socket
proc read_control {} {
    global drop_cmd
    global list_box
    global aslabel1
    global gd_sockid
    global gd_portid
    global gd_host
    global sockid
    global createhelpmsgwindow
    global createdlgwindow
    global createaoswindow

    if {$list_box == "classlist"} {
	gets $sockid msg_back
	for {set i 0} {$i<[llength $msg_back]} {incr i} {
	    if {$drop_cmd == 0} {
		.fclass.list insert end [lindex $msg_back $i]
	    } else {
		set drop_cmd [expr $drop_cmd - 1]
		puts $msg_back
	    }
	}
    } elseif {$list_box == "methodlist"} {
	gets $sockid msg_back
	if {$msg_back!=""} {
	    if {$drop_cmd == 0} {
		.fmethod.list insert end $msg_back
	    } else {
		set drop_cmd [expr $drop_cmd - 1]
	    }
	}
    } elseif {$list_box == "descdisplay"} {
	gets $sockid msg_back
	if {$msg_back!=""} {
	    if {$createhelpmsgwindow == 1} {
		.helpmsgdialog.tx.text insert end $msg_back
		.helpmsgdialog.tx.text insert end "\n"
		set createhelpmsgwindow 0
	    }
	}
    } elseif {$list_box == "droplist"} {
	#called only when there is something back from the server
	gets $sockid msg_back
	flush $sockid
	if {$msg_back!=""} {
	    #puts $aslabel1
	    set templen [expr [string length $aslabel1]-10]
	    set tmpbasename [string range $aslabel1 0 $templen] 
	    #puts $tmpbasename
	    #set tmpbasename2 [string range $aslabel1 
	    #if {[string index $tmpbasename 16]=="."} {
	    #  set tmpbasename [string range $tmpbasename 0 15]
	    #}
	    $tmpbasename.top.frame.list insert end $msg_back
	}
    } elseif {$list_box == "newsocket"} {
	gets $sockid msg_back
	puts $msg_back
	#if the msg_back is formated as sockid:8400
	#substract 'sockid' and assign 8400 as new sockid
	if {[lindex $msg_back 0] == "socketid"} { 
	    set gd_portid [lindex $msg_back 1]
	    set gd_host localhost
	    set gd_sockid [socket $gd_host $gd_portid]
	    fconfigure  $gd_sockid -blocking no -translation auto  
	    puts "after create gd_socket and configure it\n"
	    fileevent $gd_sockid readable read_data
	}
    } elseif {$list_box == "streamlist"} {
	gets $sockid msg_back
	if {$msg_back!=""} {
	    if {$createaoswindow == 1} {
		.listAOSdialog.f.list insert end $msg_back
	    }
	}
    } else {
	gets $sockid msg_back
	.tx.text insert end $msg_back
	.tx.text insert end "\n"
    }
}



proc read_data {} {
    global tmp_fileid
    global sockid 
    global gd_sockid 
    
    #create a new temp file for the client to display
    #get the message from graphical display socket
    if {[gets $gd_sockid datafilemsg] >=0} {
	#puts "after read the socket msg\n"
	#puts $datafilemsg
	puts $tmp_fileid $datafilemsg 
	flush $gd_sockid
	flush $tmp_fileid
    } else {
	if { [eof $gd_sockid] } {
            close $gd_sockid
            puts "after close gd_sockid\n"
            #invokegnuplot 
            invokeVisualize
	}
    }
}

setdefault

fileevent $sockid readable read_control

global tmp_fileid

set tmp_fileid [open fakefilename w+]

#send a script through socket to server
button .brunscript -text "run from script" -command runscript 

#run MPI 
button .brunmpiscript -text "run MPI script" -command runMPIscript 

#show all the instances been created.
button .bprintall -text "show created instance" -command showprintall

#lanch graphical display window.
button .bgdisplay -text "graph display" -command graphicaldisplaywindow

#list the available classes
button .bgclasses -text "list classes" -command listclasses

#exit button to close the program
button .bexit -text "exit" -command exitprogram 
pack .brunscript .brunmpiscript .bprintall .bgdisplay .bgclasses .bexit -side left 

vwait change_to_quit 

close $sockid

