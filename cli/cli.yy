// This is really -*- C++ -*-

%{
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <readline/history.h>
#include <iomanip>
#include <fcntl.h>
#include <string>
#include <string.h>
#include <config.h>
#include "BIEACG.h"
#include "MethodTable.h"
#include "GlobalTable.h"
#include "AData.h"
#include "CliArgList.h"
#include "SymTab.h"
#include "Statement.h"
#include "ForStatement.h"
#include "AssignStatement.h"
#include "Expr.h"
#include "CallExpr.h"
#include "SimpleExpr.h"
#include "ACG.h"
#include "BIEconfig.h"
#include <vector>
#include "gvariable.h"
#include "gfunction.h"
#include <fstream>
#include "cli_server.h"
#include "sammi.h"
#include "getfile.h"
#include "cc++/thread.h"
#include <BasicType.h>
#include <RecordOutputStream.h>
#include <BIEException.h>
#include <PrintStatement.h>
#include <signal.h>
#include <Timer.h>

#undef  USE_FE_TRAPS
#ifdef  USE_FE_TRAPS
#include <fenv.h>
#endif

using namespace std;

#ifdef HAVE_LIBCFITSIO
  #define LIBCFITSIO_ENABLED 1
#else
  #define LIBCFITSIO_ENABLED 0
#endif

#if PERSISTENCE_ENABLED
  #include <PersistenceControl.h>
#endif

using namespace BIE;

extern int yylex();
extern int yyerror(const char *);
extern int yylineno;
int parse_error=0;

namespace BIE {
  extern int myid;
}

void mpi_init(int argc, char** argv);
void mpi_slave_output(string ofile);
void mpi_quit();

char filename[40];

bool accept_interactive = true;

char exit_message[] = "Good bye . . .";
char standard_prompt[] = "BIE> ";

static int cli_sockid;
static int clinew_sockid;
static int port_id;
static struct sockaddr_in cli_sock_addr;
static struct sockaddr_in user_sock_addr;
static socklen_t userlen;
static Timer timer;
static void stopwatch(const char *msg, unsigned long t)
{
  ostringstream sout;

  sout << msg << t << " sec";

  if (t>60) {			// Don't print extra info if t<60 sec

    cout << left << setw(42) << sout.str();
    sout.str("");

    unsigned long hours=0, minutes;
    sout << "[";
    if (t>3600) {		// Number of hours
      hours = t/3600;
      t -= 3600*hours;
      sout << hours << " hr, ";
    }
    if (t>60 || hours) {        // Print minutes, even if zero if
      minutes = t/60;		// hours are non-zero
      t -= 60*minutes;
      sout << minutes << " min, ";
    }
    sout << t << " sec]";	// Remaining seconds
    cout << sout.str();		// Spit it out . . .

  } else cout << sout.str();

  cout << endl;
}

static void class_print(vector<string> out)
{
  if (myid) return;

  const unsigned many = 3;
  vector<string>::iterator it=out.begin();

  while (it!=out.end()) {
    for (unsigned i=0; i<many; i++) {
      if (it!=out.end()) cout << setw(26) << left << *(it++);
    }
    cout << endl;
  }
}

class GraphicsSocketHandler: public Thread 
{
public:
  void Start() { start();} // workaround common c++ name change
  void run() { Run();}
  void Run() 
  {
    ostream cout(checkTable("cli_console"));

    setCancel(cancelImmediate);	//THREAD_CANCEL_IMMEDIATE
    userlen = sizeof(user_sock_addr);
    clinew_sockid=accept(cli_sockid,(struct sockaddr *)&user_sock_addr,&userlen);
    cout << "clinew_sockid in c: " << clinew_sockid << "\n";
    cout.flush();

    if(clinew_sockid < 0) {
      if (errno != EINTR) /*accept was interrupted */ {
        ::close(cli_sockid);
        ::close(clinew_sockid);
        std::exit(-1);
      }
    }
    graphrequest_proc(clinew_sockid);
    if(::close(clinew_sockid)<0) {
      cout << "graphical display server: close error\n";
      std::exit(-1);
    }
    cout << "thread: before exit \n";
    cout.flush();
    terminate();
  }
};

GraphicsSocketHandler		*graphics_output_handler_ptr;

void catch_ckpt(int sig_num) 
{
  signal(SIGQUIT, catch_ckpt);
  PersistenceControl::ckon(consoleFile);
  PersistenceControl::ckmanual(consoleFile);
}

static void banner(bool b, unsigned s)
{
  if (!b) return;
  for (unsigned n=0; n<72; n++) {
    if (n%4==0) cout << '-'  << flush;
    if (n%4==1) cout << '/'  << flush;
    if (n%4==2) cout << '|'  << flush;
    if (n%4==3) cout << '\\' << flush;
    usleep(20000);
  }
  cout << endl;
  sleep(s);
}

void print_help_dir()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir (helpdir.c_str());
  if (dp != NULL)
    {
      unsigned cnt = 0;
      while ( (ep = readdir (dp)) ) {
	if (ep->d_name[0] == '.') continue;
        consoleFile << left << setw(20) << ep->d_name;
	cnt++;
	if (!(cnt % 4)) consoleFile << endl;
      }
      if (cnt % 4) consoleFile << endl;
      consoleFile << endl;
      (void) closedir (dp);
    }
  else
    consoleFile << "Could not open the help directory: " 
		<< helpdir
		<< endl;

}

void print_help_file(const char *var)
{
  char iline[256];
  ostringstream ostr;
  ostr << helpdir << "/" << var;
  ifstream infile(ostr.str().c_str());
  if (!infile) { 
    consoleFile << "No help on <" << var << ">" << endl;
    consoleFile << "Help is available for commands:" << endl << endl;
    print_help_dir();
    consoleFile << endl;
    
  } else {
    
    while (!infile.eof()) {
      infile.getline(iline, 256);
      consoleFile << iline << endl;
    }
    infile.close();
  }
}

class poptions
{
public:
  explicit poptions(const string s1, const string s2, unsigned int f) : 
		    s1_(s1), s2_(s2), f_(f) {}
private:
  string s1_, s2_;
  unsigned int f_;
  static unsigned poptions_count;
  static const unsigned field1 = 25;
  template <class charT, class Traits>
  friend basic_ostream<charT,Traits>& operator<<
  (basic_ostream<charT,Traits>& ib, const poptions& h)
  {
    ib << setw(2) << right << ++poptions_count << "| " 
       << left << setw(field1) << h.s1_
       << " |  " << setw(5) << (h.f_ ? "yes" : "no") 
       << " |  " << h.s2_ << endl;
    return ib;
  }
};

unsigned poptions::poptions_count = 0;

%}

%union{
  char *str;
  long ival;
  double dval;
  Statement *statement_;
  Expr *expr_;
  AData *data_;
  CliArgList *arglist_;
}

/* Terminal Symbols */
%token EQUALS
%token EXIT
%token QUIT
%token PRINT
%token PRINTALL
%token PRINTOBJECT
%token SET
%token NEW
%token FOR
%token IN
%token TO
%token <str> VAR
%token <str> LITSTRING
%token <ival> INT
%token <dval> DOUBLE
%token CR
%token NEWLINE
%token LP
%token RP
%token LB
%token RB
%token AMP
%token PTR
%token COMMA
%token CLASSES
%token METHODS
%token CLASSTREE
%token HELP
%token WAIT 
%token INTERACTIVE 
%token IGNORE 
%token ACCEPT 
%token LIST
%token DESC
%token CREATE
%token SOCKET
%token STREAMS
%token BASICTYPE_STRING
%token BASICTYPE_INT
%token BASICTYPE_REAL
%token BASICTYPE_BOOL
%token TRUE
%token FALSE
%token VISUALIZE
%token PNEWSESSION
%token PSAVE
%token PRESTORE
%token PHISTORY
%token PLIST
%token PINFO
%token PVERSIONS
%token CKON
%token CKOFF
%token CKTOGGLE
%token CKINTERVAL
%token CKTIMER
%token CKMANUAL
%token STARTCLOCK
%token STOPCLOCK
%token RESETCLOCK
%token ELAPSEDTIME
%token HISTORY
%token GET
%token CODEVER
%start input

/* Non Terminals */
%type <statement_> statement forloop assignment printstatement
%type <expr_> rhs call constructor method_call
%type <arglist_> args arglist
%type <data_> offset_val value
%%
input:  
      /*empty line */
    | input line 
    | input line CR
    | input error NEWLINE 
      {  
        parse_error=1;
        yyerror("I'm confused, please try again . . .\n");
        parse_error = 0;
      }
    ; 

line:
      NEWLINE {
      }
    | command {
      }
    | statement NEWLINE
      {
        try {
	  $1->process(NULL);
	} 
	catch (BIEException & e) 
	{
	  consoleFile << e.getErrorMessage() << endl;
	}
        delete $1;
      }
    | method_call NEWLINE
      {
        AData return_value;
        
	try {
	  $1->eval(NULL, &return_value);
        } 
	catch (BIEException & e)
	{
	  if (myid==0)
	  consoleFile << e.getErrorMessage() << endl;
	}
	
	delete $1;
      }
    ;

command:  
      EXIT NEWLINE 
      {
        mpi_quit();
	if (myid==0)  {
	  cout << endl << exit_message << endl;
	  if (savehist) {	// Save "savehist" of the last states
				// in "histfile"
	    stifle_history(savehist);
	    write_history (histfile.c_str());
	  }
	}
 	exit(0);
      }
    |  QUIT NEWLINE 
      {
        mpi_quit();
	if (myid==0) {
	  cout << endl << exit_message << endl;
	  if (savehist) {	// Save "savehist" of the last states
				// in "histfile"
	    stifle_history(savehist);
	    write_history (histfile.c_str());
	  }
	}
 	exit(0);
      }
    |  CODEVER NEWLINE
      {
	if (myid==0)
	  cout << "Version " << VERSION
	       << ", Copyright (C) 1999-2012 by Martin D. Weinberg" 
	       << endl;
      }
    | HISTORY NEWLINE
      {
        unsigned int entryindex = 1;
	HIST_ENTRY * histentry = history_get(1);
	
	while (histentry != NULL)
	{
	  consoleFile << setw(4) << entryindex << " " << histentry->line << endl;
	  entryindex++;
	  histentry = history_get(entryindex);
        }
      }
    | PRINTALL NEWLINE
      {
        consoleFile << "  ==== User Defined Variables ===" << endl;
	st->print_all_values(consoleFile);
        consoleFile << endl << "  ==== Global Variables ===" << endl;
        GlobalTable::print_all_values(consoleFile);
	if (ttool) consoleFile.flush();
      }
    | PRINTOBJECT VAR NEWLINE
      {
        vector<string> matchingsymbols;

	try {
	  matchingsymbols = st->get_symbols_by_class($2);
	}
	catch (BIEException & e)
	{
	  consoleFile << e.getErrorMessage() << endl;
	  if (ttool) consoleFile.flush();
	}

        for (int symindex = 0; symindex < (int)matchingsymbols.size(); symindex++)
        {
          consoleFile << matchingsymbols[symindex] << "\n";
	  if (ttool) consoleFile.flush();
        }
      }
    | VISUALIZE VAR NEWLINE
      {
        // invoke graphical display program
        char viscommand[255];
        strcpy(viscommand,visualizedir.data());
        strcat(viscommand,"/visualize ");
        consoleFile << $2;
        consoleFile.flush();
        strcat(viscommand,$2);
        (void)system(viscommand);
      }
    | HELP NEWLINE
      {
	char iline[256];
        consoleFile << "You may get help on these topics:\n\n";
        consoleFile.flush();
	ostringstream ostr;

	print_help_dir();

        consoleFile << "please select topic: ";
        consoleFile.flush();
        cin >> filename;
	if (string(filename).size()) {
	  ostr.str("");  // reset string stream
	  ostr << helpdir << "/" << filename;
	  ifstream infile(ostr.str().c_str());
	  if (!infile) { 
	    consoleFile << "non-existent help topic" << endl;
	  }
	  else {
	    while (!infile.eof()) {
	      infile.getline(iline, 256);
	      consoleFile << iline << endl;
	    }
	    infile.close();
	  }
	  consoleFile.flush();
	  cin.ignore(); /* ignore one return, otherwise we
			   get the prompt twice */
	}
      }
    | HELP VAR NEWLINE
      {
	print_help_file($2);
      }
    | HELP CODEVER NEWLINE
      {
	print_help_file("version");
      }
    | HELP DESC NEWLINE
      {
	print_help_file("desc");
      }
    | HELP EXIT NEWLINE
      {
	print_help_file("exit");
      }
    | HELP HELP NEWLINE
      {
	print_help_file("help");
      }
    | HELP NEW NEWLINE
      {
	print_help_file("new");
      }
    | HELP LIST NEWLINE
      {
	print_help_file("list");
      }
    | HELP PRINT NEWLINE
      {
	print_help_file("print");
      }
    | HELP PRINTALL NEWLINE
      {
	print_help_file("printall");
      }
    | HELP PRINTOBJECT NEWLINE
      {
	print_help_file("printobject");
      }
    | HELP SET NEWLINE
      {
	print_help_file("set");
      }
    | HELP VISUALIZE NEWLINE
      {
	print_help_file("visualize");
      }
    | WAIT NEWLINE
      {
	while (1==1) {};
      }
    | IGNORE INTERACTIVE NEWLINE 
      {
        accept_interactive = false;
      }
    | ACCEPT INTERACTIVE NEWLINE
      {
        accept_interactive = true;
      }
    | INTERACTIVE NEWLINE
      {
        if (!accept_interactive)
	{
	  consoleFile << "Cannot accept interactive command.  Use accept ";
	  consoleFile << "interactive to change settings." << endl;
	}
	else if ((mpi_used == true) && (myid != 0))
	{
	  // consoleFile << "Slave ignoring request to go interactive." << endl;
	}
	else
	{
          consoleFile << "Going back to interactive\n";
          
				// Close input file
          inFile.close();
	  if (inFile) perror("closing stdin file");	

				// Reassign buffer and prompt
	  cin.rdbuf(BIE::inbuf); 
	  strcpy(prompt,standard_prompt); 

          interactive = true;
        }
      }
    | LIST CLASSES NEWLINE 
      { 
        MethodTable::print_classes(); 
      }
    | LIST METHODS VAR NEWLINE 
      { 
	try {
	  MethodTable::print_methods($3); 
	} 
	catch (BIEException & e)
	{
	  cout << e.getErrorMessage() << endl;
	}	
      }	
    | LIST STREAMS NEWLINE
      {
        /* list all the inheritable output streams */
        vector<RecordOutputStream*> roslist;
	roslist = RecordOutputStream::getInheritableStreams();
	
	for (int rosindex = 0; rosindex < (int) roslist.size(); rosindex++)
	{
	  if (roslist[rosindex])
	  {
            consoleFile << "[" << rosindex+1 << "] ";
            int numfields = roslist[rosindex]->numFields();
            
            for (int fieldindex = 1; fieldindex <= numfields; fieldindex++)
            { 
              if (fieldindex != 1) { consoleFile << ", "; }
              consoleFile << roslist[rosindex]->getFieldName(fieldindex);
            }
                    
            consoleFile << endl;
	  }
	}
      }
    | LIST VAR NEWLINE
      {
	class_print(MethodTable::get_derived($2, true));
      }
    | LIST CLASSTREE NEWLINE
      {
	MethodTable::print_graph();
      }
    | DESC VAR NEWLINE 
      { 
        try {
	  consoleFile << MethodTable::get_class_desc($2,true) << endl; 
          consoleFile.flush();
          delete $2; 
	} catch (BIEException & e)  {
           consoleFile << e.getErrorMessage() << endl;
	}
      }
    | DESC VAR VAR NEWLINE{ 
        try {
          consoleFile << MethodTable::get_method_desc($2, $3, true) << endl; 
          consoleFile.flush();
          delete $2; 
          delete $3;
	} catch (BIEException & e)  {
           consoleFile << e.getErrorMessage() << endl;
	}
      }
    | CREATE SOCKET NEWLINE{
        cli_sockid = socket(AF_INET,SOCK_STREAM,IPPROTO_IP);
        if (cli_sockid < 0)
        {
          cout <<"problem creating new socket for user \n";
          cout.flush();
          exit(-1);
        }
        bzero((char *)&cli_sock_addr, sizeof(cli_sock_addr));
        port_id = 8000 +(int)(rand()/(RAND_MAX+1.0)*1000); 
        cli_sock_addr.sin_family = AF_INET;
        cli_sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        cli_sock_addr.sin_port = htons(port_id);
        
        while (bind(cli_sockid,(struct sockaddr *)&cli_sock_addr,
           sizeof(cli_sock_addr)) < 0)
        {
          // go to the rand generateor again and get a new number and try
          port_id = 8000 +(int)(rand()/(RAND_MAX+1.0)*1000); 
          cli_sock_addr.sin_port = htons(port_id);
        }
       
        if(listen(cli_sockid, SOMAXCONN) < 0)
        {
          cout << "cli: error in listening\n";
          cout.flush();
          close(cli_sockid);
          exit(-1);
        }
       
        // send the portid back to GUI 
        cout << "socketid " << port_id << "\n";
        cout.flush();


        graphics_output_handler_ptr = new GraphicsSocketHandler;
	
        graphics_output_handler_ptr->Start();

       }
     | PNEWSESSION VAR NEWLINE {
        PersistenceControl::pnewsession($2, consoleFile);
      }
     | PNEWSESSION VAR VAR NEWLINE {
        PersistenceControl::pnewsession($2, $3, consoleFile);
      }
     | PSAVE NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::psave(consoleFile);
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | PSAVE LITSTRING {
 #if PERSISTENCE_ENABLED
        PersistenceControl::psave($2, consoleFile);
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | PRESTORE VAR INT NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::prestore($2, $3, consoleFile);
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | PRESTORE VAR NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::prestore($2, consoleFile);
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | PHISTORY VAR INT NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::phistory($2,$3,consoleFile);        
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | PINFO VAR NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::pmetainfo($2,consoleFile);        
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | PINFO VAR INT NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::pmetainfo($2,$3,consoleFile);        
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | PLIST NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::plist(consoleFile);        
 #else
         consoleFile << "Persistence is not enabled in this build" << endl;
 #endif
      }
     | CKON NEWLINE {
       PersistenceControl::ckon(consoleFile);
      }
     | CKOFF NEWLINE {
       PersistenceControl::ckoff(consoleFile);
      }
     | CKTOGGLE NEWLINE {
       PersistenceControl::cktoggle(consoleFile);
      }
     | CKINTERVAL INT NEWLINE {
       PersistenceControl::ckinterval($2, consoleFile);
      }
     | CKTIMER INT NEWLINE {
       PersistenceControl::cktimer($2, consoleFile);
      }
     | CKMANUAL NEWLINE {
       PersistenceControl::ckmanual(consoleFile);
      }
     | STARTCLOCK NEWLINE {
       timer.start();
       if (myid==0) {
	 cout << "Watch started" << endl;
       }
      }
     | STOPCLOCK NEWLINE {
       TimeElapsed dt = timer.stop();
       if (myid==0) {
	 stopwatch("Watch stopped, time elapsed: ", dt.getRealTime());
       }
      }
     | RESETCLOCK NEWLINE {
       timer.reset();
       if (myid==0) {
	 cout << "Watch reset to zero" << endl;
       }
      }
     | ELAPSEDTIME NEWLINE {
       TimeElapsed dt = timer.getTime();
       if (myid==0) {
	 stopwatch("Current elapsed time: ", dt.getRealTime());
       }
      }
     | GET VAR NEWLINE
      { 
        vector<string> matchingsymbols;
	try {
	  matchingsymbols = st->get_symbols_by_class($2);
	}
	catch (BIEException & e)
	{
	  consoleFile << e.getErrorMessage() << endl;
	}
	
        for (int symindex = 0; symindex < (int)matchingsymbols.size(); symindex++)
        {
          consoleFile << " " << matchingsymbols[symindex];
        }
	consoleFile.flush();
      }
     | PVERSIONS VAR NEWLINE {
 #if PERSISTENCE_ENABLED
        PersistenceControl::pversions($2, cout);        
 #else
         cout << "Persistence is not enabled in this build" << endl;
 #endif
      }
;
             
statement:  
      forloop
    | assignment
    | printstatement
    ;

forloop:
      FOR VAR EQUALS INT TO INT assignment
      {
        $$ = new ForStatement($2, $4, $6, $7);
      }
      ;

assignment: 
      SET VAR EQUALS rhs
      { 
        $$ = new AssignStatement($2, $4);
      }
      ;

printstatement:
     PRINT rhs
     {
       $$ = new PrintStatement($2);
     }
     ;

rhs: 
      value { $$ = new SimpleExpr($1); }
    | call
    ;

call:  
      constructor|
      method_call
    ;

constructor:
      NEW VAR args 
      { 
        $$ = new CallExpr($2, $3);
      } 
    | NEW VAR LB offset_val RB
      {
	$$ = new CallExpr($2, $4); 
      }
    ;

method_call:
     VAR PTR VAR args 
      {
        $$ = new CallExpr($1, $3, $4);
      }
    ;

args:  
      /* empty */ 
      {
        // Make an empty arglist.
	$$ = new CliArgList();
      }
    | LP arglist RP 
      {
        $$=$2;
      }
    ;

arglist: 
      /* empty */
      {
        $$ = new CliArgList();
      }
    | arglist COMMA value { 
        $$ = $1;
	$$->add_element($3); 
      }
    | value
      {
        $$ = new CliArgList();
	$$->add_element($1);
      }
    ;

value:
      VAR 
      {  
	$$ = new AData(AData::variable_TYPE);
	$$->set_variable($1);
      } 
    | VAR LB offset_val RB 
      { 
        $$ = new AData(AData::arrayelement_TYPE);

	CliArgList * arglist = new CliArgList();
	
	AData * var = new AData(AData::variable_TYPE);
	var->set_variable($1);
	arglist->add_element(var);
	arglist->add_element($3);
      }
    | AMP VAR LB offset_val RB 
      { 
        $$ = new AData(AData::arrayelementptr_TYPE);

	CliArgList * arglist = new CliArgList();
	
	AData * var = new AData(AData::variable_TYPE);
	var->set_variable($2);
	arglist->add_element(var);
	arglist->add_element($4);
      }    
    | LITSTRING
      { 
        $$ = new AData(AData::string_TYPE);
	$$->set_string($1);
      }
    | INT
      { 
        $$ = new AData(AData::int32_TYPE);
	$$->set_int32($1);
      }
    | DOUBLE
      { 
        $$ = new AData(AData::double_TYPE);
	$$->set_double($1);
      }
    | TRUE
      {
        $$ = new AData(AData::bool_TYPE);
	$$->set_bool(true);
      }
    | FALSE
      {
        $$ = new AData(AData::bool_TYPE);
	$$->set_bool(false);
      }
    | BASICTYPE_INT
      {
        $$ = new AData((void*)BasicType::Int, "BasicType");
      }
    | BASICTYPE_REAL
      {
	$$ = new AData((void*)BasicType::Real, "BasicType");
      }
    | BASICTYPE_BOOL
      {
	$$ = new AData((void*)BasicType::Bool, "BasicType");
      }
    | BASICTYPE_STRING
      {
        $$ = new AData((void*)BasicType::String, "BasicType");
      }
    ;

offset_val:
       VAR 
       { 
         $$ = new AData(AData::variablearrayindex_TYPE); 
	 $$->set_variablearrayindex($1);
       }
     | INT 
       {
         $$ = new AData(AData::arrayindex_TYPE); 
         $$->set_arrayindex($1);
       }
    ;
%%

int yyerror(const char* msg)
{
  if (myid==0 and parse_error) {
    ostream cout(checkTable("cli_console"));
    consoleFile << msg;
    //    cout << msg;
  }
  
  return 1;
}  

void set_as_limit (long size);

int
main(int argc, char *argv[])

// flags : -f          for input redirection
//         -d          for output redirection (for main process)
//         -stf        send non-root output to a file
//         -seed N     set the random number seed
//         -v M        maximum virtual memory address space
//         -gui        for GUI interface
//         -mpi        use MPI, overriding automatic detection 
//         --version   print version and exit

{
#ifdef USE_FE_TRAPS
  // Trap the really bad ones . . . 
  feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
#endif

  int seed = 11;
  string slave_output;

  try {

    ostream cout(checkTable("cli_console"));
    consoleFile.basic_ios<char>::rdbuf(cout.rdbuf());

    mpi_init(argc, argv);

    for (int i=1; i<argc; i++) { 
      if (strcmp(argv[i],"--version")==0) {
	if (!mpi_used || myid==0) {
	  cout << PACKAGE_STRING 
	       << ", Version " << VERSION
	       << ", Copyright (C) 1999-2010 by Martin D. Weinberg" 
	       << endl << endl
	       << "Compile options:" << endl << endl
	       << poptions("Cluster test is enabled",
			   "Users can ignore this", 
			   CLUSTER_ENABLED)
	       << poptions("Paranoid mode is enabled" ,
			   "No save set overwrite on resume",
			   PARANOID_MODE)
	       << poptions("Persistence is enabled",
			   "Serialization and checkpointing",
			   PERSISTENCE_ENABLED)
	       << poptions("User project compiled",
			   "Project dir is detected and compiled",
			   PROJECT_ENABLED)
	       << poptions("Visualizer is enabled",
			   "GTK graphical display of posterior",
			   VISUALIZER_ENABLED)
	       << poptions("FITS is enabled",
			   "Can read data in FITS table format",
			   LIBCFITSIO_ENABLED)
	       << poptions("Tesstool is enabled",
			   "Experimental tessellation GUI",
			   TESSTOOL_ENABLED)
	       << endl;
	}
	mpi_quit();
	exit(0);
      }	
    }

    if (mpi_used) {
      if (myid==0) {
	cout << setw(60) << setfill('-') << '-' << endl;
	cout << numprocs 
	     << " processes detected . . . assuming parallel execution" << endl;
      }
    } else {
      cout << setw(60) << setfill('-') << '-' << endl;
      cout << "Single process detected . . . assuming serial execution" << endl;
    }

    if (mpi_used) {
      if (myid==0) {
	cout << setw(60) << setfill('-') << '-' << endl;
      }
    } else {
      cout << setw(60) << setfill('-') << '-' << endl;
    }

  
    for (int i = 1; i < argc; i++) { 

				// Set random number seed
      if (strcmp(argv[i],"-seed")==0)
	{
	  seed = atoi(argv[i+1]);
	}

      // if ((strcmp(argv[i],"-f")==0) && (i < (argc-1)) && (!mpi_used || !myid) )
      if ((strcmp(argv[i],"-f")==0) && i < (argc-1))
	{
				// Ressign standard input
	  inFile.open(argv[i+1]);
	  if (!inFile) perror("open input file (-f)");
	  inbuf = std::cin.rdbuf();
	  // find if the file is a fifo, work around for ifstream buffering bug ?
	  struct stat stat_info;
	  int rv = stat(argv[i+1], &stat_info);
	  if (rv == -1) 
	    {
	      perror("could not stat -f argument");
	    } else {
	      if( S_ISFIFO(stat_info.st_mode) ) {
		inFile.rdbuf()->pubsetbuf(ch_inbuf, 1);	
		ttool=true;
	      }	
	    }
	  std::cin.rdbuf(inFile.rdbuf());
	  interactive = false;
	}

      if ((strcmp(argv[i],"-c")==0) && (i < (argc-1)) && (!mpi_used || !myid) )
	{
	  // Ressign standard consoleFile to pipe
	  clioutFile.open(argv[i+1]);
	  if (!clioutFile) perror("open console file (-c)");
	  struct stat stat_info;
	  int rv = stat(argv[i+1], &stat_info);
	  if (rv == -1) 
	    {
	      perror("could not stat -c argument");
	    } else {
	      if( S_ISFIFO(stat_info.st_mode) ) {
		clioutFile.rdbuf()->pubsetbuf(ch_conbuf, 1);	
		ttool=true;
	      }	
	    }
	  if (!consoleFile) perror("consoleFile");
	  consoleFile.basic_ios<char>::rdbuf(clioutFile.rdbuf());
	  interactive = false;
	}
  
  
      if ((strcmp(argv[i],"-d")==0) && (i < (argc-1)) && (!mpi_used || !myid))
	{
	  // Open new output file
	  logFile.open(argv[i+1]);
	  if (!logFile) perror("opening output file (-d)");
	  struct stat stat_info;
	  int rv = stat(argv[i+1], &stat_info);
	  if (rv == -1) 
	    {
	      perror("could not stat -d argument");
	    } else {
	      if( S_ISFIFO(stat_info.st_mode) ) {
		logFile.rdbuf()->pubsetbuf(ch_logbuf, 1);	
		ttool=true;
	      }	
	    }
	  
	  // Reassign standard output and error to the file
	  outbuf = std::cout.rdbuf();
	  std::cout.rdbuf(logFile.rdbuf());

	  errbuf = std::cerr.rdbuf();
	  std::cerr.rdbuf(logFile.rdbuf());
	}
  
      if ((strcmp(argv[i],"-v")==0) && (i < (argc-1)))
	{
	  set_as_limit((long)(1024*1024*atoi(argv[i+1])));
	}
      
      if ((strcmp(argv[i],"-gui")==0)) 
	{
	  gui_used = true;
	}
      
      if ((strcmp(argv[i],"-stf")==0))
	{
	  slave_output = "outerr_for_process";
	}
    }
    
    if (mpi_used) mpi_slave_output(slave_output);

    // Identify the version (so one's log file has version info
    // and an image of a nice cat)

    if (!mpi_used || myid==0) {
      const unsigned sz = 22;
      ostringstream a, b;
      a << setw((sz - strlen(PACKAGE))/2) << "" << PACKAGE;
      b << setw((sz - strlen(VERSION))/2) << "" << VERSION;
      banner(interactive, 0);
      cout << setfill(' ') << endl << endl;
      cout << setw(sz) << left << " "      << sammi[0] << endl;
      cout << setw(sz) << left << rules[0] << sammi[1] << endl;
      cout << setw(sz) << left << " "      << sammi[2] << endl;
      cout << setw(sz) << left << a.str()  << sammi[3] << endl;
      cout << setw(sz) << left << b.str()  << sammi[4] << endl;
      cout << setw(sz) << left << rules[1] << sammi[5] << endl;
      cout << endl << endl;
      banner(interactive, 2);
    }

    if ((!mpi_used || myid == 0) && (interactive == true)) {
      strcpy(prompt,standard_prompt);
      ifstream in(string(helpdir + "/intro").c_str());
      if (!in) {
	cout << "BIE can not find your help diretory . . ." << endl;
	cout << "Make sure that the helpdir variable in the .bierc file points" << endl
	     << "to an original or copy of ./cli/help from the source tree" << endl;
      } else {
	ostringstream intro;
	intro << "cat " << helpdir << "/intro";
	system(intro.str().c_str());
      }	

    }
    else 
    {
      strcpy(prompt,"");
    } 
  

    // Initialize the random number generator for each node

    BIEgen = new BIEACG(seed+myid, 25); 
  
    // Initialize the MethodTable class, which allows reflective calls
    // to methods and constructors.
    MethodTable::initialize(methodstring);
    
    // Create a new symbol table object.
    st = new SymTab;
    
    // Says we might interact with the history, and that the number
    // of commands remembered should be unlimited.
    using_history();
    read_history(histfile.c_str());
    unstifle_history();
  
    // Set signal handler for manual checkpointing
    signal(SIGQUIT, catch_ckpt);

    yyparse();

    PersistenceControl::ckcleanup();
   
    mpi_quit();
    if (myid==0) {
      cout << endl << exit_message << endl;
      if (savehist) {		// Save "savehist" of the last states
				// in "histfile"
	stifle_history(savehist);
	write_history (histfile.c_str());
      }
    }
  }
  catch (BIEException & e)
  { 
    cout << "Exception caught by main routine in " << __FILE__ << ":" << endl;
    cout << e.getErrorMessage() << endl; 
  }

  return 0;
}
