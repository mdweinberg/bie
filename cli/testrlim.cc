#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <iostream>

extern void set_as_limit (long size);
extern long get_cur_vm(pid_t pid);

int main(int argc, char** argv)
{
  struct rlimit rlim;
  
  set_as_limit (100*1024*1024);

  if (!getrlimit(RLIMIT_AS, &rlim)) {
    std::cout << "Mem limit (cur) = " << rlim.rlim_cur/1024 << " kB\n";
    std::cout << "Mem limit (max) = " << rlim.rlim_max/1024 << " kB\n";
  }
  
  const unsigned long int csize = 80*1024*1024;
  char *tmp = new char [csize];

  std::cout << "VM size = " << get_cur_vm(getpid())/1024 << " kB\n";

  return 0;
}

