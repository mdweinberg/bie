%{
#include <iostream>
#include <mpi.h>
#include <string>
using namespace std;

#include "Statement.h"
#include "Expr.h"
#include "BIEmpi.h"
#include "gvariable.h"
#include "BIEMutex.h"
#include "rline.h"

#include "../config.h"
#if defined(BISON_USE_PARSER_H_EXTENSION)
#   include "cli.h"
#else
#   include "cli.hh"
#endif

#include "SymTab.h"
#include "AData.h"
#define PARSE_LL
#include "RunSimulation.h"
#undef  PARSE_LL

#include "BIEException.h"

extern int numprocs;

using namespace BIE;

#define YY_INPUT(buf,result,max_size) \
   { \
     if (mpi_used == true) { \
       if (myid == 0) { \
         char c; \
         if (gui_used == true) { \
           c = getchar(); \
         } \
         else \
         { \
           c = rl_gets(); \
         } \
         buf[0] = c; \
         ++MPIMutex; \
         MPI_Bcast(buf, 1,  MPI_CHAR, 0, CLI_COMM_WORLD);\
         --MPIMutex; \
         if (simulationInProgress) { \
	   const char *symbol;\
	   AData *data;\
	   RunSimulation* siml;\
           vector<string> matchingsymbols;\
	   try {\
	     matchingsymbols = st->get_symbols_by_class("RunSimulation");\
	   }\
	   catch (BIEException e)\
	   {\
	     consoleFile << e.getErrorMessage() << endl;\
	   }\
	   if(firstCommandChar) {\
	      firstCommandChar=0; \
	      for (int symindex = 0; symindex < (int)matchingsymbols.size(); symindex++)\
	      {\
	        symbol = matchingsymbols[symindex].c_str();\
	        data = st->get_symbol_value(symbol);\
	        siml = static_cast <RunSimulation*> (data->get_pointer());\
	        siml->SwitchOnCLI(); \
	      } \
	   }\
           if(buf[0] == '\n') {\
	      likelihoodSem->post(); \
	      firstCommandChar = 1;\
	   } \
	 } \
         /* fprintf(stderr, "YY_INPUT::myid=%d Sent %c\n", myid, c); */ \
         result = (c == EOF) ? YY_NULL : (buf[0] = c, 1); \
       } \
       else \
       { \
	 char c;\
         /* fprintf(stderr, "YY_INPUT::Waiting for mutex\n"); */ \
         if (!simulationInProgress) { \
	   ++MPIMutex; \
	   MPI_Bcast(buf, 1,  MPI_CHAR, 0, CLI_COMM_WORLD);\
           --MPIMutex; \
	 } else {\
	   if(firstCommandChar) {\
	     switchCLISem->wait(); \
	     firstCommandChar=0; \
	   }\
	   ++MPIMutex; \
	   MPI_Bcast(buf, 1,  MPI_CHAR, 0, CLI_COMM_WORLD);\
           --MPIMutex; \
	   if(buf[0] == '\n') {\
	     likelihoodSem->post(); \
	     firstCommandChar = 1;\
	   } \
	 } \
         c = buf[0]; \
         result = (c == EOF) ? YY_NULL : 1; \
     } \
     } \
     else { \
       char c; \
       if (gui_used == true) { \
         c = getchar(); \
       } \
       else { \
         c = rl_gets(); \
       } \
       result = (c == EOF) ? YY_NULL : (buf[0] = c, 1); \
     } \
   } 
%}

%s COMMENT_STATE
VAR [a-zA-Z][<>a-zA-Z0-9_./~:]*
LITSTRING \"([^\n\"\\]|\\(n|t|\\|\"))*\"
DIGIT [0-9]

%%

^[ \t]*"#" { BEGIN COMMENT_STATE; }
<COMMENT_STATE>.* { ; }
<COMMENT_STATE>\n { BEGIN 0; }

set {
  return SET;
}

get {
  return GET;
}

new {
  return NEW;
}

exit {
  return EXIT;
}

quit {
  return QUIT;
}

print {
  return PRINT;
}

printall {
  return PRINTALL;
}

printobject {
  return PRINTOBJECT;
}

for {
  return FOR;
}

in {
  return IN;
}

to {
  return TO;
}

help	{
        return HELP;
	}

wait 	{
	return WAIT;
	}

interactive {
	return INTERACTIVE;
	}

accept	{
	return ACCEPT;
	}

ignore 	{
	return IGNORE;
	}

list 	{
        return LIST;
        }

desc 	{
        return DESC;
        }

create 	{
      	return CREATE;
       	}

socket 	{
	return SOCKET;
	}

classes {
	return CLASSES;
	}  

streams {
	return STREAMS;
	}

methods	{
	return METHODS;
	}  

classtree {
        return CLASSTREE;
        }

string	{
	return BASICTYPE_STRING;
	}

int    {
         return BASICTYPE_INT;
       }

real   {
         return BASICTYPE_REAL;
       }

bool   {
         return BASICTYPE_BOOL;
       }

true   {
         return TRUE;
       }

false  {
         return FALSE;
       }

pnewsession {
         return PNEWSESSION;
       }
psave  {
         return PSAVE;
       }

prestore {
         return PRESTORE;
       }

phistory {
         return PHISTORY;
       }

plist {
         return PLIST;
       }

pinfo {
         return PINFO;
       }

pversions {
         return PVERSIONS;
       }

ckon {
         return CKON;
       }

ckoff {
         return CKOFF;
       }

cktoggle {
         return CKTOGGLE;
       }

ckinterval {
         return CKINTERVAL;
       }

cktimer {
        return CKTIMER;
       }
ckmanual {
        return CKMANUAL;
      }        
startclock {
        return STARTCLOCK;
      }        
stopclock {
        return STOPCLOCK;
      }        
resetclock {
        return RESETCLOCK;
      }        
elapsedtime {
        return ELAPSEDTIME;
      }        

history  {
         return HISTORY;
       }

visualize {
            return VISUALIZE;
          }

version {
            return CODEVER;
          }

{VAR} {
  yylval.str = new char[strlen(yytext)+1];
  strcpy(yylval.str, yytext);
  return VAR;
}

{LITSTRING} {
  yytext[strlen(yytext)-1] = '\0';
  yylval.str = &yytext[1];
  return LITSTRING;
}

(\-)?{DIGIT}+ {
  yylval.ival = atoi(yytext);
  return INT;
}

(\-)?{DIGIT}+"."{DIGIT}* {
  yylval.dval = atof(yytext);
  return DOUBLE;
}

\n {
  return NEWLINE;
}

\015 {
}

= {
  return EQUALS;
}

"(" {
  return LP;
}

")" {
  return RP;
}

"[" {
  return LB;
}

"]" {
  return RB;
}


"," {
  return COMMA;
}

"&" {
  return AMP;
}

"->" {
  return PTR;
}

[ \t]+ {
}

. { 
}

%%
