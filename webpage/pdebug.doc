/*! \page pdebug Parallel debugging with MPI

\section basics BASICS

<code>mpirun</code> can be used to launch non-MPI programs as long as
the programs that you run eventually launch LAM/MPI programs.
Therefore, you can use <code>mpirun</code>1 to launch debuggers on
remote nodes.

Alternatively (and maybe in combination), I often use
<code>cout</code> and <code>cerr</code> statements liberally.  The
downside of this approach is that output from the nodes can overlap
and this sort of i/o is slow.

\section debugger Launching a debugger for each rank: background

Since all ranks except Rank 0 have their stdin tied to
<code>/dev/null</code> by default, you must start
text-based debuggers (such as gdb) in separate X windows.

For text debuggers, you will need a short shell script to launch an
xterm (or whatever your favorite X window program is -- not all
systems have xterm -- other terminal programs can be used instead,
such as konsole, gnome_terminal, etc.). For example:

\code
    % mpirun N -x DISPLAY run_gdb.csh my_program_name
\endcode

The shell script <code>run_gdb.csh</code> needs to be in your path (I
usually put it in <code>~/bin</code>), and
<code>my_program_name</code> is the name of your LAM/MPI
executable. An example <code>run_gdb.csh</code> is shown below:

\code
    #!/bin/csh -f

    echo "Running GDB on node `hostname`"
    xterm -e gdb $*
    exit 0
\endcode

Also note that the DISPLAY environment variable is exported to the
processes nodes with <code>mpirun</code>. This is necessary so that the
remote processes know where to send the X display of the
<code>xterm</code>. 

NB: At this point, I have only had success using local machines of
this procedure.  I recommend using a single machine for this.  The
buzzard cluster, is not correctly forwarding X11 because of the NFSed
home setup is confusing xauth (I think).

Once <code>gdb</code> starts, you then set whatever breakpoints you
need and begin the execution with <code>run</code> in each window
followed by the command-line arguments, e.g.: 
\code
    run -mpi -f script0
\endcode

Alternatively, it is sometimes easier to debug one process at a time
with the following script:

\code
    #!/bin/csh -f

    if ("$LAMRANK" == "0") then
      gdb $*
    else
      $*
    endif
    exit 0
\endcode


*/
