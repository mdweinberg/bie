/*! \page rjpage Peter Green's Reversible Jump Algorithm

<h3>DESCRIPTION</h3>

  The Green "Reversible Jump" algorithm is implemented for two
  distinct case:
  <ol>
  <li> For mixture models.  These models have only birth and death
  moves, that is adding or deleting a component in the mixture.  The
  StateInfo keeps track of both the maximum number of components
  allowed and the current number of components in the mixture.  The
  number of mixture components has a Poisson prior whose mean is
  specified by at construction.
  <li> For two models (not a mixture) whose parameters are encoded in
  a single vector.  The first component in the parameter vector is the
  indicator (0 for Model 1 and 1 for Model 2).  Each implementation
  possibility of having parameters in common and parameters that are
  constructed by splits, joins, and bijective mappings with random
  components.  These choices are made at construction and must agree
  with the user-supplied model or likelihood function.
  </ol>

<h3>DESIGN</h3>

  For the first case, the StateInfo type must be Mixture.  The BIE
  uses the Mcur variable to access the number of components.  For the
  second case, the Stateinfo type must be RJTwo.  This provides some
  sanity checking in the class ReversibleJumpTwoModel that implements
  the algorithm and access to the internal parameters of each state by
  other classes, such as GelmanRubinConverge.

  The mapping between the two models in the second case is defined by
  the helper class RJMapping.  Currently there are two types of mappings:
  <ol>

  <li> Mappings of one parameter in Model 1 to one parameter in Model
  2 (and vice versa) using two random variates of independently
  specified distribution: \f$(\theta_1, u_1)\rightarrow(\theta_2,
  u_2)\f$.  The mappings take one of two forms, either additive or
  multiplicative.
  <ul>
  <li> Additive: 
  \f[
  \theta_2 = \theta_1 + u_1
  \f]
  \f[
  u_2 = \theta_1 - u_1
  \f]
  with the inverse
  \f[
  \theta_1 = (\theta_2 + u_2)/2
  \f]
  \f[
  u_1 = (\theta_2 - u_2)/2.
  \f]
  <li> Multiplicative:
  \f[
  \theta_2 = \theta_1 u_1
  \f]
  \f[
  u_2 = \theta_1/u_1
  \f]
  with the inverse
  \f[
  \theta_1 = \sqrt{\theta_2 u_2}
  \f]
  \f[
  u_1 = \sqrt{\theta_2 /u_2}
  \f]
  where we assume that \f$\theta, u>0\f$.
  </ul>
  <li> Mappings of one parameter in Model 1 to two parameter in Model
  2 (split) and vice for a join using one random variate: \f$(\theta,
  u)\rightarrow(\theta_1,\theta_2)\f$.  These mappings also take one
  of two forms, either additive or multiplicative.
  <ul>
  <li> Additive: 
  \f[
  \theta_1 = \theta + u
  \f]
  \f[
  \theta_2 = \theta - u
  \f]
  with the inverse
  \f[
  \theta = (\theta_1 + \theta_2)/2
  \f]
  \f[
  u = (\theta_1 - \theta_2)/2.
  \f]
  <li> Multiplicative:
  \f[
  \theta_1 = \theta u
  \f]
  \f[
  \theta_2 = \theta/u
  \f]
  with the inverse
  \f[
  \theta = \sqrt{\theta_1 \theta_2}
  \f]
  \f[
  u = \sqrt{\theta_1 /\theta_2}
  \f]
  where, again, we assume that \f$\theta, u>0\f$.
  </ul>
  </ol>

<h3>EXAMPLE CLI SESSIONS</h3>

  In CLI, one constructs the simulation as usual, instantiating one of
  the reversible jump classes (ReversibleJump for Case 1 and
  ReversibleJumpTwoModel for Case 2) for the mca instance.

  In Case 1, that is all that is necessary.  The maximum number of
  mixture components and the Poisson-prior mean is specified in the
  constructor.

  In Case 2, the prior is specified as usual using the Prior class.
  The prior for the first component is most naturally specified as a
  uniform discrete distribution, e.g.:
  \verbatim
  	set disc  = new DiscUnifDist(0, 1)
  \endverbatim

  For an example of mapping specification, assume that we have a model
  where Model 1 and Model 2 are one- and two-component one dimensional
  normal distributions with mean and variance as parameters.  We
  specify the mapping between Model 1 and Model 2 using using two
  split moves: one for the mean in Model 1 and one for the variance in
  Model 1.  Each split gives the corresponding parameters for Model 2.
  \verbatim
  #
  set nrm   = new NormalDist(0.0, 0.01)
  set sig   = new WeibullDist(1.0, 4.0)
  #
  set map = new clivectorRJmap(2)
  set rjm1  = new RJMapping()
  set rjm2  = new RJMapping()
  rjm1->SetOneToTwo(1, 3, 5, 0, nrm)
  #                          ^
  #                          |
  # Using the additive ------/
  # mapping here
  #
  rjm2->SetOneToTwo(2, 4, 6, 1, sig)
  #                          ^
  #                          |
  # Using a multiplicative---/
  # mapping here
  #
  map->setval(0, rjm1)
  map->setval(1, rjm2)
  #
  set mca = new ReversibleJumpTwoModel(si, map)
  # 
\endverbatim
 
*/

