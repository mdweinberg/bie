/*! \page project_goals Project Goals

To develop a general purpose statistical inference engine to enable
investigators in a wide variety of fields to make use of large
(terabyte) databases to confront and test physical models. The system
targets spatially-mapped heterogeneous data, typical of remote sensing
problems. These problems often require simultaneous interpretation of
current and archival databases or data collected by multiple sensors
with different spatial resolution, type, and sensitivity. For example,
researchers in geological,forest and natural resource management,
atmospheric, and biological/environmental sciences are using both
space- and Earth-based observation networks to accumulate large
databases of spatially global information of many types. Similarly,
astronomers are mapping large regions of the sky to compile catalogs
of sources at different wavebands and differing positional
accuracy. The system is designed to free the scientist from the
mechanics of large-scale statistical analysis and to make the
knowledge obtained at each stage available to direct the ongoing
exploration.

Specific details:
- \subpage persistence	 
- \subpage visualization	 
- \subpage visualizer
- \subpage GUI

*/
