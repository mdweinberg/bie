/*! \page tesstool TessTool

<br>
<h2> TessTool is currently out of service</h2>
This tool may be revived at a later date so I'm leaving the documentation 
here.

<br>

<h3>DESCRIPTION</h3>

  TessTool is a graphical user interface for visualizing the data and
  model, as well as various attributes of the individual tiles.  We
  hope to include a manipulator for the tessellation itself but this
  is not currently implemented.

<h3>DESIGN</h3>

  The TessToolSender class calls the persistence subsystem to
  serialize and store the current tessellation.  The likelihood
  computation has hooks to forward the model and data to a separate
  "consumer" process waiting to receive data.

  There are two versions of the consumer process:
  <ol>
  <li> With a controller process that connects to the MPI world
  that starts and controls the simulation
  <li> With a process spawned from the CLI to receive and write the
  data.
  </ol>


<h3>EXAMPLE CLI SESSIONS</h3>

  In CLI, one constructs the simulation as usual.  Before starting the
  simulation, one creates the TessToolSender as follows:

  For the controller method:
\verbatim
	set tts = new TessToolSender()
	like->SetTessTool(tts)
	sim->RunThread()
\endverbatim
where <code>sim</code> is the simulation instance.
  
  For the controller writer:
\verbatim
	sim->SetAutoTessTool(1)
	sim->RunThread()
\endverbatim

<code>SetAutoTessTool</code> instantiates the
<code>TessToolSender</code> and calls for a new file to be written
after every step.  The writing frequency is the argument to the
<code>SetAutoTessTool()</code> method.
  
  <h3>INSTRUCTIONS FOR USE</h3>
  
    <h4> TessTool controller example</h4>

  Assume the following diretory structure:
\verbatim
  BIE
    src/
    cli/
    tesstool/
    scripts/Galaxy/parallel
\endverbatim
 
 <ol>
 <li> Run server (which invokes cli) on root node
\verbatim
        BIE/cli/server mpirun -np 2 BIE/cli/cli  -mpi -aso 
\endverbatim
 
 <li> Run tesstool on root/other node
 
 Without gdb:
\verbatim
         mpirun -np 1 -nsigs BIE/tesstool/tesstooldriver hostname.umass.edu 8000   
\endverbatim

 With gdb:
\verbatim
         mpirun -np 1 rungdb.bash BIE/tesstool/tesstooldriver
\endverbatim
         where: rungdb and .gdbinit-tesstool are the files that I sent before
 
 <li> In tesstool: send the script named

\verbatim
         script0-with-tesstool
\endverbatim

 <li> Attach VTK
 <li> Select Scalar
 <li> Get Data
 <li> Visualize
</ol> 
 
    <h4> TessTool writer example</h4>

  Assume the following diretory structure:
\verbatim
  BIE
    src/
    cli/
    tesstool/
    scripts/Galaxy/parallel
\endverbatim
 
 <ol>
 <li> Process Data
 <li> Select Scalar
 <li> Visualize
</ol> 
 
*/

