/*! \page visualizer Visualizer

<b>Visualizer</b>

<p> Routines to compute density plots from the simulation output (and
from PostgreSQL generated input data). The current version in the main
source tree is deprecated.  The new version has a separate source tree
and is based on GTK+ and gtkmm (the C++ interface to GTK+) version 2.x
found on most Linux boxes and the Visualization Tool Kit (in some
Linux distributions and available for download from
http://www.kitware.com/vtk).  </p>

<p> Usage should be most self-explanatory. The tool allows one to plot
any two variables against each other ("X-Y Plot") or plot the
distribution in two dimensions ("3D object").  The "Fields" tab
selects the two variables and their ranges.  The final two tabs
control the rendering in the "X-Y Plot".  The "Density controls"
provides the parameters for the density estimation of the Markov Chain
and the "Render controls" provides the contouring parameters. </p>

\image html visualize_1.jpg "Visualizer startup"

\image html visualize_2.jpg "Visualizer field selection page"

*/
