#include <cstddef>
#include <sstream>
#include <cmath>
#include <PointTile.h>
#include <gfunction.h>
#include <BIEException.h>

using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(PointTile)

double PointTile::min_distance = 1.0e-10;

PointTile::PointTile()
{
  node   = NULL;
  points = vector<double>(4, 0.0);
}

PointTile::PointTile(double minx, double maxx, double miny, double maxy)
{
  node   = NULL;
  points = vector<double>(4);
  setup(minx, maxx, minx, miny);
}

PointTile* PointTile::New()
{
  return new PointTile();
}

void PointTile::setup(double minx, double maxx, double miny, double maxy)
{
  points[0] = minx;
  points[1] = maxx;
  points[2] = miny;
  points[3] = maxy;

  if (fabs(points[1]-points[0]) > min_distance ||
      fabs(points[3]-points[2]) > min_distance )
    throw BIEException("PointTile Exception", 
		       "data does not describe a point: need xmin=xmax and ymin=ymax",
		       __FILE__, __LINE__);
}

void PointTile::corners(double& minx, double& maxx, double& miny, double& maxy)
{
  minx = points[0];
  maxx = points[1];
  miny = points[2];
  maxy = points[3];
}

double PointTile::X(double u, double v) 
{
  return points[0];
}

double PointTile::Y(double u, double v) 
{
  return points[2];
}

double PointTile::measure(double u, double v) 
{
  return 0.0;
}

bool PointTile::InTile(double x, double y)
{
  if (fabs(x - points[0]) <= min_distance &&
      fabs(y - points[2]) <= min_distance    ) { return true; }
  else { return false; }
}

bool PointTile::overlapsWith(Tile * tile)
{
  double xmin1, xmax1, ymin1, ymax1;
  double xmin2, xmax2, ymin2, ymax2;

  corners(xmin1, xmax1, ymin1, ymax1);
  tile->corners(xmin2, xmax2, ymin2, ymax2);
  
  // If the other tile is entirely on one side of this tile, or
  // touching a boundary then is not overlapping.
  if ((xmin2 >= xmax1) || (xmax2 <= xmin1) || 
      (ymin2 >= ymax1) || (ymax2 <= ymin1))
    { return false; }
  else
    { return true; }
}

void PointTile::printTile(ostream & outputstream)
{
  outputstream << "X " << points[0] 
               << " Y " << points[2] << endl;
}

void PointTile::printforplot(ostream & outputstream)
{
  outputstream << points[0] << " " << points[2] << endl ;
}

string PointTile::toString()
{
  ostringstream buffer;
  printTile(buffer);
  return buffer.str();
}
