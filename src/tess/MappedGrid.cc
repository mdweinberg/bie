#include <BIEconfig.h>
#include <MappedGrid.h>
#include <BIEException.h>
#include <Node.h>
#include <Tile.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MappedGrid)

using namespace BIE;

MappedGrid::MappedGrid
(Tile * factory, double min_x, double max_x, 
 double min_y, double max_y, int numx, int numy)
{
  vector<int> frntr;

  // Check the input values and bail if they don't make sense.
  if (min_x >= max_x)
  { 
    throw BIEException("MappedGrid Exception", 
     "Min x value is greater than or equal to Max x value", __FILE__, __LINE__); 
  }
  
  if (min_y >= max_y)
  { 
    throw BIEException("MappedGrid Exception", 
     "Min y value is greater than or equal to Max y value", __FILE__, __LINE__); 
  }
  
  if (numx < 1)
  { 
    throw BIEException("MappedGrid Exception", 
     "Number of x divisions is negative", __FILE__, __LINE__); 
  }
  
  if (numy < 1)
  { 
    throw BIEException("MappedGrid Exception", 
     "Number of y divisions is negative", __FILE__, __LINE__); 
  }

  // Take a note of the defining inputs.
  tilefactory = factory;
  Xmin        = min_x;
  Xmax        = max_x;
  Ymin        = min_y;
  Ymax        = max_y;
  nx          = numx;
  ny          = numy;

  // Work out the length and breadth of each tile.
  dX = (max_x - min_x)/nx;
  dY = (max_y - min_y)/ny;

  int tileid = 0;

  // Create the tessellation.
  for (int j=0; j<ny; j++) 
  {
    for (int i=0; i<nx; i++) 
    {
      double minx, maxx, miny, maxy;

      minx = Xmin+dX*i;
      maxx = Xmin+dX*(i+1);
      miny = Ymin+dY*j;
      maxy = Ymin+dY*(j+1);
     
      // Create new tile object
      Tile * newtile = tilefactory->New();
      newtile->setup(minx, maxx, miny, maxy);
      _tiles[tileid] = newtile;
      frntr.push_back(tileid);

      // All tiles are "root" nodes with no children in a tessellation forest.
      rootnodes.push_back(new Node(newtile, tileid, 0, 0));
      
      // This vector will hold all tile ids and is returned by GetRootTiles.
      roottiles.push_back(tileid);
      
      tileid++;
    }
  }

  Ntiles = nx*ny;
}

void MappedGrid::FindAll(double x, double y, vector<int> &found)
{
  /* there's only one to find! */
  found.clear();

  if (x<Xmin || x>=Xmax || y<Ymin || y>=Ymax) return;

  int indx = (int)( (x-Xmin)/dX );
  int indy = (int)( (y-Ymin)/dY );
  
  found.push_back(indy*nx+indx);
}
