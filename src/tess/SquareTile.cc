#include <sstream>
using namespace std;

#include <Tile.h>
#include <SquareTile.h>
#include <gfunction.h>
#include <Node.h>

using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Tile)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::SquareTile)

SquareTile::SquareTile()
{
  points[0] = 0;
  points[1] = 0;
  points[2] = 0;
  points[3] = 0;
  dx = 0;
  dy = 0;
}

SquareTile::SquareTile(double minx, double maxx, double miny, double maxy)
{
  points[0] = minx;
  points[1] = maxx;
  points[2] = miny;
  points[3] = maxy;
  dx = maxx - minx;
  dy = maxy - miny;
}

SquareTile* SquareTile::New()
{
  return new SquareTile();
}

void SquareTile::setup(double minx, double maxx, double miny, double maxy)
{
  points[0] = minx;
  points[1] = maxx;
  points[2] = miny;
  points[3] = maxy;
  dx = maxx - minx;
  dy = maxy - miny;
}

void SquareTile::corners(double& minx, double& maxx, double& miny, double& maxy)
{
  minx = points[0];
  maxx = points[1];
  miny = points[2];
  maxy = points[3];
}

double SquareTile::X(double u, double v) {
  u = max<double>(0.0, u);
  u = min<double>(1.0, u);
  return points[0] + u*dx;
}

double SquareTile::Y(double u, double v) {
  v = max<double>(0.0, v);
  v = min<double>(1.0, v);
  return points[2] + v*dy;
}

double SquareTile::measure(double u, double v) 
{
  return dx*dy;
}

bool SquareTile::InTile(double x, double y)
{
  if( x>=points[0] && x<=points[1] && 
      y>=points[2] && y<=points[3] )
    return 1;
  else return 0;
}

bool SquareTile::overlapsWith(Tile * tile)
{
  double xmin1, xmax1, ymin1, ymax1;
  double xmin2, xmax2, ymin2, ymax2;

  corners(xmin1, xmax1, ymin1, ymax1);
  tile->corners(xmin2, xmax2, ymin2, ymax2);
  
  // If the other tile is entirely on one side of this tile, or
  // touching a boundary then is not overlapping.
  //
  if ((xmin2 >= xmax1) || (xmax2 <= xmin1) || 
      (ymin2 >= ymax1) || (ymax2 <= ymin1))
  { return false; }
  else
  { return true; }
}

void SquareTile::printTile(ostream & outputstream)
{
  outputstream << "xmin " << points[0] << " xmax " << points[1]
               << " ymin " << points[2] << " ymax " << points[3] << endl;
}

void SquareTile::printforplot(ostream & outputstream)
{
  outputstream << points[0] << " " << points[2] << endl 
       << points[1] << " " << points[2] << endl
       << points[1] << " " << points[3] << endl
       << points[0] << " " << points[3] << endl
       << points[0] << " " << points[2] << endl << endl;
}

string SquareTile::toString()
{
  ostringstream buffer;
  
  printTile(buffer);
  
  return buffer.str();
}
