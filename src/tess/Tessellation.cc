#include <sstream>
#include <gvariable.h>
#include <BIEmpi.h>
#include <Tessellation.h>
#include <BIEdebug.h>
#include <Node.h>
#include <Tile.h>
#include <RecordInputStream.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Tessellation)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::twodcoords)

using namespace BIE;

Tessellation::~Tessellation()
{
  for (map<int, Tile*>::iterator it=_tiles.begin(); it!=_tiles.end(); it++)
    delete it->second;
}

void Tessellation::PrintPreOrder
(vector<Node*> rootnodes, ostream & outputstream)
{
  for (int rootindex = 0; rootindex < (int) rootnodes.size(); rootindex++)
  { PrintPreOrder(rootnodes[rootindex], outputstream); }
}

void Tessellation::PrintPreOrder(ostream & outputstream)
{
  PrintPreOrder(GetRootNodes(), outputstream);
}

void Tessellation::PrintPreOrder(Node *node, ostream & outputstream)
{
  if (node == NULL) 
  { 
    return; 
  }
  else
  {
    outputstream << "id " << node->ID() << " ";
    node->printNode(outputstream);
  
    for (node->Reset(); !node->IsDone(); node->Next())
    { PrintPreOrder(node->CurrentItem(), outputstream); }
  }
}

Tile* Tessellation::GetTile(int tileid)
{
  Tile * result;

  if (!IsValidTileID(tileid))  
    { result = NULL; }
  else   
    { result = _tiles[tileid]; }

  return result;
}

bool Tessellation::IsValidTileID(int tileid)
{
  if ((tileid >= 0) && (tileid < Ntiles))
    return true;
  else
    return false;
}

void Tessellation::FindAll(double x, double y, vector<int> &found)
{
  found.clear();
  
  vector<Node*> rootnodes = GetRootNodes();
  
  for (int rootindex = 0; rootindex < (int) rootnodes.size(); rootindex++)
  {
    Tile * tile = rootnodes[rootindex]->GetTile();
    
    if (tile->InTile(x, y))
    { findall(x,y,found,rootnodes[rootindex]); }
  }
}

void Tessellation::findall
(double x, double y, vector<int> &found, Node *node)
{
  found.push_back(node->ID());
  
  for (node->Reset(); !node->IsDone(); node->Next())
  {
    Node * child = node->CurrentItem();
    
    if ((child != NULL) && (child->GetTile()->InTile(x,y)))
    { 
      findall(x,y,found,child);
      break;
    }
  }
}

int Tessellation::NumberTiles() { return Ntiles; }

void Tessellation::compute_sampling
(RecordInputStream *ris, vector<twodcoords>* sampledata, 
 double pts, double minx, double maxx, double miny, double maxy)
{
  while (ris->nextRecord())
  {
    double x = ris->getRealValue("x");
    double y = ris->getRealValue("y");
    
    // Look to see if the point is within the tessellation's
    // boundaries
    if ((x <= maxx) && (x >= minx) && (y <= maxy) && (y >= miny))
    {
      // Roll a continuously valued die to see whether we use the data
      // point
      double chance = ((double)random())/(double)RAND_MAX;
          
      if (chance <= pts)
      {
        twodcoords datapoint;
	datapoint.x = x;
	datapoint.y = y;
	
        (*sampledata).push_back(datapoint); 
      }
    }
  }
}
