#include <iostream>
#include <fstream>
using namespace std;

#include <BIEconfig.h>
#include <BinaryTessellation.h>
#include <Tile.h>
#include <BIEException.h>
#include <Node.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BinaryTessellation)

using namespace BIE;

BinaryTessellation::BinaryTessellation
(Tile * factory, double min_x, double max_x, 
 double min_y, double max_y, int depth)
{
  // Check the input parameters are sensible.
  if (min_x >= max_x)
  { throw BIEException("BinaryTessellatation Exception", 
          "Min x value greater or equal to max x value.", __FILE__, __LINE__); 
  }
  
  if (min_y >= max_y)
  { throw BIEException("BinaryTessellatation Exception", 
         "Min y value greater or equal to max y value.", __FILE__, __LINE__); 
  }
  
  if (depth < 0)
  { throw BIEException("BinaryTessellatation Exception", 
          "Depth cannot be a negative value.", __FILE__, __LINE__); 
  }

  // Take note of the defining parameters.
  Xmin        = min_x;
  Xmax        = max_x;
  Ymin        = min_y;
  Ymax        = max_y;
  maxdepth    = depth;
  tilefactory = factory;

  Ntiles = 0;

  // Create the tessellation
  tessellate(&treeroot, 0, 1, 0, Xmin, Ymin, Xmax, Ymax);
}

void BinaryTessellation::tessellate
(Node **node, int depth, int dimension, int tileid,
 double minx, double miny, double maxx, double maxy)
{
  double split;
  Node *tmpleft, *tmpright;

  // get a new tile from the factory for this node, and add to vector of tiles.
  Tile * newtile = tilefactory->New();
  newtile->setup(minx, maxx, miny, maxy);
  _tiles[tileid] = newtile;

  // Create a binary node 
  BinaryNode * binarynode = new BinaryNode(newtile, tileid, depth);
  *node = binarynode;

  Ntiles++;

  if(depth<maxdepth) {
    if(dimension==0) {
      // split on x dimension
      split = (maxx+minx)/2;
      tessellate(&tmpleft, depth+1, 1-dimension, 2*tileid+1, minx, 
		 miny, split, maxy);
      tessellate(&tmpright, depth+1, 1-dimension, 2*tileid+2, split, 
		 miny, maxx, maxy);
    }
    else {
      // split on y dimension
      split = (maxy+miny)/2;
      tessellate(&tmpleft, depth+1, 1-dimension, 2*tileid+1, minx, 
		 miny, maxx, split);
      tessellate(&tmpright, depth+1, 1-dimension, 2*tileid+2, minx, 
		 split, maxx, maxy);
    }

    binarynode->setLeft(tmpleft);
    binarynode->setRight(tmpright);
  }
}

vector<int> BinaryTessellation::GetRootTiles()
{
  vector<int> result (1, treeroot->ID());
  return result;
}

vector<Node*> BinaryTessellation::GetRootNodes()
{
  vector<Node*> result (1, treeroot);
  return result;
}
