#include <Node.h>
#include <Tile.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Node)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::BinaryNode)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::QuadNode)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::MonoNode)

using namespace BIE;

Node::Node()
{ 
  tile         = 0;
  tileid       = 0;
  depth        = 0;
  childindex   = 0;
}

Node::Node(Tile* tile, int tileid, int depth, int numchildren)
{  
  this->tile   = tile;
  this->tileid = tileid;
  this->depth  = depth;
  
  tile->node   = this;
  
  children     = vector<Node*>(numchildren, static_cast<Node*>(NULL));

  Reset();
}

Node * Node::copytree(int tileidoffset)
{
  // Copy the node described by this object
  Node * newcopy = new Node(tile, tileid+tileidoffset, depth, children.size()); 

  // Recurse to children if they exist.
  for (int childindex = 0; childindex < (int) children.size(); childindex++)
  { 
    Node * childnode = children[childindex];
    
    if (childnode != NULL)
    { newcopy->children[childindex] = childnode->copytree(tileidoffset);}
  }

  return newcopy;
}

Node* Node::First() 
{
  return children[0];
}

Node* Node::Last() 
{
  return children[children.size() - 1];
}

Node* Node::CurrentItem() 
{ 
  Node * result = NULL;
  
  if (childindex < 0 || childindex >= (int) children.size())
  {
    result = NULL;
  }
  else
  {
    result = children[childindex];
  }
  
  return result; 
}

Node* Node::Next() 
{ 
  if (childindex < (int) children.size())
  { childindex++; }
  
  return CurrentItem();
}

void Node::printNode(ostream & outputstream) 
{ 
  tile->printTile(outputstream); 
}

bool Node::IsDone() 
{
  if (childindex >= (int) children.size())
    return true;
  else
    return false;
}

void Node::Reset() 
{ 
  childindex = 0;
}
