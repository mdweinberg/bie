#include <FrontierExpansionHeuristic.h>
#include <KSDistance.h>
#include <gfunction.h>
#include <BIEException.h>
#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::FrontierExpansionHeuristic)

using namespace BIE;

/*******************************************************************************
* Always Increase Resolution
*******************************************************************************/
bool AlwaysIncreaseResolution::expandTile
(vector<int> children, int parent) 
{ return true; }

/*******************************************************************************
* KS Distance based decision.
*******************************************************************************/
bool KSDistanceHeuristic::expandTile
(vector<int> children, int parent) 
{  
  ostream cout(checkTable("simulation_output"));

  KSDistance ksd;
  
  // There must be at least two children when comparing.
  if (children.size() < 2) 
  { 
    throw BIEException("KSDistanceHeuristic Exception", 
    "There must be at least two children when applying this heuristic", __FILE__, __LINE__);
  }
  
  // Compare each child distribution to each other.
  for (int childone=0; childone < (int)children.size()-1; childone++)
  {
    for (int childtwo=childone+1; childtwo < (int)children.size(); childtwo++)
    {
      cout << "Comparing children.  Indices: " << childone << ", " << childtwo;
      cout << ".  Tileids: " << children[childone] << ", ";
      cout << children[childtwo] << endl;
      
      SampleDistribution *distone, *disttwo;
      distone = fulldist->getDistribution(children[childone]);
      disttwo = fulldist->getDistribution(children[childtwo]);
    
      double significance = ksd.kssignificance(distone, disttwo);
      
      cout << "Significance: " << significance << endl;
      
      if (significance <= threshold) { return true; }
    }
  }
  
  return false;
}

/*******************************************************************************
* DataPointCountHeuristic: Descends when the number of points in a tile
* is greater than a theshold.
*******************************************************************************/
bool DataPointCountHeuristic::expandTile 
(vector<int> children, int parent) 
{
  SampleDistribution * tiledist = fulldist->getDistribution(parent);

  if (tiledist->getDataSetSize() >= threshold)
  { return true; }
  else
  { return false; }
}
