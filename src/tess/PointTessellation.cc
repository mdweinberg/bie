#include <iostream>
#include <cfloat>
#include <cmath>

using namespace std;

#include <BIEconfig.h>
#include <PointTessellation.h>
#include <RecordInputStream.h>
#include <Tile.h>
#include <Node.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PointTessellation)

double PointTessellation::MinX = -FLT_MAX/4;
double PointTessellation::MaxX =  FLT_MAX/4;
double PointTessellation::MinY = -FLT_MAX/4;
double PointTessellation::MaxY =  FLT_MAX/4;

// Set nonzero for debugging tessellation construction
static const int deep_debug = 0;

static double logbase2(double x)
{
  return log(x)/M_LN2;   // ln(x)/ln(2) == logbase2(x)
}

using namespace BIE;

// Construct a PointTessellation from an input stream.
//
PointTessellation::PointTessellation
(Tile* t, RecordInputStream * ris)
{
  initialize(t, ris, MinX, MaxX, MinY, MaxY);
}

// Construct a PointTessellation from an input stream.
// Input points not in the bounding box defined by
// minx, maxx, miny, and maxy are discarded.
//
PointTessellation::PointTessellation
(Tile* t, RecordInputStream *ris,
 double minx, double maxx, double miny, double maxy)
{
  initialize(t, ris, minx, maxx, miny, maxy);
}

// Internal function to construct the tessellation.  Called from the
// C++ constructors.
//
void PointTessellation::initialize 
(Tile* factory, RecordInputStream *ris,
 double minx, double maxx, double miny, double maxy)
{
  // Check that the parameters are sensible.
  //
  if ((maxx <= minx) || (maxy <= miny))
  { 
    throw BIEException("PointTessellation Exception", 
		       "Bad tessellation range", __FILE__,__LINE__); 
  } 
  

  // Copy the parameters
  //
  this->Xmax          = maxx;
  this->Xmin          = minx;
  this->Ymax          = maxy;
  this->Ymin          = miny;
  this->tilefactory   = factory;

  // Correct to account for sample size.

  Ntiles = 0;

  // Sample the data stream.  Use 1.0 as threshold for 0..1 random
  // number so all points are sampled.  
  //
  // TODO. Get rid of this, don't need random numbers.
  //
  compute_sampling(ris, &sampledata, 1.0, minx, maxx, miny, maxy);
  
  // TODO
  // Randomize the sample data here.
  // look at STL shuffle
 
  // Tessellate according to this sample.
  tessellate(&treeroot, minx, miny, maxx, maxy);

  // delete sampling data - we don't need it anymore.
  sampledata.clear();
}

// Iteratively construct a hierarchical tessellation with one point
// per tile.  Lowest level of the tree has a copy of every point. The
// next level up has a copy of every other point. Next level up after
// that has every 4th, and so on.
//
void PointTessellation::tessellate
(Node **root, double minx, double miny, double maxx, double maxy)
{
  Tile *t;

  int numPoints = sampledata.size();

  // Compute depth of tree.  Level 0 is root; Level "maxLevel" are the
  // leaves.
  //
  int maxLevel = static_cast<int>(logbase2(numPoints)+1);  
  //  TODO add round off .000001?


  // Allocate a temporary array of all nodes in the tree.  This is
  // used only while building the tree.  The number of nodes is a
  // little more than twice the number of leaves. Implementation note:
  // this could be made more space efficient by calculating a better
  // upper bound on the number of nodes.
  //
  vector<BinaryNode*> nodes( (int)(pow(2.0, maxLevel+1)));  


  int tileId = 0;

  // Populate lowest layer of tree. For each point, create a tile and
  // a node (which unnecessarily have the same IDs). Save the node in
  // a hashmap hashed on the x,y coordiates.
  //
  for (int i=0; i<numPoints; i++){

    // Create new tile object, calc tileID, initialize node with it
    //
    t = tilefactory->New();
    double x = sampledata[i].x;
    double y = sampledata[i].y;
    t->setup(x, x, y, y); /* minx, maxx, miny, maxy*/
    _tiles[tileId] = t;

    BinaryNode * binarynode = new BinaryNode(_tiles[tileId], tileId, maxLevel);
    nodes[tileId] = binarynode;

    // Put node in hashmap for value-based access later (for example,
    // findall)
    //
    twodcoords coords(x,y);
    nodemap[ coords ] = binarynode;

    // For debugging:
    if (deep_debug)
      cout << "Tessellation (" << setw(10) << x << "," << setw(10) << y 
	   << ") -> ID " << setw(8) << nodemap[coords]->ID() 
	   << ", Level=" << setw(2) << maxLevel << endl;

    tileId++;
    Ntiles++;
  }

  // Fill in rest of tree from bottom up.  At each level, make a copy
  // of every other point in a new node for the next level up.  Set
  // the children of the new node to point back to current level.
  // This uses the node array to give indexed access to the current
  // level.
  //
  int startIdAtCurrentLevel = 0;
  int endIdAtCurrentLevel = numPoints-1;

  for (int level = maxLevel-1; level>=0; level--){

				// Only "left" children
    for (int i=startIdAtCurrentLevel; i<=endIdAtCurrentLevel; i += 2){
   
      // get point location
      //
      double x, y, dummy1, dummy2;
      nodes[i]->GetTile()->corners(x, dummy1, y, dummy2);          

      // Create new tile object, calc tileid, initialize node with it
      //
      t = tilefactory->New();
      t->setup(x, x, y, y); /* minx, maxx, miny, maxy*/
      _tiles[tileId] = t;
      
      // Create a new node
      //
      BinaryNode * binarynode = new BinaryNode(_tiles[tileId], tileId, level);  
      nodes[tileId] = binarynode;

      // Put node in hashmap for value-based access later (for
      // example, findall)
      //
      twodcoords coords(x,y);
      nodemap[ coords ] = binarynode;

      // For debugging
      if (deep_debug)
	cout << "Tessellation (" << setw(10) << x << "," << setw(10) << y 
	     << ") -> ID " << setw(8) << nodemap[coords]->ID() 
	     << ", Level=" << setw(2) << level << endl;

      // Add in children of new node
      //
      binarynode->setLeft(nodes[i]);
      if (i+1 <= endIdAtCurrentLevel){  // Add the "right" child of new parent
	binarynode->setRight(nodes[i+1]);
      }

      tileId++;
      Ntiles++;
    }

    startIdAtCurrentLevel = endIdAtCurrentLevel + 1;
    endIdAtCurrentLevel   = tileId - 1;  // max allocated tile
  }

  // The root is last node created
  //
  *root = nodes[tileId-1]; 
}


vector<int> PointTessellation::GetRootTiles()
{
  vector<int> result (1, treeroot->ID());
  return result;
}

vector<Node*> PointTessellation::GetRootNodes()
{
  vector<Node*> result (1, treeroot);
  return result;
}

//  Overrides FindAll in Tessellation.
//
//  The version in Tessellation assumes hierarchical tiles, and only
//  walks down into children if point is in parent.  This version
//  looks up the topmost node in the tree that is the given point
//  using a hashmap.  It then walks down the tree from that node
//  returning the copy of the point that exists at each lower level.

void PointTessellation::FindAll(double x, double y, vector<int> &found)
{
  found.clear();
  
  vector<Node*> rootnodes = GetRootNodes();
  
  twodcoords coords(x,y);
				// Highest node in tree that contains point
  Node* node = nodemap[coords];
				// If point is in tree, follow it down
  if (node) {  
    findall(x,y, found, node);
  }
}


// Recursive helper function. Visits children of a node that have the
// same value; all same valued nodes are put in the found array
//
void PointTessellation::findall
(double x, double y, vector<int> &found, Node *node)
{

  if ( node->GetTile()->InTile(x,y) ) {

    found.push_back(node->ID());

  } else {

    return;			// nothing of interest here or below
    
  }

  for (node->Reset(); !node->IsDone(); node->Next()) {
    
    Node * child = node->CurrentItem();
    
    if ((child != NULL)){

        findall(x,y,found,child);
    }
  }
}
