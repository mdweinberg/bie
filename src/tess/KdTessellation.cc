#include <cstdlib>
#include <iostream>
#include <cmath>
#include <climits>
using namespace std;

#include <BIEconfig.h>
#include <KdTessellation.h>
#include <RecordInputStream.h>
#include <Tile.h>
#include <Node.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::KdTessellation)

using namespace BIE;

// This doesn't save sampledata

KdTessellation::KdTessellation
(Tile* t, RecordInputStream * ris, int ppb, double pts)
{
  initialize(t, ris, ppb, pts, INT_MIN, INT_MAX, INT_MIN, INT_MAX);
}

KdTessellation::KdTessellation
(Tile* t, RecordInputStream *ris, int ppb, double pts,
 double minx, double maxx, double miny, double maxy)
{
  initialize(t, ris, ppb, pts, minx, maxx, miny, maxy);
}

void KdTessellation::initialize 
(Tile* factory, RecordInputStream *ris, int ppb, double pts,
 double minx, double maxx, double miny, double maxy)
{
  // Check that the parameters are sensible.
  if ((maxx <= minx) || (maxy <= miny))
  { throw BIEException("KdTessellation Exception", "Bad tessellation range", __FILE__,__LINE__); } 
  
  if (ppb < 0)
  { throw BIEException("KdTessellation Exception", "Points per bin is negative",__FILE__,__LINE__); }
  
  if ((pts <= 0) || (pts > 1.0))
  { throw BIEException("KdTessellation Exception", "Bad value for sample fraction",__FILE__,__LINE__); }

  // Take a note of the defining parameters.
  this->Xmax          = maxx;
  this->Xmin          = minx;
  this->Ymax          = maxy;
  this->Ymin          = miny;
  this->tilefactory   = factory;
  // Correct to account for sample size.
  this->maxptspertile = (int) ceil((double)ppb * pts);
  this->pcttosample   = pts;
  
  Ntiles = 0;

  // Sample the data stream.
  compute_sampling(ris, &sampledata, pts, minx, maxx, miny, maxy);
  
  // Tessellate according to this sample.
  tessellate(&treeroot, 0, sampledata.size()-1, 0, 1, 0, minx, miny, maxx, maxy);

  // remove sampling - we don't need it anymore.
  sampledata.clear();
}

void KdTessellation::tessellate
(Node **node, int low, int high, int depth, int dimension, int tileid, 
 double minx, double miny, double maxx, double maxy)
{
  int mid = (low+high)/2;
  double split;
  Node *tmpleft, *tmpright;
  Tile *t;

  /* Create new tile object, calc tileid, initialize node with it */
  t = tilefactory->New();
  t->setup(minx, maxx, miny, maxy);
  _tiles[tileid] = t;
  BinaryNode * binarynode = new BinaryNode(_tiles[tileid], tileid, depth);  
  *node = binarynode;
  Ntiles++;

  // If the number of points in the tile is too high, then tessellate further.
  if((high-low) > maxptspertile) {
    
    if(dimension==0) {
      sort(sampledata.begin()+low, sampledata.begin()+high+1, compX);
      split = ((sampledata[mid]).x + (sampledata[mid+1]).x)/2;
      tessellate(&tmpleft, low, mid, depth+1, 1-dimension, 2*tileid+1,
		 minx, miny, split, maxy);
      tessellate(&tmpright, mid+1, high, depth+1, 1-dimension, 2*tileid+2,
		 split, miny, maxx, maxy);
    }
    else {
      sort(sampledata.begin()+low, sampledata.begin()+high+1, compY);
      split = ((sampledata[mid]).y + (sampledata[mid+1]).y)/2;
      tessellate(&tmpleft, low, mid, depth+1, 1-dimension, 2*tileid+1,
		 minx, miny, maxx, split);
      tessellate(&tmpright, mid+1, high, depth+1, 1-dimension, 2*tileid+2,
		 minx, split, maxx, maxy);
    }

    binarynode->setLeft(tmpleft);
    binarynode->setRight(tmpright);
  }
}

vector<int> KdTessellation::GetRootTiles()
{
  vector<int> result (1, treeroot->ID());
  return result;
}

vector<Node*> KdTessellation::GetRootNodes()
{
  vector<Node*> result (1, treeroot);
  return result;
}
