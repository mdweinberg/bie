// This may look like C code, but it is really -*- C++ -*-

#include <cmath>
#include <CosBSquareTile.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::CosBSquareTile)

using namespace BIE;

CosBSquareTile::CosBSquareTile () : SquareTile() {}

CosBSquareTile::CosBSquareTile (double x1, double y1, double x2, double y2)
  : SquareTile(x1, y1, x2, y2) {}

CosBSquareTile* CosBSquareTile::New() 
{
  CosBSquareTile *p = new CosBSquareTile();
  return p;
}

double CosBSquareTile::measure(double u, double v) 
{
  double B = points[2] + v*dy;
  return cos(B) * dx * dy;
}
