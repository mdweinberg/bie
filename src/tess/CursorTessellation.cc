#include <iostream>
#include <cmath>
#include <climits>

using namespace std;

#include <BIEconfig.h>
#include <CursorTessellation.h>
#include <RecordInputStream.h>
#include <Tile.h>
#include <PointTile.h>
#include <Node.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::CursorTessellation)

using namespace BIE;

// Construct a CursorTessellation from an input stream.
//
CursorTessellation::CursorTessellation(RecordInputStream * ris, int ssize)
{
  Nsize = ssize;
  tilefactory = new PointTile();
  initialize(ris, INT_MIN, INT_MAX, INT_MIN, INT_MAX);
}

// Construct a CursorTessellation from an input stream.
// Input points not in the bounding box defined by
// minx, maxx, miny, and maxy are discarded.
//
CursorTessellation::CursorTessellation(RecordInputStream *ris, int ssize,
				       double minx, double maxx, 
				       double miny, double maxy)
{
  Nsize = ssize;
  tilefactory = new PointTile();
  initialize(ris, minx, maxx, miny, maxy);
}

CursorTessellation::~CursorTessellation()
{
  // Delete all the nodes
  vector<Node*>::iterator ibeg, iend, iv;
  for (MType::iterator it=nodemap.begin(); it!=nodemap.end(); it++) {
    ibeg = it->second.begin();
    iend = it->second.end();
    for (iv=ibeg; iv!=iend; iv++) delete *iv;
  }
}


// internal function to construct the tessellation.
// Called from the C++ constructors.
//
void CursorTessellation::initialize 
(RecordInputStream *ris, double minx, double maxx, double miny, double maxy)
{
  // Check that the parameters are sensible.
  //
  if ((maxx <= minx) || (maxy <= miny))
    { throw BIEException("CursorTessellation Exception", 
			 "Bad tessellation range", __FILE__,__LINE__); } 
  
  
  // Define the parameters.
  //
  Xmin = minx;
  Xmax = maxx;
  Ymin = miny;
  Ymax = maxy;
  
  // Sample the data stream.  Use 1.0 as threshold for 0..1 random number so all
  // points are sampled.
  //
  // Todo. Get rid of this, don't need random numbers.
  //
  compute_sampling(ris, &sampledata, 1.0, minx, maxx, miny, maxy);
  
  // todo
  // Randomize the sample data here.
  // look at STL shuffle
  
  // Tessellate according to this sample.
  //
  tessellate(minx, miny, maxx, maxy);
  
  // delete sampling data - we don't need it anymore.
  //
  sampledata.clear();
}

// Root has initial sample.
// Each additional "level" as an update sample size.
//
void CursorTessellation::tessellate
(double minx, double miny, double maxx, double maxy)
{
  Tile *t;
  
  int numPoints = sampledata.size();
  
  vector<MonoNode*> nodes;
  int tileId = 0, depth=0;
  Ntiles = 0;

  // Make root nodes
  //
  numRoots = min<int>(Nsize, numPoints);

  // Roots of singly linked list of nodes
  //
  for (int i=0; i<numRoots; i++) {
    
    // Create new tile object, calc tileid, initialize node with it
    //
    t = tilefactory->New();
    double x = sampledata[i].x;
    double y = sampledata[i].y;
    t->setup(x,x,y,y);		// minx, maxx, miny, maxy
    _tiles[tileId] = t;
    
    // Create a new node
    //
    MonoNode* node = new MonoNode(_tiles[tileId], tileId, depth);
    root.push_back(node);
    
    // put node in hashmap for value-based access later (for example, findall)
    //
    twodcoords coords(x,y);
    if (nodemap.find(coords) == nodemap.end())
      nodemap[coords] = vector<Node*>(1, node);
    else
      nodemap[coords].push_back(node);

    // cout << "Tesselation (" << x << "," << y << ") -> ID " 
    // << nodemap[coords]->ID() << "\n";
    
    tileId++;
    Ntiles++;
  }
  
  // Temporary node list
  //
  vector<MonoNode*> leaves = root;

  // Add the rest of the data
  //
  for (int i=numRoots; i<numPoints; i++) {
    
    if (i % numRoots == 0) depth++;

    // Create new tile object, calc tileid, initialize node with it
    //
    t = tilefactory->New();
    double x = sampledata[i].x;
    double y = sampledata[i].y;
    t->setup(x,x,y,y); // minx, maxx, miny, maxy
    _tiles[tileId] = t;
    
    // Create a new node
    //
    MonoNode * node = new MonoNode(_tiles[tileId], tileId, depth);
    
    // put node in hashmap for value-based access later (for example, findall)
    //
    twodcoords coords(x,y);
    if (nodemap.find(coords) == nodemap.end())
      nodemap[coords] = vector<Node*>(1, node);
    else
      nodemap[coords].push_back(node);

    // cout << "Tesselation (" << x << "," << y << ") -> ID " 
    // << nodemap[coords]->ID() << "\n";
    
				// Make the link
				//
    leaves[tileId % numRoots]->setChild(node);

				// Replace the old frontier node with the
				// current one
				// 
    leaves[tileId % numRoots] = node;
    
    tileId++;
    Ntiles++;
  }
  
  Debug();
}

//
// For debugging the "tree"; looks more like a field of bamboo, really
//
void CursorTessellation::Debug()
{
  if (0) {
    vector<Node*> nlist;

    cout << left << setw(10) << "Root #" << setw(10) << "Count"
	 << setw(20) << right << "Min ptr" << setw(20) << "Max ptr" << endl
	 << left << setw(10) << "------" << setw(10) << "-----"
	 << setw(20) << right << "-------" << setw(20) << "-------" << endl;
    for (int i=0; i<numRoots; i++) {
      Node *p=root[i], *pmin = root[i], *pmax = root[i];
      unsigned cnt = 1;
      nlist.push_back(p);
      while (p->First() != NULL) {
	cnt++;
	pmin = min<Node*>(pmin, p->First());
	pmax = max<Node*>(pmax, p->First());
	p = p->First();
	nlist.push_back(p);
      }
      cout << left << setw(10) << i << setw(10) << cnt << setw(20) 
	   << right << hex << pmin << setw(20) << pmax << dec << endl;
    }

    cout << endl;
    sort(nlist.begin(), nlist.end());
    vector<Node*>::iterator it=nlist.begin();
    while (it!=nlist.end()) {
      for (unsigned j=0; j<10 && it!=nlist.end(); j++, it++)
	cout << setw(15) << hex << left << *it;
      cout << endl;
    }
    cout << endl;
  }
}

vector<int> CursorTessellation::GetRootTiles()
{
  vector<int> result (numRoots);
  for (int i=0; i<numRoots; i++) result[i] = root[i]->ID();
  return result;
}

vector<Node*> CursorTessellation::GetRootNodes()
{
  vector<Node*> result = vector<Node*>(numRoots);
  for (int i=0; i<numRoots; i++) result[i] = root[i];
  return result;
}

//  Overrides FindAll in Tessellation.
//
//  Version in Tessellation assumes hierarchical tiles, and only walks
//  down into children if point is in parent.  This version uses a
//  hashmap to locate the bucket and then scans the bucket linearly.

void CursorTessellation::FindAll(double x, double y, vector<int> &found)
{
  found.clear();
  
  twodcoords coords(x,y);
  if (nodemap.find(coords) == nodemap.end()) return;

  vector<Node*>::iterator ibeg=nodemap[coords].begin(); 
  vector<Node*>::iterator iend=nodemap[coords].end(); 

  for (vector<Node*>::iterator pr=ibeg; pr!=iend; pr++) {
    Node *p = *pr;
    if (p->GetTile()->InTile(x,y)){
      found.push_back(p->ID());
    }
  }
}
