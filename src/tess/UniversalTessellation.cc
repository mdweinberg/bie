#include <iostream>
#include <cfloat>
#include <cmath>

using namespace std;

#include <BIEconfig.h>
#include <UniversalTessellation.h>
#include <SquareTile.h>
#include <Node.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::UniversalTessellation)

using namespace BIE;

double UniversalTessellation::MinX = -FLT_MAX/4;
double UniversalTessellation::MaxX =  FLT_MAX/4;
double UniversalTessellation::MinY = -FLT_MAX/4;
double UniversalTessellation::MaxY =  FLT_MAX/4;

UniversalTessellation::UniversalTessellation()
{
  initialize(MinX, MaxX, MinY, MaxY);
}

// Construct a UniversalTessellation.
// Input points not in the bounding box defined by
// minx, maxx, miny, and maxy are discarded.
//
UniversalTessellation::UniversalTessellation
(double minx, double maxx, double miny, double maxy)
{
  initialize(minx, maxx, miny, maxy);
}

// internal function to construct the tesse.lation.
// Called from the C++ constructors.
void UniversalTessellation::initialize 
(double minx, double maxx, double miny, double maxy)
{
  // Check that the parameters are sensible.
  if ((maxx <= minx) || (maxy <= miny))
  { throw BIEException("UniversalTessellation Exception", "Bad tessellation range", __FILE__,__LINE__); } 
  
  // Take a note of the defining parameters.
  this->Xmax          = maxx;
  this->Xmin          = minx;
  this->Ymax          = maxy;
  this->Ymin          = miny;
  this->tilefactory   = new SquareTile();

  // Make the one and only tile
  Tile * newtile = tilefactory->New();
  newtile->setup(minx, maxx, miny, maxy);
  _tiles[0] = newtile;

  // Create a node with no children.
  treeroot = new Node(newtile, 0, 0, 0);

  Ntiles = 1;
}

UniversalTessellation::~UniversalTessellation()
{
  delete tilefactory;
  delete treeroot;
}

vector<int> UniversalTessellation::GetRootTiles()
{
  vector<int> result (1, treeroot->ID());
  return result;
}

vector<Node*> UniversalTessellation::GetRootNodes()
{
  vector<Node*> result (1, treeroot);
  return result;
}

void UniversalTessellation::FindAll(double x, double y, vector<int> &found)
{
  found.clear();
  found.push_back(treeroot->ID());
}
