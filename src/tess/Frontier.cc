#include <cstdio>
#include <iostream>

using namespace std;

#include <PointTessellation.h>
#include <Frontier.h>
#include <BIEconfig.h>
#include <Node.h>
#include <Tile.h>
#include <FrontierExpansionHeuristic.h>
#include <algorithm>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Frontier)

using namespace BIE;

Frontier::Frontier(Tessellation * tess) 
{
  frontier_index = 0;
  frontier_tessellation = tess;
  

  // Initialize the bit vector.
  int numberoftiles = frontier_tessellation->NumberTiles();
  frontier_infrontier.resize(numberoftiles, 0);
  
  // Set the initial frontier to be the root nodes.
  vector<int> initialfrontier = frontier_tessellation->GetRootTiles();
  Set(initialfrontier);

  // Use reflection to see if we need accumulation mode
  if (dynamic_cast <PointTessellation*> (frontier_tessellation))
    accumulate_mode = false;
  else
    accumulate_mode = false;
}

Frontier::Frontier(const Frontier* p)
{
  frontier_tiles        = p->frontier_tiles;
  frontier_infrontier   = p->frontier_infrontier;
  frontier_index        = p->frontier_index;
  frontier_tessellation = p->frontier_tessellation;
  accumulate_mode       = p->accumulate_mode;
}

Frontier* Frontier::Copy()
{
  return new Frontier(this);
}

int Frontier::Next()
{
  if (frontier_index<frontier_tiles.size())
    frontier_index++;
  
  return CurrentItem();
}

int Frontier::CurrentItem()
{
  if (IsDone())
  { return frontier_tiles[frontier_index-1]; }
  else 
  { return frontier_tiles[frontier_index]; }
}

Node * Frontier::GetNode(int tileid)
{ return (frontier_tessellation->GetTile(tileid))->GetNode(); }

void Frontier::Set(vector<int> newfrontier) 
{ 
  // Empty the current frontier.
  EraseFrontier();
  
  // Make a bit vector that is true when a certain node is in the 
  // node set wanted for the new frontier.
  vector<bool> nodesetbitvector(frontier_tessellation->NumberTiles(), false);
  for (int index = 0; index < (int) newfrontier.size(); index++)
  { 
    int tileid = newfrontier[index];
    if (tileid >= frontier_tessellation->MinID() &&
        tileid <= frontier_tessellation->MaxID())
    { nodesetbitvector[tileid] = true; }
  }
  
  vector<int> roottiles = frontier_tessellation->GetRootTiles();

  // Descend recursively though the tree setting the frontier.
  for (int rootindex = 0; rootindex < (int) roottiles.size(); rootindex++)
  { RecursivelySelect(GetNode(roottiles[rootindex]), nodesetbitvector); }
}

void Frontier::RetractToTopLevel()
{
  vector<int> topleveltiles = frontier_tessellation->GetRootTiles();
  Set(topleveltiles);
}

void Frontier::UpDownLevels(int n)
{
  for (int level = 0; level < abs(n); level++)
  {
    if (n > 0)
    { ExpandOneLevel(); }
    else
    { ContractOneLevel(); }
  }
}

void Frontier::ExpandOneLevel()
{     
  vector<int> oldfrontier;

  if (accumulate_mode)
    oldfrontier = TrueFrontier(); // Compute the old frontier
  else {
    oldfrontier = frontier_tiles; // Save the old frontier
    EraseFrontier();
  }

  for (int frontierindex = 0; frontierindex < (int)oldfrontier.size(); 
       frontierindex++) 
    {
      Node * parent = GetNode(oldfrontier[frontierindex]);
    
      bool addedchild = false;
	
      // Add children into the frontier if they exist.
      for(parent->Reset(); ! parent->IsDone() ; parent->Next()) {
	Node * child = parent->CurrentItem();
	    
	if (child != NULL) {
	  addedchild = true;
	  AddToFrontier(child->ID());
	}
      }
	
      // If no children were added, re-add parent in normal mode
      if (!accumulate_mode && !addedchild) 
	{ AddToFrontier(parent->ID()); }
    }
}

void printvector(vector<int> & vec)
{
  if (vec.size())
  {
    cout << vec[0];
  }

  for (int i = 1; i < (int) vec.size(); i++)
  {
    cout << "," << vec[i];
  }
  
  cout << endl;
}


bool Frontier::WalkAndAdd(vector<bool>& frontiercopy, int& parentid)
{
  Node * parent = GetNode(parentid);
  bool add = false;		// The parent will be added if any
				// of the children are in the frontier
  
				// So look for a child in the frontier
  for (parent->Reset(); ! (parent->IsDone()); parent->Next()) {
    Node * child = parent->CurrentItem();  
      
    if (child != 0) {
      int currentid = child->ID();
      // This child is in the frontier
      if (frontiercopy[currentid]) {
	// Recurse: look for this child's children . . .
	if (WalkAndAdd(frontiercopy, currentid)) AddToFrontier(currentid); 
	add = true;
      }
    }
  }

  return add;
}
  

vector<int> Frontier::TrueFrontier()
{
  vector<int> real_frontier;
  vector<int> nodelist = frontier_tessellation->GetRootTiles();
  reverse(nodelist.begin(), nodelist.end());

				// Beginning searching for the true
				// frontier by walking down from the
				// root tiles
  while (nodelist.size() > 0) {
    int parenttileid = nodelist.back();
    nodelist.pop_back();
    TrueFrontier(real_frontier, parenttileid);
  }

  return real_frontier;
}

void Frontier::TrueFrontier(vector<int>& real_frontier, int& parentid)
{
  Node * parent = GetNode(parentid); // Start with the parent
  bool found_one = false;

				// Iterate trhough the children
  for (parent->Reset(); ! (parent->IsDone()); parent->Next()) {
    Node * child = parent->CurrentItem();  
      
				// If we have a child, see if it a 
    if (child != 0) {		// true frontier candidate
      int currentid = child->ID();
      if (frontier_infrontier[currentid]) {
	TrueFrontier(real_frontier, currentid);
	found_one = true;	// Since a child _is_ a frontier
				// candidate, the parent is not
      }
    } 
  }
				// The parent must be on the true
				// frontier since none of its children
				// are
  if (!found_one) real_frontier.push_back(parentid);
}
  

void Frontier::ContractOneLevel()
{
  // Copy the bit vector saying whether a certain tile is in the frontier.
  vector<bool> frontiercopy = frontier_infrontier;
  
  // Erase current frontier and start afresh.
  EraseFrontier();
  
  // We use a depth first search to set the new frontier, starting with
  // the root nodes.
  vector<int> nodelist = frontier_tessellation->GetRootTiles();

  // Our depth first search pops from the end of the vector, so to 
  // maintain node order the vector is reversed.
  reverse(nodelist.begin(), nodelist.end());

  while (nodelist.size() > 0)
  {
    int parenttileid = nodelist.back();
    nodelist.pop_back();
    
    if (accumulate_mode) {

      // The root note must be in the frontier
      AddToFrontier(parenttileid);
      // So we can ignore the return code here . . .
      WalkAndAdd(frontiercopy, parenttileid);

    } else {

      if (frontiercopy[parenttileid])
	{
	  // This case only occurs when a root node is in the frontier.  Since
	  // we can't contract it further, we add the root node back into the
	  // frontier.
	  AddToFrontier(parenttileid);
	}
      else
	{
	  // If a child node is in the frontier, we add the parent.
	  // Otherwise, we carry on recursing until we either hit a
	  // a subtree where a child is in the frontier, or we exhaust the
	  // search without finding anything in the frontier.
	  Node * parent = GetNode(parenttileid);
	  bool childinfrontier = false;
	  
	  for (parent->Reset(); ! (parent->IsDone()); parent->Next())
	    { 
	      Node * child = parent->CurrentItem();  
	
	      if (child != 0 && frontiercopy[child->ID()])
		{ childinfrontier = true;  break; }
	    }
      
	  if (childinfrontier)
	    { 
	      // Child in the frontier - add parent and stop recursing.
	      AddToFrontier(parenttileid); 
	    }
	  else
	    {
	      int numchildren = 0;
	      // Keep recursing to non null children.
	      for (parent->Reset(); ! (parent->IsDone()); parent->Next())
		{ 
		  Node * child = parent->CurrentItem();  
          
		  if (child != 0)
		    { nodelist.push_back(child->ID());  numchildren++;}
		}
	
	      // Try to maintain order in frontier list - reverse the order
	      // of the children we just added.
	      if (numchildren)
		{ reverse(nodelist.end() - numchildren, nodelist.end()); }
	    }
	}
    }
    
  }
}

void Frontier::EraseFrontier()
{
  // Set all bits that are 1 to 0.
  //
  // This is perhaps a little risky, but is faster none the less.
  // Rather than set all bits to be zero, we just zero the bits in the tileid
  // list.  This can make a bad situation worse where these structures are
  // not completely in sync, but this shouldn't happen.
  for (int frontierindex = 0; frontierindex < (int) frontier_tiles.size(); frontierindex++)
    { frontier_infrontier[frontier_tiles[frontierindex]] = 0; }

  frontier_tiles.erase(frontier_tiles.begin(), frontier_tiles.end());
}

void Frontier::AddToFrontier(int tileid)
{
  frontier_infrontier[tileid] = 1;
  frontier_tiles.push_back(tileid);
}

void Frontier::RecursivelySelect(Node *node, vector<bool> & nodesetbitvector)
{ 
  if(node==NULL) return;
 
  if (nodesetbitvector[node->ID()]) 
  {   
    // select the node
    AddToFrontier(node->ID());
  }  

  for (node->Reset(); ! (node->IsDone()); node->Next())
    { RecursivelySelect(node->CurrentItem(), nodesetbitvector); }

}

Node * Frontier::Find
(double x, double y)
{
  // First, find the root node containing the point.
  Node * rootnode = NULL;
  vector<int> roottiles = frontier_tessellation->GetRootTiles();
  
  for (int rootindex = 0; rootindex < (int) roottiles.size(); rootindex++)
  {
    Tile * tile = frontier_tessellation->GetTile(roottiles[rootindex]);

    if (tile->InTile(x,y))
    { 
      rootnode = tile->GetNode(); 
      break;
    }
  }

  // If the point was inside a root node, then search the corresponding tree.
  if (rootnode != NULL)
  { return FindInTree(rootnode,x,y); }
  else 
  { return NULL; }
}

Node * Frontier::FindInTree 
(Node * node, double x, double y)
{
  if (node == NULL)
  { return NULL; }
  if (frontier_infrontier[node->ID()] && (node->GetTile()->InTile(x,y)))
  { return node; }
  else
  {
    // Search child nodes.
    for (node->Reset(); !node->IsDone(); node->Next())
    {
      Node * childnode = node->CurrentItem();

      if ((childnode != NULL) && (childnode->GetTile()->InTile(x,y)))
      { return FindInTree(childnode,x,y); }
    }
  }
      
  return NULL;
}

bool Frontier::IncreaseResolution
(FrontierExpansionHeuristic * heuristic, int numlevels)
{
  vector<int> frontiercopy;
  vector<int> finalfrontier;   
  bool frontierchanged = false;



  if (accumulate_mode) { 
				// We must first compute the true frontier
    frontiercopy = TrueFrontier();
				// In accumulate mode, the new
				// frontier begins with the current
				// frontier
    finalfrontier = frontier_tiles;
				   
  } else {
    frontiercopy = frontier_tiles;  // copy frontier
  }

  for (int frontierindex = 0;
       frontierindex < (int) frontiercopy.size();
       frontierindex++)
    {
    // Get the tile id.
    int tileid = frontiercopy[frontierindex];
  
    // Change the current frontier to be this single node.
    EraseFrontier();
    AddToFrontier(tileid);
    
    // Expand this one node frontier numlevel times to preview higher resolution
    for (int expandcount = 1; expandcount <= numlevels; expandcount++)
    { ExpandOneLevel(); } 
    
    // See if it is appropriate to expand the node or not
    bool expand = false;

    if ((frontier_tiles.size() == 1) && (frontier_tiles[0] == tileid))
    {
      // Do not expand where the expansion had no effect
      expand = false;
    }
    else
    {
      // Ask the heuristic whether we should expand or not.
      expand = heuristic->expandTile(frontier_tiles, tileid);
    }
    
    if (expand)
    {
      //  the expanded nodes go into the new frontier.
      vector<int> newnodes (frontier_tiles);
      
      for (int nodeindex = 0; nodeindex < (int) newnodes.size(); nodeindex++)
      { finalfrontier.push_back(newnodes[nodeindex]); }
    
      frontierchanged = true;
    }
    else
    {
      // The original node goes back into the frontier. It is already
      // in place for accumulate mode.
      if (!accumulate_mode) finalfrontier.push_back(tileid);
    }
  }
  
  // Set the frontier to be the expanded version.
  Set(finalfrontier);

  return frontierchanged;
}
