#include <sstream>
#include <string>
#include <cstring>
#include <vector>

#include <TessToolSender.h>
#include <LikelihoodComputation.h>
#include <bieTags.h>
#include <BIEMutex.h>
#include <PersistenceControl.h>
#include <SetFilters.h>
#include <UnaryFilters.h>
#include <BinaryFilters.h>
#include <gvariable.h>

#include <Serializable.h>
#include <TestUserState.h>
#include <TestSaveManager.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif

using namespace BIE;

TessToolSender::TessToolSender(string name, bool remote)
{
  _remote = remote;
  _name = (char *) name.c_str();
  _port = 0;
  init();
}

TessToolSender::TessToolSender(bool remote)
{
  _remote = remote;
  _name = const_cast<char *>("TessTool");
  _port = 0;
  init();
}

TessToolSender::TessToolSender()
{
  _remote = false;
  _name = new char [256];
  strcpy(_name, "TessTool");
  _port = 0;
  init();
}

void TessToolSender::init()  
{
  int myRank, size, rv;
  // MPI init already done
  ++MPIMutex;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::Hello world! from " <<  myRank << " of " << size 
       << endl << flush;
#endif

  if (_remote) {  

#ifdef DEBUG_TESSTOOL
    ferr << "TessToolSender::about to look up server name . . ." << flush;
#endif

    sleep(30);

#ifdef DEBUG_TESSTOOL
    ferr << "now!" << endl << flush;
#endif

    _port = new char[MPI_MAX_PORT_NAME];
    rv = MPI_Lookup_name(_name, MPI_INFO_NULL, _port); 

#ifdef DEBUG_TESSTOOL
    ferr << "TessToolSender::completed lookup for " << _name << endl << flush;
#endif

    if (rv) {
      char errmsg[MPI_MAX_ERROR_STRING];
      int msglen;
      MPI_Error_string( rv, errmsg, &msglen );
      cerr << "Error in Publish_name: \""
	   << errmsg << "\"" << endl << flush;
    }

#ifdef DEBUG_TESSTOOL
    ferr << "lookup_name rv=" << rv << endl
	 << "TessToolSender::port name is: " << _port << endl << flush;
#endif

    MPI_Barrier(MPI_COMM_WORLD);

    rv = MPI_Comm_connect(_port, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &_tessComm); 

#ifdef DEBUG_TESSTOOL
    ferr << "TessToolSender::" <<  myRank << " Finished Connect rv=" 
	 << rv << endl << flush;
#endif
  } else { 			// Spawn the listener process and 
				// assign the communicator

      char worker_program[] = "tesstoolwriter";
      char **_argv = new char* [2];
      _argv[0] = new char [128];
      _argv[1] = 0;
      strncpy(_argv[0], nametag.c_str(), 128);
      
#ifdef DEBUG_TESSTOOL
      ferr << "TessToolSender::process " << myRank << ": about to spawn" 
	   << endl << flush;
#endif

				// This command is collective
				// so we'll only get one process
      MPI_Comm_spawn(worker_program, _argv, 1,
		     MPI_INFO_NULL, 0, MPI_COMM_WORLD, &_tessComm,
		     (int *)MPI_ERRCODES_IGNORE);
      
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::process " << myRank << ": spawn complete" 
       << endl << flush;
#endif

      delete [] _argv[0];
      delete [] _argv;
  }
    

  --MPIMutex;

  _persistedTessellation = false;
  _rt = NULL;
  _mpistrm = NULL;
  _filter = NULL;
  _session = NULL;
  _mpiStreamCreated =
    _mpiFilterAttached = _defaultFunctionSelectionFilterAttached = false;
  _filteredStreams = vector<RecordOutputStream*>();
  _filters = vector<RecordStreamFilter*>();
  _isSelectionFilter = vector<bool>();
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::" <<  myRank << " about to initialize _selectionFilters . . .";
#endif
  _selectionFilters = vector< vector<string> >();
#ifdef DEBUG_TESSTOOL
    ferr << "done" << endl << flush;
#endif
  _inputStreams = vector<RecordOutputStream*>();
  _level = -1;
  _step = -1;
  _iter = 0;
}

void TessToolSender::Detach()  
{
  // disconnect communicator
}

TessToolSender::~TessToolSender()
{
  delete [] _port;
}

void TessToolSender::SetLikelihoodComputation(LikelihoodComputation *likely)
{
  _likely = likely;
}

void TessToolSender::persistTessellation()
{
  BaseDataTree *fd = _likely->GetBaseDataTree();

#ifdef DEBUG_TESSTOOL
  ferr  << "DataTree=" << fd << endl;
  ferr.flush();
#endif

  Tessellation *tess = fd->GetTessellation();

#ifdef DEBUG_TESSTOOL
  ferr  << "Tessellation=" << tess << endl;
  ferr.flush();
#endif

  writeTessellationStore(tess);
  _persistedTessellation = true;
  cout  << "Persisted Tessellation" << endl;
}

void TessToolSender::SetSessionId(int level, int step)
{
  _level = level;
  _step = step;
}

MPI_Comm TessToolSender::GetMPIComm()
{
  return _tessComm;
}


void TessToolSender::Synchronize()
{
  // Only root node(0) calls me before calling TessToolDump method

  if (!_persistedTessellation) {
    persistTessellation();
  }

  // check if a message is pending for us
  // ask for numDataSources and their ranks from the likelihood comp(root 0)
  // send this to tesstoolrx(root 0)
  // other TMs write get the outputstream from ttsender and dump header and data
  int numTMs;
  int *tmList;

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::Synchronize(): calling  _likely->getNumAndIdsOfTileMasters " << endl;
#endif

  _likely->getNumAndIdsOfTileMasters(&numTMs, &tmList);

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::Synchronize(): numTMs= " << numTMs << endl;

  for(int i=0; i<numTMs; i++) {
    ferr << " tmList[" << i << "]= " << tmList[i] << endl;
  }
#endif

  MPI_Send((void *)&_level, 1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _tessComm);
  MPI_Send((void *)&_step, 1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _tessComm);
  MPI_Send((void *)&_iter, 1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _tessComm);
  MPI_Send((void *)&numTMs, 1, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _tessComm);
  MPI_Send((void *)tmList, numTMs, MPI_INT, 0, BIE_SYNCHRONIZE_TAG, _tessComm);

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::Synchronize(): return " << endl;
#endif

  _iter++;
 }

void TessToolSender::writeTessellationStore(Tessellation* tess)
{
  ostringstream sout;

  if (tess == NULL) {
#ifdef DEBUG_TESSTOOL
    ferr << "writeTessellationStore:: Tessellation NULL" << endl; 
    ferr.flush(); 
#endif
    abort();
    return; 
  }

#ifdef DEBUG_TESSTOOL
  cerr << "Tessellation has " << tess->NumberTiles() << " tiles" << endl;
  cerr << "Erasing any previous store" << endl;
  sout.str("");
  sout << "ls -altR " << PERSISTENCE_DIR << "/" << TESSELLATION_STORE;
  cerr << system(sout.str().c_str()) << endl;
#endif
  sout.str("");
  sout << "/bin/rm -fr " << PERSISTENCE_DIR << "/" << TESSELLATION_STORE;
#ifdef DEBUG_TESSTOOL
  cerr << sout.str() << endl;
#endif
  if (system(sout.str().c_str())) {
    std::cerr << "Error executing <" << sout.str() << ">" 
	      << std::endl;
  }
#ifdef DEBUG_TESSTOOL
  sout.str("");
  sout << "ls -altR " << PERSISTENCE_DIR << "/" << TESSELLATION_STORE;
  cerr << system(sout.str().c_str()) << endl;
#endif

  try {
    string saved(TESSELLATION_STORE);
    TestUserState savestate (saved, 0, true);
    savestate.addObject(tess);

    TestSaveManager manager(&savestate, BOOST_BINARY, BACKEND_FILE);
    manager.save();
  }
  catch (BoostSerializationException* e) {

    cout << "#########################################################" << endl;
    cout << "##### BoostSerializationException in TessTool SAVE! #####" << endl;
    cout << "Printing persistence stack trace..." << endl;
    e->print(cout);
    cout << "#########################################################" << endl;
  }


#ifdef DEBUG_TESSTOOL
  cerr << system("ls -lR tessStore") << endl;
#endif
}

// 0-indexed
bool TessToolSender::attachFilter(RecordStreamFilter* fil, int index)
{
  RecordOutputStream *istrm;
  bool fin=false;

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::attachFilter:: enter " << endl;
#endif

  int size = _inputStreams.size();

  // is this the first filter?
  if (!(index <= size)) {
    throw new TessToolException (string("Improper index"),__FILE__,__LINE__);
  } else {
    if (size == 0 || index ==0) {
      istrm  = _mpistrm;
    } else {
      istrm  = _filteredStreams[index-1];
    }
    /*
    _inputStreams[index] = istrm;
    _filters[index] = fil;
    _isSelectionFilter[index] = false;
    */
    insertInVector(_inputStreams, index, istrm);
    insertInVector(_filters, index, fil);
    insertInVector(_isSelectionFilter, index, false);

    // This filter should have been created with the correct strm
    //    RecordOutputStream *fostrm  = istrm->filterWith(fil);
    int recs = istrm->numFields();
    RecordOutputStream *fostrm  = istrm->filterWith(recs+1, fil);
    insertInVector(_filteredStreams, index, fostrm);
  }

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::attachFilter:: exit " << endl;
#endif

  return fin;
}

bool TessToolSender::attachSelectionFilter(vector<string> fil, int index)
{
  RecordOutputStream *istrm;
  bool fin=false;

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::attachSelectionFilter:: enter " << endl;
#endif

  int size = _inputStreams.size();

  // is this the first filter?
  if (!(index <= size)) {
    throw new TessToolException (string("Improper index"),__FILE__,__LINE__);
  } else {
    if (size == 0 || index ==0) {
      istrm  = _mpistrm;
    } else {
      istrm  = _filteredStreams[index-1];
    }
    /*
    _inputStreams[index] = istrm;
    _selectionFilters[index] = fil;
    _isSelectionFilter[index] = true;
    */
    insertInVector(_inputStreams, index, istrm);
    insertInVector(_selectionFilters, index, fil);
    insertInVector(_isSelectionFilter, index, true);

    // This filter should have been created with the correct strm
    RecordOutputStream *fostrm  = istrm->selectFields(&fil);
    insertInVector(_filteredStreams, index, fostrm);
  }

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::attachSelectionFilter:: exit " << endl;
#endif

  return fin;
}

bool TessToolSender::attachFilter(RecordStreamFilter* fil)
{
  int index = _inputStreams.size();
  return attachFilter(fil, index);
}

bool TessToolSender::attachSelectionFilter(vector<string> fil)
{
  int index = _inputStreams.size();
  return attachSelectionFilter(fil, index);
}

bool TessToolSender::replaceFilter(RecordStreamFilter* fil, int index)
{
  bool fin=false;
  int size = _inputStreams.size();

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::replaceFilter:: enter " << endl;
#endif

  if (!(index <= size)) {
    throw new TessToolException (string("Improper index"),__FILE__,__LINE__);
  } else {
    throw new TessToolException (string("Unimplemented"),__FILE__,__LINE__);
  }

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::replaceFilter:: exit " << endl;
#endif

  return fin;

}

bool TessToolSender::printFilterChain()
{
  bool rv = true;
  int size = _filters.size();
  int i;
  for(i=0; i<size; i++) {
    if (!_isSelectionFilter[i]) {
      (_filters[i])->toString();
    } else {
      vector<string> sel = _selectionFilters[i];
      vector<string>::iterator ib = sel.begin();
      vector<string>::iterator ie = sel.end();
      while(ib != ie) {
	cout << *ib << " ";
      }
      cout << endl;
    }
  }
  return rv;
}

bool TessToolSender::reinitializeFilterChain()
{
  bool rv = true;
  // disconnect everything, in reverse order
  MPIStreamFilter *old = dynamic_cast<MPIStreamFilter*>(_filters.back());

  int size = _filters.size();
  int i;
  for(i=0; i<size; i++) {
    if (!_isSelectionFilter[i]) {
      (_filters[i])->disconnect();

    } else {
      vector<string> sel = _selectionFilters[i];
      vector<string>::iterator ib = sel.begin();
      vector<string>::iterator ie = sel.end();
      while(ib != ie) {
	delete &*ib ;
      }
    }
    _filteredStreams[i]->close();
  }

  old->flush();
  delete old;
  _mpiFilterAttached = false;
  return rv;
}

bool TessToolSender::attachMPIFilter()
{
  bool fin=false;
  RecordOutputStream *frostrm;
  RecordOutputStream *laststrm;

#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::attachMPIFilter:: enter " << endl;
#endif

  if (!_mpiFilterAttached) {
    if (!_defaultFunctionSelectionFilterAttached) {
      attachDefaultFunctionSelectionFilter();
    }
    // attach a filter as the first entry in the list
    laststrm = _filteredStreams.back();
    _filter = new MPIStreamFilter(laststrm, GetMPIComm(), 0);
    frostrm  = laststrm->filterWith(_filter);
    _mpiFilterAttached = true;
#ifdef DEBUG_TESSTOOL
    ferr << " filter=" << _filter << endl;
#endif
  } else {
    // check if the selected stream has changed from the one  used to create it
    // attach a filter to the last entry in the list
    MPIStreamFilter *old = dynamic_cast<MPIStreamFilter*>(_filters.back());
    _filteredStreams.pop_back();
    _filters.pop_back();
    _isSelectionFilter.pop_back();
    _inputStreams.pop_back();
    old->flush();
    delete old;
    laststrm = _filteredStreams.back();
    _filter = new MPIStreamFilter(laststrm, GetMPIComm(), 0);
    frostrm  = laststrm->filterWith(_filter);
  }
  _inputStreams.push_back(laststrm);
  _filters.push_back(_filter);
  _isSelectionFilter.push_back(false);
#ifdef DEBUG_TESSTOOL
  ferr << "About to terminate _selection filters . . . ";
#endif
  _selectionFilters.push_back(static_cast< vector<string> >(0));
#ifdef DEBUG_TESSTOOL
  ferr << "done!" << endl << flush;
#endif
  _filteredStreams.push_back(frostrm);
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::attachMPIFilter:: exit " << endl;
#endif
  return fin;
}

bool TessToolSender::attachDefaultFunctionSelectionFilter()
{
  int scnt = 0;

  // -------------------------------------------------------------------------
  // data array total
  // -------------------------------------------------------------------------
  RecordStreamFilter* rsumd = new SumFilter(_mpistrm);
  rsumd->connect("Value Array", 1);
  rsumd->renameOutputField("total", "DataTotal");
#ifdef DEBUG_TESSTOOL
  ferr << rsumd->toString() << endl;
#endif
  attachFilter(rsumd, scnt);

  // -------------------------------------------------------------------------
  // model array total
  // -------------------------------------------------------------------------
  RecordStreamFilter* rsump = new SumFilter(_filteredStreams[scnt++]);
  rsump->connect("Prediction Array", 1);
  rsump->renameOutputField("total", "ModelTotal");
#ifdef DEBUG_TESSTOOL
  ferr << rsump->toString() << endl;
#endif
  attachFilter(rsump, scnt);

  // -------------------------------------------------------------------------
  // data-model difference
  // -------------------------------------------------------------------------
  RecordStreamFilter* rsf = new SubtractionFilter(_filteredStreams[scnt++]);
  rsf->connect("Value Array", 1);
  rsf->connect("Prediction Array", 2);
#ifdef DEBUG_TESSTOOL
  ferr << rsf->toString() << endl;
#endif
  attachFilter(rsf, scnt);

  // -------------------------------------------------------------------------
  // data-model difference
  // -------------------------------------------------------------------------
  RecordStreamFilter* rstdif = new SumFilter(_filteredStreams[scnt++]);
  rstdif->connect("difference", 1);
  rstdif->renameOutputField("total", "DiffTotal");
#ifdef DEBUG_TESSTOOL
  ferr << rstdif->toString() << endl;
#endif
  attachFilter(rstdif, scnt);

  // -------------------------------------------------------------------------
  // data-model absolute difference
  // -------------------------------------------------------------------------
  RecordStreamFilter* raf = new AbsFilter(_filteredStreams[scnt++]);
  raf->connect("difference", 1);
#ifdef DEBUG_TESSTOOL
  ferr << raf->toString() << endl;
#endif
  attachFilter(raf, scnt);

  // -------------------------------------------------------------------------
  // mean, variance, max, min added to output
  // -------------------------------------------------------------------------
  RecordStreamFilter * dflt = new StatisticsFilter (_filteredStreams[scnt++]);
  dflt->connect("abs", 1);
#ifdef DEBUG_TESSTOOL
  ferr << dflt->toString() << endl;
#endif
  attachFilter(dflt, scnt);

  // -------------------------------------------------------------------------
  // data-model *relative* difference
  // -------------------------------------------------------------------------
  RecordStreamFilter* rdf = new DivisionFilter(_filteredStreams[scnt++]);
  rdf->connect("abs", "numerator");
  rdf->connect("Prediction Array", "divisor");
#ifdef DEBUG_TESSTOOL
  ferr << rdf->toString() << endl;
#endif
  attachFilter(rdf, scnt);

  // -------------------------------------------------------------------------
  // Make the quantiles
  // -------------------------------------------------------------------------
  RecordStreamFilter* rqf = new QuantileFilter(_filteredStreams[scnt++]);
  rqf->connect("abs", 1);
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::connecting field DifferenceModelData to quantiles done" << endl;
  ferr << rqf->toString() << endl;
#endif
  attachFilter(rqf, scnt);

  // -------------------------------------------------------------------------
  // Get the maximum difference
  // -------------------------------------------------------------------------
  RecordStreamFilter* ssf = new MaxFilter(_filteredStreams[scnt++]);
  ssf->renameOutputField("max", "MaxDifferenceModelData");
  ssf->renameOutputField("maxindex", "MaxDifferenceModelDataIndex");
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::connecting field DifferenceModelData to max" << endl;
#endif
  ssf->connect("abs", 1);
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::connecting field DifferenceModelData done" << endl;
  ferr << ssf->toString() << endl;
#endif
  attachFilter(ssf, scnt);
  
  // -------------------------------------------------------------------------
  // Make the *relative *quantiles
  // -------------------------------------------------------------------------
  RecordStreamFilter* rrqf = new QuantileFilter(_filteredStreams[scnt++]);
  rrqf->renameOutputField("P(0.05)", "RelP(0.05)");
  rrqf->renameOutputField("P(0.10)", "RelP(0.10)");
  rrqf->renameOutputField("P(0.25)", "RelP(0.25)");
  rrqf->renameOutputField("P(0.50)", "RelP(0.50)");
  rrqf->renameOutputField("P(0.75)", "RelP(0.75)");
  rrqf->renameOutputField("P(0.90)", "RelP(0.90)");
  rrqf->renameOutputField("P(0.95)", "RelP(0.95)");
  rrqf->connect("quotient", 1);
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::connecting field DifferenceModelData to *relative* quantiles done" << endl;
  ferr << rrqf->toString() << endl;
#endif
  attachFilter(rrqf, scnt);

  // -------------------------------------------------------------------------
  // Get the maximum *relative* difference
  // -------------------------------------------------------------------------
  RecordStreamFilter* rssf = new MaxFilter(_filteredStreams[scnt++]);
  rssf->renameOutputField("max", "RelMaxDifferenceModelData");
  rssf->renameOutputField("maxindex", "RelMaxDifferenceModelDataIndex");
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::connecting field RelDifferenceModelData to max" << endl;
#endif
  rssf->connect("quotient", 1);
#ifdef DEBUG_TESSTOOL
  ferr << "TessToolSender::connecting field RelDifferenceModelData done" << endl;
  ferr << rssf->toString() << endl;
#endif
  attachFilter(rssf, scnt);
  

  // -------------------------------------------------------------------------
  // Look for model insanity
  // -------------------------------------------------------------------------
  RecordStreamFilter* rpsf = new PSanityFilter(_filteredStreams[scnt++]);
  rpsf->renameOutputField("insane", "Impossible");
  rpsf->renameOutputField("maxinsane", "MaxImpossibleIndex");
  rpsf->connect("Value Array", 1);
  rpsf->connect("Prediction Array", 2);
#ifdef DEBUG_TESSTOOL
  ferr << rpsf->toString() << endl;
#endif
  attachFilter(rpsf, scnt);
  

  // -------------------------------------------------------------------------
  // Select fields for TessTool - no Data fields
  // -------------------------------------------------------------------------
  vector<string> sflds = vector<string> ();

  sflds.push_back("NumRecords");
  sflds.push_back("TileId");
  sflds.push_back("X(0,0)");
  sflds.push_back("Y(0,0)");
  sflds.push_back("X(1,1)");
  sflds.push_back("Y(1,1)");

  sflds.push_back("NumDims");
  sflds.push_back("NumData");

  sflds.push_back("DataTotal");
  sflds.push_back("ModelTotal");
  sflds.push_back("DiffTotal");

  sflds.push_back("mean");
  sflds.push_back("variance");
  sflds.push_back("max");
  sflds.push_back("min");

  sflds.push_back("P(0.05)");
  sflds.push_back("P(0.10)");
  sflds.push_back("P(0.25)");
  sflds.push_back("P(0.50)");
  sflds.push_back("P(0.75)");
  sflds.push_back("P(0.90)");
  sflds.push_back("P(0.95)");
  sflds.push_back("MaxDifferenceModelData");
  sflds.push_back("MaxDifferenceModelDataIndex");

  sflds.push_back("RelP(0.05)");
  sflds.push_back("RelP(0.10)");
  sflds.push_back("RelP(0.25)");
  sflds.push_back("RelP(0.50)");
  sflds.push_back("RelP(0.75)");
  sflds.push_back("RelP(0.90)");
  sflds.push_back("RelP(0.95)");
  sflds.push_back("RelMaxDifferenceModelData");
  sflds.push_back("RelMaxDifferenceModelDataIndex");

  sflds.push_back("Impossible");
  sflds.push_back("MaxImpossibleIndex");

  bool rv = attachSelectionFilter(sflds, scnt+1);  
  if (rv)
    _defaultFunctionSelectionFilterAttached = true;
  return rv;
}

void TessToolSender::SetRecordType(RecordType *rt) 
{
  _rt = rt;
}

void TessToolSender::SetMPIStream(RecordOutputStream *mpistrm) 
{ 
  _mpistrm = mpistrm;
  _mpiStreamCreated = true;
}

void TessToolSender::SetMPIFilter(MPIStreamFilter *filter) 
{
  _filter = filter;
}

RecordType *TessToolSender::GetRecordType() 
{return _rt;};

RecordOutputStream *TessToolSender::GetMPIStream() 
{
  if (!_mpiStreamCreated) {
    _likely->BuildTessToolTileDumpBaseStream();
  }

  if (!_mpiFilterAttached)
    attachMPIFilter();

  return _mpistrm; 
}

MPIStreamFilter *TessToolSender::GetMPIFilter() 
{
  if (!_mpiStreamCreated) {
    _likely->BuildTessToolTileDumpBaseStream();
  }

  if (!_mpiFilterAttached)
    attachMPIFilter();

  return _filter; 
}

template<class T>
void TessToolSender::insertInVector(vector<T>& vec, int index, T item)
{
  int count=0;
  typename vector<T>::iterator itr;
  itr = vec.begin();
  while(count<index && itr <vec.end()) {
   itr++;
   count++;
  }
  vec.insert(itr, item);
}
