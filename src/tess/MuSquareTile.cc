// This may look like C code, but it is really -*- C++ -*-

#include <cmath>
#include <MuSquareTile.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MuSquareTile)

using namespace BIE;

MuSquareTile::MuSquareTile() 
{
  MU = 0.67;
}

MuSquareTile::MuSquareTile(double mu)
{
  MU = mu;
}


MuSquareTile::MuSquareTile (double x1, double y1, double x2, double y2, 
			double mu) : SquareTile(x1, y1, x2, y2) 
{
  MU = mu;
}
    
MuSquareTile* MuSquareTile::New() 
{
  MuSquareTile *p = new MuSquareTile();
  p->MU = MU;
  return p;
}

double MuSquareTile::Y(double u, double v)
{
  v = max<double>(0.0, v);
  v = min<double>(1.0, v);
      
  double Q = points[2] + v*dy;
  double QQ = fabs(Q);
  return asin(pow(QQ, 1.0/(1.0 - MU))) * copysign(1.0, Q);
}
    
double MuSquareTile::measure(double u, double v) 
{
  double Q = points[2] + v*dy;
  double QQ = fabs(Q);
  return pow(QQ, MU/(1.0 - MU))/(1.0 - MU) /
    sqrt(fabs(1.0 - pow(QQ, 2.0/(1.0 - MU)))) * dx * dy;
}

void MuSquareTile::printTile(ostream & outputstream)
{
  outputstream << "MU = " << MU
               << " xmin " << points[0] << " xmax " << points[1]
               << " ymin " << points[2] << " ymax " << points[3] << endl;
}
