
#include <iostream>
#include <fstream>
using namespace std;

#include <BIEconfig.h>
#include <QuadGrid.h>
#include <BIEException.h>
#include <Tile.h>
#include <Node.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::QuadGrid)

using namespace BIE;

QuadGrid::QuadGrid
(Tile *factory, double min_x, double max_x, double min_y, double max_y, int depth)
{
  // Check the input parameters are sensible.
  if (min_x >= max_x)
  { throw BIEException("QuadGrid Exception", "Min x value greater or equal to max x value.", __FILE__, __LINE__); }
  
  if (min_y >= max_y)
  { throw BIEException("QuadGrid Exception", "Min y value greater or equal to max y value.", __FILE__, __LINE__); }
  
  if (depth < 0)
  { throw BIEException("QuadGrid Exception", "Depth cannot be a negative value.", __FILE__, __LINE__); }
  
  // Take note of the defining parameters.
  tilefactory = factory;
  Xmin        = min_x;
  Xmax        = max_x;
  Ymin        = min_y;
  Ymax        = max_y;
  maxdepth    = depth;
  Ntiles      = 0;

  // Create the tessellation
  tessellate(&treeroot, 0, 0, Xmin, Ymin, Xmax, Ymax);
}

void QuadGrid::tessellate
(Node **node, int depth, int tileid,
 double minx, double miny, double maxx, double maxy)
{
  Node *northeast, *southeast, *northwest, *southwest;

  // Get a new tile from the factory and store it in our tile vector
  Tile * newtile = tilefactory->New();
  newtile->setup(minx, maxx, miny, maxy);
  _tiles[tileid] = newtile;
  
  // Create a node with four children.
  QuadNode * quadnode = new QuadNode(newtile, tileid, depth);
  *node = quadnode;

  Ntiles++;

  if(depth<maxdepth) 
  {
    // If we have not reached the maximum depth then carry on splitting.
    double xsplit = (maxx+minx)/2;
    double ysplit = (maxy+miny)/2;
      
    // Recursively create the tessellation nested in each child.
    tessellate(&northeast, depth+1, 4*tileid+1, xsplit, ysplit, maxx, maxy);
    tessellate(&southeast, depth+1, 4*tileid+2, xsplit, miny, maxx, ysplit);
    tessellate(&southwest, depth+1, 4*tileid+3, minx, miny, xsplit, ysplit);
    tessellate(&northwest, depth+1, 4*tileid+4, minx, ysplit, xsplit, maxy);

    (quadnode)->setNorthEast(northeast);
    (quadnode)->setSouthEast(southeast);
    (quadnode)->setSouthWest(southwest);
    (quadnode)->setNorthWest(northwest);
  }
}

vector<int> QuadGrid::GetRootTiles()
{
  vector<int> result (1, treeroot->ID());
  return result;
}

vector<Node*> QuadGrid::GetRootNodes()
{
  vector<Node*> result (1, treeroot);
  return result;
}
