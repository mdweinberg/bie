#include <ContainerTessellation.h>
#include <BIEException.h>
#include <clivector.h>
#include <iostream>
#include <Node.h>
#include <Tile.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::ContainerTessellation)

using namespace BIE;

ContainerTessellation::ContainerTessellation(clivectortess* tess)
{
  int offset = 0;
  Ntiles = 0;

  // Complain if the list of tessellations is empty.
  if ((*tess)().size() < 1)
  { throw BIEException("ContainerTessellation Exception",
          "Empty tessellation vector.", __FILE__, __LINE__); }
  
  // Create a new container tessellations based on the structures of the
  // tessellations.
  for (int tessindex = 0; tessindex < (int)(*tess)().size(); tessindex++)
  {
    Tessellation * currenttess = (*tess)()[tessindex];
    
    // Copy the tiles into this object and assign new tile ids.
    for (int tileid = 0; tileid < currenttess->NumberTiles(); tileid++)
    { _tiles[tileid+offset] = currenttess->GetTile(tileid); }
    
    // Copy the node tree and change tile ids to match the assignment above.
    vector<Node*> tessrootnodes = currenttess->GetRootNodes();

    for (int nodeindex = 0; nodeindex < (int) tessrootnodes.size(); nodeindex++)
    { _rootnodes.push_back(tessrootnodes[nodeindex]->copytree(offset)); }
    
    // Get a list of the root tiles in this tessellation.
    vector<int> tessroottiles = currenttess->GetRootTiles();
    
    // Append top level tiles in this tessellation to list of top level tiles
    for (int rootindex = 0; rootindex < (int) tessroottiles.size(); rootindex++)
    { _roottiles.push_back(tessroottiles[rootindex] + offset); }

    // Increment the offset !!
    offset += currenttess->NumberTiles();
    
    // Keep a note of the total number of tiles.
    Ntiles += currenttess->NumberTiles();
  }
  
  // Check that the boundaries do not overlap with another tessellation
  unsigned int tessoneindex, tesstwoindex, tessoneroot, tesstworoot;

  for (tessoneindex = 0; tessoneindex < (*tess)().size(); tessoneindex++)
  {
    Tessellation * tessone = (*tess)()[tessoneindex];
    vector<int> tessoneroots   = tessone->GetRootTiles();
    
    for (tesstwoindex = tessoneindex + 1; 
         tesstwoindex < (*tess)().size(); tesstwoindex++)
    {
      Tessellation * tesstwo = (*tess)()[tesstwoindex];
      vector<int> tesstworoots   = tesstwo->GetRootTiles();
      
      for (tessoneroot = 0; tessoneroot < tessoneroots.size(); tessoneroot++)
      {
        for (tesstworoot = 0; tesstworoot < tesstworoots.size(); tesstworoot++)
        {
	  Tile * tileone = tessone->GetTile(tessoneroots[tessoneroot]);
	  Tile * tiletwo = tesstwo->GetTile(tesstworoots[tesstworoot]);
	  	
          if (tileone->overlapsWith(tiletwo))
	  { throw TessellationOverlapException(__FILE__,__LINE__);}
	}
      }
    }
  }
}

ContainerTessellation::ContainerTessellation(std::vector<Tessellation*> tess)
{
  int offset = 0;
  Ntiles = 0;

  // Complain if the list of tessellations is empty.
  if (tess.size() < 1)
    { throw BIEException("ContainerTessellation Exception",
			 "Empty tessellation vector.", __FILE__, __LINE__); }
  
  // Create a new container tessellations based on the structures of the
  // tessellations.
  for (int tessindex = 0; tessindex < (int)tess.size(); tessindex++)
  {
    Tessellation * currenttess = tess[tessindex];
    
    // Copy the tiles into this object and assign new tile ids.
    for (int tileid = 0; tileid < currenttess->NumberTiles(); tileid++)
    { _tiles[tileid+offset] = currenttess->GetTile(tileid); }
    
    // Copy the node tree and change tile ids to match the assignment above.
    vector<Node*> tessrootnodes = currenttess->GetRootNodes();

    for (int nodeindex = 0; nodeindex < (int) tessrootnodes.size(); nodeindex++)
    { _rootnodes.push_back(tessrootnodes[nodeindex]->copytree(offset)); }
    
    // Get a list of the root tiles in this tessellation.
    vector<int> tessroottiles = currenttess->GetRootTiles();
    
    // Append top level tiles in this tessellation to list of top level tiles
    for (int rootindex = 0; rootindex < (int) tessroottiles.size(); rootindex++)
    { _roottiles.push_back(tessroottiles[rootindex] + offset); }

    // Increment the offset !!
    offset += currenttess->NumberTiles();
    
    // Keep a note of the total number of tiles.
    Ntiles += currenttess->NumberTiles();
  }
  
  // Check that the boundaries do not overlap with another tessellation
  unsigned int tessoneindex, tesstwoindex, tessoneroot, tesstworoot;

  for (tessoneindex = 0; tessoneindex < tess.size(); tessoneindex++)
  {
    Tessellation * tessone = tess[tessoneindex];
    vector<int> tessoneroots   = tessone->GetRootTiles();
    
    for (tesstwoindex = tessoneindex + 1; 
         tesstwoindex < tess.size(); tesstwoindex++)
    {
      Tessellation * tesstwo = tess[tesstwoindex];
      vector<int> tesstworoots   = tesstwo->GetRootTiles();
      
      for (tessoneroot = 0; tessoneroot < tessoneroots.size(); tessoneroot++)
      {
        for (tesstworoot = 0; tesstworoot < tesstworoots.size(); tesstworoot++)
        {
	  Tile * tileone = tessone->GetTile(tessoneroots[tessoneroot]);
	  Tile * tiletwo = tesstwo->GetTile(tesstworoots[tesstworoot]);
	  	
          if (tileone->overlapsWith(tiletwo))
	  { throw TessellationOverlapException(__FILE__,__LINE__);}
	}
      }
    }
  }
}
