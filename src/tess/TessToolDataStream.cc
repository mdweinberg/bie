#include <iostream>
#include <sstream>
#include <cstdlib>

#include <gvariable.h>
#include <TessToolDataStream.h>
#include <RecordStream_Ascii.h>
#include <MPICommunicationSession.h>
#include <bieTags.h>
#include <BasicType.h>
#include <tesstoolMutex.h>

using namespace BIE;

TessToolDataStream::TessToolDataStream()
{
  _first = true;
}

bool TessToolDataStream::parse_command()
{
  bool ret = false;

  ++tesstoolMutex;
  cerr  << "TessToolDataStream::parse_command: got Mutex" << endl;
  rferr << "TessToolDataStream::parse_command: got Mutex" << endl;

				// check the command buffer
  cerr  << "TessToolDataStream received command: " << _tcmd << endl << flush;
  rferr << "TessToolDataStream received command: " << _tcmd << endl << flush;
  
  int w1 = *_tcmd;
  if (w1 == -1) {
    ret = true;
  } else {
    char *cmd=_tcmd;
    if(strncmp(cmd, "Synchronize", 11)==0) {
      Synchronize();
    } else if(strncmp(cmd, "GetData", 7)==0) {
      GetData();
    } else {
      // add other commands
    }
  }
  
  --tesstoolMutex;

  cerr  << "TessToolDataStream::parse_command: Mutex released" << endl;
  rferr << "TessToolDataStream::parse_command: Mutex released" << endl;
  
  return ret;
}

void TessToolDataStream::SynchronizeCommand()
{
  char *cmd=_tcmd;
  sprintf(cmd, "Synchronize");
  return;
}

int TessToolDataStream::readBuffer(unsigned char* val, int size)
{
  // if buffer has a record item... return it
  // if buffer is empty, go refill it
  // if we are at end, return 1 else 0;

  rferr << "readBuffer _currentBufferPointer=" << _currentBufferPointer 
	<< " _currentBufferEnd=" << _currentBufferEnd << endl << flush;

  if (_currentBufferPointer < _currentBufferEnd) {
    memcpy(val, _currentBufferPointer, size);
    _currentBufferPointer = (void *)  ((char *)_currentBufferPointer + size);
    rferr << "readBuffer reading from current buffer" << endl << flush;
    return 0;
  } else {
    rferr << "readBuffer calling GetData" << endl << flush;
    GetData();
    if (_currentBufferPointer < _currentBufferEnd) {
      memcpy(val, _currentBufferPointer, size);
      _currentBufferPointer = (void *)  ((char *)_currentBufferPointer + size);
      return 0;
    } else {
      rferr << "readBuffer end of buffer" << endl << flush;
      return -1;
    }
  }
}

void TessToolDataStream::notifyConsumer()
{
  _consumerSem->post();
  rferr << "TessToolDataStrea: consumer notified" << endl << flush;
}
