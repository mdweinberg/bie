#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <list>

#include <ConfigFileReader.h>

using namespace ost;

ConfigFileReader::ConfigFileReader(const string& keyword)
{
  std::string keyPath = "/bie/" + keyword;
  std::string filPath = "~bie/" + keyword; 

				// Load from database
				// ------------------
  const char *kpath = keyPath.c_str();
  load (kpath);

  createConfigFile(keyword);
				// Load from working directory
				// ---------------------------
  const char *fpath = filPath.c_str();
  loadFile (fpath);

				// Do we have a key?
				// -----------------
  int cnt = getCount(keyword.c_str());
  if (cnt>0) {
    const char * const* clist = getList(keyword.c_str());
    for (int j=0; j<cnt; j++)
      std::cout << "Key: " << setw(15) << keyword
		<< "value: " << setw(15) << clist[j]
		<< std::endl;
  }
}


const char* ConfigFileReader::
getValue(const char *symbol, const char * defaultvalue)
{
  const char * symbolvalue = getLast(symbol);

  if (symbolvalue != 0)
  { return symbolvalue; }
  else 
  { return defaultvalue; }
}

bool ConfigFileReader::getValue(const char *symbol, bool defaultvalue)
{
  const char * symbolvalue = getLast(symbol);

  if (symbolvalue != 0)
  { return atoi(symbolvalue); }
  else 
  { return defaultvalue; }
}

int ConfigFileReader::getValue(const char *symbol, int defaultvalue)
{
  const char * symbolvalue = getLast(symbol);

  if (symbolvalue != 0)
  { return atoi(symbolvalue); }
  else 
  { return defaultvalue; }
}

double ConfigFileReader::getValue(const char *symbol, double defaultvalue)
{
  const char * symbolvalue = getLast(symbol);

  if (symbolvalue != 0)
  { return atof(symbolvalue); }
  else 
  { return defaultvalue; }
}

std::string ConfigFileReader::getBIEDIR(const std::string& homedir)
{
  // Make a guess at bie source dir
  //
  string bieconfname;
  ifstream testbieconf;

  std::list<std::string> stdlocs = {
    homedir + "/BIE",
    homedir + "/src/BIE",
    homedir + "/Projects/BIE",
    "/usr/src/BIE",
    "/usr/local/BIE",
    "/usr/local/src/BIE",
    "/opt/BIE",
    "/opt/src/BIE"
  };
  
  // Attempt to get the BIE source location from the environment
  //
  char *env = std::getenv("BIE_SOURCE_DIR");
  
  // If a valid c-string is returned, add to front of list
  //
  if (env) stdlocs.push_front(env);

  // Try each path and return the first successful one 
  for (auto s : stdlocs) {
    bieconfname = s + "/include/BIEconfig.h";
    testbieconf.close();
    testbieconf.open(bieconfname.c_str());
    if (testbieconf) return s;
  }

  // return a placeholder
  //
  return homedir + "/BIE";
}

void ConfigFileReader::createConfigFile(const string &keyword)
{
  std::string   sysconfname = "/etc/bierc";
  std::ifstream testsysconf(sysconfname.c_str());

  std::string   homedir = getenv("HOME");
  std::string   homeconfname = homedir + "/.bierc";
  std::ifstream testhomeconf(homeconfname.c_str());

  // If /etc/bierc or ~/.bierc exists, load it
  //
  if (testsysconf or testhomeconf) {
    if (testsysconf ) loadFile(sysconfname.c_str(),  keyword.c_str());
    if (testhomeconf) loadFile(homeconfname.c_str(), keyword.c_str());
    return;
  }

  // Attempt to get BIE home from the environment or standard guesses
  //
  std::string   BIEhome = getBIEDIR(homedir);
  std::string   visualizedir = BIEhome + "/Visualizer";
  std::string   clidir = BIEhome + "/cli";
  std::string   helpdir = clidir + "/help";
  
  // Otherwise, try to create a .bierc.  This may produce strange
  // results if executed concurrently from multiple processes.
  //
  std::cout << "******************************************************************" << std::endl
	    << "It appears that you don't have a default system configuration file" << std::endl
	    << "file in /etc/bierc or a ~/.bierc.  As a stop gap measure, you need" << std::endl
	    << "a configuration file in your home directory.  I will create a test" << std::endl
	    << ".bierc in your home directory.  You may generate a valid, initial " << std::endl
	    << "~/.bierc file by deleting the current one (if it exists) and      " << std::endl
	    << "setting the BIE_SOURCE_DIR env variable to the location of your   " << std::endl
	    << "top-level BIE source tree                                         " << std::endl
	    << "******************************************************************" << std::endl;

  std::ofstream config(homeconfname.c_str());
   
  // Look for systemwide help dir
  std::string systemhelp = "/usr/share/bie/help";
  if (std::ifstream(systemhelp.c_str())) {
    helpdir = systemhelp;
  }
    
  if (!config) {
    std::cout << "cannot open the file for writing: " << homeconfname <<endl;
    std::cout.flush();
  }
  
  config << "[cli]" << endl;
  config << "#              This flag turns out statefile logging" 
	 << endl;
  config << "logfile = 1" << endl;
  config << "#              The default name of your state file" << endl;
  config << "outfile = \"simulation.output\"" << endl;
  config << "#              This is the source directory containing" << endl
	 << "#              your BIE tree" << endl;
  config << "homedir = " << homedir << endl;
  config << "helpdir = " << helpdir << endl;
  config << "visualizedir = " << visualizedir << endl;
  config << "clidir = " << clidir << endl;
  config << "[output]" << endl;
  config << "mpi_slave_cli = cout" << endl;
  config << "#              Change the value of mpi_slave_cli to a" << endl
	 << "#              file name prefix if you want to see" << endl
	 << "#              standard output and standard error from" << endl
	 << "#              from your mpi processes separate files." << endl
	 << "#              Removing the mpi_slave_cli variable will" << endl
	 << "#              cause the output to be sent to /dev/null" << endl;
  config.close();

  loadFile(homeconfname.c_str(), keyword.c_str());

  std::cout << std::endl
	    << "--------------------------------------------------------------" << std::endl
	    << "Here are the default values:" << std::endl
	    << "--------------------------------------------------------------" << std::endl
	    << "** your home directory: "<< homedir << std::endl
	    << "** your BIE directory: " << BIEhome << std::endl
	    << "** your cli directory: " << clidir<< std::endl
	    << "** your cli help directory: " << helpdir << std::endl
	    << "** your cli visualizer directory: " << visualizedir << std::endl
	    << "** Log file is enabled" << std::endl
	    << "** The default simulation log will be called: \"simulation.output\"" << std::endl
	    << "** Node output will be prefixed by \"slave.out\"" << std::endl
	    << "** You can set these values in cli or in the .bierc file." << std::endl;
  
  std::cout << "**************************************************************" << std::endl
	    << "** If you are seeing this message multiple times, delete your " << std::endl
	    << "** .bierc file, set the BIE_SOURCE_DIR environment variable to" << std::endl
	    << "** the top-level BIE src dir, and rerun with one instance of  " << std::endl
	    << "** \"bie\"                                                    " << std::endl
	    << "**************************************************************" << std::endl << std::endl;
}
