#include <sstream>
#include <string>
#include <ArrayType.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::ArrayType)

using namespace BIE;

ArrayType::ArrayType (BasicType *btype, int len) : BasicType()
{
  // only allow arrays of predefined BasicTypes: Int, Real and Bool
  stringstream ats (stringstream::out);
  if ( btype->equals(BasicType::Int) ) {
    arrayBase = BasicType::Int;
  } else if ( btype->equals(BasicType::Real) ) {
    arrayBase = BasicType::Real;
  } else if ( btype->equals(BasicType::Bool) ) {
    arrayBase = BasicType::Bool;
  } else {
    throw TypeException("Can create arrays only of predefined basic types",__FILE__, __LINE__);
  }

  length = len;
  isArray = true;
  ats <<'[' << btype->getTypeName() << '@' << length;
  type_name = ats.str();
}

ArrayType::ArrayType (ArrayType *at) : BasicType()
{
  //  ArrayType(at->getBaseType(), at->getLength());
  arrayBase = at->arrayBase;
  type_name = at->type_name;
  length = at->length;
  isArray = at->isArray;
}

bool ArrayType::equals (ArrayType * type)
{
  bool result = false;
  if (type == 0) { result = false; }
  else if (this == type) { result = true;}
  else if (this->type_name == type->type_name && this->length == type->getLength()) { result = true; }
  
  return result;
}

string ArrayType::toString()
{
  return type_name;
}  

int ArrayType::getLength()
{
  return length;
}  

