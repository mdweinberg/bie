// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
using namespace std;

#include <Table.h>

bool use_guard = false;

Table::Table(int n)
{
  ntab = n;
  xx = vector<double>(n, 0.0);
  yy = vector<double>(n, 0.0);
  even_xx = 0;
}

int Table::Vlocate(double x, const vector<double>& xx)
{
  int ascnd, ju, jm, jl, min, max;
  
  min = 0;
  max = ntab-1;
  jl=min-1;
  ju=max+1;
  ascnd=xx[max] > xx[min];
  while (ju-jl > 1) {
    jm=(ju+jl) >> 1;
    if ((x > xx[jm]) == ascnd)
      jl=jm;
    else
      ju=jm;
  }
  return jl;
}

int Table::Vlocate_with_guard(double x, const vector<double>& xx)
{
  int which, min, max;

  min = 0;
  max = ntab-1;
  which = (xx[min] < xx[max]);

  if( ( (xx[min] < x) == which ) &&
      ( (x < xx[max]) == which ) ) {
    return Vlocate(x, xx);
  }
  else{
    if( (x <= xx[min]  ) == which ){
      return min;
    }
    else if( (x >= xx[max]) == which ){
      return max;
    }
    else{
      cerr << "Table: WARNING: misordered data in locate_with_guard\n";
    }
  }

  return -1;			// Should never get here
}

double Table::odd2(double x, const vector<double> &xtab, const vector<double> &ftab, int even)
{
  int index, min, max;
  double ans;


  min = 0;
  max = ntab-1;

  if (even)
    index = (int)((x-xtab[min])/(xtab[max]-xtab[min])*(double)(max-min)) + min;
  else
    index = Vlocate(x, xtab);

  if (index < min) index=min;
  if (index >= max) index=max-1;

  ans = ( ftab[index+1]*(x-xtab[index]  ) -
	  ftab[index  ]*(x-xtab[index+1]) )
    /( xtab[index+1]-xtab[index] ) ;

  return ans;
}


double Table::drv2(double x, const vector<double>& xtab, const vector<double>& ftab, int even)
{
  int index, min, max;
  double ans;

  min = 0;
  max = ntab-1;

  if (even)
    index = (int)((x-xtab[min])/(xtab[max]-xtab[min])*(double)(max-min)) + min;
  else
    index = Vlocate(x, xtab);

  if (index < min) index=min;
  if (index >= max) index=max-1;

  ans = ( ftab[index+1] -ftab[index] )/( xtab[index+1]-xtab[index] ) ;

  return ans;
}

double Table::get_x(double y)
{
  return odd2(y, yy, xx);
}

double Table::get_y(double x)
{
  return odd2(x, xx, yy, even_xx);
}

double Table::get_drv(double x)
{
  return drv2(x, xx, yy, even_xx);
}



