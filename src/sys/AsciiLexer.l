%{
#include "AsciiLexer.h"
#include "errno.h"
#include "BIEException.h"
using namespace BIE;
%}

%option nounput
%option noyywrap
%option c++
%option full 
%option yyclass="AsciiLexer"
%option prefix="Asc"

DIGIT       [0-9]
STRING      \"([^\n\"\\]|\\(n|t|\\|\"))*\" 
INTEGER     (\-|\+)?{DIGIT}+ 
EXPONENT    ((e|E)(\-|\+)?{DIGIT}+)
REALNUMBER  (\-|\+)?{DIGIT}+("."{DIGIT}*{EXPONENT}?|{EXPONENT}?)
INF         (\-|\+)?(I|i)(N|n)(F|f)
NAN         (\-|\+)?(N|n)(A|a)(N|n)
%%

{INTEGER}  {
    al_stripped = (char*) YYText(); 
    return AL_INT_VALUE_; 
  }

({REALNUMBER}|{INF}|{NAN})  {
    al_stripped = (char*) YYText(); 
    return AL_REAL_VALUE_; 
  }

{STRING} {	  
    const char * text = YYText();
    
    /* Strip off the double quotes */
    al_stripped   = &(((char*)text)[1]);
    al_stripped[strlen(al_stripped) - 1] = '\0';
    
    /* Convert the escape sequences */
    char * slash = strchr(al_stripped, '\\');
    int escapes  = 0;
  
    while (slash != NULL)
    {    
      escapes++;
      
      switch(slash[1])
      {
        case 'n': slash[1-escapes] = '\n'; break;
        case '"': slash[1-escapes] = '"'; break;
        case 't': slash[1-escapes] = '\t'; break;
        case '\\': slash[1-escapes] = '\\'; break;
        default:
          abort();
      }
      
      char * src  = &(slash[2]);
      char * dest = &(slash[2-escapes]);
      
      /* find the next backslash if there is one. */
      slash = strchr(&(slash[2]), '\\');    
      
      if (slash == NULL)
      { /* Move the remaining part of the string + the null character */
        memmove(dest,src,strlen(src)+1);
      }
      else
      {
        slash[0] = '\0';
        memmove(dest,src,sizeof(char) * strlen(src));
      }
    }

    return AL_STRING_VALUE_;
  }
        
(I|i)(N|n)(T|t)  { return AL_INT_KEYWORD_; }

(R|r)(E|e)(A|a)(L|l) { return AL_REAL_KEYWORD_; }

(B|b)(O|o)(O|o)(L|l) { return AL_BOOL_KEYWORD_; }

(S|s)(T|t)(R|r)(I|i)(N|n)(G|g)  { return AL_STRING_KEYWORD_; }

[ ]*"#"[^\n]*  { /** comment - skip over */ }

,       { return AL_COMMA_; }

=       { return AL_EQUALS_; }

\{       { return AL_LEFT_CURLY_; }

\}       { return AL_RIGHT_CURLY_; }

\[       { return AL_LEFT_SQUAREBRACKET_; }

\@       { return AL_ATRATE_; }

\n|\r   { return AL_NEWLINE_; }

[ \t]+  { /* White space - skip over */ }


<<EOF>> { return AL_EOF_; }

.       { return AL_ERROR_; }
%%

AsciiLexer::AsciiLexer 
(string filename)
{
  // Open the file.
  al_inputstream = new ifstream(filename.c_str());
  
  // Check that it was opened successfully.
  if (! al_inputstream->good())
  { throw FileOpenException (filename, errno, __FILE__, __LINE__); }
  
  // Set this stream to be the stream used by the lexer.
  switch_streams(al_inputstream, 0);

  // The token is 0 until nextToken is called.
  al_stripped = 0;

  // We will delete the stream when object is deleted.
  al_streamismine = true;
}

AsciiLexer::AsciiLexer 
(istream * inputstream)
{
  // Set this stream to be the stream used by the lexer.
  switch_streams(inputstream, 0);
  al_inputstream  = inputstream;

  // The token is 0 until nextToken is called.
  al_stripped = 0;
  
  // We will not delete the stream when this object is delete - the caller 
  // is responsible.
  al_streamismine = false;
}

AsciiLexer::~AsciiLexer ()
{
  if (al_streamismine) { 
    delete al_inputstream;
  }
}
