#include <BasicType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BasicType)

using namespace BIE;

BasicType::BasicType (string newtypename)
{
  type_name = newtypename;
  isArray = false;
}

BasicType::BasicType ()
{
  type_name = string();
  isArray = false;
}

bool BasicType::equals (BasicType * type)
{
  bool result = false;
  if (type == 0) { result = false; }
  else if (this == type) { result = true;}
  else if (this->type_name == type->type_name) { result = true; }
  
  return result;
}

string BasicType::toString()
{
  return type_name;
}  

BasicType BasicType::StringObj = BasicType("string"); 
BasicType BasicType::IntObj    = BasicType("int"); 
BasicType BasicType::RealObj   = BasicType("real"); 
BasicType BasicType::BoolObj   = BasicType("bool"); 

BasicType * BasicType::String  = &BasicType::StringObj; 
BasicType * BasicType::Int     = &BasicType::IntObj; 
BasicType * BasicType::Real    = &BasicType::RealObj; 
BasicType * BasicType::Bool    = &BasicType::BoolObj;
