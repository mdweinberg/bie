/*****************************************************************************
* This code parses the file Reflect/methods.out, which is stored interally 
* as a string.  A data structure describing the methods and constructors
* available in each class is built when this string is parsed, and then
* used to enable dynamic calls of methods and constructors.
******************************************************************************/

#include <fstream>
#include <string>
#include <vector>
#include <list>
using namespace std;

#include <MethodTable.h>
#include <GlobalTable.h>
#include <gfunction.h>
#include <gvariable.h>
#include <BIEException.h>
#include <CliArgList.h>
#include <AData.h>

using namespace BIE;

// Prototypes for automatically generated functions.
extern void Construct(char *, CliArgList *, AData *);
extern void Invoke(void *, char *, CliArgList *, AData *);
extern void InvokeStatic(char *, CliArgList *, AData *);

// Static class variables.
MethodTable::_class_info * MethodTable::classes = 0;
int MethodTable::num_classes = 0;
MethodTable::_inheritance_tree * MethodTable::the_tree = 0;

void MethodTable::initialize(string& methods)
{
  char input_line[1000], 
       sig[1000]; 
  char *str;

  // The string is treated like a file.
  istringstream ifile(methods.c_str());
  if (!ifile){ throw InternalError(__FILE__, __LINE__); }

  ifile.getline(input_line, 20);
  num_classes = atoi(&input_line[1]);

  classes = new _class_info[num_classes+1];

  ifile.getline(input_line, 1000);
  for (int i=0; i<num_classes; i++){
    str = strtok(input_line," ");
    
    if (strcmp(str, "CLICLASS") == 0 ) { classes[i].cli_class = true; }
    else if (strcmp(str, "CLASS") == 0 ) { classes[i].cli_class = false; }
    else { throw InternalError(__FILE__, __LINE__); } 
    
    str = strtok(NULL, " ");
    classes[i].class_name = new char[strlen(str)+1];
    strcpy (classes[i].class_name, str);
    str = strtok(NULL, " ");
    if (str && !strcmp(str, "SUPER")){
      str = strtok(NULL, " ");
      classes[i].super_class = new char[strlen(str)+1];
      strcpy(classes[i].super_class, str);
    }
    else
      classes[i].super_class = NULL;

    ifile.getline(input_line, 1000);
    classes[i].num_methods = atoi(&input_line[1]);
    classes[i].methods = new _method_info[classes[i].num_methods+1];

    ifile.getline(input_line, 1000);
    while (input_line[0] == '*'){
      classes[i].desc += &input_line[1];
      classes[i].desc += "\n";
      ifile.getline(input_line, 1000);
    }
    
    int num_args;
    for (int j=0; j < classes[i].num_methods; j++)
    {
      AData * an=NULL;
      _method_info *mi = &(classes[i].methods[j]);
      num_args = 0;
   
      str = strtok(input_line, " ");
      
      mi->classinfo = &(classes[i]);
      
      if ((strcmp(str, "CLIMETHOD") == 0) || (strcmp(str, "CLICONSTR") == 0)) 
      { mi->cli_invokable = true; mi->staticmethod = false; }
      else if ((strcmp(str, "METHOD") == 0) || (strcmp(str, "CONSTR") == 0)) 
      { mi->cli_invokable = false; mi->staticmethod = false; }
      else if ((strcmp(str, "STATICMETHOD") == 0)) 
      { mi->cli_invokable = false; mi->staticmethod = true; }
      else if ((strcmp(str, "STATICCLIMETHOD") == 0)) 
      { mi->cli_invokable = true; mi->staticmethod = true; }
      else  { throw InternalError(__FILE__, __LINE__); } 
      
      str = strtok(NULL, " ");
      mi->method_name = new char[strlen(str)+1];
      strcpy (mi->method_name, str);
      strcpy(sig, classes[i].class_name);
      strcat(sig,"::");
      strcat(sig, classes[i].methods[j].method_name);
      mi->alist = new CliArgList();
      mi->ret = 0;

      while( (str = strtok(NULL, " ")) ){
        strcat(sig, str);
        an = new AData; 

	switch (str[0]) 
	{
          case 'A':
	    setType(an, str[0]);
	    mi->alist->add_element(an);
	    break;
	  case 'B': case 'I': case 'U': case 'D': case 'F': case 'G': case 'J': 
	    setType(an, str[0]);
	    mi->alist->add_element(an);
	    break;
          case 'P':
	    setType(an, str[0]);
	    mi->alist->add_element(an);
            str = strtok(NULL, " ");
            an->setTypeString(str);
	    strcat(sig, str);
	    break;
          case 'Z':
            str = strtok(NULL, " ");
            
	    if (str[0] != 'V')
	    {
              mi->ret = an;
              setType(an, str[0]);
              strcat(sig, str);
  
              if (an->is_pointer())
              {
                str = strtok(NULL, " ");
                an->setTypeString(str);
                strcat(sig, str);
              }
	    }
            else
            { strcat(sig, str);  }
	    break;
          case 'V': break;
	  default:
	    throw InternalError(__FILE__,__LINE__);
        }
      }

      mi->arg_signature = stralloc(sig);
      
      ifile.getline(input_line, 1000);
      while (input_line[0] == '*'){
        mi->desc += &input_line[1];
        mi->desc += "\n";
        ifile.getline(input_line, 1000);
      }
    }
  }

  the_tree = new _inheritance_tree(classes, num_classes);
}

void MethodTable::setType(AData * a, char typechar)
{
  switch (typechar)
  {
    case 'A':
      a->setType(AData::arrayindex_TYPE);
      break;
    case 'B': 
      a->setType(AData::bool_TYPE);
      break;
    case 'I': 
      a->setType(AData::int32_TYPE);
      break;
    case 'U': 
      a->setType(AData::uint32_TYPE);
      break;
    case 'D': 
      a->setType(AData::double_TYPE);
      break;
    case 'F': 
      a->setType(AData::float_TYPE);
      break;
    case 'G': 
      a->setType(AData::charstar_TYPE);
      break;
    case 'J': 
      a->setType(AData::string_TYPE);
      break;
    case 'P':
      a->setType(AData::pointer_TYPE);
      break;
    default:
      throw InternalError(__FILE__,__LINE__);
  }
}

bool MethodTable::is_compatible_class
(const char *class_to_match, const char *actual_class_name, bool cliquery)
{
  while(actual_class_name != NULL)
  {
    _class_info * classinfo = getClassInfo(actual_class_name, cliquery);
    
    // This should not be called with a class name that does not exist.
    if (classinfo == NULL)
    { throw InternalError(__FILE__, __LINE__); }
          
    if (strcmp(class_to_match, actual_class_name) == 0)
    { return true; }
    
    actual_class_name = classinfo->super_class;
  }
  
  return false;
}

void MethodTable::print_classes()
{
  ostream cout(checkTable("cli_console"));
  
  for(int classindex = 0; classindex  < num_classes; classindex++)
  { 
    if (!restrictcli || classes[classindex].cli_class)
      { std::cout << classes[classindex].class_name << endl; }
  }
}

string MethodTable::get_method_arg_string (_method_info & m)
{
  string method_arg_string = "";
  
  // Add return type.
  if (m.ret)
  { method_arg_string += (m.ret)->get_type_name(); }
  else
  { method_arg_string +=  "void"; }
  
  // Add method name
  method_arg_string +=  " ";
  method_arg_string += m.method_name;
  method_arg_string += "( ";

  // Add argument types.
  (m.alist)->reset();
  AData * a = (m.alist)->next();
  while (a)
  {
    method_arg_string += a->get_type_name();
    method_arg_string += " ";
    a = (m.alist)->next();  
  }

  (m.alist)->reset();
  
  method_arg_string += ")";

  return method_arg_string;
}

void MethodTable::print_methods(const char * class_name)
{
  ostream cout(checkTable("cli_console"));
  string methods_str;

  // Get a list of methods.
  vector<_method_info *> callable_methods;
  callable_methods = get_callable_methods(class_name, NULL, true);
  
  for (int mindex = 0; mindex < (int) callable_methods.size(); mindex++)
  {
    cout << get_method_arg_string(*(callable_methods[mindex])) << endl;
  }
}

vector<MethodTable::_method_info *> MethodTable::get_callable_methods
(const char *class_name, const char *method_name, bool cliquery)
{
  vector<_method_info *> callable;
  bool superclass = false;

  // Check there is a class with this name.
  _class_info * classinfo = getClassInfo(class_name, cliquery);
  if (classinfo == NULL) { throw NoSuchClassException(class_name, __FILE__, __LINE__); }
  
  // If method_name is NULL, return all methods in class/superclasses.
  bool returnall = false;
  if (method_name == NULL) { returnall = true; }  
  
  // Keeps looping until we reach the top of the class hierarchy.
  while (class_name != NULL)
  {
    _class_info  * classinfo  = getClassInfo(class_name, cliquery);
    if (classinfo == NULL) { throw InternalError(__FILE__, __LINE__); } 
    
    _method_info * methodinfo = classinfo->methods;
      
    // Loops through all methods.
    for(int mindex = 0; mindex < classinfo->num_methods; mindex++)
    {
      _method_info * mi = &(methodinfo[mindex]);
 
      // If method_name is NULL, return all methods in class/superclasses.
      // Or if the method name matches the one we are looking for.
      if (returnall || (strcmp(mi->method_name, method_name) == 0))
      {
        //  Skip if this is cli query and the method is cli accessible.
	if (cliquery && !(mi->cli_invokable) && restrictcli)
	{ continue; }

	// Skip if a constructor of a superclass. 
	if (superclass && (strcmp(mi->method_name, class_name) ==0))
	{ continue; }
	
	// Skip if method overridden by derived class.
        // Search methods already on the callable list.  If a method
        // with matching arguments is found it isn't added, because
        // the version on the list is from the subclass and overrides.
        bool methodoverridden = false;
	
	if (superclass)
	{
	  for (int cindex = 0; cindex < (int) callable.size(); cindex++)
	  {
	    if (strcmp(mi->method_name, callable[cindex]->method_name) == 0)
	    {
              if (mi->classinfo != callable[cindex]->classinfo)
              { methodoverridden = true; break; }
	    }
	  }
	}
	
        if (!methodoverridden) { callable.push_back(mi); }
      }
    }

    class_name = classinfo->super_class;
    superclass = true;
  }

  return callable;
}

string MethodTable::get_class_desc
(const char *class_name, bool cliquery)
{
  _class_info * classinfo = getClassInfo(class_name, cliquery);
  
  if (classinfo == NULL) { throw NoSuchClassException(class_name, __FILE__, __LINE__); }

  return classinfo->desc;
}

string MethodTable::get_method_desc
(const char *class_name, const char *method_name, bool cliquery)
{
  string description = "";
  vector<_method_info *> callable = 
           get_callable_methods(class_name, method_name, cliquery);
  
  if (callable.size() == 0)
  { throw NoSuchMethodException(class_name, method_name, __FILE__, __LINE__); }
  
  for (int mindex = 0; mindex < (int) callable.size(); mindex++)
  {
    description += get_method_arg_string(*(callable[mindex]));
    description += "\n";
    description += callable[mindex]->desc;
    description += "\n";
  }

  return description;
}

MethodTable::_class_info * MethodTable::getClassInfo
(const char * classname, bool cliquery)
{
  if (classname == 0) { return NULL; }
  
  for (int classindex = 0; classindex < num_classes; classindex++)
  {
    if (restrictcli && cliquery && !(classes[classindex].cli_class))
    { continue; }
    
    if (strcmp(classes[classindex].class_name, classname) == 0)
    { return &(classes[classindex]); }
  }

  return NULL;
}

const char * MethodTable::get_super_classname
(const char * class_name, bool cliquery)
{
  _class_info * classinfo = getClassInfo(class_name, cliquery);
  
  if (classinfo == NULL) { return NULL; }
  else { return classinfo->super_class; }
}

bool MethodTable::is_compatible_return(const AData *expected, const AData * actual, bool cliquery)
{
  // Override for cases where formal type is not declared
  if (expected->getType() == AData::unset_TYPE) { return true; }

  if (get_compatibility_level(expected, actual, cliquery) != NOT_COMPATIBLE)
  { return true; }
  else
  { return false; }
}

bool MethodTable::is_compatible_type(const AData *fp, const AData *ap, bool cliquery)
{
  if (get_compatibility_level(fp, ap, cliquery) != NOT_COMPATIBLE)
  { return true; }
  else
  { return false; }
}

int MethodTable::get_compatibility_level
(const AData *fp, const AData * ap, bool cliquery)
{
  if (!fp || !ap) { throw InternalError(__FILE__,__LINE__); }

  if (fp->is_arrayindex())
  {
    if (ap->is_arrayindex())
    { return EXACT_MATCH; }
    else 
    {return NOT_COMPATIBLE; }
  }
  else if (fp->is_bool())
  {
    if (ap->is_bool())
    { return EXACT_MATCH; }
    else if (ap->is_int32() || ap->is_uint32())
    { return CONVERSION_MATCH; }
    else 
    {return NOT_COMPATIBLE; }
  }
  else if (fp->is_int32())
  {
    if (ap->is_int32()) { return EXACT_MATCH; }
    else if (ap->is_uint32()) { return CONVERSION_MATCH; }
    else { return NOT_COMPATIBLE; }
  }
  else if (fp->is_uint32())
  {
    if (ap->is_uint32()) { return EXACT_MATCH; }
    else if (ap->is_int32()) { return CONVERSION_MATCH; }
    else { return NOT_COMPATIBLE; }
  }
  else if (fp->is_float())
  {
    if (ap->is_float()) 
    { return EXACT_MATCH; }
    else if (ap->is_double() || ap->is_int32() || ap->is_uint32()) 
    { return CONVERSION_MATCH; }
    else { return NOT_COMPATIBLE; }
  }
  else if (fp->is_double())
  {
    if (ap->is_double()) 
    { return EXACT_MATCH; }
    else if (ap->is_float() || ap->is_int32() || ap->is_uint32()) 
    { return CONVERSION_MATCH; }
    else { return NOT_COMPATIBLE; }
  }
  else if (fp->is_charstar() || fp->is_string())
  {
    if (ap->is_charstar() || ap->is_string()) 
    { return EXACT_MATCH; }
    else { return NOT_COMPATIBLE; }
  }
  else if (fp->is_pointer())
  {
    if (ap->is_pointer())
    {
      if (strcmp(fp->getTypeString(), ap->getTypeString()) == 0)
      { return EXACT_MATCH; }
      else
      {
        if (is_compatible_class(fp->getTypeString(), ap->getTypeString(), cliquery))
        { return CONVERSION_MATCH; }
        else
        { return NOT_COMPATIBLE; }
      }
    }
    else if (ap->is_array())
    {
      if (strcmp(fp->getTypeString(), ap->getTypeString()) == 0)
      { return EXACT_MATCH; }
      else
      { return NOT_COMPATIBLE; }
    }
    else
    { return NOT_COMPATIBLE; }
  }

  // Should have returned by now.
  throw InternalError(__FILE__, __LINE__); 
  return NOT_COMPATIBLE;
}

MethodTable::NoSuchClassException::NoSuchClassException
(string classname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Class Exception";
  errormessage << classname + ".";
}

MethodTable::NoConstructorException::NoConstructorException
(string classname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Constructor Exception";
  errormessage  << "The class \"" + classname + "\" does not have";
  errormessage  << "an accessible constructor";
}

MethodTable::ReturnTypeException::ReturnTypeException
(string methodargstring, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Return Type Exception";
  errormessage << "The return type of |" << methodargstring;
  errormessage << "| is not appropriate in this context.";
}

MethodTable::NoSuchMethodException::NoSuchMethodException
(string classname, string methodname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Method Exception";
  errormessage << "There is no method called " << methodname;
  errormessage << " callable in the class " << classname << ".";
}

MethodTable::ArgumentMismatchException::ArgumentMismatchException
(string methodname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Argument Mismatch Exception";
  errormessage << "Arguments for " << methodname;
  errormessage << " do not match an accessible method.";
}

MethodTable::AmbiguousCallException::AmbiguousCallException
(string methodname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Ambiguous Call Exception";
  errormessage << "Call to " << methodname << " is ambiguous and can apply ";
  errormessage << "to more than one method.";
}

bool MethodTable::isValidClassName
(const char * classname, bool cliquery)
{
  if (getClassInfo(classname, cliquery) == NULL)
  { return false; }
  else
  { return true; }
}

void MethodTable::invoke_constructor
(const char * class_name, CliArgList *args, AData *return_value, bool cliinvocation)
{
  _class_info * classinfo = getClassInfo(class_name, cliinvocation);
  
  // Complain if there is no such class.
  if (classinfo == NULL) { throw NoSuchClassException(class_name, __FILE__, __LINE__); }

  // Cli has had problems passing null for this.
  if (args == NULL || args == 0) { throw InternalError(__FILE__, __LINE__);}
  
  // Get the list of methods for the class
  _method_info *mi = classinfo->methods;
  int num_methods = classinfo->num_methods;
  
  vector<_method_info*> possibles;
  
  // Search for constructors and rate how their formal param match actuals
  for (int mindex = 0; mindex < num_methods; mindex++)
  {
    // Constructors have the same name as the class.
    if(strcmp(mi[mindex].method_name, class_name) == 0)
    {
      if (!cliinvocation || (mi[mindex].cli_invokable) || !restrictcli)
      { possibles.push_back(&(mi[mindex])); }
    }
  }

  // If there are no constructors, throw an exception.
  if (possibles.size() == 0) 
  { throw NoConstructorException(class_name, __FILE__, __LINE__); }
  
  // Choose the best method by comparing argument lists of possible constructors
  // to the list of actual parameters.
  _method_info * bestconstr = 
      choose_best_method(possibles, class_name, args, cliinvocation);
  
  // Sometimes the return value must match a certain type, (e.g. with 
  // assignments to global variabes.
  if (!(is_compatible_return(return_value, bestconstr->ret, cliinvocation)))
  { throw ReturnTypeException(get_method_arg_string(*bestconstr), __FILE__, __LINE__); }
      
  // Coerce arguments as necessary.
  CliArgList * coercedargs = coerce_arguments(bestconstr->alist, args);

  try 
  {  
    // Call the constructor.
    Construct(bestconstr->arg_signature, coercedargs, return_value);
  }
  catch (...)
  {
    // Delete the argument list we created even if call fails.
    delete coercedargs;
    throw;
  }

  delete coercedargs;
}

void MethodTable::invoke_method
(const char *class_name, const char * method_name, CliArgList *args, 
 AData *return_value, void *object, bool cliinvocation, bool staticmethod)
{
  const char * derived_class_name = class_name;

  // Cli has had problems passing null for this.
  if (args == NULL || args == 0) { throw InternalError(__FILE__, __LINE__);}
  
  // Cli has had problems passing null for this too.
  if (class_name == 0) { throw InternalError(__FILE__, __LINE__);}
  
  // Check that the class exists.
  _class_info * classinfo = getClassInfo(class_name, cliinvocation);
  if (classinfo == NULL) { throw NoSuchClassException(class_name, __FILE__, __LINE__); }

  // Loops upwards in the class hierarchy.
  while (class_name != NULL)
  {
    // Lookup the details of the class we have reached in the hierarchy.
    classinfo = getClassInfo(class_name, cliinvocation);
    if (classinfo == NULL) { throw InternalError(__FILE__,__LINE__); }

    _method_info *mi = classinfo->methods;
    int num_methods = classinfo->num_methods;
    vector<_method_info*> possibles;
    
    for (int mindex = 0; mindex < num_methods; mindex++)
    {
      // Ignore this method if it is a constructor for the current class.
      if (strcmp(mi[mindex].method_name, class_name) == 0)
      { continue; }
      
      // Ignore this method if it is not invokable.
      if (cliinvocation && !(mi[mindex].cli_invokable) && restrictcli)
      { continue; }
     
      // If this is a static method call, ignore non static methods.
      if (staticmethod && !(mi[mindex].staticmethod))
      { continue; }
      
      // If the method names match, and method is invokable, 
      // add to list of possibles.
      if((strcmp(mi[mindex].method_name, method_name) == 0))
      { possibles.push_back(&(mi[mindex])); }
    }
    
    if (possibles.size() > 0)
    {
      // Choose the best method by comparing argument lists of possible 
      // methods to the list of actual parameters.
      _method_info * bestmethod = 
        choose_best_method(possibles, method_name, args, cliinvocation);
    
      // Sometimes the return value must match a certain type, (e.g. with 
      // assignments to global variabes.
      if (!(is_compatible_return(return_value, bestmethod->ret, cliinvocation)))
      { throw ReturnTypeException(get_method_arg_string(*bestmethod), __FILE__, __LINE__); }
      
      // Coerce arguments as necessary.
      CliArgList * coercedargs = coerce_arguments(bestmethod->alist, args);
      
      try 
      {
        // Call the method.
        if (staticmethod)
	{ InvokeStatic(bestmethod->arg_signature, coercedargs, return_value); }
	else
	{ Invoke(object, bestmethod->arg_signature, coercedargs, return_value);}
	return;
      }
      catch (...)
      {
        delete coercedargs;
	throw;
      }
      
      delete coercedargs;
    }
    else
    {
      // Move up the class hierarchy.
      class_name = classinfo->super_class;
    }
  }

  throw NoSuchMethodException(derived_class_name, method_name, __FILE__, __LINE__);
}

void MethodTable::invoke_static
(const char *class_name, const char * method_name, CliArgList *args, 
 AData *return_value, bool cliinvocation)
{
  invoke_method(class_name, method_name, args, return_value, 0, cliinvocation, true);
}

MethodTable::_method_info * MethodTable::choose_best_method
(vector<_method_info*> & possibles, string method_name, 
 CliArgList *args, bool cliquery)
{
  bool tie                  = false;
  int  bestcompatibility    = NOT_COMPATIBLE;
  _method_info * bestmethod = 0;
  
  for (int methodindex = 0; methodindex < (int)possibles.size(); methodindex++)
  {
    // If the number of arguments isn't the same, don't bother with comparisons.
    if (possibles[methodindex]->alist->num_args() != args->num_args())
    { continue; }
    
    CliArgList * formalargs = possibles[methodindex]->alist;
    formalargs->reset();
    args->reset();

    AData * ap = args->next();
    AData * fp = formalargs->next();
  
    int methodcompatibility = EXACT_MATCH;
    
    while (ap && fp)
    {
      int compatibility = get_compatibility_level(fp, ap, cliquery);
      
      // Don't bother looking at other arguments if this one isn't compatible.
      if (compatibility == NOT_COMPATIBLE)
      {
        methodcompatibility = NOT_COMPATIBLE;  
	break;
      }
      
      // Don't look at other args if already have a worse level of compatibility
      if (compatibility > bestcompatibility) 
      { 
        methodcompatibility = compatibility;
        break; 
      }
      
      if (methodcompatibility < compatibility)
      {
        // Current compatibility level for this method should be downgraded.
        methodcompatibility = compatibility;
      }
      
      // Advance to next pair of arguments.
      fp = formalargs->next();
      ap = args->next();
    }

    // Compare compatibility of this method to previous best.
    if (methodcompatibility < bestcompatibility)
    {
      bestcompatibility = methodcompatibility;
      bestmethod        = possibles[methodindex];
      tie               = false;
    }
    else if (methodcompatibility == bestcompatibility)
    {
      tie = true;
    }
  }
  
  if (bestcompatibility == NOT_COMPATIBLE)
  { throw ArgumentMismatchException(method_name, __FILE__, __LINE__); }
  else if (tie) 
  { throw AmbiguousCallException(method_name, __FILE__, __LINE__); }

  return bestmethod;
}

CliArgList * MethodTable::coerce_arguments
(CliArgList * formal_params, CliArgList * actual_params)
{
  CliArgList * newarglist = new CliArgList();  

  actual_params->reset();
  formal_params->reset();

  AData * fp = formal_params->next();
  AData * ap = actual_params->next();

  while (ap && fp)
  {
    AData * coerced = coerce_argument(fp, ap);
    newarglist->add_element(coerced);
    
    fp = formal_params->next();
    ap = actual_params->next();
  }

  // Argument lists not the same length.
  if (ap != NULL || fp != NULL) { throw InternalError(__FILE__, __LINE__); }

  actual_params->reset();
  formal_params->reset();

  return newarglist;
}

AData * MethodTable::coerce_argument
(const AData * formal_arg, const AData * actual_param)
{
  // Copy parameter.
  AData * coerced = new AData();
  coerced->copy(actual_param);

  // Coerce arguments needing coercing.
  if (formal_arg->is_bool())
  {
    if (actual_param->is_int32())
    { coerced->setType(AData::bool_TYPE); coerced->set_bool(actual_param->get_int32());}
    else if (actual_param->is_uint32())
    { coerced->setType(AData::bool_TYPE); coerced->set_bool(actual_param->get_uint32());}
  }
  else if (formal_arg->is_int32())
  {
    if (actual_param->is_uint32())
    { coerced->setType(AData::int32_TYPE); coerced->set_int32(actual_param->get_uint32());}
  }
  else if (formal_arg->is_float())
  {
    if (actual_param->is_int32())
    { coerced->setType(AData::float_TYPE); coerced->set_float(actual_param->get_int32());}
    else if (actual_param->is_uint32())
    { coerced->setType(AData::float_TYPE); coerced->set_float(actual_param->get_uint32());}
    else if (actual_param->is_double())
    { coerced->setType(AData::float_TYPE); coerced->set_float(actual_param->get_double());}
  }
  else if (formal_arg->is_double())
  {
    if (actual_param->is_int32())
    { coerced->setType(AData::double_TYPE); coerced->set_double(actual_param->get_int32());}
    else if (actual_param->is_uint32())
    { coerced->setType(AData::double_TYPE); coerced->set_double(actual_param->get_uint32());}
    else if (actual_param->is_float())
    { coerced->setType(AData::double_TYPE); coerced->set_double(actual_param->get_float());}
  }
  else if (formal_arg->is_pointer())
  {
    if (actual_param->is_array())
    { coerced->setType(AData::pointer_TYPE); coerced->set_pointer(actual_param->get_array());}
  }

  return coerced;
}


MethodTable::_inheritance_tree::~_inheritance_tree()
{
  for (map<string, _itnode*>::iterator it = root.all.begin();
       it != root.all.end(); it++) delete it->second;
}

void MethodTable::_inheritance_tree::add_to_all(_itnode *me, _itnode *p)
{
				// Add me to the current node's "all" list
  p->all[me->name] = me;
				// Add to the parent's list and so on . . .
  if (p->parent) add_to_all(me, p->parent);
    
}


struct _it_element 
{
  string name;
  string base;
  bool   cli;
};

MethodTable::_inheritance_tree::
_inheritance_tree(_class_info *classes, int num_classes)
{
  list<_it_element> derived;

  // Copy the class list info to a simple structure
  for (int n=0; n<num_classes; n++) {
    _it_element e;
				// Class name
    e.name = string(classes[n].class_name);
				// Super class name
    if (classes[n].super_class) e.base = string(classes[n].super_class);
				// True if accessible from the CLI
    e.cli  = classes[n].cli_class;
				// Save the info
    derived.push_back(e);
  }

  list<_it_element>::iterator it, kt;
  map<string, _itnode*>::iterator jt;
  _itnode* cur;
  _itnode* tmp;

  // Infinite loop safety check
  const unsigned max_depth = 100;
  unsigned cnt=0;

  // Make the tree; the number of passes should be the depth of the
  // tree at most
  while (derived.size() || cnt>max_depth) {

    for (it=derived.begin(); it!=derived.end();) {
      
      // Set "not found" to start
      cur = 0;

      // If no super class, add it to the root
      if (it->base.size()==0) 
	cur = &root;		// Cur now points to the root

      // If super class is registered, add it to the superclass list
      if ( (jt=root.all.find(it->base))!=root.all.end() ) 
	cur = jt->second;	// Cur now points to the super class node

      // Add it, if we have found the correct location
      if (cur) {
				// Make a new node
	tmp = new _itnode(it->name, cur, it->cli);
				// Add it to the child list
	cur->next[it->name] = tmp;
				// Add it to the tree
	add_to_all(tmp, cur);
				// Delete the current element from 
	kt = it++;		// the unassigned list 
	derived.erase(kt);
      } 
      else it++;
    }

    cnt++;			// Increment depth counter
  }

}


vector<string> MethodTable::_inheritance_tree::
get_derived(string name, bool cli)
{
  vector<string> ret;
  map<string, _itnode*>::iterator it = root.all.find(name);

  if (it != root.all.end()) {
    for (map<string, _itnode*>::iterator 
	   jt=it->second->all.begin(); jt!=it->second->all.end(); jt++) 
      {
	if (!cli || jt->second->cli_cls) ret.push_back(jt->first);
      }
  }
  
  return ret;
};
  
void MethodTable::_inheritance_tree::print_graph(_itnode *p)
{
				// Start at root
  if (p==0) p = &root;

  for(map<string, _itnode*>::iterator it = p->next.begin();
      it != p->next.end(); it++) 
    {
      cout << setw(8*it->second->level+1) << " " << it->first;

      if (!it->second->cli_cls) cout << " [*]" << endl;
      else cout << endl;

      if (it->second->next.size()) print_graph(it->second);
    }
}
  
vector<string> MethodTable::get_derived(string root, bool cli_only) 
{
  return the_tree->get_derived(root, cli_only); 
}


void MethodTable::print_graph()
{ 
  the_tree->print_graph(); 
}

