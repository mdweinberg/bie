#include <PrintStatement.h>
#include <CliArgList.h>
#include <AData.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(PrintStatement)

using namespace BIE;

PrintStatement::PrintStatement(Expr *rhs) {
  rhs_ = rhs;
}

PrintStatement::~PrintStatement() {
  delete rhs_;
}

void PrintStatement::process(CliArgList *loop_vars)
{
  ostream outputstream(checkTable("cli_console"));
  
  AData * expr_val = new AData();  
  rhs_->eval(loop_vars, expr_val);  
  expr_val->print_value(outputstream);
}
