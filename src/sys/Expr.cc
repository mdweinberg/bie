#include <Expr.h>
#include <BIEException.h>
#include <common.h>
#include <cstdlib>
#include <CliArgList.h>
#include <AData.h>

BIE_CLASS_EXPORT_IMPLEMENT(Expr)

using namespace BIE;

extern void size_value(char *, AData *);

CliArgList *Expr::substitute_vars(CliArgList *loop_vars, CliArgList *args) 
{
  if (!args)
    return NULL;

  args->reset();
  
  CliArgList *new_args = new CliArgList;
  AData *a=NULL, *a_new=NULL;

  while ( (a = args->next()) ) 
  {
    if (a->is_variable() || (a->is_variablearrayindex()))
    {
      a_new = substitute_variable(loop_vars, a);
    }
    else if (a->is_arrayelement() || a->is_arrayelementptr())
    {
      a_new = substitute_arrayelement(loop_vars, a);
    }
    else
    {
      a_new = new AData;
      a_new->copy(a);
    }

    new_args->add_element(a_new);
    a_new = NULL;
  }
  
  args->reset();
  return new_args;
}

AData * Expr::substitute_arrayelement(CliArgList *loop_vars, AData * var)
{ throw InternalError(__FILE__,__LINE__); return 0; }

AData * Expr::substitute_variable(CliArgList *loop_vars, AData * var)
{
  char * variablename = 0;
 
  if (var->is_variable())
  { variablename = (char*)var->get_variable(); }
  else if (var->is_variablearrayindex())
  { variablename = (char*)var->get_variablearrayindex();}

  AData * substituted = new AData();
  
  bool foundit = false;
  
  // look up the global variable names
  foundit = GlobalTable::get_variable(variablename, substituted);
  
  // Look up the symbol table.
  if (!foundit)
  { foundit = st->get_symbol_value_copy(variablename, substituted); }

  // Look up the loop variables.
  if (!foundit)
  {
    if (loop_vars){
      AData *l;
      while( (l = loop_vars->next()) ){
        if (strcmp(l->getForVariable(), variablename) == 0) {
	  substituted->copy(l);
	  foundit = true;
        }
      }
      loop_vars->reset();
    }
  }

  if (!foundit)
  { throw VarNotExistException(variablename, __FILE__, __LINE__); }

  // Where variables was used as array index or size, make the type 
  // specifically the arrayindex type..
  if (var->is_variablearrayindex())
  {
    uint32 index;
    if (substituted->is_int32())
    { index = (uint32) substituted->get_int32(); }
    else if (substituted->is_uint32())
    { index = (uint32) substituted->get_uint32(); }
    else
    { throw EvalExprException(__FILE__,__LINE__); }
    
    substituted->setType(AData::arrayindex_TYPE);
    substituted->set_arrayindex(index);
  }
  
  return substituted;
}
