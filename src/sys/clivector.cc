#include <Serializable.h>
#include <Tessellation.h>
#include <Distribution.h>
#include <Simulation.h>
#include <LikelihoodComputation.h>
#include <ReversibleJumpTwoModel.h>

#include <clivector.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::clivectord)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::clivectori)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::clivectors)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::clivectorsim)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::clivectortess)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::clivectordist)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::clivectorRJmap)

// Hopefully, force instantiation.  Seems to be a necessary workaround
// for some versions of the C++ compiler and linker. Would be nice to
// remove the following stuff ASAP.

class DummyCLIVECTOR
{
public:
  DummyCLIVECTOR() { asm(""); }
};


static DummyCLIVECTOR dummy;

