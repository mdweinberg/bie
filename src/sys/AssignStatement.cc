#include <AssignStatement.h>
#include <common.h>
#include <gfunction.h>
#include <BIEException.h>
#include <SimpleExpr.h>
#include <AData.h>

BIE_CLASS_EXPORT_IMPLEMENT(AssignStatement)

using namespace BIE;

AssignStatement::AssignStatement(const char * variablename, Expr *rhs)
{
  this->variablename = variablename;
  this->rhs      = rhs;
}

AssignStatement::~AssignStatement()
{
  delete rhs;
}

void AssignStatement::process(CliArgList *loop_vars)
{
  AData *expr_val   = new AData();  // This stores the evaluated value of the rhs

  // Evaluate the right hand side.
  bool isglobal = GlobalTable::get_variable(variablename.c_str(), expr_val);
  rhs->eval(loop_vars, expr_val);
  
  if (isglobal)
  {
    // Assign the new value to the global variable.
    GlobalTable::set_variable(variablename.c_str(), expr_val); 
  }
  else
  {
    // Insert the symbol and value into symbol table, overwriting
    // any existing entry.
    st->add_symbol(variablename.c_str(), expr_val);
  }

  delete expr_val;
}
