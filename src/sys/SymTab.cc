#include <SymTab.h>
#include <string>
#include <iomanip>

#include <gfunction.h>
#include <common.h>
#include <BIEException.h>
#include <AData.h>

// For clivector classes
#include <Tessellation.h>
#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(SymTab)

using namespace BIE;
using namespace std;

SymTab::SymTab() {
  inc_entries = 20;
}

int SymTab::add_symbol(const string symbol, AData *data) {
  AData* copy = new AData();
  copy->copy(data);
  table[symbol] = copy;
  return 0;
}

vector<string> SymTab::get_symbols_by_class(const string classname, 
                                            bool includesubclasses) const {
  vector<string> matchingsymbols;
  table_t::const_iterator itr;

  for(itr = table.begin(); itr != table.end(); itr++) {
    std::pair<const string, AData*> entry = *itr;
    // Only consider symbols that are pointers, or "other" types.
    if (entry.second->is_pointer()) {
      const char* symboltype = entry.second->getTypeString();

      // Look for an exact match of class labels.
      if(strcmp(symboltype, classname.c_str()) == 0) {
        matchingsymbols.push_back(string(entry.first));
      } else {
      // Try matching the classname with a point in the class hierarchy.
        const char* superclass = MethodTable::get_super_classname(symboltype, true);
        
        while (superclass != NULL) {
          if(strcmp(superclass, classname.c_str()) == 0) {
            matchingsymbols.push_back(string(entry.first));
            break;
          }
          superclass = MethodTable::get_super_classname(superclass, true);
        }
      }
    }
  }
  
  return matchingsymbols;
}

vector<string> SymTab::get_all_symbols() const {
  vector<string> symbollist;
  table_t::const_iterator itr;

  for (itr = table.begin(); itr != table.end(); itr++) {
    // dereferencing itr gives you an obj of type std::pair<string,AData*>
    symbollist.push_back((*itr).first);
  }

  return symbollist;
}

AData* SymTab::get_symbol_value(const string symbol) const {
  table_t::const_iterator itr = table.find(symbol);
  if (itr == table.end()) {
    return NULL;
  } else {
    return (*itr).second;
  }
}

bool SymTab::get_symbol_value_copy(const string symbol, AData *return_value) const {
  AData* finder = get_symbol_value(symbol);
  if (finder == NULL) {
    return false;
  } else {
    return_value->copy(finder);
    return true;
  }
}

int SymTab::print_value(ostream & outputstream, const string symbol) const {
  AData* finder = get_symbol_value(symbol);
  if (finder == NULL) {
    finder->print_value(outputstream);
    return true;
  } else {
    return false;
  }
}

void SymTab::print_all_values(ostream & outputstream) const {
  table_t::const_iterator itr;
  for(itr = table.begin(); itr != table.end(); itr++) {
    cout << (*itr).first << "\t ";
    (*itr).second->print_value(outputstream);
  }
}
