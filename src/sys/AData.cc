#include <AData.h>
#include <iomanip>
using namespace std;

#include <pointer.h>
#include <gfunction.h>
#include <CliArgList.h>
#include <MethodTable.h>
// Needed for clivector classes
#include <Distribution.h>
#include <Tessellation.h>

BIE_CLASS_EXPORT_IMPLEMENT(AData)

using namespace BIE;

BIEpointer size_value(const char *name);

/*******************************************************************************
* Persistence code.  
*
* The persistence system saves the AData objects in the symbol table
* and in the global table.
*******************************************************************************/

AData::AData( int32 typecode )
{
  this->type         = typecode;
  this->type_str     = NULL;
  this->next         = NULL;
  bzero((void*) &(this->value), sizeof(this->value));
  this->for_variable = 0;
  this->arraylength  = 0;
}

AData::AData(void * pointer, const char * typestring)
{
  this->type         = pointer_TYPE;
  set_pointer(pointer);
  this->type_str     = NULL;
  setTypeString(typestring);
  this->next         = NULL;
  this->for_variable = 0;
  this->arraylength  = 0;
}

AData::AData() 
{ 
  this->type         = unset_TYPE;
  this->type_str     = NULL; 
  this->next         = NULL; 
  bzero((void*) &value, sizeof(value));
  this->for_variable = 0;
  this->arraylength  = 0;
}

/// Destructor.  Reclaims space allocated for type names (pointer data) or
/// for character strings.
AData::~AData()
{
  if (type == string_TYPE && value.string_value != 0)
  { delete [] value.string_value; }
  
  if (type == charstar_TYPE && value.charstar_value != 0)
  { delete [] value.charstar_value; }

  if (type == variable_TYPE && value.variable_value != 0)
  { delete [] value.variable_value; }

  if (type_str != 0)
  { delete [] type_str; }
}



// Use this MACRO to implement get and set routines for 
// types that copy properly through assignment.  Where types require 
// manual intervention to copy, hand code it.  String/char * is common
// enough to be treated to a MACRO too - see below.
#define IMPLEMENT_GET_SET_IS(typename,prefix)           \
  typename AData::get_ ## prefix() const {              \
    if (type != prefix ## _TYPE)                        \
    { throw InternalError(__FILE__,__LINE__); }         \
    return value.prefix ## _value;                      \
  }                                                     \
  void AData::set_ ## prefix(typename newvalue) {       \
    if (type != prefix ## _TYPE)                        \
    { throw InternalError(__FILE__,__LINE__); }         \
    value.prefix ## _value = newvalue;                  \
  }                                                     \
  bool AData::is_ ## prefix() const  {                  \
   return (type == prefix ## _TYPE);                    \
  }
  
IMPLEMENT_GET_SET_IS(bool,bool)  
IMPLEMENT_GET_SET_IS(int32,int32)  
IMPLEMENT_GET_SET_IS(uint32,uint32)  
IMPLEMENT_GET_SET_IS(float,float)  
IMPLEMENT_GET_SET_IS(double,double)  
IMPLEMENT_GET_SET_IS(void*,array)
IMPLEMENT_GET_SET_IS(void*,pointer)
IMPLEMENT_GET_SET_IS(CliArgList*,arrayelement)
IMPLEMENT_GET_SET_IS(CliArgList*,arrayelementptr)
IMPLEMENT_GET_SET_IS(uint32,arrayindex)

#undef IMPLEMENT_GET_SET_IS 

#define IMPLEMENT_GET_SET_IS_STRING(prefix)           \
  const char * AData::get_ ## prefix() const {        \
    if (type != prefix ## _TYPE)                      \
    { throw InternalError(__FILE__,__LINE__); }       \
    return value.prefix ## _value;                    \
  }                                                   \
  void AData::set_ ## prefix(const char * newvalue) { \
    if (type != prefix ## _TYPE)                      \
    { throw InternalError(__FILE__,__LINE__); }       \
    if (value.prefix ## _value != 0)                  \
    { delete [] value.prefix ## _value; }             \
    value.prefix ## _value = stralloc(newvalue);      \
  }                                                   \
  bool AData::is_ ## prefix() const                   \
  { return (type == prefix ## _TYPE); }

IMPLEMENT_GET_SET_IS_STRING(charstar)  
IMPLEMENT_GET_SET_IS_STRING(variable)  
IMPLEMENT_GET_SET_IS_STRING(variablearrayindex)
  
#undef IMPLEMENT_GET_SET_IS_STRING 

const char * AData::get_string() const
{
  if (type != string_TYPE)
  { throw InternalError(__FILE__,__LINE__); }
  return value.string_value;
}

void AData::set_string(string str)
{
  if (type != string_TYPE)
  { throw InternalError(__FILE__,__LINE__); }

  if (value.string_value != 0)
  { delete [] value.string_value; }

  value.string_value = stralloc(str.c_str());
}

bool AData::is_string() const
{ return (type == string_TYPE); }

void AData::print_value(ostream & outputstream) const
{
  
  switch (type) 
  {
    case bool_TYPE:
      outputstream << "Boolean: ";
      if (value.bool_value) 
      { outputstream << "true" << endl; } 
      else 
      { outputstream <<"false" << endl;}
      break;
    case int32_TYPE:
      outputstream << "32 bit signed integer: ";
      outputstream << value.int32_value << endl;
      break;
    case uint32_TYPE:
      outputstream << "32 bit unsigned integer: ";
      outputstream << value.uint32_value << endl;
      break;
    case float_TYPE:
      outputstream << "Single precision floating point: ";
      outputstream << value.float_value << endl;
      break;
    case double_TYPE:
      outputstream << "Double precision floating point: ";
      outputstream << value.double_value << endl;
      break;
    case charstar_TYPE:
      outputstream << "Character Array: \"";
      outputstream << value.charstar_value << "\"" << endl;
      break;
    case string_TYPE:
      outputstream << "String: " << "\"";
      outputstream << value.string_value << "\"" << endl;
      break;
    case array_TYPE:
      outputstream << "Array of : " << type_str << " " << arraylength;
      outputstream << "long." << endl;
      break;
    case pointer_TYPE:
      outputstream << type_str << " Object: ";
      
      if (printaddressesincli) { outputstream << value.pointer_value; }
      outputstream << endl;

      try {
        CliArgList emptyarglist;
	AData returnvalue;

        if (value.pointer_value) { 
	  MethodTable::invoke_method(type_str, "toString", &emptyarglist,
				     &returnvalue, value.pointer_value, true);
        }
	
        outputstream << returnvalue.value.string_value << endl;
      }
      catch (...) {}
      break;
    default:
      outputstream << "Error in AData::print_value() - unhandled type code." << type << endl;
      break;
  }
}


#define COPY_VALUE_TO_COPY(typename) \
 case typename ## _TYPE: value.typename ## _value = orig_data->value.typename ## _value; break ;
void AData::copy(const AData *orig_data) 
{
  type        = orig_data->type;
  next        = 0;
  type_str    = 0;
  arraylength = orig_data->arraylength;
  
  switch (type) {
    COPY_VALUE_TO_COPY(bool)
    COPY_VALUE_TO_COPY(uint32)
    COPY_VALUE_TO_COPY(int32)
    COPY_VALUE_TO_COPY(float)
    COPY_VALUE_TO_COPY(double)
    COPY_VALUE_TO_COPY(arrayindex)
    case charstar_TYPE:
      if (value.charstar_value) { delete [] value.charstar_value; }
      value.charstar_value = stralloc(orig_data->value.charstar_value);
      break;
    case string_TYPE:
      if (value.string_value) { delete [] value.string_value; }
      value.string_value = stralloc(orig_data->value.string_value);
      break;
    case pointer_TYPE:
      {
        value.pointer_value = orig_data->value.pointer_value;
        setTypeString(orig_data->type_str);
      }
      break;
    case array_TYPE:
      {
        value.array_value = orig_data->value.array_value;
        setTypeString(orig_data->type_str);
      }
      break;
    default:
      throw InternalError(__FILE__, __LINE__); 
  }
}

#undef COPY_VALUE_TO_COPY

#define COPY_VALUE_TO_MEMORY(typename) \
case typename ## _TYPE: *((typename*)address) = value.typename ## _value; break;

void AData::copy_value_to_memory (void *address)
{
  switch (type) {
    COPY_VALUE_TO_MEMORY(bool)
    COPY_VALUE_TO_MEMORY(uint32)
    COPY_VALUE_TO_MEMORY(int32)
    COPY_VALUE_TO_MEMORY(float)
    COPY_VALUE_TO_MEMORY(double)
    COPY_VALUE_TO_MEMORY(string)
    case charstar_TYPE:
      {
        // this wastes memory, but we can't call delete without being sure
        // the memory was allocated statically.  
	unsigned int length   = strlen(value.charstar_value);
	*((char**)address) = new char[length + 1];
        strncpy(*((char**)address), value.charstar_value, length + 1);
      }
      break;
    case pointer_TYPE:
      *((void**)address) = value.pointer_value; 
      break;

    case array_TYPE: // Not implemented, I don't think it needs to be - Alistair
    default:
      throw InternalError(__FILE__,__LINE__);
  } 
}

#undef COPY_VALUE_TO_MEMORY

#define COPY_MEMORY_TO_VALUE(typename) \
case typename ## _TYPE: value.typename ## _value = *((typename*)address); break;

void AData::copy_memory_to_value
(void * address)
{
  switch (type) {
    COPY_MEMORY_TO_VALUE(bool)
    COPY_MEMORY_TO_VALUE(uint32)
    COPY_MEMORY_TO_VALUE(int32)
    COPY_MEMORY_TO_VALUE(float)
    COPY_MEMORY_TO_VALUE(double)
    case string_TYPE:
      {
        string * str        = (string*) address;
	if (value.string_value) { delete [] value.string_value; }
	value.string_value = stralloc(str->c_str());
      }
      break;
    case charstar_TYPE:
      {
        char ** str        = (char**) address;
	if (value.charstar_value) { delete [] value.charstar_value; }
	value.charstar_value = stralloc(*str);
      }
      break;
    case pointer_TYPE:
      value.pointer_value = *((void**)address); 
      break;
    case array_TYPE: // Not implemented, I don't think it needs to be - Alistair
    default:
      throw InternalError(__FILE__,__LINE__); 
  }  
}

#undef COPY_MEMORY_TO_VALUE

string AData::get_type_name()
{
  switch (type) {
  case bool_TYPE:         return string("bool");
  case uint32_TYPE:       return string("unsigned int");
  case int32_TYPE:        return string("int");
  case float_TYPE:        return string("float");
  case double_TYPE:       return string("double");
  case string_TYPE:       return string("string");
  case charstar_TYPE:     return string("char*");
  case pointer_TYPE:      return string(type_str) + string("*");
  case arrayindex_TYPE:   return string("<array capable>");
  case array_TYPE:   
    { 
      ostringstream buffer; //aah change to stringstream from strstream
      buffer << type_str << "[" << arraylength << "]";
      //char * resultascharstar = buffer.str();
      string result = buffer.str();
      //delete [] resultascharstar;
      return result;
    }
    default:
      throw InternalError(__FILE__,__LINE__);
  }
  
  return "";
}
