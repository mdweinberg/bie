#include <cstring>

#include <CallExpr.h>
#include <common.h>
#include <gfunction.h>
#include <BIEException.h>
#include <CliArgList.h>
#include <AData.h>

// Needed for clivector classes
#include <Distribution.h>
#include <Tessellation.h>

BIE_CLASS_EXPORT_IMPLEMENT(CallExpr)

using namespace BIE;

CallExpr::CallExpr
(char *class_name, CliArgList *args)
{
  this->class_name  = class_name;
  this->object_name = "";
  this->method_name = "";
  this->args        = args;
  this->call_type   = CONSTRUCTOR_CALL;
}

CallExpr::CallExpr
(char *class_name, AData * arraysize)
{
  this->class_name  = class_name;
  this->object_name = "";
  this->method_name = "";
  this->args        = new CliArgList;
  (this->args)->add_element(arraysize);
  this->call_type   = CONSTRUCTOR_CALL;
}

CallExpr::CallExpr
(char *object_name, char *method_name, CliArgList *args) 
{
  this->class_name   = "";
  this->object_name  = object_name;
  this->method_name  = method_name;
  this->args         = args;
  this->call_type    = METHOD_CALL;
}

CallExpr::~CallExpr()
{
//   if (class_name) delete [] class_name;
//   if (method_name) delete [] method_name;
//   if (object_name) delete [] object_name;
  delete args;
}

void CallExpr::eval
(CliArgList *loop_vars, AData *return_value)
{
  CliArgList* new_args = 0;
  
  try {
    // Evaluate the arguments.
    new_args = substitute_vars(loop_vars, args);

    if (call_type == METHOD_CALL)
    {
      // Figure out the object on which the method is being invoked
      AData *instance = new AData;
      
      if (!GlobalTable::get_variable(object_name.c_str(), instance) && 
          !st->get_symbol_value_copy(object_name.c_str(), instance))
      {
        // The object name could not be found.
        delete instance;        
        throw (VarNotExistException(object_name, __FILE__,__LINE__));
      }
      
      if (! instance->is_pointer()) 
      { throw MethodCallOnNonObjectException(__FILE__,__LINE__); }
      
      void* object = instance->get_pointer();

      // Get the name of the class.
      class_name = instance->getTypeString();
  
      delete instance;

      // Call the method 
      MethodTable::invoke_method(class_name.c_str(), method_name.c_str(), 
				 new_args, return_value, object, true);
    }
    else if (call_type == CONSTRUCTOR_CALL)
    {
      // Call the constructor.  There is no object associated with a constructor
      // call, so this is null.
      MethodTable::invoke_constructor(class_name.c_str() ,new_args,return_value,true);
    }
  }
  catch (...)
  {
    // Clean up the memory allocated for the substituted arguments
    if (new_args) delete new_args;

    // Rethrow the exception.
    throw;
  }
  
  // Clean up the memory allocated for the substituted arguments
  if (new_args) delete new_args;
}
