#include <SimpleExpr.h>
#include <common.h>
#include <BIEException.h>
#include <AData.h>
// Needed for clivector classes
#include <Distribution.h>
#include <Tessellation.h>

BIE_CLASS_EXPORT_IMPLEMENT(SimpleExpr)

using namespace BIE;

SimpleExpr::SimpleExpr(AData* arg) {
  this->args = new CliArgList();
  (this->args)->add_element(arg);
}

SimpleExpr::~SimpleExpr() {
   delete args;
}

void SimpleExpr::eval(CliArgList *loop_vars, AData *return_value)
{
  CliArgList *new_args;
  
  // This evaluates the expression eg, turns variables into values,
  new_args = substitute_vars(loop_vars, args);
  
  new_args->reset();
  return_value->copy(new_args->next());
}
