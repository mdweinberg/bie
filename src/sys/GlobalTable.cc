#include <GlobalTable.h>
#include <iomanip>
#include <string>
using namespace std;
#include <MethodTable.h>
#include <gfunction.h>
#include <BIEException.h>
#include <AData.h>

using namespace BIE;

bool GlobalTable::get_variable
(const char * variable, AData *returnvalue)
{
  unsigned int entryindex = 0;
  
  // Simply scan through each of the entries in the table, and
  // copy the value of the global variable into the AData container.
  while (global_variables[entryindex].name != 0)
  {
    if (strcmp(global_variables[entryindex].name, variable) == 0)
    {
      MethodTable::setType(returnvalue,global_variables[entryindex].type);
      returnvalue->setTypeString(global_variables[entryindex].type_str);
      returnvalue->copy_memory_to_value(global_variables[entryindex].addr);
      return true;
    }

    entryindex++;
  }
  
  return false;
}  

void GlobalTable::set_variable
(const char * variable, AData *newvalue)
{
  unsigned int entryindex = 0;

  while (global_variables[entryindex].name != 0)
  {
    if (strcmp(global_variables[entryindex].name, variable) == 0)
    {
      AData variabletype;
      MethodTable::setType(&variabletype,global_variables[entryindex].type);
      variabletype.setTypeString(global_variables[entryindex].type_str);
      
      // The value being assigned to must be compatible.
      if (!(MethodTable::is_compatible_type(&variabletype, newvalue, true)))
      { throw TypeMisMatchException(__FILE__, __LINE__); }
      
      AData * coerced = MethodTable::coerce_argument(&variabletype, newvalue);
      
      coerced->copy_value_to_memory(global_variables[entryindex].addr);
      return;
    }
    entryindex++;
  }
  
  throw VarNotExistException(variable, __FILE__, __LINE__);
}
 
bool GlobalTable::print_value
(const char *variable, ostream & outputstream)
{
  AData value;
  bool variable_exists = get_variable(variable, &value);
  
  if (variable_exists)
  { 
    outputstream << variable << "\t ";
    value.print_value(outputstream); 
  }
  
  return variable_exists;
}

void GlobalTable::print_all_values(ostream & outputstream)
{
  vector<string> variables = get_all_variable_names();
  
  for (unsigned int i = 0; i < variables.size(); i++)
  { print_value(variables[i].c_str(), outputstream); }
}

vector<string> GlobalTable::get_all_variable_names()
{
  vector<string> list;

  unsigned int entryindex = 0;
  while (global_variables[entryindex].name){
    list.push_back(global_variables[entryindex].name);
    entryindex++;
  }

  return list;
}
 
