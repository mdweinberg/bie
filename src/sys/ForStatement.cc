#include <ForStatement.h>
#include <CliArgList.h>
#include <AData.h>
#include <common.h>

BIE_CLASS_EXPORT_IMPLEMENT(ForStatement)

ForStatement::ForStatement
(char *var_name, int low, int high, Statement *for_stmt)
{
  this->var_name = var_name;
  this->low      = low;
  this->high     = high;
  this->for_stmt = for_stmt;
}

ForStatement::~ForStatement()
{
  delete for_stmt;
}

void ForStatement::process(CliArgList *loop_vars)
{
  bool list_alloc=false;
  
  if (!loop_vars){
    loop_vars = new CliArgList;
    list_alloc = true;
  }

  if (low < high){
    AData *loop_var = new AData(AData::uint32_TYPE);
    
    loop_var->setForVariable(var_name.c_str());
    loop_vars->add_element(loop_var);

    for(int i=low; i <= high; i++){
      loop_var->set_uint32(low);
      for_stmt->process(loop_vars);
    }
  }
  
  if (list_alloc)
  { delete loop_vars; }
}
