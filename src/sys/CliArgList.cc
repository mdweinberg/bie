#include <cstdlib>

#include <AData.h>
#include <CliArgList.h>

// Needed for clivector classes
#include <Distribution.h>
#include <Tessellation.h>

BIE_CLASS_EXPORT_IMPLEMENT(CliArgList)

CliArgList::CliArgList()
{ 
  cursor = head = tail = NULL;
  num_args_ = 0;
}

void CliArgList::add_element(AData *dat)
{
  dat->setNext(NULL);
  
  if (!head)
  { head = cursor = tail = dat; }
  else
  {
    tail->setNext(dat);
    tail = dat;
  }
   
  num_args_++;
}

void CliArgList::append_list(CliArgList *list)
{
  tail->setNext(list->head);
  tail = list->tail;
  num_args_ += list->num_args_;
  list->head = list->tail = list->cursor = NULL;
  list->num_args_ = 0;
}

void CliArgList::remove_all_elements()
{
  AData *current = head,
        *next;
  while (current){
    next = current->getNext();
    delete current;
    current = next;
  }  

  head = tail = cursor = NULL;
  num_args_=0;
}

CliArgList::~CliArgList(){
  remove_all_elements();
}
    
AData *CliArgList::next(){
  if (!cursor)
    return NULL;

  AData *retval = cursor;
  cursor = cursor->getNext();
  return retval;
}

AData *CliArgList::inspect_next(){
  if (!cursor)
    return NULL;

  return cursor->getNext();
}

void CliArgList::reset(){
  cursor = head;
}

void CliArgList::print_elements(ostream & outputstream)
{
  AData *current = head;

  while (current){
    current->print_value(outputstream);
    current = current->getNext();
  }
}
