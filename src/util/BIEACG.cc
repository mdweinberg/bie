#include <fstream>
#include <gvariable.h>
#include <BIEACG.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BIEACG)

using namespace BIE;


unsigned BIEACG::inst_count = 0;

BIEACG::BIEACG() : ACG() 
{
  my_inst = inst_count++;
}
    
BIEACG::BIEACG(uint32_t seed, int size) : ACG(seed, size) 
{
  my_inst = inst_count++;
}

void BIEACG::send_state() const
{
  MPI_Send(const_cast<unsigned*>(&initialSeed), 1, MPI_UNSIGNED, 
	   0, 220,  MPI_COMM_WORLD);
  MPI_Send(const_cast<unsigned*>(&lcgRecurr),   1, MPI_UNSIGNED, 
	   0, 221,  MPI_COMM_WORLD);
  MPI_Send(const_cast<unsigned short*>(&stateSize), 1, MPI_UNSIGNED_SHORT, 
	   0, 222, MPI_COMM_WORLD);
  MPI_Send(const_cast<unsigned short*>(&auxSize),   1, MPI_UNSIGNED_SHORT, 
	   0, 223, MPI_COMM_WORLD);
  MPI_Send(const_cast<short*>(&j), 1, MPI_SHORT, 
	   0, 224, MPI_COMM_WORLD);
  MPI_Send(const_cast<short*>(&k), 1, MPI_SHORT, 
	   0, 225, MPI_COMM_WORLD);
  MPI_Send(const_cast<uint32_t*>(state), stateSize2, MPI_UNSIGNED, 
	   0, 226, MPI_COMM_WORLD);
}

void BIEACG::pre_clean() const
{
  *const_cast<vector<unsigned>*>(&seeds) = vector<unsigned>();

  *const_cast<vector<unsigned>*>(&lcgs)  = vector<unsigned>();

  *const_cast<vector<unsigned short>*>(&statesizes) = vector<unsigned short>();

  *const_cast<vector<unsigned short>*>(&auxsizes)   = vector<unsigned short>();

  *const_cast<vector<short>*>(&js) = vector<short>();

  *const_cast<vector<short>*>(&ks) = vector<short>();

  *const_cast<vector< vector<unsigned> >*>(&states) = vector< vector<unsigned> >();
}

void BIEACG::pack_state(const int n) const
{
  unsigned us;
  short unsigned sus;

  if (MPI_Recv(&us, 1, MPI_UNSIGNED, n, 220,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG save", "receive failure",
			 __FILE__, __LINE__);
    }

  const_cast<vector<unsigned>*>(&seeds)->push_back(us);

  if (MPI_Recv(&us, 1, MPI_UNSIGNED, n, 221,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG save", "receive failure",
			 __FILE__, __LINE__);
    }

  const_cast<vector<unsigned>*>(&lcgs)->push_back(us);

  if (MPI_Recv(&sus, 1, MPI_UNSIGNED_SHORT, n, 222,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG save", "receive failure",
			 __FILE__, __LINE__);
    }

  const_cast<vector<unsigned short>*>(&statesizes)->push_back(sus);

  if (MPI_Recv(&sus, 1, MPI_UNSIGNED_SHORT, n, 223,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG save", "receive failure",
			 __FILE__, __LINE__);
    }
  
  const_cast<vector<unsigned short>*>(&auxsizes)->push_back(sus);

  if (MPI_Recv(&sus, 1, MPI_SHORT, n, 224,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG save", "receive failure",
			 __FILE__, __LINE__);
    }

  const_cast<vector<short>*>(&js)->push_back(sus);


  if (MPI_Recv(&sus, 1, MPI_SHORT, n, 225,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG save", "receive failure",
			 __FILE__, __LINE__);
    }

  const_cast<vector<short>*>(&ks)->push_back(sus);


  unsigned ssiz = statesizes.back() + auxsizes.back();
  vector<unsigned> vstate(ssiz);

  if (MPI_Recv(&vstate[0], ssiz, MPI_UNSIGNED, n, 226,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG save", "receive failure",
			 __FILE__, __LINE__);
    }

  const_cast<vector< vector<unsigned> >*>(&states)->push_back(vstate);
}

void BIEACG::recv_state() const
{
  if (MPI_Recv(const_cast<uint32_t*>(&initialSeed), 1, MPI_UNSIGNED, 
	       0, 220, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG restore", "receive failure",
			 __FILE__, __LINE__);
    }

  if (MPI_Recv(const_cast<uint32_t*>(&lcgRecurr), 1, MPI_UNSIGNED, 
	       0, 221, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG restore", "receive failure",
			 __FILE__, __LINE__);
    }

  if (MPI_Recv(const_cast<short unsigned*>(&stateSize), 1, MPI_UNSIGNED_SHORT, 
	       0, 222,  MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG restore", "receive failure",
			 __FILE__, __LINE__);
    }

  if (MPI_Recv(const_cast<short unsigned*>(&auxSize), 1, MPI_UNSIGNED_SHORT, 
	       0, 223, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG restore", "receive failure",
			 __FILE__, __LINE__);
    }

  *const_cast<short unsigned*>(&stateSize2) = stateSize + auxSize;

  if (MPI_Recv(const_cast<short*>(&j), 1, MPI_SHORT, 
	       0, 224, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG restore", "receive failure",
			 __FILE__, __LINE__);
    }

  if (MPI_Recv(const_cast<short*>(&k), 1, MPI_SHORT, 
	       0, 225, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG restore", "receive failure",
			 __FILE__, __LINE__);
    }

  delete [] const_cast<uint32_t*>(state);

  // Yeah, this is some nasty stuff needed to cast away const . . .

  uint32_t **s = const_cast<uint32_t**>(&state);
  uint32_t **x = const_cast<uint32_t**>(&auxState);

  *s = new uint32_t[stateSize2];
  *x = &((*s)[stateSize]);

  if (MPI_Recv(*s, stateSize2, MPI_UNSIGNED, 
	       0, 226, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS)
    {
      throw BIEException("BIEACG restore", "receive failure",
			 __FILE__, __LINE__);
    }
}

void BIEACG::unpack_state(const int n) const
{
  MPI_Send(const_cast<unsigned int*>(&seeds[n-1]), 1, MPI_UNSIGNED, 
	   n, 220, MPI_COMM_WORLD);
  MPI_Send(const_cast<unsigned int*>(&lcgs[n-1]),  1, MPI_UNSIGNED, 
	   n, 221, MPI_COMM_WORLD);
  MPI_Send(const_cast<unsigned short*>(&statesizes[n-1]), 1, MPI_UNSIGNED_SHORT, 
	   n, 222, MPI_COMM_WORLD);
  MPI_Send(const_cast<unsigned short*>(&auxsizes[n-1]), 1, MPI_UNSIGNED_SHORT, 
	   n, 223, MPI_COMM_WORLD);
  MPI_Send(const_cast<short*>(&js[n-1]), 1, MPI_SHORT, 
	   n, 224, MPI_COMM_WORLD);
  MPI_Send(const_cast<short*>(&ks[n-1]), 1, MPI_SHORT, 
	   n, 225, MPI_COMM_WORLD);
  MPI_Send(const_cast<unsigned int*>(&states[n-1][0]),  
	   statesizes[n-1] + auxsizes[n-1], MPI_UNSIGNED, 
	   n, 226, MPI_COMM_WORLD);
}

void BIEACG::toXML(std::string suffix)
{
  std::ofstream out;

  // Prepare 'out' to throw if failbit gets set
  std::ios_base::iostate exceptionMask = out.exceptions() | std::ios::failbit;
  out.exceptions(exceptionMask);

  try {
    out.open(nametag + "." + suffix);
    boost::archive::xml_oarchive ar(out);
    ar << boost::serialization::make_nvp("pBIEgen", *BIEgen);
  }
  catch (std::ios_base::failure& e) {
    std::cerr << e.what() << '\n';
  }
}
