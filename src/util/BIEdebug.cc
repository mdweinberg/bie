#include <iostream>
#include <fstream>
#include <BIEdebug.h>
#include <gfunction.h>
#include <Tile.h>
#include <Frontier.h>
#include <Tessellation.h>

using namespace BIE;

void BIE::printFrontierTiles(Frontier * frontier)
{
  vector <int> frontier_value = frontier->ExportFrontier();
  vector <int>::iterator it;
  Tile* _tile;
  Tessellation * tess = frontier->getTessellation();
  
  ostream cout(checkTable("debug_ouput"));  
 
  cout << "---- Tiles in frontier = " << frontier_value.size() << "\n";
  for (it=frontier_value.begin();it!=frontier_value.end();it++) {
    _tile = tess->GetTile(*it);
    cout << *it
         << ": ("  << _tile->X(0.0, 0.0)
         << ", "   << _tile->Y(0.0, 0.0)
         << ")  (" << _tile->X(1.0, 1.0)
         << ", "   << _tile->Y(1.0, 1.0)
         << ")\n";
  }
  cout << "---- Done\n";
}

void BIE::printFrontierTiles(Frontier* frontier, ostream &out)
{
  vector <int> frontier_value = frontier->ExportFrontier();
  vector <int>::iterator it;
  Tessellation * tess = frontier->getTessellation();
  Tile* _tile;

  out << "---- Tiles in frontier = " << frontier_value.size() << "\n";
  for (it=frontier_value.begin();it!=frontier_value.end();it++) {
    _tile = tess->GetTile(*it);
    out << *it
         << ": ("  << _tile->X(0.0, 0.0)
         << ", "   << _tile->Y(0.0, 0.0)
         << ")  (" << _tile->X(1.0, 1.0)
         << ", "   << _tile->Y(1.0, 1.0)
         << ")\n";
  }
  out << "---- Done\n";
}

void BIE::printFrontierTilesSM(Frontier* frontier)
{
  vector <int> frontier_value = frontier->ExportFrontier();
  vector <int>::iterator it;
  Tessellation * tess = frontier->getTessellation();
  Tile* _tile;
  //ofstream out("plottile.sm");
  int icnt=1, itile=0;

  ostream cout(checkTable("plottile_ouput"));  

  cout << "plot0 #\n";

  for (it=frontier_value.begin();it!=frontier_value.end();it++) {

    if (itile > 10) {
      cout << "plot" << icnt << " #\n\0";
      itile = 0;
      icnt++;
    }

    _tile = tess->GetTile(*it);
    cout << "\trelocate " 
	 << _tile->X(0.0, 0.0) << " " << _tile->Y(0.0, 0.0) << "\n";
    cout << "\t\tdraw " 
	 << _tile->X(1.0, 0.0) << " " << _tile->Y(1.0, 0.0) << "\n";
    cout << "\t\tdraw " 
	 << _tile->X(1.0, 1.0) << " " << _tile->Y(1.0, 1.0) << "\n";
    cout << "\t\tdraw " 
	 << _tile->X(0.0, 1.0) << " " << _tile->Y(0.0, 1.0) << "\n";
    cout << "\t\tdraw " 
	 << _tile->X(0.0, 0.0) << " " << _tile->Y(0.0, 0.0) << "\n";
    itile++;
  }


  cout << "plotmaster #\n";
  for (int i=0; i<icnt; i++) {
    cout << "\tplot" << i << "\n";
  }
}

