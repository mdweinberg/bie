//=============================================================================
// ORBTreeClass
//=============================================================================

#include <values.h>

#include <map>
#include <cmath>
#include <cfloat>
#include <vector>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <utility>
#include <algorithm>

#include <BSPTree.h>
#include <ORBTree.h>


unsigned ORBTree::maxLevel    = 20;

//
// Root node
//
ORBNode::ORBNode(ORBTree *t)
{
  T = t;
  P = 0;
  L = R = 0;
  level = 0;
}

//
// Set the range from externally
//
void ORBNode::setBounds(const std::vector<double>& L,
			const std::vector<double>& U)
 {
  if (P) return;		// root node only
  
  lower = L;
  upper = U;
  
  Lower = L;
  Upper = U;
  
  makeVolume();
}

//
// Computes the box enclosing the data
// 
void ORBNode::makeBounds()
{
  if (P) return;		// root node only
  
  lower = std::vector<double>(T->dims,  DBL_MAX);
  upper = std::vector<double>(T->dims, -DBL_MAX);
  
  for (Pool::iterator j=pool.begin(); j!=pool.end(); j++) {
    for (size_t i=0; i<T->dims; i++) {
      lower[i] = std::min<double>(lower[i], (*j)->p[i]);
      upper[i] = std::max<double>(upper[i], (*j)->p[i]);
    }
  }
  
  Lower = lower;
  Upper = upper;
  
  makeVolume();
}

//
// Compute the root node volume
//
void ORBNode::makeVolume()
{
  // Same for volume: lower case "vol" is geometric and upper case
  // "Vol" breaks on point values
  vol   = 0.0;
  for (size_t j=0; j<T->dims; j++) vol += log(upper[j] - lower[j]);
  vol   = exp(vol);
  Vol   = vol;
}


//
// Child node contructor
//
ORBNode::ORBNode(ORBTree *t, ORBNode *p, Pool& data,
		 double closest, double divide, RL node)
{
  // No children
  //
  L = R = 0;
  
  // Assign the data
  //
  pool  = data;
  
  // Copy the limits from the parent
  //
  lower = p->lower;
  upper = p->upper;
  
  Lower = p->Lower;
  Upper = p->Upper;
  
  level = p->level+1;		// Increment the level
  
  unsigned d = p->level % t->dims;
  
  if (node==left) {		// Lower-valued segment
    upper[d] = closest;
    Upper[d] = divide;
  }
  
  if (node==right) {		// Upper-valued segment
    lower[d] = closest;
    Lower[d] = divide;
  }
  
  if (0 && pool.size()<1) {	// Sanity check
    std::cout << "whaaaa?" << std::endl;
  }
  
  Vol = vol = 0.0;
  for (size_t j=0; j<t->dims; j++) {
    vol += log(upper[j] - lower[j]);
    Vol += log(Upper[j] - Lower[j]);
  }
  vol = exp(vol);
  Vol = exp(Vol);
  
  // For detailed debugging only . . .
  //
  if (ORBTree::detailed_debug) {
    if (vol <= 0.0) {
      t->minMul++;
      t->extVol += Vol;
    }
    t->totMul++;
    t->minCnt = std::min<int>(t->minCnt, pool.size());
    t->minVol = std::min<double>(t->minVol, vol);
  }
}

//
// This destructor will recursively delete all of the children
//
ORBNode::~ORBNode()
{
  delete L;
  delete R;
}

//
// Main constructor (c-style arrays)
//
ORBTree::ORBTree(unsigned long d, unsigned nc,
		 const std::vector< std::vector<double> >& values_, 
		 const double* points_, const unsigned* mult_, 
		 const double* weight_, const double* beta_)
{
  // For debugging only
  //
  minMul        = 0;
  totMul        = 0;
  minCnt        = MAXINT;
  minVol        = MAXDOUBLE;
  extVol        = 0;
  
  // Assign configuration
  //
  dims          = d;
  vol_trim      = false;
  num_points    = values_.size();
  ncut          = std::max<unsigned>(1, nc);
  maxL          = maxLevel * dims;
  
  //
  // Make the root node
  //
  root = new ORBNode(this);
  
  //
  // Assign the points
  //
  std::vector<double> p(dims);
  for (indx i=0; i<num_points; i++) {
    for (unsigned j=0; j<d; j++) p[j] = points_[i*d+j];
    root->pool.push_back(ORBdPtr(new ORBdatum(values_[i], p, mult_[i], weight_[i], beta_[i])));
  }
  
  //
  // Check for manual or automatic bounds
  //
  if (lowerBound.size() == dims && upperBound.size() == dims) {
    root->setBounds(lowerBound, upperBound);
    
  } else {
    root->makeBounds();
  }
  
  //
  // Compute lengths per dimension
  //
  scale = std::vector<double>(dims);
  for (indx i=0; i<dims; i++) scale[i] = root->upper[i] - root->lower[i];
  
  //
  // Build the tree
  //
  if (num_points > 0) buildTree(root);
  
}

//
// Main constructor
//
ORBTree::ORBTree(unsigned long d, unsigned nc,
		 const std::vector< std::vector<double> >& values_, 
		 const std::vector< std::vector<double> >& points_, 
		 const std::vector<unsigned>& mult_,
		 const std::vector<double>& weight_,
		 const std::vector<double>& beta_)
{
  // For debugging only
  //
  minMul        = 0;
  totMul        = 0;
  minCnt        = MAXINT;
  minVol        = MAXDOUBLE;
  extVol        = 0;
  
  // Configuration
  //
  dims          = d;
  vol_trim      = false;
  num_points    = points_.size();
  ncut          = std::max<unsigned>(1, nc);
  maxL          = maxLevel * dims;
  
  //
  // Make the root node
  //
  root = new ORBNode(this);
  
  //
  // Assign the points
  //
  for (indx i=0; i<num_points; i++)
    root->pool.push_back(ORBdPtr(new ORBdatum(values_[i], points_[i],
					      mult_[i], weight_[i], beta_[i])));
  
  //
  // Check for manual or automatic bounds
  //
  if (lowerBound.size() == dims && upperBound.size() == dims) {
    root->setBounds(lowerBound, upperBound);
    
  } else {
    root->makeBounds();
  }
  
  //
  // Compute lengths per dimension
  //
  scale = std::vector<double>(dims);
  for (indx i=0; i<dims; i++) scale[i] = root->upper[i] - root->lower[i];
  
  //
  // Build the tree
  //
  if (num_points > 0) buildTree(root);
  
}

//! Destructor
ORBTree::~ORBTree()
{
  delete root;
  
  //
  // For detailed debugging only . . .
  //
  if (ORBTree::detailed_debug)
    std::cerr << std::endl
	      << std::setfill('=') << std::setw(70) << '=' << std::endl
	      << std::setfill('=') << std::setw(10) << '=' 
	      << std::left << std::setw(60) << " Detailed debugging " 
	      << std::endl<< std::setfill('=') << std::setw(70) << '=' 
	      << std::endl
	      << "ORBTree: zero volume count=" << minMul << "/" << totMul 
	      << std::endl
	      << "ORBTree: minimum node count=" << minCnt << std::endl
	      << "ORBTree: minimum node volume=" << minVol << std::endl
	      << "ORBTree: excluded volume=" << extVol << std::endl
	      << std::setfill('=') << std::setw(70) << '=' << std::endl
	      << std::setfill(' ') << std::endl;
}


//! Add additional points to the tree
void ORBTree::addPoints(const std::vector< std::vector<double> >& values_, 
			const std::vector< std::vector<double> >& points_, 
			const std::vector<unsigned>& mult_,
			const std::vector<double>& weight_,
			const std::vector<double>& beta_)
{
  //
  // Assign the new points
  //
  for (size_t i=0; i<values_.size(); i++) {
    ORBdPtr p(new ORBdatum(values_[i], points_[i],
			   mult_[i], weight_[i], beta_[i]));
    addPoint(root, p);
  }
}


//! Add points one at a time
void ORBTree::addPoint(ORBNode *root, ORBdPtr p)
{
  // Make sure that the point is in the box
  //
  if (root == this->root) {
    bool outside = false;
    for (size_t d=0; d<dims; d++) {
      if (p->p[d] < root->lower[d] || p->p[d] > root->upper[d]) {
	outside = true;
	break;
      }
    }
    if (outside) return;
  }
  
  //
  // We found the leaf!
  //
  if (root->pool.size()) {
    
    // Add current node to pool
    //
    root->pool.push_back(p);
    
    // if (root->pool.size()>=2*ncut && root->level < maxL) {
    if (root->pool.size()>=2*ncut) {
      
      // Do we need to divide?
      //
      unsigned d    = root->level % dims;
      double below  = root->lower[d];	// To be the largest value to the RIGHT
      double above  = root->upper[d];	// to be the smallest value to the LEFT
      double divide = 0.5*(below + above);	// Geometric division
      
      Pool Left, Right;
      for (Pool::iterator j=root->pool.begin(); j!=root->pool.end(); j++) {
	if ((*j)->p[d] < divide) {
	  // Find the closest point to the
	  // boundary from below
	  below = std::max<double>(below, (*j)->p[d]);
	  Left.push_back(*j);
	} else {
	  // Find the closest point to the
	  // boundary from above
	  above = std::min<double>(above, (*j)->p[d]);
	  Right.push_back(*j);
	}
      }
      
      //
      // Make NEW nodes, if the counts in L & R exceed the target number
      //
      if (Left.size()>=ncut && Right.size()>=ncut) {
	
	root->L = new ORBNode(this, root, Left,  below, divide, ORBNode::left);
	buildTree(root->L);
	
	root->R = new ORBNode(this, root, Right, above, divide, ORBNode::right);
	buildTree(root->R);
	
	root->pool.erase(root->pool.begin(), root->pool.end());
	leaves.erase(root);
      }
    }
    
  } else { 
    //
    // Keep searching . . . 
    //
    unsigned d    = root->level % dims;
    double divide = 0.5*(root->lower[d] + root->upper[d]);
    
    if (p->p[d] < divide)
      addPoint(root->L, p);
    else
      addPoint(root->R, p);
  }
}

//
// Build the tree downward from the current node
//
void ORBTree::buildTree(ORBNode *root)
{
  if (0) {
    std::ostringstream ostr1, ostr2;
    ostr1 << "Count=" << root->pool.size() << "/" << ncut;
    ostr2 << "Level=" << root->level << "/" << maxL;
    std::cout << std::setw(25) << ostr1.str() 
	      << std::setw(25) << ostr2.str()
	      << std::endl;
  }
  
  // If this is a leaf, only check for division if size is more than 2
  // buckets worth and the level ceiling is not exceeded
  //
  // if (root->pool.size() >= 2*ncut && root->level < maxL) {
  if (root->pool.size() >= 2*ncut) {
    
    //
    // Sort for bisection on the next dimension
    //
    unsigned d    = root->level % dims;
    double below  = root->lower[d];	// To be the largest value to the RIGHT
    double above  = root->upper[d];	// to be the smallest value to the LEFT
    double divide = 0.5*(below + above);	// Geometric division
    
    Pool Left, Right;
    for (Pool::iterator j=root->pool.begin(); j!=root->pool.end(); j++) {
      if ((*j)->p[d] < divide) {
	// Find the closest point to the
	// boundary from below
	below = std::max<double>(below, (*j)->p[d]);
	Left.push_back(*j);
      } else {
	// Find the closest point to the
	// boundary from above
	above = std::min<double>(above, (*j)->p[d]);
	Right.push_back(*j);
      }
    }
    
    
    // Make NEW nodes, if the counts in L & R exceed the target number
    //
    if (Left.size()>=ncut && Right.size()>=ncut) {
      
      root->L = new ORBNode(this, root, Left,  below, divide, ORBNode::left);
      buildTree(root->L);
      
      root->R = new ORBNode(this, root, Right, above, divide, ORBNode::right);
      buildTree(root->R);
      
      root->pool.erase(root->pool.begin(), root->pool.end());
    }
    
  }
  
  // If I'm still a leaf, add myself to the frontier
  //
  if (root->pool.size()) leaves.insert(root);
}


//
// Compute the integral from the ORB tree for all leaves
//
void ORBTree::Integral(std::vector<double>& result,
		       std::vector<double> (*func)(std::vector<double>&))
{
  std::vector<double> ctr(dims);
  double vol;
  
  for (nodeItr it=leaves.begin(); it != leaves.end(); it++) {

    // Compute the geometric center of the cell
    //
    for (unsigned j=0; j<dims; j++) 
      ctr[j] = 0.5*((*it)->lower[j] + (*it)->upper[j]);
    
    // Fiducial volume in this cell
    //
    if (geom_volume)
      vol   = sqrt((*it)->Vol*(*it)->vol);
    else if (full_volume)
      vol   = (*it)->Vol;
    else
      vol   = (*it)->vol;
    
    // Riemann integral based on the func
    //
    std::vector<double> ret = (*func)(ctr);
    for (unsigned i=0; i<result.size(); i++) result[i] += ret[i] * vol;
  }
}

//
// Compute the integral and print the result to a stream
//
void ORBTree::Integral(std::ostream& out, int dim,
		       std::vector<double> (*func)(std::vector<double>&))
{
  std::vector<double> result(dim, 0.0);
  
  Integral(result, func);
  out << "Result=";
  for (int j=0; j<dim; j++) out << std::setw(12) << std::left << result[j];
  out << std::endl;
}

//
// Compute the Riemann integral from the field values with trimming if
// desired
//
// data and mult are ignored in this implementation
//
void ORBTree::IntegralEval(std::vector< std::vector<double> >& data, 
			   std::vector<unsigned>& mult,
			   std::map<int, std::vector<double> >& result,
			   std::map<int, std::vector<double> >& lower, 
			   std::map<int, std::vector<double> >& upper,
			   std::map<int, std::vector<double> >& vmean)
{
  // Clean the output vectors
  //
  for (unsigned l=0; l<result.size(); l++) {
    for (unsigned n=0; n<result[l].size(); n++)
      result[l][n] = lower[l][n] = upper[l][n] = vmean[l][0] = 0.0;
  }
  
  // Compute the trimmed frontier
  vfrac = computeTrim();

  // Compute the integral
  IntegralPC(result, lower, upper, vmean);
}

// For accumulating tree diagnostics
class nodeInfo 
{
public:
  unsigned long nodes;
  unsigned long N, minN, maxN;
  double meanN, minV, maxV, meanV, vvol;
  std::vector< std::vector<double> > upper;
  std::vector< std::vector<double> > lower;
  std::vector<double> value;
  
  nodeInfo() 
  {
    nodes = N = 0;
    minN  = MAXLONG;
    maxN  = 0;
    meanN = meanV = vvol = 0;
    minV  = MAXDOUBLE;
    maxV  = 0;
  }
};

// Compute tree diagnostics
void ORBTree::LevelList(std::ostream& out, bool limits)
{
  
  std::map<unsigned, nodeInfo> llist;
  std::map<unsigned, nodeInfo>::iterator itl;
  unsigned long totalN = 0, mm;
  unsigned l;
  double vol;
  
  for (nodeItr it=leaves.begin(); it!=leaves.end(); it++) {
    l = (*it)->level;
    
    llist[l].minN  = std::min<unsigned long>(llist[l].minN, (*it)->pool.size());
    llist[l].maxN  = std::max<unsigned long>(llist[l].maxN, (*it)->pool.size());
    llist[l].meanN = 
      (llist[l].meanN*llist[l].nodes + (*it)->pool.size())/(llist[l].nodes+1);
    
    // Fiducial volume in this cell
    if (geom_volume)
      vol   = sqrt((*it)->Vol*(*it)->vol);
    else if (full_volume)
      vol   = (*it)->Vol;
    else
      vol   = (*it)->vol;
    
    llist[l].vvol  = vol;
    llist[l].minV  = std::min<double>(llist[l].minV, vol);
    llist[l].maxV  = std::max<double>(llist[l].maxV, vol);
    llist[l].meanV = 
      (llist[l].meanV*llist[l].nodes + (*it)->vol)/(llist[l].nodes+1);
    
    llist[l].nodes++;
    llist[l].N += (*it)->pool.size();
    totalN += (*it)->pool.size();
    
    if (limits) {
      llist[l].lower.push_back((*it)->lower);
      llist[l].upper.push_back((*it)->upper);
    }
  }
  
  out << std::endl 
      << "----------------------" << std::endl
      << "ORBTree build analysis" << std::endl
      << "----------------------" << std::endl
      << std::endl;
  
  out << std::setw(4) << "Lev " << " | "
      << std::setw(8) << "Nodes"
      << std::setw(8) << "# pts"
      << std::setw(6) << "MinN"
      << std::setw(6) << "MaxN"
      << std::setw(6) << "MeanN"
      << std::setw(9) << "Vol"
      << std::setw(9) << "MinV"
      << std::setw(9) << "MeanV"
      << std::setw(9) << "MaxV";
  if (limits) {
    out << std::setw(9) << "CumV";
    std::ostringstream sout;
    for (indx j=0; j<dims; j++) {
      sout.str(""); sout << "Min #" << j+1;
      out << std::setw(12) << sout.str();
      sout.str(""); sout << "Max #" << j+1;
      out << std::setw(12) << sout.str();
    }
  }
  out << std::endl
      << std::setw(4) << "+---" << " | "
      << std::setw(8) << "+------"
      << std::setw(8) << "+------"
      << std::setw(6) << "+----"
      << std::setw(6) << "+----"
      << std::setw(6) << "+----"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------";
  if (limits) {
    out << std::setw(9) << "+-------";
    for (indx j=0; j<dims; j++) {
      out << std::setw(12) << "+---------"
	  << std::setw(12) << "+---------";
    }
  }
  out << std::endl;
  
  double cumv=0.0, totv=0.0;
  if (limits)
    for (itl=llist.begin(); itl!=llist.end(); itl++) totv += itl->second.vvol;
  
  unsigned old_prec = out.precision(2);
  
  for (itl=llist.begin(); itl!=llist.end(); itl++) {
    mm = static_cast<unsigned long>(floor(itl->second.meanN));
    out << std::setw(4) << itl->first << " | "
	<< std::setw(8) << itl->second.nodes
	<< std::setw(8) << itl->second.N
	<< std::setw(6) << itl->second.minN
	<< std::setw(6) << itl->second.maxN
	<< std::setw(6) << mm
	<< std::setw(9) << itl->second.vvol
	<< std::setw(9) << itl->second.minV
	<< std::setw(9) << itl->second.meanV
	<< std::setw(9) << itl->second.maxV;
    if (limits) {
      cumv += itl->second.vvol;
      out << std::setw(9) << cumv/totv;
      out.setf(std::ios::scientific);	  // Set scientific format
      unsigned prec = out.precision(4); // Set precision
      for (indx j=0; j<dims; j++)
	out << std::setw(12) << itl->second.lower[0][j]
	    << std::setw(12) << itl->second.upper[0][j];
      for (indx k=1; k<itl->second.lower.size(); k++) {
	out << std::endl << std::setw(4) << "*" << " | "
	    << std::setw(8) << "." << std::setw(8) << "."
	    << std::setw(6) << "." << std::setw(6) << "."
	    << std::setw(6) << "." << std::setw(9) << "."
	    << std::setw(9) << "." << std::setw(9) << "."
	    << std::setw(9) << "." << std::setw(9) << ".";
	for (indx j=0; j<dims; j++)
	  out << std::setw(12) << itl->second.lower[k][j]
	      << std::setw(12) << itl->second.upper[k][j];
      }
      out.unsetf(std::ios::scientific); // Restore original format
      out.precision(prec);		  // Restore precision
    }
    out << std::endl;
  }
  out << std::endl;
  out << "  Total # nodes=" << leaves.size()
      << "  Total # point=" << totalN
      << std::endl << std::endl;
  
  out.precision(old_prec);
  
}

void ORBTree::IntegralPC(std::map<int, std::vector<double> >& medn,
			 std::map<int, std::vector<double> >& lowr,
			 std::map<int, std::vector<double> >& uppr,
			 std::map<int, std::vector<double> >& mean)
  
{
  unsigned cnt = 0;
  double vol;
  
  // I assume that the tree is constructed, and "tree" contains the
  // permuted data.  Now, iterate through the leaf cells
  
  for (nodeItr it=leaves.begin(); it != leaves.end(); it++) {
    // Fiducial volume in this cell
    if (geom_volume)
      vol   = sqrt((*it)->Vol*(*it)->vol);
    else if (full_volume)
      vol   = (*it)->Vol;
    else
      vol   = (*it)->vol;
    
    // For in-depth debugging only
    if (0) {
      std::cout << std::setw(6)  << ++cnt
		<< std::setw(8)  << (*it)->pool.size()
		<< std::setw(8)  << (*it)->level
		<< std::setw(18) << vol << std::endl;
    }
    
    //------------------------------------------------------------
    // Compute the populations and pooled list
    //------------------------------------------------------------
    
    std::map<double, double> pops;
    std::map<double, double>::iterator jt;
    sz = 0;
    
    for (Pool::iterator i=(*it)->pool.begin(); i!=(*it)->pool.end(); i++) {
      if (sz==0) sz = (*i)->v.size();
      if ( (jt = pops.find((*i)->beta)) == pops.end()) {
	pops[(*i)->beta] = 1;
      } else {
	jt->second += 1;
      }
    }
    
    dpair pmax;
    for (jt = pops.begin(); jt != pops.end(); jt++) {
      if      (jt == pops.begin())     pmax = *jt;
      else if (jt->second>pmax.second) pmax = *jt;
    }
    
    //------------------------------------------------------------
    // Compute the moments in the cell
    //------------------------------------------------------------
    
    for (unsigned j=0; j<sz; j++) {
      // Sort on dimension "j" for computing
      // specified quantiles
      std::vector< std::vector<double> > v(2);
      for (Pool::iterator i=(*it)->pool.begin(); i!=(*it)->pool.end(); i++) {
	
	for (unsigned n=0; n<(*i)->mult; n++) {
	  if ((*i)->beta == pmax.first) v[0].push_back((*i)->v[j]);
	  v[1].push_back((*i)->v[j]);
	}
      }
      
      for (unsigned l=0; l<2; l++) {
	if (v[l].size() == 0) continue;
	
	std::sort(v[l].begin(), v[l].end());
	
	unsigned lo = 
	  std::min<unsigned>(floor(v[l].size()*lower_quant+0.5), v[l].size()-1);
	lo = std::max<unsigned>(lo, 0);
	
	unsigned md = 
	  std::min<unsigned>(floor(v[l].size()*0.5+0.5), v[l].size()-1);
	md = std::max<unsigned>(md, 0);
	
	unsigned hi = 
	  std::min<unsigned>(floor(v[l].size()*upper_quant+0.5), v[l].size()-1);
	hi = std::max<unsigned>(hi, 0);
	
	medn[l][j] += v[l][md] * vol;
	
	for (size_t k=0; k<v[l].size(); k++) 
	  mean[l][j] += v[l][k]/v[l].size() * vol;
	
	lowr[l][j] += v[l][lo] * vol;
	
	uppr[l][j] += v[l][hi] * vol;
	
      }
      
    }
    
  }
  
}


void ORBTree::IntegralList(std::ostream& out, 
			   std::vector< std::vector<double> >& data,
			   std::vector<unsigned>& mult,
			   bool allcells)
{
  std::map<unsigned, nodeInfo> llist;
  std::map<unsigned, nodeInfo>::iterator itl;
  unsigned long totalN = 0, mm;
  unsigned l;
  
  sz = data[0].size();
  
  if (allcells) {
    
    out << std::endl 
	<< "-------------------------------" << std::endl
	<< "ORBTree build/integral analysis" << std::endl
	<< "-------------------------------" << std::endl
	<< std::endl;
    
    out << std::setw(4) << "Lev " << " | "
	<< std::setw(8) << "# pts"
	<< std::setw(9) << "Vol";
    for (unsigned j=0; j<sz; j++) {
      std::ostringstream sout1, sout2;
      sout1 << "Posn [" << j+1 << "]";
      sout2 << "Csum [" << j+1 << "]";
      out << std::setw(12) << sout1.str()
	  << std::setw(12) << sout2.str();
    }
    
    out << std::endl
	<< std::setw(4) << "+---" << " | "
	<< std::setw(8) << "+------"
	<< std::setw(9) << "+-------";
    for (unsigned j=0; j<sz; j++) {
      out << std::setw(12) << "+---------"
	  << std::setw(12) << "+---------";
    }
    out << std::endl;
    
    unsigned old_prec = out.precision(2);
    
    typedef std::multimap<double, ORBNode*> vsort;
    typedef std::pair<double, ORBNode*> vpair;
    vsort vlist;
    for (nodeItr it=leaves.begin(); it!=leaves.end(); it++) {
      // Fiducial volume in this cell
      double vol;
      if (geom_volume)
	vol   = sqrt((*it)->Vol*(*it)->vol);
      else if (full_volume)
	vol   = (*it)->Vol;
      else
	vol   = (*it)->vol;
      
      vlist.insert(vpair(vol, *it));
    }
    
    std::vector<double> cvalue(sz, 0.0), cvalue0(sz, 0.0), value(sz), vols;
    std::vector< std::vector<double> > intg;
    std::vector<unsigned> levs, npts;
    
    for (vsort::iterator itr=vlist.begin(); itr!=vlist.end(); itr++) {
      
      ORBNode *p = itr->second;
      // Fiducial volume in this cell
      double vol;
      if (geom_volume)
	vol   = sqrt(p->Vol*p->vol);
      else if (full_volume)
	vol   = p->Vol;
      else
	vol   = p->vol;
      
      for (unsigned j=0; j<sz; j++) {
	std::vector<double> v; 
	for (Pool::iterator i=p->pool.begin(); i!=p->pool.end(); i++) 
	  for (unsigned n=0; n<(*i)->mult; n++) v.push_back((*i)->v[j]);
	std::sort(v.begin(), v.end());
	
	unsigned k;
	k = std::max<unsigned int>(0, 0.5*v.size());
	k = std::min<unsigned int>(k, v.size() - 1);
	
	value[j] = v[k] * vol;
	cvalue0[j] += value[j];
      }
      
      levs.push_back(p->level);
      npts.push_back(p->pool.size());
      vols.push_back(vol);
      intg.push_back(value);
      
      totalN += p->pool.size();
    }
    
    for (unsigned k=0; k<levs.size(); k++) {
      
      out << std::setw(4) << levs[k] << " | "
	  << std::setw(8) << npts[k]
	  << std::setw(8) << vols[k];
      
      unsigned prec = out.precision(3); // Set precision
      out.setf(std::ios::scientific);	// Set scientific format
      for (unsigned j=0; j<sz; j++) {
	cvalue[j] += intg[k][j];
	out << std::setw(12) << cvalue[j]
	    << std::setw(12) << cvalue[j]/cvalue0[j];
      }
      out.precision(prec);		// Restore precision
      out.unsetf(std::ios::scientific); // Restore format
      out << std::endl;
    }
    out << std::endl;
    out << "  Total # nodes=" << leaves.size()
	<< "  Total # point=" << totalN
	<< std::endl << std::endl;
    
    out.precision(old_prec);
    
    
  } else {
    
    for (nodeItr it=leaves.begin(); it!=leaves.end(); it++) {
      l = (*it)->level;
      
      llist[l].minN = std::min<unsigned long>(llist[l].minN, (*it)->pool.size());
      llist[l].maxN = std::max<unsigned long>(llist[l].maxN, (*it)->pool.size());
      llist[l].meanN = 
	(llist[l].meanN*llist[l].nodes + (*it)->pool.size())/(llist[l].nodes+1);
      
      
      // Fiducial volume in this cell
      double vol;
      if (geom_volume)
	vol   = sqrt((*it)->Vol*(*it)->vol);
      else if (full_volume)
	vol   = (*it)->Vol;
      else
	vol   = (*it)->vol;
      
      llist[l].vvol = vol;
      llist[l].minV = std::min<double>(llist[l].minV, vol);
      llist[l].maxV = std::max<double>(llist[l].maxV, vol);
      llist[l].meanV = 
	(llist[l].meanV*llist[l].nodes + vol)/(llist[l].nodes+1);
      
      llist[l].nodes++;
      llist[l].N += (*it)->pool.size();
      totalN += (*it)->pool.size();
      
      if (llist[l].value.size() != sz) 
	llist[l].value = std::vector<double>(sz, 0.0);
      
      for (unsigned j=0; j<sz; j++) {
	std::vector<double> v;
	for (Pool::iterator i=(*it)->pool.begin(); i!=(*it)->pool.end(); i++) 
	  v.push_back((*i)->v[j]);
	std::sort(v.begin(), v.end());
	
	unsigned k = std::min<unsigned int>(std::max<unsigned int>(0.5*v.size(), 0), v.size()-1);
	llist[l].value[j] += v[k] * vol;
      }
    }
    
    out << std::endl 
	<< "-------------------------------" << std::endl
	<< "ORBTree build/integral analysis" << std::endl
	<< "-------------------------------" << std::endl
	<< std::endl;
    
    out << std::setw(4) << "Lev " << " | "
	<< std::setw(8) << "Nodes"
	<< std::setw(8) << "# pts"
	<< std::setw(6) << "MinN"
	<< std::setw(6) << "MaxN"
	<< std::setw(6) << "MeanN"
	<< std::setw(9) << "Vol"
	<< std::setw(9) << "MinV"
	<< std::setw(9) << "MeanV"
	<< std::setw(9) << "MaxV";
    for (unsigned j=0; j<sz; j++) {
      std::ostringstream sout1, sout2;
      sout1 << "Posn [" << j+1 << "]";
      sout2 << "Csum [" << j+1 << "]";
      out << std::setw(12) << sout1.str()
	  << std::setw(12) << sout2.str();
    }
    out << std::endl
	<< std::setw(4) << "+---" << " | "
	<< std::setw(8) << "+------"
	<< std::setw(8) << "+------"
	<< std::setw(6) << "+----"
	<< std::setw(6) << "+----"
	<< std::setw(6) << "+----"
	<< std::setw(9) << "+-------"
	<< std::setw(9) << "+-------"
	<< std::setw(9) << "+-------"
	<< std::setw(9) << "+-------";
    for (unsigned j=0; j<sz; j++) {
      out << std::setw(12) << "+---------"
	  << std::setw(12) << "+---------";
    }
    out << std::endl;
    
    std::vector<double> cvalue(sz, 0.0);
    unsigned old_prec = out.precision(2);
    
    for (itl=llist.begin(); itl!=llist.end(); itl++) {
      mm = static_cast<unsigned long>(floor(itl->second.meanN));
      out << std::setw(4) << itl->first << " | "
	  << std::setw(8) << itl->second.nodes
	  << std::setw(8) << itl->second.N
	  << std::setw(6) << itl->second.minN
	  << std::setw(6) << itl->second.maxN
	  << std::setw(6) << mm
	  << std::setw(9) << itl->second.vvol
	  << std::setw(9) << itl->second.minV
	  << std::setw(9) << itl->second.meanV
	  << std::setw(9) << itl->second.maxV;
      out.setf(std::ios::scientific);   // Set scientific format
      unsigned prec = out.precision(4); // Set precision
      for (unsigned j=0; j<sz; j++) {
	cvalue[j] += itl->second.value[j];
	out << std::setw(12) << itl->second.value[j]
	    << std::setw(12) << cvalue[j];
      }
      out.unsetf(std::ios::scientific); // Restore original format
      out.precision(prec);	      // Restore precision
      out << std::endl;
    }
    out << std::endl;
    out << "  Total # nodes=" << leaves.size()
	<< "  Total # point=" << totalN
	<< std::endl << std::endl;
    
    out.precision(old_prec);
  }
  
}

void ORBTree::IntegralCellList(std::ostream& out, unsigned long ncut,
			       std::vector< std::vector<double> >& data)
{
  std::map<unsigned, nodeInfo> llist;
  std::map<unsigned, nodeInfo>::iterator itl;
  
  sz = data[0].size();
  
  typedef std::multimap<double, ORBNode*> vsort;
  typedef std::pair<double, ORBNode*> vpair;
  vsort vlist;
  for (nodeItr it=leaves.begin(); it!=leaves.end(); it++) {
    // Fiducial volume in this cell
    double vol;
    if (geom_volume)
      vol   = sqrt((*it)->Vol*(*it)->vol);
    else if (full_volume)
      vol   = (*it)->Vol;
    else
      vol   = (*it)->vol;
    
    vlist.insert(vpair(vol, *it));
  }
  
  std::vector<double> cvalue(sz, 0.0), cvalue0(sz, 0.0), value(sz), vols;
  std::vector< std::vector<double> > intg;
  std::vector<unsigned> levs, npts;
  
  out.setf(std::ios::scientific); // Set scientific format
  
  for (vsort::iterator itr=vlist.begin(); itr!=vlist.end(); itr++) {
    
    ORBNode *p = itr->second;
    
    for (unsigned j=0; j<sz; j++) {
      std::vector<double> v; 
      for (Pool::iterator i=p->pool.begin(); i!=p->pool.end(); i++) 
	v.push_back((*i)->v[j]);
      std::sort(v.begin(), v.end());
      
      unsigned k;
      k = std::max<unsigned int>(0, 0.5*v.size());
      k = std::min<unsigned int>(k, v.size() - 1);
      
      value[j] = v[k];
    }
    
    // Fiducial volume in this cell
    double vol;
    if (geom_volume)
      vol   = sqrt(p->Vol*p->vol);
    else if (full_volume)
      vol   = p->Vol;
    else
      vol   = p->vol;
    
    out << std::setw(6)  << p->level
	<< std::setw(8)  << p->pool.size()
	<< std::setw(18) << vol
	<< std::setw(6)  << sz;
    
    for (unsigned j=0; j<sz; j++)  
      out << std::setw(18) << value[j];
    
    out << std::setw(6) << dims;
    
    for (unsigned k=0; k<dims; k++)
      out << std::setw(18) << p->lower[k]
	  << std::setw(18) << p->upper[k];
    
    for (unsigned k=0; k<dims; k++)
      out << std::setw(18) << 0.5*(p->lower[k] + p->upper[k]);
    
    for (unsigned k=0; k<dims; k++)
      out << std::setw(18) << p->upper[k] - p->lower[k];
    
    out << std::endl;
  }
  
  out.unsetf(std::ios::scientific); // Unset scientific format
}

void ORBTree::createLebesgueList(unsigned dindx, unsigned long ncut,
				 std::vector< std::vector<double> >& data,
				 std::vector<unsigned>& mult, bool linear)
{
  //------------------------------------------------------------
  // Clean any existing data
  //------------------------------------------------------------
  lebesgueLeaves.erase(lebesgueLeaves.begin(), lebesgueLeaves.end());
  
  for (int l=0; l<2; l++) {
    LminP[l] =  MAXDOUBLE;
    LmaxP[l] = -MAXDOUBLE;
  }
  
  LebesgueList(dindx, ncut, data, mult, lebesgueLeaves, linear);
}

void ORBTree::LebesgueList(unsigned dindx, unsigned long ncut,
			   std::vector< std::vector<double> >& data,
			   std::vector<unsigned>& mult,
			   std::map<int, std::multimap<double, LeafData> >& ORBLeaves,
			   bool linear)
{
  
  for (nodeItr it=leaves.begin(); it != leaves.end(); it++) {
    
    // Fiducial volume in this cell
    double vol;
    if (geom_volume)
      vol   = sqrt((*it)->Vol*(*it)->vol);
    else if (full_volume)
      vol   = (*it)->Vol;
    else
      vol   = (*it)->vol;
    
    //------------------------------------------------------------
    // Compute the populations
    //------------------------------------------------------------
    
    std::map<double, double> pops;
    std::map<double, double>::iterator jt;
    
    for (Pool::iterator i=(*it)->pool.begin(); i!=(*it)->pool.end(); i++) {
      if ( (jt = pops.find((*i)->beta)) == pops.end()) {
	pops[(*i)->beta] = 1;
      } else {
	jt->second += 1;
      }
    }
    
    dpair pmax;
    for (jt = pops.begin(); jt != pops.end(); jt++) {
      if      (jt == pops.begin())     pmax = *jt;
      else if (jt->second>pmax.second) pmax = *jt;
    }
    
    //------------------------------------------------------------
    // Compute the pooled list
    //------------------------------------------------------------
    
    std::vector< std::vector<double> > pv(2);
    for (Pool::iterator i=(*it)->pool.begin(); i!=(*it)->pool.end(); i++) {
      
      for (unsigned n=0; n<(*i)->mult; n++) {
	if ((*i)->beta == pmax.first) pv[0].push_back((*i)->v[dindx]);
	pv[1].push_back((*i)->v[dindx]);
      }
    }
    
    //------------------------------------------------------------
    // Insert the data
    //------------------------------------------------------------
    
    for (unsigned l=0; l<2; l++) {
      if (pv[l].size() == 0) continue;
      
      std::sort(pv[l].begin(), pv[l].end());
      
      LeafData p;
      
      p.vol   = vol;
      
      p.minP  = pv[l].front();
      // Median prob value
      unsigned nmed = std::min<unsigned>
	(floor(0.5*pv[l].size() + 0.5), pv[l].size()-1);
      nmed = std::max<unsigned>(nmed, 0);
      
      p.medP  = pv[l][nmed];
      // Minimum prob value
      p.maxP  = pv[l].back();
      // The probability values
      if (pv[l].size()>1)  {
	double delta, val;
	if (linear) delta = 1.0/(pv[l].back() - pv[l].front());
	else        delta = 1.0/(log(pv[l].back()) - log(pv[l].front()));
	if (pv[l].back() > pv[l].front()) {
	  p.pvals.push_back(dpair(pv[l][0], 1.0));
	  for (unsigned i=1; i<pv[l].size()-1; i++) {
	    if (linear) val = (pv[l].back() - pv[l][i])*delta;
	    else        val = (log(pv[l].back()) - log(pv[l][i]))*delta;
	    p.pvals.push_back(dpair(pv[l][i], val));
	  }
	} else {
	  double delta = 1.0/pv[l].size();
	  for (unsigned i=0; i<pv[l].size()-1; i++) {
	    p.pvals.push_back(dpair(pv[l][i], 1.0 - delta*i));
	  }
	}
      }
      p.pvals.push_back(std::pair<double, double>(pv[l].back(), 0.0));
      
      // Key and sort by max probability value
      lebesgueLeaves[l].insert(std::pair<double, LeafData>(p.maxP, p));
      
      // Tabulate global extrema
      LminP[l] = std::min<double>(LminP[l], p.medP);
      LmaxP[l] = std::max<double>(LmaxP[l], p.medP);
    }
  }
}


void ORBTree::gnuplotDemo(const std::vector< double >& ctrs,
			  const unsigned dindx, const unsigned long ncut,
			  const unsigned utag)
{
  if (dims<2) return;
  
  std::string tag;
  if (utag>0) {
    std::ostringstream sout;
    sout << std::setfill('0') << std::setw(6) << std::right << utag;
    tag = sout.str();
  } else tag = getRandomFileTag();
  
  std::ostringstream ostr;
  ostr << "vta_cells_orb." << tag << ".gpl";
  std::ofstream out(ostr.str().c_str());
  
  if (out) {
    
    // Check dimensions
    if (dim1 == dim2 || dim1 >= dims || dim2 >= dims) {
      dim1 = 0;
      dim2 = 1;
    }
    
    std::vector< std::vector<double> > val;
    std::vector<double> eval(5);
    std::vector<int> max_index(dims);
    
    for (nodeItr elem=leaves.begin(); elem != leaves.end(); elem++) {
      
      bool ok = true;		// Is the cell in the desired slice?
      for (unsigned d=0; d<dims; d++) {
	if (d == dim1) continue;
	if (d == dim2) continue;
	if ( (*elem)->Lower[d] > ctrs[d] || 
	     (*elem)->Upper[d] < ctrs[d]  )  ok = false;
      }
      
      if (ok) {
	
	// Compute the pooled list
	std::vector<double> pv;
	for (Pool::iterator i=(*elem)->pool.begin(); i!=(*elem)->pool.end(); i++) {
	  for (unsigned n=0; n<(*i)->mult; n++) {
	    pv.push_back((*i)->v[dindx]);
	  }
	}
	std::sort(pv.begin(), pv.end());
	
	eval[0] = pv[pv.size()/2];
	eval[1] = (*elem)->Lower[dim1];
	eval[2] = (*elem)->Lower[dim2];
	eval[3] = (*elem)->Upper[dim1];
	eval[4] = (*elem)->Upper[dim2];
	
	val.push_back(eval);
      }
    }
    
    sort(val.begin(), val.end());
    
    if (val.size()) {
      
      double vi   = val.front()[0];
      double vf   = val.back ()[0];
      double delv = vf - vi;
      
      if (delv<=0.0) delv = 1.0;
      
      for (unsigned n=0; n<val.size(); n++) {
	out << "set obj rect from " 
	    << val[n][1] << "," << val[n][2] << " to "
	    << val[n][3] << "," << val[n][4] << " front fc palette frac "
	    << std::fixed << (val[n][0] - vi)/delv << " lw 0" << std::endl;
      }
    }
    
    out << "set term wxt enhanced" << std::endl;
    out << "set xlabel 'x_1'" << std::endl;
    out << "set ylabel 'x_2'" << std::endl;
    out << "set pm3d map" << std::endl;
    out << "set cbrange [0:1]" << std::endl;
    out << "set colorbox" << std::endl;
    out << "splot [" 
	<< root->lower[dim1] << ":" << root->upper[dim1] << "] ["
	<< root->lower[dim2] << ":" << root->upper[dim2] << "] 0*x*y t'' lw 0" 
	<< std::endl;
    
    out.close();
  }
}


void ORBTree::gnuplotLinecut(const std::vector< double >& ctrs,
			     const unsigned dim0, const unsigned dindx, 
			     const unsigned long ncut, const unsigned utag)
{
  std::string tag;
  if (utag>0) {
    std::ostringstream sout;
    sout << std::setfill('0') << std::setw(6) << std::right << utag;
    tag = sout.str();
  } else tag = getRandomFileTag();
  
  std::ostringstream ostr;
  ostr << "vta_linecut_orb." << tag << ".gpl";
  std::ofstream out(ostr.str().c_str());
  
  std::vector<ORBTree::boxPtr> rects = lineCut(ctrs, dim0, 0.5, dindx);
    
  if (rects.size()) {
      
    if (out) {

      double vi = 0.0, vf = 0.0;
      for (size_t i=0; i<rects.size(); i++)
	vf = std::max<double>(vf, std::get<2>(*rects[i])[dindx]);
      
      double delv = vf - vi;
      if (delv<=0.0) delv = 1.0;
      
      for (size_t i=0; i<rects.size(); i++) {
	boxPtr p   = rects[i];
	double val = std::get<2>(*p)[dindx];
	out << "set obj rect from " 
	    << std::get<0>(*p) << "," << 0.0       << " to "
	    << std::get<1>(*p) << "," << val       << " front fc palette frac "
	    << std::fixed << (val - vi)/delv << " lw 0" << std::endl;
      }
	
      std::ostringstream stitle;
      stitle << "Central point: " << std::setprecision(3) << std::fixed;
      for (unsigned d=0; d<dims; d++) {
	stitle << ctrs[d];
	if (d!=dims-1) stitle << ", ";
      }
      
      out << "set term wxt enhanced" << std::endl;
      out << "set xlabel 'x_1'" << std::endl;
      out << "set ylabel 'x_2'" << std::endl;
      out << "set title  '" << stitle.str() << "'" << std::endl;
      out << "set pm3d map" << std::endl;
      out << "set cbrange [0:1]" << std::endl;
      out << "set colorbox" << std::endl;
      out << "splot [" 
	  << root->lower[dim1] << ":" << root->upper[dim1] << "] ["
	  << vi                << ":" << vf                << "] 0*x*y t'' lw 0" 
	  << std::endl;
      
      out.close();
    }
  }
}


std::vector<ORBTree::boxPtr> 
ORBTree::lineCut(const std::vector< double >& ctrs, const double quantile,
		 const unsigned dim0, const unsigned long dindx)
{
  unsigned dim = dim0;
  if (dim<0 || dim>dims-1) dim = 0;
    
  typedef std::map<double, boxPtr> vMap;
    
  std::vector<int> max_index(dims);
  vMap val;
    
  // Search through the frontier cells for intersection
  //
  for (nodeItr elem=leaves.begin(); elem != leaves.end(); elem++) {

				// This is the referenced node
    ORBNode* p = *elem;
      
    bool ok = true;		// Is the cell in the desired slice?
    for (unsigned d=0; d<dims; d++) {
      if (d == dim) continue;
      if ( p->Lower[d] > ctrs[d] ) { ok = false; break; }
      if ( p->Upper[d] < ctrs[d] ) { ok = false; break; }
    }
    
    if (ok) {

      // Process the pooled list
      //
      typedef std::pair<double, std::vector<double> > sortElem;
      std::vector<sortElem> pv;
      for (Pool::iterator i=p->pool.begin(); i!=p->pool.end(); i++) {
	for (unsigned n=0; n<(*i)->mult; n++)
	  pv.push_back(sortElem( (*i)->v[dindx], (*i)->v ));
      }
      
      // Sort the field values
      //
      std::sort(pv.begin(), pv.end());
      
      // Enter a non-zero lengthed segment
      //
      if (p->Lower[dim] < p->Upper[dim]) {
	const double tol = 1.0e-12;
	size_t quant = static_cast<size_t>(floor(pv.size()*quantile-tol));
				// Sanity check
	if (pv.size()==0) {
	  std::cout << "ORBTree::lineCut: NO values in pv!" << std::endl;
	} else {
				// Sanity check
	  quant = std::max<size_t>(0, std::min<size_t>(pv.size()-1, quant));

	  val[p->Lower[dim]] = boxPtr(new Box( p->Lower[dim],
					       p->Upper[dim],
					       pv[quant].second ) );
	}
      }
    }
  }
    
  std::vector<boxPtr> ret;

  if (val.size()) {
      
    vMap::iterator cur = val.begin(), lst;
      
    while (cur != val.end()) {
				// Add current rectangle to the list
      boxPtr p = cur->second;
      ret.push_back(p);
				// Sanity check
      if (ret.size() > val.size()) {
	std::cout << std::string(60, '-') << std::endl;
	std::cout << "Rectangles" << std::endl;
	{
	  size_t t=0;
	  for (vMap::iterator j=val.begin(); j!=val.end(); j++)
	    std::cout << std::setw(4)  << t++
		    << std::setw(12) << std::get<0>(*(j->second))
		    << std::setw(12) << std::get<1>(*(j->second))
		    << std::endl;
	}
	std::cout << std::string(60, '-') << std::endl;
	std::cout << "Line so far" << std::endl;
	for (size_t j=0; j<ret.size(); j++)
	  std::cout << std::setw(4)  << j
		    << std::setw(12) << std::get<0>(*ret[j])
		    << std::setw(12) << std::get<1>(*ret[j])
		    << std::endl;
	std::cout << std::string(60, '-')    << std::endl;
	break;
      }
				// Get next point
      lst = cur;
      cur = val.find(std::get<1>(*p));
    }
    
  }
    
  return ret;
}


double ORBTree::computeTrim()
{
  if (vol_computed) return vfrac;
  
  trim_leaves = leaves;
  
  if (!vol_trim) {
    vfrac = 1.0;
    return vfrac;
  }
  
  std::multimap<double, ORBNode*> sort_frontier;
  std::multimap<double, ORBNode*>::iterator it;
  unsigned long count = 0, sofar = 0;
  for (nodeItr cur=leaves.begin(); cur != leaves.end(); cur++) {
    
    double minv_ = MAXDOUBLE;
    
    for (Pool::iterator i=(*cur)->pool.begin(); i!=(*cur)->pool.end(); i++)
      minv_ = std::min<double>(minv_, (*i)->v[0]);
    
    sort_frontier.insert(std::pair<double, ORBNode*>(minv_, *cur));
    count += (*cur)->pool.size();
  }
  
  unsigned long target = count - 
    static_cast<unsigned long>(floor(count*vol_fraction));
  for (it=sort_frontier.begin(); it!=sort_frontier.end(); it++) {
    sofar += it->second->pool.size();
    if (sofar >= target) break;
  }
  
  trim_leaves.erase(trim_leaves.begin(), trim_leaves.end());
  for (; it!=sort_frontier.end(); it++) trim_leaves.insert(it->second);
  
  vol_computed = true;
  
  vfrac = static_cast<double>(count - sofar)/count;
  return vfrac;
}


size_t ORBTree::numberInTree()
{
  size_t count = 0;
  for (nodeItr cur=leaves.begin(); cur != leaves.end(); cur++) 
    count += (*cur)->pool.size();

  return count;
}
