#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <readline/readline.h>
#include <readline/history.h>
#include <gvariable.h>
#include <gfunction.h>
#include <rline.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;     
#endif

namespace BIE {

  const int lbufsz = 1024;
  static char *line = 0;
  static unsigned int lsize = 0; 
  extern char prompt[];

  /** 
   * Gets a command from the command line if interactive or from the script
   * input file if not interactive, and returns the command char by char.
   * Returns EOF character when reading from a script and we reach the end of
   * the file.
   */
  char rl_gets ()
  {
    if ((line != 0) && (lsize < strlen(line)))
    {
      // If we previously fetched a line and we have not yet returned all 
      // the characters, then return the next character.  The position of the
      // character to return the next time is incremented.
      return line[lsize++];
    }
    else if (line != 0) 
    {
      // Free up the memory we allocated for the line when we read it.
      delete [] line;
      
      // Set line to 0, to let us know to read a new line the next time 
      // this function is called.
      line = 0;
      
      // Return a new line character to indicate the end of the command.
      return '\n';
    }

    // If we reach here, we are looking for another command line.
    if (interactive)
    {
      // Get a line from the user
      line = readline (prompt);
#ifdef DEBUG_TESSTOOL
      ferr << "got line from readline: " << line << endl;
#endif
    }
    else
    {
      // Allocate memory for the line we are about to read.
      if (line == 0) 
      { line = new char [lbufsz]; }

      // write prompt
      if (ttool) {
	consoleFile << "\n";
	consoleFile.flush();
      }

      // Get a line from the script.
      cin.getline(line, lbufsz);
#ifdef DEBUG_TESSTOOL
      ferr << "got line from cin: " << line << endl;
#endif
    }

    // We return the first character from this call, so the character
    // position for the next call is 1.
    lsize = 1;
    
    // If the line has any text in it, then add the line to the history.
    if (line && *line)
    {
      // Save it in the readline history buffer.  We do this whether or not the
      // session is interactive, since we use the readline history
      // when getting the history saved away when using persistence to save
      // away a state.
      add_history (line);
    }
    
    // Print the line to stdout if this is not an interactive session
    // and the line is not null (could have reached EOF in script file).
    if (! interactive && line != 0)
    {
      cout << line << "\n";
    }
  
    // Getting a line from readline never fails (at the end of a session
    // we just to an exit() when getting an 'exit' command), but
    // we will eventually hit EOF in a file, so before we return we check
    // on the status of our input stream.
    if (cin) 
    { 
      if (line == 0) {
	return (char) EOF;
      } else {
	return (line[0]); 
      }
    }
    else
    { return (char) EOF; }
  }

} // end of namespace
