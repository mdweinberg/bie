#include <iostream>
#include <iomanip>
#include <vector>

#include <cmath>
#include <cfloat>

using namespace std;

#include <MetricTreeDensity.h>

BIE_CLASS_EXPORT_IMPLEMENT(MetricTreeDensity)

MetricTreeDensity::MetricTreeDensity
(vector< vector<double> >* points_, 
 vector< vector<double> >* widths_,
 vector<double>* weight_, 
 KernelPtr factory, unsigned target_) : MetricTree(points_, target_)
{
  kernel = factory;

  // Set initial data
  //
  widths = widths_;
  weight = weight_;
  
  // Normalize weights
  //
  vector<double>::iterator it;
  double norm = 0.0;
  for (it=weight->begin(); it!=weight->end(); it++) norm += *it;
  for (it=weight->begin(); it!=weight->end(); it++) *it /= norm;

  makeDensity(root, 0);
}

MetricTreeDensity::MetricTreeDensity
(vector< vector<double> >* points_, 
 vector< vector<double> >* widths_,
 vector<double>* weight_, 
 vector<double>& mscale_,
 KernelPtr factory, unsigned target_) : MetricTree(points_, mscale_, target_)
{
  kernel = factory;

  // Set initial data
  //
  widths = widths_;
  weight = weight_;
  
  // Normalize weights
  //
  vector<double>::iterator it;
  double norm = 0.0;
  for (it=weight->begin(); it!=weight->end(); it++) norm += *it;
  for (it=weight->begin(); it!=weight->end(); it++) *it /= norm;

  makeDensity(root, 0);
}

//
// Load up new widths and weights
//
void MetricTreeDensity::loadData(vector< vector<double> >* widths_, 
				 vector<double> * weight_)
{
  widths = widths_;
  weight = weight_;
  makeDensity(root, 0);
}

//
// Walk the tree and accumulate the weights, widths, means, etc.
//
void MetricTreeDensity::makeDensity(ballPtr cur, unsigned cnt)
{
  // My children
  //
  ballPtr one = cur->leftKid;
  ballPtr two = cur->rightKid;

  // Do this bottom up . . . 
  //
  if (cur->Leaf()) {
    mapitD it = cur->owned.begin();
    indx cnt  = 1;

    cen[cur] = (*points)[it->second];
    wid[cur] = (*widths)[it->second];
    wgt[cur] = (*weight)[it->second];

    minWid[cur] = wid[cur];
    maxWid[cur] = wid[cur];

    for (it++; it != cur->owned.end(); it++) {
      for (size_t j=0; j<cen[cur].size(); j++) 
	cen[cur][j] += (*points)[it->second][j];
      for (size_t j=0; j<wid[cur].size(); j++) {
	wid[cur][j] += (*widths)[it->second][j];
	minWid[cur][j] = min<double>(minWid[cur][j], (*widths)[it->second][j]);
	maxWid[cur][j] = max<double>(maxWid[cur][j], (*widths)[it->second][j]);
      }
      wgt[cur] += (*weight)[it->second];
      cnt++;
    }

    for (size_t j=0; j<cen[cur].size(); j++) cen[cur][j] /= cnt;
    for (size_t j=0; j<wid[cur].size(); j++) wid[cur][j] /= cnt;

    return;
  } 

  
  makeDensity(one, cnt+1);
  makeDensity(two, cnt+1);
  
  wgt[cur] = wgt[one] + wgt[two];

  if (minWid.find(cur) == minWid.end()) minWid[cur] = vector<double>(dim, DBL_MAX);
  if (maxWid.find(cur) == maxWid.end()) maxWid[cur] = vector<double>(dim, 0.0);
  if (cen   .find(cur) == cen   .end()) cen[cur]    = vector<double>(dim);
  if (wid   .find(cur) == wid   .end()) wid[cur]    = vector<double>(dim);

  for(unsigned int k=0; k<dim; k++) {

    minWid[cur][k] = 
      (minWid[one][k] < minWid[two][k]) ? minWid[one][k] : minWid[two][k];
    
    maxWid[cur][k] = 
      (maxWid[one][k] > maxWid[two][k]) ? maxWid[one][k] : maxWid[two][k];
    
  } 
  
  double wone = wgt[one]/(wgt[one]+wgt[two]+DBL_EPSILON);
  double wtwo = wgt[two]/(wgt[one]+wgt[two]+DBL_EPSILON);

  kernel->Combine(this, wone, wtwo, cur, one, two);
}


void MetricTreeDensity::getDensity_test(ballPtr Root,
					vector<double>& point, double maxErr)
{
				// Direct evaluation for small # of pts
				// 
  if (Root->N() <= DirectSize || Root->Leaf()) { 

    dirDensity(Root, point);

  } else {

    vector<double> Kleft, Krght;

    ballPtr left = Root->leftKid;
    ballPtr rght = Root->rightKid;

    Kleft.push_back(kernel->minDistKer(this, left, point));
    Kleft.push_back(kernel->maxDistKer(this, left, point));
    Krght.push_back(kernel->minDistKer(this, rght, point));
    Krght.push_back(kernel->maxDistKer(this, rght, point));

    Kleft.push_back(0.5*(Kleft[0]+Kleft[1]));
    Krght.push_back(0.5*(Krght[0]+Krght[1]));

				// Drop the right child
    if (maxErr*Kleft[2]>Krght[2] || (Krght[1]-Krght[0])<maxErr*Krght[2]) {
      densMin += wgt[rght]*Krght[0];
      densMax += wgt[rght]*Krght[1];
				// Keep going
      getDensity(left, point, maxErr); 
				// Drop the left child
    } else if 
	(maxErr*Krght[2]>Kleft[2] || (Kleft[1]-Kleft[0])<maxErr*Kleft[2]) {
      densMin += wgt[left]*Kleft[0];
      densMax += wgt[left]*Kleft[1];
				// Keep going
      getDensity(rght, point, maxErr); 
				// Keep both children
    } else {			// 
      getDensity(left, point, maxErr); 
      getDensity(rght, point, maxErr); 
    }
  }

}


void MetricTreeDensity::getDensity(ballPtr Root,
				   vector<double>& point, double maxErr)
{
  double Kmin, Kmax, total;
    
  // find the minimum and maximum effect of these two subtrees on each other
  //
  Kmax = kernel->minDistKer(this, Root, point);
  Kmin = kernel->maxDistKer(this, Root, point);
  
  // take the minimum of data from the parent and add minimum for this ball
  //
  total = densMin + wgt[Root]*Kmin;
  
				// Check relative error
				// 
  if ( Kmax - Kmin <= maxErr * total) {                   
    densMin += wgt[Root] * Kmin;
    densMax += wgt[Root] * Kmax;
				// Direct evaluation
				// 
  } else if (Root->N() <= DirectSize) { 

    dirDensity(Root, point);

  } else {			// Walk the tree and getDensity the density
				// 
    if (!Root->Leaf()) {

      getDensity(Root->leftKid , point, maxErr); 
      getDensity(Root->rightKid, point, maxErr); 

    }
  }

}

//===================================================================
// Maybe we just want to getDensity this stuff directly.
//===================================================================
void MetricTreeDensity::dirDensity(ballPtr Root, vector<double>& point)
{
  set<ballPtr> Leaves;
  getLeaves(Root, Leaves);

  for (set<ballPtr>::iterator ii=Leaves.begin(); ii!=Leaves.end(); ii++) {

    double d = wgt[*ii] * kernel->maxDistKer(this, *ii, point);  

    densMin += d;
    densMax += d;
  }
  
}


void MetricTreeDensity::getDensity(vector< vector<double> >& Points, 
				   vector<double>& values, double maxErr)
{
  values.erase(values.begin(), values.end());
  for (indx i=0; i<Points.size(); i++) {
    densMin = 0.0;
    densMax = 0.0;
    getDensity(root, Points[i], maxErr);
    values.push_back(0.5*(densMin+densMax));
  }
}

