//=============================================================================
// BSPTree Class
//=============================================================================

#include <cmath>
#include <utility>
#include <map>
#include <vector>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <values.h>

#include <ACG.h>
#include <RndInt.h>
#include <Normal.h>
#include <BSPTree.h>
#include <gvariable.h>

BIE_CLASS_EXPORT_IMPLEMENT(LeafData)
BIE_CLASS_EXPORT_IMPLEMENT(BSPTree)

				// 95% confidence interval
double   BSPTree::lower_quant = 0.025;
double   BSPTree::upper_quant = 0.975;
				// Use the cell volume for quadrature
bool     BSPTree::full_volume = false;
bool     BSPTree::geom_volume = false;
				// Toggle based on filling factor
bool     BSPTree::fill_volume = false;
double   BSPTree::fill_factor = 0.5;
				// Slice dimensions
unsigned BSPTree::dim1        = 0;
unsigned BSPTree::dim2        = 1;
				// Probability scaling for Lebesgue
double   LeafData::exp_scale  = 0.0;

				// Random number for tagging files
RndIntPtr BSPTree::tags;

std::vector<double> BSPTree::lowerBound;
std::vector<double> BSPTree::upperBound;

BSPTree::BSPTree()
{
  //
  // Make random generator (these instances are static and only should
  // be initialized once)
  //
  if (tags.use_count() == 0) {
    std::ifstream urandom;
    uint64_t seed;
    // Get a random seed from Linux
    urandom.open("/dev/urandom");
    urandom.read(reinterpret_cast<char*> (&seed), sizeof(seed));
    urandom.close();
    // Make the generator
    if (BIEgen==0) BIEgen = new BIEACG(seed+myid, 25);
    tags = RndIntPtr(new RandomInteger(BIEgen));
  }
  vfrac = 1.0;
}


void BSPTree::LebesgueIntegral(std::map<int, double>& lower, 
			       std::map<int, double>& mean , 
			       std::map<int, double>& upper,
			       unsigned knots, 
			       unsigned dindx, unsigned long ncut,
			       std::vector< std::vector<double> >& data,
			       std::vector<unsigned>& mult,
			       bool logM, double dlogM, double dlogP,
			       bool linear)
{
  createLebesgueList(dindx, ncut, data, mult, linear);

  for (unsigned m=0; m<2; m++) {

    double dP, P, last, cur = LebesgueEval(m, LminP[m]);

    lower[m] = mean[m] = upper[m] = cur * LminP[m];

    if (logM) {
      LminP[m] = exp(std::max<double>(log(LminP[m]), log(LmaxP[m]) - dlogM));
      unsigned N = static_cast<unsigned>
	(floor((log(LmaxP[m]) - log(LminP[m]))/dlogP + 0.5));
      knots = std::max<unsigned>(knots, N);
      dP = (log(LmaxP[m]) - log(LminP[m]))/knots;
      cur *= LminP[m];
    } else {
      dP = (LmaxP[m] - LminP[m])/knots;
    }

    for (unsigned i=0; i<knots; i++) {
      last  =  cur;
      if (logM) {
	P   =  exp(log(LminP[m]) + dP*(i+1));
	cur =  LebesgueEval(m, P) * P;
      } else {
	P   =  LminP[m] + dP*(i+1);
	cur =  LebesgueEval(m, P);
      }

      lower[m] += last * dP;
      mean [m] += 0.5  * (cur + last) * dP;
      upper[m] += cur  * dP;
    }
  }

}

void BSPTree::LebesgueMeasure(std::vector<double>& Pval, 
			      std::vector<double>& Mval,
			      std::vector<double>& Yval, 
			      std::vector<double>& Erel, 
			      std::vector<double>& Eabs,
			      unsigned nval, unsigned dindx, unsigned long ncut,
			      std::vector< std::vector<double> >& data,
			      std::vector<unsigned>& mult,
			      int m, bool logM, double dlogM, double dlogP,
			      bool linear)
{
  createLebesgueList(dindx, ncut, data, mult, linear);
  ProcessMeasure(Pval, Mval, Yval, Erel, Eabs, LminP[m], LmaxP[m], 
		 nval, dindx, ncut, data, mult, m, logM, dlogM, dlogP);
}

void BSPTree::LebesgueHisto(std::vector<double>& P, 
			    std::vector< std::pair<double, double> >& V,
			    std::map<int, double>& histo,
			    unsigned dindx, unsigned long ncut,
			    std::vector< std::vector<double> >& data,
			    std::vector<unsigned>& mult, int m)
{
  unsigned number = std::max<unsigned>
    ( 1, floor((log(LmaxP[m]) - log(LminP[m]))/0.5) );
  P = std::vector<double>(number);
  V = std::vector< std::pair<double, double> >
    (number, std::pair<double, double>(0, 0));
  for (unsigned n=0; n<number; n++) P[n] = log(LminP[m]) + 0.5*(0.5 + n);
  
  for (std::multimap<double, LeafData>::iterator 
	 l = lebesgueLeaves[m].begin(); l != lebesgueLeaves[m].end(); l++) {
    
    int n = l->second.pvals.size();
    std::map<int, double>::iterator it = histo.find(n);
    double vol = l->second.volume(l->second.minP);
    if (it == histo.end())
      histo[n]  = vol;
    else
      histo[n] += vol;

    unsigned mm = std::min<unsigned>
      (floor( (log(l->second.maxP) - log(LminP[m]))/0.5 ), number - 1);
    
    V[mm].first  += vol;
    V[mm].second += vol * n;
  }

  for (unsigned n=0; n<number; n++) {
    if (V[n].first > 0.0) V[n].second /= V[n].first;
  }

}


void BSPTree::LebesgueMeasure(std::vector<double>& Pval, 
			      std::vector<double>& Mval,
			      std::vector<double>& Yval, 
			      std::vector<double>& Erel, 
			      std::vector<double>& Eabs,
			      double Pmin, double Pmax,
			      unsigned nval, unsigned dindx, unsigned long ncut,
			      std::vector< std::vector<double> >& data,
			      std::vector<unsigned>& mult, int m,
			      bool logM, double dlogM, double dlogP, 
			      bool linear)
{
  createLebesgueList(dindx, ncut, data, mult, linear);
  ProcessMeasure(Pval, Mval, Yval, Erel, Eabs, Pmin, Pmax, 
		 nval, dindx, ncut, data, mult, m, logM, dlogM, dlogP);
}


void BSPTree::ProcessMeasure(std::vector<double>& Pval, 
			     std::vector<double>& Mval,
			     std::vector<double>& Yval, 
			     std::vector<double>& Erel, 
			     std::vector<double>& Eabs,
			     double Pmin, double Pmax,
			     unsigned nval, unsigned dindx, unsigned long ncut,
			     std::vector< std::vector<double> >& data,
			     std::vector<unsigned>& mult, int m,
			     bool logM, double dlogM, double dlogP)
{
  if (logM) {
    Pmin = exp(std::max<double>(log(Pmin), log(Pmax) - dlogM));
    unsigned N = static_cast<unsigned>
      (floor((log(Pmax) - log(Pmin))/dlogP + 0.5));
    nval = std::max<unsigned>(nval, N);
  }

  Pval = std::vector<double>(nval);
  Mval = std::vector<double>(nval);
  Yval = std::vector<double>(nval);
  Erel = std::vector<double>(nval);
  Eabs = std::vector<double>(nval);

  if (logM) {
    double dP = (log(Pmax) - log(Pmin))/(nval-1), P;
    for (unsigned i=0; i<nval; i++) {
      Pval[i] =  log(Pmin) + dP*i;
      P       =  exp(Pval[i]);
      Mval[i] =  LebesgueEval(m, P) * P;
    }
  } else {
    double dP = (Pmax - Pmin)/(nval-1);
    for (unsigned i=0; i<nval; i++) {
      Pval[i] =  Pmin + dP*i;
      Mval[i] =  LebesgueEval(m, Pval[i]);
    }
  }

				// Nan check
  if (1) {
    for (unsigned i=0; i<nval; i++) {
      if (std::isnan(Pval[i]))
	std::cout << "Found nan: Pval i=" << i << "/" << nval << std::endl;
      if (std::isnan(Mval[i]))
	std::cout << "Found nan: Mval i=" << i << "/" << nval << std::endl;
    }
  }

  Yval[0] = LebesgueEval(m, Pmin)*Pmin;
  double ysum = Yval[0];
  for (unsigned i=1; i<nval; i++) {
    Yval[i] = 0.5*(Pval[i] - Pval[i-1])*(Mval[i] + Mval[i-1]);
    ysum += Yval[i];
  }

  Eabs[0] = 0.0;
  Eabs[1] = 0.25*(Pval[1] - Pval[0])*(Pval[1] - Pval[0])*
    ( (Mval[1] - Mval[0])/(Pval[1] - Pval[0]) - 
      (Mval[2] - Mval[0])/(Pval[2] - Pval[0]) );
  for (unsigned i=2; i<nval-2; i++) {
    Eabs[i] = 0.25*(Pval[i] - Pval[i-1])*(Pval[i] - Pval[i-1])*
      ( (Mval[i+0] - Mval[i-2])/(Pval[i+0] - Pval[i-2]) -
	(Mval[i+1] - Mval[i-1])/(Pval[i+1] - Pval[i-1]) );
  }
  Eabs[nval-1] = 
    0.25*(Pval[nval-1] - Pval[nval-2])*(Pval[nval-1] - Pval[nval-2])*
    ( (Mval[nval-1] - Mval[nval-3])/(Pval[nval-1] - Pval[nval-3]) - 
      (Mval[nval-1] - Mval[nval-2])/(Pval[nval-1] - Pval[nval-2]) );

  Erel[0] = 0.0;
  for (unsigned i=1; i<nval-1; i++) {
    Erel[i] = Eabs[i]/Yval[i];
  }

}

double BSPTree::LebesgueEval(int m, double y)
{
  if (0) {			// Debug check
    std::multimap<double, LeafData>::iterator l, lm;

    double dmin=lebesgueLeaves[m]. begin()->first;
    double dmax=lebesgueLeaves[m].rbegin()->first;

    l = lebesgueLeaves[m].lower_bound(y);

    if (l != lebesgueLeaves[m].begin()) {
      lm = l;
      lm--;
      if (lm != lebesgueLeaves[m].begin()) lm--;
      dmin = lm->first;
    }
    if (l != lebesgueLeaves[m].end()) {
	lm = l;
	lm++;
	dmax = lm->first;
    }
    if (dmin>y || y>dmax) {
      std::cout << "Range: [" << dmin << " : " << y << " : " << dmax << "]"
		<< " beg=" << lebesgueLeaves[m].begin()->first
		<< std::endl;
    }
  }

  double sum = 0.0;
  for (std::multimap<double, LeafData>::iterator
	 l = lebesgueLeaves[m].lower_bound(y); l != lebesgueLeaves[m].end(); l++)
    sum += l->second.volume(y);
    
  return sum;
}


struct lessdpr 
{
  //! Comparison function
  bool operator()
  (const std::pair<double, double> &a, const std::pair<double, double> &b) const
  { return a.first < b.first; }
};


double LeafData::volume(double p)
{
  if (pvals.size() == 0) return 0.0;
  if (p <= minP)         return vol;
  if (p >= maxP)         return 0.0;

  double xl, xh, yl, yh;
  size_t psiz = pvals.size();

  // Simply do a linear scan rather than a sorted search since
  // the number of points per bucket should be rather small

  if (psiz == 2) {
    xl = pvals[0].first;    yl = pvals[0].second;
    xh = pvals[1].first;    yh = pvals[1].second;

  } else {
    bool ok = false;
    for (size_t j=1; j<psiz; j++) {
      if (p<=pvals[j].first) {
	xl = pvals[j-1].first;	  yl = pvals[j-1].second;
	xh = pvals[j  ].first;	  yh = pvals[j  ].second;
	ok = true;
	break;
      }
    }
				// Assume the final interval if no
				// points are assigned so far
    if (!ok) {
      xl = pvals[psiz-2].first;   yl = pvals[psiz-2].second;
      xh = pvals[psiz-1].first;	  yh = pvals[psiz-1].second;
    }
  }

				// Linear interpolation in round off regime
  if (fabs(xh - xl)<1.0e-12) return vol*0.5*(yl + yh);

				// Do the linear interpolation
  if (fabs(exp_scale)<1.0e-8) {
    xl = log(xl);
    xh = log(xh);
    p  = log(p);
  } else {
    xl = pow(xl, exp_scale);
    xh = pow(xh, exp_scale);
    p  = pow(p,  exp_scale);
  }

  double d = (xh - xl);
  double a = (xh - p )/(xh - xl);
  double b = (p  - xl)/(xh - xl);

  // Sanity check
  if (std::isnan(d) || std::isnan(a) || std::isnan(b)) {
    std::cerr << "Found nan!" << std::endl;
  }

  return vol*( a*yl + b*yh );
}

double BSPTree::LebesgueVolume(int m)
{
  double sum = 0.0;
  for( std::multimap<double, LeafData>::iterator
	 l = lebesgueLeaves[m].begin(); l != lebesgueLeaves[m].end(); l++)
    sum += l->second.vol;
  return sum;
}

double BSPTree::MedianVolume(int m)
{
  std::vector<double> vols;
  for( std::multimap<double, LeafData>::iterator
	 l = lebesgueLeaves[m].begin(); l != lebesgueLeaves[m].end(); l++) 
    vols.push_back(l->second.vol);
  
  std::sort(vols.begin(), vols.end());
  
  if (vols.size())
    return vols[vols.size()/2];
  else
    return 0.0;
}


double BSPTree::EnclosedVolume()
{
  double vol = 1.0;
  for (unsigned i=0; i<dims; i++) vol *= scale[i];
  return vol;
}

void BSPTree::SetRangeBounds(std::vector<double>& lo, std::vector<double>& hi)
{
  lowerBound = lo;
  upperBound = hi;
}
