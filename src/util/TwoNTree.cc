//=============================================================================
// 2^n TreeClass
//=============================================================================

#include <values.h>
#include <mpi.h>

#include <map>
#include <cmath>
#include <cfloat>
#include <vector>
#include <string>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <utility>
#include <algorithm>

#include <BSPTree.h>
#include <TwoNTree.h>

// Histogram bins
//
const int ncnts   = 11;
const int vcnts[] = {16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384};

// Diagnostic cell/count histogram
//
std::map<size_t, std::vector<unsigned long> > TwoNTree::histo;
std::map<size_t, double>   TwoNTree::histR;
std::vector<unsigned long> TwoNTree::histoL;
std::vector<double>        TwoNTree::histoV;
std::vector<double>        TwoNTree::histoF;
std::vector<double>        TwoNTree::histoC;
bool                       TwoNTree::diag  = true;
bool                       TwoNTree::full  = false;
bool                       TwoNTree::zedge = false;
unsigned                   TwoNTree::ntree = 0;

typedef std::vector<unsigned> DU;
typedef std::vector<double>   DV;
typedef std::vector<DV>       DV2;

//
// This particle index flags the minimum probability value
//
const int topcat = INT_MAX;

TwoNTree::TwoNTree(int Nlev, unsigned long D, unsigned long N, unsigned nc,
		   const double* points_, 
		   const double* weight_, 
		   const double* beta_)
{
  nlev         = Nlev;
  dims         = D;
  num_points   = N;
  ncut         = std::max<unsigned>(1, nc);
  tree         = 0x0;
  
  points       = DV2(num_points);
  weight       = DV (num_points);
  beta         = DV (num_points);
  
  //
  // Assign the points and weights
  //
  for (indx i=0; i<num_points; i++) {
    weight[i] = weight_[i];
    beta  [i] = beta_  [i];
    for (unsigned j=0; j<dims; j++)
      points[i].push_back(points_[i*dims+j]);
  }
  
  //
  // Using manual or automatic bounds
  //
  if (lowerBound.size() == dims && upperBound.size() == dims) {
    lower = lowerBound;
    upper = upperBound;
  } else {
    makeBounds();
  }

  scale = DV(dims);
  for (indx i=0; i<dims; i++) scale[i] = upper[i] - lower[i];

  if (num_points > 0) {
    buildTree();
  }
}

TwoNTree::~TwoNTree()
{
  delete tree;
}

//
// Computes the box enclosing the data
// 
void TwoNTree::makeBounds()
{
  lower = DV(dims,  DBL_MAX);
  upper = DV(dims, -DBL_MAX);

  for (indx j=0; j<num_points; j++) {
    for (indx i=0; i<dims; i++) {
      lower[i] = std::min<double>(lower[i], points[j][i]);
      upper[i] = std::max<double>(upper[i], points[j][i]);
    }
  }

  lowerBound = lower;
  upperBound = upper;
}

double TwoNTree::getCellVolume(TNTree<unsigned>::listElem *elem)
{
  if (fill_volume) {
    double Vol = getEnclVolume(elem);
    double VOL = getFullVolume(elem);
    if (Vol/VOL > fill_factor) 
      return VOL;
    else
      return Vol;
  }
  if (full_volume) { return getFullVolume(elem); }
  if (geom_volume) { return sqrt(getFullVolume(elem)*getEnclVolume(elem)); }
  return getEnclVolume(elem);
}

double TwoNTree::getEnclVolume(TNTree<unsigned>::listElem *elem)
{
  int length = elem->pValues.size();
  if (length<2) { return vol * elem->pVol; }

  double VOL = 1.0;
  for (indx i=0; i<dims; i++) {
    double lower, upper;
    lower = upper = points[elem->pValues[0]][i];
    for (int j=1; j<length; j++) {
      lower = 
	std::min<double>(lower, points[elem->pValues[j]][i]);
      upper = 
	std::max<double>(upper, points[elem->pValues[j]][i]);
    }
    VOL *= upper - lower;
  }

  // If the enclosed volume is zero, assign the full volume
  if (VOL <= 0.0) { return vol * elem->pVol; }

  return VOL;
}

double TwoNTree::getFullVolume(TNTree<unsigned>::listElem *elem)
{
  return vol * elem->pVol;
}

void TwoNTree::buildTree()
{
  delete tree;

  // Make/Remake the grid
  //
  int grid = 1<<nlev;
  space = DV(dims);
  vol   = 1.0;
  for (indx i=0; i<dims; i++) {
    space[i] = (upper[i] - lower[i])/grid;
    vol *= upper[i] - lower[i];
  }
    
  if (vol <= 0.0) 
    throw std::string("At least one zero width dimension in buildTree.\n"
		      "This probably results from dependent states.\n");

  const unsigned zero = 0;
  tree = new TNTree<unsigned>(grid, dims, ncut, zero);

  std::vector<int> index(dims);
  for (indx j=0; j<num_points; j++) {

    bool ok = true;		// Make sure point is in the volume
    for (indx i=0; i<dims; i++) {
      if (points[j][i]>=upper[i] || points[j][i]<lower[i]) ok = false;
    }
				// Is it?
    if (ok) {
      for (indx i=0; i<dims; i++) {
	index[i] = std::min<int>
	  (grid-1, static_cast<int>(floor((points[j][i] - lower[i])/space[i])));
      }
      tree->add(index, j);
    }
  }

  if (diag) {
    tree->Reset();
    TNTree<unsigned>::listElem *elem;

    while ((elem = tree->Next())) {
      int length  = elem->pValues.size();
      int pcount  = elem->pCount;
      double ffac = getEnclVolume(elem)/getFullVolume(elem);

      if (histo.find(length) == histo.end()) {
	histo[length] = std::vector<unsigned long>(ncnts+2, 0);
      }
      int n = ncnts;
      for (int i=0; i<ncnts; i++) {
	if (pcount < vcnts[i]) {
	  n = i;
	  break;
	}
      }
      histo[length][n]++;
      histo[length][ncnts+1] += elem->pLevel;
      histR[length]          += ffac;

      if (histoL.size() != static_cast<unsigned>(nlev+1))
	histoL = std::vector<unsigned long>(nlev+1, 0);

      if (histoV.size() != static_cast<unsigned>(nlev+1))
	histoV = DV(nlev+1, 0);

      if (histoF.size() != static_cast<unsigned>(nlev+1))
	histoF = DV(nlev+1, 0);

      if (histoC.size() != static_cast<unsigned>(nlev+1))
	histoC = DV(nlev+1, 0);

      if (elem->pLevel < static_cast<unsigned>(nlev+1)) {
	histoL[elem->pLevel] ++;
	histoV[elem->pLevel] += getCellVolume(elem);
	histoF[elem->pLevel] += ffac;
	histoC[elem->pLevel] += length;
      } else {
	std::cout << "Error" << std::endl;
      }
    }
    ntree++;
  }
    
}

void TwoNTree::Integral(DV& result, DV (*func)(DV&))
{
  DV ctr(dims);

  tree->Reset();

  TNTree<unsigned>::listElem *elem;
  while ((elem=tree->Next())) {
				// Compute the geometric center of the
				// cell
    for (unsigned j=0; j<dims; j++) 
      ctr[j] = lower[j] + space[j]*(0.5 + elem->pIndex[j]);
      
    DV ret = (*func)(ctr);
    for (unsigned i=0; i<result.size(); i++) 
      result[i] += ret[i] * getCellVolume(elem);
  }
}

//
// Calculate the statistics of level "root" based on the statistics of
// its left and right children.
//
void TwoNTree::Integral(std::ostream& out, int dim, DV (*func)(DV&))
{
  DV result(dim, 0.0);

  Integral(result, func);
  out << "Result=";
  for (int j=0; j<dim; j++) out << std::setw(12) << std::left << result[j];
  out << std::endl;
}

void TwoNTree::IntegralEval(DV2& data, DU& mult,
			    std::map<int, DV >& result,
			    std::map<int, DV >& lower, 
			    std::map<int, DV >& upper,
			    std::map<int, DV >& vmean)
{
  sz = data[0].size();
  
  for (unsigned l=0; l<result.size(); l++) {
    for (unsigned n=0; n<result[l].size(); n++)
      result[l][n] = lower[l][n] = upper[l][n] = vmean[l][0] = 0.0;
  }

  if (full) completeVolume(data);

  IntegralPC(data, mult, result, lower, upper, vmean);
}

class nodeInfo {
public:
  unsigned long nodes;
  unsigned long N, minN, maxN;
  double meanN, minV, maxV, meanV, vvol;
  DV2 upper;
  DV2 lower;
  DV  value;

  nodeInfo() 
  {
    nodes = N = 0;
    minN = MAXLONG;
    maxN = 0;
    meanN = meanV = vvol = 0;
    minV = MAXDOUBLE;
    maxV = 0;
  }
};


void TwoNTree::IntegralPC(DV2& data, DU& mult,
			  std::map<int, DV >& medn,
			  std::map<int, DV >& lowr,
			  std::map<int, DV >& uppr,
			  std::map<int, DV >& mean)

{
  //------------------------------------------------------------
  // Iterate through the leaf cells
  //------------------------------------------------------------

  tree->Reset();
  
  TNTree<unsigned>::listElem *elem;

  while ((elem=tree->Next())) {

    unsigned length = elem->pValues.size();
    
    //------------------------------------------------------------
    // Compute the populations and pooled list
    //------------------------------------------------------------
    std::map<double, double> pops;
    std::map<double, double>::iterator jt;

    for (unsigned i=0; i<length; i++) {
      int k = elem->pValues[i];
      if (k<topcat) {
	if ( (jt = pops.find(beta[k])) == pops.end()) {
	  pops[beta[k]] = 1;
	} else {
	  jt->second += 1;
	}
      }
    }
      
    dpair pmax;
    for (jt = pops.begin(); jt != pops.end(); jt++) {
      if      (jt == pops.begin())     pmax = *jt;
      else if (jt->second>pmax.second) pmax = *jt;
    }

    //------------------------------------------------------------

    for (unsigned j=0; j<sz; j++) {
				// Sort on dimension "j" for computing
				// specified quantiles
      DV2 v(2);
      for (unsigned i=0; i<length; i++) {
	int k = elem->pValues[i];
	
	if (k<topcat) {
	  for (unsigned n=0; n<mult[k]; n++) {
	    if (beta[k] == pmax.first) v[0].push_back(data[k][j]);
	    v[1].push_back(data[k][j]);	
	  }
	} else {
	  v[0].push_back(DBL_MIN);
	  v[1].push_back(DBL_MIN);
	}
      }

      std::sort(v[0].begin(), v[0].end());
      std::sort(v[1].begin(), v[1].end());
    
      double Vol = getCellVolume(elem);
      unsigned lo, md, hi;

      for (unsigned l=0; l<2; l++) {
	if (v[l].size()==0) continue;
	
	lo = std::min<unsigned>
	  (floor(v[l].size()*lower_quant+0.5), v[l].size()-1);
	lo = std::max<unsigned>(lo, 0);
	
	md = std::min<unsigned>
	  (floor(v[l].size()*0.5+0.5), v[l].size()-1);
	md = std::max<unsigned>(md, 0);

	hi = std::min<unsigned>
	  (floor(v[l].size()*upper_quant+0.5), v[l].size()-1);
	hi = std::max<unsigned>(hi, 0);
	
	if (v[l][md] <= 0.0) {
	  std::cout << "Crazy prob: " << v[l][md] << std::endl;
	}

	if (Vol < 0.0) {
	  std::cout << "Crazy volm: " << Vol << std::endl;
	}

	medn[l][j] += v[l][md]* Vol;
	
	for (size_t k=0; k<v[l].size(); k++) 
	  mean[l][j] += v[l][k]/v[l].size() * Vol;
	
	lowr[l][j] += v[l][lo] * Vol;
	
	uppr[l][j] += v[l][hi] * Vol;
      }
	
    }
  }
}


void TwoNTree::createLebesgueList(unsigned dindx, unsigned long ncut,
				  DV2& data, DU& mult, bool linear)
{
  if (full) completeVolume(data, dindx);

  //------------------------------------------------------------
  // Clean any existing data
  //------------------------------------------------------------
  lebesgueLeaves.erase(lebesgueLeaves.begin(), lebesgueLeaves.end());

  for (unsigned l=0; l<2; l++) {
    LminP[l] =  MAXDOUBLE;
    LmaxP[l] = -MAXDOUBLE;
  }
  
  LebesgueList(dindx, ncut, data, mult, linear);
}

void TwoNTree::LebesgueList(unsigned dindx, unsigned long ncut,
			    DV2& data, DU& mult, bool linear)
{
  //------------------------------------------------------------
  // Iterate through the leaf cells
  //------------------------------------------------------------

  tree->Reset();
  
  TNTree<unsigned>::listElem *elem;

  while ((elem=tree->Next())) {
    
    unsigned length = elem->pValues.size();

    //------------------------------------------------------------
    // Compute the weights
    //------------------------------------------------------------
    std::map<double, double> pops;
    std::map<double, double>::iterator jt;

    for (unsigned i=0; i<length; i++) {
      int k = elem->pValues[i];
      if (k<topcat) {
	if ( (jt = pops.find(beta[k])) == pops.end()) {
	  pops[beta[k]] = 1;
	} else {
	  jt->second += 1;
	}
      }
    }
      
    dpair pmax;
    for (jt = pops.begin(); jt != pops.end(); jt++) {
      if      (jt == pops.begin())     pmax = *jt;
      else if (jt->second>pmax.second) pmax = *jt;
    }

    //------------------------------------------------------------
    // Make the data list
    //------------------------------------------------------------

    DV2 pv(2);

    for (unsigned i=0; i<length; i++) {
      int k = elem->pValues[i];

      if (k<topcat) {
	for (unsigned n=0; n<mult[k]; n++) {
	  if (beta[k] == pmax.first) pv[0].push_back(data[k][dindx]);
	  pv[1].push_back(data[k][dindx]);
	}
      } else {
	pv[0].push_back(DBL_MIN);
	pv[1].push_back(DBL_MIN);
      }
    }

      
    //------------------------------------------------------------
    // Insert the data
    //------------------------------------------------------------

    for (unsigned l=0; l<2; l++) {
      if (pv[l].size() == 0) continue;

      std::sort(pv[l].begin(), pv[l].end());

      LeafData p;
				// Fiducial volume in this cell
      p.vol   = getCellVolume(elem);
				// Minimum prob value
      p.minP  = pv[l].front();
				// Median prob value
      unsigned nmed = std::min<unsigned>
	(floor(pv[l].size()*0.5+0.5), pv[l].size()-1);
      nmed = std::max<unsigned>(nmed, 0);

      p.medP  = pv[l][nmed];
				// Maximum prob value
      p.maxP  = pv[l].back();
				// The probability values
      if (pv[l].size()>1) {
	if (pv[l].back() > pv[l].front()) {
	  double delta, val;
	  if (linear) delta = 1.0/(pv[l].back() - pv[l].front());
	  else        delta = 1.0/(log(pv[l].back()) - log(pv[l].front()));
	  p.pvals.push_back(dpair(pv[l][0], 1.0));
	  for (unsigned i=1; i<pv[l].size()-1; i++) {
	    if (linear) val = (pv[l].back() - pv[l][i])*delta;
	    else        val = (log(pv[l].back()) - log(pv[l][i]))*delta;
	    p.pvals.push_back(dpair(pv[l][i], val));
	  }
	} else {
	  double delta = 1.0/pv[l].size();
	  for (unsigned i=0; i<pv[l].size()-1; i++) {
	    p.pvals.push_back(dpair(pv[l][i], 1.0 - delta*i));
	  }
	}
	p.pvals.push_back(dpair(pv[l].back(), 0.0));
      } else {
	p.pvals.push_back(dpair(pv[l].back(), 1.0));
      }
    
				// Key and sort by max probability
				// value. "lebesgueLeaves" is a
				// std::multimap so many elements can
				// have the same key.

      lebesgueLeaves[l].insert(std::pair<double, LeafData>(p.maxP, p));
      
				// Tabulate global extrema

      LminP[l] = std::min<double>(LminP[l], p.medP);
      LmaxP[l] = std::max<double>(LmaxP[l], p.medP);
    }
  }
}

void TwoNTree::diagDump(MPI_Comm COMM, int nlev)
{
  //------------------------------------------------------------
  // Debugging output
  //------------------------------------------------------------

  if (diag) {		

    int numprocs, myid;
    MPI_Comm_size(COMM, &numprocs);
    MPI_Comm_rank(COMM, &myid);

    // Accumulated values from all nodes
    std::map<size_t, std::vector<unsigned long> > histo0;
    std::map<size_t, double> histR0;
    std::vector< unsigned long > histoL0;
    std::vector< double > histoV0, histoF0, histoC0;
    int ntree0(0);

    if (myid==0) {
      histo0  = histo;
      histR0  = histR;
      histoL0 = histoL;
      histoV0 = histoV;
      histoF0 = histoF;
      histoC0 = histoC;
      ntree0  = ntree;
    }
    
    if (numprocs>1) {

      //------------------------------------------------------------
      // Gather up data from all nodes
      //------------------------------------------------------------

      unsigned cnt;
      unsigned ntree1;
      MPI_Status status;
      std::map<size_t, std::vector<unsigned long> >::iterator it;
      std::map<size_t, double>::iterator iu;
      
      for (int n=1; n<numprocs; n++) {
	
	if (myid==n) {
	  MPI_Send(&ntree, 1, MPI_UNSIGNED, 0, 110, COMM);
	  
	  if (ntree) {
	    cnt = histo.size();
	    MPI_Send(&cnt, 1, MPI_UNSIGNED, 0, 111, COMM);
	    
	    if (cnt) {
	      DU dat1(cnt);
	      std::vector<unsigned long> dat2(cnt*(ncnts+2));
	      std::vector<double>        dat3(cnt);
	      it = histo.begin();
	      iu = histR.begin();
	      for (unsigned i=0; i<cnt; i++) {
		dat1[i] = it->first;
		for (int j=0; j<ncnts+2; j++)
		  dat2[i*(ncnts+2)+j] = it->second[j];
		it++;
		dat3[i] = iu->second;
		iu++;
	      }
	      MPI_Send(&dat1[0],   cnt,    MPI_UNSIGNED,      0, 112, COMM);
	      MPI_Send(&dat2[0],   cnt*(ncnts+2), 
		                           MPI_UNSIGNED_LONG, 0, 113, COMM);
	      MPI_Send(&dat3[0],   cnt,    MPI_DOUBLE,        0, 114, COMM);
	      MPI_Send(&histoL[0], nlev+1, MPI_UNSIGNED_LONG, 0, 115, COMM);
	      MPI_Send(&histoV[0], nlev+1, MPI_DOUBLE,        0, 116, COMM);
	      MPI_Send(&histoF[0], nlev+1, MPI_DOUBLE,        0, 117, COMM);
	      MPI_Send(&histoC[0], nlev+1, MPI_DOUBLE,        0, 118, COMM);
	    } 
	  }
	}
	
	if (myid==0) {
	  MPI_Recv(&ntree1, 1, MPI_UNSIGNED, n, 110, COMM, &status);
	  
	  if (ntree1) {
	    MPI_Recv(&cnt, 1, MPI_UNSIGNED, n, 111, COMM, &status);
	    
	    if (cnt) {
	      std::vector<int>           dat1(cnt);
	      std::vector<unsigned long> dat2(cnt*(ncnts+2));
	      std::vector<double>        dat3(cnt);
	      std::vector<unsigned long> dat4(nlev+1);
	      std::vector<double>        dat5(nlev+1);
	      std::vector<double>        dat6(nlev+1);
	      std::vector<double>        dat7(nlev+1);
	      
	      MPI_Recv(&dat1[0], cnt,    MPI_UNSIGNED,      n, 112, 
		       COMM, &status);
	      MPI_Recv(&dat2[0], cnt*(ncnts+2), 
		                         MPI_UNSIGNED_LONG, n, 113, 
		       COMM, &status);
	      MPI_Recv(&dat3[0], cnt,    MPI_DOUBLE,        n, 114, 
		       COMM, &status);
	      MPI_Recv(&dat4[0], nlev+1, MPI_UNSIGNED_LONG, n, 115,
		       COMM, &status);
	      MPI_Recv(&dat5[0], nlev+1, MPI_DOUBLE,        n, 116,
		       COMM, &status);
	      MPI_Recv(&dat6[0], nlev+1, MPI_DOUBLE,        n, 117,
		       COMM, &status);
	      MPI_Recv(&dat7[0], nlev+1, MPI_DOUBLE,        n, 118,
		       COMM, &status);
	      
	      for (unsigned i=0; i<cnt; i++) {
		if (histo0.find(dat1[i]) == histo0.end())
		  histo0[dat1[i]] = std::vector<unsigned long>(ncnts, 0);
		for (int j=0; j<ncnts+2; j++)
		  histo0[dat1[i]][j] += dat2[i*(ncnts+2)+j];
		if (histR0.find(dat1[i]) == histR0.end())
		  histR0[dat1[i]]  = dat3[i];
		else
		  histR0[dat1[i]] += dat3[i];
	      }
	      
	      for (int i=0; i<nlev+1; i++) {
		histoL0[i] += dat4[i];
		histoV0[i] += dat5[i];
		histoF0[i] += dat6[i];
		histoC0[i] += dat7[i];
	      }

	      ntree0 += ntree1;
	    }
	  }
	}
      }
    }

    if (myid==0) {
				// Make a file with a random suffix
      const std::string bname("twotree.debug.");
      std::ostringstream sout;
      sout << bname << getRandomFileTag();
      std::ofstream out(sout.str().c_str());

      if (out) {
	std::map<size_t, std::vector<unsigned long> >::iterator it;
	std::map<size_t, double>::iterator iu;
	double sum = 0, acc = 0, csum;
	
	for (it=histo0.begin(); it!=histo0.end(); it++) {
	  csum = 0;
	  for (int j=0; j<=ncnts; j++) csum += it->second[j];
	  sum += csum/ntree0;
	}
	
	std::ostringstream sout;
	out << "# Number of trees      = " << ntree0 << std::endl;
	out << "# Mean number of cells = " << sum    << std::endl;
	out << std::endl;
	out << "# " << std::setw(10) << std::right << "#pts/cell"
	    << std::setw(15) << "Avg. cell# |"
	    << std::setw(15) << "Acc. cell# |"
	    << std::setw(15) << "Fractional |"
	    << std::setw(15) << "Avg. level |"
	    << std::setw(15) << "Vol. ratio |";
	for (int j=0; j<ncnts; j++) {
	  sout.str("");
	  sout << vcnts[j] << " |";
	  out << std::setw(15) << std::right << sout.str();
	}
	sout.str("");
	sout << ">" << vcnts[ncnts-1] << " |";
	out << std::setw(15) << std::right << sout.str()<< std::endl;
	out << "# " << std::setw(10) << std::right << "[1] |"
	    << std::setw(15) << "[2] |"
	    << std::setw(15) << "[3] |"
	    << std::setw(15) << "[4] |"
	    << std::setw(15) << "[5] |";
	for (int j=0; j<=ncnts+1; j++) {
	  sout.str("");
	  sout << "[" << j+6 << "] |";
	  out << std::setw(15) << std::right << sout.str();
	}
	out << std::endl;
	    
	out << "# " << std::setw(10) << std::right << "------|";
	for (int j=0; j<=ncnts+5; j++)
	  out << std::setw(15) << std::right << "------|";
	out << std::endl;

	for (it=histo0.begin(), iu=histR0.begin(); it!=histo0.end(); 
	     it++, iu++) {
	  csum = 0;
	  for (int j=0; j<=ncnts; j++) csum += it->second[j];
	  acc += csum*it->first/ntree0;
	  out << std::right 
	      << std::setw(12) << it->first
	      << std::setw(15) << csum/ntree0
	      << std::setw(15) << acc
	      << std::setw(15) << acc/sum
	      << std::setw(15) << static_cast<double>(it->second[ncnts+1])/csum
	      << std::setw(15) << iu->second/csum;
	  for (int j=0; j<=ncnts; j++)
	    out << std::setw(15) << static_cast<double>(it->second[j])/ntree0;
	  out << std::endl;
	}

	out << std::endl
	    << std::endl     << "# Number of levels     = " << nlev 
	    << std::endl
	    << std::endl     << std::right << "# "
	    << std::setw(12) << "Level |"
	    << std::setw(12) << "# Cells |"
	    << std::setw(18) << "Total volume |"
	    << std::setw(18) << "Volume/cell |"
	    << std::setw(18) << "Volume fill |"
	    << std::setw(18) << "Mean occ |"
	    << std::endl;

	out << "# "
	    << std::setw(12) << "-------"
	    << std::setw(12) << "---------"
	    << std::setw(18) << "--------------"
	    << std::setw(18) << "-------------"
	    << std::setw(18) << "-------------"
	    << std::setw(18) << "----------"
	    << std::endl;

	unsigned long sumcnt = 0;
	double        totvol = 0.0;

	for (int j=0; j<nlev+1; j++) {
	  out << "  " 
	      << std::setw(12) << j
	      << std::setw(12) << histoL0[j] 
	      << std::scientific
	      << std::setw(18) << histoV0[j]
	      << std::setw(18) << (histoL0[j] ? histoV0[j]/histoL0[j] : 0.0)
	      << std::setw(18) << (histoL0[j] ? histoF0[j]/histoL0[j] : 0.0)
	      << std::fixed
	      << std::setw(18) << (histoC0[j] ? histoC0[j]/histoL0[j] : 0.0)
	      << std::endl;
	  
	  sumcnt += histoL0[j];
	  totvol += histoV0[j];
	}

	out << "# "
	    << std::setw(12) << "------"
	    << std::setw(12) << "------"
	    << std::setw(18) << "------------"
	    << std::setw(18) << "------------"
	    << std::setw(18) << "------------"
	    << std::setw(18) << "------"
	    << std::endl;
	out << "# "
	    << std::setw(12) << "Totals"
	    << std::setw(12) << sumcnt
	    << std::scientific
	    << std::setw(18) << totvol
	    << std::fixed
	    << std::endl;

      }
    }
  }
}


void TwoNTree::gnuplotDemo(const DV2& data, const DU& mult, const DV& ctrs,
			   const unsigned dindx, const unsigned long ncut)
{
  if (dims<2) return;

  std::ostringstream ostr;
  ostr << "vta_cells_2n." << getRandomFileTag() << ".gpl";
  std::ofstream out(ostr.str().c_str());

  if (out) {

    // Check dimensions
    if (dim1 == dim2 || dim1 >= dims || dim2 >= dims) {
      dim1 = 0;
      dim2 = 1;
    }
    
    tree->Reset();
  
    // Make range width for slice
    DV dels(dims);
    for (unsigned d=0; d<dims; d++) {
      if (d == dim1) continue;
      if (d == dim2) continue;
      dels[d] = upper[d] - lower[d];
    }

    DV2 val;
    DV  eval(5);
    std::vector<int> max_index(dims);
    double maxP=-DBL_MAX;

    TNTree<unsigned>::listElem *elem;
    while ((elem=tree->Next())) {

      unsigned length = elem->pValues.size();
      DV pv;

      for (unsigned i=0; i<length; i++) {
	int k = elem->pValues[i];

	if (k < topcat) {
	  for (unsigned n=0; n<mult[k]; n++)
	    pv.push_back(data[k][dindx]);

	  if (maxP<data[k][dindx]) {
	    maxP = data[k][dindx];
	    max_index = elem->pIndex;
	    for (unsigned k=0; k<dims; k++) {
	      max_index[k] <<= (nlev - elem->pLevel);
	      max_index[k] +=  (1 << (nlev-elem->pLevel))/2;
	    }
	  }
	} else {
	  pv.push_back(DBL_MIN);	  
	}

      }
      
      bool ok = true;		// Is the cell in the desired slice?
      for (unsigned d=0; d<dims; d++) {
	if (d == dim1) continue;
	if (d == dim2) continue;
	double del = dels[d]/elem->pSize;
	double low = lower[d] + del*elem->pIndex[d];
	if (low > ctrs[d] || low + del < ctrs[d]) ok = false;
      }

      if (ok) {
	eval[0] = pv[pv.size()/2];
      
	double delx = (upper[dim1] - lower[dim1])/elem->pSize;
	double dely = (upper[dim2] - lower[dim2])/elem->pSize;
      
	eval[1] = lower[dim1] + delx*elem->pIndex[dim1];
	eval[2] = lower[dim2] + dely*elem->pIndex[dim2];
	eval[3] = eval [1] + delx;
	eval[4] = eval [2] + dely;
	
	val.push_back(eval);
      }
    }
    
    double vi=0.0, vf=0.0, delv = 1.0;

    if (val.size()) {

      sort(val.begin(), val.end());

      vi   = val.front()[0];
      vf   = val.back()[0];
      delv = vf - vi;
      
      if (delv<=0.0) delv = 1.0;

      for (unsigned n=0; n<val.size(); n++) {
	out << "set obj rect from " 
	    << val[n][1] << "," << val[n][2] << " to "
	    << val[n][3] << "," << val[n][4] << " front fc palette frac "
	    << std::fixed << (val[n][0] - vi)/delv << " lw 0" << std::endl;
      }
    }
    
    out << "set term wxt enhanced" << std::endl;
    out << "set xlabel 'x_1'"      << std::endl;
    out << "set ylabel 'x_2'"      << std::endl;
    out << "set pm3d map"          << std::endl;
    out << "set cbrange [0:1]"     << std::endl;
    out << "set colorbox"          << std::endl;
    out << "splot [" 
	<< lower[dim1] << ":" << upper[dim1] << "] ["
	<< lower[dim2] << ":" << upper[dim2] << "] 0*x*y t'' lw 0" << std::endl;

    out.close();
    out.open("slice.dat");

    //
    // Make a slice in dimensions 1 and 2
    //
    if (out) {

      out << std::scientific;

      const unsigned num = 100;
      double delx = (upper[dim1] - lower[dim1])/num, x;
      double dely = (upper[dim2] - lower[dim2])/num, y;

      int grid = 1<<nlev;
      std::vector<int> index = max_index;

      index = std::vector<int>(dims, grid/2);

      out << "# Max index=[";
      for (unsigned n=0; n<dims; n++)
	out << max_index[n] << (n<dims-1 ? ", " : "]");
      out << std::endl;

      //
      // Make the array slice in the first two dimensions
      //
      for (unsigned j=0; j<=num; j++) {
	y = lower[dim2] + dely*j;

	for (unsigned i=0; i<=num; i++) {
	  x = lower[dim1] + delx*i;

	  out << std::setw(16) << x << std::setw(16) << y;
	  
	  index[dim1] = std::min<int>
	    (grid-1, static_cast<int>(floor((x - lower[dim1])/space[dim1])));

	  index[dim2] = std::min<int>
	    (grid-1, static_cast<int>(floor((y - lower[dim2])/space[dim2])));
	  
	  double eval = -0.01;
	  if ((elem = tree->find(index))) {

	    unsigned length = elem->pValues.size();

	    DV pv;
	    for (unsigned i=0; i<length; i++) {
	      int k = elem->pValues[i];

	      if (k<topcat) {
		for (unsigned n=0; n<mult[k]; n++)
		  pv.push_back(data[k][dindx]);
	      } else {
		pv.push_back(DBL_MIN);
	      }

	    }
	    eval = (pv[pv.size()/2] - vi)/delv;
	  }
	  
	  out << std::setw(16) << eval << std::endl;

	}
	out << std::endl;
      }
    }
  }
}


void TwoNTree::LevelList(std::ostream& out, bool limits)
{
  std::map<unsigned, nodeInfo> llist;
  std::map<unsigned, nodeInfo>::iterator itl;
  std::set< std::pair<indx, unsigned> > leaves;
  unsigned long totalN = 0, mm, l, n;
  TNTree<unsigned>::listElem* it;
  double v;
  
  tree->Reset();
  while ((it = tree->Next())) {

    // The level
    l = it->pLevel;

    // Number of points
    n = it->pValues.size();

    // Volume
    v = it->pVol;

    // Points per node summary stats
    llist[l].minN  = std::min<unsigned long>(llist[l].minN, n);
    llist[l].maxN  = std::max<unsigned long>(llist[l].maxN, n);
    llist[l].meanN = 
      (llist[l].meanN*llist[l].nodes + n)/(llist[l].nodes+1);
    
    
    // Volume per node summary stats
    llist[l].vvol  = v;
    llist[l].minV  = std::min<double>(llist[l].minV, v);
    llist[l].maxV  = std::max<double>(llist[l].maxV, v);
    llist[l].meanV = 
      (llist[l].meanV*llist[l].nodes + v)/(llist[l].nodes+1);
    
    // Per level stats
    llist[l].nodes++;
    llist[l].N += n;
    totalN     += n;
    
    // Verbose cell limit dump
    if (limits) {
      DV lowr(dims), uppr(dims);
      for (unsigned k=0; k<dims; k++) {
	lowr[k] = lower[k] + space[k]*(it->pIndex[k] + 0);
	uppr[k] = lower[k] + space[k]*(it->pIndex[k] + 1);
      }
      
      llist[l].lower.push_back(lowr);
      llist[l].upper.push_back(uppr);
    }
  }
  
  out << std::endl 
      << "-----------------------" << std::endl
      << "2^N tree build analysis" << std::endl
      << "-----------------------" << std::endl
      << std::endl;
  
  out << std::setw(4) << "Lev " << " | "
      << std::setw(8) << "Nodes"
      << std::setw(8) << "# pts"
      << std::setw(6) << "MinN"
      << std::setw(6) << "MaxN"
      << std::setw(6) << "MeanN"
      << std::setw(9) << "Vol"
      << std::setw(9) << "MinV"
      << std::setw(9) << "MeanV"
      << std::setw(9) << "MaxV";
  if (limits) {
    out << std::setw(9) << "CumV";
    std::ostringstream sout;
    for (indx j=0; j<dims; j++) {
      sout.str(""); sout << "Min #" << j+1;
      out << std::setw(12) << sout.str();
      sout.str(""); sout << "Max #" << j+1;
      out << std::setw(12) << sout.str();
    }
  }
  out << std::endl
      << std::setw(4) << "+---" << " | "
      << std::setw(8) << "+------"
      << std::setw(8) << "+------"
      << std::setw(6) << "+----"
      << std::setw(6) << "+----"
      << std::setw(6) << "+----"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------";
  if (limits) {
    out << std::setw(9) << "+-------";
    for (indx j=0; j<dims; j++) {
      out << std::setw(12) << "+---------"
	  << std::setw(12) << "+---------";
    }
  }
  out << std::endl;
  
  double cumv=0.0, totv=0.0;
  if (limits)
    for (itl=llist.begin(); itl!=llist.end(); itl++) totv += itl->second.vvol;
  
  unsigned old_prec = out.precision(2);
  
  for (itl=llist.begin(); itl!=llist.end(); itl++) {
    mm = static_cast<unsigned long>(floor(itl->second.meanN));
    out << std::setw(4) << itl->first << " | "
	<< std::setw(8) << itl->second.nodes
	<< std::setw(8) << itl->second.N
	<< std::setw(6) << itl->second.minN
	<< std::setw(6) << itl->second.maxN
	<< std::setw(6) << mm
	<< std::setw(9) << itl->second.vvol
	<< std::setw(9) << itl->second.minV
	<< std::setw(9) << itl->second.meanV
	<< std::setw(9) << itl->second.maxV;
    if (limits) {
      cumv += itl->second.vvol;
      out << std::setw(9) << cumv/totv;
      // out.setf(std::ios::scientific);    // Set scientific format
      unsigned prec = out.precision(3);     // Set precision
      for (indx j=0; j<dims; j++)
	out << std::setw(12) << itl->second.lower[0][j]
	    << std::setw(12) << itl->second.upper[0][j];
      for (indx k=1; k<itl->second.lower.size(); k++) {
	out << std::endl << std::setw(4) << "*" << " | "
	    << std::setw(8) << "." << std::setw(8) << "."
	    << std::setw(6) << "." << std::setw(6) << "."
	    << std::setw(6) << "." << std::setw(9) << "."
	    << std::setw(9) << "." << std::setw(9) << "."
	    << std::setw(9) << "." << std::setw(9) << ".";
	for (indx j=0; j<dims; j++)
	  out << std::setw(12) << itl->second.lower[k][j]
	      << std::setw(12) << itl->second.upper[k][j];
      }
      // out.unsetf(std::ios::scientific); // Restore original format
      out.precision(prec);	           // Restore precision
    }
    out << std::endl;
  }
  out << std::endl;
  out << "  Total # nodes=" << leaves.size()
      << "  Total # point=" << totalN
      << std::endl << std::endl;
  
  out.precision(old_prec);
}



/**
   Iterate through the frontier and force parents to have all their
   children
*/
void TwoNTree::completeVolume
(std::vector< std::vector<double> >& data, unsigned dindx)
{
  //------------------------------------------------------------
  // Find all minimum values
  //------------------------------------------------------------
  tree->computeMinValue(data, dindx);

  //------------------------------------------------------------
  // Begat missing nodes from root node down
  //------------------------------------------------------------
  walkComplete(tree->root());

}

void TwoNTree::walkComplete(TNTree<unsigned>::NodePtr n)
{
  if (!n.get()) return;
  if (n->type() == TNTree<unsigned>::LeafNode) return;

  boost::shared_ptr<TNTree<unsigned>::Branch> b = 
    boost::static_pointer_cast<TNTree<unsigned>::Branch>(n);
  
  //------------------------------------------------------------
  // Find the lowest probability value and missing children
  //------------------------------------------------------------
  int ntot = 1<<b->dim_;
  int ncomplete = 0;
  double cvol   = 0.0;

  for ( int i = 0; i < ntot; ++i ) {
    if ( b->child(i) ) {
      walkComplete(b->child(i));
      if (ncomplete==0) cvol = b->child(i)->Volume();
    } else {
      ncomplete++;
    }
  }

  if (ncomplete) {
    TNTree<unsigned>::LeafPtr p = 
      TNTree<unsigned>::
      LeafPtr(new TNTree<unsigned>::Leaf(b->leaf_, n, ncomplete*cvol));
    p->minv_ = b->minv_;
    tree->frontier[p.get()] = p;
  }
}

void TwoNTree::checkLevel(TNTree<unsigned>::NodePtr n, unsigned l)
{
  if (n->type() == TNTree<unsigned>::LeafNode) return;

  boost::shared_ptr<TNTree<unsigned>::Branch> b = 
    boost::static_pointer_cast<TNTree<unsigned>::Branch>(n);
  
  int ntot = 1<<b->dim_;

  unsigned bad = 0;
  for ( int i = 0; i < ntot; ++i ) {
    if ( b->child(i) ) {
      checkLevel(b->child(i), l+1);
    } else {
      bad++;
    }
  }
  
  if (bad) std::cout << "Level " << l << ",  Bad=" << bad  << std::endl;
}
