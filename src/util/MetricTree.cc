
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <algorithm>

// Define CIRCLES if you would like to see the tree build in action
// #define CIRCLES

// Define HISTO for nearest neighbor search diagnostics
// #define HISTO

#include <MetricTree.h>

#include "gvariable.h"
#include "AsciiProgressBar.h"

using namespace std;

BIE_CLASS_EXPORT_IMPLEMENT(Ball)
BIE_CLASS_EXPORT_IMPLEMENT(CandidateList)
BIE_CLASS_EXPORT_IMPLEMENT(TreeAnalysis)
BIE_CLASS_EXPORT_IMPLEMENT(MetricTree)

//
// Turn on for some intermediate debugging checks; turn off for production
//
bool   MetricTree::verbose_debug = false;

//
// Add a bit to the maximum distance (to deal with roundoff, etc.)
//
double MetricTree::sloppy = 1.0e-10;

//
// The constructor is passed an array of vectors
//
MetricTree::MetricTree(vector< vector<double> > *points_, unsigned target)
{
  points = points_;
  dim    = (*points)[0].size();
  mscale = std::vector<double>(dim, 1.0);
#ifdef CIRCLES
  cindx  = 0;			// For circle debugging
#endif
				// This ball owns all of the points to start
  root   = InitialBall();

  if (verbose_debug && myid==0) {
    string name = nametag + ".metrictree.test";
    ofstream out(name.c_str());
    out << "===> Root ball: center=(";
    for (indx k=0; k<dim; k++) out << setw(16) << root->Center()[k];
    out << ") radius=" << root->Radius() << " anchor=" << root->Anchor() 
	<< endl;
  }

  maxLev = 0;
  makeTree(root, target, 0);
  if (!root->Sanity()) {
    cerr << "Oops: failed check" << endl;
  }

  if (0 && myid==0)
  {
    ofstream out("debug.data");
    printTree(out);
  }
  if (0 && myid==0)
  {
    ofstream out("debug.hist");
    printHist(out);
  }
}

//
// The constructor is passed an array of vectors and a metric scaling
//
MetricTree::MetricTree(vector< vector<double> > *points_, 
		       vector<double>& mscale_, unsigned target)
{
  points = points_;
  dim    = (*points)[0].size();
  mscale = mscale_;
#ifdef CIRCLES
  cindx  = 0;			// For circle debugging
#endif
				// This ball owns all of the points to start
  root   = InitialBall();

  if (verbose_debug && myid==0) {
    string name = nametag + ".metrictree.test";
    ofstream out(name.c_str());
    out << "===> Root ball: center=(";
    for (indx k=0; k<dim; k++) out << setw(16) << root->Center()[k];
    out << ") radius=" << root->Radius() << " anchor=" << root->Anchor() 
	<< endl;
  }

  maxLev = 0;
  makeTree(root, target, 0);
  if (!root->Sanity()) {
    cerr << "Oops: failed check" << endl;
  }

  if (0 && myid==0)
  {
    ofstream out("debug.data");
    printTree(out);
  }
  if (0 && myid==0)
  {
    ofstream out("debug.hist");
    printHist(out);
  }
}

void MetricTree::getLeaves(ballPtr cur, set<ballPtr>& leaves)
{
				// My children
  ballPtr one = cur->leftKid;
  ballPtr two = cur->rightKid;

				// Do this bottom up . . . 
  if (cur->Leaf()) leaves.insert(cur);
  else {
    getLeaves(one, leaves);
    getLeaves(two, leaves);
  }
  
}

double MetricTree::distance(indx i, indx j)
{
  double ret = 0.0, tmp;;
  for (indx k=0; k<(*points)[i].size(); k++) {
    tmp = (*points)[i][k] - (*points)[j][k];
    ret += tmp*tmp/mscale[k];
  }
  return sqrt(ret);

}

double MetricTree::distance(vector<double> a, vector<double> b)
{
  double ret = 0.0, tmp;
  for (indx k=0; k<a.size(); k++) {
    tmp = a[k] - b[k];
    ret += tmp*tmp/mscale[k];
  }
  return sqrt(ret);
}


MetricTree::indx MetricTree::TotalInLeaves()
{
  indx sum = 0;
  for (set<ballPtr>::iterator i=balls.begin(); i!=balls.end(); i++) 
    if ((*i)->Leaf()) sum += (*i)->N();

  return sum;
}

MetricTree::indx MetricTree::NumberLeaves()
{
  indx sum = 0;
  for (set<ballPtr>::iterator i=balls.begin(); i!=balls.end(); i++) 
    if ((*i)->Leaf()) sum ++;

  return sum;
}


//
// Make an initial ball from all of the particles
//
ballPtr MetricTree::InitialBall()
{
  // Erase any old balls
  //
  balls.erase(balls.begin(), balls.end());

  // A new ball
  //
  ballPtr cur(new Ball(this));
  cur->leaf = true;	// The initial ball is a leaf . . . 


  // Initialize the root ball
  //
  for (size_t i=0; i<points->size(); i++) cur->addPoint(i);

  // Get center and assign anchor
  //
  cur->newCenter();
  cur->newAnchor();
  cur->newRadius();

  balls.insert(cur);
  return cur;
}

//
// Build a metric tree from a pile of points using the anchors hierarchy
//
void MetricTree::makeTree(ballPtr top, unsigned target, unsigned levl)
{
  mapitD it;
  
  //
  // This is for diagnostics only
  //
  levl++;
  maxLev = max<indx>(levl, maxLev);

  // 
  // Only one leaf node!
  //
  if (top->owned.size() <= target) return;

  //
  // Make a new ball list
  //
  set<ballPtr> newbs;

  //
  // Make a copy of the root ball
  //
  ballPtr cur(new Ball(top.get()));
  balls.insert(cur);
  newbs.insert(cur);

  //
  // Make a working ball list
  //
  set<ballPtr> nodes;

  //
  // Copy the copied root ball into the working nodes list
  //
  nodes.insert(cur);

  //
  // Erase the radius list
  //
  radii.erase(radii.begin(), radii.end());
  it_radii.erase(it_radii.begin(), it_radii.end());

  //
  // Add the copied root ball to the radius list
  //
  it_radii[cur] = radii.insert(pair<double, ballPtr>(cur->Radius(), cur));

  //
  // Now begin making new anchors
  //
				// square root of owned points following
				// Moore (2000)
  unsigned int nanchors = floor(sqrt(cur->owned.size()));

  for (unsigned n=0; n<nanchors; n++) {

    //
    // Ignore leaves with zero radius (e.g. single points)
    //
    if (radii.begin()->second->owned.size() < 2) break;

    //
    // Select the new anchor: the most distant point in the ball with
    // largest radius
    //
    ballPtr big = radii.begin()->second;

    //
    // Make a new ball and assign the anchor point
    //
    cur = ballPtr(new Ball(this));
    cur->addPoint(big->owned.begin()->second);

    balls.insert(cur);
    nodes.insert(cur);
    newbs.insert(cur);

    //
    // Erase the anchor from original ball
    //
    it = big->owned.begin();
    big->owned.erase(it);
    radii.erase(it_radii[big]);
    
    //
    // Reassign the radius of old ball
    //
    it_radii[big] = radii.insert(pair<double, ballPtr>(big->Radius(), big));

    //
    // Scan through all pre-existing balls, stealing particles
    //
    for (set<ballPtr>::iterator k=nodes.begin(); k!=nodes.end(); k++) {
      
      ballPtr j = *k;
      if (j == cur) continue;

      //
      // Can't steal from a singleton or empty node
      //
      if (j->owned.size()==1) continue;

      //
      // Don't steal from a ball of near zero radius
      //
      if (j->Radius()<sloppy) continue;
      
      double adist = 0.5*distance(cur->Anchor(), j->Anchor());

      //
      // Begin stealing
      //
      vector<mapitD> stolen;
      for (mapitD it=j->owned.begin(); it != j->owned.end(); it++) {
				// Metric test
	if (it->first < adist) break;

				// Distance test
	if (distance(cur->Anchor(), it->second) >=
	    distance(j  ->Anchor(), it->second) ) continue;

				// Ok, steal it!
	cur->addPoint(it->second);
	stolen.push_back(it);
      }

      if (verbose_debug && myid==0) {
	string name = nametag + ".metrictree.test";
	ofstream out(name.c_str(), ios::app | ios::out);
	out << "  ---> Crime report: anchor=" 
	    << setw(10) << cur->Anchor() << " center=(";
	for (indx kk=0; kk<dim; kk++) 
	  out << setw(16) << (*points)[cur->Anchor()][kk];
	out << ") stolen=" << stolen.size() << endl;
	for (indx jj=0; jj<stolen.size(); jj++) {
	  for (indx kk=0; kk<dim; kk++) 
	    out << setw(16) << (*points)[stolen[jj]->second][kk];
	  out << endl;
	}
      }

      //
      // Reassign stolen points
      //
      if (stolen.size()) {
				// Remove points
	if (j->owned.size()==stolen.size()) {
	  cerr << "No points left!!" << endl;
	}
	for (indx k=0; k<stolen.size(); k++) j->owned.erase(stolen[k]);
				// Remake radius
	radii.erase(it_radii[j]);
	it_radii[j] = radii.insert(pair<double, ballPtr>(j->Radius(), j));
      }

    }

    // Add the radius of the new ball
    it_radii[cur] = radii.insert(pair<double, ballPtr>(cur->Radius(), cur));
    cur->newCenter();
  }

#ifdef CIRCLES
  // CircleIteration();
#endif
  for (set<ballPtr>::iterator i=balls.begin(); i!=balls.end(); i++) {
    if ((*i)->Leaf() && (*i)->owned.size()==0) {
      cout << "Ooops! [very bad]" << endl;
    }
  }
  
  set<ballPtr>::iterator ii, jj;
  typedef pair<ballPtr, ballPtr> ballPair;

  while (nodes.size()>2) {
    
    std::multimap<double, ballPair> pair_radii;

    for (ii=nodes.begin(); ii!=nodes.end(); ii++) {
      for ((jj=ii)++; jj!=nodes.end(); jj++) {
	ballPair p(*ii, *jj);

	// Make a new ball from all points in the pair
	//
	vector<indx> ilist;

	(*ii)->getPoints(ilist);
	(*jj)->getPoints(ilist);

	Ball b(this);
	for (vector<indx>::iterator k=ilist.begin(); 
	     k!=ilist.end(); k++) b.addPoint(*k);

	pair_radii.insert(pair<double, ballPair>(b.Radius(), p));
      }
    }

    // DEBUGGING
    if (verbose_debug && myid==0) {
      static unsigned cntr = 0;
      string name = nametag + ".metrictree.test";
      ofstream out(name.c_str(), ios::app | ios::out);
      if (out) {

	out << "==== " << cntr++ << " ====" << endl << setprecision(10);
	string yes = " ** ";
	string no  = "    ";

	for (std::multimap<double, ballPair>::iterator 
	       it=pair_radii.begin(); it!=pair_radii.end(); it++) 
	  {
	    std::multimap<double, ballPair>::iterator itm = it; itm--;
	    std::multimap<double, ballPair>::iterator itp = it; itp++;
	    if (it==pair_radii.begin()) {
	      if ( fabs((it->first-itp->first)/it->first) < 1.0e-6)
		out << yes;
	      else
		out << no;
	    } else if (itp==pair_radii.end()) {
	      if ( fabs((it->first-itm->first)/it->first) < 1.0e-6)
		out << yes;
	      else
		out << no;
	    } else {
	      if ( fabs((it->first-itm->first)/it->first) < 1.0e-6 ||
		   fabs((it->first-itp->first)/it->first) < 1.0e-6 )
		out << yes;
	      else
		out << no;
	    }
	    out << right << setw(18) << it->first;
	    if (!it->second.first->leaf)
	      out << setw(10) << it->second.first-> pnumber;
	    if (!it->second.second->leaf)
	      out << setw(10) << it->second.second->pnumber;
	    out << endl;
	  }
      }
    }
    // END DEBUGGING

    // Make a new ball
    cur = ballPtr(new Ball(this));
    balls.insert(cur);
    newbs.insert(cur);

    // The winning pair of balls
    ballPtr i = pair_radii.begin()->second.first;
    ballPtr j = pair_radii.begin()->second.second;

    cur->leftKid  = i;
    cur->rightKid = j;
    cur->leaf     = false;
    cur->pnumber  = i->N() + j->N();

    cur->newCenter();		// Find geometric center
    cur->newAnchor();		// Find anchor and radius

    nodes.erase(i);
    nodes.erase(j);
    nodes.insert(cur);
  }


  //
  // One or two nodes left, assign these to the root node
  // 
  if (nodes.size()==1) {
    ballPtr ib = *nodes.begin();
    top = ib;
    balls.erase(ib);

  } else {
    jj = nodes.begin();
    ii = jj++;
    
    top->leftKid  = *ii;
    top->rightKid = *jj;
    top->pnumber  = (*ii)->N() + (*jj)->N();
    top->leaf     = false;

    top->newCenter();		// Find geometric center
    top->newAnchor();		// Find anchor and radius

    top->owned.erase(top->owned.begin(), top->owned.end());
  }


#ifdef CIRCLES
  // CircleIteration();
#endif

  for (set<ballPtr>::iterator cur=newbs.begin(); cur!=newbs.end(); cur++) {
    if ((*cur)->leaf && (*cur)->owned.size()>target) {
      makeTree(*cur, target, levl);
      if (!(*cur)->Sanity()) {
	cerr << "Oops: failed check" << endl;
      }
    }
  }
}


void MetricTree::printTree(ostream& out)
{
  indx maxleafcnt = 0, cnt=0;

  for (set<ballPtr>::iterator i=balls.begin(); i!=balls.end(); i++) {
    out << "Ball #" << cnt++ << ": " << (*i)->N() << " points" << endl;
    if ((*i)->Leaf())
      out << "          AM a leaf node, #" << i->get() << endl;
    else
      out << "          NOT a leaf node, #" << i->get() << endl
	  << "          My children are #" 
	  << (*i)->leftKid.get() << " and #" << (*i)->rightKid.get() << endl;

    out << "          Radius is: " << (*i)->Radius() << endl;
    out << "          Center is: ";
    for (indx k=0; k<dim; k++)
      out << setw(16) << (*i)->Center()[k];
    out << endl << endl;

    if ((*i)->Leaf()) maxleafcnt = max<indx>(maxleafcnt, (*i)->N());
  }

}


void MetricTree::printHist(ostream& out)
{
  out << "Histogram:" << endl;

  indx numhist = (indx)floor(sqrt(TotalInLeaves())), over=0;
  vector<int> hist(numhist, 0);

  for (set<ballPtr>::iterator i=balls.begin(); i!=balls.end(); i++) {
    if ((*i)->N()>numhist) over++;
    else hist[(*i)->N()-1]++;
  }

  int mxn;
  for (mxn=numhist-1; mxn>=0; mxn--) {
    if (hist[mxn]!=0) break;
  }

  for (int i=0; i<=mxn; i++)
    out << setw(8) << i+1 << setw(8) << hist[i] << endl;

  if (over) {
    ostringstream ostr;
    ostr << ">" << numhist;
    out << setw(8) << ostr.str() << setw(8) << over << endl;
  }

}


MetricTree::indx MetricTree::numberLeaves()
{
  indx ret = 0;
  for (set<ballPtr>::iterator i=balls.begin(); i!=balls.end(); i++)
    if ((*i)->Leaf()) ret++;
  return ret;
}


void MetricTree::circleWalk(ballPtr b, double minrad, 
			    int level, int lmin, int lmax, ostream& out)
{
  double rad = max<double>(b->Radius(), minrad);

  if (b->Leaf()) {
    if (level>=lmin && level<=lmax)
      out << "plot [0:2*pi]"
	  << " (" << (*points)[b->anchor][0] << "+" << rad << "*cos(t)), "
	  << " (" << (*points)[b->anchor][1] << "+" << rad << "*sin(t)) t'' w l"
	  << endl;
  } else {
    if (level>=lmin && level<=lmax)
      out << "plot [0:2*pi]"
	  << " (" << (*points)[b->anchor][0] << "+" << rad << "*cos(t)), "
	  << " (" << (*points)[b->anchor][1] << "+" << rad << "*sin(t)) t'' w l lt 2"
	  << endl;

    circleWalk(b->leftKid , minrad, level+1, lmin, lmax, out);
    circleWalk(b->rightKid, minrad, level+1, lmin, lmax, out);
  }
}


void MetricTree::printCircles(string& filename, double minrad, 
			      int lmin, int lmax, bool plot_points)
{
  if (myid)     return;
  if (dim != 2) return;

  static double xmin, xmax, ymin, ymax;
  static bool firstime = true;

  ofstream out(filename.c_str());
  if (!out) {
    cerr << "MetricTree:printCircles: could not open <" << filename 
	 << ">" << endl;
    return;
  }

  //
  // Set box size from extrema
  //
  if (firstime) {
    double Xmin = (*points)[0][0], Xmax = (*points)[0][0]; 
    double Ymin = (*points)[0][1], Ymax = (*points)[0][1]; 

    for (indx i=1; i<(*points).size(); i++) {
      Xmin = min<double>(Xmin, (*points)[i][0]);
      Xmax = max<double>(Xmax, (*points)[i][0]);
      Ymin = min<double>(Ymin, (*points)[i][1]);
      Ymax = max<double>(Ymax, (*points)[i][1]);
    }

    xmax = Xmax + 0.5*(Xmax - Xmin);
    xmin = Xmin - 0.5*(Xmax - Xmin);
    ymax = Ymax + 0.5*(Ymax - Ymin);
    ymin = Ymin - 0.5*(Ymax - Ymin);

    if (xmax - xmin > ymax - ymin) {
      double p = 0.5*(ymax + ymin);
      double m = ymax - ymin;
      double t = (xmax - xmin)/(ymax - ymin);
      ymin = p - t*m;
      ymax = p + t*m;
    } else {
      double p = 0.5*(xmax + xmin);
      double m = xmax - xmin;
      double t = (ymax - ymin)/(xmax - xmin);
      xmin = p - t*m;
      xmax = p + t*m;
    }


    firstime = false;
  }

  out << "set multiplot"   << endl;
  out << "set xrange ["    << xmin << ":" << xmax << "]" << endl;
  out << "set yrange ["    << ymin << ":" << ymax << "]" << endl;
  out << "set parametric"  << endl;
  out << "set size square" << endl;

  //
  // Tree walk
  //
  circleWalk(Root(), 0.01, 0, lmin, lmax, out);

  if (plot_points) {
    // out << "plot 'circles.dat' u 1:2 w p pt 3 lt 4" << endl;
    out << "plot 'circles.dat' u 1:2 w d lt 4" << endl;
    ofstream out2("circles.dat");
    for (unsigned i=0; i<points->size(); i++)
      out2 << setw(18) << (*points)[i][0]
	   << setw(18) << (*points)[i][1]
	   << endl;
  }
  out << "unset multiplot" << endl;
}


void MetricTree::CircleIteration(double minrad)
{
  ostringstream sout;
  sout << "circle_debug." << cindx++;
  string filename = sout.str();
  printCircles(filename, minrad, 0, 4, false);
}


void MetricTree::NearestNeighbors
(vector< vector<indx> >& indices, 
 vector< vector<double> >& dists, 
 vector< vector<double> >& pts, int& k)
{
  indx N=pts.size();

  k = min<int>(k, (*points).size());

  indices.clear();
  dists  .clear();
  
#ifdef HISTO
  const int nbin = 10;
  double delta = log(N)/nbin;
  vector<unsigned> bins(nbin, 0);
#endif
  
				// Loop through the input points
  for (indx i=0; i<N; i++) {  

    //
    // Walk the tree
    //
    vector<indx>   ii;
    vector<double> dd;
#ifdef HISTO
    unsigned long cnt = checkBall(root, k, pts[i], ii, dd);
    int nb = static_cast<int>(floor(log(cnt)/delta));
    bins[min<int>(nb, nbin-1)]++;
#else
    checkBall(root, k, pts[i], ii, dd);
#endif
    //
    // Push the results onto the return vectors
    //
    dists  .push_back(dd);
    indices.push_back(ii);
  }

#ifdef HISTO
  if (myid==0) {
    ofstream out("nn_histo.dat");
    for (int nb=0; nb<nbin; nb++) {
      out << setw(5)  << exp(delta*(nb+1))
	  << setw(10) << bins[nb]
	  << endl;
    }
  }
#endif
}

unsigned long MetricTree::checkBall(ballPtr cur, int k, 
				    vector<double>& pt,
				    vector<indx>&   ii,
				    vector<double>& dd)
{
  CandidateList cl(this, pt, k);
  checkBall(cur, cl);
  cl.get(ii, dd);
  return cl.Count();
}

void MetricTree::checkBall(ballPtr cur, CandidateList& cl)
{
				// If this ball is a leaf, add the
				// points and return
  if (cur->Leaf()) {
    cl.add(cur);
    return;
  }

				// For convenience
				// 
  ballPtr leftKid  = cur->leftKid;
  ballPtr rightKid = cur->rightKid;

				// Range bracket for points in the ball
				//
  indx leftAnchor  = leftKid ->Anchor();
  indx rightAnchor = rightKid->Anchor();

  double Dist;

  Dist = distance((*points)[leftAnchor], cl.V());
  double minDistLeft = max<double>(Dist - leftKid->Radius(), 0.0);
  double maxDistLeft = Dist + leftKid->Radius() + sloppy;

  Dist = distance((*points)[rightAnchor], cl.V());
  double minDistRight = max<double>(Dist - rightKid->Radius(), 0.0);
  double maxDistRight = Dist + rightKid->Radius() + sloppy;


  // Safe to ignore right
  if (maxDistLeft < minDistRight && leftKid->N() > cl.NN()) {
    checkBall(leftKid, cl);
    return;
  }

  // Safe to ignore left
  if (maxDistRight < minDistLeft && rightKid->N() > cl.NN()) {
    checkBall(rightKid, cl);
    return;
  }

  // Need to check the left
  if (minDistLeft < cl.maxDist()) {
    checkBall(leftKid, cl);
  }

  // Need to check the right
  if (minDistLeft < cl.maxDist()) {
    checkBall(rightKid, cl);
  }
}

//
// Return the child index closed to a ball in another tree
//
ballPtr MetricTree::nearestChild(ballPtr L, ballPtr R, ballPtr root)
{
  double distL = 0.0, distR=0.0, val;
  
  if (L.get()==0) return R;
  if (R.get()==0) return L;

  for(indx k=0; k<dim; k++) {

    val = 
      (*points)[root->Anchor()][k] - (*points)[L->Anchor()][k];

    distL += val*val;

    val = 
      (*points)[root->Anchor()][k] - (*points)[R->Anchor()][k];

    distR += val*val;
  }
  
  if (distL < distR)
    return L;
  else 
    return R;
}

vector<double> MetricTree::getCenter(ballPtr j)
{ 
  indx i = j->Anchor();
  return (*points)[i];
}

double MetricTree::getRadius(ballPtr j)
{ 
  return j->Radius(); 
}

//
// Walk the tree from the root and make a serial list of parameters
// and write to file
//
void MetricTree::serializeAscii(string filename)
{
  ofstream out(filename.c_str());
  if (!out) {
    cerr << "MetricTree::serializeAscii: could not open <" << filename
	 << "> . . . quitting" << endl;
    return;
  }

  serializeAsciiTree(root, out);
}

void MetricTree::serializeAsciiTree(ballPtr cur, ostream& out)
{
  out << "====Node " << hex << cur << dec << "====" << endl;
  out << "pn=" << cur->pnumber << endl;

  if (cur->Leaf()) {

    out << "anchor=" << cur->Anchor() << endl;
    out << "my point=" << cur->owned.begin()->second << endl;

  } else {
    
    out << "center=";
    for (unsigned i=0; i<dim; i++) 
      out << setw(20) << cur->center[i];
    out << endl;
    out << "rad=" << cur->Radius() << endl;
    out << "lk=" << cur->leftKid << " rk=" << cur->rightKid << endl;

    serializeAsciiTree(cur->leftKid , out);
    serializeAsciiTree(cur->rightKid, out);
  }
  
}

void MetricTree::treeAnalysis(std::string filename)
{
  ofstream out(filename.c_str());
  if (!out) {
    cerr << "MetricTree:treeAnalysis: error opening <"
	 << filename << "> for output" << endl;
    return;
  }

  TreeInfo info;

  treeAnalysisWalk(root, 0, info);

  // Sort at each level
  for (TreeInfo::iterator it=info.begin(); it!=info.end(); it++) {
    sort(it->second.begin(), it->second.end(), compare_centers);
    out << "==================" << endl;
    out << "==== Level " << setw(2) << it->first << " ====" << endl;
    out << "==================" << endl;
    for (vector<TreeAnalysis>::iterator 
	   jt=it->second.begin(); jt!=it->second.end(); jt++) 
      {
	if (jt->leaf) out << "   ++  ";
	else          out << "   oo  ";
	out << " C=(";
	for (size_t n=0; n<jt->center.size(); n++) 
	  out << setw(12) << setprecision(4) << jt->center[n];
	out << ")";
	if (!jt->leaf) 
	  out << " N=" << setw(4) << jt->number << " R=" << jt->radius;
	out << endl;
      }
  }

}

void MetricTree::treeAnalysisWalk(ballPtr cur, unsigned lev, TreeInfo& info)
{
  TreeAnalysis element;

  element.number = cur->pnumber;
  element.center = cur->center;
  element.radius = cur->Radius();
  element.leaf   = cur->Leaf();

  info[lev].push_back(element);

  if (!cur->Leaf()) {
    treeAnalysisWalk(cur->leftKid,  lev+1, info);
    treeAnalysisWalk(cur->rightKid, lev+1, info);
  }

}

//=============================================================================

/// Add a point to the ball
void Ball::addPoint(indx i) 
{
  if (anchor == NO_VALUE) anchor = i;
  leaf = true;
  owned.insert(std::pair<double, indx>(mt->distance(i, anchor), i));
}


/// Compute the geometric center from the current points
void Ball::newCenter() 
{
  center = std::vector<double>(mt->dim, 0.0);
  plist.erase(plist.begin(), plist.end());
  getPoints(plist);
  for (unsigned i=0; i<plist.size(); i++) {
    for (unsigned k=0; k<mt->dim; k++)
      center[k] += (*(mt->points))[plist[i]][k];
  }
  if (plist.size())
    for (unsigned k=0; k<mt->dim; k++) center[k] /= plist.size();
}

/// Find the anchor closest to the center
void Ball::newAnchor() 
{
  diMapI dists;
  for (unsigned i=0; i<plist.size(); i++) {
    double d = mt->distance((*mt->points)[plist[i]], center);
    dists.insert(std::pair<double, indx>(d, plist[i]));
  }
  anchor = dists.begin()->second;
  
  std::set<double, std::greater<double> > rads;
  for (unsigned i=0; i<plist.size(); i++) {
    double d = mt->distance(plist[i], anchor);
    rads.insert(d);
  }
  
  radius = *rads.begin();
}

/// Recompute the onwership list after assigning a new anchor
void Ball::newRadius() 
{
  diMapD new_owned;
  for (diMapD::iterator it=owned.begin(); it!=owned.end(); it++) {
    indx i = it->second;
    new_owned.insert(std::pair<double, indx>(mt->distance(i, anchor), i));
  }

  owned = new_owned;
  radius = owned.begin()->first;
}


void CandidateList::add(ballPtr b)
{
  vector<Ball::indx> pl;
  b->getPoints(pl);
  for (vector<Ball::indx>::iterator i=pl.begin(); i!=pl.end(); i++) {
    double dist = my->distance(pt, (*(my->points))[*i]);
    cl.insert(pair<double, Ball::indx>(dist, *i));
				// Pop off the largest distance
    if (cl.size()>nn) cl.erase(--cl.rbegin().base());
    ct++;
  }
}


void CandidateList::get(vector<Ball::indx>& ii, vector<double>& dd)
{
  for (multimap<double, Ball::indx>::iterator i=cl.begin(); i!=cl.end(); i++) {
    dd.push_back(i->first);
    ii.push_back(i->second);
  }
}
