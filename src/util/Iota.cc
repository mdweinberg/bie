#include <Iota.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Iota)

using namespace BIE;

void Iota::build(int beg, int end, int del)
{
  // Get a reference to my vector
  std::vector<int>& u = (*this)();

  // Empty the vector
  u.erase(u.begin(), u.end());

  // Ensure that the sign of the desired range, end-beg, matches the
  // increment, del
  //
  if ( end - beg > 0 && del > 0) 
    {
    for (int i=beg; i<end; i+=del) u.push_back(i);
    } 
  else if ( end - beg < 0 && del < 0) 
    {
      for (int i=beg; i>end; i+=del) u.push_back(i);
    } 
  else 
    {
      std::cerr << "Iota: bad range " << beg << " (" << del << ") " 
		<< end << std::endl;
    }
}
