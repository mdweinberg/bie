//=============================================================================
// KDTree class
//=============================================================================

#include <cmath>
#include <cfloat>
#include <utility>
#include <map>
#include <vector>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <ACG.h>
#include <Uniform.h>
#include <Normal.h>
#include <KDTree.h>

BIE_CLASS_EXPORT_IMPLEMENT(Delta)
BIE_CLASS_EXPORT_IMPLEMENT(LeafElem)
BIE_CLASS_EXPORT_IMPLEMENT(KDTree)

bool   KDTree::verbose_debug = false;
bool   KDTree::use_range     = false;

// =================================================================
//  Constructor: initialize the data arrays and build the tree
//  [double* pointer version]
// =================================================================
//
KDTree::KDTree( unsigned long d, unsigned long N, unsigned cut,
		const double* points_, const double* weights_,
		const double* beta_)
{
  dims         = d;
  num_points   = N;
  vol_trim     = false;
  ncut         = std::max<unsigned>(cut, 1);
  ncutM        = ncut;
  temp         = 0.0;
   
  // For preventing sorting degeneracy and sampling
  
  gen          = boost::shared_ptr<ACG>(new ACG(11, 20));
  unit         = UniformPtr(new Uniform(0.0, 1.0, gen.get()));
  norm         = NormalPtr (new Normal (0.0, 1.0, gen.get()));
  
  // The full KD tree will have twice the total number of nodes as leaf nodes
  
  centers      = std::vector<double>(num_points*dims*2);
  ranges       = std::vector<double>(num_points*dims*2);
  center0      = std::vector<double>(num_points*dims*2);
  range0       = std::vector<double>(num_points*dims*2);
  Ledge        = std::vector<double>(num_points*dims*2);
  Hedge        = std::vector<double>(num_points*dims*2);
  weights      = std::vector<double>(num_points*2);
  beta         = std::vector<double>(num_points*2);
  lower        = std::vector<double>(num_points*2, 0);
  upper        = std::vector<double>(num_points*2, 1);
  
  left_child   = std::vector<indx>(num_points*2);
  right_child  = std::vector<indx>(num_points*2);
  
  lo_leaf      = std::vector<indx>(num_points*2);
  hi_leaf      = std::vector<indx>(num_points*2);
  
  permutation  = std::vector<indx>(num_points*2);  
  
  // For debugging
  stally       = std::vector<unsigned>(dims, 0);
  
  //
  // Half of the nodes are leaves, assign them to start
  //
  for (indx j=0,i=dims*num_points; j<dims*num_points; i++,j++)
    centers[i] = points_[j];
  
  //
  // Check for manual or automatic bounds
  //
  if (lowerBound.size() == dims && upperBound.size() == dims) {
    minS = lowerBound;
    maxS = upperBound;
    scale = std::vector<double>(dims);
    for (indx i=0; i<dims; i++) scale[i] = maxS[i] - minS[i];
  } else {
    makeBounds();
  }

  //
  // Normalize the weights
  //
  double norm = 0.0;
  bool ok = true;
  for (indx j=0; j<num_points; j++) {
    if (weights_[j]<0.0) ok = false;
    norm += weights_[j];
  }
  if (norm <= 0.0) ok = false;
  
  if (ok) {
    for (indx j=0,i=num_points; j<num_points; i++,j++)
      weights[i] = weights_[j]/norm;
  } else {
    for (indx j=0,i=num_points; j<num_points; i++,j++)
      weights[i] = 1.0/num_points;
  }
  
  
  for (indx j=0,i=num_points; j<num_points; i++,j++)
    beta[i] = beta_[j];

  if (num_points > 0) {
    buildTree();
  }
  
  if (verbose_debug) {
    std::cout << std::endl << "Split tally:" << std::endl;
    for (unsigned j=0; j<dims; j++) 
      std::cout << std::setw(3)  << j + 1
		<< std::setw(16) << stally[j]
		<< std::setw(16) << KDTree::scale[j] << std::endl;
    std::cout << std::endl;
  }
  
}

// =================================================================
//  Constructor: initialize the data arrays and build the tree
//  [std::vector version]
// =================================================================
//
KDTree::KDTree( unsigned long d, unsigned long N, unsigned cut,
		const std::vector< std::vector<double> > &points_, 
		const std::vector<double> &weights_,
		const std::vector<double> &beta_)
{
  dims         = d;
  num_points   = N;
  vol_trim     = false;
  ncut         = std::max<unsigned>(cut, 1);
  ncutM        = ncut;
  temp         = 0.0;
 
  // For preventing sorting degeneracy and for sampling
  
  gen          = boost::shared_ptr<ACG>(new ACG(11, 20));
  unit         = UniformPtr(new Uniform(0.0, 1.0, gen.get()));
  norm         = NormalPtr (new Normal (0.0, 1.0, gen.get()));
  
  // The full KD tree will have twice the total number of nodes as leaf nodes
  
  centers      = std::vector<double>(num_points*dims*2);
  ranges       = std::vector<double>(num_points*dims*2);
  center0      = std::vector<double>(num_points*dims*2);
  range0       = std::vector<double>(num_points*dims*2);
  Ledge        = std::vector<double>(num_points*dims*2);
  Hedge        = std::vector<double>(num_points*dims*2);
  weights      = std::vector<double>(num_points*2);
  beta         = std::vector<double>(num_points*2);
  lower        = std::vector<double>(num_points*2, 0);
  upper        = std::vector<double>(num_points*2, 1);
  
  left_child   = std::vector<indx>  (num_points*2);
  right_child  = std::vector<indx>  (num_points*2);
  
  lo_leaf      = std::vector<indx>  (num_points*2);
  hi_leaf      = std::vector<indx>  (num_points*2);
  
  permutation  = std::vector<indx>  (num_points*2);  
  
  // For debugging
  stally       = std::vector<unsigned>(dims, 0);
  
  //
  // Half of the nodes are leaves, assign them to start
  //
  {
    indx I=dims*num_points;
    for (indx j=0; j<num_points; j++)
      for (indx i=0; i<dims; i++) centers[I++] = points_[j][i];
  }
  
  if (lowerBound.size() == dims && upperBound.size() == dims) {
    minS = lowerBound;
    maxS = upperBound;
    scale = std::vector<double>(dims);
    for (indx i=0; i<dims; i++) scale[i] = maxS[i] - minS[i];
  } else {
    makeBounds();
  }

  //
  // Normalize the weights
  //
  double norm = 0.0;
  bool ok = true;
  for (indx j=0; j<num_points; j++) {
    if (weights_[j]<0.0) ok = false;
    norm += weights_[j];
  }
  if (norm <= 0.0) ok = false;
  
  if (ok) {
    for (indx j=0,i=num_points; j<num_points; i++,j++)
      weights[i] = weights_[j]/norm;
  } else {
    for (indx j=0,i=num_points; j<num_points; i++,j++)
      weights[i] = 1.0/num_points;
  }
  
  for (indx j=0,i=num_points; j<num_points; i++,j++)
    beta[i] = beta_[j];

  if (num_points > 0) {
    buildTree();
  }
  
  if (verbose_debug) {
    std::cout << std::endl << "Split tally:" << std::endl;
    for (unsigned j=0; j<dims; j++) 
      std::cout << std::setw(3)  << j + 1
		<< std::setw(16) << stally[j]
		<< std::setw(16) << KDTree::scale[j] << std::endl;
    std::cout << std::endl;
  }
  
}

// =================================================================
// Compute minimums, maximums, and ranges for each dimension
// =================================================================
//
void KDTree::makeBounds()
{
  scale = std::vector<double>(dims);
  minS  = std::vector<double>(dims,  DBL_MAX);
  maxS  = std::vector<double>(dims, -DBL_MAX);
  
  for (indx j=num_points; j<2*num_points; j++) {
    for (indx i=0; i<dims; i++) {
      minS[i] = std::min<double>(minS[i], centers[j*dims+i]);
      maxS[i] = std::max<double>(maxS[i], centers[j*dims+i]);
    }
  }
  
  for (indx i=0; i<dims; i++) scale[i] = maxS[i] - minS[i];
}

// =================================================================
// Given the leaves, build the rest of the tree from the top down.
// Split the leaves along the most spread coordinate, build two KDs
// out of those, and then build a KD around those two children.
// =================================================================
//
void KDTree::buildKD(KDTree::indx low, KDTree::indx high, KDTree::indx root,
		     const std::vector<double>& minC, 
		     const std::vector<double>& maxC)
{
  //
  // Assign interval
  //
  
  lower[root] = static_cast<double>(low  - num_points)/(num_points-1);
  upper[root] = static_cast<double>(high - num_points)/(num_points-1);
  
  //
  // Assign box range (applies to singletons, too!)
  //
  
  for (unsigned k=0; k<dims; k++) {
    if (minC[k] >= maxC[k]) {
      cout << "KDTree::buildKD: box width for dimension " << k 
	   << " is <= 0, values=" << minC[k]  << ", " << maxC[k] 
	   << endl;
    }
    Ledge[root*dims + k] = minC[k]; // Lower edge
    Hedge[root*dims + k] = maxC[k]; // Upper edge
  }

  //
  // Compute the data range
  //
  
  makeRange(low, high, root);
  
  //
  // Special case for nodes with one point (singleton)
  //
  if (low == high) {
    
    lo_leaf     [root]  = low;
    hi_leaf     [root]  = low;
    left_child  [root]  = low;
    right_child [root]  = NO_CHILD;
    
    weights[root]       = weights[low];
    
    return;
  }
  
  
  //
  // Find dimension with the largest dispersion or range
  //
  KDTree::indx coord, split, left, right;
  
  if (use_range)
    coord = max_range(root);
  else
    coord = max_dispersion(low, high);
  
  //
  // Split the current leaves into two groups, to build KD trees on
  // each of them.  Choose the coordinate with largest dispersion for
  // the split, and make sure there are the same number of points in
  // each (up to $\pm1$ for round off error).
  //
  split = (low + high) / 2;
  select(coord, split, low, high);

  std::vector<double> maxL(maxC), minR(minC);
  
  maxL[coord] = minR[coord] = centers[split*dims+coord];
  
  // If the left sub-tree is just one leaf, don't make a new non-leaf
  // node for it, just point left index directly to the leaf itself.
  //
  if (split <= low)    left = low;
  else                 left = next++;
  
  // Same for the right
  //
  if (split+1 >= high) right = high;
  else                 right = next++;
  
  lo_leaf     [root] = low;
  hi_leaf     [root] = high;
  left_child  [root] = left;
  right_child [root] = right;
  
  // Build sub-trees
  //
  buildKD(low,     split,  left, minC, maxL);
  buildKD(split+1, high,  right, minR, maxC);
}


// =================================================================
// Find the dimension along which the leaves between low and high
// inclusive have the greatest variance
// =================================================================
//
unsigned long KDTree::max_dispersion(KDTree::indx low, KDTree::indx high) const
{
  typedef std::pair<double, KDTree::indx> pelem;
  std::vector<pelem> mrange;
  double mean, variance;
  pelem e;
  
  for (KDTree::indx d = 0; d<dims; d++) {
    mean = variance = 0;
    for (KDTree::indx p = dims*low + d; p < dims*(high+1); p += dims)
      mean += centers[p];
    mean /= (high - low);
    
    for(KDTree::indx p = dims*low + d; p < dims*(high+1); p += dims)
      variance += (centers[p] - mean) * (centers[p] - mean);
    
    e.first  = variance/(scale[d]*scale[d]) + 1.0e-4*(*norm)();
    e.second = d;
    mrange.push_back(e);
  }
  
  
  std::sort(mrange.begin(), mrange.end());
  
  return mrange.back().second;
}


// =================================================================
// Find the dimension along which the leaves between low and high
// inclusive have the greatest range
// =================================================================
//
unsigned long KDTree::max_range(KDTree::indx root) const
{
  typedef std::pair<double, KDTree::indx> pelem;
  std::vector<pelem> mrange;
  pelem e;
  
  for (KDTree::indx d = 0; d<dims; d++) {
    e.first  = ranges[root*dims + d] + 1.0e-4*(*norm)();
    e.second = d;
    mrange.push_back(e);
  }
  
  std::sort(mrange.begin(), mrange.end());
  
  return mrange.back().second;
}


// =================================================================
// Compute the data range (used in buildKD)
// =================================================================
//
void KDTree::makeRange(KDTree::indx low, KDTree::indx high, KDTree::indx root)
{
  double vmin, vmax;
  for (KDTree::indx d = 0; d<dims; d++) {
    vmin =  DBL_MAX;
    vmax = -DBL_MAX;
    for(KDTree::indx p = dims*low + d; p < dims*(high+1); p += dims) {
      vmin = std::min<double>(vmin, centers[p]);
      vmax = std::max<double>(vmax, centers[p]);
    }
    centers[root*dims + d] = 0.5*(vmin + vmax);
    ranges [root*dims + d] = vmax - vmin;
  }
}

// =================================================================
// Function to partition the data into two (equal-sized or near as
// possible) sets, one of which is uniformly greater than the other in
// the given dimension.  This is a partial binary sort.
// =================================================================
//

void KDTree::select(unsigned long sel, unsigned long pos,
		    unsigned long low, unsigned long high)
{
  // For debugging . . .
  stally[sel]++;
  
  unsigned long m, r, i;
  
  while (low < high) {
    r = (low + high)/2; // Swap the midpoint positioned particle for the low
    swap(r, low);	// positioned particle (pivot)
    m = low;
    for (i=low+1; i<=high; i++) { // Move all with coord less than the
				  // pivot to the left-hand partition
      if (centers[sel+dims*i] < centers[sel+dims*low]) {
        m++;
        swap(m, i);
      } 
    }
    swap(low, m);	       // Put the pivot at the top of the
			       // list; now all points to the left
			       // have coordinate values less than the
			       // pivot value
    
    // If the pivot equals pos, we're done
    if (m == pos) break;

    // If the pivot is smaller than pos, sort the right-hand partition
    if (m < pos) low  = m+1;
    
    // If the pivot is larger than pos, sort the left-hand partition
    if (m > pos) high = m-1;
  }    

}



// =================================================================
// Swap the ith leaf with the jth leaf.  Actually, only swap the
// weights, permutation, and centers, so only for swapping
// leaves. Will not swap children (e.g. not leaf nodes) correctly.
// =================================================================
//
void KDTree::swap(unsigned long i, unsigned long j) 
{
  KDTree::indx k;
  double tmp;
  
  if (i==j) return;
  
  // swap weights
  //
  tmp            = weights[i];    
  weights[i]     = weights[j];
  weights[j]     = tmp;
  
  // swap beta
  //
  tmp            = beta[i];    
  beta[i]        = beta[j];
  beta[j]        = tmp;
  
  // update the permutation
  //
  k = permutation[i];  
  permutation[i] = permutation[j];  
  permutation[j] = k;
  
  // swap centers
  //
  i *= dims;   
  j *= dims;
  for (k=0; k<dims; i++,j++,k++) {
    tmp         = centers[i];   
    centers[i]  = centers[j];   
    centers[j]  = tmp;
  }
}

// =================================================================
// Calculate the statistics of level "root" based on the statistics of
// its left and right children.  Called after tree build.
// =================================================================
//
void KDTree::calcStats(KDTree::indx root)
{
  //
  // Figure out the center and ranges of this cell
  //
  for (KDTree::indx d=0; d<dims; d++) {
    center0[root*dims+d] = 0.5 * (Ledge[root*dims+d] + Hedge[root*dims+d]);
    range0 [root*dims+d] = Hedge[root*dims+d] - Ledge[root*dims+d];
  }    
  
  if (!isLeaf(root)) {

    //
    // Get indices of the children
    //
    KDTree::indx indxL = left (root);
    KDTree::indx indxR = right(root);
  
    //
    // Nothing to do for empty nodes, otherwise update the weights
    //
    weights[root] = 0.0;
    
    if (validIndex(indxL)) {
      calcStats(indxL);
      weights[root] += weights[indxL];
    }
    
    if (validIndex(indxR)) {
      calcStats(indxR);
      weights[root] += weights[indxR];
    }
  }
}

// =================================================================
// Public method to build the tree, just calls the private method with
// the proper starting arguments.
// =================================================================
//
void KDTree::buildTree()
{
  for (indx j=0, i=num_points; j<num_points; i++,j++) {
    for(indx k=0; k<dims; k++)
      ranges[i*dims+k] = 0;	// Leaves are points so their ranges
				// are zero by definition
    
				// lo_leaf and hi_leaf point
				// the sequence of leaves assigned to
				// a node.  So, for a leaf, these
				// point back to the leaf itself
    lo_leaf[i] = hi_leaf[i] = i; 
    // The leaves have no children
    left_child[i]  = i; 
    right_child[i] = NO_CHILD;
    // The tree begins with the leaf nodes
    // in the original order.
    permutation[i] = j;
  }
  // This points to the next node to be
  // constructed
  next = 1;
  
  buildKD(num_points, 2*num_points - 1, 0, minS, maxS);

  // Update the weights, and fiducical center and range
  //
  calcStats(root());
}

// =================================================================
// Public method to build the tree, just calls the private method with
// the proper starting arguments.
// =================================================================
//
KDTree::indx KDTree::find(const std::vector<double>& T, unsigned long ncut)
{
  //
  // Range check on root node
  //
  bool ok = true;
  for (KDTree::indx d=0; d<dims; d++) {
    if (T[d]<Ledge[d] || T[d] > Hedge[d]) {
      ok = false;
    }
  }
  
  if (!ok) return -1;
  
  return find(T, ncut, root());
}

//
// Keep looking until occupation # is below the bucket size
//
KDTree::indx KDTree::find(const std::vector<double>& T, unsigned long ncut, 
			  KDTree::indx root)
{
  KDTree::indx ret   = root;
  if (isLeaf(root)) {
    return ret;
  }
  KDTree::indx indxL = left (root);
  KDTree::indx indxR = right(root);
  
  if (Npts(indxL) > ncut || Npts(indxR) > ncut) {
    bool Left  = true;
    bool Right = true;
    for (KDTree::indx d=0; d<dims; d++) {
      if (T[d]<Ledge[indxL*dims+d] || T[d] > Hedge[indxL*dims+d]) Left  = false;
      if (T[d]<Ledge[indxR*dims+d] || T[d] > Hedge[indxR*dims+d]) Right = false;
    }
    if      (Left  && !Right && Npts(indxL)>ncut) ret = find(T, ncut, indxL);
    else if (Right && !Left  && Npts(indxR)>ncut) ret = find(T, ncut, indxR);
    else {
      std::cerr << "KDTree::find: this should never happen!" << std::endl;
    }
  }
  
  return ret;
}


// =================================================================
// Return the data id of the point nearest the input put, with 
// Euclidean metric scaled by the circumscribing range
// =================================================================
//
KDTree::indx KDTree::getClosestPoint(std::vector<double>& T, KDTree::indx root,
				     std::vector< std::vector<double> >& data)
{
  if (T.size() != (sz=data[0].size())) {
    std::cerr << "KDTree::getClosest: vector size mismatch!" << std::endl;
    return -1;
  }
  
  indx fid = lo_leaf[root];
  std::vector<double> v;
  double maxD=DBL_MAX, dist, fac;

  for (indx i=lo_leaf[root]; i<=hi_leaf[root]; i++) {
    dist = 0.0;
    for (unsigned j=0; j<sz; j++) {
      if (range0[j]>0.0) {
	fac = (T[j] - data[permutation[i]][j])/range0[j];
	dist += fac*fac;
      }
    }
    if (maxD < dist) {
      maxD = dist;
      fid = i;
    }
  }
  
  return fid;
}

// =================================================================
// Figure out which of two children in this tree is closest to a given
// KD in another tree.  Returns the index in this tree of the closer
// child.
// =================================================================
//
KDTree::indx KDTree::closer(KDTree::indx myLeft, 
			    KDTree::indx myRight, 
			    const KDTree& otherTree,
			    KDTree::indx otherRoot) const 
{
  double dist_sq_l = 0, dist_sq_r = 0;
  for(indx i=0; i<dims; i++) {
    dist_sq_l += (otherTree.center(otherRoot)[i] - center(myLeft)[i]) * 
      (otherTree.center(otherRoot)[i] - center(myLeft)[i]);
    dist_sq_r += (otherTree.center(otherRoot)[i] - center(myRight)[i]) * 
      (otherTree.center(otherRoot)[i] - center(myRight)[i]);
  }
  
  if (dist_sq_l < dist_sq_r)
    return myLeft;
  else 
    return myRight;
}

// =================================================================
// Perform a *slight* adjustment of the tree: move the points by
// delta, but don't reform the whole tree; just fix up the statistics.
// =================================================================
//
void KDTree::movePoints(double* delta)
{
  // first adjust locations by delta
  for (indx i=leafFirst(root());i<=leafLast(root());i++)
    for (unsigned int k=0;k<dims;k++)
      centers[dims*i+k] += delta[ getIndexOf(i)*dims + k ];
  
  // recompute stats of nodes
  calcStats(root());                                    
}

// =================================================================
// Assumes newWeights is the right size (num_points)
// =================================================================
//
void KDTree::changeWeights(const double *newWeights) {
  
  for(indx i=num_points, j=0; i<num_points*2; i++, j++)
    weights[i] = newWeights[ getIndexOf(i) ];
  
  // recompute stats of nodes
  calcStats(root());                                    
}


/**
   Point could be this close to the kd box (lower limit)
 */
double 
Delta::minDist(KDTree::indx myKD, const double* point, const KDTree* kd) const
{
  double dist = DBL_MAX;

  if (kd->isLeaf(myKD)) {
    for (unsigned i=0; i<kd->dims; i++) {
      double d = point[i] - kd->center(myKD)[i];
      dist = std::min<double>(dist, fabs(d));
    }

  } else {

    bool inside = true;
    for (unsigned i=0; i<kd->dims; i++) {
      double d = point[i] - kd->center(myKD)[i];
      double r = 0.5*kd->Range(myKD)[i];
      if (d < -r || d > r) {
	inside = false;
	break;
      }
    }
    
    if (inside) return 0.0;
    
    for (unsigned i=0; i<kd->dims; i++) {
      double d = point[i] - kd->center(myKD)[i];
      double r = 0.5*kd->Range(myKD)[i];
      dist = std::min<double>(dist, fabs(d - r));
      dist = std::min<double>(dist, fabs(d + r));
    }
  }
  
  return dist;
}

/**
   Point must be at least this far from the kd box (lower limit)
 */
double 
Delta::maxDist(KDTree::indx myKD, const double* point, const KDTree* kd) const
{
  double dist = 0;

  if (kd->isLeaf(myKD)) {
    for (unsigned i=0; i<kd->dims; i++) {
      double d = point[i] - kd->center(myKD)[i];
      dist = std::max<double>(dist, fabs(d));
    }

  } else {

    for (unsigned i=0; i<kd->dims; i++) {
      double d = point[i] - kd->center(myKD)[i];
      double r = 0.5*kd->Range(myKD)[i];
      dist = std::max<double>(dist, fabs(d - r));
      dist = std::max<double>(dist, fabs(d + r));
    }
  }

  return dist;
}


/**
   Will sort kd node list by minimum distance possible
 */
struct DeltaLess {
  bool operator()(const Delta& a, const Delta& b) {
    return a.Min() < b.Min();
  }
};

/**
   nns is a K x N matrix
   dists is a 1 x N vector
   points is a D x N matrix
*/
void KDTree::kNearestNeighbors
(indx *nns, double *dists, const double *points, int N, int kk) const
{
  typedef std::multimap<Delta, KDTree::indx, DeltaLess> myMap;
  myMap m;

  int leavesDone = 0;
  indx k = kk;
  
  // Do each point sequentially
  //
  for (indx target = 0; target < N*dims; target += dims, ++leavesDone) {  
    
    Delta leastDist = Delta(root(), points+target, this), curDist;
    
    // Clean map and insert the root 
    //
    m.erase(m.begin(), m.end());
    m.insert(myMap::value_type(leastDist, root()));
    
    // Examining nodes in order of min dist means that when you see a
    // point, it is the closest point remaining
    //
    std::map<double, indx> dist2;

    while(! m.empty() ) {

      indx current = (*m.begin()).second;
      m.erase(m.begin());
      
      if (isLeaf(current)) {
	double dist = 0;
	for (indx j=0; j<dims; j++) {
	  double v = centers[current*dims+j] - points[target+j];
	  dist += v*v;
	}
	dist2.insert(std::pair<double, indx>(dist, current));
	
      } else {

	// Add both children
	m.insert
	  (myMap::value_type(Delta(left_child[current], points+target, this), 
			     left_child[current]));

	m.insert
	  (myMap::value_type(Delta(right_child[current], points+target, this), 
			     right_child[current]));
	
	myMap::iterator it   = m.begin();
	myMap::iterator last = it++;
      
	// get rid of ineligible branches
	//
	leastDist = last->first;
	while (it != m.end()) {
	  curDist = it->first;
	  current = it->second;
	  last    = it++;
	  if (curDist.Min() > leastDist.Max() && Npts(leastDist.key()) >= k) 
	    {
	      m.erase(last);
	    }

	}  // end pruning
      }
    } // end node search
    
    // clear out the remaining points
    
    m.clear();
    
    std::map<double, indx>::iterator jt = dist2.begin();
    unsigned nnsSoFar = 0;
    for (; nnsSoFar<k && jt!=dist2.end(); nnsSoFar++, jt++) 
      nns[leavesDone*k + nnsSoFar] = jt->second;

    dists[leavesDone] = sqrt(dist2.begin()->first);
    
    for(indx i=leavesDone*k; i < leavesDone*k+nnsSoFar; i++)
      nns[i] = getIndexOf(nns[i]);
    
    for(indx i=leavesDone*k+nnsSoFar; i<(leavesDone+1)*k; i++)
      nns[i] = NO_CHILD;
    
  } // end all nearest neighbors
  
}

double KDTree::Vol(KDTree::indx i)
{
  // This construction helps prevent overflow for very high
  // dimensionality
  double vol = 0.0;
  for (unsigned d=0; d<dims; d++) {
    double L = range(i)[d];
    if (L<DBL_MIN) { return 0.0; }
    vol += log(L);
  }
  return exp(vol);
}

double KDTree::VOL(KDTree::indx i)
{
  // This construction helps prevent overflow for very high
  // dimensionality
  double vol = 0.0;
  for (unsigned d=0; d<dims; d++) {
    double L = Range(i)[d];
    if (L<DBL_MIN) { return 0.0; }
    vol += log(L);
  }
  return exp(vol);
}

void KDTree::Volume(KDTree::indx root, unsigned long ncut, double& volume)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [Volume]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }

  if (frontier) {
    //------------------------------------------------------------
    // Fiducial volume in this cell
    //------------------------------------------------------------
    if (geom_volume)
      volume += sqrt(VOL(root)*Vol(root));
    else if (full_volume)
      volume += VOL(root);
    else
      volume += Vol(root);
  } else {
    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    Volume(indxL, ncut, volume);
    Volume(indxR, ncut, volume);
  }
}

void KDTree::Integral(KDTree::indx root, unsigned long ncut,
		      std::vector<double>& result,
		      std::vector<double> (*func)(std::vector<double>&))
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [Integral]" << std::endl;
      return;
    }  

    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }

  if (frontier) {

    //------------------------------------------------------------
    // Compute a term in the Riemann sum
    //------------------------------------------------------------

    std::vector<double> ctr(dims);
    for (unsigned d=0; d<dims; d++) ctr[d] =  center(root)[d];

    //------------------------------------------------------------
    // First, do the fiducial volume in this cell
    //------------------------------------------------------------
    double vol;
    if (geom_volume)
      vol = sqrt(VOL(root)*Vol(root));
    else if (full_volume)
      vol = VOL(root);
    else
      vol = Vol(root);
    
    std::vector<double> ret = (*func)(ctr);
    for (unsigned i=0; i<result.size(); i++) result[i] += ret[i] * vol;
      
  } else {

    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    
    Integral(indxL, ncut, result, func);
    Integral(indxR, ncut, result, func);
    
  }
    
}

//
// Calculate the statistics of level "root" based on the statistics of
// its left and right children.
//
void KDTree::Diagnostics(std::ostream& out, unsigned long ncut)
{
  double volume = 0.0;
  
  Volume(root(), ncut, volume);
  out << "Volume=" << volume << std::endl;
}


void KDTree::Diagnostics(std::ostream& out, unsigned long ncut, int dim,
			 std::vector<double> (*func)(std::vector<double>&))
{
  std::vector<double> result(dim, 0.0);
  ncut = std::max<unsigned long>(ncut, 1);
  
  Integral(root(), ncut, result, func);
  out << "Result=";
  for (int j=0; j<dim; j++) out << std::setw(12) << std::left << result[j];
  out << std::endl;
}


void KDTree::Expectation(unsigned long ncut, 
			 std::vector< std::vector<double> >& data, 
			 std::vector<double>& result)
{
  sz = data[0].size();
  
  std::vector<double> datum(sz, 0);
  std::vector< std::vector<double> > treedata(num_points*2);
  for (indx j=0, i=num_points; j<num_points; i++, j++) {
    treedata[j] = datum;
    treedata[i] = data[j];
  }
  
  Expect(root(), ncut, treedata);
  
  result = treedata[root()];
}

typedef std::vector<unsigned> DU;
typedef std::vector<double>   DV;
typedef std::vector<DV>       DV2;
typedef	std::map<int, DV>     intMapDV;
typedef	std::map<int, DV2>    intMapDV2;

void KDTree::IntegralEval
(std::vector< std::vector<double> >& data, std::vector<unsigned>& mult,
 intMapDV& result, intMapDV& lower, intMapDV& upper, intMapDV& vmean)
{
  // The size of the point data record
  sz = data[0].size();
  
  // An empty result datum
  DV  datum(sz, 0.0);
  DV2 tree(num_points*2);
  DU  dupl(num_points*2);

  for (indx j=0, i=num_points; j<num_points; i++, j++) {
    dupl[j] = 0;
    // Enter the data in permuted order
    dupl[i] = mult[permutation[i]];
    tree[i] = data[permutation[i]];
  }

  intMapDV2 medn, lowr, uppr, mean;

  // The data tree and quantile bounds
  for (unsigned l=0; l<2; l++) {
    medn[l] = DV2(num_points*2);
    lowr[l] = DV2(num_points*2);
    uppr[l] = DV2(num_points*2);
    mean[l] = DV2(num_points*2);
  
    for (indx j=0, i=num_points; j<num_points; i++, j++) {
      medn[l][i] = lowr[l][i] = uppr[l][i] = mean[l][i] = datum;
      medn[l][j] = lowr[l][j] = uppr[l][j] = mean[l][j] = datum;
    }
  }
  
  vfrac = computeTrim(tree);
  IntegralPC(root(), ncut, tree, dupl, medn, lowr, uppr, mean);
  
  for (unsigned l=0; l<2; l++) {
    result[l] = medn[l][root()];
    lower[l]  = lowr[l][root()];
    upper[l]  = uppr[l][root()];
    vmean[l]  = mean[l][root()];
  }
}

void KDTree::MedianEval(unsigned long ncut, 
			std::vector< std::vector<double> >& data, 
			std::vector<double>& result)
{
  sz = data[0].size();
  ncut = std::max<unsigned long>(ncut, 2);
  
  DV  datum(sz, 0);
  DV2 treedata(num_points*2);
  
  for (indx j=0, i=num_points; j<num_points; i++, j++) {
    treedata[j] = datum;	// Enter the data in permuted order
    treedata[i] = data[permutation[i]];
  }
  
  Median(root(), ncut, treedata);
  
  result = treedata[root()];
}

void KDTree::Expect(KDTree::indx root, unsigned long ncut, 
		    std::vector< std::vector<double> >& treedata)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [Expect]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }

  if (frontier) {
    // Weight the average since the count criteria
    // is now met . . .
    for (unsigned j=0; j<sz; j++) {
      treedata[root][j] = 
	weights[indxL] * treedata[indxL][j]  +  
	weights[indxR] * treedata[indxR][j];
    }
  } else {
    Expect(indxL, ncut, treedata);
    Expect(indxR, ncut, treedata);
  }  

  //------------------------------------------------------------
  // Compute the weighted average
  //------------------------------------------------------------
  for (unsigned j=0; j<sz; j++) {
    treedata[root][j] =
      ( weights[indxL] * treedata[indxL][j]  + 
	weights[indxR] * treedata[indxR][j]  ) / weights[root];
  }
}

void KDTree::Median(KDTree::indx root, unsigned long ncut, 
		    std::vector< std::vector<double> >& treedata)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [Median]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }

  if (frontier) {
    //------------------------------------------------------------
    // Compute the median
    //------------------------------------------------------------
    for (unsigned j=0; j<sz; j++) {
      std::vector<double> coord;

      for (indx k=lo_leaf[root]; k<=hi_leaf[root]; k++) 
	coord.push_back(treedata[k][j]);
      
      std::sort(coord.begin(), coord.end());
      treedata[root][j] = weights[root] * coord[coord.size()/2];
    }
    
  } else {
    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    Median(indxL, ncut, treedata);
    Median(indxR, ncut, treedata);
  }

  //------------------------------------------------------------
  // Simply sum the children
  //------------------------------------------------------------
  for (unsigned j=0; j<sz; j++) {
    treedata[root][j] = treedata[indxL][j] + treedata[indxR][j];
  }
    
}



void KDTree::IntegralPC(KDTree::indx root, unsigned long ncut,
			DV2& tree, DU& mult,
			intMapDV2& medn, intMapDV2& lowr,
			intMapDV2& uppr, intMapDV2& mean)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [IntegralPC]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }

  if (frontier) {

    if (TrimOK(root)) {

      //------------------------------------------------------------
      // Fiducial volume in this cell
      //------------------------------------------------------------

      double vol;
      if (geom_volume)
	vol = sqrt(VOL(root)*Vol(root));
      else if (full_volume)
	vol = VOL(root);
      else
	vol = Vol(root);
    
      //------------------------------------------------------------
      // Compute the weights
      //------------------------------------------------------------
      
      std::map<double, double> pops;
      std::map<double, double>::iterator it;

      for (indx i=lo_leaf[root]; i<=hi_leaf[root]; i++) {
	if ( (it = pops.find(beta[i])) == pops.end()) {
	  pops[beta[i]] = 1;
	} else {
	  it->second += 1;
	}
      }
      
      dpair pmax;
      for (it = pops.begin(); it != pops.end(); it++) {
	if      (it == pops.begin())     pmax = *it;
	else if (it->second>pmax.second) pmax = *it;
      }
      
      //------------------------------------------------------------
      // Compute the moments of the field values in the cell
      //------------------------------------------------------------
      
      for (unsigned j=0; j<sz; j++) {
	
	std::vector< std::vector<double> > v(2);

	for (indx i=lo_leaf[root]; i<=hi_leaf[root]; i++) {
	  for (unsigned k=0; k<mult[i]; k++) {
	    if (beta[i] == pmax.first) v[0].push_back(tree[i][j]);
	    v[1].push_back(tree[i][j]);
	  }
	}
	
	for (unsigned l=0; l<2; l++) {
	  if (v[l].size()==0) continue;

	  std::sort(v[l].begin(), v[l].end());
	
	  unsigned lo = 
	    std::min<unsigned>
	    (floor(v[l].size()*lower_quant+0.5), v[l].size()-1);
	  lo = std::max<unsigned>(lo, 0);
	  
	  unsigned md = 
	    std::min<unsigned>
	    (floor(v[l].size()*0.5+0.5), v[l].size()-1);
	  md = std::max<unsigned>(md, 0);
	
	  unsigned hi = 
	    std::min<unsigned>
	    (floor(v[l].size()*upper_quant+0.5), v[l].size()-1);
	  hi = std::max<unsigned>(hi, 0);
	  
	  medn[l][root][j] = v[l][md] * vol;
	
	  mean[l][root][j] = 0.0;
	  for (unsigned k=0; k<v[l].size(); k++) 
	    mean[l][root][j] += v[l][k]/v[l].size() * vol;
	  
	  lowr[l][root][j] = v[l][lo] * vol;
	  
	  uppr[l][root][j] = v[l][hi] * vol;
	}
      }      
    }
    
  } else {

    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------

    IntegralPC(indxL, ncut, tree, mult, medn, lowr, uppr, mean);
    IntegralPC(indxR, ncut, tree, mult, medn, lowr, uppr, mean);
    
    //------------------------------------------------------------
    // Sum up all the hyperboxes
    //------------------------------------------------------------
  
    for (unsigned j=0; j<sz; j++) {
      for (unsigned l=0; l<2; l++) {
	medn[l][root][j] += medn[l][indxL][j] + medn[l][indxR][j];
	lowr[l][root][j] += lowr[l][indxL][j] + lowr[l][indxR][j];
	uppr[l][root][j] += uppr[l][indxL][j] + uppr[l][indxR][j];
	mean[l][root][j] += mean[l][indxL][j] + mean[l][indxR][j];
      }
    }
  }
}


class nodeInfo {
public:
  unsigned long nodes;
  unsigned long N, minN, maxN;
  double meanN, minV, maxV, meanV, vvol;
  std::vector< std::vector<double> > upper;
  std::vector< std::vector<double> > lower;
  std::vector<double> value;
  
  nodeInfo() 
  {
    nodes = N = 0;
    minN  = LONG_MAX;
    maxN  = 0;
    meanN = meanV = vvol = 0;
    minV  = DBL_MAX;
    maxV  = 0;
  }
};


void KDTree::NodeList(KDTree::indx root, unsigned lev, unsigned long ncut,
		      std::set< std::pair<indx, unsigned> >& leaves)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [NodeList]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }
  
  if (frontier) {

    if (TrimOK(root)) leaves.insert(std::pair<indx, unsigned>(root, lev));

  } else {
    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    NodeList(indxL, lev+1, ncut, leaves);
    NodeList(indxR, lev+1, ncut, leaves);
  }  

}

void KDTree::MeasureList(std::ostream& out, unsigned long ncut,
			 std::vector< std::vector<double> >& data)
{
  createMeasureList(ncut, data);
  
  std::map<double, LeafElem>::iterator u;
  
  std::string labels[6] = {"Count", "Key",
			   "Like", "PDF", "CDF", "P"};
  
  out << std::left << "# " << std::setw(6) << labels[0];
  for (int i=1; i<6; i++) 
    out << std::left << "+ " << std::setw(10) << labels[i];
  out << std::endl << std::left << "# " << std::setw(6) << 1;
  for (int i=1; i<6; i++) 
    out << std::left << "+ " << std::setw(10) << i+1;
  out << std::endl << std::left << std::setw(8) << std::setfill('-') << "#";
  for (int i=1; i<6; i++) 
    out << std::left << std::setw(12) << std::setfill('-') << "+";
  out << std::endl << std::setfill(' ');
  
  unsigned indx = 0;
  for (u=measureList.begin(); u!=measureList.end(); u++) {
      out << std::left << std::setw(8) << indx++
	  << std::left << std::setw(12) << u->second.key
	  << std::left << std::setw(12) << u->second.L
	  << std::left << std::setw(12) << u->second.dL
	  << std::left << std::setw(12) << u->second.cdf
	  << std::endl;
  }
}

void KDTree::createMeasureList(unsigned long ncut,
			       std::vector< std::vector<double> >& data)
{
  if (measureList.size()>0) return;

  ncutM = ncut;

  std::multimap<double, LeafElem> leaves;

  PDFList(root(), ncut, data, leaves);

  minL =  DBL_MAX;
  maxL = -DBL_MAX;

  // Compute the normalization and extrema in likelihood summing from
  // maximum to minimum (e.g. measure function)
  double sumL = 0.0;
  for (std::multimap<double, LeafElem>::reverse_iterator
	 rl=leaves.rbegin(); rl!=leaves.rend(); rl++) {
    sumL += rl->second.dL;
    rl->second.cdf = sumL;
    minL = std::min<double>(minL, rl->second.L);
    maxL = std::max<double>(maxL, rl->second.L);
  }
  
  // The cell indx keyed value vector
  typedef std::pair<indx,   LeafElem> Melem;

  // The CDF keyed [value vector, cell indx]
  typedef std::pair<double, LeafElem> Lelem;

  for (std::multimap<double, LeafElem>::iterator
	 l=leaves.begin(); l!=leaves.end(); l++) {
    // Normalize the posterior density and CDF
    l->second.cdf /= sumL;
      
    // Map the CDF to the data vector (LeafElem)
    measureList.insert(Lelem(l->second.cdf, l->second));
      
    // Map the cell index to the data vector (LeafElem)
    measureMap.insert(Melem(l->second.cell, l->second));
  }
}

KDTree::indx KDTree::FThetaInv(double u)
{
  std::map<double, LeafElem>::iterator it = measureList.lower_bound(u);
  if (it != measureList.end()) {
    return it->second.point;
  } else {
    return -1;
  }
}

double KDTree::FTheta(const std::vector<double>& T)
{
  KDTree::indx k = find(T);
  if (measureMap.find(k) == measureMap.end()) return 1.0;
  return measureMap[k].cdf;
}

double KDTree::PosteriorProb(const std::vector<double>& T)
{
  KDTree::indx k = find(T);
  if (measureMap.find(k) == measureMap.end()) return 0.0;
  return measureMap[k].P;
}

void KDTree::PDFList(KDTree::indx root, unsigned long ncut,
		     std::vector< std::vector<double> >& data,
		     std::multimap<double, LeafElem>& leaves)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [PDFList]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(indxL) > ncut || Npts(indxR) > ncut) frontier = false;

  }
  
  if (frontier) {

    if (TrimOK(root)) {

      //------------------------------------------------------------
      // Fiducial volume in this cell
      //------------------------------------------------------------

      double vol;
      if (geom_volume)
	vol = sqrt(VOL(root)*Vol(root));
      else if (full_volume)
	vol = VOL(root);
      else
	vol = Vol(root);
      
      //------------------------------------------------------------
      // Find the median likelihood value
      //------------------------------------------------------------
      
      indx k;
      std::vector< std::pair<double, indx> > lval;
      double twght = 0.0;

      for (indx i=lo_leaf[root]; i<=hi_leaf[root]; i++) {
	k = permutation[i];
	lval.push_back(std::pair<double, indx>(data[k][1], k));
	twght += weights[k];
      }
      std::sort(lval.begin(), lval.end());
      
      //------------------------------------------------------------
      // Insert the data
      //------------------------------------------------------------
    
      LeafElem p;
				// Cell index
      p.cell  = root;
				// Particle id for fiducial point
      p.point = lval[lval.size()/2].second;
				// Number of particles in cell
      p.N     = Npts(root);
				// Weight of particles in cell
      p.W     = twght;
				// Center of partition interval (for keying)
      p.key   = 0.5*(lower[root] + upper[root]);
				// Will be the CDF value for this cell
      p.cdf   = 0.0;
				// The posterior value for the fiducial point
      p.P     = data[p.point][0];
				// The likelihood value for the fiducial point
      p.L     = data[p.point][1];
				// The weight in the cell (for computing CDF)
      p.dL    = vol*p.L;
				// Cell volume
      p.V     = vol;
				// Key and sort by likelihood value
      leaves.insert(std::pair<double, LeafElem>(p.L, p));

    }

  } else {
    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    if (Npts(indxL)>ncut) PDFList(indxL, ncut, data, leaves);
    if (Npts(indxR)>ncut) PDFList(indxR, ncut, data, leaves);
  }
  
}



void KDTree::LevelList(std::ostream& out, bool limits)
{
  std::map<unsigned, nodeInfo> llist;
  std::map<unsigned, nodeInfo>::iterator itl;
  std::set< std::pair<indx, unsigned> > leaves;
  std::set< std::pair<indx, unsigned> >::iterator it;
  unsigned long totalN = 0, mm, l;
  double vol;
  indx p;
  
  NodeList(root(), 0, ncut, leaves);
  
  for (it=leaves.begin(); it!=leaves.end(); it++) {
    p = it->first;
    l = it->second;
    
    //
    // Fiducial volume in this cell
    //
    if (geom_volume)
      vol = sqrt(VOL(p)*Vol(p));
    else if (full_volume)
      vol = VOL(p);
    else
      vol = Vol(p);

    llist[l].minN = std::min<unsigned long>(llist[l].minN, Npts(p));
    llist[l].maxN = std::max<unsigned long>(llist[l].maxN, Npts(p));
    llist[l].meanN = 
      (llist[l].meanN*llist[l].nodes + Npts(p))/(llist[l].nodes+1);
    
    llist[l].vvol = vol;
    llist[l].minV = std::min<double>(llist[l].minV, vol);
    llist[l].maxV = std::max<double>(llist[l].maxV, vol);
    llist[l].meanV = 
      (llist[l].meanV*llist[l].nodes + vol)/(llist[l].nodes+1);
    
    llist[l].nodes++;
    llist[l].N += Npts(p);
    totalN += Npts(p);
    
    if (limits) {
      std::vector<double> lowr(dims), uppr(dims);
      for (unsigned k=0; k<dims; k++) {
	lowr[k] = center(p)[k] - 0.5*range(p)[k];
	uppr[k] = center(p)[k] + 0.5*range(p)[k];
      }
      
      llist[l].lower.push_back(lowr);
      llist[l].upper.push_back(uppr);
    }
  }
  
  out << std::endl 
      << "---------------------" << std::endl
      << "KDTree build analysis" << std::endl
      << "---------------------" << std::endl
      << std::endl;
  
  out << std::setw(4) << "Lev " << " | "
      << std::setw(8) << "Nodes"
      << std::setw(8) << "# pts"
      << std::setw(6) << "MinN"
      << std::setw(6) << "MaxN"
      << std::setw(6) << "MeanN"
      << std::setw(9) << "Vol"
      << std::setw(9) << "MinV"
      << std::setw(9) << "MeanV"
      << std::setw(9) << "MaxV";
  if (limits) {
    out << std::setw(9) << "CumV";
    std::ostringstream sout;
    for (indx j=0; j<dims; j++) {
      sout.str(""); sout << "Min #" << j+1;
      out << std::setw(12) << sout.str();
      sout.str(""); sout << "Max #" << j+1;
      out << std::setw(12) << sout.str();
    }
  }
  out << std::endl
      << std::setw(4) << "+---" << " | "
      << std::setw(8) << "+------"
      << std::setw(8) << "+------"
      << std::setw(6) << "+----"
      << std::setw(6) << "+----"
      << std::setw(6) << "+----"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------"
      << std::setw(9) << "+-------";
  if (limits) {
    out << std::setw(9) << "+-------";
    for (indx j=0; j<dims; j++) {
      out << std::setw(12) << "+---------"
	  << std::setw(12) << "+---------";
    }
  }
  out << std::endl;
  
  double cumv=0.0, totv=0.0;
  if (limits)
    for (itl=llist.begin(); itl!=llist.end(); itl++) totv += itl->second.vvol;
  
  unsigned old_prec = out.precision(2);
  
  for (itl=llist.begin(); itl!=llist.end(); itl++) {
    mm = static_cast<unsigned long>(floor(itl->second.meanN));
    out << std::setw(4) << itl->first << " | "
	<< std::setw(8) << itl->second.nodes
	<< std::setw(8) << itl->second.N
	<< std::setw(6) << itl->second.minN
	<< std::setw(6) << itl->second.maxN
	<< std::setw(6) << mm
	<< std::setw(9) << itl->second.vvol
	<< std::setw(9) << itl->second.minV
	<< std::setw(9) << itl->second.meanV
	<< std::setw(9) << itl->second.maxV;
    if (limits) {
      cumv += itl->second.vvol;
      out << std::setw(9) << cumv/totv;
      // out.setf(std::ios::scientific);	  // Set scientific format
      unsigned prec = out.precision(3); // Set precision
      for (indx j=0; j<dims; j++)
	out << std::setw(12) << itl->second.lower[0][j]
	    << std::setw(12) << itl->second.upper[0][j];
      for (indx k=1; k<itl->second.lower.size(); k++) {
	out << std::endl << std::setw(4) << "*" << " | "
	    << std::setw(8) << "." << std::setw(8) << "."
	    << std::setw(6) << "." << std::setw(6) << "."
	    << std::setw(6) << "." << std::setw(9) << "."
	    << std::setw(9) << "." << std::setw(9) << "."
	    << std::setw(9) << "." << std::setw(9) << ".";
	for (indx j=0; j<dims; j++)
	  out << std::setw(12) << itl->second.lower[k][j]
	      << std::setw(12) << itl->second.upper[k][j];
      }
      // out.unsetf(std::ios::scientific); // Restore original format
      out.precision(prec);		  // Restore precision
    }
    out << std::endl;
  }
  out << std::endl;
  out << "  Total # nodes=" << leaves.size()
      << "  Total # point=" << totalN
      << std::endl << std::endl;
  
  out.precision(old_prec);
}



void KDTree::IntegralList(std::ostream& out,
			  DV2& data, DU& mult, bool allcells)
{
  std::map<unsigned, nodeInfo> llist;
  std::map<unsigned, nodeInfo>::iterator itl;
  std::set< std::pair<indx, unsigned> > leaves;
  std::set< std::pair<indx, unsigned> >::iterator it;
  unsigned long totalN = 0, mm, l;
  indx p;
  
  NodeList(root(), 0, ncut, leaves);
  
  sz = data[0].size();
  
  DV2 tree(num_points*2);
  DU  dupl(num_points*2);
  for (indx j=0, i=num_points; j<num_points; i++, j++) {
    // Enter the data in permuted order
    tree[i] = data[permutation[i]];
    dupl[i] = mult[permutation[i]];
  }
  
  if (allcells) {
    
    out << std::endl 
	<< "------------------------------" << std::endl
	<< "KDTree build/integral analysis" << std::endl
	<< "------------------------------" << std::endl
	<< std::endl;
    
    out << std::setw(4) << "Lev " << " | "
	<< std::setw(8) << "# pts"
	<< std::setw(9) << "Vol";
    for (unsigned j=0; j<sz; j++) {
      std::ostringstream sout1, sout2;
      sout1 << "Posn [" << j+1 << "]";
      sout2 << "Csum [" << j+1 << "]";
      out << std::setw(12) << sout1.str()
	  << std::setw(12) << sout2.str();
    }
    
    out << std::endl
	<< std::setw(4) << "+---" << " | "
	<< std::setw(8) << "+------"
	<< std::setw(9) << "+-------";
    for (unsigned j=0; j<sz; j++) {
      out << std::setw(12) << "+---------"
	  << std::setw(12) << "+---------";
    }
    out << std::endl;
    
    unsigned old_prec = out.precision(2);
    
    typedef std::pair<indx, unsigned> pelem;
    typedef std::map<double, pelem>   vsort;
    
    vsort vlist;
    for (it=leaves.begin(); it!=leaves.end(); it++)
      vlist[Vol(it->first)] = pelem(it->first, it->second);
    
    
    DV     cvalue(sz, 0.0), cvalue0(sz, 0.0), value(sz), vols;
    DV2    intg;
    DU     levs, npts;
    double vol;
    
    for (vsort::iterator itr=vlist.begin(); itr!=vlist.end(); itr++) {
      
      p = itr->second.first;
      l = itr->second.second;
      
      //
      // Fiducial volume in this cell
      //
      if (geom_volume)
	vol = sqrt(VOL(p)*Vol(p));
      else if (full_volume)
	vol = VOL(p);
      else
	vol = Vol(p);

      for (unsigned j=0; j<sz; j++) {

	std::vector<double> v;

	for (indx i=lo_leaf[p]; i<=hi_leaf[p]; i++) {
	  for (unsigned n=0; n<dupl[i]; n++) v.push_back(tree[i][j]);
	}
	
	std::sort(v.begin(), v.end());
	
	unsigned k;
	k = std::max<unsigned int>(0, 0.5*v.size());
	k = std::min<unsigned int>(k, v.size() - 1);
	
	value[j] = v[k] * vol;
	cvalue0[j] += value[j];
      }
      
      levs.push_back(l);
      npts.push_back(Npts(p));
      vols.push_back(vol);
      intg.push_back(value);
      
      totalN += Npts(p);
    }
    
    for (unsigned k=0; k<levs.size(); k++) {
      
      out << std::setw(4) << levs[k] << " | "
	  << std::setw(8) << npts[k]
	  << std::setw(8) << vols[k];
      
      unsigned prec = out.precision(3); // Set precision
      for (unsigned j=0; j<sz; j++) {
	cvalue[j] += intg[k][j];
	out << std::setw(12) << intg[k][j]
	    << std::setw(12) << cvalue[j]/cvalue0[j];
      }
      out.precision(prec);	      // Restore precision
      out << std::endl;
    }
    out << std::endl;
    out << "  Total # nodes=" << leaves.size()
	<< "  Total # point=" << totalN
	<< std::endl << std::endl;
    
    out.precision(old_prec);
    
  } else {
    
    for (it=leaves.begin(); it!=leaves.end(); it++) {
      p = it->first;
      l = it->second;
      
      //
      // Fiducial volume in this cell
      //
      double vol;
      if (geom_volume)
	vol = sqrt(VOL(p)*Vol(p));
      else if (full_volume)
	vol = VOL(p);
      else
	vol = Vol(p);

      llist[l].minN = std::min<unsigned long>(llist[l].minN, Npts(p));
      llist[l].maxN = std::max<unsigned long>(llist[l].maxN, Npts(p));
      llist[l].meanN = 
	(llist[l].meanN*llist[l].nodes + Npts(p))/(llist[l].nodes+1);
      
      
      llist[l].vvol = vol;
      llist[l].minV = std::min<double>(llist[l].minV, vol);
      llist[l].maxV = std::max<double>(llist[l].maxV, vol);
      llist[l].meanV = 
	(llist[l].meanV*llist[l].nodes + vol)/(llist[l].nodes+1);
      
      llist[l].nodes++;
      llist[l].N += Npts(p);
      totalN += Npts(p);
      
      if (llist[l].value.size() != sz) 
	llist[l].value = std::vector<double>(sz, 0.0);
      
      for (unsigned j=0; j<sz; j++) {

	std::vector<double> v;

	for (indx i=lo_leaf[p]; i<=hi_leaf[p]; i++) {
	  for (unsigned n=0; n<dupl[i]; n++) v.push_back(tree[i][j]);
	}
	
	std::sort(v.begin(), v.end());
	
	unsigned k = std::min<unsigned int>(std::max<unsigned int>(0.5*v.size(), 0), v.size()-1);
	llist[l].value[j] += v[k] * vol;
      }
    }

    out << std::endl 
	<< "------------------------------" << std::endl
	<< "KDTree build/integral analysis" << std::endl
	<< "------------------------------" << std::endl
	<< std::endl;
    
    out << std::setw(4) << "Lev " << " | "
	<< std::setw(8) << "Nodes"
	<< std::setw(8) << "# pts"
	<< std::setw(6) << "MinN"
	<< std::setw(6) << "MaxN"
	<< std::setw(6) << "MeanN"
	<< std::setw(9) << "Vol"
	<< std::setw(9) << "MinV"
	<< std::setw(9) << "MeanV"
	<< std::setw(9) << "MaxV";
    for (unsigned j=0; j<sz; j++) {
      std::ostringstream sout1, sout2;
      sout1 << "Posn [" << j+1 << "]";
      sout2 << "Csum [" << j+1 << "]";
      out << std::setw(12) << sout1.str()
	  << std::setw(12) << sout2.str();
    }
    out << std::endl
	<< std::setw(4) << "+---" << " | "
	<< std::setw(8) << "+------"
	<< std::setw(8) << "+------"
	<< std::setw(6) << "+----"
	<< std::setw(6) << "+----"
	<< std::setw(6) << "+----"
	<< std::setw(9) << "+-------"
	<< std::setw(9) << "+-------"
	<< std::setw(9) << "+-------"
	<< std::setw(9) << "+-------";
    for (unsigned j=0; j<sz; j++) {
      out << std::setw(12) << "+---------"
	  << std::setw(12) << "+---------";
    }
    out << std::endl;
    
    std::vector<double> cvalue(sz, 0.0);
    unsigned old_prec = out.precision(2);
    
    for (itl=llist.begin(); itl!=llist.end(); itl++) {
      mm = static_cast<unsigned long>(floor(itl->second.meanN));
      out << std::setw(4) << itl->first << " | "
	  << std::setw(8) << itl->second.nodes
	  << std::setw(8) << itl->second.N
	  << std::setw(6) << itl->second.minN
	  << std::setw(6) << itl->second.maxN
	  << std::setw(6) << mm
	  << std::setw(9) << itl->second.vvol
	  << std::setw(9) << itl->second.minV
	  << std::setw(9) << itl->second.meanV
	  << std::setw(9) << itl->second.maxV;
      // out.setf(std::ios::scientific);   // Set scientific format
      unsigned prec = out.precision(3); // Set precision
      for (unsigned j=0; j<sz; j++) {
	cvalue[j] += itl->second.value[j];
	out << std::setw(12) << itl->second.value[j]
	    << std::setw(12) << cvalue[j];
      }
      // out.unsetf(std::ios::scientific); // Restore original format
      out.precision(prec);	      // Restore precision
      out << std::endl;
    }
    out << std::endl;
    out << "  Total # nodes=" << leaves.size()
	<< "  Total # point=" << totalN
	<< std::endl << std::endl;
    
    out.precision(old_prec);
  }
  
}


void KDTree::IntegralCellList(std::ostream& out, unsigned long ncut,
			      std::vector< std::vector<double> >& data)
{
  std::map<unsigned, nodeInfo> llist;
  std::map<unsigned, nodeInfo>::iterator itl;
  std::set< std::pair<indx, unsigned> > leaves;
  std::set< std::pair<indx, unsigned> >::iterator it;
  unsigned long l;
  indx p;
  
  NodeList(root(), 0, ncut, leaves);
  
  sz = data[0].size();
  
  std::vector< std::vector<double> > tree(num_points*2);
  for (indx j=0, i=num_points; j<num_points; i++, j++) {
    // Enter the data in permuted order
    tree[i] = data[permutation[i]];
  }
  
  typedef std::pair<indx, unsigned> pelem;
  typedef std::map<double, pelem>   vsort;
  
  vsort vlist;
  for (it=leaves.begin(); it!=leaves.end(); it++) {
    //
    // Fiducial volume in this cell
    //
    double vol;
    if (geom_volume)
      vol = sqrt(VOL(it->first)*Vol(it->first));
    else if (full_volume)
      vol = VOL(it->first);
    else
      vol = Vol(it->first);

    vlist[vol] = pelem(it->first, it->second);
  }
  
  
  std::vector<double> cvalue(sz, 0.0), cvalue0(sz, 0.0), value(sz), vols;
  std::vector< std::vector<double> > intg;
  std::vector<unsigned> levs, npts;
  
  out.setf(std::ios::scientific); // Set scientific format
  
  for (vsort::iterator itr=vlist.begin(); itr!=vlist.end(); itr++) {
    
    p = itr->second.first;
    l = itr->second.second;
    
    //
    // Fiducial volume in this cell
    //
    double vol;
    if (geom_volume)
      vol = sqrt(VOL(p)*Vol(p));
    else if (full_volume)
      vol = VOL(p);
    else
      vol = Vol(p);

    for (unsigned j=0; j<sz; j++) {

      std::vector<double> v;

      for (indx i=lo_leaf[p]; i<=hi_leaf[p]; i++) v.push_back(tree[i][j]);
      
      std::sort(v.begin(), v.end());
      
      unsigned k;
      k = std::max<unsigned int>(0, 0.5*v.size());
      k = std::min<unsigned int>(k, v.size() - 1);
      
      value[j] = v[k];
    }
    
    out << std::setw(6)  << l
	<< std::setw(8)  << Npts(p)
	<< std::setw(18) << vol
	<< std::setw(6)  << sz;
    
    for (unsigned j=0; j<sz; j++)  
      out << std::setw(18) << value[j];
    
    out << std::setw(6)  << dims;
    
    for (unsigned k=0; k<dims; k++)
      out << std::setw(18) << Ledge[p*dims + k]
	  << std::setw(18) << Hedge[p*dims + k];
    
    for (unsigned k=0; k<dims; k++)
      out << std::setw(18) << center(p)[k];
    
    for (unsigned k=0; k<dims; k++)
      out << std::setw(18) << range(p)[k];
    
    out << std::endl;
  }
  
  out.unsetf(std::ios::scientific); // Unset scientific format
}


void KDTree::createLebesgueList(unsigned dindx, unsigned long ncut,
				std::vector< std::vector<double> >& data,
				std::vector<unsigned>& mult, bool linear)
{
  // Clean any existing data
  lebesgueLeaves.erase(lebesgueLeaves.begin(), lebesgueLeaves.end());

  for (unsigned l=0; l<2; l++) {
    LminP[l] =  DBL_MAX;
    LmaxP[l] = -DBL_MAX;
  }

  LebesgueList(root(), dindx, ncut, data, mult, lebesgueLeaves, linear);
}

void KDTree::LebesgueList(KDTree::indx root, 
			  unsigned dindx, unsigned long ncut,
			  std::vector< std::vector<double> >& data,
			  std::vector<unsigned>& mult,
			  std::map<int, std::multimap<double, LeafData> >& leaves,
			  bool linear)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [LebesgueList]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }
  
  if (frontier) {

    if (TrimOK(root)) {

      //------------------------------------------------------------
      // Fiducial volume in this cell
      //------------------------------------------------------------
      double vol;
      if (geom_volume)
	vol   = sqrt(VOL(root)*Vol(root));
      else if (full_volume)
	vol   = VOL(root);
      else
	vol   = Vol(root);
    
      //------------------------------------------------------------
      // Compute the weights
      //------------------------------------------------------------
      std::map<double, double> pops;
      std::map<double, double>::iterator it;

      for (indx i=lo_leaf[root]; i<=hi_leaf[root]; i++) {
	if ( (it = pops.find(beta[i])) == pops.end()) {
	  pops[beta[i]] = 1;
	} else {
	  it->second += 1;
	}
      }
    
      dpair pmax;
      for (it = pops.begin(); it != pops.end(); it++) {
	if      (it == pops.begin())     pmax = *it;
	else if (it->second>pmax.second) pmax = *it;
      }
      
      //------------------------------------------------------------
      // Find the extremum probability values
      //------------------------------------------------------------
    
      std::vector< std::vector<double> > pv(2);

      for (indx i=lo_leaf[root]; i<=hi_leaf[root]; i++) {
	indx k = permutation[i];
	
	if (data[k][dindx]>0.0) {
	  for (unsigned n=0; n<mult[k]; n++) {
	    if (beta[i] == pmax.first) pv[0].push_back(data[k][dindx]);
	    pv[1].push_back(data[k][dindx]);
	  }
	} else {
	  std::cerr << "Value oob!" << std::endl;
	}
      }
      
      //------------------------------------------------------------
      // Insert the data
      //------------------------------------------------------------
      for (unsigned l=0; l<2; l++) {
	if (pv[l].size()==0) continue;
      
	std::sort(pv[l].begin(), pv[l].end());

	LeafData p;

	p.vol   = vol;
				// Minimum prob value
	p.minP  = pv[l].front();
				// Median prob value
	unsigned nmed = std::min<unsigned>
	  (floor(pv[l].size()*0.5), pv[l].size()-1);
	nmed = std::max<unsigned>(nmed, 0);

	p.medP  = pv[l][nmed];
				// Minimum prob value
	p.maxP  = pv[l].back();
				// The probability values
	if (pv[l].size()>1)  {
	  double delta, val;
	  if (linear) delta = 1.0/(pv[l].back() - pv[l].front());
	  else        delta = 1.0/(log(pv[l].back()) - log(pv[l].front()));
	  if (pv[l].back() > pv[l].front()) {
	    p.pvals.push_back(dpair(pv[l][0], 1.0));
	    for (unsigned i=1; i<pv[l].size()-1; i++) {
	      if (linear) val = (pv[l].back() - pv[l][i])*delta;
	      else        val = (log(pv[l].back()) - log(pv[l][i]))*delta;
	      p.pvals.push_back(dpair(pv[l][i], val));
	    }
	  } else {
	    double delta = 1.0/pv[l].size();
	    for (unsigned i=0; i<pv[l].size()-1; i++) {
	      p.pvals.push_back(dpair(pv[l][i], 1.0 - delta*i));
	    }
	  }
	  p.pvals.push_back(std::pair<double, double>(pv[l].back(), 0.0));
	} else {
	  p.pvals.push_back(std::pair<double, double>(pv[l].back(), 1.0));
	}
      
				// Key and sort by max probability value
	leaves[l].insert(std::pair<double, LeafData>(p.maxP, p));

				// Tabulate global extrema
	LminP[l] = std::min<double>(LminP[l], p.medP);
	LmaxP[l] = std::max<double>(LmaxP[l], p.medP);
      }
    }

  } else {

    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    LebesgueList(indxL, dindx, ncut, data, mult, leaves, linear);
    LebesgueList(indxR, dindx, ncut, data, mult, leaves, linear);
  }
}


void KDTree::cellList(KDTree::indx root, 
		      const unsigned long ncut,
		      const unsigned dindx, 
		      const std::vector< std::vector<double> >& data,
		      const std::vector<unsigned>& mult,
		      std::vector<CellElem>& leaves)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {

    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);

    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [cellList]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }
  
  if (frontier) {

    if (TrimOK(root)) {

      //------------------------------------------------------------
      // Fiducial cell volume
      //------------------------------------------------------------
      double vol;
      if (geom_volume)
	vol = sqrt(VOL(root)*Vol(root));
      else if (full_volume)
	vol = VOL(root);
      else
	vol = Vol(root);
      
      //------------------------------------------------------------
      // Find the median likelihood value
      //------------------------------------------------------------
      indx k;
      std::vector< std::pair<double, indx> > lval;

      for (indx i=lo_leaf[root]; i<=hi_leaf[root]; i++) {
	k = permutation[i];
	for (unsigned j=0; j<mult[k]; j++)
	  lval.push_back(std::pair<double, indx>(data[k][1], k));
      }
      std::sort(lval.begin(), lval.end());
      
      //------------------------------------------------------------
      // Insert the data
      //------------------------------------------------------------
      CellElem p;
				// Particle id for fiducial point
      p.point = lval[lval.size()/2].second;
				// Number of particles in cell
      p.N     = Npts(root);

      p.lo    = std::vector<double>(dims);
      p.hi    = std::vector<double>(dims);
      for (unsigned d=0; d<dims; d++) {
	p.lo[d] = Low (root)[d];
	p.hi[d] = High(root)[d];
      }
    
				// Desired probability value
      p.P     = data[p.point][dindx];
				// Volume
      p.V     = vol;
				// Key and sort by likelihood value
      leaves.push_back(p);

    }

  } else {
    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    cellList(indxL, ncut, dindx, data, mult, leaves);
    cellList(indxR, ncut, dindx, data, mult, leaves);
  }
  
}

void KDTree::gnuplotDemo(const std::vector< std::vector<double> >& data,
			 const std::vector< unsigned >& mult,
			 const std::vector< double >& ctrs,
			 const unsigned dindx, const unsigned long ncut)
{
  if (dims<2) return;

  std::ostringstream ostr;
  ostr << "vta_cells_kd." << getRandomFileTag() << ".gpl";
  std::ofstream out(ostr.str().c_str());

  if (out) {

    // Check dimensions
    if (dim1 == dim2 || dim1 >= dims || dim2 >= dims) {
      dim1 = 0;
      dim2 = 1;
    }

    std::vector<CellElem> leaves;
    cellList(root(), ncut, dindx, data, mult, leaves);

    std::vector< std::vector<double> > val;
    std::vector<double> eval(5);
    std::vector<int> max_index(dims);

    for (std::vector<CellElem>::iterator
	   elem=leaves.begin(); elem!=leaves.end(); elem++) {

      bool ok = true;		// Is the cell in the desired slice?
      for (unsigned d=0; d<dims; d++) {
	if (d == dim1) continue;
	if (d == dim2) continue;
	if ( elem->lo[d] > ctrs[d] || 
	     elem->hi[d] < ctrs[d]  )  ok = false;
      }

      if (ok) {
	eval[0] = elem->P;
	eval[1] = elem->lo[dim1];
	eval[2] = elem->lo[dim2];
	eval[3] = elem->hi[dim1];
	eval[4] = elem->hi[dim2];
	
	val.push_back(eval);
      }
    }
    
    sort(val.begin(), val.end());

    if (val.size()) {

      double vi   = val.front()[0];
      double vf   = val.back ()[0];
      double delv = vf - vi;
    
      if (delv<=0.0) delv = 1.0;

      for (unsigned n=0; n<val.size(); n++) {
	out << "set obj rect from " 
	    << val[n][1] << "," << val[n][2] << " to "
	    << val[n][3] << "," << val[n][4] << " front fc palette frac "
	    << std::fixed << (val[n][0] - vi)/delv << " lw 0" << std::endl;
      }
    }
      
    out << "set term wxt enhanced" << std::endl;
    out << "set xlabel 'x_1'" << std::endl;
    out << "set ylabel 'x_2'" << std::endl;
    out << "set pm3d map" << std::endl;
    out << "set cbrange [0:1]" << std::endl;
    out << "set colorbox" << std::endl;
    out << "splot [" 
	<< Low(root())[dim1] << ":" << High(root())[dim1] << "] ["
	<< Low(root())[dim2] << ":" << High(root())[dim2] << "] 0*x*y t'' lw 0" 
	<< std::endl;

    out.close();
  }
}


double KDTree::computeTrim(std::vector< std::vector<double> >& tree)
{
  if (!vol_trim)    return 1.0;
  if (vol_computed) return vfrac;

  std::multimap<double, KDTree::indx> sort_frontier;
  unsigned long count = 0, sofar = 0;
  double used = 0;

  findTrimList(root(), ncut, tree, sort_frontier, count);

  std::multimap<double, KDTree::indx>::iterator it;
  unsigned long target = count -  
    static_cast<unsigned long>(floor(count*vol_frac));
  for (it=sort_frontier.begin(); it!=sort_frontier.end(); it++) {
    indx nlo = lo_leaf[it->second]; 
    indx nhi = std::max<indx>(hi_leaf[it->second], nlo+1);
    sofar   += nhi - nlo;
    if (sofar >= target) break;
  }

  trim_list.erase(trim_list.begin(), trim_list.end());
  for (; it!=sort_frontier.end(); it++) {
    trim_list.insert(it->second);
    indx nlo = lo_leaf[it->second]; 
    indx nhi = std::max<indx>(hi_leaf[it->second], nlo+1);
    used    += nhi - nlo;
  }

  vol_computed = true;

  return used/count;
}

void KDTree::findTrimList(KDTree::indx root,
			  const unsigned long ncut,
			  std::vector< std::vector<double> >& tree,
			  std::multimap<double, KDTree::indx> &vol_frontier,
			  unsigned long &vol_count)
{
  bool frontier = true;
  KDTree::indx indxL=0, indxR=0;

  if (!isLeaf(root)) {
    //------------------------------------------------------------
    // Get indices for the children
    //------------------------------------------------------------
    indxL = left (root);
    indxR = right(root);
  
    //------------------------------------------------------------
    // Sanity check
    //------------------------------------------------------------
    if (!validIndex(indxL) || !validIndex(indxR)) {
      std::cerr << "Shouldn't be here [findTrimList]" << std::endl;
      return;
    }
  
    //------------------------------------------------------------
    // Have not passed the threshold?
    //------------------------------------------------------------
    if (Npts(root) > ncut) frontier = false;

  }

  if (frontier) {
    //------------------------------------------------------------
    // Compute the minimum posterior value in the cell
    //------------------------------------------------------------
    
    double minp_ = DBL_MAX;
    indx nlo     = lo_leaf[root]; 
    indx nhi     = hi_leaf[root];

    for (indx i=nlo; i<=nhi; i++)
      minp_ = std::min<double>(minp_, tree[i][0]);

    vol_frontier.insert(std::pair<double, KDTree::indx>(minp_, root));
    vol_count += std::max<indx>(nhi, nlo+1) - nlo;
          
  } else {

    //------------------------------------------------------------
    // Keep decending the tree, if we haven't reached the leaves yet
    //------------------------------------------------------------
    
    findTrimList(indxL, ncut, tree, vol_frontier, vol_count);
    findTrimList(indxR, ncut, tree, vol_frontier, vol_count);
  }
    
}

void KDTree::computeDensity(double Temp)
{
  // Do we need a recomputation
  if (fabs(Temp - temp) > DBL_MIN) {
    dnorm = 0.0;
    temp  = Temp;
    double vv, nn;
    densityMap.erase(densityMap.begin(), densityMap.end());
    std::map<double, LeafElem>::iterator it = measureList.begin();
    while (it != measureList.end()) {
      nn = it->second.W;
      vv = it->second.V;
      if (vv <= 0.0) vv = it->second.G;
      if (vv >  0.0) {
	dnorm += pow(nn/vv, 1.0/temp) * vv;
	densityMap.insert(pair<double, unsigned long>(dnorm, it->second.cell));
      }
      it++;
    }
    
    // DEBUG
    if (verbose_debug) {
      ofstream out("kd_dens.debug");
      if (out) {
	double tnorm = 0.0;
	map<double, unsigned long>::iterator it;
	for (it=densityMap.begin(); it!=densityMap.end(); it++) {
	  out << setw(18) << it->first
	      << setw(12) << Npts(it->second)
	      << endl;
	  tnorm += 
	    pow(static_cast<double>(measureMap[it->second].W)/measureMap[it->second].V, 1.0/Temp) * measureMap[it->second].V / dnorm;
	}
	out << "# check norm=" << tnorm << endl;
      }
      out.close();
      
      out.open("kd_measure.debug");
      if (out) {
	for (map<indx, LeafElem>::iterator
	       it=measureMap.begin(); it!=measureMap.end(); it++) {
	  out << setw(18) << it->first;
	  const double *lo = Low(it->second.cell), *hi = High(it->second.cell);
	  for (size_t i=0; i<dims; i++)
	    out << setw(18) << lo[i] << setw(18) << hi[i];
	  out << endl;
	}
      }
      out.close();
      
      const double lsafe = 1.0e-10;

      for (unsigned dim1=0; dim1<dims; dim1++) {
	for (unsigned dim2=dim1+1; dim2<dims; dim2++) {
	  std::ostringstream sout;
	  sout << "kd_measure_" << dim1+1 << "_" << dim2+1 << ".gpl";
	  ofstream out(sout.str().c_str());
	  if (out) {
	    double dmin = DBL_MAX, dmax = -DBL_MAX;
	    for (map<indx, LeafElem>::iterator
		   it=measureMap.begin(); it!=measureMap.end(); it++) {
	      double dens = log(it->second.W/it->second.V + lsafe)/log(10.0);
	      dmin = std::min<double>(dmin, dens);
	      dmax = std::max<double>(dmax, dens);
	    }
	    out << "# Min log(dens)=" << dmin 
		<< "  Max log(dens)=" << dmax << std::endl;
	    double del = dmax - dmin;
	    for (map<indx, LeafElem>::iterator
		   it=measureMap.begin(); it!=measureMap.end(); it++) {
	      const double *lo = Low (it->second.cell);
	      const double *hi = High(it->second.cell);
	      double dens = log(it->second.W/it->second.V + lsafe)/log(10.0);
	      
	      out << "set obj rect from " << lo[dim1] << ", " << lo[dim2]
		  << " to " << hi[dim1] << ", " << hi[dim2] 
		  << " front fc palette frac " 
		  << (dens - dmin)/del*0.99 + 0.01 << std::endl;
	    }
	    out << "set term wxt enhanced" << std::endl
		<< "set title 'Log_{10} Density'" << std::endl
		<< "set xlabel 'x_" << dim1+1 << "'" << std::endl
		<< "set ylabel 'x_" << dim2+1 << "'" << std::endl
		<< "set pm3d map" << std::endl
		<< "set cbrange [0:1]" << std::endl
		<< "set colorbox" << std::endl
		<< "splot "
		<< "[" << Ledge[dim1] << ":" << Hedge[dim1] << "] "
		<< "[" << Ledge[dim2] << ":" << Hedge[dim2] << "] "
		<< "0*x*y t'' lw 0" << std::endl;
	  }
	}
      }
    }
  }
}

vector<double> KDTree::DensitySample(const double Temp)
{
  computeDensity(Temp);

  // Pick a cell according to the CDF
  double u = dnorm*(*unit)();
  std::map<double, unsigned long>::iterator it = densityMap.lower_bound(u);

  // Select a point in volume unformly
  vector<double> ret(dims);
  for (unsigned n=0; n<dims; n++) 
    ret[n] = Low(it->second)[n] + 
      (High(it->second)[n] - Low(it->second)[n]) * (*unit)();
  return ret;
}

double KDTree::Density(const vector<double>& T, const double Temp)
{
  computeDensity(Temp);
  KDTree::indx k = find(T, ncut);
  std::map<indx, LeafElem>::iterator it = measureMap.find(k);
  if (it == measureMap.end()) return 0.0;
  
  if (it->second.V <= 0.0)
    return pow(it->second.W/it->second.G, 1.0/Temp) / dnorm;
  else
    return pow(it->second.W/it->second.V, 1.0/Temp) / dnorm;
}


KDTree *KDTree::New()
{
  KDTree *p    = new KDTree();
				// BSP data members
  p->ncut           = ncut;
  p->dims           = dims;
  p->scale          = scale;
  p->sz             = sz;
  p->num_points     = num_points;
  p->lebesgueLeaves = lebesgueLeaves;
  p->LminP          = LminP;
  p->LmaxP          = LmaxP;
				// KDTree data members
  p->temp           = temp;
  p->ncutM          = ncutM;
  gen               = boost::shared_ptr<ACG>(new ACG(11, 20));
  unit              = UniformPtr(new Uniform(0.0, 1.0, gen.get()));
  norm              = NormalPtr (new Normal (0.0, 1.0, gen.get()));
  
  p->centers        = centers;
  p->ranges         = ranges;
  p->center0        = center0;
  p->range0         = range0;
  p->Ledge          = Ledge;
  p->Hedge          = Hedge;
  p->weights        = weights;
  p->beta           = beta;
  p->lower          = lower;
  p->upper          = upper;
  p->minS           = minS;
  p->maxS           = maxS;
  p->next           = next;
  p->minL           = minL;
  p->maxL           = maxL;

  p->left_child     = left_child;
  p->right_child    = right_child;
  
  p->lo_leaf        = lo_leaf;
  p->hi_leaf        = hi_leaf;
  
  p->permutation    = permutation;
  p->stally         = stally;
  
  p->trim_list      = trim_list;
  p->vol_frac       = vol_frac;
  p->vol_trim       = vol_trim;
  p->vol_computed   = vol_computed;

  p->measureList    = measureList;
  p->measureMap     = measureMap;
  p->densityMap     = densityMap;
  p->dnorm          = dnorm;

  return p;
}
