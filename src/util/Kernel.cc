#include <MetricTreeDensity.h>
#include <Kernel.h>

BIE_CLASS_EXPORT_IMPLEMENT(Kernel)
BIE_CLASS_EXPORT_IMPLEMENT(GaussKernel)
BIE_CLASS_EXPORT_IMPLEMENT(LaplaceKernel)
BIE_CLASS_EXPORT_IMPLEMENT(EpanetchnikovKernel)
BIE_CLASS_EXPORT_IMPLEMENT(SplineKernel)


double GaussKernel::minDistKer(MetricTreeDensity *p,
			       ballPtr Root, vector<double>& point)
{
  double val=0.0, ret=0;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val  = fabs( point[k] - densCenter[k] ) - p->getRadius(Root);
    val  = max<double>(0.0, val);
    ret -= (val*val)/(wid[k]*wid[k]) + 
      log(2.0*M_PI*p->minWid[Root][k]*p->minWid[Root][k]);
  }
  ret = exp(ret/2);
  return ret;
}

double GaussKernel::maxDistKer(MetricTreeDensity *p,
			       ballPtr Root, vector<double>& point)
{
  double val, ret=0;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val = fabs( point[k] - densCenter[k] ) + p->getRadius(Root);
    ret -= (val*val)/(wid[k]*wid[k]) + 
      log(2.0*M_PI*p->maxWid[Root][k]*p->maxWid[Root][k]);
  }
  ret = exp(ret/2);
  return ret;
}

void GaussKernel::Combine(MetricTreeDensity *p, 
			  double wL, double wR, 
			  ballPtr cur, ballPtr L, ballPtr R)
{
  MetricTree::indx dim = p->Dim();
  if (p->cen.find(cur) == p->cen.end()) p->cen[cur] = vector<double>(dim);
  if (p->wid.find(cur) == p->wid.end()) p->wid[cur] = vector<double>(dim);

  for(unsigned int k=0; k<dim; k++) {
    p->cen[cur][k] = wL * p->cen[L][k] + wR * p->cen[R][k];
    p->wid[cur][k] = 
      wL*(p->wid[L][k]*p->wid[L][k]+ p->cen[L][k]*p->cen[L][k]) +
      wR*(p->wid[R][k]*p->wid[R][k]+ p->cen[R][k]*p->cen[R][k]) -
      p->cen[cur][k]*p->cen[cur][k];

    p->wid[cur][k] = sqrt(p->wid[cur][k]);
  }
}


double LaplaceKernel::minDistKer(MetricTreeDensity *p,
				 ballPtr Root, vector<double>& point)
{
  double val, ret=0;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val = fabs( point[k] - densCenter[k] ) - p->getRadius(Root);
    val = (val > 0) ? val : 0;
    ret -= val/wid[k] + log(2.0*p->minWid[Root][k]);
  }
  ret = exp(ret);
  return ret;
}

double LaplaceKernel::maxDistKer(MetricTreeDensity *p,
				 ballPtr Root, vector<double>& point)
{
  double val,ret=0;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val = fabs( point[k] - densCenter[k] ) + p->getRadius(Root);
    ret -= val/wid[k] + log(2.0*p->maxWid[Root][k]);
  }
  ret = exp(ret);
  return ret;
}

void LaplaceKernel::Combine(MetricTreeDensity *p, 
			    double wL, double wR,
			    ballPtr cur, ballPtr L, ballPtr R)
{
  MetricTree::indx dim = p->Dim();
  if (p->cen.find(cur) == p->cen.end()) p->cen[cur] = vector<double>(dim);
  if (p->wid.find(cur) == p->wid.end()) p->wid[cur] = vector<double>(dim);

  for (unsigned int k=0; k < dim; k++) {
    p->cen[cur][k] = wL * p->cen[L][k] + wR * p->cen[R][k];
    p->wid[cur][k] = 
      wL*(2.0*p->wid[L][k]*p->wid[L][k] + p->cen[L][k]*p->cen[L][k]) +
      wR*(2.0*p->wid[R][k]*p->wid[R][k] + p->cen[R][k]*p->cen[R][k]) -
      p->cen[cur][k]*p->cen[cur][k];
    p->wid[cur][k] = sqrt(0.5*p->wid[cur][k]);
  }
}


double 
EpanetchnikovKernel::minDistKer(MetricTreeDensity *p,
				ballPtr Root, vector<double>& point)
{
  double val, ret=1;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val = fabs( point[k] - densCenter[k] ) - p->getRadius(Root);
    val = (val > 0) ? val : 0;
    val = (val > wid[k]) ? wid[k] : val;
    ret *= (1.0-val*val/(wid[k]*wid[k]))/(4.0*p->minWid[Root][k]/3);
  }
  return ret;
}

double 
EpanetchnikovKernel::maxDistKer(MetricTreeDensity *p,
				ballPtr Root, vector<double>& point)
{
  double val,ret=1;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val = fabs( point[k] - densCenter[k] ) + p->getRadius(Root);
    val = (val > wid[k]) ? wid[k] : val;
    ret *= (1.0-val*val/(wid[k]*wid[k]))/(4.0*p->maxWid[Root][k]/3);
  }
  return ret;
}


void EpanetchnikovKernel::Combine(MetricTreeDensity *p, 
				  double wL, double wR, 
				  ballPtr cur, ballPtr L, ballPtr R)
{
  MetricTree::indx dim = p->Dim();
  if (p->cen.find(cur) == p->cen.end()) p->cen[cur] = vector<double>(dim);
  if (p->wid.find(cur) == p->wid.end()) p->wid[cur] = vector<double>(dim);

  for (unsigned int k=0; k < dim; k++) {
    p->cen[cur][k] = wL * p->cen[L][k] + wR * p->cen[R][k];
    p->wid[cur][k] = 
      wL*(0.2*p->wid[L][k]*p->wid[L][k] + p->cen[L][k]*p->cen[L][k]) +
      wR*(0.2*p->wid[R][k]*p->wid[R][k] + p->cen[R][k]*p->cen[R][k]) -
      p->cen[cur][k]*p->cen[cur][k];
    p->wid[cur][k] = sqrt(5.0*p->wid[cur][k]);
  }
}


double SplineKernel::minDistKer(MetricTreeDensity *p,
				ballPtr Root, vector<double>& point)
{
  double val, ret=1;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val = fabs( point[k] - densCenter[k] ) + p->getRadius(Root);
    val = (val > 0) ? val*val : 0;
    val = (val > wid[k]) ? wid[k] : val;
    ret *= pow(1-val/wid[k], 2.0)/(16.0*sqrt(p->minWid[Root][k])/15);
  }
  return ret;
}

double SplineKernel::maxDistKer(MetricTreeDensity *p,
				ballPtr Root, vector<double>& point)
{
  double val,ret=1;
  
  vector<double> densCenter = p->getCenter(Root);
  vector<double> wid        = p->maxWid[Root];
  
  for (unsigned int k=0; k<point.size(); k++) {
    val = fabs( point[k] - densCenter[k] ) + p->getRadius(Root);
    val = (val*val > wid[k]) ? wid[k] : val*val;
    ret *= pow(1.0-val/wid[k], 2.0)/(16.0*sqrt(p->maxWid[Root][k])/15);
  }
  return ret;
}


void SplineKernel::Combine(MetricTreeDensity *p, 
			   double wL, double wR,
			   ballPtr cur, ballPtr L, ballPtr R)
{
MetricTree::indx dim = p->Dim();
  if (p->cen.find(cur) == p->cen.end()) p->cen[cur] = vector<double>(dim);
  if (p->wid.find(cur) == p->wid.end()) p->wid[cur] = vector<double>(dim);

  for (unsigned int k=0; k < dim; k++) {
    p->cen[cur][k] = wL * p->cen[L][k] + wR * p->cen[R][k];
    p->wid[cur][k] = 
      wL*(p->wid[L][k]*p->wid[L][k]/7.0 + p->cen[L][k]*p->cen[L][k]) +
      wR*(p->wid[R][k]*p->wid[R][k]/7.0 + p->cen[R][k]*p->cen[R][k]) -
      p->cen[cur][k]*p->cen[cur][k];
    p->wid[cur][k] = sqrt(p->wid[cur][k]);
  }
}
