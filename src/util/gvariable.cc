
#include <iostream>
using namespace std;

#include <bieTags.h>
#include <gvariable.h>
#include <ConfigFileReader.h>

namespace BIE { 

  // These variables are all initialized by entries in the .bierc file.
  ConfigFileReader * cliconfig = new ConfigFileReader("cli"); 

  bool logfile             = cliconfig->getValue("logfile", true);
  bool mstat               = cliconfig->getValue("mstat",   true);
  string initfile          = cliconfig->getValue("initfile","");
  string outfile           = cliconfig->getValue("outfile", "");
  string nametag           = cliconfig->getValue("nametag", "bie");
  string metfile           = cliconfig->getValue("metfile", "");
  string homedir           = cliconfig->getValue("homedir", "");
  string helpdir           = cliconfig->getValue("helpdir", "");
  string visualizedir      = cliconfig->getValue("visualizedir", "");
  string clidir            = cliconfig->getValue("clidir",  "");
  string cntrlfile         = cliconfig->getValue("cntrlfile", ".control");
  string histfile          = cliconfig->getValue("histfile", ".bie_history");
  bool mpi_used            = cliconfig->getValue("mpi_used", false);
  bool gui_used            = cliconfig->getValue("gui_used", false);
  bool socket_used         = cliconfig->getValue("socket_used", false);
  int savehist             = cliconfig->getValue("savehist", 0);

 
  // By default we print pointer addresses in CLI.
  bool printaddressesincli = true;

  // By default CLI is not allowed to call non CLI methods.
  bool restrictcli = true;
  
  // Assume fresh state by default
  bool restart = false;
  
  // By default, debugging is off.
  int debugflags = 0;

  // simulationInProgress is set by RunThread()
  int simulationInProgress=0, firstCommandChar=1, suspendedSimulation=0;
  ost::Semaphore *switchCLISem  = new Semaphore(0);
  ost::Semaphore *likelihoodSem = new Semaphore(0);

  // Store std/err output buffers when reassigned
  bool ttool=false;
  char ch_inbuf [80];
  char ch_logbuf[80];
  char ch_conbuf[80];
  ifstream inFile;
  ofstream logFile;
  ofstream clioutFile;
  std::streambuf *inbuf;
  std::streambuf *outbuf;
  std::streambuf *errbuf;

  ostream* saved_cout;
  ostream* saved_cerr; // aah.not sure what this should be.
  istream* saved_cin;
  ofstream consoleFile;
  ofstream slaveFile;

  // This doesn't appear to b1e initialized
  char prompt[255];
  
  // Examples for illustrative purposes.
  int exampleint;
  double exampledouble;
  char examplestring[] = "This is a test";

  // Initial value determined by mode of execution - non interactive for
  // script execution and interactive for user interactive use.
  bool interactive = true;

  // Global pointer to base random number generator
  BIEACG* BIEgen = 0;
  
  // For evaluation timing
  int likelihood_count;

  // Default sampling method in EnsembleStat
  bool sample_discrete = false;

  // The value of the current level for use by user
  int current_level;

  // For tesstool
  ofstream rferr;
  ofstream wrerr;

  // The default base temperature
  double beta0 = 1.0;

  // The default boost archive type
  string persistType = "BINARY";
}

SymTab *st;
Mutex tesstoolMutex;

CLICheckpointManager *ckm = 0;
