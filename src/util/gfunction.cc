#include <gfunction.h>
#include <ConfigFileReader.h>
#include <unistd.h>
#include <sys/resource.h>


#ifdef HAVE_IEEEFP_H
  /** Define isinf using the fpclass function.  If we don't have
      <ieeefp.h> then just assume we have isinf - the compiler will
      complain if we don't
  */  
  int isinf(double x)
  {
    fpclass_t fpc = fpclass(x);
    
    if (fpc == FP_NINF)
      return -1;
    else if (fpc == FP_PINF)
      return 1;
    else
      return 0;
  }
#endif

char * stralloc(const char * str)
{
  char * newstr = new char [strlen(str) + 1];
  strcpy(newstr, str);
  return newstr;
}

/*
 *
 *  Get the maximum and current virtual memomy size for the process
 *
 *
 */

long get_max_vm()
{
  struct rlimit rlim;
  getrlimit(RLIMIT_AS, &rlim);
  return rlim.rlim_cur;
}

// Uses proc . . . and therefore will be OS dependent
long get_cur_vm(pid_t pid)
{
  ostringstream sout;
  sout << "/proc/" << pid << "/stat";

  string word;
  ifstream in(sout.str().c_str());  // ridiculous that streams don't take <string>s
  
  char line[2048];
  in.getline(line, 2048);

  int pid1;
  char comm[1024];
  char state;
  int ppid, pgrp, session, tty, tpgid;
  unsigned flags, minflt, cminflt, majflt, cmajflt;
  unsigned utime, stime, cutime, cstime;
  int counter, priority, timeout, itrealvalue;
  unsigned starttime;
  unsigned vsize;
  unsigned int  rss;                      /** Resident Set Size **/
  unsigned int  rlim;                     /** Current limit in bytes on the rss **/
  unsigned int  startcode;                /** The address above which program text can run **/
  unsigned int	endcode;                  /** The address below which program text can run **/
  unsigned int  startstack;               /** The address of the start of the stack **/
  unsigned int  kstkesp;                  /** The current value of ESP **/
  unsigned int  kstkeip;                  /** The current value of EIP **/
  int		signal;                   /** The bitmap of pending signals **/
  int           blocked; /** 30 **/       /** The bitmap of blocked signals **/
  int           sigignore;                /** The bitmap of ignored signals **/
  int           sigcatch;                 /** The bitmap of catched signals **/
  unsigned int  wchan;  /** 33 **/        /** (too long) **/

  /** pid **/
  sscanf (line, "%u", &pid1);
  char *s = strchr (line, '(') + 1;
  char *t = strchr (line, ')');
  strncpy (comm, s, t - s);
  comm [t - s] = '\0';
  
  sscanf (t + 2, "%c %d %d %d %d %d %u %u %u %u %u %d %d %d %d %d %d %u %u %d %u %u %u %u %u %u %u %u %d %d %d %d %u",
	  &state, &ppid, &pgrp, &session, &tty, &tpgid,
	  &flags, &minflt, &cminflt, &majflt, &cmajflt,
	  &utime, &stime, &cutime, &cstime,
	  &counter, &priority, &timeout, 
	  &itrealvalue, &starttime, &vsize, &rss,
	  &rlim, &startcode, &endcode, &startstack, &kstkesp,
	  &kstkeip, &signal, &blocked, &sigignore, &sigcatch, &wchan);

#ifdef DEBUG
  cout << "pid, command, rss=" << pid << ", " << comm << ", " << rss << "\n";
#endif

  return vsize;
}

long get_cur_vm3(pid_t pid)
{
  ostringstream sout;
  sout << "/proc/" << pid << "/stat";

  string word;
  ifstream in(sout.str().c_str());
  for (int i=0; i<23; i++)
    in >> word;
  
  return atoi(word.c_str());
}

long get_cur_vm2(pid_t pid)
{
  char* line = new char [2048];
  ostringstream sout;
  sout << "/proc/" << pid << "/status";
  ifstream in(sout.str().c_str());

  string word;
  long vm=0;
  bool notdone = true;

  while (notdone && in) {
    in.getline(line, 2048);
    istringstream sin(line);
    word = string(line);

    if (!word.find("VmSize:")) {
      word.replace(word.begin(), word.begin()+word.find_first_of(":")+1, "");
      word.replace(word.begin()+word.find_first_of("kB"), word.end(), "");
      vm = atoi(word.c_str());
      notdone = false;
    }
  }
  
  return vm*1024;
}


namespace BIE
{
  /// This object provides access to the [Output] section in the config file.
  ConfigFileReader * outputconfig = new ConfigFileReader("output");
 
  /// Get the output information from a string 
  streambuf * checkTable(const char *function_name, 
                         const char *function_print_value)
  {
    outputconfig->setValue(function_name, function_print_value);
    static ofstream out(function_print_value);
    return out.rdbuf();
  }
 
  /// Get the output information from the .bierc
  streambuf * checkTable(const char *function_name) 
  {
    const char *getOutput = outputconfig->getLast(function_name);
    if (getOutput && strcmp(getOutput,"cout"))
    {
      static ofstream out(outputconfig->getLast(function_name));

      if (!out) {
        cout << "can not open output file\n";
        cout.flush();
      }
      return out.rdbuf();
    }
    else 
    {
      if (ttool)
	return consoleFile.rdbuf();
      else {
	return std::cout.rdbuf();
      }
    }
  }

}
