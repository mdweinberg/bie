#include <cmath>
#include <Quantile.h>

BIE_CLASS_EXPORT_IMPLEMENT(Quantile)

Quantile::Quantile(double xmin, double xmax, double n) : 
xmin(xmin), xmax(xmax), n(n)
{
  if (xmax>xmin && n>1) good = true;
  else good = false;
  dx = (xmax - xmin)/n;
  reset();
}

Quantile::Quantile(const Quantile& p)
{
  hist  = p.hist;
  cuml  = p.cuml;
  xmin  = p.xmin;
  xmax  = p.xmax;
  dx    = p.dx;
  n     = p.n;
  good  = p.good;
  comp  = p.comp;
}

void Quantile::add(double x)
{
  if (!good)     return;
  if (x <  xmin) return;
  if (x >= xmax) return;
  
  unsigned indx = std::floor( (x - xmin)/(xmax - xmin)*n );
  hist[indx]++;
  comp = false;
}

//! Return the quantile value
double Quantile::operator()(double y)
{
  typedef std::vector<unsigned>::iterator loc;
				// Limiting values
  if (y<=0.0) return xmin;
  if (y>=1.0) return xmax;
				// Compute the cumulative distribution
  cumulate();
				// Get the bound interators
  unsigned t = std::floor(y * cuml.back());

  loc top = std::upper_bound(cuml.begin(), cuml.end(), t);

  if ( top == cuml.begin() ) return xmin;
  if ( top == cuml.end()   ) return xmax;

  // Lower bound
  //
  loc bot = top - 1;

  // Compute index
  //
  size_t indx = top - cuml.begin();

  // Linear interpolation
  //
  double c = 1.0 / cuml.back();
  double d = c * (*top - *bot);
  double a = (c*(*top) - y) / d;
  double b = (y - c*(*bot)) / d;
  
  double ret = a*(xmin + dx*(indx-1)) + b*(xmin + dx*indx);
  
  return ret;
}
