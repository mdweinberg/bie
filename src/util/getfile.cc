#include <getfile.h>
#include <gfunction.h>
#include <cstdio>
#include <RecordOutputStream.h>
#include <RecordStream_Ascii.h>

namespace BIE {

void graphrequest_proc(int new_id)
{
  char client_request[MAX_REQUEST_LEN];
  char file_name[255];
  //char return_buf[255];
  FILE *fp;
  char line_buf[MAXBUFSIZE];
  char *request_ptr;
  const char *delim =" ";
  int  msg_size = 255;
  int flags = 0;
  const char *delim1 =",";

  ostream cout(checkTable("cli_console"));
 
  //read client command and put it into a buffer
  cout << "cli: received request follows:\n"; 
  cout.flush();

  if((recv(new_id, client_request, msg_size, flags))<0)
  {
    cout << "error in receiving client commands\n";
    cout.flush();
    exit(-1);
  }

  cout << "\n";
  cout.flush();

  // interprete the client command 
  request_ptr = strtok(client_request,delim);
  cout <<"getfile: client command is: " <<  request_ptr << "\n";
  cout.flush();
  
  // if the command is "get"
  if(strcmp(request_ptr,"get")==0)
  {
    // get the file name from the client_request buffer and store
    // it in file_name buffer
    strcpy(file_name, strtok(NULL,delim));
    file_name[strlen(file_name)-2] = '\0'; 
    cout <<"cli:file name is: " << "--" <<file_name<<"--\n";
    cout.flush();
 
    // check the file, if it exists, send it to the client, if not
    // send the message to client that the file doesn't exist
    if((fp=fopen(file_name,"r"))==NULL)
    {
      cout << "cli: file does not exist.\n";
      cout.flush();
      close (new_id);
      return;
    }

    //send the server respond message to the client
    while(fgets(line_buf,MAXBUFSIZE-1,fp) != NULL)
    {
      if(sendmsg(new_id,line_buf,strlen(line_buf))<0)
      {
        cout << "error in sending msg\n";
        cout.flush();
        exit(-1);
      }
    }

    if(fclose(fp)<0)
    {
      cout << "cli:fclose error\n";
      cout.flush();
      exit(-1);
    }
  } /* end of if "get" */
  if(strcmp(request_ptr,"select")==0)
  {
    // select RecordOutputStream and fields to display
    // use getStream()
    vector<RecordOutputStream*> roslist;
    roslist = RecordOutputStream::getInheritableStreams();
    
    RecordOutputStream *selectROS;
    RecordOutputStream *selectROS1;
    char selected[255];
    char name[81];
    vector<string>* selection;
    int rosindex;
    char * strptr ;
    // RecordOutputStream_Ascii *guiROS;
    // ofstream *ofs;

    strcpy(selected, strtok(NULL,delim));
    selected[strlen(selected)-2] = '\0'; 
    cout <<"getfile:selected OS and fields: " << "--" <<selected<<"--\n";
    cout.flush();
    //strcpy((char*)rosindex, strtok(NULL,delim1));
    rosindex = atoi(strtok(selected,delim1));
    //get selected field name to set the Filter 
    selection = new vector<string>;
    while ((strptr = strtok(NULL,delim1)) != NULL) { 
      strcpy(name, strptr);
      selection->push_back(name);
      cout <<"getfile:selected field: " <<"--"<<name<<"--\n";
      cout.flush();
    }
    cout <<"getfile:before select fields, index " << rosindex << "\n";
    cout.flush();
    // roslist index starts with 0 but the screen display start with 1
    selectROS = roslist[rosindex-1];
    //selectROS = getStream[rosindex];
    try {
      selectROS1 = selectROS->selectFields(selection);
    }
    catch (...) {
      cout <<"getfile: ROS Exception\n";
      cout.flush();
      exit(0);
    }
    // aah deleting this because it is unused (the rest of this routine
    // is commented out) and it's a pain to make it work int standard C++
    //ofs = new ofstream(new_id);

    //guiROS = new RecordOutputStream_Ascii(selectROS1, ofs, true);


    //below is the code to create a file on server and send it to client.
    //I may need it later.
    //guiROS = new RecordOutputStream_Ascii(selectROS1,"outputtest.dat",true);
    // check the file, if it exists, send it to the client, if not
    // send the message to client that the file doesn't exist
    //if((fp=fopen("delete.dat","r"))==NULL)
    //{
    //  cout << "cli: file does not exist.\n";
    //  cout.flush();
    //  close (new_id);
    //  return;
    //}
    //send the server respond message to the client
    //while(fgets(line_buf,MAXBUFSIZE-1,fp) != NULL)
    //{
      //cout << line_buf << "--\n";
      //cout.flush();
    //  if(sendmsg(new_id,line_buf,strlen(line_buf))<0)
    //  {
    //    cout << "error in sending msg\n";
    //    cout.flush();
    //    exit(-1);
     // }
   // }

   // if(fclose(fp)<0)
   // {
    //  cout << "cli:fclose error\n";
    //  cout.flush();
    //  exit(-1);
    //}

    //sendmsg(new_id,"1 2 3",6);

  }
  return;
}


// return 0, OK, return < 0 error
int sendmsg(int socket, char *msg, int size)
{
  int left_byte;
  int sent_byte;
  
  left_byte = size;
  sent_byte = 1; 
//initialize any number except 0
  while(left_byte !=0)
  {
    sent_byte = send(socket,msg,left_byte,0);
    if(sent_byte<0)
    {
      cout << "cli: error in sending data error\n";
      cout.flush();
      exit(-1);
    }
    left_byte = left_byte - sent_byte;
  }
  return left_byte;
}

}
