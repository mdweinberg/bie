#include <BIEException.h>
#include <errno.h>
#include <RecordType.h>
#include <BasicType.h>
#include <State.h>
#include <gfunction.h>
#include <mpi.h>

using namespace BIE;

string BIEException::getErrorMessage()
{
  // This combines the error name, error message, and the throw
  // locations.  errormessage << ends; removed for conversion to
  // stringstream char * buffer = errormessage.str();
  //
  string buffer = errormessage.str();

  ostringstream wholemessage;
  wholemessage << exceptionname << ": " << buffer << endl;
  wholemessage << "Thrown from " << sourcefilename << ":" << sourcelinenumber;
  return wholemessage.str();  // stringstream returns a string
}

BIEException::BIEException
(string exceptionname, string message, 
 string sourcefilename, int sourcelinenumber)
{
  errormessage.str("");
  this->sourcefilename   = sourcefilename;
  this->sourcelinenumber = sourcelinenumber; 
  this->exceptionname    = exceptionname;

  errormessage << message;
}

BIEException::BIEException(string sourcefilename, int sourcelinenumber)
{
  errormessage.str("");
  this->sourcefilename   = sourcefilename;
  this->sourcelinenumber = sourcelinenumber; 
}

BIEException::BIEException(const BIEException& e)
{
  exceptionname = e.exceptionname;
  errormessage.str(e.errormessage.str());
  this->sourcefilename   = e.sourcefilename;
  this->sourcelinenumber = e.sourcelinenumber; 
}

FileFormatException::FileFormatException
(string message, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "File Format Exception";
  errormessage << message;
}

FileCreateException::FileCreateException
(string filename, int errno_, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "File Exception";
  errormessage << "Could not create the file \"" << filename
	       << "\": " << strerror(errno_) << ".";
}

FileOpenException::FileOpenException
(string filename, int errno_, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "File Exception";
  errormessage << "Could not open the file \"" << filename
	       << "\": " << strerror(errno_) << ".";
}

EndofStreamException::EndofStreamException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "End of Stream Exception";
  errormessage << "The stream has been closed.";
}

StreamInheritanceException::StreamInheritanceException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Stream Inheritance Exception";
  errormessage << "One or more of the source streams could not be inherited.";
}

UnusableFilterException::UnusableFilterException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Unusable Filter Exception";
  errormessage << "Check the filter is not already being used, all inputs ";
  errormessage << "are connected and that output names do not clash.";
}

NotRootException::NotRootException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Not Root Stream Exception";
  errormessage << "Values can only be set in the root output stream.";
}

NoSuchConnection::NoSuchConnection
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Connection Exception";
  errormessage << "The filter connection specified does not exist.";
}

AttachedFilterException::AttachedFilterException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Attached Filter Exception";
  errormessage << "This filter has already been attached to a stream.";
}

NoSuchFieldException::NoSuchFieldException
(string fieldname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Field Exception";
  errormessage << "No field named \"" << fieldname <<	 "\" exists.";
}

NoSuchFieldException::NoSuchFieldException
(int fieldindex, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Field Exception";
  errormessage << fieldindex << " is not a valid field index.";
}

NotSetInputException::NotSetInputException
(int inputindex, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Not a set input exception";
  errormessage << "Field " << inputindex << " is not a set input.";
}

NotScalarInputException::NotScalarInputException
(int inputindex, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Not a scalar input exception";
  errormessage << "Field " << inputindex << " is not a scalar input";
}

NoSuchFilterInputException::NoSuchFilterInputException
(int inputindex, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Filter Input Exception";
  errormessage << inputindex;
  errormessage << " is not a valid input index for this filter.";
}

NoSuchFilterInputException::NoSuchFilterInputException
(string inputname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Filter Input Exception";
  errormessage << "No input named \"" << inputname << "\" exists in this filter.";
}  

NoSuchFilterOutputException::NoSuchFilterOutputException
(int outputindex, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Filter Output Exception";
  errormessage << outputindex;
  errormessage << " is not a valid output index for this filter.";
}

NoSuchFilterOutputException::NoSuchFilterOutputException
(string outputname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Such Filter Output Exception";
  errormessage << "No output field named \"" << outputname << "\" exists in this filter.";
}  

BadRangeException::BadRangeException
(int startindex, int endindex, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Bad Range Exception";
  errormessage << "The first index must be lower than the second.";
}

NameClashException::NameClashException
(string clashname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Name Clash Exception";
  errormessage << "A field with the name " << clashname << " already exists.";
}

DuplicateFieldException::DuplicateFieldException
(int fieldindex, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Duplicate Field Exception";
  errormessage << "Field " << fieldindex;
  errormessage << " was specified more than once.";
}

DuplicateFieldException::DuplicateFieldException
(string fieldname, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Duplicate Field Exception";
  errormessage << "Field \"" << fieldname;
  errormessage << "\" was specified more than once.";
}

InsertPositionException::InsertPositionException
(int insertpos, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Insertion Position Exception";
  errormessage << insertpos << " is not a valid insertion position.";
}

TypeException::TypeException
(BasicType * attempted, BasicType* actual, 
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Type Exception";

  errormessage << "Attempt to access field with type " << actual->toString();
  errormessage << " as through it were of type " << attempted->toString() << ".";
}

TypeException::TypeException
(string error, string sourcefilename, int sourcelinenumber) 
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Type Exception";
  errormessage << error;
}

TypeException::TypeException
(RecordType * inputtype, RecordType * expectedtype,
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Type Exception";
  errormessage << "Input type :\n" << inputtype->toString() << endl;
  errormessage << "Expected :\n" << expectedtype->toString();
}

TypeException::TypeException
(BasicType * unsupported, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Type Exception";
  errormessage  << "Type not recognized: " <<  unsupported->toString() << ".";
}

NoValueException::NoValueException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Value Exception";
  errormessage  << "This field does not contain a value.";
}

InvalidStreamIDException::InvalidStreamIDException
(int streamid, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Invalid Stream ID exception";
  errormessage << "The stream ID " << streamid << " does not correspond ";
  errormessage << "to an output stream.";
}

CliException::CliException
(string message, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Cli Exception";
  errormessage << message;
}

VarNotExistException::VarNotExistException
(string var_name, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "CLI Variable exception";
  errormessage << "Variable " << var_name << " does not exist or is of incompatible type";
}

MethodCallOnNonObjectException::MethodCallOnNonObjectException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Method Call On Non Pointer";
  errormessage  << "Methods can only be invoked on objects.";
}

TypeMisMatchException::TypeMisMatchException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "CLI Exception";
  errormessage << "Type mismatch in assignment";
}
  
EvalExprException::EvalExprException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "CLI Exception";
  errormessage << "Error in evaluating expression";
}

PriorException ::PriorException
(const char *error_msg, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Prior Exception";
  errormessage << error_msg; 
}

FileNotExistException ::FileNotExistException
(string error_msg,string filename, string sourcefilename, int sourcelinenumber) 
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "File Exception";
  errormessage << error_msg <<"Could not open the file " << filename;
}

DimNotSupportException ::DimNotSupportException
(string sourcefilename, int sourcelinenumber) 
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Dimension Exception";
  errormessage << "Only one dimensional arrays are supported currently";
}

TypeNotSupportException ::TypeNotSupportException
(string type, string sourcefilename, int sourcelinenumber) 
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Type Exception";
  errormessage << type <<"is not currently supported in array operations";
}

DimNotMatchException ::DimNotMatchException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Dimension Exception";
  errormessage << "Dimension Mismatch";
}

DimNotMatchException ::DimNotMatchException
(string error_msg, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Dimension Exception";
  errormessage << "Dimension Mismatch: " << error_msg;
}

DirichletValueException ::DirichletValueException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Dirichlet Exception";
  errormessage << "alpha value must be >=0.0";
}

DirichletSumException ::DirichletSumException
(double val, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Dirichlet Exception";
  errormessage << "Sum(x) must equal 1.0, was " << val;
}

DirichletMomException ::DirichletMomException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Dirichlet Exception";
  errormessage << "moments greater than 2 not implemented";
}

PriorTypeException ::PriorTypeException
(int type, string sourcefilename, int sourcelinenumber) 
  : BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Prior Type Exception";
  errormessage << "no such type: "<< type;
}

DimValueException ::DimValueException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Dimension Exception";
  errormessage << "Dimension must be: 1<dim<15";
}

TessellationOverlapException ::TessellationOverlapException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Tessellation Exception";
  errormessage << "The areas covered by two tessellations overlap";
}

KSDistanceException::KSDistanceException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "KS Distance Exception";
  errormessage << "The KS Distance statistic must be passed at least two distributions.";
}

KSDistanceException::KSDistanceException
(int distindex, int dimensions, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "KS Distance Exception";
  errormessage << "The KS Distance statistic can only be used on";
  errormessage << " one-dimensional distributions.  Distribution " << distindex;
  errormessage << " has dimension " << dimensions << ".";
}

KSDistanceException::KSDistanceException
(int distoneindex, RecordType * distonetype, 
 int disttwoindex, RecordType * disttwotype, 
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "KS Distance Exception";
  errormessage << "The KS Distance statistic can only be used on distributions";
  errormessage << " with matching record type descriptors.\n";
  errormessage << " Record type of distribution " << distoneindex << ": ";
  errormessage << distonetype->toString();
  errormessage << " Record type of distribution " << disttwoindex << ": ";
  errormessage << disttwotype->toString();
} 

DataSetFieldException::DataSetFieldException
(string field, string msg, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "DataSet Field Exception";
  errormessage << "The requested field named <";
  errormessage << field << "> " << msg;
}

StringBufferOverflowException::StringBufferOverflowException
(int bufferlen, int requiredlen, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "String Buffer Overflow Exception";
  errormessage << "Required buffer size " << requiredlen << " but only have ";
  errormessage << bufferlen << ".";
}

NetCDFException::NetCDFException 
(int ncstatus, string sourcefilename, int sourcelinenumber) 
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "NetCDF Exception";
  errormessage << nc_strerror(ncstatus);
}

NetCDFFormatException::NetCDFFormatException
(string varname, int varid, int vardim, string netcdffilename, 
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "NetCDF Format Exception";
  errormessage << netcdffilename << ": All variables in NetCDF files passed to";
  errormessage << " this reader must be one dimensional arrays with unlimited ";
  errormessage << "dimension." << endl;
  errormessage << "Variable \"" << varname << "\" (id = " << varid << ")";
  errormessage << " has " << vardim << " dimensions.";
}
 
NetCDFFormatException::NetCDFFormatException
(string varname, int varid, string netcdffilename, 
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "NetCDF Format Exception";
  errormessage << netcdffilename << ": All variables in NetCDF files passed to";
  errormessage << " this reader must be one dimensional arrays with unlimited ";
  errormessage << "dimension." << endl;
  errormessage << "Check the format of variable \"" << varname;
  errormessage << "\" (id = " << varid << ").";
}

NetCDFFormatException::NetCDFFormatException
(string varname, int varid, string netcdffilename, nc_type vartype,
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  string vartypename;
  
  switch (vartype) {
    case NC_NAT: vartypename = "NC_NAT"; break;
    case NC_CHAR: vartypename = "NC_CHAR"; break;
    case NC_SHORT: vartypename = "NC_SHORT"; break;
    case NC_FLOAT: vartypename = "NC_FLOAT"; break;
    default: vartypename = "[error, type ID not found]";
  }

  exceptionname = "NetCDF Format Exception";
  errormessage << netcdffilename << ": This reader requires that variables ";
  errormessage << "must have type either NC_BYTE, NC_INT, or NC_DOUBLE.";
  errormessage << "Variable \"" << varname << "\" (id = " << varid << ")";
  errormessage << " has type " << vartypename << ".";
}

NetCDFFormatException::NetCDFFormatException
(string fieldname, int fieldindex,
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "NetCDF Format Exception";
  errormessage << "Fields with String type cannot be stored by this writer ";
  errormessage << "in NetCDF files" << endl << " Check field \"";
  errormessage << fieldname << "\" (index " << fieldindex << ").";
}

InternalError::InternalError
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Internal Error Exception";
  errormessage << "Execution reached a state that should not reachable.";
}

InternalError::InternalError
(string msg, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Internal Error Exception";
  errormessage << "Execution reached a state that should not reachable. "
		  << msg << ".";
}

InternalError::InternalError
(int err, string msg, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Internal Error Exception";
  errormessage << "Execution reached a state that should not reachable. "
		  << msg << ".";
}

TileIDException::TileIDException
(int tileid, int mintileid, int maxtileid, 
 string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Tile ID Exception";
  errormessage << "Invalid tile id: " << tileid << endl;
  errormessage << "Min tile ID: " << mintileid << ", Max tile id: ";
  errormessage << maxtileid;
}

InvalidFrontierException::InvalidFrontierException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Invalid Frontier Exception";
}

NoSuchDistributionException::NoSuchDistributionException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Distribution type does not exist.\nValid types are BinnedDistribution, PointDistribution, and NullDistribution";
}

NoPointDistributionException::NoPointDistributionException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Point Distribution type exist for this class";
}

NoBinnedDistributionException::NoBinnedDistributionException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "No Binned Distribution type exists for this class";
}

InappropriateDistributionException::InappropriateDistributionException
(string stype, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "This Distribution type " + stype + 
    " is inappropriate for the application at this point";
}

TessToolException::TessToolException
(string msg, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "TessTool Exception";
  errormessage << msg;
}

ImpossibleStateException::ImpossibleStateException
(string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "ImpossibleState Exception";
  errormessage << "impossible parameters";
}

ImpossibleStateException::ImpossibleStateException
(string msg, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "ImpossibleState Exception";
  errormessage << "impossible parameters, " << msg;
}

MetropolisHastingsException:: MetropolisHastingsException
(string file, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = " Metropolis-Hastings Exception";
  errormessage << "error reading proposal widths from input file <"
		  << file << ">" << endl;
}

EmptyStateException:: EmptyStateException
(string sourcefilename, int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = " Empty State Exception";
  errormessage << " you can not fill a state until its dimension is defined"
	       << " by assigning it a non-empty state"
	       << endl;
}

StateCreateException:: StateCreateException
(unsigned idim, string sourcefilename, int sourcelinenumber)
  : BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = " State Create Exception";
  errormessage << " bad dimension=" << idim
		  << endl;
}

StateBoundsException:: StateBoundsException
(unsigned indx, unsigned idim, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = " State Bounds Exception";
  errormessage << " index=" << indx 
		  << " and dimension=" << idim
		  << endl;
}

StateBoundsException:: StateBoundsException
(unsigned indx, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = " State Bounds Exception";
  errormessage << " index=" << indx 
		  << endl;
}

ResumeLogException::ResumeLogException
(string message, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "ResumeLog exception";
  errormessage << message << endl;
}

BadParameterException::BadParameterException
(string classname, string message, string sourcefilename, int sourcelinenumber)
: BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Bad Parameter exception";
  errormessage << "You have supplied a bad parameter to " 
	       << classname << ": " << message << endl;
}

StateBlockingException::StateBlockingException
(string sourcefilename, int sourcelinenumber) :
  BIEException(sourcefilename, sourcelinenumber)
{
  exceptionname = "State blocking error";
  errormessage << "ERROR in " << sourcefilename << ":" << sourcelinenumber;
}

NonExistentBlock::NonExistentBlock
(int j, const State* s, string sourcefilename, int sourcelinenumber) :
  StateBlockingException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Non Existent State Block";
  errormessage << ", Block " << j << " requested but State block is "
	       << s->bIndex() << " out of a total of " 
	       << s->nBlocks() << " blocks";
}

GalphatException::GalphatException
(string sourcefilename, int sourcelinenumber) :
  BIEException(sourcefilename, sourcelinenumber)
{
  errormessage << "GALPHAT ERROR in " << sourcefilename 
	       << ":" << sourcelinenumber;
}

GalphatBadCode::GalphatBadCode
(string classname, string method, string message, 
 string sourcefilename, int sourcelinenumber) : 
  GalphatException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Unexpected state error";
  errormessage << ". Variables in an unexpected or uninitialized state "
	       << "in method " << method
	       << ", classname " << classname << ": " << message << endl;
}

GalphatBadConfig::GalphatBadConfig
(string configfile, string classname, string method, string message, 
 string sourcefilename, int sourcelinenumber) : 
  GalphatException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Configuration parsing error";
  errormessage << ". A bad parameter in configuration file <"
	       << configfile << "> detected in method "
	       << ", classname " << classname << ": " << message << endl;
}

GalphatBadParameter::GalphatBadParameter
(string classname, string method, string message, 
 string sourcefilename, int sourcelinenumber) : 
  GalphatException(sourcefilename, sourcelinenumber)
{
  exceptionname = "Bad parameter value in Galphat";
  errormessage << ". Bad parameter supplied to method "
	       << method << " in " << classname << ": " << message << endl;

};



