//----------------------------------------------------------------------
//  Bayesian evidence computation using KD-/ORB-tree/2^n-tree
//  quadrature cells
//
//  [Computes bootstrap or batch confidence limits]
//
//----------------------------------------------------------------------

using namespace std;

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cfloat>
#include <vector>
#include <deque>
#include <tuple>

//----------------------------------------------------------------------

#include <boost/program_options.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/random.hpp>

namespace po = boost::program_options;

//----------------------------------------------------------------------

#include <mpi.h>

#include <VectorM.h>
#include <linalg.h>

#include <LikelihoodComputation.h>
#include <MarginalLikelihood.h>
#include <AsciiProgressBar.h>
#include <gvariable.h>
#include <TwoNTree.h>
#include <KDTree.h>
#include <MLData.h>
#include <TNTree.h>
#include <VTA.h>


BIE_CLASS_EXPORT_IMPLEMENT(BIE::MarginalLikelihood)
using namespace BIE;

#include "config.h"
#ifdef FPEDEBUG
#include <fenv.h>
#include "fpetrap.h"
#endif

static bool MargLikeDebug = false;
static bool ResampDiagOut = true;

//----------------------------------------------------------------------

double volumeTrim(size_t& beg, size_t& end, deque<element>& prob, 
		  double edgeL, int maxiter);

//----------------------------------------------------------------------

MarginalLikelihood::MarginalLikelihood (Ensemble* ens, Simulation* sim) :
  ens_(ens), sim_(sim), 
  use_nla(false), 
  use_vta(false), 
  use_imp(true), minfrac(0.5), Nresample(100000), inner(1000)
{
				// Tree types for VTA
				// -----------------------
  KD           = false;		// <== KD-tree
  ORB          = false;		// <== ORB-tree
  TNT          = true;		// <== 2^n (hyperoct) tree

  LONGFORM     = false;		// Use short form output by default

  batch        = false;		// Use batch variance

  quiet        = false;		// Single-line output
  headr        = false;		// Print header for single-line output

  dump         = false;		// Dump the sampled marginal likelihood values

  ssfac        = 0.5;		// Default subsample fraction
  strd         = 1;		// Stride: frequency to skip (1=every state)
  nlev         = 14;		// Maximum tree level for 2^n tree
  keep         = -1;		// Number of states to retain (-1
				// means keep all)
  cut          = 32;		// Bucket size
  nboot        = 1; 		// Number of bootstrap samples
  ssamp        = -1; 		// Target number of batches
  Lkts         = 1000; 
  SCALE        = 0; 
  itmax        = 24;
  use_nla      = false; 
  use_vta      = false; 
  use_imp      = true; 
  uniq         = true; 
  logM         = false; 
  fixbox       = false;
  fullV        = false; 
  fillV        = false; 
  geomV        = false; 
  fullT        = false;
  range        = false;
  minfrac      = 0.5;
  ssfac        = 0.1; 
  thold        = 0.1; 
  loffs        = 0.1; 
  edgeL        = 1.0; 
  trimV        = 1.0; 
  ffactor      = 0.5;
  qeps1        = 0.1586552; 
  qeps2        = 0.025;
  ignr         = 100;
  resampF      = 0.0;
  prefix       = "marglike";

				// Number of states to skip (-1, skip
				// first half)
  skip = sim_->getConverge()->ConvergedIndex();
  if (skip<=0) skip = -1;
				// Share will all (only root has the
				// convergence results)
  MPI_Bcast(&skip, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

void MarginalLikelihood::setTreeType(string ttype)
{
  if (ttype.compare("kd") == 0 || ttype.compare("KD") == 0) {
    KD  = true;
    ORB = false;
    TNT = false;
  } else if (ttype.compare("orb") == 0 || ttype.compare("ORB") == 0) {
    ORB = true;
    KD  = false;
    TNT = false;
  } else if (ttype.compare("tnt") == 0 || ttype.compare("TNT") == 0) {
    TNT = true;
    ORB = false;
    KD  = false;
  }
}


void MarginalLikelihood::compute()
{

  //--------------------------------------------------
  // Begin: set variables
  //--------------------------------------------------

  BSPTree ::full_volume = fullV;
  BSPTree ::fill_volume = fillV;
  BSPTree ::geom_volume = geomV;
  BSPTree ::fill_factor = ffactor;
  BSPTree ::lower_quant = qeps1;
  BSPTree ::upper_quant = 1.0 - qeps1;
  BSPTree ::dim1        = 0;
  BSPTree ::dim2        = 1;
  KDTree  ::use_range   = range;
  TwoNTree::diag        = false;
  TwoNTree::full        = fullT;
  TwoNTree::zedge       = false;
  
  //--------------------------------------------------
  // Create the StateFile
  //--------------------------------------------------

  if (sf.get()==0) {
    // For sanity . . . this would not make sense so this protects the
    // user from something silly
    trimV = 1.0;
    if (use_imp) {
      if (Nresample)
	sf = GetStateFileSubsample();
      else
	sf = GetStateFileSubvolume();
    } else {
      sf = GetStateFileFull();
    }
  }

  //--------------------------------------------------
  // Debug
  //--------------------------------------------------
  if (MargLikeDebug) {
    std::ostringstream sfile;
    sfile << prefix << "." << myid;
    sf->dumpFile(sfile.str());
  }

  unsigned dim = sf->prob.front().point.size();

  //--------------------------------------------------
  // Bootstrap loop
  //--------------------------------------------------

  vector<unsigned> numerr(2, 0);

  VTA::useMPI = true;
  VTA::treMPI = true;
  VTA::useORB = ORB;
  VTA::useTNT = TNT;
  VTA::TNTlev = nlev;

  const double SCEXP = 1.0;
  const   bool measr = false;
  const   bool debug = false;

  VTA vt(SCALE, uniq, SCEXP), nl(SCALE, uniq, SCEXP);

  vt.setFile(nametag);
  vt.Measure(measr);
  vt.setDBG (debug);
  nl.setDBG (debug);
				// Boost random # stuff for resampling
  boost::mt19937 rng (11+myid);
  boost::uniform_int<> ns(0, sf->prob.size()-1);
  boost::variate_generator<boost::mt19937, boost::uniform_int<> > nsamp(rng, ns);
  
  unsigned fileCtr = 0;

  // If batch sampling (bootstrap variance is selected) choose the
  // number of samples based on ssamp or nboot
  //
  if (batch) {
				// Assign number batches based on ssamp
    if (ssamp>0 && sf->prob.size()/ssamp>0)
      nboot = min<int>(sf->prob.size()/ssamp, nboot);
    else			// Assign ssamp based on the number of batches
      ssamp = sf->prob.size()/nboot;
  }
				// This structure will hold the
				// intermediate results per process
  MLData ml(qeps1, qeps2, sf->temps, LONGFORM);

  ml.subSample(use_imp, inner, Nresample);
  
  // Set the sample size based on batch or single chain usage
  //
  if (ssamp<=0)
    ml.nsample = min<unsigned>(floor(ssfac*sf->prob.size()+0.5), sf->prob.size());
  else
    ml.nsample = ssamp;

				// So we can see the progress . . .
				// 
  if (myid==0 && !quiet) AsciiProgressBar(cout, 0, nboot, 2);


				// This is the main bootstrap sampling loop
				// 
  for (int nb=0; nb<nboot; nb++) {
    
    if (nb % numprocs == myid) {

      deque<element> prob;
				
      if (batch) {		// Partition
	for (unsigned i=0; i<ml.nsample; i++) 
	  prob.push_back(sf->prob[ml.nsample*nb+i]);
      } else {			// Compute sample with replacment
	for (unsigned i=0; i<ml.nsample; i++) 
	  prob.push_back(sf->prob[nsamp()]);
      }
    
      
      //--------------------------------------------------
      // Compute the harmonic mean approximation
      //--------------------------------------------------
      
      double l1st=prob.back().pval[1];

      typedef pair<double, double> dpair;
      typedef map<double, dpair>   kappaMap;
      
      kappaMap::iterator kit;
      kappaMap kappa;
      
      for (unsigned t=0; t<prob.size(); t++) {
	if ( (kit=kappa.find(prob[t].beta)) == kappa.end()) {
	  kappa[prob[t].beta] = 
	    dpair(exp((1.0 - prob[t].beta)*prob[t].pval[2] + 
		      prob[t].beta*(l1st-prob[t].pval[1])), 1);
	} else {
	  kit->second.first  += exp((1.0 - kit->first)*prob[t].pval[2] + 
				    kit->first*(l1st-prob[t].pval[1]));
	  kit->second.second += 1;
	}
      }
      
      double Nt = 0.0;
      double Hm = 0.0;
      for (kit=kappa.begin(); kit!=kappa.end(); kit++) {
	double N = kit->second.second;
	Nt += N;
	Hm += N*N * exp(kit->first*l1st) / kit->second.first;
      }
      if (Hm>0.0) Hm = log(Hm/Nt);


      //--------------------------------------------------
      // Compute Laplace approximation
      //--------------------------------------------------
      
      double Lm=0.0;
      
      // Sort the list by posterior value
      sort(prob.begin(), prob.end(), compare_posterior);
      
      double fac = 1.0/prob.size();
      
      MatrixM disp(1, dim, 1, dim);
      VectorM mean(1, dim);
      
      disp.zero();
      mean.zero();
      
      deque<element>::iterator itp;
      for (itp=prob.begin(); itp!=prob.end(); itp++) {
	for (unsigned i=0; i<dim; i++) mean[i+1] += itp->point[i] * fac;
      }

      for (itp=prob.begin(); itp!=prob.end(); itp++) {
	for (unsigned i=0; i<dim; i++) {
	  for (unsigned j=0; j<dim; j++) 
	    disp[i+1][j+1] += 
	      (itp->point[i] - mean[i+1]) *
	      (itp->point[j] - mean[j+1]) * fac;
	}
      }
      
      Lm = 0.5*dim*log(2.0*M_PI) + 
	0.5*log(fabs(determinant(disp))) + prob.back().pval[0];

      //--------------------------------------------------
      // Trim the points for VTA
      //--------------------------------------------------
      
      size_t beg_count, fin_count;

      double Vfac = volumeTrim(beg_count, fin_count, prob, edgeL, itmax);
      double lVfac = log(Vfac);
      
      double Vfac_vt  = Vfac;
      double lVfac_vt = lVfac + resampF;
      
      double Vfac_nl  = Vfac;
      double lVfac_nl = lVfac;
      
      std::sort(prob.begin(), prob.end(), compare_likelihood);
      double Lrat = prob.back().pval[1] - prob.front().pval[1];

      try {
	vt.compute(cut, Lkts, logM, trimV, prob.begin(), prob.end());
	if (trimV<1.0) {
	  Vfac_vt  = 1.0/vt.getFraction();
	  lVfac_vt = log(Vfac_vt);
	}
      }
      catch (string error) {
	if (myid==0) {
	  numerr[0]++;
	  if (numerr[0]<=10)
	    cout << endl << "VTA [#" << numerr[0] << "]: " 
		 << error << endl;
	  if (numerr[0]==10)
	    cout << "Suppressing any further messages about this" << endl;
	}
      }

      
      // if (debug) 
	cout << endl
	     << "Volume trim: [ini=" << beg_count << ", fin=" << fin_count
	     << ", Vfac="    << Vfac_vt
	     << ", lVfac="   << lVfac_vt
	     << ", vol="     << vt.Yvolume[1]
	     << ", resampF=" << resampF << "]"
	     << endl << endl;
      

      //--------------------------------------------------
      // Compute mean in the subvolume
      //--------------------------------------------------
      double Nmean = 0.0;
      if (use_imp) {
	double Nnorm = 0.0;
	deque<element>::iterator itp;
	double first = prob.back().pval[0];
	for (itp=prob.begin(); itp!=prob.end(); itp++) {
	  Nmean += exp(itp->pval[0] - first);
	  Nnorm += 1.0;
	}
	Nmean = log(Nmean/Nnorm) + first + log(vt.Yvolume[1]) + resampF;
      }
      
      double Ht, Lt;
      vector<double> L, X;
      
      if (use_nla) {

	//--------------------------------------------------
	// Compute unnormalized posterior value
	//--------------------------------------------------
	
	// Sort the list by likelihood value
	//
	sort(prob.begin(), prob.end(), compare_likelihood);
	
	//--------------------------------------------------
	// Look for threshold value
	//--------------------------------------------------
	
	deque<element>::reverse_iterator rit0, rit1;
	unsigned ncnt=0;
	
	rit0 = prob.rbegin();
	++(rit1 = rit0);
	double lmax = rit0->pval[1];
	for (; rit1!=prob.rend(); rit1++, rit0++) {
	  if (ncnt>ignr) {
	    if (rit0->pval[1] - rit1->pval[1] > thold) break;
	    if (lmax - rit1->pval[1] > loffs) break;
	  }
	  ncnt++;
	}
      
	if (ncnt<1) {			// Need at least one interval . . .
	  cerr << "Fewer than 2 points for threshold=" << thold 
	       << " and L offset=" << loffs << endl;
	}
	
	//--------------------------------------------------
	// Compute prior measure
	//--------------------------------------------------
	
	deque<element>::iterator it=rit0.base();
      
	//--------------------------------------------------
	// Compute the integrated prior for trimmed dist
	//--------------------------------------------------
	try {
	  nl.compute(cut, Lkts, logM, trimV, it, prob.end());
	  if (trimV<1.0) {
	    Vfac_nl  = 1.0/nl.getFraction();
	    lVfac_nl = log(Vfac_nl);
	  }
	}
	catch (string error) {
	  if (myid==0) {
	    numerr[1]++;
	    if (numerr[1]<=10)
	      cout << endl << "Trimmed NLA [#" << numerr[1] << "]: " 
		   << error << endl;
	    if (numerr[1]==10)
	      cout << "Suppressing any further messages about this" << endl;
	  }
	}
	

	//--------------------------------------------------
	// Normalization for NLA
	//--------------------------------------------------
      
	unsigned x = 1;
      
	for (; it!=prob.end(); it++) {
	  X.push_back(x++);
	  L.push_back(it->pval[1]);
	}
	
	double norm = X.back();
	for (unsigned t=0; t<X.size(); t++) X[t] = X[t]/norm;
	
	//--------------------------------------------------
	// Compute Laplace approximation for trimmed dist
	//--------------------------------------------------
	
	// Trim the list to values below threshold
	prob.erase(prob.begin(), rit0.base());

	fac = 1.0/prob.size();


	MatrixM disp(1, dim, 1, dim);
	VectorM mean(1, dim);
	
	disp.zero();
	mean.zero();
	
	deque<element>::iterator itp;
	for (itp=prob.begin(); itp!=prob.end(); itp++) {
	  for (unsigned i=0; i<dim; i++) mean[i+1] += itp->point[i] * fac;
	}
	
	for (itp=prob.begin(); itp!=prob.end(); itp++) {
	  for (unsigned i=0; i<dim; i++) {
	    for (unsigned j=0; j<dim; j++) 
	      disp[i+1][j+1] += 
		(itp->point[i] - mean[i+1]) *
		(itp->point[j] - mean[j+1]) * fac;
	  }
	}
	
	Lt = 0.5*dim*log(2.0*M_PI) + 
	  0.5*log(fabs(determinant(disp))) + prob.back().pval[0];
	
	//--------------------------------------------------
	// Compute harmonic mean for trimmed sample
	//--------------------------------------------------
	
	kappa.erase(kappa.begin(), kappa.end());
	lmax = prob.back().pval[1];
	
	for (unsigned t=0; t<prob.size(); t++) {
	  if ( (kit=kappa.find(prob[t].beta)) == kappa.end()) {
	    kappa[prob[t].beta] = 
	      dpair(exp((1.0 - prob[t].beta)*prob[t].pval[2] + 
			prob[t].beta*(lmax-prob[t].pval[1])), 1);
	  } else {
	    kit->second.first  += exp((1.0 - prob[t].beta)*prob[t].pval[2] + 
				      prob[t].beta*(lmax-prob[t].pval[1]));
	    kit->second.second += 1;
	  }
	}
	
	Nt = 0.0;
	Ht = 0.0;
	for (kit=kappa.begin(); kit!=kappa.end(); kit++) {
	  double N = kit->second.second;
	  Nt += N;
	  Ht += N*N * exp(kit->first*l1st) / kit->second.first;
	}
	Ht = log(Nt/Nt);
      }
      
      //--------------------------------------------------
      // Results
      //--------------------------------------------------
      
      if (use_nla) {
	
	ofstream out;
	if (measr && nametag.size()) {
	  ostringstream file;
	  file << nametag << ".NLA.";
	  if (numprocs>1) file << myid << ".";
	  file << fileCtr;

	  out.open(file.str().c_str());
	  out << "# " << right << setw(14) << "Y "
	      << "| " << setw(14) << "Measr [mean] "
	      << "| " << setw(14) << "Measr [lower] "
	      << "| " << setw(14) << "Measr [upper] "
	      << "| " << setw(14) << "K(<Y) [mean] "
	      << "| " << setw(14) << "K(<Y) [lower] "
	      << "| " << setw(14) << "K(<Y) [upper] "
	      << endl << setfill('-')
	      << "# " << setw(14) << '-'
	      << "| " << setw(14) << '-'
	      << "| " << setw(14) << '-'
	      << "| " << setw(14) << '-'
	      << "| " << setw(14) << '-'
	      << "| " << setw(14) << '-'
	      << "| " << setw(14) << '-'
	      << endl << setfill(' ');
	}
	
	double Lmax = L.back();
	double Ml=1.0, Mh=1.0, Mm;
	for (unsigned t=L.size()-1; t>0; t--) {
	  Ml += exp(Lmax - L[t-1]) * (1.0 - exp(L[t-1] - L[t])) * X[t  ];
	  Mh += exp(Lmax - L[t-1]) * (1.0 - exp(L[t-1] - L[t])) * X[t-1];
	  if (out)  out << setw(16) << Lmax - 0.5*(L[t-1] + L[t])
			<< setw(16) << 0.5*(X[t-1] + X[t])
			<< setw(16) << X[t-1]
			<< setw(16) << X[t]
			<< setw(16) << 0.5*(Ml + Mh)
			<< setw(16) << Ml
			<< setw(16) << Mh
			<< endl;
	}
	
	Mm = Lmax - log(0.5*(Ml + Mh));
	Ml = Lmax - log(Ml);
	Mh = Lmax - log(Mh);
	
	// Store the results
	for (unsigned l=0; l<2; l++) {
	  ml.AlNL1[l].push_back(Mm + nl.reslt[l][2] + lVfac_nl);
	  ml.AlNL2[l].push_back(Mm + nl.Rmean[l] + lVfac_nl   );
	  ml.Prio1[l].push_back(nl.reslt[l][2]);
	  ml.Prio2[l].push_back(nl.Rmean[l]);
	}
	
	ml.Htrim.push_back(Ht + lVfac_nl);
	ml.Ltrim.push_back(Lt + lVfac_nl);
	
	ml.Used. push_back(L.size());
      }
      
      for (unsigned l=0; l<2; l++) {
	ml.AlVT1[l].push_back(vt.reslt[l][0] + lVfac_vt);
	ml.Pr1Lo[l].push_back(vt.lower[l][0] + lVfac_vt);
	ml.Pr1Hi[l].push_back(vt.upper[l][0] + lVfac_vt);
	ml.Pmean[l].push_back(vt.vmean[l][0] + lVfac_vt);
	ml.Volm1[l].push_back(vt.reslt[l][3]);
	
	ml.AlVT2[l].push_back(vt.Lmean  [l] + lVfac_vt);
	ml.Pr2Lo[l].push_back(vt.Llower [l] + lVfac_vt);
	ml.Pr2Hi[l].push_back(vt.Lupper [l] + lVfac_vt);
	ml.Volm2[l].push_back(vt.Yvolume[l]);
      }
      
      ml.VolEP.push_back(vt.Xvolume);
      ml.Vrat .push_back(Vfac);
      ml.Lrat .push_back(Lrat);
      ml.Nmean.push_back(Nmean);
      ml.Hmean.push_back(Hm + lVfac_vt);
      ml.Lplce.push_back(Lm + lVfac_vt);
      
      ml.Size. push_back(prob.size());
      
      if (myid==0 && !quiet) {
	AsciiProgressBar(cout, nb, nboot, 2);
	std::cout << std::endl;
      }
    }
  }

    
  if (myid==0 && !quiet) {
    AsciiProgressBar(cout, nboot, nboot, 2);
    cout << endl;
    cout << "Gathering data . . . " << flush;
  }

  ml.share_data();

  if (myid==0) {

    if (!quiet) cout << "done" << endl;

    //
    // Dump sample and stats
    //
    if (dump) ml.dump_sample(nametag);

    ml.print_stats(keep, cut, thold, loffs, skip, outfile, comment);
  }

  if (TNT) TwoNTree::diagDump(MPI_COMM_WORLD, nlev);
}


double MarginalLikelihood::volumeTrim
(size_t& beg, size_t& end, deque<element>& prob, double edgeL, int maxiter)
{
				// No trimming for edgeL=1 (full
				// volume)
  if (edgeL>=1.0) return 1.0;

  beg = prob.size();
  size_t dim = prob.front().point.size(), i;

				// Compute the enclosing hypercube
  vector<double> dmin(dim,  DBL_MAX);
  vector<double> dmax(dim, -DBL_MAX);

  for (deque<element>::iterator it=prob.begin(); it!=prob.end(); it++) {
    for (i=0; i<dim; i++) {
      dmin[i] = min<double>(dmin[i], it->point[i]);
      dmax[i] = max<double>(dmax[i], it->point[i]);
    }
  }

  deque<element> newp;

  if (maxiter) {
    
    vector<double> dwid;
    for (i=0; i<dim; i++) dwid.push_back(dmax[i] - dmin[i]);
    
    double dfracL = 0.0, fracL = 0.0;
    double dfracH = 0.5, fracH = 1.0;
    double dfrac;

    for (int n=0; n<maxiter; n++) {

      dfrac = 0.5*(dfracL + dfracH);
      unsigned cnt = 0;

      for (deque<element>::iterator it=prob.begin(); it!=prob.end(); it++) {
	for (i=0; i<dim; i++) {
	  if (it->point[i] < dmin[i]+dfrac*dwid[i] || 
	      it->point[i] > dmax[i]-dfrac*dwid[i]) break;
	}
	if (i==dim) cnt++;
      }

      double frac = static_cast<double>(cnt)/beg;
      if (frac > edgeL) {
	dfracL = dfrac;
	fracL  = frac;
      } else {
	dfracH = dfrac;
	fracH  = frac;
      }

      if (fabs(fracH - fracL) < 1.5/beg) break;

    }
    
				// Make a new list of inner volume
				// subsample
    dfrac = 0.5*(dfracL + dfracH);
    for (deque<element>::iterator it=prob.begin(); it!=prob.end(); it++) {
      for (i=0; i<dim; i++) {
	if (it->point[i] < dmin[i]+dfrac*dwid[i] || 
	    it->point[i] > dmax[i]-dfrac*dwid[i]) break;
      }
      if (i==dim) newp.push_back(*it);
    }
    
  } else {
				// Find the new cube side lengths by
				// scaling the original values to
				// obtain the desired volume fraction
    double dfrac = 1.0 - pow(edgeL, 1.0/dim);
    for (i=0; i<dim; i++) {
      double delt = 0.5*dfrac*(dmax[i] - dmin[i]);
      dmin[i] += delt;
      dmax[i] -= delt;
    }
				// Make a new list of inner volume
				// subsample
    deque<element> newp;
    for (deque<element>::iterator it=prob.begin(); it!=prob.end(); it++) {
      for (i=0; i<dim; i++) {
	if (it->point[i] < dmin[i] || 
	    it->point[i] > dmax[i]) break;
      }
      if (i==dim) newp.push_back(*it);
    }
  }

  beg = prob.size();
  end = newp.size();
				// Returned the trimmed data in the
				// original deque
  prob = newp;

				// The normalization correction
  return static_cast<double>(beg)/end;
}


boost::shared_ptr<StateFile> MarginalLikelihood::GetStateFileFull()
{

  //--------------------------------------------------
  // Create the StateFile
  //--------------------------------------------------
  
  const bool ignore = false;
  const bool quiet  = false;

  // Sanity check
  //
  int Navail = ens_->Nstates() - ens_->Npopped();
  if (skip >= Navail) {
    if (myid==0) {
      std::cout << "MarginalLikelihood: {skip=" << skip << "} >= {Navail="
		<< Navail << "}.  Setting skip=" << Navail/2 << std::endl;
    }
    skip = Navail/2;
  }

  boost::shared_ptr<StateFile> sf =
    boost::shared_ptr<StateFile>
    (new StateFile(ens_, skip, strd, keep, ignore, quiet));

  //--------------------------------------------------
  // Debug
  //--------------------------------------------------
  if (MargLikeDebug) {
    std::ostringstream sfile;
    sfile << prefix << ".orig." << myid;
    sf->dumpFile(sfile.str());
  }

  //--------------------------------------------------
  // Compute enclosing data range
  //--------------------------------------------------

  unsigned dim = sf->prob.front().point.size();

  if (fixbox) {

    VTA::lowerBound = std::vector<double>(dim,  DBL_MAX);
    VTA::upperBound = std::vector<double>(dim, -DBL_MAX);
    
    std::deque<element>::iterator it=sf->prob.begin();
    while (it!=sf->prob.end()) {
      for (unsigned i=0; i<dim; i++) {
	VTA::lowerBound[i] = 
	  std::min<double>(VTA::lowerBound[i], it->point[i]);
	VTA::upperBound[i] = 
	  std::max<double>(VTA::upperBound[i], it->point[i]);
      }
      it++;
    }
  }

  //--------------------------------------------------
  // Sanity check
  //--------------------------------------------------

  if (sf->prob.size()<3) {
    if (myid==0) {
      cout << "There are two or fewer points, exiting . . ." << endl;
    }
    return sf;
  }

  //--------------------------------------------------
  // Dump first half
  //--------------------------------------------------

  if (skip<0) {
    long cp = sf->prob.size()/2 - sf->popped/2;
    if (cp>0) sf->prob.erase(sf->prob.begin(), sf->prob.begin() + cp);
  }

  return sf;
}


boost::shared_ptr<StateFile> MarginalLikelihood::GetStateFileSubsample()
{
  //--------------------------------------------------
  // Create the StateFile
  //--------------------------------------------------

  const bool ignore = false;
  const bool quiet  = false;

  // Sanity check
  //
  int Navail = ens_->Nstates() - ens_->Npopped();
  if (skip >= Navail) {
    if (myid==0) {
      std::cout << "MarginalLikelihood: {skip=" << skip << "} >= {Navail="
		<< Navail << "}.  Setting skip=" << Navail/2 << std::endl;
    }
    skip = Navail/2;
  }

  boost::shared_ptr<StateFile> sf =
    boost::shared_ptr<StateFile>
    (new StateFile(ens_, skip, strd, keep, ignore, quiet));

  //--------------------------------------------------
  // Debug
  //--------------------------------------------------
  if (MargLikeDebug) {
    std::ostringstream sfile;
    sfile << prefix << ".orig." << myid;
    sf->dumpFile(sfile.str());
  }


  //--------------------------------------------------
  // Compute enclosing data range
  //--------------------------------------------------

  unsigned dim = sf->prob.front().point.size();

  std::vector<double> lowerBound(dim,  DBL_MAX);
  std::vector<double> upperBound(dim, -DBL_MAX);
  std::vector<double> scale     (dim);

  for (auto v : sf->prob) {
    for (unsigned i=0; i<dim; i++) {
      lowerBound[i] = std::min<double>(lowerBound[i], v.point[i]);
      upperBound[i] = std::max<double>(upperBound[i], v.point[i]);
    }
  }

  double initvol = 1.0;
  for (unsigned i=0; i<dim; i++) {
    scale[i] = upperBound[i] - lowerBound[i];
    initvol *= scale[i];
  }

  //--------------------------------------------------
  // Dump first half
  //--------------------------------------------------

  if (skip<0) {
    long cp = sf->prob.size()/2 - sf->popped/2;
    if (cp>0) sf->prob.erase(sf->prob.begin(), sf->prob.begin() + cp);
  }

  //--------------------------------------------------
  // Sort the list by posterior value
  //--------------------------------------------------

  std::sort(sf->prob.begin(), sf->prob.end(), compare_posterior);

  //--------------------------------------------------
  // Compute the L1 distance
  //--------------------------------------------------

  std::vector<double> center = sf->prob.back().point;
  typedef std::pair<double, size_t> iKey;
  std::vector<iKey> expfct0, expfct1;

  size_t i = 0;
  for (auto v : sf->prob) {
    double L1 = 0.0;
    for (unsigned j=0; j<dim; j++) {
      L1 = std::max<double>(L1, fabs(v.point[j] - center[j])/scale[j]);
    }
    expfct0.push_back(iKey(L1, i++));
  }
  
  //--------------------------------------------------
  // Find the position
  //--------------------------------------------------

  std::sort(expfct0.begin(), expfct0.end());

  // Strip leading zero entries (probably due to stuck chain)
  std::vector<iKey>::iterator vK = expfct0.begin();
  while (vK != expfct0.end()) {
    if (vK->first==0.0) vK++;
    else break;
  }
  if (vK != expfct0.end()) expfct0.erase(expfct0.begin(), vK);
  
  
  size_t maxsize = expfct0.size();
  unsigned partn = 
    std::min<unsigned>(inner,
		       static_cast<unsigned>(floor(minfrac*maxsize+0.5)));
  
  //--------------------------------------------------
  // Compute the variance
  //--------------------------------------------------
  
  std::vector<double> var(dim, 0.0);
  for (unsigned i=0; i<partn; i++) {
    size_t k = expfct0[i].second;
    for (unsigned j=0; j<dim; j++) {
      double tmp = sf->prob[k].point[j] - center[j];
      var[j] += tmp*tmp;
    }
  }
	
  for (unsigned j=0; j<dim; j++) scale[j] = sqrt(var[j]/partn);
  
  //--------------------------------------------------
  // Recompute the L1 distance
  //--------------------------------------------------

  // Adjust the norm to enforce prior limits
  //
  std::vector<double> plo = sim_->GetPrior()->lower();
  std::vector<double> phi = sim_->GetPrior()->upper();

  std::vector<double> T1;
  for (unsigned j=0; j<dim; j++)
    T1.push_back(std::min<double>(fabs(center[j] - plo[j])/scale[j],
				  fabs(phi[j] - center[j])/scale[j]));
  i = 0;
  for (auto v : sf->prob) {
    double L1 = 0.0;
    for (unsigned j=0; j<dim; j++) {
      double S1 = fabs(v.point[j] - center[j])/scale[j];
      S1 = std::min<double>(S1, T1[j]);
      L1 = std::max<double>(L1, S1);
    }
    expfct1.push_back(iKey(L1, i++));
  }
  
  //--------------------------------------------------
  // Resort and find the position in the list
  //--------------------------------------------------

  std::sort(expfct1.begin(), expfct1.end());

  maxsize = expfct1.size();
  partn = std::min<unsigned>
    (inner, static_cast<unsigned>(floor(minfrac*maxsize+0.5)));
  
  resampF = log(static_cast<double>(maxsize)/(partn+1));
  
  std::vector<double> lowerBound1(dim,  DBL_MAX);
  std::vector<double> upperBound1(dim, -DBL_MAX);

  // Compute the bounds from the new sorted vector of parameters
  //
  for (unsigned i=0; i<partn; i++) {
    size_t k  = expfct1[i].second;
    for (unsigned j=0; j<dim; j++) {
      double val = sf->prob[k].point[j];
      lowerBound1[j] = std::min<double>(lowerBound1[j], val);
      upperBound1[j] = std::max<double>(upperBound1[j], val);
    }
  }
    
  double finlvol = 1.0;
  for (unsigned i=0; i<dim; i++) {
    finlvol *= upperBound1[i] - lowerBound1[i];
  }

  //--------------------------------------------------
  // Diagnostic output
  //--------------------------------------------------

  if (myid==0 and ResampDiagOut) {

    size_t recount = 0;
    for (auto p : sf->prob) {
      bool okay = true;
      for (unsigned j=0; j<dim; j++) {
	double x = p.point[j];
	if (x < lowerBound1[j] or x > upperBound1[j]) okay = false;
      }
      if (okay) recount++;
    }
    
    double finlvol = 1.0;
    for (unsigned i=0; i<dim; i++) {
      finlvol *= upperBound1[i] - lowerBound1[i];
    }

    //--------------------------------------------------
    // Print the box
    //--------------------------------------------------

    std::cout << right
	      << std::setw(5)  << "Dim"
	      << std::setw(18) << "Low"
	      << std::setw(18) << "Center"
	      << std::setw(18) << "High"
	      << std::setw(18) << "Scale"
	      << std::endl
	      << std::setw(5)  << "----"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::endl;
    
    
    for (unsigned j=0; j<dim; j++)
      std::cout << std::setw(5) << j+1
		<< std::setw(18) << lowerBound1[j]
		<< std::setw(18) << center[j]
		<< std::setw(18) << upperBound1[j]
		<< std::setw(18) << scale[j]
		<< std::endl;
    
    std::vector<double> like;
    for (auto v : expfct1) {
      size_t k = v.second;
      like.push_back(sf->prob[k].pval[1]);
    }
    std::sort(like.begin(), like.end());

    std::cout << std::endl
	      << std::string(5+18*4, '-')          << std::endl
	      << "Prob size : " << sf->prob.size() << std::endl
	      << "Exp1 size : " << expfct1.size()  << std::endl
	      << "Part size : " << partn           << std::endl
	      << "Recount   : " << recount         << std::endl
	      << "Initl vol : " << initvol         << std::endl
	      << "Final vol : " << finlvol         << std::endl
	      << "Min like  : " << like.front()    << std::endl
	      << "Max like  : " << like.back()     << std::endl
	      << std::string(5+18*4, '-')          << std::endl;
  }
    
  resampF = log(static_cast<double>(expfct1.size())/(partn+1));
  
  boost::shared_ptr<StateFile> ret = 
    boost::shared_ptr<StateFile>(new StateFile(sim_->GetPrior()));

  State s(sim_->GetPrior()->SI());
  std::vector<double> prob(3);

  Uniform unit(0.0, 1.0, BIEgen);

  element elem;
  elem.pval  = std::vector<double>(3);
  elem.point = std::vector<double>(s.N());

  for (unsigned i=0; i<Nresample*nboot/numprocs; i++) {
    //
    // Populate the state vector, should be fine for simple states and
    // mixtures since ignore=false
    //
    for (unsigned j=0; j<s.N(); j++) 
      s[j] = lowerBound1[j] + (upperBound1[j] - lowerBound1[j])*unit();

    //
    // If it's a mixture, normalize the mixture parameters
    //
    if (ret->mixture) {
      double sum = 0.0;
      for (unsigned j=0; j<s.M(); j++) sum += s[j];
      for (unsigned j=0; j<s.M(); j++) s[j] /= sum;
    }

    sim_->GetPrior()->EnforceBoundsState(s);

    //
    // Evaluate the probabilities
    //
    elem.pval[2] = sim_->PriorValue(s);
    elem.pval[1] = sim_->Likelihood(s, 0);
    elem.pval[0] = elem.pval[1] + elem.pval[2];
    
    //
    // Install the new parameter vector
    //
    elem.point   = s;

    //
    // Register the new state
    //
    ret->prob.push_back(elem);
  }

  //
  // Share all states
  // 
  ret->shareFile();

  return ret;
}

boost::shared_ptr<StateFile> MarginalLikelihood::GetStateFileSubvolume()
{
  //--------------------------------------------------
  // Create the StateFile
  //--------------------------------------------------

  const bool ignore = false;
  const bool quiet  = false;

  // Sanity check
  //
  int Navail = ens_->Nstates() - ens_->Npopped();
  if (skip >= Navail) {
    if (myid==0) {
      std::cout << "MarginalLikelihood: {skip=" << skip << "} >= {Navail="
		<< Navail << "}.  Setting skip=" << Navail/2 << std::endl;
    }
    skip = Navail/2;
  }

  boost::shared_ptr<StateFile> sf =
    boost::shared_ptr<StateFile>
    (new StateFile(ens_, skip, strd, keep, ignore, quiet));

  //--------------------------------------------------
  // Debug
  //--------------------------------------------------
  if (MargLikeDebug) {
    std::ostringstream sfile;
    sfile << prefix << ".orig." << myid;
    sf->dumpFile(sfile.str());
  }


  //--------------------------------------------------
  // Compute enclosing data range
  //--------------------------------------------------

  unsigned dim = sf->prob.front().point.size();

  std::vector<double> lowerBound0(dim,  DBL_MAX);
  std::vector<double> upperBound0(dim, -DBL_MAX);
  std::vector<double> scale0     (dim);

  std::vector<double> lowerBound1(dim,  DBL_MAX);
  std::vector<double> upperBound1(dim, -DBL_MAX);
  std::vector<double> scale1     (dim);

  for (auto v : sf->prob) {
    for (unsigned i=0; i<dim; i++) {
      lowerBound0[i] = std::min<double>(lowerBound0[i], v.point[i]);
      upperBound0[i] = std::max<double>(upperBound0[i], v.point[i]);
    }
  }

  double initvol = 1.0;
  for (unsigned i=0; i<dim; i++) {
    scale0[i] = upperBound0[i] - lowerBound0[i];
    initvol *= scale0[i];
  }

  //--------------------------------------------------
  // Dump first half
  //--------------------------------------------------

  if (skip<0) {
    long cp = sf->prob.size()/2 - sf->popped/2;
    if (cp>0) sf->prob.erase(sf->prob.begin(), sf->prob.begin() + cp);
  }

  //--------------------------------------------------
  // Sort the list by posterior value
  //--------------------------------------------------

  std::sort(sf->prob.begin(), sf->prob.end(), compare_posterior);

  //--------------------------------------------------
  // Compute the L1 distance
  //--------------------------------------------------

  std::vector<double> center = sf->prob.back().point;
  typedef std::pair<double, size_t> iKey;
  std::vector<iKey> expfct0, expfct1;

  size_t i = 0;
  for (auto v : sf->prob) {
    double L1 = 0.0;
    for (unsigned j=0; j<dim; j++) {
      L1 = std::max<double>(L1, fabs(v.point[j] - center[j])/scale0[j]);
    }
    expfct0.push_back(iKey(L1, i++));
  }
  
  //--------------------------------------------------
  // Find the position
  //--------------------------------------------------
  
  std::sort(expfct0.begin(), expfct0.end());

  // Strip leading zero entries (probably due to stuck chain)
  std::vector<iKey>::iterator vK = expfct0.begin();
  while (vK != expfct0.end()) {
    if (vK->first==0.0) vK++;
    else break;
  }
  if (vK != expfct0.end()) expfct0.erase(expfct0.begin(), vK);
    
  unsigned partn = 
    std::min<unsigned>
    (inner, static_cast<unsigned>(floor(minfrac*expfct0.size()+0.5)));
		       

  //--------------------------------------------------
  // Compute the variance
  //--------------------------------------------------
  
  std::vector<double> var(dim, 0.0);
  for (unsigned i=0; i<partn; i++) {
    size_t k = expfct0[i].second;
    for (unsigned j=0; j<dim; j++) {
      double tmp = sf->prob[k].point[j] - center[j];
      var[j] += tmp*tmp;
    }
  }
	
  for (unsigned j=0; j<dim; j++) scale0[j] = sqrt(var[j]/partn);
  
  //--------------------------------------------------
  // Recompute the L1 distance
  //--------------------------------------------------

  // Adjust the max norm to enforce prior limits
  //
  std::vector<double> plo = sim_->GetPrior()->lower();
  std::vector<double> phi = sim_->GetPrior()->upper();

  std::vector<double> T1;
  for (unsigned j=0; j<dim; j++)
    T1.push_back(std::min<double>(fabs(center[j] - plo[j])/scale0[j],
				  fabs(phi[j] - center[j])/scale0[j]));
  i = 0;
  for (auto v : sf->prob) {
    double L1 = 0.0;
    for (unsigned j=0; j<dim; j++) {
      double S1 = fabs(v.point[j] - center[j])/scale0[j];
      S1 = std::min<double>(S1, T1[j]);
      L1 = std::max<double>(L1, S1);
    }
    expfct1.push_back(iKey(L1, i++));
  }
  
  //--------------------------------------------------
  // Resort and find the position in the list
  //--------------------------------------------------

  std::sort(expfct1.begin(), expfct1.end());

  partn = std::min<unsigned>
    (inner, static_cast<unsigned>(floor(minfrac*expfct1.size()+0.5)));

  
  for (unsigned i=0; i<partn; i++) {
    int k = expfct1[i].second;
    for (unsigned j=0; j<dim; j++) {
      double v = sf->prob[k].point[j];
      lowerBound1[j] = std::min<double>(lowerBound1[j], v);
      upperBound1[j] = std::max<double>(upperBound1[j], v);
    }
  }


  double volume = 1.0;
  for (unsigned j=0; j<dim; j++) {
    scale1[j] = upperBound1[j] - lowerBound1[j];
    volume *= scale1[j];
  }

  resampF = log(static_cast<double>(expfct1.size())/(partn+1));
  
  if (myid==0) {

    //--------------------------------------------------
    // Print the box
    //--------------------------------------------------

    std::cout << right
	      << std::setw(5)  << "Dim"
	      << std::setw(18) << "Left"
	      << std::setw(18) << "Center"
	      << std::setw(18) << "Right"
	      << std::setw(18) << "Scale"
	      << std::endl
	      << std::setw(5)  << "----"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::setw(18) << "------"
	      << std::endl;
    
    
    for (unsigned j=0; j<dim; j++)
      std::cout << std::setw(5) << j+1
		<< std::setw(18) << lowerBound1[j]
		<< std::setw(18) << center[j]
		<< std::setw(18) << upperBound1[j]
		<< std::setw(18) << scale1[j]
		<< std::endl;
    
    std::cout << std::endl
	      << std::string(5+18*4, '-')        << std::endl
	      << "log(volume) = " << log(volume) << std::endl
	      << "log(resamp) = " << resampF     << std::endl
	      << std::string(5+18*4, '-')        << std::endl;
  }
    
  boost::shared_ptr<StateFile> ret = 
    boost::shared_ptr<StateFile>(new StateFile(sim_->GetPrior()));

  for (unsigned i=0; i<partn+1; i++) 
    ret->prob.push_back(sf->prob[expfct1[i].second]);

  return ret;
}

