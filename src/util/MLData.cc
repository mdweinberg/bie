#include <algorithm>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>

#include <MLData.h>

// Field width form long form
const unsigned vwid = 16;

void MLData::share_data()
{
  MPI_Status status;
  unsigned nm1, nm2;
  int numprocs, myid, lm = ntemp>1 ? 2 : 1;
  
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  
  //
  // Send all data to root asynchronously
  //
  if (myid) {
    
    std::vector<MPI_Request> vreq;
    MPI_Request r1, r2;
    
    nm1 = Htrim.size();
    nm2 = Hmean.size();
    
    MPI_Isend(&nm1, 1, MPI_UNSIGNED, 0, 10, MPI_COMM_WORLD, &r1);
    MPI_Isend(&nm2, 1, MPI_UNSIGNED, 0, 11, MPI_COMM_WORLD, &r2);
    
    vreq.push_back(r1);
    vreq.push_back(r2);
    
    if (nm1>0) {
      std::vector<MPI_Request> v(3);
      MPI_Isend(&Htrim[0], nm1, MPI_DOUBLE,   0, 12, MPI_COMM_WORLD, &v[0]);
      MPI_Isend(&Ltrim[0], nm1, MPI_DOUBLE,   0, 13, MPI_COMM_WORLD, &v[1]);
      MPI_Isend(&Used [0], nm1, MPI_UNSIGNED, 0, 14, MPI_COMM_WORLD, &v[2]);
      vreq.insert(vreq.end(), v.begin(), v.end());
    }
    if (nm2>0) {
      std::vector<MPI_Request> v(5);
      MPI_Isend(&VolEP[0], nm2, MPI_DOUBLE,   0, 15, MPI_COMM_WORLD, &v[0]);
      MPI_Isend(&Hmean[0], nm2, MPI_DOUBLE,   0, 16, MPI_COMM_WORLD, &v[1]);
      MPI_Isend(&Lplce[0], nm2, MPI_DOUBLE,   0, 17, MPI_COMM_WORLD, &v[2]);
      MPI_Isend(&Vrat [0], nm2, MPI_DOUBLE,   0, 18, MPI_COMM_WORLD, &v[2]);
      MPI_Isend(&Lrat [0], nm2, MPI_DOUBLE,   0, 19, MPI_COMM_WORLD, &v[2]);
      MPI_Isend(&Size [0], nm2, MPI_UNSIGNED, 0, 20, MPI_COMM_WORLD, &v[3]);
      MPI_Isend(&Nmean[0], nm2, MPI_DOUBLE,   0, 40, MPI_COMM_WORLD, &v[4]);
      vreq.insert(vreq.end(), v.begin(), v.end());
    }
    
    for (int l=0; l<lm; l++) {
      
      nm1 = AlNL1[l].size();
      nm2 = AlVT1[l].size();
      
      MPI_Isend(&nm1, 1, MPI_UNSIGNED, 0, 20, MPI_COMM_WORLD, &r1);
      MPI_Isend(&nm2, 1, MPI_UNSIGNED, 0, 21, MPI_COMM_WORLD, &r2);
      
      vreq.push_back(r1);
      vreq.push_back(r2);
      
      if (nm1>0) {
	std::vector<MPI_Request> v(4);
	MPI_Isend(&AlNL1[l][0], nm2, MPI_DOUBLE, 0, 22, MPI_COMM_WORLD, &v[0]);
	MPI_Isend(&AlNL2[l][0], nm2, MPI_DOUBLE, 0, 23, MPI_COMM_WORLD, &v[1]);
	MPI_Isend(&Prio1[l][0], nm2, MPI_DOUBLE, 0, 24, MPI_COMM_WORLD, &v[2]);
	MPI_Isend(&Prio2[l][0], nm2, MPI_DOUBLE, 0, 25, MPI_COMM_WORLD, &v[3]);
	vreq.insert(vreq.end(), v.begin(), v.end());
      }
      if (nm2>0) {
	std::vector<MPI_Request> v(9);
	MPI_Isend(&AlVT1[l][0], nm2, MPI_DOUBLE, 0, 26, MPI_COMM_WORLD, &v[0]);
	MPI_Isend(&AlVT2[l][0], nm2, MPI_DOUBLE, 0, 27, MPI_COMM_WORLD, &v[1]);
	MPI_Isend(&Pr1Lo[l][0], nm2, MPI_DOUBLE, 0, 28, MPI_COMM_WORLD, &v[2]);
	MPI_Isend(&Pr1Hi[l][0], nm2, MPI_DOUBLE, 0, 29, MPI_COMM_WORLD, &v[3]);
	MPI_Isend(&Pmean[l][0], nm2, MPI_DOUBLE, 0, 30, MPI_COMM_WORLD, &v[4]);
	MPI_Isend(&Pr2Lo[l][0], nm2, MPI_DOUBLE, 0, 31, MPI_COMM_WORLD, &v[5]);
	MPI_Isend(&Pr2Hi[l][0], nm2, MPI_DOUBLE, 0, 32, MPI_COMM_WORLD, &v[6]);
	MPI_Isend(&Volm1[l][0], nm2, MPI_DOUBLE, 0, 33, MPI_COMM_WORLD, &v[7]);
	MPI_Isend(&Volm2[l][0], nm2, MPI_DOUBLE, 0, 34, MPI_COMM_WORLD, &v[8]);
	vreq.insert(vreq.end(), v.begin(), v.end());
      }
    }
    
    // Wait until done . . . 
    std::vector<MPI_Status> vstat(vreq.size());
    MPI_Waitall(vreq.size(), &vreq[0], &vstat[0]);
  }
  
  //
  // Get the data from all processes
  //
  if (0==myid) {
    
    int cnt = numprocs-1;
    while (cnt) {
      
      // Get the next one that is ready!
      MPI_Recv(&nm1, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &status);

      // Receive all data from this one
      int n = status.MPI_SOURCE;
      
      MPI_Recv(&nm2, 1, MPI_UNSIGNED, n, 11, MPI_COMM_WORLD, &status);
      
      if (nm1>0) {
	
	vector<double>   recv(nm1);
	vector<unsigned> reci(nm1);
	
	MPI_Recv(&recv[0], nm1, MPI_DOUBLE, n, 12, MPI_COMM_WORLD, &status);
	Htrim.insert(Htrim.end(), recv.begin(), recv.end());
	
	MPI_Recv(&recv[0], nm1, MPI_DOUBLE, n, 13, MPI_COMM_WORLD, &status);
	Ltrim.insert(Ltrim.end(), recv.begin(), recv.end());
	
	MPI_Recv(&reci[0], nm1, MPI_UNSIGNED, n, 14, MPI_COMM_WORLD, &status);
	Used.insert(Used.end(), reci.begin(), reci.end());
      }
      
      if (nm2>0) {
	
	vector<double>   recv(nm2);
	vector<unsigned> reci(nm2);
	
	MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 15, MPI_COMM_WORLD, &status);
	VolEP.insert(VolEP.end(), recv.begin(), recv.end());
	
	MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 16, MPI_COMM_WORLD, &status);
	Hmean.insert(Hmean.end(), recv.begin(), recv.end());
	
	MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 17, MPI_COMM_WORLD, &status);
	Lplce.insert(Lplce.end(), recv.begin(), recv.end());
	
	MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 18, MPI_COMM_WORLD, &status);
	Vrat.insert(Vrat.end(), recv.begin(), recv.end());
	
	MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 19, MPI_COMM_WORLD, &status);
	Lrat.insert(Lrat.end(), recv.begin(), recv.end());
	
	MPI_Recv(&reci[0], nm2, MPI_UNSIGNED, n, 20, MPI_COMM_WORLD, &status);
	Size.insert(Size.end(), reci.begin(), reci.end());

	MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 40, MPI_COMM_WORLD, &status);
	Nmean.insert(Nmean.end(), recv.begin(), recv.end());
      }
      
      
      for (int l=0; l<lm; l++) {
	
	MPI_Recv(&nm1, 1, MPI_UNSIGNED, n, 20, MPI_COMM_WORLD, &status);
	MPI_Recv(&nm2, 1, MPI_UNSIGNED, n, 21, MPI_COMM_WORLD, &status);
	
	if (nm1>0) {
	  
	  vector<double>   recv(nm1);
	  vector<unsigned> reci(nm1);
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 22, MPI_COMM_WORLD, &status);
	  AlNL1[l].insert(AlNL1[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 23, MPI_COMM_WORLD, &status);
	  AlNL2[l].insert(AlNL2[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 24, MPI_COMM_WORLD, &status);
	  Prio1[l].insert(Prio1[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 25, MPI_COMM_WORLD, &status);
	  Prio2[l].insert(Prio2[l].end(), recv.begin(), recv.end());
	}
	
	if (nm2>0) {
	  
	  vector<double>   recv(nm2);
	  vector<unsigned> reci(nm2);
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 26, MPI_COMM_WORLD, &status);
	  AlVT1[l].insert(AlVT1[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 27, MPI_COMM_WORLD, &status);
	  AlVT2[l].insert(AlVT2[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 28, MPI_COMM_WORLD, &status);
	  Pr1Lo[l].insert(Pr1Lo[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 29, MPI_COMM_WORLD, &status);
	  Pr1Hi[l].insert(Pr1Hi[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 30, MPI_COMM_WORLD, &status);
	  Pmean[l].insert(Pmean[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 31, MPI_COMM_WORLD, &status);
	  Pr2Lo[l].insert(Pr2Lo[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 32, MPI_COMM_WORLD, &status);
	  Pr2Hi[l].insert(Pr2Hi[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 33, MPI_COMM_WORLD, &status);
	  Volm1[l].insert(Volm1[l].end(), recv.begin(), recv.end());
	  
	  MPI_Recv(&recv[0], nm2, MPI_DOUBLE, n, 34, MPI_COMM_WORLD, &status);
	  Volm2[l].insert(Volm2[l].end(), recv.begin(), recv.end());
	}
      }

      cnt--;
    }
  }
  
}


void MLData::dump_sample(const string& outputtag)
{
  //
  // Dump sample
  //
  std::string file = outputtag + ".trials";
  std::ofstream samp(file.c_str());
  
  unsigned lm = ntemp>1 ? 2 : 1;
  
  if (samp) {
    
    if (AlNL1.size()) {
      
      for (unsigned j=0; j<AlNL1[0].size(); j++) {
	
	for (unsigned l=0; l<lm; l++) {
	  
	  samp << setw(18) << AlNL1[l][j]   // 1   14
	       << setw(18) << AlNL2[l][j]   // 2   15
	       << setw(18) << AlVT1[l][j]   // 3   16
	       << setw(18) << AlVT2[l][j]   // 4   17
	       << setw(18) << Volm1[l][j]   // 5   18
	       << setw(18) << Volm2[l][j]   // 6   19
	       << setw(18) << Pr1Lo[l][j]   // 7   20
	       << setw(18) << Pr2Hi[l][j]   // 8   21
	       << setw(18) << Pmean[l][j]   // 9   22
	       << setw(18) << Pr2Lo[l][j]   // 10  23
	       << setw(18) << Pr2Hi[l][j]   // 11  24
	       << setw(18) << Prio1[l][j]   // 12  25
	       << setw(18) << Prio2[l][j];  // 13  26
	}
	
	samp<< setw(18) << VolEP[j]         // 14  27
	    << setw(18) << Nmean[j]	    // 15  28
	    << setw(18) << Hmean[j]	    // 16  29
	    << setw(18) << Htrim[j]	    // 17  30
	    << setw(18) << Lplce[j]	    // 18  31
	    << setw(18) << Ltrim[j]	    // 19  32
	    << setw(10) << Used[j]	    // 20  33
	    << setw(10) << Size[j]	    // 21  34
	    << endl;
      }
      
    } else {
      
      for (unsigned j=0; j<AlVT1[0].size(); j++) {
	
	for (unsigned l=0; l<lm; l++) {
	  
	  samp << setw(18) << AlVT1[l][j]   // 1   10
	       << setw(18) << AlVT2[l][j]   // 2   11
	       << setw(18) << Volm1[l][j]   // 3   12
	       << setw(18) << Volm2[l][j]   // 4   13
	       << setw(18) << Pr1Lo[l][j]   // 5   14
	       << setw(18) << Pr2Hi[l][j]   // 6   15
	       << setw(18) << Pr2Lo[l][j]   // 8   16
	       << setw(18) << Pr2Hi[l][j];  // 9   17
	}
	
	samp << setw(18) << Nmean[j]        // 10  18
	     << setw(18) << Hmean[j]        // 11  19
	     << setw(18) << Lplce[j]	    // 12  20
	     << setw(18) << VolEP[j]	    // 13  21
	     << setw(10) << Size[j]	    // 14  22
	     << endl;
      }
      
    }
  }
}

const char titles[] = {'M', 'P', ' '};

void MLData::L0(ostream& out, string field, int m)
{
  int n = m;
  if (ntemp<2) { m = 0; n = 2; }
  out << titles[n] << " | "
      << setw(16) << right << field << " | " << setw(vwid) << nmid[m];
  if (nmax[m]>nmid[m] || nmid[m]>nmin[m]) 
    out << "  [" << setw(vwid) << nmin[m]
	<< ", "  << setw(vwid) << nmax[m] << "]";
  out << endl;
}

void MLData::L4(ostream& out, string field, vector<unsigned>& v)
{
  const int m = 2;
  out << " " << " | "
      << setw(16) << right << field << " | " << setw(vwid) << v[nmid[m]];
  if (nmax[m]>nmid[m] || nmid[m]>nmin[m]) 
    out << "  [" << setw(vwid) << v[nmin[m]]
	<< ", "  << setw(vwid) << v[nmax[m]] << "]";
  out << endl;
}

void MLData::L1(ostream& out, string field, 
		map<int, vector<unsigned> >& v, int m)
{
  int n = m;
  if (ntemp<2) { m = 0; n = 2; }
  out << titles[n] << " | "
      << setw(16) << right << field << " | " << setw(vwid) << v[m][nmid[m]];
  if (nmax[m]>nmid[m] || nmid[m]>nmin[m]) 
    out << "  [" << setw(vwid) << v[m][nmin[m]] - v[m][nmid[m]]
	<< ", "  << setw(vwid) << v[m][nmax[m]] - v[m][nmid[m]] << "]";
  out << endl;
}

void MLData::L2(ostream& out, string field, 
		map<int, vector<double> >& v, int m)
{
  int n = m;
  if (ntemp<2) { m = 0; n = 2; }
  if (fabs(v[m][nmid[m]])>1.0e10) out << scientific;
  out << titles[n] << " | "
      << setw(16) << right << field << " | " << setw(vwid) << v[m][nmid[m]];
  if (nmax[m]>nmid[m] || nmid[m]>nmin[m]) 
    out << "  [" << setw(vwid) << v[m][nmin[m]] - v[m][nmid[m]]
	<< ", "  << setw(vwid) << v[m][nmax[m]] - v[m][nmid[m]] << "]";
  out << endl << fixed;
}

void MLData::L3(ostream& out, string field, vector<double>& v)
{
  const int m = 2;
  if (fabs(v[nmid[m]])>1.0e10) out << scientific;
  out << " "  << " | "
      << setw(16) << right << field << " | " << setw(vwid) << v[nmid[m]];
  if (nmax[m]>nmid[m] || nmid[m]>nmin[m]) 
    out << "  [" << setw(vwid) << v[nmin[m]] - v[nmid[m]]
	<< ", "  << setw(vwid) << v[nmax[m]] - v[nmid[m]] << "]";
  out << endl << fixed;
}


void MLData::print_stats(int keep, int cut, double thold, double loffs, 
			 int skip, string outfile, string comment)
{
  if (ALL) 
    print_long(keep, cut, thold, loffs, skip, outfile, comment);
  else
    print_short(keep, cut, thold, loffs, skip, outfile, comment);
}

void MLData::print_short(int keep, int cut, double thold, double loffs,
			 int skip, string outfile, string comment)
{
  bool quiet = false, headr = false;
  
  // Check to see if output file exists and is readable
  ofstream out;
  if (outfile.size()) {
    // Test for existence and readability
    ifstream test_file(outfile.c_str());

    if (test_file.good()) {
      headr = false;
      test_file.close();
      out.open(outfile.c_str(), ios::out | ios::app);
    } else {
      headr = true;
      test_file.close();
      out.open(outfile.c_str(), ios::out);
    }
    
    // Good output file?
    if (out.good()) quiet  = true;
    else std::cout << "Error opening desired output file, "
		   << "falling back to stdout" << std::endl;
  }
  
  unsigned lm = ntemp>1 ? 2 : 1;
  
  vector<unsigned> dsiz(3);
  dsiz[0] = AlVT2[0].size();
  dsiz[1] = AlVT2[1].size();
  dsiz[2] = Nmean.size();
  
  //
  // Header output
  //
  
  for (unsigned l=0; l<lm; l++) {
    std::sort(AlVT2[l].begin(), AlVT2[l].end());
    std::sort(Pr2Lo[l].begin(), Pr2Lo[l].end());
    std::sort(Pr2Hi[l].begin(), Pr2Hi[l].end());
  }
  std::sort(Nmean.begin(), Nmean.end());
  
  for (unsigned l=0; l<3; l++) {
    nmin[l] = min<unsigned>(floor(0.5 + qeps2*dsiz[l]),       dsiz[l]-1);
    nmid[l] = min<unsigned>(floor(0.5 + 0.5*dsiz[l]),         dsiz[l]-1);
    nmax[l] = min<unsigned>(floor(0.5 + (1.0-qeps2)*dsiz[l]), dsiz[l]-1);
    
    nmid[l] = min<unsigned>(nmid[l], dsiz[l]-1);
    nmin[l] = min<unsigned>(nmin[l], dsiz[l]-1);
    nmax[l] = min<unsigned>(nmax[l], dsiz[l]-1);
  }
  
  if (!quiet) {
    cout   << left << setprecision(4) << fixed;
    
    cout   << setw(14) << "Bpts"       << cut                << std::endl;
    if (SUB)
      cout << setw(14) << "Ninner"     << inner              << std::endl
	   << setw(14) << "Nresample"  << nresample          << std::endl;
    else
      cout << setw(14) << "Nsample"    << nsample            << std::endl;
	
    cout   << setw(14) << "VTA (mid)"  << AlVT2[0][nmid[0]]  << std::endl
	   << setw(14) << "VTA (lo)"   << Pr2Lo[0][nmin[0]]  << std::endl
	   << setw(14) << "VTA (hi)"   << Pr2Hi[0][nmax[0]]  << std::endl
	   << setw(14) << "VTA (sz)"   << dsiz[0]            << std::endl;
    if (nresample)
      cout << setw(14) << "MC (mid)"   << Nmean[nmid[2]]     << std::endl
	   << setw(14) << "MC (lo)"    << Nmean[nmin[2]]     << std::endl
	   << setw(14) << "MC (hi)"    << Nmean[nmax[2]]     << std::endl
	   << setw(14) << "MC (sz)"    << dsiz[2]            << std::endl;
    cout   << setw(14) << "Comment"    << comment            << std::endl
	   << std::endl;
  } else {
    
    out << left;
    
    if (headr) {
      std::vector<int> widthsSUB;

      out   << "#"
	    << setw(10) << "Bpts";
      if (SUB) {
	out << setw(10) << "Ninner"
	    << setw(10) << "Nresample";
	widthsSUB = std::vector<int> {10, 10, 10, 18, 18, 18, 10, 18, 18, 18, 10, 60};
      } else {
	out << setw(10) << "Nsample";
	widthsSUB = std::vector<int> {10, 10, 18, 18, 18, 10, 18, 18, 18, 10, 60};
      }
      out   << setw(18) << "VTA (mid)"
	    << setw(18) << "VTA (lo)"
	    << setw(18) << "VTA (hi)"
	    << setw(10) << "VTA (sz)";
      if (nresample)
	out << setw(18) << "MC (mid)"
	    << setw(18) << "MC (lo)"
	    << setw(18) << "MC (hi)"
	    << setw(10) << "MC (sz)";
      out   << setw(60) << "Comment"
	    << endl;

      out << "#";
      vector<int> widths;
      size_t imax =sizeof(widthsSUB)/sizeof(int);
      for (size_t i=0; i<imax; i++) widths.push_back(widthsSUB[i]);
      
      for (size_t i=0; i<widths.size(); i++) {
	ostringstream sout;
	sout << " [" << i+1 << "]";
	out << setw(widths[i]) << sout.str();
      }
      out << endl;
    }
    
    out << setprecision(10) << scientific;
    
    out   << setw(10) << cut;
    if (SUB)
      out << setw(10) << inner << setw(10) << nresample;
    else
      out << setw(10) << nsample;
    out   << setw(18) << AlVT2[0][nmid[0]]
	  << setw(18) << Pr2Lo[0][nmid[0]]
	  << setw(18) << Pr2Hi[0][nmid[0]]
	  << setw(10) << dsiz[0]
	  << setw(18) << Nmean[nmid[2]]
	  << setw(18) << Nmean[nmin[2]]
	  << setw(18) << Nmean[nmax[2]]
	  << setw(10) << dsiz[2]
	  << setw(60) << string("'" + comment   + "'")
	  << endl;
  }
}

void MLData::print_long(int keep, int cut, double thold, double loffs,
			int skip, string outfile, string comment)
{
  bool quiet = false, headr = false;
  
  // Check to see if output file exists and is readable
  ofstream out;
  if (outfile.size()) {
    // Test for existence and readability
    ifstream test_file(outfile.c_str());

    if (test_file.good()) {
      headr = false;
      test_file.close();
      out.open(outfile.c_str(), ios::out | ios::app);
    } else {
      headr = true;
      test_file.close();
      out.open(outfile.c_str(), ios::out);
    }
    
    // Good output file?
    if (out.good()) quiet  = true;
    else std::cout << "Error opening desired output file, "
		   << "falling back to stdout" << std::endl;
  }
  
  unsigned lm = ntemp>1 ? 2 : 1;
  
  vector<unsigned> dsiz(3);
  dsiz[0] = AlNL1[0].size();
  dsiz[1] = AlNL1[1].size();
  dsiz[2] = Hmean.size();
  
  bool NLA = true;
  if (dsiz[0] == 0) {
    NLA = false;
    dsiz[0] = AlVT1[0].size();
    dsiz[1] = AlVT1[1].size();
  }
  
  //
  // Header output
  //
  if (!quiet) {
    
    cout << endl;
    cout << setw(78) << setfill('-') << '-' << endl << setfill(' ');
    std::ostringstream pct1, pct2;
    pct1 << setprecision(2) << fixed << qeps2*100.0 << "%";
    pct2 << setprecision(2) << fixed << (1.0-qeps2)*100.0 << "%";
    
    bool multiple = false;
    for (unsigned l=0; l<lm; l++) {
      if (dsiz[l]>2) multiple = true;
    }
    
    if (multiple) {
      cout << "+ | "
	   << "        Quantity | " << setw(vwid) << "Median"
	   << "  [" << setw(vwid) << pct1.str()
	   << ", "  << setw(vwid) << pct2.str() << "]" << endl;
    } else {
      cout << "+ | "
	   << "        Quantity | " << setw(vwid) << "Value" << endl;
    }
    cout << setw(78) << setfill('-') << '-' << endl << setfill(' ');
  }
  
  
  for (unsigned l=0; l<lm; l++) {
    std::sort(AlNL1[l].begin(), AlNL1[l].end());
    std::sort(AlNL2[l].begin(), AlNL2[l].end());
    std::sort(AlVT1[l].begin(), AlVT1[l].end());
    std::sort(AlVT2[l].begin(), AlVT2[l].end());
    std::sort(Volm1[l].begin(), Volm1[l].end());
    std::sort(Volm2[l].begin(), Volm2[l].end());
    std::sort(Pr1Lo[l].begin(), Pr1Lo[l].end());
    std::sort(Pr1Hi[l].begin(), Pr1Hi[l].end());    
    std::sort(Pr2Lo[l].begin(), Pr2Lo[l].end());
    std::sort(Pr2Hi[l].begin(), Pr2Hi[l].end());
    std::sort(Pmean[l].begin(), Pmean[l].end());
    std::sort(Prio1[l].begin(), Prio1[l].end());
    std::sort(Prio2[l].begin(), Prio2[l].end());
  }
  std::sort(VolEP.begin(), VolEP.end());
  std::sort(Vrat .begin(), Vrat .end());
  std::sort(Lrat .begin(), Lrat .end());
  std::sort(Nmean.begin(), Nmean.end());
  std::sort(Hmean.begin(), Hmean.end());
  std::sort(Htrim.begin(), Htrim.end());
  std::sort(Lplce.begin(), Lplce.end());
  std::sort(Ltrim.begin(), Ltrim.end());
  std::sort(Used .begin(), Used .end());
  std::sort(Size .begin(), Size .end());
  
  for (unsigned l=0; l<3; l++) {
    nmin[l] = min<unsigned>(floor(0.5 + qeps2*dsiz[l]),       dsiz[l]-1);
    nmid[l] = min<unsigned>(floor(0.5 + 0.5*dsiz[l]),         dsiz[l]-1);
    nmax[l] = min<unsigned>(floor(0.5 + (1.0-qeps2)*dsiz[l]), dsiz[l]-1);
    
    nmid[l] = min<unsigned>(nmid[l], dsiz[l]-1);
    nmin[l] = min<unsigned>(nmin[l], dsiz[l]-1);
    nmax[l] = min<unsigned>(nmax[l], dsiz[l]-1);
  }
  
  if (!quiet) {
    cout << right << setprecision(4) << fixed;
    
    for (unsigned l=0; l<lm; l++) L0(cout, "Nmid", l);
    if (NLA) L4(cout, "Number", Used);
    L4(cout, "Prob size", Size);
    
    
    for (unsigned l=0; l<lm; l++) {
      if (NLA) {
	cout << setw(78) << setfill('-') << '-' << endl << setfill(' ');
	
	L2(cout, "Alg NL1", AlNL1, l);
	L2(cout, "Alg NL2", AlNL2, l);
      }
    }
    cout << setw(78) << setfill('-') << '-' << endl << setfill(' ');
    
    for (unsigned l=0; l<lm; l++) {
      L2(cout, "[med] Alg VT1",   AlVT1, l);
      L2(cout, "[low] Alg VT1",   Pr1Lo, l);
      L2(cout, "[high] Alg VT1",  Pr1Hi, l);
      L2(cout, "[mean] Alg VT1",  Pmean, l);
      if (NLA) L2(cout, "[prior] Alg VT1", Prio1, l);
      if (Volm1[l][nmid[l]]<1.0e-2) cout << scientific;
      L2(cout, "Volume 1",        Volm1, l);
      cout << setw(78) << setfill('-') << '-' << endl << setfill(' ') << fixed;
    }
    
    for (unsigned l=0; l<lm; l++) {
      L2(cout, "[med] Alg VT2",   AlVT2, l);
      L2(cout, "[lo] Alg VT2",    Pr2Lo, l);
      L2(cout, "[up] Alg VT2",    Pr2Hi, l);
      if (NLA) L2(cout, "[prior] Alg VT2", Prio2, l);
      if (Volm2[l][nmid[l]]<1.0e-2) cout << scientific;
      L2(cout, "Volume 2",        Volm2, l);
      cout << setw(78) << setfill('-') << '-' << endl << setfill(' ') << fixed;
    }
    
    L3(cout, "Enclosed volume", 	VolEP);
    L3(cout, "Volume ratio",	 	Vrat );
    L3(cout, "Likelihood ratio", 	Lrat );
    L3(cout, "Enclosed volume", 	VolEP);
    if (SUB and nresample>0)
             L3(cout, "Mean Z",		Nmean);
    if (NLA) L3(cout, "H mean trim",	Htrim);
    L3(cout, "Laplace approx",  	Lplce);
    if (NLA) L3(cout, "Laplace trim",	Ltrim);
    
    cout << setw(78) << setfill('-') << '-' << endl << setfill(' ')
	 << "Variant 1 --> Riemann  computation using VTA" << endl
	 << "    * Range from quantiles of probability values per cell" << endl
	 << setw(78) << setfill('-') << '-' << endl << setfill(' ')
	 << "Variant 2 --> Lebesgue computation using VTA" << endl
	 << "    * Mean and range from lower and upper sums over measure" << endl
	 << setw(78) << setfill('-') << '-' << endl << setfill(' ');
    if (lm>1) {
      cout << "M --> computation from modal temperature only" << endl
	   << "P --> computation from all temperatures pooled" << endl
	   << setw(78) << setfill('-') << '-' << endl << setfill(' ');
    }
    
  } else {
    
    out << left;
    
    if (headr) {
      out << "#" << setw(9) << "Keep"
	  << setw(10) << "Bpts"
	  << setw(18) << "Quantile (1)"
	  << setw(18) << "Quantile (2)"
	  << setw(18) << "L_threshold"
	  << setw(18) << "L_offset"
	  << setw(10) << "Skip";

      if (SUB)
	out << setw(10) << "Ninner" << setw(10) << "Nresample";
      else
	out << setw(10) << "Nsample";

      if (NLA) {
	if (lm>1) {
	  out << setw(18) << "AlgNL1(mid)|M"
	      << setw(18) << "AlgNL1(lo) |M"
	      << setw(18) << "AlgNL1(hi) |M"
	      << setw(18) << "AlgNL2(mid)|M"
	      << setw(18) << "AlgNL2(lo) |M"
	      << setw(18) << "AlgNL2(hi) |M";
	  out << setw(18) << "AlgNL1(mid)|P"
	      << setw(18) << "AlgNL1(lo) |P"
	      << setw(18) << "AlgNL1(hi) |P"
	      << setw(18) << "AlgNL2(mid)|P"
	      << setw(18) << "AlgNL2(lo) |P"
	      << setw(18) << "AlgNL2(hi) |P";
	} else {
	  out << setw(18) << "AlgNL1(mid)"
	      << setw(18) << "AlgNL1(lo) "
	      << setw(18) << "AlgNL1(hi) "
	      << setw(18) << "AlgNL2(mid)"
	      << setw(18) << "AlgNL2(lo) "
	      << setw(18) << "AlgNL2(hi) ";
	}
      }
      if (lm>1) {
	out << setw(18) << "AlgVT1(mid)|M"
	    << setw(18) << "AlgVT1(lo) |M"
	    << setw(18) << "AlgVT1(hi) |M"
	    << setw(18) << "AlgVT2(mid)|M"
	    << setw(18) << "AlgVT2(lo) |M"
	    << setw(18) << "AlgVT2(hi) |M"
	    << setw(18) << "Mean1(mid) |M"
	    << setw(18) << "Mean1(lo)  |M"
	    << setw(18) << "Mean1(hi)  |M";
	out << setw(18) << "AlgVT1(mid)|P"
	    << setw(18) << "AlgVT1(lo) |P"
	    << setw(18) << "AlgVT1(hi) |P"
	    << setw(18) << "AlgVT2(mid)|P"
	    << setw(18) << "AlgVT2(lo) |P"
	    << setw(18) << "AlgVT2(hi) |P"
	    << setw(18) << "Mean1(mid) |P"
	    << setw(18) << "Mean1(lo)  |P"
	    << setw(18) << "Mean1(hi)  |P";
      } else {
	out << setw(18) << "AlgVT1(mid)"
	    << setw(18) << "AlgVT1(lo) "
	    << setw(18) << "AlgVT1(hi) "
	    << setw(18) << "AlgVT2(mid)"
	    << setw(18) << "AlgVT2(lo) "
	    << setw(18) << "AlgVT2(hi) "
	    << setw(18) << "Mean1(mid) "
	    << setw(18) << "Mean1(lo)  "
	    << setw(18) << "Mean1(hi)  ";
      }
      if (NLA) {
	if (lm>1) {
	  out << setw(18) << "Prior1(mid)|M"
	      << setw(18) << "Prior1(lo) |M"
	      << setw(18) << "Prior1(hi) |M"
	      << setw(18) << "Prior2(mid)|M"
	      << setw(18) << "Prior2(lo) |M"
	      << setw(18) << "Prior2(hi) |M";
	  out << setw(18) << "Prior1(mid)|P"
	      << setw(18) << "Prior1(lo) |P"
	      << setw(18) << "Prior1(hi) |P"
	      << setw(18) << "Prior2(mid)|P"
	      << setw(18) << "Prior2(lo) |P"
	      << setw(18) << "Prior2(hi) |P";
	} else {
	  out << setw(18) << "Prior1(mid)"
	      << setw(18) << "Prior1(lo) "
	      << setw(18) << "Prior1(hi) "
	      << setw(18) << "Prior2(mid)"
	      << setw(18) << "Prior2(lo) "
	      << setw(18) << "Prior2(hi) ";
	}
      }
      if (SUB and nresample>0) {
	out << setw(18) << "Nmean(hi)"
	    << setw(18) << "Nmean(mid)"
	    << setw(18) << "Nmean(lo)";
      }
      out << setw(18) << "Hmean(hi)"
	  << setw(18) << "Hmean(mid)"
	  << setw(18) << "Hmean(lo)";
      if (NLA) {
	out << setw(18) << "Htrim(hi)"
	    << setw(18) << "Htrim(mid)"
	    << setw(18) << "Htrim(lo)";
      }
      out << setw(18) << "Laplace(mid)"
	  << setw(18) << "Laplace(lo)"
	  << setw(18) << "Laplace(hi)";
      if (NLA) {
	out << setw(18) << "Ltrim(mid)"
	    << setw(18) << "Ltrim(lo)"
	    << setw(18) << "Ltrim(hi)";
      }
      if (lm>1) {
	out << setw(18) << "Volume1(mid)|M"
	    << setw(18) << "Volume1(lo) |M"
	    << setw(18) << "Volume1(hi) |M"
	    << setw(18) << "Volume2(mid)|M"
	    << setw(18) << "Volume2(lo) |M"
	    << setw(18) << "Volume2(hi) |M";
	out << setw(18) << "Volume1(mid)|P"
	    << setw(18) << "Volume1(lo) |P"
	    << setw(18) << "Volume1(hi) |P"
	    << setw(18) << "Volume2(mid)|P"
	    << setw(18) << "Volume2(lo) |P"
	    << setw(18) << "Volume2(hi) |P";
      } else {
	out << setw(18) << "Volume1(mid)"
	    << setw(18) << "Volume1(lo) "
	    << setw(18) << "Volume1(hi) "
	    << setw(18) << "Volume2(mid)"
	    << setw(18) << "Volume2(lo) "
	    << setw(18) << "Volume2(hi) ";
      }
      out << setw(18) << "Enclosed vol";
      if (NLA) {
	out << setw(10) << "Used(mid)"
	    << setw(10) << "Used(lo)"
	    << setw(10) << "Used(hi)";
      }
      out << setw(10) << "Size(mid)"
	  << setw(10) << "Size(lo)"
	  << setw(10) << "Size(hi)"
	  << setw(60) << "Comment"
	  << endl;
      
      const int widthsSUB1[] = { 9, 10, 18, 18, 18, 18, 10, 10,
				 18, 18, 18, 18, 18, 18, 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 10, 10, 10, 60
      };
      
      const int widthsSUB2[] = { 9, 10, 18, 18, 18, 18, 10, 10,
				 18, 18, 18, 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18, 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 10, 10, 10, 60
      };
      
      const int widthsNLA1[] = { 9, 10, 18, 18, 18, 18, 10, 10,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 10, 10, 10,
				 10, 10, 10, 60
      };
      
      const int widthsNLA2[] = { 9, 10, 18, 18, 18, 18, 10, 10,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 18, 18, 18, 18, 18,
				 18, 10, 10, 10,
				 10, 10, 10, 60
      };
      
      out << "#";
      vector<int> widths;
      if (NLA) {
	if (lm<2) {
	  size_t imax =sizeof(widthsNLA1)/sizeof(int);
	  for (size_t i=0; i<imax; i++) widths.push_back(widthsNLA1[i]);
	} else {
	  size_t imax =sizeof(widthsNLA2)/sizeof(int);
	  for (size_t i=0; i<imax; i++) widths.push_back(widthsNLA2[i]);
	}
      } else {
	if (lm<2) {
	  size_t imax =sizeof(widthsSUB1)/sizeof(int);
	  for (size_t i=0; i<imax; i++) widths.push_back(widthsSUB1[i]);
	} else {
	  size_t imax =sizeof(widthsSUB2)/sizeof(int);
	  for (size_t i=0; i<imax; i++) widths.push_back(widthsSUB2[i]);
	}
      }
      
      for (size_t i=0; i<widths.size(); i++) {
	ostringstream sout;
	sout << " [" << i+1 << "]";
	out << setw(widths[i]) << sout.str();
      }
      out << endl;
    }
    
    out << setprecision(10) << scientific;
    
    out << setw(10) << keep
	<< setw(10) << cut
	<< setw(18) << qeps1
	<< setw(18) << qeps2
	<< setw(18) << thold
	<< setw(18) << loffs
	<< setw(10) << skip
	<< setw(10) << nsample;
    if (NLA) {
      for (unsigned l=0; l<lm; l++) {
	out << setw(18) << AlNL1[l][nmid[l]]
	    << setw(18) << AlNL1[l][nmin[l]]
	    << setw(18) << AlNL1[l][nmax[l]]
	    << setw(18) << AlNL2[l][nmid[l]]
	    << setw(18) << AlNL2[l][nmin[l]]
	    << setw(18) << AlNL2[l][nmax[l]];
      }
    }
    for (unsigned l=0; l<lm; l++) {
      out << setw(18) << AlVT1[l][nmid[l]]
	  << setw(18) << AlVT1[l][nmin[l]]
	  << setw(18) << AlVT1[l][nmax[l]]
	  << setw(18) << AlVT2[l][nmid[l]]
	  << setw(18) << AlVT2[l][nmin[l]]
	  << setw(18) << AlVT2[l][nmax[l]]
	  << setw(18) << Pmean[l][nmid[l]]
	  << setw(18) << Pmean[l][nmin[l]]
	  << setw(18) << Pmean[l][nmax[l]];
    }
    if (NLA) {
      for (unsigned l=0; l<lm; l++) {
	out << setw(18) << Prio1[l][nmid[l]]
	    << setw(18) << Prio1[l][nmin[l]]
	    << setw(18) << Prio1[l][nmax[l]]
	    << setw(18) << Prio2[l][nmid[l]]
	    << setw(18) << Prio2[l][nmin[l]]
	    << setw(18) << Prio2[l][nmax[l]];
      }
    }
    if (SUB) {
      out << setw(18) << Nmean[nmid[2]]
	  << setw(18) << Nmean[nmin[2]]
	  << setw(18) << Nmean[nmax[2]];
    }
    out << setw(18) << Hmean[nmid[2]]
	<< setw(18) << Hmean[nmin[2]]
	<< setw(18) << Hmean[nmax[2]];
    if (NLA) {
      out << setw(18) << Htrim[nmid[2]]
	  << setw(18) << Htrim[nmin[2]]
	  << setw(18) << Htrim[nmax[2]];
    }
    out << setw(18) << Lplce[nmid[2]]
	<< setw(18) << Lplce[nmin[2]]
	<< setw(18) << Lplce[nmax[2]];
    if (NLA) {
      out << setw(18) << Ltrim[nmid[2]]
	  << setw(18) << Ltrim[nmin[2]]
	  << setw(18) << Ltrim[nmax[2]];
    }
    for (unsigned l=0; l<lm; l++)
      out << setw(18) << Volm1[l][nmid[l]]
	  << setw(18) << Volm1[l][nmin[l]]
	  << setw(18) << Volm1[l][nmax[l]]
	  << setw(18) << Volm2[l][nmid[l]]
	  << setw(18) << Volm2[l][nmin[l]]
	  << setw(18) << Volm2[l][nmax[l]];
    out << setw(18) << VolEP[nmax[2]];
    if (NLA) {
      out << setw(10) << Used [nmid[2]]
	  << setw(10) << Used [nmin[2]]
	  << setw(10) << Used [nmax[2]];
    }
    out << setw(10) << Size [nmid[2]]
	<< setw(10) << Size [nmin[2]]
	<< setw(10) << Size [nmax[2]]
	<< setw(60) << string("'" + comment   + "'")
	<< endl;
  }
}
