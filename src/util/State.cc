#include <State.h>
#include <Model.h>
#include <BIEException.h>
#include <LikelihoodFunction.h>

#include <boost/serialization/export.hpp>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::StateInfo)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::State)

//
// Construct from a StateInfo with zeroed data.
//
State::State(StateInfo* s)
{
  if (s == 0) throw EmptyStateException(__FILE__, __LINE__);

  si    = s;
  op    = 15;
  Mcur  = si->M;
  Ntot  = si->Ntot;
  bIndx = -1;
  *static_cast<vector<double>*>(this) = vector<double>(Ntot, 0);
}

//
// Construct from an STL vector, with sanity checking
//
State::State(StateInfo* s, std::vector<double> v)
{
  if (s == 0) throw EmptyStateException(__FILE__, __LINE__);

  si    = s;
  op    = 15;
  Mcur  = 0;
  Ntot  = v.size();
  bIndx = -1;

  //
  // Sanity check
  //
  switch (si->ptype) {
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    if (Ntot > si->Ntot) {
      ostringstream ostr;
      ostr << "StateInfo: the vector size of " << Ntot
	   << " is larger than the StateInfo size of " << si->Ntot;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
    break;
    
  case StateInfo::Mixture:
  case StateInfo::Extended:
    {
      unsigned tM = (Ntot-si->Next)/(si->Ndim+1);
      if (tM*(si->Ndim+1) + si->Next != Ntot) {
	ostringstream ostr;
	ostr << "StateInfo: the vector size of " << Ntot
	     << " does not correspond to a valid mixture cardinality" 
	     << " with Ndim=" << si->Ndim << " and Next=" << si->Next;
	throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
      }
      Mcur = tM;
    }
    
    break;
  }

  *static_cast<vector<double>*>(this) = v;
}


//
// Construct from an STL vector
//
State::State(std::vector<double> v)
{
  ssi   = boost::shared_ptr<StateInfo>(new StateInfo(v.size()));
  si    = ssi.get();
  op    = 15;
  Mcur  = 0;
  Ntot  = v.size();
  bIndx = -1;

  *static_cast<vector<double>*>(this) = v;
}


//
// Copy constructor
//
State::State(const State& s)
{
  si    = s.si;
  ssi   = s.ssi;
  op    = s.op;
  Mcur  = s.Mcur;
  Ntot  = s.Ntot;
  bIndx = s.bIndx;

  *static_cast<vector<double>*>(this) = static_cast<vector<double> >(s);
}

//
// Assignment operator
//
State &State::operator=(const State& s)
{
  si    = s.si;
  ssi   = s.ssi;
  op    = s.op;
  Mcur  = s.Mcur;
  Ntot  = s.Ntot;
  bIndx = s.bIndx;

  *(static_cast< std::vector<double>* >(this)) = s;

  return *this;
}

//
// Instantiate from a vector range, with sanity checking
//
State::State(StateInfo* s,
	     std::vector<double>::iterator one, 
	     std::vector<double>::iterator two)
{
  if (s == 0) throw EmptyStateException(__FILE__, __LINE__);

  si    = s;
  op    = 15;
  static_cast<vector<double>*>(this)->assign(one, two);
  Mcur  = 0;
  Ntot  = this->size();
  bIndx = -1;

  //
  // Sanity check
  //
  switch (si->ptype) {
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    if (Ntot > si->Ntot) {
      ostringstream ostr;
      ostr << "StateInfo: the vector size of " << Ntot
	   << " is larger than the StateInfo size of " << si->Ntot;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
    break;
    
  case StateInfo::Mixture:
  case StateInfo::Extended:
    {
      unsigned tM = (Ntot-si->Next)/(si->Ndim+1);
      if (tM*(si->Ndim+1) + si->Next != Ntot) {
	ostringstream ostr;
	ostr << "StateInfo: the vector size of " << Ntot
	     << " does not correspond to a valid mixture cardinality" 
	     << " with Ndim=" << si->Ndim << " and Next=" << si->Next;
	throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
      }
      Mcur = tM;
    }
    
    break;
  }
}

//
// Assign the internal STL vector to new values, with some sanity checking
//
State& State::operator=(const std::vector<double>& v)
{
  if (si == 0) throw EmptyStateException(__FILE__, __LINE__);

  Ntot = v.size();
  // Check expected size
  if (si->Ntot != Ntot) {
    ostringstream ostr;
    ostr << "StateInfo: my internal vector has the size " 
	 << si->Ntot
	 << ", but you tried to replace it with size " << Ntot;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

  Mcur = si->M;
  if (si->ptype == StateInfo::Mixture || si->ptype == StateInfo::Extended) {
    Mcur = (Ntot - si->Next)/(si->Ndim + 1);
    if (Mcur*(si->Ndim+1) + si->Next != Ntot) {
      ostringstream ostr;
      ostr << "StateInfo: vector size of " << Ntot << " with Ndim="
	   << si->Ndim << ", Next=" << si->Next 
	   << " does not match the total size" << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }
  *(static_cast< std::vector<double>* >(this)) = v;
  return *this;
}


//
// An accessor for the mixture weights, if mixtures are defined.
// This will throw an exception otherwise.
//
double& State::Wght(int m)
{
  if (si->ptype != StateInfo::Mixture &&
      si->ptype != StateInfo::Extended)
    {
      throw BIEException("Inconsistent request",
			 "This state is not defined as a mixture",
			 __FILE__, __LINE__);
    }
  
  if (static_cast<unsigned>(m) >= Mcur) {
    ostringstream ostr;
    ostr << "State: this state has " << Mcur
	 << " components, but you asked for index " << m;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  return (*this)[m];
}

//
// An accessor for the mixture component vectors, if mixtures are
// defined.  This will throw an exception otherwise.
//
double& State::Phi(int m, int j)
{
  if (si->ptype != StateInfo::Mixture &&
      si->ptype != StateInfo::Extended)
    {
      throw BIEException("Inconsistent request",
			 "This state is not defined as a mixture",
			 __FILE__, __LINE__);
    }

  if (static_cast<unsigned>(m) >= Mcur) {
    ostringstream ostr;
    ostr << "State: this state has " << Mcur
	 << " components, but you asked for index " << m;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  if (static_cast<unsigned>(j) >= si->Ndim) {
    ostringstream ostr;
    ostr << "State: this state has dimension " << si->Ndim
	 << ", but you asked for index " << j;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  return (*this)[Mcur + si->Ndim*m + j];
}

//
// An accessor for the underlying vector state (const version)
//
double State::vec(int j) const
{
  if (static_cast<unsigned>(j) >= si->Ntot) {
    ostringstream ostr;
    ostr << "State: this state has with dimension " << si->Ntot
	 << ", but you asked for index " << j;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  const std::vector<double> &z(*this);

  return z[j];
}

//
// An accessor for the underlying vector state
//
double& State::vec(int j)
{
  if (static_cast<unsigned>(j) >= si->Ntot) {
    ostringstream ostr;
    ostr << "State: this state has with dimension " << si->Ntot
	 << ", but you asked for index " << j;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  return (*this)[j];
}

//
// An accessor for the non-mixture part of a complex state, if
// mixtures are defined.  This will throw an exception otherwise.
//
double& State::Ext(int j)
{
  if (si->ptype != StateInfo::Extended)
    {
      throw BIEException("Inconsistent request",
			 "This state has does not have an extended component",
			 __FILE__, __LINE__);
    }
  
  if (static_cast<unsigned>(j) >= si->Next) {
    ostringstream ostr;
    ostr << "State: this state has an extended component"
	 << " with dimension " << si->Next
	 << ", but you asked for index " << j;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  return (*this)[Mcur*(1 + si->Ndim) + j];
}


//
// An accessor to the entire weight vector, if mixtures are defined.
// This will throw an exception otherwise.
//
vector<double> State::Wght()
{
  if (si->ptype != StateInfo::Mixture &&
      si->ptype != StateInfo::Extended)
    {
      throw BIEException("Inconsistent request",
			 "This state is not defined as a mixture",
			 __FILE__, __LINE__);
    }

  vector<double> ret((*this).begin(), (*this).begin() + Mcur);
  return ret;
}

//
// An accessor to the mth component vector of the mixture, if mixtures
// are defined.  This will throw an exception otherwise.
//
vector<double> State::Phi(int m)
{
  if (si->ptype != StateInfo::Mixture &&
      si->ptype != StateInfo::Extended)
    {
      throw BIEException("Inconsistent request",
			 "This state is not defined as a mixture",
			 __FILE__, __LINE__);
    }

  if (static_cast<unsigned>(m) >= Mcur) {
    ostringstream ostr;
    ostr << "State: this state has " << Mcur
	 << " components, but you asked for index " << m;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  vector<double> ret;
  ret.assign(this->begin() + Mcur + si->Ndim*m,
	     this->begin() + Mcur + si->Ndim*(m+1));

  return ret;
}

//
// An accessor to the all the component vectors of the mixture, if
// mixtures are defined.  This will throw an exception otherwise.
//
vector< vector<double> > State::Phi()
{
  if (si->ptype != StateInfo::Mixture &&
      si->ptype != StateInfo::Extended)
    {
      throw BIEException("Inconsistent request",
			 "This state is not defined as a mixture",
			 __FILE__, __LINE__);
    }
  
  vector< vector<double> > ret(Mcur);
  for (unsigned m=0; m<Mcur; m++)
    ret[m].assign(this->begin() + Mcur + si->Ndim*m,
		  this->begin() + Mcur + si->Ndim*(m+1));

  return ret;
}

//
// Pack the state with sanity checks.
//
void State::setState(int M, vector<double>& vec)
{
  if (si->ptype != StateInfo::Mixture &&
      si->ptype != StateInfo::Extended) {
    if (vec.size() != si->Ntot) {
      ostringstream ostr;
      ostr << "State: this state has size " << si->Ntot
	   << " but you supplied  " << vec.size() << " components";
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
    
    *static_cast<vector<double>*>(this) = vec;

  } else {

    if (static_cast<unsigned>(M) > si->M) {
      ostringstream ostr;
      ostr << "State: this state has maximum cardinality of " << si->M
	   << " components, but you asked to use  " << M << " components";
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
      
    *static_cast<vector<double>*>(this) = vec;
  }
}

void State::setState(int M, vector<double>& wght, 
		     vector< vector<double> > &phi)
{
  if (si->ptype != StateInfo::Mixture &&
      si->ptype != StateInfo::Extended)
    {
      throw BIEException("Inconsistent request",
			 "This state is not defined as a mixture",
			 __FILE__, __LINE__);
    }

  if (static_cast<unsigned>(M) > si->M) {
    ostringstream ostr;
    ostr << "State: this state has maximum cardinality of " << si->M
	 << " components, but you asked to use  " << M << " components";
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  Reset(M);
  for (unsigned m=0; m<Mcur; m++) {
    Wght(m) = wght[m];
    for (unsigned j=0; j<si->Ndim; j++) {
      Phi(m, j) = phi[m][j];
    }
  }
}

//
// Pack the full vector from a mixture model breakdown including an
// non-mixture component, if mixtures are defined.  This will throw an
// exception otherwise.
//
void State::setState(int M, vector<double>& wght, 
		     vector< vector<double> > &phi,
		     vector<double>& ext)
{
  if (ext.size() != si->Next) {
    ostringstream ostr;
    ostr << "State: the extended state should have length " << si->Next
	 << ", but you supplied length " << ext.size();
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  Reset(M);
  for (unsigned m=0; m<Mcur; m++) {
    Wght(m) = wght[m];
    for (unsigned j=0; j<si->Ndim; j++) {
      Phi(m, j) = phi[m][j];
    }
  }

  for (unsigned j=0; j<si->Next; j++) Ext(j) = ext[j];
}


StateInfo::StateInfo()
{
  M       = 0;
  Ndim    = 0;
  Next    = 0;
  Ntot    = 0;
  ptype   = None;
  nBlocks = 1;
  iorder  = -1;
  labeld  = false;
}

StateInfo::StateInfo(int n)
{
  M       = 0;
  Ndim    = 0;
  Next    = 0;
  Ntot    = n;
  ptype   = None;
  nBlocks = 1;
  iorder  = -1;
  labeld  = false;
  createDefaultLabels();
}

StateInfo::StateInfo(int nmix, int ndim)
{
  M       = nmix;
  Ndim    = ndim;
  Next    = 0;
  Ntot    = nmix*(ndim+1);
  ptype   = Mixture;
  nBlocks = 1;
  iorder  = -1;
  labeld  = false;
  createDefaultLabels();
}

StateInfo::StateInfo(int nmix, int ndim, int next)
{
  M       = nmix;
  Ndim    = ndim;
  Next    = next;
  Ntot    = nmix*(ndim+1) + next;
  ptype   = Extended;
  nBlocks = 1;
  iorder  = -1;
  labeld  = false;
  createDefaultLabels();
}

StateInfo::StateInfo(clivectori* beg, clivectori* end, clivectori *itype)
{
  ptype     = Block;

  Block_beg = (*beg)();
  Block_end = (*end)();

  if (Block_beg.size() != Block_end.size()) {
    ostringstream ostr;
    ostr << "StateInfo: the block position vectors have unequal dimenions: "
	 << "dim(beg)=" << Block_beg.size() << " != "
	 << "dim(end)=" << Block_end.size();
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

  nBlocks   = Block_beg.size();

  if (nBlocks != (*itype)().size()) {
    ostringstream ostr;
    ostr << "StateInfo: end points imply a size " << nBlocks 
	 << " but the type vector has size " << (*itype)().size();
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

  std::vector<int>& v = (*itype)();

  for (size_t n=0; n<nBlocks; n++) {
    switch (v[n]) {
    case Standard:
      bType.push_back(Standard);
      break;
    case PositiveDef:
      bType.push_back(PositiveDef);
      break;
    case FixedNorm:
      bType.push_back(FixedNorm);
      break;
    default:
      std::cout << "StateInfo: no such BlockType [" << v[n] << "] "
		<< "at position " << n << ", assuming Standard" << std::endl;
      bType.push_back(Standard);
      break;
    }
  }

  Ndim      = Block_end.back();
  Ntot      = Ndim;
  ptype     = Block;
  iorder    = -1;
  labeld    = false;

  // Build blocking masks
  for (size_t n=0; n<nBlocks; n++) {
    bMask mask(Ntot, false);
    for (int i=Block_beg[n]; i<Block_end[n]; i++) mask[i] = true;
    Block_mask[n] = mask;
  }
  Block_mask[-1] = bMask(Ntot, true);

  createDefaultLabels();
}

StateInfo::StateInfo(int ndim, int t, int n1, int n2)
{
  Ndim    = ndim;
  T       = t;
  N1      = n1;
  N2      = n2;
  Ntot    = 1 + T + N1 + N2;
  ptype   = RJTwo;
  nBlocks = 1;
  iorder  = -1;
  labeld  = false;

  createDefaultLabels();
}

StateInfo::StateInfo(const StateInfo& p)
{
  M           = p.M;
  Ndim        = p.Ndim;
  Next        = p.Next;
  Ntot        = p.Ntot;
  ptype       = p.ptype;
  iorder      = p.iorder;
  labeld      = p.labeld;
  nBlocks     = p.nBlocks;
  Block_beg   = p.Block_beg;
  Block_end   = p.Block_end;
  ParamLabels = p.ParamLabels;
  ExtLabels   = p.ExtLabels;
  StateLabels = p.StateLabels;
}

void StateInfo::Reset(unsigned Mnew, State* const s)
{
  if (ptype == None)  return;
  if (ptype == Block) return;
  if (Mnew==s->Mcur)  return;

  s->Mcur   = min<unsigned>(M, Mnew);
  s->Ntot   = s->Mcur*(Ndim+1) + Next;
  if (static_cast<vector<double>*>(s)->size() < s->Ntot)
    *static_cast<vector<double>*>(s) = vector<double>(s->Ntot);

  labeld = false;
  createDefaultLabels();
}


void StateInfo::createDefaultLabels()
{
  if (labeld) return;
  ostringstream ostr;

  if (ptype == None || ptype == Block) {
    for (unsigned n=0; n<Ntot; n++) {
      ostr.str("");
      ostr << "Comp(" << n+1 << ")";
      StateLabels.push_back(ostr.str());
    }
  } else if (ptype == RJTwo) {
    StateLabels.push_back(string("Ind"));

    for (unsigned n=0; n<T; n++) {
      ostr.str("");
      ostr << "Common(" << n+1 << ")";
      StateLabels.push_back(ostr.str());
    }

    for (unsigned n=0; n<N1; n++) {
      ostr.str("");
      ostr << "Model_1(" << n+1 << ")";
      StateLabels.push_back(ostr.str());
    }

    for (unsigned n=0; n<N2; n++) {
      ostr.str("");
      ostr << "Model_2(" << n+1 << ")";
      StateLabels.push_back(ostr.str());
    }

  } else {

    StateLabels = vector<string>(Ntot);
    ParamLabels = vector<string>(Ndim);
    ExtLabels   = vector<string>(Next);

    for (unsigned m=0; m<M; m++) {
      ostr.str("");
      ostr << "Weight[" << m+1 << "]";
      StateLabels[m] = ostr.str();
    }
    
    for (unsigned m=0; m<M; m++) {
      for (unsigned j=0; j<Ndim; j++) {
	ostr.str("");
	ostr << "Phi(" << j+1 << ")[" << m+1 << "]";
	StateLabels[M + Ndim*m + j] = ostr.str();
      }
    }

    // Labels for each parameter
    for (unsigned j=0; j<Ndim; j++) {
      ostr.str("");
      ostr << "Phi(" << j+1 << ")";
      ParamLabels[j] = ostr.str();
    }

    // Create default labels for extended component
    for (unsigned j=0; j<Next; j++) {
      ostr.str("Psi(");
      ostr << j+1 << ")";
      ExtLabels.push_back(ostr.str());
      StateLabels[M*(Ndim+1) + j] = ostr.str();
    }
  }

  labeld = true;
}

void StateInfo::labelAll      (clivectors* labels)
{
  labelAll((*labels)());
}

void StateInfo::labelAll      (const vector<string>& labels)
{
  createDefaultLabels();

  // Check expected size
  if (static_cast<unsigned>(Ntot) != labels.size()) {
    ostringstream ostr;
    ostr << "StateInfo: I need a string vector of size " 
	 << Ntot << ", one for every element"
	 << ", you supplied one with size " << labels.size();
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

  StateLabels.assign(labels.begin(), labels.end());

}

void StateInfo::labelMixture  (clivectors* labels)
{
  labelMixture((*labels)());
}

void StateInfo::labelMixture  (const vector<string>& labels)
{
  createDefaultLabels();

  ostringstream ostr;

  // Sanity check on type
  if (ptype == None || ptype == Block)
    {
      string msg("StateInfo: you can not set labels for a mixture model "
		 "with a simple state type");

      throw BIEException("Inconsistent request", msg,
			 __FILE__, __LINE__);
    }

  // Check expected size
  if (Ndim != labels.size()) {
    ostr << "StateInfo: I need a string vector of size " 
	 << Ndim << ", one for each parameter in the mixture model"
	 << ", you supplied one with size " << labels.size();
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

  ParamLabels.assign(labels.begin(), labels.end());

  for (unsigned m=0; m<M; m++) {
    for (unsigned j=0; j<Ndim; j++) {
      ostr.str("");
      ostr << labels[j] << "[" << m+1 << "]";
      StateLabels[M + Ndim*m + j] = ostr.str();
    }
  }

}

void StateInfo::labelExtended  (clivectors* labels)
{
  labelExtended((*labels)());
}


void StateInfo::labelExtended  (const vector<string>& labels)
{
  createDefaultLabels();

  ostringstream ostr;
  
  // Sanity check on type
  if (ptype != Extended)
    {
      string msg("StateInfo: you can not set extended labels for"
		 " a simple mixture model");

      throw BIEException("Inconsistent request", msg,
			 __FILE__, __LINE__);
    }

  // Check expected size
  if (Next != labels.size()) {
    ostr << "StateInfo: I need a string vector of size " 
	 << Ndim << ", one for each parameter in the extended component"
	 << ", you supplied one with size " << labels.size();
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

  ExtLabels.assign(labels.begin(), labels.end());

  for (unsigned j=0; j<Next; j++)
    StateLabels[M*(Ndim+1) + j] = ExtLabels[j];
}

void StateInfo::labelFromModel(Model* model)
{
  // Just in case the model does not define any labels
  createDefaultLabels();

  vector<string> labels = model->ParameterLabels();

  // If the model has not defined parameter labels, we'll use the
  // default
  if (labels.size()) {

    // If model is not a mixture, ParameterLabels() is assumed to
    // provide labels for entire vector
    if (ptype == None)  labelAll(labels);
    if (ptype == Block) labelAll(labels);

    // If the model is a mixture, call the mixture member
    if (ptype == Mixture || ptype == Extended) {
      labelMixture (labels);
      
      // If an extended component exists, get the extended labels
      if (ptype == Extended) {
	vector<string> labext = model->ExtendedLabels();
	// If the model defines labels for the extended component, use
	// them instead of the default values
	if (labext.size()) labelExtended(labext);
      }
    }
  }

}

void StateInfo::labelFromLike(LikelihoodFunction* like)
{
  // Just in case the model does not define any labels
  createDefaultLabels();

  // Search through list from likelihood function for non-zero entries
  for (size_t i=0; i<StateLabels.size(); i++) {
    std::string ret = like->ParameterDescription(i);
    if (ret.size()) StateLabels[i] = ret;
  }
}

const string StateInfo::ParameterDescription(unsigned i)
{
  createDefaultLabels();

  if (i<Ntot) return StateLabels[i];
  else return string("*ERROR*");
}

void StateInfo::setOrdering(int n)
{
  // Sanity check on type
  if (ptype == None || ptype == Block)
    {
      string msg("StateInfo: you can _only_ set ordering for a mixture model");
      throw BIEException("Inconsistent request", msg,
			 __FILE__, __LINE__);
    }

  // Because CLI doesn't do unsigned ints, sigh
  if (static_cast<unsigned>(n)<=Ndim)
    iorder = n;
  else {
    ostringstream ostr;
    ostr << "StateInfo: you must specify a rank <= " << Ndim
	 << ", you supplied the value " << n;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
 
}

void State::Reorder()
{
  if (si->iorder == static_cast<unsigned>(-1)) return;

				// This will sort pair by double value
  typedef pair<double, unsigned> pDI;
  set<pDI> val;

  for (unsigned m=0; m<Mcur; m++) {
    if (si->iorder == 0)
      val.insert(pDI(Wght(m), m));
    else
      val.insert(pDI(Phi(m, si->iorder-1), m));
  }
				// Save the state to a vector
  vector<double> sav = static_cast< vector<double> >(*this);
  set<pDI>::iterator it=val.begin();
  for (unsigned m=0; m<Mcur; m++, it++) {
				// it->second points to the current rank
				// of the new mth component.  Copy only
    if (it->second != m) {	// if different . . .
      (*this)[m] = sav[it->second];
      for (unsigned j=0; j<si->Ndim; j++) 
	(*this)[Mcur+si->Ndim*m + j] = sav[Mcur+si->Ndim*it->second + j];
    }
  }
}


const string State::LabelHeader()
{
  StateInfo::Pattern ptype = si->ptype;
  ostringstream ostr;

  switch (ptype) {
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    for (unsigned j=0; j<si->Ntot; j++) {
      ostr << setw(op) << si->ParameterDescription(j);
    }
    break;
    
  case StateInfo::Mixture:
  case StateInfo::Extended:
    {
      ostr << setw(4)  << "N" << ": ";
      ostr << setw(op) << "Weight" << " | ";
      for (unsigned j=0; j<si->Ndim; j++)
	ostr << setw(op) << si->ParamLabels[j];
      if (ptype==StateInfo::Extended) {
	ostr << " | ";
	for (unsigned j=0; j<si->Next; j++)
	  ostr << setw(op) << si->ExtLabels[j];
      }
    }
    
    break;
  }

  return ostr.str();
}


void State::Broadcast(int id)
{
  if (!mpi_used) return;

  unsigned sz;
  if (myid==id) sz = (*this).size();
  MPI_Bcast(&sz,         1,  MPI_UNSIGNED, id, MPI_COMM_WORLD);
  MPI_Bcast(&(*this)[0], sz, MPI_DOUBLE,   id, MPI_COMM_WORLD);
  MPI_Bcast(&Mcur,       1,  MPI_UNSIGNED, id, MPI_COMM_WORLD);
  MPI_Bcast(&Ntot,       1,  MPI_UNSIGNED, id, MPI_COMM_WORLD);
}  


void State::enforce()
{
  if (si->ptype == StateInfo::Block) 
    {
      for (size_t n=0; n<si->nBlocks; n++) {
	// Conditions enforced here . . .
	//
	if (si->bType[n] == StateInfo::PositiveDef) {
	  for (int j=si->Block_beg[n]; j<si->Block_end[n]; j++) {
	    if ((*this)[j]<=0.0) (*this)[j] = minC;
	  }
	}
	if (si->bType[n] == StateInfo::FixedNorm) {
	  double norm = 0.0;
	  for (int j=si->Block_beg[n]; j<si->Block_end[n]; j++) {
	    if ((*this)[j]<=0.0) (*this)[j] = minC;
	    norm += (*this)[j];
	  }
	  for (int j=si->Block_beg[n]; j<si->Block_end[n]; j++) {
	    (*this)[j] /= norm;
	  }
	}
      }
    }
}

std::string StateInfo::Info()
{
  std::ostringstream sout;
 
  sout << std::string(60, '-') << std::endl;

  switch (ptype) {
  case StateInfo::None:
    sout << std::setw(10) << std::left << "Pattern" << "None" << std::endl;
    break;
  case StateInfo::Block:
    sout << std::setw(10) << std::left << "Pattern" << "Block" << std::endl;
    break;
  case StateInfo::Mixture:
    sout << std::setw(10) << std::left << "Pattern" << "Mixture" << std::endl
	 << std::setw(10) << std::left << ""
	 << std::setw(10) << std::left << "M"  << M  << std::endl
	 << std::setw(10) << std::left << ""
	 << std::setw(10) << std::left << "Ndim" << Ndim << std::endl
	 << std::setw(10) << std::left << ""
	 << std::setw(10) << std::left << "iorder" << iorder << std::endl;
  case StateInfo::Extended:
    sout << std::setw(10) << std::left << "Pattern" << "Extended" << std::endl
	 << std::setw(10) << std::left << ""
	 << std::setw(10) << std::left << "Next" << Next << std::endl;
    break;
  case StateInfo::RJTwo:
    sout << std::setw(10) << std::left << "Pattern" << "RJTwo" << std::endl
	 << std::setw(10) << std::left << ""
	 << std::setw(10) << std::left << "T"  << T  << std::endl
	 << std::setw(10) << std::left << ""
	 << std::setw(10) << std::left << "N1" << N1 << std::endl
	 << std::setw(10) << std::left << ""
	 << std::setw(10) << std::left << "N2" << N2 << std::endl;
  break;
  }

  sout << std::setw(10) << std::left << "Ntot" << Ntot << std::endl;

  if (ParamLabels.size()) {
    sout << "Parameter labels" << std::endl;
    for (auto s : ParamLabels)
      sout << std::setw(10) << std::left << "" << s << std::endl;
  }

  if (ExtLabels.size()) {
    sout << "Extended labels" << std::endl;
    for (auto s : ExtLabels)
      sout << std::setw(10) << std::left << "" << s << std::endl;
  }

  if (StateLabels.size()) {
    sout << "State labels" << std::endl;
    for (auto s : StateLabels)
      sout << std::setw(10) << std::left << "" << s << std::endl;
  }

  sout << std::string(60, '-') << std::endl;

  return sout.str();
}
