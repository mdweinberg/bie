
#include <StateFile.h>

#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cstdlib>
				// For file stat
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <BIEmpi.h>

using namespace std;
using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(BIE::element)

bool BIE::compare_likelihood(const element& a, const element& b)
{
  if (a.pval[1] != b.pval[1]) return a.pval[1] < b.pval[1];
  else {
    for (unsigned n=0; n<a.point.size(); n++) {
      if (a.point[n] != b.point[n]) return a.point[n] < b.point[n];
    }
    return false;
  }
}

bool BIE::compare_posterior(const element& a, const element& b)
{
  return a.pval[0] < b.pval[0];
}

element::element(int root)
{
  unsigned n;
  MPI_Bcast(&n,        1, MPI_UNSIGNED, root, MPI_COMM_WORLD);
  pval = std::vector<double>(n);
  MPI_Bcast(&pval[0],  n, MPI_DOUBLE,   root, MPI_COMM_WORLD);
  MPI_Bcast(&beta,     1, MPI_DOUBLE,   root, MPI_COMM_WORLD);
  MPI_Bcast(&X,        1, MPI_DOUBLE,   root, MPI_COMM_WORLD);
  MPI_Bcast(&weight,   1, MPI_DOUBLE,   root, MPI_COMM_WORLD);

  MPI_Bcast(&n,        1, MPI_UNSIGNED, root, MPI_COMM_WORLD);
  point = std::vector<double>(n);
  MPI_Bcast(&point[0], n, MPI_DOUBLE,   root, MPI_COMM_WORLD);
}

element::element(const element &p)
{
  pval   = p.pval;
  beta   = p.beta;
  X      = p.X;
  weight = p.weight;
  point  = p.point;
}

void element::mpiSend()
{
  unsigned n = pval.size();
  MPI_Bcast(&n,        1, MPI_UNSIGNED, myid, MPI_COMM_WORLD);
  MPI_Bcast(&pval[0],  n, MPI_DOUBLE,   myid, MPI_COMM_WORLD);
  MPI_Bcast(&beta,     1, MPI_DOUBLE,   myid, MPI_COMM_WORLD);
  MPI_Bcast(&X,        1, MPI_DOUBLE,   myid, MPI_COMM_WORLD);
  MPI_Bcast(&weight,   1, MPI_DOUBLE,   myid, MPI_COMM_WORLD);

  n = point.size();
  MPI_Bcast(&n,        1, MPI_UNSIGNED, myid, MPI_COMM_WORLD);
  MPI_Bcast(&point[0], n, MPI_DOUBLE,   myid, MPI_COMM_WORLD);
}

void element::write(ostream& out) 
{
  unsigned n;

  n = pval.size();
  out.write((const char *)&n, sizeof(unsigned));
  for (unsigned i=0; i<n; i++)
      out.write((const char *)&pval[i], sizeof(double));

  out.write((const char *)&weight, sizeof(double));
  out.write((const char *)&beta,   sizeof(double));

  n = point.size();
  out.write((const char *)&n, sizeof(unsigned));
  for (unsigned i=0; i<n; i++)
    out.write((const char *)&point[i], sizeof(double));
}

bool element::read(istream& in) 
{
  unsigned n;

  in.read((char *)&n, sizeof(unsigned));
  if (in.bad() || in.eof()) return false;

  pval = vector<double>(n);

  for (unsigned i=0; i<n; i++)
    in.read((char *)&pval[i], sizeof(double));

  if (in.bad() || in.eof()) return false;

  in.read((char *)&weight, sizeof(double));
  in.read((char *)&beta,   sizeof(double));

  in.read((char *)&n, sizeof(unsigned));

  if (in.bad() || in.eof()) return false;

  point = vector<double>(n);

  for (unsigned i=0; i<n; i++)
    in.read((char *)&point[i], sizeof(double));

  if (in.good()) return true;
  else           return false;
}

StateFile::StateFile(const string& in_file, 
		     const string& cache_file, 
		     const string& limit_file, 
		     int level, int skip, int strd, int keep, 
		     bool ignore, bool quiet, int dump)
{
  file         = in_file;
  cache        = cache_file;
  limit        = limit_file;
  this->level  = level;
  this->skip   = skip;
  this->strd   = strd;
  this->keep   = keep;
  this->ignore = ignore;
  this->quiet  = quiet;
  temps        = 1;

  if (myid) quiet = true;

  //--------------------------------------------------
  // Read in state file if cache file is mismatched,
  // expired, or not desired
  //--------------------------------------------------

  if (!ReadCache()) {
    ReadData();
    WriteCache();
  }
}

void StateFile::ReadData()
{
  ifstream in(file.c_str());
  if (!in) {
    cerr << "StateFile: error opening <" << file << ">\n";
    exit(-1);
  }

  if (!quiet) {
    cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
    cout << "Reading the input file <" << file << "> . . . " << flush;
  }

  const int linesize = 131072;
  char line[linesize];
  vector<double> pvals(4, 1.0);
  double dd;
  bool firsttime = true;
  bool mixture   = false;
  unsigned use   = 0;
  
  popped = 0;
  cnt    = 0;
  nmix = ndim = nmix0 = ndim0 = -1;
  
				// Read in and parse header string
  in.getline(line, linesize);
  mixture = isMixture(line);
				// Read in the lines at the desired level
  int l;
  unsigned totalL = 0;
  while (in) {
    in.getline(line, linesize);
    if (!in) continue;
    totalL++;
    
    istringstream istr(line);	// check level
    istr >> l;
    if (l != level) continue;
    if (cnt++<skip) continue;
    if (use++%strd) continue;
    

    element elem;
    
    istr >> l;			// dump counter
    istr >> pvals[0];		// read prob level
    istr >> pvals[1];		// read likelihood
    istr >> pvals[2];		// read prior
    if (mixture)
      istr >> nmix;		// mixture count
    
    elem.pval = pvals;
    
    ndim = 0;
    
    if (mixture) {
				// Select which weight to ignore
      ignr = dump;
      if (ignore && (dump<0 || dump>nmix-1)) ignr = nmix-1;
      
      for (int i=0; i<nmix; i++)  {
	istr >> dd;
	if (i == static_cast<int>(ignr)) continue;
	elem.point.push_back(dd);
	ndim++;
      }
    }
    
    
    while (istr.good()) {
      istr >> dd;
      elem.point.push_back(dd);
      ndim++;
    }
    
    prob.push_back(elem);
    if (keep>0 && static_cast<int>(prob.size())>keep) {
      prob.pop_front();
      popped++;
    }
    
    if (firsttime) {
      nmix0 = nmix;
      ndim0 = ndim;
      firsttime = false;
    } else {
      if (nmix!=nmix0) {
	cout << "Mixture count inconsistent! [" << nmix << " != " << nmix0
	     << "] line=" << cnt << endl;
      }
      if (ndim!=ndim0) {
	cout << "Dimension count inconsistent! [" << ndim << " != " << ndim0
	     << "] line=" << cnt << endl;
      }
    }
    
  }

  //
  // Compute requested skip
  //
  if (skip<0) {
    size_t dsize = prob.size();
    size_t nskip = totalL/2;
    if (dsize*strd>nskip) {
      size_t remove = (dsize*strd - nskip)/strd;
      prob.erase(prob.begin(), prob.begin() + remove);
    }
  }
}


void StateFile::WriteCache()
{
  if (cache.length() && myid==0) {
    char line [256];
    strncpy(line, file.c_str(), 256);
    
    ofstream bin(cache.c_str());
    if (bin) {
      bin.write((const char *)&magic,  sizeof(unsigned int));
      bin.write((const char *)line,    256);
      
      bin.write((const char *)&level,  sizeof(int));
      bin.write((const char *)&skip,   sizeof(int));
      bin.write((const char *)&strd,   sizeof(int));
      bin.write((const char *)&keep,   sizeof(int));
      bin.write((const char *)&dump,   sizeof(int));
      
      bin.write((const char *)&popped, sizeof(int));
      bin.write((const char *)&cnt,    sizeof(int));
      bin.write((const char *)&nmix,   sizeof(int));
      bin.write((const char *)&ndim,   sizeof(int));
      bin.write((const char *)&nmix0,  sizeof(int));
      bin.write((const char *)&ndim0,  sizeof(int));
      
      size_t nlabs = labs.size();
      bin.write((const char *)&nlabs,  sizeof(size_t));
      bin.write((const char *)&lbeg,   sizeof(size_t));
      for (size_t n=0; n<nlabs; n++) {
	size_t nchar = labs[n].size();
	bin.write((const char *)&nchar, sizeof(size_t));
	bin.write((const char *)labs[n].c_str(), nchar*sizeof(char));
      }

      deque<element>::iterator it;
      for (it=prob.begin(); it!=prob.end(); it++) it->write(bin);
      
      if (!quiet) cout << "cache written with " 
		       << prob.size() << " states . . . " << flush;
      
    }
  }
  
  if (!quiet) cout << "done" << endl;
}

bool StateFile::ReadCache()
{
  bool cache_read = false;

  //--------------------------------------------------
  // Check
  //--------------------------------------------------

  time_t f_time=0, c_time=0;

  bool f_exists = false;
  bool c_exists = false;
  bool c_expire = false;

  struct stat b;

				// Check the existence and mod time of
				// the input file
  if (!stat(file.c_str(), &b)) {
    f_exists = true;
    f_time = b.st_mtime;
  }

				// Check the existence and mod time of
				// the cache file
  if (cache.length() && !stat(cache.c_str(), &b)) {
    c_exists = true;
    c_time = b.st_mtime;
  }

				// Check to see if the cache file is
				// newer than the input file
  if ( c_exists && f_exists && c_time<f_time ) {
    c_expire = true;
    if (!quiet) {
      cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
      cout << "Cache file <" << cache << "> is expired" << endl;
    }
    return cache_read;
  }

  //--------------------------------------------------
  // Attempt to read a cache file if not expired
  //--------------------------------------------------

				// Make a vector to hold posterior
				// probability and prior
  popped = 0;
  cnt    = 0;
  ndim   = -1;
  ndim0  = -1;

  if ( c_exists && !c_expire ) {

    ifstream bin(cache.c_str());

    if (bin) {

      if (!quiet) {
	cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
	cout << "Reading the cache file <" << cache << "> . . . " << flush;
      }

      int level0, skip0, strd0, keep0, dump0;

      unsigned magic1;
      bin.read((char *)&magic1, sizeof(unsigned int));

      if (magic != magic1) {
	if (!quiet) std::cout << "FAILED" << std::endl
			      << "Wrong cache type detected"
			      << std::endl;
      } else {

	char line[256];
	bin.read((char *)line,    256);
	string cfile(line);
      
	bin.read((char *)&level0, sizeof(int));
	bin.read((char *)&skip0,  sizeof(int));
	bin.read((char *)&strd0,  sizeof(int));
	bin.read((char *)&keep0,  sizeof(int));
	bin.read((char *)&dump0,  sizeof(int));
	
	bin.read((char *)&popped, sizeof(int));
	bin.read((char *)&cnt,    sizeof(int));
	bin.read((char *)&nmix,   sizeof(int));
	bin.read((char *)&ndim,   sizeof(int));
	bin.read((char *)&nmix0,  sizeof(int));
	bin.read((char *)&ndim0,  sizeof(int));
	
	size_t nlabs;
	bin.read((char *)&nlabs,  sizeof(size_t));
	bin.read((char *)&lbeg,   sizeof(size_t));
	labs.resize(nlabs);
	for (size_t n=0; n<nlabs; n++) {
	  size_t nchar;
	  bin.read((char *)&nchar, sizeof(size_t));
	  labs[n] = std::string(nchar, '\0');
	  bin.read((char *)labs[n].c_str(), nchar*sizeof(char));
	}

	if (bin.good() && cfile.compare(file) == 0 && level == level0 &&
	    skip == skip0 && strd == strd0 && keep == keep0) {

	  element elem;
	  while (bin) {
	    if (elem.read(bin)) prob.push_back(elem);
	  }
	  cache_read = true;
	  if (!quiet) cout << "good! " << prob.size() << " states." << endl;
	}
      }
    }

    if (!quiet) {
      cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
    }

  }

  return cache_read;
}


const unsigned StateFile::magic = 0xfabd;

StateFile::StateFile(Ensemble *ens,
		     int skip, int strd, int keep, 
		     bool ignore, bool quiet, int dump)
{
  this->skip   = skip;
  this->strd   = strd;
  this->keep   = keep;
  this->ignore = ignore;
  this->quiet  = quiet;

  temps = 1;

  if (myid) quiet = true;

  // Labels

  labs.clear();
  labs.push_back("Level");
  labs.push_back("Iter");
  labs.push_back("Probability");
  labs.push_back("Likelihood");
  labs.push_back("Prior");
  lbeg = 5;


  // Make a vector to hold posterior
  // probability and prior

  level = 0;
  cnt = popped = ens->Npopped();

  bool mixture   = false;
  StateInfo *si  = ens->getStateInfo();
  if (si->ptype == StateInfo::Mixture  || 
      si->ptype == StateInfo::Extended) {
    mixture = true;
    nmix = nmix0 = si->M;
    ndim = ndim0 = si->Ndim;
    lbeg++;
    labs.push_back("Number");
    for (int i=0; i<nmix; i++) {
      ostringstream s;
      s << "Wgt(" << i << ")";
      labs.push_back(s.str());
    }
    for (int i=0; i<nmix; i++) {
      for (int j=0; j<ndim; j++) {
	ostringstream s;
	s << "Phi(" << i << ")[" << j << "]";
	labs.push_back(s.str());
      }
    }
  } else {
    ndim = ndim0 = si->Ntot;
    for (int j=0; j<ndim; j++) {
      ostringstream s;
      s << "Phi[" << j << "]";
      labs.push_back(s.str());
    }
  }
  
  if (myid==0) {

    vector<double> pvals(4, 1.0);
    deque<StateData>::iterator it     = ens->getStates().begin();
    deque<StateData>::iterator it_end = ens->getStates().end();

    unsigned use = 0;
    
    for (; it != it_end; it++) {
      if (cnt++<skip) continue;
      if (use++%strd) continue;
    
      element elem;
      elem.pval   = it->prob;
    
      vector<double> vec(it->p.vec());
      unsigned k = 0;
      
      if (mixture) {
	// Select which weight to ignore
	ignr = dump;
	if (ignore && (dump<0 || dump>nmix-1)) ignr = nmix-1;
	
	for (int i=0; i<nmix; i++)  {
	  if (i != static_cast<int>(ignr)) elem.point.push_back(vec[k]);
	  k++;
	}
	
	for (int i=0; i<nmix; i++) {
	  for (int j=0; j<ndim; j++) {
	    if (i != static_cast<int>(ignr)) elem.point.push_back(vec[k]);
	    k++;
	  }
	}

	if (si->ptype == StateInfo::Extended) {
	  for (; k<si->Ntot; k++) elem.point.push_back(vec[k]);
	}
	
      } else {
	for (int j=0; j<ndim; j++) {
	  elem.point.push_back(vec[j]);
	}
      }
      
      prob.push_back(elem);
      if (keep>0 && static_cast<int>(prob.size())>keep) {
	prob.pop_front();
	popped++;
      }
    }
    
    // Send the prob deque
    unsigned n = prob.size();
    MPI_Bcast(&n, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    for (std::deque<element>::iterator 
	   it = prob.begin(); it != prob.end(); it++) it->mpiSend();

  } else {
    unsigned n;
    MPI_Bcast(&n, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    for (unsigned i=0; i<n; i++) {
      element elem(0);
      prob.push_back(elem);
    }
  }

  MPI_Bcast(&cnt,    1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&popped, 1, MPI_INT, 0, MPI_COMM_WORLD);
}


StateFile::StateFile(Prior *prior)
{
  skip   = 0;
  strd    = 0;
  keep    = 0;
  quiet   = false;
  dump    = -1;

  cnt     = 0;
  popped  = 0;

  temps   = 1;
  if (myid) quiet = true;

  // Labels

  labs.clear();
  labs.push_back("Level");
  labs.push_back("Iter");
  labs.push_back("Probability");
  labs.push_back("Likelihood");
  labs.push_back("Prior");
  lbeg = 5;


  // Make a vector to hold posterior
  // probability and prior

  mixture = false;
  const StateInfo *si  = prior->SI();
  if (si->ptype == StateInfo::Mixture  || 
      si->ptype == StateInfo::Extended) {
    mixture = true;
    nmix = nmix0 = si->M;
    ndim = ndim0 = si->Ndim;
    lbeg++;
    labs.push_back("Number");
    for (int i=0; i<nmix; i++) {
      ostringstream s;
      s << "Wgt(" << i << ")";
      labs.push_back(s.str());
    }
    for (int i=0; i<nmix; i++) {
      for (int j=0; j<ndim; j++) {
	ostringstream s;
	s << "Phi(" << i << ")[" << j << "]";
	labs.push_back(s.str());
      }
    }
  } else {
    ndim = ndim0 = si->Ntot;
    for (int j=0; j<ndim; j++) {
      ostringstream s;
      s << "Phi[" << j << "]";
      labs.push_back(s.str());
    }
  }
}


//
// Parse the header into field labels.  Look for "Number" after
// the "Prior" field as a sign of a mixture
//
bool StateFile::isMixture(char *line)
{
  labs.clear();
  char *p = line;
  while (*p!='\0') {
    if (*p=='"') {
      labs.push_back(string(""));
      while (*(++p)!='"') labs.back().push_back(*p);
    }
    ++p;
  }

  bool ret = false;
  for (unsigned n=0; n<labs.size(); n++) {
    if (labs[n].compare("Prior")==0) {
      lbeg = n+1;
      if (labs[n+1].compare("Number")==0) {
	ret = true;
	lbeg = n+2;
      }
      break;
    }
  }
  
  return ret;
}

string StateFile::label(size_t n)
{
  if (n+lbeg<labs.size())
    if (n<ignr)
      return labs[n+lbeg];
    else
      return labs[n+lbeg+1];
  else {
    ostringstream str;
    str << "Param(" << n+1 << ")";
    return str.str();
  }
}

void StateFile::dumpFile(const string filename)
{
  const size_t fmtszI = 10, fmtszF = 16;

  std::ofstream out(filename.c_str());

  if (out) {
    for (size_t i=0; i<labs.size(); i++) {
      std::ostringstream sout;
      sout << '"' << labs[i] << '"';
      if (i==0 || i==1 || (lbeg==6 && i==6))
	out << std::setw(fmtszI) << sout.str();
      else
	out << std::setw(fmtszF) << sout.str();
    }
    out << std::endl;
    
    unsigned cnt=0;
    for (std::deque<element>::iterator it=prob.begin(); it!= prob.end(); it++) {
      out << std::setw(fmtszI) << level
	  << std::setw(fmtszI) << cnt++;
      for (size_t i=0; i<3; i++)
	out << std::setw(fmtszF) << it->pval[i];
      if (lbeg == 6)
	out << std::setw(fmtszI) << nmix;
      for (size_t i=0; i<it->point.size(); i++)
	out << std::setw(fmtszF) << it->point[i];
      out << std::endl;
    }
  }
}

void StateFile::shareFile()
{
  if (numprocs==1) return;

  //! Copy the existing deque
  std::deque<element> prob1 = prob;

  for (int id=0; id<numprocs; id++) {
    
    if (id==myid) {

      // Send the prob1 deque to all other processes
      unsigned n = prob1.size();
      MPI_Bcast(&n, 1, MPI_UNSIGNED, id, MPI_COMM_WORLD);
      for (std::deque<element>::iterator 
	     it = prob1.begin(); it != prob1.end(); it++) it->mpiSend();
      
    } else {
      unsigned n;
      MPI_Bcast(&n, 1, MPI_UNSIGNED, id, MPI_COMM_WORLD);
      for (unsigned i=0; i<n; i++) {
	element elem(id);
	prob.push_back(elem);
      }
    }
  }
}

