#include <BIEException.h>
#include <BasicType.h>
#include <RecordType.h>
#include <RecordStream.h>
#include <MPIStreamFilter.h>
#include <MPICommunicationSession.h>
#include <BIEmpi.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif

using namespace BIE;

/*******************************************************************************
* MPI Buffer Filter.
*******************************************************************************/
MPICommunicationSession::MPICommunicationSession
(MPIStreamFilter *filter, int sessionId, MPI_Comm communicator)
{ 
#ifdef DEBUG_TESSTOOL
  ferr << "MPICommunicationSession::MPICommunicationSession Enter" << endl;
#endif

  _sessionId = sessionId;
  _filter = filter;
  _communicator = communicator;

  _numSends =0;

  // create data type for use in MPI_Send
  _sendDtype = convertRecordToDatatype(_filter->_rtype);

#ifdef DEBUG_TESSTOOL
  ferr << "MPICommunicationSession::MPICommunicationSession Leave" << endl;
#endif
}

  
MPICommunicationSession::~MPICommunicationSession()
  {
    MPI_Type_free(&_sendDtype);
  }
 
MPI_Datatype MPICommunicationSession::convertRecordToDatatype(RecordType *rt)
{
  //  ferr << "MPICommunicationSession::convertRecordToDatatype Enter" << endl;
  int numFields = rt->numFields();
  MPI_Datatype dtype;
  int          *len = new int[numFields]; 
  MPI_Aint     *loc= new MPI_Aint[numFields];  
  MPI_Datatype *typ = new MPI_Datatype[numFields]; 
  int offset = 0;
  
  for (int fieldindex = 1; fieldindex <= numFields; fieldindex++)
    {
      BasicType *btype = rt->getFieldType(fieldindex);
    len[fieldindex-1] = 1; 
    loc[fieldindex-1] = offset; 
    if (btype == BasicType::Int) {
	typ[fieldindex-1] = MPI_INT;
      offset += sizeof(int); 
    } else if (btype == BasicType::Real) {
      typ[fieldindex-1] = MPI_DOUBLE;
      offset += sizeof(double); 
    } else if (btype == BasicType::Bool) {
      typ[fieldindex-1] = MPI_BYTE;
      offset += sizeof(bool); 
    } else {
      throw StreamInheritanceException(__FILE__, __LINE__);
    }

    }
    MPI_Type_struct(numFields, len, loc, typ, &dtype);
    MPI_Type_commit(&dtype);
    //    ferr << "MPICommunicationSession::convertRecordToDatatype Exit" << endl;
    return dtype;
}

void MPICommunicationSession::startNewSession(int newSessionId)
{
  // reset the numrecords
  _sessionId = newSessionId;
  _numSends = 0;
}

void MPICommunicationSession::initializeSendTx()
{
  MPI_Send((void *)&_sessionId, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _communicator);

  MPI_Send((void *)&_filter->_capacityInRecords, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _communicator);

  MPI_Send((void *)&_filter->_recordSize, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _communicator);

  // send recordType, a string
  // send record size in number of bytes
  // send buffer size in number of records
  // send sessionId
  string rt_serialized = _filter->_rtype->createHeaderForStream();
  cout << "Sending \n" << rt_serialized << "\n";
  int datatype_size = rt_serialized.size(); 

  // send it
  MPI_Send((void *)&datatype_size, 1, MPI_INT, 0, 
	   BIE_SYNCHRONIZE_TAG, _communicator);

  MPI_Send((void *)rt_serialized.data(), datatype_size, MPI_BYTE, 0, 
	   BIE_DATATYPE_TAG, _communicator);
}

void MPICommunicationSession::finishSendTx()
{
  // send EndOfSession Record
  // this record has all fields as 0 or the first field(TileId) has a marker (-1)

  int datatype_size = _filter->_recordSize;
  int *buf = new int[datatype_size];
  for (int i=0; i<datatype_size; i++)
    *((int *) buf+i) = BIE_ENDOFDATA_MARKER;

#ifdef DEBUG_TESSTOOL
  ferr << " MPICommunicationSession::about to send end of data record" << endl;
#endif

  // sending one record with end of stream markers
  MPI_Send((void *)buf, 1, _sendDtype, 0, BIE_DATA_TAG, _communicator);

#ifdef DEBUG_TESSTOOL
  ferr << " MPICommunicationSession::sent end of data record" << endl;
#endif
}

extern int myid;
void MPICommunicationSession::send()
{
  if(_numSends == 0){
    // only the root sends some of the stuff
    if(myid == 0) {
      initializeSendTx();
    }
  }

  // send current buffer
#ifdef DEBUG_TESSTOOL
  ferr << " MPICommunicationSession::send _numSends=" << _numSends 
       << " _filter->_numRecords=" << _filter->_numRecords << endl;
#endif

  MPI_Send((void *)_filter->_buffer, _filter->_numRecords, 
	   _sendDtype, 0, BIE_DATA_TAG, _communicator);
  
  _numSends++;
}
