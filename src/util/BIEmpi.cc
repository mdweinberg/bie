#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include <gvariable.h>
#include <BIEmpi.h>
#include <ConfigFileReader.h>

#ifdef DEBUG_TESSTOOL
ofstream ferr;
#endif

namespace BIE 
{
  MPI_Comm MPI_COMM_SLAVE;
  MPI_Comm CLI_COMM_WORLD;
  int numprocs=1, slaves=0, myid=0, proc_namelen;
  char processor_name[MPI_MAX_PROCESSOR_NAME];
}

void mpi_init(int argc, char** argv)
{
  MPI_Group world_group, slave_group;
  int n;
  
  // Scan argv[] for the special "-mpi/--mpi" option
  //
  bool launch_mpi = true;
  for (int i=1; i<argc; i++) { 
    if (strcmp(argv[i],  "-mpi")==0 ||
	strcmp(argv[i], "--mpi")==0) {
      launch_mpi = false;
      break;
    }
  }

  // Check environment for openmpi
  //
  if (launch_mpi and getenv("OMPI_COMM_WORLD_SIZE")==NULL) {

    // #args = argc + 4 ("mpirun -np 1" and "-mpi" added)
    char **args = (char **)calloc(argc + 4, sizeof(char *));

    // Arguments to add
    const char* nargs[] = {"mpirun", "-np", "1", "-mpi"};
    for (int i=0; i<3; i++) args[i] = strdup(nargs[i]);

    // Copy the entire argv to the rest of args but skip "-launchmpi"
    for (int i=0; i<argc; i++) args[i+3] = argv[i];

    // Add mpi switch
    args[argc+3] = strdup(nargs[3]);
    
    execvp("mpirun", args);
    
    return;
  }

  // MPI preliminaries
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  
  if (numprocs<=1) {		// Only one process, forget it!
    mpi_used = false;
    return;
  } else {			// At least one process . . .
    mpi_used = true;
  }
  
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Get_processor_name(processor_name, &proc_namelen);
  
  //
  // Make SLAVE group
  //
  slaves = numprocs - 1;
  MPI_Comm_group(MPI_COMM_WORLD, &world_group);
  int *nslaves = new int [slaves];
  
  for (n=1; n<numprocs; n++) nslaves[n-1] = n;
  MPI_Group_incl(world_group, slaves, nslaves, &slave_group);
  MPI_Comm_create(MPI_COMM_WORLD, slave_group, &MPI_COMM_SLAVE);
  delete [] nslaves;
  
  //
  // Debug id
  //
  MPI_Group_rank ( slave_group, &n );
  cerr << "Process " << myid << " on " << processor_name
       << "    rank in SLAVE: " << n << "\n";
  
  MPI_Comm_dup(MPI_COMM_WORLD, &CLI_COMM_WORLD);
  
  MPI_Barrier(MPI_COMM_WORLD);
  {
#ifdef DEBUG_TESSTOOL
    char scratch[80]={0};
    strcat(scratch, "errfile.");
    sprintf(scratch+8, "%d", myid);
    cerr << scratch << " " << myid << endl;
    ferr.open(scratch);
    ferr << "Starting on node " << myid << endl;
    ferr.flush();
#endif
  }
  
}

void mpi_slave_output(string ofile)
{
  if (myid) {
    
    bool ofile_add = true;

    if (ofile.size()==0) {
      //
      // Redirect slave output to file if specfied in the argument
      // above or in the .bierc config files, otherwise dump it to
      // /dev/null
      //
      string getOutput =
	ConfigFileReader("output").getValue("mpi_slave_cli", "/dev/null");
    
      if (getOutput.compare("cout")==0) return;

      ofile = getOutput;
      if (getOutput.compare("/dev/null")!=0) {
	if (nametag.size()) ofile += "." + nametag;
      } else {
	ofile_add = false;
      }
    }

    if (ofile.size()) {
      if (ofile_add) {
	ostringstream buf;
	buf << "." << myid;
	ofile += buf.str();
      }

      logFile.open(ofile.c_str());
      
      if (logFile) {		// Redefine std output and error to file
	BIE::outbuf = cout.rdbuf(logFile.rdbuf());
	BIE::errbuf = cerr.rdbuf(logFile.rdbuf());
	cout << "Using file <" << ofile << "> for Process " << myid 
	     << " output" << endl;
	cout.flush();
      } else {
	cout << "Can not open output log file <" << ofile << "> for slaves, "
	     << "defaulting to standard output" << endl;
	cout.flush();
      }
    }

  }
  
}

void mpi_quit()
{
  if (!mpi_used) return;	// Safety
  
  // EXPTCODE
  // cerr << "in mpi_quit in " << myid  << endl;
  if (myid) {
    // Restore the standard io buffers
    if (cout.rdbuf() != BIE::outbuf) {
      
      cout.rdbuf(BIE::outbuf);
      cerr.rdbuf(BIE::errbuf);
      
      logFile.close();
    }
  }
  
  MPI_Finalize();
  {
#ifdef DEBUG_TESSTOOL
    ferr.close();
    ferr.flush();
#endif
  }
}

