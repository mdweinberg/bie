#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#include <TwoNTree.h>
#include <ORBTree.h>
#include <KDTree.h>
#include <VTA.h>
#include <values.h>
#include <mpi.h>

using namespace std;

const double google = 230.25850929940458; // log(1.0e100)

double VTA::deltaP  = 20.0;
double VTA::dPmin   = 0.01;
int    VTA::TNTlev  = 6;
bool   VTA::useNNT  = false;
bool   VTA::useORB  = false;
bool   VTA::useTNT  = false;
bool   VTA::useMPI  = false;
bool   VTA::treMPI  = false;
int    VTA::fileCtr = 0;

std::vector<double> VTA::lowerBound;
std::vector<double> VTA::upperBound;

void VTA::setScl (int v)
{
  if (v>=0 || v<=3) {
    scale = v;
  } else {
    std::cout << "No such scaling method (" << scale 
	      << "), setting to the idenitity" << std::endl;
    scale = 0;
  }
}


void VTA::compute(int cut, int Lkts, bool logM, double trimV,
		  deque<element>::iterator first,
		  deque<element>::iterator last,
		  int mid)
{
  unsigned ndim = first->point.size();

  vector< vector<double> > pvalue;
  vector<double> points, weights, beta;
  vector<unsigned> mult;
  vector<double> meanpt(ndim, 0.0), widthpt(ndim, 0.0);

  // This is only used for toggling debug output
  int myid = 0, numprocs = 1;
  if (useMPI) {
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  }

  //--------------------------------------------------
  // Zero results
  //--------------------------------------------------

  for (unsigned l=0; l<2; l++) {
    for (int k=0; k<4; k++) {
      reslt[l][k] = 0.0;
      lower[l][k] = 0.0;
      upper[l][k] = 0.0;
      vmean[l][k] = 0.0;
    }
  
    Llower[l] = 0.0;
    Lupper[l] = 0.0;
    Lmean [l] = 0.0;
  
    Rlower[l] = 0.0;
    Rupper[l] = 0.0;
    Rmean [l] = 0.0;
  }

  //--------------------------------------------------
  // Set manual range bounds
  //--------------------------------------------------

  if (lowerBound.size() == ndim && upperBound.size() == ndim)
    BSPTree::SetRangeBounds(lowerBound, upperBound);

  //--------------------------------------------------
  // Scale for tree construction
  //--------------------------------------------------

  vector<double> pmax(3, -DBL_MAX);
  vector<double> lmax;
  for (deque<element>::iterator it=first; it!=last; it++) {
				// Point with maximum posterior probability
    if (pmax[0] < it->pval[0]) lmax = it->point;
				// Maximum for all probability values
    for (int k=0; k<3; k++) 
      pmax[k] = max<double>(pmax[k], it->pval[k]);
  }

  double pfac = exp(pmax[0]);


  //--------------------------------------------------
  // Check for unique states
  //--------------------------------------------------
  
  deque<element>::iterator it=first, itl=first;
  unsigned dup = 0, totl = 0;

  for (it++; it!=last; it++) {
    bool ok = false;

    // Not a duplicate if at least one dimension differs!
    if (uniq) {
      for (unsigned n=0; n<ndim; n++) {
	if (fabs(it->point[n] - itl->point[n]) > 1.0e-10) {
	  ok = true;
	  break;
	}
      }
      if (!ok) dup++;
    }
    
    if (!uniq || ok) {
      // Check for very low prob values
      if (it->pval[0] - pmax[0] > -google) {
	weights.push_back(it->weight);
	beta.push_back(it->beta);
	pvalue.push_back(it->pval);
	mult.push_back(dup+1);
	for (unsigned n=0; n<ndim; n++) {
	  meanpt [n] += it->point[n] * (dup + 1);
	  widthpt[n] += it->point[n] * it->point[n] * (dup + 1);
	  points.push_back(it->point[n]);
	}
	totl += dup + 1;
      }
      dup = 0;
    }
      
    if (uniq && ok) itl = it;
  }

  for (unsigned n=0; n<ndim; n++) {
    meanpt[n] /= totl;
    widthpt[n] = (widthpt[n] - meanpt[n]*meanpt[n]*totl)/(totl-1);
    widthpt[n] = sqrt(widthpt[n]);
  }

  if (debug && myid==0) {
    cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
    cout << "Mean and width computation" << endl;
    cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
    cout << left 
	 << setw(6)  << "Dim"
	 << setw(18) << "Mean"
	 << setw(18) << "Width"
	 << endl;
    for (unsigned n=0; n<ndim; n++) {
      cout << setw(6)  << n 
	   << setw(18) << meanpt[n] 
	   << setw(18) << widthpt[n]
	   << endl;
    }
    cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
  }

  ScaleFct *sc;
  switch (scale) {
  case 0:
    sc = new IdentScaleFct(meanpt, widthpt);
    break;
  case 1:
    sc = new PolyScaleFct (meanpt, widthpt, scexp);
    break;
  case 2:
    sc = new ExpScaleFct  (meanpt, widthpt);
    break;
  case 3:
    sc = new TransScaleFct(meanpt, widthpt, scexp);
    break;
  default:
    sc = new IdentScaleFct(meanpt, widthpt);
    break;
  }
  
  sc->Transform(pvalue, points, ndim);
  

  //-------------------------------------------------- 
  // Exponentiate scaled probabilities: 
  // probabilites are assumed to be passed in as log(P)
  // --------------------------------------------------
  
  for (vector< vector<double> >::iterator it=pvalue.begin();
       it != pvalue.end(); it++) {
    for (int k=0; k<3; k++) (*it)[k] = exp((*it)[k] - pmax[k]);
  }

  if (pvalue.size() == 0)
    throw string("Zero valid points: skipping VTA computation");


  BSPTree  *bsp = 0;
  TwoNTree *tnt = 0;
  ORBTree  *orb = 0;
  KDTree   *kdt = 0;


  //--------------------------------------------------
  // Catch any exceptions from the tree constructors
  //--------------------------------------------------
  
  try {
    if (useTNT)
      bsp = tnt = new TwoNTree(TNTlev, ndim, pvalue.size(), cut, 
			       &points[0], &weights[0], &beta[0]);
    else if (useORB)
      bsp = orb = new ORBTree(ndim, cut, pvalue, 
			      &points[0], &mult[0], &weights[0], &beta[0]);
    else
      bsp = kdt = new KDTree(ndim, weights.size(), cut, 
			     &points[0], &weights[0], &beta[0]);
  }
  catch (string err) {
    delete bsp;
    delete sc;
    throw err;
  }

  //--------------------------------------------------
  // Ok, continue with the remaining processes
  //--------------------------------------------------

  if (debug && myid==0) bsp->LevelList(cout, lims);
  
  if (trimV<1.0) bsp->SetVTrim(trimV);

  bsp->IntegralEval(pvalue, mult, reslt, lower, upper, vmean);

  bsp->LebesgueIntegral(Llower, Lmean, Lupper, Lkts, 0, cut, pvalue, mult,
			logM, deltaP, dPmin);
  
  bsp->LebesgueIntegral(Rlower, Rmean, Rupper, Lkts, 2, cut, pvalue, mult,
			logM, deltaP, dPmin);
  
  vfrac = bsp->GetVTrim();

  Xvolume = bsp->EnclosedVolume();
  for (unsigned l=0; l<2; l++) Yvolume[l] = bsp->LebesgueVolume(l);
  
  if (measr && tag.size()) {
    ostringstream file;
    file << tag << ".measure.";
    if (mid<0) {
      if (numprocs>1) file << myid << ".";
      file << fileCtr;
    } else {
      file << mid;
    }

    ofstream out(file.str().c_str());
    vector<double> P, M, Y, A, R;

    bsp->LebesgueMeasure(P, M, Y, R, A, Lkts, 0, cut, pvalue, mult,
			 1, logM, deltaP, dPmin);
    //                   ^
    //                   |
    // Use pooled values-/
    //

    if (out) {
      // Header
      out << "# Lebesgue measure" << endl
	  << "# " << setw(16) << right << " [1]"
	  << setw(18) << " [2]"
	  << setw(18) << " [3]"
	  << setw(18) << " [4]"
	  << setw(18) << " [5]"
	  << setw(18) << " [6]"
	  << setw(18) << " [7]"
	  << setw(18) << " [8]"
	  << setw(18) << " [9]"
	  << setw(18) << " [10]"
	  << endl
	  << "# " << setw(16) << right << "Prob"
	  << setw(18) << "Measure"
	  << setw(18) << "d(Measure)"
	  << setw(18) << "Integral"
	  << setw(18) << "Relative"
	  << setw(18) << "Absolute"
	  << setw(18) << "Cumulative"
	  << setw(18) << "Cum. abs."
	  << setw(18) << "Scaled"
	  << setw(18) << "Integral"
	  << endl
	  << "# " << setw(16) << right << "----"
	  << setw(18) << "-------"
	  << setw(18) << "--------"
	  << setw(18) << "-----------"
	  << setw(18) << "--------"
	  << setw(18) << "--------"
	  << setw(18) << "----------"
	  << setw(18) << "---------"
	  << setw(18) << "------"
	  << setw(18) << "--------"
	  << endl;
      
      double cumint=0.0, abserr=0.0, integral=0.0;
      for (unsigned i=0; i<P.size(); i++) {

	cumint += Y[i]*pfac;
	abserr += A[i]*pfac;
	
	if (i>0) integral += 0.5*(M[i] + M[i-1])*(P[i] - P[i-1]) * pfac;

	double dM, PP, MM;
	if (logM) {
	  PP = P[i] + pmax[0];
	  MM = dM = M[i]/exp(P[i]);
	  if (i<P.size()-1) dM -= M[i+1]/exp(P[i+1]);
	} else {
	  PP = log(P[i]) + pmax[0];
	  MM = dM = M[i];
	  if (i<P.size()-1) dM -= M[i+1];
	}

	out << setw(18) << PP
	    << setw(18) << MM
	    << setw(18) << dM
	    << setw(18) << Y[i]*pfac
	    << setw(18) << R[i]
	    << setw(18) << A[i]*pfac
	    << setw(18) << cumint
	    << setw(18) << abserr
	    << setw(18) << log(cumint)
	    << setw(18) << integral
	    << endl;
      }
    }

    std::vector<double> Pr;
    std::vector<std::pair<double, double> > Vm;
    std::map<int, double> histo;
    bsp->LebesgueHisto(Pr, Vm, histo, 0, cut, pvalue, mult, 0);

    file.str("");
    file << tag << ".histoV." << fileCtr;
    out.close();
    out.open(file.str().c_str());
    if (out) {
      double volsum = 0.0, vol;
      out << "#" << std::right << std::setw(7)  << "Number"
	  << std::setw(18) << "Volume"
	  << std::setw(18) << "Cum volume"
	  << std::endl;
      for (std::map<int, double>::iterator 
	     it=histo.begin(); it!=histo.end(); it++) {

	vol = it->second;
	volsum += vol;
	out << std::setw(8)  << it->first
	    << std::setw(18) << vol
	    << std::setw(18) << volsum
	    << std::endl;
      }
    } else {
      std::cerr << "Could not open <" << file.str() << "> for output"
		<< std::endl;
    }

    file.str("");
    file << tag << ".histoP." << fileCtr;
    out.close();
    out.open(file.str().c_str());
    if (out) {
      out << "#" << std::right << std::setw(7)  << "Prob"
	  << std::setw(18) << "Number"
	  << std::setw(18) << "Volume"
	  << std::setw(18) << "Cum volume"
	  << std::endl;
      double volsum = 0.0;
      for (unsigned n=0; n<Pr.size(); n++) {
	volsum += Vm[n].first;
	out << std::setw(8)  << Pr[n] + pmax[0]
	    << std::setw(18) << Vm[n].second
	    << std::setw(18) << Vm[n].first
	    << std::setw(18) << volsum
	    << std::endl;
      }
    } else {
      std::cerr << "Could not open <" << file.str() << "> for output"
		<< std::endl;
    }

  }

  if (kdt && debug && myid==0) {
    cout << "Range:" << endl
	 << "----- " << endl << left
	 << setw(4)  << "d" 
	 << setw(18) << "point range" 
	 << setw(18) << "cell  range" 
	 << endl << setfill('-')
	 << setw(4)  << "+" 
	 << setw(18) << "+"
	 << setw(18) << "+"
	 << endl << setfill(' ');
    const double *r = kdt->range(0), *R = kdt->Range(0);
    for (unsigned i=0; i<kdt->Ndim(); i++)
      cout << left << setw(4) << i 
	   << setw(18) << r[i]
	   << setw(18) << R[i]
	   << endl;
    cout << setw(70) << setfill('-') << '-' << endl << setfill(' ');
  }
  
  if (kdt && tag.size() && myid==0) {
    if (kdt->Ndim()==2) {
      string file = tag + ".mgrid";
      ofstream output(file.c_str());
      const unsigned grid = 200;
      const double *lo = kdt->Low(0), *hi = kdt->High(0);
      double dx = (hi[0] - lo[0])/grid;
      double dy = (hi[1] - lo[1])/grid;
      vector<double> T(2);
      for (unsigned i=0; i<grid; i++) {
	T[0] = lo[0] + dx*(0.5+i);
	for (unsigned j=0; j<grid; j++) {
	  T[1] = lo[1] + dy*(0.5+j);
	  
	  output << setw(18) << T[0] 
		 << setw(18) << T[1] 
		 << setw(18) << kdt->FTheta(T)
		 << setw(18) << kdt->PosteriorProb(T)*pfac
		 << endl;
	}
	output << endl;
      }
    }
  }

  //--------------------------------------------------
  // Diagnostics and free allocations
  //--------------------------------------------------

  if ( myid==0 && ndim>=2) 
    bsp->gnuplotDemo(pvalue, mult, lmax, 0, cut);

  delete bsp;
  delete sc;

  incrementCounter();

  //--------------------------------------------------
  // Descale probabilities
  //--------------------------------------------------

  for (int l=0; l<2; l++) {
    for (int k=0; k<3; k++) {
      reslt[l][k] = log(reslt[l][k]) + pmax[k];
      lower[l][k] = log(lower[l][k]) + pmax[k];
      upper[l][k] = log(upper[l][k]) + pmax[k];
      vmean[l][k] = log(vmean[l][k]) + pmax[k];
    }
  
    Llower[l] = log(Llower[l]) + pmax[0];
    Lupper[l] = log(Lupper[l]) + pmax[0];
    Lmean [l] = log(Lmean [l]) + pmax[0];
    
    Rlower[l] = log(Rlower[l]) + pmax[2];
    Rupper[l] = log(Rupper[l]) + pmax[2];
    Rmean [l] = log(Rmean [l]) + pmax[2];
  }
}

