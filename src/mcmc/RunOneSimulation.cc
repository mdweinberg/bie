#include <cmath>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <typeinfo>

using namespace std;

#include <BIEdebug.h>
#include <BIEException.h>
#include <BIEMutex.h>
#include <RunOneSimulation.h>
#include <gfunction.h>
#include <CLICheckpointManager.h>
#include <LikelihoodComputationMPI.h>
#include <LikelihoodComputationMPITP.h>
#include <Timer.h>
#include <TessToolSender.h>
#include <MHWidthOne.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RunOneSimulation)

using namespace BIE;


//
// Constructor, the user can set all variables and pass object pointers.
//

RunOneSimulation::RunOneSimulation
(
 int nsteps, double wscale,
 Ensemble *sstat,  Prior *prior,  Simulation *sim
 )
{
  initial();

  this->nsteps = nsteps;	// Maximum number of steps
  
				// Width for weights and parameters
  Prior::width_factor = wscale;
  
  this->sstat  = sstat;		// Ensemble instance for distribution of states
  this->prior  = prior;		// Prior distribution
  this->sim    = sim;		// Simulation instance
  
  ttfreq       = 0;
  resumelog    = false;
  enslaved     = false;
  ordered      = false;
  clev         = current_level;
}


void RunOneSimulation::initial()
{
  ostream cout(checkTable("simulation_output"));
  
  //
  // Get Node 0 working directory
  //
  const int hdbufsize=1024;
  vector<char> hdbuffer(hdbufsize, 0);
  char *hdbuf = &hdbuffer[0];
  
  if (!mpi_used || myid == 0) {
    hdbuf = getcwd(hdbuf, (size_t)hdbufsize);
  }
  
  if (mpi_used) {
    ++MPIMutex;
    MPI_Bcast(hdbuf, hdbufsize, MPI_CHAR, 0, MPI_COMM_WORLD);
    --MPIMutex;
  }
  
  homedir = hdbuf;
  homedir += "/";
  if (myid) {
    int ret = chdir(hdbuf);
    if (ret==-1) cout << "Process " << myid << ": error changing directory\n";
  }
  cout << "Process " << myid << ": homedir=" << homedir << endl;
  hdbuf = getcwd(hdbuf, (size_t)hdbufsize);
  cout << "Process " << myid << ": cwd=" << hdbuf << endl;

  // Prefix root directory
  //
  cout << "Process "    << myid 
       << "   outfile=" << outfile
       << endl;
}

void RunOneSimulation::Run(void)
{
  simulationInProgress = 1;
  ostream cout(checkTable("simulation_output"));
  Timer timer;
  
  try {
    
    // wrap the whole thing in a try block because
    // exceptions don't propogate out of threads.
    
    // Diagnostic: print number of particles on and off grid
    //
    if (!mpi_used || myid==0)
      cout << "---- Tessellation diagnostic" << endl
	   << "      Number of data points on grid:  " 
	   << sim->GetDataTree()->Total() << endl
	   << "      Number of data points off grid: "
	   << sim->GetDataTree()->Offgrid() << endl 
	   << "---- Done" << endl 
	   << flush;
    
    // Get a state
    //
    State s = sim->GetFromPrior();
    
    // Get the frontier from the simulation
    //
    Frontier * frontier = sim->currentFrontier();
    
    if (!mpi_used || myid==0) {
      printFrontierTiles(frontier);
    }
    
    // Put the all nodes *but* Node 0 into slave mode
    //
    if (mpi_used && myid) {
      cout << "Process " << myid << ": enslaved\n";
      sim->GetLike()->startThread();
      enslaved = true;
    }
    
    // Set the initial state for the first time only
    //
    try {
      sim->NewState(s);
#ifdef DEBUG
      sim->AdditionalInfo();
#endif
    }
    catch (ImpossibleStateException e) {
      cout << "Process " << myid << ": screwy state, continuing anyway"
	   << endl;
    }
    
    double value = sim->GetValue();
    
    if (myid==0) {
      StateInfo *si = sim->SI();
      StateInfo::Pattern ptype = si->ptype;
      if (ptype == StateInfo::None  ||
	  ptype == StateInfo::Block ||
	  ptype == StateInfo::RJTwo )
	cout << "**Initial Ntot=" << si->Ntot;
      if (ptype == StateInfo::Mixture ||
	  ptype == StateInfo::Extended ) {
	cout << "**Initial M=" << si->M << ", Ndim=" << si->Ndim;
	if (ptype == StateInfo::Extended)
	  cout << ", Next=" << si->Next;
      }
      cout << ", value=" << value << endl;
      sim->PrintState();
    }

    // Now do the work
    //
    steps = 0;
    Resume();
  } 
  catch (BIEException &e){
    cout << e.getErrorMessage() << endl;
  }
  
  simulationInProgress = 0;
}

void RunOneSimulation::Resume() 
{
  // Put the all nodes *but* Node 0 into slave mode
  // 
  if (mpi_used && myid && !enslaved) {
    cout << "Process " << myid << ": enslaved\n";
    sim->GetLike()->startThread();
    enslaved = true;
  }
    
  ostream cout(checkTable("simulation_output"));
  Timer timer;

  resumeLog();

  StateInfo::Pattern ptype = sim->SI()->ptype;

  try {

    for (; steps<nsteps; steps++) {

      if (myid==0) {
	likelihood_count = 0;
	timer.reset();
	timer.start();
      }
    
      //
      // Do we want to take a checkpoint?
      //
      if (ckm) {
	ckm->checkpoint(cout);
      }

      //
      // DO THE WORK!
      // (Attempt to provide some modicum of a traceback on failure)
      //
      try {
	sim->OneStep();
      }
      catch (BIEException& e) {
	cout << "Process " << myid
	     << ": in RunOneSimulation::Run() on calling sim->OneStep(): "
	     << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	     << e.getErrorMessage();
	//
	// Rethrow . . .
	//
	throw e;
      }

#ifdef DEBUG
      sim->AdditionalInfo();
#endif
      
      if (myid==0) {
      
	timer.stop();
      
	double value = sim->GetValue();
	State s = sim->GetState();
	
	double mean, stdev;
	sstat->stats(10, mean, stdev);
	
	double dtime = timer.getTime().getRealTime();
	
	if (timer.Precision()) dtime *= 1.0e-6;
	if (dtime>1000.0) timer.Seconds();
	if (dtime<100.0)  timer.Microseconds();
	
	double lktime = dtime/likelihood_count;
	
	// Print diagnostics to screen
	//
	std::cout << std::setfill('-') << std::setw(80) << '-' 
		  << std::setfill(' ') << std::endl;
	std::cout << "Step " << std::setw(4) << steps
		  << " :  value=" << std::setw(20) << std::setprecision(12) << value 
		  << "  mean="  << std::setprecision(6) << std::setw(12) << mean 
		  << "  stdev=" << std::setprecision(6) << std::setw(12) << stdev 
		  << "  count=" << std::setw(9) << sstat->Nstates() << std::endl;
      
	sim->PrintStepDiagnostic();  // simulation type specific diagnostic
      
	cout << "           time=" << dtime
	     << "  per eval=" << lktime
	     << "  #evals=" << likelihood_count;

	if (ptype == StateInfo::Mixture || ptype == StateInfo::Extended)
	  cout << "  #comps=" << sim->SI()->M;

	cout <<  endl;
      
	// Simulation specific; mixing stats for temperedsim
	//
	sim->PrintStateDiagnostic();  
	
	// Base-level output
	//
	sim->ReportState();
	
	cout << endl;
      
	// Tess Tool output
	//
	if (ttfreq) {
	  if (steps % ttfreq == 0) {
	    tesstool->SetSessionId(BIE::current_level, steps);
	    sim->GetLike()->SampleNext();
	  }
	}
      
	// Logfile
	//
	if (logfile) sim->LogState(BIE::current_level, steps, outfile);
      }
    
      sim->ResetDiagnostics();

      // Control parsing
      //
      int brk = 0;
      if (myid==0) {
	if (sim->Convergence()) brk = 1; // Test convergence
      }
    
      // Send control message to all processes
      //
      if (mpi_used) {
	++MPIMutex;
	MPI_Bcast(&brk, 1, MPI_INT, 0, MPI_COMM_WORLD);
	--MPIMutex;
      }	  
      if (brk) break;
    
      // Did user flag a stop?
      //
      if (finish()) break;
    
    }  // end for each step
  
  }
  catch (BIEException& e) {
    // Try to exit cleanly on a BIE exception by stopping any captive
    // processes and returning to the caller.  Diagnostic error
    // messages should already have been printed to console.
  }


  // Free up the captive processes and signal an exit
  //
  sim->GetLike()->free_workers();
  
  // Stop the worker threads
  //
  if (mpi_used && myid) {
    sim->GetLike()->stopThread();
    enslaved = false;
  }
  
  // All done
  //
}

void RunOneSimulation::SuspendSimulation()
{
  sim->GetLike()->SuspendSimulation();
}

void RunOneSimulation::ResumeSimulation()
{
  sim->GetLike()->ResumeSimulation();
}

void RunOneSimulation::SetAutoTessTool(int freq)
{
  ttfreq = freq;
  tesstool = new TessToolSender(0);
  sim->GetLike()->SetTessTool(tesstool);
}

void RunOneSimulation::SetTessTool(TessToolSender *tt) 
{
  tesstool = tt;
  sim->GetLike()->SetTessTool(tt);
}


void RunOneSimulation::SampleNext(void)
{
  sim->GetLike()->SampleNext();
}

void RunOneSimulation::SwitchOnCLI()
{
  sim->GetLike()->SwitchOnCLI();
}


void RunOneSimulation::resumeLog()
{
  if (!resumelog) return;
  resumelog = false;
  if (myid!=0)    return;

  //
  // Attempt to open file to check for existence
  //
  ifstream in(outfile.c_str());
  in.close();
  if (in.fail()) return;

  //
  // Backup the original file and make sure 
  // the move is successful
  //
  string backup = outfile  + ".bak";
  if (rename(outfile.c_str(), backup.c_str())) {
    ostringstream msg;
    msg << "Could not make backup file <"
	 << backup << "> from original file <" << outfile << ">";
    throw ResumeLogException(msg.str(), __FILE__, __LINE__);
  }
  
  //
  // Read from the backup, rewrite the rolled
  // back state log
  //
  ifstream fin(backup.c_str());
  ofstream fot(outfile.c_str());

  if (fin && fot) {

    const unsigned inbufsize = 32768;
    char inbuf[inbufsize];
    istringstream sin;

				// Remember: iostream::getline discards "\n"
    fin.getline(inbuf, inbufsize);
    fot << inbuf << endl;	// This should be the header line
    
				// Read lines until empty or done
    int lev1, itr1;
    while (fin.getline(inbuf, inbufsize)) {

      if (fin.fail()) {
	ostringstream msg;
	msg << "Error or end reading backup file"
	    << " before desired level [" << current_level 
	    << "] and state [" << steps << "] was found";
	throw ResumeLogException(msg.str(), __FILE__, __LINE__);
      }

      sin.str(inbuf);		// Set the string stream to the input buffer
      sin >> lev1;		// Get the current level
      sin >> itr1;		// Get the current iteration

      if (lev1 >= current_level && itr1 >= steps) break;
      
				// Otherwise append line to file
      fot << inbuf << endl;
    }
  } else {
    ostringstream msg;
    msg << "Error opening input and/or output files";
    if (rename(backup.c_str(), outfile.c_str()))
      msg << " and could not restore original state file";
    throw ResumeLogException(msg.str(), __FILE__, __LINE__);
  }

}
