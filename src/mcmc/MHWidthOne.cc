#include <iostream>
#include <iomanip>
#include <vector>

#include <BIEmpi.h>
#include <gvariable.h>
#include <MHWidthOne.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MHWidthOne)

using namespace std;
using namespace BIE;

MHWidthOne::MHWidthOne(StateInfo *si, clivectord* W) : MHWidth(si), w((*W)()) 
{
  unsigned sz = w.size();
  if (si->ptype == StateInfo::None  || 
      si->ptype == StateInfo::Block || 
      si->ptype == StateInfo::RJTwo) {
    if (sz != si->Ntot) {
      ostringstream ostr;
      ostr << "MHWidthOne: the StateInfo size for the state is "
	   <<  _si->Ntot << " but the supplied width vector has the the size "
	   << sz << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (sz != 1+si->Ndim) {
      ostringstream ostr;
      ostr << "MHWidthOne: the StateInfo size for the parameter vector is "
	   << _si->Ndim << " and one needs a vector of size "
	   << _si->Ndim+1 << " (weight + parameter widths for each component)"
	   << " but the supplied width vector has the the size " << sz << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }
#ifdef DEBUG
  if (mpi_used)
    cout << "Process " << myid << ": widths ";
  else
    cout << "Widths ";
  for (unsigned i=0; i<w.size(); i++) cout << setw(18) << w[i];
  cout << endl;
#endif
}


MHWidthOne::MHWidthOne(StateInfo *si, vector<double>* W) : MHWidth(si), w(*W) 
{
  unsigned sz = w.size();
  if (si->ptype == StateInfo::None  || 
      si->ptype == StateInfo::Block || 
      si->ptype == StateInfo::RJTwo) {
    if (sz != si->Ntot) {
      ostringstream ostr;
      ostr << "MHWidthOne: the StateInfo size for the state is "
	   <<  _si->Ntot << " but the supplied width vector has the the size "
	   << sz << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (sz != 1+si->Ndim) {
      ostringstream ostr;
      ostr << "MHWidthOne: the StateInfo size for the parameter vector is "
	   << _si->Ndim << " and one needs a vector of size "
	   << _si->Ndim+1 << " (weight + parameter widths for each component)"
	   << " but the supplied width vector has the the size " << sz << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }
#ifdef DEBUG
  if (mpi_used)
    cout << "Process " << myid << ": widths ";
  else
    cout << "Widths ";
  for (unsigned i=0; i<w.size(); i++) cout << setw(18) << w[i];
  cout << endl;
#endif
}


MHWidthOne::MHWidthOne(StateInfo *si, string file) : MHWidth(si)
{
  ifstream metf(file.c_str());
  if (!metf) {
    throw FileNotExistException("MHWidthOne: ", file, __FILE__, __LINE__);
  }

  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo) {

  } else {

    w = vector<double>(_si->Ndim+1+_si->Next);
    for (unsigned i=0; i<_si->Ndim+1; i++) {
      metf >> w[i];
      if (!metf) {
	throw MetropolisHastingsException(file, __FILE__, __LINE__);
      }
    }
    for (unsigned i=0; i<_si->Next; i++) {
      metf >> w[i];
      if (!metf) {
	throw MetropolisHastingsException(file, __FILE__, __LINE__);
      }
    }
  }
    
  
#ifdef DEBUG
  if (mpi_used)
    cout << "Process " << myid << ": widths ";
  else
    cout << "Widths ";
  for (unsigned i=0; i<w.size(); i++) cout << setw(18) << w[i];
  cout << endl;
#endif
}

double MHWidthOne::width(int j)
{
  switch (_si->ptype) {
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    if (j>=0 && j<static_cast<int>(_si->Ntot))
      return w[j];
    else {
      ostringstream ostr;
      ostr << "MHWidthOne::width: requested index " << j 
	   << " is larger than the StateInfo size of " << _si->Ntot
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  case StateInfo::Extended:
    if (j>=0 && j<static_cast<int>(_si->Next))
      return w[(1+_si->Ndim) + j];
    else {
      ostringstream ostr;
      ostr << "MHWidthOne::width: requested index " << j 
	   << " is larger than the StateInfo size of " << _si->Next
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  default:
    throw BIEException("MHWidthOne::width: Inconsistent request",
		       "This state is a pure mixture",
		       __FILE__, __LINE__);
  }

  return 0.0;
}

double MHWidthOne::widthM(int M)
{
  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo) {
    throw BIEException("MHWidthOne::widthM: Inconsistent request",
		       "This state is not a mixture!",
		       __FILE__, __LINE__);
  }

  if (M<0 || M>static_cast<int>(_si->M)) {
    ostringstream ostr;
    ostr << "MHWidthOne::widthM: requested mixture component " << M 
	 << " which is larger than the StateInfo size of " << _si->M
	 << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  
  return w[0];
}
double MHWidthOne::widthM(int M, int j)
{
  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block  ) 
    {
      throw BIEException("MHWidthOne::widthM: Inconsistent request",
			 "This state is not a mixture!",
			 __FILE__, __LINE__);
    }

  if (M<0 || M>static_cast<int>(_si->M)) {
    ostringstream ostr;
    ostr << "MHWidthOne::widthM: requested mixture component " << M 
	 << " which is larger than the StateInfo size of " << _si->M
	 << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  
  if (j<0 || j>static_cast<int>(_si->Ndim)) {
    ostringstream ostr;
    ostr << "MHWidthOne::widthM: requested mixture element " << j
	 << " in subspace " << M
	 << " which is larger than the StateInfo size of " << _si->Ndim
	 << endl;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  
  return w[j];
}
