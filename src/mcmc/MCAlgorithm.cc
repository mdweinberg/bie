#include <MCAlgorithm.h>
#include <gvariable.h>
#include <Simulation.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MCAlgorithm)

using namespace BIE;

MCAlgorithm::MCAlgorithm()
{
  // Algorithm name (for diagnostic output)
  //
  algoname = "Monte Carlo Algorithm";

  // Random number generator (uniform)
  //
  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
}

