#include <typeinfo>
#include <sstream>

using namespace std;

#include <LikelihoodComputation.h>
#include <MetropolisHastings.h>
#include <ReversibleJump.h>
#include <MixturePrior.h>
#include <BIEconfig.h>
#include <BIEMutex.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::ReversibleJump)

using namespace BIE;

ReversibleJump::ReversibleJump(int mmax, double lambda) : MCAlgorithm()
{
  _mrate  = 0.1;
  _create = 0.5;
  _birth  = 0.5;
  _split  = 0.5;
  _delta  = 0.1;

  _level  = -1;
  _nbirth = 0;
  _nsplit = 0;

  _addspl = false;
  _debug  = false;

  Mmax    = mmax;		// Maximum mixture cardinality
  Lambda  = lambda;		// Poisson prior
  
  double cum = 0.0;
  PoisDif    = vector<double>(Mmax);
  PoisCum    = vector<double>(Mmax);
  for (int k=0; k<Mmax; k++) {
    PoisDif[k] = exp(-Lambda-lgamma(2.0+k))*pow(Lambda, 1.0+k);
    cum += PoisDif[k];
    PoisCum[k] = cum;
  }
  for (int k=0; k<Mmax; k++) {
    PoisDif[k] /= cum;
    PoisCum[k] /= cum;
  }

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));

  _moves = true;
}

// Table lookup

int ReversibleJump::GenPoisson()
{
  double R = (*unit)();

  int lo=0, hi=Mmax-1, cur;
  while (hi - lo > 1) 
    {
      cur = (lo + hi)/2;

      if (PoisCum[cur] < R) lo = cur;
      else                  hi = cur;
    }
  
  return lo+1;
}

void ReversibleJump::GetNewState(Simulation* s, Chain* ch)
{
				// Select number of components
  ch->Cur()->Reset(GenPoisson());
				// Initialize state
  s->_prior->SamplePrior(ch->Cur());
  ch->Order0();
  ComputeNewState(s, ch);
}


void ReversibleJump::ComputeNewState(Simulation* s, Chain* ch)
{
  if (s->Serial()) {

    ch->prior_value = s->PriorValue(ch->GetState0());
    ch->like_value  = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value       = (ch->like_value + ch->prior_value)*ch->beta;

  } else {
    
    int bad;
    if (myid==0) {
      try {
	ch->prior_value = s->PriorValue(ch->GetState0());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
    }

    MPI_Bcast(&bad, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);
    
    ch->like_value = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value = (ch->like_value + ch->prior_value)*ch->beta;
  }

}



double ReversibleJump::ComputeCurrentState(Simulation* s, Chain* ch)
{
  if (s->Serial()) {

    ch->prior_value = s->PriorValue(ch->GetState0());
    ch->like_value  = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value       = (ch->like_value + ch->prior_value)*ch->beta;

  } else {
    
    int bad;
    if (myid==0) {
      try {
	ch->prior_value = s->PriorValue(ch->GetState0());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
    }

    MPI_Bcast(&bad, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);
    
    
    ch->like_value = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value = (ch->like_value + ch->prior_value)*ch->beta;
  }

  if (myid==0 || s->Serial())
    return ch->value;
  else
    return 0.0;
}



double ReversibleJump::ComputeState(Simulation* s, Chain* ch)
{

  if (s->Serial()) {

    ch->prior_value1 = s->PriorValue(ch->GetState1());
    ch->like_value1  = s->Likelihood(ch->GetState1(), ch->indx);
    ch->value1       = (ch->like_value1 + ch->prior_value1)*ch->beta;

  } else {
    
    int bad;
    if (myid==0) {
      try {
	ch->prior_value1 = s->PriorValue(ch->GetState1());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
    }



    MPI_Bcast(&bad, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);

    ch->like_value1 = s->Likelihood(ch->GetState1(), ch->indx);
    ch->value1      = (ch->like_value1 + ch->prior_value1)*ch->beta;
  }

  if (myid==0 || s->Serial())
    return ch->value1;
  else
    return 0.0;
}


bool ReversibleJump::Sweep(Simulation* s, Chain* ch)
{
  //
  // Use RTTI to enforce mixture model
  //
  MixturePrior *_mix;
  try {
    _mix = dynamic_cast<MixturePrior*>(s->_prior);
  }
  catch (const std::bad_cast& e) {
    std::cerr << e.what() << std::endl;
    string msg(e.what());
    msg += ": ReversibleJump is implemented for mixtures only";
    throw PriorException(msg.c_str(), __FILE__, __LINE__);
  }

  //
  // New level?
  //
  if (_level != current_level) {
    _level  = current_level;
    _nbirth = 0;
    _nsplit = 0;
  }

  bool accepted = false;

  //
  // A simple Metropolis-Hastings step . . .
  //

  ComputeState(s, ch);

  if (myid==0 || s->Serial()) {

    double accept_prob = min<double>(1.0, exp(ch->value1 - ch->value));

    ch->stat[0]++;

    if (accept_prob >= (*unit)()) {
      ch->AcceptState();
      ch->stat[1]++;
      accepted = true;
    }
  }

  if (mpi_used && s->Parallel()) {
    ch->BroadcastChain();
  }

  //
  // Perform a transdimensional move?
  //

  if (_moves && genRate(_mrate, s)) {

    // Copy the fiducial to trial state
    ch->AssignState1();

    if (genRate(_create, s)) {
      // 
      // Generate (or a kill) a component
      //
      _nbirth++;
      if (genRate(_birth, s))
	accepted = BirthStep(s, ch, _mix);
      else
	accepted = DeathStep(s, ch, _mix);

    } else {
      // 
      // Split (or combine) a component
      //
      _nsplit++;
      if (genRate(_split, s))
	accepted = SplitStep(s, ch, _mix);
      else
	accepted = UniteStep(s, ch, _mix);
      
    }
  }

  //
  // Sort by weight values if no other order is chosen
  //

  if (ch->Cur()->Type() == StateInfo::Mixture ||
      ch->Cur()->Type() == StateInfo::Extended ) {
    
    if (ch->Cur()->getOrdering() == static_cast<unsigned>(-1))
      ch->Order0(0);		// Ordering is not currently set
    else 
      ch->Order0();		// Ordering is already set
  }
  

  return accepted;
}


void ReversibleJump::PrintAlgorithmDiagnostics(Simulation* s)
{
  vector<int> nb, nd, ns, nu;
  s->GetStateDiag(nb, nd, ns, nu);

				// Get max number of digits
  int imax = *max_element(nb.begin(), nb.end());
  imax = max<int>(imax, *max_element(nd.begin(), nd.end()));
  imax = max<int>(imax, *max_element(ns.begin(), ns.end()));
  imax = max<int>(imax, *max_element(nu.begin(), nu.end()));

  if (imax>0) imax = static_cast<int>(floor(log(0.1+imax)/log(10.0))) + 2;
  else        imax = 2;

  int jmax = max<int>(_nbirth, _nsplit);
  jmax = static_cast<int>(floor(log(0.1+jmax)/log(10.0))) + 2;
  jmax = max<int>(4, jmax);

  cout << setw(46+6*imax+jmax) << setfill('-') << '-' << setfill(' ') << endl;

  cout << right << setw(imax+1) << "RJ" << " " << setw(jmax) << "#"
       << " |" << setw(16+2*imax) << "Current step ---->" << "|"
       << setw(23+3*imax) << left << "<---- Total for all steps" << endl;

  cout << setw(46+6*imax+jmax) << setfill('-') << '-' << setfill(' ') << endl;

  cout << right << setw(imax+1) << nb[1] + nd[1] 
       << " " << setw(jmax) << _nbirth << " |"
       << "   born="   << setw(imax)    << nb[0]
       << "   died="   << setw(imax)    << nd[0] << "|"
       << "   born="   << setw(imax)    << nb[1]
       << "   died="   << setw(imax)    << nd[1]
       << "   net="    << setw(imax+1)  << nb[1] - nd[1]
       << endl << left;

  cout << right << setw(imax+1) << ns[1] + nu[1] 
       << " " << setw(jmax) << _nsplit << " |"
       << "  split="   << setw(imax)    << ns[0]
       << "  unite="   << setw(imax)    << nu[0] << "|"
       << "  split="   << setw(imax)    << ns[1]
       << "  unite="   << setw(imax)    << nu[1]
       << "   net="    << setw(imax+1)  << ns[1] - nu[1]
       << endl << left;

  cout << setw(46+6*imax+jmax) << setfill('-') << '-' << setfill(' ') << endl;
}

//
// Random boolean variate which is true with probability min(rate, 1)
//
bool ReversibleJump::genRate(double rate, Simulation *s)
{
  bool ret = ((*unit)()<rate) ? true : false;

  if (mpi_used && s->Parallel()) {
    unsigned char ok = ret ? 1 : 0;
    ++MPIMutex;
    MPI_Bcast(&ok, 1, MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);
    --MPIMutex;
    ret = ok ? true : false;
  }

  return ret;
}


bool ReversibleJump::BirthStep(Simulation* s, Chain* ch, MixturePrior *mix)
{
  double nw=0, nwP=0;
  unsigned ncomp=0;
  bool accepted = false;
  
  vector<double> ph;
  unsigned M    = ch->M0();
  unsigned Ndim = ch->Cur()->Ndim();
  
  // Communicate the mixture cardinality
  //
  if (mpi_used && s->Parallel()) {
    ++MPIMutex;
    MPI_Bcast(&M, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    --MPIMutex;
  }
  
  // Check to see if a space with one more component is allowed
  //
  if (mix->Exists(M+1)) {
    
    if (myid==0 || s->Serial()) {
      
      // Propose a component to select
      //
      ncomp = min<int>((int)floor((*unit)()*M+1), M);
      
      // Realize a new component from the prior
      //
      mix->SamplePrior(M+1, ncomp, ph);
      
      // Get the new weight and prior prob
      //
      nw  = mix->BirthWeight(M);
      nwP = mix->BirthWeightPDF(M, nw);
      
      // Append the new component state
      //
      ch->Tri()->Reset(M+1);
      for (unsigned m=0; m<M; m++) ch->Wght1(m) = ch->Wght0(m);
      
      ch->Wght1(M) = nw;
      for (unsigned m=0; m<M; m++) ch->Wght1(m) *= 1.0 - nw;
      for (unsigned m=0; m<M; m++) {
	for (unsigned j=0; j<Ndim; j++) 
	  ch->Phi1(m, j) = ch->Phi0(m, j);
      }
      for (unsigned j=0; j<Ndim; j++) ch->Phi1(M, j) = ph[j];
      
      // Renormalize weights
      //
      double norm = 0.0;
      for (unsigned i=0; i<M+1; i++) norm += ch->Wght1(i);
      for (unsigned i=0; i<M+1; i++) ch->Wght1(i) /= norm;
      
      // Update order
      //
      ch->Order1();
    }
    
    if (mpi_used && s->Parallel()) {
      ch->BroadcastChain();
    }
    
    // Compute the state and acceptance
    //
    try {
      
      ComputeState(s, ch);
      
      if (myid==0 || s->Serial()) {
	
	double Pcomp = log(1.0 - nw)*ch->M0() - log(nwP);
	double Rcomp = mix->logPDFMarginal(ch->M1(), ncomp, ph);
	double A     = exp( ch->value1 - ch->value + Pcomp - Rcomp ) *
	  (1.0 - _birth)/(_birth*ch->M1()) * 
	  PoisDif[ch->M1()-1]/PoisDif[ch->M0()-1];
	
	double accept_prob = min<double>(1.0, A);
	
	if (myid==0 && _debug) {
	  ofstream tout("birth.dat", ios::app);
	  if (tout) {
	    tout << setw(3)  << current_level 
		 << setw(15) << accept_prob
		 << setw(15) << ch->value1 - ch->value + Pcomp - Rcomp
		 << setw(15) << ch->value1 - ch->value
		 << setw(15) << Rcomp
		 << setw(15) << Pcomp
		 << endl;
	  }
	}
	
	if (accept_prob >= (*unit)()) {
	  ch->born[2]++;
	  ch->born[3]++;
	  
	  ch->AcceptState();
	  accepted = true;
	}
      }
      
    }
    catch (ImpossibleStateException &e) {
      // Ok, move on . . . 
    }
  }
  
  return accepted;
}


bool ReversibleJump::DeathStep(Simulation* s, Chain* ch, MixturePrior *mix)
{
  double nw=0, nwP=0;
  unsigned ncomp=0;
  bool accepted = false;
  
  State* f      = ch->Cur(); // For notational convenience . . .
  unsigned M    = ch->M0();
  unsigned Ndim = f->Ndim();
  
  // Communicate the mixture cardinality
  //
  if (mpi_used && s->Parallel()) {
    ++MPIMutex;
    MPI_Bcast(&M, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    --MPIMutex;
  }
  
  // Check to see if a space with one fewer component is allowed
  //
  if (mix->Exists(M-1)) {
    
    if (myid==0 || s->Serial()) {
      
      // Propose a component to kill
      //
      ncomp = min<int>((int)floor((*unit)()*M), M-1);
      
      ch->Tri()->Reset(M-1);
      
      // Copy the old components, dropping index <ncomp>
      //
      for (unsigned i=0; i<ncomp; i++) {
	ch->Wght1(i) = f->Wght(i);
	for (unsigned j=0; j<Ndim; j++)
	  ch->Phi1(i, j) = f->Phi(i, j);
      }
      
      for (unsigned i=ncomp+1; i<M; i++) {
	ch->Wght1(i-1) = f->Wght(i);
	for (unsigned j=0; j<Ndim; j++)
	  ch->Phi1(i-1, j) = f->Phi(i, j);
      }
      
      // Renormalize weights
      //
      double norm = 0.0;
      for (unsigned i=0; i<ch->M1(); i++) norm += ch->Wght1(i);
      for (unsigned i=0; i<ch->M1(); i++) ch->Wght1(i) /= norm;
      
      nw  = ch->Wght0(ncomp);
      nwP = mix->BirthWeightPDF(ch->M0(), nw);
      
      // Update order
      //
      ch->Order1();
    }
    
    if (mpi_used && s->Parallel()) {
      ch->BroadcastChain();
    }
    
    //
    // Compute the state and acceptance
    //
    try {
      
      ComputeState(s, ch);
      
      if (myid==0 || s->Serial()) {
	
	double Pcomp = log(1.0 - nw)*ch->M1() - log(nwP);
	double Rcomp = mix->logPDFMarginal(ch->M0(), ncomp, f->Phi(ncomp));
	double A     =  exp( ch->value - ch->value1 + Pcomp - Rcomp ) *
	  (1.0 - _birth/(_birth * ch->M0()))* 
	  PoisDif[ch->M0()-1]/PoisDif[ch->M1()-1];
	
	double accept_prob = min<double>(1.0, 1.0/A);
	
	if (myid==0 && _debug) {
	  ofstream tout("death.dat", ios::app);
	  if (tout) {
	    tout << setw(3)  << current_level 
		 << setw(15) << accept_prob
		 << setw(15) << ch->value - ch->value1 + Pcomp - Rcomp
		 << setw(15) << ch->value - ch->value1
		 << setw(15) << Pcomp
		 << setw(15) << Rcomp
		 << endl;
	  }
	}
	
	if (accept_prob >= (*unit)()) {
	  ch->died[2]++;
	  ch->died[3]++;
	  
	  ch->AcceptState();
	  accepted = true;
	}
	
      }
    }
    catch (ImpossibleStateException &e) {
      // Ok, move on . . . 
    }
    
  }

  return accepted;
}

bool ReversibleJump::SplitStep(Simulation* s, Chain* ch, MixturePrior *mix)
{
  unsigned ncomp=0;
  bool accepted = false;
  
  unsigned M    = ch->M0();
  unsigned Ndim = ch->Cur()->Ndim();

  vector<double> np(Ndim);
  
  // Communicate the mixture cardinality
  //
  if (mpi_used && s->Parallel()) {
    ++MPIMutex;
    MPI_Bcast(&M, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    --MPIMutex;
  }
  
  // Check to see if a space with one more component is allowed
  //
  if (mix->Exists(M+1)) {
    
    if (myid==0 || s->Serial()) {
      
      // Select a component for splitting
      //
      ncomp = min<int>((int)floor((*unit)()*M), M-1);
      
      // Assign the old state
      //
      ch->Tri()->Reset(M+1);
      for (unsigned m=0; m<M; m++) {
	ch->Wght1(m) = ch->Wght0(m);
	for (unsigned j=0; j<Ndim; j++) 
	  ch->Phi1(m, j) = ch->Phi0(m, j);
      }
      
      // Append the new component and update the original
      // 
      double nw = (*unit)();
      ch->Wght1(M)      = nw * ch->Wght0(ncomp);
      ch->Wght1(ncomp) *= 1.0 - nw;

      for (unsigned j=0; j<Ndim; j++) {
	if (_addspl) {
	  np[j] = _weight[j] * ((*unit)()-0.5);
	  ch->Phi1(M, j)     = ch->Phi0(ncomp, j) + np[j];
	  ch->Phi1(ncomp, j) = ch->Phi0(ncomp, j) - np[j];
	} else {
	  np[j] = _delta * ((*unit)()-0.5);
	  ch->Phi1(M, j)     = ch->Phi0(ncomp, j)*(1.0 + np[j]);
	  ch->Phi1(ncomp, j) = ch->Phi0(ncomp, j)*(1.0 - np[j]);
	}
      }

      // Renormalize weights
      //
      double norm = 0.0;
      for (unsigned i=0; i<M+1; i++) norm += ch->Wght1(i);
      for (unsigned i=0; i<M+1; i++) ch->Wght1(i) /= norm;
      
      // Update order
      //
      ch->Order1();
    }
    
    if (mpi_used && s->Parallel()) {
      ch->BroadcastChain();
    }
    
    //
    // Compute the state and acceptance
    //
    try {
      
      ComputeState(s, ch);
      
      if (myid==0 || s->Serial()) {
	
				// Computing the Jacobian
	double detrT = 1.0;
	double detrP = 1.0;
	double detrM = 1.0;

	for (unsigned i=0; i<Ndim; i++) {
	  if (_addspl) {
	    detrT *= _weight[i];
	  } else {
	    detrP *= 1.0 + np[i];
	    detrM *= 1.0 - np[i];
	    detrT *= ch->Phi0(ncomp, i) * _delta;
	  }
	}

	detrT = fabs( detrT*(detrP + detrM)*ch->Wght0(ncomp) );

	double probSplit = 1.0/ch->M0();
	double probUnite = 2.0/(ch->M1()*(ch->M1()-1));

	double A = exp( ch->value1 - ch->value ) * detrT *
	  (1.0 - _split)/_split *
	  probUnite/probSplit *
	  PoisDif[ch->M1()-1]/PoisDif[ch->M0()-1];
	
	double accept_prob = min<double>(1.0, A);
	
	if (myid==0 && _debug) {
	  ofstream tout("split.dat", ios::app);
	  if (tout) {
	    tout << setw(3)  << current_level 
		 << setw(15) << accept_prob
		 << setw(15) << ch->value1 - ch->value
		 << endl;
	  }
	}
	
	if (accept_prob >= (*unit)()) {
	  ch->split[2]++;
	  ch->split[3]++;
	  
	  ch->AcceptState();
	  accepted = true;
	}
      }
      
    }
    catch (ImpossibleStateException &e) {
      // Ok, move on . . . 
    }
  }
  
  return accepted;
}



bool ReversibleJump::UniteStep(Simulation* s, Chain* ch, MixturePrior *mix)
{
  unsigned nc1=0, nc2=1;
  bool accepted = false;
  bool goodeps = true;
  
  unsigned M    = ch->M0();
  unsigned Ndim = ch->Cur()->Ndim();

  vector<double> np(Ndim);
  
  // Communicate the mixture cardinality
  //
  if (mpi_used && s->Parallel()) {
    ++MPIMutex;
    MPI_Bcast(&M, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    --MPIMutex;
  }
  
  // Check to see if a space with one fewer component is allowed
  //
  if (mix->Exists(M-1)) {
    
    if (myid==0 || s->Serial()) {
      
      // Select pair at random
      //
      typedef pair<unsigned, unsigned> uup;
      map<double, uup> pr;
      for (unsigned i=0; i<M; i++) {
	for (unsigned j=i+1; j<M; j++) {
	  pr[(*unit)()] = uup(i, j);
	}
      }

      // The lucky couple
      //
      nc1 = pr.begin()->second.first;
      nc2 = pr.begin()->second.second;
      
      // Assign the unchanged part of the state
      //
      ch->Tri()->Reset(M-1);
      unsigned mm=0;
      for (unsigned m=0; m<M; m++) {
	if (m!=nc1 && m!=nc2) {
	  ch->Wght1(mm) = ch->Wght0(m);
	  for (unsigned j=0; j<Ndim; j++) 
	    ch->Phi1(mm, j) = ch->Phi0(m, j);
	}
	if (m!=nc2) mm++;
      }
      
      // Compute the new combined state
      // 
      ch->Wght1(nc1) = ch->Wght0(nc1) + ch->Wght0(nc2);
      for (unsigned j=0; j<Ndim; j++) {
	double aj = ch->Phi0(nc1, j);
	double bj = ch->Phi0(nc2, j);
	if (_addspl) {
	  ch->Phi1(nc1, j) = 0.5*(aj + bj);
	  np[j] = (bj - aj)/(2.0*_weight[j]);
	  if ( fabs(np[j]) > 1.0 ) goodeps = false;
	} else {
	  ch->Phi1(nc1, j) = 0.5*(aj + bj);
	  np[j] = (bj - aj)/(bj + aj);
	  if ( fabs(np[j]/_delta) > 1.0 ) goodeps = false;
	}
      }

      // Renormalize weights
      //
      double norm = 0.0;
      for (unsigned i=0; i<M-1; i++) norm += ch->Wght1(i);
      for (unsigned i=0; i<M-1; i++) ch->Wght1(i) /= norm;
      
      // Update order
      //
      ch->Order1();
    }
    
    if (mpi_used && s->Parallel()) {
      unsigned char ge = goodeps ? 1 : 0;
      MPI_Bcast(&ge, 1, MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);
      goodeps = ge ? true : false;
      if (goodeps) ch->BroadcastChain();
    }
    
    //
    // Compute the state and acceptance
    //
    if (goodeps) {

      try {
      
	ComputeState(s, ch);
      
	if (myid==0 || s->Serial()) {
	
	  // Computing the Jacobian
	  double detrT = 1.0;
	  double detrP = 1.0;	
	  double detrM = 1.0;

	  for (unsigned i=0; i<Ndim; i++) {
	    if (_addspl) {
	      detrT *= _weight[i];
	    } else {
	      detrP *= 1.0 + np[i];
	      detrM *= 1.0 - np[i];
	      detrT *= ch->Phi1(nc1, i) * _delta;
	    }
	  }

	  detrT = fabs( detrT*(detrP + detrM)*ch->Wght1(nc1) );

	  double probSplit = 1.0/ch->M1();
	  double probUnite = 2.0/(ch->M0()*(ch->M0()-1));

	  double A = exp( ch->value - ch->value1 ) * detrT *
	    (1.0 - _split)/_split *
	    probUnite/probSplit *
	    PoisDif[ch->M0()-1]/PoisDif[ch->M1()-1];
	
	  double accept_prob = min<double>(1.0, 1.0/A);
	
	  if (myid==0 && _debug) {
	    ofstream tout("unite.dat", ios::app);
	    if (tout) {
	      tout << setw(3)  << 1
		   << setw(3)  << current_level 
		   << setw(3)  << nc1
		   << setw(3)  << nc2
		   << setw(15) << accept_prob
		   << setw(15) << ch->value - ch->value1;
	      for (unsigned j=0; j<Ndim; j++) 
		tout << setw(15) << ch->Phi0(nc1, j)
		     << setw(15) << ch->Phi0(nc2, j);
	      tout << endl;
	    }
	  }
	  
	  if (accept_prob >= (*unit)()) {
	    ch->unite[2]++;
	    ch->unite[3]++;
	    
	    ch->AcceptState();
	    accepted = true;
	  }
	}
	
      }
      catch (ImpossibleStateException &e) {
	// Ok, move on . . . 
      }
    } else {
      if (myid==0 && _debug) {
	ofstream tout("unite.dat", ios::app);
	if (tout) {
	  tout << setw(3)  << 0
	       << setw(3)  << current_level 
	       << setw(3)  << nc1
	       << setw(3)  << nc2;
	  for (unsigned j=0; j<Ndim; j++) {
	    if (_addspl) tout << setw(15) << np[j];
	    else         tout << setw(15) << np[j]/_delta;
	  }
	  tout << endl;
	}
      }
    }
  }

  return accepted;
}
