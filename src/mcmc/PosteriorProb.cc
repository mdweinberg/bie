// This is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

#include <Tessellation.h>
#include <PosteriorProb.h>
#include <LikelihoodComputation.h>
#include <gvariable.h>
#include <BIEmpi.h>
#include <BIEdebug.h>
#include <BIEMutex.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PosteriorProb)

using namespace BIE;

Converge PosteriorProb::c; // Default action is to always return False

PosteriorProb::Control PosteriorProb::cntrl = PosteriorProb::parallel;

PosteriorProb::PosteriorProb(StateInfo *si,
			     BaseDataTree* d,
			     Model* m,
			     Integration* i,
			     Prior* p,
			     LikelihoodComputation* l)
  : Simulation(si, d, m, i, &c, p, l, NULL)
{
  algoname = "Posterior Prob";

  state_count = 0;
  interactive = true;
}

PosteriorProb::PosteriorProb(StateInfo *si,
			     Prior* p,
			     LikelihoodComputation* l)
  : Simulation(si, &c, p, l, NULL)
{
  algoname = "Posterior Prob";

  state_count = 0;
  interactive = true;
}

PosteriorProb::~PosteriorProb()
{
}

void PosteriorProb::SaveFrontier()
{
  delete _priorFrontier;
  _priorFrontier = new Frontier(*(_dist->GetDefaultFrontier()));
}

void PosteriorProb::NullFrontier()
{
  delete _priorFrontier;
  _priorFrontier = 0;
}

void PosteriorProb::Run()
{
  std::ostream out(checkTable("cli_console"));

  interactive = true;
  state_count = 0;

  // Start the worker threads
  //
  if (mpi_used && myid) 
    _likelihoodComputation->startThread();

  // Get the trial state interactively from the console
  //
  GetUserState();

  // Compute the posterior probability
  //
  try {

    ch.like_value  = Likelihood(ch.GetState0(), ch.indx);
    ch.prior_value = PriorValue(ch.GetState0());

    if (myid==0) {

      if (ch.like_value > -0.99e30 && ch.prior_value > -0.99e30)
	ch.value = ch.like_value + ch.prior_value;
      else
	ch.value = -2.0e30;
      
      PrintState(out);

      _likelihoodComputation->free_workers();
    }
  
    if (mpi_used && myid) 
      _likelihoodComputation->stopThread();

  }
  catch (ImpossibleStateException &e) {
    if (myid==0) {
      cerr << "Bad state: ";
      for (size_t k=0; k<ch.Cur()->size(); k++)
	cerr << setw(14) << setprecision(6) << ch.Vec0(k);
      cerr << endl << " . . . try again" << endl;
    }
  }

}

void PosteriorProb::Run(string infile, string outfile)
{
  interactive = false;
  state_count = 0;

  // Start the worker threads
  //
  if (mpi_used && myid) 
    _likelihoodComputation->startThread();

  // Read the file containing the trial state
  //
  try {
    ifstream fin;
    ofstream fout;

    if (myid==0) {
      fin.open(infile.c_str());
      if (!fin) {
	cerr << "Error opening <" << infile << ">" << endl;
	throw FileOpenException(infile, errno, __FILE__, __LINE__);
      }

      if (outfile.size()) {
	fout.open(outfile.c_str());
	if (!fout) {
	  cerr << "Error opening <" << outfile << ">" << endl;
	  throw FileOpenException(outfile, errno, __FILE__, __LINE__);
	}
      }
    }

    ostream out(checkTable("cli_console"));

    if (fout) {
      out.copyfmt(fout);
      out.clear(fout.rdstate());
      out.basic_ios<char>::rdbuf(fout.rdbuf());
    }

    while (ReadState(fin)) {
      // Compute the posterior
      //
      try {
	ch.like_value  = Likelihood(ch.GetState0(), ch.indx);
	ch.prior_value = PriorValue(ch.GetState0());
	ch.value       = ch.like_value + ch.prior_value;

	PrintState(out);
      }
      catch (ImpossibleStateException &e) {
	if (myid==0)
	  cerr << "Bad state . . . skipping" << endl;
      }
    }
  } 
  catch (string data) {
    string msg = "PosteriorProb::ReadState: error in parsing string <" + 
      data + ">";
    throw FileFormatException(msg, __FILE__, __LINE__);
  }
  catch (BIEException e) {
    if (mpi_used) cerr << "Process " << myid << ": ";
    cerr << e.getErrorMessage() << endl;
  }

  // Free up the captive processes and signal an exit
  //
  _likelihoodComputation->free_workers();
  
  // Stop the worker threads
  //
  if (mpi_used && myid) {
    _likelihoodComputation->stopThread();
  }

}

void PosteriorProb::Run(clivectord* vec, string outfile)
{
  Run((*vec)(), outfile);
}

void PosteriorProb::Run(std::vector<double> vec, string outfile)
{
  interactive = false;
  state_count = 0;

  // Start the worker threads
  //
  if (mpi_used && myid) 
    _likelihoodComputation->startThread();

  // Create the new state from the vector
  //
  State s(_si, vec);

  // Create a new chain from the state
  //
  Chain ch(s);
  if (mpi_used) ch.BroadcastChain();

  // Open output file, if specified.  Otherwise default to console.
  //
  std::ostream out(checkTable("cli_console"));

  if (myid==0) {

    ofstream fout;

    if (outfile.size()) {
      fout.open(outfile.c_str());
      if (!fout) {
	cerr << "Error opening <" << outfile << ">" << endl;
	throw FileOpenException(outfile, errno, __FILE__, __LINE__);
      }
    }
    
    if (fout) {
      out.copyfmt(fout);
      out.clear(fout.rdstate());
      out.basic_ios<char>::rdbuf(fout.rdbuf());
    }
  }

  // Compute the posterior
  //
  try {
    ch.like_value  = Likelihood(ch.GetState0(), ch.indx);
    ch.prior_value = PriorValue(ch.GetState0());
    ch.value       = ch.like_value + ch.prior_value;
    
    PrintState(out);
  }
  catch (ImpossibleStateException &e) {
    if (myid==0)
      cerr << "Bad state . . . skipping" << endl;
  }
  catch (string data) {
    string msg = "PosteriorProb::ReadState: error in parsing string <" + 
      data + ">";
    throw FileFormatException(msg, __FILE__, __LINE__);
  }
  catch (BIEException e) {
    if (mpi_used) cerr << "Process " << myid << ": ";
    cerr << e.getErrorMessage() << endl;
  }

  // Free up the captive processes and signal an exit
  //
  _likelihoodComputation->free_workers();
  
  // Stop the worker threads
  //
  if (mpi_used && myid) {
    _likelihoodComputation->stopThread();
  }

}

void PosteriorProb::Run(Prior *prior, int number, string outfile)
{
  interactive = false;
  state_count = 0;
  unsigned bad_states = 0;

  // Start the worker threads
  //
  if (mpi_used && myid) 
    _likelihoodComputation->startThread();

  ofstream fout;

  if (myid==0) {
    if (outfile.size()) {
      fout.open(outfile.c_str());
      if (!fout) {
	cerr << "Error opening <" << outfile << ">" << endl;
	throw FileOpenException(outfile, errno, __FILE__, __LINE__);
      }
    }
  }

  ostream out(checkTable("cli_console"));

  if (fout) {
    out.copyfmt(fout);
    out.clear(fout.rdstate());
    out.basic_ios<char>::rdbuf(fout.rdbuf());
  }

  unsigned long imax = 1000*number, itry=0;

  while (state_count < static_cast<unsigned>(number) && itry++ < imax) {
    
    State cur = prior->Sample();
    ch.AssignState0(cur);
    
    // Compute the posterior
    //
    try {
      ch.like_value  = Likelihood(ch.GetState0(), ch.indx);
      ch.prior_value = PriorValue(ch.GetState0());
      ch.value       = ch.like_value + ch.prior_value;
      
      PrintState(out);
    }
    catch (ImpossibleStateException &e) {
      bad_states++;
    }
  }

  // Free up the captive processes and signal an exit
  //
  _likelihoodComputation->free_workers();
  
  // Stop the worker threads
  //
  if (mpi_used && myid) {
    _likelihoodComputation->stopThread();
  }

  // Report the number of states created
  //
  if (myid==0) {
    cout << "PosteriorProp::Run(): created " << state_count << " states" 
	 << endl;
    if (bad_states) 
      cout << "PosteriorProp::Run(): " << bad_states << " states rejected" 
	   << endl;
  }

}

void PosteriorProb::GetUserState()
{
  std::ostream out(checkTable("cli_console"));

  if (myid==0) {

    switch (_si->ptype) {
    case StateInfo::None:
    case StateInfo::Block:
    case StateInfo::RJTwo:

      out << "Need " << _si->Ndim  << " state values:\n";
      for (unsigned i=0; i<_si->Ntot; i++) {
	out << setw(20) << ch.Cur()->ParameterDescription(i) << ": ";
	std::cin >> ch.Vec0(i);
      }

      break;

    case StateInfo::Mixture:
    case StateInfo::Extended:

      out << "Number of weights (<=" << _si->M << ">)? ";
      unsigned MM;
      std::cin >> MM;
      if (MM != _si->M) ch.Cur()->Reset(MM);

      out << "Need " << MM  << " weights:\n";
      double sum = 0.0;
      for (unsigned i=0; i<MM; i++) {
	out << setw(4) << i << ": ";
	std::cin >> ch.Wght0(i);
	sum += ch.Wght0(i);
      }
      for (unsigned i=0; i<MM; i++) ch.Wght0(i) /= sum;
    
      out << "Need " << MM  << " components:\n";
      for (unsigned i=0; i<MM; i++) {
	out << "Component #" << i << "\n";
	for (unsigned j=0; j<_si->Ndim; j++) {
	  out << setw(20) 
	       << ch.Cur()->ParameterDescription(MM+_si->Ndim*i+j) << ": ";
	  std::cin >> ch.Phi0(i, j);
	}
      }
    
      break;
    }
  }

  if (mpi_used) ch.BroadcastChain();
}


bool PosteriorProb::ReadState(istream& infile)
{
  if (myid==0) {

    const size_t rdsiz = 4196;
    char rdbuf[rdsiz];

    infile.getline(rdbuf, rdsiz);
    if (!infile.good()) return false;

    istringstream in(rdbuf);

    switch (_si->ptype) {
    case StateInfo::None:
    case StateInfo::Block:
    case StateInfo::RJTwo:

      for (unsigned j=0; j<_si->Ntot; j++) in >> ch.Vec0(j);
      break;
      
    case StateInfo::Mixture:
    case StateInfo::Extended:

      unsigned M;
      in >> M;

      if (ch.M0() != M) ch.Cur()->Reset(M);

      for (unsigned i=0; i<M; i++) {
	in >> ch.Wght0(i);
	for (unsigned j=0; j<_si->Ndim; j++) {
	  in >> ch.Phi0(i, j);
	}
      }

      if (!in) {
	throw in.str();
      }
      
      double sum = 0.0;
      for (unsigned i=0; i<M; i++) sum += ch.Wght0(i);
      for (unsigned i=0; i<M; i++) ch.Wght0(i) /= sum;

      break;
    }
  }

  if (mpi_used) ch.BroadcastChain();

  return true;
}

void PosteriorProb::MCMethod() {}

void PosteriorProb::PrintState(ostream& out)
{
  if (myid) return;

  if (interactive || state_count==0) {

    ostringstream sout;
    char q = ' ';

    out << right;

    if (!interactive) {
      q = '"';

      sout.str(""); sout << q << "Level" << q;
      out << setw(10) << sout.str();
    
      sout.str(""); sout << q << "Iter" << q;
      out << setw(10) << sout.str();
    }

    sout.str(""); sout << q << "Probability" << q;
    out << setw(20) << sout.str();
    
    sout.str(""); sout << q << "Likelihood" << q;
    out << setw(20) << sout.str();
    
    sout.str(""); sout << q << "Prior" << q;
    out << setw(20) << sout.str();
    
    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) {
      sout.str(""); sout << q << "Number" << q;
      out << setw(12)  << sout.str();
    }
  
    for (unsigned i=0; i<ch.N0(); i++) {
      sout.str("");
      sout << q << ch.Cur()->ParameterDescription(i) << q;
      out << setw(15) << sout.str();
    }
    out << endl;
  }

  if (!interactive) {
    out << setw(10) << 0
	 << setw(10) << state_count++;
  }

  out << setw(20) << setprecision(12) << ch.value
       << setw(20) << setprecision(12) << ch.like_value
       << setw(20) << setprecision(12) << ch.prior_value;

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) 
    out << setw(12) << setprecision(8)  << ch.M0();
  
  for (unsigned k=0; k<ch.Cur()->size(); k++)
    out << setw(15) << setprecision(15) << ch.Vec0(k);
  
  out << endl;
}

void PosteriorProb::ReportState(void)
{
  ostream cout(checkTable("simulation_output"));
  vector<double>::iterator cur;
  
  // Column descriptions
  for (unsigned i=0; i<ch.N0(); i++) {
    if (i==0) cout << "# " << setw(12);
    else cout << setw(14);
    cout << ch.Cur()->ParameterDescription(i);
  }
  
  // Print parameters
  for (unsigned i=0; i<ch.N0(); i++)
    cout << setw(14) << ch.Vec0(i);
  cout << endl;
}

void PosteriorProb::setLevels(int Ndecend)
{
  Frontier * frontier = _dist->GetDefaultFrontier();
  
  // Set the frontier to be the root of the tessellation
  frontier->RetractToTopLevel(); 
  frontier->UpDownLevels(Ndecend);
  
  if (!mpi_used || myid==0) {
    printFrontierTiles(frontier);
  }

}

void PosteriorProb::Image(string filename)
{
  string input;
  Image(filename, input);
}


bool PosteriorProb::GetImageDataUser(int &numx, int &numy, 
				     vector<double>& wght0, 
				     vector< vector<double> >& phi0,
				     vector<char>& bwght, 
				     vector< vector<char> >& bphi,
				     vector< vector<double> >& range
				     )
{
  unsigned MM = _si->M;

  if (myid==0) {
    if (_si->ptype == StateInfo::Mixture ||
	_si->ptype == StateInfo::Extended) 
      {
	//
	// Ask for number of weights
	//
	cout << "Number of weights (<=" << _si->M << ">)? ";
	cin >> MM;

	MM = min<unsigned>(_si->M, MM);
      }
  }

  MPI_Bcast(&MM, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  ch.Cur()->Reset(MM);

  // Fiducial vectors
  //
  wght0 = vector<double>(MM);
  phi0  = vector< vector<double> >(MM);
  for (unsigned k=0; k<MM; k++) phi0[k] = vector<double>(_si->Ndim);

  // Flag vectors
  //
  bwght = vector<char>(MM, 0);
  bphi  = vector< vector<char> >(MM);
  for (unsigned k=0; k<MM; k++) bphi[k]  = vector<char>(_si->Ndim);
  
  if (myid==0) {

    unsigned M = bwght.size();
    int rangecnt = 0;
    char c;

    while (rangecnt != 2) {

      cout << "Need " << M  << " weights:\n";
      for (unsigned i=0; i<M; i++) {
	if (rangecnt<2) {
	  cout << setw(4) << i << ": Sample [y/n]? ";
	  cin >> c;
	} else c = 'n';
	switch (c) {
	case 'y':
	  cout << setw(20) << "Enter lower upper: ";
	  cin >> range[rangecnt][0];
	  cin >> range[rangecnt][1];
	  bwght[i] = 1;
	  rangecnt++;
	  break;
	default:
	  cout << setw(20) << "Enter value: ";
	  cin >> wght0[i];
	}
      }
	
      cout << "Need " << M  << " component vectors:\n";
      for (unsigned i=0; i<M; i++) {
	cout << "Component #" << i << "\n";
	for (unsigned j=0; j<_si->Ndim; j++) {
	  cout << setw(20) 
	       << ch.Cur()->ParameterDescription(M+_si->Ndim*i+j) << ": ";
	  if (rangecnt<2) {
	    cout << " Sample [y/n]? ";
	    cin >> c;
	  } else  c = 'n';
	    
	  switch (c) {
	  case 'y':
	    cout << setw(20) << "Enter lower upper: ";
	    cin >> range[rangecnt][0];
	    cin >> range[rangecnt][1];
	    bphi[i][j] = 1;
	    rangecnt++;
	    break;
	  default:
	    cout << setw(20) << "Enter value: ";
	    cin >> phi0[i][j];
	  }
	}
      }
    }

    cout << "Number for first range: ";
    cin >> numx;
    cout << "Number for second range: ";
    cin >> numy;
  }
  
  ShareImageData(numx, numy, wght0, phi0, bwght, bphi, range);

  return false;
}



bool PosteriorProb::GetImageDataUser(int &numx, int &numy, 
				     vector<double>& vec0, 
				     vector<char>& bvec, 
				     vector< vector<double> >& range
				     )
{
  if (myid==0) {

    int rangecnt = 0;
    char c;

    while (rangecnt != 2) {

      cout << "Enter two ranges and fixed values for state vector image . . ."
	   << endl;

      // Reset all to false
      for (unsigned j=0; j<bvec.size(); j++) bvec[j] = 0;
      rangecnt = 0;

      for (unsigned j=0; j<_si->Ntot; j++) {
	cout << setw(20) << ch.Cur()->ParameterDescription(j) << ": ";
	if (rangecnt<2) {
	  cout << " Sample [y/n]? ";
	  cin >> c;
	} else  c = 'n';
	
	switch (c) {
	case 'y':
	  cout << setw(20) << "Enter lower upper: ";
	  cin >> range[rangecnt][0];
	  cin >> range[rangecnt][1];
	  bvec[j] = 1;
	  rangecnt++;
	  break;
	default:
	  cout << setw(20) << "Enter value: ";
	  cin >> vec0[j];
	}
      }
    }
    
    cout << "Number for first range: ";
    cin >> numx;
    cout << "Number for second range: ";
    cin >> numy;
  }
  
  ShareImageData(numx, numy, vec0, bvec, range);

  return false;
}



bool PosteriorProb::GetImageDataFile(string& input,
				     int &numx, int &numy, 
				     vector<double>& vec0, 
				     vector<char>& bvec, 
				     vector< vector<double> >& range)
{
  string ierr;
  int file_error = 0;

  if (myid==0) {

    const int linsiz = 4096;
    char line[linsiz];
    int rangecnt = 0;

    ifstream in(input.c_str());

    try {

      if (!in.good()) throw "unexpected failure opening data file";

      while (rangecnt != 2) {

	// Reset all to false
	//
	for (unsigned k=0; k<bvec.size(); k++) bvec[k] = 0;
	
	rangecnt = 0;

	for (unsigned j=0; j<_si->Ntot; j++) {
	  in.getline(line, linsiz);
	  if (!in.good()) {
	    ostringstream eout;
	    eout << "failure reading line from input file for "
		 << j+1 << "/" << _si->Ntot << " parameter";
	    throw eout.str();
	  }

	  istringstream sin(line);
	  string v;

	  sin >> v;

	  switch (v[0]) {
	  case 'r':
	    sin >> range[rangecnt][0];
	    sin >> range[rangecnt][1];
	    bvec[j] = 1;
	    rangecnt++;
	    break;
	  case 'f':
	    sin >> vec0[j];
	    break;
	  default:
	    {
	      ostringstream eout;
	      eout << "failure parsing data line for component " 
		   << j+1 << "/" << _si->Ndim;
	      throw eout.str();
	    }
	  }
	}
      }
      
      in.getline(line, linsiz);
      if (!in.good()) {
	ostringstream eout;
	eout << "error reading the final image dimension line, " 
	     << "expected <numx numy>, line=" << line;
	throw eout.str();
      }

      istringstream sin(line);
      sin >> numx;
      sin >> numy;
    }
    catch (string ret) {
      ierr = ret;
      file_error = 1;
    }
  }

  MPI_Bcast(&file_error, 1, MPI_INT, 0, MPI_COMM_WORLD);
  
  if (file_error) {
    if (myid==0) cout << "File format error in input data [1]:" 
		      << endl << ierr << endl;
    
    // Free up the captive processes and signal an exit
    //
    _likelihoodComputation->free_workers();
    
    // Stop the worker threads
    //
    if (mpi_used && myid) {
      _likelihoodComputation->stopThread();
    }
    
    return true;
  }

  ShareImageData(numx, numy, vec0, bvec, range);

  return false;
}


bool PosteriorProb::GetImageDataFile(string& input,
				     int &numx, int &numy, 
				     vector<double>& wght0, 
				     vector< vector<double> >& phi0,
				     vector<char>& bwght, 
				     vector< vector<char> >& bphi,
				     vector< vector<double> >& range)
{
  string ierr;
  int file_error = 0;
  unsigned MM = _si->M;

  const int linsiz = 4096;
  char line[linsiz];
  ifstream in;

  if (myid==0) {
    
    in.open(input.c_str());
    
    try {
      if (!in.good()) throw "unexpected failure opening data file";

      if (_si->ptype == StateInfo::Mixture ||
	  _si->ptype == StateInfo::Extended) 
	{
	  in.getline(line, linsiz);	
	  if (!in.good()) {
	    ostringstream eout;
	    eout << "failure reading line from input file for " 
		 << " mixture count";
	    throw eout.str();
	  }

	  istringstream sin(line);
	  sin >> MM;
	  MM = min<unsigned>(_si->M, MM);
	}
    }
    catch (string ret) {
      ierr = ret;
      file_error = 1;
    }
  }

  MPI_Bcast(&file_error, 1, MPI_INT, 0, MPI_COMM_WORLD);
  
  if (file_error) {
    if (myid==0) cout << "File format error in input data [0]:" 
		      << endl << ierr << endl;
    
    // Free up the captive processes and signal an exit
    //
    _likelihoodComputation->free_workers();
    
    // Stop the worker threads
    //
    if (mpi_used && myid) {
      _likelihoodComputation->stopThread();
    }
    
    return true;
  }

  MPI_Bcast(&MM, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  ch.Cur()->Reset(MM);

  // Fiducial vectors
  //
  wght0 = vector<double>(MM);
  phi0  = vector< vector<double> >(MM);
  for (unsigned k=0; k<MM; k++) phi0[k] = vector<double>(_si->Ndim);

  // Flag vectors
  //
  bwght = vector<char>(MM, 0);
  bphi  = vector< vector<char> >(MM);
  for (unsigned k=0; k<MM; k++) bphi[k]  = vector<char>(_si->Ndim);

  if (myid==0) {

    unsigned M = bwght.size();
    const int linsiz = 4096;
    char line[linsiz];

    int rangecnt = 0;

    ifstream in(input.c_str());

    try {

      if (!in.good()) throw "unexpected failure opening data file";

      while (rangecnt != 2) {

	// Reset all to false
	//
	for (unsigned k=0; k<M; k++) {
	  bwght[k] = 0;
	  bphi[k]  = vector<char>(_si->Ndim, 0);
	}
	
	rangecnt = 0;

	for (unsigned i=0; i<M; i++) {
	  in.getline(line, linsiz);
	  if (!in.good()) {
	    ostringstream eout;
	    eout << "failure reading line from input file for " 
		 << i+1 << "/" << M << " field";
	    throw eout.str();
	  }

	  istringstream sin(line);
	  string v;
	  
	  sin >> v;
	  
	  switch (v[0]) {
	  case 'r':
	    sin >> range[rangecnt][0];
	    sin >> range[rangecnt][1];
	    bwght[i] = 1;
	    rangecnt++;
	    break;
	  case 'f':
	    sin >> wght0[i];
	    break;
	  default:
	    {
	      ostringstream eout;
	      eout << "failure parsing data line for weight " 
		   << i+1 << "/" << M;
	      throw eout.str();
	    }

	  }
	}
	
	for (unsigned i=0; i<M; i++) {
	  for (unsigned j=0; j<_si->Ndim; j++) {
	    in.getline(line, linsiz);	
	    if (!in.good()) {
	      ostringstream eout;
	      eout << "failure reading line from input file for " 
		   << i+1 << "/" << M << " component, "
		   << j+1 << "/" << _si->Ndim << " parameter";
	      throw eout.str();
	    }
	    
	    istringstream sin(line);
	    string v;
	
	    sin >> v;

	    switch (v[0]) {
	    case 'r':
	      sin >> range[rangecnt][0];
	      sin >> range[rangecnt][1];
	      bphi[i][j] = 1;
	      rangecnt++;
	      break;
	    case 'f':
	      sin >> phi0[i][j];
	      break;
	    default:
	      {
		ostringstream eout;
		eout << "failure parsing data line for component " 
		     << i+1 << "/" << M << " and parameter "
		     << j+1 << "/" << _si->Ndim;
		throw eout.str();
	      }
	    }
	  }
	}
      }
      
      in.getline(line, linsiz);
      if (!in.good()) {
	ostringstream eout;
	eout << "error reading the final image dimension line, " 
	     << "expected <numx numy>, line=" << line;
	throw eout.str();
      }

      istringstream sin(line);
      sin >> numx;
      sin >> numy;
    }
    catch (string ret) {
      ierr = ret;
      file_error = 1;
    }
  }

  MPI_Bcast(&file_error, 1, MPI_INT, 0, MPI_COMM_WORLD);
  
  if (file_error) {
    if (myid==0) cout << "File format error in input data [2]:" 
		      << endl << ierr << endl;
    
    // Free up the captive processes and signal an exit
    //
    _likelihoodComputation->free_workers();
    
    // Stop the worker threads
    //
    if (mpi_used && myid) {
      _likelihoodComputation->stopThread();
    }
    
    return true;
  }

  ShareImageData(numx, numy, wght0, phi0, bwght, bphi, range);

  return false;
}

void PosteriorProb::ShareImageData(int &numx, int &numy, 
				   vector<double>& wght0, 
				   vector< vector<double> >& phi0,
				   vector<char>& bwght, 
				   vector< vector<char> >& bphi,
				   vector< vector<double> >& range
				   )
{
  if (!mpi_used) return;

  int M = wght0.size();

  // Range counts
  //
  MPI_Bcast(&numx, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&numy, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // Fiducial vectors
  //
  MPI_Bcast(&wght0[0], M, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  for (int k=0; k<M; k++) 
    MPI_Bcast(&phi0[k][0], _si->Ndim, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  // Flag vectors
  //
  MPI_Bcast(&bwght[0], M, MPI_CHAR, 0, MPI_COMM_WORLD);
  for (int k=0; k<M; k++) 
    MPI_Bcast(&bphi[k][0], _si->Ndim, MPI_CHAR, 0, MPI_COMM_WORLD);
  
  // Range vector
  //
  for (int k=0; k<2; k++)
    MPI_Bcast(&range[k][0], 2, MPI_DOUBLE, 0, MPI_COMM_WORLD);

}


void PosteriorProb::ShareImageData(int &numx, int &numy, 
				   vector<double>& vec0,
				   vector<char>& bvec, 
				   vector< vector<double> >& range
				   )
{
  if (!mpi_used) return;

  // Range counts
  //
  MPI_Bcast(&numx, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&numy, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // Fiducial vector
  //
  MPI_Bcast(&vec0[0], vec0.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);

  // Flag vectors
  //
  MPI_Bcast(&bvec[0], bvec.size(), MPI_CHAR, 0, MPI_COMM_WORLD);
  
  // Range vector
  //
  for (int k=0; k<2; k++)
    MPI_Bcast(&range[k][0], 2, MPI_DOUBLE, 0, MPI_COMM_WORLD);

}


void PosteriorProb::Image(string filename, string input)
{
  if (mpi_used && myid) 
    _likelihoodComputation->startThread();
  
  if (myid) {
    if  (_likelihoodComputation->Serial() != (cntrl==serial) ) {
      cerr << "Logic error: you need to use serial control" << endl
	   << "with a serial likelihood type and vice versa" << endl
	   << "(this will be made automatic in the future . . . )" << endl;
      return;
    }
  }

  int nump = 1;
  if (mpi_used && cntrl==serial) nump = numprocs;

  // Range counts
  //
  int numx, numy;

  int rangecnt = 0;
  bool go = true;
    
  vector<double> data;
  double vmax, vmax1 = -1.0e32, x, y, dx, dy;
  
  // Range vector
  //
  vector< vector<double> > range(2);
  for (int k=0; k<2; k++) range[k] = vector<double>(2);

  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo) {

    // Fiducial vectors
    //
    vector<double> vec0(_si->Ntot);

    // Flag vectors
    //
    vector<char> bvec(_si->Ntot, 0);

    bool ret;
    if (input.size())
      ret = GetImageDataFile(input, numx, numy, vec0, bvec, range);
    else
      ret = GetImageDataUser(numx, numy, vec0, bvec, range);

    if (ret) return;

    data = vector<double>(numx*numy, 0);

    dx = (range[0][1] - range[0][0])/(numx-1);
    dy = (range[1][1] - range[1][0])/(numy-1);
    
    for (int iy=0; iy<numy; iy++) {

      y = range[1][0] + dy*iy;

      for (int ix=0; ix<numx; ix++) {
	
	x = range[0][0] + dx*ix;
      
	int indx = numx*iy + ix;

	if (mpi_used && cntrl==serial) {
	  go =  (indx % nump == myid);
	}
       
	if (go) {
	  rangecnt = 0;
	 
	  // Mixture family parameters
	  double sum = 0.0;
	  for (unsigned j=0; j<_si->Ntot; j++) {
	    if (bvec[j]) {
	      if (rangecnt++ == 0) {
		ch.Vec0(j) = x;
		if (j<ch.M0()) sum += x;
	      } else {
		ch.Vec0(j) = y;
		if (j<ch.M0()) sum += y;
	      }
	    } else {
	      ch.Vec0(j) = vec0[j];
	      if (j<ch.M0()) sum += vec0[j];
	    }
	  }

	  for (unsigned j=0; j<ch.M0(); j++) ch.Vec0(j) /= sum;

	  try {
	    ch.like_value = Likelihood(ch.GetState0(), ch.indx);
	    ch.prior_value =  PriorValue(ch.GetState0());
	    if (ch.like_value>-0.99e30 && ch.prior_value>-0.99e30)
	      ch.value = ch.like_value + ch.prior_value;
	    else
	      ch.value = -2.0e30;
	  }
	  catch (ImpossibleStateException err) {
	    ch.value= -2.0e30;
	  }
	  
	  data[indx] = ch.value;
	  vmax1 = max<double>(ch.value, vmax1);
	}

      } // END: ix loop

    } // END: iy loop
    
  } else {

    // Fiducial vectors
    //
    vector<double> wght0;
    vector< vector<double> > phi0;

    // Flag vectors
    //
    vector<char> bwght;
    vector< vector<char> > bphi;

    bool ret;
    if (input.size())
      ret = GetImageDataFile(input, numx, numy, wght0, phi0, bwght, bphi, range);
    else
      ret = GetImageDataUser(numx, numy, wght0, phi0, bwght, bphi, range);

    if (ret) return;

    data = vector<double>(numx*numy, 0);

    dx = (range[0][1] - range[0][0])/(numx-1);
    dy = (range[1][1] - range[1][0])/(numy-1);
    
    for (int iy=0; iy<numy; iy++) {

      y = range[1][0] + dy*iy;

      for (int ix=0; ix<numx; ix++) {

	x = range[0][0] + dx*ix;
      
	int indx = numx*iy + ix;

	if (mpi_used && cntrl==serial) 
	  go = (indx % nump == myid);
       
	if (go) {
	  rangecnt = 0;
	 
	  double sum = 0.0;
	  for (unsigned i=0; i<ch.M0(); i++) {
	    ch.Wght0(i) = wght0[i];
	    if (bwght[i]) {
	      if (rangecnt++ == 0) ch.Wght0(i) = x;
	      else ch.Wght0(i) = y;
	    }
	    sum += ch.Wght0(i);
	  }
	  
	  // Final normalization
	  for (unsigned i=0; i<ch.M0(); i++) ch.Wght0(i) /= sum;
	 
	 
	  // Mixture family parameters
	  for (unsigned i=0; i<ch.M0(); i++) {
	    for (unsigned j=0; j<_si->Ndim; j++) {
	      if (bphi[i][j]) {
		if (rangecnt++ == 0) ch.Phi0(i, j) = x;
		else ch.Phi0(i, j) = y;
	      }
	      else ch.Phi0(i, j) = phi0[i][j];
	    }
	  }
	  
	  try {
	    ch.like_value = Likelihood(ch.GetState0(), ch.indx);
	    ch.prior_value =  PriorValue(ch.GetState0());
	    if (ch.like_value>-0.99e30 && ch.prior_value>-0.99e30)
	      ch.value = ch.like_value + ch.prior_value;
	    else
	      ch.value = -2.0e30;
	  }
	  catch (ImpossibleStateException err) {
	    ch.value= -2.0e30;
	  }
	  
	  data[indx] = ch.value;
	  vmax1 = max<double>(ch.value, vmax1);
	}

      } // END: ix loop

    } // END: iy loop

  }
   
    
  // Gather up data if likelihood was serial,
  // otherwise, the root already has the result
  //

  vmax = vmax1;			// Default value for root node

  if (mpi_used && cntrl==serial) {

    MPI_Reduce(&vmax1, &vmax, 1, MPI_DOUBLE, MPI_MAX,
	       0, MPI_COMM_WORLD);

    if (myid==0)
      MPI_Reduce(MPI_IN_PLACE, &data[0], numx*numy, MPI_DOUBLE, MPI_SUM, 
		 0, MPI_COMM_WORLD);
    else
      MPI_Reduce(&data[0], NULL, numx*numy, MPI_DOUBLE, MPI_SUM, 
		 0, MPI_COMM_WORLD);
  }

  if (myid==0) {

    cout << "Printing output to <" << filename << "> in GNUPLOT format" << endl;
    cout << "Peak pixel value = " << vmax << endl;

    //
    // Print out the posterior image
    //
    ofstream out(filename.c_str());

    out << "# Peak pixel value = " << vmax << ", subtracted" << endl;

    for (int iy=0; iy<numy; iy++) {
      y = range[1][0] + dy*iy;
      for (int ix=0; ix<numx; ix++) {
	x = range[0][0] + dx*ix;
	out << setw(18) << x 
	    << setw(18) << y 
	    << setw(18) << data[numx*iy+ix] - vmax
	    << endl;
      }
      out << endl;
    }

  }
  
  // Free up the captive processes and signal an exit
  //
  _likelihoodComputation->free_workers();

  // Stop the worker threads
  //
  if (mpi_used && myid) {
    _likelihoodComputation->stopThread();
  }
  
}


void PosteriorProb::SetControl(int m)
{
  switch(m) {
  case parallel:
    cntrl = parallel;
    break;
  case serial:
    if (mpi_used)
      cntrl = serial;
    else {
      cout << "PosteriorProb: you want _serial_ control but you must"   << endl
	   << "               use MPI for that to make any sense.  You" << endl
	   << "               are getting _parallel_ control."          << endl;
    }
    break;
  default:
    ostringstream out;
    out << "Control method should be <parallel> (" << parallel
	<< ") or <serial> (" << serial << ")."
	<< "  input value was " << m;
    throw InternalError(out.str(), __FILE__, __LINE__);
  }
}

