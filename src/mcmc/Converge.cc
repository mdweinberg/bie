#include <Converge.h>

using namespace BIE;

void Converge::DumpConverged(string filename)
{
  if ( Converged() ) {
    unsigned long beg = 0;
    if (_dist->Npopped() > nburn) beg = _dist->Npopped() - nburn;
    _dist->dumpStates(filename, beg);
  }
}

