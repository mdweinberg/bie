#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
using namespace std;

#include <BIEException.h>
#include <RunSimulation.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RunSimulation)

using namespace BIE;


// Set flags in control file
void RunSimulation::SetControlFlags(bool finish)
{
  unsigned int flag = 0x0;
  if (finish)
    flag = flag + 0x4;

  ofstream out(cntrlfile.c_str());
  if (out) {

    out << flag;
    out.close();

  } else {
    throw FileNotExistException("RunSimulation::SetControlFlags: ",cntrlfile, __FILE__, __LINE__);
  }
}

// If 3rd bit is 1, clean up and finish
bool RunSimulation::finish_readfile(void)
{
  bool ans = false;

  ifstream in(cntrlfile.c_str());
  if (in) {
                                // Read the flag
    unsigned int flag;
    in >> flag;
    in.close();
    
    if (flag & 4) {
      ans = true;
                                // Reset the flag (bits 1--3 zeroed)
      flag = flag & 0xFFF8;
      ofstream out(cntrlfile.c_str());
      out << flag << endl;
    }
  }

  return ans;
}

bool RunSimulation::finish(void)
{
  bool ans = false;
                                // Read control file (if MPI, only master
				// reads file and shares with slaves)
  if (mpi_used) {
    int sig = 0;

    if (myid==0) {
      ans = finish_readfile();
      if (ans) sig = 1;
    }

    MPI_Bcast(&sig, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (myid && sig) ans = true;
  }
  else ans = finish_readfile();

  return ans;
}

void RunSimulation::printFrontier(vector<int> frontier_value)
{
    ostream cout(checkTable("simulation_output"));
    vector <int>::iterator     it;

    cout << "---- new frontier is: (";
    for (it=frontier_value.begin();it!=frontier_value.end();it++)
        cout << (*it) << ",";
    cout << ")" << endl;
  
}

void RunSimulation::inputNodes(vector<int> *input_vector)
{
    ostream cout(checkTable("cli_console"));
    int nodeset[31];
    int nodenum;

    input_vector->clear();

    cout << "input the number of nodes:";
    cin >> nodenum;
    cout << "input the nodes selected:";
    for (int i=0; i<nodenum; i++)
        cin >> nodeset[i];

    for (int i=0; i<nodenum; i++)
        input_vector->push_back(nodeset[i]);

}

void RunSimulation::SetTessTool(TessToolSender *tt) 
{
  tesstool = tt;
}

void RunSimulation::SampleNext()
{
}

void RunSimulation::SwitchOnCLI()
{
}

void RunSimulation::SuspendSimulation()
{
}

void RunSimulation::ResumeSimulation()
{
}





