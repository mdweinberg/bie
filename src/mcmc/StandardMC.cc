// This may look like C code, but it is really -*- C++ -*-

#include <sstream>
using namespace std;

#include <LikelihoodComputation.h>
#include <StandardMC.h>
#include <gfunction.h>
#include <MixturePrior.h>
#include <BIEconfig.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::StandardMC)

using namespace BIE;


void StandardMC::GetNewState(Simulation* s, Chain* ch)
{
  s->_prior->SamplePrior(ch->Cur());
  ComputeNewState(s, ch);
}

void StandardMC::ComputeNewState(Simulation* s, Chain *ch)
{
  State state = ch->GetState0();
  int   indx  = ch->indx;

  if (s->Serial())
    ch->prior_value = s->PriorValue(state);

  else {
    int bad;
    if (myid==0) {
      try {
	ch->prior_value = s->PriorValue(ch->GetState0());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
      catch (BIEException& e) {
	cout << "Process " << myid 
	     << ": in StandardMC::ComputeNewState(): "
	     << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	     << e.getErrorMessage();
	throw e;
      }
    }

    MPI_Bcast(&bad, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);
  }    

  ch->like_value  = s->Likelihood(state, indx);

  if (myid==0 || s->Serial())
    ch->value = (ch->like_value + ch->prior_value)*ch->beta;
}


double StandardMC::ComputeCurrentState(Simulation* s, Chain* ch)
{
  State state = ch->GetState0();
  int indx    = ch->indx;

  if (s->Serial())
    ch->prior_value = s->PriorValue(state);
  else {
    int bad;
    if (myid==0) {
      try {
	ch->prior_value = s->PriorValue(ch->GetState0());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
      catch (BIEException& e) {
	cout << "Process " << myid 
	     << ": in StandardMC::ComputeCurrentState(): "
	     << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	     << e.getErrorMessage();
	throw e;
      }
    }

    MPI_Bcast(&bad, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);
  }    

  ch->like_value  = s->Likelihood(state, indx);

  if (myid==0 || s->Serial()) {
    ch->value = (ch->like_value + ch->prior_value)*ch->beta;
    return ch->value;
  } else
    return 0.0;
}



double StandardMC::ComputeState(Simulation* s, Chain* ch)
{
  State state = ch->GetState1();
  int indx =    ch->indx;

  if (s->Serial())
    ch->prior_value1 = s->PriorValue(state);
  else {
    int bad;
    if (myid==0) {
      try {
	ch->prior_value1 = s->PriorValue(state);
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
      catch (BIEException& e) {
	cout << "Process " << myid 
	     << ": in StandardMC::ComputeState(): "
	     << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	     << e.getErrorMessage();
	throw e;
      }
    }

    MPI_Bcast(&bad, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);
  }    

  ch->like_value1  = s->Likelihood(state, indx);

  if (myid==0 || s->Serial()) {
    ch->value1 = (ch->like_value1 + ch->prior_value1)*ch->beta;
    return ch->value1;
  } else
    return 0.0;
}


