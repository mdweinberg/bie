#include <ParallelChains.h>
#include <gfunction.h>
#include <Prior.h>
#include <BIEconfig.h>
#include <BIEMutex.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::ParallelChains)

using namespace BIE;

				// Global parameters
				// -----------------

ParallelChains::Algorithm ParallelChains::algo  = standard;
ParallelChains::Control   ParallelChains::cntrl = parallel;

				// Smallest number of chains to run
int   ParallelChains::minmc		= 6;
				// Powerlaw scaling for temperature space
				// 1.0 is logarithmic (e.g pure geometric)
				// >1.0 weighted towards smaller than geometric
				// <1.0 weighted towards larger than geometric
				// 0.23 is approximately linear from 1 to T_max
				// <0.23 is strongly weighted toward T_max
double ParallelChains::apow             = 1.0;
				// A value of 0.5 is Gaussian scaling . . .
double ParallelChains::tpow		= 0.5;
				// Probability of executing the swap step
double ParallelChains::swapprob		= 0.5;

				// Number of iterations to find a good state
int ParallelChains::state_iter          = 1000;

				// How to assign initial chain state
ParallelChains::Initial ParallelChains::initial 
                                        = ParallelChains::prior_sampled;

void ParallelChains::Initialize(int minmc_p, double maxT)
{
  algoname = "Parallel Chains";

  if (minmc_p) minmc = max<int>(2, minmc_p);

  MaxT = maxT;			// Temperature range for the tempering

				// MaxT=1 and minmc=1 will select straight
				// Metropolis-Hastings

  Mchains = max<int>(minmc, (int)(log(MaxT)*sqrt( static_cast<float>(_si->Ntot))+1));
  cout << "ParallelChains: assigning " << Mchains 
       << " temperature levels\n";

  swapnum = 0;			// Number of accepted swaps
  swaptry = 0;			// Number of attempted swaps
  ncount = 0;

  unit = new Uniform(0.0, 1.0, BIEgen);

  chains_initialized = false;

  caching = false;

  CreateChains();
}

ParallelChains::~ParallelChains()
{
  delete unit;
}

void ParallelChains::NewNumber(int mchains)
{
  if (mchains>0) {
    cout << "ParallelChains::NewNumber: you requested " << mchains << " chains." << endl
	 << "FYI, I predict that you need " << Mchains << " for T=" << MaxT << "." << endl 
	 << "I'm assigning " << mchains << " as specified." << endl;
  
    Mchains = mchains;

    CreateChains();
  }
}


void ParallelChains::CreateChains()

{
				// Erase all previous chains
  chains.clear();

  update_flag .clear();
  update_flag1.clear();

  stat0.clear();
  stat1.clear();

  nstat0.clear();
  nstat1.clear();

  mstat0.clear();
  mstat1.clear();
  
				// Create and initialize new chains
  for (int k=0; k<Mchains; k++) {
    chains.push_back(Chain(_si));

    update_flag.push_back(0);
    update_flag1.push_back(0);

    stat0.push_back(0);
    stat1.push_back(0);

    nstat0.push_back(0);
    nstat1.push_back(0);

    mstat0.push_back(0);
    mstat1.push_back(0);

    chains[k].indx = k;
    chains[k].beta = exp(-log(MaxT)*pow((double)k/(Mchains-1), apow));;
    if (user_widths.size())
      chains[k].factor = user_widths[k];
    else
      chains[k].factor = 1.0/pow(chains[k].beta, tpow);
    
				// Assign ownership
    if (cntrl==serial) {
      chains[k].owner = k % numprocs;
    } else
      chains[k].owner = 0;

				// Logging?
    if (caching) {
      if ( chains[k].owner == myid ) {
	ostringstream name;
	name << nametag << ".chainlog." << k;
	chains[k].Logging(name.str());
      }
    }
  }

				// For reporting . . .
  chfid = &chains[0];
}

void ParallelChains::SetNewWidths(vector<double> *d)
{
  if (static_cast<int>(d->size()) != Mchains) {
    ostringstream ostr;
    ostr << "The input vector dimension <" << d->size()
	 << "> does not match the number of chains <" << Mchains << ">";
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }
  for (int k=0; k<Mchains; k++) chains[k].factor = (*d)[k];
}

void ParallelChains::SetNewWidths(string file)
{
  ifstream in(file.c_str());
  if (!in) {
    throw FileOpenException(file, errno, __FILE__, __LINE__);
  }

  double v;
  vector<double> d;
  while (in.good() || static_cast<int>(d.size())==Mchains) {
    in >> v;
    d.push_back(v);
  }

  if (static_cast<int>(d.size()) != Mchains) {
    ostringstream ostr;
    ostr << "The vector dimension <" << d.size()
	 << "> does not match the number of chains <" << Mchains << ">";
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

  for (int k=0; k<Mchains; k++) chains[k].factor = d[k];
}

void ParallelChains::ResetChainStats()
{
  if (mpi_used && myid) return;

  vector<Chain>::iterator j;
  for (int k=0; k<Mchains; k++) {
    nstat0[k] = 0;
    nstat1[k] = 0;
    mstat0[k] = 0;
    mstat1[k] = 0;
  }
}


void ParallelChains::NewState(int M, vector<double>& wght, 
			      vector< vector<double> >& phi)
{
  State s(_si);
  s.setState(M, wght, phi);
  
  if (myid==0)  chfid->AssignState0(s);
  if (mpi_used) chfid->BroadcastChain();

  Initialize();
}

void ParallelChains::NewState(Chain &ch)
{ 
  chfid->AssignState0(ch);
  if (mpi_used) chfid->BroadcastChain();
  Initialize();
}


void ParallelChains::NewState(State& s) 
{ 
  if (myid==0) chains[0].AssignState0(s);

  chains_initialized = false;
  Initialize();
}


void ParallelChains::Initialize()
{
  if (chains_initialized) return;

  switch(cntrl) {

  case parallel:		// Root initiates assignement
				// but likelihood is computed in parallel
    for (int k=0; k<Mchains; k++) SampleNewStateParallel(k);
    break;

  case serial:

				// Owner initiates assignement and
				// computes likelihood
    for (int k=0; k<Mchains; k++) SampleNewStateSerial(k);
    
				// Share the newly initialized chains with
    if (mpi_used)		// all procs
      for (int k=1; k<Mchains; k++) chains[k].BroadcastChain();
    
    break;

  default:
    ostringstream out;
    out << "We don't know about the control type #" << cntrl;
    
    throw InternalError(out.str(), __FILE__, __LINE__);
  }
  
  chains_initialized = true;

#ifdef DEBUG2
  ostringstream sout;
  sout << "debug_chain_init." << myid;
  ofstream out(sout.str().c_str());
  for (int k=0; k<Mchains; k++) {
    out << endl << "Chain " << k << endl;
    chains[k].PrintStats(out);
    chains[k].PrintState(out);
    chains[k].PrintState1(out);
  }
#endif


}

void ParallelChains::Reinitialize(MHWidth* width, Prior *mp)
{

  // reset state with new info
  //
  Simulation::Reinitialize(width, mp);

  // don't reset minmc because it shouldn't be changing between levels
  // don't reset Mchains because it shouldn't be changing size
  // don't reset beta because it is although initialized the values do
  // not change and it does not change size
  
  chains_initialized = false;
  Initialize();
  ResetChainStats();
    
  swapnum = 0;			// Number of accepted swaps
  swaptry = 0;			// Number of attempted swaps
  ncount  = 0;
}


void ParallelChains::SampleNewStateParallel(int k)
{  
  const string member_name = "ParallelChains::SampleNewStateParallel: ";

  if (initial==user_supplied) {

    if (myid==0 && k!=0) chains[k].AssignState0(chains[0]);	
    try {
      _mca->ComputeNewState(this, &chains[k]);
    }
    catch (ImpossibleStateException &e) {
      ostringstream out;
      out << member_name
	  << "user-supplied initial state is bad.";

      throw InternalError(-66, out.str(), __FILE__, __LINE__);
    }
    
  } else if (initial==prior_sampled) {

    int niter = 0;
    bool bad = true;

    while (bad) {

      try {
	if (myid==0 && (k!=0 || niter)) 
	  _prior->SamplePrior(chains[k].Cur());
	_mca->ComputeNewState(this, &chains[k]);
	bad = false;
      }
      catch (ImpossibleStateException &e) {
	niter++;
	if (niter > state_iter) {
	  ostringstream out;
	  out << member_name
	      << "failed to produce a good initial state after "
	      << niter << " attempts.";
	  
	  throw InternalError(-66, out.str(), __FILE__, __LINE__);
	}
      }
    }

  } else {
    ostringstream out;
    out << member_name
	<< "undefined chain-state initialization type.";

    throw InternalError(-67, out.str(), __FILE__, __LINE__);
  }
    
  if (mpi_used) chains[k].BroadcastChain();

}


void ParallelChains::NewStateSerialInit()
{
				// Share the fiducial chain with all procs
				//
  if (mpi_used) chains[0].BroadcastChain();

				// Status list
				//
  fail = vector<unsigned short>(Mchains, 0);
}

void ParallelChains::NewStateSerialStatus()
{  
  const string member_name = "ParallelChains::SampleNewStateSerial: ";

  vector<unsigned short> fail0(Mchains, 0);
  if (mpi_used)
    MPI_Allreduce(&fail[0], &fail0[0], Mchains, MPI_UNSIGNED_SHORT, MPI_SUM,
		  MPI_COMM_WORLD);
  else
    fail0 = fail;

  unsigned ncnt = 0;
  for (int k=0; k<Mchains; k++) ncnt += fail0[k];

  if (ncnt) {
    ostringstream out;
    out << member_name
	<< ncnt << " chains ";
    switch(initial) {
    case user_supplied:
    case prior_sampled:
      out << "failed to produce a good initial state after "
	  << state_iter << " attempts.";
      break;
    default:
      out << "undefined chain-state initialization type.";
    }
    throw InternalError(-71, out.str(), __FILE__, __LINE__);
  }

  if (mpi_used) {
    for (int k=0; k<Mchains; k++) chains[k].BroadcastChain();
  }

}


void ParallelChains::SampleNewStateSerial(int k)
{  
  if (k==0) NewStateSerialInit();

				// The chain owner attempts to find an
				// initial state
  if (myid==chains[k].owner) {
  
    if (initial==user_supplied) {

      if (k) chains[k].AssignState0(chains[0]);	
      try {
	_mca->ComputeNewState(this, &chains[k]);
      }
      catch (ImpossibleStateException &e) {
	fail[chains[k].owner] = 1;
      }
    
    } else if (initial==prior_sampled) {

      int niter = 0;
      bool bad = true;

      while (bad) {

	try {
	  if (k || niter) 
	    _prior->SamplePrior(chains[k].Cur());
	  _mca->ComputeNewState(this, &chains[k]);
	  bad = false;
	}
	catch (ImpossibleStateException &e) {
	  if (niter++ > state_iter) {
	    fail[chains[k].owner] = 1;
	    break;
	  }
	}
      }
      
    } else {
      fail[chains[k].owner] = 1;
    }

  }
    
  if (k==Mchains-1) NewStateSerialStatus();
}


void ParallelChains::MCMethod()
{
#ifdef DEBUG2
  bool firstime = true;
#endif

  MPI_Status status;
  int k, ok0, ok1, ok;
  double prob, logprob0=0, logprob1, logprob;

  ncount++;

  switch (algo) {

  case standard:

    // Are we proposing a swap?

    prob = (*unit)();
				// Broadcast the result
    if (mpi_used) {
      MPI_Bcast(&prob, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }

    if ( prob < swapprob ) {
	
				// Choose the proposed swap state
      if (myid==0)
	k = min<int>(Mchains-1, 1+(int)floor((*unit)()*(Mchains-1)));
      
				// Broadcast the result
      if (mpi_used) {
	++MPIMutex;
	MPI_Bcast(&k, 1, MPI_INT, 0, MPI_COMM_WORLD);
	--MPIMutex;
      }

      if (myid==0) {		// "Swap attempted" counters
	nstat0[k-1]++;
	nstat0[k  ]++;
      }
				
				// ==============================
      if (cntrl == parallel) {	// cntrl==parallel
				// ==============================

				// Handle the bad state exceptions
	try {
	
	  logprob = 0.0;
				// Colder level, warmer state
				//
	  if (myid==0)  chains[k-1].AssignState1(chains[k]);
	  if (mpi_used) chains[k-1].BroadcastChain();
	  logprob0 = 
	    _mca->ComputeState(this, &chains[k-1]) - chains[k-1].value;

	  logprob += logprob0;

				// Warmer level, colder state
				//
	  if (myid==0)  chains[k].AssignState1(chains[k-1]);
	  if (mpi_used) chains[k].BroadcastChain();
	  logprob1 = 
	    _mca->ComputeState(this, &chains[k]) - chains[k].value;

	  logprob += logprob1;
	    
	  if (myid==0) {

	    double accept_prob = min<double>(1.0, exp(logprob));
	    
	    swaptry++;

	    if (accept_prob >= (*unit)()) {
				// Do the swap
	      chains[k-1].AcceptState();
	      chains[k  ].AcceptState();

				// Keep tally of swaps
	      nstat1[k-1]++;
	      nstat1[k  ]++;
	      swapnum++;

#ifdef DEBUG2
	      if (k-1==0) {
		cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
		cout << "Proposal to exchange Chains 0 & 1 is ";
		cout << "ACCEPTED!\n";
		cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
	      }
#endif

	    } else {
#ifdef DEBUG2
	      if (k-1==0) {
		cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
		cout << "Proposal to exchange Chains 0 & 1 is ";
		cout << "REJECTED! (" << logprob << ", " 
		     << logprob0 << ", " << logprob1 << ")\n";
		cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
	      }
#endif
	    }

	  }

	}
	catch (ImpossibleStateException &e) {
#ifdef DEBUG
	  cerr << "ParallelChains: bad state in exchange" << endl
	       << e.getErrorMessage() << "... process " << myid 
	       << " continuing" << endl;
#endif
	}

				// ==============================
      } else {			// cntrl==serial
				// ==============================

	if (myid==chains[k-1].owner) {
				// Colder level, warmer state
				//
	  chains[k-1].AssignState1(chains[k]);
	  try {
	    logprob0 = _mca->ComputeState(this, &chains[k-1]) - 
	      chains[k-1].value;
	    ok0 = 1;
	  }
	  catch (ImpossibleStateException &e) {
	    ok0 = 0;
	  }
	  
	  ++MPIMutex;
	  MPI_Recv(&ok1, 1, MPI_INT, chains[k].owner, 
		   33, MPI_COMM_WORLD, &status);
	  if (ok1)
	    MPI_Recv(&logprob1, 1, MPI_DOUBLE, chains[k].owner, 
		       34, MPI_COMM_WORLD, &status);
	  --MPIMutex;
	    
	  if (ok0 && ok1) {
	    logprob = logprob0 + logprob1;
	    double accept_prob = min<double>(1.0, exp(logprob));
	    
	    if (accept_prob >= (*unit)()) {
	      ok = 1;
	    } else {
	      ok = 0;
	    }
	      
	  } else {
	    ok = 0;
	  }
	    
				// Broadcast the swap status
	  ++MPIMutex;
	  MPI_Bcast(&ok, 1, MPI_INT, chains[k-1].owner, MPI_COMM_WORLD);
	  --MPIMutex;
	    
	  if (ok) chains[k-1].AcceptState();

#ifdef DEBUG2
	  if (k-1==0) {
	    cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
	    cout << "Proposal to exchange Chains 0 & 1 is ";
	    if (ok) cout << "ACCEPTED!\n";
	    else    cout << "REJECTED! (" << logprob << ", " 
			 << logprob0 << ", " << logprob1 << ")\n";
	    cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
	  }
#endif

	} else if (myid==chains[k].owner) {
				// Warmer level, colder state
				//
	  chains[k].AssignState1(chains[k-1]);
	  try {
	    logprob1 = _mca->ComputeState(this, &chains[k]) - chains[k].value;
	    ok1 = 1;
	  } catch (ImpossibleStateException &e) {
	    ok1 = 0;
	  }

	  ++MPIMutex;
	  MPI_Send(&ok1, 1, MPI_INT, chains[k-1].owner, 
		     33, MPI_COMM_WORLD);
	  if (ok1) 
	    MPI_Send(&logprob1, 1, MPI_DOUBLE, chains[k-1].owner, 
		     34, MPI_COMM_WORLD);
	    
	  MPI_Bcast(&ok, 1, MPI_INT,  chains[k-1].owner, MPI_COMM_WORLD);
	  --MPIMutex;
	  
	  if (ok) chains[k].AcceptState();

	} else {
	  ++MPIMutex;
	  MPI_Bcast(&ok, 1, MPI_INT,  chains[k-1].owner, MPI_COMM_WORLD);
	  --MPIMutex;
	}

				// Send new states for the swapped chains
	swaptry++;
	if (ok) {
#ifdef DEBUG2
	  if (firstime) {
	    ostringstream sout;
	    sout << "debug_chain." << myid;
	    ofstream out(sout.str().c_str());
	    out << endl << "Chain " << k-1 << " [before]" << endl;
	    chains[k-1].PrintStats(out);
	    chains[k-1].PrintState(out);
	    chains[k-1].PrintState1(out);

	    out << endl << "Chain " << k   << " [before]" << endl;
	    chains[k  ].PrintStats(out);
	    chains[k  ].PrintState(out);
	    chains[k  ].PrintState1(out);
	  }
#endif
	  chains[k-1].BroadcastChain();
	  chains[k  ].BroadcastChain();
#ifdef DEBUG2
	  if (firstime) {
	    ostringstream sout;
	    sout << "debug_chain." << myid;
	    ofstream out(sout.str().c_str(), ios::app);
	    out << endl << "Chain " << k-1 << " [after]" << endl;
	    chains[k-1].PrintStats(out);
	    chains[k-1].PrintState(out);
	    chains[k-1].PrintState1(out);

	    out << endl << "Chain " << k   << " [after]" << endl;
	    chains[k  ].PrintStats(out);
	    chains[k  ].PrintState(out);
	    chains[k  ].PrintState1(out);

	    firstime = false;
	  }
#endif

	  if (myid==0) {	// Only root keeps the statistics
	    nstat1[k-1]++;
	    nstat1[k  ]++;
	    swapnum++;
	  }
	}
	  
      }
				// No, update chains
    } else {
      
      if (cntrl == parallel) {
	
	for (int k=0; k<Mchains; k++) {
	  if (myid==0)  _prior->SampleProposal(chains[k], _mhwidth);
	  if (mpi_used) chains[k].BroadcastChain();
	  try {
	    mstat0[k]++;
	    if (_mca->Sweep(this, &chains[k])) mstat1[k]++;
	  } 
	  catch (ImpossibleStateException &e) {
#ifdef DEBUG
	    cerr << "ParallelChains: bad state in update" << endl
		 << e.getErrorMessage() << "... continuing"
		 << endl;
#endif
	  }
	}
	
      } else {			// cntrl==serial

	for (int k=0; k<Mchains; k++) update_flag[k] = update_flag1[k] = 0;

	for (int k=0; k<Mchains; k++) {
	  if (chains[k].owner == myid) {
	    _prior->SampleProposal(chains[k], _mhwidth);
	    try {
	      if (_mca->Sweep(this, &chains[k])) update_flag1[k] = 1;
	    }
	    catch (ImpossibleStateException &e) {
#ifdef DEBUG
	      cerr << "ParallelChains: bad state in update" << endl
		   << e.getErrorMessage() << "... continuing"
		   << endl;
#endif
	    }
	  }
	}

	++MPIMutex;
	MPI_Allreduce(&update_flag1[0], &update_flag[0], Mchains, 
		      MPI_UNSIGNED_CHAR, MPI_SUM, MPI_COMM_WORLD);
	--MPIMutex;
	
	for (int k=0; k<Mchains; k++) {
	  if (update_flag[k]) chains[k].BroadcastChain();
	  if (myid==0) {
	    mstat0[k]++;
	    if (update_flag[k]) mstat1[k]++;
	  }
	}
	
      }
	
    }

    break;

  case hierarchical:

				// Choose the proposed swap state
    k = min<int>(Mchains-1, 1+(int)floor((*unit)()*(Mchains-1)));

				// Broadcast the result
    if (mpi_used) {
      ++MPIMutex;
      MPI_Bcast(&k, 1, MPI_INT, 0, MPI_COMM_WORLD);
      --MPIMutex;
    }

    if (myid==0) {
      nstat0[0]++;
      nstat0[k]++;
    }

				// ==============================
    if (cntrl == parallel) {	// cntrl==parallel
				// ==============================

      try {
	logprob = 0.0;
				// Cold level, warmer state
				//
	if (myid==0)  chains[0].AssignState1(chains[k]);
	if (mpi_used) chains[0].BroadcastChain();
	logprob += 
	  _mca->ComputeState(this, &chains[0]) - chains[0].value;

				// Warmer level, cold state
				//
	if (myid==0)  chains[k].AssignState1(chains[0]);
	if (mpi_used) chains[k].BroadcastChain();
	logprob += 
	  _mca->ComputeState(this, &chains[k]) - chains[k].value;
	
	if (myid==0) {

	  double accept_prob = min<double>(1.0, exp(logprob));

	  swaptry++;

	  if (accept_prob >= (*unit)()) {
				// Do the swap
	    chains[0].AcceptState();
	    chains[k].AcceptState();
				// Keep tally
	    nstat1[0]++;
	    nstat1[k]++;
	    swapnum++;
	  }
	}
      }
      catch (ImpossibleStateException &e) {
#ifdef DEBUG
	cerr << "ParallelChains: bad state in exchange" << endl
	     << e.getErrorMessage() << "... process " << myid 
	     << " continuing" << endl;
#endif
      }



      // Update the remaining states

      for (int j=1; j<Mchains; j++) {
	if (j==k)     continue;
	if (myid==0)  _prior->SampleProposal(chains[j], _mhwidth);
	if (mpi_used) chains[j].BroadcastChain();
	try {
	  mstat0[j]++;
	  if (_mca->Sweep(this, &chains[j])) mstat1[j]++;
	}
	catch (ImpossibleStateException &e) {
#ifdef DEBUG
	  cerr << "ParallelChains: bad state in update" << endl
	       << e.getErrorMessage() << "... process " << myid 
	       << " continuing" << endl;
#endif
	}
      }
				// ==============================
    } else {			// cntrl==serial
				// ==============================

      for (int j=0; j<Mchains; j++) update_flag[j] = update_flag1[j] = 0;

      if (myid==0) {
				// Cold level, warmer state
				//
	chains[0].AssignState1(chains[k]);
	try {
	  logprob0 = _mca->ComputeState(this, &chains[0]) - chains[0].value;
	  ok0 = 1;
	}
	catch (ImpossibleStateException &e) {
	  ok0 = 0;
	}
	
	if (chains[k].owner != 0) {
	  ++MPIMutex;
	  MPI_Recv(&ok1, 1, MPI_INT, chains[k].owner, 33, 
		   MPI_COMM_WORLD, &status);
	  if (ok1)
	    MPI_Recv(&logprob1, 1, MPI_DOUBLE, chains[k].owner, 34, 
		     MPI_COMM_WORLD, &status);
	  --MPIMutex;
	  
	} else {		// Warmer level, cold state
				//
	  chains[k].AssignState1(chains[0]);
	  try {
	    logprob1 = _mca->ComputeState(this, &chains[k]) - chains[k].value;
	    ok1 = 1;
	  }
	  catch (ImpossibleStateException &e) {
	    ok1 = 0;
	  }
	}


	if (ok0==1 && ok1==1) {
	  logprob = logprob0 + logprob1;
	  double accept_prob = min<double>(1.0, exp(logprob));

	  if (accept_prob >= (*unit)()) {
	    ok = 1;
	  } else {
	    ok = 0;
	  }

	} else {
	  ok = 0;
	}

#ifdef DEBUG2
	cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
	cout << "Proposal to exchange Chains 0 & 1 is ";
	if (ok) cout << "ACCEPTED!";
	else    cout << "REJECTED!";
	cout << " (" << logprob << ", " 
	     << logprob0 << ", " << logprob1 << ")\n";
	cout << setw(72) << setfill('-') << '-' << endl << setfill(' ');
#endif

	if (chains[k].owner != 0) 
	  {
	    ++MPIMutex;
	    MPI_Send(&ok, 1, MPI_INT, chains[k].owner, 35, MPI_COMM_WORLD);
	    --MPIMutex;
	  }
	
	swaptry++;

	if (ok) {
	  chains[0].AcceptState();
	  update_flag1[0] = 1;
	  update_flag1[k] = 1;
	  if (myid==0) {
	    nstat1[0]++;
	    nstat1[k]++;
	    swapnum++;
	  }
	}
      }

      				// Root is not owner of chain k
				// 
      if (myid>0 && myid==chains[k].owner) {
				// Warmer level, cold state
				//
	chains[k].AssignState1(chains[0]);
	try {
	  logprob1 = _mca->ComputeState(this, &chains[k]) - chains[k].value;
	  ok1 = 1;
	}
	catch (ImpossibleStateException &e) {
	  ok1 = 0;
	}


	++MPIMutex;
	MPI_Send(&ok1, 1, MPI_INT, 0, 33, MPI_COMM_WORLD);
	if (ok1)
	  MPI_Send(&logprob1, 1, MPI_DOUBLE, 0, 34, MPI_COMM_WORLD);

	MPI_Recv(&ok, 1, MPI_INT, 0, 35, MPI_COMM_WORLD, &status);
	--MPIMutex;

	if (ok) {
	  chains[k].AcceptState();
	}
      }
      

      // Update the remaining states

      for (int j=1; j<Mchains; j++) {
	if (j==k) continue;
	if (chains[j].owner == myid) {
	  _prior->SampleProposal(chains[j], _mhwidth);
	  try {
	    if (_mca->Sweep(this, &chains[j])) update_flag1[j] = 1;
	  }
	  catch (ImpossibleStateException &e) {
#ifdef DEBUG
	    cerr << "ParallelChains: bad state in update" << endl
		 << e.getErrorMessage() << "... continuing"
		 << endl;
#endif
	  }
	}
      }
      
      ++MPIMutex;
      MPI_Allreduce(&update_flag1[0], &update_flag[0], Mchains, 
		    MPI_UNSIGNED_CHAR, MPI_SUM, MPI_COMM_WORLD);
      --MPIMutex;
	
      for (int k=0; k<Mchains; k++) {
	if (update_flag[k]) chains[k].BroadcastChain();
	if (myid==0) {
	  mstat0[k]++;
	  if (update_flag[k]) mstat1[k]++;
	}
      }

    }

    break;

  default:
    
    ostringstream out;
    out << "We don't know about the algorithm #" << algo;

    throw InternalError(out.str(), __FILE__, __LINE__);
  }

  getChainCounts();

  //
  // Write all chain states
  //
  for (int j=0; j<Mchains; j++) {
    if (chains[j].owner == myid) chains[j].WriteLog();
  }

}


void ParallelChains::getChainCounts()
{
  if (!mstat) return;

  if (cntrl == serial) {

    vector<unsigned long> stat0T(Mchains, 0);
    vector<unsigned long> stat1T(Mchains, 0);
    
    for (int k=0; k<Mchains; k++) {
      if (chains[k].owner == myid) {
	stat0T[k] = chains[k].stat[0];
	stat1T[k] = chains[k].stat[1];
      }
    }
    
    MPI_Reduce(&stat0T[0], &stat0[0], Mchains, MPI_UNSIGNED_LONG, MPI_SUM, 
	       0, MPI_COMM_WORLD);

    MPI_Reduce(&stat1T[0], &stat1[0], Mchains, MPI_UNSIGNED_LONG, MPI_SUM, 
	       0, MPI_COMM_WORLD);

  }

  if (cntrl == parallel) {

    for (int k=0; k<Mchains; k++) {
      if (chains[k].owner == myid) {
	stat0[k] = chains[k].stat[0];
	stat1[k] = chains[k].stat[1];
      }
    }

  }

}


vector<double> ParallelChains::GetMixstat(void)
{
  vector<double> ret;

  if (mpi_used && myid) return ret;

  for (int k=0; k<Mchains; k++) {
    if (nstat0[k]==0) ret.push_back(0.0);
    else              ret.push_back((double)nstat1[k]/(double)nstat0[k]);
  }

  return ret;
}

vector<double> ParallelChains::GetSweepstat(void)
{
  vector<double> ret;

  if (mpi_used && myid) return ret;

  for (int k=0; k<Mchains; k++) {
    if (mstat0[k]==0) ret.push_back(0.0);
    else              ret.push_back((double)mstat1[k]/(double)mstat0[k]);
  }

  return ret;
}

void ParallelChains::PrintStepDiagnostic()
{
  ostream cout(checkTable("simulation_output"));

  // could access all of these state variables directly...
  //
  cout << "           swaps=" << swapnum << "/" << swaptry;
  Simulation::PrintStepDiagnostic();
  
}

void ParallelChains::PrintStateDiagnostic()
{
  if (mstat) {
    ostream cout(checkTable("simulation_output"));

    vector<double> ret = GetMixstat();
    vector<double> swp = GetSweepstat();

    cout << endl << setw(20) << " Acceptance tally (per chain):" << endl;
    for (int k=0; k<(int)stat0.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	ostringstream out;
	out << stat1[k] << "/" << stat0[k];
	cout << " " << setw(9) << out.str();
	if (++k>=(int)stat0.size()) break;
      }
      cout << endl;
    }


    cout << endl << setw(20) << "P(swap) per chain:" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	cout << " " << setw(9) << setprecision(2) << fixed << ret[k];
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }

    cout << endl << setw(20) << "P(update) per chain:" << endl;
    for (int k=0; k<(int)swp.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	cout << " " << setw(9) << setprecision(2) << fixed << swp[k];
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }

    cout << endl << setw(20) << " Posterior values (per chain):" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	cout << " " << setw(9) << setprecision(2) << scientific 
	     << chains[k].value;
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }

    if (ncount > state_diag) {

      vector<double> c1(3, 0), c2(3, 0);
      double r1 = 1.0/ret.size(), r2 = 1.0/swp.size();

      cout << endl << setw(20) << "Summary:" << endl;

      for (int k=0; k<(int)ret.size(); k++) {
	if (ret[k]>0.1)       c1[0] += r1;
	else if (ret[k]>0.02) c1[1] += r1;
	else                  c1[2] += r1;
      }

      for (int k=0; k<(int)swp.size(); k++) {
	if (swp[k]>0.1)       c2[0] += r2;
	else if (swp[k]>0.02) c2[1] += r2;
	else                  c2[2] += r2;
      }
    
      if (ret[0] > 0.1)
	cout << setw(20) << " " 
	     << "Fiducial chain swapping is excellent!" << endl;
      else if (ret[0] > 0.02)
	cout << setw(20) << " " 
	     << "Fiducial chain swapping is mediocre but ok" << endl;
      else 
	cout << setw(20) << " " 
	     << "Fiducial chain swapping is horrible" << endl
	     << setw(25) << " "
	     << "==> Do something!" << endl;

      if (c1[0]>0.5 && c1[2]<0.25)
	cout << setw(20) << " " 
	     << "Interchain swapping is excellent!" << endl;
      else if (c1[1]>0.5)
	cout << setw(20) << " " 
	     << "Interchain swapping is mediocre but ok" << endl;
      else 
	cout << setw(20) << " " 
	     << "Interchain swapping is horrible" << endl
	     << setw(20) << " " << "[good=" << c1[0] 
	     << " soso=" << c1[1] << " vbad=" << c1[2] << "]" << endl
	     << setw(25) << " "
	     << "==> Do something!" << endl
	     << setw(25) << " "
	     << "    Change temperature or chain #!" << endl;

      if (c2[0]>0.5 && c2[2]<0.25)
	cout << setw(20) << " " 
	     << "In-chain acceptance is excellent!" << endl;
      else if (c2[1]>0.5)
	cout << setw(20) << " " 
	     << "In-chain acceptance is mediocre but ok" << endl;
      else 
	cout << setw(20) << " " 
	     << "In-chain acceptance is horrible" << endl
	     << setw(20) << " " << "[good=" << c2[0]
	     << " soso=" << c2[1] << " vbad=" << c2[2] << "]" << endl
	     << setw(25) << " "
	     << "==> Do something!" << endl
	     << setw(25) << " "
	     << "    Try decreasing the proposal width" << endl;
      
    }

  }

  // Algorithm specific info; may be blank
  //
  _mca->PrintAlgorithmDiagnostics(this);
}


void ParallelChains::SetAlgorithm(int m)
{
  switch(m) {
  case standard:
    algo = standard;
    break;
  case hierarchical:
    algo = hierarchical;
    break;
  default:
    ostringstream out;
    out << "Algorithm should be <standard> (" << standard 
	<< ") or <hierarchical> (" << hierarchical << ")."
	<< "  input value was " << m;
    throw InternalError(out.str(), __FILE__, __LINE__);
  }
}

void ParallelChains::SetControl(int m)
{
  switch(m) {
  case parallel:
    cntrl = parallel;
    break;
  case serial:
    if (mpi_used)
      cntrl = serial;
    else {
      cout << "ParallelChains: you want _serial_ control but you must"   << endl
	   << "                use MPI for that to make any sense.  You" << endl
	   << "                are getting _parallel_ control."          << endl;
    }
    break;
  default:
    ostringstream out;
    out << "Control method should be <parallel> (" << parallel
	<< ") or <serial> (" << serial << ")."
	<< "  input value was " << m;
    throw InternalError(out.str(), __FILE__, __LINE__);
  }

  CreateChains();
}

void ParallelChains::SetInitial(int m)
{
  switch(m) {
  case user_supplied:
    initial = user_supplied;
    break;
  case prior_sampled:
    initial = prior_sampled;
    break;
  default:
    ostringstream out;
    out << "Initialization type should be <user_supplied> (" << user_supplied
	<< ") or <prior_sampled> (" << prior_sampled << ")."
	<< "  input value was " << m;
    throw InternalError(out.str(), __FILE__, __LINE__);
  }

  if (chains_initialized) {
    chains_initialized = false;
    Initialize();
  }
}

double ParallelChains::GetValue() 
{ 
  Initialize(); 
  return chains[0].value; 
}
    
double ParallelChains::GetPrior() 
{
  Initialize(); 
  return chains[0].prior_value; 
}
    
double ParallelChains::GetLikelihood() 
{ 
  Initialize(); 
  return chains[0].like_value; 
}

State ParallelChains::GetState() 
{ 
  Initialize(); 
  return chains[0].GetState0(); 
}
    
void ParallelChains::ReportState() 
{ 
  if (mpi_used && myid && cntrl==parallel) return;
  Simulation::ReportState(chains[0].GetState0());
}

void ParallelChains::AdditionalInfo()
{
  if (myid) return;

  cout << "Chain report" << endl << left
       << setw(6)  << "#"
       << setw(6)  << "indx"
       << setw(18) << "Beta"
       << setw(6)  << "Mcur"
       << setw(18) << "Value"
       << setw(6)  << "Mcur1"
       << setw(18) << "Value1"
       << endl;

  for (int i=0; i<Mchains; i++) {
    cout << left
	 << setw(6)  << i 
	 << setw(6)  << chains[i].indx
	 << setw(18) << chains[i].beta
	 << setw(6)  << chains[i].M0()
	 << setw(18) << chains[i].value
	 << setw(6)  << chains[i].M1()
	 << setw(18) << chains[i].value1
	 << endl;
  }

}
