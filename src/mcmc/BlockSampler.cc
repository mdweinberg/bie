#include <sstream>

#include <gfunction.h>
#include <BIEconfig.h>
#include <BIEMutex.h>
#include <FileExists.H>

#include <Prior.h>
#include <gfunction.h>
#include <MixturePrior.h>
#include <BIEconfig.h>
#include <BlockSampler.h>
#include <LikelihoodComputation.h>

using namespace std;
using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BlockSampler)

BlockSampler::BlockSampler(StateInfo* si, std::vector<Simulation*> simV)
{
  sims       = simV;	    // Assign the vector of simulations
  nBlocks    = sims.size(); // Cache the number of blocks for convenience
  _si        = si;
  _blocking  = true;

  for (simVecItr i=sims.begin(); i!=sims.end(); i++) (*i)->_blocking = true;
}

BlockSampler::BlockSampler(StateInfo* si, clivectorsim* simV)
{
  sims       = (*simV)();	// Assign the vector of simulations
  nBlocks    = sims.size();	// Cache the number of blocks for convenience
  _si        = si;
  _blocking  = true;

  for (simVecItr i=sims.begin(); i!=sims.end(); i++) (*i)->_blocking = true;
}

void BlockSampler::MCMethod()
{
				// Cycle through the blocks
  for (size_t n=0; n<nBlocks; n++) {
				// Set the state to block n
    ch.Cur()->bIndex() = n;
    sims[n]->MCMethod();
  }
  ch.Cur()->bIndex() = -1;	// Reset
}


void BlockSampler::PrintStateDiagnostic() 
{
				// Cycle through the blocks
  for (size_t n=0; n<nBlocks; n++) {
				// Set the state to block n
    ch.Cur()->bIndex() = n;
    sims[n]->PrintStateDiagnostic();
  }
  ch.Cur()->bIndex() = -1;	// Reset
}

void BlockSampler::ResetDiagnostics() 
{
				// Cycle through the blocks
  for (size_t n=0; n<nBlocks; n++) {
				// Set the state to block n
    ch.Cur()->bIndex() = n;
    sims[n]->ResetDiagnostics();
  }
  ch.Cur()->bIndex() = -1;	// Reset
}
