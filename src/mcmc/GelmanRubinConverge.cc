/* 
   Analyzes convergence for multiple chains following Gelman and Rubin
   (1992), Gelman and Brooks (1998), Gelman et al. (2004) 
*/

#include <cstdlib>
#include <cfloat>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>

using namespace std;

#include <string>

#include <gvariable.h>
#include <Distribution.h>
#include <GelmanRubinConverge.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GelmanRubinConverge)

using namespace BIE;
                                // Number between tests
unsigned GelmanRubinConverge::nskip = 100;

                                // Number of converged states
unsigned GelmanRubinConverge::ngood = 1000;

                                // Threshold
double GelmanRubinConverge::rtol = 1.2;

                                // Outlier confidence
double GelmanRubinConverge::alpha = 0.05;

                                // Maximum number of outliers
int GelmanRubinConverge::maxoutlier = 6;

                                // First attempt to detect outliers
int GelmanRubinConverge::noutlier = 500;

                                // Required min offset in log(P) for
                                // trimming outliers
double GelmanRubinConverge::poffset = -30.0;

                                // Diagnostic output
bool GelmanRubinConverge::verbose = true;

                                // Additional debugging output
bool GelmanRubinConverge::debug = true;

                                // Number of instances created so far
unsigned GelmanRubinConverge::ninstance = 0;

                                // Maximum number of states to keep if maxit=0
int GelmanRubinConverge::maxkp = 100000;


GelmanRubinConverge::GelmanRubinConverge(int m, Ensemble* d, string id) : Converge(d)
{
  maxit  = abs(m);
  next   = std::max<int>(nskip, maxit);
  nconv  = -1;
  _id    = id;
  ninstance++;
}


GelmanRubinConverge* GelmanRubinConverge::New(int m, Ensemble* d, string id)
{
  cerr << "New instance of GelmanRubinConverge created" << endl;
  return new GelmanRubinConverge(m, d, id);
}

bool GelmanRubinConverge::AccumData(vector< vector<double> >& values, 
                                    vector<State> & states)
{
  if (chain_mask.size() != values.size()) 
    chain_mask = vector<unsigned char>(values.size(), 0);

  for (unsigned n=0; n<values.size(); n++) {
    // if (chain_mask[n]) continue;
    _dist->AccumData(values[n], states[n]);
  }

  /*
    Strategy:   analysis performed every nskip steps

    maxit==0            Means last half of sample will be used
                        once count is greater than nskip

    maxit>0             Means that maxit states will be dumped
                        before before testing, regardless of nskip.
                        maxit states retained for analysis
  */
  data.push_back(states);
  prob.push_back(values[0]);
  count++;

  if (maxit>0) {
    if (static_cast<int>(data.size())>maxit) {
      data.pop_front();
      prob.pop_front();
    }
  } else {
    while (static_cast<int>(data.size())>count/2) {
      data.pop_front();
      prob.pop_front();
    }
    while (static_cast<int>(data.size())>maxkp) {
      data.pop_front();
      prob.pop_front();
    }
  }

  return true;
}

/*
  Grubbs' for outlier detection.

  Grubbs' test (Grubbs 1969 and Stefansky 1972) is used to detect
  outliers in a univariate data set. It is based on the assumption of
  normality.  This should be the case for converged MCMC calc.
*/
int GelmanRubinConverge::ComputeMaskAll()
{
  vector<State> z = data.back();
  unsigned n = z.size();
  unsigned m = z.front().size();
  vector<unsigned char> new_mask(n, 0);
  vector<double> prb = prob.back();
  int cnt=0;

  double maxLP = -1.0e20;
  for (unsigned i=0; i<n; i++) maxLP = max<double>(maxLP, prb[i]);

  ofstream out;
  if (verbose) {
    ostringstream sout, sout1;
    sout << "---- Grubbs' outlier tests, Count=" << count 
         << " Instance=" << ninstance << " ";  
    sout1 << nametag << ".GRConverge";
    out.open(sout1.str().c_str(), ios::app);
    out << setw(72) << setfill('-') << "-" << endl
        << setw(72) << setfill('-') << left << sout.str() << endl 
        << setw(72) << setfill('-') << "-" << endl
        << setfill(' ');
  }

  //
  // Do each variable separately
  //
  for (unsigned q=0; q<m; q++) {
                                // P is the sorted list of (value, index) pairs
    vector< pair<double, int> > p;
    for (unsigned i=0; i<n; i++) {
      if (new_mask[i]==0)
        p.push_back(pair<double, int>(z[i][q], i));
    }
  
                                // Compute the Student statistic for 
                                // k-cnt possible outliers
    double val, pcrit, t, G, gmax;
    unsigned sz=p.size();
    unsigned k = sz*3/4;
    if (k<2) break;

    double mean, var;

    for (unsigned j=0; j<k; j++) {
      mean = var = 0.0;         // Compute mean and variance 
                                // for the remaining sample
      for (unsigned i=0; i<sz; i++) {
        val = z[p[i].second][q];
        mean += val/sz;
        var  += val*val/(sz-1);
      }
      var -= mean*mean*sz/(sz-1);

      if (var<1.0e-18) break;

      for (unsigned i=0; i<sz; i++) p[i].first = fabs(z[p[i].second][q]-mean);
      std::sort(p.begin(), p.end());

                                // The supremum value
      G = p.back().first / sqrt(var);

                                // 2-sided Grubb's test to find outliers
      pcrit = 1.0 - (alpha*0.5/sz);
    
      t = inv_student_t_1sided(pcrit, sz-2);

      gmax = t*(sz-1)/sqrt(((t*t + (sz-2))*sz));

                                // Report
      if (verbose) {
        if (G > gmax && prb[p.back().second]+poffset<maxLP)
          out << "***** Outlier #" << cnt+1
              << " in Chain #" << p.back().second 
              << ", analysis:" << endl
              << "  " << setw(20) << "Variable #"  << " = " << setw(15) << q << endl
              << "  " << setw(20) << "Sample size" << " = " << setw(15) << sz << endl
              << "  " << setw(20) << "Minimum"     << " = " << setw(15) << p[0].first << endl
              << "  " << setw(20) << "Maximum"     << " = " << setw(15) << p[sz-1].first << endl
              << "  " << setw(20) << "Mean"        << " = " << setw(15) << mean << endl
              << "  " << setw(20) << "Std dev"     << " = " << setw(15) << sqrt(var) << endl
              << "  " << setw(20) << "Grubbs test" << " = " << setw(15) << G << endl
              << "  " << setw(20) << "Lambda"      << " = " << setw(15) << gmax << endl
              << "  " << setw(20) << "Crit value"  << " = " << setw(15) << alpha << endl
              << "  " << setw(20) << "logP offset" << " = " << setw(15) << prb[p.back().second]-maxLP << endl
              << endl;
      }
      
                                // Record the outlier
      if (G > gmax && prb[p.back().second]+poffset<maxLP) {
        new_mask[p.back().second] = 1;
        p.pop_back();           // and delete it from the list
        cnt++;
      }
      else break;                       // No more outliers . . . we're done
    }
  }

  if (cnt > maxoutlier) {
                                // Warning for the naive.
                                // (If it's good enough for Braak 2006 it's
                                //  good enough for me . . .)
    out << "****** Too many aberrant chains!  ******" << endl
        << "****** Will continue sans masking ******" << endl
        << setw(72) << setfill('-') << "-" << endl << setfill(' ');

    return n;
  }

                                // Set the new mask
  chain_mask = new_mask;

  if (verbose) {
    if (cnt==0) out << "No outliers" << endl;
    else {
      out << "Outlier mask: ";
      for (unsigned i=0; i<n; i++) 
        if (new_mask[i]==0) out << setw(2) << 0;
        else                out << setw(2) << 1;
      out << endl;
    }
    out << setw(72) << setfill('-') << "-" << endl << setfill(' ');
  }

  return n - cnt;
}

int GelmanRubinConverge::ComputeMaskRJ()
{
  vector<State> z = data.back();
  unsigned n = z.size();

  vector<unsigned char> new_mask(n, 0);
  vector<double> prb = prob.back();
  int cnt=0;

  unsigned T  = z.front().SI()->T;
  unsigned N1 = z.front().SI()->N1;
  unsigned N2 = z.front().SI()->N2;

  vector<unsigned> Nparam(2);
  Nparam[0] = T + N1;
  Nparam[1] = T + N2;

  vector< vector<unsigned> > MAP(2);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<T; i++) MAP[m].push_back(1 + i);
  }
  for (unsigned i=0; i<N1; i++) MAP[0].push_back(1 + T + i);
  for (unsigned i=0; i<N2; i++) MAP[1].push_back(1 + T + N1 + i);

  double maxLP = -1.0e20;
  for (unsigned i=0; i<n; i++) maxLP = max<double>(maxLP, prb[i]);

  ofstream out;
  if (verbose) {
    ostringstream sout, sout1;
    sout << "---- Grubbs' outlier tests, Count=" << count 
         << " Instance=" << ninstance << " ";  
    sout1 << nametag << ".GRConverge";
    out.open(sout1.str().c_str(), ios::app);
    out << setw(72) << setfill('-') << "-" << endl
        << setw(72) << setfill('-') << left << sout.str() << endl 
        << setw(72) << setfill('-') << "-" << endl
        << setfill(' ');
  }

  // Do each variable separately
  //
  for (unsigned m=0; m<2; m++) {
    for (unsigned q=0; q<Nparam[m]; q++) {
                                // P is the sorted list of (value, index) pairs
      vector< pair<double, int> > p;
      for (unsigned i=0; i<n; i++) {
	if (new_mask[i]==0)
	  p.push_back(pair<double, int>(z[i][MAP[m][q]], i));
      }
  
				// Compute the Student statistic for 
                                // k-cnt possible outliers
      double val, pcrit, t, G, gmax;
      unsigned sz=p.size();
      unsigned k = sz*3/4;
      if (k<2) break;

      double mean, var;
      
      for (unsigned j=0; j<k; j++) {
	mean = var = 0.0;         // Compute mean and variance 
				// for the remaining sample
	for (unsigned i=0; i<sz; i++) {
	  val = z[p[i].second][MAP[m][q]];
	  mean += val/sz;
	  var  += val*val/(sz-1);
	}
	var -= mean*mean*sz/(sz-1);

	if (var<1.0e-18) break;
      
	for (unsigned i=0; i<sz; i++) 
	  p[i].first = fabs(z[p[i].second][MAP[m][q]]-mean);
	sort(p.begin(), p.end());

				// The supremum value
	G = p.back().first / sqrt(var);

                                // 2-sided Grubb's test to find outliers
	pcrit = 1.0 - (alpha*0.5/sz);
    
	t = inv_student_t_1sided(pcrit, sz-2);

	gmax = t*(sz-1)/sqrt(((t*t + (sz-2))*sz));

				// Report
	if (verbose) {
	  if (G > gmax && prb[p.back().second]+poffset<maxLP)
	    out << "***** Outlier #" << cnt+1
		<< " in Chain #" << p.back().second 
		<< ", analysis:" << endl
		<< "  " << setw(10) << "Model #"     << " = " << setw(15) << m << endl
		<< "  " << setw(20) << "Variable #"  << " = " << setw(15) << q << endl
		<< "  " << setw(20) << "Sample size" << " = " << setw(15) << sz << endl
		<< "  " << setw(20) << "Minimum"     << " = " << setw(15) << p[0].first << endl
		<< "  " << setw(20) << "Maximum"     << " = " << setw(15) << p[sz-1].first << endl
		<< "  " << setw(20) << "Mean"        << " = " << setw(15) << mean << endl
		<< "  " << setw(20) << "Std dev"     << " = " << setw(15) << sqrt(var) << endl
		<< "  " << setw(20) << "Grubbs test" << " = " << setw(15) << G << endl
		<< "  " << setw(20) << "Lambda"      << " = " << setw(15) << gmax << endl
		<< "  " << setw(20) << "Crit value"  << " = " << setw(15) << alpha << endl
		<< "  " << setw(20) << "logP offset" << " = " << setw(15) << prb[p.back().second]-maxLP << endl
		<< endl;
	}
      
				// Record the outlier
	if (G > gmax && prb[p.back().second]+poffset<maxLP) {
	  new_mask[p.back().second] = 1;
	  p.pop_back();           // and delete it from the list
	  cnt++;
	}
	else break;                       // No more outliers . . . we're done
      }

      if (cnt > maxoutlier) {
                                // Warning for the naive.
                                // (If it's good enough for Braak 2006 it's
                                //  good enough for me . . .)
	out << "****** Too many aberrant chains!  ******" << endl
	    << "****** Will continue sans masking ******" << endl
	    << setw(72) << setfill('-') << "-" << endl << setfill(' ');
	
	return n;
      }
    }
  }

                                // Set the new mask
  chain_mask = new_mask;

  if (verbose) {
    if (cnt==0) out << "No outliers" << endl;
    else {
      out << "Outlier mask: ";
      for (unsigned i=0; i<n; i++) 
        if (new_mask[i]==0) out << setw(2) << 0;
        else                out << setw(2) << 1;
      out << endl;
    }
    out << setw(72) << setfill('-') << "-" << endl << setfill(' ');
  }

  return n - cnt;
}



bool GelmanRubinConverge::Converged()
{
  // Sanity
  if (prob.size() < 2) return false;

  // Number of values per chain 
  if (data[0][0].Type() == StateInfo::RJTwo)
    return ConvergedRJ();
  else
    return ConvergedAll();
}


bool GelmanRubinConverge::ConvergedAll()
{
  ofstream out;
  
  // Too few states for a test
  if (maxit>0) {
    if (static_cast<int>(prob.size()) < maxit) return false;
  }

  if (maxit==0) {
    if (prob.size() < nskip) return false;
  }

  // Not long enough since the last test
  if (count < next) return false;

  // Chain has already converged and
  // desired number of states samples
  if (nburn > 0) return true;

  // Otherwise, test again
  
  cout << setfill('-') << setw(80) << '-' << setfill(' ') << endl;
  cout << "GelmanRubinConverge: starting a convergence test" << endl;

  if (verbose) {
    
    //***************************************************
    // Initialize output files
    //***************************************************
    
    ostringstream out0, outfile;
    out0 << nametag;
    if (_id.size()>0) out0 << "." << _id;
    outfile << out0.str() << ".log";
    out.open(outfile.str().c_str(), ios::app);
    if (!out) {
      throw FileNotExistException("GelmanRubinConverge: ", 
                                  outfile.str(), __FILE__, __LINE__);
    }
    
    // Separator
    out << setfill('-') << setw(70) << "-" << endl
        << "Count = " << count << endl
        << setw(70) << "-" << endl << setfill(' ');

  }
  


  //***************************************************
  //  Begin computation
  //***************************************************
  
                                // Number of entries (steps)
  unsigned Nsteps = prob.size();
                                // Number of chains
  unsigned Mchain = data[0].size();
                                // Number of values per chain 
  unsigned Nparam = data[0][0].size();

                                // 
                                // Look for aberrant chains
                                // 

  unsigned Mgood = Mchain;      // Number of good chains
  if (alpha>0.0 && count > noutlier) Mgood = ComputeMaskAll();

                                // 
                                // Gelman & Rubin quantities
                                // 
  vector<double> B(Nparam, 0.0), W(Nparam, 0.0), meanS(Nparam, 0.0);
  vector<double> Sig2Hat(Nparam, 0.0), rHat(Nparam, 0.0);

                                // 
                                // Set up vectors for in-chain analysis
                                // 
  vector< vector<double> > meanCh(Mchain), varCh(Mchain);
  for (unsigned i=0; i<Mchain; i++) {
    if (chain_mask[i]) continue;
    meanCh[i]  = vector<double>(Nparam, 0.0);
    varCh[i]   = vector<double>(Nparam, 0.0);
  }
                                // 
                                // Compute means and variance of each chain
                                //
  for (unsigned n=0; n<Nsteps; n++) {
    for (unsigned i=0; i<Mchain; i++) {
      if (chain_mask[i]) continue;
      for (unsigned j=0; j<Nparam; j++) {
        meanCh[i][j]  += data[n][i][j];
        varCh [i][j]  += data[n][i][j]*data[n][i][j];
      }
    }
  }

  for (unsigned i=0; i<Mchain; i++) {
    if (chain_mask[i]) continue;
    for (unsigned j=0; j<Nparam; j++) {
      meanCh[i][j] /= Nsteps;
      varCh[i][j] = 
        (varCh[i][j] - meanCh[i][j]*meanCh[i][j]*Nsteps)/(Nsteps-1);
      meanS[j] += meanCh[i][j]/Mgood;
    }
  }
                                // 
                                // Compute the Gelman & Rubin quantities
                                // 
  
  for (unsigned i=0; i<Mchain; i++) {
    if (chain_mask[i]) continue;
    for (unsigned j=0; j<Nparam; j++) {
      B[j] += 
        (meanCh[i][j] - meanS[j])*(meanCh[i][j] - meanS[j]) *
        Nsteps/(Mgood-1);
      W[j] += varCh[i][j]/Mgood;
    }
  }
    
  
  for (unsigned j=0; j<Nparam; j++) {
    if (W[j]>0.0) {
      Sig2Hat[j] = (W[j]*(Nsteps-1) + B[j])/Nsteps;
      // Gelman & Brooks 1998 "correction"
      rHat[j] = sqrt(Sig2Hat[j]/W[j]*(Mgood+1)/Mgood - 
		     static_cast<double>(Nsteps-1)/(Mgood*Nsteps) );
    }
  }
  
  
  if (verbose && debug) {
    out << endl 
        << "Per chain data, N=" << Nsteps
        << " M=" << Mgood << "/" << Mchain
        << ": " << endl << endl
        << setw(4) << left << "#";
    for (unsigned j=0; j<Nparam; j++) {
      ostringstream out1, out2;
      out1 << "Mean [" << j << "]";
      out2 << "Var [" << j << "]";
      out << setw(15) << left << out1.str()
          << setw(15) << left << out2.str();
    }
    out << endl
        << setw(4) << left << "-";
    for (unsigned j=0; j<Nparam; j++) {
      ostringstream out1, out2;
      out1 << "Mean [" << j << "]";
      out2 << "Var [" << j << "]";
      out << setw(15) << left << "---------"
          << setw(15) << left << "---------";
    }
    out << endl;
    
    for (unsigned i=0; i<Mchain; i++) {
      out << setw(4) << i;
      if (chain_mask[i])
        for (unsigned j=0; j<Nparam; j++) {
          out << setw(15) << left << "*******"
              << setw(15) << left << "*******";
        }
      else
        for (unsigned j=0; j<Nparam; j++) {
          out << setw(15) << left << meanCh[i][j]
              << setw(15) << left << varCh[i][j];
        }
      out << endl;
    }
    out << endl << setw(4) << "**";
    for (unsigned j=0; j<Nparam; j++) {
      out << setw(15) << left << meanS[j]
          << setw(15) << left << B[j]/Nsteps;
    }
    out << endl;
  }
  
  
  if (verbose) out << endl 
                   << "Convergence summary, N=" << Nsteps
                   << " M=" << Mgood << "/" << Mchain
                   << ": " << endl << endl
                   << setw(4) << left << "#"
                   << setw(15) << left << "Mean"
                   << setw(15) << left << "Between"
                   << setw(15) << left << "Within"
                   << setw(15) << left << "Total"
                   << setw(15) << left << "Rhat"
                   << endl
                   << setw(4) << left << "-"
                   << setw(15) << left << "----"
                   << setw(15) << left << "-------"
                   << setw(15) << left << "------"
                   << setw(15) << left << "-----"
                   << setw(15) << left << "----"
                   << endl;
  
  double minParam = DBL_MAX, maxParam = 0.0;
  for (unsigned j=0; j<Nparam; j++) {
    
    if (verbose) out << setw(4)  << j
                     << setw(15) << meanS[j]
                     << setw(15) << B[j]/Nsteps
                     << setw(15) << W[j]
                     << setw(15) << Sig2Hat[j]
                     << setw(15) << rHat[j]
                     << endl;
    
    minParam = min<double>(rHat[j], minParam);
    maxParam = max<double>(rHat[j], maxParam);
  }
  
  if (maxParam < rtol) {
    nburn = count;
    nconv = _dist->Nstates();
    next = nburn + ngood/Mgood+1;
    Converge::setBurnIn(nburn);
  } else {
    next = count + nskip;
    // Chains were trimmed but still no convergence
    if (Mgood<Mchain)           // Revert to no masking . . . and continue
      chain_mask = vector<unsigned char>(Mchain, 0);
  }
  
  if (verbose) {
    
    out << endl
        << "Min, Max = " << minParam << ", " << maxParam << endl;
    
    if (nburn>0) {
      out << endl
          << "Converged!  Nburn = " << nburn
          << "  Next = " << next << endl;
    }
  }
  
  out.close();
  
  cout << setw(20) << left << "Count"  << prob.size() << endl;
  cout << setw(20) << left << "Nskip"  << nskip       << endl;
  cout << setw(20) << left << "Count"  << count       << endl;
  cout << setw(20) << left << "Nburn"  << nburn       << endl;
  cout << setw(20) << left << "Nconv"  << nconv       << endl;
  cout << setw(20) << left << "Mgood"  << Mgood       << endl;
  cout << setw(20) << left << "Mchain" << Mchain      << endl;
  cout << setfill('-') << setw(80) << '-' << setfill(' ') << endl;

  return false;
}


bool GelmanRubinConverge::ConvergedRJ()
{
  ofstream out;
  
  // Too few states for a test
  if (maxit>0) {
    if (static_cast<int>(prob.size()) < maxit) return false;
  }

  if (maxit==0) {
    if (prob.size() < nskip) return false;
  }

  // Not long enough since the last test
  if (count < next)        return false;

  // Chain has already converged and
  // desired number of states samples
  if (nburn > 0)           return true;

  // Otherwise, test again
  
  cout << setfill('-') << setw(80) << '-' << setfill(' ') << endl;
  cout << "GelmanRubinConverge: starting a convergence test" << endl;

  if (verbose) {
    
    //***************************************************
    // Initialize output files
    //***************************************************
    
    ostringstream out0, outfile;
    out0 << nametag;
    if (_id.size()>0) out0 << "." << _id;
    outfile << out0.str() << ".log";
    out.open(outfile.str().c_str(), ios::app);
    if (!out) {
      throw FileNotExistException("GelmanRubinConverge: ", 
                                  outfile.str(), __FILE__, __LINE__);
    }
    
    // Separator
    out << setfill('-') << setw(70) << "-" << endl
        << "Count = " << count << endl
        << setw(70) << "-" << endl << setfill(' ');

  }
  


  //***************************************************
  //  Begin computation
  //***************************************************
  
                                // Number of entries (steps)
  unsigned Nsteps = prob.size();
                                // Number of chains
  unsigned Mchain = data[0].size();
                                // Number of values per chain 

  unsigned T  = data.back().front().SI()->T;
  unsigned N1 = data.back().front().SI()->N1;
  unsigned N2 = data.back().front().SI()->N2;

  vector<unsigned> Nparam(2);
  Nparam[0] = T + N1;
  Nparam[1] = T + N2;
				// Map to parameter vector for each component
  vector< vector<unsigned> > MAP(2);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<T; i++) MAP[m].push_back(1 + i);
  }
  for (unsigned i=0; i<N1; i++) MAP[0].push_back(1 + T + i);
  for (unsigned i=0; i<N2; i++) MAP[1].push_back(1 + T + N1 + i);

                                // 
                                // Look for aberrant chains
                                // 

  unsigned Mgood = Mchain;      // Number of good chains
  if (alpha>0.0 && count > noutlier) Mgood = ComputeMaskRJ();

                                // 
                                // Gelman & Rubin quantities
                                // 
  vector< vector<double> > B(2), W(2), meanS(2);
  vector< vector<double> > Sig2Hat(2), rHat(2);

  for (unsigned m=0; m<2; m++) {
    vector<double> tmp(Nparam[m], 0.0);
    B[m]       = tmp;
    W[m]       = tmp;
    meanS[m]   = tmp;
    Sig2Hat[m] = tmp;
    rHat[m]    = tmp;

  }
                                // 
                                // Set up vectors for in-chain analysis
                                // 
  vector< vector< vector<double> > > meanCh(2), varCh(2);
  vector< vector<unsigned> > Nmodel(2);

				// For indicator variable
  vector<double> meanCh_ind(Mchain, 0), varCh_ind(Mchain, 0);

  for (unsigned m=0; m<2; m++) {
    meanCh[m] = vector< vector<double> >(Mchain);
    varCh [m] = vector< vector<double> >(Mchain);
    Nmodel[m] = vector<unsigned>(Mchain, 0);
    for (unsigned i=0; i<Mchain; i++) {
      if (chain_mask[i]) continue;
      meanCh[m][i] = vector<double>(Nparam[m], 0.0);
      varCh [m][i] = vector<double>(Nparam[m], 0.0);
    }
  } 
				// 
				// Compute means and variance of each chain
                                //
  for (unsigned n=0; n<Nsteps; n++) {
    for (unsigned i=0; i<Mchain; i++) {
      if (chain_mask[i]) continue;
      unsigned m = static_cast<unsigned>(floor(data[n][i][0]+0.01));
      meanCh_ind[i] += m;
      varCh_ind[i]  += m*m;
      Nmodel[m][i]++;
      for (unsigned j=0; j<Nparam[m]; j++) {
        meanCh[m][i][j]  += data[n][i][MAP[m][j]];
        varCh [m][i][j]  += data[n][i][MAP[m][j]]*data[n][i][MAP[m][j]];
      }
    }
  }

				// Count good chains per model
  vector<unsigned> Ngood(2, 0);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<Mchain; i++)
      if (Nmodel[m][i]) Ngood[m]++;
  }

				// For indicator variable
  unsigned Mgood_ind = 0;
  for (unsigned i=0; i<Mchain; i++) { if (!chain_mask[i]) Mgood_ind++; }
  double meanS_ind = 0.0;

  for (unsigned i=0; i<Mchain; i++) {
    if (chain_mask[i]) continue;
    unsigned Ntot = 0;
    for (unsigned m=0; m<2; m++) {
      Ntot += Nmodel[m][i];
      if (Nmodel[m][i]) {
	for (unsigned j=0; j<Nparam[m]; j++) {
	  meanCh[m][i][j] /= Nmodel[m][i];
	  if (Nmodel[m][i]>1)
	    varCh[m][i][j] = 
	      (varCh[m][i][j] - meanCh[m][i][j]*meanCh[m][i][j]*Nmodel[m][i])
	      /(Nmodel[m][i]-1);
	  else
	    varCh[m][i][j] = 0.0;

	  meanS[m][j] += meanCh[m][i][j]/Ngood[m];
	}
      }
    }

    if (Ntot) meanCh_ind[i] /= Ntot;
    if (Ntot>1)
      varCh_ind[i] = (varCh_ind[i] - meanCh_ind[i]*meanCh_ind[i]*Ntot)/(Ntot-1);
    else
      varCh_ind[i] = 0.0;
    
    meanS_ind += meanCh_ind[i]/Mgood_ind;
  }
                                // 
                                // Compute the Gelman & Rubin quantities
                                // 
  
  vector<unsigned> Wcnt(2, 0);

  for (unsigned i=0; i<Mchain; i++) {
    if (chain_mask[i]) continue;
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	if (Ngood[m]>1) {
	  double del = meanCh[m][i][j] - meanS[m][j];
	  B[m][j] += del*del * Nmodel[m][i]/(Ngood[m]-1);
	}
	if (Ngood[m]) W[m][j] += varCh[m][i][j]/Ngood[m];
	Wcnt[m] += Nmodel[m][i];
      }
    }
  }
    
  for (unsigned m=0; m<2; m++)
    if (Ngood[m]) Wcnt[m] /= Ngood[m];
  
  for (unsigned m=0; m<2; m++) {
    for (unsigned j=0; j<Nparam[m]; j++) {
      if (Wcnt[m]>0) {
	Sig2Hat[m][j] = (W[m][j]*(Wcnt[m]-1) + B[m][j])/Wcnt[m];
	// Gelman & Brooks 1998 "correction"
	rHat[m][j] = sqrt( Sig2Hat[m][j]/W[m][j]*(Ngood[m]+1)/Ngood[m] -
			   static_cast<double>(Wcnt[m]-1)/(Ngood[m]*Wcnt[m]) );
      }
    }
  }
  
  
  double B_ind = 0.0, W_ind = 0.0;
  for (unsigned i=0; i<Mchain; i++) {
    B_ind += 
      (meanCh_ind[i] - meanS_ind)*(meanCh_ind[i] - meanS_ind)*Nsteps /
      (Mgood_ind-1);
    W_ind += varCh_ind[i]/Mgood_ind;
  }

  double Sig2Hat_ind = (W_ind*(Nsteps - 1) + B_ind)/Nsteps;

  // Gelman & Brooks 1998 "correction"
  double rHat_ind = 1.0;
  if (W_ind>1.0e-12) 
    rHat_ind = sqrt(Sig2Hat_ind/W_ind*(Mgood_ind+1)/Mgood_ind -
		    static_cast<double>(Nsteps-1)/(Mgood_ind*Nsteps));
  
  vector<unsigned> Tsteps(2, 0);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<Mchain; i++) Tsteps[m] += Nmodel[m][i];
  }

  if (verbose && debug) {

    cout << endl 
	 << "Per chain data, N=" << Tsteps[0] << ", " << Tsteps[1]
	 << " M=[" << Ngood[0] << ", " << Ngood[1] << "]/" << Mchain
	 << ": " << endl << endl
	 << setw(4) << left << "#";

    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	ostringstream out1, out2;
	out1 << "Mean [" << m << ", " << j << "]";
	out2 << "Var [" << m << ", " << j << "]";
	cout << setw(15) << left << out1.str()
	     << setw(15) << left << out2.str();
      }
    }
    cout << endl
	 << setw(4) << left << "-";
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	ostringstream out1, out2;
	out1 << "Mean [" << m << ", " << j << "]";
	out2 << "Var [" << m << ", " << j << "]";
	cout << setw(15) << left << "---------"
	     << setw(15) << left << "---------";
      }
      cout << endl;
    }
    for (unsigned i=0; i<Mchain; i++) {
      cout << setw(4) << i;
      for (unsigned m=0; m<2; m++) {
	if (chain_mask[i])
	  for (unsigned j=0; j<Nparam[m]; j++) {
	    cout << setw(15) << left << "*******"
		 << setw(15) << left << "*******";
	  }
	else
	  for (unsigned j=0; j<Nparam[m]; j++) {
	    cout << setw(15) << left << meanCh[m][i][j]
		 << setw(15) << left << varCh[m][i][j];
	  }
      }
      cout << endl;
    }
    cout << endl << setw(4) << "**";
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	cout << setw(15) << left << meanS[m][j]
	     << setw(15) << left << B[m][j]/Wcnt[m];
      }
    }
    cout << endl;
  }
      

  if (verbose) 
    cout << endl 
	 << "Convergence summary, N=[" << Tsteps[0] << ", " << Tsteps[1] << "]"
	 << " M=[" << Ngood[0] << ", " << Ngood[1] << "]/" << Mchain 
	 << endl << endl
	 << setw(4)  << left << "Mod"
	 << setw(4)  << left << "#"
	 << setw(15) << left << "Mean"
	 << setw(15) << left << "Between"
	 << setw(15) << left << "Within"
	 << setw(15) << left << "Total"
	 << setw(15) << left << "Rhat"
	 << endl
	 << setw(4)  << left << "---"
	 << setw(4)  << left << "---"
	 << setw(15) << left << "----"
	 << setw(15) << left << "-------"
	 << setw(15) << left << "------"
	 << setw(15) << left << "-----"
	 << setw(15) << left << "----"
	 << endl;
  
  double minParam = DBL_MAX, maxParam = 0.0;
  for (unsigned m=0; m<2; m++) {
    for (unsigned j=0; j<Nparam[m]; j++) {

      if (verbose) cout << setw(4)  << m
			<< setw(4)  << j
			<< setw(15) << meanS[m][j]
			<< setw(15) << B[m][j]/Wcnt[m]
			<< setw(15) << W[m][j]
			<< setw(15) << Sig2Hat[m][j]
			<< setw(15) << rHat[m][j]
			<< endl;
      
      minParam = min<double>(rHat[m][j], minParam);
      maxParam = max<double>(rHat[m][j], maxParam);
    }

  }

  if (verbose) cout << setw(8)  << "Ind *"
		    << setw(15) << meanS_ind
		    << setw(15) << B_ind/Nsteps
		    << setw(15) << W_ind
		    << setw(15) << Sig2Hat_ind
		    << setw(15) << rHat_ind
		    << endl;
  
  minParam = min<double>(rHat_ind, minParam);
  maxParam = max<double>(rHat_ind, maxParam);
  
  if (maxParam < rtol) {
    nburn = count;
    nconv = _dist->Nstates();
    next = nburn + ngood/Mgood+1;
  } else {
    next = count + nskip;
    // Chains were trimmed but still no convergence
    if (Mgood<Mchain)    // Revert to no masking . . . and continue
      chain_mask = vector<unsigned char>(Mchain, 0);
  }
  
  if (verbose) {
    
    out << endl
        << "Min, Max = " << minParam << ", " << maxParam << endl;
    
    if (nburn>0) {
      out << endl
          << "Converged!  Nburn = " << nburn
          << "  Next = " << next << endl;
    }
  }
  
  out.close();
  
  cout << setw(20) << left << "Count"  << prob.size() << endl;
  cout << setw(20) << left << "Nskip"  << nskip       << endl;
  cout << setw(20) << left << "Count"  << count       << endl;
  cout << setw(20) << left << "Nburn"  << nburn       << endl;
  cout << setw(20) << left << "Nconv"  << nconv       << endl;
  cout << setw(20) << left << "Mgood"  << Mgood       << endl;
  cout << setw(20) << left << "Mchain" << Mchain      << endl;
  cout << setfill('-') << setw(80) << '-' << setfill(' ') << endl;

  return false;
}
