#include <cmath>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>

using namespace std;

#include <BIEdebug.h>
#include <BIEException.h>
#include <BIEMutex.h>
#include <PFSimulation.h>
#include <gfunction.h>
#include <CLICheckpointManager.h>
#include <LikelihoodComputationSerial.h>
#include <LikelihoodComputationMPI.h>
#include <LikelihoodComputationMPITP.h>
#include <Timer.h>
#include <TessToolSender.h>
#include <Ensemble.h>
#include <MHWidthOne.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PFSimulation)

using namespace BIE;

double PFSimulation::ESSprob = 0.1;
int PFSimulation::maxTries   = 400;

// constructor, the user can set all variables and pass object pointers.
//
PFSimulation::PFSimulation
(int nsteps, int npart, Simulation *sim, Ensemble *factory)
{
  //
  // Check likelihood computation: must be serial
  //
  if (dynamic_cast <LikelihoodComputationSerial*> (sim->GetLike()) )
    {
      LikelihoodComputationSerial* l = 
	(LikelihoodComputationSerial*)sim->GetLike();
      l->TessToolOn(false);
    }
  else 
    {
      throw InternalError("PFSimulation: LikelhoodComputation must be serial!",
			  __FILE__, __LINE__);
    }

  ostream cout(checkTable("simulation_output"));

  //
  // Get Node 0 working directory
  //
  const int hdbufsize=1024;
  char *hdbuffer = new char [hdbufsize];
  
  if (!mpi_used || myid == 0) {
    hdbuffer = getcwd(hdbuffer, (size_t)hdbufsize);
  }
  
  if (mpi_used){
    ++MPIMutex;
    MPI_Bcast(hdbuffer, hdbufsize, MPI_CHAR, 0, MPI_COMM_WORLD);
    --MPIMutex;
  }
  
  homedir = (string)hdbuffer + "/";
  if (myid) {
    int ret = chdir(hdbuffer);
    if (ret==-1) cout << "Process " << myid << ": error changing directory\n";
  }
  cout << "Process " << myid << ": homedir=" << homedir << endl;
  hdbuffer = getcwd(hdbuffer, (size_t)hdbufsize);
  cout << "Process " << myid << ": cwd=" << hdbuffer << endl;
  
  delete [] hdbuffer;

  // Prefix root directory
  //
  cout << "Process "    << myid << endl
       << "   outfile=" << outfile << endl << endl;
  
  this->nsteps  = nsteps;	// needs to be passed in 
  this->npart   = npart;
  this->sim     = sim;
  this->factory = factory;
  
  this->threshval = 0.0;
  
  frontier = 0;
  ttfreq   = 0;
  unit     = UniformPtr(new Uniform(0.0, 1.0, BIEgen));

  resumelog = false;
}

void PFSimulation::Run(void)
{
  ostream cout(checkTable("simulation_output"));
  
  try {
    // wrap the whole thing in a try block because
    // exceptions don't propogate out of threads.
    
    // does full simulation at multiple levels of resolution.  
    
    // Diagnostic: print number of particles on and off grid
    //
    if (myid==0)
      cout << "---- Tessellation diagnostic" << endl
	   << "      Number of data points in bounds:  " 
	   << sim->GetDataTree()->Total() << endl
	   << "      Number of data points out of bounds: "
	   << sim->GetDataTree()->Offgrid() << endl 
	   << "---- Done" << endl 
	   << flush;
    
    // Set the frontier to be the root of the tessellation. 
    //
    frontier = sim->GetDataTree()->GetDefaultFrontier();
    frontier->RetractToTopLevel(); 
    
    // Size of the frontier is . . . 
    //
    vector<int> tiles = frontier->ExportFrontier();
    int fsize = tiles.size();

    if (myid==0) {
      cout << "Frontier size is " << fsize << endl;
    }
    
    // Save top-level frontier
    //
    dataSoFar = tiles;
    curLevel = 0;

    int idone;
    bool bad;
      
    // Initialize particles: try "maxTries" times to find a good state
    //
    for (unsigned n=0; n<npart; n++) {
      if (!mpi_used || myid == (int)(n % numprocs)) {

	ChainPtr chain(new Chain(sim->SI()));

	bad = true;		// State condition
	idone = 0;		// # tries so far
	while (bad && idone<maxTries) {

	  State s(sim->SI());
	  sim->GetPrior()->SamplePrior(&s);
	  chain->AssignState0(s);
	  
	  try {
	    sim->GetMCA()->ComputeCurrentState(sim, chain.get());
	    bad = false;
	  } 
	  catch (ImpossibleStateException &e) {
	    bad = true;
	  }
	  catch (BIEException& e) {
	    cout << "Process " << myid 
		 << ": in PFSimulation::Run(): "
		 << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
		 << e.getErrorMessage();
	    throw e;
	  }

	  idone++;
	}

	if (idone==maxTries) {
	  throw BIEException("TooManyTriesException", 
			     "could not find a good initial state",
			     __FILE__, __LINE__);
	}

	particles.push_back(chain);
      }
    }

    WeightNormalize();
    
    // The freshly initialized particles DO NOT need to be exchanged
    // But now do an MCMC step for each particle
    //
    swapstat[0] = swapstat[1] = 0;
    for (int n=0; n<nMCMC; n++) MCMCStep();

    Resume();

  } 
  catch (BIEException& e) {
    cout << "Process " << myid 
	 << ": in PFSimulation::Run(): "
	 << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	 << e.getErrorMessage();
    throw e;
  }
  
}
 
void PFSimulation::Resume() 
{
  ostream cout(checkTable("simulation_output"));
  Timer timer;

  resumeLog();

  // ==================================================
  // MAIN LOOP
  // ==================================================

  unsigned nsize = sim->GetDataTree()->GetTessellation()->NumberTiles();

  sstat = boost::shared_ptr<Ensemble>(factory->New());
  sstat->Reset(sim->SI());

  while (dataSoFar.size() < nsize) {

    //
    // Do the work!
    //
    if (ckm) {
      ckm->checkpoint(cout);
    }

    timer.reset();
    timer.start();

    // Down one level
    //
    IncreaseResolution();

    // Update particles weights
    //
    UpdateStep();

    // Accumulate statistics
    //
    AccumulateStats();

    // Test ESS and Resample
    //
    
    int ret = Resample();

    // Diagnostics
    //
    timer.stop();
    
    double dtime = timer.getTime().getRealTime();
	  
    if (timer.Precision()) dtime *= 1.0e-6;
    if (dtime>1000.0) timer.Seconds();
    if (dtime<100.0)  timer.Microseconds();
	  
    if (myid==0) {
      cout << setfill('=') << setw(80) << "=" << endl << setfill(' ');
      cout << "Level="       << setw(4)  << curLevel 
	   << "  ESS="       << setw(10) << curESS
	   << "  Time/step=" << setw(10) << dtime
	   << "  Resample="  << ret
	   << "  Data size=" << dataSoFar.size() 
	   << "/" << nsize
	   << endl;
    }

    if (ret) {
      if (myid==0) {
	// Swap statistics
	cout << "Swap stats: " << swapstat[0] << "/" << swapstat[1];
	if (swapstat[1]>0) 
	  cout << "=" << (double)swapstat[0]/swapstat[1] << endl;
	else
	  cout << "=" << 0.0 << endl;
      }
				// Ensemble statistics
      sstat->ComputeDistribution();
      if (myid==0) sstat->PrintDiag(cout);
      WeightedStats();
				// Reset the ensemble
      sstat->setDimensions(sim->SI());
    }
    if (myid==0) 
      cout << setfill('=') << setw(80) << "=" << endl << setfill(' ');

    PrintParticles(outfile);

    // LOOP until done
    // or
    if (nsteps>0 && curLevel >= nsteps) break;
    //
  }

  // Final evaluation
  //
  swapstat[0] = swapstat[1] = 0;
  for (int n=0; n<nMCMC; n++) MCMCStep();
  
  PrintParticles(outfile);

}


void PFSimulation::MCMCStep(void)
{
  vector<ChainPtr>::iterator ch;
  int swaps[2] = {0, 0};

  for (ch=particles.begin(); ch!=particles.end(); ch++) {
    //
    // Compute current state
    //
    sim->GetMCA()->ComputeCurrentState(sim, ch->get());

    // 
    // Compute the proposed state
    //
    sim->GetPrior()->SampleProposal( **ch, sim->GetMHWidth() );

    //
    // Monte Carlo
    //
    try {
      if (sim->GetMCA()->Sweep(sim, ch->get())) swaps[0]++;
      swaps[1]++;
    }
    catch (ImpossibleStateException &e) {
      cerr << "Process " << myid << ": Bad state . . ." << endl;
    }
  }
  
  if (mpi_used) {
    int total[2] = {0, 0};
    
    ++MPIMutex;
    MPI_Reduce(&swaps, &total, 2, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    --MPIMutex;

    if (myid==0) {
      swapstat[0] += total[0];
      swapstat[1] += total[1];
    }

  } else {
      swapstat[0] += swaps[0];
      swapstat[1] += swaps[1];
  }

}

bool PFSimulation::Resample() 
{
  vector<ChainPtr>::iterator ch;
  double ess1 = 0.0, ess = 0.0;

  enum ResampleCommand { send, recv, keep, quit };
  int c;

  for (ch=particles.begin(); ch!=particles.end(); ch++) {
    ess1 += exp(2.0*(*ch)->weight);
  }

  if (mpi_used) {
    ++MPIMutex;
    MPI_Allreduce(&ess1, &ess, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    --MPIMutex;
  }
  else
    ess = ess1;

  curESS = 1.0/ess;			// Assign to global variable

  if (curESS > ESSprob*npart) return false;

  vector<double> wght, wcum;
  vector<int> nodes, pindx;

  MPI_Status status;
  int len = particles.size();
    
  //
  // Do the repartition
  //
  if (!mpi_used || myid==0) {
      
    int len1, ii;

    // First get the weights for Node 0
    //
    for (ch=particles.begin(), ii=0; ch!=particles.end(); ch++, ii++) {
      wght.push_back(exp((*ch)->weight));
      nodes.push_back(0);
      pindx.push_back(ii);
    }

    // Get weights for all other nodes
    //
    if (mpi_used) {

      for (int n=1; n<numprocs; n++) {
	++MPIMutex;
	MPI_Recv(&len1, 1, MPI_INT, n, 113, MPI_COMM_WORLD, &status);
	vector<double> w(len1);
	MPI_Recv(&w[0], len1, MPI_DOUBLE, n, 113, MPI_COMM_WORLD, &status);
	--MPIMutex;
	for (int i=0; i<len1; i++) {
	  wght.push_back(exp(w[i]));
	  nodes.push_back(n);
	  pindx.push_back(i);
	}
      }
    }

#ifdef DEBUG
    if (npart != wght.size())
      cout << "Error in particle number sanity check: "
	   << npart << " != " << wght.size() 
	   << ", particle array size=" << particles.size()
	   << endl;
#endif

    // Do the cumulation
    //
    wcum = wght;
    for (unsigned i=1; i<npart; i++) wcum[i] += wcum[i-1];

    // Pick new particles
    //
    double prob;
    int lower, upper, middle;
    vector<int> newp;		// index for new particle selection
    vector<int> lnode, lindx;	// local node and index for newp

    for (unsigned i=0; i<npart; i++) {
      prob = (*unit)() * wcum[npart-1];
      lower = 0;
      upper = npart;

      while (upper-lower > 1) {
	middle = (upper+lower) >> 1;
	if (prob > wcum[middle])
	  lower = middle;
	else
	  upper = middle;
      }
	
				// Just in case
      upper = min<int>(npart-1, upper);

      newp.push_back(upper);
      lnode.push_back(nodes[upper]);
      lindx.push_back(pindx[upper]);
    }
	
    for (unsigned i=0; i<npart; i++) {

				// Use the same initial particle partition
      if (nodes[i]==0) {

	if (lnode[i]==0) {

	  ChainPtr p(new Chain(*particles[lindx[i]]));

	  particles.push_back(p);

	} else {	 // Instruct nodes to send particles to Node 0
#ifdef DEBUG
	  if (lnode[i]<0 || lnode[i]>=numprocs) {
	    cout << "Node out of bounds sending n=" << lnode[i] << " from "
		 << nodes[i] << ", N=" << i << "!\n";
	  }
#endif
	  ++MPIMutex;
	  MPI_Send(&(c=send), 1, MPI_INT, lnode[i], 114, MPI_COMM_WORLD);
	  MPI_Send(&nodes[i], 1, MPI_INT, lnode[i], 115, MPI_COMM_WORLD);
	  MPI_Send(&lindx[i], 1, MPI_INT, lnode[i], 116, MPI_COMM_WORLD);
	  --MPIMutex;

				// Node 0 receives particles
	  ChainPtr p(new Chain(sim->SI()));
	  p->RecvChain(lnode[i]);
	  particles.push_back(p);
	}

      } else {
#ifdef DEBUG
	if (lnode[i]<0 || lnode[i]>=numprocs) {
	  cout << "Node out of bounds sending n=" << lnode[i] << " from "
	       << nodes[i] << ", N=" << i << "!\n";
	}
#endif
	++MPIMutex;
				// Keep existing particle?
	if (nodes[i] == lnode[i]) {

	  MPI_Send(&(c=keep), 1, MPI_INT, lnode[i], 114, MPI_COMM_WORLD);
	  MPI_Send(&lindx[i], 1, MPI_INT, lnode[i], 116, MPI_COMM_WORLD);

	} else {		// Node n1 = lnode[i]
				// Node n2 = nodes[i]

				// Instruct Node n2 to receive from Node n1
	  MPI_Send(&(c=recv), 1, MPI_INT, nodes[i], 114, MPI_COMM_WORLD);
	  MPI_Send(&lnode[i], 1, MPI_INT, nodes[i], 115, MPI_COMM_WORLD);
	    
	  if (lnode[i]==0) {

	    particles[lindx[i]]->SendChain(nodes[i]);
	    
	  } else {
				// Instruct nodes to send particles to Node n1
	    MPI_Send(&(c=send), 1, MPI_INT, lnode[i], 114, MPI_COMM_WORLD);
	    MPI_Send(&nodes[i], 1, MPI_INT, lnode[i], 115, MPI_COMM_WORLD);
	    MPI_Send(&lindx[i], 1, MPI_INT, lnode[i], 116, MPI_COMM_WORLD);
	    
	  }
	}
      }
      
      --MPIMutex;
    }

    if (mpi_used) {
      ++MPIMutex;
      for (int n=1; n<numprocs; n++) 
	MPI_Send(&(c=quit), 1, MPI_INT, n, 114, MPI_COMM_WORLD);
      --MPIMutex;
    }

  } else {

    --MPIMutex;
    MPI_Send(&len, 1, MPI_INT, 0, 113, MPI_COMM_WORLD);
    --MPIMutex;
    vector<double> w(len);
    for (int i=0; i<len; i++) w[i] = particles[i]->weight;
    --MPIMutex;
    MPI_Send(&w[0], len, MPI_DOUBLE, 0, 113, MPI_COMM_WORLD);
    --MPIMutex;

    // Get commands from master
    //

    int node, indx;

    MPI_Status status;
				// Get command
    ++MPIMutex;
    MPI_Recv(&c, 1, MPI_INT, 0, 114, MPI_COMM_WORLD, &status);
    --MPIMutex;
    
    while (c!=quit) {

      if (c == keep) {
	++MPIMutex;
	MPI_Recv(&indx, 1, MPI_INT, 0, 116, MPI_COMM_WORLD, &status);
	--MPIMutex;
	ChainPtr p(new Chain(*particles[indx]));
	particles.push_back(p);
      }

      if (c == send) {
	++MPIMutex;
	MPI_Recv(&node, 1, MPI_INT, 0, 115, MPI_COMM_WORLD, &status);
	MPI_Recv(&indx, 1, MPI_INT, 0, 116, MPI_COMM_WORLD, &status);
	--MPIMutex;
	particles[indx]->SendChain(node);
      }

      if (c == recv) {
	++MPIMutex;
	MPI_Recv(&node, 1, MPI_INT, 0, 115, MPI_COMM_WORLD, &status);
	--MPIMutex;
	ChainPtr p(new Chain(sim->SI()));
	p->RecvChain(node);
	particles.push_back(p);
      }

				// Get command
      ++MPIMutex;
      MPI_Recv(&c, 1, MPI_INT, 0, 114, MPI_COMM_WORLD, &status);
      --MPIMutex;
    }

  }

  // Dump old particles at the front of the list
  // 
  for (int i=0; i<len; i++) particles.erase(particles.begin());
  
#ifdef DEBUG
  if (myid==0) {
    sort(wght.begin(), wght.end());
    int cnt = 0;
    int wsize = wght.size();
    double pfrac = 0.001;
    double quantiles[] = {0.1, 0.2, 0.3, 0.4, 0.5, 
			  0.6, 0.7, 0.8, 0.9, 0.95, 
			  0.99, 0.995, 0.999};

    cout << "Weight statistics: " << endl;
    for (int i=0; i<13; i++) {
      cout << setw(5)  << quantiles[i] 
	   << setw(16) << wght[(int)(quantiles[i]*wsize)] << endl;
    }
    for (int i=wsize-1; i>=0; i--) {
      if (wght[i]<pfrac) break;
      cnt++;
    }
    cout << cnt << " particles with w>=" << pfrac << endl;
    cout << endl;
  }
#endif

  swapstat[0] = swapstat[1] = 0;
  for (int n=0; n<nMCMC; n++) MCMCStep();

  // Reset the weights and renormalize
  //
  for (ch=particles.begin(); ch!=particles.end(); ch++) 
    (*ch)->weight = 1.0;
  WeightNormalize();

  return true;
}

void PFSimulation::WeightNormalize(void)
{
  vector<ChainPtr>::iterator ch;
  double sum1  = 0.0, sum  = 0.0;
  double maxA1 = -1.0e20, maxA = -1.0e20;
  
  //
  // Compute normalization in two passes (to improve accuracy)
  //

  //
  // (1) Get average to estimate normalization
  //
  for (ch=particles.begin(); ch!=particles.end(); ch++) {
    maxA1 = max<double>(maxA1, (*ch)->weight);
  }

  if (mpi_used) {
    ++MPIMutex;
    MPI_Allreduce(&maxA1, &maxA, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    --MPIMutex;
  }
  else
    maxA = maxA1;

  //
  // (2) Preapply average hopefully get a more accurate sum
  //
  for (ch=particles.begin(); ch!=particles.end(); ch++) {
    (*ch)->weight -= maxA;
    sum1 += exp((*ch)->weight);
  }

  if (mpi_used) {
    ++MPIMutex;
    MPI_Allreduce(&sum1, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    --MPIMutex;
  }
  else
    sum = sum1;

  if (sum<=0.0) {
    cerr << "Oops!\n";
  }
		 
  for (ch=particles.begin(); ch!=particles.end(); ch++) 
    (*ch)->weight -= log(sum);

#ifdef DEBUG
  // 
  // Sanity check
  //
  sum1 = sum = 0.0;
  for (ch=particles.begin(); ch!=particles.end(); ch++) 
    sum1 += exp((*ch)->weight);


  if (mpi_used) {
    ++MPIMutex;
    MPI_Reduce(&sum1, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    --MPIMutex;
  }
  else
    sum = sum1;

  if (myid==0) cout << "SUM=" << sum << endl;

#endif
}

void PFSimulation::UpdateStep(void)
{
  vector<ChainPtr>::iterator ch;
  State state;
  int indx;

  for (ch=particles.begin(); ch!=particles.end(); ch++) {
  
    try {
      state = (*ch)->GetState0();
      indx = (*ch)->indx;
      (*ch)->weight += sim->Likelihood(state, indx);
    }
    catch (ImpossibleStateException &e) {
      cerr << "Process " << myid << ": Bad state . . ." << endl;
    }
  }

  WeightNormalize();
}

//
// Currently not used; for alternative strategy where data
// are partitioned among processors, not particles
//
void PFSimulation::ExchangeParticles(void)
{
  if (!mpi_used) return;

  MPI_Status status;
  int newSize, mySize = particles.size();
  int to, from;
  
  for (int rank=0; rank<numprocs; rank++) {

    to   = rank+1 % numprocs;
    from = rank;

    if (myid==from) {		// Send particles
      ++MPIMutex;
      MPI_Send(&mySize, 1, MPI_INT, to, 112, MPI_COMM_WORLD);
      --MPIMutex;
      
      for (int i=0; i<mySize; i++) {
	ChainPtr p(particles.back());
	p->SendChain(to);
	particles.pop_back();
      }
    } 
    else if (myid==to) {	// Receive particles
      ++MPIMutex;
      MPI_Recv(&newSize, 1, MPI_INT, from, 112, MPI_COMM_WORLD, &status);
      --MPIMutex;

      for (int i=0; i<newSize; i++) {
	ChainPtr p(new Chain(sim->SI()));
	p->RecvChain(from);

	particles.push_back(p);
      }
    }
  }

}


void PFSimulation::AccumulateStats(void)
{
  MPI_Status status;
  unsigned newSize, mySize = particles.size(), ssize;
  vector<double> v(3);
  ChainPtr ch;
  State s(sim->SI());
  
  if (!mpi_used || myid==0) {
    for (unsigned i=0; i<mySize; i++) {
      ch = particles[i];
      sstat->AccumData(ch->GetProbs(), *ch->Cur());
    }
  }

  if (mpi_used) {

    if (myid==0) ssize = ch->Cur()->size();
    ++MPIMutex;
    MPI_Bcast(&ssize, 1, MPI_INT, 0, MPI_COMM_WORLD);
    --MPIMutex;

    for (int rank=1; rank<numprocs; rank++) {

      if (myid==0) {

	++MPIMutex;
	MPI_Recv(&newSize, 1, MPI_INT, rank, 118, MPI_COMM_WORLD, &status);

	for (unsigned i=0; i<newSize; i++) {
	  MPI_Recv(&v[0], 3, MPI_DOUBLE, rank, 119, MPI_COMM_WORLD, &status);
	  MPI_Recv(&s[0], ssize, MPI_DOUBLE, rank, 120, MPI_COMM_WORLD, &status);
	  sstat->AccumData(v, s);
	}

	--MPIMutex;
      }
      else if (myid==rank) {

	++MPIMutex;
	MPI_Send(&mySize, 1, MPI_INT, 0, 118, MPI_COMM_WORLD);
	--MPIMutex;
      
	for (unsigned i=0; i<mySize; i++) {

	  ch = particles[i];

	  ++MPIMutex;
	  MPI_Send(&ch->GetProbs()[0], 3, MPI_DOUBLE, 0, 119, MPI_COMM_WORLD);
	  MPI_Send(&ch->Vec0(0), ssize, MPI_DOUBLE, 0, 120, MPI_COMM_WORLD);
	  --MPIMutex;
	}
      }
    }
  }
}


void PFSimulation::resumeLog()
{
  if (!resumelog) return;
  resumelog = false;
  if (myid!=0)    return;

  //
  // Attempt to open file to check for existence
  //
  ifstream in(outfile.c_str());
  in.close();
  if (in.fail()) return;

  //
  // Backup the original file and make sure 
  // the move is successful
  //
  string backup = outfile  + ".bak";
  if (rename(outfile.c_str(), backup.c_str())) {
    ostringstream msg;
    msg << "Could not make backup file <"
	 << backup << "> from original file <" << outfile << ">";
    throw ResumeLogException(msg.str(), __FILE__, __LINE__);
  }
  
  //
  // Read from the backup, rewrite the rolled
  // back state log
  //
  ifstream fin(backup.c_str());
  ofstream fot(outfile.c_str());
  
  if (fin && fot) {
    
    const unsigned inbufsize = 32768;
    char inbuf[inbufsize];
    istringstream sin;
    string tmp;
				// Read lines until empty or done
    int lev1;
    while (fin.getline(inbuf, inbufsize)) {
				// Is this a new stanza?
      string line(inbuf);
      if (line.find("Level") != string::npos) {
	sin.str(inbuf);

	sin >> tmp;
	sin >> tmp;
	sin >> lev1;
				// Have we gone far enough?
	if (lev1 >= curLevel) break;
      }


      if (fin.fail()) {
	ostringstream msg;
	msg << "Error or end reading backup file"
	    << " before desired level [" << curLevel
	    << "] was found";
	throw ResumeLogException(msg.str(), __FILE__, __LINE__);
      }
				// Otherwise append line to file
      fot << inbuf << endl;
    }

  } else {

    ostringstream msg;
    msg << "Error opening input and/or output files";
    if (rename(backup.c_str(), outfile.c_str()))
      msg << " and could not restore original state file";
    throw ResumeLogException(msg.str(), __FILE__, __LINE__);
  }
}

void PFSimulation::IncreaseResolution()
{
  curLevel++; 
  frontier->UpDownLevels(1);

  vector<int> ftmp = frontier->ExportFrontier();
  for (vector<int>::iterator i=ftmp.begin(); i!=ftmp.end(); i++)
    dataSoFar.push_back(*i);
}


void PFSimulation::SetTessTool(TessToolSender *tt) 
{
  tesstool = tt;
  sim->GetLike()->SetTessTool(tt);
}


void PFSimulation::SampleNext(void)
{
  sim->GetLike()->SampleNext();
}

void PFSimulation::SwitchOnCLI()
{
  sim->GetLike()->SwitchOnCLI();
}

void PFSimulation::SuspendSimulation()
{
  sim->GetLike()->SuspendSimulation();
}

void PFSimulation::ResumeSimulation()
{
  sim->GetLike()->ResumeSimulation();
}

void PFSimulation::SetAutoTessTool(int freq)
{
  ttfreq = freq;
  tesstool = new TessToolSender(0);
  sim->GetLike()->SetTessTool(tesstool);
}

void PFSimulation::WeightedStats()
{
  vector<ChainPtr>::iterator ch = particles.begin();
  unsigned dim = (*ch)->N0();

  double mass1=0.0, mass=0.0, w;
  vector<double> mean1(dim, 0.0), mean(dim, 0.0);
  vector<double> var1(dim, 0.0),  var(dim, 0.0);

  for (ch=particles.begin(); ch!=particles.end(); ch++) {
    w = exp((*ch)->weight);
    mass1 += w;
    for (unsigned j=0; j<dim; j++) {
      mean1[j] += w * (*ch)->Vec0(j);
      var1[j]  += w * (*ch)->Vec0(j)*(*ch)->Vec0(j);
    }
  }
  
  if (mpi_used) {

    ++MPIMutex;
    MPI_Reduce(&mass1, &mass, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&(mean1[0]), &(mean[0]), dim, 
	       MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&(var1[0]), &(var[0]), dim, 
	       MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    --MPIMutex;

  } else {
    mass = mass1;
    mean = mean1;
    var  = var1;
  }

  if (!mpi_used || myid==0) {
    for (unsigned i=0; i<dim; i++) mean[i] /= mass;
    for (unsigned i=0; i<dim; i++) var[i] = var[i]/mass - mean[i]*mean[i];

    cout << "Weighted mean & variance:" << endl << endl;
    for (unsigned i=0; i<dim; i++)
      cout << setw(5) << i+1
	   << setw(15) << mean[i]
	   << setw(15) << var[i]
	   << setw(15) << sqrt(var[i])
	   << endl;
  }
}

void PFSimulation::PrintParticles(string filename)
{
  for (int n=0; n<numprocs; n++) {

    if (myid==n) {
      //
      // Open file for append
      //
      ofstream outlog(filename.c_str(), ios::app);

      if (!outlog) {
	cout << "PFSimulation::PrintParticles: process "
	     << myid << " can't open <" << filename << "> for append\n";
	return;
      }
      
      
      //
      // Write header
      //
      if (myid==0) {
	outlog << endl
	       << "# Level= " << curLevel;
	if (sim->SI()->ptype == StateInfo::Mixture ||
	    sim->SI()->ptype == StateInfo::Extended)
	  outlog << " Nmix= " << sim->SI()->M
		 << " Ndim= " << sim->SI()->Ndim;
	if (sim->SI()->ptype == StateInfo::Extended)
	  outlog << " Next= " << sim->SI()->Next;
	outlog << endl;
      }

      outlog.setf(ios::scientific);
      outlog.precision(8);
	
      for (vector<ChainPtr>::iterator 
	     i=particles.begin(); i!=particles.end(); i++) {

	outlog << setw(5)  << myid;
	outlog << setw(16) << (*i)->weight 
	       << setw(16) << (*i)->value;
	for (unsigned j=0; j<(*i)->N0(); j++) 
	  outlog << setw(16) << (*i)->Vec0(j);
	outlog << endl;
      }
      
      outlog.close();
    
    }
  }
  
  if (mpi_used) {
      ++MPIMutex;    
      MPI_Barrier(MPI_COMM_WORLD);
      --MPIMutex;    
  }
}
