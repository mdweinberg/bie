#include <sstream>

#include <gfunction.h>
#include <BIEconfig.h>
#include <BIEMutex.h>
#include <FileExists.H>

#include <Prior.h>
#include <gfunction.h>
#include <MixturePrior.h>
#include <BIEconfig.h>
#include <SliceSampler.h>
#include <LikelihoodComputation.h>

using namespace std;
using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SliceSampler)

				// Global parameters
				// -----------------

SliceSampler::
Control SliceSampler::cntrl                    = parallel;


SliceSampler::SliceSampler(int number, double wfrac,
			   StateInfo* si, BaseDataTree* d, Model *m,
			   Integration* i, Converge* c, Prior* p, 
			   LikelihoodComputation *l, MCAlgorithm *mca,
			   Simulation* last) : 
Simulation(si, d, m, i, c, p, l, mca, last)
{
  algoname   = "Block Sampler";


  ninit      = number;
  w          = wfrac;
  first      = true;
  clamp      = false;

  totalnum   = 0;		// Number of accepted states
  totaltry   = 0;		// Number of attempted states

				// Exponential variates
  nexp       = NegExpPtr(new NegativeExpntl(1.0, BIEgen));

				// Count interactions to select dimension
  iterCount  = 0;

				// Diagnostic counters
  minCut     =  INT_MAX;
  maxCut     =  0;
  totStep    =  0;
  leftStep   =  0;
  rightStep  =  0;
  iterations =  0;

				// Diagnostic timers
  totlTime.Microseconds();
  lineTime.Microseconds();

  CreateChains();
}

SliceSampler::SliceSampler(int number, double wfrac,
			   StateInfo* si, Converge* c, Prior* p, 
			   LikelihoodComputation *l, MCAlgorithm *mca,
			   Simulation* last) :
  Simulation(si, c, p, l, mca, last)
{
  algoname   = "Block Sampler";


  ninit      = number;
  w          = wfrac;
  first      = true;
  clamp      = false;

  totalnum   = 0;		// Number of accepted states
  totaltry   = 0;		// Number of attempted states

				// Uniform variates
  unit       = UniformPtr(new Uniform(0.0, 1.0, BIEgen));

				// Exponential variates
  nexp       = NegExpPtr(new NegativeExpntl(1.0, BIEgen));

				// Count interactions to select dimension
  iterCount  = 0;

				// Diagnostic counters
  minCut     =  INT_MAX;
  maxCut     =  0;
  totStep    =  0;
  leftStep   =  0;
  rightStep  =  0;
  iterations =  0;

				// Diagnostic timers
  totlTime.Microseconds();
  lineTime.Microseconds();
}


void SliceSampler::CreateChains()

{
  if (cntrl!=parallel) return;

  if (myid==0) {
    chains.clear();		// Erase all previous chains
    for (int n=0; n<numprocs; n++) {
				// Create new chains
      chains.push_back(Chain(_si));
				// Assign ownership
      chains.back().owner = n;
    }
  }

  GatherChains();		// Root gets values from all other
				// processes
}

void SliceSampler::GatherChains()
{
  if (cntrl!=parallel) return;
  
  if (myid==0) {
    chains[0] = ch;
    for (int n=1; n<numprocs; n++)
      chains[n].RecvChain(n);
  } else {
    ch.SendChain(0);
  }
}


void SliceSampler::MCMethod()
{
  totlTime.restart();

  if (first) {
    initialize();
    first = false;
  }

  // Get the vector parameter for this state;
  //
  std::vector<double> st = ch.Vec0();
  unsigned d = st.size(), beg = 0;

  // Are we in a block?
  //
  if (ch.Type()==StateInfo::Block && ch.Cur()->bIndex()>=0) {
    beg = ch.Cur()->bBeg();
    d = beg - ch.Cur()->bEnd() + 1;
  }

  // Values for tree update
  //
  DV2 points;
  DV2 values;
  DV  valueS;

  //------------------------------------------------------------------------
  // Coordinate loop
  //------------------------------------------------------------------------
  //
  for (unsigned long c=0; c<d; c++) {

    // Temporary chains
    //
    Chain test0(ch), test1(ch);

    //------------------------------------------------------------------------
    // Step 1: get y
    //------------------------------------------------------------------------
    double x = st[c];
    double y = ch.value - (*nexp)();

    //------------------------------------------------------------------------
    // Step 2: get estimate of bounds along dimension c by getting density cut.
    //         The "lineCut" dominates the CPU time in this routine (sans the 
    //         time spent in the target density evaluation.
    //------------------------------------------------------------------------
    
    // Timing data for debugging
    lineTime.restart();

    std::vector<ORBTree::boxPtr> rects = tree->lineCut(st, 1.0, c);

    minCut = std::min<unsigned>(minCut, rects.size());
    maxCut = std::max<unsigned>(maxCut, rects.size());

    // Timing data for debugging
    //
    lineTime.stop();

				// Make an array and find bounding interval
    DV xx(1, std::get<0>(*rects[0]));
    for (size_t i=0; i<rects.size(); i++)
      xx.push_back(std::get<1>(*rects[i]));

				// Vector xx is sorted by construction
    DV::iterator it = std::upper_bound(xx.begin(), xx.end(), x);
    size_t indx     = rects.size() - 1;

				// Sanity check
    if (it != xx.end()) indx = std::max<size_t>(it - xx.begin() - 1, 0);

    // Indx should point to the rectangle containing x; and presumably
    // contain f(x), since the largest sample so far exceeds y by
    // construction

    // So, begin at position indx and go backwards and forward to find
    // end points of the range
    //
    double xmin = xx.front(), xmax = xx.back();
    double intvl0 = xmax - xmin;

    if (indx > 0) {		// Smaller x values
      
      for (int i=indx; i>=0; i--) {
	double yt = std::get<2>(*rects[i])[0];
	if (y > yt) {
	  xmin = std::get<0>(*rects[i]);
	  break;
	}
      }
    }

    if (indx < xx.size()-1) {	// Larger x values
      for (size_t i=indx; i<rects.size(); i++) {
	double yt = std::get<2>(*rects[i])[0];
	if (y > yt) {
	  xmax = std::get<1>(*rects[i]);
	  break;
	}
      }
    }
  
    double intvl1 = xmax - xmin;

    //------------------------------------------------------------------------
    // Step 3: evalute point in the interval
    //------------------------------------------------------------------------
    
				// Evaluate the endpoints
    test0.Vec0(c) = xmin;
    test1.Vec0(c) = xmax;

    _mca->ComputeCurrentState(this, &test0);
    _mca->ComputeCurrentState(this, &test1);

    DV i0(1, test0.value), i1(1, test1.value);

    points.push_back(test0.Vec0());
    points.push_back(test1.Vec0());
    valueS.push_back(i0[0]);
    valueS.push_back(i1[0]);
    
    //------------------------------------------------------------------------
    // Step 4: apply Neal's step-out algorithm and save all the points
    //------------------------------------------------------------------------

    double W = w * (xmax - xmin);

    bool leftDone  = false;	// Logic to prevent endless loop due to
    bool rightDone = false;	// renormalization of state to enforce
				// limits

				// Ok iterate until we have good sample . . . 
    while ( (i0[0]>y && !leftDone ) || (i1[0]>y && !rightDone) ) {

      // Left side
      //
      if (i0[0]>y && !leftDone) { 
	test0.Vec0(c) = std::max<double>(test0.Vec0(c)-W, xx.front());
	if (test0.Vec0(c)<=xx.front()) leftDone = true;
	_mca->ComputeCurrentState(this, &test0);
	points.push_back(test0.Vec0());
	i0[0] = test0.value;
	valueS.push_back(i0[0]);
	leftStep++;
      }
      
      // Right side
      //
      if (i1[0]>y && !rightDone) {
	test1.Vec0(c) = std::min<double>(test1.Vec0(c)+W, xx.back());
	if (test1.Vec0(c)>=xx.back()) rightDone = true;
	_mca->ComputeCurrentState(this, &test1);
	points.push_back(test1.Vec0());
	i1[0] = test1.value;
	valueS.push_back(i1[0]);
	rightStep++;
      }
    }

				// Found a "good" interval, now
				// iterate to find a sample
    xmin = std::max<double>(test0.Vec0(c), xx.front());
    xmax = std::min<double>(test1.Vec0(c), xx.back());
    qint0.add((xmax - xmin)/intvl0);
    totStep++;

    double x0 = ch.Vec0(c);	// Fiducial point

    unsigned iter = 0;
    const unsigned safety = 40000;

    while (iter++<safety) {
      double xt = test0.Vec0(c) = xmin + (*unit)()*(xmax - xmin);
      _mca->ComputeCurrentState(this, &test0);
      iterations++;
				// Save the states for the tree
      i0[0] = test0.value;
      points.push_back(test0.Vec0());
      valueS.push_back(i0[0]);

      if (i0[0] >= y) break;	// Done?
      
				// Shrink the interval
      if (xt < x0) xmin = xt;
      else         xmax = xt;

      if (xmax <= xmin) break;	// Prevent renormalization loop
    } 

    if (iter<safety) {
      qint1.add((xmax - xmin)/intvl1);
      // This is the sample from the target distribution!  
      ch = test0;
    } else {
      std::cerr << "SliceSampler: bad sample!" << std::endl;
      // Reuse the current sample
    }
  
    // Contine on to next dimension . . .
  }

  //
  // New state is now in the chain
  //

  if (clamp) {
    size_t dim = ch.N0();
    for (size_t d=0; d<dim; d++) {
      ch.Vec0(d) = std::min<double>(ch.Vec0(d), _upper[d]);
      ch.Vec0(d) = std::max<double>(ch.Vec0(d), _lower[d]);
    }
  }

  //------------------------------------------------------------------------
  // Update the tree
  //------------------------------------------------------------------------

  // Only have evaluations on other processes than root if the control
  // method is "parallel"

  if (cntrl == parallel) {
				// Number of points cached in this step
    int size = points.size();
    
				// Get the counts from each process
    std::vector<int> receiveCounts(numprocs);
    std::vector<int> displacements(numprocs);
    std::vector<int> numberCounts (numprocs, 1);

    for (int i=0; i<numprocs; i++) displacements[i] = i;
    
    MPI_Allgatherv(&size, 1, MPI_INT, &receiveCounts[0], &numberCounts[0], 
		   &displacements[0], MPI_INT, MPI_COMM_WORLD); 
   
    int totalR = 0;		// Total number of records from all
				// processes
    for (int i=0; i<numprocs; i++) totalR += receiveCounts[i];

				// Now prepare the data blocks
    int dim = points[0].size();
    std::vector<double> sndblk(size*(dim+1));
    
    int icnt = 0;		// Coordinates + prob value
    for (int i=0; i<size; i++) {
      for (int j=0; j<dim; j++) sndblk[icnt++] = points[i][j];
      sndblk[icnt++] = valueS[i];
    }
				// Create the displacments for data
				//
    numberCounts [0]    = receiveCounts[0] * (dim + 1);
    displacements[0]    = 0;
    int totsiz          = numberCounts[0];
    for (int i=1; i<numprocs; i++) {
      displacements[i]  = displacements[i-1] + numberCounts [i-1];
      numberCounts [i]  = receiveCounts[i] * (dim + 1);
      totsiz           += numberCounts[i];
    }

    std::vector<double> recvblk(totsiz);
    
    MPI_Allgatherv(&sndblk[0], icnt, MPI_DOUBLE, &recvblk[0],
		   &numberCounts[0], &displacements[0], MPI_DOUBLE, 
		   MPI_COMM_WORLD);
    
    points.resize(totalR);
    values.resize(totalR);

    icnt = 0;
    for (int i=0; i<totalR; i++) {
      points[i].resize(dim);
      for (int j=0; j<dim; j++) points[i][j] = recvblk[icnt++];
      values[i] = DV(1, recvblk[icnt++]);
    }
  }

  std::vector<double>   weight(points.size(), 1.0);
  std::vector<double>   beta  (points.size(), 1.0);
  std::vector<unsigned> mult  (points.size(), 1  );

  Timer orbTime(true);

  if (myid==0) orbTime.start();

  tree->addPoints(values, points, mult, weight, beta);

  if (myid==0) {
    double time = 1.0e-6 * orbTime.stop().getTotalTime();
    std::cout << "Spent " << time << " seconds in orbTree adding " 
	      << points.size() << " points" << std::endl;
  }

  GatherChains();

  //------------------------------------------------------------------------
  // Done
  //------------------------------------------------------------------------

  totlTime.stop();
}


void SliceSampler::PrintStepDiagnostic() 
{
  TimeElapsed totl = totlTime.getTime();
  TimeElapsed line = lineTime.getTime();
  TimeElapsed rest = totl - line;

  static bool first = true;
  static std::string head0, labl0, head1, labl1;

  if (first) {

    std::ostringstream header0, labels0;
    std::ostringstream header1, labels1;

    header0 << std::setfill('-')
	    << std::string(5, ' ')  << " + "
	    << std::setw( 7) << '-' << " + " 
	    << std::setw( 7) << '-' << " + " 
	    << std::setw( 7) << '-' << " + " 
	    << std::setw( 7) << '-' << " + " 
	    << std::setw( 7) << '-' << " + "
	    << std::setw(10) << '-' << " + "
	    << std::setw(10) << '-' << " + "
	    << std::setw( 9) << '-' << " + ";

    labels0 << std::string(5, ' ')         << " | "
	    << std::setw( 7) << "Min C"    << " | " 
	    << std::setw( 7) << "Max C"    << " | " 
	    << std::setw( 7) << "Right"    << " | " 
	    << std::setw( 7) << "Left"     << " | " 
	    << std::setw( 7) << "Iters"    << " | "
	    << std::setw(10) << "Cut time" << " | "
	    << std::setw(10) << "Tot time" << " | "
	    << std::setw( 9) << "Tree pop" << " | ";

    header1 << std::setfill('-')
	    << std::string(5, ' ')  << " + "
	    << std::setw( 7) << '-' << " + " 
	    << std::setw(10) << '-' << " + " 
	    << std::setw(10) << '-' << " + " 
	    << std::setw(10) << '-' << " + " 
	    << std::setw(10) << '-' << " + " 
	    << std::setw(10) << '-' << " + " 
	    << std::setw(10) << '-' << " + ";

    labels1 << std::string(5, ' ')         << " | "
	    << std::setw( 7) << "Steps"    << " | "
	    << std::setw(10) << "10% I"    << " | " 
	    << std::setw(10) << "50% I"    << " | " 
	    << std::setw(10) << "90% I"    << " | "
	    << std::setw(10) << "10% S"    << " | " 
	    << std::setw(10) << "50% S"    << " | " 
	    << std::setw(10) << "90% S"    << " | ";

    head0 = header0.str();
    labl0 = labels0.str();

    head1 = header1.str();
    labl1 = labels1.str();

    first = false;
  }

  std::cout << std::endl
	    << head0 << std::endl 
	    << labl0 << std::endl 
	    << head0 << std::endl;

  std::cout << std::setprecision(2)
	    << std::string(5, ' ')         << " | "
	    << std::setw( 7) << minCut     << " | " 
	    << std::setw( 7) << maxCut     << " | " 
	    << std::setw( 7) << rightStep  << " | " 
	    << std::setw( 7) << leftStep   << " | " 
	    << std::setw( 7) << iterations << " | "
	    << std::setw(10) << 1.0e-6*line.getTotalTime() << " | "
	    << std::setw(10) << 1.0e-6*rest.getTotalTime() << " | "
	    << std::setw( 9) << tree->numberInTree()       << " | "
	    << std::endl << head0 << std::endl << std::endl;

  std::cout << head1 << std::endl 
	    << labl1 << std::endl 
	    << head1 << std::endl;

  std::cout << std::setprecision(3)
	    << std::string(5, ' ')         << " | "
	    << std::setw( 7) << totStep    << " | " 
	    << std::setw(10) << qint0(0.1) << " | " 
	    << std::setw(10) << qint0(0.5) << " | " 
	    << std::setw(10) << qint0(0.9) << " | " 
	    << std::setw(10) << qint1(0.1) << " | " 
	    << std::setw(10) << qint1(0.5) << " | " 
	    << std::setw(10) << qint1(0.9) << " | " 
	    << std::endl << head1 << std::endl << std::endl;
}

void SliceSampler::ResetDiagnostics() 
{
  iterations =  0;
  rightStep  =  0;
  leftStep   =  0;
  totStep    =  0;
  minCut     =  INT_MAX;
  maxCut     =  0;

  qint0.reset();
  qint1.reset();
}

void SliceSampler::initialize()
{
  // Initialize interval statistics for diagnosing the performance of
  // the slice sampler algorithm
  //
  qint0 = Quantile(0.0, 1.0, 100);
  qint1 = Quantile(0.0, 1.0, 100);

  // Cache for convenience and speed; uses a weensy bit more memory
  //
  _lower = GetPrior()->lower();
  _upper = GetPrior()->upper();

  if (myid==0) {
    cout << "-----------------------------" << endl
	 << " Slice Sampler: range limits " << endl
	 << "-----------------------------" << endl;
    for (size_t i=0; i<_lower.size(); i++) {
      cout << right << setw(3) << i 
	   << setw(16) << _lower[i]
	   << setw(16) << _upper[i]
	   << endl;
    }
    cout << "--------------" << endl;  
  }

  BSPTree::SetRangeBounds(_lower, _upper);

				// Sample from prior and make a list
				// of states to initialize the ORBTree
				//
  std::vector< std::vector<double> > pvalue, points;
  std::vector< double >              weight, beta, in(1);
  std::vector< unsigned >            mult;

  Chain c(ch);

  if (cntrl == parallel) {

    size_t dim = c.N0();
    size_t in_size = ninit * (dim+1);
    size_t ot_size = in_size * numprocs;
    std::vector<double> indata(in_size);
    std::vector<double> otdata(ot_size);
    unsigned cnt = 0;

    for (int i=0; i<ninit; i++) {
				// Sample and compute a new state from
				// the prior distribution
      _mca->ComputeNewState(this, &c);
				// Add data to the array buffer
      indata[cnt++] = c.value;
      for (size_t j=0; j<dim; j++)
	indata[cnt++] = c.Vec0(j);
    }

    MPI_Allgather(&indata[0], in_size, MPI_DOUBLE, &otdata[0], in_size,
		  MPI_DOUBLE, MPI_COMM_WORLD);

    cnt = 0;
    DV pnt(dim), in(1);
    for (int i=0; i<ninit*numprocs; i++) {
      in[0] = otdata[cnt++];
      for (size_t j=0; j<dim; j++)
	pnt[j] = otdata[cnt++];

      pvalue.push_back(in);
      points.push_back(pnt);
      weight.push_back(1.0);
      beta  .push_back(1.0);
      mult  .push_back(1  );
    }

  } else {

    for (int i=0; i<ninit; i++) {
				// Sample and compute a new state from
				// the prior distribution
      _mca->ComputeNewState(this, &c);
				// Make the input arrays
      DV in(1, c.value);
      pvalue.push_back(in);
      points.push_back(c.Vec0());
      weight.push_back(1.0);
      beta  .push_back(1.0);
      mult  .push_back(1  );
    }
  }

  // Construct the tree
  //
  Timer orbTime(true);
  if (myid==0) {
    std::cout << "SliceSampler: initialized ORBTree with "
	      << points.size() << " points . . . " << std::flush;
    orbTime.start();
  }

  ORBTree::maxLevel = 14;	// Maximum number of bisections per
				// dimension in the orb tree, approx 1e-4

  tree = ORBTreePtr(new ORBTree(points[0].size(), 1, 
				pvalue, points, mult, weight, beta));

  if (myid==0) {
    std::cout << "done in " << 1.0e-6*orbTime.stop().getTotalTime()
	      << " seconds" << std::endl;
  }
  
  // Chain initialization
  //

  CreateChains();
}

void SliceSampler::SetControl(int m)
{
  switch (m) {
  case parallel:
    cntrl = parallel;
    break;
  case serial:
    if (mpi_used)
      cntrl = serial;
    else {
      cout << "SliceSampler: you want _serial_ control but you must"   << endl
	   << "                use MPI for that to make any sense.  You" << endl
	   << "                are getting _parallel_ control."          << endl;
    }
    break;
  default:
    ostringstream out;
    out << "Control method should be <parallel> (" << parallel
	<< ") or <serial> (" << serial << ")."
	<< "  input value was " << m;
    throw InternalError(out.str(), __FILE__, __LINE__);
  }
}


void SliceSampler::LogState(string& outfile)
{
  ostream cout(checkTable("cli_console"));

  State currentState = chfid->GetState0();

                                // Try only if outfile is not null
  if (outfile.size()>0) {
    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile <<
        "> for append\n";
      return;
    }

    if (first_log) {
      char q = '"';

      logf << setw(20) << "\"Probability\"";
      logf << setw(20) << "\"Likelihood\"";
      logf << setw(20) << "\"Prior\"";

      if (currentState.Type() == StateInfo::Mixture ||
	  currentState.Type() == StateInfo::Extended )
	logf << setw(10) << "\"Number\"";

      for (unsigned k=0; k<currentState.size(); k++) {
	ostringstream ostr;
	ostr << q << currentState.ParameterDescription(k) << q;
        logf << setw(output_prec+10) << ostr.str();
      }
      logf << endl;

      first_log = false;
    }

    for (std::vector<Chain>::iterator c=chains.begin(); c!=chains.end(); c++) {

      logf << scientific
	   << setw(20) << setprecision(12) << c->value
	   << setw(20) << setprecision(12) << c->like_value
	   << setw(20) << setprecision(12) << c->prior_value;
    
      currentState = c->GetState0();

      if (currentState.Type() == StateInfo::Mixture ||
	  currentState.Type() == StateInfo::Extended ) {

	logf << setw(10) << currentState.M();

	// DEBUG: weight check
	double sum = 0.0;
	for (unsigned k=0; k<currentState.M(); k++) {
	  if (currentState.Wght(k)<0.0 || currentState.Wght(k)>1.0) {
	    cerr << "Weight is out of bounds!" << endl;
	  }
	  sum += currentState.Wght(k);
	}
	if (fabs(sum-1.0)>1.0e-18) {
	  cerr << "Weight NORM is out of bounds!" << endl;
	}
	//
      }
    
      logf << setprecision(output_prec);

      for (int k=0; k<(int)currentState.size(); k++)
	logf << setw(output_prec+10) << currentState[k];
      logf << endl;
    }
  }
}


void SliceSampler::LogState(int level, int iterno, string& outfile)
{
  ostream cout(checkTable("cli_console"));
				// Try only if outfile is not null
  if (outfile.size()>0) {


    State currentState = chfid->GetState0();

				// Check for existence of output file 
				// before creating
    static bool firstime = true;
    bool exists = true;
    if (firstime) {
      exists = FileExists(outfile);
      firstime = false;
    }

				// Now, open it
    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile << "> for append\n";
      return;
    }
				// Label fields first time through . . .
				// Labels are in " " (so that embedded 
				// spaces are ok)
    if (exists == false) {

      char q = '"';

      logf << setw(7)              << "\"Level\"" 
	   << setw(10)             << "\"Iter\""
	   << setw(output_prec+10) << "\"Probability\""
	   << setw(output_prec+10) << "\"Likelihood\""
	   << setw(output_prec+10) << "\"Prior\"";

      if (_si->ptype==StateInfo::Mixture || _si->ptype==StateInfo::Extended)
	logf << setw(10) << "\"Number\"";

      for (unsigned k=0; k<_si->Ntot; k++) {
	ostringstream ostr;
	ostr << q << _si->ParameterDescription(k) << q;
        logf << setw(output_prec+10) << ostr.str();
      }
      logf << endl;
    }

    for (std::vector<Chain>::iterator c=chains.begin(); c!=chains.end(); c++) {

				// Write state
      logf << setw(7) << level << setw(10) << iterno;
      logf << scientific
	   << setw(output_prec+10) << setprecision(output_prec) << c->value 
	   << setw(output_prec+10) << setprecision(output_prec) << c->like_value
	   << setw(output_prec+10) << setprecision(output_prec) << c->prior_value;

      currentState = c->GetState0();

      if (_si->ptype==StateInfo::Mixture || _si->ptype==StateInfo::Extended) {

	unsigned Mcur = currentState.M();
	unsigned Mmax = currentState.Mmax();
	unsigned Ndim = currentState.Ndim();
	unsigned Next = currentState.Next();
	
	logf << setw(10) << Mcur;
	logf << setprecision(output_prec);
	
	for (unsigned i=0; i<Mcur; i++)
	  logf << setw(output_prec+10) << currentState.Wght(i);
	
	for (unsigned i=Mcur; i<Mmax; i++)
	  logf << setw(output_prec+10) << 0.0;
	
	for (unsigned i=0; i<Mcur; i++) {
	  for (unsigned j=0; j<Ndim; j++) 
	    logf << setw(output_prec+10) << currentState.Phi(i, j);
	}
	for (unsigned i=Mcur; i<Mmax; i++) {
	  for (unsigned j=0; j<Ndim; j++) 
	    logf << setw(output_prec+10) << 0.0;
	}
	
	for (unsigned j=0; j<Next; j++) 
	  logf << setw(output_prec+10) << currentState.Ext(j);
	
      } else {
	unsigned Ntot = currentState.N();
	logf << setprecision(output_prec);
	for (unsigned i=0; i<Ntot; i++)
	  logf << setw(output_prec+10) << currentState[i];
      }
      logf << endl;
    }
  }
}
