// -*- C++ -*-

/* 
   Reads in a multilevel output file and analyzes convergence
   using Gelman and Rubin (1992) with outlier detection and
   marginal likelihood computation
*/


#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <memory>
#include <cfloat>
#include <string>
#include <vector>
#include <random>
#include <array>
#include <cmath>
#include <deque>
#include <map>

#include <unistd.h>

#include <GRanalyze.h>


double GRanalyze::GR(unsigned endpt, unsigned psize)
{

  if (verbose and myid==0) {
				// Separator
    std::cout << std::setfill('-') << std::setw(72) << "-" << std::endl
	      << "End point=" << endpt << "  Count=" << psize << std::endl
	      << std::setw(72) << "-" << std::endl << std::setfill(' ');
  }
  


  //***************************************************
  //  Begin computation
  //***************************************************
  
				// Number of chains
  unsigned Mchain = data[0].size();
                                // Number of values per chain 
  unsigned Nparam = data[0][0].size();

				// Number of steps to use
  unsigned Nsteps = psize;

  unsigned begpt;
  if (endpt+1<psize) begpt = 0;
  else begpt = endpt+1-psize;
				// 
				// Gelman & Rubin quantities
				// 
  std::vector<double> B(Nparam, 0.0), W(Nparam, 0.0), meanS(Nparam, 0.0);
  std::vector<double> Sig2Hat(Nparam, 0.0), rHat(Nparam, 0.0);

				// 
				// Count non-aberrant chains
				// 
  unsigned Mgood = 0;
  for (unsigned n=0; n<mask.size(); n++) 
    if (mask[n]==0) Mgood++;

				// 
				// Set up vectors for in-chain analysis
				// 
  std::vector< std::vector<double> > meanCh(Mchain), varCh(Mchain);
  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    meanCh[i]  = std::vector<double>(Nparam, 0.0);
    varCh[i]   = std::vector<double>(Nparam, 0.0);
  }
				// 
				// Compute means and variance of each chain
				//
  for (unsigned n=begpt; n<=endpt; n++) {
    for (unsigned i=0; i<Mchain; i++) {
      if (mask[i]) continue;
      for (unsigned j=0; j<Nparam; j++) {
	meanCh[i][j]  += data[n][i][j];
	varCh [i][j]  += data[n][i][j]*data[n][i][j];
      }
    }
  }

  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    for (unsigned j=0; j<Nparam; j++) {
      meanCh[i][j] /= Nsteps;
      varCh[i][j] = 
        (varCh[i][j] - meanCh[i][j]*meanCh[i][j]*Nsteps)/(Nsteps-1);
      meanS[j] += meanCh[i][j]/Mgood;
    }
  }
                                // 
                                // Compute the Gelman & Rubin quantities
                                // 
  
  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    for (unsigned j=0; j<Nparam; j++) {
      B[j] += 
        (meanCh[i][j] - meanS[j])*(meanCh[i][j] - meanS[j]) *
        Nsteps/(Mgood-1);
      W[j] += varCh[i][j]/Mgood;
    }
  }
    
  
  for (unsigned j=0; j<Nparam; j++) {
    if (W[j]>0.0) {
      Sig2Hat[j] = (W[j]*(Nsteps-1) + B[j])/Nsteps;
      // Gelman & Brooks 1998 "correction"
      rHat[j] = sqrt(Sig2Hat[j]/W[j]*(Mgood+1)/Mgood - 
		     static_cast<double>(Nsteps-1)/(Mgood*Nsteps) );
    }
  }
  
  
  if (verbose and dbg_verbose and myid==0) {
    std::cout << std::endl 
	      << "Per chain data, N=" << Nsteps
	      << " M=" << Mgood << "/" << Mchain
	      << ": " << std::endl << std::endl
	      << std::setw(4) << std::left << "#";
    for (unsigned j=0; j<Nparam; j++) {
      ostringstream out1, out2;
      out1 << "Mean [" << j << "]";
      out2 << "Var [" << j << "]";
      std::cout << std::setw(15) << std::left << out1.str()
		<< std::setw(15) << std::left << out2.str();
    }
    std::cout << std::endl
	      << std::setw(4) << std::left << "-";
    for (unsigned j=0; j<Nparam; j++) {
      ostringstream out1, out2;
      out1 << "Mean [" << j << "]";
      out2 << "Var [" << j << "]";
      std::cout << std::setw(15) << std::left << "---------"
		<< std::setw(15) << std::left << "---------";
    }
    std::cout << std::endl;
    
    for (unsigned i=0; i<Mchain; i++) {
      std::cout << std::setw(4) << i;
      if (mask[i])
	for (unsigned j=0; j<Nparam; j++) {
	  std::cout << std::setw(15) << std::left << "*******"
		    << std::setw(15) << std::left << "*******";
	}
      else
	for (unsigned j=0; j<Nparam; j++) {
	  std::cout << std::setw(15) << std::left << meanCh[i][j]
		    << std::setw(15) << std::left << varCh[i][j];
	}
      std::cout << std::endl;
    }
    std::cout << std::endl << std::setw(4) << "**";
    for (unsigned j=0; j<Nparam; j++) {
      std::cout << std::setw(15) << std::left << meanS[j]
		<< std::setw(15) << std::left << B[j]/Nsteps;
    }
    std::cout << std::endl;
  }
      

  if (verbose and myid==0)
    std::cout << std::endl 
	      << "Convergence summary, N=" << Nsteps
	      << " M=" << Mgood << "/" << Mchain
	      << ": " << std::endl << std::endl
	      << std::setw(4)  << std::left << "#"
	      << std::setw(15) << std::left << "Mean"
	      << std::setw(15) << std::left << "Between"
	      << std::setw(15) << std::left << "Within"
	      << std::setw(15) << std::left << "Total"
	      << std::setw(15) << std::left << "Rhat"
	      << std::endl
	      << std::setw(4)  << std::left << "-"
	      << std::setw(15) << std::left << "----"
	      << std::setw(15) << std::left << "-------"
	      << std::setw(15) << std::left << "------"
	      << std::setw(15) << std::left << "-----"
	      << std::setw(15) << std::left << "----"
	      << std::endl;
  
  double minParam = DBL_MAX, maxParam = 0.0;
  for (unsigned j=0; j<Nparam; j++) {

    if (verbose and myid==0)
      std::cout << std::setw(4)  << j
		<< std::setw(15) << meanS[j]
		<< std::setw(15) << B[j]/Nsteps
		<< std::setw(15) << W[j]
		<< std::setw(15) << Sig2Hat[j]
		<< std::setw(15) << rHat[j]
		<< std::endl;
    
    minParam = std::min<double>(rHat[j], minParam);
    maxParam = std::max<double>(rHat[j], maxParam);
  }

  if (verbose and myid==0) {
    std::cout << std::endl
	      << "Min, Max = " << minParam << ", " << maxParam << std::endl;
  }

  return maxParam;
}

double GRanalyze::GR(unsigned endpt, unsigned psize, 
		     int T, int N1, int N2)
{

  if (verbose and myid==0) {
				// Separator
    std::cout << setfill('-') << std::setw(72) << "-" << std::endl
	      << "End point=" << endpt << "  Count=" << psize << std::endl
	      << std::setw(72) << "-" << std::endl << setfill(' ');
  }
  

  //***************************************************
  //  Begin computation
  //***************************************************
  
				// Number of chains
  unsigned Mchain = data[0].size();

				// Number of values per chain 
  std::vector<unsigned> Nparam(2);
  Nparam[0] = T + N1;
  Nparam[1] = T + N2;
				// Map to parameter vector for each component
  std::vector< std::vector<unsigned> > MAP(2);
  for (unsigned m=0; m<2; m++) {
    for (int i=0; i<T; i++) MAP[m].push_back(1 + i);
  }
  for (int i=0; i<N1; i++) MAP[0].push_back(1 + T + i);
  for (int i=0; i<N2; i++) MAP[1].push_back(1 + T + N1 + i);

  unsigned begpt;
  if (endpt+1<psize) begpt = 0;
  else begpt = endpt+1-psize;
				// 
				// Gelman & Rubin quantities
				// 
  // For each component
  std::vector< std::vector<double> > B(2), W(2), meanS(2);
  std::vector< std::vector<double> > Sig2Hat(2), rHat(2);

  for (unsigned m=0; m<2; m++) {
    std::vector<double> tmp(Nparam[m], 0.0);
    B[m]       = tmp;
    W[m]       = tmp;
    meanS[m]   = tmp;
    Sig2Hat[m] = tmp;
    rHat[m]    = tmp;

  }
                                // 
                                // Set up vectors for in-chain analysis
                                // 
  std::vector< std::vector< std::vector<double> > > meanCh(2), varCh(2);
  std::vector< std::vector<unsigned> > Nmodel(2);

				// For indicator variable
  std::vector<double> meanCh_ind(Mchain, 0), varCh_ind(Mchain, 0);
  unsigned Nsteps = endpt - begpt + 1;

  for (unsigned m=0; m<2; m++) {
    meanCh[m] = std::vector< std::vector<double> >(Mchain);
    varCh [m] = std::vector< std::vector<double> >(Mchain);
    Nmodel[m] = std::vector<unsigned>(Mchain, 0);
    for (unsigned i=0; i<Mchain; i++) {
      if (mask[i]) continue;
      meanCh[m][i] = std::vector<double>(Nparam[m], 0.0);
      varCh [m][i] = std::vector<double>(Nparam[m], 0.0);
    }
  } 
				// 
				// Compute means and variance of each chain
				//
  for (unsigned n=begpt; n<=endpt; n++) {
    for (unsigned i=0; i<Mchain; i++) {
      if (mask[i]) continue;
      unsigned m = static_cast<unsigned>(floor(data[n][i][0]+0.01));
      meanCh_ind[i] += m;
      varCh_ind[i]  += m*m;
      Nmodel[m][i]++;
      for (unsigned j=0; j<Nparam[m]; j++) {
        meanCh[m][i][j]  += data[n][i][MAP[m][j]];
        varCh [m][i][j]  += data[n][i][MAP[m][j]]*data[n][i][MAP[m][j]];
      }
    }
  }

				// Count good chains per model
  std::vector<unsigned> Ngood(2, 0);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<Mchain; i++)
      if (Nmodel[m][i]) Ngood[m]++;
  }

				// For indicator variable
  unsigned Mgood_ind = 0;
  for (unsigned i=0; i<Mchain; i++) { if (!mask[i]) Mgood_ind++; }
  double meanS_ind = 0.0;

  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    unsigned Ntot = 0;
    for (unsigned m=0; m<2; m++) {
      Ntot += Nmodel[m][i];
      if (Nmodel[m][i]) {
	for (unsigned j=0; j<Nparam[m]; j++) {
	  meanCh[m][i][j] /= Nmodel[m][i];
	  if (Nmodel[m][i]>1)
	    varCh[m][i][j] = 
	      (varCh[m][i][j] - meanCh[m][i][j]*meanCh[m][i][j]*Nmodel[m][i])
	      /(Nmodel[m][i]-1);
	  else
	    varCh[m][i][j] = 0.0;

	  meanS[m][j] += meanCh[m][i][j]/Ngood[m];
	}
      }
    }

    if (Ntot) meanCh_ind[i] /= Ntot;
    if (Ntot>1)
      varCh_ind[i] = (varCh_ind[i] - meanCh_ind[i]*meanCh_ind[i]*Ntot)/(Ntot-1);
    else
      varCh_ind[i] = 0.0;
    
    meanS_ind += meanCh_ind[i]/Mgood_ind;
  }

				// Compute the Gelman & Rubin quantities
				// 
  std::vector<unsigned> Wcnt(2, 0), Wvar(2, 0);
  
  for (unsigned i=0; i<Mchain; i++) {
    if (mask[i]) continue;
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	if (Ngood[m]>1) {
	  double del = meanCh[m][i][j] - meanS[m][j];
	  B[m][j] += del*del * Nmodel[m][i]/(Ngood[m]-1);
	}
	if (Ngood[m]) W[m][j] += varCh[m][i][j]/Ngood[m];
	Wcnt[m] += Nmodel[m][i];
	Wvar[m] += Nmodel[m][i] * Nmodel[m][i];
      }
    }
  }
  
  for (unsigned m=0; m<2; m++) 
    if (Ngood[m]) {
      Wcnt[m] /= Ngood[m];
      if (Ngood[m]>1) {
	Wvar[m] = (Wvar[m] - Wcnt[m]*Wcnt[m]*Ngood[m])/(Ngood[m]-1);
      } else {
	Wvar[m] = 0.0;
      }
    }

  for (unsigned m=0; m<2; m++) {
    for (unsigned j=0; j<Nparam[m]; j++) {
      if (Wcnt[m]>0) {
	Sig2Hat[m][j] = (W[m][j]*(Wcnt[m]-1) + B[m][j])/Wcnt[m];
	// Gelman & Brooks 1998 "correction"
	rHat[m][j] = sqrt( Sig2Hat[m][j]/W[m][j]*(Ngood[m]+1)/Ngood[m] -
			   static_cast<double>(Wcnt[m]-1)/(Ngood[m]*Wcnt[m]) );
      }
    }
  }
  
  
  double B_ind = 0.0, W_ind = 0.0;
  for (unsigned i=0; i<Mchain; i++) {
    B_ind += 
      (meanCh_ind[i] - meanS_ind)*(meanCh_ind[i] - meanS_ind)*Nsteps /
      (Mgood_ind-1);
    W_ind += varCh_ind[i]/Mgood_ind;
  }

  double Sig2Hat_ind = (W_ind*(Nsteps - 1) + B_ind)/Nsteps;

  // Gelman & Brooks 1998 "correction"
  double rHat_ind = 1.0;
  if (W_ind>1.0e-12) 
    rHat_ind = sqrt(Sig2Hat_ind/W_ind*(Mgood_ind+1)/Mgood_ind -
		    static_cast<double>(Nsteps-1)/(Mgood_ind*Nsteps));
  
  std::vector<unsigned> Tsteps(2, 0);
  for (unsigned m=0; m<2; m++) {
    for (unsigned i=0; i<Mchain; i++) Tsteps[m] += Nmodel[m][i];
  }

  if (verbose and dbg_verbose and myid==0) {

    std::cout << std::endl 
	      << "Per chain data, N=[" << Tsteps[0] << ", " << Tsteps[1]
	      << "] M=[" << Ngood[0] << ", " << Ngood[1] << "]/" << Mchain
	      << ": " << std::endl << std::endl
	      << std::setw(4) << std::left << "#";

    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	ostringstream out1, out2;
	out1 << "Mean [" << m << ", " << j << "]";
	out2 << "Var [" << m << ", " << j << "]";
	std::cout << std::setw(15) << std::left << out1.str()
		  << std::setw(15) << std::left << out2.str();
      }
    }
    std::cout << std::endl
	      << std::setw(4) << std::left << "-";
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	ostringstream out1, out2;
	out1 << "Mean [" << m << ", " << j << "]";
	out2 << "Var [" << m << ", " << j << "]";
	std::cout << std::setw(15) << std::left << "---------"
		  << std::setw(15) << std::left << "---------";
      }
      std::cout << std::endl;
    }
    for (unsigned i=0; i<Mchain; i++) {
      std::cout << std::setw(4) << i;
      for (unsigned m=0; m<2; m++) {
	if (mask[i])
	  for (unsigned j=0; j<Nparam[m]; j++) {
	    std::cout << std::setw(15) << std::left << "*******"
		      << std::setw(15) << std::left << "*******";
	  }
	else
	  for (unsigned j=0; j<Nparam[m]; j++) {
	    std::cout << std::setw(15) << std::left << meanCh[m][i][j]
		      << std::setw(15) << std::left << varCh[m][i][j];
	  }
      }
      std::cout << std::endl;
    }
    std::cout << std::endl << std::setw(4) << "**";
    for (unsigned m=0; m<2; m++) {
      for (unsigned j=0; j<Nparam[m]; j++) {
	std::cout << std::setw(15) << std::left << meanS[m][j]
		  << std::setw(15) << std::left << B[m][j]/Wcnt[m];
      }
    }
    std::cout << std::endl;
  }
      

  if (verbose and myid==0)
    std::cout << std::endl 
	      << "Convergence summary" << std::endl
	      << "-------------------" << std::endl
	      << " N=[" << Tsteps[0] << ", " << Tsteps[1] << "]"
	      << " M=[" << Ngood[0]  << ", " << Ngood[1] << "]/" << Mchain << std::endl
	      << " Count=[" << Wcnt[0]  << ", " << Wcnt[1]  << "]"
	      << " Sigma=[" << floor(sqrt(Wvar[0])+0.5)  
	      << ", " << floor(sqrt(Wvar[1])+0.5)  << "]"
	      << std::endl << std::endl
	      << std::setw(4)  << std::left << "Mod"
	      << std::setw(4)  << std::left << "#"
	      << std::setw(15) << std::left << "Mean"
	      << std::setw(15) << std::left << "Between"
	      << std::setw(15) << std::left << "Within"
	      << std::setw(15) << std::left << "Total"
	      << std::setw(15) << std::left << "Rhat"
	      << std::endl
	      << std::setw(4)  << std::left << "---"
	      << std::setw(4)  << std::left << "---"
	      << std::setw(15) << std::left << "----"
	      << std::setw(15) << std::left << "-------"
	      << std::setw(15) << std::left << "------"
	      << std::setw(15) << std::left << "-----"
	      << std::setw(15) << std::left << "----"
	      << std::endl;

  double minParam = DBL_MAX, maxParam = 0.0;
  for (unsigned m=0; m<2; m++) {
    for (unsigned j=0; j<Nparam[m]; j++) {

      if (verbose and myid==0)
	std::cout << std::setw(4)  << m
		  << std::setw(4)  << j
		  << std::setw(15) << meanS[m][j]
		  << std::setw(15) << B[m][j]/Wcnt[m]
		  << std::setw(15) << W[m][j]
		  << std::setw(15) << Sig2Hat[m][j]
		  << std::setw(15) << rHat[m][j]
		  << std::endl;
      
      minParam = std::min<double>(rHat[m][j], minParam);
      maxParam = std::max<double>(rHat[m][j], maxParam);
    }

  }

  if (verbose and myid==0)
    std::cout << std::setw(8)  << "Ind *"
	      << std::setw(15) << meanS_ind
	      << std::setw(15) << B_ind/Nsteps
	      << std::setw(15) << W_ind
	      << std::setw(15) << Sig2Hat_ind
	      << std::setw(15) << rHat_ind
	      << std::endl;
  
  minParam = std::min<double>(rHat_ind, minParam);
  maxParam = std::max<double>(rHat_ind, maxParam);
  

  if (verbose and myid==0) {
    std::cout << std::endl
	      << "Min, Max = " << minParam << ", " << maxParam << std::endl;
  }

  return maxParam;
}


GRanalyze::BSret GRanalyze::bootstrap(int nconverge, unsigned sampN, double probD)
{
  BSret ret = {0, 0, 0};

  if (sampN<=1) return ret;

  // For debugging
  //
  std::string header(4+10+6*18, '-');

  if (dbg_verbose and myid==0)
    std::cout << header << std::endl << std::left
	      << std::setw( 4) << "#"	    // 1
	      << std::setw(10) << "Nbox"    // 2
	      << std::setw(18) << "Mean"    // 3
	      << std::setw(18) << "P_min"   // 4
	      << std::setw(18) << "P_max"   // 5
	      << std::setw(18) << "Volume"  // 6
	      << std::setw(18) << "ML"	    // 7
	      << std::setw(18) << "log(ML)" // 8
	      << std::endl
	      << std::setw( 4) << "---"
	      << std::setw(10) << "------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::setw(18) << "-------"
	      << std::endl;

  // Flatten multiple chain data
  //
  std::vector<double> prob1;
  std::vector< std::vector<double> > data1;
  unsigned ndata0 = data.size();
  unsigned nchain = mask.size();
  std::vector<unsigned> partn;

  for (unsigned k=nconverge; k<ndata0; k++) {
    for (unsigned j=0; j<nchain; j++) {
      if (mask[j]) continue;
      prob1.push_back(prob[k][j][0]);
      data1.push_back(data[k][j]);
      partn.push_back(partn.size());
    }
  }

  unsigned dsize  = data1.size();
  unsigned rsize  = data1[0].size();

  // Generate random index selection: resampling with replacement
  //
  std::random_device rd;	// Use random pool for seed
  std::mt19937 gen(rd());
  std::uniform_int_distribution<unsigned> dis(0, dsize-1);
 
  std::vector<double> trials(sampN);

  // Get maximum a posterior value
  //
  ret[2] = -DBL_MAX;
  for (auto v : prob1) ret[2] = std::max<double>(ret[2], v);

  // Begin bootstrap statistic loop
  //
  typedef std::pair<double, int> Dvalue;
  typedef std::vector<Dvalue>    Dtype;

  for (unsigned n=0; n<sampN; n++) {

    // Map ranked by posterior probability
    //
    Dtype llist;
    for (unsigned k=0; k<dsize; k++) {
      unsigned j = dis(gen);
      llist.push_back(Dvalue(prob1[j], j));
    }

    // Compute the bounding box
    //
    std::vector<double> pminT(rsize, DBL_MAX), pmaxT(rsize, -DBL_MAX);
    Dvalue maxP(-DBL_MAX, 0);
    for (auto v : llist) {
      size_t j = v.second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data1[j][k];
	pminT[k] = std::min<double>(pminT[k], val);
	pmaxT[k] = std::max<double>(pmaxT[k], val);
      }
      if (maxP.first < v.first) maxP = v;
    }
    
    // Compute a scaling length
    //
    std::vector<double> scale(rsize);
    double initvol = 1.0;
    for (unsigned k=0; k<rsize; k++) {
      scale[k] = pmaxT[k] - pminT[k];
      initvol *= scale[k];
    }

    // Number of states in this resample
    //
    size_t nstates = llist.size();

    if (nstates != dsize) {
      if (myid==0) std::cout << "Resampled data size mismatch" << std::endl;
    }

    //--------------------------------------------------
    // Compute the L1 distance
    //--------------------------------------------------
    
    // Use the MAP as the center
    //
    std::vector<double> center(rsize);
    for (unsigned k=0; k<rsize; k++) center[k] = data1[maxP.second][k];
    
    // Create map ranked by L1 distance from MAP value
    //
    Dtype expfct;
    for (auto v : llist) {
      int     j = v.second;
      double L1 = 0.0;
      for (unsigned k=0; k<rsize; k++) {
	L1 = std::max<double>(L1, fabs(data1[j][k] - center[k])/scale[k]);
      }
      expfct.push_back(Dvalue(L1, j));
    }
  
    Dtype::iterator it = expfct.begin(), jt = expfct.end(), kt;
    Dtype::iterator mt = it;
    size_t maxStates = std::min<size_t>(nstates/10, 10000);

    for (size_t n=0; n<maxStates and mt!=jt; n++) mt++;
    
    // Use partial sort to reduce the cost of a full sort or a multimap
    //
    std::partial_sort(it, mt, jt);

    for (kt=it; kt!=mt; kt++) {
      int k = kt->second;
      if (maxP.first - prob1[k] > probD) break;
    }

    size_t nmode = std::distance(it, kt);

    // Find enclosing box and mean posterior value
    //
    std::vector<double> pmin(rsize, DBL_MAX), pmax(rsize, -DBL_MAX);
    double mean = 0.0;
    for (Dtype::iterator qt=it; qt!=kt; qt++) {
      size_t j = qt->second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data1[j][k];
	pmin[k] = std::min<double>(pmin[k], val);
	pmax[k] = std::max<double>(pmax[k], val);
      }
      mean += exp(prob1[j] - ret[2]);
    }
    
    // Compute the MC integral
    //
    mean /= nmode;
    double volume = 1.0;
    for (unsigned k=0; k<rsize; k++) volume *= pmax[k] - pmin[k];
    
    trials[n] = mean * volume * nstates/nmode;

    if (dbg_verbose and myid==0)
      std::cout << std::setw( 4) << n
		<< std::setw(10) << nmode
		<< std::setw(18) << mean
		<< std::setw(18) << prob1[kt->second] - ret[2]
		<< std::setw(18) << prob1[jt->second] - ret[2]
		<< std::setw(18) << volume
		<< std::setw(18) << trials[n]
		<< std::setw(18) << log(trials[n])
		<< std::endl;
  }

  if (dbg_verbose and myid==0)
    std::cout << header << std::endl
	      << "Peak prob = " << ret[2] << std::endl
	      << header << std::endl;

  // Final results
  //
  for (auto v : trials) ret[0] += v/sampN;
  for (auto v : trials) ret[1] += (v - ret[0])*(v - ret[0])/(sampN-1);

  return ret;
}


GRanalyze::GRanalyze(Ensemble* d, int Nchain, int Level)
{
				// Set default verbose level
  verbose     = false;		// to off
  dbg_verbose = false;
				// Set interval reporting on 
  report      = true;		// by default

				// The ensemble instance
  _ens        = d;

				// Number of chains
  nchain      = Nchain;

				// Use this level
  level       = Level;
				// Number of states in ensemble before testing
  maxit       = 500;
				// Number of steps between tests
  nskip       = 100;
				// Confidence interval for two-sided 
  alpha       = 0.05;		// Grubbs test

				// Maximum offset for accepting
  poffset     = -30.0;		// outlier in addition to Grubbs test


  maxout      = 6;		// Maximum number of outliers

				// For RJTwo models
				// ----------------
  T           = -1;		// # of common parameters
  N1          =  1;		// # of Model 1 parameters
  N2          =  1;		// # of Model 2 parameters
  
  MaxR        = 1.2;		// GR statistic threshold

				// Use Grubbs' algorithm for detecting
  maskType    = grubbs;		// outliers by default
}

void GRanalyze::Compute()
{
  std::shared_ptr<OutlierMask> outlier;
  if (maskType == grubbs)
    outlier = std::shared_ptr<OutlierMask>(new Grubbs);
  else 
    outlier = std::shared_ptr<OutlierMask>(new Rosner);


  deque<StateData>& states = _ens->getStates();

  unsigned dsize = states.size();

  if (dsize < 2 and myid==0) {
    std::cout << "No states at Level " << level << std::endl;
    return;
  }

  if (dsize % nchain != 0 and myid==0) {
    std::cout << "State size mistmatch: " << dsize << " is not a multiple of chain size"
	      << nchain << "." << std::endl << "Have you provided the correct chain size?"
	      << std::endl;
    return;
  }

  // TEST
  if (myid==0)
    std::cout << std::string(40, '-') << std::endl
	      << "Mixture and extended state info" << std::endl
	      << std::setw(10) << std::left << "M"    << " : "
	      << states[0].p.SI()->M << std::endl
	      << std::setw(10) << std::left << "Ndim" << " : "
	      << states[0].p.SI()->Ndim << std::endl
	      << std::setw(10) << std::left << "Next" << " : "
	      << states[0].p.SI()->Next << std::endl
	      << std::setw(10) << std::left << "Ntot" << " : "
	      << states[0].p.SI()->Ntot << std::endl
	      << std::setw(10) << std::left << "iorder" << " : "
	      << states[0].p.SI()->iorder << std::endl
	      << std::string(40, '-') << std::endl;
  // END TEST

  unsigned cnt = 0;
  unsigned nsize = dsize/nchain;
  for (unsigned n=0; n<nsize; n++) {
    chainType p(nchain), d(nchain);
    for (unsigned k=0; k<nchain; k++) {
      p[k] = states[cnt].prob;
      d[k] = states[cnt].p;
      cnt++;
    }
    prob.push_back(p);
    data.push_back(d);
  }

  nconverge=-1;

  unsigned nend=std::max<unsigned>(maxit, nskip), count=0, ssize=maxit;

  nend = std::min<unsigned>(nend, nsize);

  while (nend<nsize) {

    count++;

    if (maxit==0) ssize = nend/2;

    if (report and myid==0) {
      std::ostringstream ostr;
      if (maxit)
	ostr << "==== Interval #" << count << ": [" 
	     << std::max<int>(0, (int)nend-(int)maxit) << ", " << nend << "] ";
      else
	ostr << "==== Interval #" << count << ": [" 
	     << std::max<unsigned>(0, (int)nend-(int)ssize) << ", " << nend << "] ";
      std::cout << std::setw(72) << std::setfill('=') << "=" << std::endl
		<< std::setw(72) << std::left << ostr.str()  << std::endl
		<< std::setw(72) << std::setfill('=') << "=" << std::endl 
		<< std::setfill(' ');
    }

    
    bool ret;
    if (T>=0)
      ret = outlier->compute(prob[nend], data[nend], 
			     alpha, maxout, T, N1, N2, poffset, report);
    else
      ret = outlier->compute(prob[nend], data[nend], 
			     alpha, maxout, poffset, report);

    mask = (*outlier)();

    if (report and myid==0) {
      std::cout << "Chains masked (+), unmasked(o)" << std::endl;
      printMask();
      std::cout << std::endl;
    }

    if (ret) {

      std::vector<double> frac = MixingFraction(nend, ssize);
      double wmix=2.0;
      unsigned wnum=-1;
      for (unsigned n=0; n<frac.size(); n++) {
	if (mask[n]==1) continue;
	if (frac[n]<wmix) {
	  wmix = frac[n];
	  wnum = n;
	}
      }
      
      if (report and myid==0)
	std::cout << "Worst non-masked mixing fraction in Chain #" << wnum
		  << " frac=" << wmix << std::endl;
      
      double val;
      if (T>=0) 
	val = GR(nend, ssize, T, N1, N2);
      else
	val = GR(nend, ssize);
      
      if (val < MaxR) {
	nconverge = nend;
      }
    }
    
    nend += nskip;
  }

  if (myid==0) {
    std::cout << std::endl;

    if (nconverge>=0) {
      std::cout << std::string(40, '-') << std::endl
		<< std::setw(20) << std::left << "Converged step"
		<< nconverge << std::endl;
      unsigned goodCnt = 0; for (auto c : mask) if (c==0) goodCnt++;
      std::cout << std::setw(20) << std::left << "Total size"
		<< states.size() << std::endl
		<< std::setw(20) << std::left << "Total steps="
		<< data.size()   << std::endl
		<< std::setw(20) << std::left << "Good chains="
		<< goodCnt << "/" << nchain << std::endl
		<< std::setw(20) << std::left << "Good states="
		<< (data.size() - nconverge)*goodCnt << std::endl
		<< std::string(40, '-') << std::endl;
      
    } else
      std::cout << "No convergence" << std::endl;

    std::cout << std::endl;
  }
}


double GRanalyze::MarginalLikelihood(double probD, int sampN)
{
  double ret = 0.0;

  if (myid) return ret;

  if (probD>0.0 and nconverge<0) {
    std::cout << std::endl
	      << "Skipping marginal likelihood computation due because of no convergence" << std::endl;
  }
  
  if (probD>0.0 and nconverge>=0) {

    std::cout << std::endl
	      << std::string(30+2*18, '-') << std::endl
	      << "START MARGINAL LIKELIHOOD COMPUTATION" << std::endl
	      << std::string(30+2*18, '-') << std::endl;
    
    unsigned nchain = mask.size();
    unsigned nmask  = 0;
    std::vector<unsigned char>::iterator ip;
    for (ip=mask.begin(); ip!=mask.end(); ip++) {
      if (*ip == 1) nmask++;
    }
    
    unsigned dsize = data.size();
    unsigned rsize = data[0][0].size();

    // Map ranked by posterior probability
    //
    Pmap llist;
    for (unsigned i=nconverge; i<dsize; i++) {
      for (unsigned j=0; j<nchain; j++) {
	if (mask[j]) continue;
	llist.push_back(Pmap_value(prob[i][j][0], Ielem(i, j)));
      }
    }

    // Compute the bounding box
    //
    std::vector<double> pminT(rsize, DBL_MAX), pmaxT(rsize, -DBL_MAX);
    Pmap_value maxP(-DBL_MAX, Ielem(0, 0));
    for (auto v : llist) {
      size_t k1 = v.second.first;
      size_t k2 = v.second.second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data[k1][k2][k];
	pminT[k] = std::min<double>(pminT[k], val);
	pmaxT[k] = std::max<double>(pmaxT[k], val);
      }
      if (maxP.first < v.first) maxP = v;
    }

    // Compute a scaling length
    //
    std::vector<double> scale(rsize);
    double initvol = 1.0;
    for (unsigned k=0; k<rsize; k++) {
      scale[k] = pmaxT[k] - pminT[k];
      initvol *= scale[k];
    }

    //--------------------------------------------------
    // Compute the L1 distance
    //--------------------------------------------------
    
    // Use the MAP as the center
    //
    std::vector<double> center(rsize);
    {
      size_t c1 = maxP.second.first;
      size_t c2 = maxP.second.second;
      for (unsigned k=0; k<rsize; k++) center[k] = data[c1][c2][k];
    }
    
    // Create map ranked by L1 distance from MAP value
    //
    Pmap expfct;
    for (auto v : llist) {
      size_t k1 = v.second.first;
      size_t k2 = v.second.second;
      double L1 = 0.0;
      for (unsigned k=0; k<rsize; k++) {
	L1 = std::max<double>(L1, fabs(data[k1][k2][k] - center[k])/scale[k]);
      }
      expfct.push_back(Pmap_value(L1, v.second));
    }
  
    size_t nstates = llist.size();
    size_t maxStates = std::min<size_t>(nstates/10, 10000);

    Pmap::iterator it = expfct.begin(), jt = expfct.end(), kt;
    Pmap::iterator mt = it;

    for (size_t n=0; n<maxStates and mt!=jt; n++) mt++;
    
    // Use partial sort to reduce the cost of a full sort or a multimap
    //
    std::partial_sort(it, mt, jt);

    for (kt=it; kt!=mt; kt++) {
      int k1 = kt->second.first;
      int k2 = kt->second.second;
      if (maxP.first - prob[k1][k2][0] > probD) break;
    }

    size_t nsubbox = std::distance(it, kt);

				// Initial point in list
    size_t c1 = it->second.first;
    size_t c2 = it->second.second;
    
				// Final point in sub box list
    size_t d1 = kt->second.first;
    size_t d2 = kt->second.second;

    double Prmax = prob[c1][c2][0];
    double Prmin = prob[d1][d2][0];
 
    std::cout << "Computing marginal likelhood with " << nmask 
	      << " chains removed" << std::endl
	      << "Using " << nstates << " states in total" << std::endl
	      << "Using " << nsubbox << " states in the sub box" << std::endl
	      << "[Min : Max] prob in sub box: ["
	      << Prmin << " : " << Prmax << "]" << std::endl;

    // Find enclosing box and mean posterior value
    //
    std::vector<double> pmin(rsize, DBL_MAX), pmax(rsize, -DBL_MAX);
    double mean = 0.0;
    for (iterType qt=it; qt!=kt; qt++) {
      size_t k1 = qt->second.first;
      size_t k2 = qt->second.second;
      for (unsigned k=0; k<rsize; k++) {
	double val = data[k1][k2][k];
	pmin[k] = std::min<double>(pmin[k], val);
	pmax[k] = std::max<double>(pmax[k], val);
      }
      mean += exp(prob[k1][k2][0] - Prmax);
    }
    
    std::cout << std::string(24+6*16, '-') << std::endl
	      << "Box stats" << std::endl
	      << std::string(24+6*16, '-') << std::endl
	      << std::left
	      << std::setw(24) << "Label"
	      << std::setw(16) << "Peak"
	      << std::setw(16) << "Sub min"
	      << std::setw(16) << "Sub max"
	      << std::setw(16) << "Tot min"
	      << std::setw(16) << "Tot max"
	      << std::setw(16) << "Ratio"
	      << std::endl
	      << std::setw(24) << "------------";

    for (size_t k=0; k<6; k++) std::cout << std::setw(16) << "-------";
    std::cout << std::endl;

    for (unsigned k=0; k<rsize; k++) {
      std::cout << std::left
		<< std::setw(24) << _ens->getStateInfo()->ParameterDescription(k)
		<< std::setw(16) << center[k]
		<< std::setw(16) << pmin[k]
		<< std::setw(16) << pmax[k]
		<< std::setw(16) << pminT[k]
		<< std::setw(16) << pmaxT[k]
		<< std::setw(16) << (pmax[k] - pmin[k])/(pmaxT[k] - pminT[k])
		<< std::endl;
    }
    std::cout << std::endl;

    // Compute the MC integral
    //
    mean /= nsubbox;
    double volume = 1.0;
    for (unsigned k=0; k<rsize; k++) volume *= pmax[k] - pmin[k];
    
    BSret bs = bootstrap(nconverge, sampN, probD);

    std::cout << std::string(30+2*18, '-') << std::endl
	      << "MARGINAL LIKELIHOOD COMPUTATION SUMMARY" << std::endl
	      << std::string(30+2*18, '-') << std::endl
	      << std::setw(30) << "Quantity"
	      << std::setw(18) << "Value"
	      << std::setw(18) << "log(Value)"
	      << std::endl
	      << std::string(30+2*18, '-') << std::endl
	      << std::setw(30) << "Mean prob in sub box"
	      << std::setw(18) << mean
	      << std::setw(18) << log(mean) + Prmax
	      << std::endl
	      << std::setw(30) << "Volume of initial box"
	      << std::setw(18) << initvol
	      << std::setw(18) << log(initvol)
	      << std::endl
	      << std::setw(30) << "Volume of sub box"
	      << std::setw(18) << volume
	      << std::setw(18) << log(volume)
	      << std::endl
	      << std::setw(30) << "Count ratio"
	      << std::setw(18) << static_cast<double>(nstates)/nsubbox
	      << std::setw(18) << log(nstates) - log(nsubbox)
	      << std::endl
	      << std::setw(30) << "Sub/initial volume ratio"
	      << std::setw(18) << volume/initvol
	      << std::setw(18) << log(volume/initvol)
	      << std::endl;

    if (sampN>1) {
      std::cout << std::setw(30) << "Mean"
		<< std::setw(18) << bs[0]
		<< std::setw(18) << log(bs[0])
		<< std::endl
		<< std::setw(30) << "Variance"
		<< std::setw(18) << sqrt(bs[1])
		<< std::setw(18) << log(sqrt(bs[1]))
		<< std::endl
		<< std::setw(30) << "Ratio"
		<< std::setw(18) << sqrt(bs[1])/bs[0]
		<< std::setw(18) << log(sqrt(bs[1])/bs[0])
		<< std::endl
		<< std::setw(30) << "Bootstrap error [neg/pos]"
		<< std::setw(18) << log(bs[0] - sqrt(bs[1])) + bs[2]
		<< std::setw(18) << log(bs[0] + sqrt(bs[1])) + bs[2]
		<< std::endl;
    }
    
    // Upscale by count ratio
    //
    mean *= volume * nstates/nsubbox;
    
    std::cout << std::string(30+2*18, '-') << std::endl
	      << std::setw(30) << "**Marginal likelihood**"
	      << std::setw(18) << mean * exp(Prmax)
	      << std::setw(18) << log(mean) + Prmax
	      << std::endl << std::string(30+2*18, '-') << std::endl;

    ret = log(mean) + Prmax;

  }

  return ret;
}


std::vector<double> GRanalyze::MixingFraction
(unsigned endpt, unsigned psize)
{
  if (verbose) {
				// Separator
    cout << setfill('-') << setw(72) << "-" << std::endl << setfill(' ');
  }
  


  //***************************************************
  //  Begin computation
  //***************************************************
  
				// Number of chains
  unsigned Mchain = data[0].size();
				// Number of values per chain 
				// (ignore mixture count)
  unsigned Nparam = data[0][0].size();


  unsigned begpt;
  if (endpt<psize) begpt = 0;
  else begpt = endpt-psize;
				// 
				// Count aberrant chains
				// 
  unsigned Mgood = 0;
  for (unsigned n=0; n<mask.size(); n++) 
    if (mask[n]==0) Mgood++;


				// Analyze chains
  vector<double> frac(Mchain, 1.0);
  for (unsigned n=0; n<Mchain; n++) {
    for (unsigned m=0; m<Nparam; m++) {
      set<double> s;
      for (unsigned k=begpt; k<endpt; k++) s.insert(data[k][n][m]);
      frac[n] = min<double>(frac[n], (double)s.size()/(endpt-begpt));
    }
  }
  
  if (verbose) {
    cout << "Fractions: ";
    unsigned origp = cout.precision(2);
    cout.setf(ios::fixed);
    for (unsigned n=0; n<Mchain; n++) cout << setw(5) << frac[n];
    cout << std::endl;
    cout.precision(origp);
    cout.unsetf(ios::fixed);
				// Separator
    cout << setfill('-') << setw(72) << "-" << std::endl << setfill(' ');
  }

  return frac;
}

Ensemble* GRanalyze::cleanConverged()
{
  if (nconverge>=0)
    std::cout << "Converged at n=" << nconverge << std::endl;
  else {
    std::cout << "No convergence" << std::endl;
    return NULL;
  }
    
  Ensemble* _ret = _ens->New();

  deque<StateData>& states = _ens->getStates();
  unsigned dsize = states.size();
  unsigned nsize = dsize/nchain;
  
  for (unsigned n=nconverge; n<nsize; n++)  {
    for (unsigned k=0; k<nchain; k++) {
      if (mask[k]) continue;
      _ret->AccumData(states[n*nchain+k]);
    }
  }

  _ret->ComputeDistribution(0);

  return _ret;
}

void GRanalyze::printMask()
{
  size_t nsize = mask.size();
  size_t ncols = 10;
  size_t nrows = nsize/ncols + 1;

  for (size_t n=0; n<nrows; n++) {
    for (size_t k=0; k<std::min<size_t>(nsize-n*ncols, ncols); k++) {
      if (mask[k+n*ncols]) std::cout << " +";
      else                 std::cout << " o";
    }
    std::cout << std::endl;
  }

}
