#include <iostream>
#include <iomanip>
#include <sstream>

#include <BIEMutex.h>
#include <Chain.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(Chain)


Chain::Chain(void)
{
  si          = 0;
  owner       = 0;
  cache       = false;
  cache_ctr   = 0;

  born        = vector<int>(4, 0);
  died        = vector<int>(4, 0);
  split       = vector<int>(4, 0);
  unite       = vector<int>(4, 0);
  stat        = vector<int>(2, 0);

  like_value  = prior_value  = value  = 0.0;
  like_value1 = prior_value1 = value1 = 0.0;
  prbs        = vector<double>(3);

  indx        = 0;
  beta        = beta0;
  factor      = 1.0;
  weight      = 1.0;
}


Chain::Chain(StateInfo *SI)
{
  owner       = 0;
  cache       = false;
  cache_ctr   = 0;
  si          = SI;

  fid         = boost::shared_ptr<State>(new State(si));
  tmp         = boost::shared_ptr<State>(new State(si));
  tst         = boost::shared_ptr<State>(new State(si));

  born        = vector<int>(4, 0);
  died        = vector<int>(4, 0);
  split       = vector<int>(4, 0);
  unite       = vector<int>(4, 0);
  stat        = vector<int>(2, 0);

  like_value  = prior_value  = value  = 0.0;
  like_value1 = prior_value1 = value1 = 0.0;
  prbs        = vector<double>(3);

  indx        = 0;
  beta        = beta0;
  factor      = 1.0;
  weight      = 1.0;
}


Chain::Chain(State& s)
{
  owner       = 0;
  cache       = false;
  cache_ctr   = 0;
  si          = s.SI();

  fid         = boost::shared_ptr<State>(new State(si));
  tmp         = boost::shared_ptr<State>(new State(si));
  tst         = boost::shared_ptr<State>(new State(si));

  born        = vector<int>(4, 0);
  died        = vector<int>(4, 0);
  split       = vector<int>(4, 0);
  unite       = vector<int>(4, 0);
  stat        = vector<int>(2, 0);

  like_value  = prior_value  = value  = 0.0;
  like_value1 = prior_value1 = value1 = 0.0;
  prbs        = vector<double>(3);

  indx        = 0;
  beta        = beta0;
  factor      = 1.0;
  weight      = 1.0;

  AssignState0(s);
}


Chain::Chain(const Chain& ch)
{
  owner      = ch.owner;
  cache      = ch.cache;
  cache_name = ch.cache_name;
  cache_ctr  = ch.cache_ctr;

  Ntot0      = ch.Ntot0;
  Ntot1      = ch.Ntot1;

  born       = ch.born;
  died       = ch.died;
  split      = ch.split;
  unite      = ch.unite;
  stat       = ch.stat;
				// The is the state metadata, copy the
				// pointer
  si         = ch.si;

				// Initialize states, if necessary
  if (fid.get()==0) fid = boost::shared_ptr<State>(new State(si));
  if (tmp.get()==0) tmp = boost::shared_ptr<State>(new State(si));
  if (tst.get()==0) tst = boost::shared_ptr<State>(new State(si));

				// Copy the states, not the pointers
  *fid  = *ch.fid;
  *tst  = *ch.tst;
  *tmp  = *ch.tmp;

  like_value   = ch.like_value;
  prior_value  = ch.prior_value;
  value        = ch.value;

  like_value1  = ch.like_value1;
  prior_value1 = ch.prior_value1;
  value1       = ch.value1;

  prbs         = ch.prbs;

  indx         = ch.indx;
  beta         = ch.beta;
  factor       = ch.factor;
  weight       = ch.weight;
}

Chain& Chain::operator=(const Chain& ch)
{
  owner      = ch.owner;
  cache      = ch.cache;

  cache_name = ch.cache_name;
  cache_ctr  = ch.cache_ctr;

  Ntot0      = ch.Ntot0;
  Ntot1      = ch.Ntot1;

  born       = ch.born;
  died       = ch.died;
  split      = ch.split;
  unite      = ch.unite;
  stat       = ch.stat;
    
				// The is the state metadata, copy the
				// pointer
  si    = ch.si;

				// Initialize states, if necessary
  if (fid.get()==0) fid = boost::shared_ptr<State>(new State(si));
  if (tmp.get()==0) tmp = boost::shared_ptr<State>(new State(si));
  if (tst.get()==0) tst = boost::shared_ptr<State>(new State(si));

				// Copy the states, not the pointers
  *fid  = *ch.fid;
  *tst  = *ch.tst;
  *tmp  = *ch.tmp;

  like_value   = ch.like_value;
  prior_value  = ch.prior_value;
  value        = ch.value;

  like_value1  = ch.like_value1;
  prior_value1 = ch.prior_value1;
  value1       = ch.value1;

  prbs         = ch.prbs;

  indx         = ch.indx;
  beta         = ch.beta;
  factor       = ch.factor;
  weight       = ch.weight;

  return *this;
}

void Chain::AssignState0(const Chain& ch)
{
  born        = ch.born;
  died        = ch.died;
  split       = ch.split;
  unite       = ch.unite;
  stat        = ch.stat;
				// Copy the fiducial, current state
  *fid = *ch.fid;

  like_value  = ch.like_value;
  prior_value = ch.prior_value;
  value       = ch.value;

  WriteLog();
}

void Chain::Logging(const string& str)
{
  ifstream infile;
  ofstream outfile;

  cache = true;
  cache_name = str;
  cache_ctr = 0;
				// Check for file
  infile.open (cache_name.c_str(), ifstream::in);

  bool header = true;
  unsigned Mcur;

  if (infile.good()) {

    int pptype, iindx;
    unsigned nntot, mm, nndim, nnext;
    double bbeta, ffactor, wweight;
    bool ok = true;

    infile >> iindx;
    infile >> bbeta;
    infile >> ffactor;
    infile >> wweight;
    infile >> pptype;

    // Compare initial info
    //
    if (iindx  != indx                ||
	pptype != si->ptype           ||
	fabs(bbeta-beta)     > 1.0e-8 ||
	fabs(ffactor-factor) > 1.0e-8 ||
	fabs(wweight-weight) > 1.0e-8) ok = false;
      

    // Compare state specific info
    //
    if (ok) {

      switch(si->ptype) {
      case StateInfo::None:
      case StateInfo::Block:
      case StateInfo::RJTwo:
	infile >> nntot;
	if (nntot != si->Ntot) ok = false;
	break;

      case StateInfo::Mixture:
      case StateInfo::Extended:
	infile >> mm;
	infile >> nndim;
	infile >> nnext;

	if (si->M != mm || si->Ndim != nndim || si->Next != nnext) ok = false;

	break;
      }

    }

    if (ok) {

      const int linesize = 65536;
      char line[linesize];

      infile.getline(line, linesize);

      while(infile.good() && strlen(line)>0) {
	istringstream in(line);
	in >> value;
	in >> like_value;
	in >> prior_value;

	switch (si->ptype) {

	case StateInfo::None:
	case StateInfo::Block:
	case StateInfo::RJTwo:
	  for(unsigned i=0; i<si->Ntot; i++) in >> (*fid)[i];
	  break;

	case StateInfo::Mixture:
	  
	  in >> Mcur;
	  if (Mcur != fid->M()) fid->Reset(Mcur);
	  for(unsigned i=0; i<Mcur; i++) in >> fid->Wght(i);
	  for(unsigned i=0; i<Mcur; i++)
	    for(unsigned j=0; j<fid->Ndim(); j++) in >> fid->Phi(i, j);

	case StateInfo::Extended:
	  for (unsigned i=0; i<fid->Next(); i++) in >> fid->Ext(i);

	}
	infile.getline(line, linesize);
      }
      
      header = false;
      
    } else {
      ostringstream command;
      command << "mv " << str << " " << str << ".bak";
      if (system(command.str().c_str())) {
	std::cerr << "Error moving <" << str << "> to <"
		  << str << ".bak>" << std::endl;
      }
    }

  }

  infile.close();

  if (header) {

    outfile.open (cache_name.c_str());

    if (outfile.good()) {

      outfile << indx      << endl;
      outfile << beta      << endl;
      outfile << factor    << endl;
      outfile << weight    << endl;
      outfile << si->ptype << endl;
      
      switch (si->ptype) {

      case StateInfo::None:
      case StateInfo::Block:
      case StateInfo::RJTwo:
	outfile << fid->N() << endl;
	break;

      case StateInfo::Mixture:
      case StateInfo::Extended:
	outfile << fid->M()    << endl;
	outfile << fid->Ndim() << endl;
	outfile << fid->Next() << endl;
	break;
      }
      outfile.close();
    }
  }

}

void Chain::WriteLog()
{
  if (cache) {
    ofstream out(cache_name.c_str(), ios::out | ios::app);

    out << setw(8)  << cache_ctr++
	<< setw(18) << value
	<< setw(18) << like_value
	<< setw(18) << prior_value;

    switch (si->ptype) {

    case StateInfo::None:
    case StateInfo::Block:
    case StateInfo::RJTwo:
      for (unsigned i=0; i<fid->N(); i++)
	out << setw(18) << (*fid)[i];
      break;
      
    case StateInfo::Mixture:

      out << setw(8)<< fid->M();

      for (unsigned i=0; i<fid->M(); i++) 
	out << setw(18) << fid->Wght(i);

      for (unsigned i=0; i<fid->M(); i++)
	for(unsigned j=0; j<fid->Ndim(); j++) 
	  out << setw(18) << fid->Phi(i, j);
    
    case StateInfo::Extended:
      for (unsigned i=0; i<fid->Next(); i++)
	out << setw(18) << fid->Ext(i);
    }
    out << endl;
  }
}


void Chain::AssignState0(const State& s)
{
  // Copy the state
  *fid = s;

  born [0] = died [0] = 0;
  split[0] = unite[0] = 0;
  for (int i=0; i<2; i++) stat[i] = 0;

  Order0();
}

void Chain::AssignState1(const Chain& ch)
{
  // Copy the state
  *tst = *ch.fid;

  // Assign the per-step counters (from current to trial)
  for (int i=0; i<2; i++) born [2+i] = ch.born [i];
  for (int i=0; i<2; i++) died [2+i] = ch.died [i];
  for (int i=0; i<2; i++) split[2+i] = ch.split[i];
  for (int i=0; i<2; i++) unite[2+i] = ch.unite[i];
  for (int i=0; i<2; i++) stat [i]   = ch.stat [i];

  Order1();
}

void Chain::AcceptState()
{
  // Copy the state
  *fid = *tst;

  // Copy the trial counters (2,3) to the current counters (0,1)
  for (int i=0; i<2; i++) born [i] = born [2+i];
  for (int i=0; i<2; i++) died [i] = died [2+i];
  for (int i=0; i<2; i++) split[i] = split[2+i];
  for (int i=0; i<2; i++) unite[i] = unite[2+i];

  like_value  = like_value1;
  prior_value = prior_value1;
  value       = value1;
}

void Chain::AssignState1(const State& s)
{
  // Copy the state
  *tst = s;

  // Assign the per-step counters (2,3) from the current counters (0,1)
  for (int i=0; i<2; i++) born [2+i] = born[i];
  for (int i=0; i<2; i++) died [2+i] = died[i];
  for (int i=0; i<2; i++) split[2+i] = split[i];
  for (int i=0; i<2; i++) unite[2+i] = unite[i];
}


void Chain::MakeMPI()
{
  if (!mpi_used)     return;
  if (Buffer.size()) return;
				                     // Address offsets
  int ix = 0;                                        // ---------------

  ofs.push_back(0);                                  // indx..........0
  ofs.push_back(ofs[ix++] + sizeof(int));            // beta..........1
  ofs.push_back(ofs[ix++] + sizeof(double));         // factor........2
  ofs.push_back(ofs[ix++] + sizeof(double));         // weight........3
  ofs.push_back(ofs[ix++] + sizeof(double));         // like_value....4
  ofs.push_back(ofs[ix++] + sizeof(double));         // prior_value...5
  ofs.push_back(ofs[ix++] + sizeof(double));         // value.........6
  ofs.push_back(ofs[ix++] + sizeof(double));         // like_value1...7
  ofs.push_back(ofs[ix++] + sizeof(double));         // prior_value1..8
  ofs.push_back(ofs[ix++] + sizeof(double));         // value1........9
  ofs.push_back(ofs[ix++] + sizeof(double));         // owner........10
  ofs.push_back(ofs[ix++] + sizeof(int));            // born.........11
  ofs.push_back(ofs[ix++] + 4*sizeof(int));          // died.........12
  ofs.push_back(ofs[ix++] + 4*sizeof(int));          // stat.........13
  ofs.push_back(ofs[ix++] + 4*sizeof(int));          // split........14
  ofs.push_back(ofs[ix++] + 4*sizeof(int));          // unite........15
  ofs.push_back(ofs[ix++] + 2*sizeof(int));          // Ntot0........16
  ofs.push_back(ofs[ix++] + sizeof(unsigned));       // Ntot1........17
  ofs.push_back(ofs[ix++] + sizeof(unsigned));       // M0...........18
  ofs.push_back(ofs[ix++] + sizeof(unsigned));       // M1...........19

  Buffer_size = ofs[ix]   + sizeof(unsigned);

  Buffer = vector<char>(Buffer_size);
}


void Chain::Chain_to_Buffer()
{
  MakeMPI();

  *reinterpret_cast<int *>   (&Buffer[ofs[0]])  = indx;
  *reinterpret_cast<double *>(&Buffer[ofs[1]])  = beta;
  *reinterpret_cast<double *>(&Buffer[ofs[2]])  = factor;
  *reinterpret_cast<double *>(&Buffer[ofs[3]])  = weight;

  *reinterpret_cast<double *>(&Buffer[ofs[4]])  = like_value;
  *reinterpret_cast<double *>(&Buffer[ofs[5]])  = prior_value;
  *reinterpret_cast<double *>(&Buffer[ofs[6]])  = value;

  *reinterpret_cast<double *>(&Buffer[ofs[7]])  = like_value1;
  *reinterpret_cast<double *>(&Buffer[ofs[8]])  = prior_value1;
  *reinterpret_cast<double *>(&Buffer[ofs[9]])  = value1;
  *reinterpret_cast<int *   >(&Buffer[ofs[10]]) = owner;

  for (int i=0; i<4; i++)
    reinterpret_cast<int *>(&Buffer[ofs[11]])[i] = born[i];

  for (int i=0; i<4; i++)
    reinterpret_cast<int *>(&Buffer[ofs[12]])[i] = died[i];

  for (int i=0; i<4; i++)
    reinterpret_cast<int *>(&Buffer[ofs[13]])[i] = split[i];

  for (int i=0; i<4; i++)
    reinterpret_cast<int *>(&Buffer[ofs[14]])[i] = unite[i];

  for (int i=0; i<2; i++)
    reinterpret_cast<int *>(&Buffer[ofs[15]])[i] = stat[i];

  // These four communicate possibly varying StateInfo.  The last two
  // only matter for mixture models.

  *reinterpret_cast<unsigned*>(&Buffer[ofs[16]]) = Ntot0 = fid->N();
  *reinterpret_cast<unsigned*>(&Buffer[ofs[17]]) = Ntot1 = tst->N();
  *reinterpret_cast<unsigned*>(&Buffer[ofs[18]]) = fid->M();
  *reinterpret_cast<unsigned*>(&Buffer[ofs[19]]) = tst->M();

}

void Chain::Buffer_to_Chain()
{
  MakeMPI();

  indx         = *reinterpret_cast<int *>   (&Buffer[ofs[0]]);
  beta         = *reinterpret_cast<double *>(&Buffer[ofs[1]]);
  factor       = *reinterpret_cast<double *>(&Buffer[ofs[2]]);
  weight       = *reinterpret_cast<double *>(&Buffer[ofs[3]]);

  like_value   = *reinterpret_cast<double *>(&Buffer[ofs[4]]);
  prior_value  = *reinterpret_cast<double *>(&Buffer[ofs[5]]);
  value        = *reinterpret_cast<double *>(&Buffer[ofs[6]]);

  like_value1  = *reinterpret_cast<double *>(&Buffer[ofs[7]]);
  prior_value1 = *reinterpret_cast<double *>(&Buffer[ofs[8]]);
  value1       = *reinterpret_cast<double *>(&Buffer[ofs[9]]);

  owner        = *reinterpret_cast<int *>   (&Buffer[ofs[10]]);
  
  for (int i=0; i<4; i++)
    born[i] = reinterpret_cast<int *>(&Buffer[ofs[11]])[i];

  for (int i=0; i<4; i++)
    died[i] = reinterpret_cast<int *>(&Buffer[ofs[12]])[i];

  for (int i=0; i<4; i++)
    split[i] = reinterpret_cast<int *>(&Buffer[ofs[13]])[i];

  for (int i=0; i<4; i++)
    unite[i] = reinterpret_cast<int *>(&Buffer[ofs[14]])[i];

  for (int i=0; i<2; i++)
    stat[i] = reinterpret_cast<int *>(&Buffer[ofs[15]])[i];

  // These are for resetting, possibly, the State instances in case of
  // any changes in dimensions (e.g. changing the mixture component
  // count or when using ReversibleJump)

  Ntot0        = *reinterpret_cast<unsigned*>(&Buffer[ofs[16]]);
  Ntot1        = *reinterpret_cast<unsigned*>(&Buffer[ofs[17]]);
  unsigned M0  = *reinterpret_cast<unsigned*>(&Buffer[ofs[18]]);
  unsigned M1  = *reinterpret_cast<unsigned*>(&Buffer[ofs[19]]);

  // States fid and tst should always have the same ptype, but I've
  // let it be general for now

  switch (fid->Type()) {
  case StateInfo::Mixture:
  case StateInfo::Extended:
    fid->Reset(M0);
    break;
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    break;
  }
    
  switch (tst->Type()) {
  case StateInfo::Mixture:
  case StateInfo::Extended:
    tst->Reset(M1);
    break;
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    break;
  }
}

void Chain::BroadcastChain()
{
  MakeMPI();

  if (owner == BIE::myid) Chain_to_Buffer();

  int ret = MPI_Bcast(&Buffer[0], Buffer_size, MPI_CHAR, owner, MPI_COMM_WORLD);

  switch (ret) {
  case MPI_SUCCESS:
    break;
  default:
    {
      ostringstream sout;
      sout << "Chain::BroadcastChain: "
	   << "MPI error in metadata broadcast, "
	   << "node id=" << BIE::myid;
      throw InternalError(sout.str(), __FILE__, __LINE__);
    }
  }

  if (owner != BIE::myid) {
    Buffer_to_Chain();
				// Sanity checks
    if (Ntot0 > si->Ntot) {
      ostringstream sout;
      sout << "Chain::BroadcastChain: "
	   << "requested size Ntot0=" << Ntot0 
	   << " is larger than allocated size of " << si->Ntot << BIE::myid;
      throw InternalError(sout.str(), __FILE__, __LINE__);
    }
    if (Ntot1 > si->Ntot) {
      ostringstream sout;
      sout << "Chain::BroadcastChain: "
	   << "requested size Ntot1=" << Ntot1 
	   << " is larger than allocated size of " << si->Ntot << BIE::myid;
      throw InternalError(sout.str(), __FILE__, __LINE__);
    }
  }

  
  ret = MPI_Bcast(&(*fid)[0], Ntot0, MPI_DOUBLE, owner, MPI_COMM_WORLD);

  switch (ret)
    {
    case MPI_SUCCESS:
      break;

    default:
      {
	ostringstream sout;
	sout << "Chain::BroadcastChain: "
	     << "MPI error in fiducial state broadcast, "
	     << "node id=" << BIE::myid;
	throw InternalError(sout.str(), __FILE__, __LINE__);
      }
    }
    

  ret = MPI_Bcast(&(*tst)[0], Ntot1, MPI_DOUBLE, owner, MPI_COMM_WORLD);
  
  switch (ret)
    {
    case MPI_SUCCESS:
      break;

    default:
      {
	ostringstream sout;
	sout << "Chain::BroadcastChain: "
	     << "MPI error in trial state broadcast, "
	     << "node id=" << BIE::myid;
	throw InternalError(sout.str(), __FILE__, __LINE__);
      }
    }
    


}

void Chain::SendChain(int to)
{
  Chain_to_Buffer();
  MPI_Send(&Buffer[0], Buffer_size, MPI_CHAR, to, 111, MPI_COMM_WORLD);
  MPI_Send(&(*fid)[0], Ntot0, MPI_DOUBLE, to, 112, MPI_COMM_WORLD);
  MPI_Send(&(*tst)[0], Ntot1, MPI_DOUBLE, to, 113, MPI_COMM_WORLD);
}

void Chain::RecvChain(int from)
{
  MakeMPI();

  MPI_Status status;

  MPI_Recv(&Buffer[0], Buffer_size, MPI_CHAR, from, 111, MPI_COMM_WORLD, &status);
  Buffer_to_Chain();
				// Sanity checks
  if (Ntot0 > si->Ntot) {
    ostringstream sout;
    sout << "Chain::RecvChain: "
	 << "requested size Ntot0=" << Ntot0 
	 << " is larger than allocated size of " << si->Ntot << BIE::myid;
    throw InternalError(sout.str(), __FILE__, __LINE__);
  }
  if (Ntot1 > si->Ntot) {
    ostringstream sout;
    sout << "Chain::RecvChain: "
	 << "requested size Ntot1=" << Ntot1 
	 << " is larger than allocated size of " << si->Ntot << BIE::myid;
    throw InternalError(sout.str(), __FILE__, __LINE__);
  }

  MPI_Recv(&(*fid)[0], Ntot0, MPI_DOUBLE, from, 112, MPI_COMM_WORLD, &status);
  MPI_Recv(&(*tst)[0], Ntot1, MPI_DOUBLE, from, 113, MPI_COMM_WORLD, &status);

}

void Chain::PrintStats(ostream& out)
{
  out << setw(80) << setfill('-') << '-' << endl << setfill(' ');
  out << setw(12) << "Index="  << indx << endl;
  out << setw(12) << "Beta="   << beta << endl;
  out << setw(12) << "Factor=" << factor << endl;
  out << setw(12) << "Owner="  << owner << endl;

  out << setw(12) << "Born=";
  for (int i=0; i<4; i++) out << setw(5) << born[i];
  out << endl;

  out << setw(12) << "Died=";
  for (int i=0; i<4; i++) out << setw(5) << died[i];
  out << endl;

  out << setw(12) << "Split=";
  for (int i=0; i<4; i++) out << setw(5) << split[i];
  out << endl;

  out << setw(12) << "Unite=";
  for (int i=0; i<4; i++) out << setw(5) << unite[i];
  out << endl;

  out << setw(12) << "Stat=";
  for (int i=0; i<2; i++) out << setw(5) << stat[i];
  out << endl;

  out << setw(80) << setfill('-') << '-' << endl << setfill(' ');
}

void Chain::PrintState(ostream& out)
{
  out << setw(80) << setfill('-') << '-' << endl << setfill(' ');
  out << "Value=" << value << endl << *fid;
  out << setw(80) << setfill('-') << '-' << endl << setfill(' ');
}

void Chain::PrintState1(ostream& out)
{
  out << setw(80) << setfill('-') << '-' << endl << setfill(' ');
  out << "Value1=" << value1 << endl << *tst;
  out << setw(60) << setfill('-') << '-' << endl << setfill(' ');
}


State* Chain::get_fid()
{
  if (!fid) {
    if (si==0) {
      string msg("Cannot define fid chain, undefined state!");
      throw InternalError(msg, __FILE__, __LINE__);
      return 0;
    }
    fid = boost::shared_ptr<State>(new State(si));
  }
  return fid.get();
}

State* Chain::get_tst()
{
  if (!tst) {
    if (si==0) {
      string msg("Cannot define tst chain, undefined state!");
      throw InternalError(msg, __FILE__, __LINE__);
      return 0;
    }
    tst = boost::shared_ptr<State>(new State(si));
  }
  return tst.get();
}

State* Chain::get_tmp()
{
  if (!tmp) {
    if (si==0) {
      string msg("Cannot define tmp chain, undefined state!");
      throw InternalError(msg, __FILE__, __LINE__);
      return 0;
    }
    tmp = boost::shared_ptr<State>(new State(si));
  }
  return tmp.get();
}
