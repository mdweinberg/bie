#include <CountConverge.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::CountConverge)

using namespace BIE;

CountConverge::CountConverge(int m, Ensemble* d) : Converge(d)
{
  count = 0;
  nmin  = m;
  nmax  = m;

  cout << "Classname: " << typeid(*d).name() << endl;
}

CountConverge::CountConverge(int m, Ensemble* d, int n) : Converge(d)
{
  count = 0;
  nmin  = n;
  nmax  = m;
  
  cout << "Classname: " << typeid(*d).name() << endl;
}

int CountConverge::ConvergedIndex()
{
  nburn = count>=nmin ? nmin : 0;
  MPI_Bcast(&nburn, 1, MPI_INT, 0, MPI_COMM_WORLD);
  return nburn;
}

