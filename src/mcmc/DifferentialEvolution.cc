
#include <DifferentialEvolution.h>
#include <gfunction.h>
#include <Prior.h>
#include <BIEconfig.h>
#include <BIEMutex.h>
#include <UniformDist.h>
#include <NormalDist.h>
#include <CauchyDist.h>
#include <FileExists.H>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::DifferentialEvolution)

using namespace BIE;
				// Global parameters
				// -----------------

DifferentialEvolution::
Control  DifferentialEvolution::cntrl          = parallel;

				// Smallest number of chains to run
				// --------------------------------
int      DifferentialEvolution::minmc	       = 6;

				// Number of iterations to find a good state
				// -----------------------------------------
int      DifferentialEvolution::state_iter     = 1000;

				// Small offset parameter for Logit scaling
				// -----------------------------------------
double   DifferentialEvolution::tiny           = 1.0e-12;


				// Frequency for unit gamma value
				// ------------------------------
unsigned DifferentialEvolution::jfreq          = 10;

				// Use linear or logit mapping
				// ---------------------------
bool     DifferentialEvolution::linear         = true;

				// Number of steps in rate queue
				// -----------------------------
unsigned DifferentialEvolution::nfifo          = 100;



DifferentialEvolution::~DifferentialEvolution()
{
  delete unit;
  if (local_dist) {
    for (unsigned j=0; j<distrib.size(); j++) delete distrib[j];
  }
}


void DifferentialEvolution::initialize(int mchains)
{
  algoname = "Differential Evolution";

  local_dist = false;

  Mchains = mchains;

				// Optimal width based on Gaussian model
  gamma0 = 2.38/sqrt(2.0*_si->Ntot);
  ncount = 0;

  totalnum = 0;			// Number of accepted states
  totaltry = 0;			// Number of attempted states

				// Uniform variates
  unit = new Uniform(0.0, 1.0, BIEgen);

  chains_initialized = false;

  caching = false;

  _lower = _prior->lower();
  _upper = _prior->upper();

  if (myid==0) {
    cout << "--------------" << endl
	 << " Prior limits " << endl
	 << "--------------" << endl;
    for (size_t i=0; i<_lower.size(); i++) {
      cout << right << setw(3) << i 
	   << setw(16) << _lower[i]
	   << setw(16) << _upper[i]
	   << endl;
    }
    cout << "--------------" << endl;  
  }

  CreateChains();
}

void DifferentialEvolution::initializeSampler(string deinit)
{
  ifstream in(deinit.c_str());
  if (!in) {
    ostringstream out;
    out << "I can't open the inputfile <" << deinit << ">";
    
    throw InternalError(out.str(), __FILE__, __LINE__);
  }

  double plower, pupper;
  unsigned index;
  double scale;

  Distribution* p;

  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block ||
      _si->ptype == StateInfo::RJTwo) {

    for (unsigned j=0; j<_si->Ntot; j++) {

      in >> index;
      in >> scale;
      in >> plower;
      in >> pupper;
      
      switch (index) {
      case uniform:
	p = new UniformDist(-0.5*scale, 0.5*scale);
	break;
      case normal:
	p = new NormalDist(0.0, scale*scale, plower, pupper);
	break;
      case cauchy:
	p = new CauchyDist(0.0, scale, plower, pupper);
	break;
      default:
	if (!in) {
	  ostringstream out;
	  out << "Illegal distribution index <" << index << ">";
	  throw InternalError(out.str(), __FILE__, __LINE__);
	}
      }
      
      distrib.push_back(p);
      
      if (!in) {
	ostringstream out;
	out << "Error reading the inputfile <" << deinit << ">";
	throw InternalError(out.str(), __FILE__, __LINE__);
      }
    }
    
  } else {

    in >> index;
    in >> scale;
  
    switch (index) {
    case uniform:
      p = new UniformDist(-0.5*scale, 0.5*scale);
      break;
    case normal:
      p = new NormalDist(0.0, scale*scale);
      break;
    case cauchy:
      p = new CauchyDist(0.0, scale);
      break;
    default:
      if (!in) {
	ostringstream out;
	out << "Illegal distribution index <" << index << ">";
	throw InternalError(out.str(), __FILE__, __LINE__);
      }
    }
    
    for (unsigned m=0; m<_si->M; m++) distrib.push_back(p);
      
    for (unsigned j=0; j<_si->Ndim + _si->Next; j++) {

      in >> index;
      in >> scale;
      in >> plower;
      in >> pupper;
      
      switch (index) {
      case uniform:
	p = new UniformDist(-0.5*scale, 0.5*scale);
	break;
      case normal:
	p = new NormalDist(0.0, scale*scale, plower, pupper);
	break;
      case cauchy:
	p = new CauchyDist(0.0, scale, plower, pupper);
	break;
      default:
	if (!in) {
	  ostringstream out;
	  out << "Illegal distribution index <" << index << ">";
	  throw InternalError(out.str(), __FILE__, __LINE__);
	}
      }
      
      distrib.push_back(p);
      
      if (!in) {
	ostringstream out;
	out << "Error reading the inputfile <" << deinit << ">";
	throw InternalError(out.str(), __FILE__, __LINE__);
      }
    }

  }

}


void DifferentialEvolution::initializeSampler(vector<Distribution*>& _eps)
{
  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block ||
      _si->ptype == StateInfo::RJTwo) {
    // Sanity check
    if (_eps.size() != _si->Ntot) {
      ostringstream msg;
      msg << "You provided " << _eps.size() 
	  << " distributions but I need " << _si->Ntot << endl;
      throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
    }

    distrib = _eps;

  } else {

    unsigned sz = 1 + _si->Ndim + _si->Next;

    // Sanity check
    if (_eps.size() != sz) {
      ostringstream msg;
      msg << "You provided " << _eps.size() 
	  << " distributions but I need " << sz << " which is "
	  << " 1+Ndim+Next=" << "1+" << _si->Ndim << "+"
	  << _si->Next << endl;
      throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
    }
    
    for (unsigned m=0; m<_si->M; m++) distrib.push_back(_eps[0]);
    for (unsigned m=0; m<_si->M; m++) {
      for (unsigned j=0; j<_si->Ndim; j++) 
	distrib.push_back(_eps[1+j]);
    }
    for (unsigned j=0; j<_si->Next; j++) 
      distrib.push_back(_eps[1+_si->Ndim + j]);
  }

}

void DifferentialEvolution::NewNumber(int mchains)
{
  if (mchains>0) {
    Mchains = mchains;
    CreateChains();
  }
}


void DifferentialEvolution::CreateChains()

{
				// Erase all previous chains
  chains.clear();
  update_flag .clear();
  update_flag1.clear();
  
				// Create and initialize new chains
  for (int k=0; k<Mchains; k++) {
    chains.push_back(Chain(_si));

    update_flag .push_back(0);
    update_flag1.push_back(0);

    nstat0.push_back(0);
    nstat1.push_back(0);
    
				// Assign ownership
    if (cntrl==serial) {
      chains[k].owner = k % numprocs;
    } else
      chains[k].owner = 0;

				// Assign internal index
    chains[k].indx = k;
				// Logging?
    if (caching) {
      if ( chains[k].owner == myid ) {
	ostringstream name;
	name << nametag << ".chainlog." << k;
	chains[k].Logging(name.str());
      }
    }
  }

				// For reporting . . .
  R0 = 0;
  chfid = &chains[0];
}

void DifferentialEvolution::ResetChainStats()
{
  if (mpi_used && myid) return;

  vector<Chain>::iterator j;
  for (int k=0; k<Mchains; k++) {
    nstat0[k] = 0;
    nstat1[k] = 0;
  }
  arate.erase(arate.begin(), arate.end());
}


void DifferentialEvolution::NewState(int M, vector<double>& wght, 
			      vector< vector<double> >& phi)
{
  chains_initialized = false;
  Initialize();
}

void DifferentialEvolution::NewState(Chain &ch)
{ 
  chains_initialized = false;
  Initialize();
}


void DifferentialEvolution::NewState(State& s) 
{ 
  chains_initialized = false;
  Initialize();
}


void DifferentialEvolution::Initialize()
{
  if (chains_initialized) return;

  switch(cntrl) {

  case parallel:		// Root initiates assignment
				// but likelihood is computed in parallel
    for (int k=0; k<Mchains; k++) SampleNewStateParallel(k);

    break;
      
  case serial:

				// Owner initiates assignement and
				// computes likelihood
    for (int k=0; k<Mchains; k++) SampleNewStateSerial(k);
    
				// Share the newly initialized chains with
    if (mpi_used)		// all procs
      for (int k=1; k<Mchains; k++) chains[k].BroadcastChain();
    
    break;

  default:
    ostringstream out;
    out << "We don't know about the control type #" << cntrl;
    
    throw InternalError(out.str(), __FILE__, __LINE__);
  }

  chains_initialized = true;

#ifdef DEBUG2
  ostringstream sout;
  sout << "debug_chain_init." << myid;
  ofstream out(sout.str().c_str());
  for (int k=0; k<Mchains; k++) {
    out << endl << "Chain " << k << endl;
    chains[k].PrintStats (out);
    chains[k].PrintState (out);
    chains[k].PrintState1(out);
  }
#endif
}


void DifferentialEvolution::Reinitialize(MHWidth* width, Prior *mp)
{

  // reset state with new info
  //
  Simulation::Reinitialize(width, mp);

  // don't reset minmc because it shouldn't be changing between levels
  // don't reset Mchains because it shouldn't be changing size
  // don't reset beta because it is although initialized the values do
  // not change and it does not change size
  
  chains_initialized = false;
  Initialize();
  ResetChainStats();
    
  totalnum = 0;
  totaltry = 0;
  ncount = 0;
}


void DifferentialEvolution::SampleNewStateParallel(int k)
{  
  const string member_name = "DifferentialEvolution::SampleNewStateParallel: ";

  int niter = 0;
  bool bad = true;

  while (bad) {

    try {
      if (myid==0) 
	_prior->SamplePrior(chains[k].Cur());
      _mca->ComputeNewState(this, &chains[k]);
      bad = false;
    }
    catch (ImpossibleStateException &e) {
      niter++;
      if (niter > state_iter) {
	ostringstream out;
	out << member_name
	    << "failed to produce a good initial state after "
	    << niter << " attempts.";
	throw InternalError(-70, out.str(), __FILE__, __LINE__);
      }
    }
  }
  
  if (mpi_used) chains[k].BroadcastChain();

}


void DifferentialEvolution::SampleNewStateSerial(int k)
{  
  if (k==0) NewStateSerialInit();

  if (myid==chains[k].owner) {
  
    int niter = 0;
    bool bad = true;
    
    while (bad) {

      try {
	_prior->SamplePrior(chains[k].Cur());
	_mca->ComputeNewState(this, &chains[k]);
	bad = false;
      }
      catch (ImpossibleStateException &e) {
	niter++;
	if (niter > state_iter) {
	  fail[chains[k].owner] = 1;
	  break;
	}
      }
    }
  }

  if (k==Mchains-1) NewStateSerialStatus();
}


void DifferentialEvolution::NewStateSerialInit()
{
  fail = vector<unsigned short>(Mchains, 0);
}

void DifferentialEvolution::NewStateSerialStatus()
{  
  const string member_name = "DifferentialEvolution::SampleNewStateSerial: ";

  vector<unsigned short> fail0(Mchains, 0);
  if (mpi_used)
    MPI_Allreduce(&fail[0], &fail0[0], Mchains, MPI_UNSIGNED_SHORT, MPI_SUM,
		  MPI_COMM_WORLD);
  else
    fail0 = fail;

  unsigned ncnt = 0;
  for (int i=0; i<Mchains; i++) ncnt += fail0[i];

  if (ncnt) {
    ostringstream out;
    out << member_name
	<< ncnt << " chains "
	<< "failed to produce a good initial state after "
	<< state_iter << " attempts";

    throw InternalError(-71, out.str(), __FILE__, __LINE__);
  }
}


void DifferentialEvolution::TrialState(int zero, int one, int two)
{
  chainT = chains[zero];

  double w[3];
  bool bad = false;

  unsigned ibeg = 0;
  if (_si->ptype == StateInfo::RJTwo) ibeg = 1;

  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::RJTwo) {

    for (unsigned j=ibeg; j<chainT.N0(); j++) {
	
      w[0] = chainT.Vec0(j);
      w[1] = chains[one].Vec0(j);
      w[2] = chains[two].Vec0(j);
	
      chainT.Vec0(j) = w[0] + gamma*(w[1]-w[2]) +  (*(distrib[j])).Sample()[0];
    }

  } else if (_si->ptype == StateInfo::Block) {
    
    for (unsigned j=ibeg; j<chainT.N0(); j++) {
	
      if (chainT.change0(j)) {
	  w[0] = chainT.Vec0(j);
	  w[1] = chains[one].Vec0(j);
	  w[2] = chains[two].Vec0(j);
	  
	  chainT.Vec0(j) = w[0] + gamma*(w[1]-w[2]) +  (*(distrib[j])).Sample()[0];
      }
    }
    
  } else {
    
    unsigned M    = chainT.M0();
    unsigned Ndim = _si->Ndim;
    unsigned Next = _si->Next;
    double   sum  = 0.0;

    for (unsigned i=0; i<M; i++) {

      //
      // Weight
      //
      w[0] = Logit(chainT.Wght0(i));
      w[1] = Logit(chains[one].Wght0(i));
      w[2] = Logit(chains[two].Wght0(i));
    
      chainT.Wght0(i) = 
	InverseLogit(w[0] + gamma*(w[1] - w[2]) + (*(distrib[0])).Sample()[0]);
      
      chainT.Wght0(i) 
	= max<double>(_lower[0], min<double>(_upper[0], chainT.Wght0(i)));

      sum += chainT.Wght0(i);

      //
      // Mixture parameters
      //
      
      for (unsigned j=0; j<Ndim; j++) {
	
	w[0] = chainT.Phi0(i, j);
	w[1] = chains[one].Phi0(i, j);
	w[2] = chains[two].Phi0(i, j);
	
	chainT.Phi0(i, j) = w[0] + gamma*(w[1] - w[2]) +  
	  (*(distrib[1+j])).Sample()[0];
      }
    }

    //
    // Normalize the weights
    //
    for (unsigned i=0; i<M; i++) chainT.Wght0(i) /= sum;
    
    //
    // Extended parameters
    //
      
    for (unsigned j=0; j<Next; j++) {
	
      w[0] = chainT.Ext0(j);
      w[1] = chains[one].Ext0(j);
      w[2] = chains[two].Ext0(j);
	
      chainT.Ext0(j) = w[0] + gamma*(w[1] - w[2]) +  
	(*(distrib[1+Ndim+j])).Sample()[0];
    }
  }
  
  //
  // Enforce bounds
  //

  _prior->EnforceBounds(chainT);

  //
  // Check bounds on proposed state from prior distribution
  //
  
  for (unsigned j=ibeg; j<chainT.N0(); j++) {
    if (chainT.Vec0(j) < _lower[j]) bad = true;
    if (chainT.Vec0(j) > _upper[j]) bad = true;
  }

  if (bad) throw ImpossibleStateException(__FILE__, __LINE__);

  chainT.Order0();
}

void DifferentialEvolution::MCMethod()
{
  int ok;
  State state1;
  int R1, R2;
  double logprob;

  vector<unsigned char> brate(Mchains, 0);

  //
  // Set default gamma parameter
  //
  if (ncount++ % jfreq) gamma = gamma0;
  else                  gamma = 1.0;

  //
  // Prolog for mixture models only
  //
  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended ) {

    if (myid==0) {		// Sanity check of weights
      for (int k=0; k<Mchains; k++) {
	bool bad = false;
	double sum = 0.0;
	for (unsigned j=0; j<chains[k].M0(); j++) {
	  if (chains[k].Wght0(j)<0.0 || chains[k].Wght0(j)>1.0) bad = true;
	  sum += chains[k].Wght0(j);
	}
	if (fabs(sum-1.0) > 1.0e-10 || bad) {
	  
	  cerr << "DifferentialEvolution: LOGIC ERROR!! "
	       << "[BEGIN] Inconsistent weights for chain k=" << k << endl;
	}
      }
    }

  }

				// ==============================
  if (cntrl == parallel) {	// cntrl==parallel
				// ==============================


				// Cycle through all chains
				// ------------------------
    for (int k=0; k<Mchains; k++) {

      int trial_ok = 1;		// Set to zero if trial state is out of bounds

      if (myid==0) {

	try {			// Pick two *other* chains at random
				// ---------------------------------
	  do {
	    R1 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	  } while (R1 == k);
	  
	  do {
	    R2 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	  } while (R2 == k || R1 == R2);

				// Make trial state
				// ----------------
	  TrialState(k, R1, R2);

	}
	catch (ImpossibleStateException &e) {
#ifdef DEBUG
	  cerr << "DifferentialEvolution: bad state proposed" << endl
	       << e.getErrorMessage() << "... process " << myid 
	       << " continuing" << endl;
#endif
	  trial_ok = 0;
	}

      }

      MPI_Bcast(&trial_ok, 1, MPI_INT, 0, MPI_COMM_WORLD);
      
				// Evaluate and handle the bad state exceptions
				// --------------------------------------------
      nstat0[k]++;
      totaltry++;

				// If state is in bounds, do the evaluation
				// ----------------------------------------
      if (trial_ok) {

	try {
				// Assign and send state to everywhere
				// -----------------------------------
	  if (myid==0)  chains[k].AssignState1(chainT);
	  if (mpi_used) chains[k].BroadcastChain();

	  logprob = 
	    _mca->ComputeState(this, &chains[k]) - chains[k].value;

	  if (myid==0) {

	    double accept_prob = min<double>(1.0, exp(logprob));
	    
	    if (accept_prob >= (*unit)()) {
	    
	      chains[k].AcceptState();

				// Keep tally of accepted states
				// -----------------------------
	      nstat1[k]++;
	      brate[k] = 1;
	      totalnum++;
	    }
	  }

	}
	catch (ImpossibleStateException &e) {
#ifdef DEBUG
	  cerr << "DifferentialEvolution: bad state proposed" << endl
	       << e.getErrorMessage() << "... process " << myid 
	       << " continuing" << endl;
#endif
	}
      }

      // Record the state (diagnostic)
      if (myid==chains[k].owner) chains[k].WriteLog();
    }

				// ==============================
  } else {			// cntrl==serial
				// ==============================

    for (int k=0; k<Mchains; k++) {
      update_flag [k] = 0;
      update_flag1[k] = 0;
    }

				// Cycle through all chains
				// ------------------------
    for (int k=0; k<Mchains; k++) {

      if (myid==chains[k].owner) {
				// Pick two *other* chains at random
				// ---------------------------------
	do {
	  R1 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	} while (R1 == k);
	  
	do {
	  R2 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	} while (R2 == k || R1 == R2);

				// Evaluate and handle the bad state exceptions
				// --------------------------------------------
	
	try {			// Make trial state
				// ----------------
	  TrialState(k, R1, R2);
	
				// Assign
				// ------
	  chains[k].AssignState1(chainT);

	  logprob = _mca->ComputeState(this, &chains[k]) - chains[k].value;
	  ok = 1;
	}
	catch (ImpossibleStateException &e) {
	  ok = 0;
	}
	  
	if (ok) {
	  double accept_prob = min<double>(1.0, exp(logprob));
	    
	  if (accept_prob >= (*unit)()) {

	    chains[k].AcceptState();

	    update_flag1[k] = 1;
	  }
	}
				// Owner records state
				// -------------------
	chains[k].WriteLog();
      }
    }

				// Update chains everywhere
				// ------------------------
    if (mpi_used) {
      ++MPIMutex;
      MPI_Allreduce(&update_flag1[0], &update_flag[0], Mchains, 
		    MPI_UNSIGNED_CHAR, MPI_SUM, MPI_COMM_WORLD);
      --MPIMutex;
	
      for (int k=0; k<Mchains; k++) {
	if (update_flag[k]) chains[k].BroadcastChain();
      }
    }

    if (myid==0) {
      for (int k=0; k<Mchains; k++) {
	nstat0[k]++;
	totaltry++;
	if (update_flag[k]) {
	  nstat1[k]++;
	  brate[k] = 1;
	  totalnum++;
	}
      }
    }
  }


  //
  // Epilog for RJTwo models only (propose splits and joints)
  //
  if (_si->ptype == StateInfo::RJTwo) {

    if (cntrl == parallel) {

      for (int k=0; k<Mchains; k++) {
	try {
	  _mca->Sweep(this, &chains[k]);
	}
	catch (ImpossibleStateException &e) {
	  // Ok, move on . . . 
	}
      }

    } else {

				// Reset the chain update flags
      if (mpi_used) {
	for (int k=0; k<Mchains; k++) {
	  update_flag [k] = 0;
	  update_flag1[k] = 0;
	}
      }
    
      for (int k=0; k<Mchains; k++) {
	if (myid==chains[k].owner) {
	  try {
	    if (_mca->Sweep(this, &chains[k]) && mpi_used) 
	      update_flag1[k] = 1;
	  }
	  catch (ImpossibleStateException &e) {
	    // Ok, move on . . . 
	  }
	}
      }
	
      if (mpi_used) {
	++MPIMutex;
	MPI_Allreduce(&update_flag1[0], &update_flag[0], Mchains, 
		      MPI_UNSIGNED_CHAR, MPI_SUM, MPI_COMM_WORLD);
	--MPIMutex;
	
	for (int k=0; k<Mchains; k++) {
	  if (update_flag[k]) chains[k].BroadcastChain();
	}
      }
    }
  }


  //
  // Epilog for mixture models only
  //
  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended ) {

    if (myid==0) {		// Sanity check of weights
      for (int k=0; k<Mchains; k++) {
	bool bad = false;
	double sum = 0.0;
	for (unsigned j=0; j<chains[k].M0(); j++) {
	  if (chains[k].Wght0(j)<0.0 || chains[k].Wght0(j)>1.0) bad = true;
	  sum += chains[k].Wght0(j);
	}
	if (fabs(sum-1.0) > 1.0e-10 || bad) {
	  
	  cerr << "DifferentialEvolution: LOGIC ERROR!! "
	       << "[END] Inconsistent weights for chain k=" << k << endl;
	}
      }
    }
  }

  arate.push_back(brate);
  if (arate.size()>nfifo) arate.pop_front();

  //
  // Set fiducial chain to largest posterior prob
  //
  if (myid==0) {
    double maxval = chains[0].value;
    R0 = 0;
    for (int k=1; k<Mchains; k++) {
      if (maxval<chains[k].value) {
	R0 = k;
	maxval = chains[k].value;
      }
    }

    chfid = &chains[R0];
  }

}


vector<double> DifferentialEvolution::GetStat(void)
{
  vector<double> ret;

  if (mpi_used && myid) return ret;

  unsigned arsiz = arate.size();

  if (arsiz==0) {
    ret = vector<double>(Mchains, 0.0);
    return ret;
  }
    
  unsigned cnt;

  for (int k=0; k<Mchains; k++) {
    cnt = 0;
    for (unsigned i=0; i<arsiz; i++) cnt += arate[i][k];
    ret.push_back((double)cnt/arsiz);
  }

  return ret;
}

void DifferentialEvolution::PrintStepDiagnostic()
{
  ostream cout(checkTable("simulation_output"));

  // could access all of these state variables directly...
  //
  cout << "           accept=" << totalnum << "/" << totaltry;
  //
  Simulation::PrintStepDiagnostic();
}

void DifferentialEvolution::PrintStateDiagnostic()
{
  if (mstat) {
    ostream cout(checkTable("simulation_output"));

    vector<double> ret = GetStat();

    cout << endl << setw(20) << " DE acceptance tally (per chain):" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	ostringstream out;
	out << nstat1[k] << "/" << nstat0[k];
	cout << " " << setw(9) << out.str();
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }

    cout << endl << setw(20) << " Rate (averaged over last " << arate.size() 
	 << " steps):" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	cout << " " << setw(9) << setprecision(3) << ret[k];
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }

    cout << endl << setw(20) << " Posterior values (per chain):" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	cout << " " << setw(9) << setprecision(2) << scientific
	     << chains[k].value;
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }
    cout << endl;

    if (ncount > state_diag) {

      int r_good = 0, r_soso = 0, r_vbad = 0;

      cout << endl << setw(20) << "Summary:" << endl;

      for (int k=0; k<(int)ret.size(); k++) {
	if (ret[k]>0.1) r_good++;
	else if (ret[k]>0.02) r_soso++;
	else r_vbad++;
      }

      if (r_good>(int)ret.size()/2 && r_vbad<(int)ret.size()/8)
	cout << setw(20) << " " 
	     << "Acceptance rate is excellent!" << endl;
      else if (r_soso>(int)ret.size()/2)
	cout << setw(20) << " " 
	     << "Acceptance rate is mediocre but ok" << endl;
      else 
	cout << setw(20) << " " 
	     << "Acceptance rate is horrible" << endl
	     << setw(25) << " "
	     << "==> Do something!" << endl
	     << setw(25) << " "
	     << "    Increase chain #, adjust gamma and/or the eps distributions!" << endl;
    }

  }

  // Algorithm specific info; may be blank
  //
  _mca->PrintAlgorithmDiagnostics(this);
}


void DifferentialEvolution::SetControl(int m)
{
  switch(m) {
  case parallel:
    cntrl = parallel;
    break;
  case serial:
    if (mpi_used)
      cntrl = serial;
    else {
      cout << "DifferentialEvolution: you want _serial_ control but you must"   << endl
	   << "                use MPI for that to make any sense.  You" << endl
	   << "                are getting _parallel_ control."          << endl;
    }
    break;
  default:
    ostringstream out;
    out << "Control method should be <parallel> (" << parallel
	<< ") or <serial> (" << serial << ")."
	<< "  input value was " << m;
    throw InternalError(out.str(), __FILE__, __LINE__);
  }

  CreateChains();
}

double DifferentialEvolution::GetValue() 
{ 
  Initialize(); 
  return chains[0].value; 
}
    
double DifferentialEvolution::GetPrior() 
{
  Initialize(); 
  return chains[0].prior_value; 
}
    
double DifferentialEvolution::GetLikelihood() 
{ 
  Initialize(); 
  return chains[0].like_value; 
}

State DifferentialEvolution::GetState() 
{ 
  Initialize(); 
  return chains[0].GetState0(); 
}
    
void DifferentialEvolution::ReportState() 
{ 
  if (mpi_used && myid && cntrl==parallel) return;
  Simulation::ReportState(chains[0].GetState0());
}

void DifferentialEvolution::AdditionalInfo()
{
  if (myid) return;

  cout << "Chain report" << endl << left
       << setw(6)  << "#"
       << setw(6)  << "indx"
       << setw(18) << "Beta"
       << setw(6)  << "Mcur"
       << setw(18) << "Value"
       << setw(6)  << "Mcur1"
       << setw(18) << "Value1"
       << endl;

  for (int i=0; i<Mchains; i++) {
    cout << left
	 << setw(6)  << i 
	 << setw(6)  << chains[i].indx
	 << setw(18) << chains[i].beta
	 << setw(6)  << chains[i].M0()
	 << setw(18) << chains[i].value
	 << setw(6)  << chains[i].M1()
	 << setw(18) << chains[i].value1
	 << endl;
  }

}


void DifferentialEvolution::LogState(string& outfile)
{
  ostream cout(checkTable("cli_console"));

  StateInfo::Pattern ptype = _si->ptype;
  State currentState = chfid->GetState0();

                                // Try only if outfile is not null
  if (outfile.size()>0) {
    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile <<
        "> for append\n";
      return;
    }

    if (first_log) {
      char q = '"';		// String delimiter

      logf << setw(20) << "\"Probability\"";
      logf << setw(20) << "\"Likelihood\"";
      logf << setw(20) << "\"Prior\"";
      
      if (ptype==StateInfo::Mixture ||
	  ptype==StateInfo::Extended )
	logf << setw(10) << "\"Number\"";

      for (unsigned k=0; k<_si->Ntot; k++) {
	ostringstream ostr;
	ostr << q << _si->ParameterDescription(k) << q;
        logf << setw(20) << ostr.str();
      }
      logf << endl;
      first_log = false;
    }

    for (int k=0; k<Mchains; k++) {
				// Probability values
      logf << setw(20) << setprecision(12) << chains[k].value
	   << setw(20) << setprecision(12) << chains[k].like_value
	   << setw(20) << setprecision(12) << chains[k].prior_value;
				// Mixture cardinality
      if (ptype==StateInfo::Mixture ||
	  ptype==StateInfo::Extended )
	logf << setw(10) << chains[k].M0();
				// State
      for (int j=0; j<(int)chains[k].GetState0().size(); j++)
	logf << setw(20) << setprecision(12) << chains[k].GetState0()[j];
      logf << endl;
    }
  }
}


void DifferentialEvolution::LogState(int level, int iterno, string& outfile)
{
  ostream cout(checkTable("cli_console"));

  StateInfo::Pattern ptype = _si->ptype;
  unsigned Ntot = _si->Ntot;

				// Try only if outfile is not null
  if (outfile.size()>0) {

				// Check for existence of output file 
				// before creating
    static bool firstime = true;
    bool exists = true;
    if (firstime) {
      exists   = FileExists(outfile);
      firstime = false;
    }

    State currentState = chfid->GetState0();

    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile << "> for append\n";
      return;
    }
				// Label fields first time through . . .
				// Labels are in " " (so that embedded 
				// spaces are ok)
    if (exists == false) {

      char q = '"';		// String delimiter


      logf << setw(7)  << "\"Level\"" 
	   << setw(10) << "\"Iter\""
	   << setw(20) << "\"Probability\""
	   << setw(20) << "\"Likelihood\""
	   << setw(20) << "\"Prior\"";

      if (ptype==StateInfo::Mixture ||
	  ptype==StateInfo::Extended )
	logf << setw(10) << "\"Number\"";

      for (unsigned j=0; j<Ntot; j++) {
	ostringstream ostr;
	ostr << q << _si->ParameterDescription(j) << q;
	logf << setw(20) << ostr.str();
      }

      logf << endl;

    }

    for (int k=0; k<Mchains; k++) {
				// Probability values
      logf << setw(7) << level << setw(10) << iterno;
      logf << setw(20) << setprecision(12) << chains[k].value 
	   << setw(20) << setprecision(12) << chains[k].like_value
	   << setw(20) << setprecision(12) << chains[k].prior_value;
				// Mixture cardinality
      if (ptype==StateInfo::Mixture ||
	  ptype==StateInfo::Extended )
	logf << setw(10) << chains[k].M0();
				// Write state
      for (unsigned j=0; j<Ntot; j++)
	logf << setw(20) << setprecision(12) << chains[k].Vec0(j);

      logf << endl;
      
    }

  }

}
