#include <iostream>
#include <iomanip>
#include <vector>

#include <BIEmpi.h>
#include <gvariable.h>
#include <MHWidthEns.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MHWidthEns)

using namespace std;
using namespace BIE;

MHWidthEns::MHWidthEns(StateInfo* si, Ensemble* e) : MHWidth(si), ensemble(e)
{
  //
  // Get the mixture list
  //
  vector<double> w;
  set<int> mixtures = ensemble->getValidSubspaceMixtures();
  for (set<int>::iterator m=mixtures.begin(); m!=mixtures.end(); m++) {
    int  MM = *m;
    spaces.insert(MM);

    //
    // Get the widths
    //
    w = ensemble->Width(MM);

    if (si->ptype == StateInfo::None || si->ptype == StateInfo::Block) {

      widths[MM] = w;

    } else {

      unsigned N = _si->Ndim;

      if (w.size() != MM*(N+1)) {
	ostringstream sout;
	sout << "MHWidthEns: dimension mismatch, M=" << MM
	     << " and Ndim=" << _si->Ndim << ", but width vector has dimension "
	     << w.size() << endl;
	throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
      }
      //
      // Add in quadrature
      //
      vector<double> ww(1+N, 0.0);
      for (int m=0; m<MM; m++) {
	ww[0] += w[m]*w[m];
	for (unsigned i=0; i<N; i++)
	  ww[1+i] += w[MM + N*m + i]*w[MM + N*m + i];
      }
      for (unsigned j=0; j<1+N; j++) ww[j] = sqrt(ww[j]/MM);
      widths[MM] = ww;
    }
  }
}

double MHWidthEns::widthM(int M)
{
  if (_si->ptype == StateInfo::None || _si->ptype == StateInfo::Block) {
    throw BIEException("MHWidthEns::widthM: Inconsistent request",
		       "This state is not a mixture!",
		       __FILE__, __LINE__);
  }

  if (spaces.find(M) == spaces.end()) {
    ostringstream sout;
    sout << "MHWidthEns::widthM: Inconsistent request. "
	 << "You wanted a mixture in subspace "
	 << M << " but it does not exist!" << endl;
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }

  return widths[M][0];
}


double MHWidthEns::widthM(int M, int j)
{
  if (_si->ptype == StateInfo::None || _si->ptype == StateInfo::Block) {
    throw BIEException("MHWidthEns::widthM: Inconsistent request",
		       "This state is not a mixture!",
		       __FILE__, __LINE__);
  }

  if (spaces.find(M) == spaces.end()) {
    ostringstream sout;
    sout << "MHWidthEns::WidthM: Inconsistent request. "
	 << "You wanted mixture in subspace "
	 << M << " but it does not exist!" << endl;
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }

  if (j>=static_cast<int>(_si->Ndim)) {
    ostringstream sout;
    sout << "MHWidthEns::widthM: you wanted position " << j
         << " in subspace " << M << " but parameter dimension is "
         << _si->Ndim << endl;
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }

  return widths[M][j+1];
}


double MHWidthEns::width(int j)
{
  switch (_si->ptype) {
  case StateInfo::None:
  case StateInfo::Block:
    if (spaces.find(1) == spaces.end()) {
      ostringstream sout;
      sout << "MHWidthEns::width: You wanted a non-mixture width but the "
	   << " space does not exist!" << endl;
      throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
    }

    if (j>=0 && j<static_cast<int>(_si->Ntot))
      return widths[1][j];
    else {
      ostringstream ostr;
      ostr << "MHWidthEns::width: requested index " << j 
	   << " is larger than the StateInfo size of " << _si->Ntot
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  case StateInfo::Extended:
    if (j>=0 && j<static_cast<int>(_si->Next))
      return widths[1][(1+_si->Ndim) + j];
    else {
      ostringstream ostr;
      ostr << "MHWidthEns::width: requested index " << j 
	   << " is larger than the StateInfo size of " << _si->Next
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  default:
    throw BIEException("MHWidthEns::width: Inconsistent request",
		       "This state is a pure mixture!",
		       __FILE__, __LINE__);
  }

  return 0.0;
}

