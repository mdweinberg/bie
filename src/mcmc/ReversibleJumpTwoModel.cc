#include <sstream>
using namespace std;

#include <ReversibleJumpTwoModel.h>
#include <LikelihoodComputation.h>
#include <MetropolisHastings.h>
#include <MixturePrior.h>
#include <gfunction.h>
#include <BIEconfig.h>
#include <BIEMutex.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::ReversibleJumpTwoModel)

using namespace BIE;

double ReversibleJumpTwoModel::JPROB = 0.5;
string ReversibleJumpTwoModel::name("ReversibleJumpTwoModel");


//! Constructor for mapping
RJMapping::RJMapping() : 
  d(0), mtype(Unary), rtype(Add), i0(0), i1(0), i2(0) 
{
  z  = std::vector<double>(2); 
  si = boost::shared_ptr<StateInfo>(new StateInfo(1));
  Q  = State(si.get());
}


//! Constructor for algorithm
ReversibleJumpTwoModel::ReversibleJumpTwoModel
(StateInfo* si, clivectorRJmap* map) : MCAlgorithm()
{
  _t     = si->T;
  _n1    = si->N1;
  _n2    = si->N2;
  _map   = (*map)();

  SanityCheck();		// Just like it says . . . see below.

  unit   = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm   = NormalPtr (new Normal (0.0, 1.0, BIEgen));

  jump   = true;
  nfreq  = 10;
  ncount = 0;
}

//! Constructor for algorithm
ReversibleJumpTwoModel::ReversibleJumpTwoModel
(StateInfo* si, std::vector<RJMapping*> map) : MCAlgorithm()
{
  _t     = si->T;
  _n1    = si->N1;
  _n2    = si->N2;
  _map   = map;

  SanityCheck();		// Just like it says . . . see below.

  unit   = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm   = NormalPtr (new Normal (0.0, 1.0, BIEgen));

  jump   = true;
  nfreq  = 10;
  ncount = 0;
}

//! Check to make sure that the mapping is consistent
void ReversibleJumpTwoModel::SanityCheck()
{
  ostringstream out;

  vector<bool>  model1(_n1, false); 
  vector<bool>  model2(_n2, false); 
				// Locations in the parameter vector
  unsigned beg1 = 1 + _t;
  unsigned beg2 = 1 + _t + _n1;
  unsigned end2 = 1 + _t + _n1 + _n2;

  for (vector<RJMapping*>::iterator it=_map.begin(); it!=_map.end(); it++) 
    {
      RJMapping* q = *it;
      //
      // First index
      //
      if (q->mtype == Sample) {
	if (q->i1==0) {
	  if (q->i0 < beg1 || q->i0 > beg2) {
	    out << "Sample index " << q->i0 << " must be >= " << beg1
		<< " and < " << beg2;
	    throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	  }
	  if (model2[q->i0-beg1]) {
	    out << "Sample index " << q->i0 << " is duplicated";
	    throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	  }
	  model1[q->i0-beg1] = true;
	} else {
	  if (q->i1 < beg2  || q->i1 > end2) {
	    out << "Sample index " << q->i1 << " must be >= " << beg2
		<< " and < " << end2;
	    throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	  }
	  if (model2[q->i1-beg2]) {
	    out << "Sample index " << q->i1 << " is duplicated";
	    throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	  }
	  model2[q->i1-beg2] = true;
	}
	
      } else {
	
	if (q->i0 < beg1 || q->i0 > beg2) {
	  out << "First index " << q->i0 << " must be >= " << beg1
	      << " and < " << beg2;
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	if (model1[q->i0-beg1]) {
	  out << "First index " << q->i0 << " is duplicated";
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	model1[q->i0-beg1] = true;
      }
      
      //
      // Second index
      //
      if (q->mtype == Join || q->mtype == SampleJoin) {
	if (q->i1<beg1 || q->i1>beg2) {
	  out << "Second index " << q->i1 << " must be >= " << beg1
	      << " and < " << beg2;
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	if (model1[q->i1-beg1]) {
	  out << "Second index " << q->i1 << " is duplicated";
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	model1[q->i1-beg1] = true;
      } 
      if (q->mtype == Split || q->mtype == SampleSplit || q->mtype == Unary) {
	if (q->i1<beg2 || q->i1>end2) {
	  out << "Second index " << q->i1 << " must be >= " << beg2
	      << " and < " << end2;
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	if (model2[q->i1-beg2]) {
	  out << "Second index " << q->i1 << " is duplicated";
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	model2[q->i1-beg2] = true;
      }

      //
      // Third index
      //
      if (q->mtype == Split || q->mtype == SampleSplit || q->mtype == Join) {
	if (q->i2<beg2 || q->i2>end2) {
	  out << "Third index " << q->i2 << " must be >= " << beg2
	      << " and < " << end2;
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	if (model2[q->i2-beg2]) {
	  out << "Third index " << q->i2 << " is duplicated";
	  throw BadParameterException(name, out.str(), __FILE__, __LINE__);
	}
	model2[q->i2-beg2] = true;
      }
    }

  for (unsigned k=0; k<_n1; k++) {
    if (model1[k]==false) {
      cout << "Index n_1=" << k << " not matched" << endl;
    }
  }
  for (unsigned k=0; k<_n2; k++) {
    if (model2[k]==false) {
      cout << "Index n_2" << k << " not matched" << endl;
    }
  }
}

void ReversibleJumpTwoModel::TestModeOn()
{
  jump = false;
}

void ReversibleJumpTwoModel::TestModeOff()
{
  jump = true;
}

void ReversibleJumpTwoModel::GetNewState(Simulation* s, Chain* ch)
{
				// Initialize state
  s->GetPrior()->SamplePrior(ch->Cur());
  ComputeNewState(s, ch);
}


void ReversibleJumpTwoModel::ComputeNewState(Simulation* s, Chain* ch)
{
  if (s->Serial()) {

    ch->prior_value = s->PriorValue(ch->GetState0());
    ch->like_value  = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value       = (ch->like_value + ch->prior_value)*ch->beta;

  } else {
    
    int bad;
    if (myid==ch->owner) {
      try {
	ch->prior_value = s->PriorValue(ch->GetState0());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
    }

    MPI_Bcast(&bad, 1, MPI_INT, ch->owner, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);
    
    ch->like_value = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value = (ch->like_value + ch->prior_value)*ch->beta;
  }

}



double ReversibleJumpTwoModel::ComputeCurrentState(Simulation* s, Chain* ch)
{
  if (s->Serial()) {

    ch->prior_value = s->PriorValue(ch->GetState0());
    ch->like_value = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value = (ch->like_value + ch->prior_value)*ch->beta;

  } else {
    
    int bad;
    if (myid==0) {
      try {
	ch->prior_value = s->PriorValue(ch->GetState0());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
    }

    MPI_Bcast(&bad, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);
    
    
    ch->like_value = s->Likelihood(ch->GetState0(), ch->indx);
    ch->value = (ch->like_value + ch->prior_value)*ch->beta;
  }

  if (myid==0 || s->Serial())
    return ch->value;
  else
    return 0.0;
}



double ReversibleJumpTwoModel::ComputeState(Simulation* s, Chain* ch)
{
  if (s->Serial()) {

    ch->prior_value1 = s->PriorValue(ch->GetState1());
    ch->like_value1 = s->Likelihood(ch->GetState1(), ch->indx);
    ch->value1 = (ch->like_value1 + ch->prior_value1)*ch->beta;

  } else {
    
    int bad;
    if (myid==ch->owner) {
      try {
	ch->prior_value1 = s->PriorValue(ch->GetState1());
	bad = 0;
      }
      catch (ImpossibleStateException &e) {
	bad = 1;
      }
    }



    MPI_Bcast(&bad, 1, MPI_INT, ch->owner, MPI_COMM_WORLD);
    if (bad) throw ImpossibleStateException(__FILE__, __LINE__);

    ch->like_value1 = s->Likelihood(ch->GetState1(), ch->indx);
    ch->value1 = (ch->like_value1 + ch->prior_value1)*ch->beta;
  }

  if (myid==ch->owner || s->Serial())
    return ch->value1;
  else
    return 0.0;
}



bool ReversibleJumpTwoModel::Sweep(Simulation* s, Chain* ch)
{
  bool do_jump  = jump;
  bool accepted = false;

  if (nfreq>0) {

    if (ncount++ % nfreq) do_jump = false;

    // Compute likelihood and posterior for new state
    //
    ComputeState(s, ch);
    
    // Update
    //
    if (myid==ch->owner || s->Serial()) {
				// Metropolis-Hastings
      double accept_prob = min<double>(1.0, exp(ch->value1 - ch->value));

      ch->stat[0]++;
      
      if (accept_prob >= (*unit)()) {
	ch->AcceptState();
	ch->stat[1]++;
	accepted = true;
      }
    }
    
    if (mpi_used && s->Parallel()) {
      ch->BroadcastChain();
    }
  }
    
  // Propose jump to alt model
  //
  if (do_jump) {

    double newP = 0;
    bool doit = ((*unit)()<JPROB) ? true : false;
    
    if (mpi_used && s->Parallel()) {
      unsigned ok = doit;
      ++MPIMutex;
      MPI_Bcast(&ok, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
      --MPIMutex;
      doit = ok ? true : false;
    }

    if (doit) {

      vector<double> ph;

      if (myid==ch->owner || s->Serial()) {
	newP = MakeProposal(ch);
      }

      if (mpi_used && s->Parallel()) {
	ch->BroadcastChain();
      }
      
      // Compute the state
      //
      try {
	ComputeState(s, ch);
      }
      catch (ImpossibleStateException &e) {
	throw e;
      }
	
      if (myid==ch->owner || s->Serial()) {
	  
	double A = exp(ch->value1 - ch->value + log(newP) );
	  
	double accept_prob = min<double>(1.0, A);
	  
	if (myid==0) {
	  ofstream tout("jump.dat", ios::app);
	  if (tout) {
	    tout << setw(3)  << current_level 
		 << setw(18) << accept_prob   << endl;
	  }
	}
	
	if (accept_prob >= (*unit)()) {
	  if (ch->Vec1(0) == 1) { // Split
	    ch->born[2]++;
	    ch->born[3]++;
	  }
	  if (ch->Vec1(0) == 0) { // Join
	    ch->died[2]++;
	    ch->died[3]++;
	  }
	  
	  ch->AcceptState();
	  accepted = true;
	  
	}
      }
      
    }
  }


  return accepted;
}


void ReversibleJumpTwoModel::PrintAlgorithmDiagnostic(Simulation* s)
{
  vector<int> nb, nd, ns, nu;
  s->GetStateDiag(nb, nd, ns, nu);

  cout << endl
       << "        1->2 move=" << left << setw(8) << nb[0]
       << "        2->1 move=" << left << setw(8) << nd[0] << endl
       << "  total 1->2 move=" << left << setw(8) << nb[1]
       << "  total 2->1 move=" << left << setw(8) << nd[1] << endl
       << "        3->4 move=" << left << setw(8) << ns[0]
       << "        4->3 move=" << left << setw(8) << nu[0] << endl
       << "  total 3->4 move=" << left << setw(8) << ns[1]
       << "  total 4->3 move=" << left << setw(8) << nu[1] << endl;
}


double RJMapping::fSample()
{
  q = d->Sample()[0];
  return q;
}

double RJMapping::fUnary(double X)
{
  x  = X;
  q1 = d1->Sample()[0];

  double y = 0.0;
  switch (rtype)
    {
    case Add:
      y  = x + q1;
      q2 = x - q1;
      break;
    case Mult:
      y  = x * q1;
      q2 = x / q1;
      break;
    case Cons:
      y  = x * q1;
      q2 = x *(1.0 - q1);
    }

  return y;
}


double RJMapping::fInvUnary(double y)
{
  q2 = d2->Sample()[0];

  switch (rtype)
    {
    case Add:
      x  = 0.5*(y + q2);
      q1 = 0.5*(y - q2);
      break;
    case Mult:
      x  = sqrt(fabs(y*q2));
      q1 = sqrt(fabs(y / q2));
      break;
    case Cons:
      x  = y + q2;
      q1 = y / x;
    }

  return x;
}


std::vector<double> RJMapping::fBinary(double X)
{
  x = X;
  q = d->Sample()[0];

  switch (rtype) 
    {
    case Add:  
      z[0] = x + q; 
      z[1] = x - q; 
      break;
    case Mult: 
      z[0] = x * q; 
      z[1] = x / q; 
      break;
    case Cons:
      z[0] = x * q;
      z[1] = x *(1.0 - q);
    }

  return z;
}

double RJMapping::fInvBinary(std::vector<double>& Z)
{
  z = Z;
  switch (rtype) 
    {
    case Add:  
      x = 0.5*(z[0] + z[1]);
      q = 0.5*(z[0] - z[1]);
      break;
    case Mult:
      x = sqrt(fabs(z[0] * z[1]));
      q = sqrt(fabs(z[0] / z[1]));
      break;
    case Cons:
      x = z[0] + z[1];
      q = z[0] / x;
    }

  return x;
}


std::vector<double> RJMapping::fTriple(double X)
{
  x  = X;
  q1 = d1->Sample()[0];
  q2 = d2->Sample()[0];

  switch (rtype) 
    {
    case Add:  
      z[0] = x + q1; 
      z[1] = x + q2; 
      q3   = 0.5*(q1 + q2);
      break;
    case Mult: 
    case Cons:
      z[0] = x * q1; 
      z[1] = x * q2; 
      q3   = sqrt(q1*q2);
      break;
    }
  
  return z;
}

double RJMapping::fInvTriple(std::vector<double>& Z)
{
  z  = Z;
  q3 = d2->Sample()[0];
  switch (rtype) 
    {
    case Add:  
      x  = 0.5*(z[0] + z[1]) - q3;
      q1 = z[0] - x + q3;
      q2 = x - z[0] + q3;
      break;
    case Mult:
    case Cons:
      x  = sqrt(fabs(z[0] * z[1] / q3));
      q1 = sqrt(fabs(z[0] * q3 / z[1]));
      q2 = sqrt(fabs(z[1] * q3 / z[0]));
      break;
    }

  return x;
}


double RJMapping::Prob() 
{ 
  double prob = 1.0;
  if (mtype == Unary) {
    if (rtype == Add) {
      Q[0] = q1; prob  = 2.0 / d1->PDF(Q);
      Q[0] = q2; prob *= d2->PDF(Q);
    }
    if (rtype == Mult) {
      Q[0] = q1; prob  = 2.0 * fabs(q2) / d1->PDF(Q);
      Q[0] = q2; prob *= d2->PDF(Q);
    }
    if (rtype == Cons) {
      Q[0] = q1; prob = fabs(x) / d1->PDF(Q);
      Q[1] = q2; prob *= d2->PDF(Q);
    }
  } else if (mtype == Sample) {
    Q[0]  = q;
    prob  = d->PDF(Q);
  } else if (mtype == SampleSplit || mtype == SampleJoin) {
    Q[0]  = q1;
    prob  = d1->PDF(Q);
    Q[0]  = q2;
    prob *= d2->PDF(Q); 
    Q[0]  = q3;
    prob /= d3->PDF(Q); 
    if (rtype == Mult)
      prob *= 2.0 * sqrt(z[0]*z[1]*z[2]);
    if (mtype == SampleJoin) prob = 1.0/prob;
  } else {			// Binary
    Q[0] = q;
    if (rtype == Add)      prob = 2.0 / d->PDF(Q);
    if (rtype == Mult)     prob = 2.0 * fabs(z[1]) / d->PDF(Q);
    if (rtype == Cons)     prob = fabs(z[1]) / d->PDF(Q);
    if (mtype == Join) prob = 1.0/prob;
  }

  return prob;
}


double ReversibleJumpTwoModel::MakeProposal(Chain *ch)
{
  // Temporary vector
  std::vector<double> Z(2);

  //
  // Copy the unchanged components to the test vector
  //
  for (unsigned t=1; t<=_t; t++) ch->Vec1(t) = ch->Vec0(t);
  
  //
  // Compute the mapping and probability
  //
  double prob = 1.0;
  for (vector<RJMapping*>::iterator it=_map.begin(); it!=_map.end(); it++)  {
    RJMapping* m = *it;
    
    // Currently: we are using Model 1
    //
    if (ch->Vec0(0) == 0) {

      // Try Model 2
      //
      ch->Vec1(0) = 1;

      switch (m->mtype) {

      case Unary:
	ch->Vec1(m->i1) = m->fUnary(ch->Vec0(m->i0));
	break;

      case Split:
	Z = m->fBinary(ch->Vec0(m->i0));
	ch->Vec1(m->i1) = Z[0];
	ch->Vec1(m->i2) = Z[1];
	break;
	
      case Join:
	Z[0] = ch->Vec0(m->i0);
	Z[1] = ch->Vec0(m->i1);
	ch->Vec1(m->i2) = m->fInvBinary(Z);
	break;

      case SampleSplit:
	Z = m->fTriple(ch->Vec0(m->i0));
	ch->Vec1(m->i1) = Z[0];
	ch->Vec1(m->i2) = Z[1];
	break;

      case SampleJoin:
	Z[0] = ch->Vec0(m->i0);
	Z[1] = ch->Vec0(m->i1);
	ch->Vec1(m->i2) = m->fInvTriple(Z);
	break;

      case Sample:
	if (m->i0==0) ch->Vec1(m->i1) = m->fSample();
	break;

      }
      
      prob *= m->Prob();

    } else {			// Current: we are using Model 2
				// 
      
      // Try Model 1
      //
      ch->Vec1(0) = 0;

      switch (m->mtype) {

      case Unary:
	ch->Vec1(m->i0) = m->fInvUnary(ch->Vec0(m->i1));
	break;

      case Split:
	Z[0] = ch->Vec0(m->i1);
	Z[1] = ch->Vec0(m->i2);
	ch->Vec1(m->i0) = m->fInvBinary(Z);
	break;
	  
      case Join:
	Z = m->fBinary(ch->Vec0(m->i2));
	ch->Vec1(m->i0) = Z[0];
	ch->Vec1(m->i1) = Z[1];
	break;

      case SampleSplit:
	Z[0] = ch->Vec0(m->i1);
	Z[1] = ch->Vec0(m->i2);
	ch->Vec1(m->i0) = m->fInvBinary(Z);
	break;
	  
      case SampleJoin:
	Z = m->fBinary(ch->Vec0(m->i2));
	ch->Vec1(m->i0) = Z[0];
	ch->Vec1(m->i1) = Z[1];
	break;

      case Sample:
	if (m->i1==0) ch->Vec1(m->i0) = m->fSample();
	break;

      }
	
      prob /= m->Prob();
    }
  }

  // Assign diag counters from current (0,1) to trial (2,3)
  //
  for (int i=0; i<2; i++) ch->born[2+i] = ch->born[i];
  for (int i=0; i<2; i++) ch->died[2+i] = ch->died[i];

  // Return the probability (including the Jacobian) for the mapping
  //
  return prob;
}
