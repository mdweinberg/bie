// -*- C++ -*-


/* 
   Analyzes convergence and provides graphical output

   Implementation of algorithm proposed by:

   	Giakoumatos, Vrontos, Dellaportas and Politis, "An MCMC 
	Convergence Diagnostic using Subsampling" (1999), preprint 
	to appear in to in Journal of Computational and Graphical 
	Statistics. 

	Published version:

	Giakoumatos S.G., Vrontos I.D., Dellaportas P., and Politis
	D.N.(1999). An MCMC Convergence Diagnostic using
	Subsampling. Journal of Computational and Graphical
	Statistics, 8, 431-451.

   This generalizes Gelman and Rubin (1992).  */

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>  //math.h is in the c++ standard library! equiv to <cmath>

using namespace std;

#include <string>

#include <gvariable.h>
#include <Distribution.h>
#include <SubsampleConverge.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SubsampleConverge)

using namespace BIE;
  
				// Number of subintervals
int SubsampleConverge::Nint = 0;
				// Number in each linear correlation test
int SubsampleConverge::Ngrp = 0;
				// Initial burn in
unsigned SubsampleConverge::nstart = 100;
				// Number of converged states
unsigned SubsampleConverge::ngood = 1000;
				// Quantile
double SubsampleConverge::alpha = 0.05;
				// Tolerance for COD
double SubsampleConverge::rtol = 0.99;
				// Number of groups to pass test
int SubsampleConverge::nconsec = 0;
				// Scale coordinates
bool SubsampleConverge::scale = true;
				// Diagnostic file & graphical output
bool SubsampleConverge::verbose = true;
				// Small pos double constant
double SubsampleConverge::tiny = 1.0e-12;


SubsampleConverge::SubsampleConverge(int m, Ensemble* d, string id) : Converge(d)
{
  max   = m;
  next  = 0;
  _id   = id;
}


SubsampleConverge* SubsampleConverge::New(int m, Ensemble* d, string id)
{
  return new SubsampleConverge(m, d, id);
}


bool SubsampleConverge::AccumData(vector<double>& value, State& state)
{
  _dist->AccumData(value, state);

  data.push_back(state);
  prob.push_back(value[0]);
  count++;

  if (count>max) {
    data.pop_front();
    prob.pop_front();
  }
    
  return true;
}

bool SubsampleConverge::Converged()
{
  ofstream **out = NULL;	// Assigning to NULL to suppress 
				// compiler warnings

  if (prob.size() < nstart || count < next) return false;
  
				// Chain has already converged and
				// desired number of states samples
  if (nburn > 0) return true;
				// Otherwise, test again

  if (verbose) {

    out = new ofstream* [4];

    //***************************************************
    // Initialize output files
    //***************************************************

    ostringstream out0; out0 << nametag;
    if (_id.size()>0) out0 << "." << _id;

    {
      ostringstream outfile;
      outfile << out0.str() << ".log";
      out[0] = new ofstream(outfile.str().c_str());
      if (!*out[0]) {
	throw FileNotExistException("SubsampleConverge: ", 
				    outfile.str(), __FILE__, __LINE__);
      }
    }

    {
      ostringstream outfile;
      outfile << out0.str() << ".maxdev";
      out[1] = new ofstream(outfile.str().c_str());
      if (!*out[1]) {
	throw FileNotExistException("SubsampleConverge: ",
				    outfile.str(), __FILE__, __LINE__);
      }
    }
    
    {
      ostringstream outfile;
      outfile << out0.str() << ".corr";
      out[2] = new ofstream(outfile.str().c_str());
      if (!*out[2]) {
	throw FileNotExistException("SubsampleConverge: ",
				    outfile.str(), __FILE__, __LINE__);
      }
    }
    
    {
      ostringstream outfile;
      outfile << out0.str() << ".bounds";
      out[3] = new ofstream(outfile.str().c_str());
      if (!*out[3]) {
	throw FileNotExistException("SubsampleConverge: ",
				    outfile.str(), __FILE__, __LINE__);
      }
    }

  }
  
  //***************************************************
  //  Begin computation
  //***************************************************
  
  int N = prob.size();
  int q = data[0].size();
  
				// Set up for correlation analysis
  
  vector<double> xx, yy, uu;
  
				// Total mean and variance
  vector<double> tmean(q, 0.0);
  vector<double> tvar(q, 0.0);
  for (int i=0; i<N; i++) {
    for (int k=0; k<q; k++) tmean[k] += data[i][k];
    for (int k=0; k<q; k++) tvar[k] += data[i][k] * data[i][k];
  }
  for (int k=0; k<q; k++) tmean[k] /= N;
  for (int k=0; k<q; k++) tvar[k] = (tvar[k] - tmean[k]*tmean[k]*N)/(N-1);
  
  
  if (verbose) *out[0] << endl 
		       << "Maximum deviations: " << endl << endl;
  
  int nint = Nint;
  int ngrp = Ngrp;
  if (!nint) nint = (int)sqrt( static_cast<float>(N));
  if (!ngrp) ngrp = (int)pow( static_cast<double>(nint), 0.7);
  
				// Step 1: choose subsamples
  for (int j=1; j<=nint; j++) {
    
    int Nj = j*N/nint;
    int bj = (int)(sqrt( static_cast<float>(Nj)));
    int Bj = Nj - bj + 1;
    
				// Step 2: compute total statistic
    vector<double> mean(q, 0.0);
    vector<double> var(q, 0.0);
    for (int i=(j-1)*N/nint; i<Nj; i++) {
      for (int k=0; k<q; k++) mean[k] += data[i][k];
      for (int k=0; k<q; k++) var[k] += data[i][k] * data[i][k];
    }
				// Mean
    for (int k=0; k<q; k++) mean[k] /= Nj;
				// Variance
    for (int k=0; k<q; k++) var[k] = (var[k] - mean[k]*mean[k]*Nj)/(Nj-1);
    
    
    
				// Step 3: compute subsample statistic
    vector<dvec> smean(Bj);
    for (int i=0; i<Bj; i++) {
      smean[i] = vector<double>(q, 0.0);
      for (int i2=0; i2<bj; i2++)
	for (int k=0; k<q; k++) smean[i][k] += data[i+i2][k];
      for (int k=0; k<q; k++) smean[i][k] /= bj;
    }
				// Step 4: compute maximum deviation
    
    vector<double> deviation(Bj, 0.0);
    double testdif;
    for (int i=0; i<Bj; i++) {
      for (int k=0; k<q; k++) {
	if (scale)
	  testdif = sqrt( static_cast<float>(bj))*fabs(smean[i][k] - mean[k])/
	    sqrt(fabs(tvar[k])+tiny);
	else
	  testdif = sqrt( static_cast<float>(bj))*fabs(smean[i][k] - mean[k]);
	if (testdif > deviation[i]) deviation[i] = testdif;
      }
    }
    
				// Step 5: sort and find quantile
    sort(deviation.begin(), deviation.end());
    double value = 
      deviation[std::min<int>( static_cast<int>(rint((1.0 - alpha)*Bj)), 
			       deviation.size()-1 )];
    
    
    // Step 6: output range
    double range = 2.0*value/sqrt( static_cast<float>(Nj));
    if (verbose) {
      
      *out[0] << setw(15) << Nj
	      << setw(15) << 1.0/sqrt( static_cast<float>(Nj))
	      << setw(15) << range
	      << endl;
      
      *out[1] << setw(15) << Nj
	      << setw(15) << 1.0/sqrt( static_cast<float>(Nj))
	      << setw(15) << range
	      << endl;
    }
				// Cache result vectors for correlation
				// analysis
    xx.push_back(1.0/sqrt( static_cast<float>(Nj)));
    yy.push_back(range);
    uu.push_back(bj);
    
  }
  
  if (verbose) {
    *out[0] << endl;
    *out[1] << endl;
  }
  
  
  //***************************************************
  // Correlation analysis
  //***************************************************
  
  if (verbose) {
    *out[0] << endl 
	    << "Coefficient of deviation: " << endl << endl;
  }
  
  double R2;
  int nvecs = xx.size();
  int nk = nvecs - ngrp + 1;

  vector<double> r2list;
  vector<int> nburnlist;
  nburn = -1;
  
  for (int i=0; i<nk; i++) {
    double sxx=0.0, sxy=0.0, syy=0.0, wght;
    double sx=0.0, sy=0.0, sum=0.0;
    
    for (int j=0; j<ngrp; j++) {
      wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
      sum += wght;
      sx += wght*xx[i+j];
      sy += wght*yy[i+j];
    }
    
    sx /= sum;
    sy /= sum;
    
    for (int j=0; j<ngrp; j++) {
      wght = sqrt(uu[i+j])*xx[i+j]*xx[i+j];
      sxy += wght*(xx[i+j] - sx)*(yy[i+j] - sy);
      sxx += wght*(xx[i+j] - sx)*(xx[i+j] - sx);
      syy += wght*(yy[i+j] - sy)*(yy[i+j] - sy);
    }
    
    R2 = sxy*sxy/(sxx*syy);

				// Print out number and coefficient
				// of determination, R2
    
    if (verbose) {
      
      *out[0] << setw(15) << 1.0/(xx[i]*xx[i])
	      << setw(15) << R2
	      << endl;
      
      *out[2] << setw(15) << 1.0/(xx[i]*xx[i])
	      << setw(15) << R2
	      << endl;
      
    }
    
				// Set burn in period based on COD
    if (R2>rtol && nburn<0) {
      nburn = (int)( 1.0/(xx[i]*xx[i]) );
      Converge::setBurnIn(nburn);
    }
				// Save values
    nburnlist.push_back(nburn);
    r2list.push_back(R2);
    
  }
  
				// Look for <nconsec> consecutive converged
				// results

  if (nconsec) {
				// Number of groups
    int ns = (int)r2list.size();
    if (ns>=nconsec) {
				// Check if last nconsec groups are converged
      bool tconsec = true;
      for (int i=ns-1; i>=ns-nconsec; i--) tconsec = tconsec && r2list[i]>rtol;

      if (tconsec)
	nburn = nburnlist[ns-1];
      else
	nburn = -1;
    }

  }

  //***************************************************
  //  Mode and quantiles after burn in
  //***************************************************
  
  if (nburn>0) {
    
    vector<dvec> qparm(q+1);
    
    if (verbose)
      *out[0] << endl 
	    << "Post burn in bounds [N=" << nburn << "]: " << endl << endl;
    
    for (int i=nburn; i<N; i++) {
      for (int k=0; k<q; k++) qparm[k].push_back(data[i][k]);
      qparm[q].push_back(prob[i]);
    }
    
    int beg = (int)( (N-nburn+1)*alpha );
    int mid = (int)( (N-nburn+1)*0.5 );
    int end = (int)( (N-nburn+1)*(1.0 - alpha) );
    
    if (verbose) {
      
      for (int k=0; k<=q; k++) {
	sort(qparm[k].begin(), qparm[k].end());
	*out[0]
	  << setw(15) << qparm[k][beg]
	  << setw(15) << qparm[k][mid]
	  << setw(15) << qparm[k][end]
	  << endl;
	
	*out[3]
	  << setw(15) << qparm[k][beg]
	  << setw(15) << qparm[k][mid]
	  << setw(15) << qparm[k][end]
	  << endl;
      }
    }
  }
  
  if (nburn>0) {
    next = nburn + ngood;
    if (verbose) {
      *out[0] << endl << "Converged!" << endl
	      << "Nburn=" << nburn;
      if (next - count>0) 
	*out[0] << ", still need to compute "
		<< next - count << " states" << endl;
      else
	*out[0] << ", we have enough states" << endl;
    }
  } else {
    next = count + (int)sqrt( static_cast<float>(max));
    if (verbose) *out[0] << endl << "No convergence!" << endl
			 << "Will test again in "
			 << next - count << " states" << endl;
  }

  if (verbose) {
    for (int i=0; i<4; i++) delete out[i];
    delete [] out;
  }    

  return false;
}

/*
  
  Notes:
  
  Coefficient of Determination: R Squared 
  ---------------------------------------
  
  COD = R^2
  
  where R is the Correlation Coefficient. 
  
  The coefficient of determination indicates how much of the total
  variation in the dependent variable can be accounted for by the
  regression function.  For example, a COD if .70 implies that 70% of
  the variation in y is accounted for by the Regression Equation. Most
  statisticians consider a COD of .7 or higher for a reasonable model.
  
*/
