#include <new>
#include <typeinfo>
#include <fstream>
#include <sstream>

#include <Serializable.h>

#include <FileExists.H>
#include <BIEACG.h>
#include <BIEmpi.h>
#include <gvariable.h>
#include <gfunction.h>
#include <LikelihoodComputation.h>
#include <MHWidth.h>
#include <Simulation.h>
#include <BIEdebug.h>
#include <BIEMutex.h>
#include <DataTree.h>
#include <EmptyDataTree.h>
#include <NullModel.h>
#include <BinnedLikelihoodFunction.h>
#include <PointLikelihoodFunction.h>
#include <PointIntegration.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Simulation)

using namespace BIE;

// For debug checking and output
// Set all to false for production
//
static const bool DEBUG_BINS = false;
static const bool DEBUG_CNTR = false;

				// Number of states to wait before 
				// "verbal" diagnostic output
unsigned int Simulation::state_diag  = 40;
				// Precision for float values in state
				// log
unsigned int Simulation::output_prec = 14;


Simulation::Simulation(StateInfo* si,
		       MHWidth* mhwidth,
		       BaseDataTree* d,
		       Model* m,
		       Integration* i,
		       Converge* c,
		       Prior* p,
		       LikelihoodComputation *l,
		       MCAlgorithm *mca,
		       Simulation *last)
{
  _si          = si;
  _mhwidth     = mhwidth;
  _dist        = d;
  _model       = m;
  _intgr       = i;
  _user_like   = false;
  _conv        = c;
  _prior       = p;
  _last        = last;
  _likelihoodComputation = l;
  _mca         = mca;
  _ngood       = 0;
  _nbad        = 0;
  _blocking    = false;
  _debug_state = false;
  _debug_cnt   = 0;

  Initial1();
}


Simulation::Simulation(StateInfo* si,
		       BaseDataTree* d,
		       Model* m,
		       Integration* i,
		       Converge* c,
		       Prior* p,
		       LikelihoodComputation *l,
		       MCAlgorithm *mca,
		       Simulation *last)
{
  _si          = si;
  _mhwidth     = 0;
  _dist        = d;
  _model       = m;
  _intgr       = i;
  _user_like   = false;
  _conv        = c;
  _prior       = p;
  _last        = last;
  _likelihoodComputation = l;
  _mca         = mca;
  _ngood       = 0;
  _nbad        = 0;
  _blocking    = false;
  _debug_state = false;
  _debug_cnt   = 0;

  Initial1();
}


Simulation::Simulation(StateInfo* si,
		       MHWidth* mhwidth,
		       Converge* c,
		       Prior* p,
		       LikelihoodComputation *l,
		       MCAlgorithm *mca,
		       Simulation *last)
{
  _si          = si;
  _mhwidth     = mhwidth;
  if (last==0) {
    _dist      = new EmptyDataTree();
    _model     = new NullModel();
    _intgr     = new PointIntegration();
    _blocking  = false;
    _user_like = true;
  } else {
    _dist      = last->_dist;
    _model     = last->_model;
    _intgr     = last->_intgr;
    _blocking  = last->_blocking;
    _user_like = false;
  }
  _conv        = c;
  _prior       = p;
  _last        = last;
  _likelihoodComputation = l;
  _mca         = mca;
  _ngood       = 0;
  _nbad        = 0;
  _debug_state = false;
  _debug_cnt   = 0;

  Initial2();
}


Simulation::Simulation(StateInfo* si,
		       Converge* c,
		       Prior* p,
		       LikelihoodComputation *l,
		       MCAlgorithm *mca,
		       Simulation *last)
{
  _si          = si;
  _mhwidth     = 0;
  if (last==0) {
    _dist      = new EmptyDataTree();
    _model     = new NullModel();
    _intgr     = new PointIntegration();
    _blocking  = false;
    _user_like = true;
  } else {
    _dist      = last->_dist;
    _model     = last->_model;
    _intgr     = last->_intgr;
    _blocking  = last->_blocking;
    _user_like = false;
  }
  _conv        = c;
  _prior       = p;
  _last        = last;
  _likelihoodComputation = l;
  _mca         = mca;
  _ngood       = 0;
  _nbad        = 0;
  _debug_state = false;
  _debug_cnt   = 0;

  Initial2();
}


void Simulation::InitialA()
{
  _priorFrontier = 0;
  if (_last) {
    _priorFrontier = _last->currentFrontier()->Copy();
  }
  _currentFrontier = _dist->GetDefaultFrontier()->Copy();

  _likelihoodComputation->init(this);

  first_log   = true;
  first_state = true;

  serialLP    = _likelihoodComputation->Serial();
}


void Simulation::InitialB()
{
  if (DEBUG_BINS) print_bin_frequency();

  state_not_initialized = true;

				// Initialize chain (but no state assigned)
  ch = Chain(_si);

				// Fiducial chain to report
  chfid = &ch;

  algoname = "Simulation";	// Algorithm name
}


void Simulation::Initial1()
{

  InitialA();

  if (_last) {

    internalLP = _last->internalLP;
    LP         = _last->LP;
				// Informational message . . .
    if (myid==0 && internalLP==false) 
      cout << "Simulation: using previous UserLikelihood.  "
	   << "You may use SetUserLikelihood()" << endl
	   << "to change this, although I'm not sure why you "
	   << "would want to do so" << endl;
    
  } else {


    if (dynamic_cast <BinnedDistribution*> (_dist->CurrentItem())) {
      internalLP = true;
      LP = new BinnedLikelihoodFunction();
      if (myid==0) cerr << "Simulation: using Binned probability" << endl;
    }
    else if (dynamic_cast <PointDistribution*> (_dist->CurrentItem())) {
      internalLP = true;
      LP = new PointLikelihoodFunction();
      if (myid==0) cerr << "Simulation: using Point probability" << endl;
    }
    else if (dynamic_cast <NullDistribution*> (_dist->CurrentItem())) {
      internalLP = false;
      LP = 0;
      if (myid==0) cerr << "Simulation: UserLikelihood to be set" << endl;
    }
    else
      throw NoSuchDistributionException(__FILE__, __LINE__);
  }

  InitialB();
}


void Simulation::Initial2()
{
  InitialA();

  if (_last) {

    internalLP = _last->internalLP;
    LP         = _last->LP;
				// Informational message . . .
    if (myid==0 && internalLP==false) 
      cout << "Simulation: using previous UserLikelihood.  "
	   << "You may use SetUserLikelihood()" << endl
	   << "to change this, although I'm not sure why you "
	   << "would want to do so" << endl;
    
  } else {

    internalLP = false;
    LP = 0;

  }

  InitialB();
}

Simulation::~Simulation()
{
  if (internalLP) delete LP;
  delete _priorFrontier;
  delete _currentFrontier;
  if (_user_like) {
    delete _dist;
    delete _model;
    delete _intgr;
  }
}


/*
 * Called as each level changes
 */

void Simulation::NewConvergence(int m, Ensemble* d, string id)
{
  Converge* _old = _conv;
  _conv  = _old->New(m, d, id);
  delete _old;
  delete _priorFrontier;
  delete _currentFrontier;
}


void Simulation::Reinitialize(MHWidth* width, Prior *p)
{
  // Since this is a replacement for reinstantiation during each level
  // reset to whatever values would be seen if it were freshly constructed
  // check these

  _mhwidth = width;
  _prior = p;

  first_log   = true;
  first_state = true;

  if (DEBUG_BINS) print_bin_frequency();

}

void Simulation::OneStep()
{
  //
  // To ensure synchronization
  //
  MPI_Barrier(MPI_COMM_WORLD);

  try {
    MCMethod();
  } 
  catch (BIEException& e) {
    cout << "Process " << myid 
	 << ": in Simulation::OneStep() calling MCMethod: "
	 << e.getErrorMessage();
    throw e;
  }

  //
  // Accumulate for convergence testing
  //
  if (myid==0) {

    vector<double> pr(3);	// Hold the prob values for accumulation

    if (chains.size()) {
      //
      // Parallel convergence interface: pass a list of fiducial (kT=1)
      // chains for accumulation
      //
      vector< vector<double> >values;
      vector<State>  states;
      for (unsigned m=0; m<chains.size(); m++) {
	if (fabs(chains[m].beta-beta0)<1.0e-18) {
	  pr[0] = chains[m].value;
	  pr[1] = chains[m].like_value;
	  pr[2] = chains[m].prior_value;

	  values.push_back(pr);
	  states.push_back(chains[m].GetState0());
	}
      }

      _conv->AccumData(values, states);

    } else {
      //
      // Single-chain convergence interface
      //
      pr[0] = chfid->value;
      pr[1] = chfid->like_value;
      pr[2] = chfid->prior_value;
      _conv->AccumData(pr, chfid->GetState0());
    }
  }

}

State Simulation::GetFromPrior() 
{
  State s(_si);

  if (_last)
    s = _last->_conv->Sample();
  else
    s = _prior->Sample();

  return s;
}

double Simulation::PriorValue(State& s)
{
  ostream cout(checkTable("simulation_output"));

  double ret = _prior->logPDF(s);

  if (std::isinf(ret)) {
    _nbad++;
    throw ImpossibleStateException("in Simulation::PriorValue",
				   __FILE__, __LINE__);
  }

  _ngood++;
  return ret;
}

// defer Likelihood implementation to supplied class
//
double Simulation::Likelihood(State& s, int indx)
{
  double ans=0.0, ans1=0.0, ans2=0.0;
  int bad1, bad;

				// First, compute the likelihood at the
				// current level
  try {
    ans = ans1 = _likelihoodComputation->Likelihood(s, indx);
    bad1 = 0;
  }
  catch (ImpossibleStateException &e) {
    bad1 = 1;
  }
  catch (BIEException& e) {
    cout << "Process " << myid << ": in Simulation::Likelihood(): "
	 << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	 << e.getErrorMessage();
    throw e;
  }

				// Share return state with all processes
  if (mpi_used && !serialLP) {
    ++MPIMutex;
    MPI_Allreduce(&bad1, &bad, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    --MPIMutex;
  }  else {
    bad = bad1;
  }

				// Abort if state is bad
  if (bad) {
    throw ImpossibleStateException(__FILE__, __LINE__);
  }


				// Next, compute the likelihood at the
				// previous level
  try {
    ans2 = _likelihoodComputation->LikelihoodPrevious(s, indx);
    ans -= ans2;		// Keep a temporary for debugging
    bad1 = 0;
  }
  catch (ImpossibleStateException &e) {
    bad1 = 1;
  }
  catch (BIEException& e) {
    cout << "Process " << myid << ": in Simulation::Likelihood(): "
	 << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	 << e.getErrorMessage();
    throw e;
  }
  
				// Share return state with all processes
  if (mpi_used && !serialLP) {
    ++MPIMutex;
    MPI_Allreduce(&bad1, &bad, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    --MPIMutex;
  }  else {
    bad = bad1;
  }

				// Abort if state is bad
  if (bad) throw ImpossibleStateException(__FILE__, __LINE__);

				// Log for debugging
  if (_debug_state) {
    if (!mpi_used || myid == 0) {
      ofstream dbg(_debug_tag.c_str(), ios::app);
      if (dbg) {
	dbg << setw(8)  << _debug_cnt++
	    << setw(15) << ans1 
	    << setw(15) << ans2 
	    << endl;
      }
    }
  }

  // Done!

  return ans;
}


#undef PER_BIN

void Simulation::print_bin_frequency(void)
{
  map<const int, int> X;
  typedef map<const int, int>::iterator myiter;
  int n;

  _dist->Reset();
  while (!_dist->IsDone()) {

#ifdef PER_BIN
    for (int j=0; j<_dist->CurrentItem()->numberData(); j++) {
      n = (int)_dist->CurrentItem()->getValue(j);
      myiter tst = X.find(n);
      if (tst == X.end()) {
	X[n] = 1;
      }
      else
	tst->second++;
    }
#else
    n = 0;
    for (int j=0; j<_dist->CurrentItem()->numberData(); j++)
      n += (int)_dist->CurrentItem()->getValue(j);

    myiter tst = X.find(n);
    if (tst == X.end()) {
      X[n] = 1;
    }
    else
      tst->second++;
    
#endif // PER_BIN

    _dist->Next();
  }

  ofstream out("tile.distribution");

  out << "#Number Frequency\n";
  if (out) {
    for (myiter i=X.begin(); i!=X.end(); i++) {
      out 
	<< setw(10) << i->first
	<< setw(10) << i->second
	<< endl;
    }
  }

  exit(0);
}

void Simulation::ReportState()
{
  ReportState(chfid->GetState0());
}

void Simulation::ReportState(State& s)
{
  ostream cout(checkTable("simulation_output"));

  vector<double>::iterator cur;

				// Column descriptions
  cout << endl;
  cout << chfid->Cur()->LabelHeader() << endl;
				// The state
  cout << *chfid->Cur() << endl;
  cout << endl;
}

void Simulation::LogState(string& outfile)
{
  ostream cout(checkTable("cli_console"));

  State currentState = chfid->GetState0();

                                // Try only if outfile is not null
  if (outfile.size()>0) {
    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile <<
        "> for append\n";
      return;
    }

    if (first_log) {
      char q = '"';

      logf << setw(20) << "\"Probability\"";
      logf << setw(20) << "\"Likelihood\"";
      logf << setw(20) << "\"Prior\"";

      if (currentState.Type() == StateInfo::Mixture ||
	  currentState.Type() == StateInfo::Extended )
	logf << setw(10) << "\"Number\"";

      for (unsigned k=0; k<currentState.size(); k++) {
	ostringstream ostr;
	ostr << q << currentState.ParameterDescription(k) << q;
        logf << setw(output_prec+10) << ostr.str();
      }
      logf << endl;

      first_log = false;
    }

    logf << scientific
	 << setw(20) << setprecision(12) << chfid->value
         << setw(20) << setprecision(12) << chfid->like_value
         << setw(20) << setprecision(12) << chfid->prior_value;
    
    if (currentState.Type() == StateInfo::Mixture ||
	currentState.Type() == StateInfo::Extended ) {

      logf << setw(10) << currentState.M();

      // DEBUG: weight check
      double sum = 0.0;
      for (unsigned k=0; k<currentState.M(); k++) {
	if (currentState.Wght(k)<0.0 || currentState.Wght(k)>1.0) {
	  cerr << "Weight is out of bounds!" << endl;
	}
	sum += currentState.Wght(k);
      }
      if (fabs(sum-1.0)>1.0e-18) {
	cerr << "Weight NORM is out of bounds!" << endl;
      }
      //
    }
    
    logf << setprecision(output_prec);

    for (int k=0; k<(int)currentState.size(); k++)
      logf << setw(output_prec+10) << currentState[k];
    logf << endl;
  }
}


void Simulation::LogState(int level, int iterno, string& outfile)
{
  ostream cout(checkTable("cli_console"));
				// Try only if outfile is not null
  if (outfile.size()>0) {


    State currentState = chfid->GetState0();

				// Check for existence of output file 
				// before creating
    static bool firstime = true;
    bool exists = true;
    if (firstime) {
      exists = FileExists(outfile);
      firstime = false;
    }

				// Now, open it
    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile << "> for append\n";
      return;
    }
				// Label fields first time through . . .
				// Labels are in " " (so that embedded 
				// spaces are ok)
    if (exists == false) {

      char q = '"';

      logf << setw(7)              << "\"Level\"" 
	   << setw(10)             << "\"Iter\""
	   << setw(output_prec+10) << "\"Probability\""
	   << setw(output_prec+10) << "\"Likelihood\""
	   << setw(output_prec+10) << "\"Prior\"";

      if (_si->ptype==StateInfo::Mixture || _si->ptype==StateInfo::Extended)
	logf << setw(10) << "\"Number\"";

      for (unsigned k=0; k<_si->Ntot; k++) {
	ostringstream ostr;
	ostr << q << _si->ParameterDescription(k) << q;
        logf << setw(output_prec+10) << ostr.str();
      }
      logf << endl;
    }

				// Write state
    logf << setw(7) << level << setw(10) << iterno;
    logf << scientific
	 << setw(output_prec+10) << setprecision(output_prec) << chfid->value 
	 << setw(output_prec+10) << setprecision(output_prec) << chfid->like_value
	 << setw(output_prec+10) << setprecision(output_prec) << chfid->prior_value;

    if (_si->ptype==StateInfo::Mixture || _si->ptype==StateInfo::Extended) {

      unsigned Mcur = currentState.M();
      unsigned Mmax = currentState.Mmax();
      unsigned Ndim = currentState.Ndim();
      unsigned Next = currentState.Next();

      logf << setw(10) << Mcur;
      logf << setprecision(output_prec);
    
      for (unsigned i=0; i<Mcur; i++)
	logf << setw(output_prec+10) << currentState.Wght(i);

      for (unsigned i=Mcur; i<Mmax; i++)
	logf << setw(output_prec+10) << 0.0;
      
      for (unsigned i=0; i<Mcur; i++) {
	for (unsigned j=0; j<Ndim; j++) 
	  logf << setw(output_prec+10) << currentState.Phi(i, j);
      }
      for (unsigned i=Mcur; i<Mmax; i++) {
	for (unsigned j=0; j<Ndim; j++) 
	  logf << setw(output_prec+10) << 0.0;
      }
      
      for (unsigned j=0; j<Next; j++) 
	logf << setw(output_prec+10) << currentState.Ext(j);

    } else {
      unsigned Ntot = currentState.N();
      logf << setprecision(output_prec);
      for (unsigned i=0; i<Ntot; i++)
	logf << setw(output_prec+10) << currentState[i];
    }
    logf << endl;

  }
}

void Simulation::MCMethod()
{
  //
  // Only the root process sets the state
  //
  if (myid==0) {

    if (state_not_initialized) {
      _prior->SamplePrior(chfid->Cur());
      state_not_initialized = false;
    }
  
    _prior->SampleProposal(ch, _mhwidth);
  
  }

  //
  // Compute value for the fidicial chain on the first time through
  //
  if (first_state) {

    try {
      _mca->ComputeNewState(this, chfid);
    }
    catch (ImpossibleStateException &e) {
      if (myid==0) cerr << "Simulation: bad state; " 
			<< "presumably this state was good at a previous"
			<< "level so this should not happen . . ."
			<< endl;
    } 
    catch (BIEException& e) {
      cout << "Process " << myid << ": in Simulation::MCMethod(): "
	   << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	   << e.getErrorMessage();
      throw e;
    }

    first_state = false;
  }

  //
  // Communicate the chain to all states
  //
  if (mpi_used) chfid->BroadcastChain();
  

  //
  // All processes are supposed to enter the likelihood function
  //
  try {
    mstat0++;
    if (_mca->Sweep(this, chfid)) mstat1++;
  }
  catch (ImpossibleStateException &e) {
    if (myid==0)
      cerr << "Simulation: bad state . . ." << endl;
  }
  catch (BIEException& e) {
    cout << "Process " << myid << ": in Simulation::MCMethod(): "
	 << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	 << e.getErrorMessage();
    throw e;
  }
}

void Simulation::PrintStateDiagnostic()
{
  if (mstat) {
    ostream cout(checkTable("simulation_output"));

    double ret = 0.0;
    if (mstat0>0) ret = (double)mstat1/(double)mstat0;

    cout << endl;
    cout << endl << setw(10) << "P(accept): " << ret 
	 << setw(15) << "# good/bad: " << _ngood << "/" << _nbad
	 << "  [" << algoname << "]" << endl;
    cout << endl;

    if (mstat0> (int)state_diag) {
      cout << endl << setw(10) << "Summary:" << endl;
    
      if (ret>0.2)
	cout << setw(15) << " " << "Acceptance is excellent!" 
	     << endl;
      else if (ret>0.05)
	cout << setw(15) << " " << "Acceptance is mediocre" << endl
	     << setw(20) << " " << "==> Consider decreasing MH width" 
	     << endl;
      else 
	cout << setw(15) << " " << "Acceptance is horrible" << endl
	     << setw(20) << " " << "==> Do something! E.g. decrease MH width" 
	     << endl;
    }
  }

  // Algorithm specific info; may be blank
  //
  _mca->PrintAlgorithmDiagnostics(this);
}


void Simulation::PrintStepDiagnostic()
{
  ostream cout(checkTable("simulation_output"));
  cout << "  [" << algoname << "]" << endl;
}


void Simulation::NewState(int M, vector<double>& wght, 
			  vector< vector<double> >& phi)
{
  State s(_si);
  s.setState(M, wght, phi);

  if (myid==0)  chfid->AssignState0(s);
  if (mpi_used) chfid->BroadcastChain();

  _mca->ComputeNewState(this, chfid); 
  state_not_initialized = false;
}

void Simulation::NewState(Chain &ch)
{ 
  chfid->AssignState0(ch);
  if (mpi_used) chfid->BroadcastChain();
  _mca->ComputeNewState(this, chfid);
  state_not_initialized = false;
}


void Simulation::NewState(State& s) 
{ 
  if (myid==0) chfid->AssignState0(s);
  if (mpi_used) chfid->BroadcastChain();
  _mca->ComputeNewState(this, chfid); 
  state_not_initialized = false;
}


void Simulation::SetUserLikelihood(LikelihoodFunction*fct)
{
  if (internalLP) delete LP;
  LP = fct;
  internalLP = false;
  if (myid==0) 
    cerr << "Simulation: using user defined likelihood"
	 << " (type id=" << typeid(*LP).name() << ")" << endl;
}


void Simulation::GetStateDiag(vector<int>& b, vector<int>& d,
			      vector<int>& s, vector<int>& u)
{
  if (chains.size()) {

    b = chains[0].born; 
    d = chains[0].died; 
    s = chains[0].split; 
    u = chains[0].unite; 

    for (unsigned n=1; n<chains.size(); n++) {
      for (unsigned i=0; i<4; i++) b[i] += chains[n].born [i]; 
      for (unsigned i=0; i<4; i++) d[i] += chains[n].died [i];
      for (unsigned i=0; i<4; i++) s[i] += chains[n].split[i];
      for (unsigned i=0; i<4; i++) u[i] += chains[n].unite[i];
    }

    if (myid==0 && DEBUG_CNTR) {
				// Get max number of digits
      int imax = *max_element(b.begin(), b.end());
      imax = max<int>(imax, *max_element(d.begin(), d.end()));
      imax = max<int>(imax, *max_element(s.begin(), s.end()));
      imax = max<int>(imax, *max_element(u.begin(), u.end()));

      if (imax>0) imax = static_cast<int>(floor(log(0.1+imax)/log(10.0))) + 2;
      else        imax = 2;

      cout << "*** Born:  ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << b[i];
      cout << endl;

      cout << "*** Died:  ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << d[i];
      cout << endl;

      cout << "*** Split: ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << s[i];
      cout << endl;

      cout << "*** Unite: ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << u[i];
      cout << endl;
    }

  } else {

    b = chfid->born;
    d = chfid->died;
    s = chfid->split;
    u = chfid->unite;

    if (myid==0 && DEBUG_CNTR) {
				// Get max number of digits
      int imax = *max_element(b.begin(), b.end());
      imax = max<int>(imax, *max_element(d.begin(), d.end()));
      imax = max<int>(imax, *max_element(s.begin(), s.end()));
      imax = max<int>(imax, *max_element(u.begin(), u.end()));

      if (imax>0) imax = static_cast<int>(floor(log(0.1+imax)/log(10.0))) + 2;
      else        imax = 2;

      cout << "*** Born:  ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << b[i];
      cout << endl;

      cout << "*** Died:  ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << d[i];
      cout << endl;

      cout << "*** Split: ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << s[i];
      cout << endl;

      cout << "*** Unite: ";
      for (unsigned i=0; i<4; i++) cout << setw(imax) << u[i];
      cout << endl;
    }

  }
}
