// This may look like C code, but it is really -*- C++ -*-

#include <TemperedSimulation.h>
#include <gfunction.h>
#include <Prior.h>
#include <BIEconfig.h>
#include <BIEMutex.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::TemperedSimulation)

using namespace BIE;

				// Global parameters
int    TemperedSimulation::Ninter 	 = 20;
bool   TemperedSimulation::use_tempering = true;
int    TemperedSimulation::minmc	 = 6;
				// A value of 0.5 is Gaussian scaling . . .
double TemperedSimulation::tpow		 = 0.5;
int    TemperedSimulation::ITMAX         = 2000;


void TemperedSimulation::Initialize(int minmc_p, double maxT)
{
  algoname = "Tempered Simulation";

  bwrap = new BarrierWrapper(MPI_COMM_WORLD, true);
  callCount = 0;

  if (minmc_p) minmc = minmc_p;

  MaxT = maxT;			// Temperature range for the tempering

  Ntot = _si->Ntot;		// Maximum dimension of state vector

  Mchains = max<int>(minmc, (int)(log(MaxT)*sqrt( static_cast<float>(Ntot))+1));
  cout << "TemperedSimulation: assigning " << Mchains 
       << " temperature levels\n";
  beta   = vector<double>(Mchains, 1.0);
  factor = vector<double>(Mchains, 1.0);

				// MaxT=1 and minmc=1 will select straight
				// Metropolis-Hastings
  if (Mchains==1) {
    use_tempering = false;
  }
  else {
    for (int k=1; k<Mchains; k++) {
      beta[k]   = exp(-log(MaxT)*k/(Mchains-1));
      factor[k] = 1.0/pow(beta[k], tpow);
    }
  }
    
  mstat0 = vector<int>(Mchains, 0);
  mstat1 = vector<int>(Mchains, 0);


  swapnum = 0;			// Number accepted tempered states
  accnum = 0;			// Number of accepted mixing states
  upM = downM = 0.0;		// Log prob of heated or cooled states
  updownNum = 0;		// Number of temperings

  unit = new Uniform(0.0, 1.0, BIEgen);
}

TemperedSimulation::~TemperedSimulation()
{
  delete unit;
  delete bwrap;
}

void TemperedSimulation::Reinitialize(MHWidth* width, Prior *mp)
{

  // reset state with new info
  Simulation::Reinitialize(width, mp);

  // don't reset minmc because it shouldn't be changing between levels
  // don't reset Mchains because it shouldn't be changing size
  // don't reset beta because it is although initialized the values do
  // not change and it does not change size
  
  for(int i=0; i<Mchains; i++) mstat0[i] = 0;
  for(int i=0; i<Mchains; i++) mstat1[i] = 0;
    
  swapnum = 0;			// Number accepted tempered states
  upM = downM = 0.0;		// Log prob of heated or cooled states
  updownNum = 0;		// Number of temperings

}


void TemperedSimulation::compute_proposal_state(int k)
{
				// Manipulate the chain's factor variable
				// rather than clone chains
  ch.indx   = k;
  ch.beta   = beta[k];
  ch.factor = factor[k];
  if (myid==0) {
    _prior->SampleProposal(ch, _mhwidth);
  }

  mstat0[k]++;

  if (mpi_used) ch.BroadcastChain();

  if (_mca->Sweep(this, &ch)) {
    accnum++;
    mstat1[k]++;
  }
				// Reset to default
  ch.indx   = 0;
  ch.beta   = 1.0;
  ch.factor = 1.0;
}
    

void TemperedSimulation::MCMethod()
{
  // Debug barrier . . . 
  {
    ostringstream ostr;
    ostr << "TemperedSimulation::MCMethod: begin call #" << callCount;
    (*bwrap)(ostr.str());
  }

  // Simplify notation
  double *V = &ch.value;
  //
  
  if (state_not_initialized && (!mpi_used || myid==0)) {
    _prior->SamplePrior(ch.Cur());
    ch.Order0();
    state_not_initialized = false;
  }

  up = down = 0.0;
  
  // **Compute value for initial state**

  int icnt=0;
  bool bad = false;

  do {				// Not executed on first try
    if (bad) {
      _prior->SamplePrior(ch.Cur());
      ch.Order0();
    }
    
    try {
      _mca->ComputeNewState(this, &ch);
      bad = false;		// Sucessful!!
    }
    catch (ImpossibleStateException &e) {
      if (myid==0)
	cerr << "TemperedSimulation: bad state at top level . . . "
	     << "sampling from prior [try #" << icnt+1 << "]" << endl;
      bad = true;		// Unsucessful :(
    }
    catch (BIEException& e) {
      cout << "Process " << myid 
	   << ": in TemperedSimulation::MCMethod(): "
	   << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	   << e.getErrorMessage();
      throw e;
    }

				// Quit with a good state or too many tries
  } while (bad>0 && ++icnt<ITMAX);

				// Abort if too many tries
  string msg;
  int status = 0;
  if (icnt>=ITMAX) {
    status = 1;
    ostringstream sout;
    sout << "Tried " << ITMAX << " times to find a good initial state." 
	 << endl << "Use global variable ITMAX in class TemperedSimulation "
	 << "to change this limit. " << endl;
    msg = sout.str();
  }
				// Throw exception if any process exceeds the
				// ITMAX limit
  MPI_Allreduce(MPI_IN_PLACE, &status, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  if (status) {			// All process now throw the exception
    BIEException e("TooManyTriesException", msg, __FILE__, __LINE__);
    throw e;
  }

  // **Mix**
  for (int j=0; j<2*Ninter; j++) {
    
    // Step 1: Select trial state based on proposal function
    // Step 2: Compute likelihood and posterior for new state
    // Step 3: Update
    
    int icnt=0;
    bool bad = false;
    do {
      try {
	compute_proposal_state(0);
	bad = false;		// Sucessful!!
      }
      catch (ImpossibleStateException &e) {
#ifdef DEBUG_VERBOSE
	if (myid==0)
	  cerr << "ParallelChains: bad state during cold chain mix" << endl
	       << e.getErrorMessage() << "... continuing" << endl;
#endif
	bad = true;		// Unsucessful :(
      }
      catch (BIEException& e) {
	cout << "Process " << myid 
	     << ": in TemperedSimulation::MCMethod(): "
	     << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
	     << e.getErrorMessage();
	throw e;
      }
    } while (bad && ++icnt<ITMAX);

    if (bad) {
      ostringstream sout;
      sout << "Tried " << ITMAX 
	   << " times to find a good state during cold chain mix." << endl 
	   << "Use global variable ITMAX in class TemperedSimulation "
	   << "to change this limit. " << endl;
      BIEException e("TooManyTriesException", msg, __FILE__, __LINE__);
      throw e;
    }
  }

  if (use_tempering) {
    
    // Step 0: cache original state
    Chain cache0(ch);
	
    // Value at level 0, T=1
    up   += -(*V);
    (*V) *= beta[1]/beta[0];
    up   +=  (*V);
    
    // **Melt**
    for (int k=1; k<Mchains; k++) {
      // Metropolis-Hasting updates at
      // current level
      
      for (int j=0; j<Ninter; j++) {
	
	// Step 1: Select trial state based on proposal function
	// Step 2: Compute likelihood and posterior for new state
	// Step 3: Update
	    
	int icnt=0;
	bool bad = false;
	do {
	  try {
	    compute_proposal_state(k);
	    bad = false;	// Sucessful!!
	  }
	  catch (ImpossibleStateException &e) {
#ifdef DEBUG_VERBOSE
	    if (myid==0)
	      cerr << "TemperedSimulation: bad state during cold chain melt" 
		   << endl << e.getErrorMessage() << "... continuing" << endl;
#endif
	    bad = true;		// Unsucessful :(
	  }
	  catch (BIEException& e) {
	    cout << "Process " << myid 
		 << ": in TemperedSimulation::MCMethod(): "
		 << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
		 << e.getErrorMessage();
	    throw e;
	  }
	} while (bad && ++icnt<ITMAX);

	if (bad) {
	  ostringstream sout;
	  sout << "Tried " << ITMAX 
	       << " times to find a good state during cold chain melt." << endl 
	       << "Use global variable ITMAX in class TemperedSimulation "
	       << "to change this limit. " << endl;
	  BIEException e("TooManyTriesException", msg, __FILE__, __LINE__);
	  throw e;
	}
      }
      
      if (k<Mchains-1) {
	up   += -(*V);
	(*V) *= beta[k+1]/beta[k];
	up   += +(*V);
      }
	  
    }
	
    
    // **Freeze**
    for (int k=Mchains-1; k>0; k--) {
      // Metropolis-Hasting updates at
      // current level
	  
      for (int j=0; j<Ninter; j++) {
	    
	// Step 1: Select trial state based on proposal function
	// Step 2: Compute likelihood and posterior for new state
	// Step 3: Update
	
	int icnt=0;
	bool bad = false;
	do {
	  try {
	    compute_proposal_state(k);
	    bad = false;	// Sucessful!!
	  }
	  catch (ImpossibleStateException &e) {
#ifdef DEBUG_VERBOSE
	    if (myid==0)
	      cerr << "TemperedSimulation: bad state during warm chain freeze" 
		   << endl << e.getErrorMessage() << "... continuing" << endl;
#endif
	    bad = true;		// Unsucessful :(
	  }
	  catch (BIEException& e) {
	    cout << "Process " << myid 
		 << ": in TemperedSimulation::MCMethod(): "
		 << "[File=" << __FILE__ << ", Line=" << __LINE__ << "] "
		 << e.getErrorMessage();
	    throw e;
	  }
	} while (bad && ++icnt<ITMAX);

	if (bad) {
	  ostringstream sout;
	  sout << "Tried " << ITMAX 
	       << " times to find a good state during warm chain freeze." 
	       << endl 
	       << "Use global variable ITMAX in class TemperedSimulation "
	       << "to change this limit. " << endl;
	  BIEException e("TooManyTriesException", msg, __FILE__, __LINE__);
	  throw e;
	}
      }
      
      down += +(*V);
      (*V) *= beta[k-1]/beta[k];
      down += -(*V);
    }
    
    // Tally for average
    upM   += up;
    downM += down;
    updownNum++;
	
    // Step 4: propose to change state

    if (myid==0) {
      double accept_prob = min<double>(1.0, exp(up-down));
    
      // Use (restore) original
      if (accept_prob < (*unit)()) {
	  
	ch = cache0;
	  
      }
      else {			// Accept the new one
	swapnum++;
      }
    }
    
  }

  // Debug barrier . . . 
  {
    ostringstream ostr;
    ostr << "TemperedSimulation::MCMethod: end call #" << callCount;
    (*bwrap)(ostr.str());
    callCount++;
  }

}


vector<double> TemperedSimulation::GetMixstat(void)
{
  vector<double> ret(Mchains, 0.0);
  for (int i=0; i<Mchains; i++) 
    if (mstat0[i]) ret[i] = (double)mstat1[i]/(double)mstat0[i];

  return ret;
}

void TemperedSimulation::PrintStepDiagnostic()
{
  ostream cout(checkTable("simulation_output"));

  // could access all of these state variables directly...

   double up, down;

   GetLastCycle(up, down);

   cout << "           swaps=" << GetSwap() 
        << "[" << GetAcceptance() << "]"
        << "  up=" << up << "  down=" << down
        << "  prob=" << exp(up-down);

   Simulation::PrintStepDiagnostic();
}

void TemperedSimulation::PrintStateDiagnostic()
{
  if (mstat) {
    ostream cout(checkTable("simulation_output"));

    vector<double> ret = GetMixstat();
    cout << endl;
    cout << "  P(accept):";
    for (int ik=0; ik<(int)ret.size(); ik++) cout << " " << ret[ik];
    cout << endl;

    unsigned int min_ret = 2 << 29;
    for (int k=0; k<(int)mstat0.size(); k++) 
      min_ret = min<int>(min_ret, mstat0[k]);

    if (min_ret >= state_diag) {

      int r_good = 0, r_soso = 0, r_vbad = 0;

      cout << endl << setw(20) << "Summary:" << endl;
      
      for (int k=0; k<(int)ret.size(); k++) {
	if (ret[k]>0.2) r_good++;
	else if (ret[k]>0.05) r_soso++;
	else r_vbad++;
      }

      if (r_good>(int)ret.size()/2 && r_vbad<(int)ret.size()/8)
	cout << setw(15) << " " 
	     << "Interlevel swapping is excellent!" << endl;
      else if (r_soso>(int)ret.size()/2)
	cout << setw(15) << " " 
	     << "Interlevel swapping is mediocre" << endl
	     << setw(20) << " "
	     << "==> Consider changing the maximum temperature and/or the proposal width" << endl;
      else 
	cout << setw(15) << " " 
	     << "Interlevel swapping is horrible"<< endl
	     << setw(20) << " " 
	     << "==> Do something! Change the maximum temperature or proposal width" << endl;
      
      cout << endl;
      
    }
				// Reset
    mstat0 = vector<int>(Mchains, 0);
    mstat1 = vector<int>(Mchains, 0);
  }
				// Algorithm specific info; may be blank
  _mca->PrintAlgorithmDiagnostics(this);
}
