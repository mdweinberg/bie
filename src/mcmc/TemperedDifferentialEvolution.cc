#include <TemperedDifferentialEvolution.h>
#include <BIEMutex.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::TemperedDifferentialEvolution)

using namespace BIE;
				// Global parameters
				// -----------------
int TemperedDifferentialEvolution::Ninter      = 10;
int TemperedDifferentialEvolution::minmc       = 4;
// A value of 0.5 for tpow is Gaussian scaling . . .
double TemperedDifferentialEvolution::tpow     = 0.5;

				// Frequency for a tempered step
				// ------------------------------
unsigned TemperedDifferentialEvolution::tfreq  = 11;


void TemperedDifferentialEvolution::Initialize(int minmc_p, double maxT)
{
  algoname = "Tempered Differential Evolution";

  tcount = 1;			// Start counting at one so first step is not
				// tempered

  jcount = 0;			// Gamma counter

  if (minmc_p) minmc = minmc_p;

  MaxT = maxT;

  Nlevels = max<int>(minmc, (int)(log(MaxT)*sqrt( static_cast<float>(_si->Ntot))+1));
  if (myid==0) cout << "TemperedDifferentialEvolution: assigning " << Nlevels
		    << " temperature levels\n";
  beta = vector<double>(Nlevels);
  factor = vector<double>(Nlevels, 1.0);

  if (Nlevels>1) {
    for (int k=0; k<Nlevels; k++) {
      beta[k] = exp(-log(MaxT)*k/(Nlevels-1));
      factor[k] = 1.0/pow(beta[k], tpow);
    }
  }
  
  up   = vector<double>(Mchains, 0);
  down = vector<double>(Mchains, 0);

  //
  // Per level acceptance rates
  //
  mstat0 = vector<unsigned long>(Nlevels);
  mstat1 = vector<unsigned long>(Nlevels);

  stat0T = vector<unsigned long>(Nlevels, 0);
  stat1T = vector<unsigned long>(Nlevels, 0);

  tstat = false;

  curtry = 0;
  curswp = 0;
  tottry = 0;
  totswp = 0;
}


void TemperedDifferentialEvolution::OneDifferentialEvolutionStep()
{
  int ok;
  int R1, R2;
  double logprob;

  //
  // Set default gamma parameter for this step
  //
  if (jcount++ % jfreq) gamma = gamma0;
  else                  gamma = 1.0;

  // ==============================
  // cntrl==parallel
  // ==============================
  if (cntrl == parallel) {  

    // Cycle through all chains
    // ------------------------
    for (int k=0; k<Mchains; k++) {
      
      int trial_ok = 1;	// Set to zero if trial state is out of bounds

      try {

	// Pick two *other* chains at random
	// ---------------------------------
	do {
	  R1 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	} while (R1 == k);
	
	do {
	  R2 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	} while (R2 == k || R1 == R2);


	// Make trial state
	// ----------------
	TrialState(k, R1, R2);
      
      }
      catch (ImpossibleStateException &e) {
#ifdef DEBUG
	  cerr << "DifferentialEvolution: bad state proposed" << endl
	       << e.getErrorMessage() << "... process " << myid 
	       << " continuing" << endl;
#endif
	  trial_ok = 0;
      }

      MPI_Bcast(&trial_ok, 1, MPI_INT, 0, MPI_COMM_WORLD);

      
				// If state is in bounds, do the evaluation
				// ----------------------------------------
      if (trial_ok) {

	if (myid==0) stat0T[pk]++;

	// Assign and send state to everywhere
	// -----------------------------------
	if (myid==0)  chains[k].AssignState1(chainT);
	if (mpi_used) chains[k].BroadcastChain();

	// Evaluate and handle the bad state exceptions
	// --------------------------------------------
	try {
	
	  logprob =
	    _mca->ComputeState(this, &chains[k]) - chains[k].value;

	  if (myid==0) {
	    double accept_prob = min<double>(1.0, exp(logprob));
	  
	    if (accept_prob >= (*unit)()) {
	      
	      chains[k].AcceptState();

	      stat1T[pk]++;
	    }	
	  }	
	  
	}
	catch (ImpossibleStateException &e) {
#ifdef DEBUG
	  cerr << "TemperedDifferentialEvolution: bad state proposed" << endl
	       << e.getErrorMessage() << "... process " << myid
	       << " continuing" << endl;
#endif
	}

      }

    }


    // ==============================
    // cntrl==serial
    // ==============================
  } else { 

    // Clear chain update markers
    // --------------------------
    for (int k=0; k<Mchains; k++) {
      update_flag [k] = 0;
      update_flag1[k] = 0;
    }

    // Cycle through all chains
    // ------------------------
    for (int k=0; k<Mchains; k++) {

      if (myid==chains[k].owner) {

	// Pick two *other* chains at random
	// ---------------------------------
	do {
	  R1 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	} while (R1 == k);
	
	do {
	  R2 = min<int>((int)floor((*unit)()*Mchains), Mchains-1);
	} while (R2 == k || R1 == R2);


				// Evaluate and handle the bad state exceptions
				// --------------------------------------------

	try {			// Make trial state
				// ----------------
	  TrialState(k, R1, R2);

				// Assign
				// ------
	  chains[k].AssignState1(chainT);
	
	  logprob = _mca->ComputeState(this, &chains[k]) - chains[k].value;
	  ok = 1;
	}
	catch (ImpossibleStateException &e) {
	  ok = 0;
	}
	
	if (ok) {
	  double accept_prob = min<double>(1.0, exp(logprob));

	  stat0T[pk]++;
	  
	  if (accept_prob >= (*unit)()) {
	    
	    chains[k].AcceptState();
	    
	    stat1T[pk]++;

	    update_flag1[k] = 1;
	  }
	}
      }
    }

    //
    // Update chains everywhere
    // ------------------------
    if (mpi_used) {
      ++MPIMutex;
      MPI_Allreduce(&update_flag1[0], &update_flag[0], Mchains,
		    MPI_UNSIGNED_CHAR, MPI_SUM, MPI_COMM_WORLD);
      --MPIMutex;
      
      for (int k=0; k<Mchains; k++) {
	if (update_flag[k]) {
	  chains[k].BroadcastChain();
	  MPI_Barrier(MPI_COMM_WORLD);
	}
      }
    }
  }
      
}

void TemperedDifferentialEvolution::MCMethod()
{
  //
  // is it a tempering step?
  //
  if (tcount++ % tfreq) 
    {
      DifferentialEvolution::MCMethod();
      return;
    }

  vector<Chain> cache_chains(chains); // Step 0: cache original state

  for (int m=0; m<Mchains; m++) up[m] = down[m] = 0.0;

  for (int k=0; k<Nlevels; k++) stat0T[k] = stat1T[k] = 0;
  
  tstat = true;

  //
  // Step 1: Differential Evolution step at the base level 
  // -----------------------------------------------------
  // (ech, could be skipped)
  //
  /*
  for (int i=0; i<Ninter; i++)
    OneDifferentialEvolutionStep();
  */

  //
  // Step 2: **Melt**
  // ----------------
  //
  for (int k=0; k<Nlevels-1; k++) {
    
    PowerUP(k);			// Heat chains to next temp level

    pk = k+1;
    for (int i=0; i<Ninter; i++) OneDifferentialEvolutionStep();
  }


  //
  // Step 3: **Freeze**
  // ------------------
  //
  for (int k=Nlevels-1; k>0; k--) {
    //
    // Differential Evoluation updates at
    // current level
    //
    pk = k;
    for (int i=0; i<Ninter; i++) OneDifferentialEvolutionStep();
      
    PowerDOWN(k);		// Cool chains to previous temp level
  }
  
  //
  // Step 4: propose to change state
  // -------------------------------
  //
  for (int m=0; m<Mchains; m++) {
    if (myid==chains[m].owner) {
      double accept_prob = min<double>(1.0, exp(up[m]-down[m]));
      
      // Use (restore) original
      curtry++;
      if (accept_prob < (*unit)()) {
	chains[m] = cache_chains[m];
      }
      else {            // Accept the new one
	curswp++;
      }
    }
  }

  //
  // Set fiducial chain to largest posterior prob
  // --------------------------------------------
  //
  if (myid==0) {
    double maxval = chains[0].value;
    R0 = 0;
    for (int k=1; k<Mchains; k++) {
      if (maxval<chains[k].value) {
	R0 = k;
	maxval = chains[k].value;
      }
    }
    
    chfid = &chains[R0];
  }

  //
  // Temperature statistics (if mstat=1)
  // -----------------------------------
  if (mpi_used && mstat) {
    
    MPI_Reduce(&stat0T[0], &mstat0[0], Nlevels, MPI_UNSIGNED_LONG, MPI_SUM, 0,
	       MPI_COMM_WORLD);

    MPI_Reduce(&stat1T[0], &mstat1[0], Nlevels, MPI_UNSIGNED_LONG, MPI_SUM, 0,
	       MPI_COMM_WORLD);
  }

}

void TemperedDifferentialEvolution::PowerUP(int k)
{
    for (int m=0; m<Mchains; m++) {
      if (myid==chains[m].owner) up[m] -= chains[m].value;
      chains[m].beta   =  beta  [k+1];
      chains[m].factor =  factor[k+1];
      chains[m].value  *= beta[k+1]/beta[k];
      chains[m].value1 *= beta[k+1]/beta[k];
      if (myid==chains[m].owner) up[m] += chains[m].value;
    }
}

void TemperedDifferentialEvolution::PowerDOWN(int k)
{
    for (int m=0; m<Mchains; m++) {
      if (myid==chains[m].owner) down[m] += chains[m].value;
      chains[m].beta   =  beta  [k-1];
      chains[m].factor =  factor[k-1];
      chains[m].value  *= beta[k-1]/beta[k];
      chains[m].value1 *= beta[k-1]/beta[k];
      if (myid==chains[m].owner) down[m] -= chains[m].value;
    }
}


void TemperedDifferentialEvolution::PrintStepDiagnostic()
{
  ostream cout(checkTable("simulation_output"));

  // Specific to Tempered States step
  //
  if (mstat && tstat) {
    cout << endl << " Tempered acceptance rate (per level):" << endl;
    for (int k=0; k<Nlevels; ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	ostringstream out;
	if (k) out << mstat1[k] << "/" << mstat0[k];
	else   out << "XXX";
	cout << " " << setw(9) << out.str();
	if (++k>=Nlevels) break;
      }
      cout << endl;
    }
    cout << endl;
    tstat = false;
  }

  if (curtry) {

    tottry += curtry;
    totswp += curswp;
    cout << "           last accept=" << curswp << "/" << curtry
	 << "  total accept=" << totswp << "/" << tottry
	 << "  [Tempered proposals]" << endl;
    curtry = 0;
    curswp = 0;
  }
  
  // Specific to DE step
  //
  DifferentialEvolution::PrintStepDiagnostic();
}

