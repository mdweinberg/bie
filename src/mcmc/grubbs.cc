#include "OutlierMask.h"

/*
  Grubbs' for outlier detection.

  Grubbs' test (Grubbs 1969 and Stefansky 1972) is used to detect
  outliers in a univariate data set. It is based on the assumption of
  normality.  This should be the case for converged MCMC calc.
*/

bool Grubbs::compute(std::vector< std::vector<double> >& prb,
		     std::vector< std::vector<double> >& z,
		     double alpha, int maxoutlier, 
		     double poffset, bool verbose)
{

  unsigned n = z.size();
  unsigned m = z.front().size();
  int cnt = 0;

  mask = std::vector<unsigned char>(n, 0);
  if (alpha <= 0.0) return true;

  double maxLP = -1.0e20;
  for (unsigned i=0; i<n; i++) maxLP = std::max<double>(maxLP, prb[i][0]);

  //
  // Do each variable separately
  //
  for (unsigned q=0; q<m; q++) {
				// P is the sorted list of (value, index) pairs
    std::vector< std::pair<double, int> > p;
    for (unsigned i=0; i<n; i++) {
      if (mask[i]==0)
	p.push_back(std::pair<double, int>(z[i][q], i));
    }
  
				// Compute the Student statistic for 
				// k-cnt possible outliers
    double val, pcrit, t, G, gmax;
    unsigned sz=p.size();
    unsigned k = sz*3/4;
    if (k<2) break;

    double mean, var;

    for (unsigned j=0; j<k; j++) {
      mean = var = 0.0;		// Compute mean and variance 
				// for the remaining sample
      for (unsigned i=0; i<sz; i++) {
	val = z[p[i].second][q];
	mean += val/sz;
	var  += val*val/(sz-1);
      }
      var -= mean*mean*sz/(sz-1);

      if (var<1.0e-18) break;

      for (unsigned i=0; i<sz; i++) p[i].first = fabs(z[p[i].second][q]-mean);
      sort(p.begin(), p.end());

				// The supremum value
      G = p.back().first / sqrt(var);

				// 2-sided Grubb's test to find outliers
      pcrit = 1.0 - (alpha*0.5/sz);
    
      t = inv_student_t_1sided(pcrit, sz-2);

      gmax = t*(sz-1)/sqrt(((t*t + (sz-2))*sz));

				// Report
      if (verbose) {
	if (G > gmax && prb[p.back().second][0]+poffset<maxLP)
	  std::cout << "***** Outlier #" << cnt+1
		    << " in Chain #" << p.back().second 
		    << ", analysis:" << std::endl
		    << "  " << std::setw(20) << "Variable #"  << " = " << std::setw(15) << q << std::endl
		    << "  " << std::setw(20) << "Sample size" << " = " << std::setw(15) << sz << std::endl
		    << "  " << std::setw(20) << "Minimum"     << " = " << std::setw(15) << p[0].first << std::endl
		    << "  " << std::setw(20) << "Maximum"     << " = " << std::setw(15) << p[sz-1].first << std::endl
		    << "  " << std::setw(20) << "Mean"        << " = " << std::setw(15) << mean << std::endl
		    << "  " << std::setw(20) << "Std dev"     << " = " << std::setw(15) << sqrt(var) << std::endl
		    << "  " << std::setw(20) << "Grubbs test" << " = " << std::setw(15) << G << std::endl
		    << "  " << std::setw(20) << "Lambda"      << " = " << std::setw(15) << gmax << std::endl
		    << "  " << std::setw(20) << "Crit value"  << " = " << std::setw(15) << alpha << std::endl
		    << "  " << std::setw(20) << "logP offset" << " = " << std::setw(15) << prb[p.back().second][0]-maxLP << std::endl
		    << std::endl;
      }
      
				// Record the outlier
      if (G > gmax && prb[p.back().second][0]+poffset<maxLP) {
	mask[p.back().second] = 1;
	p.pop_back();		// and delete it from the list
	cnt++;
      }
      else break;			// No more outliers . . . we're done
    }
  }

  if (cnt > maxoutlier) {
				// Warning for the naive.
				// (If it's good enough for Braak 2006 it's
				//  good enough for me . . .)
    std::cout << "****** Too many aberrant chains!  ******" << std::endl
	      << "****** Will continue sans masking ******" << std::endl
	      << std::setw(72) << std::setfill('-') << "-" << std::endl
	      << std::setfill(' ');

    return false;
  }

				// Set the new mask
  if (verbose) {
    if (cnt==0) std::cout << "No outliers" << std::endl;
    else {
      std::cout << "Outlier mask: ";
      for (unsigned i=0; i<n; i++) 
	if (mask[i]==0) std::cout << std::setw(2) << 0;
	else            std::cout << std::setw(2) << 1;
      std::cout << std::endl;
    }
    std::cout << std::setw(72) << std::setfill('-') << "-" << std::endl
	      << std::setfill(' ');
  }

  return true;
}

bool Grubbs::compute(std::vector< std::vector<double> >& prb,
		     std::vector< std::vector<double> >& z,
		     double alpha, int maxoutlier, 
		     int T, int N1, int N2,
		     double poffset, bool verbose)
{
  unsigned n = z.size();

  mask = std::vector<unsigned char>(n, 0);
  if (alpha <= 0.0) return true;

  int cnt=0;

				// Number of values per chain 
  std::vector<unsigned> Nparam(2), Nsteps(2, 0);
  Nparam[0] = T + N1;
  Nparam[1] = T + N2;

				// Make the parameter map for the two
				// models
  std::vector< std::vector<unsigned> > MAP(2);
  for (unsigned m=0; m<2; m++) {
    for (int i=0; i<T; i++) MAP[m].push_back(1 + i);
  }
  for (int i=0; i<N1; i++) MAP[0].push_back(1 + T + i);
  for (int i=0; i<N2; i++) MAP[1].push_back(1 + T + N1 + i);


				// Compute the maximum posterior
				// probability for computing the final
				// rejection offset criterion

  double maxLP = -1.0e20;
  for (unsigned i=0; i<n; i++) maxLP = std::max<double>(maxLP, prb[i][0]);

  //
  // Do each variable separately
  //
				// Model loop
  for (unsigned m=0; m<2; m++) {
				// Variable loop
    for (unsigned q=0; q<Nparam[m]; q++) {
				// P is the sorted list of (value, index) pairs
      std::vector< std::pair<double, int> > p;
      for (unsigned i=0; i<n; i++) {
	if (mask[i]==0)
	  p.push_back(std::pair<double, int>(z[i][MAP[m][q]], i));
      }
  
				// Compute the Student statistic for 
				// k-cnt possible outliers
      double val, pcrit, t, G, gmax;
      unsigned sz=p.size();
      unsigned k = sz*3/4;
      if (k<2) break;

      double mean, var;

      for (unsigned j=0; j<k; j++) {
	mean = var = 0.0;	// Compute mean and variance 
				// for the remaining sample
	for (unsigned i=0; i<sz; i++) {
	  val = z[p[i].second][MAP[m][q]];
	  mean += val/sz;
	  var  += val*val/(sz-1);
	}
	var -= mean*mean*sz/(sz-1);
	
	if (var<1.0e-18) break;
	
	for (unsigned i=0; i<sz; i++) 
	  p[i].first = fabs(z[p[i].second][MAP[m][q]]-mean);
	sort(p.begin(), p.end());
	
				// The supremum value
	G = p.back().first / sqrt(var);

				// 2-sided Grubb's test to find outliers
	pcrit = 1.0 - (alpha*0.5/sz);
    
	t = inv_student_t_1sided(pcrit, sz-2);

	gmax = t*(sz-1)/sqrt(((t*t + (sz-2))*sz));

				// Report
	if (verbose) {
	  if (G > gmax && prb[p.back().second][0]+poffset<maxLP)
	    std::cout << "***** Outlier #" << cnt+1
		 << " in Chain #" << p.back().second 
		 << ", analysis:" << std::endl
		 << "  " << std::setw(20) << "Variable #"  << " = " << std::setw(15) << q << std::endl
		 << "  " << std::setw(20) << "Sample size" << " = " << std::setw(15) << sz << std::endl
		 << "  " << std::setw(20) << "Minimum"     << " = " << std::setw(15) << p[0].first << std::endl
		 << "  " << std::setw(20) << "Maximum"     << " = " << std::setw(15) << p[sz-1].first << std::endl
		 << "  " << std::setw(20) << "Mean"        << " = " << std::setw(15) << mean << std::endl
		 << "  " << std::setw(20) << "Std dev"     << " = " << std::setw(15) << sqrt(var) << std::endl
		 << "  " << std::setw(20) << "Grubbs test" << " = " << std::setw(15) << G << std::endl
		 << "  " << std::setw(20) << "Lambda"      << " = " << std::setw(15) << gmax << std::endl
		 << "  " << std::setw(20) << "Crit value"  << " = " << std::setw(15) << alpha << std::endl
		 << "  " << std::setw(20) << "logP offset" << " = " << std::setw(15) << prb[p.back().second][0]-maxLP << std::endl
		 << std::endl;
	}
      
				// Record the outlier
	if (G > gmax && prb[p.back().second][0]+poffset<maxLP) {
	  mask[p.back().second] = 1;
	  p.pop_back();		// and delete it from the list
	  cnt++;
	}
	else break;			// No more outliers . . . we're done
      }
    }
  }

  if (cnt > maxoutlier) {
				// Warning for the naive.
				// (If it's good enough for Braak 2006 it's
				//  good enough for me . . .)
    std::cout << "****** Too many aberrant chains!  ******" << std::endl
	<< "****** Will continue sans masking ******" << std::endl
	      << std::setw(72) << std::setfill('-') << "-" << std::endl
	      << std::setfill(' ');

    return false;
  }

				// Set the new mask
  if (verbose) {
    if (cnt==0) std::cout << "No outliers" << std::endl;
    else {
      std::cout << "Outlier mask: ";
      for (unsigned i=0; i<n; i++) 
	if (mask[i]==0) std::cout << std::setw(2) << 0;
	else            std::cout << std::setw(2) << 1;
      std::cout << std::endl;
    }
    std::cout << std::setw(72) << std::setfill('-') << "-" << std::endl
	      << std::setfill(' ');
  }
  
  return true;
}
