// This may look like C code, but it is really -*- C++ -*-

#include <sstream>
using namespace std;

#include <LikelihoodComputation.h>
#include <MetropolisHastings.h>
#include <gfunction.h>
#include <MixturePrior.h>
#include <BIEconfig.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MetropolisHastings)

using namespace BIE;

bool MetropolisHastings::Sweep(Simulation* s, Chain* ch)
{
  bool accepted = false;

  // Step 1: Select trial state based on proposal function
  // Step 2: Compute likelihood and posterior for new state
  
  ComputeState(s, ch);


  // Step 3: Update
                                // Metropolis-Hastings becomes
                                // Metropolis for Gaussian proposal
                                // distribution

  if (myid==0 || s->Serial()) {

    double accept_prob = min<double>(1.0, exp(ch->value1 - ch->value));

    ch->stat[0]++;
    if (accept_prob >= (*unit)()) {
      ch->AcceptState();
      ch->stat[1]++;
      accepted = true;
    }
  }

  return accepted;
}

