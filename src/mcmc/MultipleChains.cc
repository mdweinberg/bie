
#include <gfunction.h>
#include <MultipleChains.h>
#include <Prior.h>
#include <BIEconfig.h>
#include <BIEMutex.h>
#include <Cauchy.h>
#include <FileExists.H>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MultipleChains)

using namespace BIE;
				// Global parameters
				// -----------------

MultipleChains::Control MultipleChains::cntrl = parallel;

				// Number of steps in rate queue
				// -----------------------------
unsigned MultipleChains::nfifo          = 100;

				// Number of iterations to find a good state
				// -----------------------------------------
int MultipleChains::state_iter          = 1000;


void MultipleChains::initialize(int mchains)
{
  algoname = "Multiple Chains";

  Mchains = mchains;

  ncount = 0;			// Number of steps so far
  totalnum = 0;			// Number of accepted states
  totaltry = 0;			// Number of attempted states

  chains_initialized = false;

  CreateChains();
}

MultipleChains::~MultipleChains()
{
  delete width;
}


void MultipleChains::CreateChains()

{
				// Erase all previous chains
  chains.clear();

  update_flag .clear();
  update_flag1.clear();

  stat0 .clear();
  stat1 .clear();
  nstat0.clear();
  nstat1.clear();
  
				// Create and initialize new chains
  for (int k=0; k<Mchains; k++) {
    chains.push_back(Chain(_si));

    update_flag .push_back(0);
    update_flag1.push_back(0);

    stat0.push_back(0);
    stat1.push_back(0);

    nstat0.push_back(0);
    nstat1.push_back(0);
    
				// Assign ownership
    if (cntrl == serial) {
      chains[k].owner = k % numprocs;
    }

    if (cntrl == parallel) {
      chains[k].owner = 0;
    }

				// Logging?
    if (caching) {
      if ( chains[k].owner == myid ) {
	ostringstream name;
	name << nametag << ".chainlog." << k;
	chains[k].Logging(name.str());
      }
    }
  }

				// For reporting . . .
  chfid = &chains[0];
}

void MultipleChains::ResetChainStats()
{
  if (mpi_used && myid) return;

  vector<Chain>::iterator j;
  for (int k=0; k<Mchains; k++) {
    nstat0[k] = 0;
    nstat1[k] = 0;
  }
  arate.erase(arate.begin(), arate.end());
}


void MultipleChains::NewState(int M, vector<double>& wght, 
			      vector< vector<double> >& phi)
{
  chains_initialized = false;
  Initialize();
}

void MultipleChains::NewState(Chain &ch)
{ 
  chains_initialized = false;
  Initialize();
}


void MultipleChains::NewState(State& s) 
{ 
  chains_initialized = false;
  Initialize();
}


void MultipleChains::Initialize()
{
  if (chains_initialized) return;

  static bool once = true;

  if (once && restart) {

    once = false;		// Restart initialization 
				// can only performed ONCE

    vector< vector<double> > values;
    vector<State>  states;
    int nsiz, ndim;

				// Only the root node has the
				// convergence info
    if (myid==0) {
      if (!_conv->GetLast(values, states)) {
	ostringstream out;
	out << "error retrieving last state from Converge" << cntrl;
      
	throw InternalError(-66, out.str(), __FILE__, __LINE__);
      }

      nsiz = values.size();
      ndim = states[0].size();

      if (_si->ptype == StateInfo::Mixture ||
	  _si->ptype == StateInfo::Extended ) {
				// Test weight norm (sanity check)
	bool badnorm = false;
	for (int i=0; i<nsiz; i++) {
	  double sum=0.0;
	  for (unsigned j=0; j<states[i].M(); j++) sum += states[i].Wght(j);
	  if ( fabs(sum-1.0) > 0.001 ) badnorm = true;
	  else {
	    for (unsigned j=0; j<states[i].M(); j++) states[i].Wght(j) /= sum;
	  }
	}


	if (badnorm) {
	  ostringstream out;
	  out << "error in state normalizations" << cntrl;
      
	  throw InternalError(-67, out.str(), __FILE__, __LINE__);
	}
      }

    }

    //
    // Send the state to all nodes
    //
    MPI_Bcast(&nsiz, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&ndim, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (myid) {
      values = vector< vector<double> >(nsiz);
      for (int i=0; i<nsiz; i++) values[i] = vector<double>(3);
    }

    for (int i=0; i<nsiz; i++)
      MPI_Bcast(&values[i], 3, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    if (myid) {
      State s(_si, vector<double>(ndim));
      for (int i=0; i<nsiz; i++) {
	MPI_Bcast(&s[0], ndim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	states.push_back(s);
      }
    } else {
      for (int i=0; i<nsiz; i++)
	MPI_Bcast(&states[i][0], ndim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }	


    //
    // Assign the states . . .
    //

    for (int k=0; k<Mchains; k++) chains[k].AssignState0(states[k]);

    //
    // Compute the states . . .
    //

    switch(cntrl) {

    case parallel:		// Root initiates assignement
				// but likelihood is computed in parallel

      for (int k=0; k<Mchains; k++) {

	try {
	  _mca->ComputeNewState(this, &chains[k]);
	}
	catch (ImpossibleStateException &e) {
	  ostringstream out;
	  out << "Process #" << myid
	      << ", restored state is bad! [parallel]";

	  throw InternalError(-68, out.str(), __FILE__, __LINE__);
	}

      }

      break;
      
    case serial:

				// Owner initiates assignement and
				// computes likelihood
      for (int k=0; k<Mchains; k++) {

	if (myid==chains[k].owner) {

	  try {
	    _mca->ComputeNewState(this, &chains[k]);
	  }
	  catch (ImpossibleStateException &e) {
	    ostringstream out;
	    out << "Process #" << myid
		<< ", restored state is bad! [serial]";

	    throw InternalError(-69, out.str(), __FILE__, __LINE__);
	  }
	}
      }
      
				// Share the newly initialized chains with
      if (mpi_used)		// all procs
	for (int k=0; k<Mchains; k++) chains[k].BroadcastChain();
      
      break;

    default:
      ostringstream out;
      out << "We don't know about the control type #" << cntrl;
      
      throw InternalError(out.str(), __FILE__, __LINE__);
    }

  } else {

    switch(cntrl) {

    case parallel:		// Root initiates assignment
				// but likelihood is computed in parallel
      for (int k=0; k<Mchains; k++) SampleNewStateParallel(k);
      break;
      
    case serial:

				// Owner initiates assignement and
				// computes likelihood
      for (int k=0; k<Mchains; k++) SampleNewStateSerial(k);
    
				// Share the newly initialized chains with
      if (mpi_used)		// all procs
	for (int k=1; k<Mchains; k++) chains[k].BroadcastChain();
      
      break;

    default:
      ostringstream out;
      out << "We don't know about the control type #" << cntrl;
      
      throw InternalError(out.str(), __FILE__, __LINE__);
    }

  }
  
  chains_initialized = true;

#ifdef DEBUG2
  ostringstream sout;
  sout << "debug_chain_init." << myid;
  ofstream out(sout.str().c_str());
  for (int k=0; k<Mchains; k++) {
    out << endl << "Chain " << k << endl;
    chains[k].PrintStats(out);
    chains[k].PrintState(out);
    chains[k].PrintState1(out);
  }
#endif


}

void MultipleChains::Reinitialize(MHWidth* width, Prior *mp)
{

  // reset state with new info
  //
  Simulation::Reinitialize(width, mp);

  // don't reset minmc because it shouldn't be changing between levels
  // don't reset Mchains because it shouldn't be changing size
  // don't reset beta because it is although initialized the values do
  // not change and it does not change size
  
  chains_initialized = false;
  Initialize();
  ResetChainStats();
    
  totalnum = 0;
  totaltry = 0;
  ncount = 0;
}


void MultipleChains::SampleNewStateParallel(int k)
{  
  const string member_name = "MultipleChains::SampleNewStateParallel: ";

  int niter = 0;
  bool bad = true;

  while (bad) {

    try {
      if (myid==0) _prior->SamplePrior(chains[k].Cur());
      _mca->ComputeNewState(this, &chains[k]);
      bad = false;
    }
    catch (ImpossibleStateException &e) {
      niter++;
      if (niter > state_iter) {
	ostringstream out;
	out << member_name
	    << "failed to produce a good initial state after "
	    << niter << " attempts.";

	throw InternalError(-70, out.str(), __FILE__, __LINE__);
      }
    }
  }
  
  if (mpi_used) chains[k].BroadcastChain();

}


void MultipleChains::SampleNewStateSerial(int k)
{  
  const string member_name = "MultipleChains::SampleNewStateSerial: ";

  int fail = 0;
  string fail_message;

  if (myid==chains[k].owner) {
  
    int niter = 0;
    bool bad = true;
    
    while (bad) {

      try {
	_prior->SamplePrior(chains[k].Cur());
	_mca->ComputeNewState(this, &chains[k]);
	bad = false;
      }
      catch (ImpossibleStateException &e) {
	niter++;
	if (niter > state_iter) {
	  ostringstream out;
	  out << member_name
	      << "failed to produce a good initial state after "
	      << niter << " attempts";

	  fail = 1;
	  fail_message = out.str();
	}
      }
    }
    
  }

  if (mpi_used) {

    MPI_Bcast(&fail, 1, MPI_INT, chains[k].owner, MPI_COMM_WORLD);

    if (fail) {
      int ssiz = fail_message.size() + 1;
      char *schar = new char [ssiz];
      strncpy(schar, fail_message.c_str(), ssiz);

      MPI_Bcast(&ssiz, 1, MPI_INT, chains[k].owner, MPI_COMM_WORLD);
      MPI_Bcast(schar, ssiz, MPI_CHAR, chains[k].owner, MPI_COMM_WORLD);

      fail_message = string(schar);
      delete [] schar;
    }
  }

  if (fail) 
    throw InternalError(-71, fail_message, __FILE__, __LINE__);

}

void MultipleChains::MCMethod()
{
  State state1;

  vector<unsigned char> brate(Mchains, 0);

  ncount++;
  
				// ==============================
  if (cntrl == parallel) {	// cntrl==parallel
				// ==============================

				// Cycle through all chains
				// ------------------------
    for (int k=0; k<Mchains; k++) {

				// Propose a state
				// ---------------
      if (myid==0) _prior->SampleProposal(chains[k], _mhwidth);

				// Assign and send state to everywhere
				// -----------------------------------
      if (mpi_used) chains[k].BroadcastChain();

				// Evaluate and handle the bad state exceptions
				// --------------------------------------------
      nstat0[k]++;
      totaltry++;

      try {
	
	if (_mca->Sweep(this, &chains[k])) {
	  chains[k].Order0();

				// Keep tally of accepted states
				// -----------------------------
	  nstat1[k]++;
	  brate[k] = 1;
	  totalnum++;
	}

      }
      catch (ImpossibleStateException &e) {
#ifdef DEBUG
	cerr << "MultipleChains: bad state proposed" << endl
	     << e.getErrorMessage() << "... process " << myid 
	     << " continuing" << endl;
#endif
      }

      // Record the state (diagnostic)
      if (myid==chains[k].owner) chains[k].WriteLog();
    }

  }


				// ==============================
  if (cntrl == serial) {	// cntrl==serial
				// ==============================

    for (int k=0; k<Mchains; k++) {
      update_flag [k] = 0;
      update_flag1[k] = 0;
    }
				// Cycle through all chains
				// ------------------------
    for (int k=0; k<Mchains; k++) {

      if (myid==chains[k].owner) {

				// Propose a state
				// ---------------
	_prior->SampleProposal(chains[k], _mhwidth);


				// Evaluate and handle the bad state exceptions
				// --------------------------------------------
	try {
	
	  if (_mca->Sweep(this, &chains[k])) {
	    chains[k].Order0();
	    update_flag1[k] = 1;
	  }

	}
	catch (ImpossibleStateException &e) {
#ifdef DEBUG
	  cerr << "MultipleChains: bad state proposed" << endl
	       << e.getErrorMessage() << "... process " << myid 
	       << ", continuing" << endl;
#endif
	}
	
				// Owner records state
				// -------------------
	chains[k].WriteLog();
      }

    }

				// Update chains everywhere
				// ------------------------
    if (mpi_used) {
      ++MPIMutex;
      MPI_Allreduce(&update_flag1[0], &update_flag[0], Mchains, 
		    MPI_UNSIGNED_CHAR, MPI_SUM, MPI_COMM_WORLD);
      --MPIMutex;
	
      for (int k=0; k<Mchains; k++) {
	if (update_flag[k]) chains[k].BroadcastChain();
      }
    }

    if (myid==0) {
      for (int k=0; k<Mchains; k++) {
	nstat0[k]++;
	totaltry++;
	if (update_flag[k]) {
	  nstat1[k]++;
	  brate[k] = 1;
	  totalnum++;
	}
      }
    }
  }

				// Gather end-of-step diagnostics
				// ------------------------------
  arate.push_back(brate);
  if (arate.size()>nfifo) arate.pop_front();
  getChainCounts();

}


void MultipleChains::getChainCounts()
{
  if (!mstat) return;

  if (cntrl == serial) {

    vector<int> stat0T(Mchains, 0);
    vector<int> stat1T(Mchains, 0);
    
    for (int k=0; k<Mchains; k++) {
      if (chains[k].owner == myid) {
	stat0T[k] = chains[k].stat[0];
	stat1T[k] = chains[k].stat[1];
      }
    }
    
    MPI_Reduce(&stat0T[0], &stat0[0], Mchains, MPI_INT, MPI_SUM, 
	       0, MPI_COMM_WORLD);

    MPI_Reduce(&stat1T[0], &stat1[0], Mchains, MPI_INT, MPI_SUM, 
	       0, MPI_COMM_WORLD);

  }

  if (cntrl == parallel) {

    for (int k=0; k<Mchains; k++) {
      if (chains[k].owner == myid) {
	stat0[k] = chains[k].stat[0];
	stat1[k] = chains[k].stat[1];
      }
    }

  }

}

vector<double> MultipleChains::GetStat(void)
{
  vector<double> ret;

  if (mpi_used && myid) return ret;

  unsigned arsiz = arate.size();

  if (arsiz==0) {
    ret = vector<double>(Mchains, 0.0);
    return ret;
  }
    
  unsigned cnt;

  for (int k=0; k<Mchains; k++) {
    cnt = 0;
    for (unsigned i=0; i<arsiz; i++) cnt += arate[i][k];
    ret.push_back((double)cnt/arsiz);
  }

  return ret;
}

void MultipleChains::PrintStepDiagnostic()
{
  ostream cout(checkTable("simulation_output"));

  // could access all of these state variables directly...
  //
  cout << "           accept=" << totalnum << "/" << totaltry
       << "  [" << algoname << "]" << endl;
  
}

void MultipleChains::PrintStateDiagnostic()
{
  if (mstat) {
    ostream cout(checkTable("simulation_output"));

    cout << endl << setw(20) << " Acceptance rate (per chain):" << endl;
    for (int k=0; k<(int)stat0.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	ostringstream out;
	out << stat1[k] << "/" << stat0[k];
	cout << " " << setw(9) << out.str();
	if (++k>=(int)stat0.size()) break;
      }
      cout << endl;
    }

    vector<double> ret = GetStat();

    cout << endl << setw(20) << " Sweep rate (per chain):" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	ostringstream out;
	out << nstat1[k] << "/" << nstat0[k];
	cout << " " << setw(9) << out.str();
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }

    cout << endl << setw(20) << " Rate (averaged over last " << arate.size() 
	 << " steps):" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	cout << " " << setw(9) << setprecision(3) << ret[k];
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }

    cout << endl << setw(20) << " Posterior values (per chain):" << endl;
    for (int k=0; k<(int)ret.size(); ) {
      cout << setw(20) << k << ":";
      for (int q=0; q<5; q++) {
	cout << " " << setw(9) << setprecision(2) << scientific
	     << chains[k].value;
	if (++k>=(int)ret.size()) break;
      }
      cout << endl;
    }
    cout << endl;

    if (ncount > state_diag) {

      int r_good = 0, r_soso = 0, r_vbad = 0;

      cout << endl << setw(20) << "Summary:" << endl;

      for (int k=0; k<(int)ret.size(); k++) {
	if (ret[k]>0.1)       r_good++;
	else if (ret[k]>0.02) r_soso++;
	else                  r_vbad++;
      }

      if (r_good>(int)ret.size()/2 && r_vbad<(int)ret.size()/8)
	cout << setw(20) << " " 
	     << "Acceptance rate is excellent!" << endl;
      else if (r_soso>(int)ret.size()/2)
	cout << setw(20) << " " 
	     << "Acceptance rate is mediocre but ok" << endl;
      else 
	cout << setw(20) << " " 
	     << "Acceptance rate is horrible" << endl
	     << setw(25) << " "
	     << "==> Do something!" << endl
	     << setw(25) << " "
	     << "    Increase chain # or adjust the sampling distributions!" << endl;
    }

  }

  // Algorithm specific info; may be blank
  //
  _mca->PrintAlgorithmDiagnostics(this);
}


void MultipleChains::SetControl(int m)
{
  switch(m) {
  case parallel:
    cntrl = parallel;
    break;
  case serial:
    if (mpi_used)
      cntrl = serial;
    else {
      cout << "MultipleChains: you want _serial_ control but you must"   << endl
	   << "                use MPI for that to make any sense.  You" << endl
	   << "                are getting _parallel_ control."          << endl;
    }
    break;
  default:
    ostringstream out;
    out << "Control method should be <parallel> (" << parallel
	<< ") or <serial> (" << serial << ")."
	<< "  input value was " << m;
    throw InternalError(out.str(), __FILE__, __LINE__);
  }

  CreateChains();
}

double MultipleChains::GetValue() 
{ 
  Initialize(); 
  return chains[0].value; 
}
    
double MultipleChains::GetPrior() 
{
  Initialize(); 
  return chains[0].prior_value; 
}
    
double MultipleChains::GetLikelihood() 
{ 
  Initialize(); 
  return chains[0].like_value; 
}

State MultipleChains::GetState() 
{ 
  Initialize(); 
  return chains[0].GetState0(); 
}
    
void MultipleChains::ReportState() 
{ 
  if (mpi_used && myid && cntrl==parallel) return;
  Simulation::ReportState(chains[0].GetState0());
}

void MultipleChains::AdditionalInfo()
{
  if (myid) return;

  cout << "Chain report" << endl << left
       << setw(6)  << "#"
       << setw(6)  << "indx"
       << setw(18) << "Beta"
       << setw(6)  << "Mcur"
       << setw(18) << "Value"
       << setw(6)  << "Mcur1"
       << setw(18) << "Value1"
       << endl;

  for (int i=0; i<Mchains; i++) {
    cout << left
	 << setw(6)  << i 
	 << setw(6)  << chains[i].indx
	 << setw(18) << chains[i].beta
	 << setw(6)  << chains[i].M0()
	 << setw(18) << chains[i].value
	 << setw(6)  << chains[i].M1()
	 << setw(18) << chains[i].value1
	 << endl;
  }

}

void MultipleChains::LogState(string& outfile)
{
  ostream cout(checkTable("cli_console"));

  State currentState = chfid->GetState0();

                                // Try only if outfile is not null
  if (outfile.size()>0) {
    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile <<
        "> for append\n";
      return;
    }

    if (first_log) {
      char q = '"';

      logf << setw(20) << "\"Probability\"";
      logf << setw(20) << "\"Likelihood\"";
      logf << setw(20) << "\"Prior\"";

      if (currentState.Type() == StateInfo::Mixture ||
	  currentState.Type() == StateInfo::Extended )
	logf << setw(10) << "\"Number\"";

      for (unsigned k=0; k<currentState.N(); k++) {
	ostringstream ostr;
	ostr << q << _si->ParameterDescription(k);
        logf << setw(20) << ostr.str();
      }
      logf << endl;
      first_log = false;
    }

    for (int k=0; k<Mchains; k++) {
      logf << setw(20) << setprecision(12) << chains[k].value
	   << setw(20) << setprecision(12) << chains[k].like_value
	   << setw(20) << setprecision(12) << chains[k].prior_value;

      if (currentState.Type() == StateInfo::Mixture ||
	  currentState.Type() == StateInfo::Extended )
	logf << setw(10) << chains[k].N0();
      
      for (unsigned j=0; j<chains[k].N0(); j++)
	logf << setw(20) << setprecision(12) << chains[k].Vec0(j);
      for (unsigned j=chains[k].N0(); j<_si->Ntot; j++)
	logf << setw(20) << 0.0;
      
      logf << endl;
    }
  }
}


void MultipleChains::LogState(int level, int iterno, string& outfile)
{
  ostream cout(checkTable("cli_console"));
				// Try only if outfile is not null
  if (outfile.size()>0) {

				// Check for existence of output file 
				// before creating
    static bool firstime = true;
    bool exists = true;
    if (firstime) {
      exists = FileExists(outfile);
      firstime = false;
    }

    State currentState = chfid->GetState0();

    ofstream logf(outfile.c_str(), ios::out | ios::app);
    if (!logf) {
      cout << "Simulation::LogState: can't open <" << outfile << "> for append\n";
      return;
    }
				// Label fields first time through . . .
				// Labels are in " " (so that embedded 
				// spaces are ok)
    if (exists == false) {
      char q = '"';

      logf << setw(7)  << "\"Level\"" 
	   << setw(10) << "\"Iter\""
	   << setw(20) << "\"Probability\""
	   << setw(20) << "\"Likelihood\""
	   << setw(20) << "\"Prior\"";

      if (_si->ptype != StateInfo::None  ||
	  _si->ptype != StateInfo::Block )
	logf << setw(10) << "\"Number\"";

      for (unsigned j=0; j<_si->Ntot; j++) {
	ostringstream ostr;
	ostr << q << _si->ParameterDescription(j) << q;
	logf << setw(20) << ostr.str();
      }
      logf << endl;
    }

    for (int k=0; k<Mchains; k++) {

      unsigned Ntot = chains[k].N0();

				// Write state
      logf << setw(7) << level << setw(10) << iterno;
      logf << setw(20) << setprecision(12) << chains[k].value 
	   << setw(20) << setprecision(12) << chains[k].like_value
	   << setw(20) << setprecision(12) << chains[k].prior_value;

      if (_si->ptype != StateInfo::None  ||
	  _si->ptype != StateInfo::Block )
	logf << setw(10) << chains[k].M0();

      for (unsigned i=0; i<Ntot; i++)
	logf << setw(20) << setprecision(12) << chains[k].Vec0(i);
      for (unsigned i=Ntot; i<_si->Ntot; i++)
	logf << setw(20) << 0.0;

      logf << endl;
    }
  }
}

