#include <iostream>
#include <fstream>

using namespace std;

#include <BIEconfig.h>
#include <ACG.h>
#include <Uniform.h>
#include <Normal.h>
#include <Dirichlet.h>
#include <Poisson.h>
#include <BIEException.h>
#include <TransitionProb.h>
#include <UniformAdd.h>
#include <UniformMult.h>
#include <NormalAdd.h>
#include <NormalMult.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::TransitionProb)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::UniformAdd)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::UniformMult)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::NormalAdd)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::NormalMult)

using namespace BIE;

TransitionProb::TransitionProb()
{
  dim     = 1;
  pmean   = 0.0;

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();
}

TransitionProb::TransitionProb(int N)
{
  dim     = N;
  pmean   = 0.0;

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();
}

