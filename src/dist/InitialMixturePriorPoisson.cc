#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include <InitialMixturePriorPoisson.h>
#include <Dirichlet.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::InitialMixturePriorPoisson)

using namespace BIE;

InitialMixturePriorPoisson::InitialMixturePriorPoisson()
{
  unit = 0;
}


InitialMixturePriorPoisson::
InitialMixturePriorPoisson(StateInfo *si, double lambda, double alpha, 
			   clivectordist *vdist) : 
  InitialMixturePrior(si, alpha, vdist)
{
  Lambda = lambda;		// Poisson prior

  double cum = 0.0;
  Poisson = vector<double>(_si->M+1);
  for (unsigned k=0; k<=_si->M; k++) {
    Poisson[k] = exp(-Lambda-lgamma(2.0+k))*pow(Lambda, 1.0+k) + cum;
    cum = Poisson[k];
  }
  for (unsigned k=0; k<=_si->M; k++) Poisson[k] /= cum;

  unit = new Uniform(0.0, 1.0, BIEgen);
}

InitialMixturePriorPoisson::
InitialMixturePriorPoisson(StateInfo *si, double lambda, double alpha, 
			   std::vector<Distribution*> vdist) : 
  InitialMixturePrior(si, alpha, vdist)
{
  Lambda = lambda;		// Poisson prior

  double cum = 0.0;
  Poisson = vector<double>(_si->M+1);
  for (unsigned k=0; k<=_si->M; k++) {
    Poisson[k] = exp(-Lambda-lgamma(2.0+k))*pow(Lambda, 1.0+k) + cum;
    cum = Poisson[k];
  }
  for (unsigned k=0; k<=_si->M; k++) Poisson[k] /= cum;

  unit = new Uniform(0.0, 1.0, BIEgen);
}

InitialMixturePriorPoisson::~InitialMixturePriorPoisson()
{
  delete unit;
}

// Table lookup
int InitialMixturePriorPoisson::GenPoisson()
{
  double R = (*unit)();

  int lo=0, hi=_si->M, cur;
  while (hi - lo > 1) 
    {
      cur = (lo + hi)/2;

      if (Poisson[cur] < R) lo = cur;
      else                  hi = cur;
    }
  
  return lo+1;
}


void InitialMixturePriorPoisson::SamplePrior(unsigned& M, 
					     vector<double>& wght, 
					     vector< vector<double> >& phi)
{
  M = min<int>(GenPoisson(), _si->M );
  vector<double> pp = mix[M-1]->Sample();
  double sum = 0.0;
  for (unsigned i=0; i<M; i++) {
    wght[i] = pp[i];
    sum += wght[i];
    for (unsigned j=0; j<_si->Ndim; j++) phi[i][j] = _dist[j]->Sample()[0];
  }
  for (unsigned i=0; i<M; i++) wght[i] /= sum;
}

void InitialMixturePriorPoisson::SamplePrior(unsigned M, vector<double>& V)
{
  V.erase(V.begin(), V.end());
  for (unsigned j=0; j<_si->Ndim; j++) V.push_back( _dist[j]->Sample()[0] );
}

InitialMixturePriorPoisson* InitialMixturePriorPoisson::New()
{
  InitialMixturePriorPoisson* p = 
    (InitialMixturePriorPoisson*)this->Prior::New();
  p->_si = _si;
  p->Alpha = Alpha;
  p->Lambda = Lambda;
  p->Poisson = Poisson;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha);
    p->mix.push_back(DirichletPtr(new Dirichlet(&A)));
  }

  return p;
};
    
