#include <gvariable.h>
#include <Dirichlet.h>
#include <BIEException.h>
#include <Gamma.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Dirichlet)

using namespace BIE;
  
Dirichlet::Dirichlet()
{
  A = vector<double>(1, 1.0);
  n = 1;
  sumA = 0.0;
  pdf_fac = 0.0;
  for (int i=0; i<n; i++) {
    if (A[i]<0.0) A[i] = 0.0;
    sumA += A[i];
    pdf_fac -= lgamma(A[i]);
  }
  if (sumA<=0.0) {
    throw DirichletValueException(__FILE__, __LINE__);
  }
  varA = 1.0/( sumA*sumA*(1.0 + sumA) );
  pdf_fac += lgamma(sumA);
  pdf_fac = exp(pdf_fac);
  
  for (int i=0; i<n; i++)
    { gengam.push_back(GammaPtr(new Gamma(A[i], BIEgen))); }
}

Dirichlet::Dirichlet(clivectord* a)
{
  A = (*a)();
  n = (int)A.size();
  sumA = 0.0;
  pdf_fac = 0.0;
  for (int i=0; i<n; i++) {
    if (A[i]<0.0) A[i] = 0.0;
    sumA += A[i];
    pdf_fac -= lgamma(A[i]);
  }
  if (sumA<=0.0) {
    throw DirichletValueException(__FILE__, __LINE__);
  }
  varA = 1.0/( sumA*sumA*(1.0 + sumA) );
  pdf_fac += lgamma(sumA);
  pdf_fac = exp(pdf_fac);
  
  for (int i=0; i<n; i++)
    { gengam.push_back(GammaPtr(new Gamma(A[i], BIEgen))); }
}

Dirichlet::Dirichlet(vector<double>* a)
{
  A = *a;
  n = (int)a->size();
  sumA = 0.0;
  pdf_fac = 0.0;
  for (int i=0; i<n; i++) {
    if (A[i]<0.0) A[i] = 0.0;
    sumA += A[i];
    pdf_fac -= lgamma(A[i]);
  }
  if (sumA<=0.0) {
    throw DirichletValueException(__FILE__, __LINE__);
  }
  varA = 1.0/( sumA*sumA*(1.0 + sumA) );
  pdf_fac += lgamma(sumA);
  pdf_fac = exp(pdf_fac);
  
  for (int i=0; i<n; i++)
    { gengam.push_back(GammaPtr(new Gamma(A[i], BIEgen))); }
}

double Dirichlet::PDF(State& x)
{
  if (n==1) return 1.0;

  double norm = 0.0;
  for (int i=0; i<n; i++) norm += x[i];
  if (fabs(norm-1.0) > 1.0e-3) {
    throw DirichletSumException(norm, __FILE__, __LINE__);
  }
  double ans = pdf_fac;
  for (int i=0; i<n; i++) ans *= pow(x[i]/norm, A[i] - 1.0);
  return ans;
}

double Dirichlet::logPDF(State& x)
{
  if (n==1) return 0.0;

  double z = PDF(x);
  if (z<=0.0)
    throw ImpossibleStateException(__FILE__, __LINE__);
  else
    return log(z);
}

vector<double> Dirichlet::Moments(int i)
{
  vector<double> ans(n);

  if (n==1) {
    ans[0] = 1.0;
    return ans;
  }

  switch(i) {
  case 0:
    ans = vector<double>(n, 1.0);
    break;
  case 1:
    ans = Mean();
    break;
  case 2:
    for (int i=0; i<n; i++)ans[i] = A[i]*(1.0 + A[i])/(sumA*(1.0+sumA));
    break;
  default:
    throw DirichletMomException(__FILE__, __LINE__);
  }
  
  return ans;
}

State Dirichlet::Sample(void) 
{
  vector<double> ans(n);
  double norm = 0.0;
  
  if (n==1) {
    ans[0] = 1.0;
    return ans;
  }

  for (int i=0; i<n; i++) {
    ans[i] = (*(gengam[i]))();
    norm += ans[i];
  }
  for (int i=0; i<n; i++) ans[i] /= norm;
  
  return State(ans);
}

