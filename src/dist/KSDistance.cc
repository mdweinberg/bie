#include <queue>
#include <cmath>
#include <Distribution.h>
#include <KSDistance.h>

using namespace BIE;

double KSDistance::kssignificance
(SampleDistribution * distone, SampleDistribution * disttwo)
{
  // Find the KS distance
  vector<SampleDistribution *> distributions(2);
  distributions[0] = distone;
  distributions[1] = disttwo;  
  double distance = ksdistance(distributions);
  
  // Significance calculation based on code provided in Numerical Recipies
  // pp. 623 - 626.
  double N1     = distone->getDataSetSize();
  double N2     = disttwo->getDataSetSize();
  double Ne     = (N1*N2)/(N1+N2);
  double NeSqrt = sqrt(Ne);
  double lambda = (NeSqrt + 0.12 + (0.11/NeSqrt)) * distance;

  int j;
  double a2, fac=2.0, sum=0.0, term, termbf=0.0;

  a2 = -2.0*lambda*lambda;
  
  for (j=1; j<=CONVERGELIMIT; j++)
  {
    term   = fac*exp(a2*j*j);
    sum   += term;
    if(fabs(term) <= EPS1*termbf || fabs(term) <= EPS2*sum) return sum;
    
    fac    = -fac;
    termbf = fabs(term);
  }
  
  return 1.0;    // Failed to converge.
}

// Computes the KS distance between distributions
double KSDistance::ksdistance
(vector<SampleDistribution*> distributions)
{
  // Check that we have at least 2 distributions.
  if (distributions.size() < 2) 
  { throw KSDistanceException(__FILE__, __LINE__); }

  // Check all distributions are one dimensional with matching record type
  RecordType * recordtype = distributions[0]->getRecordType();
  
  for (int distindex = 0; distindex < (int)distributions.size(); distindex++)
  {
    RecordType * disttype = distributions[distindex]->getRecordType();
    
    if (disttype->numFields() != 1)
    { throw KSDistanceException(distindex, disttype->numFields(),__FILE__,__LINE__); }
  
    if (! disttype->equals(recordtype))
    { throw KSDistanceException(0, recordtype, distindex, disttype,__FILE__,__LINE__); }
  }

  priority_queue<double> databoundaries;
  
  // get bin boundaries and place them into a priority queue.
  for (int distindex = 0; distindex < (int)distributions.size(); distindex++)
  {
    vector<double> boundary;
    
    if (dynamic_cast <BinnedDistribution*> (distributions[distindex])) {

      BinnedDistribution *histo = (BinnedDistribution*)distributions[distindex];

      // The getLow and getHigh methods return vectors, but we are definitely in
      // one dimension, so using [0] is fine.
      databoundaries.push((histo->getLow(0))[0]);
    
      for (int binid = 0; binid < histo->numberData(); binid++)
	{ databoundaries.push((histo->getHigh(binid))[0]); }

    } else if (dynamic_cast <PointDistribution*> (distributions[distindex])) {

      PointDistribution *point = (PointDistribution*)distributions[distindex];

      for (int binid = 0; binid < point->numberData(); binid++)
	{ databoundaries.push((point->Point())[0]); }
    }
    else
      { throw InternalError("Not a binned or point distribution", 
			    __FILE__, __LINE__); }

  }
  
  // Keep track of maximum distance.
  double distance        = 0.0;
  // Keep track of last bin boundary so we can skip duplicates easily.
  double lastboundary    = 0.0;
  // We need to know when we are dealing with the first boundary
  bool   isfirstboundary = true;
  
  while(! databoundaries.empty())
  {
    double boundary = databoundaries.top();	
    databoundaries.pop();
  
    if ((lastboundary != boundary) || isfirstboundary)
    {
      isfirstboundary = false;
      
      vector<double> boundaryvector(1, boundary);
      double min = distributions[0]->CDF(boundaryvector);
      double max = min;
    
      for (int distindex=0; distindex < (int)distributions.size(); distindex++)
      {
        // This may want to use a linearly smoothed CDF function in the future.
        boundaryvector[0] = boundary;
	double value = distributions[distindex]->CDF(boundaryvector);
		
        if (value < min) { min = value; }
        if (value > max) { max = value; }
      }
    
      if ((max-min) > distance) { distance = (max-min); }
    }
    
    lastboundary = boundary;
  }
  
  return distance;
}
