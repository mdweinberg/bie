#include <cfloat>

#include <GammaDist.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GammaDist)

using namespace BIE;

GammaDist::GammaDist() : k(1.0), t(1.0), vmin(0.0), vmax(DBL_MAX)
{
  _type  = NormalMult;
  mean   = 1.0;
  var    = 1.0;
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = 1.0;
  ugam   = GammaPtr(new Gamma(k, BIEgen));
}

// The last two parameters can be defaulted
GammaDist::GammaDist(double shape, double scale, double minv, double maxv)
{
  _type  = NormalMult;
  k    = shape;
  t    = scale;
  mean = shape*scale;
  var  = shape*scale*scale;
  vmin   = max<double>(0.0, minv);
  vmax   = min<double>(DBL_MAX, maxv);
  
  mincdf = gamma_p(k, vmin/scale);
  maxcdf = gamma_p(k, vmax/scale);
  norm   = maxcdf - mincdf;

  ugam   = GammaPtr(new Gamma(k, BIEgen));
}

Distribution* GammaDist::New() 
{ return new GammaDist(k, t); }
  
double GammaDist::PDF(State& x)
{
  double ans;
  double q = x[0];
  if (q>=vmin && q<=vmax)
    ans = exp(-q/t + log(q)*(k-1.0) - lgamma(k) - log(t)*k);
  else
    ans = 0.0;
  return ans;
}

double GammaDist::logPDF(State& x)
{
  double ans = 0.0;
  double q = x[0];
  if (q>=vmin && q<=vmax)
    ans = -q/t + log(q)*(k-1.0) - lgamma(k) - log(t)*k;
  else
    throw ImpossibleStateException(__FILE__, __LINE__);
  return ans;
}

double GammaDist::CDF(State& x) 
{
  double ans;
  double q = x[0];
  if (q>=vmax)
    ans = 1.0;
  else if (q<=vmin)
    ans = 0.0;
  else
    ans = (gamma_p(k, q/t) - mincdf)/norm;
  return ans;
}

vector<double> GammaDist::lower(void)
{ return vector<double>(1, vmin); }

vector<double> GammaDist::upper(void)
{ return vector<double>(1, vmax); }

vector<double> GammaDist::Mean(void)
{ return vector<double>(1, mean); }

vector<double> GammaDist::StdDev(void)
{ return vector<double>(1, sqrt(var)); }

vector<double> GammaDist::Moments(int i)
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = mean;
    break;
  case 2:
    ans = var + mean*mean;
    break;
  default:
    cerr << "GammaDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State GammaDist::Sample(void)
{
  return State(vector<double>(1, t*(*ugam)()));
}
