
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

#include <Uniform.h>
#include <Normal.h>
#include <Prior.h>
#include <NormalAdd.h>
#include <BIEException.h>

#include <KDTree.h>
#include <TreePrior.h>
#include <StateFile.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::TreePrior)

using namespace BIE;

extern BIEACG* BIEgen;

TreePrior::TreePrior(StateInfo *si, string chainfile, int level, int cut,
		     int skip, int stride, int keep)
{
  //--------------------------------------------------
  // Base-class data (bypassing Prior constructor)
  //--------------------------------------------------
  _si        = si;
  local_dist = false;

  //--------------------------------------------------
  // This default heating (exponent) is none
  //--------------------------------------------------
  temp = 1.0;

  //--------------------------------------------------
  // Read the chain from the file
  //--------------------------------------------------
  StateFile sf(chainfile, "", "", level, skip, stride, keep, true);

  //--------------------------------------------------
  // Dump first half
  //--------------------------------------------------
  if (skip<0) {
    long cp = sf.prob.size()/2 - sf.popped/2;
    if (cp>0) sf.prob.erase(sf.prob.begin(), sf.prob.begin() + cp);
  }

  //--------------------------------------------------
  // Pack arrays for kd-tree
  //--------------------------------------------------

  ndim   = sf.prob.begin()->point.size();
  mean_  = vector<double>(ndim, 0.0);
  msqr_  = vector<double>(ndim, 0.0);
  stdv_  = vector<double>(ndim, 0.0);
  lower_ = vector<double>(ndim,  DBL_MAX);
  upper_ = vector<double>(ndim, -DBL_MAX);

  vector< vector<double> > pvalue;
  vector<double> points, weights, beta;

  num   = 0;
  for (deque<element>::iterator
       it=sf.prob.begin(); it!=sf.prob.end(); it++) {

    beta   .push_back(1.0);
    weights.push_back(it->weight);
    pvalue .push_back(it->pval);

    num++;
    for (unsigned n=0; n<ndim; n++) {
      points.push_back(it->point[n]);
      mean_[n] += it->point[n];
      msqr_[n] += it->point[n] * it->point[n];
      lower_[n] = max<double>(lower_[n], it->point[n]);
      upper_[n] = max<double>(upper_[n], it->point[n]);
    }
  }
  
  for (unsigned n=0; n<ndim; n++) {
    mean_[n] /= num;
    if (num>1) stdv_[n] = sqrt( (msqr_[n] - mean_[n]*mean_[n]*num)/(num-1) );
    msqr_[n] /= num;
  }

				// Construct the kd-tree
  kdt = new KDTree(ndim, weights.size(), cut, &points[0], &weights[0], &beta[0]);

				// Compute per-cell values
  kdt->createMeasureList(cut, pvalue);

				// Delete the old MH proposal
				// distribution list
  _trans.erase(_trans.begin(), _trans.end());
  for (unsigned n=0; n<ndim; n++) {
    _trans.push_back(TransProbPtr(new BIE::NormalAdd()));
  }

}

TreePrior::~TreePrior()
{
  delete kdt;
}


// Copy constructor
TreePrior::TreePrior(TreePrior* p) 
{
  _si      = p->_si;
  kdt      = p->kdt->New();
  ndim     = p->ndim;
  ndim     = p->num;
  mean_    = p->mean_;
  msqr_    = p->msqr_;
  stdv_    = p->stdv_;
  lower_   = p->lower_;
  upper_   = p->upper_;
  temp     = p->temp;
}

// Factory
TreePrior* TreePrior::New()
{
  TreePrior* p = new TreePrior();
  return p;
}
    
