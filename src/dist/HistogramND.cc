#include <cfloat>
#include <iostream>
#include <fstream>
#include <cerrno>

using namespace std;

#include <HistogramND.h>
#include <BIEException.h>
#include <gfunction.h>
#include <BasicType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::HistogramND)

using namespace BIE;

HistogramND::HistogramND
(clivectord *lo_in, 
 clivectord *hi_in, 
 clivectord *w_in,
 RecordType *type) : low((*lo_in)()), high((*hi_in)()), width((*w_in)())
{
  // Make our own copy of the record type.
  recordtype = new RecordType(type);
  
  // Common initialization
  initialize();
}

HistogramND::HistogramND
(std::vector<double>& lo_in, 
 std::vector<double>& hi_in, 
 std::vector<double>& w_in,
 RecordType *type) : low(lo_in), high(hi_in), width(w_in)
{
  // Make our own copy of the record type.
  recordtype = new RecordType(type);
  
  // Common initialization
  initialize();
}

HistogramND::HistogramND
(clivectord *lo_in, 
 clivectord *hi_in, 
 clivectord *w_in,
 clivectors *names) : low((*lo_in)()), high((*hi_in)()), width((*w_in)())
{
  // Construct the record type.
  recordtype = new RecordType();
  
  for (int fieldindex = 0; fieldindex < (int)(*names)().size(); fieldindex++)
  {
    RecordType * rt = recordtype->insertField(fieldindex+1, 
					      (*names)()[fieldindex],
                                              BasicType::Real);
    
    delete recordtype;
    recordtype = rt;
  }

  // Common initialization
  initialize();
}

HistogramND::HistogramND
(std::vector<double>& lo_in, 
 std::vector<double>& hi_in, 
 std::vector<double>& w_in,
 std::vector<string>& names) :
  low(lo_in), high(hi_in), width(w_in)
{
  // Construct the record type.
  recordtype = new RecordType();
  
  for (size_t fieldindex = 0; fieldindex < names.size(); fieldindex++)
  {
    RecordType * rt = recordtype->insertField(fieldindex+1, 
					      names[fieldindex],
                                              BasicType::Real);
    
    delete recordtype;
    recordtype = rt;
  }

  // Common initialization
  initialize();
}

// Common initialization routine.
void HistogramND::initialize()
{
  // The dimension is the number of fields in the record type.
  dim = recordtype->numFields();
  
  // Check there is at least one field in the type.
  if (dim  < 1) 
  { throw TypeException("The record type of a Histogram cannot be empty", __FILE__, __LINE__); }
  
  // Check that the vector dimensions match the RecordType dimensions.
  if ((int)low.size() != dim) 
  { 
    throw TypeException("Dimension of low vector does not match record type",
                        __FILE__, __LINE__); 
  }
  
  if ((int)high.size() != dim) 
  { 
    throw TypeException("Dimension of high vector does not match record type",
                        __FILE__, __LINE__); 
  }

  if ((int)width.size() != dim) 
  { 
    throw TypeException("Dimension of width vector does not match record type",
                        __FILE__, __LINE__); 
  }

  // Check the bounds and width of the bins.
  for (int dimindex = 0; dimindex < (int) low.size(); dimindex++)
  {
    if (low[dimindex] >= high[dimindex])
    {
      ostringstream errorbuffer;
      errorbuffer << "Low bound less or equal to high bound : ";
      errorbuffer << "dimension index: " << dimindex << ".";
      throw BIEException("HistogramND Exception", errorbuffer.str(), __FILE__, __LINE__);
    }
    
    if (width[dimindex] <= 0)
    {
      ostringstream errorbuffer;
      errorbuffer << "Bin width cannot be zero or less: ";
      errorbuffer << "dimension index: " << dimindex << ".";
      throw BIEException("HistogramND Exception", errorbuffer.str(), __FILE__, __LINE__);
    }
  }

  // Check every field is real valued.
  for (int fieldindex = 1; fieldindex <= recordtype->numFields(); fieldindex++)
  {
    BasicType * fieldtype = recordtype->getFieldType(fieldindex);

    if (! (fieldtype->equals(BasicType::Real)))
    { throw TypeException("Every field in the histogram type must be a real type.", __FILE__, __LINE__); }
  }  
  
  // Truncate bin width to match limits
  for (int i=0; i<dim; i++) width[i] = min<double>(high[i]-low[i], width[i]);


  // Setup for indexing
  irank = vector<int>(dim);
  nbins = 1;
  vol = 1.0;
  for (int i=0; i<dim; i++) {
    irank[i] = (int)( (high[i]-low[i])/(fabs(width[i])+1.0e-12) ) + 1;
    high[i] = low[i] + width[i]*irank[i];
    nbins *= irank[i];
    vol *= width[i];
  }

  // Rank block offsets for indexing
  imarg = vector<int>(dim);
  imarg[0] = 1;
  for (int i=1; i<dim; i++) imarg[i] = irank[i-1]*imarg[i-1];

  bins = vector<double>(nbins, 0.0);

  // Bin centers
  centers.resize(dim);
  for (int i=0; i<dim; i++) {
    centers[i].resize(irank[i]);
    for (int j=0; j<irank[i]; j++)
      centers[i][j] = low[i] + (0.5 + j)*width[i];
  }

  mass = 0.0;
}

// Null constructor
HistogramND::HistogramND()
{
  dim        = 0;
  recordtype = 0;
}

HistogramND::~HistogramND(void)
{
}

// Factory
HistogramND* HistogramND::New()
{
  HistogramND *p = new HistogramND();

  p->dim     = dim;
  p->nbins   = nbins;
  p->mass    = mass;

  p->bins    = bins;
  p->low     = low;
  p->high    = high;
  p->width   = width;

  p->centers = centers;
  
  p->irank   = irank;
  p->imarg   = imarg;

  p->mean    = mean;
  p->square  = square;
  p->stdev   = stdev;

  // Make our own copy of the record type.
  p->recordtype = new RecordType(recordtype);

  return p;
}

bool HistogramND::AccumulateData(double v, RecordBuffer* datapoint)
{
  StateInfo si(recordtype->numFields());
  State x(&si);

  // foreach field THIS CLASS knows about and expects...
  for (int fieldindex = 1; fieldindex <= recordtype->numFields(); fieldindex++)
  {
    // Get the fieldname
    string fieldname = recordtype->getFieldName(fieldindex);
    
    // Get the value of the field with this name.
    if (datapoint->isValidFieldName(fieldname))
    {
      x[fieldindex-1]  = datapoint->getRealValue(fieldname);
    }
    else
    {
      // This data point is no use to us.
      return false;
    }
  }

  return AccumulateData(v, x);
}

bool HistogramND::AccumulateData(double v, State& x)
{
  if (OK(x)) {
    bins[INDX(x)] += v;
    mass += v;
    return true;
  }
  return false;
}

double HistogramND::PDF(State& x)
{
  if (OK(x)) return bins[INDX(x)];
  return 0.0;
}

double HistogramND::logPDF(State& x) {
  if (OK(x) && bins[INDX(x)]) return log(bins[INDX(x)]);
  return -DBL_MAX;
}

double HistogramND::CDF(State& x)
{
  if (OK(x) && mass>0.0) {

    vector<int> in0 = IV(INDX(x));

    double sum = 0.0;
    for (int i=0; i<nbins; i++) {

      vector<int> in = IV(i);
      for (int indx=0; indx<dim; indx++) {
	if (in[indx] > in0[indx]) continue;
      }
      sum += bins[i];
    }
    return sum/mass;
  }
  else
    return 0.0;
}

void HistogramND::ComputeDistribution(void)
{
  mean   = vector<double>(dim, 0.0);
  square = vector<double>(dim, 0.0);
  stdev  = vector<double>(dim);

  for (int i=0; i<nbins; i++) {
    
    vector<int> in = IV(i);
    for (int indx=0; indx<dim; indx++) {
      mean[indx]   += bins[i] * centers[indx][in[indx]];
      square[indx] += bins[i] * 
	centers[indx][in[indx]]*centers[indx][in[indx]];
    }
  }

  for (int indx=0; indx<dim; indx++) {
    mean[indx]   /= mass;
    square[indx] /= mass;
    stdev[indx]   = sqrt(square[indx] - mean[indx]*mean[indx]);
  }

}


void HistogramND::dumpraw(string file, int i, int j)
{
  ostream cout(checkTable("simulation_output"));

  ofstream raw(file.c_str());
  if (!raw) { 
    throw FileNotExistException("HistogramND: ", file, __FILE__, __LINE__); 
  }

				// Print size

  cout << "HisotgramND: size = [" << irank[i] 
       << ", " << irank[j] << "]\n";


				// Make matrix and marginalize
  typedef vector<double> vec;
  vector<vec> matrix(irank[i]);
  for (int k=0; k<irank[i]; k++) matrix[k] = vector<double>(irank[j], 0.0);

  
  vector<int> in;
  for (int n=0; n<nbins; n++) {
    in = IV(n);
    matrix[in[i]][in[j]] += bins[n];
  }

				// Find max

  double minimum =  1.0e20;
  double maximum = -1.0e20;

  for (int jn=0; jn<irank[j]; jn++) {
    for (int in=0; in<irank[i]; in++) {
      minimum = min<double>(minimum, matrix[in][jn]);
      maximum = max<double>(maximum, matrix[in][jn]);
    }
  }
      
  unsigned char c;
  for (int jn=0; jn<irank[j]; jn++) {
    for (int in=0; in<irank[i]; in++) {
      // c = (unsigned char)(256*(matrix[in][jn] - minimum)/(maximum-minimum));
      c = (unsigned char)(255*matrix[in][jn]/maximum);
      raw.write(reinterpret_cast<char*>(&c), sizeof(unsigned char));
    }
  }

}

void HistogramND::dumpsm(string file, int i, int j)
{
  ostream cout(checkTable("simulation_output"));

  ofstream sm(file.c_str());
  if (!sm) { 
    throw FileNotExistException("HistogramND: ", file, __FILE__, __LINE__); 
  }

				// Print size

  float z;

  sm.write((char *)&irank[i], sizeof(int));
  sm.write((char *)&irank[j], sizeof(int));
  
  sm.write((char *)&(z=low[i]  + 0.5*width[i]), sizeof(float));
  sm.write((char *)&(z=high[i] - 0.5*width[i]), sizeof(float));
  sm.write((char *)&(z=low[j]  + 0.5*width[j]), sizeof(float));
  sm.write((char *)&(z=high[i] - 0.5*width[j]), sizeof(float));

				// Make matrix and marginalize
  typedef vector<double> vec;
  vector<vec> matrix(irank[i]);
  for (int k=0; k<irank[i]; k++) matrix[k] = vector<double>(irank[j], 0.0);

  
  vector<int> in;
  for (int n=0; n<nbins; n++) {
    in = IV(n);
    matrix[in[i]][in[j]] += bins[n];
  }

  for (int jn=0; jn<irank[j]; jn++) {
    for (int in=0; in<irank[i]; in++) {
      sm.write((char *)&(z=matrix[in][jn]), sizeof(float));
    }
  }
  
}

void HistogramND::dumpmat(vector< vector<double> >& matrix, 
			  vector<double>& x, vector<double>& y,
			  int i, int j)
{
				// Assign grid points
  x = vector<double>(irank[i]);
  for (int k=0; k<irank[i]; k++) x[k] = low[i] + (0.5 + k)*width[i];
  
  y  = vector<double>(irank[j]);
  for (int k=0; k<irank[j]; k++) y[k] = low[j] + (0.5 + k)*width[j];
  

				// Make matrix
  typedef vector<double> vec;
  matrix = vector<vec> (irank[i]);
  for (int k=0; k<irank[i]; k++) matrix[k] = vector<double>(irank[j], 0.0);
  
				// Marginalize
  vector<int> in;
  for (int n=0; n<nbins; n++) {
    in = IV(n);
    matrix[in[i]][in[j]] += bins[n];
  }
}
