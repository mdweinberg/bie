#include <Distribution.h>
#include <BasicType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PointDistribution)

using namespace BIE;

PointDistribution::PointDistribution (string dataname) : full(false)
{
  // Create a record type containing a field with the specified name.
  RecordType::fieldstruct typetable[] = {{dataname.c_str(),BasicType::Real},
					 {0,0}};
  recordtype = new RecordType(typetable);
}


PointDistribution::PointDistribution (RecordType * type) : full(false)
{
  for (int i=1; i<=type->numFields(); i++) {
    // Check that this field is of type real.
    if (!((type->getFieldType(i))->equals(BasicType::Real))) {
      throw TypeException("Type of field in PointDistribution "
			  "record type must be real.", __FILE__, __LINE__); 
    }
  }
  
  // Make a copy of the record type.
  recordtype = new RecordType(type);
}


PointDistribution* PointDistribution::New()
{
  return new PointDistribution(recordtype);
}


bool PointDistribution::AccumulateData(double v, RecordBuffer* datapoint)
{
  vector<double> x (recordtype->numFields());
  
  // foreach field THIS CLASS knows about and expects...
  for (int fieldindex = 1; fieldindex <= recordtype->numFields(); fieldindex++)
  {
    // Get the fieldname
    string fieldname = recordtype->getFieldName(fieldindex);
    
    // Get the value of the field with this name.
    if (datapoint->isValidFieldName(fieldname))
    {
      x[fieldindex-1]  = datapoint->getRealValue(fieldname);
    }
    else
    {
      // This data point is no use to us.
      return false;
    }
  }

  return AccumulateData(v, x);
}


bool PointDistribution::AccumulateData(double v, vector<double>& x)
{
  if (full) {
    return false;
  } else {
    data = x;
    full = true;
    return true;
  }
}
