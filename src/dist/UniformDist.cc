#include <UniformDist.h>
#include <Uniform.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::UniformDist)

using namespace BIE;

UniformDist::UniformDist()
{
  _type = UniformAdd;
  a = 0.0;
  b = 1.0;
  mean = 0.5*(a + b);
  var = (pow(b, 3.0) - pow(a, 3.0))/(3.0*(b - a)) - mean*mean;

  unit = UniformPtr(new Uniform(a, b, BIEgen));
}

UniformDist::UniformDist(double minv, double maxv)
{
  _type = UniformAdd;
  a = minv;
  b = maxv;
  mean = 0.5*(a+b);
  var = (pow(b, 3.0) - pow(a, 3.0))/(3.0*(b - a)) - mean*mean;

  unit = UniformPtr(new Uniform(a, b, BIEgen));
}

UniformDist::UniformDist(double scale)
{
  _type = UniformAdd;
  a = -0.5*scale;
  b =  0.5*scale;
  mean = 0.5*(a+b);
  var = (pow(b, 3.0) - pow(a, 3.0))/(3.0*(b - a)) - mean*mean;

  unit = UniformPtr(new Uniform(a, b, BIEgen));
}

Distribution* UniformDist::New() 
{ return new UniformDist(a, b); }

double UniformDist::PDF(State& x) 
{
  double ans = 0.0;
  if (x[0]<=b && x[0]>=a)
    ans = 1.0/(b - a);
  return ans;
}

double UniformDist::logPDF(State& x) 
{
  double ans = 0.0;
  if (x[0]<=b && x[0]>=a)
    ans = -log(b - a);
  else
    throw ImpossibleStateException(__FILE__, __LINE__);
  return ans;
}

double UniformDist::CDF(State& x) 
{
  double ans;
  if (x[0]<=a)
    ans = 0.0;
  else if (x[0]>=b)
    ans = 1.0;
  else
    ans = (x[0] - a)/(b - a);
  return ans;
}

vector<double> UniformDist::lower(void) 
{ return vector<double>(1, a); }

vector<double> UniformDist::upper(void) 
{ return vector<double>(1, b); }

vector<double> UniformDist::Mean(void) 
{ return vector<double>(1, mean); }

vector<double> UniformDist::StdDev(void) 
{ return vector<double>(1, sqrt(var)); }

vector<double> UniformDist::Moments(int i) \
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = mean;
    break;
  case 2:
    ans = var + mean*mean;
    break;
  default:
    cerr << "UniformDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State UniformDist::Sample(void) 
{ return State(vector<double>(1, (*unit)())); }
