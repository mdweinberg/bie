#include <Histogram1D.h>
#include <BasicType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::CumSampleHistogram)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Histogram1D)

using namespace BIE;

void CumSampleHistogram::compute_accum(void)
{
  n = buckets();
  
  // Complain if there are less than three bins.
  if (n < 3) { throw InternalError(__FILE__, __LINE__); }
  
  cum = vector<Bin>(n-2);
  bin = vector<double>(n-2);
  
  double cur = inBucket(0);
  for (int i=0; i<n-2; i++) {
    cur += inBucket(i+1);

    cum[i].bot = bucketThreshold(i);
    cum[i].top = bucketThreshold(i+1);
    cum[i].center = 0.5*(bucketThreshold(i+1) + bucketThreshold(i));
    cum[i].val = cur;
  }
  cur += inBucket(n-1);

  for (int i=0; i<n-2; i++) cum[i].val /= cur;

  for (int i=0; i<n-2; i++) bin[i] = cum[i].top;

  accum = true;
}


double CumSampleHistogram::quartile(double s)
{
  if (!accum) compute_accum();

  vector<Bin>::iterator i = lower_bound(cum.begin(), cum.end(), s);

  // Bin center
  // return i->center;
  // Linear interp
  double ans;
  if (i == cum.begin())
    ans = 
      i->top*(s - inBucket(0))/(i->val - inBucket(0)) +
      i->bot*(i->val - s)/(i->val - inBucket(0));
  else
    ans = 
      i->top*(s - (i-1)->val)/(i->val - (i-1)->val) +
      i->bot*(i->val - s)/(i->val - (i-1)->val);

  return ans;
}


double CumSampleHistogram::fraction(double s)
{
  if (!accum) compute_accum();

  double ans;

  if (s<*bin.begin())
    ans = 0.0;
  else if (s>*(bin.end()-1))
    ans = 1.0;
  else {
    
    int i1;
    vector<double>::iterator i = upper_bound(bin.begin(), bin.end(), s);
    if (i==bin.end())
      i1 = n-3;
    else
      i1 = i-bin.begin();

    double dif = cum[i1].top - cum[i1-1].top;

    ans = cum[i1].val * (s - cum[i1-1].top)/dif;
    if (i1) ans += cum[i1-1].val * (cum[i1].top - s)/dif;
  }

  return ans;
}


Histogram1D::Histogram1D
(double lo, double hi, double w, string dataname)
{
  initialize(lo,hi,w);

  // Create a record type containing a field with the specified name.
  RecordType::fieldstruct typetable[] = {{dataname.c_str(),BasicType::Real},{0,0}};
  recordtype = new RecordType(typetable);
}


Histogram1D::Histogram1D
(double lo, double hi, double w, RecordType * type)
{
  initialize(lo,hi,w);

  // check there is only one field
  if (type->numFields() != 1)
  { throw TypeException("Histogram1D type must have only one field", __FILE__, __LINE__); }

  // Check that this field is of type real.
  if (!((type->getFieldType(1))->equals(BasicType::Real)))
  { throw TypeException("Type of field in Histogram1D record type must be real.", __FILE__, __LINE__); }
  
  // Make a copy of the record type.
  recordtype = new RecordType(type);
}

void Histogram1D::initialize
(double lo, double hi, double w)
{
  // Set this to be 0 to avoid deletion in case of an exception.
  ch = 0;

  // Check the values of low and high are sensible.
  if (lo>=hi) 
  { 
    throw BIEException("Histogram1D Exception", "Low bound less or equal to high bound in Histogram1D.", 
                       __FILE__, __LINE__);
  }

  low  = lo;
  high = hi;
  
  // Check the value of width is sensible.
  if (w <= 0)
  { 
    throw BIEException("Histogram1D Exception", "Bin width cannot be zero or less in Histogram1D.",
                        __FILE__, __LINE__ );
  }

  if (w >= high - low) {
    // The width is wider than the entire range - scale it down.
    width = high-low;
    toplimit = high;
  } else {
    // Compute the upper limit - stretch it a little so the bins are same width
    width = w;
    toplimit = lo + (int)((hi-lo)/w) * w;
  }

  ch = new CumSampleHistogram(low, high, width);
}

Histogram1D::~Histogram1D(void)
{
  if (ch != 0) { delete ch; }
}


Histogram1D* Histogram1D::New()
{
  return new Histogram1D(low, high, width, recordtype);
}


bool Histogram1D::AccumulateData(double v, RecordBuffer* datapoint)
{
  vector<double> x (1);

  // Get the name of the field we are interested in.
  string fieldname = recordtype->getFieldName(1);
    
  // Get the value of the field with this name and store it in the vector.
  // Avoid throwing an exception if the field does not exist.
  if (datapoint->isValidFieldName(fieldname))
  {
    x[0] = datapoint->getRealValue(fieldname);
  }

  return AccumulateData(v, x);
}


bool Histogram1D::AccumulateData(double v, vector<double>& x)
{
  *ch += x[0];
  bool ret = false;
  if (x[0]>=low && x[0]<toplimit) ret = true;
 
  return ret;
}


void Histogram1D::ComputeDistribution()
{
  // Do nothing
}
