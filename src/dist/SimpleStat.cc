// This may look like C code, but it is really -*- C++ -*-

#include <cfloat>
#include <cmath>

#include <Normal.h>
#include <Uniform.h>
#include <SimpleStat.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SimpleStat)

using namespace BIE;

SimpleStat::SimpleStat(int n)
{
  dimen = n;
  num = 0;
  mom1 = vector<double>(n, 0.0);
  mom2 = vector<double>(n, 0.0);
  mean = vector<double>(n);
  var  = vector<double>(n);
  ret  = vector<double>(n);

  normal = new Normal(0.0, 1.0, BIEgen);
  computed = false;
}

SimpleStat::~SimpleStat()
{
  delete normal;
}

void SimpleStat::ComputeDistribution(void)
{
  if (num<2) {

    mean = mom1;
    var = vector<double>(dimen, 0.0);

  } else {

    for (int i=0; i<dimen; i++) {
      mean[i] = mom1[i]/num;
      var[i]  = (mom2[i] - num*mean[i]*mean[i])/(num-1);
    }

  }
    
  computed = true;

}

// Value info currently ignored
bool SimpleStat::AccumData(vector<double>& z, vector<double>& x)
{
  for (int i=0; i<(int)x.size(); i++) {
    mom1[i] += x[i];
    mom2[i] += x[i]*x[i];
  }
  num++;
  computed = false;

  return true;
}

double SimpleStat::CDF(State& x) 
{
  double ans = 1.0;
  
  if (!computed) ComputeDistribution();

  for (int i=0; i<(int)x.size(); i++)
    ans *= 0.5*(1.0 + erf((x[i]-mean[i])/sqrt(2.0*var[i])));
  
  return ans;
}

double SimpleStat::PDF(State& x) 
{
  double ans = 1.0;
  
  if (!computed) ComputeDistribution();

  for (int i=0; i<(int)x.size(); i++)
    ans *= exp(-(x[i]-mean[i])*(x[i]-mean[i])/(2.0*var[i])) /
      sqrt(2.0*M_PI*var[i]);
  
  return ans;
}

double SimpleStat::logPDF(State& x) 
{
  double ans = 0.0;
  
  if (!computed) ComputeDistribution();

  for (int i=0; i<(int)x.size(); i++)
    ans += -(x[i]-mean[i])*(x[i]-mean[i])/(2.0*var[i]) - 
      0.5*log(2.0*M_PI*var[i]);
  
  return ans;
}

vector<double> SimpleStat::lower(void)
{
  for (int i=0; i<dimen; i++) ret[i] = -DBL_MAX;
  return ret;
}

vector<double> SimpleStat::upper(void)
{
  for (int i=0; i<dimen; i++) ret[i] = DBL_MAX;
  return ret;
}

vector<double> SimpleStat::Mean()
{
  ret = mean;
  return ret;
}

vector<double> SimpleStat::StdDev()
{
  for (int i=0; i<dimen; i++) ret[i] = sqrt(var[i]);
  return ret;
}

vector<double> SimpleStat::Moments(int m)
{
  if (m==2)
    return mom2;
  else
    return mom1;
}

State SimpleStat::Sample() 
{
  if (!computed) ComputeDistribution();
  for (int i=0; i<dimen; i++) ret[i] = mean[i] + sqrt(var[i])*(*normal)();
  return State(ret);
}
