#include <DiscUnifDist.h>
#include <DiscUnif.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::DiscUnifDist)

using namespace BIE;

DiscUnifDist::DiscUnifDist()
{
  _type = UniformAdd;
  a = 0;
  b = 1;
  mean = 0.5*(a + b);
  var = (pow(b, 3.0) - pow(a, 3.0))/(3.0*(b - a)) - mean*mean;

  unit = DiscUnifPtr(new DiscreteUniform(a, b, BIEgen));
}

DiscUnifDist::DiscUnifDist(int minv, int maxv)
{
  _type = UniformAdd;
  a = minv;
  b = maxv;
  mean = 0.5*(a+b);
  var = (pow(b, 3.0) - pow(a, 3.0))/(3.0*(b - a)) - mean*mean;

  unit = DiscUnifPtr(new DiscreteUniform(a, b, BIEgen));
}

Distribution* DiscUnifDist::New() 
{ return new DiscUnifDist(a, b); }

double DiscUnifDist::PDF(State& x) 
{
  double ans = 0.0;
  if (x[0]<=b && x[0]>=a)
    ans = 1.0/(b - a + 1);
  return ans;
}

double DiscUnifDist::logPDF(State& x) 
{
  double ans = 0.0;
  if (x[0]<=b && x[0]>=a)
    ans = -log(b - a + 1);
  else
    throw ImpossibleStateException(__FILE__, __LINE__);
  return ans;
}

double DiscUnifDist::CDF(State& x) 
{
  double ans;
  if (x[0]<=a)
    ans = 0.0;
  else if (x[0]>=b)
    ans = 1.0;
  else
    ans = (x[0] - a)/(b - a + 1);
  return ans;
}

vector<double> DiscUnifDist::lower(void) 
{ return vector<double>(1, a); }

vector<double> DiscUnifDist::upper(void) 
{ return vector<double>(1, b); }

vector<double> DiscUnifDist::Mean(void) 
{ return vector<double>(1, mean); }

vector<double> DiscUnifDist::StdDev(void) 
{ return vector<double>(1, sqrt(var)); }

vector<double> DiscUnifDist::Moments(int i) \
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = mean;
    break;
  case 2:
    ans = var + mean*mean;
    break;
  default:
    cerr << "DiscUnifDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State DiscUnifDist::Sample(void) 
{ return State(vector<double>(1, (*unit)())); }
