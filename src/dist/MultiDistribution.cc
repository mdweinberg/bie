#include <MultiDistribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MultiDistribution)

using namespace BIE;

MultiDistribution::MultiDistribution(int number, BinnedDistribution** list)
{
  dists.resize(number);
  
  for (unsigned int i = 0; i < dists.size(); i++)
  { dists[i] = list[i]; }

  initialize();
}

MultiDistribution::MultiDistribution(vector<BinnedDistribution*> list)
{
  dists = list;

  initialize();
}

void MultiDistribution::initialize()
{
  // Set this pointer to 0 to avoid deletion in case of exception.
  uniontype = 0;
  
  // Get number of bins per distribution
  for (unsigned int distindex = 0; distindex < dists.size(); distindex++) 
  { bin_counts.push_back(dists[distindex]->numberData()); }
  
  // Compute offsets for linear bin lists
  bin_offsets = vector<int>(dists.size()+1, 0);
  for (unsigned int i=1; i<=dists.size(); i++) 
    bin_offsets[i] += bin_offsets[i-1] + bin_counts[i-1];
  
  // Get dimensionality of each distribution
  Ndim = 0;
  for (unsigned int i=0; i<dists.size(); i++) {
    dim_counts.push_back(dists[i]->lower().size());
    Ndim += dim_counts[i];
  }
  
  // Compute offsets for dimension lists
  dim_offsets = vector<int>(dists.size()+1, 0);
  for (unsigned int i=1; i<=dists.size(); i++) 
    dim_offsets[i] += dim_offsets[i-1] + dim_counts[i-1];

  // We do not expect any particular fields.
  recordtype = new RecordType();
  
  // Construct a record type unioning all the fields used.
  uniontype  = new RecordType();

  for (unsigned int distindex = 0; distindex < dists.size(); distindex++)
  {
    RecordType * distrecordtype = dists[distindex]->getRecordType();
    RecordType * temp           = uniontype->unionWithRecord(distrecordtype);
    
    delete uniontype;
    uniontype = temp;
  }
}

MultiDistribution::~MultiDistribution()
{ 
  if (uniontype != 0) 
    delete uniontype;
}

MultiDistribution::MultiDistribution()
{
  uniontype = 0;
}

MultiDistribution* MultiDistribution::New()
{
  MultiDistribution *p = new MultiDistribution;
  (p->dists).resize(dists.size());
  for (unsigned int n=0; n<dists.size(); n++) p->dists[n] = dists[n]->New();

  p->bin_counts = bin_counts;
  p->bin_offsets = bin_offsets;
  p->dim_counts = dim_counts;
  p->dim_offsets = dim_offsets;

  p->recordtype = new RecordType(recordtype);
  p->uniontype = new RecordType(uniontype);

  return p;
}


void MultiDistribution::ComputeDistribution()
{
  for (unsigned int i=0; i<dists.size(); i++) dists[i]->ComputeDistribution();
}

double MultiDistribution::PDF(State& z)
{
  double ans = 1.0;
  for (unsigned int i=0; i<dists.size(); i++) ans *= dists[i]->PDF(z);

  return ans;
}

double MultiDistribution::logPDF(State& z)
{
  double ans = 0.0;
  for (unsigned int i=0; i<dists.size(); i++) ans += dists[i]->logPDF(z);

  return ans;
}

vector<double> MultiDistribution::lower()
{
  vector<double> ans(Ndim), ret;
  for (unsigned int i=0; i<dists.size(); i++) {
    ret = dists[i]->lower();
    for (int j=dim_offsets[i]; j<dim_offsets[i+1]; j++) 
      ans[j] = ret[j-dim_offsets[i]];
  }
  
  return ans;
}

vector<double> MultiDistribution::upper()
{
  vector<double> ans(Ndim), ret;
  for (unsigned int i=0; i<dists.size(); i++) {
    ret = dists[i]->upper();
    for (int j=dim_offsets[i]; j<dim_offsets[i+1]; j++) 
      ans[j] = ret[j-dim_offsets[i]];
  }
  
  return ans;
}

vector<double> MultiDistribution::Mean()
{
  vector<double> ans(Ndim), ret;
  for (unsigned int i=0; i<dists.size(); i++) {
    ret = dists[i]->Mean();
    for (int j=dim_offsets[i]; j<dim_offsets[i+1]; j++) 
      ans[j] = ret[j-dim_offsets[i]];
  }
  
  return ans;
}

vector<double> MultiDistribution::StdDev()
{
  vector<double> ans(Ndim), ret;
  for (unsigned int i=0; i<dists.size(); i++) {
    ret = dists[i]->StdDev();
    for (int j=dim_offsets[i]; j<dim_offsets[i+1]; j++) 
      ans[j] = ret[j-dim_offsets[i]];
  }
  
  return ans;
}

vector<double> MultiDistribution::Moments(unsigned n)
{
  vector<double> ans(Ndim), ret;
  for (unsigned int i=0; i<dists.size(); i++) {
    ret = dists[i]->Moments(n);
    for (int j=dim_offsets[i]; j<dim_offsets[i+1]; j++) 
      ans[j] = ret[j-dim_offsets[i]];
  }
  
  return ans;
}

State MultiDistribution::Sample()
{
  vector<double> ans(Ndim), ret;
  for (unsigned int i=0; i<dists.size(); i++) {
    ret = dists[i]->Sample();
    for (int j=dim_offsets[i]; j<dim_offsets[i+1]; j++) 
      ans[j] = ret[j-dim_offsets[i]];
  }
  
  return State(ans);
}

bool MultiDistribution::AccumulateData(double v, RecordBuffer * datapoint)
{
  bool ret = false;
  
  for (unsigned int i=0; i<dists.size(); i++) {
    if (dists[i]->AccumData(v, datapoint)) ret = true;
  }
  
  return ret;
}

