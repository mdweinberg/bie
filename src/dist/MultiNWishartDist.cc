#include <MultiNWishartDist.h>
#include <linalg.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MultiNWishartDist)

using namespace BIE;

MultiNWishartDist::MultiNWishartDist(double s, clivectord* d, int copies)
{
  shape  = s;
  diag   = (*d)();
  N      = diag.size();
  M      = copies;
  _type  = NormalAdd;
  
  for (unsigned n=0; n<N; n++) 
    gamma.push_back(GammaPtr(new Gamma(shape-n, BIEgen)));

  normal = NormalPtr(new Normal(0.0, 1.0, BIEgen));

  evals  = VectorM(1, N);
  eigen  = MatrixM(1, N, 1, N);
  W      = MatrixM(1, N, 1, N);
  A      = MatrixM(1, N, 1, N);

  A.zero();
  for (unsigned n=0; n<N; n++) A[n+1][n+1] = shape*diag[n];

  VectorM p(1, N);
  try {
    C = A;
    cholesky_decomp(C, p);
  }
  catch (string error) {
    throw BIEException("MultiNWishartDist failure", error, __FILE__, __LINE__);
  }

  for (unsigned i=1; i<=N; i++) {
    C[i][i] = p[i];
    for (unsigned j=i+1; j<=N; j++) C[i][j] = 0.0;
  }

  realize();
}

MultiNWishartDist::MultiNWishartDist(double s, std::vector<double> d, int copies)
{
  shape  = s;
  diag   = d;
  N      = diag.size();
  M      = copies;
  _type  = NormalAdd;
  
  for (unsigned n=0; n<N; n++) 
    gamma.push_back(GammaPtr(new Gamma(shape-n, BIEgen)));

  normal = NormalPtr(new Normal(0.0, 1.0, BIEgen));

  evals  = VectorM(1, N);
  eigen  = MatrixM(1, N, 1, N);
  W      = MatrixM(1, N, 1, N);
  A      = MatrixM(1, N, 1, N);

  A.zero();
  for (unsigned n=0; n<N; n++) A[n+1][n+1] = shape*diag[n];

  VectorM p(1, N);
  try {
    C = A;
    cholesky_decomp(C, p);
  }
  catch (string error) {
    throw BIEException("MultiNWishartDist failure", error, __FILE__, __LINE__);
  }

  for (unsigned i=1; i<=N; i++) {
    C[i][i] = p[i];
    for (unsigned j=i+1; j<=N; j++) C[i][j] = 0.0;
  }

  realize();
}

Distribution* MultiNWishartDist::New() 
{
  clivectord d(diag);
  return new MultiNWishartDist(shape, &d, M);
}
  
void MultiNWishartDist::realize()
{
  // Generate A
  A.zero();
  for (unsigned i=1; i<=N; i++) A[i][i] = sqrt(2.0*(*gamma[i-1])());
  for (unsigned i=1; i<=N; i++) {
    for (unsigned j=1; j<i; j++) A[i][j] = (*normal)();
  }
  
  // Generate W
  W = C * A * A.Transpose() * C.Transpose();

  // Compute Eigenanalysis
  evals = Symmetric_Eigenvalues_GHQL(W, eigen);

  // Compute the determinant of the inverse
  det = 1.0;
  for (unsigned i=1; i<=N; i++) det /= evals[i];
}

double MultiNWishartDist::PDF(State& x) 
{
  double disp = 0.0;
  vector<double> v = x.vec();
  for (unsigned i=1; i<=N; i++) {
    for (unsigned j=1; j<=N; j++) {
      disp += v[i-1]*W[i][j]*v[j-1];
    }
  }

  return exp(-0.5*disp)/(pow(2.0*M_PI, 0.5*N)*sqrt(det));
}

double MultiNWishartDist::logPDF(State& x) 
{
  double disp = 0.0;
  vector<double> v = x.vec();
  for (unsigned i=1; i<=N; i++) {
    for (unsigned j=1; j<=N; j++) {
      disp += v[i-1]*W[i][j]*v[j-1];
    }
  }

  return -0.5*disp - 0.5*N*log(2.0*M_PI) -0.5*log(det);
}

vector<double> MultiNWishartDist::lower(void)
{ 
  vector<double> ret(N*M, -DBL_MAX);
  return ret;
}

vector<double> MultiNWishartDist::upper(void)
{ 
  vector<double> ret(N*M, DBL_MAX);
  return ret;
}

vector<double> MultiNWishartDist::Mean(void)
{ return vector<double>(N*M, 0.0); }

vector<double> MultiNWishartDist::StdDev(void)
{ 
  vector<double> ans(N);
  for (int j=0; j<M; j++) {
    for (unsigned i=0; i<N; i++) ans[j*N+i] = sqrt(evals[i+1]);
  }
  return ans;
}

double MultiNWishartDist::CDF(State& x) 
{
  // No analytic expression
  return 1.0;
}

vector<double> MultiNWishartDist::Moments(int i) 
{
  vector<double> ans;

  switch(i) {
  case 0:
    ans = vector<double>(N*M, 1.0);
    break;
  case 1:
    ans = vector<double>(N*M, 0.0);
    break;
  case 2:
    ans = vector<double>(N*M);
    for (int j=0; j<M; j++) {
      for (unsigned i=0; i<N; i++) ans[j*N+i] = evals[i+1];
    }
    break;
  default:
    cerr << "MultiNWishartDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return ans;
}

State MultiNWishartDist::Sample(void) 
{
  vector<double> result;
  VectorM input(1, N), output;

  // Generate a new prior inverse variance
  realize();

  //
  // Generate random numbers from orthogonal vectors
  //
  for (int n=0; n<M; n++) {
    
    for (unsigned k=1; k<=N; k++)
      input[k] = sqrt(fabs(1.0/evals[k]))*(*normal)();
  
    // Transform to original variables
    output = eigen * input;

    double *p = &output[1];
    vector<double> r(p, p+N);
    
    // Append the vector
    result.insert(result.end(), r.begin(), r.end());
  }

  return State(result);
}

