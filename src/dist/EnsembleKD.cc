
#include <iostream>
#include <iomanip>
#include <cfloat>

#include <BIEException.h>
#include <BIEMutex.h>
#include <gfunction.h>
#include <BIEmpi.h>
#include <EnsembleKD.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::EnsembleKD)

using namespace std;
using namespace BIE;

const bool verbose_debug = true;

static
bool compare_vector (const std::vector<double>& a, 
		     const std::vector<double>& b)
{
  for (size_t n=0; n<min<size_t>(a.size(), b.size()); n++) {
    if ( fabs(a[n]-b[n]) > min<double>(fabs(a[n]), fabs(b[n]))*1.0e-10 ) 
      return a[n] < b[n];
  }
  return false;
}

static
bool compare_vector_pair (const std::pair<std::vector<double>, double>& a, 
			  const std::pair<std::vector<double>, double>& b)
{
  for (size_t n=0; n<min<size_t>(a.first.size(), b.first.size()); n++) {
    if ( fabs(a.first[n]-b.first[n]) > 
	 min<double>(fabs(a.first[n]), fabs(b.first[n]))*1.0e-10 ) 
      return a.first[n] < b.first[n];
  }
  return false;
}

				// Target bucket size for KD-tree
int    EnsembleKD::ncut     = 32;
				// Minimum number of states per subspace
int    EnsembleKD::minsub   = 3;

EnsembleKD::EnsembleKD() : Ensemble()
{
  dist_computed = false;
  cache = CachePtr();
}


EnsembleKD::EnsembleKD(StateInfo *si,
		       int level, int begin, string filename, int keypos) :
  Ensemble(si, level, begin, filename, keypos)
{
  dist_computed = false;
  cache = CachePtr();

  setDimensions(si);

  ComputeDistribution(begin);
}

EnsembleKD::EnsembleKD(StateInfo *si) : Ensemble(si)
{
  dist_computed = false;
  cache = CachePtr();

  setDimensions(si);
}

EnsembleKD::EnsembleKD(const EnsembleKD &p)
{
  dist_computed = p.dist_computed;
  _si    = p._si;
  cnts   = p.cnts;
  mcnt   = p.mcnt;
  vlower = p.vlower;
  vupper = p.vupper;
  vmean  = p.vmean;
  vvar   = p.vvar;
  mmean  = p.mmean;
  mvar   = p.mvar;
  peak   = p.peak;

  if (cache) cache = CachePtr(new StateCache(*(p.cache)));
  dataScale = p.dataScale;
}


EnsembleKD::EnsembleKD(Ensemble *p) : Ensemble(*p)
{
  dist_computed = false;
  cache = CachePtr();

  ComputeDistribution();
}

void EnsembleKD::Reset(StateInfo* si,
		       int level, int begin, string filename, int keypos)
{
  dist_computed = false;
  cache = CachePtr();

  Ensemble::Reset(si, level, begin, filename, keypos);
}

void EnsembleKD::Reset(StateInfo *si)
{
  dist_computed = false;
  cache = CachePtr();

  Ensemble::Reset(si);
}

void EnsembleKD::ComputeDistribution(void)
{
  if (!dimensions_set) {
    string msg = "Must call EnsembleKD::SetDimensions before "
      "computing the distribution";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (dist_computed) return;

  if (simulationInProgress == 0) Broadcast();
				// Non-mixture defaults
  unsigned n=_si->Ntot;
  vector< vector<unsigned> > MAP(2);
  bool mixture = false, rjtwo = false;

  if (_si->ptype == StateInfo::RJTwo) {
    for (unsigned i=1; i<=_si->T; i++) {
      MAP[0].push_back(i);
      MAP[1].push_back(i);
    }
    for (unsigned i=_si->T+1; i<=_si->T+_si->N1; i++) 
      MAP[0].push_back(i);

    for (unsigned i=_si->T+_si->N1+1; i<=_si->T+_si->N1+_si->N2; i++) 
      MAP[1].push_back(i);

    rjtwo = true;
  }

				// Mixture settings
  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) 
    {
      mixture = true;
    }

				// Clean up working arrays
  cnts   .erase(cnts  .begin(), cnts  .end());
  ccum   .erase(ccum  .begin(), ccum  .end());
  mean   .erase(mean  .begin(), mean  .end());
  covar  .erase(covar .begin(), covar .end());

  vmean  .erase(vmean .begin(), vmean .end());
  vvar   .erase(vvar  .begin(), vvar  .end());

  mmean  .erase(mmean .begin(), mmean .end());
  mvar   .erase(mvar  .begin(), mvar  .end());
  covarM .erase(covarM.begin(), covarM.end());

  peak   .erase(peak  .begin(), peak  .end());

  vlower = std::vector<double>(_si->Ntot,  DBL_MAX);
  vupper = std::vector<double>(_si->Ntot, -DBL_MAX);

  map<int, double> ppeak;

  ofstream out;

  if (verbose) {
    ostringstream sout; sout << "EnsembleKD.log." << _id;
    out.open(sout.str().c_str());
    out.precision(6);
    out << "ComputeDistribution: initialized, count=" << count << endl;
  }
				// Putting check here makes sure that
				// "clean" mean & stdev is returned to
				// any diagnostic routines
  if (count<2) return;

  ibeg = max<int>(0, burnIn - offset);

				// Use some sane value . . . 
  if (ibeg >= count) {
    if (myid==0)
      std::cout << "EnsembleKD: too few states in remaining chain. "
		<< "Discarding the first half (" << count/2
		<< ") and continuing" << std::endl;
    ibeg = count/2;
  }

  if (burnIn - offset > count) {
    ostringstream msg;
    msg << "Logic error: requested burnIn=" << burnIn 
	<< " is larger than the number of recorded states=" << offset+count;
    throw InternalError(msg.str(), __FILE__, __LINE__);
  }

  if (verbose_debug)
    cout << "EnsembleKD: using " << count-ibeg << " states in ensemble"
	 << endl;

  // Setting unity log probability scaling by default
  dataScale = vector<double>(states[0].prob.size(), 0.0);

  for (int i=ibeg; i<count; i++) {
    unsigned m = 1;
    if (rjtwo)   m = states[i].p.M();
    if (mixture) m = states[i].p.M();
    n = states[i].p.N();

    // Get lower and upper bounds
    for (unsigned k=0; k<n; k++) {
      vlower[k] = std::min<double>(vlower[k], states[i].p.vec(k));
      vupper[k] = std::max<double>(vupper[k], states[i].p.vec(k));
    }

    // Compute peak
    if (ppeak.find(m) == ppeak.end()) {
      ppeak[m] = -DBL_MAX;
    }
    if (ppeak[m] < states[i].prob[0]) {
      ppeak[m] = states[i].prob[0];
      peak [m] = states[i].p.vec();
    }

    // Accumulate subspace counts
    addCount(m);

    // Mean and covariance are done _universally_, independent of the
    // State vector packing

    if (rjtwo) {

      // Pass #1
      for (unsigned k1=0; k1<MAP[m-1].size(); k1++)
	mean[m][1 + k1] += states[i].p[MAP[m-1][k1]];
      
    } else {

      // Pass #1
      for (unsigned k1=0; k1<n; k1++)
	mean[m][1 + k1] += states[i].p[k1];

    }

    vector<double> pt, wd;

    if (mixture) {
      // Skip the last mixture weight since
      // it is a dependent parameter
      for (unsigned k1=0; k1<m-1; k1++) {
	pt.push_back(states[i].p[k1]);
	wd.push_back(0.1);
      }
      for (unsigned k1=m; k1<n; k1++) {
	pt.push_back(states[i].p[k1]);
	wd.push_back(0.1);
      }

    } else {

      if (rjtwo) {
	n = MAP[m-1].size();
	for (unsigned k1=0; k1<n; k1++) {
	  pt.push_back(states[i].p[MAP[m-1][k1]]);
	  wd.push_back(0.1);
	}

      } else {
	// Use all of the vector elements for non mixtures
	for (unsigned k1=0; k1<n; k1++) {
	  pt.push_back(states[i].p[k1]);
	  wd.push_back(0.1);
	}
      }
    }

    points[m].push_back(pt);
  }

  for (auto v : cnts) {
    unsigned m = v.first;
    int      N = v.second;
    if (N>0) mean[m] /= N; 
  }

  for (int i=ibeg; i<count; i++) {
    unsigned m = 1;
    if (rjtwo)   m = states[i].p.M();
    if (mixture) m = states[i].p.M();
    n = states[i].p.N();

    // Mean and covariance are done _universally_, independent of the
    // State vector packing

    if (rjtwo) {
      // Pass #2
      for (unsigned k1=0; k1<MAP[m-1].size(); k1++) {
	double dif1 = states[i].p[MAP[m-1][k1]] - mean[m][1 + k1];
	for (unsigned k2=0; k2<MAP[m-1].size(); k2++) {
	  double dif2 = states[i].p[MAP[m-1][k2]] - mean[m][1 + k2];
	  covar[m][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
    } else {
      // Pass #2
      for (unsigned k1=0; k1<n; k1++) {
	double dif1 = states[i].p[k1] - mean[m][1 + k1];
	for (unsigned k2=0; k2<n; k2++) {
	  double dif2 = states[i].p[k2] - mean[m][1 + k2];
	  covar[m][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
    }
  }


  for (auto v : cnts) {

    int m = v.first;
    int N = v.second;

    if (N>1) 
      covar[m] /= N - 1;
    else
      covar[m].zero();

    //--------------------------------------------------
    // Check for unique states
    //--------------------------------------------------
  
    sort(points[m].begin(), points[m].end(), compare_vector);

    vector< vector<double> >::iterator it  = points[m].begin();
    vector< vector<double> >::iterator itl = points[m].begin();
    unsigned dup = 1, ndim = points[m][0].size();

    //
    // Some debug output
    //
    if (0) {
      ostringstream sout;
      sout << "test." << m << ".dat";
      ofstream tout(sout.str().c_str());
      for (size_t j=0; j<points[m].size(); j++) {
	for (unsigned k=0; k<ndim; k++) tout << setw(18) << points[m][j][k];
	tout << endl;
      }
    }

    while (++it != points[m].end()) {
      
      bool ok = false;

      // Not a duplicate if at least one dimension differs!
      for (unsigned n=0; n<ndim; n++) {
	if (fabs((*it)[n] - (*itl)[n]) > 1.0e-10) {
	  ok = true;
	  break;
	}
      }

      if (ok) {
	weight[m].push_back(dup);
				// Erase duplicates from list
	if (it>itl+1) points[m].erase(itl+1, it);
	it   = ++itl;		// Advance to next point
	dup  = 1;
      } else {
	dup++;			// Augment weight for duplicate
      }
    }

    weight[m].push_back(dup);
    if (points[m].end()>itl+1) points[m].erase(itl+1, points[m].end());
    
    if (points[m].size() != weight[m].size()) {
      ostringstream sout;
      sout << "EnsembleKD: duplicate removal error, pt size="
	   << points[m].size() << ", wt size=" << weight[m].size();
      throw InternalError(sout.str(), __FILE__, __LINE__);
    }

    //--------------------------------------------------
    // Check again for duplicates (debugging)
    //--------------------------------------------------
  
    if (verbose_debug) {
      it  = points[m].begin();
      itl = points[m].begin();
      unsigned cnt1 = 0;

      while (++it != points[m].end()) {

	bool ok = false;

	// Not a duplicate if at least one dimension differs!
	for (unsigned n=0; n<ndim; n++) {
	  if (fabs((*it)[n] - (*itl)[n]) > 1.0e-10) {
	    ok = true;
	    break;
	  }
	}
	
	if (!ok) cnt1++;
	++itl;
      }

      if (cnt1) {
	cout << "EnsembleKD::ComputeDistribution debug info: "
	     << " still have " << cnt1 << " duplicates after removal" << endl;
      } else {
	cout << "EnsembleKD::ComputeDistribution debug info: "
	     << " NO duplicates after removal (good!)" << endl;
      }
    }

    int L = points[m].size();

    if (L < N && verbose_debug) {
      cout << "EnsembleKD::ComputeDistribution debug info: "
	   << "subspace " << m << ": " << N - L << " duplicates" << endl;
    }

    vector<double> beta(weight[m].size(), 1.0);

    KDTree::verbose_debug = verbose_debug;

    kde[m] = boost::shared_ptr<KDTree>
      (new KDTree(points[m][0].size(), L, ncut, points[m], weight[m], beta));

    kde[m]->createMeasureList(ncut, points[m]);

    if (_si->ptype == StateInfo::RJTwo)
      n = MAP[m-1].size();

    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) {
      
      n = (_si->Ndim+1)*m;

      for (int c=0; c<m; c++) {
	vector< pair<vector<double>, double> > V(L);
	for (int j=0; j<L; j++) {
	  for (size_t k=0; k<_si->Ndim; k++) {
	    V[j].first.push_back(points[m][j][m-1+_si->Ndim*c+k]);
	    V[j].second = weight[m][j];
	  }
	}

	//--------------------------------------------------
	// Check for unique states among mixture components
	//--------------------------------------------------
  
	sort(V.begin(), V.end(), compare_vector_pair);
	
	vector< pair<vector<double>, double> >::iterator it  = V.begin();
	vector< pair<vector<double>, double> >::iterator itl = V.begin();

	double dup = it->second;

	while (++it != V.end()) {

	  bool ok = false;
      
	  //--------------------------------------------------
	  // Not a duplicate if at least one dimension differs!
	  //--------------------------------------------------
	  for (unsigned n=0; n<_si->Ndim; n++) {
	    if (fabs((it->first)[n] - (itl->first)[n]) > 1.0e-10) {
	      ok = true;
	      break;
	    }
	  }
    
	  if (ok) {		// Erase duplicates from list
	    if (it>itl+1) V.erase(itl+1, it);
	    itl->second = dup;	// Assign new weight
	    it   = ++itl;	// Advance to next point
	    dup  = it->second;	// Initialize sumof weights
	  } else {
	    dup += it->second;	// Augment weight for duplicate
	  }
	}

	itl->second = dup;
	if (V.end()>itl+1) V.erase(itl+1, V.end());

	//--------------------------------------------------
	// Make points and weights from the pairs
	//--------------------------------------------------
	
	vector< vector<double> > v;
	vector<double> w, b;
	for (it=V.begin(); it!=V.end(); it++) {
	  v.push_back(it->first);
	  w.push_back(it->second);
	  b.push_back(1.0);
	}

	int K = v.size();

	if (K < L && verbose_debug) {
	  cout << "EnsembleKD::ComputeDistribution debug info: "
	       << "subspace " << m << ", component " << c + 1
	       << ": " << L - K << " duplicates" << endl;
	}

	kdeM[m].push_back
	  (boost::shared_ptr<KDTree>(new KDTree(_si->Ndim, K, ncut, v, w, b)));

	kdeM[m].back()->createMeasureList(ncut, v);
      }
    }

    if (N<2) covar[m].zero();

  }
  
  if (verbose)
    out << "ComputeDistribution: covariance matrix done" << endl;

				// Cache return vectors for mean and variance
  for (auto v : cnts) {

    int m = v.first;
    int N = v.second;
    int n = _si->Ntot;
    
    if (_si->ptype == StateInfo::RJTwo) n = MAP[m-1].size();

    if (_si->ptype == StateInfo::Mixture ||
        _si->ptype == StateInfo::Extended   ) n = (_si->Ndim+1)*m;
    
    if (verbose) {
      out << "ComputeDistribution: m=" << m 
	  << " counts=" << N << endl;
      if (cnts[m]) {
	out << "ComputeDistribution: about to erase at m=" << m 
	    << ", ndim=" << vmean[m].size() << ", "
	    << vvar[m].size() << endl;
	
	out << "Mean:" << endl;
	for (int k1=1; k1<=n; k1++)
	  out << setw(16) << mean[m][k1] << endl;
	out << endl;

	out << "Covariance:" << endl;
	for (int k1=1; k1<=n; k1++) {
	  for (int k2=1; k2<=n; k2++) 
	    out << setw(16) << covar[m][k1][k2];
	  out << endl;
	}
      }
      else
	out << "ComputeDistribution: skipping m=" << m << endl;
    }

    vmean[m].erase(vmean[m].begin(), vmean[m].end());
    vvar [m].erase(vvar [m].begin(), vvar [m].end());

    if (verbose)
      out << "ComputeDistribution: mean & cov, m=" << m << endl;

    for (int i=0; i<n; i++) {
      vmean[m].push_back(mean [m][1+i]);
      vvar [m].push_back(covar[m][1+i][1+i]);
    }

    if (verbose) {
      out << "Mean and Var:" << endl;
      for (int i=0; i<n; i++)
	out << setw(5) << i+1 << ": "
	    << setw(16) << vmean[m][i]
	    << setw(16) << vvar [m][i]
	    << endl;
    }
  }

  if (verbose)
    out << "ComputeDistribution: mean and var done" << endl;

  makeSampleFraction();

  dist_computed = true;

  cache = CachePtr(new StateCache(ibeg, states, key_pos));

  if (verbose) {
    out << "Counts for sampling: " << ibeg << ", " << count-1 << endl;
    out << "Limits: MinC=" << cnts.begin()->first 
	<< "  MaxC=" << cnts.rbegin()->first << endl;
    out << "Cumulative states:" << endl;
    
    for (mapIDiter it=ccum.begin(); it!=ccum.end(); it++)
      out << setw(5) << it->first << ": " << it->second << endl;
  }

  
  ComputeDistributionMarginal(out);

  if (verbose)
    out << "ComputeDistribution: everything done" << endl;
}


void EnsembleKD::RestoreDistribution(void)
{
  vector< vector<unsigned> > MAP(2);

  if (_si->ptype == StateInfo::RJTwo) {
    for (unsigned i=1; i<=_si->T; i++) {
      MAP[0].push_back(i);
      MAP[1].push_back(i);
    }
    for (unsigned i=_si->T+1; i<=_si->T+_si->N1; i++) 
      MAP[0].push_back(i);

    for (unsigned i=_si->T+_si->N1+1; i<=_si->T+_si->N1+_si->N2; i++) 
      MAP[1].push_back(i);
  }


  for (auto v : cnts) {

    int m = v.first;
    int N = v.second;
    int L = points[m].size();

    if (L < N && verbose_debug) {
      cout << "EnsembleKD::RestoreDistribution debug info: "
	   << "subspace " << m << ": " << N - L << " duplicates" << endl;
    }

    vector<double> beta(weight[m].size(), 1.0);

    kde[m] = boost::shared_ptr<KDTree>
      (new KDTree(points[m][0].size(), L, ncut, points[m], weight[m], beta));

    kde[m]->createMeasureList(ncut, points[m]);

    if (_si->ptype == StateInfo::RJTwo)

    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) {
      
      for (int c=0; c<m; c++) {
	vector< pair<vector<double>, double> > V(L);
	for (int j=0; j<L; j++) {
	  for (size_t k=0; k<_si->Ndim; k++) {
	    V[j].first.push_back(points[m][j][m-1+_si->Ndim*c+k]);
	    V[j].second = weight[m][j];
	  }
	}

	//--------------------------------------------------
	// Check for unique states among mixture components
	//--------------------------------------------------
  
	sort(V.begin(), V.end(), compare_vector_pair);
	
	vector< pair<vector<double>, double> >::iterator it  = V.begin();
	vector< pair<vector<double>, double> >::iterator itl = V.begin();

	double dup = it->second;

	while (++it != V.end()) {

	  bool ok = false;
      
	  //--------------------------------------------------
	  // Not a duplicate if at least one dimension differs!
	  //--------------------------------------------------
	  for (unsigned n=0; n<_si->Ndim; n++) {
	    if (fabs((it->first)[n] - (itl->first)[n]) > 1.0e-10) {
	      ok = true;
	      break;
	    }
	  }
    
	  if (ok) {		// Erase duplicates from list
	    if (it>itl+1) V.erase(itl+1, it);
	    itl->second = dup;	// Assign new weight
	    it   = ++itl;	// Advance to next point
	    dup  = it->second;	// Initialize sumof weights
	  } else {
	    dup += it->second;	// Augment weight for duplicate
	  }
	}

	itl->second = dup;
	if (V.end()>itl+1) V.erase(itl+1, V.end());

	//--------------------------------------------------
	// Make points and weights from the pairs
	//--------------------------------------------------
	
	vector< vector<double> > v;
	vector<double> w, b;
	for (it=V.begin(); it!=V.end(); it++) {
	  v.push_back(it->first);
	  w.push_back(it->second);
	  b.push_back(1.0);
	}

	int K = v.size();

	if (K < L && verbose_debug) {
	  cout << "EnsembleKD::ComputeDistribution debug info: "
	       << "subspace " << m << ", component " << c + 1
	       << ": " << L - K << " duplicates" << endl;
	}

	kdeM[m].push_back
	  (boost::shared_ptr<KDTree>(new KDTree(_si->Ndim, K, ncut, v, w, b)));

	kdeM[m].back()->createMeasureList(ncut, v);
      }
    }
  }

}


void EnsembleKD::ComputeDistributionMarginal(ostream& out)
{
  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo ) return;

				// Clean up working arrays
  for (auto v : cnts) {
    int m = v.first;
    mcnt[m] = 0;
    mcum[m] = 0;
    for (int mm=0; mm<m; mm++) {
      meanM [m][mm].zero();
      covarM[m][mm].zero();
    }
  }

  vector<double> pt(_si->Ndim), wd(_si->Ndim, 0.1);


  for (int i=ibeg; i<count; i++) {
    
    unsigned m = states[i].p.M();

				// Make a stanza for this mixture?
    if (pointsM.find(m) == pointsM.end()) {
      pointsM[m] = vector< vector< vector<double> > > (m);
      weightM[m] = vector< vector<double> > (m);
    }
      
    mcnt[m]++;

    for (unsigned mm=0; mm<m; mm++) {

      pt = states[i].p.Phi(mm);
	
      for (unsigned k1=0; k1<_si->Ndim; k1++)
	meanM[m][mm][1 + k1] += pt[k1];

      pointsM[m][mm].push_back(pt);
      weightM[m][mm].push_back(1);
    }
  }

  for (auto v : mcnt) {
    unsigned m = v.first;
    int N      = v.second;
    for (unsigned mm=0; mm<m; mm++) {
      for (unsigned k1=0; k1<_si->Ndim; k1++)
	meanM[m][mm][1 + k1] /= N;
    }
  }

  for (int i=ibeg; i<count; i++) {
    unsigned m = states[i].p.M();
    for (unsigned mm=0; mm<m; mm++) {
      pt = states[i].p.Phi(mm);
      for (unsigned k1=0; k1<_si->Ndim; k1++) {
	double dif1 = pt[k1] - meanM[m][mm][1 + k1];
	for (unsigned k2=0; k2<_si->Ndim; k2++) {
	  double dif2 = pt[k2] - meanM[m][mm][1 + k2];
	  covarM[m][mm][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
    }
  }

  for (auto v : mcnt) {
    unsigned m = v.first;
    int      N = v.second;
    for (unsigned mm=0; mm<m; mm++) {
      if (N>1) {
	covarM[m][mm] /= N - 1;
      } else {
	covarM[m][mm].zero();
      }
    }
  }

  if (verbose) {
    out << "ComputeDistributionMarginal: points sorted" << endl;
    for (map<int, vector< vector< vector<double> > > >::iterator 
	   it = pointsM.begin(); it != pointsM.end(); it++)
      
      out << "#" << setw(3) << it->first << ": number=" 
	  << it->second[0].size() << endl;
  }


  if (verbose)
    out << "ComputeDistributionMarginal: covariance matrix done" << endl;

				// Cache return vectors for mean and variance
  for (auto v : mcnt) {

    unsigned m = v.first;
    int N      = v.second;

    if (verbose) {
      out << "ComputeDistribution: m=" << m 
	  << " counts=" << N << endl;
      if (mcnt[m]) {
	out << "ComputeDistribution: about to erase at m=" << m 
	    << ", ndim=" << mmean[m].size() << ", "
	    << mvar[m][0].size() << endl;
	
	out << "Marginal covariance:" << endl;
	for (unsigned mm=0; mm<m; mm++) {
	  out << "===> Component " << mm+1 << endl;
	  for (unsigned k1=1; k1<=_si->Ndim; k1++) {
	    for (unsigned k2=1; k2<=_si->Ndim; k2++) 
	      out << setw(16) << covarM[m][mm][k1][k2];
	    out << endl;
	  }
	}
      }
      else
	out << "ComputeDistribution: skipping m=" << m << endl;
    }

    if (!mcnt[m]) continue;

    for (unsigned mm=0; mm<m; mm++) {
      mmean[m][mm].erase(mmean[m][mm].begin(), mmean[m][mm].end());
      mvar [m][mm].erase( mvar[m][mm].begin(), mvar[m][mm].end());
    }

    if (verbose)
      out << "ComputeDistribution: marginal mean & cov, m=" << m << endl;
    
    for (unsigned mm=0; mm<m; mm++) {
      for (unsigned i=0; i<_si->Ndim; i++) {
	mmean[m][mm].push_back(meanM [m][mm][1+i]);
	mvar [m][mm].push_back(covarM[m][mm][1+i][1+i]);
      }
    }

    
    if (verbose) {
      out << "Mean and Var:" << endl;
      for (unsigned mm=0; mm<m; mm++) {
	out << "===> Component " << mm+1 << endl;
	for (unsigned i=0; i<_si->Ndim; i++)
	  out << setw(5) << i+1 << ": "
	      << setw(16) << mmean[m][mm][i]
	      << setw(16) << mvar [m][mm][i]
	      << endl;
      }
    }
  }
  

  if (verbose)
    out << "ComputeDistribution: mean and var done" << endl;

				// Throw out subspaces that do not meet our
				// minimum occupation requirement
  list<int> er;
  for (auto v : mcnt) {
    if (v.second < minsub) er.push_back(v.first);
  }
  for (list<int>::iterator it=er.begin(); it!=er.end(); it++) {
    mcnt.erase(*it);
    mcum.erase(*it);
  }

				// Cumulative distribution for 
				// component states
  mapIIiter cur=mcnt.begin(), lst=mcnt.begin();
  for (; cur!=mcnt.end(); cur++) {
    mcum[cur->first] = cur->second;
    if (cur!=lst) mcum[cur->first] += lst->second;
  }
  double norm = mcnt.rbegin()->second;
  for (auto &v : mcnt) v.second /= norm;
  mcnt.rbegin()->second = 1.0;

  if (verbose) {
    out << "Cumulative states:" << endl;
    for (mapIDiter j=mcum.begin(); j!=mcum.end(); j++)
      out << setw(5) << j->first << ": " << j->second << endl;
  }
  
}

//
// Use the convariance analysis to compute the posterior probability
// based on the simulation data
//
double EnsembleKD::PDF(State& p)
{
  double ret;

  if (!dist_computed) ComputeDistribution();

  if (p.Type() == StateInfo::None || p.Type() == StateInfo::Block) {
    
    vector<double> pt;
    for (unsigned int i=0; i<p.N(); i++) pt.push_back(p[i]);

    ret = kde[1]->Density(pt);

    if (ret<=0.0) ret = 0.0;

  } else {

				// Number of components in this state
    unsigned m = p.M();
  
    if (m > _si->M) {
      throw DimNotMatchException(__FILE__, __LINE__);
    }

    vector<double> pt, val;
    for (unsigned int i=0; i<m-1  ; i++) pt.push_back(p[i]);
    for (unsigned int i=m; i<p.N(); i++) pt.push_back(p[i]);

    double prob1 = kde[m]->Density(pt);
    double prob2 = PDFSubspace(m);

    if (prob1<=0.0 || prob2<=0.0) ret = 0.0;
    else ret = prob1 * prob2 * exp(dataScale[0]);
  }

  return ret;
}

double EnsembleKD::logPDF(State& p)
{
  double ret;

  if (!dist_computed) ComputeDistribution();

  if (p.Type() == StateInfo::None || p.Type() == StateInfo::Block) {
    
    vector<double> pt;
    for (unsigned int i=0; i<p.N(); i++) pt.push_back(p[i]);

    double prob = kde[1]->Density(pt);

    if (prob<=0.0) ret = -INFINITY;
    else ret = log(prob);

  } else {

				// Number of components in this state
    unsigned m = p.M();
  
    if (m > _si->M) {
      throw DimNotMatchException(__FILE__, __LINE__);
    }
    
    vector<double> pt;
    for (unsigned int i=0; i<m-1  ; i++) pt.push_back(p[i]);
    for (unsigned int i=m; i<p.N(); i++) pt.push_back(p[i]);

    double prob1 = kde[m]->Density(pt);
    double prob2 = PDFSubspace(m);

    if (prob1<=0.0 || prob2<=0.0)
      ret = -INFINITY;
    else
      ret = log(prob1) + log(prob2) + dataScale[0];
  }

  return ret;
}

double EnsembleKD::logPDFMarginal(unsigned m, unsigned n, vector<double>& p)
{
  if (!dist_computed) ComputeDistribution();

  if (_si->ptype == StateInfo::None || _si->ptype == StateInfo::Block) {
    string msg = "You can not call logPDFMarginal if the state is not a mixture!";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (m > _si->M) {
    throw DimNotMatchException(__FILE__, __LINE__);
  }

  unsigned psz = p.size();

  if (_si->Ndim > psz) {
      throw DimNotMatchException(__FILE__, __LINE__);
  }

  vector<double> pt;
  for (unsigned i=0; i<m-1; i++) pt.push_back(p[i]);
  for (unsigned i=m; i<psz; i++) pt.push_back(p[i]);
  
  double prob = kdeM[m][n]->Density(pt);

  if (prob<=0.0) return -INFINITY;

  return log(prob) + dataScale[0];
}

//
// Generates variates from the same estimated posterior probability
// distribution
//
State EnsembleKD::Sample(unsigned m)
{
  return cache->Sample(m);
}

vector<double> EnsembleKD::SampleMarginal(unsigned m, unsigned n)
{
  vector<double> p = cache->Sample(m);
  vector<double> ret;
  for (unsigned k=0; k<_si->Ndim; k++) ret.push_back(p[1+m+n*_si->Ndim+k]);
  return ret;
}

vector<double> EnsembleKD::lower(void)
{
  if (!dist_computed) ComputeDistribution();
  if (vlower.size() == _si->Ntot) return vlower;
  return vector<double>(_si->Ntot, -DBL_MAX);
}

vector<double> EnsembleKD::upper(void)
{
  if (!dist_computed) ComputeDistribution();
  if (vupper.size() == _si->Ntot) return vupper;
  return vector<double>(_si->Ntot, DBL_MAX);
}

vector<double>  EnsembleKD::Mean(unsigned m)
{
  if (!dist_computed) ComputeDistribution();
  return vmean[m];
}

vector<double>  EnsembleKD::StdDev(unsigned m)
{
  unsigned n = _si->Ntot;
  unsigned M = 1;

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended ) {

    if (m>_si->M || m<1) {
      ostringstream sout;
      sout << "EnsembleKD::StdDev: called with m=" << m
	   << " but Mmix=" << _si->M;
      throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
    }
    n = (_si->Ndim+1)*m;
    M = m;
  }

  if (!dist_computed) ComputeDistribution();
  vector<double> stddev;
  for (unsigned i=1; i<n; i++) 
    stddev.push_back(sqrt(vvar[M-1][i]));
    
  return stddev;
}

vector<double> EnsembleKD:: Moments(unsigned m, unsigned i)
{

  ostream cout(checkTable("cli_console"));

  if (!dist_computed) ComputeDistribution();
  vector<double> moms;
  switch (i) {
  case 0:
    for (unsigned i=0; i<m; i++) moms.push_back(1.0);;
    break;
  case 1:
    for (unsigned i=0; i<m; i++) moms.push_back(vmean[m][i]);
    break;
  case 2:
    for (unsigned i=0; i<m; i++) moms.push_back(vvar[m][i] + 
						vmean[m][i]*vmean[m][i]);
    break;
  default:
    cout << "EnsembleKD::Moments: moments greater than 2 are not defined\n";
  }

  return moms;
}

void EnsembleKD::PrintDiag(string& outfile)
{
  std::ofstream out;

  if (myid==0) {
    out.open(outfile.c_str(), ios::out | ios::app);
  }

  PrintDiag(out);
}

void EnsembleKD::PrintDiag(ostream& out)
{
  ComputeDistribution();

  if (myid==0) {

    out << endl
	<< "My instance=" << this << endl
	<< "Total counts=" << count << endl
	<< "Counts per component" << endl;
    
    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++)
      out << setw(5)  << it->first << ": " 
	  << setw(8)  << it->second
	  << setw(15) << ccum[it->first] 
	  << endl;
    out << endl;
    
    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
      int m = it->first;
      int N = it->second;
      
      if (N<2) continue;
      
      out << endl << endl << "*** " << m << " ***" << endl;
      if (_si->ptype == StateInfo::Mixture || 
	  _si->ptype == StateInfo::Extended   ) out << m;
      else out << "non-mixture";
      out << " [" << (double)N/count << "] ***" << endl;
      
      for (int k=mean[m].getlow(); k<=mean[m].gethigh(); k++) {
	out << setw(5)  << k 
	    << setw(15) << mean[m][k] 
	    << setw(15) << covar[m][k][k]
	    << setw(15) << sqrt(covar[m][k][k])
	    << endl;
      }
      out << endl;
      out << endl;
      for (int i=covar[m].getrlow(); i<=covar[m].getrhigh(); i++) {
	for (int j=covar[m].getclow(); j<=covar[m].getchigh(); j++)
	  out << setw(15) << covar[m][i][j];
	out << endl;
      }
      out << endl;
    }

    out << endl;
    out << endl;
  }

}

void EnsembleKD::PrintDiag()
{
  PrintDiag(cout);
}

void EnsembleKD::addCount(int m)
{
  mapIIiter it = cnts.find(m);
  if (it != cnts.end()) it->second++;
  else {
    cnts[m] = 1;
    mcnt[m] = 0;
    mcum[m] = 0;

    int n = _si->Ntot;

    if (_si->ptype == StateInfo::RJTwo) {
      if (m==1) n = _si->T + _si->N1;
      else      n = _si->T + _si->N2;
    }
    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) n = (_si->Ndim+1)*m;

    //
    // Allocate and initialize to zero
    //
    vmean[m] = vector<double>(n, 0.0);
    vvar [m] = vector<double>(n, 0.0);
    //
    mmean[m] = vector< vector<double> >(m);
    mvar [m] = vector< vector<double> >(m);
    for (int mm=0; mm<m; mm++) {
      mmean[m][mm] = vector<double>(n, 0.0);
      mvar [m][mm] = vector<double>(n, 0.0);
    }
    //
    mean [m] = VectorM(1, n);
    covar[m] = MatrixM(1, n, 1, n);
    mean [m].zero();
    covar[m].zero();
    //
    meanM [m] = vector<VectorM>(m);
    covarM[m] = vector<MatrixM>(m);
    for (int mm=0; mm<m; mm++) {
      meanM [m][mm].setsize(1, n);
      covarM[m][mm].setsize(1, n, 1, n);
      meanM [m][mm].zero();
      covarM[m][mm].zero();
    }
  }
}

void EnsembleKD::PrintDensity(int dim1, int dim2, int num1, int num2,
			      string file)
{
  // Only root node needs to do this
  if (myid) return;

  // Sanity check
  if (dim1 == dim2) {
    cout << "This routine provides a density cut through the plane specifed by dim1  " << std::endl
	 << "and dim2.  Therefore, dim1 must NOT EQUAL dim2.  Please try again . . . " << std::endl;
    return;
  }

  if (_si->ptype == StateInfo::RJTwo) {
    cout << "PrintDensity is not implemented for an RJTwo state" << endl;
    return;
  }

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {

    if (static_cast<unsigned>(dim1) >= _si->M*_si->Ndim || 
	static_cast<unsigned>(dim2) >= _si->M*_si->Ndim) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ndim=" << _si->Ndim << endl;
      return;
    }
  } else {

    if (static_cast<unsigned>(dim1) >= _si->Ntot || 
	static_cast<unsigned>(dim2) >= _si->Ntot) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ntot=" << _si->Ntot << endl;
      return;
    }
  }
    
  ofstream out(file.c_str());
  if (!out) {
    cout << "PrintDensity: could not open file <" << file << "> for output"
	 << endl;
    return;
  }

  ComputeDistribution();
  
  State p(_si);
  
  unsigned M = 1, prefix = 0;

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {
    
    double dmax = 0.0;

    for (unsigned m=0; m<fraction.size(); m++) {
      if (fraction[m].first > dmax) {
	dmax = fraction[m].first;
	M    = fraction[m].second;
      }
    }

    if (dmax <= 0.0) {
      cout << "PrintDensity: could not find any states, strange" << endl;
      return;
    }

    unsigned nd = _si->Ndim;

    vector<double> wght(M);
    vector< vector<double> > phi(M);

    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
      int m   = it->first;
      wght[m] = peak[M][m];
      phi[m]  = vector<double>(nd);
      for (unsigned n=0; n<nd; n++) phi[m][n] = peak[M][M+nd*m+n];
    }
    if (_si->ptype == StateInfo::Mixture) {
      p.setState(M, wght, phi);
    } else {
      vector<double> ext(_si->Next);
      for (unsigned n=0; n<_si->Next; n++) peak[M][M*(1+nd)+n];
      p.setState(M, wght, phi, ext);
    }

    prefix = M;

  } else {
    p.setState(peak[M]);
  }

  double xmin = lower()[prefix + dim1];
  double xmax = upper()[prefix + dim1];

  double ymin = lower()[prefix + dim2];
  double ymax = upper()[prefix + dim2];

  double dx   = (xmax - xmin)/(num1-1);
  double dy   = (ymax - ymin)/(num2-1);

  out << "# [" << xmin << ", " << ymin 
      << "] ==> [" << xmax << ", " << ymax << "]" << std::endl;

  for (int i=0; i<num1; i++) {

    p.vec(prefix + dim1) = xmin + dx*i;
    
    for (int j=0; j<num2; j++) {
    
      p.vec(prefix + dim2) = ymin + dy*j;

      out << setw(18) << xmin + dx*i
	  << setw(18) << ymin + dy*j
	  << setw(18) << PDF(p)
	  << endl;
    }
    out << endl;
  }

}


