#include <ACG.h>
#include <Uniform.h>
#include <BetaRDist.h>

#include <boost/math/special_functions/beta.hpp>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BetaRDist)

using namespace BIE;

BetaRDist::BetaRDist()
{
  _type  = NormalAdd;
  shape  = 0.0;
  dim    = 1;
  center = vector<double>(1, 0.0);
  scale  = vector<double>(1, 1.0);
  limits = vector<double>(1, DBL_MAX);
  mincdf = 0.0;
  maxcdf = 1.0;

  computeNorm();
}

BetaRDist::BetaRDist(double a, clivectord* c)
{
  _type  = NormalAdd;
  shape  = a;
  center = (*c)();
  dim    = center.size();
  scale  = vector<double>(dim, 1.0);
  limits = vector<double>(dim, DBL_MAX);
  mincdf = 0.0;
  maxcdf = 1.0;

  computeNorm();
}

BetaRDist::BetaRDist(double a, clivectord* c, clivectord* s)
{
  _type  = NormalAdd;
  shape  = a;
  center = (*c)();
  scale  = (*s)();
  if (center.size() != scale.size()) {
    ostringstream msg;
    msg << "Center (=" << center.size() << ") and Scale (="
	<< scale.size() << ") dimensions must match!" << endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }
  dim    = center.size();
  limits = vector<double>(dim, DBL_MAX);
  mincdf = 0.0;
  maxcdf = 1.0;

  computeNorm();
}

BetaRDist::BetaRDist(double a, clivectord* c, clivectord *s, clivectord* l)
{
  _type  = NormalAdd;
  shape  = a;
  center = (*c)();
  scale  = (*s)();
  limits = (*l)();
  if (center.size() != scale.size() || center.size() != limits.size()) {
    ostringstream msg;
    msg << "Center (=" << center.size() << "), Scale (="
	<< scale.size() << ") and Limit (=" << limits.size()
	<< ") dimensions must match!" << endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }
  dim    = center.size();
  mincdf = 0.0;

  StateInfo si(dim);
  State st(&si);
  st = center;
  for (unsigned j=0; j<dim; j++) st[j] += limits[j];
  maxcdf = CDF(st);

  computeNorm();
}

BetaRDist::BetaRDist(double a,
		     std::vector<double> c,
		     std::vector<double> s,
		     std::vector<double> l)
{
  _type  = NormalAdd;
  shape  = a;
  center = c;
  dim    = center.size();

  if (scale.size() ==0) scale.resize(dim, 1.0);
  else scale = s;

  if (limits.size()==0) limits.resize(dim, DBL_MAX);
  else limits = l;
  
  if (center.size() != scale.size() || center.size() != limits.size()) {
    ostringstream msg;
    msg << "Center (=" << center.size() << "), Scale (="
	<< scale.size() << ") and Limit (=" << limits.size()
	<< ") dimensions must match!" << endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }
  dim    = center.size();
  mincdf = 0.0;

  StateInfo si(dim);
  State st(&si);
  st = center;
  for (unsigned j=0; j<dim; j++) st[j] += limits[j];
  maxcdf = CDF(st);

  computeNorm();
}

void BetaRDist::computeNorm()
{
  norm = 1.0;
  for (unsigned j=0; j<dim; j++) norm *= scale[j];
  norm *= dim * pow(M_PI, 0.5*dim)*exp(-lgamma(0.5*dim+1.0)) * 
    boost::math::beta(shape, static_cast<double>(dim));

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  nrml = NormalPtr (new Normal (0.1, 1.0, BIEgen));
}

Distribution* BetaRDist::New() 
{
  clivectord c(center), s(scale), l(limits);
  return new BetaRDist(shape, &c, &l, &s);
}
  
double BetaRDist::PDF(State& x) 
{
  double r = 0.0, z;
  for (unsigned i=0; i<dim; i++) {
    z = (x[i] - center[i])/scale[i];
    r += z*z;
  }
  r = sqrt(r);

  return 1.0/(norm*pow(1.0 + r,shape+dim));
}

double BetaRDist::logPDF(State& x) 
{
  double r = 0.0, z;
  for (unsigned i=0; i<dim; i++) {
    z = (x[i] - center[i])/scale[i];
    r += z*z;
  }
  r = sqrt(r);

  return -(shape+dim)*log(1.0 + r) - log(norm);
}

vector<double> BetaRDist::lower(void)
{ 
  vector<double> ret(limits);
  for (unsigned i=0; i<dim; i++) ret[i] *= -1.0;
  return ret;
}

vector<double> BetaRDist::upper(void)
{ return vector<double>(limits); }

vector<double> BetaRDist::Mean(void)
{ return vector<double>(center); }

vector<double> BetaRDist::StdDev(void)
{ 
  if (shape<=2.0) return vector<double>(scale.size(), INFINITY);

  vector<double> s(scale);
  double factor = sqrt( ( (1.0+dim)*dim )/( (shape-1.0)*shape ) );
  for (unsigned i=0; i<dim; i++) scale[i] *= factor;
  return s;
}

double BetaRDist::CDF(State& x) 
{
  double r = 0.0, u;
  for (unsigned i=0; i<dim; i++) {
    u = (x[i] - center[i])/scale[i];
    r += u*u;
  }
  
  u = 1.0/(1.0 + sqrt(r));	// This is the variable u in [0,1]

  return boost::math::ibeta(shape, static_cast<double>(dim), u);
}

vector<double> BetaRDist::Moments(int i) 
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = center[i];
    break;
  case 2:
    if (shape>2.0)
      ans = scale[i]*scale[i]*(1.0+dim)*dim/((shape-1.0)*shape);
    else
      ans = INFINITY;

    break;
  default:
    cerr << "BetaRDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State BetaRDist::Sample(void) 
{
  double u = boost::math::ibeta_inv(shape, static_cast<double>(dim), 
				    (*unit)());
  double r = (1.0 - u)/u;
  vector<double> ret;
  double sum = 0.0, v;
  for (unsigned i=0; i<dim; i++) {
    v = (*nrml)();
    ret.push_back(v);
    sum += v*v;
  }

  for (unsigned i=0; i<dim; i++)
    ret[i] = center[i] + ret[i]*r/sum;

  return State(ret);
}

