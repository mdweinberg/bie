#include <VWPrior.h>
#include <ACG.h>
#include <Normal.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::VWPrior)

using namespace BIE;

double VWPrior::taumax = 10.0;
double VWPrior::psimax = 10.0;

VWPrior::VWPrior()
{
  _type = NormalAdd;
}

VWPrior::VWPrior(StateInfo *si, int n, double w, Prior* pri) : Prior(si)
{
  _type  = NormalAdd;

  Nmax   = n;
  W      = w;
  WW     = w*w;
  P      = pri;

  double c = 2.0;
  cj .push_back(0);
  cj2.push_back(0);
  for (int i=1; i<=Nmax; i++) {
    cj .push_back(c);
    cj2.push_back(c*c);
    c *= 2.0;
  }
    
  normal = NormalPtr(new Normal(0.0, 1.0, BIEgen));
}

VWPrior* VWPrior::New() 
{
  return new VWPrior(_si, Nmax, W, P);
}
  
double VWPrior::PDF(State& y) 
{
  // Get the dimension of the old state vector using
  // the previous prior distribution

  // Break up the state vector into two the two parts

  vector<double> x;
  State s(P->SI());

  s.assign(y.begin(), y.begin()+P->SI()->Ntot);
  x.assign(y.begin()+P->SI()->Ntot, y.end());

  // Now, compute the PDF

  double tau = x[0], v;
				// One sided distribution for tau
  double ret = exp(-0.5*tau*tau/WW)/sqrt(0.5*M_PI*WW);
  int sz = x.size();

  for (int i=1; i<min<double>(sz, Nmax+1); i++) {
    v = tau*tau/cj2[i];
    ret *= exp(-0.5*x[i]*x[i]/v)/sqrt(2.0*M_PI*v);
  }

  ret *= P->PDF(s);

  return ret;
}

double VWPrior::logPDF(State& y) 
{
  // Get the dimension of the old state vector using
  // the previous prior distribution

  // Break up the state vector into two the two parts

  vector<double> x;
  State s(P->SI());

  s.assign(y.begin(), y.begin()+P->SI()->Ntot);
  x.assign(y.begin()+P->SI()->Ntot, y.end());

  // Now, compute the PDF

  double tau = x[0], v;
				// One sided distribution for tau
  double ret = -0.5*tau*tau/WW - 0.5*log(0.5*M_PI*WW);
  int sz = x.size();

  for (int i=1; i<sz; i++) {
    v = tau*tau/cj2[i];
    ret += -0.5*x[i]*x[i]/v - 0.5*log(2.0*M_PI*v);
  }

  ret += P->logPDF(s);

  return ret;
}

vector<double> VWPrior::lower(void)
{ 
  vector<double> ret(P->lower());

  ret.insert(ret.end(),   0.0);	// Tau
  ret.insert(ret.end(), Nmax, -psimax);

  return ret;
}

vector<double> VWPrior::upper(void)
{ 
  vector<double> ret(P->upper());

  ret.insert(ret.end(), taumax); // Tau
  ret.insert(ret.end(), Nmax, psimax);

  return ret;
}

vector<double> VWPrior::Mean(void)
{ 
  vector<double> ret(P->Mean());
  double val = 0.0;

  ret.insert(ret.end(), W);
  ret.insert(ret.end(), Nmax, val);

  return ret;
}

vector<double> VWPrior::StdDev(void)
{
  vector<double> ret(P->StdDev());

  ret.insert(ret.end(), W);
  for (int i=1; i<=Nmax; i++) ret.push_back(1.0/cj2[i]);

  return ret;
}

double VWPrior::CDF(State& x) 
{
  return 0.0;
}

vector<double> VWPrior::Moments(int i) 
{
  int ndim = P->SI()->Ntot + 1 + Nmax;
  return vector<double>(ndim, 0.0);
}

State VWPrior::Sample(void) 
{
  vector<double> ret = P->Sample();
  double tau = fabs(W*(*normal)());

  ret.push_back(tau);
  for (int i=1; i<=Nmax; i++) ret.push_back(tau/cj[i]*(*normal)());

  return State(ret);
}

void VWPrior::SamplePrior(int& M, vector<double>& wght, 
			  vector< vector<double> >& phi) 
{
  vector<double> p = Sample();
  //
  // Sanity check #1
  //
  if (wght.size() != 1) {
    ostringstream sout;
    sout << "expected wght dimension of 1, found " << wght.size() << endl;
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }
  //
  // Sanity check #2
  //
  if (phi[0].size() != p.size()) {
    ostringstream sout;
    sout << "expected phi[0] dimension of " << p.size()
	 << ", found " << phi[0].size() << endl;
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }
  //
  // OK . . .
  //
  M       = 1;
  wght[0] = 1;
  phi [0].assign(p.begin(), p.end());
}

void VWPrior::SamplePrior(State *s)
{
  vector<double> p = Sample();
  //
  // Sanity check: requested vs supplied dimension
  //
  if (p.size() != s->N()) {
    ostringstream sout;
    sout << "VWPrior::SamplePrior: expected dimension of " << p.size()
	 << " but the passed State has dimension of " << s->N() << endl;
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }
  //
  // Assign the sampled vector to the state
  //
  *static_cast<vector<double>*>(s) = p;
}
