#include <cmath>

#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Distribution)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::SampleDistribution)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::BinnedDistribution)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::NullDistribution)

using namespace BIE;

double PointDistribution::CDF(vector<double> &v) 
{
  double ret = 0.0;

  double one = true;

  for (unsigned i=0; i<data.size(); i++) {
    if (data[i] < v[i]) one = false;
  }

  if (one) ret = 1.0;

  return ret;
}

