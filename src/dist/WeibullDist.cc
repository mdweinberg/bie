#include <cfloat>

#include <WeibullDist.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::WeibullDist)

using namespace BIE;

WeibullDist::WeibullDist() : alpha(1.0), beta(1.0), vmin(0.0), vmax(DBL_MAX)
{
  _type  = NormalMult;
  mean   = 1.0;
  var    = 1.0;
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = 1.0;

  unit   = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
}

// The last two parameters can be defaulted
WeibullDist::WeibullDist(double a, double b, double minv, double maxv)
{
  _type  = NormalMult;
  alpha  = a;
  beta   = b;
  mean   = alpha*exp(lgamma(1.0 + 1.0/beta));
  var    = alpha*alpha*exp(lgamma(1.0 + 2.0/beta)) - mean*mean;
  vmin   = max<double>(0.0, minv);
  vmax   = min<double>(DBL_MAX, maxv);
  mincdf = 1.0 - exp(-pow(minv/alpha, beta));
  maxcdf = 1.0 - exp(-pow(maxv/alpha, beta));
  norm   = maxcdf - mincdf;

  unit   = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
}

Distribution* WeibullDist::New() 
{ return new WeibullDist(alpha, beta); }
  
double WeibullDist::PDF(State& x)
{
  double ans;
  if (x[0]>=vmin && x[0]<=vmax)
    ans = beta*pow(alpha, -beta)*pow(x[0], beta-1.0)
      * exp(-pow(x[0]/alpha, beta)) / norm;
  else
    ans = 0.0;
  return ans;
}

double WeibullDist::logPDF(State& x)
{
  double ans = 0.0;
  if (x[0]>=vmin && x[0]<=vmax)
    ans = log(beta) - beta*log(alpha) + (beta-1.0)*log(x[0])
      -pow(x[0]/alpha, beta) - log(norm);
  else
    throw ImpossibleStateException(__FILE__, __LINE__);
  return ans;
}

double WeibullDist::CDF(State& x) 
{
  double ans;
  if (x[0]>=vmax)
    ans = 1.0;
  else if (x[0]<=vmin)
    ans = 0.0;
  else
    ans = (maxcdf - exp(-pow(x[0]/alpha, beta)))/norm;
  return ans;
}

vector<double> WeibullDist::lower(void)
{ return vector<double>(1, vmin); }

vector<double> WeibullDist::upper(void)
{ return vector<double>(1, vmax); }

vector<double> WeibullDist::Mean(void)
{ return vector<double>(1, mean); }

vector<double> WeibullDist::StdDev(void)
{ return vector<double>(1, sqrt(var)); }

vector<double> WeibullDist::Moments(int i)
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = mean;
    break;
  case 2:
    ans = var + mean*mean;
    break;
  default:
    cerr << "WeibullDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State WeibullDist::Sample(void)
{
  const int itmax=10000;
  int i=0;
  double x;
  while (i<itmax) {
    x = alpha*pow(-log(1.0 - (*unit)()), 1.0/beta);
    if (x<=vmax && x>=vmin) break;
  }
  if (i==itmax) cerr << "WeibullDist: sample not converge\n";
  return State(vector<double>(1, x));
}
