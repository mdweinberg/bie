
#include <cfloat>
#include <iostream>
#include <iomanip>

using namespace std;

#include <BIEException.h>
#include <BIEMutex.h>
#include <EnsembleStat.h>
#include <gfunction.h>
#include <BIEmpi.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::EnsembleStat)

using namespace BIE;

// Minimum number of states per subspace
int EnsembleStat::minsub = 3;

EnsembleStat::EnsembleStat() : Ensemble()
{
  var_computed = false;
  
  normal       = NormalPtr(new Normal(0.0, 1.0, BIEgen));
  factor       = 1.0;
}


EnsembleStat::EnsembleStat(StateInfo *si,
			   int level, int begin, string filename, int keypos) :
  Ensemble(si, level, begin, filename, keypos)
{
  var_computed = false;

  normal       = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  factor       = 1.0;
  
  setDimensions(_si);
  
  ComputeDistribution(begin);
}

EnsembleStat::EnsembleStat(StateInfo *si) : Ensemble(si)
{
  var_computed = false;

  normal       = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  factor       = 1.0;

  setDimensions(_si);
}

EnsembleStat::EnsembleStat(const EnsembleStat &p) : Ensemble(p)
{
  icut           = p.icut;
  cnts           = p.cnts;
  mcnt           = p.mcnt;
  mcut           = p.mcut;
  
  var_computed   = p.var_computed;
  dimensions_set = p.dimensions_set;
  
  vlower         = p.vlower;
  vupper         = p.vupper;

  vmean          = p.vmean;
  vvar           = p.vvar;
  
  mmean          = p.mmean;
  mvar           = p.mvar;
  
  normal         = NormalPtr(new Normal(0.0, 1.0, BIEgen));
  
  factor         = p.factor;
}


EnsembleStat::EnsembleStat(Ensemble *p) : Ensemble(*p)
{
  var_computed = false;
  normal       = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  factor       = 1.0;

  setDimensions(_si);
  
  ComputeDistribution();
}


void EnsembleStat::Reset(StateInfo *si,
			 int level, int begin, string filename, int keypos)
{
  var_computed = false;
  
  Ensemble::Reset(si, level, begin, filename, keypos);
}

void EnsembleStat::Reset(StateInfo *si)
{
  var_computed = false;
  
  Ensemble::Reset(si);
}


void EnsembleStat::setDimensions(StateInfo *si)
{
  Ensemble::setDimensions(si);
}

//
// This routine diagonalizes the covariance matrix and
// computes the transformation to the implied uncorrelated
// coordinate system.  Null subspace is dropped in estimating
// the posterior distribution.
//
// See the "posterior_probability_EnsembleStat" member function 
// below
//
void EnsembleStat::ComputeDistribution(void)
{
  if (!dimensions_set) {
    string msg = "Must call EnsembleStat::SetDimensions before computing the distribution";
    throw InternalError(msg, __FILE__, __LINE__);
  }
  
  if (var_computed) return;
  
  if (simulationInProgress == 0) Broadcast();

  // Non-mixture defaults
  unsigned m=1, n=_si->Ntot;
  vector< vector<unsigned> > MAP(2);
  bool mixture = false, rjtwo = false;

  if (_si->ptype == StateInfo::RJTwo) {
    for (unsigned i=1; i<=_si->T; i++) {
      MAP[0].push_back(i);
      MAP[1].push_back(i);
    }
    for (unsigned i=_si->T+1; i<=_si->T+_si->N1; i++) 
      MAP[0].push_back(i);

    for (unsigned i=_si->T+_si->N1+1; i<=_si->T+_si->N1+_si->N2; i++) 
      MAP[1].push_back(i);

    rjtwo = true;
  }

				// Mixture settings
  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) 
    {
      mixture = true;
    }

				// Clean up working arrays
  cnts   .erase(cnts  .begin(), cnts  .end());
  ccum   .erase(ccum  .begin(), ccum  .end());
  mean   .erase(mean  .begin(), mean  .end());
  covar  .erase(covar .begin(), covar .end());
  eigen  .erase(eigen .begin(), eigen .end());

  vmean  .erase(vmean .begin(), vmean .end());
  vvar   .erase(vvar  .begin(), vvar  .end());
  evals  .erase(evals .begin(), evals .end());
  Teigen .erase(Teigen.begin(), Teigen.end());

  mmean  .erase(mmean .begin(), mmean .end());
  mvar   .erase(mvar  .begin(), mvar  .end());

  evalsM .erase(evalsM.begin(), evalsM.end());
  meanM  .erase(meanM .begin(), meanM .end());
  covarM .erase(covarM.begin(), covarM.end());
  eigenM .erase(eigenM.begin(), eigenM.end());
  TeigeM .erase(TeigeM.begin(), TeigeM.end());
  
  vlower = vector<double>(_si->Ntot,  DBL_MAX);
  vupper = vector<double>(_si->Ntot, -DBL_MAX);

  ofstream out;
  
  if (verbose) {
    ostringstream sout; sout << "EnsembleStat.log." << _id;
    out.open(sout.str().c_str());
    out.precision(6);
    out << "ComputeDistribution: initialized, count=" << count << endl;
  }
				// Putting check here makes sure that
				// "clean" mean & stdev is returned to
				// any diagnostic routines
  if (count<2) return;

  ibeg = max<int>(0, burnIn - offset);

  if (ibeg >= count) {
    if (myid==0)
      std::cout << "EnsembleStat: too few states in remaining chain."
		<< "Discarding the first half (" << count/2
		<< ") and continuing" << std::endl;
    ibeg = count/2;
  }

  if (burnIn - offset > count) {
    ostringstream msg;
    msg << "Logic error: requested burnIn=" << burnIn 
	<< " is larger than the number of recorded states=" << offset+count;
    throw InternalError(msg.str(), __FILE__, __LINE__);
  }
  
  for (int i=ibeg; i<count; i++) {
    
    if (rjtwo) {
      m = floor(states[i].p[0]+1.01);
      n = states[i].p.N();
    }
    if (mixture) {
      m = states[i].p.M();
      n = (_si->Ndim+1)*m;
    }

    addCount(m);

    // Compute min and max
    for (unsigned k=0; k<_si->Ntot; k++) {
      vlower[k] = min<double>(vlower[k], states[i].p[k]);
      vupper[k] = max<double>(vupper[k], states[i].p[k]);
    }

    // Mean and covariance are done _universally_, independent of the
    // State vector packing

    if (rjtwo) {

      // Pass #1
      for (unsigned k1=0; k1<MAP[m-1].size(); k1++)
	mean[m][1 + k1] += states[i].p[MAP[m-1][k1]];

    } else {

      // Pass #1
      for (unsigned k1=0; k1<n; k1++)
	mean[m][1 + k1] += states[i].p[k1];
    }
  }
  
  for (auto v : cnts) {
    unsigned m = v.first;
    int      N = v.second;
    if (N>0) mean[m] /= N;
  }

  for (int i=ibeg; i<count; i++) {
    
    if (rjtwo) {
      m = floor(states[i].p[0]+1.01);
      n = states[i].p.N();
    }
    if (mixture) {
      m = states[i].p.M();
      n = (_si->Ndim+1)*m;
    }

    if (rjtwo) {

      // Pass #2
      for (unsigned k1=0; k1<MAP[m-1].size(); k1++) {
	double dif1 = states[i].p[MAP[m-1][k1]] - mean[m][1 + k1];
      
	for (unsigned k2=0; k2<MAP[m-1].size(); k2++) {
	  double dif2 = states[i].p[MAP[m-1][k2]] - mean[m][1 + k2];

	  covar[m][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
      
    } else {

      // Pass #2
      for (unsigned k1=0; k1<n; k1++) {
	double dif1 = states[i].p[k1] - mean[m][1 + k1];
      
	for (unsigned k2=0; k2<n; k2++) {
	  double dif2 = states[i].p[k2] - mean[m][1 + k2];

	  covar[m][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
    }
  }
  
  for (auto v : cnts) {
    unsigned m = v.first;
    int      N = v.second;
    if (N>1) 
      covar[m] /= N - 1;
    else
      covar[m].zero();
  }

  if (verbose)
    out << "ComputeDistribution: covariance matrix done" << endl;

				// Cache return vectors for mean and variance
  for (auto v : cnts) {

    int m = v.first;
    int N = v.second;
    int n = _si->Ntot;

    if (_si->ptype == StateInfo::RJTwo) n = MAP[m-1].size();

    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) n = (_si->Ndim+1)*m;

    if (verbose) {
      out << "ComputeDistribution: m=" << m 
	  << " counts=" << N << endl;
      if (N) {
	out << "ComputeDistribution: about to erase at m=" << m 
	    << ", ndim=" << vmean[m].size() << ", "
	    << vvar[m].size() << endl;
	
	out << "Covariance:" << endl;
	for (int k1=1; k1<=n; k1++) {
	  for (int k2=1; k2<=n; k2++) 
	    out << setw(16) << covar[m][k1][k2];
	  out << endl;
	}
      }
      else
	out << "ComputeDistribution: skipping m=" << m << endl;
    }

    vmean[m].erase(vmean[m].begin(), vmean[m].end());
    vvar [m].erase(vvar [m].begin(), vvar [m].end());

    if (verbose)
      out << "ComputeDistribution: mean & cov, m=" << m << endl;

    for (int i=0; i<n; i++) {
      vmean[m].push_back(mean [m][1+i]);
      vvar [m].push_back(covar[m][1+i][1+i]);
    }

    if (verbose) {
      out << "Mean and Var:" << endl;
      for (int i=0; i<n; i++)
	out << setw(5) << i+1 << ": "
	    << setw(16) << vmean[m][i]
	    << setw(16) << vvar [m][i]
	    << endl;
    }
  }

  if (verbose)
    out << "ComputeDistribution: mean and var done" << endl;
  
  // It would probably be more efficient to do this with Cholesky
  // decomposition but we need the eigenvectors for computing the PDF
  // and CDF
  
  for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
    int m = it->first;

    {				// Sanity check
      bool nan_m = false, nan_c = false;
      for (int i=mean[m].getlow(); i<=mean[m].gethigh(); i++)
	if (std::isnan(mean[m][i])) nan_m = true;

      for (int i=covar[m].getrlow(); i<=covar[m].getrhigh(); i++)
	for (int j=covar[m].getclow(); j<=covar[m].getchigh(); j++)
	  if (std::isnan(covar[m][i][j])) nan_c = true;

      if (nan_m) {
	std::cerr << "#1: Found not-a-number in mean vector. Bye." 
		  << std::endl;
	exit(-1);
      }

      if (nan_c) {
	std::cerr << "#1: Found not-a-number in covariance matrix. Bye." 
		  << std::endl;
	exit(-1);
      }
    }

    try {
      evals [m] = Symmetric_Eigenvalues_GHQL(covar[m], eigen[m]);
    }
    catch (ArrayException error) {
      std::cerr << "Error: " << error.getErrorMessage() << std::endl;
      std::cerr << "Mean vector:" << std::endl;
      mean[m].print(std::cerr);
      std::cerr << "Covariance matrix:" << std::endl;
      covar[m].print(std::cerr);
      throw error;		// Rethrow
    }

    Teigen[m] = eigen[m].Transpose();
    
    if (verbose)
      out << "ComputeDistribution: eigenfct for m=" << m << endl;
    
    // Truncate zero variance components
    // (This is the null space)
    int elo = evals[m].getlow ();
    int ehi = evals[m].gethigh();

    icut[m] = ehi;
    double sum = 0.0;
    for (int i=elo; i<=ehi; i++) sum += evals[m][i];
    for (int i=elo; i<=ehi; i++) {
      if (evals[m][i] < thresh*sum && i>1) {
	icut[m] = i - 1;
	break;
      }
    }
    
    if (verbose) {
      out << "Eigenvalues:" << endl;
      for (int i=elo; i<=ehi; i++) out << setw(5) << i << ": " 
				       << evals[m][i] << endl;
      out << "Eigenvectors:" << endl;
      for (int i=elo; i<=ehi; i++) {
	for (int j=elo; j<=ehi; j++) 
	  out << setw(16) << Teigen[m][i][j];
	out << endl;
      }
    }
  }
  
  if (verbose) 
    out << "ComputeDistribution: eigenanalysis done" << endl;
  
  // Cumulative distribution for sampling
  //
  makeSampleFraction();
  
  
  var_computed = true;
  
#ifdef NDEBUG
  out << "Limits: MinC=" << minc << "  MaxC=" << maxc << endl;
  out << "Cumulative states:" << endl;
  for (mapIDiter it=ccum.begin(); it!=ccum.end(); it++) {
    out << setw(5) << it->first << ": " << it->second << endl;
#endif
  
  ComputeDistributionMarginal(out);
  
#ifdef NDEBUG
  out << "ComputeDistribution: everything done" << endl;
#endif
  
}




void EnsembleStat::ComputeDistributionMarginal(ostream& out)
{
  if (_si->ptype == StateInfo::None)  return;
  if (_si->ptype == StateInfo::Block) return;
  if (_si->ptype == StateInfo::RJTwo) return;

				// Clean up working arrays
  for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
    int m = it->first;
    for (int mm=0; mm<m; mm++) {
      meanM [m][mm].zero();
      covarM[m][mm].zero();
      eigenM[m][mm].zero();
    }
    mcnt[m] = 0;
    mcum[m] = 0;
  }
  
  ibeg = max<int>(0, burnIn - offset);
  
  for (int i=ibeg; i<count; i++) {
    
    unsigned m = states[i].p.M();
    unsigned n = states[i].p.Ndim();

    mcnt[m]++;

    for (unsigned mm=0; mm<m; mm++) {
      for (unsigned k1=0; k1<_si->Ndim; k1++) {
	meanM[m][mm][1 + k1] += states[i].p[m+n*mm+k1];
      }
    }
  }
  

  for (auto v : mcnt) {
    unsigned m = v.first;
    int N      = v.second;
    if (N > 0) {
      for (unsigned mm=0; mm<m; mm++) {
	for (unsigned k1=0; k1<_si->Ndim; k1++) {
	  meanM[m][mm][1 + k1] /= v.second;
	}
      }
    }
  }
    

  for (int i=ibeg; i<count; i++) {
    
    unsigned m = states[i].p.M();
    unsigned n = states[i].p.Ndim();

    for (unsigned mm=0; mm<m; mm++) {
      
      for (unsigned k1=0; k1<_si->Ndim; k1++) {
	double dif1 = states[i].p[m+n*mm+k1] - meanM[m][mm][1 + k1];
	for (unsigned k2=0; k2<n; k2++) {
	  double dif2 = states[i].p[m+n*mm+k2] - meanM[m][mm][1 + k2];
	  covarM[m][mm][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
    }
  }
  
  for (auto v : mcnt) {
    unsigned m = v.first;
    int N      = v.second;
    for (unsigned mm=0; mm<m; mm++) {
      if (N > 1) {
	covarM[m][mm] /= N - 1;
      } else {
	covarM[m][mm].zero();
      }
    }
  }
    
  
#ifdef NDEBUG
  out << "ComputeDistributionMarginal: covariance matrix done" << endl;
#endif
  
  // Cache return vectors for mean and variance
  for (auto v : mcnt) {
    unsigned m = v.first;
    int N      = v.second;
    
#ifdef NDEBUG
    out << "ComputeDistribution: m=" << m 
	<< " counts=" << N << endl;
    if (N) {
      out << "ComputeDistribution: about to erase at m=" << m 
	  << ", ndim=" << mmean[m][0].size() << ", "
	  << mvar[m][0].size() << endl;
      
      out << "Covariance:" << endl;
      for (int k1=covarM[m][0].getrlow(); k1<=covarM[m][0].getrhigh(); k1++) {
	for (int k2=covarM[m][0].getclow(); k2<=covarM[m][0].getchigh(); k2++) out << setw(16) << covarM[m][0][k1][k2];
	out << endl;
      }
    }
    else {
      out << "ComputeDistribution: skipping m=" << m << endl;
    }
#endif
    
    if (!N) continue;

    for (unsigned mm=0; mm<m; mm++) {
      mmean[m][mm].erase(mmean[m][mm].begin(), mmean[m][mm].end());
      mvar [m][mm].erase(mvar [m][mm].begin(), mvar [m][mm].end());
      
#ifdef NDEBUG
      out << "ComputeDistribution: marginal mean & cov, m=" << m 
	  << ", comp=" << mm << endl;
#endif
      
      for (unsigned i=0; i<_si->Ndim; i++) {
	mmean[m][mm].push_back( meanM[m][mm][1+i]);
	mvar [m][mm].push_back(covarM[m][mm][1+i][1+i]);
      }
      
#ifdef NDEBUG
      out << "Marginal mean and var:" << endl;
      for (int i=0; i<mmean[m][mm].size(); i++)
	out << setw(5) << i+1 << ": "
	    << setw(16) << mmean[m][mm][i]
	    << setw(16) <<  mvar[m][mm][i]
	    << endl;
#endif
    }
  }

#ifdef NDEBUG
  out << "ComputeDistribution: Marginal mean and var done" << endl;
#endif
  // It would probably be more efficient to do this with Cholesky
  // decomposition but we need the eigenvectors for computing the PDF
  // and CDF
  
  for (auto v : mcnt) {

    int m = v.first;
    
    for (int mm=0; mm<m; mm++) {

      {				// Sanity check
	bool nan = false;
	for (int i=covarM[m][mm].getrlow(); i<=covarM[m][mm].getrhigh(); i++)
	  for (int j=covarM[m][mm].getclow(); j<=covarM[m][mm].getchigh(); j++)
	    if (std::isnan(covarM[m][mm][i][j])) nan = true;
	
	if (nan) {
	  std::cerr << "#2: Found not-a-number in covariance matrix. Bye." 
		    << std::endl;
	  exit(-1);
	} 
      }

      try {
	evalsM[m][mm] = 
	  Symmetric_Eigenvalues_GHQL(covarM[m][mm], eigenM[m][mm]);
      }
      catch (ArrayException error) {
	std::cerr << "Error: " << error.getErrorMessage() << std::endl;
	std::cerr << "Mean vector:" << std::endl;
	meanM[m][mm].print(std::cerr);
	std::cerr << "Covariance matrix:" << std::endl;
	covarM[m][mm].print(std::cerr);
	throw error; 		// Rethrow
      }

      TeigeM[m][mm] = eigenM[m][mm].Transpose();
      
#ifdef NDEBUG
      out << "ComputeDistribution: marginal eigenfct for m=" << m 
	  << ", comp=" << mm+1 << endl;
#endif
      
      double sum = 0.0;
      int dim = _si->Ndim;
      
      mcut[m][mm] = dim;
      for (int i=1; i<=dim; i++) sum += evalsM[m][mm][i];
      for (int i=1; i<=dim; i++) {
	if (evalsM[m][mm][i] < thresh*sum && i>1) {
	  mcut[m][mm] = i - 1;
	  break;
	}
      }
      
#ifdef NDEBUG
      out << "Marginal eigenvalues:" << endl;
      for (int i=1; i<=dim; i++) out << setw(5) << i << ": " 
				     << evalsM[m][mm][i] << endl;
      out << "Marginal eigenvectors:" << endl;
      for (int i=1; i<=dim; i++) {
	for (int j=1; j<=dim; j++) 
	  out << setw(16) << TeigeM[m][mm][i][j];
	out << endl;
      }
#endif
    }
  }
  
#ifdef NDEBUG
  out << "ComputeDistribution: marginal eigenanalysis done" << endl;
#endif
  // Throw out subspaces that do not meet our
  // minimum occupation requirement
  for (unsigned m=0; m<_si->M; m++) {
    if (mcnt[m] < minsub) mcnt[m] = 0;
  }
  
  // Cumulative distribution for 
  // component states
  mapIIiter cur, lst=mcnt.begin();
  for (cur=mcnt.begin(); cur!=mcnt.end(); cur++) {
    int m  = cur->first;
    mcum[m] = cur->second;
    if (cur!=lst) mcum[m] += lst->second;
    lst = cur;
  }

  double norm = mcum.rbegin()->second;
  for (mapIDiter it=mcum.begin(); it!=mcum.end(); it++) it->second /= norm;
  mcum.rbegin()->second = 1.0;
  
#ifdef NDEBUG
  out << "Marginal cumulative states:" << endl;
  for (auto v : mcnt)
    out << setw(5) << v.first << ": " << v.second << endl;
#endif
  
}

//
// Use the covariance analysis to compute the posterior probability
// based on the simulation data
//
double EnsembleStat::PDF(State& p)
{
  if (!var_computed) ComputeDistribution();
  
  // State dimensions
  unsigned m=1, n=_si->Ntot;

  if (_si->ptype == StateInfo::RJTwo) 
    {
      m = floor(p[0]+0.01);
      n = m ? (_si->T + _si->N2) : (_si->T + _si->N1);
    }

  // Number of parameters in the mixture state
  if (_si->ptype == StateInfo::Mixture ||
      _si->ptype == StateInfo::Extended) 
    {
      // Number of components in this state
      if (p.M() > _si->M) {
	throw DimNotMatchException(__FILE__, __LINE__);
      }
      m = p.M();
      n = (_si->Ndim+1)*m;
    }
  
  VectorM output, trial(1, n);
  
  for (unsigned k=0; k<n; k++) trial[1+k] = p[k] - mean[m][1+k];
  
  output = Teigen[m] * trial;
  
  double ret = 1.0;
  
  for (int k=1; k<=trim_location(m); k++) {
    ret *= exp( -0.5*log(2.0*M_PI*evals[m][k]) - 
		output[k]*output[k] / (2.0*evals[m][k]) );
  }
  
  return ret;
}

double EnsembleStat::logPDF(State& p)
{
  if (!var_computed) ComputeDistribution();
  
  // State dimensions
  unsigned m=1, n=_si->Ntot;

  if (_si->ptype == StateInfo::RJTwo) 
    {
      m = floor(p[0]+0.01);
      n = m ? (_si->T + _si->N2) : (_si->T + _si->N1);
    }

  // Number of parameters in the mixture state
  if (_si->ptype == StateInfo::Mixture ||
      _si->ptype == StateInfo::Extended) 
    {
      // Number of components in this state
      if (p.M() > _si->M) {
	throw DimNotMatchException(__FILE__, __LINE__);
      }

      m = p.M();
      n = (_si->Ndim+1)*m;
    }
  
  
  VectorM output, trial(1, n);
  
  for (unsigned k=0; k<n; k++) trial[1+k] = p[k] - mean[m][1+k];
  
  output = Teigen[m] * trial;
  
  double ret = 0.0;
  
  for (int k=1; k<=trim_location(m); k++) {
    ret += -0.5*log(2.0*M_PI*evals[m][k]) 
      - output[k]*output[k] / (2.0*evals[m][k]);
  }

  return ret;
}

double EnsembleStat::logPDFMarginal(unsigned m, unsigned n, 
				    const vector<double>& p)
{
  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block ||
      _si->ptype == StateInfo::RJTwo) {
    string msg =
      "Logic error: you are calling logPDFMarginal for a non-mixture model";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (!var_computed) ComputeDistribution();
  
  if (m > _si->M) {
    throw DimNotMatchException(__FILE__, __LINE__);
  }
  
  if (_si->Ndim > p.size()) {
    throw DimNotMatchException(__FILE__, __LINE__);
  }
  
  
  VectorM output, trial(1, _si->Ndim);
  
  for (unsigned k=0; k<_si->Ndim; k++) trial[1+k] = p[k] - meanM[m][n][1+k];
  
  output = TeigeM[m][n] * trial;
  
  double ret = 0.0;
  
  for (int k=1; k<=trim_location_marginal(m, n); k++) {
    ret += -0.5*log(2.0*M_PI*evalsM[m][n][k]) 
      - output[k]*output[k] / (2.0*evalsM[m][n][k]);
  }
  
  return ret;
}


double EnsembleStat::PDF(unsigned k, State& p)
{
  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo) {
    string msg =
      "Logic error: you are calling PDF(#, State) for a non-mixture model";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (!var_computed) ComputeDistribution();
  
  unsigned m = p.M();

  // Number of components in this state
  if (m > _si->M) {
    throw DimNotMatchException(__FILE__, __LINE__);
  }
  
  
  MatrixM  tcovar(1, _si->Ndim, 1, _si->Ndim);
  MatrixM  teigen(1, _si->Ndim, 1, _si->Ndim);
  MatrixM Tteigen(1, _si->Ndim, 1, _si->Ndim);
  VectorM tmean(1, _si->Ndim), Ttmean(1, _si->Ndim);
  
  for (unsigned i=1; i<=_si->Ndim; i++) {
    tmean[i] = mean[m][(_si->Ndim+1)*k+m+i];
    for (unsigned j=1; j<=_si->Ndim; j++) 
      tcovar[i][j] = covar[m][m+_si->Ndim*k+i][m+_si->Ndim*k+j];
  }
  
  {				// Sanity check
    bool nan = false;
    for (int i=tcovar.getrlow(); i<=tcovar.getrhigh(); i++)
      for (int j=tcovar.getclow(); j<=tcovar.getchigh(); j++)
	if (std::isnan(tcovar[i][j])) nan = true;
    
    if (nan) {
      std::cerr << "#3: Found not-a-number in covariance matrix. Bye." 
		<< std::endl;
      exit(-1);
    } 
  }    

  VectorM tevals;
  try {
    tevals = Symmetric_Eigenvalues_GHQL(tcovar, teigen);
  }
  catch (ArrayException error) {
    std::cerr << "Error: " << error.getErrorMessage() << std::endl;
    std::cerr << "Mean vector:" << std::endl;
    mean[m].print(std::cerr);
    std::cerr << "Covariance matrix:" << std::endl;
    covar[m].print(std::cerr);
    bool nan = false;
    for (int i=covar[m].getrlow(); i<=covar[m].getrhigh(); i++)
      for (int j=covar[m].getclow(); j<=covar[m].getchigh(); j++)
	if (std::isnan(covar[m][i][j])) nan = true;
    
    if (nan) {
      std::cerr << "Found not-a-number. Bye." << std::endl;
      exit(-1);
    } else {
      std::cerr << "Will try Jacobi method . . . " << std::endl;
      try {
	tevals = Symmetric_Eigenvalues(tcovar, teigen);
      }
      catch (ArrayException error) {
	std::cerr << "Error: " << error.getErrorMessage() << std::endl;
	exit(-1);
      }
    }
  }
  
  Tteigen = teigen.Transpose();
  Ttmean = Tteigen * tmean;
  
  
  // Truncate zero variance components
  // (This is the null space)
  double sum = 0.0;
  int ticut = _si->Ndim;
  for (unsigned i=1; i<=_si->Ndim; i++) sum += tevals[i];
  for (unsigned i=1; i<=_si->Ndim; i++) {
    if (tevals[i] < thresh*sum && i>1) {
      ticut = i - 1;
      break;
    }
  }
  
  
  VectorM trial(1, _si->Ndim), output(1, _si->Ndim);
  for (unsigned i=0; i<_si->Ndim; i++) trial[1+i] = p[m+_si->Ndim*k+i];
  
  output = Tteigen * trial;
  
  double ret = 1.0;
  
  for (int i=1; i<=ticut; i++) {
    ret *= exp(
	       -0.5*log(2.0*M_PI*tevals[i]) - 
	       (output[k] - Ttmean[i])*(output[k] - Ttmean[i])/(2.0*tevals[i])
	       );
  }
  
  return ret;
}


//
// Generates variates from the same estimated posterior probability
// distribution
//
State EnsembleStat::Sample(unsigned m)
{
  if (!var_computed) ComputeDistribution();
  
  // DEBUG (print every sampled state)
  static unsigned cntd   = 0;
  static bool     debugd = false;

  State ret(_si);
  ret.Reset(m);
  
  // Throw an exception for
  // out of bounds requests
  if (cnts.find(m) == cnts.end()) {
    throw DimNotMatchException(__FILE__, __LINE__);
  }
  
  // Number of parameters
  unsigned n = mean[m].getlength();
  
  //
  // Generate random numbers from orthogonal vectors
  //
  VectorM input(1, n), output;
  for (unsigned k=1; k<=n; k++)
    input[k] = factor*sqrt(fabs(evals[m][k]))*(*normal)();
  
  // Transform to orginal variables
  output = eigen[m] * input + mean[m];
  
  // Enter the weights and parameters
  for (unsigned k=1; k<=n; k++) ret.vec(k-1) = output[k];
  
  // Epilog for mixture
  if (_si->ptype == StateInfo::Mixture ||
      _si->ptype == StateInfo::Extended ) {
    
    // Enforce positivity and normalize the weights
    double sum = 0.0;
    for (unsigned i=0; i<m; i++) {
      ret.Wght(i) = min<double>(1.0, max<double>(0.0, ret.Wght(i)));
      sum += ret.Wght(i);
    }
    for (unsigned i=0; i<m; i++) ret.Wght(i) /= sum;
    
    // DEBUG
    if (debugd) {
      cout << "Weights [" << cntd << "], sum=" << sum << endl;
      cout << setw(3) << m;
      for (unsigned i=0; i<m; i++) cout << setw(10) << ret.Wght(i);
      cout << endl;
    }
  }
    
  // DEBUG
  if (debugd) {
    cout << "State [" << cntd << "]" << endl;
    cout << setw(3) << m;
    for (unsigned n=1; n<ret.size(); n++) cout << setw(10) << ret.vec(n);
    cout << endl;
    cntd++;
  }

  return ret;
}


vector<double> EnsembleStat::SampleMarginal(unsigned m, unsigned n)
{
  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo) {
    string msg =
      "Logic error: you are calling SampleMarginal for a non-mixture model";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (!var_computed) ComputeDistribution();
  
  vector<double> ret;
  
  // Throw an exception for
  // out of bounds requests
  if (cnts.find(m) == cnts.end()) throw DimNotMatchException(__FILE__, __LINE__);
  
  VectorM input(1, _si->Ndim);
  VectorM output;
  // Generate random numbers from othogonal
  // vectors
  input.zero();
  for (unsigned k=1; k<=_si->Ndim; k++)
    input[k] = factor*sqrt(fabs(evalsM[m][n][k]))*(*normal)();
  
  // Transform to orginal variables
  output = eigenM[m][n] * input + meanM[m][n];
  
  for (unsigned k=1; k<=_si->Ndim; k++) ret.push_back(output[k]);
  
  return ret;
}


vector<double> EnsembleStat::lower(void)
{
  if (!var_computed) ComputeDistribution();
  return vector<double>(_si->Ntot, -DBL_MAX);
}

vector<double> EnsembleStat::upper(void)
{
  if (!var_computed) ComputeDistribution();
  return vector<double>(_si->Ntot, DBL_MAX);
}

vector<double>  EnsembleStat::Mean(unsigned m)
{
  if (m<1) {
    ostringstream sout;
    sout << "EnsembleDisc::Mean: called with m=" << m
	 << " but must be >=1";
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }

  if (!var_computed) ComputeDistribution();
  return vmean[m];
}

vector<double>  EnsembleStat::StdDev(unsigned m)
{
  if (m<1) {
    ostringstream sout;
    sout << "EnsembleDisc::StdDev: called with m=" << m
	 << " but must be >=1";
    throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
  }

  vector<double> stddev;
  if (!var_computed) ComputeDistribution();

  if (_si->ptype == StateInfo::Mixture || _si->ptype == StateInfo::Extended) {
    if (m>_si->M) {
      ostringstream sout;
      sout << "EnsembleDisc::StdDev: called with m=" << m
	   << " but Mmix=" << _si->M;
      throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
    }
    for (unsigned i=0; i<(_si->Ndim+1)*m; i++) 
      stddev.push_back(sqrt(vvar[m][i]));
  } else {
    for (unsigned i=0; i<vvar[m].size(); i++) 
      stddev.push_back(sqrt(vvar[m][i]));
  }
  
  return stddev;
}

vector<double>  EnsembleStat::MeanMarginal(unsigned m, unsigned n)
{
  if (_si->ptype == StateInfo::None  || 
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo) {
    string msg =
      "Logic error: you are calling MeanMarginal for a non-mixture model";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (!var_computed) ComputeDistribution();
  return mmean[m][n];
}

vector<double>  EnsembleStat::StdDevMarginal(unsigned m, unsigned n)
{
  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block ||
      _si->ptype == StateInfo::RJTwo) {
    string msg =
      "Logic error: you are calling StdDevMarginal for a non-mixture model";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (!var_computed) ComputeDistribution();
  vector<double> stddev;
  for (unsigned i=0; i<_si->Ndim; i++) 
    stddev.push_back(sqrt(mvar[m][n][i]));
  
  return stddev;
}

vector<double> EnsembleStat:: Moments(unsigned m, unsigned i)
{
  ostream cout(checkTable("cli_console"));
  
  if (!var_computed) ComputeDistribution();
  vector<double> moms;
  switch (i) {
  case 0:
    for (unsigned i=0; i<m; i++) moms.push_back(1.0);;
    break;
  case 1:
    for (unsigned i=0; i<m; i++) moms.push_back(vmean[m][i]);
    break;
  case 2:
    for (unsigned i=0; i<m; i++) moms.push_back(vvar[m][i] + 
						vmean[m][i]*vmean[m][i]);
    break;
  default:
    cout << "EnsembleStat::Moments: moments greater than 2 are not defined\n";
  }
  
  return moms;
}

void EnsembleStat::PrintDiag(string& outfile)
{
  std::ofstream out;

  if (myid==0) {
    out.open(outfile.c_str(), ios::out | ios::app);
  }

  PrintDiag(out);
}

void EnsembleStat::PrintDiag(ostream& out)
{
  ComputeDistribution();

  if (myid==0) {

    out << endl
	<< "My instance=" << this << endl
	<< "Total counts=" << count << endl
	<< "Counts per component" << endl;
    
    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++)
      out << setw(5)  << it->first << ": " 
	  << setw(8)  << it->second
	  << setw(15) << ccum[it->first] 
	  << endl;
    out << endl;
    
    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
      int m = it->first;
      int N = it->second;
      
      if (N<2) continue;
      
      out << endl << endl << "*** " << m << " ***" << endl;
      if (_si->ptype == StateInfo::Mixture || 
	  _si->ptype == StateInfo::Extended   ) out << m;
      else out << "non-mixture";
      out << " [" << (double)N/count << "] ***" << endl;
      
      for (int k=mean[m].getlow(); k<=mean[m].gethigh(); k++) {
	out << setw(5)  << k 
	    << setw(15) << mean[m][k] 
	    << setw(15) << covar[m][k][k]
	    << setw(15) << sqrt(covar[m][k][k])
	    << endl;
      }
      out << endl;
      out << endl;
      for (int i=covar[m].getrlow(); i<=covar[m].getrhigh(); i++) {
	for (int j=covar[m].getclow(); j<=covar[m].getchigh(); j++)
	  out << setw(15) << covar[m][i][j];
	out << endl;
      }
      out << endl;
    }

    out << endl;
    out << endl;
  }
  
}

void EnsembleStat::PrintDiag()
{
  PrintDiag(cout);
}

double EnsembleStat::SampleOne(unsigned m, unsigned j)
{
  if (!var_computed) ComputeDistribution();
  
  double z;
  
  if (m > _si->M) {
    throw DimNotMatchException(__FILE__, __LINE__);
  }
  
  z = vmean[m][j] + sqrt(vvar[m][j])*(*normal)();
  
  if (j < m) {
    if (z<0.0) z=0.0;
    if (z>1.0) z=1.0;
  }
  
  return z;
}

void EnsembleStat::addCount(int m)
{
  mapIIiter it = cnts.find(m);

  if (it != cnts.end()) it->second++;
  else {
    cnts[m]  = 1;
    mcnt[m]  = 0;
    mcum[m]  = 0;
    //
    int n    = _si->Ntot;
    //
    if (_si->ptype == StateInfo::RJTwo) {
      if (m==1) n = _si->T + _si->N1;
      else      n = _si->T + _si->N2;
    }
    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) n = (_si->Ndim+1)*m;
    //
    vmean [m] = vector<double>(n, 0.0);
    vvar  [m] = vector<double>(n, 0.0);
    //
    mmean [m] = vector< vector<double> >(m);
    mvar  [m] = vector< vector<double> >(m);
    //
    evals [m] = VectorM(1, n);
    mean  [m] = VectorM(1, n);
    covar [m] = MatrixM(1, n, 1, n);
    eigen [m] = MatrixM(1, n, 1, n);
    Teigen[m] = MatrixM(1, n, 1, n);
    //
    evals [m].zero();
    mean  [m].zero();
    covar [m].zero();
    eigen [m].zero();
    //
    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) {
      //
      evalsM[m] = vector<VectorM>(m);
      meanM [m] = vector<VectorM>(m);
      covarM[m] = vector<MatrixM>(m);
      eigenM[m] = vector<MatrixM>(m);
      TeigeM[m] = vector<MatrixM>(m);
      //
      for (int mm=0; mm<m; mm++) {
	mmean[m][mm] = vector<double>(_si->Ndim, 0.0);
	mvar [m][mm] = vector<double>(_si->Ndim, 0.0);
	//
	evalsM[m][mm].setsize(1, _si->Ndim);
	meanM [m][mm].setsize(1, _si->Ndim);
	covarM[m][mm].setsize(1, _si->Ndim, 1, _si->Ndim);
	eigenM[m][mm].setsize(1, _si->Ndim, 1, _si->Ndim);
	TeigeM[m][mm].setsize(1, _si->Ndim, 1, _si->Ndim);
	//
	evalsM[m][mm].zero();
	meanM [m][mm].zero();
	covarM[m][mm].zero();
	eigenM[m][mm].zero();
	TeigeM[m][mm].zero();
      }
    }
    //
    icut[m] = 0;
    mcut[m] = vector<int>(m);
  }
}


void EnsembleStat::PrintDensity(int dim1, int dim2, int num1, int num2,
				string file)
{
  // Only root node needs to do this
  if (myid) return;

  // Sanity check
  if (dim1 == dim2) {
    cout << "This routine provides a density cut through the plane specifed by dim1  " << std::endl
	 << "and dim2.  Therefore, dim1 must NOT EQUAL dim2.  Please try again . . . " << std::endl;
    return;
  }

  if (_si->ptype == StateInfo::RJTwo) {
    cout << "PrintDensity is not implemented for an RJTwo state" << endl;
    return;
  }

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {

    if (static_cast<unsigned>(dim1) >= _si->M*_si->Ndim || 
	static_cast<unsigned>(dim2) >= _si->M*_si->Ndim) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ndim=" << _si->Ndim << endl;
      return;
    }
  } else {

    if (static_cast<unsigned>(dim1) >= _si->Ntot || 
	static_cast<unsigned>(dim2) >= _si->Ntot) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ntot=" << _si->Ntot << endl;
      return;
    }
  }
    
  ofstream out(file.c_str());
  if (!out) {
    cout << "PrintDensity: could not open file <" << file << "> for output"
	 << endl;
    return;
  }

  ComputeDistribution();
  
  State p(_si);
  
  unsigned M = 1, prefix = 0;

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {
    
    double dmax = 0.0;

    for (unsigned m=0; m<fraction.size(); m++) {
      if (fraction[m].first > dmax) {
	dmax = fraction[m].first;
	M    = fraction[m].second;
      }
    }

    if (dmax <= 0.0) {
      cout << "PrintDensity: could not find any states, strange" << endl;
      return;
    }

    unsigned nd = _si->Ndim;

    vector<double> wght(M);
    vector< vector<double> > phi(M);

    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
      int m   = it->first;
      wght[m] = vmean[M][m];
      phi[m]  = vector<double>(nd);
      for (unsigned n=0; n<nd; n++) phi[m][n] = vmean[M][M+nd*m+n];
    }
    if (_si->ptype == StateInfo::Mixture) {
      p.setState(M, wght, phi);
    } else {
      vector<double> ext(_si->Next);
      for (unsigned n=0; n<_si->Next; n++) vmean[M][M*(1+nd)+n];
      p.setState(M, wght, phi, ext);
    }

    prefix = M;

  } else {
    p.setState(vmean[M]);
  }

  double xmin = vlower[prefix + dim1];
  double xmax = vupper[prefix + dim1];

  double ymin = vlower[prefix + dim2];
  double ymax = vupper[prefix + dim2];

  double dx   = (xmax - xmin)/(num1-1);
  double dy   = (ymax - ymin)/(num2-1);

  out << "# [" << xmin << ", " << ymin 
      << "] ==> [" << xmax << ", " << ymax << "]" << std::endl;

  for (int i=0; i<num1; i++) {

    p.vec(prefix + dim1) = xmin + dx*i;
    
    for (int j=0; j<num2; j++) {
    
      p.vec(prefix + dim2) = ymin + dy*j;

      out << setw(18) << xmin + dx*i
	  << setw(18) << xmin + dy*j
	  << setw(18) << PDF(p)
	  << endl;
    }
    out << endl;
  }

}

