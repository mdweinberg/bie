
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

#include <PostMixturePrior.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PostMixturePrior)

using namespace BIE;


PostMixturePrior::PostMixturePrior
(MixturePrior * old, Ensemble* dist) : MixturePrior(old)
{
  _dist        = dist;
  _lower       = old->lower();
  _upper       = old->upper();
  _manage_dist = false;
  _clamp       = true;
  _sample      = false;

}


PostMixturePrior::PostMixturePrior
(MixturePrior * old, Ensemble * dist,
 int level, int nburn, string filename, int keypos)
  : MixturePrior(old)
{
  _dist        = _dist->New();
  _lower       = old->lower();
  _upper       = old->upper();
  _manage_dist = false;
  _clamp       = true;
  _sample      = false;

  _dist->Reset(_si, level, nburn, filename, keypos);

}


PostMixturePrior::~PostMixturePrior()
{
  if (_manage_dist) delete _dist;
}

void PostMixturePrior::SamplePrior(unsigned &M,
				   vector<double>& wght, 
				   vector< vector<double> >& p)
{
  State pp(_dist->Sample());

  M = pp.M();

  unsigned indx = 0, iter = 0;

  if (_clamp) {
    while (iter++ < max_iter) {
      bool check = true;
      for (unsigned i=0; i<M; i++) 
	for (unsigned j=0; j<_si->Ndim; j++) {
	  indx = _si->M + _si->Ndim*i + j;
	  if (pp.Phi(i, j) > _upper[indx]) check = false;
	  if (pp.Phi(i, j) < _lower[indx]) check = false;
	}
      if (check) break;
      pp = _dist->Sample();
      M = pp.M();
    }

    if (iter >= max_iter) {
      for (unsigned i=0; i<M; i++) 
	for (unsigned j=0; j<_si->Ndim; j++) {
	  indx = _si->M + _si->Ndim*i + j;
	  if (pp.Phi(i, j) > _upper[indx]) pp.Phi(i, j) = _upper[indx];
	  if (pp.Phi(i, j) < _lower[indx]) pp.Phi(i, j) = _lower[indx];
	}
    
    }
  }

  double sum = 0.0;
  for (unsigned i=0; i<M; i++) {
    wght[i] = pp.Wght(i);
    sum += wght[i];
  }
  for (unsigned i=0; i<M; i++) {
    wght[i] /= sum;
    for (unsigned j=0; j<_si->Ndim; j++) p[i][j] = pp.Phi(i, j);
  }
}


void PostMixturePrior::SamplePrior(State *s)
{
  *s = _dist->Sample();
  
  unsigned indx = 0, iter = 0;

  if (_clamp) {
    while (iter++ < max_iter) {
      bool check = true;
      for (unsigned i=0; i<s->M(); i++) 
	for (unsigned j=0; j<_si->Ndim; j++) {
	  indx = _si->M + _si->Ndim*i + j;
	  if (s->Phi(i, j) > _upper[indx]) check = false;
	  if (s->Phi(i, j) < _lower[indx]) check = false;
	}
      if (check) break;
      // Try again:
      *s = _dist->Sample();
    }

    if (iter >= max_iter) {
      for (unsigned i=0; i<s->M(); i++) 
	for (unsigned j=0; j<_si->Ndim; j++) {
	  indx = _si->M + _si->Ndim*i + j;
	  if (s->Phi(i, j) > _upper[indx]) s->Phi(i, j) = _upper[indx];
	  if (s->Phi(i, j) < _lower[indx]) s->Phi(i, j) = _lower[indx];
	}    
    }
  }

  double sum = 0.0;
  for (unsigned i=0; i<s->M(); i++) sum += s->Wght(i);
  for (unsigned i=0; i<s->M(); i++) s->Wght(i) /= sum;

}


void PostMixturePrior::SamplePrior(unsigned M, unsigned n, vector<double>& V)
{
  V = _dist->SampleMarginal(M, n);

  unsigned indx=0, iter=0;
  if (_clamp) {
    while (iter++ < max_iter) {
      bool check = true;
      for (unsigned j=0; j<_si->Ndim; j++) {
	indx = _si->M + _si->Ndim*n + j;
	if (V[j] > _upper[indx]) check = false;
	if (V[j] < _lower[indx]) check = false;
      }
      if (check) break;
      V = _dist->Sample();
    }

    if (iter >= max_iter) {
      for (unsigned j=0; j<_si->Ndim; j++) {
	indx = _si->M + _si->Ndim*n + j;
	if (V[j] > _upper[indx])  V[j] = _upper[indx];
	if (V[j] < _lower[indx])  V[j] = _lower[indx];
      }
    }
  }

}


PostMixturePrior* PostMixturePrior::New()
{
  PostMixturePrior *p = new PostMixturePrior();

  p->_si   = _si;
  p->_dist = _dist;

  vector<double> ret(_si->M*(_si->Ndim+1));

  p->unit   = UniformPtr(new Uniform (-1.0, 1.0, BIEgen));
  p->normal = NormalPtr (new Normal  ( 0.0, 1.0, BIEgen));
  
  return p;
}


void 
PostMixturePrior::SampleProposal(Chain& ch, MHWidth* width)
{
  const int safety = 10000;
  bool check = true, done = false;
  int count = 0;
  int M = ch.M0(), Ndim = ch.Cur()->Ndim(), Next = ch.Cur()->Next();
  State s(*ch.Cur()), m(*ch.Cur());
  
				// Ideal broadening for optimizing mixing
				// properties for Gasssian targets and 
				// proposals following Gelman et al. 1996,
				// [Efficient Metropolis jumping rules,
				// Bayesian Statistics V, eds. Bernado, 
				// Berger, David & Smith, Oxford,  pp. 599-608]
  double factor = ch.factor;
  factor *= 2.4/sqrt(static_cast<float>(Ndim)) * width_factor;

				// Reset the temporary to the correct
				// dimension, if necessary
  ch.Reset1(M);

  while (!done && count<safety) {
    if (_sample) {
				// Sample posterior distribution
      s = _dist->Sample(M);
      m = _dist->Mean(M);
      for (unsigned int i=1; i<s.size(); i++) 
	s[i] = factor * (s[i] - m[i]);

    } else {

      for (int i=0; i<M; i++) {
	s.Wght(i) = width_factor * width->widthM(M) * (*normal)();
	for (int j=0; j<Ndim; j++) 
	  s.Phi(i, j) = width_factor * width->widthM(M, j) * (*normal)();
      }
      
      for (int j=0; j<Next; j++)
	s.Ext(j) = width_factor * width->width(j) * (*normal)();
    }
    
    // Compute normalized weights
    double sum = 0.0;
    for (int i=0; i<M; i++) {
      // Enforce positivity
      ch.Wght1(i) = max<double>(0.0, ch.Wght0(i) + s.Wght(i));
      sum += ch.Wght1(i);
    }
    for (int i=0; i<M; i++) ch.Wght1(i) /= sum;
    
    // Check weights
    for (int i=0; i<M; i++) {
      if (ch.Wght1(i) > 1.0) check = false;
      if (ch.Wght1(i) < 0.0) check = false;
      // Sanity check
      if (std::isinf(ch.Wght1(i)) || 
	  std::isnan(ch.Wght1(i)) ) check = false;
    }
    
				// Save some CPU time but not executing
    if (check) {		// if state is already marked as bad
      
      for (int i=0; i<M; i++)
	for (int j=0; j<Ndim; j++) 
	  ch.Phi1(i, j) =  ch.Phi0(i, j) + s.Phi(i, j);
      
      for (int j=0; j<Next; j++)
	ch.Ext1(j) =  ch.Ext0(j) + s.Ext(j);
      
      unsigned indx;
      for (int i=0; i<M; i++) {
	for (int j=0; j<Ndim; j++) {
	  indx = _si->M + _si->Ndim*i + j;
	  if (ch.Phi1(i, j) > _upper[indx]) check = false;
	  if (ch.Phi1(i, j) < _lower[indx]) check = false;
	  // Sanity check
	  if (std::isinf(ch.Phi1(i, j)) || 
	      std::isnan(ch.Phi1(i, j)) ) check = false;
	}
      }
      
      for (unsigned j=0; j<_si->Next; j++) {
	indx = _si->M*(1 + _si->Ndim) + j;
	if (ch.Ext1(j) > _upper[indx]) check = false;
	if (ch.Ext1(j) < _lower[indx]) check = false;
	  // Sanity check
	if (std::isinf(ch.Ext1(j)) || 
	    std::isnan(ch.Ext1(j)) ) check = false;
      }
      
    }

    // State is in bounds: we have our proposal
    if (check) {
      done = true;
      ch.Order1();
    }

    count++;
  }
    
  //
  // Revert to the original state, if unsuccessful
  //
  if (!done) ch.AssignState1(ch);

  //
  // For debugging . . .
  //
  if (0) {
    if (myid==0) {
      cout << "TEST PostMixturePrior, level=" << current_level << endl;
      ch.PrintState ();
      ch.PrintState1();
      cout << setw(80) << setfill('-') << '-' << endl << setfill(' ');
      cout << "Proposed delta: ";
      if (done) cout << " good" << endl;
      else      cout << " bad"  << endl;
      for (unsigned i=0; i<s.M(); i++) {
	cout << setw(4) << i+1 << ": ";
	cout << setw(15) << s.Wght(i) << " | ";
	for (unsigned j=0; j<s.Ndim(); j++)
	  cout << setw(15) << s.Phi(i, j);
	cout << endl;
      }
      cout << setw(80) << setfill('-') << '-' << endl << setfill(' ');
    }
  }
}
    
