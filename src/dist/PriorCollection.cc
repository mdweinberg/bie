
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

#include <BIEconfig.h>
#include <ACG.h>
#include <Uniform.h>
#include <Normal.h>
#include <Dirichlet.h>
#include <BIEException.h>
#include <PriorCollection.h>

#include <boost/serialization/export.hpp>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PriorCollection)

using namespace BIE;

bool     PriorCollection::WRAP   = true;
unsigned PriorCollection::ITMAX  = 10000;
double   PriorCollection::min_prob = 1.0e-30;

PriorCollection::PriorCollection()
{
  pmean   = 0.0;
  Alpha   = 1.0;

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();
};

PriorCollection::PriorCollection(StateInfo *si) : Prior(si)
{
  pmean   = 0.0;
  Alpha   = 1.0;
  props   = vector<TransitionProb*>(_si->Ntot, NULL);
  ret = vector<double>(_si->Ntot);

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
};


PriorCollection::PriorCollection(StateInfo *si, double alpha) : Prior(si)
{
  pmean   = 0.0;
  props   = vector<TransitionProb*>(_si->Ndim, NULL);
  Alpha   = alpha;

  ret = vector<double>(_si->M*(_si->Ndim+1));

  pmean   = 0.0;

  Alpha   = alpha;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/(i+1));
    mix.push_back(new Dirichlet(&A));
  }

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();
};

PriorCollection::PriorCollection(PriorCollection * p) : Prior(p)
{
  pmean   = p->pmean;
  Alpha   = p->Alpha;

  if (_si->Ndim>0) {
    ret   = vector<double>(_si->Ndim);
    dists = p->dists;
    props = p->props;
  }

  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/(i+1));
    mix.push_back(new Dirichlet(&A));
  }

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();
  if (pmean > 0.0)
    pois = PoissonPtr(new Poisson(pmean, BIEgen));
  else
    pois = PoissonPtr();
};


PriorCollection* PriorCollection::New()
{
  switch (_si->ptype) {
  case StateInfo::None:
  case StateInfo::Block:
    return new PriorCollection(_si);
  default:
    return new PriorCollection(_si, Alpha);
  }
}

vector<double> 
PriorCollection::create_standard_vector(State& s, const vector<int>& m)
{
  // The return vector
  vector<double> ret;

  // The dimension of the output vector
  int sz  = m.size();

  // Dimension of the state vector
  int dim = s.N();

  for (int i=0; i<sz; i++) {
    if (m[i]>=dim) {
      ostringstream sout;
      sout << "PriorCollection plumbing error: you want to map from position "
	   << m[i] << "to position " << i << " but the full dimension is "
	   << dim << "!!";
      throw PriorException(sout.str().c_str(), __FILE__, __LINE__);
    }

    ret.push_back(s[m[i]]);
  }

  return ret;
}

vector<double> 
PriorCollection::create_mix_comp_vector(State& s, int i, const vector<int>& m)
{
  // The return vector
  vector<double> ret;

  // The dimension of the output vector
  int sz  = m.size();

  // Dimension of the allowable state vector
  int dim = s.Ndim();

  for (int i=0; i<sz; i++) {
    if (m[i]>=dim) {
      ostringstream sout;
      sout << "PriorCollection plumbing error: you want to map from position "
	   << m[i] << "to position " << i << " but the full dimension is "
	   << dim << "!!";
      throw PriorException(sout.str().c_str(), __FILE__, __LINE__);
    }

    ret.push_back(s.Phi(i, m[i]));
  }

  return ret;
}

vector<double> 
PriorCollection::create_extended_vector(State& s, const vector<int>& m)
{
  // The return vector
  vector<double> ret;

  // The dimension of the output vector
  int sz  = m.size();

  // Dimension of the state vector
  int dim = s.Next();

  for (int i=0; i<sz; i++) {
    if (m[i]>=dim) {
      ostringstream sout;
      sout << "PriorCollection plumbing error: you want to map from position "
	   << m[i] << "to position " << i << " but the full dimension is "
	   << dim << "!!";
      throw PriorException(sout.str().c_str(), __FILE__, __LINE__);
    }

    ret.push_back(s.Ext(m[i]));
  }

  return ret;
}

void PriorCollection::SampleProposal(Chain& ch, MHWidth* w)
{
  State OLD(*ch.Cur());
  State NEW(*ch.Cur());

  // What kind of state do we have here?

  switch (ch.Cur()->Type()) {
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    {
      unsigned Ntot = ch.Cur()->N();
      //
      // Model parameters
      //
      for (unsigned j=0; j<Ntot; j++) {
	NEW[j] = (*props[j])(OLD[j], ch.factor*w->width(j));
	if (std::isnan(NEW[j])) {
	  cerr << "NaN in PriorCollection, rank: " << j;
	}
      }
    }
    break;

  case StateInfo::Mixture:
  case StateInfo::Extended:
    {
      unsigned Mcur = ch.M0(), Ndim = ch.Cur()->Ndim();
      double sum=0.0, z;
      
      for (unsigned i=0; i<Mcur; i++) {
	/*
	  Mapping between z ~ (-infty, infty) and w ~ [0, 1]
	  
	  z = log(w/(1-w))
	  
	  w = e^z/(1+e^z) = 1/(1+e^{-z})
	  
	*/
				// Shift transformed variable
	z = log(OLD.Wght(i)/(1.0 - OLD.Wght(i))) + 
	  ch.factor*w->widthM(Mcur)*(*normal)();

				// Transform back to unit interval
	NEW.Wght(i) = 1.0/(1.0 + exp(-z));

	// Impose limits
	NEW.Wght(i) = max<double>(0.0, min<double>(1.0, NEW.Wght(i)));
	
	sum += NEW.Wght(i);
      }
      for (unsigned i=0; i<Mcur; i++) NEW.Wght(i) /= sum;

      //
      // Model parameters
      //
      for (unsigned j=0; j<Ndim; j++) {
	for (unsigned i=0; i<Mcur; i++)
	  NEW.Phi(i, j) = 
	    (*props[j])(OLD.Phi(i, j), ch.factor*w->widthM(Mcur, j));
      }
    
      // Sanity check
      for (unsigned i=0; i<Mcur; i++) {
	for (unsigned j=0; j<Ndim; j++) {
	  if (std::isnan(NEW.Phi(i, j))) {
	    cerr << "NaN in PriorCollection [after]: ";
	    if (Mcur>1) cerr << " imix=" << i;
	    cerr << " idim=" << j << endl;
	  }
	}
      }
    }
  }

  EnforceBoundsState(NEW);

  *ch.Tri() = NEW;
      
}

double PriorCollection::PDF(State& S)
{
  double ret  = 1.0;
  
  if (S.Type() == StateInfo::Mixture ||
      S.Type() == StateInfo::Extended ) 
    {
      unsigned szM = distsM.size();
      unsigned szE = distsE.size();

				// Get mixture prob
      State T(S.Wght());
      ret *= mix[S.M()-1]->PDF(T);

				// Get component probs
      for (unsigned j=0; j<S.M(); j++) {
	for (unsigned n=0; n<szM; n++) {
	  State T(create_mix_comp_vector(S, j, distsM[n].second));
	  ret *= dists[n].first->PDF(T);
	}
      }

				// Get extended component
      if (S.Type() == StateInfo::Extended ) {
	for (unsigned n=0; n<szE; n++) {
	  State T(create_extended_vector(S, distsE[n].second));
	  ret *= dists[n].first->PDF(T);
	}
      }
    } 
  else 				// Standard state
    {
      unsigned sz = dists.size();

      for (unsigned n=0; n<sz; n++) {
	State T(create_standard_vector(S, dists[n].second));
	ret *= dists[n].first->PDF(T);
      }
    }
  
  return ret;
}

double PriorCollection::logPDF(State& S)
{
  double ret = 0.0;
  
  if (S.Type() == StateInfo::Mixture ||
      S.Type() == StateInfo::Extended ) 
    {
      unsigned szM = distsM.size();
      unsigned szE = distsE.size();

				// Get mixture prob
      State T(S.Wght());
      ret += mix[S.M()-1]->logPDF(T);

				// Get component probs
      for (unsigned j=0; j<S.M(); j++) {
	for (unsigned n=0; n<szM; n++) {
	  State T(create_mix_comp_vector(S, j, distsM[n].second));
	  ret += dists[n].first->logPDF(T);
	}
      }

				// Get extended component
      if (S.Type() == StateInfo::Extended ) {
	for (unsigned n=0; n<szE; n++) {
	  State T(create_extended_vector(S, distsE[n].second));
	  ret += dists[n].first->logPDF(T);
	}
      }
    } 
  else 				// Standard state
    {
      unsigned sz = dists.size();
      for (unsigned n=0; n<sz; n++) {
	State T(create_standard_vector(S, dists[n].second));
	ret += dists[n].first->logPDF(T);
      }
    }
  
  return ret;
}

vector<double> PriorCollection::lower(void)
{
  vector<double> ret;
  
  if (_si->ptype == StateInfo::Mixture ||
      _si->ptype == StateInfo::Extended ) 
    {
      unsigned szM = distsM.size();
      unsigned szE = distsE.size();

				// Get mixture prob
      ret = std::vector<double>(_si->M, 0.0);

      std::vector<double> t(_si->Ndim);

      for (unsigned n=0; n<szM; n++) {
	std::vector<double> lower = distsM[n].first->lower();
	for (unsigned i=0; i<lower.size(); i++) 
	  t[distsM[n].second[i]] = lower[i];
      }

      for (unsigned j=0; j<_si->M; j++) 
	std::copy(t.begin(), t.end(), back_inserter(ret));

      if (_si->ptype == StateInfo::Extended) {
	std::vector<double> t(_si->Next);
	for (unsigned n=0; n<szE; n++) {
	  std::vector<double> lower = distsE[n].first->lower();
	  for (unsigned i=0; i<lower.size(); i++) 
	    t[distsE[n].second[i]] = lower[i];
	}

	std::copy(t.begin(), t.end(), back_inserter(ret));
      }
    }
  else 
    {
      unsigned sz = dists.size();
      ret.resize(_si->Ntot);

      for (unsigned n=0; n<sz; n++) {
	std::vector<double> lower = dists[n].first->lower();
	for (unsigned i=0; i<lower.size(); i++) 
	  ret[dists[n].second[i]] = lower[i];
      }
    }

  return ret;

};

vector<double> PriorCollection::upper(void)
{
  vector<double> ret;
  
  if (_si->ptype == StateInfo::Mixture ||
      _si->ptype == StateInfo::Extended ) 
    {
      unsigned szM = distsM.size();
      unsigned szE = distsE.size();

				// Get mixture prob
      ret = std::vector<double>(_si->M, 0.0);

      std::vector<double> t(_si->Ndim);

      for (unsigned n=0; n<szM; n++) {
	std::vector<double> upper = distsM[n].first->upper();
	for (unsigned i=0; i<upper.size(); i++) t[distsM[n].second[i]] = upper[i];
      }

      for (unsigned j=0; j<_si->M; j++) 
	std::copy(t.begin(), t.end(), back_inserter(ret));

      if (_si->ptype == StateInfo::Extended) {
	std::vector<double> t(_si->Next);
	for (unsigned n=0; n<szE; n++) {
	  std::vector<double> upper = distsE[n].first->upper();
	  for (unsigned i=0; i<upper.size(); i++) 
	    t[distsE[n].second[i]] = upper[i];
	}

	std::copy(t.begin(), t.end(), back_inserter(ret));
      }
    }
  else 
    {
      unsigned sz = dists.size();
      ret.resize(_si->Ntot);

      for (unsigned n=0; n<sz; n++) {
	std::vector<double> upper = dists[n].first->upper();
	for (unsigned i=0; i<upper.size(); i++) 
	  ret[dists[n].second[i]] = upper[i];
      }
    }
  
  return ret;

};

State PriorCollection::Sample(void)
{
  State ret(_si);
  
  if (_si->ptype == StateInfo::Mixture ||
      _si->ptype == StateInfo::Extended ) 
    {
      unsigned szM = distsM.size();
      unsigned szE = distsE.size();

				// Get mixture prob
      std::vector<double> t = mix[_si->M-1]->Sample();
      std::copy(t.begin(), t.end(), ret.begin());
      size_t pos = _si->M;

      t.resize(_si->Ndim);
      for (unsigned i=0; i<_si->M; i++) {
	for (unsigned n=0; n<szM; n++) {
	  std::vector<double> v = distsM[n].first->Sample();
	  for (unsigned i=0; i<v.size(); i++) 
	    t[distsM[n].second[i]] = v[i];
	}
	std::copy(t.begin(), t.end(), ret.begin()+pos);
	pos += _si->Ndim;
      }

      if (_si->ptype == StateInfo::Extended) {
	t.resize(_si->Next);
	for (unsigned n=0; n<szE; n++) {
	  std::vector<double> v = distsE[n].first->Sample();
	  for (unsigned i=0; i<v.size(); i++) 
	    t[dists[n].second[i]] = v[i];
	}
	std::copy(t.begin(), t.end(), ret.end()+pos);
      }
    }
  else 
    {
      unsigned sz = dists.size();
      for (unsigned n=0; n<sz; n++) {
	std::vector<double> v = dists[n].first->Sample();
	for (unsigned i=0; i<v.size(); i++) 
	  ret[dists[n].second[i]] = v[i];
      }
    }

  return ret;
}

void PriorCollection::SamplePrior(State *s)
{
  vector<double> ret(_si->Ntot);

  if (_si->ptype == StateInfo::Mixture ||
      _si->ptype == StateInfo::Extended ) 
    {
      unsigned szM = distsM.size();
      unsigned szE = distsE.size();

				// Get mixture prob
      std::vector<double> t = mix[_si->M-1]->Sample();
      std::copy(t.begin(), t.end(), ret.begin());
      size_t pos = _si->M;

      t.resize(_si->Ndim);
      for (unsigned i=0; i<_si->M; i++) {
	for (unsigned n=0; n<szM; n++) {
	  std::vector<double> v = distsM[n].first->Sample();
	  for (unsigned i=0; i<v.size(); i++) 
	    t[distsM[n].second[i]] = v[i];
	}
	std::copy(t.begin(), t.end(), ret.begin()+pos);
	pos += _si->Ndim;
      }

      if (_si->ptype == StateInfo::Extended) {
	t.resize(_si->Next);
	for (unsigned n=0; n<szE; n++) {
	  std::vector<double> v = distsE[n].first->Sample();
	  for (unsigned i=0; i<v.size(); i++) 
	    t[dists[n].second[i]] = v[i];
	}
	std::copy(t.begin(), t.end(), ret.end()+pos);
      }
    }
  else 
    {
      unsigned sz = dists.size();
      for (unsigned n=0; n<sz; n++) {
	std::vector<double> v = dists[n].first->Sample();
	for (unsigned i=0; i<v.size(); i++) 
	  ret[dists[n].second[i]] = v[i];
      }
    }

  s->setState(ret);
}


void PriorCollection::EnforceBounds(Chain &ch)
{
  unsigned sz = dists.size();
  for (unsigned n=0; n<sz; n++) {
    // Does this component need positive definite normalized weights?
    if (distW[n]) {
      size_t vsz = dists[n].second.size();
      // L1 norm
      double sum = 0.0;
      for (size_t i=0; i<vsz; i++) {
	size_t j = dists[n].second[i];
	double v;		// Weights must be > 0
	if (WRAP) v = fabs(ch.Vec0(j));
	else      v = std::max<double>(0.0, ch.Vec0(j));
	ch.Vec0(j) = v;
	sum += v;
      }
      // Apply the norm
      for (size_t i=0; i<vsz; i++) {
	size_t j = dists[n].second[i];
	ch.Vec0(j) /= sum;
      }
    }
  }
}

void PriorCollection::EnforceBoundsState(State &s)
{
  size_t sz = dists.size();
  for (size_t n=0; n<sz; n++) {
    // Does this component need positive definite normalized weights?
    if (distW[n]) {
      size_t vsz = dists[n].second.size();
      // L1 norm
      double sum = 0.0;
      for (size_t i=0; i<vsz; i++) {
	size_t j = dists[n].second[i];
	if (WRAP) s[j] = fabs(s[j]);
	else      s[j] = std::max<double>(0.0, s[j]);
	sum += s[j];
      }
      // Apply the norm
      for (size_t i=0; i<vsz; i++) {
	size_t j = dists[n].second[i];
	s[j] /= sum;
      }
    }
  }
}
