#include <State.h>
#include <CDFGenerator.h>

//
// This version of the constructor samples the distribution
//
CDFGenerator::CDFGenerator(int N, int ncut, FctDVPtr F)
{
  vector< vector<double> > points, data;
  vector<double> weights, beta;
  unsigned pdim = 0;
  PairDV pt;

  for (int i=0; i<N; i++) {
    pt = F->Sampler();
    weights.push_back(1.0);
    beta   .push_back(1.0);
    data   .push_back(pt.first);
    points .push_back(pt.second);
    if (i==0) {
      maxP = pt.first;
      pdim = maxP.size();
    } else {
      for (unsigned j=0; j<pdim; j++)
	maxP[j] = max<double>(pt.first[j], maxP[j]);
    }
  }

  for (int i=0; i<N; i++) {
    for (unsigned j=0; j<pdim; j++)
      data[i][j] = exp(data[i][j]-maxP[j]);
  }

  kd = boost::shared_ptr<KDTree>
    (new KDTree(points[0].size(), points.size(), ncut, points, weights, beta));
     
  kd->createMeasureList(ncut, data);
}


//
// This version of the constructor uses an existing sample
//
CDFGenerator::CDFGenerator(int N, int ncut, Ensemble* ens, int nburn)
{
  deque<StateData> states = ens->getStates();
  int Ntot                = ens->Nstates();
  int Nsav                = states.size();

  vector< vector<double> > points, data;
  vector<double> weights, beta;
  unsigned pdim = 0;

  for (int i=max<int>(0, Ntot-nburn-Nsav); i<Nsav; i++) {
    weights.push_back(1.0);
    beta   .push_back(1.0);
    data   .push_back(states[i].prob);
    points .push_back(states[i].p.vec());
    if (i==0) {
      maxP = states[i].prob;
      pdim = maxP.size();
    } else {
      for (unsigned j=0; j<pdim; j++)
	maxP[j] = max<double>(states[i].prob[j], maxP[j]);
    }
  }

  for (int i=0; i<N; i++) {
    for (unsigned j=0; j<pdim; j++)
      data[i][j] = exp(data[i][j]-maxP[j]);
  }

  kd = boost::shared_ptr<KDTree>
    (new KDTree(points[0].size(), points.size(), ncut, points, weights, beta));
     
  kd->createMeasureList(ncut, data);
}


double CDFGenerator::FTheta(vector<double>& data,
			    vector<double>& wght,
			    vector<vector<double> >& loc,
			    vector<vector<double> >& scl)
{
  double ret = 0.0;
  unsigned ddim = data.size();
  vector<double> d(ddim);

  for (unsigned n=0; n<wght.size(); n++) {
    for (unsigned i=0; i<ddim; i++) d[i] = (data[i] - loc[n][i])/scl[n][i];
    ret += wght[n] * kd->FTheta(d);
  }

  return ret;
}

double CDFGenerator::FTheta(vector<double>& data,
			    vector<double>& loc,
			    vector<double>& scl)
{
  unsigned ddim = data.size();
  vector<double> d(ddim);

  for (unsigned i=0; i<ddim; i++) d[i] = (data[i] - loc[i])/scl[i];
  double ret = kd->FTheta(d);
  
  return ret;
}

