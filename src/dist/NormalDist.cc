#include <cfloat>

#include <NormalDist.h>
#include <ACG.h>
#include <Normal.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::NormalDist)

using namespace BIE;

NormalDist::NormalDist()
{
  _type = NormalAdd;
  mean  = 0.0;
  var   = 1.0;
  vmin  = -DBL_MAX;
  vmax  =  DBL_MAX; 
  mincdf = 0.0;
  maxcdf = 1.0;
  norm  = 1.0;
}

NormalDist::NormalDist
(double m, double v)
{
  _type  = NormalAdd;
  mean   = m;
  var    = v;
  vmin   = -DBL_MAX;
  vmax   =  DBL_MAX;
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = 1.0;
  
  normal = NormalPtr(new Normal(mean, var, BIEgen));
}

NormalDist::NormalDist
(double m, double v, double minv, double maxv) 
{
  _type  = NormalAdd;
  mean   = m;
  var    = v;
  vmin   = minv;
  vmax   = maxv;
  mincdf = 0.5*(1.0+erf((minv-m)/sqrt(2.0*v)));
  maxcdf = 0.5*(1.0+erf((maxv-m)/sqrt(2.0*v)));
  norm   = maxcdf - mincdf;
  
  normal = NormalPtr(new Normal(mean, var, BIEgen));
}

NormalDist::NormalDist
(double v)
{
  _type  = NormalAdd;
  mean   = 0.0;
  var    = v;
  vmin   = -DBL_MAX;
  vmax   =  DBL_MAX;
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = maxcdf - mincdf;
  
  normal = NormalPtr(new Normal(mean, var, BIEgen));
}

NormalDist::NormalDist
(double v, double minv, double maxv) 
{
  _type  = NormalAdd;
  mean   = 0.0;
  var    = v;
  vmin   = minv;
  vmax   = maxv;
  mincdf = 0.5*(1.0+erf((minv-mean)/sqrt(2.0*v)));
  maxcdf = 0.5*(1.0+erf((maxv-mean)/sqrt(2.0*v)));
  norm   = maxcdf - mincdf;
  
  normal = NormalPtr(new Normal(mean, var, BIEgen));
}

Distribution* NormalDist::New() 
{
  return new NormalDist(mean, var, vmin, vmax);
}
  
double NormalDist::PDF(State& x) 
{
  if (x[0]>=vmax || x[0]<=vmin)
    return 0.0;
  else
    return exp(-0.5*(x[0] - mean)*(x[0] - mean)/var)/sqrt(2.0*M_PI*var)/norm;
}

double NormalDist::logPDF(State& x) 
{
  if (x[0]>vmax || x[0]<vmin)
    throw ImpossibleStateException(__FILE__, __LINE__);
  else 
    return -0.5*(x[0] - mean)*(x[0] - mean)/var 
      - 0.5*log(2.0*M_PI*var) - log(norm);
}

vector<double> NormalDist::lower(void)
{ return vector<double>(1, vmin); }

vector<double> NormalDist::upper(void)
{ return vector<double>(1, vmax); }

vector<double> NormalDist::Mean(void)
{ return vector<double>(1, mean); }

vector<double> NormalDist::StdDev(void)
{ return vector<double>(1, sqrt(var)); }

double NormalDist::CDF(State& x) {
  double ans;
  if (x[0]<vmin)
    ans = 0.0;
  else if (x[0]>vmax)
    ans = 1.0;
  else
    ans = 0.5*(1.0+erf((x[0]-mean)/sqrt(2.0*var))) - mincdf;
  return ans;
}

vector<double> NormalDist::Moments(int i) 
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = mean;
    break;
  case 2:
    ans = var + mean*mean;
    break;
  default:
    cerr << "NormalDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State NormalDist::Sample(void) 
{
  const int itmax=10000;
  int i=0;
  double x;
  while (i<itmax) {
    x = (*normal)();
    if (x<=vmax && x>=vmin) break;
  }
  if (i==itmax) cerr << "NormalDist: sampler did not converge in "
		     << itmax << " iterations" << endl;
  return State(vector<double>(1, x));
}

