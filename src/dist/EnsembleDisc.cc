#define DEBUG_TIMER		// For timing metric sort in
				// ComputeDistribution()
#include <cfloat>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

#include <BIEException.h>
#include <BIEMutex.h>
#include <BIEmpi.h>

#include <EnsembleDisc.h>
#include <gfunction.h>
#include <gvariable.h>

using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(BIE::EnsembleDisc)

static
bool compare_vector (const std::vector<double>& a, 
		     const std::vector<double>& b)
{
  for (size_t n=0; n<min<size_t>(a.size(), b.size()); n++) {
    if ( fabs(a[n]-b[n]) > min<double>(fabs(a[n]), fabs(b[n]))*1.0e-10 ) 
      return a[n] < b[n];
  }
  return false;
}
				// Set to true for some extra verbose
				// debugging output
static const bool verbose_out = true;
				// Set unique points culling to true
static const bool uniq_state  = true;
				// Number of nearest neightbors for
				// width estimate
int    EnsembleDisc::nnear    = 32;
				// Target tolerance on density estimation
double EnsembleDisc::errortol = 1.0e-06;
				// Square of the distance threshold on
				// individual state separation
double EnsembleDisc::mindist  = 1.0e-12;
				// Minimum width for KDE
double EnsembleDisc::minwidth = 1.0e-06;
				// Scale each dimension for state separation
bool   EnsembleDisc::scaled   = true;
				// Scale distance metric by variance
bool   EnsembleDisc::mscale   = true;
				// Debug output counter
int    EnsembleDisc::dbg_ctr  = 0;

EnsembleDisc::EnsembleDisc() : Ensemble()
{
  nMax           = 0;
  dist_computed  = false;
  kde_computed   = false;
  kdeM_computed  = false;
  bucketSize     = 1;
  targetSize     = 0;
  bbfac          = 1.0;
  cache          = CachePtr();
}

EnsembleDisc::EnsembleDisc(StateInfo *si,
			   int level, int begin, string filename, int keypos) :
  Ensemble(si, level, begin, filename, keypos)
{
  nMax           = 0;
  dist_computed  = false;
  kde_computed   = false;
  kdeM_computed  = false;
  bucketSize     = 1;
  targetSize     = 0;
  bbfac          = 1.0;
  cache          = CachePtr();
  kernel         = boost::shared_ptr<EpanetchnikovKernel>();

  setDimensions(si);

  ComputeDistribution(begin);
}

EnsembleDisc::EnsembleDisc(StateInfo *si) : Ensemble(si)
{
  nMax           = 0;
  dist_computed  = false;
  kde_computed   = false;
  kdeM_computed  = false;
  bbfac          = 1.0;
  cache          = CachePtr();
  bucketSize     = 1;
  targetSize     = 0;
  kernel         = boost::shared_ptr<EpanetchnikovKernel>();

  setDimensions(si);
}

EnsembleDisc::EnsembleDisc(const EnsembleDisc &p) : Ensemble(p)
{
  nMax           = p.nMax;
  dist_computed  = p.dist_computed;
  kde_computed   = false;
  kdeM_computed  = false;
  bucketSize     = p.bucketSize;
  targetSize     = p.targetSize;
  bbfac          = p.bbfac;
  cnts           = p.cnts;
  mcnt           = p.mcnt;
  vmean          = p.vmean;
  vvar           = p.vvar;
  mmean          = p.mmean;
  mvar           = p.mvar;
  peak           = p.peak;

  if (cache.get())  
    cache  = CachePtr(new StateCache(*(p.cache)));

  if (kernel.get()) 
    kernel = KernelPtr(p.kernel->New());
}

EnsembleDisc::EnsembleDisc(Ensemble *p) : Ensemble(*p)
{
  dist_computed  = false;
  kde_computed   = false;
  kdeM_computed  = false;

  nMax           = 0;
  bucketSize     = 1;
  targetSize     = 0;
  bbfac          = 1.0;
  cache          = CachePtr();
  kernel         = boost::shared_ptr<EpanetchnikovKernel>();

  ComputeDistribution();
}


void EnsembleDisc::Reset(StateInfo* si,
			 int level, int begin, string filename, int keypos)
{
  dist_computed  = false;
  kde_computed   = false;
  kdeM_computed  = false;
  cache          = CachePtr();

  Ensemble::Reset(si, level, begin, filename, keypos);
}

void EnsembleDisc::Reset(StateInfo* si)
{
  dist_computed  = false;
  kde_computed   = false;
  kdeM_computed  = false;
  cache          = CachePtr();

  Ensemble::Reset(si);
}

void EnsembleDisc::setDimensions(StateInfo *si)
{
  Ensemble::setDimensions(si);
}

//
// Data tuple for an ensemble point
//
struct Tuple {
  vector<double> width;
  vector<double> point;
  double weight;
};


static int which_col = 0;

struct tuple_sort_less
{
  bool operator()(const Tuple& a, const Tuple& b)
  {
    return a.point[which_col] < b.point[which_col];
  }
};

//
// Recursive sort on columns for point vector in the tuple list
//
void ntuple_sort(vector<Tuple>::iterator beg,
		 vector<Tuple>::iterator end,
		 int ibeg, int iend)
{
  vector<Tuple>::iterator it1, it2;
  unsigned cnt;

  //
  // Do the sort . . .
  //
  which_col = ibeg;
  sort(beg, end, tuple_sort_less());

  //
  // Are we on the last column?  Then return . . .
  //
  if (ibeg==iend) return;

  //
  // Look for groups of more than one
  //
  it2 = beg;
  while ( it2!=end ) {
    it1 = it2; cnt = 0;
    do {
      it2++; cnt++;
      if (it2==end) break;
    } while ( !((it1->point)[ibeg]<(it2->point)[ibeg]) );
    if (cnt>1) ntuple_sort(it1, it2, ibeg+1, iend);
  }
}

/*
  Sort the points and remove duplicates.  Duplicates will break
  the ball tree construction algorithm.
*/
void EnsembleDisc::sortPoints(map<int, vector< vector<double> > >& points,
			      map<int, vector< vector<double> > >& widths,
			      map<int, vector<double> >&           weight )
{
  map<int, vector< vector<double> > >::iterator it0 = widths.begin();
  map<int, vector< vector<double> > >::iterator it1 = points.begin();
  map<int, vector<double> >::iterator it2 = weight.begin();

  vector<Tuple>::iterator tt0, tt1;

  //
  // Do each stanza for an ordinal mixture separately
  //
  for (;it1 != points.end(); it0++, it1++, it2++) {

    //
    // Load tuple vector for recursive sorting on columns
    //
    vector< vector<double> >::iterator iwid, ipts;
    vector<double>::iterator iwgt;

    Tuple tuple;
    vector<Tuple> tuples;

    for (iwid=it0->second.begin(),  ipts=it1->second.begin(), 
	   iwgt=it2->second.begin(); iwid!=it0->second.end();
	 iwid++, ipts++, iwgt++) {
      //
      tuple.width  = *iwid;
      tuple.point  = *ipts;
      tuple.weight = *iwgt;
      //
      tuples.push_back(tuple);
    }
	 
    //
    // Sort the tuple vector on all point columns
    //
    int dim = it1->second.begin()->size();
    ntuple_sort(tuples.begin(), tuples.end(), 0, dim-1);
    
    //
    // Erase the stanza before loading sorted data sans duplicates
    //
    it0->second.erase(it0->second.begin(), it0->second.end());
    it1->second.erase(it1->second.begin(), it1->second.end());
    it2->second.erase(it2->second.begin(), it2->second.end());

    //
    // Look for adjacent points with (nearly) zero distance
    //
    tt0 = tuples.begin();

    while (tt0 != tuples.end()) {
      
				// Begin at this point
      tt1 = tt0;

      while (1) {
				// Compare with next point.  No more
				// points to compare, break out
				// 
	if (++tt1 == tuples.end()) break;
				// 
				// Compute the absolute Euclidean distance
				// or scaled Euclidean distance
	double dist = 0.0, tmp;
	for (unsigned int k=0; k<tt1->point.size(); k++) {
	  tmp = 
	    ( tt1->point[k] - tt0->point[k] ) * 
	    ( tt1->point[k] - tt0->point[k] );
	  
	  if (scaled)
	    dist += tmp/(tt0->point[k]*tt0->point[k] + mindist);
	  else
	    dist += tmp;
	}

	if (dist > mindist) break;
				// Found a duplicate, consolidate!
	tt0->weight += tt1->weight;

      }

      //
      // Reload the stanza from the tuple vector
      //
      it0->second.push_back(tt0->width);
      it1->second.push_back(tt0->point);
      it2->second.push_back(tt0->weight);

      //
      // Set the current point in the vector
      //
      tt0 = tt1;

      // Move on to the next point
    }

    // Move on to the next stanza
  }

}


//
// Identify coincident points
//
void EnsembleDisc::sortPointsM
(map<int, vector< vector< vector<double> > > >& points,
 map<int, vector< vector< vector<double> > > >& widths,
 map<int, vector< vector<double> > >&           weight )
{
  map<int, vector< vector< vector<double> > > >::iterator it0 = widths.begin();
  map<int, vector< vector< vector<double> > > >::iterator it1 = points.begin();
  map<int, vector< vector<double> > >::iterator it2 = weight.begin();

  vector< vector< vector<double> > >::iterator im0;
  vector< vector< vector<double> > >::iterator im1;
  vector< vector<double> >::iterator im2;

  vector<Tuple>::iterator tt0, tt1;
  
  //
  // Do each stanza for an ordinal mixture separately
  //
  for (;it1 != points.end(); it0++, it1++, it2++) {

    //
    // Do each component separately
    //
    im0 = it0->second.begin();
    im1 = it1->second.begin();
    im2 = it2->second.begin();

    for (;im0 != it0->second.end(); im0++, im1++, im2++) {

      //
      // Load tuple vector for recursive sorting on columns
      //
      vector< vector<double> >::iterator iwid, ipts;
      vector<double>::iterator iwgt;

      Tuple tuple;
      vector<Tuple> tuples;

      for (iwid=im0->begin(), ipts=im1->begin(), iwgt=im2->begin(); 
	   iwid!=im0->end(); iwid++, ipts++, iwgt++) {
	
	tuple.width  = *iwid;
	tuple.point  = *ipts;
	tuple.weight = *iwgt;

	tuples.push_back(tuple);
      }
	 
      //
      // Sort the tuple vector on all point columns
      //
      int dim = im1->begin()->size();
      ntuple_sort(tuples.begin(), tuples.end(), 0, dim-1);
    
      //
      // Erase the stanza before loading sorted data sans duplicates
      //
      im0->erase(im0->begin(), im0->end());
      im1->erase(im1->begin(), im1->end());
      im2->erase(im2->begin(), im2->end());

      //
      // Look for adjacent points with (nearly) zero distance
      //
      tt0 = tuples.begin();
    
      while (tt0 != tuples.end()) {
	
				// Begin at the current point
	tt1 = tt0;
	
	while (1) {
				// Compare with the next point.  No
				// more points to compare, break out
				// 
	  if (++tt1 == tuples.end()) break;
				// 
				// Compute the absolute Euclidean distance
				// or scaled Euclidean distance
	  double dist = 0.0, tmp;
	  for (unsigned int k=0; k<tt1->point.size(); k++) {
	    tmp = 
	      ( tt1->point[k] - tt0->point[k] ) * 
	      ( tt1->point[k] - tt0->point[k] );
	    
	    if (scaled)
	      dist += tmp/(tt1->point[k]*tt1->point[k] + mindist);
	    else
	      dist += tmp;
	  }

	  if (dist > mindist) break;

				// Found a duplicate, consolidate!
	  tt0->weight += tt1->weight;
	}
	
	//
	// Reload the stanza from the tuple vector
	//
	im0->push_back(tt0->width);
	im1->push_back(tt0->point);
	im2->push_back(tt0->weight);

	//
	// Set the current point in the vector
	//
	tt0 = tt1;
	
	// Move on to the next point
      }
      // Move on to the next M space
    }
    // Move on to the next stanza
  }
}


void EnsembleDisc::setDensityScale(clivectord* f)
{
  setDensityScale((*f)());
}

void EnsembleDisc::setDensityScale(std::vector<double> f)
{
  unsigned nsiz = f.size(), nexp;
  bool bad = false;

  switch (_si->ptype) {
  case StateInfo::None:
  case StateInfo::Block:
  case StateInfo::RJTwo:
    nexp = _si->Ntot;
    if (nsiz == _si->Ntot) fscale = f;
    else bad = true;
    break;
    
  case StateInfo::Mixture:
  case StateInfo::Extended:
    nexp = 1 + _si->Ndim + _si->Next;
    if (nsiz == nexp) fscale = f; 
    else bad = true;
  }

  if (bad)
    std::cerr << "EnsembleDisc::setDensity scale: wrong dimensionality, "
	      << "expected " << nexp << " but found " << nsiz
	      << std::endl;
}

void EnsembleDisc::ComputeDensity(void)
{
  if (kde_computed) return;

  kernel = boost::shared_ptr<EpanetchnikovKernel>(new EpanetchnikovKernel);

  map<int, vector< vector<double> > >::iterator _it;

  for (_it=points.begin(); _it != points.end(); _it++) {

    int m = _it->first;
    int N = _it->second.size();

    //--------------------------------------------------
    // Check for unique states
    //--------------------------------------------------
  
    if (uniq_state) {

      sort(points[m].begin(), points[m].end(), compare_vector);

      vector< vector<double> >::iterator it  = points[m].begin();
      vector< vector<double> >::iterator itl = points[m].begin();
      vector< vector<double> >::iterator kt  = widths[m].begin();
      vector< vector<double> >::iterator ktl = widths[m].begin();
      vector<double>::iterator           jt  = weight[m].begin();
      vector<double>::iterator           jtl = weight[m].begin();
      unsigned dup = 1, ndim = points[m][0].size();

      //
      // Some debug output
      //
      if (verbose_debug) {
	std::ostringstream sout;
	sout << "test." << m << ".dat";
	ofstream tout(sout.str().c_str());
	for (size_t j=0; j<points[m].size(); j++) {
	  for (unsigned k=0; k<ndim; k++) tout << setw(18) << points[m][j][k];
	  tout << endl;
	}
      }
      
      while (++it != points[m].end()) {
	++jt; ++kt;

	bool ok = false;

	// Not a duplicate if at least one dimension differs!
	for (unsigned n=0; n<ndim; n++) {
	  if (fabs((*it)[n] - (*itl)[n]) > 1.0e-10) {
	    ok = true;
	    break;
	  }
	}
	
	if (ok) {
	  *jtl = dup;
	  // Erase duplicates from list
	  if (it>itl+1) points[m].erase(itl+1, it);
	  if (jt>jtl+1) weight[m].erase(jtl+1, jt);
	  if (kt>ktl+1) widths[m].erase(ktl+1, kt);
	  it   = ++itl;		// Advance to next point
	  jt   = ++jtl;		// Advance to next point
	  kt   = ++ktl;		// Advance to next point
	  dup  = *jt;
	} else {
	  dup += *jt;
	}
      }
      
      *jtl = dup;
      if (points[m].end()>itl+1) points[m].erase(itl+1, points[m].end());
      if (weight[m].end()>jtl+1) weight[m].erase(jtl+1, weight[m].end());
      if (widths[m].end()>ktl+1) widths[m].erase(ktl+1, widths[m].end());
      
      //--------------------------------------------------
      // Sanity check
      //--------------------------------------------------

      if (points[m].size() != weight[m].size() ||
	  points[m].size() != widths[m].size()   ) 
	{
	  size_t psize = points[m].size();
	  size_t wsize = weight[m].size();
	  size_t lsize = widths[m].size();

	  std::ostringstream ostr;
	  ostr << "EnsembleDisc: duplicate removal error"
	       << " psize=" << psize 
	       << " wsize=" << wsize 
	       << " lsize=" << lsize;
	
	  throw InternalError(ostr.str(), __FILE__, __LINE__);
      }

      //--------------------------------------------------
      // Check again for duplicates (debugging)
      //--------------------------------------------------
      
      if (verbose_out) {
	it  = points[m].begin();
	itl = points[m].begin();
	unsigned cnt1 = 0;
	
	while (++it != points[m].end()) {

	  bool ok = false;

	  // Not a duplicate if at least one dimension differs!
	  for (unsigned n=0; n<ndim; n++) {
	    if (fabs((*it)[n] - (*itl)[n]) > 1.0e-10) {
	      ok = true;
	      break;
	    }
	  }
	  
	  if (!ok) cnt1++;
	  ++itl;
	}
	
	if (cnt1) {
	  cout << "EnsembleDisc::ComputeDensity debug info: "
	       << " still have " << cnt1 << " duplicates after removal" << endl;
	} else {
	  cout << "EnsembleDisc::ComputeDensity debug info: "
	       << " NO duplicates after removal (good!)" << endl;
	}
      }

      int L = points[m].size();
      
      if (L < N && verbose_out) {
	cout << "EnsembleDisc::ComputeDensity debug info: "
	     << "subspace " << m << ": " << N - L << " duplicates" << endl;
      }
    }

    // Reduce list size
    if (targetSize>0 && static_cast<size_t>(targetSize) < points[m].size()) {
      
      vector< vector<double> > Points = points[m];
      vector< vector<double> > Widths = widths[m];
      vector<double> Weight = weight[m];

      points[m].erase(points[m].begin(), points[m].end());
      widths[m].erase(widths[m].begin(), widths[m].end());
      weight[m].erase(weight[m].begin(), weight[m].end());

      double rate = static_cast<double>(targetSize)/Points.size();
    
      for (size_t i=0; i<Points.size(); i++) {
	if (rate*i > points[m].size()) {
	  points[m].push_back(Points[i]);
	  widths[m].push_back(Widths[i]);
	  weight[m].push_back(Weight[i]);
	}
      }
    }

    MetricTree::verbose_debug = verbose_debug;

    if (fscale.size() ) {

      std::vector<double> cs;

      switch (_si->ptype) {
      case StateInfo::None:
      case StateInfo::Block:
      case StateInfo::RJTwo:
	cs = fscale;
	break;

      case StateInfo::Mixture:
	for (int k=0; k<m; k++) cs.push_back(fscale[0]);
	for (int k=0; k<m; k++) {
	  for (unsigned j=0; j<_si->Ndim; j++) 
	    cs.push_back(fscale[1+j]);
	}

      case StateInfo::Extended:
	for (unsigned j=0; j<_si->Next; j++) 
	  cs.push_back(fscale[1+_si->Ndim+j]);
      }

      kde[m] = MetricDensPtr
	(new MetricTreeDensity(&points[m], &widths[m], &weight[m],
			       cs, kernel, bucketSize));
    } else if (mscale) {
      kde[m] = MetricDensPtr
	(new MetricTreeDensity(&points[m], &widths[m], &weight[m],
			       vvar[m], kernel, bucketSize));
    } else {
      kde[m] = MetricDensPtr
	(new MetricTreeDensity(&points[m], &widths[m], &weight[m],
			       kernel, bucketSize));
    }

    // DEBUGGING
    if (verbose_out) {
      std::ostringstream sout;
      sout << nametag << ".ballTree." << m << '.' << dbg_ctr;
      kde[m]->treeAnalysis(sout.str());

      sout.str("");
      sout << nametag << ".ballTree.points." << m << '.' << dbg_ctr;
      {
	std::ofstream pout(sout.str().c_str());

	for (auto V : points[m]) {
	  for (auto v : V)
	    pout << std::setw(26) << std::setprecision(18) << v;
	  pout << std::endl;
	}
      }

      sout.str("");
      sout << nametag << ".ballTree.widths." << m << '.' << dbg_ctr;
      {
	std::ofstream pout(sout.str().c_str());

	for (auto V : widths[m]) {
	  for (auto v : V)  pout << std::setw(16) << v;
	  pout << std::endl;
	}
      }

      sout.str("");
      sout << nametag << ".ballTree.var." << m << '.' << dbg_ctr;
      {
	std::ofstream pout(sout.str().c_str());

	for (auto v : vvar[m])
	  pout << std::setw(26) << std::setprecision(18)  << v << std::endl;
      }
    }
    // END DEBUGGING

    vector< vector<MetricTree::indx> > indices;
    vector< vector<double> > dists;
    
    kde[m]->NearestNeighbors(indices, dists, points[m], nnear);

    // Make new widths
    for (size_t i=0; i<points[m].size(); i++) {
      for (size_t j=0; j<points[m][i].size(); j++) {
	double extm = minwidth;
	size_t mnear = min<size_t>(nnear, indices[i].size());
	for (size_t k=0; k<mnear; k++) {
	  extm = max<double>
	    (extm, fabs(points[m][i][j] - points[m][indices[i][k]][j]));
	}
	if (indices[i].size()>0) widths[m][i][j] = bbfac*extm;
      }
    }
    
    kde[m]->loadData(&widths[m], &weight[m]);

    // DEBUGGING
    if (verbose_out) {
      std::ostringstream sout;

      sout << nametag << ".ballTree.new_widths." << m << '.' << dbg_ctr;
      {
	std::ofstream pout(sout.str().c_str());

	for (auto V : widths[m]) {
	  for (auto v : V)  pout << std::setw(16) << v;
	  pout << std::endl;
	}
      }

      dbg_ctr++;
    }
    // END DEBUGGING
  }

  kde_computed = true;
}

void EnsembleDisc::ComputeDensityM(void)
{
  if (kdeM_computed) return;

  kernel = KernelPtr(new EpanetchnikovKernel);

  map<int, vector< vector< vector<double> > > >::iterator it;

  // DEBUG
  for (it=pointsM.begin(); it != pointsM.end(); it++) {
    int m = it->first;
    cout << "Subspace " << m << endl;
    for (int mm=0; mm<m; mm++) {
      cout << "  comp=" << mm 
	   << "  size=" << pointsM[m][mm].size()
	   << "  dimension=" << pointsM[m][mm][0].size() << endl;
    }
  } // END DEBUG

  for (it=pointsM.begin(); it != pointsM.end(); it++) {
    int m = it->first;
    kdeM[m] = vector<MetricDensPtr >(m);

    if (fscale.size() ) {

      std::vector<double> cs;

      switch (_si->ptype) {
      case StateInfo::None:
      case StateInfo::Block:
      case StateInfo::RJTwo:
	cs = fscale;
	break;
	
      case StateInfo::Mixture:
	for (int k=0; k<m; k++) cs.push_back(fscale[0]);
	for (int k=0; k<m; m++) {
	  for (unsigned j=0; j<_si->Ndim; j++) 
	    cs.push_back(fscale[1+j]);
	}
	
      case StateInfo::Extended:
	for (unsigned j=0; j<_si->Next; j++) 
	  cs.push_back(fscale[1+_si->Ndim+j]);
      }

      kde[m] = MetricDensPtr
	(new MetricTreeDensity(&points[m], &widths[m], &weight[m],
			       cs, kernel, bucketSize));
    } else {
      
      if (mscale) {
	kde[m] = MetricDensPtr
	  (new MetricTreeDensity(&points[m], &widths[m], &weight[m],
				 vvar[m], kernel, bucketSize));
      } else {
	kde[m] = MetricDensPtr
	  (new MetricTreeDensity(&points[m], &widths[m], &weight[m],
				 kernel, bucketSize));
      }

      for (int mm=0; mm<m; mm++) {

	if (fscale.size()) {
	  
	  std::vector<double> cs;
	  for (unsigned j=0; j<_si->Ndim+_si->Next; j++) 
	    cs.push_back(fscale[1+j]);

	  kdeM[m][mm] = MetricDensPtr
	    (new MetricTreeDensity(&pointsM[m][mm], &widthsM[m][mm], 
				   &weightM[m][mm], cs,
				   kernel, bucketSize));
	  
	} else if (mscale) {
	  kdeM[m][mm] = MetricDensPtr
	    (new MetricTreeDensity(&pointsM[m][mm], &widthsM[m][mm], 
				   &weightM[m][mm], mvar[m][mm],
				   kernel, bucketSize));
	} else {
	  kdeM[m][mm] = MetricDensPtr
	  (new MetricTreeDensity(&pointsM[m][mm], &widthsM[m][mm], 
				 &weightM[m][mm], kernel, bucketSize));
	}
      
	vector< vector<MetricTree::indx> > indices;
	vector< vector<double> > dists;
	
	kdeM[m][mm]->NearestNeighbors(indices, dists, pointsM[m][mm], nnear);
	
	// Make new widths
	for (size_t i=0; i<pointsM[m][mm].size(); i++) {
	  for (size_t j=0; j<pointsM[m][mm][i].size(); j++) {
	    double extm = minwidth;
	    for (size_t k=0; k<indices[i].size(); k++) {
	      extm = max<double>
		(extm, fabs(pointsM[m][mm][i][j] - 
			    pointsM[m][mm][indices[i][k]][j]));
	    }
	    if (indices[i].size()>0) widthsM[m][mm][i][j] = bbfac*extm;
	  }
	}
	
	kdeM[m][mm]->loadData(&widthsM[m][mm], &weightM[m][mm]);
      }
    }
  }

  kdeM_computed  = true;
}

void EnsembleDisc::ComputeDistribution(void)
{
  if (!dimensions_set) {
    string msg = "Must call EnsembleDisc::SetDimensions before "
      "computing the distribution";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (dist_computed) return;

  if (simulationInProgress == 0) Broadcast();
				// Non-mixture defaults
  vector< vector<unsigned> > MAP(2);
  bool mixture = false, rjtwo = false;

  if (_si->ptype == StateInfo::RJTwo) {
    for (unsigned i=1; i<=_si->T; i++) {
      MAP[0].push_back(i);
      MAP[1].push_back(i);
    }
    for (unsigned i=_si->T+1; i<=_si->T+_si->N1; i++) 
      MAP[0].push_back(i);

    for (unsigned i=_si->T+_si->N1+1; i<=_si->T+_si->N1+_si->N2; i++) 
      MAP[1].push_back(i);

    rjtwo = true;
  }

				// Mixture settings
  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) 
    {
      mixture = true;
    }

				// Clean up working arrays
  cnts   .erase(cnts  .begin(), cnts  .end());
  ccum   .erase(ccum  .begin(), ccum  .end());
  mean   .erase(mean  .begin(), mean  .end());
  covar  .erase(covar .begin(), covar .end());

  vmean  .erase(vmean .begin(), vmean .end());
  vvar   .erase(vvar  .begin(), vvar  .end());

  mmean  .erase(mmean .begin(), mmean .end());
  mvar   .erase(mvar  .begin(), mvar  .end());
  covarM .erase(covarM.begin(), covarM.end());

  peak   .erase(peak .begin(),  peak .end());

  vlower = vector<double>(_si->Ntot,  DBL_MAX);
  vupper = vector<double>(_si->Ntot, -DBL_MAX);

  map<int, double> ppeak;

  ofstream out;

  if (verbose) {
    std::ostringstream sout;
    sout << "EnsembleDisc.log." << nametag << "." << _id;
    out.open(sout.str().c_str());
    out.precision(6);
    out << "ComputeDistribution: initialized, count=" << count << std::endl
	<< "ComputeDistribution: BurnIn=" << burnIn << " offset=" << offset << " ibeg=" << ibeg
	<< std::endl;
  }
				// Putting check here makes sure that
				// "clean" mean & stdev is returned to
				// any diagnostic routines
  if (count<2) return;

  ibeg = max<int>(0, burnIn - offset);

  if (ibeg >= count) {
    if (myid==0)
      std::cout << "EnsembleDisc: too few states in remaining chain. "
		<< "Discarding the first half (" << count/2
		<< ") and continuing" << std::endl;
    ibeg = count/2;
  }

  if (burnIn - offset > count) {
    std::ostringstream msg;
    msg << "Logic error: requested burnIn=" << burnIn 
	<< " is larger than the number of recorded states=" << offset+count;
    throw InternalError(msg.str(), __FILE__, __LINE__);
  }

				// Set maximum number of states for KDE
				// (if set)
  if (nMax) {
    ibeg = max<int>(ibeg, count - nMax);
  }

  // Debug
  if (myid==0) {
    cout << "EnsembleDisc: using " << count-ibeg << " states in ensemble"
	 << ", ibeg="  << ibeg << ", count=" << count << endl;
  }
  // End Debug

  for (int i=ibeg; i<count; i++) {
    unsigned int m = 1;
    unsigned int n = states[i].p.N();
    if (rjtwo)   m = states[i].p.M();
    if (mixture) m = states[i].p.M();

    // Accumulate subspace counts
    addCount(m);

    // Compute min and max
    for (unsigned k=0; k<_si->Ntot; k++) {
      vlower[k] = min<double>(vlower[k], states[i].p[k]);
      vupper[k] = max<double>(vupper[k], states[i].p[k]);
    }

    // Compute peak
    if (ppeak.find(m) == ppeak.end()) {
      ppeak[m] = -DBL_MAX;
    }
    if (ppeak[m] < states[i].prob[0]) {
      ppeak[m] = states[i].prob[0];
      peak [m] = states[i].p.vec();
    }

    // Mean and covariance are done _universally_, independent of the
    // State vector packing

    if (rjtwo) {
      // Pass #1
      for (unsigned k1=0; k1<MAP[m-1].size(); k1++)
	mean[m][1 + k1] += states[i].p[MAP[m-1][k1]];
    } else {
      // Pass #1
      for (unsigned k1=0; k1<n; k1++)
	mean[m][1 + k1] += states[i].p[k1];
    }

    vector<double> pt, wd;

    if (mixture) {
      // Skip the last mixture weight since
      // it is a dependent parameter
      for (unsigned k1=0; k1<m-1; k1++) {
	pt.push_back(states[i].p[k1]);
	wd.push_back(0.1);
      }
      for (unsigned k1=m; k1<n; k1++) {
	pt.push_back(states[i].p[k1]);
	wd.push_back(0.1);
      }

    } else {

      if (rjtwo) {
	unsigned n = MAP[m-1].size();
	for (unsigned k1=0; k1<n; k1++) {
	  pt.push_back(states[i].p[MAP[m-1][k1]]);
	  wd.push_back(0.1);
	}

      } else {
	// Use all of the vector elements for non mixtures
	for (unsigned k1=0; k1<n; k1++) {
	  pt.push_back(states[i].p[k1]);
	  wd.push_back(0.1);
	}
      }
    }

    points[m].push_back(pt);
    widths[m].push_back(wd);
    weight[m].push_back(1);
  }


  for (auto v : cnts) {
    unsigned m = v.first;
    int      N = v.second;
    if (N>0) mean[m] /= N;
  }

  for (int i=ibeg; i<count; i++) {
    unsigned int m = 1;
    unsigned int n = states[i].p.N();
    if (rjtwo)   m = states[i].p.M();
    if (mixture) m = states[i].p.M();

    if (rjtwo) {
      // Pass #2
      for (unsigned k1=0; k1<MAP[m-1].size(); k1++) {
	double dif1 = states[i].p[MAP[m-1][k1]] - mean[m][1 + k1];
	for (unsigned k2=0; k2<MAP[m-1].size(); k2++) {
	  double dif2 = states[i].p[MAP[m-1][k2]] - mean[m][1 + k2];
	  covar[m][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
      
    } else {
      // Pass #2
      for (unsigned k1=0; k1<n; k1++) {
	double dif1 = states[i].p[k1] - mean[m][1 + k1];
	for (unsigned k2=0; k2<n; k2++) {
	  double dif2 = states[i].p[k2] - mean[m][1 + k2];
	  covar[m][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
    }
  }

  for (auto v : cnts) {
    unsigned m = v.first;
    int      N = v.second;
    if (N>1) 
      covar[m] /= N - 1;
    else
      covar[m].zero();
  }

  if (verbose)
    out << "ComputeDistribution: initialized, moments computed" << endl;

  sortPoints(points, widths, weight);

  if (verbose) {
    out << "ComputeDistribution: points sorted" << endl;
    for (map<int, vector< vector<double> > >::iterator 
	   it = points.begin(); it != points.end(); it++)
      
      out << "#" << setw(3) << it->first << ": number=" 
	  << it->second.size() << endl;
  }

  if (verbose)
    out << "ComputeDistribution: covariance matrix done" << endl;

				// Cache return vectors for mean and variance
  for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {

    int m = it->first;
    int N = it->second;
    int n = _si->Ntot;
    
    if (_si->ptype == StateInfo::RJTwo) n = MAP[m-1].size();

    if (_si->ptype == StateInfo::Mixture ||
        _si->ptype == StateInfo::Extended   ) n = (_si->Ndim+1)*m;
    
    if (verbose) {
      out << "ComputeDistribution: m=" << m 
	  << " counts=" << N << endl;
      if (cnts[m]) {
	out << "ComputeDistribution: about to erase at m=" << m 
	    << ", ndim=" << vmean[m].size() << ", "
	    << vvar[m].size() << endl;
	
	out << "Mean:" << endl;
	for (int k1=1; k1<=n; k1++)
	  out << setw(16) << mean[m][k1] << endl;
	out << endl;

	out << "Covariance:" << endl;
	for (int k1=1; k1<=n; k1++) {
	  for (int k2=1; k2<=n; k2++) 
	    out << setw(16) << covar[m][k1][k2];
	  out << endl;
	}
      }
      else
	out << "ComputeDistribution: skipping m=" << m << endl;
    }

    vmean[m].erase(vmean[m].begin(), vmean[m].end());
    vvar [m].erase(vvar [m].begin(), vvar [m].end());

    if (verbose)
      out << "ComputeDistribution: mean & cov, m=" << m << endl;

    for (int i=0; i<n; i++) {
      vmean[m].push_back(mean [m][1+i]);
      vvar [m].push_back(covar[m][1+i][1+i]);
    }

    if (verbose) {
      out << "Mean and Var:" << endl;
      for (int i=0; i<n; i++)
	out << setw(5) << i+1 << ": "
	    << setw(16) << vmean[m][i]
	    << setw(16) << vvar [m][i]
	    << endl;
    }
  }

  if (verbose)
    out << "ComputeDistribution: mean and var done" << endl;

  makeSampleFraction();

  dist_computed = true;

  cache = CachePtr(new StateCache(ibeg, states, key_pos));

  if (verbose) {
    out << "Counts for sampling: " << ibeg << ", " << count-1 << endl;
    out << "Limits: MinC=" << cnts.begin()->first 
	<< "  MaxC=" << cnts.rbegin()->first << endl;
    out << "Cumulative states:" << endl;
    
    for (mapIDiter it=ccum.begin(); it!=ccum.end(); it++)
      out << setw(5) << it->first << ": " << it->second << endl;
  }

  
  ComputeDistributionMarginal(out);

  if (verbose)
    out << "ComputeDistribution: everything done" << endl;
}


void EnsembleDisc::ComputeDistributionMarginal(ostream& out)
{
  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block || 
      _si->ptype == StateInfo::RJTwo ) return;

				// Clean up working arrays
  for (auto v : cnts) {
    int m = v.first;
    mcnt[m] = 0;
    mcum[m] = 0;
    for (int mm=0; mm<m; mm++) {
      meanM [m][mm].zero();
      covarM[m][mm].zero();
    }
  }

  vector<double> pt(_si->Ndim), wd(_si->Ndim, 0.1);


  for (int i=ibeg; i<count; i++) {
    
    unsigned m = states[i].p.M();

				// Make a stanza for this mixture?
    if (pointsM.find(m) == pointsM.end()) {
      pointsM[m] = vector< vector< vector<double> > > (m);
      widthsM[m] = vector< vector< vector<double> > > (m);
      weightM[m] = vector< vector<double> > (m);
    }
      
    mcnt[m]++;

    for (unsigned mm=0; mm<m; mm++) {

      pt = states[i].p.Phi(mm);
	
      for (unsigned k1=0; k1<_si->Ndim; k1++)
	meanM[m][mm][1 + k1] += pt[k1];
      
      pointsM[m][mm].push_back(pt);
      widthsM[m][mm].push_back(wd);
      weightM[m][mm].push_back(1);
    }
  }

  for (auto v : mcnt) {
    unsigned m = v.first;
    int N      = v.second;
    if (N>0) {
      for (unsigned mm=0; mm<m; mm++) {
	meanM[m][mm] /= N;
      }
    }
  }

  for (int i=ibeg; i<count; i++) {
    unsigned m = states[i].p.M();
    for (unsigned mm=0; mm<m; mm++) {
      pt = states[i].p.Phi(mm);
      for (unsigned k1=0; k1<_si->Ndim; k1++) {
	double dif1 = pt[k1] - meanM[m][mm][1 + k1];
	for (unsigned k2=0; k2<_si->Ndim; k2++) {
	  double dif2 =  pt[k2] - meanM[m][mm][1 + k2];
	  covarM[m][mm][1 + k1][1 + k2] += dif1 * dif2;
	}
      }
    }
  }

  for (auto v : mcnt) {
    unsigned m = v.first;
    int N      = v.second;
    for (unsigned mm=0; mm<m; mm++) {
      if (N>1) {
	covarM[m][mm] /= N - 1;
      } else {
	covarM[m][mm].zero();
      }
    }
  }


  if (verbose)
    out << "ComputeDistribution: initialized, marginal moments computed" 
	<< endl;
  
  sortPointsM(pointsM, widthsM, weightM);

  if (verbose) {
    out << "ComputeDistributionMarginal: points sorted" << endl;
    for (map<int, vector< vector< vector<double> > > >::iterator 
	   it = pointsM.begin(); it != pointsM.end(); it++)
      
      out << "#" << setw(3) << it->first << ": number=" 
	  << it->second[0].size() << endl;
  }

  if (verbose)
    out << "ComputeDistributionMarginal: covariance matrix done" << endl;

				// Cache return vectors for mean and variance
  for (auto v : mcnt) {

    unsigned m = v.first;
    int N = v.second;

    if (verbose) {
      out << "ComputeDistribution: m=" << m 
	  << " counts=" << N << endl;
      if (mcnt[m]) {
	out << "ComputeDistribution: about to erase at m=" << m 
	    << ", ndim=" << mmean[m].size() << ", "
	    << mvar[m][0].size() << endl;
	
	out << "Marginal covariance:" << endl;
	for (unsigned mm=0; mm<m; mm++) {
	  out << "===> Component " << mm+1 << endl;
	  for (unsigned k1=1; k1<=_si->Ndim; k1++) {
	    for (unsigned k2=1; k2<=_si->Ndim; k2++) 
	      out << setw(16) << covarM[m][mm][k1][k2];
	    out << endl;
	  }
	}
      }
      else
	out << "ComputeDistribution: skipping m=" << m << endl;
    }

    if (!mcnt[m]) continue;

    for (unsigned mm=0; mm<m; mm++) {
      mmean[m][mm].erase(mmean[m][mm].begin(), mmean[m][mm].end());
      mvar [m][mm].erase( mvar[m][mm].begin(), mvar[m][mm].end());
    }

    if (verbose)
      out << "ComputeDistribution: marginal mean & cov, m=" << m << endl;
    
    for (unsigned mm=0; mm<m; mm++) {
      for (unsigned i=0; i<_si->Ndim; i++) {
	mmean[m][mm].push_back(meanM [m][mm][1+i]);
	mvar [m][mm].push_back(covarM[m][mm][1+i][1+i]);
      }
    }

    
    if (verbose) {
      out << "Mean and Var:" << endl;
      for (unsigned mm=0; mm<m; mm++) {
	out << "===> Component " << mm+1 << endl;
	for (unsigned i=0; i<_si->Ndim; i++)
	  out << setw(5) << i+1 << ": "
	      << setw(16) << mmean[m][mm][i]
	      << setw(16) << mvar [m][mm][i]
	      << endl;
      }
    }
  }
  

  if (verbose)
    out << "ComputeDistribution: mean and var done" << endl;

				// Throw out subspaces that do not meet our
				// minimum occupation requirement
  list<int> er;
  for (auto v : mcnt) {
    if (v.second < minsub) er.push_back(v.first);
  }
  for (list<int>::iterator it=er.begin(); it!=er.end(); it++) {
    mcnt.erase(*it);
    mcum.erase(*it);
  }

				// Cumulative distribution for 
				// component states
  mapIIiter cur=mcnt.begin(), lst=mcnt.begin();
  for (; cur!=mcnt.end(); cur++) {
    mcum[cur->first] = cur->second;
    if (cur!=lst) mcum[cur->first] += lst->second;
  }
  double norm = mcnt.rbegin()->second;
  for (cur=mcnt.begin(); cur!=mcnt.end(); cur++) cur->second /= norm;
  mcnt.rbegin()->second = 1.0;

  if (verbose) {
    out << "Cumulative states:" << endl;
    for (mapIDiter j=mcum.begin(); j!=mcum.end(); j++)
      out << setw(5) << j->first << ": " << j->second << endl;
  }
  
}


//
// Use the convariance analysis to compute the posterior probability
// based on the simulation data
//
double EnsembleDisc::PDF(State& p)
{
  double ret;

  if (!dist_computed) ComputeDistribution();
  if (!kde_computed)  ComputeDensity();

				// Do the NON-MIXTURE case
  if (p.Type() == StateInfo::None || p.Type() == StateInfo::Block) {

    vector<double> pt, val;
    for (unsigned int i=0; i<p.size(); i++) pt.push_back(p[i]);
    
    vector< vector<double> > pts;
    pts.push_back(pt);

    kde[1]->getDensity(pts, val, errortol);
    
    double prob = *val.begin();

    if (prob<=0.0)
      ret = 0.0;
    else
      ret = prob;
    
  } else if (p.Type() == StateInfo::RJTwo) {

    vector<double> pt, val;
    unsigned offset = 0;
    for (unsigned int i=0; i<_si->T; i++) 
      pt.push_back(p[offset+i]);
    if (p.M()==1) {
      offset = _si->T;
      for (unsigned int i=0; i<_si->N1; i++) pt.push_back(p[offset+i]);
    } else {
      offset = _si->T + _si->N1;
      for (unsigned int i=0; i<_si->N2; i++)
	pt.push_back(p[i]);
    }

    vector< vector<double> > pts;
    pts.push_back(pt);

    kde[p.M()]->getDensity(pts, val, errortol);
    
    double prob = *val.begin();

    if (prob<=0.0)
      ret = 0.0;
    else
      ret = prob;

  } else {			// Do the MIXTURE case

    vector<double> pt, val;
    for (unsigned int i=0; i<p.M()-1  ; i++) pt.push_back(p[i]);
    for (unsigned int i=p.M(); i<p.N(); i++) pt.push_back(p[i]);
    
    vector< vector<double> > pts;
    pts.push_back(pt);
    
    kde[p.M()]->getDensity(pts, val, errortol);
    
    double prob1 = *val.begin();
    double prob2 = PDFSubspace(p.M());
    
    if (prob1<=0.0 || prob2<=0.0)
      ret = 0.0;
    else 
      ret = prob1 * prob2;
  }

  return ret;
}

double EnsembleDisc::logPDF(State& p)
{
  double ret;

  if (!dist_computed) ComputeDistribution();
  if (!kde_computed)  ComputeDensity();

				// Do the NON-MIXTURE case
  if (p.Type() == StateInfo::None || p.Type() == StateInfo::Block) {
    vector<double> pt, val;

    for (unsigned int i=0; i<p.N(); i++) pt.push_back(p[i]);
    
    vector< vector<double> > pts;
    pts.push_back(pt);

    kde[1]->getDensity(pts, val, errortol);
    
    double prob = *val.begin();

    if (prob<=0.0)
      ret = -INFINITY;
    else
      ret = log(prob);
    
  } else if (p.Type() == StateInfo::RJTwo) {
    vector<double> pt, val;

    unsigned offset = 1;
    for (unsigned int i=0; i<_si->T; i++) 
      pt.push_back(p[offset+i]);
    if (p.M()==1) {
      offset = _si->T;
      for (unsigned int i=0; i<_si->N1; i++) pt.push_back(p[offset+i]);
    } else {
      offset = _si->T + _si->N1;
      for (unsigned int i=0; i<_si->N2; i++)
	pt.push_back(p[i]);
    }

    vector< vector<double> > pts;
    pts.push_back(pt);

    kde[p.M()]->getDensity(pts, val, errortol);
    
    double prob = *val.begin();

    if (prob<=0.0)
      ret = -INFINITY;
    else
      ret = log(prob);
    
				// Do the MIXTURE case
  } else {

    vector<double> pt, val;
    for (unsigned int i=0; i<p.M()-1  ; i++) pt.push_back(p[i]);
    for (unsigned int i=p.M(); i<p.N(); i++) pt.push_back(p[i]);

    vector< vector<double> > pts;
    pts.push_back(pt);

    kde[p.M()]->getDensity(pts, val, errortol);

    double prob1 = *val.begin();
    double prob2 = PDFSubspace(p.M());
    
    if (prob1<=0.0 || prob2<=0.0)
      ret = -INFINITY;
    else
      ret = log(prob1) + log(prob2);
  }

  return ret;
}

double EnsembleDisc::logPDFMarginal(unsigned m, unsigned n, 
				    const vector<double>& p)
{
  if (!dist_computed) ComputeDistribution();
  if (!kdeM_computed) ComputeDensityM();

  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block ||
      _si->ptype == StateInfo::RJTwo) 
    {
      string msg = "You can not call logPDFMarginal if the state is not a mixture!";
      throw InternalError(msg, __FILE__, __LINE__);
    }

  unsigned psz = p.size();

  if (_si->Ndim > psz) {
    throw DimNotMatchException(__FILE__, __LINE__);
  }

  vector<double> pt, val;
  for (unsigned i=0; i<m-1; i++) pt.push_back(p[i]);
  for (unsigned i=m; i<psz; i++) pt.push_back(p[i]);
  
  vector< vector<double> > pts;
  pts.push_back(pt);

  kdeM[m][n]->getDensity(pts, val, errortol);

  double prob = *val.begin();

  if (prob<=0.0) return -INFINITY;

  return log(prob);
}

//
// Generates variates from the same estimated posterior probability
// distribution
//
State EnsembleDisc::Sample(unsigned m)
{
  return cache->Sample(m);
}

vector<double> EnsembleDisc::SampleMarginal(unsigned m, unsigned n)
{
  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block ||
      _si->ptype == StateInfo::RJTwo) 
    {
      string msg = "You can not call SampleMarginal if the state is not a mixture!";
      throw InternalError(msg, __FILE__, __LINE__);
    }

  vector<double> p = cache->Sample(m);
  vector<double> ret;
  for (unsigned k=0; k<_si->Ndim; k++) ret.push_back(p[m+n*_si->Ndim+k]);
  return ret;
}

vector<double> EnsembleDisc::lower(void)
{
  if (!dist_computed) ComputeDistribution();
  return vlower;
}

vector<double> EnsembleDisc::upper(void)
{
  if (!dist_computed) ComputeDistribution();
  return vupper;
}

vector<double>  EnsembleDisc::Mean(unsigned m)
{
  unsigned M = 1;

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended ) {

    if (m>_si->M || m<1) {
      std::ostringstream sout;
      sout << "EnsembleDisc::StdDev: called with m=" << m
	   << " but Mmix=" << _si->M;
      throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
    }
    M = m;
  }

  if (!dist_computed) ComputeDistribution();

  return vmean[M];
}

vector<double>  EnsembleDisc::StdDev(unsigned m)
{
  unsigned n = _si->Ntot;
  unsigned M = 1;
  
  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended ) {

    if (m>_si->M || m<1) {
      std::ostringstream sout;
      sout << "EnsembleDisc::StdDev: called with m=" << m
	   << " but Mmix=" << _si->M;
      throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
    }
    n = (_si->Ndim+1)*m;
    M = m;
  }

  if (!dist_computed) ComputeDistribution();

  vector<double> stddev;
  for (unsigned i=0; i<n; i++) 
    stddev.push_back(sqrt(vvar[M][i]));
    
  return stddev;
}

vector<double> EnsembleDisc:: Moments(unsigned m, unsigned i)
{
  unsigned M = 1;
  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended ) {

    if (m>_si->M || m<1) {
      std::ostringstream sout;
      sout << "EnsembleDisc::StdDev: called with m=" << m
	   << " but Mmix=" << _si->M;
      throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
    }
    M = m;
  }

  ostream cout(checkTable("cli_console"));

  if (!dist_computed) ComputeDistribution();
  vector<double> moms;
  switch (i) {
  case 0:
    for (unsigned i=0; i<m; i++) moms.push_back(1.0);;
    break;
  case 1:
    for (unsigned i=0; i<m; i++) moms.push_back(vmean[M][i]);
    break;
  case 2:
    for (unsigned i=0; i<m; i++) moms.push_back(vvar[M][i] + 
					   vmean[M][i]*vmean[M][i]);
    break;
  default:
    cout << "EnsembleDisc::Moments: moments greater than 2 are not defined\n";
  }

  return moms;
}

void EnsembleDisc::PrintDiag(string& outfile)
{
  std::ofstream out;

  if (myid==0) {
    out.open(outfile.c_str(), ios::out | ios::app);
  }

  PrintDiag(out);
}

void EnsembleDisc::PrintDiag(ostream& out)
{
  ComputeDistribution();

  if (myid==0) {

    out << endl
	<< "My instance=" << this << endl
	<< "Total counts=" << count << endl
	<< "Counts per component" << endl;
    
    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++)
      out << setw(5)  << it->first << ": " 
	  << setw(8)  << it->second
	  << setw(15) << ccum[it->first] 
	  << endl;
    out << endl;
    
    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
      int m = it->first;
      int N = it->second;
      
      if (N<2) continue;
      
      out << endl << endl << "*** " << m << " ***" << endl;
      if (_si->ptype == StateInfo::Mixture || 
	  _si->ptype == StateInfo::Extended   ) out << m;
      else out << "non-mixture";
      out << " [" << (double)N/count << "] ***" << endl;
      
      for (int k=mean[m].getlow(); k<=mean[m].gethigh(); k++) {
	out << setw(5)  << k 
	    << setw(15) << mean[m][k] 
	    << setw(15) << covar[m][k][k]
	    << setw(15) << sqrt(covar[m][k][k])
	    << endl;
      }
      out << endl;
      out << endl;
      for (int i=covar[m].getrlow(); i<=covar[m].getrhigh(); i++) {
	for (int j=covar[m].getclow(); j<=covar[m].getchigh(); j++)
	  out << setw(15) << covar[m][i][j];
	out << endl;
      }
      out << endl;
    }

    out << endl;
    out << endl;
  }

}

void EnsembleDisc::PrintDiag()
{
  PrintDiag(cout);
}

void EnsembleDisc::PrintDensity(int dim1, int dim2, int num1, int num2,
				string file)
{
  // Only root node needs to do this
  if (myid) return;

  // Sanity check
  if (dim1 == dim2) {
    cout << "This routine provides a density cut through the plane specifed by dim1  " << std::endl
	 << "and dim2.  Therefore, dim1 must NOT EQUAL dim2.  Please try again . . . " << std::endl;
    return;
  }

  if (_si->ptype == StateInfo::RJTwo) {
    cout << "PrintDensity is not implemented for an RJTwo state" << endl;
    return;
  }

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {

    if (static_cast<unsigned>(dim1) >= _si->M*_si->Ndim || 
	static_cast<unsigned>(dim2) >= _si->M*_si->Ndim) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ndim=" << _si->Ndim << endl;
      return;
    }
  } else {

    if (static_cast<unsigned>(dim1) >= _si->Ntot || 
	static_cast<unsigned>(dim2) >= _si->Ntot) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ntot=" << _si->Ntot << endl;
      return;
    }
  }
    
  ofstream out(file.c_str());
  if (!out) {
    cout << "PrintDensity: could not open file <" << file << "> for output"
	 << endl;
    return;
  }

  ComputeDistribution();
  
  State p(_si);
  
  unsigned M = 1, prefix = 0;

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {
    
    double dmax = 0.0;

    for (unsigned m=0; m<fraction.size(); m++) {
      if (fraction[m].first > dmax) {
	dmax = fraction[m].first;
	M    = fraction[m].second;
      }
    }

    if (dmax <= 0.0) {
      cout << "PrintDensity: could not find any states, strange" << endl;
      return;
    }

    unsigned nd = _si->Ndim;

    vector<double> wght(M);
    vector< vector<double> > phi(M);

    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
      int m   = it->first;
      wght[m] = peak[M][m];
      phi[m]  = vector<double>(nd);
      for (unsigned n=0; n<nd; n++) phi[m][n] = peak[M][M+nd*m+n];
    }
    if (_si->ptype == StateInfo::Mixture) {
      p.setState(M, wght, phi);
    } else {
      vector<double> ext(_si->Next);
      for (unsigned n=0; n<_si->Next; n++) peak[M][M*(1+nd)+n];
      p.setState(M, wght, phi, ext);
    }

    prefix = M;

  } else {
    p.setState(peak[M]);
  }

  double xmin = lower()[prefix + dim1];
  double xmax = upper()[prefix + dim1];

  double ymin = lower()[prefix + dim2];
  double ymax = upper()[prefix + dim2];

  double dx   = (xmax - xmin)/(num1-1);
  double dy   = (ymax - ymin)/(num2-1);

  out << "# [" << xmin << ", " << ymin 
      << "] ==> [" << xmax << ", " << ymax << "]" << std::endl;

  for (int i=0; i<num1; i++) {

    p.vec(prefix + dim1) = xmin + dx*i;
    
    for (int j=0; j<num2; j++) {
    
      p.vec(prefix + dim2) = ymin + dy*j;

      out << setw(18) << xmin + dx*i
	  << setw(18) << ymin + dy*j
	  << setw(18) << PDF(p)
	  << endl;
    }
    out << endl;
  }

}

void EnsembleDisc::PrintDensity(int dim1, int dim2, int num1, int num2,
				clivectord* deflt, string file)
{
  // Only root node needs to do this
  if (myid) return;

  // Sanity check
  if (dim1 == dim2) {
    cout << "This routine provides a density cut through the plane specifed by dim1  " << std::endl
	 << "and dim2.  Therefore, dim1 must NOT EQUAL dim2.  Please try again . . . " << std::endl;
    return;
  }

  if (_si->ptype == StateInfo::RJTwo) {
    cout << "PrintDensity is not implemented for an RJTwo state" << endl;
    return;
  }

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {

    if (static_cast<unsigned>(dim1) >= _si->M*_si->Ndim || 
	static_cast<unsigned>(dim2) >= _si->M*_si->Ndim) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ndim=" << _si->Ndim << endl;
      return;
    }

    unsigned defsz = (*deflt)().size();
    if (defsz != _si->M*(_si->Ndim + 1) + _si->Next) {
      cout << "PrintDensity: provided default vector has wrong dimension, "
	   << "found " << defsz << " but expected "
	   << _si->M*(_si->Ndim + 1) + _si->Next << std::endl;
      return;
    }

  } else {

    if (static_cast<unsigned>(dim1) >= _si->Ntot || 
	static_cast<unsigned>(dim2) >= _si->Ntot) {
      cout << "PrintDensity: dim1=" << dim1 << " or dim2=" << dim2
	   << " is greater than Ntot=" << _si->Ntot << endl;
      return;
    }

    unsigned defsz = (*deflt)().size();
    if (defsz != _si->Ntot) {
      cout << "PrintDensity: provided default vector has wrong dimension"
	   << "found " << defsz << " but expected " << _si->Ntot << endl;
      return;
    }
    
  }
    
  ofstream out(file.c_str());
  if (!out) {
    cout << "PrintDensity: could not open file <" << file << "> for output"
	 << endl;
    return;
  }

  ComputeDistribution();
  
  State p(_si);
  
  unsigned M = 1, prefix = 0;

  if (_si->ptype == StateInfo::Mixture || 
      _si->ptype == StateInfo::Extended   ) {
    
    double dmax = 0.0;

    for (unsigned m=0; m<fraction.size(); m++) {
      if (fraction[m].first > dmax) {
	dmax = fraction[m].first;
	M    = fraction[m].second;
      }
    }

    if (dmax <= 0.0) {
      cout << "PrintDensity: could not find any states, strange" << endl;
      return;
    }

    unsigned nd = _si->Ndim;

    vector<double> wght(M);
    vector< vector<double> > phi(M);

    for (mapIIiter it=cnts.begin(); it!=cnts.end(); it++) {
      int m   = it->first;
      wght[m] = (*deflt)()[m];
      phi[m]  = vector<double>(nd);
      for (unsigned n=0; n<nd; n++) phi[m][n] = (*deflt)()[M+nd*m+n];
    }
    if (_si->ptype == StateInfo::Mixture) {
      p.setState(M, wght, phi);
    } else {
      vector<double> ext(_si->Next);
      for (unsigned n=0; n<_si->Next; n++) (*deflt)()[M*(1+nd)+n];
      p.setState(M, wght, phi, ext);
    }

    prefix = M;

  } else {
    p.setState((*deflt)());
  }

  double xmin = lower()[prefix + dim1];
  double xmax = upper()[prefix + dim1];

  double ymin = lower()[prefix + dim2];
  double ymax = upper()[prefix + dim2];

  double dx   = (xmax - xmin)/(num1-1);
  double dy   = (ymax - ymin)/(num2-1);

  out << "# [" << xmin << ", " << ymin 
      << "] ==> [" << xmax << ", " << ymax << "]" << std::endl;

  for (int i=0; i<num1; i++) {

    p.vec(prefix + dim1) = xmin + dx*i;
    
    for (int j=0; j<num2; j++) {
    
      p.vec(prefix + dim2) = ymin + dy*j;

      out << setw(18) << xmin + dx*i
	  << setw(18) << ymin + dy*j
	  << setw(18) << PDF(p)
	  << endl;
    }
    out << endl;
  }

}


void EnsembleDisc::addCount(int m)
{
  mapIIiter it = cnts.find(m);
  if (it != cnts.end()) it->second++;
  else {
    cnts[m] = 1;
    mcnt[m] = 0;
    mcum[m] = 0;

    int n = _si->Ntot;

    if (_si->ptype == StateInfo::RJTwo) {
      if (m==1) n = _si->T + _si->N1;
      else      n = _si->T + _si->N2;
    }
    if (_si->ptype == StateInfo::Mixture || 
	_si->ptype == StateInfo::Extended   ) n = (_si->Ndim+1)*m;

    // Allocate and initialize to zero
    //
    vmean[m] = vector<double>(n, 0.0);
    vvar [m] = vector<double>(n, 0.0);
    //
    mmean[m] = vector< vector<double> >(m);
    mvar [m] = vector< vector<double> >(m);
    for (int mm=0; mm<m; mm++) {
      mmean[m][mm] = vector<double>(n, 0.0);
      mvar [m][mm] = vector<double>(n, 0.0);
    }
    //
    mean [m] = VectorM(1, n);
    mean [m].zero();
    //
    covar[m] = MatrixM(1, n, 1, n);
    covar[m].zero();
    //
    meanM [m] = vector<VectorM>(m);
    covarM[m] = vector<MatrixM>(m);
    for (int mm=0; mm<m; mm++) {
      meanM [m][mm].setsize(1, n);
      meanM [m][mm].zero();
      //
      covarM[m][mm].setsize(1, n, 1, n);
      covarM[m][mm].zero();
    }
  }
}
