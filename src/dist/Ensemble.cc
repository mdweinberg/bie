#include <values.h>
#include <iostream>
#include <iomanip>

using namespace std;

#include <BIEException.h>
#include <BIEMutex.h>
#include <Ensemble.h>
#include <gfunction.h>
#include <BIEmpi.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Ensemble)

				// Set to true for a dump of the input
				// points for debugging.  Set to false
				// for production.
bool Ensemble::verbose_debug   = false;
				// Is verbose output ON by default?
				// Set to false for production.
bool Ensemble::verbose_default = false;

using namespace BIE;

StateData::StateData(const vector<double>& v, const State& pp)
{
  prob = vector<double>(3);
  unsigned sz = min<unsigned>(3, v.size());
  for (unsigned i=0; i<sz; i++) prob[i] = v[i];
  p = pp;
}

StateData::StateData(const StateData &v)
{
  prob = v.prob;
  p    = v.p;
}


void StateData::Broadcast()
{
  if (!mpi_used) return;
  MPI_Bcast(&prob[0], 3, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  p.Broadcast(0);
}



StateCache::StateCache(int nburn, deque<StateData> &list, int keypos)
{
  begin     = nburn;
  key_pos   = 0;
  cur_index = 0;
  states    = &list;
  ssize     = 0;

  for (unsigned k=begin; k<(*states).size(); k++) {
    PairIndex pair;
    pair.index = k;
				// If the state is not a mixture and
				// not a RJ state, everything goes
				// into the m=0 map, and retrieval is
				// consistent
    unsigned m = 1;
    StateInfo *si = (*states)[k].p.SI();
    if (si->ptype == StateInfo::RJTwo)     m = floor((*states)[k].p[0]+1.01);
    if (si->ptype == StateInfo::Mixture || 
	si->ptype == StateInfo::Extended ) m = (*states)[k].p.M();

    pair.value = (*states)[k].p[key_pos];
    pairs[m].push_back(pair);
    if (ssize == 0) ssize = (*states)[k].p.size();
  }

  unsigned dcnt = 0;
  for (map<int, vector<PairIndex> >::iterator 
	 it=pairs.begin(); it!=pairs.end(); it++) {
    disc[it->first] = 
      DiscUnifPtr(new DiscreteUniform(0, it->second.size()-1, BIEgen));
    if (myid==0)
      cout << "Size[" << ++dcnt << "] = " << it->second.size() << endl;
  }
}

  
StateCache::StateCache(const StateCache &p)
{
  begin     = p.begin;
  key_pos   = p.key_pos;

  count     = p.count;
  cur_index = p.cur_index;

  states    = p.states;
  pairs     = p.pairs;
  ssize     = p.ssize;

  for (map<int, vector<PairIndex> >::iterator 
	 it=pairs.begin(); it!=pairs.end(); it++) 
    disc[it->first] = 
      DiscUnifPtr(new DiscreteUniform(0, it->second.size()-1, BIEgen));
}


double StateCache::logPDF(State &p)
{
  PairIndex pair;
  
  unsigned m = p.M();
  pair.value = p[key_pos];
                                // First try the cached index
  if (pair == pairs[m][cur_index]) {
    return (*states)[pairs[m][cur_index].index].prob[0];
  }

  // Search
  vector<PairIndex>::iterator result =
    find(pairs[m].begin(), pairs[m].end(), pair);

                                // Error!
  if (result == pairs[m].end()) {
    string msg = "StateCache: could not find index corresponding to desired state key, permanent error!!";
    throw InternalError(msg, __FILE__, __LINE__);
  }

  cur_index = result->index;

  for (unsigned i=0; i<p.size(); i++) {
    if (fabs(p[i] - (*states)[cur_index].p[i]) > fabs(p[i]*1.0e-12)) {
      ostringstream msg;
      msg << "StateCache: mismatch in parameter vector ID at index="
	  << i;
      throw msg.str();
    }
  }

  return (*states)[result->index].prob[0];
}


State StateCache::Sample(unsigned m)
{
				// Sanity check
  if (pairs.find(m) == pairs.end()) {
    cerr << "StateCache: Error!!  You wanted non-existent subspace m=" 
	 << m << endl;
  }
				// Choose state at random
  cur_index = pairs[m][(*disc[m])()].index;

				// Return a copy of the state
  return (*states)[cur_index].p;
}



int    Ensemble::keep       = 	400000;
double Ensemble::thresh     =	1.0e-12;
bool   Ensemble::continuous =	true;
int    Ensemble::instance   =	0;
int    Ensemble::key_pos    =	0;
				// Minimum number of states per subspace
int    Ensemble::minsub   = 3;


/// Set multiple component range to continuous
void Ensemble::setContinuous() 
{ 
  continuous = true; 
}

/// Set multiple component range to maximum range (allow zero freq. states)
void Ensemble::setMaxRange()
{ 
  continuous = false; 
}


Ensemble::Ensemble()
{
  dimensions_set = false;
  verbose        = verbose_default;
  count          = 0;
  offset         = 0;
  burnIn         = 0;
  _id            = instance++;
  clev           = current_level;
  enable_dump    = false;
  binary_dump    = false;

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
}


Ensemble::Ensemble(StateInfo *si,
		   int level, int begin, string filename, int keypos)
{
  _si            = si;
  verbose        = verbose_default;
  _id            = instance++;
  clev           = current_level;
  enable_dump    = false;
  binary_dump    = false;


  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));

  Reset(_si, level, begin, filename, keypos);
}

Ensemble::Ensemble(StateInfo *si)
{
  _si            = si;
  verbose        = verbose_default;
  _id            = instance++;
  clev           = current_level;
  enable_dump    = false;
  binary_dump    = false;

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));

  Reset(_si);
}

Ensemble::Ensemble(const Ensemble &p)
{
  _si            = p._si;
  
  count          = p.count;
  ibeg           = p.ibeg;
  offset         = p.offset;
  burnIn         = p.burnIn;
  verbose        = p.verbose;

  ccum           = p.ccum;

  states         = p.states;

  dimensions_set = p.dimensions_set;

  clev           = p.clev;

  _id            = instance++;

  enable_dump    = p.enable_dump;
  binary_dump    = p.binary_dump;

}


void Ensemble::Reset(StateInfo *si,
		     int level, int begin, string filename, int keypos)
{
  count          = 0;
  offset         = 0;
  burnIn         = begin;

  setDimensions(si);

  // Read input file

  ifstream in(filename.c_str());
  if (!in) {
    ostringstream msg;
    msg << "EnsembleStat: could not read input filename containing states <"
	<< filename << ">, permanent error!!";
    throw InternalError(msg.str(), __FILE__, __LINE__);
  }

  const int max_dataline = 2048;
  char dataline[max_dataline];

  in.getline(dataline, max_dataline);
  
  // Make token list

  string line(dataline);
  size_t first=0, second;
  vector<string> tokens;
  int ntokens = 0;

  while (1) {
    first = line.find("\"", first);
    if (first == string::npos) break;
    second = line.find("\"", first+1);
    if (second == string::npos) break;
    tokens.push_back(line.substr(first+1, second-first-1));
    ntokens++;

    first = second+1;
  }

#if 1
  cout << endl << "# tokens=" << ntokens << endl << endl;
  for (int i=0; i<ntokens; i++) 
    cout << setw(4) << i << setw(20) << tokens[i] << endl;
#endif

  if (tokens[5] == "Number" && 
      !(_si->ptype == StateInfo::Mixture   || 
	_si->ptype == StateInfo::Extended   )  )
    {
      string msg = "StateCache: could not find <Number> in Positon #6";
      throw InternalError(msg, __FILE__, __LINE__);
    }

  // Load the state cache
  in.getline(dataline, max_dataline);

  int lev, itr, m = 1;
  double value;

  do {
    StateData data(_si);

    istringstream ins(dataline);
    ins >> lev;
    if (lev != level) continue;

    ins >> itr;
    ins >> data.prob[0];
    ins >> data.prob[1];
    ins >> data.prob[2];

    if (_si->ptype == StateInfo::Mixture   || 
	_si->ptype == StateInfo::Extended   ) ins >> m;
    
    vector<double> dat;
    ins >> value;  
    while (ins) {
      dat.push_back(value);
      ins >> value;  
    }

    data.p.setState(m, dat);
    
    AccumData(data);

  } while (in.getline(dataline, max_dataline));

  
  key_pos = keypos;

  ComputeDistribution(begin);
}

void Ensemble::Reset(StateInfo *si)
{

  count = 0;
  offset = 0;
  burnIn = 0;
  key_pos = 0;

  setDimensions(si);
}

void Ensemble::setDimensions(StateInfo *si)
{
  dimensions_set = true;
  _si = si;
}

bool Ensemble::AccumData(const vector<double>& v, const State& p)
{
  states.push_back(StateData(v, p));
  count++;
  if (count>keep) {
    states.pop_front();
    count--;
    offset++;
  }

  return true;
}



void Ensemble::stats(int n, double& m, double& s)
{
  m = s = 0.0;
  if (count<2) return;

  n = min<int>(count, n);

  for (int i=count-1; i>=count-n; i--) {
    m += states[i].prob[0];
    s += states[i].prob[0] * states[i].prob[0];
  }

  m /= n;
  s = sqrt(fabs((s - n*m*m)/(n-1)));
}



void Ensemble::makeSampleFraction()
{
  npop.erase(npop.begin(), npop.end());

  if (_si->ptype == StateInfo::None  ||
      _si->ptype == StateInfo::Block  )
    {
      npop.insert(1);
      return;
    }

  if (cnts.size()>1) {
    list<int> er;
    for (mapIIiter m=cnts.begin(); m!=cnts.end(); m++) {
      if (m->second < minsub) er.push_back(m->first);
    }
    for (list<int>::iterator it=er.begin(); it!=er.end(); it++) cnts.erase(*it);
  }

  for (mapIIiter m=cnts.begin(); m!=cnts.end(); m++) {
    fraction[m->first] = pair<double, int>(m->second, m->second);
    npop.insert(m->first);
  }

  fracMapIter cur = fraction.begin();
  fracMapIter lst = cur++;
  for (; cur!=fraction.end(); cur++) {
    cur->second.first += lst->second.first;
    lst = cur;
  }
  
  double fnrm = fraction.rbegin()->second.first;
  for (cur=fraction.begin(); cur!=fraction.end(); cur++) {
    cur->second.first /= fnrm;
    ccum[cur->first] = cur->second.first;
    MM.push_back(cur->first);
  }
  fraction.rbegin()->second.first = 1.0;
  
  if (fraction.size()==0) {
    cout << "***********************" << endl;
    cout << "makeSampleFraction:\nno states?" << endl;
    cout << "***********************" << endl;
    cout << "cnts:" << endl;
    for (mapIIiter m=cnts.begin(); m!=cnts.end(); m++)
      cout << setw(4) << m->first << setw(9) << m->second << endl;
    cout << "***********************" << endl;
  }

  // For debugging
  if (1) {
    cout << "***********************" << endl;
    cout << "makeSampleFraction:\ndebug for Process " << myid << endl;
    cout << "***********************" << endl;
    cout << "Counts:" << endl;
    for (mapIIiter m=cnts.begin(); m!=cnts.end(); m++)
      cout << setw(4) << m->first << setw(9) << m->second << endl;
    cout << "Fraction:" << endl;
    for (cur=fraction.begin(); cur!=fraction.end(); cur++)
      cout << setw(4) << cur->first << setw(9) << cur->second.second 
	   << setw(9) << cur->second.first << endl;
    cout << "***********************" << endl;
  }
  
}

unsigned Ensemble::sampleSubspace()
{
  if (_si->ptype == StateInfo::None ) return 1;
  if (_si->ptype == StateInfo::Block) return 1;

  ComputeDistribution();

  int nsize = fraction.size();

  if (nsize==0) {
    string msg("sampleSubspace: fraction map has zero size!");
    throw InternalError(msg, __FILE__, __LINE__);
  }

  if (nsize==1) return fraction.begin()->first;

				// Choose a subspace
  double frac = (*unit)();
  int lo = 0, hi = nsize-1;
  int m = hi;

  if      (frac<fraction. begin()->second.first) m = lo;
  else if (frac>fraction.rbegin()->second.first) m = hi;
  else {
    while (hi-lo>1) {
      m = (lo + hi)/2;
      if (frac>fraction[MM[m]].first) {
	lo = m;
      } else
	hi = m;
    }
  }

  return MM[m];
}


double Ensemble::PDFSubspace(unsigned m)
{
  mapIDiter j = ccum.find(m);
  if (j == ccum.end())   
    return 0.0;
  else if (j == ccum.begin()) 
    return j->second;
  else {
    double ret = (j--)->second;
    return ret - j->second;
  }
}


void Ensemble::Broadcast()
{
  if (!mpi_used) return;

  ++MPIMutex;

  // Broadcast from root to all . . . 

  int size = states.size();
  MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

  if (size) {

    if (myid==0) {
      for (deque<StateData>::iterator i=states.begin(); i!=states.end(); i++)
	i->Broadcast();
    } else {
				// For security . . . shouldn't really
				// need this if I've stuck to my own
				// rule: the root process accumulates
				// the ensemble
      states.erase(states.begin(), states.end());
      StateData p(_si);

      for (int i=0; i<size; i++) {
	p.Broadcast();
	states.push_back(p);
      }
    }
  }

  MPI_Bcast(&count,  1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&offset, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&burnIn, 1, MPI_INT, 0, MPI_COMM_WORLD);


  --MPIMutex;
}

void Ensemble::Order(int icomp)
{
  //
  // Used an ordered association to do the work
  //
  typedef pair<double, vector<double> > ostate;
  typedef map< double, ostate > omap;

  omap::iterator jt;
  double val;

  //
  // Iterate through the sampled posterior
  //
  for (deque<StateData>::iterator it=states.begin(); it!=states.end(); it++) {

    unsigned sz = it->p.size();
    unsigned mm = it->p.M();
    unsigned dd = (sz - _si->Next - mm)/mm;

    ostate p;
    p.second = vector<double>(dd);

    omap data;
    for (unsigned j=0; j<mm; j++) {
				// Set the weight
      p.first = it->p[j];
				// Set the parameter vector
      for (unsigned k=0; k<dd; k++) p.second[k] = it->p[mm+j*dd+k];

				// Set the sort key
      if (icomp<0) val = p.first;
      else         val = p.second[icomp];

				// Load the record
      data[val] = p;
    }

				// Reload the ordered state
    jt = data.begin();
    for (unsigned j=0; j<mm; j++) {
      it->p[1+j] = jt->second.first;
      for (unsigned k=0; k<dd; k++) it->p[1+mm+j*dd+k] = jt->second.second[k];
      jt++;
    }

  }

  // Done!
}

void Ensemble::dumpStates(const string& filename, int beg, int end) const
{
  // Precision for ascii output
  const int output_prec = 16;

  ofstream out(filename.c_str());

  if (out) {

    size_t count = states.size();
    
    if (binary_dump) {
      out.write((const char*)count, sizeof(size_t));
      for (size_t i=0; i<states.size(); i++) {
	size_t N = states[i].prob.size();
	size_t M = states[i].p.size();
	out.write((const char*)N, sizeof(size_t));
	out.write((const char*)M, sizeof(size_t));
	for (size_t j=0; j<N; j++) {
	  const double v = states[i].prob.at(j);
	  out.write((const char*)&v, sizeof(double));
	}
	for (size_t j=0; j<M; j++) {
	  const double v = states[i].p.at(j);
	  out.write((const char*)&v, sizeof(double));
	}
      }
      
    } else {

      char q = '"';

      out << std::setw(10) << "\"Level\"";
      out << std::setw(10) << "\"Iter #\"";
      out << std::setw(8+output_prec) << "\"Probability\"";
      out << std::setw(8+output_prec) << "\"Likelihood\"";
      out << std::setw(8+output_prec) << "\"Prior\"";

      if (_si->ptype == StateInfo::Mixture ||
	  _si->ptype == StateInfo::Extended )
	out << setw(10) << "\"Number\"";
      
      for (unsigned k=0; k<_si->Ntot; k++) {
	ostringstream ostr;
	ostr << q << _si->StateLabels[k] << q;
        out << setw(output_prec+10) << ostr.str();
      }
      out << endl;

      int ibeg = std::max<int>(beg, 0);
      int iend = std::min<int>(end, count);

      for (int i = ibeg; i < iend; i++) {

	State p = states[i].p;

	out << std::setw(10) << current_level
	    << std::setw(10) << i
	    << scientific
	    << std::setw(8+output_prec) 
	    << std::setprecision(output_prec) << states[i].prob[0]
	    << std::setw(8+output_prec) 
	    << std::setprecision(output_prec) << states[i].prob[1]
	    << std::setw(8+output_prec) 
	    << std::setprecision(output_prec) << states[i].prob[2];
      
	if (p.Type() == StateInfo::Mixture ||
	    p.Type() == StateInfo::Extended )
	  out << std::setw(10) << p.M();

	for (unsigned k=0; k<p.N(); k++)
	  out << std::setw(output_prec+10) << p[k];

	out << std::endl;
      }
      if (myid==0) 
	std::cout << "Ensemble::dumpStates: debug file <" << filename 
		  << "> dumped" << std::endl;
    } 
  } else {
    if (myid==0)
      std::cout << "Ensemble::dumpStates: can't open dump file <" << filename
		<< ">!" << std::endl;
  }

}

static bool _state_prob_compare(StateData& a, StateData& b)
{
    return (a.prob[0] < b.prob[0]);
}
 
static bool _state_like_compare(StateData& a, StateData& b)
{
    return (a.prob[1] < b.prob[1]);
}
 

// Return the state (from the converged distribution if possible)
// with the maximum likelihood value
//
std::vector<double> Ensemble::getMaxLikeStateVector()
{
  std::vector<double> v;
  unsigned siz = 0;

  // Root node checks ensemble and broadcasts result to slaves.  This
  // accomodates parallel and serial likelihood functions.
  //
  if (myid==0) {

    std::deque<StateData>::iterator initl = states.begin();
    std::deque<StateData>::iterator final = states.end();
  
    if (burnIn>0 and burnIn > offset) initl += burnIn - offset;

    std::deque<StateData>::iterator ret = 
      std::max_element(initl, final, _state_like_compare);

    cout << setw(10) << "" << setw(20) << left
	 << "Total count" << states.size() << endl
	 << setw(10) << "" << setw(20) << left
	 << "Offset"      << offset        << endl
	 << setw(10) << "" << setw(20) << left
	 << "burnIn"      << burnIn        << endl
	 << setw(10) << "" << setw(20) << left
	 << "position"    << ret - initl   << endl
	 << setw(10) << "" << setw(20) << left
	 << "prob"        << ret->prob[0]  << endl
	 << setw(10) << "" << setw(20) << left
	 << "like"        << ret->prob[1]  << endl << endl;


    v   = ret->p.vec();
    siz = v.size();

    MPI_Bcast(&siz,    1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    MPI_Bcast(&v[0], siz, MPI_DOUBLE,   0, MPI_COMM_WORLD);

  
  } else {
    
    MPI_Bcast(&siz,    1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    v.resize(siz);
    MPI_Bcast(&v[0], siz, MPI_DOUBLE,   0, MPI_COMM_WORLD);

  }

  return v;
}

// Convert to clivectord type
//
clivectord* Ensemble::getMaxLikeState()
{
  std::vector<double> v = getMaxLikeStateVector();
  return new clivectord(v);
}

// Return the state (from the converged distribution if possible)
// with the maximum likelihood value
//
std::vector<double> Ensemble::getMaxProbStateVector()
{
  std::vector<double> v;
  unsigned siz = 0;

  // Root node checks ensemble and broadcasts result to slaves.  This
  // accomodates parallel and serial likelihood functions.
  //
  if (myid==0) {

    std::deque<StateData>::iterator initl = states.begin();
    std::deque<StateData>::iterator final = states.end();

    if (burnIn>0 and burnIn > offset) initl += burnIn - offset;

    std::deque<StateData>::iterator ret = 
      std::max_element(initl, final, _state_prob_compare);

    cout << setw(10) << "" << setw(20) << left
	 << "Total count" << states.size() << endl
	 << setw(10) << "" << setw(20) << left
	 << "Offset"      << offset        << endl
	 << setw(10) << "" << setw(20) << left
	 << "burnIn"      << burnIn        << endl
	 << setw(10) << "" << setw(20) << left
	 << "position"    << ret - initl   << endl
	 << setw(10) << "" << setw(20) << left
	 << "prob"        << ret->prob[0]  << endl
	 << setw(10) << "" << setw(20) << left
	 << "like"        << ret->prob[1]  << endl << endl;

    v   = ret->p.vec();
    siz = v.size();


    MPI_Bcast(&siz,    1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    MPI_Bcast(&v[0], siz, MPI_DOUBLE,   0, MPI_COMM_WORLD);

  
  } else {
    
    MPI_Bcast(&siz,    1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    v.resize(siz);
    MPI_Bcast(&v[0], siz, MPI_DOUBLE,   0, MPI_COMM_WORLD);

  }

  return v;
}

// Convert to clivectord type
//
clivectord* Ensemble::getMaxProbState()
{
  std::vector<double> v = getMaxProbStateVector();
  return new clivectord(v);
}

void Ensemble::PrintDiag()
{
  PrintDiag(cout);
}

//! Temporary debug
/*
void Ensemble::fraction_debug()
{
  cout << "***********************" << endl;
  cout << "Save/Restore"            << endl;
  cout << "***********************" << endl;
  cout << "Fraction:" << endl;
  for (fracMapIter cur=fraction.begin(); cur!=fraction.end(); cur++)
    cout << setw(4) << cur->first << setw(9) << cur->second.second 
	 << setw(9) << cur->second.first << endl;
  cout << "***********************" << endl;
}
*/

// SWIG interface for returning a numpy 2-d array of states
void Ensemble::getStatesNumpy(int* n1, int* n2, double **seq)
{
				// Number of states
  *n1 = static_cast<int>(states.size());
				// Dimension of parameter vector
  *n2 = static_cast<int>(states[0].p.size());
				// Resize array storage
  numpy_view.resize((*n1)*(*n2));
				// Assign the return pointer
  *seq = &numpy_view[0];
				// Copy the data
  for (int n=0; n<*n1; n++) {
    for (int k=0; k<*n2; k++) {
      (*seq)[n*(*n2) + k] = states[n].p[k];
    }
  }
}


void Ensemble::TestState()
{
  State s = Sample();
  std::cout << s.SI()->Info() << std::endl;
}
