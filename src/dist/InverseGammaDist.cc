#include <cfloat>

#include <InverseGammaDist.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::InverseGammaDist)

using namespace BIE;

InverseGammaDist::InverseGammaDist() : alpha(1.0), beta(1.0), 
				       vmin(0.0), vmax(DBL_MAX)
{
  _type  = NormalMult;
  mean   = 1.0;
  var    = 1.0;
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = 1.0;

  ugam   = GammaPtr(new Gamma(alpha, BIEgen));
}

// The last two parameters can be defaulted
InverseGammaDist::InverseGammaDist(double a, double b, double minv, double maxv)
{
  _type  = NormalMult;
  alpha  = a;
  beta   = b;
  if (a<=1)
    mean = DBL_MAX;
  else
    mean = b/(a - 1.0);
  if (a<=2)
    var  = DBL_MAX;
  else
    var  = b*b/( (a - 1.0)*(a - 1.0)*(a - 2.0) );

  vmin   = max<double>(0.0, minv);
  vmax   = min<double>(DBL_MAX, maxv);
  
  qmin   = 1.0/(b*vmax);

  if (vmin <= 0.0)
    qmax = DBL_MAX;
  else
    qmax = 1.0/(b*vmin);

  mincdf = gamma_p(a, qmin);
  maxcdf = gamma_p(a, qmax);
  norm   = maxcdf - mincdf;

  ugam   = GammaPtr(new Gamma(alpha, BIEgen));
}

Distribution* InverseGammaDist::New() 
{ return new InverseGammaDist(alpha, beta); }
  
double InverseGammaDist::PDF(State& x)
{
  double ans;
  double q = 1.0/(beta*x[0]);
  if (q>=qmin && q<=qmax)
    ans = beta * pow(q, alpha+1.0) * exp(-q) / norm;
  else
    ans = 0.0;
  return ans;
}

double InverseGammaDist::logPDF(State& x)
{
  double ans = 0.0;
  double q = 1.0/(beta*x[0]);
  if (q>=qmin && q<=qmax)
    ans = log(beta) + (alpha+1.0)*log(q) - q - log(norm);
  else
    throw ImpossibleStateException(__FILE__, __LINE__);
  return ans;
}

double InverseGammaDist::CDF(State& x) 
{
  double ans;
  double q = 1.0/(beta*x[0]);
  if (q>=vmax)
    ans = 1.0;
  else if (q<=vmin)
    ans = 0.0;
  else
    ans = (gamma_p(alpha, q) - mincdf)/norm;
  return ans;
}

vector<double> InverseGammaDist::lower(void)
{ return vector<double>(1, vmin); }

vector<double> InverseGammaDist::upper(void)
{ return vector<double>(1, vmax); }

vector<double> InverseGammaDist::Mean(void)
{ return vector<double>(1, mean); }

vector<double> InverseGammaDist::StdDev(void)
{ return vector<double>(1, sqrt(var)); }

vector<double> InverseGammaDist::Moments(int i)
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = mean;
    break;
  case 2:
    ans = var + mean*mean;
    break;
  default:
    cerr << "InverseGammaDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State InverseGammaDist::Sample(void)
{
  return State(vector<double>(1, 1.0/(beta*(*ugam)())));
}
