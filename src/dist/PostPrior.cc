
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

#include <PostPrior.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PostPrior)

using namespace BIE;

PostPrior::PostPrior(Prior * old, Ensemble* dist) : Prior(old)
{
  _dist        = dist;
  _lower       = old->lower();
  _upper       = old->upper();
  _clamp       = true;
  _sample      = false;
  _manage_dist = false;

  normal       = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
}


PostPrior::PostPrior
(Prior * old, Ensemble * dist,
 int level, int nburn, string filename, int keypos) : Prior(old)
{
  _dist        = dist->New();
  _clamp       = true;
  _sample      = false;
  _manage_dist = false;
  _lower       = old->lower();
  _upper       = old->upper();

  normal       = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));

  _dist->Reset(_si, level, nburn, filename, keypos);
}


PostPrior::~PostPrior()
{
  if (_manage_dist) delete _dist;
}


void PostPrior::SamplePrior(State *s)
{
  State t(*s);

  t = _dist->Sample();

  unsigned indx=0, iter=0;
  if (_clamp) {
    while (iter++ < max_iter) {
      bool check = true;
      for (unsigned j=0; j<_si->Ntot; j++) {
	if (t[j] > _upper[j]) check = false;
	if (t[j] < _lower[j]) check = false;
      }
      if (check) break;
      // Try again:
      *s = _dist->Sample();
    }
    
    if (iter >= max_iter) {
      for (unsigned j=0; j<_si->Ntot; j++) {
	if (t[j] > _upper[indx])  t[j] = _upper[indx];
	if (t[j] < _lower[indx])  t[j] = _lower[indx];
      }
    }
  }

  if (t.Type() == StateInfo::Block) {
    for (unsigned n=0; n<t.N(); n++) {
      if (t.change(n)) (*s).vec(n) = t.vec(n);
    }
  } else {
    *s = t;
  }

}


PostPrior* PostPrior::New()
{
  PostPrior *p = new PostPrior();

  p->_si   = _si;
  p->_dist = _dist;

  vector<double> ret(_si->Ntot);

  return p;
}


void 
PostPrior::SampleProposal(Chain& ch, MHWidth* width)
{
  const int safety = 10000;
  bool check = true, done = false;
  int count = 0, N = ch.N0();
  State s(*ch.Cur()), m(*ch.Cur());
  
				// Ideal broadening for optimizing mixing
				// properties for Gasssian targets and 
				// proposals following Gelman et al. 1996,
				// [Efficient Metropolis jumping rules,
				// Bayesian Statistics V, eds. Bernado, 
				// Berger, David & Smith, Oxford,  pp. 599-608]
  double factor = ch.factor;
  factor *= 2.4/sqrt(static_cast<float>(N)) * width_factor;

  while (!done && count<safety) {
    if (_sample) {
				// Sample posterior distribution
      s = _dist->Sample(_si->M);
      m = _dist->Sample(_si->M);
      for (unsigned int i=0; i<s.size(); i++) 
	s[i] = factor * (s[i] - m[i]);

    } else {

      s = vector<double>(N);
      for (int i=0; i<N; i++) 
	s[i] = width_factor * width->width(i) * (*normal)();

    }

      
    for (int j=0; j<N; j++)  {
      ch.Vec1(j) =  ch.Vec0(j) + s[j];
      if (ch.Vec1(j) > _upper[j]) check = false;
      if (ch.Vec1(j) < _lower[j]) check = false;
      // Sanity check
      if (std::isinf(ch.Vec1(j)) || 
	  std::isnan(ch.Vec1(j)) ) check = false;
    }

				// State is in bounds: we have our proposal
    if (check) done = true;

    count++;
  }
    
  //
  // Revert to the original state, if unsuccessful
  //
  if (!done) ch.AssignState1(ch);

  //
  // For debugging . . .
  //
  if (0) {
    if (myid==0) {
      cout << "TEST PostPrior, level=" << current_level << endl;
      ch.PrintState ();
      ch.PrintState1();
      cout << setw(80) << setfill('-') << '-' << endl << setfill(' ');
      cout << "Proposed delta: ";
      if (done) cout << " good" << endl;
      else      cout << " bad"  << endl;
      for (unsigned i=0; i<s.size(); i++) cout << setw(15) << s[i];
      cout << endl;
      cout << setw(80) << setfill('-') << '-' << endl << setfill(' ');
    }
  }
}
    
