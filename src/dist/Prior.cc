// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#include <Uniform.h>
#include <Normal.h>
#include <Cauchy.h>
#include <Prior.h>
#include <UniformAdd.h>
#include <UniformMult.h>
#include <NormalAdd.h>
#include <NormalMult.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Prior)

using namespace BIE;

extern BIEACG* BIEgen;

double   Prior::width_factor = 0.1;
unsigned Prior::max_iter     = 2000;

Prior::Prior(StateInfo *si, clivectordist *v)  : _initialdist(0), _si(si)
{
  _dist = (*v)();
  local_dist = false;
				// Assign proposal types based on
				// distribution, check distribution size
  initialize_proposal();
}

Prior::Prior(StateInfo *si, vector<Distribution*>& dist)  : _initialdist(0), _si(si)
{
  _dist = dist;
  local_dist = false;
				// Assign proposal types based on
				// distribution, check distribution size
  initialize_proposal();
}

Prior::Prior(StateInfo *si)  : _initialdist(0), _si(si)
{
  local_dist = false;
  // Proposal types, distribution assignment and size check to be
  // performed by derived class
}

Prior::~Prior()
{
  if (local_dist)
    for (vector<Distribution*>::iterator
	   it=_dist.begin(); it!=_dist.end(); it++) delete *it;
}

State Prior::Sample(void)
{
  vector<double> p;

  for (vector<Distribution*>::iterator
	 it=_dist.begin(); it!=_dist.end(); it++) {
    vector<double> v = (*it)->Sample();
    p.insert(p.end(), v.begin(), v.end());
  }
  
  return State(_si, p);
}

void Prior::SampleProposal(Chain& ch, MHWidth* w)
{
  if (ch.Type() == StateInfo::RJTwo) {
    ch.Vec1(0) = ch.Vec0(0);
    unsigned m  = static_cast<unsigned>(floor(ch.Vec1(0)+0.01));
    unsigned T  = ch.Cur()->SI()->T;
    unsigned N1 = ch.Cur()->SI()->N1;
    unsigned N2 = ch.Cur()->SI()->N2;
    for (unsigned j=1; j<ch.N0(); j++) {
      if (j<=T)
	ch.Vec1(j) = (*_trans[j])(ch.Vec0(j),ch.factor*w->width(j));
      else if (m==0 && j>=1+T && j<1+T+N1)
	ch.Vec1(j) = (*_trans[j])(ch.Vec0(j),ch.factor*w->width(j));
      else if (m==1 && j>=1+T+N1 && j<1+T+N1+N2)
	ch.Vec1(j) = (*_trans[j])(ch.Vec0(j),ch.factor*w->width(j));
      else
	ch.Vec1(j) = ch.Vec0(j);
    }
  } else {
    for (unsigned j=0; j<ch.N0(); j++) {
      ch.Vec1(j) = (*_trans[j])(ch.Vec0(j),ch.factor*w->width(j));
    }
  }
				// Sanity check
  for (unsigned j=0; j<ch.N0(); j++) {
    if (std::isnan(ch.Vec1(j))) {
      cerr << "NaN in Prior [after]: icomp" << j << endl;
    }
  }
}

void Prior::initialize_proposal()
{
				// Delete the old MH proposal
				// distribution list
  _trans.erase(_trans.begin(), _trans.end());
  
  unsigned N = 0, nd;
  for (unsigned j=0; j<_dist.size(); j++) {
				// Dimensionality of distribution
    nd = _dist[j]->Dim();
    N += nd;
				// Location in state
    if (j==0) _pos.push_back(0);
    else      _pos.push_back(_pos.back()+_dist[j-1]->Dim());

    _len.push_back(nd);
    _tsi.push_back(boost::shared_ptr<StateInfo>(new StateInfo(nd)));
    _sta.push_back(State(_tsi.back().get()));

    switch (_dist[j]->Type()) {	// Use the distribution supplied info to
				// choose the MH proposal
    case Distribution::UniformAdd:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::UniformAdd()));
      break;
    case Distribution::UniformMult:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::UniformMult()));
      break;
    case Distribution::NormalAdd:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::NormalAdd()));
      break;
    case Distribution::NormalMult:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::NormalMult()));
      break;
    default:
      throw PriorTypeException(_dist[j]->Type(), __FILE__, __LINE__);
    }
  }

				// Check the dimensionality:
  if (N > _si->Ntot) {		// allow for subdimensional popluation 
    ostringstream sout;		// for use with PriorCollection
    sout << "Dimensionality mismatch between total distribution length ("
	 << N << ") and StateInfo definition (" << _si->Ntot << ")";
    throw PriorException(sout.str().c_str(),  __FILE__, __LINE__); 
  }

}


Prior::Prior(Prior* p)
{
  _si = p->_si;
  _initialdist = p->_initialdist;
  for (unsigned i=0; i<_dist.size(); i++) {
    _dist.push_back((p->_dist[i])->New());
  }
}

Prior* Prior::New()
{
  Prior* p = new Prior();
  for (unsigned i=0; i<_dist.size(); i++) {
    p->_dist.push_back(_dist[i]->New());
  }

  return p;
}
    
double Prior::PDF(State& p)
{
  double ans = 1.0;

  if (_si->ptype == StateInfo::RJTwo) {
    unsigned ind  = floor(p[0]);
    unsigned icnt = 0;
    for (unsigned i=0; i<_dist.size(); i++) {
      if (icnt < 1 + _si->T || 
	  (ind == 0 && icnt < 1 + _si->T + _si->N1) ||
	  (ind == 1 && icnt > 1 + _si->T + _si->N1)  ) 
	{
	  vector<double> v(p.begin()+_pos[i], 
			   p.begin()+_pos[i] + _len[i]);
	  _sta[i].setState(v);
	  ans *= _dist[i]->PDF(_sta[i]);
	}
      icnt += _len[i];
    }

  } else {
    for (unsigned i=0; i<_dist.size(); i++) {
      vector<double> v(p.begin()+_pos[i], 
		       p.begin()+_pos[i] + _len[i]);
      _sta[i].setState(v);
      ans *= _dist[i]->PDF(_sta[i]);
    }
  }

  return ans;
}


double Prior::logPDF(State& p)
{
  double ans = 0.0;

  if (_si->ptype == StateInfo::RJTwo) {
    unsigned ind  = floor(p[0]);
    unsigned icnt = 1;
    for (unsigned i=0; i<_dist.size(); i++) {
      if (icnt < 1 + _si->T || 
	  (ind == 0 && icnt < 1 + _si->T + _si->N1) ||
	  (ind == 1 && icnt > 1 + _si->T + _si->N1)  ) 
	{
	  vector<double> v(p.begin()+_pos[i], 
			   p.begin()+_pos[i] + _len[i]);
	  _sta[i].setState(v);
	  ans += _dist[i]->logPDF(_sta[i]);
	}
      icnt += _len[i];
    }

  } else {
    for (unsigned i=0; i<_dist.size(); i++) {
      vector<double> v(p.begin()+_pos[i], 
		       p.begin()+_pos[i] + _len[i]);
      _sta[i].setState(v);
      ans += _dist[i]->logPDF(_sta[i]);
    }
  }

  return ans;
}


vector<double> Prior::lower(void)
{
  vector<double> ans, v;
  for (unsigned i=0; i<_dist.size(); i++) {
    v = _dist[i]->lower();
    ans.insert(ans.end(), v.begin(), v.end());
  }
  return ans;
}

vector<double> Prior::upper(void)
{
  vector<double> ans, v;
  for (unsigned i=0; i<_dist.size(); i++) {
    v = _dist[i]->upper();
    ans.insert(ans.end(), v.begin(), v.end());
  }
  return ans;
}
    
vector<double> Prior::Mean(void)
{
  vector<double> ans, v;
  for (unsigned i=0; i<_dist.size(); i++) {
    v = _dist[i]->Mean();
    ans.insert(ans.end(), v.begin(), v.end());
  }
  return ans;
}

vector<double> Prior::StdDev(void)
{
  vector<double> ans, v;
  for (unsigned i=0; i<_dist.size(); i++) {
    v = _dist[i]->StdDev();
    ans.insert(ans.end(), v.begin(), v.end());
  }
  return ans;
}


vector<double> Prior::Moments(unsigned n)
{
  vector<double> ans, v;
  for (unsigned i=0; i<_dist.size(); i++) {
    v = _dist[i]->Moments(n);
    ans.insert(ans.end(), v.begin(), v.end());
  }
  return ans;
}

void Prior::SamplePrior(State *s)
{
  if (_initialdist) {
    *s = State(_si, _initialdist->Sample());
    return;
  }

  vector<double> ans, v;
  switch (s->Type()) {
  case StateInfo::None:
  case StateInfo::RJTwo:
    for (unsigned i=0; i<_dist.size(); i++) {
      v = _dist[i]->Sample();
      ans.insert(ans.end(), v.begin(), v.end());
    }
    s->setState(ans);
    break;
  case StateInfo::Block:
    {
      unsigned k = 0;
      for (size_t i=0; i<_dist.size(); i++) {
	v = _dist[i]->Sample();
	for (size_t j=0; j<v.size(); j++, k++) {
	  if (s->change(k))  ans.push_back(v[j]);
	  else               ans.push_back(s->vec(k));
	}
      }
    }
    s->setState(ans);
    break;
  default:
    string msg = "You are calling Prior for a mixture state. "
      "I don't know what to do!";
      throw PriorException(msg.c_str(), __FILE__, __LINE__);
  }
}
