
#include <iostream>
#include <fstream>

using namespace std;

#include <BIEconfig.h>
#include <ACG.h>
#include <Uniform.h>
#include <Normal.h>
#include <Dirichlet.h>
#include <BIEException.h>
#include <MixturePrior.h>
#include <UniformAdd.h>
#include <UniformMult.h>
#include <NormalAdd.h>
#include <NormalMult.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MixturePrior)

using namespace BIE;

unsigned MixturePrior::ITMAX  = 10000;
double MixturePrior::min_prob = 1.0e-30;

MixturePrior::MixturePrior() : Prior()
{
  pmean   = 0.0;

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();
};

MixturePrior::MixturePrior(MixturePrior * p)
{
  // Base class data
  _si     = p->_si;

  // Derived class data
  pmean   = p->pmean;
  _trans  = p->_trans;
  _prior0 = p->_prior0;
  _prior1 = p->_prior1;

  ret = State(_si);

  Alpha = 1.0;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/(i+1));
    mix.push_back(DirichletPtr(new Dirichlet(&A)));
  }

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal(0.0, 1.0, BIEgen));
  if (pmean > 0.0)
    pois = PoissonPtr(new Poisson(pmean, BIEgen));
  else
    pois  = PoissonPtr();
};


MixturePrior::
MixturePrior(StateInfo *si, double alpha, 
	     clivectordist *prior0, clivectordist *prior1) : Prior(si)
{
  // Begin sanity checking

  if (_si->ptype == StateInfo::None || _si->ptype == StateInfo::Block) {
    string msg = "You must define a mixture state in the StateInfo instance "
      " to implement a mixture model\n";
    throw PriorException(msg.c_str(), __FILE__, __LINE__);
  }

  if (prior0==0) {
    if (_si->Ndim) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Ndim != (*prior0)().size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank "
	   << (*prior0)().size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }
    
  if (prior1==0) {
    if (_si->Next) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Next != (*prior1)().size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank "
	   << (*prior1)().size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }

  // End sanity checking
    
  ret = State(_si);

  pmean   = 0.0;
  if (prior0) _prior0 = (*prior0)();
  if (prior1) _prior1 = (*prior1)();

  Alpha   = alpha;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/(i+1));
    mix.push_back(DirichletPtr(new Dirichlet(&A)));
  }

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();

  initialize_proposal();
}


MixturePrior::
MixturePrior(StateInfo *si, double alpha, 
	     std::vector<Distribution*> prior0,
	     std::vector<Distribution*> prior1) : Prior(si)
{
  // Begin sanity checking

  if (_si->ptype == StateInfo::None || _si->ptype == StateInfo::Block) {
    string msg = "You must define a mixture state in the StateInfo instance "
      " to implement a mixture model\n";
    throw PriorException(msg.c_str(), __FILE__, __LINE__);
  }

  if (prior0.size()==0) {
    if (_si->Ndim) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Ndim != prior0.size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank "
	   << prior0.size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }
    
  if (prior1.size()==0) {
    if (_si->Next) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Next != prior1.size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank "
	   << prior1.size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }

  // End sanity checking
    
  ret = State(_si);

  pmean   = 0.0;
  _prior0 = prior0;
  _prior1 = prior1;

  Alpha   = alpha;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/(i+1));
    mix.push_back(DirichletPtr(new Dirichlet(&A)));
  }

  unit    = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg    = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal  = NormalPtr(new Normal ( 0.0, 1.0, BIEgen));
  pois    = PoissonPtr();

  initialize_proposal();
}


MixturePrior::
MixturePrior(StateInfo *si, double alpha, double Pmean, 
	     clivectordist *prior0, clivectordist *prior1) : Prior(si)
{
  // Begin sanity checking

  if (_si->ptype == StateInfo::None || _si->ptype == StateInfo::Block) {
    string msg = "You must define a mixture state in the StateInfo instance "
      " to implement a mixture model\n";
    throw PriorException(msg.c_str(), __FILE__, __LINE__);
  }

  if (prior0==0) {
    if (_si->Ndim) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Ndim != (*prior0)().size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank "
	   << (*prior0)().size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }
    
  if (prior1==0) {
    if (_si->Next) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Next != (*prior1)().size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank "
	   << (*prior1)().size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }

  // End sanity checking
    
  ret = State(_si);

  pmean   = Pmean;
  if (prior0) _prior0 = (*prior0)();
  if (prior1) _prior1 = (*prior1)();

  Alpha = alpha;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/(i+1));
    mix.push_back(DirichletPtr(new Dirichlet(&A)));
  }

  unit   = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg   = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal = NormalPtr(new Normal ( 0.0, 1.0, BIEgen));
  if (pmean > 0.0)
    pois = PoissonPtr(new Poisson(pmean,     BIEgen));
  else
    pois = PoissonPtr();

  initialize_proposal();
}

MixturePrior::
MixturePrior(StateInfo *si, double alpha, double Pmean, 
	     std::vector<Distribution*> prior0,
	     std::vector<Distribution*> prior1) : Prior(si)
{
  // Begin sanity checking

  if (_si->ptype == StateInfo::None || _si->ptype == StateInfo::Block) {
    string msg = "You must define a mixture state in the StateInfo instance "
      " to implement a mixture model\n";
    throw PriorException(msg.c_str(), __FILE__, __LINE__);
  }

  if (prior0.size()==0) {
    if (_si->Ndim) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Ndim != prior0.size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies a mixture parameter vector of rank "
	   << _si->Ndim << ", but you specified a vector of rank "
	   << prior0.size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }
    
  if (prior1.size()==0) {
    if (_si->Next) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank zero"
	   << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  } else {
    if (_si->Next != prior1.size()) {
      ostringstream ostr;
      ostr << "The StateInfo specifies an extended parameter vector of rank "
	   << _si->Next << ", but you specified a vector of rank "
	   << prior1.size() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }

  // End sanity checking
    
  ret = State(_si);

  pmean   = Pmean;
  _prior0 = prior0;
  _prior1 = prior1;

  Alpha = alpha;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/(i+1));
    mix.push_back(DirichletPtr(new Dirichlet(&A)));
  }

  unit   = UniformPtr(new Uniform(-1.0, 1.0, BIEgen));
  useg   = UniformPtr(new Uniform( 0.0, 1.0, BIEgen));
  normal = NormalPtr (new Normal ( 0.0, 1.0, BIEgen));
  if (pmean > 0.0)
    pois = PoissonPtr(new Poisson(pmean,     BIEgen));
  else
    pois = PoissonPtr();

  initialize_proposal();
}

void MixturePrior::initialize_proposal()
{
  for (unsigned j=0; j<_prior0.size(); j++) {

    unsigned nd = _prior0[j]->Dim();
    if (j==0) _pos0.push_back(0);
    else      _pos0.push_back(_pos0[j-1]+_len0[j-1]);

    _len0.push_back(nd);
    _tsi0.push_back(boost::shared_ptr<StateInfo>(new StateInfo(nd)));
    _sta0.push_back(State(_tsi0.back().get()));

    switch (_prior0[j]->Type()) { 
				// Use the distribution supplied info to
				// choose the MH proposal
    case Distribution::UniformAdd:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::UniformAdd()));
      break;
    case Distribution::UniformMult:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::UniformMult()));
      break;
    case Distribution::NormalAdd:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::NormalAdd()));
      break;
    case Distribution::NormalMult:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::NormalMult()));
      break;
    default:
      throw PriorTypeException(_prior0[j]->Type(), __FILE__, __LINE__);
    }
  }

  for (unsigned j=0; j<_prior1.size(); j++) {

    unsigned nd = _prior1[j]->Dim();
    if (j==0) _pos1.push_back(0);
    else      _pos1.push_back(_pos1[j-1]+_len1[j-1]);

    _len1.push_back(nd);
    _tsi1.push_back(boost::shared_ptr<StateInfo>(new StateInfo(nd)));
    _sta1.push_back(State(_tsi1.back().get()));

    switch (_prior1[j]->Type()) { 
				// Use the distribution supplied info to
				// choose the MH proposal
    case Distribution::UniformAdd:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::UniformAdd()));
      break;
    case Distribution::UniformMult:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::UniformMult()));
      break;
    case Distribution::NormalAdd:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::NormalAdd()));
      break;
    case Distribution::NormalMult:
      for (unsigned i=0; i<nd; i++)
	_trans.push_back(TransProbPtr(new BIE::NormalMult()));
      break;
    default:
      throw PriorTypeException(_prior1[j]->Type(), __FILE__, __LINE__);
    }
  }

}

MixturePrior::~MixturePrior()
{
}

void MixturePrior::SampleProposal(Chain& ch, MHWidth* w)
{
				// Weight parameters
  ch.Tri()->Reset(ch.M0());

  double sum=0.0, z;
  for (unsigned i=0; i<ch.M0(); i++) {
    /*
      Mapping between z ~ (-infty, infty) and w ~ [0, 1]

     z = log(w/(1-w))

     w = e^z/(1+e^z) = 1/(1+e^{-z})

    */
				// Shift transformed variable
    z = log(ch.Wght0(i)/(1.0 - ch.Wght0(i))) + 
      ch.factor*w->widthM(ch.M0())*(*normal)();

				// Transform back to unit interval
    ch.Wght1(i) = 1.0/(1.0 + exp(-z));

    sum += ch.Wght1(i);
  }
  for (unsigned i=0; i<ch.M0(); i++) ch.Wght1(i) /= sum;


  // Model parameters
  //
  for (unsigned j=0; j<_si->Ndim; j++) {
    for (unsigned i=0; i<ch.M0(); i++) {
      ch.Phi1(i, j) = (*_trans[j])(ch.Phi0(i, j),
				   ch.factor*w->widthM(ch.M0(), j));
    }
  }

  // Extended parameters
  //
  for (unsigned j=0; j<_si->Next; j++) {
    ch.Ext1(j) = (*_trans[_si->Ndim+j])(ch.Ext0(j), ch.factor*w->width(j));
  }
    
  // Sanity check
  //
  for (unsigned i=0; i<ch.M0(); i++) {
    for (unsigned j=0; j<_si->Ndim; j++) {
      if (std::isnan(ch.Phi1(i, j))) {
	cerr << "NaN in MixturePrior [after]: imix=" << i 
	     << " idim=" << j << endl;
      }
    }
  }
  for (unsigned j=0; j<_si->Next; j++) {
    if (std::isnan(ch.Ext1(j))) {
      cerr << "NaN in MixturePrior [after]: iext=" << j << endl; 
    }
  }
  
}

// GAMMA GENERATOR.  Generates a positive real number, r, with density
// proportional to r^(a-1) * exp(-r).  See Devroye, p. 410 and p. 420.


double MixturePrior::GenGamma(double a)
{
  double b, c, X, Y, Z, U, V, W;

  if (a<0.00001) X = a;

  else if (a<=1) {
    U = (*useg)();
    X = GenGamma(1.0+a) * pow(U, 1.0/a);
  }

  else if (a<1.00001) X = -log(max<double>((*useg)(), min_prob));

  else {
    b = a - 1.0;
    c = 3.0*a - 0.75;
    
    for (;;)
    {
      U = (*useg)();
      V = (*useg)();

      W = U*(1.0-U);
      Y = sqrt(c/W) * (U-0.5);
      X = b + Y;

      if (X >= 0.0)
      {
        Z = 64.0*W*W*W*V*V;

        if (Z <= 1 - 2.0*Y*Y/X || log(Z) <= 2.0 * (b*log(X/b) - Y)) break;
      }
    }
  }

  return X<min_prob && X<a ? (a<min_prob ? a : min_prob) : X;
}

// BETA GENERATOR. Generates a real number, r, in (0,1), with density
// proportional to r^(a-1) * (1-r)^(b-1).

double MixturePrior::GenBeta(double a, double b)
{
  double x, y, r;

  do
  { x = GenGamma(a);
    y = GenGamma(b);
    r = 1.0 + x/(x+y);
    r = r - 1.0;
  } while (r<=0.0 || r>=1.0);

  return r;
}

double MixturePrior::BirthWeightPDF(unsigned M, double p)
{
  double a = Alpha;
  double b = Alpha*M;

  return exp(lgamma(a+b) - lgamma(a) - lgamma(b)) *
    pow(p, a - 1.0) * pow(1.0 - p, b - 1.0);
}

double MixturePrior::BirthWeight(unsigned M)
{
  return GenBeta(Alpha, Alpha*M);
}


void MixturePrior::SamplePrior(State *s)
{
  switch (s->Type()) {
  case StateInfo::None:
  case StateInfo::Block:
    {
      string msg = "You are calling a MixturePrior for a non-mixture state. "
	"I don't know what to do!";
      throw PriorException(msg.c_str(), __FILE__, __LINE__);
    }
  case StateInfo::Mixture:
    {
      vector<double> p = mix[s->M()-1]->Sample();
				// Impose limits
      double sum = 0.0;
      for (unsigned m=0; m<s->M(); m++)	{
	s->Wght(m) = p[m];
	sum += s->Wght(m);
	vector<double> v = priorSampleMix();
	for (unsigned j=0; j<s->Ndim(); j++) s->Phi(m, j) = v[j];
      }
      for (unsigned m=0; m<s->M(); m++)	s->Wght(m) /= sum;
    }
  case StateInfo::Extended:
    {
      vector<double> v = priorSampleExt();
      for (unsigned j=0; j<s->Next(); j++) s->Ext(j) = v[j];
    }
    break;
  default:
    throw PriorException("State type error", __FILE__, __LINE__); 
  }
}

vector<double> MixturePrior::priorSampleMix()
{
  vector<double> ans, v;
  for (unsigned j=0; j<_prior0.size(); j++) {
    v = _prior0[j]->Sample();
    ans.insert(ans.end(), v.begin(), v.end());
  }
  return ans;
}

vector<double> MixturePrior::priorSampleExt()
{
  vector<double> ans, v;
  for (unsigned j=0; j<_prior1.size(); j++) {
    v = _prior1[j]->Sample();
    ans.insert(ans.end(), v.begin(), v.end());
  }
  return ans;
}
