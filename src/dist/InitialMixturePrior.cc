#include <iostream>
#include <fstream>

using namespace std;

#include <InitialMixturePrior.h>
#include <Dirichlet.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::InitialMixturePrior)

using namespace BIE;

InitialMixturePrior::InitialMixturePrior()
{
}


InitialMixturePrior::InitialMixturePrior(StateInfo *si, double alpha, 
					 clivectordist *prior) : 
  MixturePrior(si, alpha, prior)
{
  ttwght = State(_si);
}

InitialMixturePrior::InitialMixturePrior(StateInfo *si, double alpha, 
					 std::vector<Distribution*> prior) : 
  MixturePrior(si, alpha, prior)
{
  ttwght = State(_si);
}


InitialMixturePrior::InitialMixturePrior
(StateInfo *si, double alpha, double pmean, 
 clivectordist *prior0, clivectordist *prior1) : 
  MixturePrior(si, alpha, pmean, prior0, prior1)
{
  ttwght = State(_si);
}

InitialMixturePrior::InitialMixturePrior
(StateInfo *si, double alpha, double pmean, 
 std::vector<Distribution*> prior0, std::vector<Distribution*> prior1) : 
  MixturePrior(si, alpha, pmean, prior0, prior1)
{
  ttwght = State(_si);
}


State InitialMixturePrior::Sample(unsigned m)
{
  ret.Reset(m);

  p = mix[m-1]->Sample();
  for (unsigned i=0; i<m; i++) ret.Wght(i) = p[i];

  for (unsigned i=0; i<m; i++) {
    vector<double> v = priorSampleMix();
    for (unsigned j=0; j<_si->Ndim; j++) ret.Phi(i, j) = v[j];
  }

  vector<double> v = priorSampleExt();
  for (unsigned j=0; j<_si->Next; j++) ret.Ext(j) = v[j];

  return ret;
}


void InitialMixturePrior::SamplePrior(State *s)
{
  unsigned M = s->M();

  // M must between 1 and Nmix
  if (pois) s->Reset((M=SampleM()));
  else      s->Reset(max<unsigned>(min<unsigned>(M, _si->M), 1));

  vector<double> pp = mix[M-1]->Sample();
  double sum = 0.0;
  for (unsigned i=0; i<M; i++) {
    s->Wght(i) = pp[i];
    sum += s->Wght(i);
    vector<double> v = priorSampleMix();
    for (unsigned j=0; j<s->Ndim(); j++) s->Phi(i, j) = v[j];
  }
  for (unsigned i=0; i<M; i++) s->Wght(i) /= sum;

  vector<double> v = priorSampleExt();
  for (unsigned j=0; j<s->Next(); j++) s->Ext(j) = v[j];
}


void InitialMixturePrior::SamplePrior(unsigned M, unsigned n, vector<double>& V)
{
  V = priorSampleMix();
}


InitialMixturePrior* InitialMixturePrior::New()
{
  InitialMixturePrior* p = (InitialMixturePrior*)this->Prior::New();
  p->_si  = _si;
  for (unsigned i=0; i<_si->M; i++) {
    vector<double> A(i+1, Alpha/_si->M);
    p->mix.push_back(DirichletPtr(new Dirichlet(&A)));
  }

  return p;
};
    

double InitialMixturePrior::PDF(State& p)
{
  for (unsigned i=0; i<p.M(); i++) ttwght[i] = p[i];
  
  double ans = mix[p.M()-1]->PDF(ttwght);
  for (unsigned i=0; i<p.M(); i++) {
    vector<double> v;
    for (unsigned j=0; j<p.Ndim(); j++) v.push_back(p.Phi(i, j));
    for (unsigned k=0; k<_prior0.size(); k++) {
      vector<double> v1(v.begin()+_pos0[k], 
			v.begin()+_pos0[k]+_len0[k]);
      _sta0[k].setState(v1);
      ans *= _prior0[k]->PDF(_sta0[k]);
    }
  }

  vector<double> v;
  for (unsigned j=0; j<p.Next(); j++) v.push_back(p.Ext(j));
  for (unsigned k=0; k<_prior1.size(); k++) {
    vector<double> v1(v.begin()+_pos1[k], 
		      v.begin()+_pos1[k]+_len1[k]);
    _sta1[k].setState(v1);
    ans *= _prior1[k]->PDF(_sta1[k]);
  }

  return ans;
}


double InitialMixturePrior::logPDF(State& p)
{
  for (unsigned i=0; i<p.M(); i++) ttwght[i] = p[i];
  
  double ans = mix[p.M()-1]->logPDF(ttwght);
  for (unsigned i=0; i<p.M(); i++) {
    vector<double> v;
    for (unsigned j=0; j<p.Ndim(); j++) v.push_back(p.Phi(i, j));
    for (unsigned k=0; k<_prior0.size(); k++) {
      vector<double> v1(v.begin()+_pos0[k], 
			v.begin()+_pos0[k]+_len0[k]);
      _sta0[k].setState(v1);
      ans += _prior0[k]->logPDF(_sta0[k]);
    }
  }

  vector<double> v;
  for (unsigned j=0; j<p.Next(); j++) v.push_back(p.Ext(j));
  for (unsigned k=0; k<_prior1.size(); k++) {
    vector<double> v1(v.begin()+_pos1[k], 
		      v.begin()+_pos1[k]+_len1[k]);
    _sta1[k].setState(v1);
    ans *= _prior1[k]->logPDF(_sta1[k]);
  }

  return ans;
}


double InitialMixturePrior::logPDFMarginal(unsigned M, unsigned n, 
					   const vector<double>& V)
{
  double ans = 0.0;
  for (unsigned j=0; j<_prior0.size(); j++) {
    vector<double> v1(V.begin()+_pos0[j],
		      V.begin()+_pos0[j] + _len0[j]);
    _sta0[j].setState(v1);
    ans += _prior0[j]->logPDF(_sta0[j]);
  }

  return ans;
}


vector<double> InitialMixturePrior::lower(void)
{
  vector<double> ret(_si->Ntot);
  for (unsigned m=0; m<_si->M; m++) {
    ret[m] = mix[m]->lower()[0];
    for (unsigned j=0; j<_prior0.size(); j++) {
      std::vector<double> v = _prior0[j]->lower();
      std::copy(v.begin(), v.end(), ret.begin()+_si->M+_si->Ndim*m+j);
    }
  }

  for (unsigned j=0; j<_prior1.size(); j++)  {
    std::vector<double> v = _prior1[j]->lower();
    std::copy(v.begin(), v.end(), ret.begin()+_si->M*(1+_si->Ndim)+j);
  }
  
  return ret;
}


vector<double> InitialMixturePrior::upper(void)
{
  vector<double> ret(_si->Ntot);
  for (unsigned m=0; m<_si->M; m++) {
    ret[m] = mix[m]->upper()[0];
    for (unsigned j=0; j<_prior0.size(); j++) {
      std::vector<double> v = _prior0[j]->upper();
      std::copy(v.begin(), v.end(), ret.begin()+_si->M+_si->Ndim*m+j);
    }      
  }

  for (unsigned j=0; j<_prior1.size(); j++) {
    std::vector<double> v = _prior1[j]->upper();
    std::copy(v.begin(), v.end(), ret.begin()+_si->M*(1+_si->Ndim)+j);
  }  

  return ret;
}

    
vector<double> InitialMixturePrior::Mean(unsigned m)
{
  vector<double> ret(_si->Ntot);
  for (unsigned m=0; m<_si->M; m++) {
    ret[m] = mix[m]->Mean()[0];
    for (unsigned j=0; j<_prior0.size(); j++) {
      std::vector<double> v = _prior0[j]->Mean();
      std::copy(v.begin(), v.end(), ret.begin()+_si->M+_si->Ndim*m);
    }
  }

  for (unsigned j=0; j<_prior1.size(); j++) {
    std::vector<double> v = _prior1[j]->Mean();
    std::copy(v.begin(), v.end(), ret.begin()+_si->M*(1+_si->Ndim));
  }  
  return ret;
}


vector<double> InitialMixturePrior::StdDev(unsigned m)
{
  vector<double> ret(_si->Ntot);
  for (unsigned m=0; m<_si->M; m++) {
    ret[m] = mix[m]->StdDev()[0];
    for (unsigned j=0; j<_prior0.size(); j++) {
      std::vector<double> v = _prior0[j]->StdDev();
      std::copy(v.begin(), v.end(), ret.begin()+_si->M+_si->Ndim*m);
    }
  }

  for (unsigned j=0; j<_prior1.size(); j++) {
    std::vector<double> v = _prior1[j]->StdDev();
    std::copy(v.begin(), v.end(), ret.begin()+_si->M*(1+_si->Ndim));
  }  

  return ret;
}


vector<double> InitialMixturePrior::Moments(unsigned m, unsigned n)
{
  vector<double> ret(_si->Ntot);
  for (unsigned m=0; m<_si->M; m++) {
    ret[m] = mix[m]->Moments(n)[0];
    for (unsigned j=0; j<_prior0.size(); j++) {
      std::vector<double> v = _prior0[j]->Moments(n);
      std::copy(v.begin(), v.end(), ret.begin()+_si->M+_si->Ndim*m);
    }
  }
  
  for (unsigned j=0; j<_prior1.size(); j++) {
    std::vector<double> v = _prior1[j]->Moments(n);
    std::copy(v.begin(), v.end(), ret.begin()+_si->M*(1+_si->Ndim));
  }
  
  return ret;
}
