#include <CauchyDist.h>
#include <ACG.h>
#include <Cauchy.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::CauchyDist)

using namespace BIE;

CauchyDist::CauchyDist()
{
  _type  = NormalAdd;
  center = 0.0;
  scale  = 1.0;
  vmin   = -DBL_MAX;
  vmax   =  DBL_MAX; 
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = 1.0;
}

CauchyDist::CauchyDist
(double m, double v)
{
  _type  = NormalAdd;
  center = m;
  scale  = v;
  vmin   = -DBL_MAX;
  vmax   =  DBL_MAX;
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = 1.0;
  
  cauchy = CauchyPtr(new Cauchy(center, scale, BIEgen));
}

CauchyDist::CauchyDist
(double m, double v, double minv, double maxv) 
{
  _type  = NormalAdd;
  center = m;
  scale  = v;
  vmin   = minv;
  vmax   = maxv;
  mincdf = atan( (minv - center)/scale )/M_PI + 0.5;
  maxcdf = atan( (maxv - center)/scale )/M_PI + 0.5;
  norm   = maxcdf - mincdf;
  
  cauchy = CauchyPtr(new Cauchy(center, scale, BIEgen));
}

CauchyDist::CauchyDist
(double v)
{
  _type  = NormalAdd;
  center = 0.0;
  scale  = v;
  vmin   = -DBL_MAX;
  vmax   =  DBL_MAX;
  mincdf = 0.0;
  maxcdf = 1.0;
  norm   = 1.0;
  
  cauchy = CauchyPtr(new Cauchy(center, scale, BIEgen));
}

CauchyDist::CauchyDist
(double v, double minv, double maxv) 
{
  _type  = NormalAdd;
  center = 0.0;
  scale  = v;
  vmin   = minv;
  vmax   = maxv;
  mincdf = atan( (minv - center)/scale )/M_PI + 0.5;
  maxcdf = atan( (maxv - center)/scale )/M_PI + 0.5;
  norm   = maxcdf - mincdf;
  
  cauchy = CauchyPtr(new Cauchy(center, scale, BIEgen));
}

Distribution* CauchyDist::New() 
{
  return new CauchyDist(center, scale, vmin, vmax);
}
  
double CauchyDist::PDF(State& x) 
{
  if (x[0]>=vmax || x[0]<=vmin)
    return 0.0;
  else {
    double fac = (x[0] - center)/scale;
    return M_PI*scale*(1.0 + fac*fac)/norm;
  }
}

double CauchyDist::logPDF(State& x) 
{
  if (x[0]>vmax || x[0]<vmin)
    throw ImpossibleStateException(__FILE__, __LINE__);
  else 
    return log(PDF(x));
}

vector<double> CauchyDist::lower(void)
{ return vector<double>(1, vmin); }

vector<double> CauchyDist::upper(void)
{ return vector<double>(1, vmax); }

vector<double> CauchyDist::Mean(void)
{ return vector<double>(1, center); }

vector<double> CauchyDist::StdDev(void)
{ return vector<double>(1, DBL_MAX); }

double CauchyDist::CDF(State& x) {
  double ans;
  if (x[0]<vmin)
    ans = 0.0;
  else if (x[0]>vmax)
    ans = 1.0;
  else
    ans = atan( (x[0] - center)/scale )/M_PI + 0.5 - mincdf;
  return ans;
}

vector<double> CauchyDist::Moments(int i) {
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = center;
    break;
  case 2:
    ans = DBL_MAX;
    break;
  default:
    cerr << "CauchyDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State CauchyDist::Sample(void) 
{
  const int itmax=10000;
  int i=0;
  double x;
  while (i++<itmax) {
    x = (*cauchy)();
    if (x<=vmax && x>=vmin) break;
  }
  if (i==itmax) cerr << "CauchyDist: sampler did not converge in "
		     << itmax << " iterations" << endl;
  return State(vector<double>(1, x));
}

