#include <cfloat>

#include <ErfDist.h>
#include <ACG.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::ErfDist)

using namespace BIE;

ErfDist::ErfDist()
{
  _type = NormalAdd;

  a     = 0.0;
  b     = 1.0;
  c     = -DBL_MAX;
  d     =  DBL_MAX; 

  generate();
}

ErfDist::ErfDist
(double offset, double width)
{
  _type = NormalAdd;

  a  = offset;
  b  = width;
  c  = -DBL_MAX;
  d  =  DBL_MAX;

  generate();
}

ErfDist::ErfDist
(double offset, double width, double lower, double upper)
{
  _type = NormalAdd;

  a  = offset;
  b  = width;
  c  = lower;
  d  = upper;

  generate();
}

Distribution* ErfDist::New() 
{
  return new ErfDist(a, b, c, d);
}
  
void ErfDist::generate()
{
  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));

  double bb = fabs(b);
  if (c < a - bb*Nwid) c = a - bb*Nwid;
  if (d > a + bb*Nwid) d = a + bb*Nwid;

  _gmin = _g_erf(c, a, b);
  _gmax = _g_erf(d, a, b);
  _norm = 1.0/(_gmax - _gmin);

  unsigned N = std::floor( Nspc*(d - c)/bb ) + 1;
  _dv = (d - c)/N;

  double sum = 0.0;
  _x.clear();
  _p.clear();
  _mean = _var = 0.0;

  _x.push_back(c);
  for (unsigned i=1; i<=N; i++) {
    double x = c + _dv*i;
    double y = _f_erf(x, a, b);

    sum   += y;
    _mean += y * x;
    _var  += y * x*x;

    _x.push_back(x);
    _p.push_back(sum);
  }

  if (sum>0.0) {
    for (auto & v : _p) v /= sum;
    _mean /= sum;
    _var  /= sum;
    _var  -= _mean*_mean;
  }
}

double ErfDist::PDF(State& x) 
{
  if (x[0]<=c || x[0]>=d)
    return 0.0;
  else
    return _norm * _f_erf(x[0], a, b);
}

double ErfDist::logPDF(State& x) 
{
  if (x[0]>d || x[0]<c)
    throw ImpossibleStateException(__FILE__, __LINE__);
  else {
    double P = _norm * _f_erf(x[0], a, b);
    if (P>0.0) return log(P);	// Check for underflow tails
    else throw ImpossibleStateException(__FILE__, __LINE__);
  }
}

vector<double> ErfDist::lower(void)
{ return vector<double>(1, c); }

vector<double> ErfDist::upper(void)
{ return vector<double>(1, d); }

vector<double> ErfDist::Mean(void)
{ return vector<double>(1, _mean); }

vector<double> ErfDist::StdDev(void)
{ return vector<double>(1, sqrt(_var)); }

double ErfDist::CDF(State& x)
{
  double ans;
  if (x[0]<=c)
    ans = 0.0;
  else if (x[0]>=d)
    ans = 1.0;
  else
    ans = _norm * (_g_erf(x[0], a, b) - _gmin);

  return ans;
}

vector<double> ErfDist::Moments(int i) 
{
  double ans;

  switch(i) {
  case 0:
    ans = 1.0;
    break;
  case 1:
    ans = _mean;
    break;
  case 2:
    ans = _var + _mean*_mean;
    break;
  default:
    cerr << "ErfDist: moments greater than 2 not implemented" << endl;
    exit(-1);
  }

  return vector<double>(1, ans);
}

State ErfDist::Sample(void) 
{
  double x = 0.0;
  while (x<=0.0) x = (*unit)();

  auto it = std::lower_bound(_p.begin(), _p.end(), x);
  if (it == _p.end()) return State(std::vector<double>(1, d));

  size_t hi = std::distance(_p.begin(), it);
  size_t lo = hi;

  if (lo == 0) hi++;
  else         lo--;

  double denom = _p[hi] - _p[lo];
  if (denom <= 0.0) return State(vector<double>(1, *it));

  double A = (_p[hi] - x)/denom;
  double B = (x - _p[lo])/denom;

  return State(vector<double>(1, A*_x[lo] + B*_x[hi]));
}

