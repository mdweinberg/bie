// -*- C++ -*-

#include <cmath>
#include <MultiDSplat.h>
#include <Distribution.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MultiDSplat)

using namespace BIE;

double MultiDSplat::SIG = 0.1;

/*
  Splats can either be defined by location or variance
*/
MultiDSplat::MultiDSplat(int ndim, int nmix, clivectord* Var, bool width)
{
  Ndim = ndim;
  M    = nmix;
  Mcur = nmix;
  Ntot = ndim;

  for (int n=0; n<ndim; n++) {
    ostringstream sout;
    sout << "Pos(" << n+1 << ")";
    params.push_back(sout.str());
  }
  
  if (width) {
    params.push_back("Var");
    Ntot++;
  }

  parametertype = createMixParameterType(params, Ntot, M);

  wt = vector<double>(M);
  pos = vector< vector<double> >(M);
  for (int m=0; m<M; m++) pos[m] = vector<double>(Ndim);
  if (Var)
    var = (*Var)();
  else
    var = vector<double>(M, SIG*SIG);
}

MultiDSplat::MultiDSplat(int ndim, int nmix, std::vector<double> Var, bool width)
{
  Ndim = ndim;
  M    = nmix;
  Mcur = nmix;
  Ntot = ndim;

  for (int n=0; n<ndim; n++) {
    ostringstream sout;
    sout << "Pos(" << n+1 << ")";
    params.push_back(sout.str());
  }
  
  if (width) {
    params.push_back("Var");
    Ntot++;
  }

  parametertype = createMixParameterType(params, Ntot, M);

  wt = vector<double>(M);
  pos = vector< vector<double> >(M);
  for (int m=0; m<M; m++) pos[m] = vector<double>(Ndim);
  if (Var.size())
    var = Var;
  else
    var = vector<double>(M, SIG*SIG);
}

vector<string> MultiDSplat::ParameterLabels()
{
  vector<string> ret;
  for (int j=0; j<Ntot; j++) ret.push_back(params[j]);
  return ret;
}

void MultiDSplat::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) {
    wt[k] = s.Wght(k);
    for (int j=0; j<Ndim; j++) pos[k][j] = s.Phi(k, j);
    if (Ntot>Ndim) var[k] = s.Phi(k, Ndim);
  }
}

void MultiDSplat::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    for (int j=0; j<Ndim; j++) pos[k][j] = p[k][j];
    if (Ntot>Ndim) var[k] = p[k][Ndim];
  }
}


vector<double> MultiDSplat::EvaluatePoint(double x, double y, 
					  PointDistribution *d)
{
  double r2, dif;

  int dim = d->Point().size();
  int ndt = d->numberData();
  vector<double> ret(ndt, 1.0);

  for (int n=0; n<ndt; n++) {
    
    ret[n] = 0.0;

    for (int k=0; k<Mcur; k++) {
      r2 = 0.0;
      for (int j=0; j<dim; j++) {
	dif = pos[k][j] - d->Point()[j];
	r2 += dif*dif;
      }
      
      ret[n] += wt[k] * 
	exp(-0.5*r2/var[k] - 0.5*dim*log(2.0*M_PI*var[k]));
    }
  }
  
  return ret;
}

