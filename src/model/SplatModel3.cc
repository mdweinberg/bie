// This may look like C code, but it is really -*- C++ -*-

#include <cmath>
#include <SplatModel3.h>
#include <Distribution.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SplatModel3)

using namespace BIE;

double SplatModel3::SIGX = 0.1;
double SplatModel3::SIGY = 0.1;
double SplatModel3::A1   = 5.0;
double SplatModel3::A2   = 5.0;
double SplatModel3::A3   = 5.0;
double SplatModel3::V1   = 1.0;
double SplatModel3::V2   = 1.0;
double SplatModel3::V3   = 1.0;

// Constants relating to the record type of the model.
const char* SplatModel3::XPOS_FIELDNAME    = "X pos";
const char* SplatModel3::YPOS_FIELDNAME    = "Y pos";
const char* SplatModel3::BANDONE_FIELDNAME = "Band One";
const char* SplatModel3::BANDTWO_FIELDNAME = "Band Two";
const char * SplatModel3::PARAM_NAMES []   =
  {XPOS_FIELDNAME,YPOS_FIELDNAME,BANDONE_FIELDNAME,BANDTWO_FIELDNAME};

/*
  Splats can either be defined by location (ndim=2) or location
  and dispersion (ndim=4)
*/
SplatModel3::SplatModel3(int ndim, int mdim)
{
  // ndim must be 2 or 4.
  if (!(ndim == 2 || ndim == 4)) 
  { 
    throw BIEException("SplatModel3 Exception",
     "Splats can either be defined by location (ndim=2) or location and dispersion (ndim=4)",
     __FILE__, __LINE__);
  }

  // If N is two, use location params only, if four use dispersion params also.
  parametertype = createMixParameterType(PARAM_NAMES, ndim, mdim);
  
  Ndim = ndim;
  M = mdim;
  Mcur = M;
  
  wt = vector<double>(M);
  xx = vector<double>(M);
  yy = vector<double>(M);
  sx = vector<double>(M);
  sy = vector<double>(M);

				// Variance for 2-dimensional case
  if (Ndim == 2) {
    for (int m=0; m<M; m++) {
      sx[m] = SIGX*SIGX;
      sy[m] = SIGY*SIGY;
    }
  }

}


vector<string> SplatModel3::ParameterLabels()
{
  vector<string> ret;
  for (int j=0; j<Ndim; j++) ret.push_back(PARAM_NAMES[j]);
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void SplatModel3::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    xx[k] = s.Phi(k, 0);
    yy[k] = s.Phi(k, 1);
    if (Ndim==4) {
      sx[k] = s.Phi(k, 2);
      sy[k] = s.Phi(k, 3);
    }
  }
}

void SplatModel3::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    xx[k] = p[k][0];
    yy[k] = p[k][1];
    if (Ndim==4) {
      sx[k] = p[k][2];
      sy[k] = p[k][3];
    }
  }
}

double SplatModel3::NormEval(double xmin, double xmax, 
			     double ymin, double ymax)
{
  double ans = 0.0;

  for (int k=0; k<Mcur; k++) {

    ans += wt[k] * 0.25 *
      ( erf( (xmax - xx[k])/sqrt(2.0*sx[k]) ) -
	erf( (xmin - xx[k])/sqrt(2.0*sx[k]) ) ) *
      ( erf( (ymax - xx[k])/sqrt(2.0*sy[k]) ) -
	erf( (ymin - xx[k])/sqrt(2.0*sy[k]) ) ) ;
  }
	
  return ans;
}


double SplatModel3::NormEval(double x, double y, SampleDistribution *d)
{
  double ans = 0.0;

  for (int k=0; k<Mcur; k++)
    ans += wt[k] * exp(
		       -0.5*(x-xx[k])*(x-xx[k])/sx[k]
		       -0.5*(y-yy[k])*(y-yy[k])/sy[k]
		       ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));

  return ans;
}


vector<double> SplatModel3::EvaluateBinned(double x, double y, 
					   BinnedDistribution *d)
{
  vector<double> ret(d->numberData(), 0.0);
  double z = 0.0;

  for (int k=0; k<Mcur; k++) {
    z += wt[k] * exp(
		     -0.5*(x-xx[k])*(x-xx[k])/sx[k]
		     -0.5*(y-yy[k])*(y-yy[k])/sy[k]
		     ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
  }
    
				// Compute contribution to
				// distribution in each dimension

  for (int j=0; j<d->numberData(); j++) {
    ret[j] = z *
	
      0.5*( erf((d->getHigh(j)[0] - A1)/sqrt(2.0*V1)) + 
	    erf((A1 -  d->getLow(j)[0])/sqrt(2.0*V1)) ) *
      0.5*( erf((d->getHigh(j)[1] - A2)/sqrt(2.0*V2)) +
	    erf((A2 -  d->getLow(j)[1])/sqrt(2.0*V2)) ) *
      0.5*( erf((d->getHigh(j)[2] - A3)/sqrt(2.0*V3)) +
	    erf((A3 -  d->getLow(j)[2])/sqrt(2.0*V3)) ) ;
  }
  
  return ret;
}

vector<double> SplatModel3::EvaluatePoint(double x, double y, 
					  PointDistribution *d)
{
  vector<double> ret(d->numberData(), 0.0);
  double z = 0.0;

  for (int k=0; k<Mcur; k++) {
    z += wt[k] * exp(
		     -0.5*(x-xx[k])*(x-xx[k])/sx[k]
		     -0.5*(y-yy[k])*(y-yy[k])/sy[k]
		     ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
  }
    
				// Compute contribution to
				// distribution in each dimension

  if (d->numberData())
    ret[0] = z * 
      exp(
	  -(d->Point()[0] - A1)*(d->Point()[0] - A1)/(2.0*V1)
	  -(d->Point()[1] - A2)*(d->Point()[1] - A2)/(2.0*V2)
	  -(d->Point()[2] - A3)*(d->Point()[2] - A3)/(2.0*V3)
	  ) / sqrt(2.480502134423985e+02*V1*V2*V3);
  
  return ret;
}

