// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

#include <GalaxyModelTwoD.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GalaxyModelTwoD)

using namespace BIE;

double GalaxyModelTwoD::A1        = 20.0;
double GalaxyModelTwoD::Z1        = 100.0;
int    GalaxyModelTwoD::NUM       = 200;
double GalaxyModelTwoD::ALPHA     = 0.0;
double GalaxyModelTwoD::BETA      = 0.0;
double GalaxyModelTwoD::R0        = 8.0;
double GalaxyModelTwoD::RMAX      = 100.0;
double GalaxyModelTwoD::AK        = 0.1;
double GalaxyModelTwoD::K0        =-4.0;
double GalaxyModelTwoD::AMIN      = 0.5;
double GalaxyModelTwoD::AMAX      = 8.0;
double GalaxyModelTwoD::HMIN      = 100.0;
double GalaxyModelTwoD::HMAX      = 1200.0;
double GalaxyModelTwoD::LMAG      = 6.0;
double GalaxyModelTwoD::HMAG      = 16.0;
double GalaxyModelTwoD::Log10     = log(10.0);
double GalaxyModelTwoD::onedeg    = M_PI/180.0;
string GalaxyModelTwoD::BASISDATA = "basisdata.2d";

const char* GalaxyModelTwoD::LENGTH_FIELDNAME       = "Length";
const char* GalaxyModelTwoD::HEIGHT_FIELDNAME       = "Height";
const char* GalaxyModelTwoD::PARAM_NAMES[] = {LENGTH_FIELDNAME,HEIGHT_FIELDNAME};

GalaxyModelTwoD::GalaxyModelTwoD(int ndim, int mdim, SampleDistribution* _dist)
{
  Ndim = ndim;
  M    = mdim;
  Mcur = M;

  // Sanity check
  if (_dist->getdim(0) != 2)
  { 
    throw BIEException("GalaxyModelTwoD Exception", 
          "BinnedDistribution is not two dimensional", __FILE__, __LINE__); 
  }

  // Create record type describing parameters.
  parametertype = createMixParameterType(PARAM_NAMES, Ndim, mdim);

  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

  if (dynamic_cast <BinnedDistribution*> (_dist)) {
    
    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
    


				// Cache the bin boundaries for the data
    nbins = histo->numberData(); // histogram
    for (int i=0; i<nbins; i++) {
      lowb1.push_back(histo->getLow(i)[0]);
      highb1.push_back(histo->getHigh(i)[0]);
      lowb2.push_back(histo->getLow(i)[1]);
      highb2.push_back(histo->getHigh(i)[1]);
    }
  
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    PointDistribution *_point = (PointDistribution*)_dist;
    type = point;

    nbins = 1;
    flux = _point->Point();
  } else if (dynamic_cast <NullDistribution*> (_dist)) {
    throw InappropriateDistributionException("NullDistribution", 
					     __FILE__, __LINE__);
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);


  // Read in parameters
  ifstream in(BASISDATA.c_str());
  if (!in) { throw FileOpenException(BASISDATA, errno, __FILE__, __LINE__); }

  const int linesize = 512;
  char line[linesize];
  
  in.getline(line, linesize);
  string lineBuf(line);
  istringstream ins(lineBuf);
  ins >> nparam;
  for (int i=0; i<nparam; i++) {
    double xx, yy, sxx, syy, ww;
    in.getline(line, linesize);
    string lineBuf(line);
    istringstream ins(lineBuf);
    ins >> xx;		x.push_back(xx);
    ins >> yy;		y.push_back(yy);
    ins >> sxx;		sx.push_back(sxx);
    ins >> syy;		sy.push_back(syy);
    ins >> ww;		w.push_back(ww);
  }
				// Default line-of-sight integration knots
				// 
  intgr = new JacoQuad(NUM, ALPHA, BETA);

  current = 0;
}

GalaxyModelTwoD::~GalaxyModelTwoD()
{
  delete intgr;

				// Delete the cache
				// 
  for (mit=cache.begin(); mit != cache.end(); mit++) 
    delete mit->second;
}

				// Model: exponential * sech^2 disk

vector<string> GalaxyModelTwoD::ParameterLabels()
{
  vector<string> ret;
  ret.push_back("Length");
  ret.push_back("Height");
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void GalaxyModelTwoD::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s.Phi(k, j);
  }

  check_bounds();
}

void GalaxyModelTwoD::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}

void GalaxyModelTwoD::check_bounds()
{
  good_bounds = true;
				// Check bounds
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;
  }
  for (int k=0; k<Mcur; k++) {
    if (pt[k][0] > AMAX || pt[k][0] < AMIN   ||
	pt[k][1] > HMAX || pt[k][1] < HMIN    )  good_bounds = false;
  }

				// DEBUG
  if (good_bounds == false) {
    cout << "State rejected (" << Mcur << "):\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<2; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
}

void GalaxyModelTwoD::generate(double L, double B, SampleDistribution *d)
{

  double s, R, z, fac, mm1, mm2;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

				// Extinction slab
  double smaxr, smaxz, smax;
  if (fabs(cosB)<1.0e-8) 
    smaxr = 1.0e30;
  else
    smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));

  if (fabs(sinB)<1.0e-8) 
    smaxz = 1.0e30;
  else
    smaxz = Z1*1.0e-3/fabs(sinB);

  smax = min<double>(smaxr, smaxz);

  current->work = vector<rvector>(NUM);
  current->R    = vector<real>(NUM);
  current->z    = vector<real>(NUM);
  current->fac  = vector<real>(NUM);

				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    s = intgr->knot(n) * RMAX;
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;
    fac = s*s*intgr->weight(n) * RMAX;

    current->work[n-1] = vector<real>(nbins, 0.0);
    current->R[n-1]    = R;
    current->z[n-1]    = z;
    current->fac[n-1]  = fac;

    for (int k=0; k<nparam; k++) {
      mm1 = x[k] + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);
      mm2 = y[k] + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

      switch (type) {

      case binned:
			
				// Bin visibility
	for (int j=0; j<nbins; j++) {

	  current->work[n-1][j] +=
	    0.25*w[k]*
	    (
	     erf( (highb1[j] - mm1)/(M_SQRT2*sx[k]) )
	   -
	     erf( ( lowb1[j] - mm1)/(M_SQRT2*sx[k]) )
	     )
	    *
	    (
	     erf( (highb2[j] - mm2)/(M_SQRT2*sy[k]) )
	     -
	     erf( ( lowb2[j] - mm2)/(M_SQRT2*sy[k]) )
	     )
	    ;
	}

	break;
	
      case point:

	current->work[n-1][0] +=
	  exp(-(((PointDistribution*)d)->Point()[0] - mm1)*
	       (((PointDistribution*)d)->Point()[0] - mm1)/(2.0*sx[k])) /
	  sqrt(2.0*M_PI*sx[k]*sx[k])
	  *
	  exp(-(flux[1] - mm2)*(flux[1] - mm2)/(2.0*sy[k])) /
	  sqrt(2.0*M_PI*sy[k]*sy[k]);

	break;

      }

    }

  }

}

void GalaxyModelTwoD::compute_bins()
{
  double sech, R, z, fac, dens;
				// Zero out theoretical bins
  current->bins = vector<double>(nbins, 0.0);

  for (int n=1; n<=NUM; n++) {
				// Coordinates
    fac = current->fac[n-1];
    R = current->R[n-1];
    z = current->z[n-1];
				// Loop over each components
    for (int k=0; k<Mcur; k++) {
				// Disk model
      sech = 2.0/(exp(z*1.0e3/pt[k][1]) + exp(-z*1.0e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	current->bins[j] += wt[k]*fac*dens*
	  current->work[n-1][j];
    }
  }

}


double GalaxyModelTwoD::NormEval(double L, double B)
{
  double s, R, z, fac, mm1, mm2;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

				// Extinction slab
  double smaxr, smaxz, smax;
  if (fabs(cosB)<1.0e-8) 
    smaxr = 1.0e30;
  else
    smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));

  if (fabs(sinB)<1.0e-8) 
    smaxz = 1.0e30;
  else
    smaxz = Z1*1.0e-3/fabs(sinB);

  smax = min<double>(smaxr, smaxz);


  double sech, dens, sum1, sum0 = 0.0;
				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    s = intgr->knot(n) * RMAX;
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;
    fac = s*s*intgr->weight(n) * RMAX;

    sum1 = 0.0;

    //
    // Basis components
    //
    for (int k=0; k<nparam; k++) {
				// Make fluxes for each component
				// 
      mm1 = x[k] + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);
      mm2 = y[k] + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

      // Visibility
      //
      double val = 0.25 *
	  (
	   erf( (HMAG - mm1)/(M_SQRT2*sx[k]) )
	   -
	   erf( (LMAG - mm1)/(M_SQRT2*sx[k]) )
	   ) *
	  (
	   erf( (HMAG - mm2)/(M_SQRT2*sy[k]) )
	   -
	   erf( (LMAG - mm2)/(M_SQRT2*sy[k]) )
	   ) ;

      sum1 += w[k]*val;
    }
	
				// Disk model
				// ----------
				// Loop over each component
    for (int k=0; k<Mcur; k++) {
      sech = 2.0/(exp(z*1.0e3/pt[k][1]) + exp(-z*1.0e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	sum0 += wt[k]*fac * dens * sum1;
    }
  }

  return sum0;
}

double GalaxyModelTwoD::NormEval(double L, double B, SampleDistribution *d)
{
  if (!good_bounds) return 1.0;

  coordPair P(L, B);

  if ( (mit=cache.find(P)) == cache.end() ) {

    current = new CacheGalaxyModel(L, B);
    cache[P] = current;
    generate(L, B, d);

  } else {

    current = mit->second;

  }

  compute_bins();

  double norm = 0.0;
  for (int j=0; j<nbins; j++)  norm += current->bins[j];

  return norm;
}




vector<double> GalaxyModelTwoD::Evaluate(double L, double B, 
					 SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);

  coordPair P(L, B);

  if ( (mit=cache.find(P)) == cache.end() ) {

    current = new CacheGalaxyModel(L, B);
    generate(L, B, d);
    compute_bins();

  } else {

    current = mit->second;

  }
  
  return current->bins; 
}

