#include <new>
#include <typeinfo>

using namespace std;

#include <SimpleGalaxyModel.h>
#include <gaussQ.h>
#include <Distribution.h>
#include <Uniform.h>
#include <Normal.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SimpleGalaxyModel)

using namespace BIE;

double SimpleGalaxyModel::A1   = 3.5;
double SimpleGalaxyModel::Z1   = 100.0;
double SimpleGalaxyModel::K0   = -8.0;
double SimpleGalaxyModel::SIGK = 0.25;
double SimpleGalaxyModel::KLOW = 0.0;
double SimpleGalaxyModel::KLIM = 14.3;
double SimpleGalaxyModel::MU   = 0.67;

const char* SimpleGalaxyModel::LENGTH_FIELDNAME  = "Length";
const char* SimpleGalaxyModel::HEIGHT_FIELDNAME  = "Height";
const char* SimpleGalaxyModel::PARAM_NAMES[] = {LENGTH_FIELDNAME,HEIGHT_FIELDNAME};

SimpleGalaxyModel::SimpleGalaxyModel(int ndim, int mdim, bool sinb)
{
  Ndim = ndim;
  M = mdim;
  mapped = sinb;

  // Create record type describing parameters.
  parametertype = createMixParameterType(PARAM_NAMES, Ndim, mdim);
  
  norm    = vector<double>(M);
  normoff = vector<double>(M);

  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

  std0.set(A1, Z1, K0, SIGK, KLOW, KLIM);

}

SimpleGalaxyModel::~SimpleGalaxyModel()
{
  for (mit=cache.begin(); mit != cache.end(); mit++) 
    delete mit->second;
}

				// Model: exponential * sech^2 disk

vector<string> SimpleGalaxyModel::ParameterLabels()
{
  vector<string> ret;
  ret.push_back("Length");
  ret.push_back("Height");
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void SimpleGalaxyModel::Initialize(State& s)
{
  M = s.M();
  for (int k=0; k<M; k++) wt[k] = s.Wght(k);
  for (int k=0; k<M; k++) {
    norm[k] = 0.0;
    normoff[k] = 0.0;
    for (int j=0; j<Ndim; j++) pt[k][j] = s.Phi(k, j);
  }
}

void SimpleGalaxyModel::Initialize(vector<double>& w, 
				   vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    norm[k] = 0.0;
    pt[k] = p[k];
  }

}

double SimpleGalaxyModel::NormEval(double L, double B)
{
  double ans = 0.0;

  if (mapped) B = asin(pow(fabs(B), 1.0/(1.0 - MU))) * copysign(1.0, B);

  coordPair P(L, B);

  if ( (mit=cache.find(P)) == cache.end() ) {
    current = new CachedModel(L, B, std0.New());
    cache[P] = current;
  } else {
    current = mit->second;
  }

  for (int k=0; k<M; k++)
    ans += wt[k] * current->std->dens(pt[k][0], pt[k][1], L, B, true);

  return ans;
}



double SimpleGalaxyModel::NormEval(double L, double B, SampleDistribution *d)
{
  double ans = 0.0;

  if (mapped) B = asin(pow(fabs(B), 1.0/(1.0 - MU))) * copysign(1.0, B);

  coordPair P(L, B);

  if ( (mit=cache.find(P)) == cache.end() ) {
    current = new CachedModel(L, B, std0.New());
    cache[P] = current;
  } else {
    current = mit->second;
  }

  for (int k=0; k<M; k++)
    ans += wt[k] * current->std->dens(pt[k][0], pt[k][1], L, B, true);

  return ans;
}

vector<double> SimpleGalaxyModel::EvaluatePoint(double L, double B, 
						PointDistribution *d)
{
  if (mapped) B = asin(pow(fabs(B), 1.0/(1.0 - MU))) * copysign(1.0, B);

  coordPair P(L, B);

  if ( (mit=cache.find(P)) == cache.end() ) {
    current = new CachedModel(L, B, std0.New());
    cache[P] = current;
  } else {
    current = mit->second;
  }

  vector<double> ret;

  double cur, z;

  ret = vector<double>(d->numberData());

  for (int j=0; j<d->numberData(); j++) {
    z = 0.0;

    for (int k=0; k<M; k++) {
      cur = current->std->dens(pt[k][0], pt[k][1], L, B, true);
      z += wt[k] * cur *
	current->std->kfracD(d->Point()[0]);
      }
    
    ret[j] = z;
  }

  return ret;
}

vector<double> SimpleGalaxyModel::EvaluateBinned(double L, double B, 
						 BinnedDistribution *d)
{

  coordPair P(L, B);

  mit = cache.find(P);

  if (mit == cache.end()) {
    current = new CachedModel(L, B, std0.New());
    cache[P] = current;
  } else {
    current = mit->second;
  }

  vector<double> ret;

  double cur, z;
    
  ret = vector<double>(d->numberData());

  for (int j=0; j<d->numberData(); j++) {
    z = 0.0;
    
    for (int k=0; k<M; k++) {
      cur = current->std->dens(pt[k][0], pt[k][1], L, B, true);
      z += wt[k] * cur * 
	current->std->kfrac(d->getLow(j)[0], d->getHigh(j)[0]);
    }
    
    ret[j] = z;
  }

  return ret;
}

