// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
using namespace std;

#include <GalaxyModelNDCached.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GalaxyModelNDCached)


using namespace BIE;

int GalaxyModelNDCached::numA = 24;
int GalaxyModelNDCached::numH = 24;


GalaxyModelNDCached::GalaxyModelNDCached(int ndim, int mdim, 
					 SampleDistribution *_dist) :
  GalaxyModelND(ndim, mdim, _dist) 
{
  if (type == point) throw NoPointDistributionException(__FILE__, __LINE__);
  current2 = 0;
}

GalaxyModelNDCached::~GalaxyModelNDCached()
{
				// Delete the cache
  for (mit2=cache2.begin(); mit2 != cache2.end(); mit2++) 
    delete mit2->second;
}

void GalaxyModelNDCached::generate(double L, double B, SampleDistribution *sd)
{

  vector<double> mm(Nflux);
  double s, R, z, fac;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

				// Extinction slab
  double smaxr, smaxz, smax;
  if (fabs(cosB)<1.0e-8) 
    smaxr = 1.0e30;
  else
    smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));

  if (fabs(sinB)<1.0e-8) 
    smaxz = 1.0e30;
  else
    smaxz = Z1*1.0e-3/fabs(sinB);

  smax = min<double>(smaxr, smaxz);


  current2->work = vector<rvector>(NUM);
  current2->R = vector<real>(NUM);
  current2->z = vector<real>(NUM);
  current2->fac = vector<real>(NUM);


				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    s = intgr->knot(n) * RMAX;
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;
    fac = s*s*intgr->weight(n) * RMAX;

    current2->work[n-1] = vector<real>(nbins, 0.0);
    current2->R[n-1] = R;
    current2->z[n-1] = z;
    current2->fac[n-1] = fac;

    for (int k=0; k<nparam; k++) {

				// Make fluxes for each component
      for (int l=0; l<Nflux; l++)
	mm[l] = pos[k][l] 
	  + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

				// Bin visibility
      for (int j=0; j<nbins; j++) {

	double val = 1.0;
	
	for (int l=0; l<Nflux; l++) {
	  val *= 0.5*
	    (
	     erf( (highb[l][j] - mm[l])/(M_SQRT2*sig[k][l]) )
	     -
	     erf( ( lowb[l][j] - mm[l])/(M_SQRT2*sig[k][l]) )
	     );
	}
	
	current2->work[n-1][j] += w[k]*val;
      }
    }

  }
				// Compute bins

  current2->grid = vector<dvector>(numA*numH);


  double sech, dens;
  int indx;

  double A, H;

  dA = (AMAX - AMIN)/(numA-1);
  dH = (HMAX - HMIN)/(numH-1);

  for (int ii=0; ii<numA; ii++) {
    A = AMIN + dA*ii;

    for (int jj=0; jj<numH; jj++) {
      H = HMIN + dH*jj;

      indx = ii*numH + jj;

				// Zero out theoretical bins

      current2->grid[indx] = vector<double>(nbins, 0.0);
      
      
      for (int n=1; n<=NUM; n++) {

				// Coordinates
	fac = current2->fac[n-1];
	R = current2->R[n-1];
	z = current2->z[n-1];

				// Disk model
	sech = 2.0/(exp(z*1.0e3/H) + exp(-z*1.0e3/H));
	dens = exp(-R/A)*sech*sech/(A*A*H);
	for (int j=0; j<nbins; j++) 
	  current2->grid[indx][j] += 
	    fac * dens * current2->work[n-1][j];
      }
    }
  }
  
}


void GalaxyModelNDCached::compute_bins()
{
  double facA, facH, a[4];
  int indA, indH, indx[4];
  current2->bins = vector<double>(nbins, 0.0);

				// Loop over each components
  for (int k=0; k<Mcur; k++) {

    indA = (int)( (pt[k][0] - AMIN)/dA );
    indA = min<int>(indA, numA-1);
    indA = max<int>(indA, 1);
  
    indH = (int)( (pt[k][1] - HMIN)/dH );
    indH = min<int>(indH, numH-1);
    indH = max<int>(indH, 1);
  
    facA = (pt[k][0] - AMIN - dA*indA)/dA;
    facH = (pt[k][1] - HMIN - dH*indH)/dH;
    
    indx[0] = indA    *numH + indH;
    indx[1] = (indA+1)*numH + indH;
    indx[2] = (indA+1)*numH + indH+1;
    indx[3] = indA    *numH + indH+1;

    a[0] = (1.0-facA)*(1.0-facH);
    a[1] = facA      *(1.0-facH);
    a[2] = facA      *     facH;
    a[3] = (1.0-facA)*     facH;

    for (int j=0; j<nbins; j++)
      current2->bins[j] += wt[k] *
	(a[0]*current2->grid[indx[0]][j] +
	 a[1]*current2->grid[indx[1]][j] +
	 a[2]*current2->grid[indx[2]][j] +
	 a[3]*current2->grid[indx[3]][j] );
  }

}

double GalaxyModelNDCached::NormEval(double L, double B, SampleDistribution *d)
{
  if (!good_bounds) return 1.0;

  coordPair P(L, B);

  mit2 = cache2.find(P);

  if (mit2 == cache2.end()) {
    current2 = new CacheGalaxyModelGrid(L, B);
    cache2[P] = current2;
    generate(L, B, d);
  } else {
    current2 = mit2->second;
  }

  compute_bins();

  double norm = 0.0;
  for (int j=0; j<nbins; j++)  norm += current2->bins[j];

  return norm;
}




vector<double> GalaxyModelNDCached::EvaluateBinned(double L, double B, 
						   SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);

  coordPair P(L, B);

  mit2 = cache2.find(P);

  if (mit2 == cache2.end()) {
    current2 = new CacheGalaxyModelGrid(L, B);
    cache2[P] = current2;
    generate(L, B, d);
  } else {
    current2 = mit2->second;
  }

  unsigned int n = current2->bins.size();
  if (work.size() != n) work = vector<double>(n);
  for (unsigned int i=0; i<n; i++) work[i] = current2->bins[i];

  return work;
}

