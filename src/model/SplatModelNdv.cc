// This may look like C code, but it is really -*- C++ -*-

#include <cmath>
#include <cstdio>
#include <SplatModelNdv.h>
#include <Distribution.h>
#include <BIEException.h>

#include <new>
#include <typeinfo>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SplatModelNdv)

using namespace BIE;

double SplatModelNdv::SIGX = 0.1;
double SplatModelNdv::SIGY = 0.1;
double SplatModelNdv::VV = 1.0;

// Constants related to the record type of this model.
const char* SplatModelNdv::XPOS_FIELDNAME = "X pos";
const char* SplatModelNdv::YPOS_FIELDNAME = "Y pos";
const char* SplatModelNdv::BAND_FIELDNAME = "Band";

SplatModelNdv::SplatModelNdv(int mdim, int nband, 
			     double fluxmin, double fluxmax)
{
				// Number of data attributes
  Nband = nband;
				// Two location dimensions
  Ndim = 2+Nband;
  M = mdim;
  Mcur = M;

  // Create an array of parameter names.
  char ** paramnames = new char * [Ndim];
  paramnames[0] = (char *) XPOS_FIELDNAME;
  paramnames[1] = (char *) YPOS_FIELDNAME;
  
  for (int band = 1; band <= nband; band++)
  {
    // Allocate enough space for "Band N" string.
    char * bandname = new char [strlen(BAND_FIELDNAME) + Nband/10 + 3];
    sprintf(bandname, "%s %i", BAND_FIELDNAME, band);
    paramnames[1+band] = bandname;
  }

  // Create record type describing parameters.
  parametertype = createMixParameterType((const char**)paramnames, Ndim, mdim);

  // Delete all the stuff we just used for specifying param names.
  for (int band = 1; band <= nband; band++)
  { delete paramnames[1+band]; }

  delete paramnames;
 
				// Min and max for flux grid
  fmin = fluxmin;
  fmax = fluxmax;
  
				// Position arrays
  wt = vector<double>(M);
  xx = vector<double>(M);
  yy = vector<double>(M);
  sx = vector<double>(M);
  sy = vector<double>(M);

				// Attribute array
  aa = vector<dvector>(M);
  for (int m=0; m<M; m++) aa[m] = vector<double>(Nband);

				// This are not variables but fixed
				// constants (at this point . . .)
  for (int m=0; m<M; m++) {
    sx[m] = SIGX*SIGX;
    sy[m] = SIGY*SIGY;
  }
}

vector<string> SplatModelNdv::ParameterLabels()
{
  ostringstream ostr;
  vector<string> ret;
  ret.push_back(XPOS_FIELDNAME);
  ret.push_back(YPOS_FIELDNAME);
  for (int n=0; n<Nband; n++) {
    ostr.str("");
    ostr << BAND_FIELDNAME << " " << n+1;
    ret.push_back(ostr.str());
  }
  return ret;
}


/** Compute normalization
*/

void SplatModelNdv::Initialize(State& s)
{
  Mcur = s.M();

  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<M; k++) {
    xx[k] = s.Phi(k, 0);
    yy[k] = s.Phi(k, 1);
    for (int j=0; j<Nband; j++) aa[k][j] = s.Phi(k, 2+j);
  }
}

void SplatModelNdv::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    xx[k] = p[k][0];
    yy[k] = p[k][1];
    for (int j=0; j<Nband; j++) aa[k][j] = p[k][2+j];
  }
}

double SplatModelNdv::NormEval(double xmin, double xmax,
			       double ymin, double ymax)
{
  double bnorm, ans = 0.0;

  for (int k=0; k<Mcur; k++) {

    bnorm = 1.0;		// Bin normalization
    for (int j=0; j<Nband; j++)
      bnorm *=  0.5*( erf((fmax - aa[k][j])/sqrt(2.0*VV)) + 
		      erf((aa[k][j] - fmin)/sqrt(2.0*VV)) ) ;

				// Position distribution
    ans += wt[k] * bnorm * 0.25 *
      ( erf( (xmax - xx[k])/sqrt(2.0*sx[k]) ) -
	erf( (xmin - xx[k])/sqrt(2.0*sx[k]) ) ) *
      ( erf( (ymax - xx[k])/sqrt(2.0*sy[k]) ) -
	erf( (ymin - xx[k])/sqrt(2.0*sy[k]) ) ) ;
  }

  return ans;
}


double SplatModelNdv::NormEval(double x, double y, SampleDistribution *d)
{
  double bnorm, ans = 0.0;

  if (dynamic_cast <PointDistribution*> (d)) return ans;

  for (int k=0; k<Mcur; k++) {

    bnorm = 1.0;		// Bin normalization
    for (int j=0; j<Nband; j++)
      bnorm *=  0.5*( erf((fmax - aa[k][j])/sqrt(2.0*VV)) + 
		      erf((aa[k][j] - fmin)/sqrt(2.0*VV)) ) ;

				// Position distribution
    ans += wt[k] * bnorm * exp(
			       -0.5*(x-xx[k])*(x-xx[k])/sx[k]
			       -0.5*(y-yy[k])*(y-yy[k])/sy[k]
			       ) / (2.0*M_PI*sqrt(sx[k]*sy[k])) ;

  }

  return ans;
}


vector<double> SplatModelNdv::EvaluateBinned(double x, double y, 
					     BinnedDistribution *d)
{
  vector<double> ret(d->numberData(), 0.0);
  double bval, tmp;

				// Compute contribution to
				// distribution in each dimension
  for (int k=0; k<Mcur; k++) {
      
				// Positional info
    tmp = exp(
	      -0.5*(x-xx[k])*(x-xx[k])/sx[k]
	      -0.5*(y-yy[k])*(y-yy[k])/sy[k]
	      ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
      
    
    for (int j=0; j<d->numberData(); j++) {
      
      bval = 1.0;               // Flux bin info
      for (int l=0; l<Nband; l++)
	bval *= 0.5*( erf((d->getHigh(j)[l] - aa[k][l])/sqrt(2.0*VV)) + 
		      erf((aa[k][l] -  d->getLow(j)[l])/sqrt(2.0*VV)) ) ;
      
      ret[j] += tmp * wt[k] * bval;

    }
    
  }

  return ret;
}

vector<double> SplatModelNdv::EvaluatePoint(double x, double y, 
					    PointDistribution *d)
{
  vector<double> ret(d->numberData(), 0.0);
  double bval, tmp;

				// Compute contribution to
				// distribution in each dimension
  for (int k=0; k<Mcur; k++) {
    
				// Positional info
    tmp = exp(
	      -0.5*(x-xx[k])*(x-xx[k])/sx[k]
	      -0.5*(y-yy[k])*(y-yy[k])/sy[k]
	      ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
      
    for (int j=0; j<d->numberData(); j++) {

      bval = 1.0;		// Flux bin info
      for (int l=0; l<Nband; l++)
	bval *= exp( 
		    -(d->Point()[l] - aa[k][l])
		    *(d->Point()[l] - aa[k][l])/(2.0*VV)
		    ) / sqrt(2.0*M_PI*VV);
      ret[j] += tmp * wt[k] * bval;
    }

  }

  return ret;
}

