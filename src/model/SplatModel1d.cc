// This may look like C code, but it is really -*- C++ -*-

#include <cmath>
#include <SplatModel1d.h>
#include <Distribution.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SplatModel1d)

using namespace BIE;

double SplatModel1d::SIGX = 0.1;
double SplatModel1d::VALUE = 1.0;

// Static variables relating to the record type of this Model.
const char* SplatModel1d::XPOS_FIELDNAME   = "X pos";
const char* SplatModel1d::YPOS_FIELDNAME   = "Y pos";
const char* SplatModel1d::BANDONE_FIELDNAME   = "Band One";
const char* SplatModel1d::BANDTWO_FIELDNAME   = "Band Two";

const char * SplatModel1d::PARAM_NAMES [] =
  {XPOS_FIELDNAME,YPOS_FIELDNAME,BANDONE_FIELDNAME,BANDTWO_FIELDNAME};

/*
  Splats can either be defined by location (ndim=2) or location
  and dispersion (ndim=4)
*/
SplatModel1d::SplatModel1d(int ndim, int mdim)
{
  // ndim must be 1 or 2.
  if (!(ndim == 1 || ndim == 2)) 
  { 
    throw BIEException("SplatModel1d Exception",
            "These Splats can be defined by location (ndim=1) or location and dispersion (ndim=2)",
	    __FILE__, __LINE__); 
  }
  
  parametertype = createMixParameterType(PARAM_NAMES, 2, mdim);

  Ndim = ndim;
  M = mdim;
  Mcur = mdim;
  
  wt = vector<double>(M);
  xx = vector<double>(M);
  sx = vector<double>(M);
  
				// Root variance for 2-dimensional case
  if (Ndim == 1) {
    for (int m=0; m<M; m++) {
      sx[m] = SIGX*SIGX;
    }
  }

}

void SplatModel1d::setSIG(double sigx)
{
  SIGX = sigx;
				// Root variance for 2-dimensional case
  if (Ndim == 1) {
    for (int m=0; m<M; m++) {
      sx[m] = SIGX*SIGX;
    }
  }
}

vector<string> SplatModel1d::ParameterLabels()
{
  vector<string> ret;
  for (int j=0; j<Ndim; j++) ret.push_back(PARAM_NAMES[j]);
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void SplatModel1d::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    xx[k] = s.Phi(k, 0);
    if (Ndim==2) {
      sx[k] = s.Phi(k, 1);
    }
  }
}

void SplatModel1d::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    xx[k] = p[k][0];
    if (Ndim==2) {
      sx[k] = p[k][1];
    }
  }
}


double SplatModel1d::NormEval(double xmin, double xmax, 
			      double ymin, double ymax)
{
  double ans = 0.0;

  for (int k=0; k<Mcur; k++) {

    ans += wt[k] * 0.5 *
      ( erf( (xmax - xx[k])/sqrt(2.0*sx[k]) ) -
	erf( (xmin - xx[k])/sqrt(2.0*sx[k]) ) ) ;
  }
  
  return ans;
}


double SplatModel1d::NormEval(double x, double y, SampleDistribution *d)
{
  double ans = 0.0;
  if (y <= 0.0 || y >= 1.0) return 1.0e-20;

  BinnedDistribution *dd;

  if (dynamic_cast <PointDistribution*> (d)) return ans;

    
  if ((dd=dynamic_cast<BinnedDistribution*> (d))) {

    for (int j=0; j<dd->numberData(); j++) {

      // Single dimensional attribute per bin
      // This is only for testing . . .
      if (dd->getLow(j)[0] <= VALUE && dd->getHigh(j)[0] >= VALUE) {

	for (int k=0; k<Mcur; k++) {

	  // Position distribution
	  ans += wt[k] * exp(
			     -0.5*(x-xx[k])*(x-xx[k])/sx[k]
			     ) / sqrt(2.0*M_PI*sx[k]);
	}
	
      }
      
    }

  }

  return ans;
}


vector<double> SplatModel1d::EvaluateBinned(double x, double y, 
					  BinnedDistribution *d)
{
  vector<double> ret(d->numberData(), 1.0e-20);
  if (y <= 0.0 || y >= 1.0) return ret;

  double z;

  for (int j=0; j<d->numberData(); j++) {
    
    z = 0.0;

				// Single dimensional attribute per bin
				// This is only for testing . . .
    if (d->getLow(j)[0] <= VALUE && d->getHigh(j)[0] >= VALUE) {

      for (int k=0; k<Mcur; k++) {
	z += wt[k] * exp(
			 -0.5*(x-xx[k])*(x-xx[k])/sx[k]
			 ) / sqrt(2.0*M_PI*sx[k]);
      }
    }
    
    ret[j] = z;
  }

  return ret;
}

vector<double> SplatModel1d::EvaluatePoint(double x, double y, 
					   PointDistribution *d)
{
  vector<double> ret(d->numberData(), 1.0e-20);
  if (y <= 0.0 || y >= 1.0) return ret;

  double z;

  for (int j=0; j<d->numberData(); j++) {

    z = 0.0;

    if (d->Point()[j] <= VALUE*1.01 && d->Point()[j] >= VALUE/1.01) {

      for (int k=0; k<Mcur; k++) {
	z += wt[k] * exp(
			 -0.5*(x-xx[k])*(x-xx[k])/sx[k]
			 ) / sqrt(2.0*M_PI*sx[k]);
      }
    }
    
    ret[j] = z;
  }

  return ret;
}

