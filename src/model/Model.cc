#include <Model.h>
#include <QuadTreeIntegrator.h>
#include <BasicType.h>
#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::Model)

using namespace BIE;

//
// Static variables for Model class
//
bool   Model::quadtree  = true;
int    Model::maxlevels = 16;
double Model::qeps      = 0.0001;
double Model::dX0       = 0.5;
double Model::dY0       = 0.1;
double Model::dX1       = 0.005;
double Model::dY1       = 0.001;
int    Model::numX      = 40;
int    Model::numY      = 40;

//
// I'd rather do this with functionoids rather than wrappers.  But
// this would require multiple inheritence and we've decided against
// this as a policy.
//

static Model* wrapper_ptr;

vector<double> ModelNormWrapper(double x, double y)
{
  double ans = wrapper_ptr->NormEvalMeasure(x, y) * wrapper_ptr->NormEval(x, y);
  return vector<double>(1, ans);
}

void Model::setQuadTreeParams(double dx0, double dy0, double dx1, double dy1,
			      double eps, int mlev)
{
  dX0       = dx0;
  dY0       = dy0;
  dX1       = dx1;
  dY1       = dy1;
  qeps      = eps;
  maxlevels = mlev;

  //
  // Check for sanity
  //
  if (dX0<dX1) {
    ostringstream msg;
    msg << "dX0(" << dX0 << ") must be >= dX1(" << dX1 << ")";
    throw BadParameterException("Model::setQuadTreeParams", msg.str(), 
				__FILE__, __LINE__);
  }

  if (dY0<dY1) {
    ostringstream msg;
    msg << "dY0(" << dY0 << ") must be >= dY1(" << dY1 << ")";
    throw BadParameterException("Model::setQuadTreeParams", msg.str(), 
				__FILE__, __LINE__);
  }

  if (maxlevels < 1) {
    ostringstream msg;
    msg << "maxlevels(" << maxlevels << ") must be > 0";
    throw BadParameterException("Model::setQuadTreeParams", msg.str(), 
				__FILE__, __LINE__);
  }

}
    

RecordType* Model::createMixParameterType
(const char ** paramnames,
 int           numparams,
 int           mixturedim)
{
  vector<string> params;
  for (int n = 0; n < numparams; n++) params.push_back(paramnames[n]);
  return createMixParameterType(params, numparams, mixturedim);
}

RecordType* Model::createMixParameterType
(const vector<string>&  paramnames,
 int                    numparams,
 int                    mixturedim)
{
  RecordType * resulttype = new RecordType;
  RecordType * newtype;
  
  // All weight fields come first.
  //
  for (int m = 1; m <= mixturedim; m++)
  {
    // Create the name of the field by appending the mixture id.
    //
    ostringstream namebuffer;
    if (mixturedim == 1)
    { namebuffer << "Weight"; }
    else
    { namebuffer << "Weight" << "[" << m << "]"; }
    
    // Append the field to the type.
    //
    newtype = resulttype->insertField(resulttype->numFields() + 1,
                                      namebuffer.str(),BasicType::Real);

    // Delete the old one and assign the new one for the next time round.
    //
    delete resulttype;
    resulttype = newtype;
  }
  
  // The parameter names come after the weights
  //
  for (int m = 1; m <= mixturedim; m++)
  {
    for(int n = 0; n < numparams; n++)
    {
      RecordType * newtype;
      ostringstream namebuffer;

      // Create the name of the field by appending the mixture id.
      //
      if (mixturedim == 1)
      { namebuffer << paramnames[n]; }
      else
      { namebuffer << paramnames[n] << "[" << m << "]"; }
      
      // Add the field to the type.
      //
      newtype = resulttype->insertField(resulttype->numFields() + 1,
                                        namebuffer.str(), BasicType::Real);

      // Delete the old one and assign the new one for the next time round.
      //
      delete resulttype;
      resulttype = newtype;
    }
  }
  
  return resulttype;
}

vector<double> Model::Evaluate(double x, double y, SampleDistribution* d)
{
  vector<double> ans;
  if (dynamic_cast <BinnedDistribution*> (d)) {
    BinnedDistribution *d0 = (BinnedDistribution*)d;
    ans = EvaluateBinned(x, y, d0);
  }
  else if (dynamic_cast <PointDistribution*> (d)) {
    PointDistribution *d0 = (PointDistribution*)d;
    ans = EvaluatePoint(x, y, d0);
  }
  else if (dynamic_cast <NullDistribution*> (d)) {
    ans = vector<double>();	// Return an empty vector (that will not 
				// be used the caller, but, if it is, 
				// trouble will ensue with any luck)
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);

  return ans;
}

double Model::NormEval(double xmin, double xmax, double ymin, double ymax,
		       bool serial)
{
  double dX = xmax - xmin;
  double dY = ymax - ymin;
  


  // Used the cubature algorithm (only possible for serial computation for now)
  //
  if (serial && quadtree) {
    
    int minlev = 
      static_cast<int>(floor(log(max<double>(dX/dX0, dY/dY0))/log(2.0)));
    
    minlev = min<int>(minlev, maxlevels);
    
    int maxlev = 
      static_cast<int>( ceil(log(max<double>(dX/dX1, dY/dY1))/log(2.0)));
    
    maxlev = min<int>(maxlev, maxlevels);
    
    wrapper_ptr = this;

    QuadTreeIntegrator quad(xmin, xmax, ymin, ymax, 1, ModelNormWrapper, qeps, 
			    minlev, maxlev);

    vector<double> ans = quad.Integral();

    // Diagnostics
    //
    //   ____/ set to zero (one) to turn this off (on)
    //  |
    //  v
    if (0 && myid==0) {
      const unsigned int boxc  = 20;
      const unsigned int rate  = 100;
      static unsigned int icnt = 0;

      static deque<double> avg1, avg2;

      double rerr = quad.MaxRelError();
      
      avg1.push_back(rerr);
      avg2.push_back(rerr*rerr);

      if (avg1.size() > boxc) {
	avg1.pop_front();
	avg2.pop_front();
      }

      if (icnt++ % rate == 0) {
	int num = 0;
	double avg = 0.0, stdev = 0.0;
	deque<double>::iterator it1=avg1.begin();
	deque<double>::iterator it2=avg2.begin();
	for (; it1!=avg1.end() && it2!=avg2.end(); it1++, it2++) {
	  avg   += *it1;
	  stdev += *it2;
	  num++;
	}

	if (num>1) {
	  avg /= num;
	  stdev = sqrt( (stdev - avg*avg*num)/(num-1) );
	  
	  cout << "Process "        << myid << ": "
	       << "Lat norm="           << ans[0] 
	       << ", last abs error="    << quad.MaxAbsError() 
	       << ", last rel error="    << quad.MaxRelError() 
	       << ", last # evals="      << quad.NumEvals()
	       << ", last level used="   << quad.MaxLevel()
	       << ", last lower limit="  << minlev
	       << ", last upper limit="  << maxlev
	       << ", average="           << avg
	       << ", std dev="           << stdev
	       << ", count="             << icnt
	       << endl;
	}
      }
    }
    
    return ans[0];
  }


  //
  // Use the Gauss-Legendre algorithm, otherwise.
  // Implemented for both serial and parallel cases.
  //

  double norm=0, norm0, X, Y;
  
  if (iX == 0) iX = new LegeQuad(numX);
  if (iY == 0) iY = new LegeQuad(numY);

  int icnt = 0;
  for (int i=1; i<=numX; i++) {
    for (int j=1; j<=numY; j++) {
      if ( !serial && (icnt++ % numprocs) ) continue;

      X = xmin + dX*iX->knot(i);
      Y = ymin + dY*iY->knot(j);

      norm += iX->weight(i) * iY->weight(j) *
	dX * dY * NormEvalMeasure(X, Y) * NormEval(X, Y);
    }
  }
    
  if (!serial && numprocs>1) 
    MPI_Allreduce(&norm, &norm0, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  else
    norm0 = norm;

  return norm0;
}


