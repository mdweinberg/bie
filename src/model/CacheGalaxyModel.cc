#include <CacheGalaxyModel.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::CacheGalaxyModel)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::CacheGalaxyModelGrid)

//
// These members and functions will only be used for debugging
// and sanity checks
//

void CacheGalaxyModel::info(ostream& out, unsigned ctr)
{
  out << setw(5) << left << ctr << setw(60) 
      << setfill('-') << '-' << setfill(' ') << right << endl;
  out << "--- L, B=(" 
       << setw(15) << L << ", " 
       << setw(15) << B << ")" 
       << endl;
  out << "--- binning size=" << bins.size() << endl;
  out << "--- work[0] size=" << work.front().size() << endl;
  out << "--- work[n] size=" << work.back() .size() << endl;
}

void CacheGalaxyModelGrid::info(ostream& out, unsigned ctr)
{
  CacheGalaxyModel::info(out, ctr);
  out << "--- 1d grid size=" << grid.size() << endl;
  out << "--- grid[0] size=" << grid.front().size() << endl;
  out << "--- grid[n] size=" << grid.back() .size() << endl;
}


void BIE::CacheGalaxyModelDump(mmapGalCM&  cache, ostream& out)
{
  out << "Size of cache=" << cache.size() << endl << endl;
  unsigned ctr=1;
  for (mmapGalCM::iterator it=cache.begin();
       it!=cache.end(); it++, ctr++) it->second->info(out, ctr);
}
    
void BIE::CacheGalaxyModelDump(mmapGalCMG& cache, ostream& out)
{
  out << "Size of cache=" << cache.size() << endl << endl;
  unsigned ctr=1;
  for (mmapGalCMG::iterator it=cache.begin();
       it!=cache.end(); it++, ctr++) it->second->info(out, ctr);
}
