#include <iomanip>
using namespace std;

#include <GalaxyModelOneD.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GalaxyModelOneD)

using namespace BIE;

double GalaxyModelOneD::A1     = 3.5;
double GalaxyModelOneD::Z1     = 100.0;
double GalaxyModelOneD::K0     = -4.0;
double GalaxyModelOneD::SIGK   = 0.25;
int    GalaxyModelOneD::NUM    = 200;
double GalaxyModelOneD::ALPHA  = 0.0;
double GalaxyModelOneD::BETA   = 0.0;
double GalaxyModelOneD::R0     = 8.0;
double GalaxyModelOneD::RMAX   = 100.0;
double GalaxyModelOneD::AK     = 0.1;
double GalaxyModelOneD::AMIN   = 0.5;
double GalaxyModelOneD::AMAX   = 8.0;
double GalaxyModelOneD::HMIN   = 100.0;
double GalaxyModelOneD::HMAX   = 1200.0;
double GalaxyModelOneD::HMAG   = 16.0;
double GalaxyModelOneD::LMAG   = 6.0;
double GalaxyModelOneD::Log10  = log(10.0);
double GalaxyModelOneD::onedeg = M_PI/180.0;
double GalaxyModelOneD::smin   = 0.05;
bool   GalaxyModelOneD::logs   = false;

const char* GalaxyModelOneD::LENGTH_FIELDNAME = "Length";
const char* GalaxyModelOneD::HEIGHT_FIELDNAME = "Height";
const char* GalaxyModelOneD::PARAM_NAMES[]    = {LENGTH_FIELDNAME, 
						 HEIGHT_FIELDNAME};

/**
   Model: exponential * sech^2 disk
*/
GalaxyModelOneD::GalaxyModelOneD(int ndim, int mdim, SampleDistribution* _dist)
{
  Ndim = ndim;
  M = mdim;
  Mcur = M;

  // Create record type describing parameters.
  //
  parametertype = createMixParameterType(PARAM_NAMES, Ndim, mdim);

  // Storage for the mixture model
  //
  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
				// Cache the bin boundaries for the
				// data histogram
    nbins = histo->numberData();
    for (int i=0; i<nbins; i++) {
      lowb. push_back(histo->getLow (i)[0]);
      highb.push_back(histo->getHigh(i)[0]);
    }
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
    nbins = 1;
  } else if (dynamic_cast <NullDistribution*> (_dist)) {
    throw InappropriateDistributionException("NullDistribution", 
					     __FILE__, __LINE__);
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);


  // Default line-of-sight integration knots
  //
  intgr = new JacoQuad(NUM, ALPHA, BETA);

  cache_limit = 0;
  cache_full  = false;
  current     = 0;
}


GalaxyModelOneD::GalaxyModelOneD()
{
  cache_limit = 0;
  cache_full  = false;
  current     = 0;
  intgr       = 0;
}


GalaxyModelOneD::~GalaxyModelOneD()
{
  delete intgr;
				// Delete the cache
  for (mit=cache.begin(); mit != cache.end(); mit++) 
    delete mit->second;
}

vector<string> GalaxyModelOneD::ParameterLabels()
{
  vector<string> ret;
  ret.push_back(LENGTH_FIELDNAME);
  ret.push_back(HEIGHT_FIELDNAME);
  return ret;
}

/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/
void GalaxyModelOneD::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s.Phi(k, j);
  }

  check_bounds();
}

void GalaxyModelOneD::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}

void GalaxyModelOneD::check_bounds()
{
  good_bounds = true;
				// Check bounds
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;
  }
  for (int k=0; k<Mcur; k++) {
    if (pt[k][0] > AMAX || pt[k][0] < AMIN   ||
	pt[k][1] > HMAX || pt[k][1] < HMIN    )  good_bounds = false;
  }

				// DEBUG
  if (good_bounds == false) {
    cout << "State rejected (" << Mcur << "):\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<2; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
}

double GalaxyModelOneD::NormEval(double L, double B)
{
  double s, R, z, fac;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);
				// Extinction slab
  double smaxr, smaxz, smax;
  if (fabs(cosB)<1.0e-8) 
    smaxr = 1.0e30;
  else
    smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));

  if (fabs(sinB)<1.0e-8) 
    smaxz = 1.0e30;
  else
    smaxz = Z1*1.0e-3/fabs(sinB);

  smax = min<double>(smaxr, smaxz);


  double mm, sech, dens, sum=0.0;
				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    if (logs) {
      s = smin*exp(intgr->knot(n) * (log(RMAX) - log(smin)));
      fac = s*s*s*intgr->weight(n) * (log(RMAX) - log(smin));
    } else {
      s = intgr->knot(n) * RMAX;
      fac = s*s*intgr->weight(n) * RMAX;
    }
      
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;

    mm = K0 + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

    // Visibility
    //
    double val = 0.5 *
      (
       erf( (HMAG - mm)/(M_SQRT2*SIGK) ) - 
       erf( (LMAG - mm)/(M_SQRT2*SIGK) )
       );
				// Disk model
				// ----------
				// Loop over each component
    for (int k=0; k<Mcur; k++) {
      sech = 2.0/(exp(z*1.0e3/pt[k][1]) + exp(-z*1.0e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	sum += wt[k]*fac * dens * val;
    }
  }

  return sum;
}


void GalaxyModelOneD::generate(double L, double B, SampleDistribution *sd)
{

  double s, R, z, fac, mm;

  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

				// Extinction slab
  double smax;
  if (fabs(cosB)<1.0e-6) 
    smax = Z1*1.0e-3/fabs(sinB);
  else if (fabs(sinB)<1.0e-6) 
    smax = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));
  else
    smax = min<double>(
		       Z1*1.0e-3/fabs(sinB), 
		       R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL))
		       );

  current->work = vector<rvector>(NUM);
  current->R    = vector<real>(NUM);
  current->z    = vector<real>(NUM);
  current->fac  = vector<real>(NUM);

				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    if (logs) {
      s = smin*exp(intgr->knot(n) * (log(RMAX) - log(smin)));
      fac = s*s*s*intgr->weight(n) * (log(RMAX) - log(smin));
    } else {
      s = intgr->knot(n) * RMAX;
      fac = s*s*intgr->weight(n) * RMAX;
    }
      
    R   = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z   = s*sinB;

    current->work[n-1] = vector<real>(nbins);
    current->R[n-1]    = R;
    current->z[n-1]    = z;
    current->fac[n-1]  = fac;

    mm = K0 + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

    switch (type) {
      
    case binned:
				// Bin visibility
      for (int j=0; j<nbins; j++) {
	
	current->work[n-1][j] = 
	  0.5*erf( (highb[j] - mm)/(M_SQRT2*SIGK) )
	  -
	  0.5*erf( ( lowb[j] - mm)/(M_SQRT2*SIGK) )
	  ;
      }

      break;

    case point:

      PointDistribution *pnt = (PointDistribution *)sd;

      current->work[n-1][0] = 
	exp(-(pnt->Point()[0] - mm)*(pnt->Point()[0] - mm)/(2.0*SIGK*SIGK) ) /
	sqrt(2.0*M_PI*SIGK*SIGK);

      break;

    }

  }

}

void GalaxyModelOneD::compute_bins()
{
  double sech, R, z, fac, dens;
				// Zero out theoretical bins
  current->bins = vector<double>(nbins, 0.0);

  for (int n=1; n<=NUM; n++) {
				// Coordinates
    fac = current->fac[n-1];
    R   = current->R[n-1];
    z   = current->z[n-1];
				// Loop over each components
    for (int k=0; k<Mcur; k++) {
				// Disk model
      sech = 2.0/(exp(z*1.0e3/pt[k][1]) + exp(-z*1.0e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	current->bins[j] += wt[k]*fac*dens*current->work[n-1][j];
    }
  }

}

void GalaxyModelOneD::manageCache(coordPair& P)
{
  if (cache_limit) {

    // Save element
    cacheList.push_back(P);

    // FIFO delete elements
    while (cacheList.size() > cache_limit) {

      if (!cache_full) {	// Informational message
	cache_full = true;
	cout << "Process " << myid 
	     << ": GalaxyModelOneD::manageCache: "
	     << "cache is now full" << endl;
      }
				// Find the element to be deleted
      mit = cache.find(cacheList.front());
				// If it exists, delete it!
      if (mit != cache.end()) {
	delete mit->second;
	cache.erase(mit);
	cacheList.pop_front();
      } else {
	string msg("GalaxyModelOneD cache is inconsistent");
	throw InternalError(msg, __FILE__, __LINE__);
      }
      
    }

  }

}



double GalaxyModelOneD::NormEval(double L, double B, SampleDistribution *sd)
{
  if (!good_bounds) return 1.0;

  coordPair P(L, B);
  
  if ((mit=cache.find(P)) == cache.end()) {

    current = new CacheGalaxyModel(L, B);
    cache[P] = current;
    generate(L, B, sd);

    manageCache(P);

  } else {

    current = mit->second;

  }

  compute_bins();

  double norm = 0.0;
  for (int j=0; j<nbins; j++)  norm += current->bins[j];

  return norm;
}




vector<double> GalaxyModelOneD::Evaluate(double L, double B,
					 SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);

  coordPair P(L, B);
  
  if ( (mit=cache.find(P)) == cache.end() ) {

    current = new CacheGalaxyModel(L, B);
    cache[P] = current;
    generate(L, B, d);
    compute_bins();

    manageCache(P);

  } else {

    current = mit->second;
    if (type==point) compute_bins();
    
  }
  
  return current->bins; 
}
