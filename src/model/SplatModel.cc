// -*- C++ -*-

#include <cmath>
#include <SplatModel.h>
#include <Distribution.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SplatModel)

using namespace BIE;

double SplatModel::SIGX  = 0.1;
double SplatModel::SIGY  = 0.1;
double SplatModel::VALUE = 1.0;

// Static variables relating to the record type of this Model.
const char* SplatModel::XPOS_FIELDNAME     = "X pos";
const char* SplatModel::YPOS_FIELDNAME     = "Y pos";
const char* SplatModel::BANDONE_FIELDNAME  = "Band One";
const char* SplatModel::BANDTWO_FIELDNAME  = "Band Two";

const char * SplatModel::PARAM_NAMES [] =
  {XPOS_FIELDNAME,YPOS_FIELDNAME,BANDONE_FIELDNAME,BANDTWO_FIELDNAME};

/*
  Splats can either be defined by location (ndim=2) or location
  and dispersion (ndim=4)
*/
SplatModel::SplatModel(int ndim, int mdim)
{
  // ndim must be 2 or 4.
  if (!(ndim == 2 || ndim == 4)) 
  { 
    throw BIEException("SplatModel Exception",
            "These 2d Splats can be defined by location (ndim=2) or location and dispersion (ndim=4)",
	    __FILE__, __LINE__); 
  }
  
  parametertype = createMixParameterType(PARAM_NAMES, 2, mdim);

  Ndim = ndim;
  M    = mdim;
  Mcur = mdim;
  
  wt = vector<double>(M);
  xx = vector<double>(M);
  yy = vector<double>(M);
  sx = vector<double>(M);
  sy = vector<double>(M);

  setSIG(SIGX, SIGY);
}

void SplatModel::setSIG(double sigx, double sigy)
{
  SIGX = sigx;
  SIGY = sigy;
				// Root variance for 2-dimensional case:
  if (Ndim == 2) {		// constants
    for (int m=0; m<M; m++) {
      sx[m] = SIGX*SIGX;
      sy[m] = SIGY*SIGY;
    }
  }
}


vector<string> SplatModel::ParameterLabels()
{
  vector<string> ret;
  for (int j=0; j<Ndim; j++) ret.push_back(PARAM_NAMES[j]);
  return ret;
}

/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void SplatModel::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    xx[k] = s.Phi(k, 0);
    yy[k] = s.Phi(k, 1);
    if (Ndim==4) {
      sx[k] = s.Phi(k, 2);
      sy[k] = s.Phi(k, 3);
    }
  }
}

void SplatModel::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    xx[k] = p[k][0];
    yy[k] = p[k][1];
    if (Ndim==4) {
      sx[k] = p[k][2];
      sy[k] = p[k][3];
    }
  }
}


double SplatModel::NormEval(double x, double y, SampleDistribution *d)
{
  double ans = 0.0;
  BinnedDistribution *dd;

  if (dynamic_cast <PointDistribution*> (d)) return ans;

    
  if ((dd=dynamic_cast<BinnedDistribution*> (d))) {

    for (int j=0; j<dd->numberData(); j++) {

      // Single dimensional attribute per bin
      // This is only for testing . . .
      if (dd->getLow(j)[0] <= VALUE && dd->getHigh(j)[0] >= VALUE) {

	for (int k=0; k<Mcur; k++) {

	  // Position distribution
	  ans += wt[k] * exp(
			     -0.5*(x-xx[k])*(x-xx[k])/sx[k]
			     -0.5*(y-yy[k])*(y-yy[k])/sy[k]
			   ) / (2.0*M_PI*sqrt(sx[k]*sy[k])) ;
	}
	
      }
      
    }

  }

  return ans;
}


double SplatModel::NormEval(double xmin, double xmax, double ymin, double ymax)
{
  double ans = 0.0;

  for (int k=0; k<Mcur; k++) {

    ans += wt[k] * 0.25 *
      ( erf( (xmax - xx[k])/sqrt(2.0*sx[k]) ) -
	erf( (xmin - xx[k])/sqrt(2.0*sx[k]) ) ) *
      ( erf( (ymax - xx[k])/sqrt(2.0*sy[k]) ) -
	erf( (ymin - xx[k])/sqrt(2.0*sy[k]) ) ) ;
  }
	
  return ans;
}


vector<double> SplatModel::EvaluateBinned(double x, double y, 
					  BinnedDistribution *d)
{
  double z;
  vector<double> ret(d->numberData());

  for (int j=0; j<d->numberData(); j++) {

    z = 0.0;

				// Single dimensional attribute per bin
				// This is only for testing . . .
    if (d->getLow(j)[0] <= VALUE && d->getHigh(j)[0] >= VALUE) {

      for (int k=0; k<Mcur; k++) {
	z += wt[k] * exp(
			 -0.5*(x-xx[k])*(x-xx[k])/sx[k]
			 -0.5*(y-yy[k])*(y-yy[k])/sy[k]
			 ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
      }
    }
    
    ret[j] = z;
  }

  return ret;
}

vector<double> SplatModel::EvaluatePoint(double x, double y, 
					 PointDistribution *d)
{
  double z;
  vector<double> ret(d->numberData());

  for (int j=0; j<d->numberData(); j++) {

    z = 0.0;

    if (d->Point()[j] <= VALUE*1.01 && d->Point()[j] >= VALUE/1.01) {

      for (int k=0; k<Mcur; k++) {
	z += wt[k] * exp(
			 -0.5*(x-xx[k])*(x-xx[k])/sx[k]
			 -0.5*(y-yy[k])*(y-yy[k])/sy[k]
			 ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
      }
    }
    
    ret[j] = z;
  }

  return ret;
}

