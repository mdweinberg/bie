// This may look like C code, but it is really -*- C++ -*-

#include <BIEconfig.h>
#include <StdCandle2.h>
#include <BIEException.h>
#include <gfunction.h>
#include <ACG.h>
#include <Normal.h>
#include <Uniform.h>
#include <gaussQ.h>
#include <Table.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::StdCandle)

using namespace BIE;

int StdCandle::NUM=200;
int StdCandle::NTAB=100;
double StdCandle::ALPHA=0.0;
double StdCandle::BETA=0.0;
double StdCandle::R=8.0;
double StdCandle::AK=0.1;
int StdCandle::NITER=100;
double StdCandle::tol=1.0e-5;
double StdCandle::Log10 = log(10.0);
double StdCandle::onedeg = M_PI/180.0;


StdCandle::StdCandle()
{
				// Some default values
  set(3.5, 100.0, -8.0, 0.25, 0.0, 14.3);

  dext  = NULL;
  ext   = NULL;
  table = NULL;
  kdist = NULL;
  kprob = NULL;
  intgr = NULL;

  lastL = -9999.0;
  lastB = -9999.0;

  allocated  = false;
  initrandom = false;
  initkdist  = false;

  uniform = NULL;
  normal  = NULL;
}

void StdCandle::allocate(void)
{
  dext = new Table(NTAB);
  ext = new Table(NTAB);
  table = new Table(NTAB);
  kdist = new Table(NTAB);
  kprob = new Table(NUM);
  intgr = new JacoQuad(NUM, ALPHA, BETA);
  tt  = vector<double>(NUM);
  ttC = vector<double>(NUM);
  uu  = vector<double>(NUM);
  uuC = vector<double>(NUM);
  rr  = vector<double>(NUM);
  zz  = vector<double>(NUM);
  mat = vector<double>(NUM*NTAB);

				// Optimize abscissa search

#ifdef TABLE_EVEN
  dext->set_even();
  ext->set_even();
  table->set_even();
  kdist->set_even();
  kprob->set_even();
#endif
				// Normalize extinction to
				// solar neighborhood
  aknorm = 1.0/exp(-R/A1);

  allocated = true;
}

void StdCandle::set(double a1, double h, 
		    double k0, double sigk, double klow, double klim)
{
  A1 = a1;
  H = h;
  K0 = k0;
  SIGK = sigk;
  KLOW = klow;
  KLIM = klim;

  reset = true;
}


StdCandle::StdCandle(double a1, double h, 
		     double k0, double sigk, double klow, double klim)
{
  set(a1, h, k0, sigk, klow, klim);

				// Construct K-q table
  dext = NULL;
  ext = NULL;
  table = NULL;
  kdist = NULL;
  kprob = NULL;
  intgr = NULL;

  lastL = -9999.0;
  lastB = -9999.0;

  allocated = false;
  initrandom = false;
  iseed = 11;
  uniform = NULL;
  normal = NULL;
}

StdCandle* StdCandle::New()
{
  return new StdCandle(A1, H, K0, SIGK, KLOW, KLIM);
}

StdCandle::~StdCandle()
{
  delete dext;
  delete ext;
  delete table;
  delete kdist;
  delete kprob;
  delete intgr;

  delete uniform;
  delete normal;
}

void StdCandle::q_tabs(void)
{
  double F, dF;
  double r = 100.0;

  qmin = 1.0;

				// Compute max radius without extinction

  for (int i=0; i<NITER; i++) {
    F = K0 - 10.0*SIGK + 5.0*log(r/10.0)/Log10 - KLIM;
    dF = 5.0/(r*Log10);
    r += -F/dF;
    if (fabs(F)<tol) break;
  }

				// Compute extinction along line of sight
				// out to r_max
  double ss, xx, yy, zz, rr, q;
  for (int i=0; i<NTAB; i++) {
    ext->xx[i] = dext->xx[i] = qmin + (log(r) - qmin)*i/(NTAB-1);
    ss = exp(dext->xx[i]);
    xx = ss*CosL*CosB;
    yy = ss*SinL*CosB;
    zz = ss*SinB;
    rr = sqrt((xx - R*1e3)*(xx - R*1e3) + yy*yy + zz*zz);
    dext->yy[i] = 0.001*AK*aknorm*exp(-0.001*rr/A1)/cosh(zz/H)/cosh(zz/H);
  }
  
				// Tabulate integrating extinction
  ext->yy[0] = 0.001*AK*aknorm*exp(qmin);
  for (int i=1; i<NTAB; i++)
    ext->yy[i] = ext->yy[i-1] +
      0.5*(exp(ext->xx[i])-exp(ext->xx[i-1]))*(dext->yy[i] + dext->yy[i-1]);
    
				// Iterate r_max to find value with
				// extinction along column
  for (int i=0; i<NITER; i++) {
    F = K0 - 10.0*SIGK + 5.0*log(r/10.0)/Log10 + ext->get_y(log(r)) - KLIM;
    dF = 5.0/(r*Log10) + dext->get_y(log(r));
    r += -F/dF;
				// Minimum value
				// (needed if far from convergence)
    if (r<0.01) r = 0.01;
    if (fabs(F)<tol) break;
  }

  qmax = log(r);

				// Tabulate distance vs. limiting absolution
				// magnitude
  for (int i=0; i<NTAB; i++) {
    table->xx[i] = q = qmin + (qmax-qmin)*i/(NTAB-1);
    table->yy[i] = KLIM - 5.0*(q/Log10 - 1.0) - ext->get_y(q);
  }
  
  reset = false;
}

void StdCandle::initdens(double L, double B, bool kd)
{
				// Only recompute if necessary . . .
				// Data should be ordered so that
				// all L and B values are done sequentially
  if (fabs(L-lastL)<1.0e-6 && fabs(B-lastB)<1.0e-6 && 
      (kd == false || kd == initkdist) && reset == false) return;

  if (!allocated) allocate();

  initkdist = kd;

  CosL = cos(L);
  SinL = sin(L);
  CosB = cos(B);
  SinB = sin(B);

				// Compute extinction table and limits
  q_tabs();

				// Do integral along line of sight to
				// get density in logarithmic units.
  double k, q, r, s;

  for (int i=1; i<=NUM; i++) {

    q = kprob->xx[i-1] = qmin + (qmax - qmin)*intgr->knot(i);
    k = table->get_y(q);
    s = exp(q);
    r = rr[i-1] = sqrt( (s*CosL*CosB - R*1e3)*(s*CosL*CosB - R*1e3) +
			s*s*(SinL*SinL*CosB*CosB + SinB*SinB) );

    zz[i-1] = s*SinB;

    uuC[i-1] = 1.0e-9*pow(intgr->knot(i), -ALPHA+1.0e-8) * 
      pow(1.0 - intgr->knot(i), -BETA+1.0e-8) *
      s*s*s;

    ttC[i-1] = 0.5*(1.0 + erf((k-K0)/(M_SQRT2*SIGK)));
  }
  
  if (initkdist)
    {
      double k, kdmin=KLOW, kdmax=KLIM;
      double dkd = (kdmax - kdmin)/(NTAB-1);
      double fac;

      for (int i=0; i<NTAB; i++) {
	k = kdist->xx[i] = kdmin + dkd*i;
	
	fac = k - 5.0*(kprob->xx[0]/Log10 - 1.0) - ext->get_y(kprob->xx[0]) 
	  - K0;
	mat[i*NUM] = exp(-fac*fac/(2.0*SIGK*SIGK));

	for (int j=1; j<NUM; j++) {

	  fac = k - 5.0*(kprob->xx[j]/Log10 - 1.0) - ext->get_y(kprob->xx[j]) 
	    - K0;

	  mat[i*NUM+j] = exp(-fac*fac/(2.0*SIGK*SIGK));
	}
      }
    }
  
  lastL = L;
  lastB = B;

  return;
}


double StdCandle::dens(double a, double z, double l, double b, bool kd)
{
  if (fabs(l-lastL)<1.0e-6 && fabs(b-lastB)<1.0e-6 &&
      fabs(a-A)<1.0e-6 && fabs(z-Z)<1.0e-6 && 
      (kd==false || kd==initkdist) ) return lastDen;

  if (fabs(l-lastL)>1.0e-6 || fabs(b-lastB)>1.0e-6 || (initkdist==false && kd))
    initdens(l, b, kd);

  A = a;
  Z = z;
				// Do integral along line of sight to
				// get density in logarithmic units.
  double zden, ans = 0.0;

  for (int i=1; i<=NUM; i++) {

    zden = 1.0/cosh(zz[i-1]/Z);

    uu[i-1] = uuC[i-1] * exp(-0.001*rr[i-1]/A) * zden*zden;

    tt[i-1] = ttC[i-1] * uu[i-1];

    ans += intgr->weight(i) * tt[i-1];
  }
  
  if (initrandom)
    {
      kprob->yy[0] = 0.0;
      for (int i=1; i<NUM; i++) {
	kprob->yy[i] = kprob->yy[i-1] + 
	  0.5*(kprob->xx[i] - kprob->xx[i-1])*(tt[i] + tt[i-1]);
      }
    }

  if (initkdist)
    {
      double kdmin=KLOW, kdmax=KLIM;
      double dkd = (kdmax - kdmin)/(NTAB-1);
      double ans1;
      double curd, lastd=0.0;

      for (int i=0; i<NTAB; i++) {
	ans1 = 0.0;
	for (int j=1; j<NUM; j++)
	  ans1 += 0.5*(kprob->xx[j] - kprob->xx[j-1]) * 
	    (uu[j]*mat[i*NUM+j] + uu[j-1]*mat[i*NUM+j-1]);
	
	curd = ans1/sqrt(2.0*M_PI*SIGK*SIGK);
	if (i) 
	  kdist->yy[i] = kdist->yy[i-1] +  0.5*dkd*(lastd + curd);
	else
	  kdist->yy[i] = 0.0;

	lastd = curd;
      }
    }

  ans *= qmax - qmin;
  lastDen = ans;

  return ans;
}


vector<double> StdCandle::realize(double a, double z)
{
  if (!initrandom) {
    initrandom = true;

    uniform = new Uniform(0.0, 1.0, BIEgen);
    normal = new Normal(K0, SIGK*SIGK, BIEgen);

    ret = vector<double>(3);

				// Find maximum value

    A = a;
    Z = z;
				// Begin with value toward center of Galaxy
    dmax = dens(A, Z, 0.0, 0.0);

				// Granularity
    int num=1000;
				// Bmax in degrees
    double bmax=10.0;

    double dt;
    for (int i=0; i<num; i++) {
      dt = dens(A, Z, 0.0, bmax*onedeg*i/num);
      if (dt>dmax) dmax = dt;
    }
    dmax *= 1.02;
  }

  const int MAXIT = 40000;
  double l, b, q, k;
  int i;
  for (i=0; i<MAXIT; i++) {
    l = -M_PI + 2.0*M_PI*(*uniform)();
    b = asin(2.0*(*uniform)()-1.0);
    if (dens(A, Z, l, b)/dmax > (*uniform)()) break;
  }

  if (i==MAXIT) cerr << "StdCandle: no point selected!\n";
  
				// Alternative
  q = dens(A, Z, l, b, true);
  q = kfrac(kdist->xx[0], KLOW) + kfrac(KLOW, KLIM) * (*uniform)();
  k = kdist->get_x(q*kdist->yy[NTAB-1]);

  if (std::isinf(k)) {
    ostream cout(checkTable("simulation_output"));

    cout << "Oops\n";
  }

  /*
  q = kprob->get_x(kprob->yy[NUM-1]*(*uniform)());
  k = (*normal)() + 5.0*(q/Log10 - 1.0) + ext->get_y(q);
  */

  ret[0] = l;
  ret[1] = b;
  ret[2] = k;

  return ret;
}

double StdCandle::kfrac(double kmin, double kmax)
{
  kmin = max<double>(kmin, kdist->xx[0]    );
  kmax = min<double>(kmax, kdist->xx[NTAB-1]);

  double ans = kdist->get_y(kmax) - kdist->get_y(kmin);

  if (kdist->yy[NTAB-1] > 0.0) ans /= kdist->yy[NTAB-1];

  return ans;
}

double StdCandle::kfracD(double k)
{
  k = max<double>(k, kdist->xx[0]    );
  k = min<double>(k, kdist->xx[NTAB-1]);

  double ans = kdist->get_drv(k);

  if (kdist->yy[NTAB-1]>0.0) ans /= kdist->yy[NTAB-1];

  return ans;
}


void StdCandle::print_kfrac(const char *name)
{
  ofstream out(name);
  if (!out) {
    throw FileNotExistException("StdCandle: ",name, __FILE__, __LINE__);
  }
  for (int i=0; i<NTAB; i++)
    out << " " << kdist->xx[i]
	<< " " << kdist->yy[i]
	<< "\n";
}
