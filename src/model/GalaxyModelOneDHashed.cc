// This is really -*- C++ -*-

#include <iomanip>
using namespace std;

#include <GalaxyModelOneDHashed.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GalaxyModelOneDHashed)

using namespace BIE;

double GalaxyModelOneDHashed::A1     = 20.0;
double GalaxyModelOneDHashed::Z1     = 100.0;
double GalaxyModelOneDHashed::K0     = -4.0;
double GalaxyModelOneDHashed::SIGK   = 0.25;
int    GalaxyModelOneDHashed::NUM    = 200;
double GalaxyModelOneDHashed::ALPHA  = 0.0;
double GalaxyModelOneDHashed::BETA   = 0.0;
double GalaxyModelOneDHashed::R0     = 8.0;
double GalaxyModelOneDHashed::RMAX   = 25.0;
double GalaxyModelOneDHashed::AK     = 0.1;
double GalaxyModelOneDHashed::AMIN   = 0.5;
double GalaxyModelOneDHashed::AMAX   = 8.0;
double GalaxyModelOneDHashed::HMIN   = 100.0;
double GalaxyModelOneDHashed::HMAX   = 1200.0;
double GalaxyModelOneDHashed::Log10  = log(10.0);
double GalaxyModelOneDHashed::onedeg = M_PI/180.0;
bool   GalaxyModelOneDHashed::logs   = false;
double GalaxyModelOneDHashed::smin   = 0.01;

const char* GalaxyModelOneDHashed::LENGTH_FIELDNAME = "Length";
const char* GalaxyModelOneDHashed::HEIGHT_FIELDNAME = "Height";
const char* GalaxyModelOneDHashed::PARAM_NAMES[]    = {LENGTH_FIELDNAME,HEIGHT_FIELDNAME};

GalaxyModelOneDHashed::GalaxyModelOneDHashed(int ndim, int mdim, SampleDistribution* _dist)
{
  Ndim = ndim;
  M = mdim;
  Mcur = M;

  // Create record type describing parameters.
  parametertype = createMixParameterType(PARAM_NAMES, Ndim, mdim);

  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;

				// Cache the bin boundaries for the data
    nbins = histo->numberData(); // histogram
    for (int i=0; i<nbins; i++) {
      lowb.push_back(histo->getLow(i)[0]);
      highb.push_back(histo->getHigh(i)[0]);
    }
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
    nbins = 1;
  } else if (dynamic_cast <NullDistribution*> (_dist)) {
    throw InappropriateDistributionException("NullDistribution", 
					     __FILE__, __LINE__);
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);


  // Default line-of-sight integration knots
  intgr = new JacoQuad(NUM, ALPHA, BETA);
}

GalaxyModelOneDHashed::~GalaxyModelOneDHashed()
{
  delete intgr;

				// Delete the cache
  mmapGalCM::iterator i;
  for (i=cache.begin(); i != cache.end(); i++) delete (*i).second;
}

				// Model: exponential * sech^2 disk

vector<string> GalaxyModelOneDHashed::ParameterLabels()
{
  vector<string> ret;
  ret.push_back(LENGTH_FIELDNAME);
  ret.push_back(HEIGHT_FIELDNAME);
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void GalaxyModelOneDHashed::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s.Phi(k, j);
  }

  check_bounds();
}

void GalaxyModelOneDHashed::Initialize(vector<double>& w, 
				       vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<Mcur; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}

void GalaxyModelOneDHashed::check_bounds()
{
  good_bounds = true;
				// Check bounds
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;
  }
  for (int k=0; k<Mcur; k++) {
    if (pt[k][0] > AMAX || pt[k][0] < AMIN   ||
	pt[k][1] > HMAX || pt[k][1] < HMIN    )  good_bounds = false;
  }

				// DEBUG
  if (good_bounds == false) {
    cout << "State rejected (" << Mcur << "):\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<2; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
}


CacheGalaxyModel* GalaxyModelOneDHashed::generate
(const coordPair& P, SampleDistribution *sd)
{
  double L = P.first;
  double B = P.second;

  double s, R, z, fac, mm;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

				// Make hash table entry
  CacheGalaxyModel *p = new CacheGalaxyModel(L, B);
  cache[P] = p;

				// Extinction slab
  double smaxr, smaxz, smax;
  if (fabs(cosB)<1.0e-8) 
    smaxr = 1.0e30;
  else
    smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));

  if (fabs(sinB)<1.0e-8) 
    smaxz = 1.0e30;
  else
    smaxz = Z1*1.0e-3/fabs(sinB);

  smax = min<double>(smaxr, smaxz);

  p->work = vector<rvector>(NUM);
  p->R    = vector<real>(NUM);
  p->z    = vector<real>(NUM);
  p->fac  = vector<real>(NUM);

				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    if (logs) {
      s  = smin*exp(intgr->knot(n) * (log(RMAX) - log(smin)));
      fac = s*s*s*intgr->weight(n) * (log(RMAX) - log(smin));
    } else {
      s = intgr->knot(n) * RMAX;
      fac = s*s*intgr->weight(n) * RMAX;
    }

    s = intgr->knot(n) * RMAX;
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;

    p->work[n-1] = vector<real>(nbins);
    p->R[n-1]    = R;
    p->z[n-1]    = z;
    p->fac[n-1]  = fac;

    mm = K0 + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

    switch (type) {
      
    case binned:
				// Bin visibility
      for (int j=0; j<nbins; j++) {
	
	p->work[n-1][j] = 
	  0.5*erf( (highb[j] - mm)/(M_SQRT2*SIGK) )
	  -
	  0.5*erf( ( lowb[j] - mm)/(M_SQRT2*SIGK) )
	  ;
      }

      break;

    case point:

      PointDistribution *pnt = (PointDistribution *)sd;

      p->work[n-1][0] = 
	exp(-(pnt->Point()[0] - mm)*(pnt->Point()[0] - mm)/(2.0*SIGK*SIGK) ) /
	sqrt(2.0*M_PI*SIGK*SIGK);

      break;

    }

  }

  return p;
}

CacheGalaxyModel* GalaxyModelOneDHashed::compute_bins
(const coordPair& P, SampleDistribution *d)
{
  double sech, R, z, fac, dens;

  CacheGalaxyModel *p;
  mmapGalCM::iterator it = cache.find(P);
				// Is it in the cache?
  if (it == cache.end()) {
    p = generate(P, d);		// No.
  }  else {
    p = cache[P];		// Yes.
  }


				// Zero out theoretical bins
  p->bins = vector<double>(nbins, 0.0);

  for (int n=1; n<=NUM; n++) {
				// Coordinates
    fac = p->fac[n-1];
    R = p->R[n-1];
    z = p->z[n-1];
				// Loop over each components
    for (int k=0; k<Mcur; k++) {
				// Disk model
      sech = 2.0/(exp(z*1.0e3/pt[k][1]) + exp(-z*1.0e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	p->bins[j] += wt[k]*fac*dens*p->work[n-1][j];
    }
  }

  return p;
}

double GalaxyModelOneDHashed::NormEval(double L, double B, SampleDistribution *sd)
{
  if (!good_bounds) return 1.0;

  coordPair P(L, B);
  CacheGalaxyModel *p = compute_bins(P, sd);
  lastP = P;

  double norm = 0.0;
  for(int j=0; j<nbins; j++) norm += p->bins[j];

  return norm;
}




vector<double> GalaxyModelOneDHashed::Evaluate(double L, double B, 
					 SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);

  CacheGalaxyModel *p;
  eqcoord eq;

  coordPair P(L, B);
  if (eq(P, lastP))		// Was this just done?
    p = cache[P];		// Then it's current in the cache
  else
    p = compute_bins(P, d);	// Otherwise, recompute
  
  return p->bins; 
}
