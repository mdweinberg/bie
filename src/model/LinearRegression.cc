#include <new>
#include <typeinfo>

using namespace std;

#include <LinearRegression.h>
#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::LinearRegression)

using namespace BIE;

const char* LinearRegression::FIELDNAME1 = "Intercept";
const char* LinearRegression::FIELDNAME2 = "Slope";
const char* LinearRegression::FIELDNAME3 = "Variance";
const char* LinearRegression::PARAM_NAMES[] = {FIELDNAME1, FIELDNAME2, FIELDNAME3};

LinearRegression::LinearRegression(int pos1, int pos2, string filename)
{
  // Set columns
  //
  col1 = pos1;
  col2 = pos2;

  // Open data file
  //
  ifstream in(filename.c_str());
  if (!in) {
    throw FileOpenException(filename, errno, __FILE__, __LINE__);
  }

  // Read data file
  //
  const unsigned lsiz = 2048;
  unsigned minf = INT_MAX;	// For sanity checking
  char *line = new char [lsiz];	// Create a line buffer
  do {				// Read a line
    in.getline(line, lsiz);
    if (in.good()) {
      istringstream sin(line);
      vector<double> dat;
      double value;
      while (sin.good()) {	// Parse the line into fields
	sin >> value;
	dat.push_back(value);
      }
				// Load a record
      if (dat.size()) {
	data.push_back(dat);
	minf = min<unsigned>(minf, dat.size());
      }
    }
  } while (in.good());

				// Delete the line buffer
  delete [] line;
				// Sanity check on requested column indices
				// 
  if (max<unsigned>(col1, col2) >= minf) {
    throw BadRangeException(0, minf-1, __FILE__, __LINE__);
  }

  dmean = vector<double>(minf, 0.0);
  double norm = 1.0/data.size();
  for (unsigned j=0; j<data.size(); j++) {
    for (unsigned i=0; i<minf; i++) dmean[i] += norm*data[j][i];
  }

}

// Used to label output
vector<string> LinearRegression::ParameterLabels()
{
  vector<string> ret;

  ret.push_back(FIELDNAME1);
  ret.push_back(FIELDNAME2);
  ret.push_back(FIELDNAME3);

  return ret;
}

double LinearRegression::LocalLikelihood(State *s)
{
  // Position one in State is # of mixture components, ignored
  // Position two in State is is the mixture weight,   ignored
  double b0 = (*s)[0];
  double b1 = (*s)[1];
  double s2 = (*s)[2];

  double z2 = 0.0, f;
  for (unsigned j=0; j<data.size(); j++) {
    f = data[j][col2]  - b0 - b1*(data[j][col1] - dmean[col1]);
    z2 += f*f;
  }

  return -z2/(2.0*s2) - 0.5*log(2.0*M_PI*s2)*data.size();
}

