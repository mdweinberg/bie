#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

#include <BIEException.h>
#include <GalaxyModelND.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GalaxyModelND)

union coordUnion {
  float z;
  unsigned int i;
  unsigned char c[4];
};

size_t coordHash(coordPair P) {
  coordUnion u1, u2, u;
  u1.z = P.first;
  u2.z = P.second;

  u.c[0] = u1.c[1];
  u.c[1] = u2.c[1];
  u.c[2] = u1.c[2];
  u.c[3] = u2.c[2];
  
  return u.i*0x9e3779b1u;
}


using namespace BIE;

double GalaxyModelND::A1        = 20.0;
double GalaxyModelND::Z1        = 100.0;
int    GalaxyModelND::NUM       = 800;
double GalaxyModelND::ALPHA     = 0.0;
double GalaxyModelND::BETA      = 0.0;
double GalaxyModelND::R0        = 8.0;
double GalaxyModelND::RMAX      = 50.0;
double GalaxyModelND::AK        = 0.1;
double GalaxyModelND::AMIN      = 0.2;
double GalaxyModelND::AMAX      = 8.0;
double GalaxyModelND::HMIN      = 50.0;
double GalaxyModelND::HMAX      = 2000.0;
double GalaxyModelND::Log10     = log(10.0);
double GalaxyModelND::onedeg    = M_PI/180.0;
double GalaxyModelND::K0        = -4.0;
string GalaxyModelND::BASISDATA = "basisdata.nd";
double GalaxyModelND::LMAG      = 6.0;
double GalaxyModelND::HMAG      = 16.0;

const char* GalaxyModelND::LENGTH_FIELDNAME = "Length";
const char* GalaxyModelND::HEIGHT_FIELDNAME = "Height";
const char* GalaxyModelND::PARAM_NAMES[] = {LENGTH_FIELDNAME, HEIGHT_FIELDNAME};


GalaxyModelND::GalaxyModelND(int ndim, int mdim, SampleDistribution* _dist)
{
  Ndim = ndim;
  M    = mdim;
  Mcur = mdim;

				// Create record type describing parameters.
				//
  parametertype = createMixParameterType(PARAM_NAMES, Ndim, mdim);

				// Mixture data storage
				// 
  wt = vector<double>(M);
  pt = vector< vector<double> >(M);
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

				// Read in basis parameters
				// 
  ifstream in(BASISDATA.c_str());
  if (!in) { throw FileOpenException(BASISDATA, errno,__FILE__, __LINE__); }

  const int linesize = 512;
  char line[linesize];
  // Get number of basis elements
  {
    in.getline(line, linesize);
    string lineString(line);
    istringstream ins(lineString);
    ins >> nparam;
  }
  // Get number of fluxes
  {
    in.getline(line, linesize);
    string lineString(line);
    istringstream ins(lineString);
    ins >> Nflux;
  }
  // Make vector of vectors
  for (int j=0; j<nparam; j++) {
    dvector t;
    pos.push_back(t);
    sig.push_back(t);
  }
  // Read components
  for (int i=0; i<nparam; i++) {

    in.getline(line, linesize);
    string lineString(line);
    istringstream ins(lineString);
    double val;
				// Positions
    for (int j=0; j<Nflux; j++) {
      ins >> val;
      pos[i].push_back(val);
    }
				// Dispersions
    for (int j=0; j<Nflux; j++) {
      ins >> val;
      sig[i].push_back(val);
    }
				// Weight
    ins >> val;		
    w.push_back(val);
  }

  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
    
				// Sanity check
    if (histo->getdim(0) != Nflux) {
      ostringstream msg;
      msg << "GalaxyModelND: BinnedDistribution is not " << histo->getdim(0)
	  << " dimensional, the model has " << Nflux << " fluxes";
      throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
    }
    

				// Create bin boundary vectors
    for (int j=0; j<Nflux; j++) {
      dvector t;
      lowb.push_back(t);
      highb.push_back(t);
    }
				// Cache the bin boundaries for the data
				// histogram
				//
    nbins = histo->numberData(); 
    for (int i=0; i<nbins; i++) {
      for (int j=0; j<Nflux; j++) {
	lowb[j].push_back(histo->getLow(i)[j]);
	highb[j].push_back(histo->getHigh(i)[j]);
      }
    }
  
  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    PointDistribution *_point = (PointDistribution*)_dist;
    type = point;

    nbins = 1;
    flux = _point->Point();

    for (int j=0; j<Nflux; j++) {
      lolim.push_back(LMAG);
      hilim.push_back(HMAG);
    }
  } else if (dynamic_cast <NullDistribution*> (_dist)) {
    throw InappropriateDistributionException("NullDistribution", 
					     __FILE__, __LINE__);
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);

				// Default line-of-sight integration knots
				//  
  intgr = new JacoQuad(NUM, ALPHA, BETA);

  cache_limit = 0;
  current = 0;
}

GalaxyModelND::~GalaxyModelND()
{
  delete intgr;
				// Delete the cache
				// 
  for (mit=cache.begin();  mit!=cache.end(); mit++) 
    delete mit->second;
}


void GalaxyModelND::manageCache(coordPair& P)
{
  if (cache_limit) {

    // Save element
    cacheList.push_back(P);

    // FIFO delete elements
    while (cacheList.size() > cache_limit) {
				// Find the element to be deleted
      mit = cache.find(cacheList.front());
				// If it exists, delete it!
      if (mit != cache.end()) {
	delete mit->second;
	cache.erase(mit);
	cacheList.pop_front();
      } else {
	string msg("GalaxyModelND cache is inconsistent");
	throw InternalError(msg, __FILE__, __LINE__);
      }
      
    }

  }

}

				// Model: exponential * sech^2 disk

vector<string> GalaxyModelND::ParameterLabels()
{
  vector<string> ret;
  ret.push_back(LENGTH_FIELDNAME);
  ret.push_back(HEIGHT_FIELDNAME);
  return ret;
}


/** Compute normalization, exploit bisymmetry

    Use Gaussian quadrature
*/

void GalaxyModelND::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s.Phi(k, j);
  }

  check_bounds();
}

void GalaxyModelND::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}

void GalaxyModelND::check_bounds()
{
  good_bounds = true;
				// Check bounds
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;
  }
  for (int k=0; k<Mcur; k++) {
    if (pt[k][0] > AMAX || pt[k][0] < AMIN   ||
	pt[k][1] > HMAX || pt[k][1] < HMIN    )  good_bounds = false;
  }

				// DEBUG
  if (good_bounds == false) {
    cout << "State rejected (" << Mcur << "):\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<2; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }

}

void GalaxyModelND::generate(double L, double B, SampleDistribution *sd)
{
  vector<double> mm(Nflux);
  double s, R, z, fac;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

				// Extinction slab
  double smaxr, smaxz, smax;
  if (fabs(cosB)<1.0e-8) 
    smaxr = 1.0e30;
  else
    smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));

  if (fabs(sinB)<1.0e-8) 
    smaxz = 1.0e30;
  else
    smaxz = Z1*1.0e-3/fabs(sinB);

  smax = min<double>(smaxr, smaxz);

  current->work = vector<rvector>(NUM);
  current->R    = vector<real>(NUM);
  current->z    = vector<real>(NUM);
  current->fac  = vector<real>(NUM);

				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    s = intgr->knot(n) * RMAX;
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;
    fac = s*s*intgr->weight(n) * RMAX;

    current->work[n-1] = vector<real>(nbins, 0.0);
    current->R[n-1] = R;
    current->z[n-1] = z;
    current->fac[n-1] = fac;

    for (int k=0; k<nparam; k++) {
      
				// Make fluxes for each component
      for (int l=0; l<Nflux; l++)
	mm[l] = pos[k][l] 
	  + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

      switch (type) {

      case binned:
	
				// Bin visibility
	for (int j=0; j<nbins; j++) {

	  double val = 1.0;
	  
	  for (int l=0; l<Nflux; l++) {
	    val *= 0.5*
	      (
	       erf( (highb[l][j] - mm[l])/(M_SQRT2*sig[k][l]) )
	       -
	       erf( ( lowb[l][j] - mm[l])/(M_SQRT2*sig[k][l]) )
	       );
	  }
	
	  current->work[n-1][j] += w[k]*val;
	}

	break;

      case point:

	PointDistribution *pt = (PointDistribution*)sd;
	double val = 1.0;
	  
	for (int l=0; l<Nflux; l++)
	  val *= 
	    exp( -(pt->Point()[l] - mm[l])*(pt->Point()[l] - mm[l])/
		 (2.0*sig[k][l]) )
	    / sqrt(2.0*M_PI*sig[k][l]);
	
	current->work[n-1][0] += w[k]*val;
      }

    }

  }

}

void GalaxyModelND::compute_bins()
{
  double sech, R, z, fac, dens;
				// Zero out theoretical bins
  current->bins = vector<double>(nbins, 0.0);

  for (int n=1; n<=NUM; n++) {
				// Coordinates
    fac = current->fac[n-1];
    R = current->R[n-1];
    z = current->z[n-1];
				// Loop over each components
    for (int k=0; k<Mcur; k++) {
				// Disk model
      sech = 2.0/(exp(z*1.0e3/pt[k][1]) + exp(-z*1.0e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	current->bins[j] += wt[k]*fac * dens * current->work[n-1][j];
    }
  }

}


double GalaxyModelND::NormEval(double L, double B)
{
  vector<double> mm(Nflux);
  double s, R, z, fac;
  double cosL = cos(L);
  double sinL = sin(L);
  double cosB = cos(B);
  double sinB = sin(B);

				// Extinction slab
  double smaxr, smaxz, smax;
  if (fabs(cosB)<1.0e-8) 
    smaxr = 1.0e30;
  else
    smaxr = R0/cosB * (cosL + sqrt(A1*A1/(R0*R0) - sinL*sinL));

  if (fabs(sinB)<1.0e-8) 
    smaxz = 1.0e30;
  else
    smaxz = Z1*1.0e-3/fabs(sinB);

  smax = min<double>(smaxr, smaxz);


  double sech, dens, sum1, sum0 = 0.0;

				// Begin integration over line of sight
  for (int n=1; n<=NUM; n++) {

    s = intgr->knot(n) * RMAX;
    R = sqrt(R0*R0 + s*s*cosB*cosB - 2.0*R0*s*cosB*cosL);
    z = s*sinB;
    fac = s*s*intgr->weight(n) * RMAX;

    sum1 = 0.0;

    //
    // Basis components
    //
    for (int k=0; k<nparam; k++) {
				// Make fluxes for each component
				// 
      for (int l=0; l<Nflux; l++)
	mm[l] = pos[k][l] 
	  + 5.0*log(1.0e3*s)/Log10 - 5.0 + AK*min<double>(s, smax);

	
      // Visibility
      //
      double val = 1.0;
	  
      for (int l=0; l<Nflux; l++) {
	val *= 0.5*
	  (
	   erf( (hilim[l] - mm[l])/(M_SQRT2*sig[k][l]) )
	   -
	   erf( (lolim[l] - mm[l])/(M_SQRT2*sig[k][l]) )
	   );
      }

      sum1 += w[k]*val;
    }
	
				// Disk model
				// ----------
				// Loop over each component
    for (int k=0; k<Mcur; k++) {
      sech = 2.0/(exp(z*1.0e3/pt[k][1]) + exp(-z*1.0e3/pt[k][1]));
      dens = exp(-R/pt[k][0])*sech*sech/(pt[k][0]*pt[k][0]*pt[k][1]);
      for (int j=0; j<nbins; j++) 
	sum0 += wt[k]*fac * dens * sum1;
    }
  }

  return sum0;
}


double GalaxyModelND::NormEval(double L, double B, SampleDistribution *d)
{
  if (!good_bounds) return 1.0;

  coordPair P(L, B);

  if ((mit=cache.find(P)) == cache.end()) {

    current = new CacheGalaxyModel(L, B);
    cache[P] = current;
    generate(L, B, d);

    manageCache(P);

  } else {
    current = mit->second;
  }

  compute_bins();

  double norm = 0.0;
  for (int j=0; j<nbins; j++)  norm += current->bins[j];

  return norm;
}




vector<double> GalaxyModelND::Evaluate(double L, double B, 
				       SampleDistribution *d)
{
  if (!good_bounds) return vector<double>(nbins, 0.0);

  coordPair P(L, B);

  if ( (mit=cache.find(P)) == cache.end()) {
  
    current = new CacheGalaxyModel(L, B);
    cache[P] = current;
    generate(L, B, d);
    compute_bins();
  
    manageCache(P);

  } else {

    current = mit->second;
    if (type==point) compute_bins();

  }
  
  int n = current->bins.size();
  if (work.size() != (unsigned int) n) work = vector<double>(n);
  for (int i=0; i<n; i++) work[i] = current->bins[i];

  return work;
}


