#include <cmath>

#include <SplatModel3dv.h>
#include <Distribution.h>
#include <BIEException.h>

#include <new>
#include <typeinfo>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SplatModel3dv)

using namespace BIE;

double SplatModel3dv::SIGX = 0.1;
double SplatModel3dv::SIGY = 0.1;
double SplatModel3dv::V1 = 1.0;
double SplatModel3dv::V2 = 1.0;
double SplatModel3dv::V3 = 1.0;

// Constants related to the record type of this model.
const char* SplatModel3dv::XPOS_FIELDNAME       = "X pos";
const char* SplatModel3dv::YPOS_FIELDNAME       = "Y pos";
const char* SplatModel3dv::BANDONE_FIELDNAME    = "Band One";
const char* SplatModel3dv::BANDTWO_FIELDNAME    = "Band Two";
const char* SplatModel3dv::BANDTHREE_FIELDNAME  = "Band Three";
const char*  SplatModel3dv::PARAM_NAMES [] =
 {XPOS_FIELDNAME, YPOS_FIELDNAME, 
  BANDONE_FIELDNAME, BANDTWO_FIELDNAME, BANDTHREE_FIELDNAME};


SplatModel3dv::SplatModel3dv(int mdim, double fluxmin, double fluxmax)
{
  // Create record type describing parameters.
  parametertype = createMixParameterType(PARAM_NAMES, 5, mdim);

  Ndim = 5;
  M = mdim;	// M is the maximum number of elements in the mixture.
  Mcur = M;			// Mcur is the current number of elements

				// Min and max for flux grid
  fmin = fluxmin;
  fmax = fluxmax;
  
				// Internal model arrays
  wt = vector<double>(M);
  xx = vector<double>(M);
  yy = vector<double>(M);
  sx = vector<double>(M);
  sy = vector<double>(M);
  a1 = vector<double>(M);
  a2 = vector<double>(M);
  a3 = vector<double>(M);

				// This are not variables but fixed
				// constants (at this point . . .)
  for (int m=0; m<M; m++) {
    sx[m] = SIGX*SIGX;
    sy[m] = SIGY*SIGY;
  }
}


vector<string> SplatModel3dv::ParameterLabels()
{
  vector<string> ret;
  for (int j=0; j<Ndim; j++) ret.push_back(PARAM_NAMES[j]);
  return ret;
}


/** Compute normalization
*/

void SplatModel3dv::Initialize(State& s)
{
  Mcur = s.M();
  for (int k=0; k<Mcur; k++) wt[k] = s.Wght(k);
  for (int k=0; k<Mcur; k++) {
    xx[k] = s.Phi(k, 0);
    yy[k] = s.Phi(k, 1);
    a1[k] = s.Phi(k, 2);
    a2[k] = s.Phi(k, 3);
    a3[k] = s.Phi(k, 4);
  }
}

void SplatModel3dv::Initialize(vector<double>& w, vector< vector<double> >& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    xx[k] = p[k][0];
    yy[k] = p[k][1];
    a1[k] = p[k][2];
    a2[k] = p[k][3];
    a3[k] = p[k][4];
  }
}

double SplatModel3dv::NormEval(double xmin, double xmax, 
			       double ymin, double ymax)
{
  double ans = 0.0;

  for (int k=0; k<Mcur; k++) {
				// Position distribution
    ans += wt[k] * 0.25 *
      ( erf( (xmax - xx[k])/sqrt(2.0*sx[k]) ) -
	erf( (xmin - xx[k])/sqrt(2.0*sx[k]) ) ) *
      ( erf( (ymax - xx[k])/sqrt(2.0*sy[k]) ) -
	erf( (ymin - xx[k])/sqrt(2.0*sy[k]) ) ) *

      0.5*( erf((fmax - a1[k])/sqrt(2.0*V1)) + 
	    erf((a1[k] - fmin)/sqrt(2.0*V1)) ) *
      0.5*( erf((fmax - a2[k])/sqrt(2.0*V2)) +
	    erf((a2[k] - fmin)/sqrt(2.0*V2)) ) *
      0.5*( erf((fmax - a3[k])/sqrt(2.0*V3)) +
	    erf((a3[k] - fmin)/sqrt(2.0*V3)) ) ;
  }

  return ans;
}


double SplatModel3dv::NormEval(double x, double y, SampleDistribution *d)
{
  double ans = 0.0;

  if (dynamic_cast <PointDistribution*> (d)) return ans;

  for (int k=0; k<Mcur; k++)
				// Position distribution
    ans += wt[k] * exp(
		       -0.5*(x-xx[k])*(x-xx[k])/sx[k]
		       -0.5*(y-yy[k])*(y-yy[k])/sy[k]
		       ) / (2.0*M_PI*sqrt(sx[k]*sy[k]))
      *				// Bin normalization
      0.5*( erf((fmax - a1[k])/sqrt(2.0*V1)) + 
	    erf((a1[k] - fmin)/sqrt(2.0*V1)) ) *
      0.5*( erf((fmax - a2[k])/sqrt(2.0*V2)) +
	    erf((a2[k] - fmin)/sqrt(2.0*V2)) ) *
      0.5*( erf((fmax - a3[k])/sqrt(2.0*V3)) +
	    erf((a3[k] - fmin)/sqrt(2.0*V3)) ) ;

  return ans;
}


vector<double> SplatModel3dv::EvaluateBinned(double x, double y, 
					     BinnedDistribution *d)
{
  vector<double> ret(d->numberData(), 0.0);
  double tmp;

  // Compute contribution to
  // distribution in each dimension
  for (int k=0; k<Mcur; k++) {
      
    // Positional info
    tmp = exp(
	      -0.5*(x-xx[k])*(x-xx[k])/sx[k]
	      -0.5*(y-yy[k])*(y-yy[k])/sy[k]
	      ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
    
    for (int j=0; j<d->numberData(); j++) {
      
      // Flux bin info
      ret[j] += tmp * wt[k] *
	0.5*( erf((d->getHigh(j)[0] - a1[k])/sqrt(2.0*V1)) + 
	      erf((a1[k] -  d->getLow(j)[0])/sqrt(2.0*V1)) ) *
	0.5*( erf((d->getHigh(j)[1] - a2[k])/sqrt(2.0*V2)) +
	      erf((a2[k] -  d->getLow(j)[1])/sqrt(2.0*V2)) ) *
	0.5*( erf((d->getHigh(j)[2] - a3[k])/sqrt(2.0*V3)) +
	      erf((a3[k] -  d->getLow(j)[2])/sqrt(2.0*V3)) ) ;
      
    }
  }

  return ret;
}

vector<double> SplatModel3dv::EvaluatePoint(double x, double y, 
					    PointDistribution *d)
{
  vector<double> ret(d->numberData(), 0.0);
  double tmp;

  // Compute contribution to
  // distribution in each dimension
  for (int k=0; k<Mcur; k++) {
      
    // Positional info
    tmp = exp(
	      -0.5*(x-xx[k])*(x-xx[k])/sx[k]
	      -0.5*(y-yy[k])*(y-yy[k])/sy[k]
	      ) / (2.0*M_PI*sqrt(sx[k]*sy[k]));
    
    for (int j=0; j<d->numberData(); j++) {
      
				// Flux bin info
      ret[j] += tmp * wt[k] * 
	exp(
	    -(d->Point()[0] - a1[k])*(d->Point()[0] - a1[k])/(2.0*V1)
	    -(d->Point()[1] - a2[k])*(d->Point()[1] - a2[k])/(2.0*V2)
	    -(d->Point()[2] - a3[k])*(d->Point()[2] - a3[k])/(2.0*V3)
	    ) / sqrt(2.480502134423985e+02*V1*V2*V3);
    }
  }

  return ret;
}

