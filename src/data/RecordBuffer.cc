#include <RecordBuffer.h>
#include <RecordType.h>
#include <BasicType.h>
#include <TypedBuffer.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordBuffer)

using namespace BIE;

/*******************************************************************************
* Object lifetime - constructors and destructors.
*******************************************************************************/
RecordBuffer::RecordBuffer() { record_type = new RecordType(); }

RecordBuffer::RecordBuffer
(RecordType * recordtype)
{
  // Create a new copy of the record type for this object.
  record_type = new RecordType(recordtype);

  // Create new buffers for each field.
  for (int fieldindex = 1; fieldindex <= recordtype->numFields(); fieldindex++)
  {
    TypedBuffer * newbuffer = TypedBuffer::createNew
                             (recordtype->getFieldType(fieldindex));
    
    record_buffers.push_back(newbuffer);
  }
}

RecordBuffer::~RecordBuffer() { delete record_type; }

/*******************************************************************************
* Operations to add, delete, move and rename fields.
*******************************************************************************/
RecordBuffer * RecordBuffer::deleteField 
(string fieldname)
{
  // Get the field index of the name and delegate to delete field by index.
  int fieldindex         = getFieldIndex(fieldname);  
  return deleteField(fieldindex);
} 

RecordBuffer * RecordBuffer::deleteField 
(int fieldindex)
{
  // Delegate to delete range. 
  return deleteRange(fieldindex, fieldindex); 
}

RecordBuffer * RecordBuffer::deleteRange
(int startindex, int endindex)
{
  RecordBuffer * result  = new RecordBuffer();
  // Create a new record type with the specified fields removed.
  // Field indice range checks are done by this call.
  (result->record_type) = record_type->deleteRange(startindex,endindex);

  // Copy the vector holding the record buffers.
  result->record_buffers = record_buffers;

  // Vectors start their index at zero, so decrement both indices.
  startindex--;
  endindex--;
  
  // Delete the references to the buffers we do not need.
  // Indices are funky because the vectors rearrange themselves. 
  int deleteindex = startindex;
  for (;startindex <= endindex; startindex++)
  {
    (result->record_buffers).erase
      ((result->record_buffers).begin()+deleteindex);  
  }
  
  return result;
}

RecordBuffer * RecordBuffer::insertField
(int position, string name, BasicType * type)
{
  RecordBuffer * result  = new RecordBuffer();
 
  // Create a new record type with the new field inserted.
  result->record_type = record_type->insertField (position, name, type);

  // Copy (inherit) the record buffer vector in this object
  result->record_buffers = record_buffers;
 
  // Create a new typed buffer for the new field.
  TypedBuffer * newbuffer = TypedBuffer::createNew(type);
  
  // Insert the new buffer.
  (result->record_buffers).insert
    ((result->record_buffers.begin() + position - 1), newbuffer);

  return result;    
}

RecordBuffer * RecordBuffer::insertRecord
(int position, RecordBuffer * record)
{
  // Create a new Record buffer.
  RecordBuffer * result  = new RecordBuffer();

  // Create a new record type with the new record type inserted.
  result->record_type = record_type->insertRecord(position, record->getType());

  // Copy (inherit) the record buffer vector in the existing object
  result->record_buffers = record_buffers;
  
  for (int fieldindex = 1; fieldindex <= record->numFields(); fieldindex++)
  {    
    TypedBuffer * buffer = record->getFieldBuffer(fieldindex);
    int offset = fieldindex + position - 2;
    
    // Insert the buffer from the record being inserted.
    (result->record_buffers).insert
      ((result->record_buffers.begin() + offset), buffer);
  }

  return result;    
}      

RecordBuffer * RecordBuffer::selectFields
(RecordType * selection)
{
  // Check that the specified type is a subset of the existing type.
  if (!record_type->isSubset(selection))
  { throw TypeException(selection,record_type,__FILE__, __LINE__); }
  
  // Get a list of the field names being selected.
  vector<string> fieldnames;
  for (int selectindex = 1;selectindex <= selection->numFields();selectindex++)
  { fieldnames.push_back(selection->getFieldName(selectindex)); }
  
  // Delegate to the select fields by name method.
  RecordBuffer * result = selectFields(&fieldnames);
  
  return result;  
}

RecordBuffer * RecordBuffer::selectFields
(vector<string>* selection)
{
  vector<int> indices; 
 
  for(int selectindex = 0; selectindex < (int)selection->size(); selectindex++)
  {
    // Get the field index of this name in this buffers type descriptor.
    int fieldindex = getFieldIndex((*selection)[selectindex]);
  
    // Append the index to our list.
    indices.push_back(fieldindex);
  }
  
  // Delegate to the select fields by index method.
  RecordBuffer * result = selectFields(&indices);

  return result;
}

RecordBuffer * RecordBuffer::selectFields
(vector<int>* selection)
{
  RecordBuffer * result = new RecordBuffer();
  
  // Create the new record type.  This call also does range checks
  // on indices and looks for duplicates.
  result->record_type = record_type->selectFields(selection);
  
  // Create the new vector of inherited field buffers.
  for (int selectindex = 0; selectindex < (int)selection->size(); selectindex++)
  { 
    int fieldindex = (*selection)[selectindex];
    TypedBuffer * buffer = record_buffers[fieldindex-1];
    (result->record_buffers).push_back(buffer);
  }

  return result;  
}

RecordBuffer * RecordBuffer::renameField
(string oldname, string newname)
{
  // Get the index for the name and delegate to rename field by index.
  int fieldindex        = getFieldIndex(oldname);
  RecordBuffer * result = renameField(fieldindex, newname);
  return result;
}

RecordBuffer * RecordBuffer::renameField
(int fieldindex, string newname)
{
  // Create a new Record buffer.
  RecordBuffer * result  = new RecordBuffer();
  
  // Copy (inherit) the record buffer vector in existing object
  result->record_buffers = record_buffers;

  // Create a new record type with the specified field renamed.
  result->record_type = record_type->renameField(fieldindex, newname); 

  return result;
}

RecordBuffer * RecordBuffer::moveField
(string fieldname, int newposition)
{
  // Get the index for the name and delegate to "move to by index".
  int oldposition = getFieldIndex(fieldname);
  RecordBuffer * result = moveField(oldposition, newposition);
  return result;
}

RecordBuffer * RecordBuffer::moveField
(int oldposition, int newposition)
{
  RecordBuffer * result = new RecordBuffer();

  // Call the corresponding method on the result's type.
  result->record_type = record_type->moveField(oldposition, newposition);

  // Copy (inherit) the record buffer vector in existing object
  result->record_buffers = record_buffers;

  // Get a reference to the buffer being moved.
  TypedBuffer * buffer   = getFieldBuffer(oldposition);

  // Vector indices are one lower than field indices.
  oldposition--;
  newposition--;

  // delete the old position.
  (result->record_buffers).erase
    ((result->record_buffers.begin() + oldposition));

  // Now insert the field in the new position 
  (result->record_buffers).insert
    ((result->record_buffers.begin() + newposition), buffer);

  return result;
}
  
/*******************************************************************************
* getFieldBuffer methods.
*******************************************************************************/
TypedBuffer * RecordBuffer::getFieldBuffer
(string fieldname)
{
  int fieldindex       = getFieldIndex(fieldname);
  TypedBuffer * result = getFieldBuffer(fieldindex);
  return result;
}
  

TypedBuffer * RecordBuffer::getFieldBuffer
(int fieldindex)
{
  if (!isValidFieldIndex(fieldindex))
  { throw NoSuchFieldException(fieldindex, __FILE__, __LINE__); }
  
  return record_buffers[fieldindex-1];
}

/*******************************************************************************
* Value retrieval.
*******************************************************************************/
string RecordBuffer::getStringValue(int fieldindex)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  return (record_buffers[fieldindex-1])->getStringValue();
}

int RecordBuffer::getIntValue (int fieldindex)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  return (record_buffers[fieldindex-1])->getIntValue();
}

double RecordBuffer::getRealValue(int fieldindex)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  return (record_buffers[fieldindex-1])->getRealValue();
}

bool RecordBuffer::getBoolValue (int fieldindex)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  return (record_buffers[fieldindex-1])->getBoolValue();
}

string RecordBuffer::getStringValue(string fieldname)
{
  int fieldindex = getFieldIndex(fieldname);
  return getStringValue(fieldindex);
}

int RecordBuffer::getIntValue(string fieldname)
{
  int fieldindex = getFieldIndex(fieldname);
  return getIntValue(fieldindex);
}

double RecordBuffer::getRealValue  (string fieldname)
{
  int fieldindex = getFieldIndex(fieldname);
  return getRealValue(fieldindex);
}

bool RecordBuffer::getBoolValue  (string fieldname)
{
  int fieldindex = getFieldIndex(fieldname);
  return getBoolValue(fieldindex);
}

vector<int> RecordBuffer::getIntArrayValue(int fieldindex)
{
  if (!isValidFieldIndex(fieldindex)) 
    {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  return (record_buffers[fieldindex-1])->getIntArrayValue();
}

vector<double> RecordBuffer::getRealArrayValue(int fieldindex)
{
  if (!isValidFieldIndex(fieldindex)) 
    {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  return (record_buffers[fieldindex-1])->getRealArrayValue();
}

vector<bool> RecordBuffer::getBoolArrayValue(int fieldindex)
{
  if (!isValidFieldIndex(fieldindex)) 
    {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  return (record_buffers[fieldindex-1])->getBoolArrayValue();
}

/*******************************************************************************
* Value setting methods.
*******************************************************************************/
void RecordBuffer::setStringValue(int fieldindex, string value)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  (record_buffers[fieldindex-1])->setStringValue(value);
}

void RecordBuffer::setIntValue (int fieldindex, int  value)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  (record_buffers[fieldindex-1])->setIntValue(value);
}

void RecordBuffer::setRealValue  (int fieldindex, double value)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  (record_buffers[fieldindex-1])->setRealValue(value);
}

void RecordBuffer::setBoolValue  (int fieldindex, bool value)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  (record_buffers[fieldindex-1])->setBoolValue(value);
}

void RecordBuffer::setStringValue(string fieldname, string value)
{
  int fieldindex = getFieldIndex(fieldname);
  setStringValue(fieldindex, value);
}

void RecordBuffer::setIntValue   (string fieldname, int value)
{
  int fieldindex = getFieldIndex(fieldname);
  setIntValue(fieldindex, value);
}

void RecordBuffer::setRealValue  (string fieldname, double value)
{
  int fieldindex = getFieldIndex(fieldname);
  setRealValue(fieldindex, value);
}

void RecordBuffer::setBoolValue  (string fieldname, bool value)
{
  int fieldindex = getFieldIndex(fieldname);
  setBoolValue(fieldindex, value);
}

void RecordBuffer::setIntArrayValue(int fieldindex, vector<int> value)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  (record_buffers[fieldindex-1])->setIntArrayValue(value);
}

void RecordBuffer::setRealArrayValue(int fieldindex, vector<double> value)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  (record_buffers[fieldindex-1])->setRealArrayValue(value);
}

void RecordBuffer::setBoolArrayValue(int fieldindex, vector<bool> value)
{
  if (!isValidFieldIndex(fieldindex)) 
  {throw NoSuchFieldException(fieldindex, __FILE__, __LINE__);}
  (record_buffers[fieldindex-1])->setBoolArrayValue(value);
}

/*******************************************************************************
* Has Value queries.
*******************************************************************************/
bool RecordBuffer::hasValue (string fieldname)
{
  int fieldindex = getFieldIndex(fieldname);
  return hasValue(fieldindex);
}

bool RecordBuffer::hasValue(int fieldindex)
{
  if(!isValidFieldIndex(fieldindex))
  { throw NoSuchFieldException (fieldindex, __FILE__, __LINE__); }
  
  bool result = (record_buffers[fieldindex-1])->hasValue();
  return result;
}

bool RecordBuffer::hasValue()
{
  bool result = true;
  
  // Run through each field in turn seeing if it has a value.
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    if (! ((record_buffers[fieldindex-1])->hasValue()) )
    {
      result = false;
      break;
    }
  }
  
  return result;
}

/*******************************************************************************
* Reset all values. 
*******************************************************************************/
void RecordBuffer::reset()
{
  // Run through each field in turn calling reset.
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    (record_buffers[fieldindex-1])->reset();
  }
}

/*******************************************************************************
* Type query methods
*******************************************************************************/
int RecordBuffer::getFieldIndex (string fieldname)
{ return record_type->getFieldIndex(fieldname);}  

string RecordBuffer::getFieldName (int fieldindex)
{ return record_type->getFieldName(fieldindex);}

bool RecordBuffer::isValidFieldIndex(int fieldindex)
{ return record_type->isValidFieldIndex(fieldindex); }

bool RecordBuffer::isValidFieldName(string fieldname)
{ return record_type->isValidFieldName(fieldname); }

BasicType * RecordBuffer::getFieldType(int fieldindex)
{ return record_type->getFieldType(fieldindex); }

BasicType * RecordBuffer::getFieldType(string fieldname)
{ return record_type->getFieldType(fieldname); }

int RecordBuffer::numFields()
{  return record_type->numFields(); }
  
RecordType * RecordBuffer::getType()
{ return record_type; }

/*******************************************************************************
* To String Method.
*******************************************************************************/
string RecordBuffer::toString()
{
  // Can't concatenate ints/reals to strings in C++, so use a string stream.
  std::ostringstream buffer; 
  
  for (int fieldindex = 1; fieldindex <= numFields();fieldindex++)
  {
    string fieldname          = getFieldName(fieldindex);
    TypedBuffer * fieldbuffer = getFieldBuffer(fieldindex);
    string fieldvalue         = fieldbuffer->toString();
    
    buffer << fieldindex << ". " << fieldname << ": " << fieldvalue << endl;
  }

  // buffer << ends;  // not needed with stringstreams

  return buffer.str();  
}
