#include <RecordStream_NetCDF.h>
#include <BasicType.h>
#include <BIEException.h>
#include <RecordType.h>
#include <RecordBuffer.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordInputStream_NetCDF)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordOutputStream_NetCDF)

using namespace BIE;

/**
This will possibly be useful once I've figured out how to handle streams.
If this comment is lying around for ages, clearly I didn't figure out
how to handle streams: delete it.

IMPLEMENT_PERSISTENCE_NOREADWRITE(BIE,RecordInputStream_NetCDF,RecordInputStream)

void RecordInputStream_NetCDF::Read(ReadEngine &archive)
{
  archive >> risnc_filename;
  initialize();
  delete rs_buffer;  // We don't need this one.

  // rs_buffer restored by this call.
  RecordInputStream::Read(archive);

  archive >> risnc_eof;
  archive >> risnc_error;
  archive >> risnc_recordindex;
}

void RecordInputStream_NetCDF::Write(WriteEngine &archive) const
{
  archive << risnc_filename;

  // Write superclass things, including rs_buffer...
  RecordInputStream::Write(archive);
  
  archive << risnc_eof;
  archive << risnc_error;
  archive << risnc_recordindex;
} 

*/


/*******************************************************************************
* NetCDF Record Input Stream Implementation.
*******************************************************************************/
RecordInputStream_NetCDF::RecordInputStream_NetCDF (string filename)
{
  // Initialize object status variables.
  rs_buffer               = 0;
  risnc_eof               = false;
  risnc_error             = false;
  risnc_filename          = filename;
  risnc_recordindex       = 0;
  use_float               = false;

  initialize();
}

void RecordInputStream_NetCDF::initialize()
{
  RecordType * streamtype = 0;

  try {
    int ncstatus = 0;  // Holds status indicator returned by NetCDF calls.

    // This stream is a root stream.
    this->setAsRoot();
    
    // Open the NetCDF file for reading.
    ncstatus = nc_open(risnc_filename.c_str(), NC_NOWRITE, &risnc_ncid);
    if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
    // Extract the record type from the meta data.  We reject files that don't 
    // have an acceptable structure by throwing an exception
    streamtype = new RecordType();
  
    // Get the number of variables.
    int numvariables;
    ncstatus = nc_inq_nvars(risnc_ncid, &numvariables);
    if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
    // Process each variable in turn to get and check the type of each.
    char    fieldname[NC_MAX_NAME];
    nc_type fieldtype;
    int     fielddim;
    int     dimensionids[NC_MAX_VAR_DIMS];
    int     unlimiteddimid;
    
    for (int varid = 0; varid < numvariables; varid++)
    {
      ncstatus = nc_inq_var(risnc_ncid, varid, fieldname, &fieldtype, 
                            &fielddim, dimensionids, &unlimiteddimid);
      if (ncstatus != NC_NOERR) 
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
      // Check that the variable has exactly one dimension
      if (fielddim != 1)
      {
        throw NetCDFFormatException(fieldname, varid, fielddim, 
                                    risnc_filename, __FILE__, __LINE__);
      }
      
      // Check that the dimension size is set to "unlimited"
      if (unlimiteddimid != dimensionids[0])
      {
        throw NetCDFFormatException(fieldname, varid, risnc_filename, __FILE__, __LINE__);
      }
  
      // Create a new field in record type.
      RecordType * amendedtype;
      if (fieldtype == NC_BYTE)
      {
        amendedtype = streamtype->insertField(varid+1,fieldname,BasicType::Bool);
      }
      else if (fieldtype == NC_INT)
      {
        amendedtype = streamtype->insertField(varid+1,fieldname,BasicType::Int);
      } 
      else if (fieldtype == NC_DOUBLE)
      {
	use_float = false;
        amendedtype = streamtype->insertField(varid+1,fieldname,BasicType::Real);
      }
      else if (fieldtype == NC_FLOAT)
      {
	use_float = true;
        amendedtype = streamtype->insertField(varid+1,fieldname,BasicType::Real);
      }
      else
      {
        // No other types are currently supported.
        throw NetCDFFormatException(fieldname, varid, risnc_filename, 
                                    fieldtype, __FILE__, __LINE__);
      }
      
      delete streamtype;
      streamtype = amendedtype;
    }
  
    // Create a buffer with the specified type.
    rs_buffer = new RecordBuffer(streamtype);
  
    // We don't need the type any more.
    delete streamtype;
    streamtype = 0;
  
    // Get the number of records in the NetCDF file.
    ncstatus = nc_inq_unlimdim(risnc_ncid, &unlimiteddimid);
    if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
       
    ncstatus = nc_inq_dimlen(risnc_ncid, unlimiteddimid, &risnc_numrecords);
    if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  }
  catch (BIEException &e)
  {
    // Clean things up and set in an unusable state if there was an exception.
    if (rs_buffer) { delete rs_buffer; }
    if (streamtype) { delete streamtype; }
    risnc_error = true;
  
    // Rethrow exception.
    throw;
  }
}
  
RecordInputStream_NetCDF::~RecordInputStream_NetCDF()
{ 
  // Close NetCDF file if we haven't already reached the end.
  // Ignore closing errors - there really isn't much we can do.
  if (! risnc_eof)
  {
    nc_close(risnc_ncid);
  }
}

bool RecordInputStream_NetCDF::eos() {return risnc_eof; }
  
bool RecordInputStream_NetCDF::error() {return risnc_error; }

bool RecordInputStream_NetCDF::nextRecord() 
{
  bool result = true;
  int ncstatus = 0;
  
  if (risnc_eof || risnc_error)
  {
    result = false;
  }
  else if (risnc_recordindex >= risnc_numrecords)
  {
    // We have already processed the last record in the NetCDF file.
    risnc_eof = true;
    result    = false;
    
    // Close NetCDF file, since we are completely finished with it.
    ncstatus = nc_close(risnc_ncid);
    if (ncstatus != NC_NOERR) 
    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  }
  else
  {
    try 
    {
      for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
      {
        BasicType * fieldtype = getFieldType(fieldindex );
        int varid = fieldindex - 1;  // NetCDF var ids start at 0.
	
        if (fieldtype->equals(BasicType::Int))
        {
          int int_value;
          ncstatus = nc_get_var1_int(risnc_ncid, varid, 
	                             &risnc_recordindex, &int_value);
          if (ncstatus != NC_NOERR) 
          { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

          rs_buffer->setIntValue(fieldindex, int_value);
        }
        else if (fieldtype->equals(BasicType::Bool))
        {
          unsigned char bool_value;
          ncstatus = nc_get_var1_uchar(risnc_ncid, varid, 
	                               &risnc_recordindex, &bool_value);
          if (ncstatus != NC_NOERR) 
          { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
          
          rs_buffer->setBoolValue(fieldindex, (bool) bool_value);
        }
        else if (fieldtype->equals(BasicType::Real))
        {
	  double dbl_value;

	  if (use_float) {
	    float flt_value;
	    ncstatus = nc_get_var1_float(risnc_ncid, varid, 
					 &risnc_recordindex, &flt_value);
	    dbl_value = flt_value;
	    
	  } else {
	    ncstatus = nc_get_var1_double(risnc_ncid, varid, 
					  &risnc_recordindex, &dbl_value);
	  }

	  if (ncstatus != NC_NOERR) 
	    { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

          rs_buffer->setRealValue(fieldindex, dbl_value);
        }
        else
        {
          throw InternalError(__FILE__, __LINE__);
        }
      }
      
      risnc_recordindex++;
    }
    catch (BIEException &e)
    {
      risnc_error = true;
      risnc_eof   = true;
      result     = false;   
      
      // Reset the buffer to forget all the previous values.
      rs_buffer->reset();

      // Close NetCDF file, since we are completely finished with it.
      // Ignore closing errors - we already have one exception to report.
      nc_close(risnc_ncid);

      throw;  // Rethrows the original exception.
    }
  }

  if (!result)
  {
    // Reset the buffer to forget all the previous values.
    rs_buffer->reset();
  }
  
  return result;
}

string RecordInputStream_NetCDF::toString()
{
  ostringstream buffer;
  buffer << "NetCDF Record Input Stream" << endl;
  buffer << "Name of NetCDF file: " << risnc_filename << endl;
  buffer << "Record Type: " << endl << getType();
  buffer << "Number of records in file: " << risnc_numrecords << endl;
  buffer << "Number of records read: " << risnc_recordindex << endl;
  buffer << "Error Status (0 indicates no error): " << risnc_error << endl;
  buffer << "EOF Status (0 indicated not at EOF): " << risnc_eof << endl;

  return buffer.str();
}

/*******************************************************************************
* NetCDF Record Output Stream Implementation.
*******************************************************************************/
RecordOutputStream_NetCDF::RecordOutputStream_NetCDF 
(RecordOutputStream * stream, string filename)
{
  int ncstatus;      // Variable used to catch NetCDF return codes.
  int unlimiteddim;  // Identifies unlimited dimension we define in NetCDF file
  rosnc_recordindex = 0;   
  rosnc_filename    = filename;
  use_float         = false;
 
  // We don't need our own buffer: reference buffer in stream we are inheriting.
  rs_buffer = stream->getBuffer();
  
  // Create a new NetCDF file.  Don't overwrite an existing file.
  ncstatus = nc_create(filename.c_str(), NC_NOCLOBBER, &rosnc_ncid);
  if (ncstatus != NC_NOERR) 
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
  // Define the unlimited dimension.  Using the unlimited dimension allows
  // us to continue pushing out records for ever.
  ncstatus = nc_def_dim (rosnc_ncid, "unlimited", NC_UNLIMITED, &unlimiteddim);
  if (ncstatus != NC_NOERR) 
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  // Define each of the fields in the record type in the NetCDF file.
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    BasicType * fieldtype = getFieldType(fieldindex);
    string      fieldname = getFieldName(fieldindex);
    int         varid     = fieldindex-1; // NetCDF indices start at 0.
    
    // Write out the value of the field.
    if (fieldtype->equals(BasicType::Int))
    {
      ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_INT, 1, 
                               &unlimiteddim, &varid);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::Real))
    {
      if (use_float)
	ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_FLOAT, 1, 
                               &unlimiteddim, &varid);
      else
	ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_DOUBLE, 1, 
			       &unlimiteddim, &varid);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::Bool))
    {
      ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_BYTE, 1, 
                               &unlimiteddim, &varid);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::String))
    { 
      throw NetCDFFormatException(fieldname, varid, __FILE__, __LINE__);
    }
    else
    {
      throw InternalError(__FILE__, __LINE__);
    }
  }

  // Exit define mode.  We are now ready to write data.
  ncstatus = nc_enddef(rosnc_ncid);
  if (ncstatus != NC_NOERR)
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }


  ros_isroot      = false;
  ros_inheritable = false;  // Says that this stream cannot be inherited.
  ros_closed      = false;

  stream->registerForUpdates(this);
  addNewStream(this);
}

RecordOutputStream_NetCDF::RecordOutputStream_NetCDF 
(RecordOutputStream * stream, string filename, bool write_float)
{
  int ncstatus;      // Variable used to catch NetCDF return codes.
  int unlimiteddim;  // Identifies unlimited dimension we define in NetCDF file
  rosnc_recordindex = 0;   
  rosnc_filename    = filename;
  use_float         = write_float;
 
  // We don't need our own buffer: reference buffer in stream we are inheriting.
  rs_buffer = stream->getBuffer();
  
  // Create a new NetCDF file.  Don't overwrite an existing file.
  ncstatus = nc_create(filename.c_str(), NC_NOCLOBBER, &rosnc_ncid);
  if (ncstatus != NC_NOERR) 
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
  
  // Define the unlimited dimension.  Using the unlimited dimension allows
  // us to continue pushing out records for ever.
  ncstatus = nc_def_dim (rosnc_ncid, "unlimited", NC_UNLIMITED, &unlimiteddim);
  if (ncstatus != NC_NOERR) 
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  // Define each of the fields in the record type in the NetCDF file.
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    BasicType * fieldtype = getFieldType(fieldindex);
    string      fieldname = getFieldName(fieldindex);
    int         varid     = fieldindex-1; // NetCDF indices start at 0.
    
    // Write out the value of the field.
    if (fieldtype->equals(BasicType::Int))
    {
      ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_INT, 1, 
                               &unlimiteddim, &varid);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::Real))
    {
      if (use_float)
	ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_FLOAT, 1, 
                               &unlimiteddim, &varid);
      else
	ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_DOUBLE, 1, 
			       &unlimiteddim, &varid);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::Bool))
    {
      ncstatus = nc_def_var (rosnc_ncid, fieldname.c_str(), NC_BYTE, 1, 
                               &unlimiteddim, &varid);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::String))
    { 
      throw NetCDFFormatException(fieldname, varid, __FILE__, __LINE__);
    }
    else
    {
      throw InternalError(__FILE__, __LINE__);
    }
  }

  // Exit define mode.  We are now ready to write data.
  ncstatus = nc_enddef(rosnc_ncid);
  if (ncstatus != NC_NOERR)
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }


  ros_isroot      = false;
  ros_inheritable = false;  // Says that this stream cannot be inherited.
  ros_closed      = false;

  stream->registerForUpdates(this);
  addNewStream(this);
}

RecordOutputStream_NetCDF::~RecordOutputStream_NetCDF() 
{
  // Close NetCDF file if not already closed.
  if (!ros_closed)
  { nc_close(rosnc_ncid); }

  removeOldStream(this);
  
  // We shared our buffer, so avoid deletion by the base class.
  rs_buffer = 0;
}

void RecordOutputStream_NetCDF::close()
{
  ros_closed = true;
  
  int ncstatus = nc_close(rosnc_ncid);
  if (ncstatus != NC_NOERR)
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
} 

void RecordOutputStream_NetCDF::pushRecord() 
{
  int ncstatus = 0;
  
  // Complain if the stream has already been closed.
  if (ros_closed)
  { throw EndofStreamException(__FILE__, __LINE__); }
  
  // Write out each field.  If a field does not have a value then
  // an exception will be thrown.
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    int varid = fieldindex - 1;
    BasicType * fieldtype = getFieldType(fieldindex);
    
    // Write out the value of the field.
    if (fieldtype->equals(BasicType::Int))
    {
      int int_value = rs_buffer->getIntValue(fieldindex);

      ncstatus = nc_put_var1_int(rosnc_ncid, varid, 
                                 &rosnc_recordindex, &int_value);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::Real))
    {
      if (use_float) {
	float flt_value = rs_buffer->getRealValue(fieldindex);

	ncstatus = nc_put_var1_float(rosnc_ncid, varid, 
				     &rosnc_recordindex, &flt_value);
      } else {
	double dbl_value = rs_buffer->getRealValue(fieldindex);

	ncstatus = nc_put_var1_double(rosnc_ncid, varid, 
				      &rosnc_recordindex, &dbl_value);
      }
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else if (fieldtype->equals(BasicType::Bool))
    {
      unsigned char bool_value = (unsigned char) rs_buffer->getBoolValue(fieldindex);

      ncstatus = nc_put_var1_uchar(rosnc_ncid, varid, 
                                   &rosnc_recordindex, &bool_value);
      if (ncstatus != NC_NOERR)
      { throw NetCDFException(ncstatus, __FILE__, __LINE__); }
    }
    else
    {
      throw InternalError(__FILE__, __LINE__);
    }
  }

  // Flush the buffer to push out the record straight away.
  nc_sync(rosnc_ncid);
  if (ncstatus != NC_NOERR)
  { throw NetCDFException(ncstatus, __FILE__, __LINE__); }

  rosnc_recordindex++;
}
 
string RecordOutputStream_NetCDF::toString()
{
  ostringstream buffer;
  buffer << "NetCDF Record Output Stream" << endl;
  buffer << "Name of NetCDF file: " << rosnc_filename << endl;
  buffer << "Record Type: " << endl << getType()->toString();
  buffer << "Number of records written: " << rosnc_recordindex << endl;
  buffer << "Open Status (0 indicates open, 1 indicates closed): " << ros_closed << endl;

  return buffer.str();
}
