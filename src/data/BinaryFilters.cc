#include <cmath>
#include <BasicType.h>
#include <BinaryFilters.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::AdditionFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::SubtractionFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::ProductFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::DivisionFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::PowerFilter)

using namespace BIE;

/*******************************************************************************
* Addition Filter.
*******************************************************************************/
const filterIn AdditionFilter::input[] = 
  { filterIn("x", BasicType::Real, false), 
    filterIn("y", BasicType::Real, false), 
    filterIn() };

const filterOut AdditionFilter::output[] = 
  { filterOut("sum", BasicType::Real), 
    filterOut() };

AdditionFilter::AdditionFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  y_index = getInputIndex("y");
  sum_index = getDefaultOutputIndex("sum");
}

void AdditionFilter::compute()
{
  double x,y;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    y = getRealInput(y_index);
    setRealOutput(sum_index, x+y);
  } else {
    vector<double> xdbl = getRealArrayInput(x_index);
    vector<double> ydbl = getRealArrayInput(y_index);
    vector<double> rdbl = vector<double>();
    vector<double>::iterator xitr = xdbl.begin();
    vector<double>::iterator yitr = ydbl.begin();
    while(xitr != xdbl.end()) {
      x = *xitr++; y = *yitr++;
      rdbl.push_back(x+y);
    }
    setRealArrayOutput(sum_index, rdbl);
  }
}

/*******************************************************************************
* Subtraction Filter.
*******************************************************************************/
const filterIn SubtractionFilter::input [] = 
  { filterIn("x", BasicType::Real, false), 
    filterIn("y", BasicType::Real, false), 
    filterIn() };

const filterOut SubtractionFilter::output[] = 
  { filterOut("difference", BasicType::Real), 
    filterOut() };

SubtractionFilter::SubtractionFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index          = getInputIndex("x");
  y_index          = getInputIndex("y");
  difference_index = getDefaultOutputIndex("difference");
}

void SubtractionFilter::compute()
{
  double x,y;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    y = getRealInput(y_index);
    setRealOutput(difference_index, x-y);
  } else {
    vector<double> xdbl = getRealArrayInput(x_index);
    vector<double> ydbl = getRealArrayInput(y_index);
    vector<double> rdbl = vector<double>();
    vector<double>::iterator xitr = xdbl.begin();
    vector<double>::iterator yitr = ydbl.begin();
    while(xitr != xdbl.end()) {
      x = *xitr++; y = *yitr++;
      rdbl.push_back(x-y);
    }
    setRealArrayOutput(difference_index, rdbl);
  }
}

/*******************************************************************************
* Multiplication Filter.
*******************************************************************************/
const filterIn ProductFilter::input [] = 
  { filterIn("x", BasicType::Real, false), 
    filterIn("y", BasicType::Real, false), 
    filterIn() };

const filterOut ProductFilter::output[] = 
  { filterOut("product", BasicType::Real),
    filterOut() };

ProductFilter::ProductFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 

  // Look up indices now to avoid a name look up for every computation.
  x_index = getInputIndex("x");
  y_index = getInputIndex("y");
  product_index = getDefaultOutputIndex("product");
}

void ProductFilter::compute()
{
  double x,y;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    y = getRealInput(y_index);
    setRealOutput(product_index, x*y);
  } else {
    vector<double> xdbl = getRealArrayInput(x_index);
    vector<double> ydbl = getRealArrayInput(y_index);
    vector<double> rdbl = vector<double>();
    vector<double>::iterator xitr = xdbl.begin();
    vector<double>::iterator yitr = ydbl.begin();
    while(xitr != xdbl.end()) {
      x = *xitr++; y = *yitr++;
      rdbl.push_back(x*y);
    }
    setRealArrayOutput(product_index, rdbl);
  }
}

/*******************************************************************************
* Division Filter.
*******************************************************************************/
const filterIn DivisionFilter::input [] = 
  { filterIn("numerator", BasicType::Real, false), 
    filterIn("divisor",   BasicType::Real, false), 
    filterIn() };

const filterOut DivisionFilter::output[] = 
  { filterOut("quotient", BasicType::Real), 
    filterOut() };

DivisionFilter::DivisionFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 

  // Look up indices now to avoid a name look up for every computation.
  numerator_index = getInputIndex("numerator");
  divisor_index   = getInputIndex("divisor");
  quotient_index  = getDefaultOutputIndex("quotient");
}

void DivisionFilter::compute()
{
  double x,y;
  if (!isArray(numerator_index)) {
    x = getRealInput(numerator_index);
    y = getRealInput(divisor_index);
    setRealOutput(quotient_index, x/y);
  } else {
    vector<double> xdbl = getRealArrayInput(numerator_index);
    vector<double> ydbl = getRealArrayInput(divisor_index);
    vector<double> rdbl = vector<double>();
    vector<double>::iterator xitr = xdbl.begin();
    vector<double>::iterator yitr = ydbl.begin();
    while(xitr != xdbl.end()) {
      x = *xitr++; y = *yitr++;
      rdbl.push_back(x/y);
    }
    setRealArrayOutput(quotient_index, rdbl);
  }
}

/*******************************************************************************
* Power (Exponentiation) Filter.
*******************************************************************************/
const filterIn PowerFilter::input [] = 
  { filterIn("x", BasicType::Real, false), 
    filterIn("exponent", BasicType::Real, false), 
    filterIn() };

const filterOut PowerFilter::output[] = 
  { filterOut("power", BasicType::Real),
    filterOut() };

PowerFilter::PowerFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 

  // Look up indices now to avoid a name look up for every computation.
  x_index        = getInputIndex("x");
  exponent_index = getInputIndex("exponent");
  power_index    = getDefaultOutputIndex("power");
}

void PowerFilter::compute()
{
  double x,y;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    y = getRealInput(exponent_index);
    setRealOutput(power_index, pow(x,y));
  } else {
    vector<double> xdbl = getRealArrayInput(x_index);
    vector<double> ydbl = getRealArrayInput(exponent_index);
    vector<double> rdbl = vector<double>();
    vector<double>::iterator xitr = xdbl.begin();
    vector<double>::iterator yitr = ydbl.begin();
    while(xitr != xdbl.end()) {
      x = *xitr++; y = *yitr++;
      rdbl.push_back(pow(x,y));
    }
    setRealArrayOutput(power_index, rdbl);
  }
}
