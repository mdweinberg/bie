#include <UnaryFilters.h>
#include <BasicType.h>
#include <vector>
#include <cmath>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::AbsFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::LogFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::LogBaseNFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::ScaleFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::CosineFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::SineFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::MuFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::InverseMuFilter)

using namespace BIE;

/*******************************************************************************
* Absolute Value Filter.
*******************************************************************************/
const filterIn AbsFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut AbsFilter::output[] = 
  { filterOut("abs", BasicType::Real), filterOut() };

AbsFilter::AbsFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  abs_index = getDefaultOutputIndex("abs");
}

void AbsFilter::compute()
{
  double x;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    setRealOutput(abs_index, fabs(x));
  } else {
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      svdbl.push_back(fabs(x));
    }
    setRealArrayOutput(abs_index, svdbl);
  }
}

/*******************************************************************************
* Natural Log Filter.
*******************************************************************************/
const filterIn LogFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut LogFilter::output[] = 
  { filterOut("ln", BasicType::Real), filterOut() };

LogFilter::LogFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  log_index = getDefaultOutputIndex("ln");
}

void LogFilter::compute()
{
  double x;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    setRealOutput(log_index, log(x));
  } else {
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      svdbl.push_back(log(x));
    }
    setRealArrayOutput(log_index, svdbl);
  }
}

/*******************************************************************************
* Log with specified base filter.
*******************************************************************************/
const filterIn LogBaseNFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut LogBaseNFilter::output[] = 
  { filterOut("log", BasicType::Real), filterOut() };

LogBaseNFilter::LogBaseNFilter(RecordStream * stream, double base)
{ 
  initialize(input, output, stream); 
  
  // Precompute the value of ln(base).
  lnbase = log(base);
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  logbasen_index = getDefaultOutputIndex("log");
}

void LogBaseNFilter::compute()
{
  double x;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    
    // LOGb(x) = LN(x)/LN(b)
    setRealOutput(logbasen_index, log(x)/lnbase);
  } else {
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      svdbl.push_back(log(x)/lnbase);
    }
    setRealArrayOutput(logbasen_index, svdbl);
  }
}

/*******************************************************************************
* Scale Filter.
*******************************************************************************/
const filterIn ScaleFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut ScaleFilter::output[] = 
  { filterOut("scaled", BasicType::Real), filterOut() };

ScaleFilter::ScaleFilter
(RecordStream * stream, double scalefactor) 
{
  this->scalefactor = scalefactor; 
  initialize(input, output, stream);
}

void ScaleFilter::compute()
{
  int inputindex, outputindex;
  double x;
  inputindex = getInputIndex("x");
  outputindex = getDefaultOutputIndex("scaled");
  //  if (!filter_input[inputindex-1].isarray) {
  if (!isArray(inputindex)) {
    x = getRealInput("x");
    setRealOutput("scaled", x * scalefactor);
  } else {
    vector<double> vdbl = getRealArrayInput(inputindex);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      svdbl.push_back(x * scalefactor);
    }
    setRealArrayOutput(outputindex, svdbl);
  }
}

/*******************************************************************************
* Cosine Filter.
*******************************************************************************/
const filterIn CosineFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut CosineFilter::output[] = 
  { filterOut("cosx", BasicType::Real), filterOut() };

CosineFilter::CosineFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  cosx_index = getDefaultOutputIndex("cosx");

}

void CosineFilter::compute()
{
  double x;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    setRealOutput(cosx_index, cos(x));
  } else {
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      svdbl.push_back(cos(x));
    }
    setRealArrayOutput(cosx_index, svdbl);
  }
}

/*******************************************************************************
* Sine Filter.
*******************************************************************************/
const filterIn SineFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut SineFilter::output[] = 
  { filterOut("sinx", BasicType::Real), filterOut() };

SineFilter::SineFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  sinx_index = getDefaultOutputIndex("sinx");

}

void SineFilter::compute()
{
  double x;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    setRealOutput(sinx_index, sin(x));
  } else {
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      svdbl.push_back(sin(x));
    }
    setRealArrayOutput(sinx_index, svdbl);
  }
}

/*******************************************************************************
* Mu Filter.
*******************************************************************************/
const filterIn MuFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut MuFilter::output[] = 
  { filterOut("mu", BasicType::Real), filterOut() };

MuFilter::MuFilter(RecordStream * stream, double MU)
{ 
  mu = MU;
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  mu_index = getDefaultOutputIndex("mu");

}

void MuFilter::compute()
{
  double x, q;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    q = sin(x);
    q = pow(fabs(q), 1.0-mu) * copysign(1.0, q);
    setRealOutput(mu_index, q);
  } else {
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      q = sin(x);
      q = pow(fabs(q), 1.0-mu) * copysign(1.0, q);
      svdbl.push_back(q);
    }
    setRealArrayOutput(mu_index, svdbl);
  }
}

/*******************************************************************************
* InverseMu Filter.
*******************************************************************************/
const filterIn InverseMuFilter::input [] = 
  { filterIn("x", BasicType::Real, false), filterIn() };

const filterOut InverseMuFilter::output[] = 
  { filterOut("b", BasicType::Real), filterOut() };

InverseMuFilter::InverseMuFilter(RecordStream * stream, double MU)
{ 
  mu = MU;
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  b_index = getDefaultOutputIndex("b");
}

void InverseMuFilter::compute()
{
  double x, b;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    b = asin(pow(fabs(x), 1.0/(1.0 - mu))) * copysign(1.0, x);
    setRealOutput(b_index, b);
  } else {
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double> svdbl = vector<double>();
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      x = *itr++;
      b = asin(pow(fabs(x), 1.0/(1.0 - mu))) * copysign(1.0, x);
      svdbl.push_back(b);
    }
    setRealArrayOutput(b_index, svdbl);
  }
}

