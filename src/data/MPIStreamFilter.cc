#include <iomanip>

#include <BIEException.h>
#include <BasicType.h>
#include <RecordType.h>
#include <RecordStream.h>
#include <MPIStreamFilter.h>
#include <MPICommunicationSession.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
static int record_cnt = 0;
#endif

using namespace BIE;

/*******************************************************************************
* MPI Buffer Filter.
*******************************************************************************/
MPIStreamFilter::MPIStreamFilter(RecordStream *stream, MPI_Comm communicator,
				 int sessionId, int capacityInWords)
{ 
  _inSession = false;

#ifdef DEBUG_TESSTOOL
  ferr << "stream=" << stream << " MPI_Comm=" << communicator 
       << " sessionId=" << sessionId 
       << " capacityInWords=" << capacityInWords 
       << endl;
#endif
  _bufferSize = capacityInWords;
  
  output = (filterOut *) new filterOut[1];
  output[0].outputname.clear();
  output[0].outputtype = NULL;


  // input has to be constructed based on the RecordType
  _rtype = stream->getType();
  _numFields= _rtype->numFields();
  int recordSize=0;
#ifdef DEBUG_TESSTOOL
  ferr << "_numFields=" << _numFields << endl; 
#endif

  input = (filterIn *) new filterIn[_numFields+1];
  _sfn = new storeFn [_numFields+1];

  if (_numFields == 0)
    { 
      input[0].inputname.clear();
      input[0].inputtype = NULL;
      input[0].isset = 0;
    }
  else
    {
      for (int fieldindex = 1; fieldindex < _numFields+1; fieldindex++)
	{
	  string fieldname  = _rtype->getFieldName(fieldindex);
	  BasicType *btype = (_rtype->getFieldType(fieldindex));
	  input [fieldindex-1].inputname = (char *) fieldname.c_str();
	  input [fieldindex-1].inputtype = btype;
	  input [fieldindex-1].isset = false;

	  if ( btype == BasicType::String ) {
	    throw StreamInheritanceException(__FILE__, __LINE__);
	  }
	  if ( btype == BasicType::Int ) {
	    recordSize += sizeof(int); 
	    _sfn[fieldindex]=&MPIStreamFilter::storeInt;
	  } else if ( btype == BasicType::Real ) {
	    recordSize += sizeof(double);
	    _sfn[fieldindex]=&MPIStreamFilter::storeReal;
	  } else if ( btype == BasicType::Bool ) {
	    recordSize += sizeof(bool);
	    _sfn[fieldindex]=&MPIStreamFilter::storeBool;
	  }
	}
    }
  input [_numFields].inputname.clear();
  input [_numFields].inputtype = NULL;
  input [_numFields].isset = false;
  
  // allocate buffer with default number of records 
  _recordSize = recordSize;
#ifdef DEBUG_TESSTOOL
  ferr << "_bufferSize=" << _bufferSize << endl;
  ferr << "_recordSize=" << _recordSize << endl; 
#endif
  _buffer = new int[_bufferSize];
#ifdef DEBUG_TESSTOOL
  ferr << "_buffer=" << _buffer << endl << std::flush;
#endif
  _capacityInRecords = _bufferSize *sizeof(int) / _recordSize;
  if (_capacityInRecords == 0) {
    throw BIEException("MPIStreamFilterParameter exception", 
		       "buffer size is too small, recompile",
		       __FILE__, __LINE__);
  }
  _numRecords = 0;
#ifdef DEBUG_TESSTOOL
  ferr << "capacity in records=" << _capacityInRecords 
       << ", about to initialize filtered stream" << endl << std::flush;
#endif
  initialize(input, output, stream); 
#ifdef DEBUG_TESSTOOL
  ferr << "initialized filtered stream" << endl << std::flush;
#endif

  // connect the input stream fields to filter input
  for (int fieldindex = 1; fieldindex <= _numFields; fieldindex++)
    {
      this->connect(fieldindex, fieldindex);
    }
#ifdef DEBUG_TESSTOOL
  ferr << "connected filter to stream" << endl << std::flush;
#endif

  // filterWith has to be issued on the stream

  // setup a communication session
  _mpiSendSession = new MPICommunicationSession(this, sessionId, communicator);
#ifdef DEBUG_TESSTOOL
  ferr << "_mpiSendSession=" << _mpiSendSession << endl << std::flush;
#endif
}

int MPIStreamFilter::getRecordBufferSizeInBytes(RecordType *rt)
{
  int rsize=0;
  for (int fieldindex = 1; fieldindex <= rt->numFields(); fieldindex++)
    {
      BasicType *btype = rt->getFieldType(fieldindex);

      if ( btype == BasicType::String ) {
	throw StreamInheritanceException(__FILE__, __LINE__);
      }
      if ( btype == BasicType::Int ) {
	rsize += sizeof(int); 
      } else if ( btype == BasicType::Real ) {
	rsize += sizeof(double);
      } else if ( btype == BasicType::Bool ) {
	rsize += sizeof(bool);
      }
    }
  return rsize;
}

void *MPIStreamFilter::packRecordBuffer(RecordBuffer *rb, int *size)
{
  void *buf, *tmp;
  int rsize;
  RecordType *rt = rb->getType();
  int numFlds = rt->numFields();

  rsize = getRecordBufferSizeInBytes(rt);
  *size = rsize;

  // allocate buffer with default number of records 
  buf = new char[rsize];
  tmp = buf;
  for (int fieldindex = 1; fieldindex <= numFlds; fieldindex++)
    {
      BasicType *btype = (rt->getFieldType(fieldindex));
      if ( btype == BasicType::String ) {
	throw StreamInheritanceException(__FILE__, __LINE__);
      }
      if ( btype == BasicType::Int ) {
	*(int *) tmp = rb->getIntValue(fieldindex);
	tmp = (char *) tmp + sizeof(int);
      } else if ( btype == BasicType::Real ) {
	*(double *) tmp = rb->getRealValue(fieldindex);
	tmp = (char *) tmp + sizeof(double);
      } else if ( btype == BasicType::Bool ) {
	*(bool *) tmp = rb->getBoolValue(fieldindex);
	tmp = (char *) tmp + sizeof(bool);
      }
    }
  return buf;
}
  
MPIStreamFilter::~MPIStreamFilter()
{ 
  finishSession();
  delete [] (int*)_buffer;
  delete [] _sfn;
  delete input;
  delete output;
}
void MPIStreamFilter::setHeaderRecordBuffer(RecordBuffer *hdrbuf)
{
  _hdrBuffer = hdrbuf; 
}

void MPIStreamFilter::startNewSession(int newSessionId)
{
  _inSession = true;
  _mpiSendSession->startNewSession(newSessionId);
}

void MPIStreamFilter::storeInt(int fieldindex)
{ 
  *(int *) _bufferIndex = getIntInput(fieldindex);
  _bufferIndex = (char *)_bufferIndex + sizeof(int);
}

void MPIStreamFilter::storeReal(int fieldindex)
{ 
  *(double *) _bufferIndex = getRealInput(fieldindex);
  _bufferIndex = (char *)_bufferIndex + sizeof(double);
}

void MPIStreamFilter::storeBool(int fieldindex)
{
  *(bool *) _bufferIndex = getBoolInput(fieldindex); 
  _bufferIndex = (char *)_bufferIndex + sizeof(bool);
}

void MPIStreamFilter::compute()
{
#ifdef DEBUG_TESSTOOL
  ferr << "MPIStreamFilter::compute _numRecords=" << _numRecords 
       << " / " << _capacityInRecords << endl;
#endif

  // Is the buffer full?
  if (_numRecords == _capacityInRecords) flush();

  // Get the location in the buffer
  _bufferIndex = (void *) ((char *)_buffer + _numRecords*_recordSize);

  for (int fieldindex = 1; fieldindex <= _numFields; fieldindex++)
    {
      storeFn fn;
      fn = _sfn[fieldindex];
      (*this.*fn)(fieldindex);
    }
  
  _numRecords++;

#ifdef DEBUG_TESSTOOL
  ferr << "### "
       << setw(15) << getRealInput(3)
       << setw(15) << getRealInput(4)
       << setw(15) << getRealInput(5)
       << setw(15) << getRealInput(6)
       << endl;
#endif
}

void MPIStreamFilter::flush()
{
#ifdef DEBUG_TESSTOOL
  ferr << "MPIStreamFilter::flush" << endl;
  record_cnt += _numRecords;
#endif

  // Send via MPI
  _mpiSendSession->send();

  // Set buffer to empty
  _numRecords = 0;
}

void MPIStreamFilter::finishSession()
{
  // Send remaining records
  if (_numRecords > 0) flush();

  // Send end of stream record
  _mpiSendSession->finishSendTx();

#ifdef DEBUG_TESSTOOL
  ferr << "MPIStreamFilter::finishSession, records=" << record_cnt << endl;
  record_cnt = 0;
#endif

  _inSession = false;
}

