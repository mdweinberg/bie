#include <sstream>

#include <TypedBuffer.h>
#include <BIEException.h>
#include <BasicType.h>
#include <ArrayType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::TypedBuffer)


using namespace BIE;

/*******************************************************************************
* Subclasses implementing a typed buffer override the methods related
* only to that type, meaning the default is for a type exception to be thrown.
*******************************************************************************/
void TypedBuffer::setStringValue(string value)
{ throw TypeException(BasicType::String, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setIntValue(int value)
{ throw TypeException(BasicType::Int, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setRealValue(double value)
{ throw TypeException(BasicType::Real, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setBoolValue(bool)
{ throw TypeException(BasicType::Bool, tb_type,__FILE__,__LINE__); }
  
string TypedBuffer::getStringValue()
{ throw TypeException(BasicType::String, tb_type,__FILE__,__LINE__); }

int TypedBuffer::getIntValue()
{ throw TypeException(BasicType::Int, tb_type,__FILE__,__LINE__); }

double TypedBuffer::getRealValue()
{ throw TypeException(BasicType::Real, tb_type,__FILE__,__LINE__); }

bool TypedBuffer::getBoolValue()
{ throw TypeException(BasicType::Bool, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setIntArrayValue(vector<int>)
{ throw TypeException(BasicType::Int, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setIntArrayValue(int iv)
{ throw TypeException(BasicType::Int, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setIntArrayValue(int offset, int iv)
{ throw TypeException(BasicType::Int, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setRealArrayValue(vector<double>)
{ throw TypeException(BasicType::Real, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setRealArrayValue(double)
{ throw TypeException(BasicType::Real, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setRealArrayValue(int offset, double dv)
{ throw TypeException(BasicType::Real, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setBoolArrayValue(vector<bool>)
{ throw TypeException(BasicType::Bool, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setBoolArrayValue(bool)
{ throw TypeException(BasicType::Bool, tb_type,__FILE__,__LINE__); }

void TypedBuffer::setBoolArrayValue(int offset, bool bv)
{ throw TypeException(BasicType::Bool, tb_type,__FILE__,__LINE__); }

vector<int> TypedBuffer::getIntArrayValue()
{ throw TypeException(BasicType::Int, tb_type,__FILE__,__LINE__); }

int TypedBuffer::getIntArrayValue(int offset)
{ throw TypeException(BasicType::Int, tb_type,__FILE__,__LINE__); }

vector<double> TypedBuffer::getRealArrayValue()
{ throw TypeException(BasicType::Real, tb_type,__FILE__,__LINE__); }

double TypedBuffer::getRealArrayValue(int offset)
{ throw TypeException(BasicType::Real, tb_type,__FILE__,__LINE__); }

vector<bool> TypedBuffer::getBoolArrayValue()
{ throw TypeException(BasicType::Bool, tb_type,__FILE__,__LINE__); }

bool TypedBuffer::getBoolArrayValue(int offset)
{ throw TypeException(BasicType::Bool, tb_type,__FILE__,__LINE__); }

/*****************************************************************************
* Typed buffer methods common to all subclasses.
*****************************************************************************/
bool TypedBuffer::hasValue()
{ return tb_hasvalue; }

BasicType * TypedBuffer::getType()
{ return tb_type; }

TypedBuffer * TypedBuffer::createNew (BasicType * type)
{
  TypedBuffer * result = 0; 
  BasicType *btype = type;

  if (type->isArrayType()) {
    ArrayType *atype = static_cast<ArrayType*>(type);
    btype = atype->getBaseType();
    if ((BasicType::String)->equals(btype)) {
      throw TypeException("Unsupported type.",__FILE__,__LINE__);
    }
    else if ((BasicType::Int)->equals(btype)) {
      result = (TypedBuffer *) new TypedBuffer_IntArray(type);
    }
    else if ((BasicType::Real)->equals(btype)) {
      result = (TypedBuffer *) new TypedBuffer_RealArray(type);
    } 
    else if ((BasicType::Bool)->equals(btype)) {
      result = (TypedBuffer *) new TypedBuffer_BoolArray(type);
    }
    else {
      throw TypeException("Unsupported type.",__FILE__,__LINE__);
    }
  } else {
    if ((BasicType::String)->equals(btype)) {
      result = (TypedBuffer *) new TypedBuffer_String();
    }
    else if ((BasicType::Int)->equals(btype)) {
      result = (TypedBuffer *) new TypedBuffer_Int();
    }
    else if ((BasicType::Real)->equals(btype)) {
      result = (TypedBuffer *) new TypedBuffer_Real();
    } 
    else if ((BasicType::Bool)->equals(btype)) {
      result = (TypedBuffer *) new TypedBuffer_Bool();
    }
    else {
      throw TypeException("Unsupported type.",__FILE__,__LINE__);
    }
  }
  return result;
}

/*******************************************************************************
* Implementation of the String basic type.
*******************************************************************************/

TypedBuffer_String::TypedBuffer_String()
{
  tb_hasvalue    = false;
  tb_type        = BasicType::String;
  tbstring_value = "";
}

void TypedBuffer_String::setStringValue
(string newvalue)
{
  tb_hasvalue = true;
  tbstring_value = newvalue;
}

string TypedBuffer_String::getStringValue()
{
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbstring_value;
}

string TypedBuffer_String::toString()
{
  string result;
  
  if (tb_hasvalue)
  { result = tb_type->toString() + ": \"" + tbstring_value + "\""; }
  else
  { result = tb_type->toString() + ": <no value>"; }
  
  return result;
}

/*******************************************************************************
* Implementation of the int basic type.
*******************************************************************************/

TypedBuffer_Int::TypedBuffer_Int() 
{
  tb_hasvalue = false;
  tb_type        = BasicType::Int;
  tbint_value    = 0;
}

void TypedBuffer_Int::setIntValue
(int newvalue)
{
  tb_hasvalue = true;
  tbint_value    = newvalue;
}

int TypedBuffer_Int::getIntValue() 
{
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbint_value;
}

string TypedBuffer_Int::toString()
{
  // Can't concatenate ints/reals to strings in C++, so use a string stream.
  std::ostringstream buffer;
  buffer << tb_type->toString() << ": ";

  if (tb_hasvalue)
  {  buffer << tbint_value; }
  else
  {  buffer << "<no value>";}

  return buffer.str();  
}

/*******************************************************************************
* Implementation of the real basic type.
*******************************************************************************/

TypedBuffer_Real::TypedBuffer_Real()
{
  tb_hasvalue = false;
  tb_type        = BasicType::Real;
  tbreal_value   = 0.0;
}

void TypedBuffer_Real::setRealValue
(double newvalue)
{
  tb_hasvalue = true;
  tbreal_value   = newvalue;
}

double TypedBuffer_Real::getRealValue() 
{
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbreal_value;
}

string TypedBuffer_Real::toString()
{
  std::ostringstream buffer;
  buffer << tb_type->toString() << ": ";
  
  if (tb_hasvalue)
  { buffer << tbreal_value; }
  else 
  { buffer << "<no value>"; }
  
  return buffer.str();  
}

/*******************************************************************************
* Implementation of the boolean basic type.
*******************************************************************************/

TypedBuffer_Bool::TypedBuffer_Bool()
{
  tb_hasvalue = false;
  tb_type        = BasicType::Bool;
  tbbool_value   = false;
}

void TypedBuffer_Bool::setBoolValue
(bool newvalue)
{
  tb_hasvalue = true;
  tbbool_value   = newvalue;
}

bool TypedBuffer_Bool::getBoolValue() 
{
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbbool_value;  
}

string TypedBuffer_Bool::toString()
{
  std::ostringstream buffer;
  buffer << tb_type->toString() << ": ";
  
  if (tb_hasvalue)
  {
    if (tbbool_value)
    { buffer << "true" ; }
    else
    { buffer << "false" ; }
  }
  else
  {
    buffer << "<no value>";
  }
  
  return buffer.str();  
} 


/*******************************************************************************
* Implementation of the array of int basic type.
*******************************************************************************/

TypedBuffer_IntArray::TypedBuffer_IntArray(BasicType *btype)
{
  tbintarray_value = vector<int>();
  tb_hasvalue = false;
  tb_type = btype;
}

TypedBuffer_IntArray::TypedBuffer_IntArray(BasicType *btype, vector<int> vi)
{
  tbintarray_value = vi;
  tb_hasvalue = true;
  tb_type = btype;
}

vector<int> TypedBuffer_IntArray::getIntArrayValue()
{ 
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbintarray_value;
}

int TypedBuffer_IntArray::getIntArrayValue(int offset)
{ 
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbintarray_value[offset];
}

void TypedBuffer_IntArray::setIntArrayValue(vector<int> vi)
{ 
  //  if( tbintarray_value != vector<int>())
  //   delete &tbintarray_value;
  tbintarray_value = vi;
  tb_hasvalue = true;
}

void TypedBuffer_IntArray::setIntArrayValue(int iv)
{ 
  tb_hasvalue = true;
  tbintarray_value.push_back(iv);
}

void TypedBuffer_IntArray::setIntArrayValue(int offset, int iv)
{ 
  tb_hasvalue = true;
  tbintarray_value[offset] = iv;
}

string TypedBuffer_IntArray::toString()
{
  stringstream ss (stringstream::out);
  vector<int>::iterator itr = tbintarray_value.begin();
  while(itr != tbintarray_value.end()) {
    ss << *itr++ << " ";
  }
  ss << endl;
  return ss.str();
}

/*******************************************************************************
* Implementation of the array of real basic type.
*******************************************************************************/

TypedBuffer_RealArray::TypedBuffer_RealArray(BasicType *btype)
{
  tbrealarray_value = vector<double>();
  tb_hasvalue = false;
  tb_type = btype;
}

TypedBuffer_RealArray::TypedBuffer_RealArray(BasicType *btype, vector<double> vr)
{
  tbrealarray_value = vr;
  tb_hasvalue = true;
  tb_type = btype;
}

vector<double> TypedBuffer_RealArray::getRealArrayValue()
{ 
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbrealarray_value;
}

double TypedBuffer_RealArray::getRealArrayValue(int offset)
{ 
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbrealarray_value[offset];
}

void TypedBuffer_RealArray::setRealArrayValue(vector<double> vr)
{ 
  // if( tbrealarray_value != vector<double>())
  //  delete &tbrealarray_value;
  tbrealarray_value = vr;
  tb_hasvalue = true;
}

void TypedBuffer_RealArray::setRealArrayValue(double rv)
{ 
  tb_hasvalue = true;
  tbrealarray_value.push_back(rv);
}

void TypedBuffer_RealArray::setRealArrayValue(int offset, double rv)
{ 
  tb_hasvalue = true;
  tbrealarray_value[offset] = rv;
}

string TypedBuffer_RealArray::toString()
{
  stringstream ss (stringstream::out);
  vector<double>::iterator itr = tbrealarray_value.begin();
  while(itr != tbrealarray_value.end()) {
    ss << *itr++ << " ";
  }
  ss << endl;
  return ss.str();
}

/*******************************************************************************
* Implementation of the array of boolean basic type.
*******************************************************************************/

TypedBuffer_BoolArray::TypedBuffer_BoolArray(BasicType *btype)
{
  tbboolarray_value = vector<bool>();
  tb_hasvalue = false;
  tb_type = btype;
}

TypedBuffer_BoolArray::TypedBuffer_BoolArray(BasicType *btype, vector<bool> vb)
{
  tbboolarray_value = vb;
  tb_hasvalue = true;
  tb_type = btype;
}

vector<bool> TypedBuffer_BoolArray::getBoolArrayValue()
{ 
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbboolarray_value;
}

bool TypedBuffer_BoolArray::getBoolArrayValue(int offset)
{ 
  if (!tb_hasvalue) {throw NoValueException(__FILE__, __LINE__);}
  return tbboolarray_value[offset];
}

void TypedBuffer_BoolArray::setBoolArrayValue(vector<bool> vb)
{ 
  // if( tbboolarray_value != vector<bool>())
  //  delete &tbboolarray_value;
  tbboolarray_value = vb;
  tb_hasvalue = true;
}

void TypedBuffer_BoolArray::setBoolArrayValue(bool bv)
{ 
  tb_hasvalue = true;
  tbboolarray_value.push_back(bv);
}

void TypedBuffer_BoolArray::setBoolArrayValue(int offset, bool bv)
{ 
  tb_hasvalue = true;
  tbboolarray_value[offset] = bv;
}

string TypedBuffer_BoolArray::toString()
{
  stringstream ss (stringstream::out);
  vector<bool>::iterator itr = tbboolarray_value.begin();
  while(itr != tbboolarray_value.end()) {
    ss << *itr++ << " ";
  }
  ss << endl;
  return ss.str();
}
