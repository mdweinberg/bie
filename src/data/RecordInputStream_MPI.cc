#include <gvariable.h>
#include <RecordInputStream_MPI.h>
#include <BasicType.h>
#include <BIEException.h>

// BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordInputStream_MPI)


using namespace BIE;

RecordInputStream_MPI::RecordInputStream_MPI(RecordType *rt, TessToolDataStream *ttrcvr)
{
  _ttrcvr = ttrcvr;
  rs_buffer = new RecordBuffer(rt); 
  rismpi_streamismine = true;
  rismpi_eof=false;
  rismpi_error=false;
  //  ris_isroot = true;
  //  ris_inherited = NULL;
  //  ris_filter = NULL;
}

RecordInputStream_MPI::~RecordInputStream_MPI()
{
  if (rismpi_streamismine) { ;}
  
}


bool RecordInputStream_MPI::eos() {return rismpi_eof; }
  
bool RecordInputStream_MPI::error() {return rismpi_error; }

bool RecordInputStream_MPI::nextRecord() 
{
  bool result = true;
#ifdef DEBUG_TESSTOOL
  rferr << "RecordInputStream_MPI::nextRecord enter rismpi_eof=" << rismpi_eof << endl;
#endif

  if (rismpi_eof || rismpi_error)
  {
    result = false;
#ifdef DEBUG_TESSTOOL
    rferr << "RecordInputStream_MPI::nextRecord  rismpi_eof=" << rismpi_eof 
	  << "rismpi_error=" << rismpi_error << endl;
#endif
  }
  else
  {
    try 
    {
      for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
      {
        BasicType * type = rs_buffer->getFieldType(fieldindex);
  
        if (type->equals(BasicType::Int)) {  
          rs_buffer->setIntValue(fieldindex, readInt());
        }
        else if (type->equals(BasicType::Real)) {  
          rs_buffer->setRealValue(fieldindex, readReal());
        }
        else if (type->equals(BasicType::Bool))  {  
          rs_buffer->setBoolValue(fieldindex, readBool());
        }
        else if (type->equals(BasicType::String)) {  
	  throw FileFormatException("Unexpected String or error",__FILE__, __LINE__);
          rs_buffer->setStringValue(fieldindex, readString()); 
        }
        else 
        { throw InternalError(__FILE__,__LINE__);  } 
        
        // Check we successfully got our value.
        if (rismpi_eof && fieldindex == 1) {
          result = false;
          break;
        }
        else if (rismpi_eof || rismpi_error) {
          throw FileFormatException("Unexpected EOF or error",__FILE__, __LINE__);
        }
      }
    }
    catch (...)
    {
      rismpi_error = true;
      result     = false;
      
      // Reset the buffer to forget all the previous values.
      rs_buffer->reset();

      throw;  // Rethrows the original exception.
    }
  }
    

  if (! result)
  {
    // Reset the buffer to forget all the previous values.
    rs_buffer->reset();
  }

#ifdef DEBUG_TESSTOOL
  rferr << "RecordInputStream_MPI::nextRecord exit result=" << result << endl;
  rferr.flush();
#endif 

  return result;
}

int RecordInputStream_MPI::getTileId()
{
  // lookup field with name TileId
  int tileid = -1;
  RecordType *rt = getType();
  int index = rt->getFieldIndex("TileId");
#ifdef DEBUG_TESSTOOL
  cerr << "RecordInputStream_MPI::getTileId():TileId index=" << index << endl;
#endif
  tileid = getIntValue(index);
  return tileid;
}

int RecordInputStream_MPI::getNumRecords()
{
  // lookup field with name NumTiles
  int numrecs = -1;
  RecordType *rt = getType();
  int index = rt->getFieldIndex("NumRecords");
#ifdef DEBUG_TESSTOOL
  cerr << "RecordInputStream_MPI::getNumRecords():index=" << index << endl;
#endif
  numrecs = getIntValue(index);
  return numrecs;
}

double RecordInputStream_MPI::getScalar()
{
  // lookup field with name Scalar, if absent, return the field after TileId
  int index;
  double scalar;

  RecordType *rt = getType();
  try {
    index = rt->getFieldIndex("Scalar");
  } catch (BIEException &e) {
    index = rt->getFieldIndex("TileId");
    index++;
  }
#ifdef DEBUG_TESSTOOL
  cerr << "RecordInputStream_MPI::getTileId():Scalar index=" << index << endl;
#endif
  scalar = getRealValue(index);
  return scalar;

}

int RecordInputStream_MPI::readInt()
{
  int rv;
  int value = 0;
  rv = _ttrcvr->readBuffer((unsigned char*) &value, 4);
  if (rv == -1) rismpi_eof = true;
  return value;
}

double RecordInputStream_MPI::readReal()
{
  int rv;
  double value = 0;
  rv = _ttrcvr->readBuffer((unsigned char*) &value, 8);
  if (rv == -1) rismpi_eof = true;
 
  return value;
}

bool RecordInputStream_MPI::readBool()
{
  int rv;
  bool value = false;
  rv = _ttrcvr->readBuffer((unsigned char*) &value, 1);
  if (rv == -1) rismpi_eof = true;
 
  return value;
}

const char * RecordInputStream_MPI::readString()
{
  throw FileFormatException("Unexpected String or error",__FILE__, __LINE__);
}
