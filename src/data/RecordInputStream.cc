#include <RecordInputStream.h>
#include <RecordStreamFilter.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordInputStream)

using namespace BIE;

RecordInputStream::~RecordInputStream()
{
  // All inherited streams become root streams again.
  for (int streamindex = 0; streamindex < (int) ris_inherited.size(); streamindex++)
  { ris_inherited[streamindex]->setAsRoot(); }
}


bool RecordInputStream::nextRecord()
{
  bool result = true;
  
  try 
  {
    // If there are no streams to inherit from then return failure.
    if (ris_inherited.size() == 0)
    {  result = false; }
    else
    {  
      for (int streamindex = 0; streamindex < (int) ris_inherited.size(); streamindex++)
      {
        result = ris_inherited[streamindex]->nextRecord();    
        if (!result) { break; }
      }
    }
  
    // Call the filter to fill its output buffers - 
    // but **only if using a filter and if the nextRecord on other streams was OK
    if (result && (ris_filter != 0)) 
    { ris_filter->compute(); }
  
  }
  catch (...)
  {
    // Clear out values in buffer.
    rs_buffer->reset();

    throw;
  }

  
  // Clear out values in buffer on failure.
  if (!result)
  { rs_buffer->reset(); }

  return result;
}

bool RecordInputStream::isRootStream() { return ris_isroot; }
void RecordInputStream::noLongerRoot() { ris_isroot = false; }
void RecordInputStream::setAsRoot()    { ris_isroot = true; }

bool RecordInputStream::eos()
{
  bool result = false;
  
  // Ask each of the inherited streams if they think they have reached EOS.
  for (int streamindex = 0; streamindex < (int) ris_inherited.size(); streamindex++)
  {
    if (ris_inherited[streamindex]->eos())
    {  
      result = true;
      break;
    }
  }

  return result;
}
  

bool RecordInputStream::error()
{
  bool result = false;
  
  // Ask each of the streams if they think an error has occured.
  for (int streamindex = 0; streamindex < (int) ris_inherited.size(); streamindex++)
  {
    if (ris_inherited[streamindex]->error())
    {  
      result = true;
      break;
    }
  }

  return result;
}


RecordInputStream* RecordInputStream::deleteField
(string fieldname) 
{
  RecordBuffer * newbuffer = rs_buffer->deleteField(fieldname);
  return createDerivative(newbuffer);
}

RecordInputStream* RecordInputStream::deleteField
(int fieldindex) 
{
  RecordBuffer * newbuffer = rs_buffer->deleteField(fieldindex);
  return createDerivative(newbuffer);
}

RecordInputStream * RecordInputStream::deleteRange
(int startfield, int endfield) 
{
  RecordBuffer * newbuffer = rs_buffer->deleteRange(startfield, endfield);
  return createDerivative(newbuffer);
}

RecordInputStream *  RecordInputStream::joinWithStream
(int position, RecordInputStream * stream)
{
  RecordInputStream * result = new RecordInputStream();

  // Check both streams are root streams and can be inherited.
  if (!(this->isRootStream() && stream->isRootStream()))
  {  throw StreamInheritanceException(__FILE__, __LINE__); }
  
  // Create a new buffer that inherits all fields not deleted.
  result->rs_buffer = rs_buffer->insertRecord(position, stream->getBuffer());

  // The new stream inherits both streams.
  (result->ris_inherited).push_back(this);
  (result->ris_inherited).push_back(stream);
  
  // Both streams are now no longer root streams
  this->noLongerRoot();
  stream->noLongerRoot();
  
  // The new stream is a root stream.
  result->setAsRoot();

  // There is no filter when joining streams.
  result->ris_filter = 0;

  return result;    
}

RecordInputStream *  RecordInputStream::filterWith
(int position, RecordStreamFilter * filter)
{
  // Indicate that the filter is being used and plumb the inputs.
  filter->attachToStream(this);

  // Create a new buffer that inherits fields from this stream and the filter
  RecordBuffer* newbuffer = rs_buffer->insertRecord
                              (position,filter->getBuffer());

  // Inherit this stream stream
  RecordInputStream * result = createDerivative(newbuffer);

  result->ris_filter = filter;
    
  return result;    
}

      
RecordInputStream *  RecordInputStream::selectFields
(RecordType * selection)      
{
  RecordBuffer * newbuffer = rs_buffer->selectFields(selection);
  return createDerivative(newbuffer);
}
    
RecordInputStream * RecordInputStream::selectFields
(vector<int>* selection)      
{
  RecordBuffer * newbuffer = rs_buffer->selectFields(selection);
  return createDerivative(newbuffer);
}

RecordInputStream * RecordInputStream::selectFields
(vector<string>* selection)    
{
  RecordBuffer * newbuffer = rs_buffer->selectFields(selection);
  return createDerivative(newbuffer);
}

RecordInputStream * RecordInputStream::renameField
(string oldname, string newname) 
{
  RecordBuffer * newbuffer = rs_buffer->renameField(oldname, newname);
  return createDerivative(newbuffer);
}

RecordInputStream * RecordInputStream::renameField
(int fieldindex, string newname) 
{
  RecordBuffer * newbuffer = rs_buffer->renameField(fieldindex, newname);
  return createDerivative(newbuffer);
}
     
RecordInputStream * RecordInputStream::moveField
(string fieldname, int newposition)
{
  RecordBuffer * newbuffer = rs_buffer->moveField(fieldname, newposition);
  return createDerivative(newbuffer);
}

RecordInputStream * RecordInputStream::moveField
(int oldposition, int newposition)
{
  RecordBuffer * newbuffer = rs_buffer->moveField(oldposition, newposition);
  return createDerivative(newbuffer);
}

RecordInputStream * RecordInputStream::createDerivative 
(RecordBuffer * newbuffer)
{
  RecordInputStream * result = new RecordInputStream();

  // Check this stream is a root stream and can be inherited.
  if (!isRootStream())
  {  throw StreamInheritanceException(__FILE__, __LINE__); }
  
  result->rs_buffer = newbuffer;

  // The new stream inherits from this stream.
  (result->ris_inherited).push_back(this);

  // This stream is now no longer a root stream.  Only perform this
  // operation once the rest has succeeded.
  this->noLongerRoot();

  // The new stream is a root stream.
  result->setAsRoot();
  
  // By default there is no filter - one can be specified later.
  result->ris_filter = 0;
      
  return result;    
}

string RecordInputStream::toString()
{
  return "hack";
}
