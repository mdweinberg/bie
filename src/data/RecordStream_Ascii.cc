#include <RecordStream_Ascii.h>
#include <AsciiLexer.h>
#include <errno.h>
#include <limits.h>
#include <RecordBuffer.h>
#include <RecordType.h>
#include <BasicType.h>
#include <ArrayType.h>
#include <BIEException.h>

#include <iomanip>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordInputStream_Ascii)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordOutputStream_Ascii)

using namespace BIE;

/*******************************************************************************
* Ascii Record Input Stream Implementation.
*******************************************************************************/
RecordInputStream_Ascii::RecordInputStream_Ascii 
(RecordType * type, string filename)
{
  // common initialization
  initialize();

  // Create new buffer with the specified type.
  rs_buffer = new RecordBuffer(type);
  
  // Create a new lexer object.
  risa_lexer = new AsciiLexer(filename);
}
  
RecordInputStream_Ascii::RecordInputStream_Ascii
(RecordType * type, istream * asciistream)
{
  // common initialization
  initialize();

  // Create new buffer with the specified type.
  rs_buffer = new RecordBuffer(type);
  
  // Create a new lexer object.
  risa_lexer = new AsciiLexer(asciistream);
}

RecordInputStream_Ascii::RecordInputStream_Ascii
(string filename)
{   
  // common initialization
  initialize();

  // Create a new lexer object.
  risa_lexer = new AsciiLexer(filename);
  
  // Extract the type from the top of the file
  RecordType * type = readMetaData();

  // Create a buffer with the specified type.
  rs_buffer = new RecordBuffer(type);

  // We don't need the type any more.
  delete type;
}
  
RecordInputStream_Ascii::RecordInputStream_Ascii
(istream * asciistream)
{ 
  // common initialization
  initialize();

  // Create a new lexer object.
  risa_lexer = new AsciiLexer(asciistream);
  
  // Extract the type from the top of the file
  RecordType * type = readMetaData();

  // Create a buffer with the specified type.
  rs_buffer = new RecordBuffer(type);

  // We don't need the type any more.
  delete type;
}

void RecordInputStream_Ascii::initialize()
{
  rs_buffer = 0;
  risa_eof   = false;
  risa_error = false;

  // This stream is a root stream.
  this->setAsRoot();
}

RecordInputStream_Ascii::~RecordInputStream_Ascii()
{ delete risa_lexer; }

RecordType * RecordInputStream_Ascii::readMetaData() 
{
  int fieldindex = 1;
  
  // Start with an empty type.
  RecordType * result = new RecordType();
  bool finished = false;
  
  try
  {
    while (! finished)
    {
      int lexcode = risa_lexer->nextToken();
      BasicType * fieldtype = 0;
      bool isArray=false;
  
      if (lexcode == AsciiLexer::AL_LEFT_SQUAREBRACKET_) {
	lexcode = risa_lexer->nextToken();
	isArray = true;
      }
  
      switch (lexcode)
      {
        case AsciiLexer::AL_NEWLINE_: 
          // Finished reading type header.
          finished = true;
          break;
        case AsciiLexer::AL_INT_KEYWORD_: 
	  if (!isArray) {
	    fieldtype = BasicType::Int;
	  } else {
	    int length = RecordType::getLength(risa_lexer);
	    fieldtype = new ArrayType(BasicType::Int, length);
	  }
          break;
        case AsciiLexer::AL_REAL_KEYWORD_:
	  if (!isArray) {
	    fieldtype = BasicType::Real;
	  } else {
	    int length = RecordType::getLength(risa_lexer);
	    fieldtype = new ArrayType(BasicType::Real, length);
	  }
          break;
        case AsciiLexer::AL_BOOL_KEYWORD_:
	  if (!isArray) {
	    fieldtype = BasicType::Bool;
	  } else {
	    int length = RecordType::getLength(risa_lexer);
	    fieldtype = new ArrayType(BasicType::Bool, length);
	  }
          break;
        case AsciiLexer::AL_STRING_KEYWORD_:
	  if (!isArray) {
	    fieldtype = BasicType::String;
	  } else {
	    throw FileFormatException("Unknown field type: Array of string not implemented", 
				      __FILE__, __LINE__);
	  }
          break;
        default:
          throw FileFormatException("Unknown field type (should be int, real, bool or string", 
	                            __FILE__, __LINE__);
      }
      
      if (!finished)
      {
        lexcode = risa_lexer->nextToken();
      
        if (lexcode != AsciiLexer::AL_STRING_VALUE_)
        { throw FileFormatException("Expected string value", __FILE__, __LINE__); }
      
        // Add this field to the record type
        RecordType * newtype = result->insertField
                               (fieldindex, risa_lexer->tokenText(), fieldtype);
  
        // We created a new type object and do not need the old one.
        delete result;
        result = newtype;
      
        // Read a new line character.
        lexcode = risa_lexer->nextToken();    
        if (lexcode != AsciiLexer::AL_NEWLINE_)
        { throw FileFormatException("Expected newline", __FILE__, __LINE__); }
      
        fieldindex++;
      }
    }
    
    // Check that there was at least one field.
    if (fieldindex == 1)
    { throw FileFormatException("Must be at least one field", __FILE__, __LINE__); }  
  }
  catch (...)
  {
    risa_error = true;
    risa_eof   = true;
    throw;  // Rethrows the original exception.
  }
    
  return result;        
}

bool RecordInputStream_Ascii::eos() {return risa_eof; }
  
bool RecordInputStream_Ascii::error() {return risa_error; }

bool RecordInputStream_Ascii::nextRecord() 
{
  bool result = true;

  if (risa_eof || risa_error)
  {
    result = false;
  }
  else
  {
    try
    {
      int lexcode;
      
      // Skip all blank lines.
      do {
        lexcode = risa_lexer->nextToken();
      } while (lexcode == AsciiLexer::AL_NEWLINE_);
      
      if (lexcode == AsciiLexer::AL_EOF_)
      {
        // We reached end of file.
        result   = false; 
        risa_eof = true;
      }
      else if (lexcode == AsciiLexer::AL_LEFT_CURLY_)
      { 
	// Read a record of the form {var=value, var=value ....} 
        bool gettingrecord = true;
	bool gotanentry    = false;

	// A field has no value unless given one, so remove all existing values
	rs_buffer->reset();
      
	while (gettingrecord)
	{
          lexcode = risa_lexer->nextToken();
	  
	  if (lexcode == AsciiLexer::AL_STRING_VALUE_)
	  {
	    gotanentry = true;
	    const char * fieldname = risa_lexer->tokenText();

	    // Get the index of the field.  This will throw an exception
	    // if the field does not exist.
	    int fieldindex = rs_buffer->getFieldIndex(fieldname);
	    
	    // Check there is no value already - the value can only be set once.
	    if (rs_buffer->hasValue(fieldindex))
	    { throw FileFormatException("Value can only be set once", __FILE__,__LINE__); }
	    
            // Parse the equals character.
	    lexcode = risa_lexer->nextToken();
	    if (lexcode != AsciiLexer::AL_EQUALS_) 
	    { throw FileFormatException("Expected equals character", __FILE__, __LINE__); }
	    
	    // Parse the field value.
	    lexcode = risa_lexer->nextToken();
	    const char * text = risa_lexer->tokenText();
	    setFieldValue(fieldindex, lexcode, text);

	    // A comma indicates there is more, right bracket indicates the end.
	    // Nothing else is allowed.
	    lexcode = risa_lexer->nextToken();
	    if (lexcode == AsciiLexer::AL_RIGHT_CURLY_) { 
	      gettingrecord = false; 
	    }
	    else if (lexcode != AsciiLexer::AL_COMMA_) { 
	      throw FileFormatException("Expected a comma character", __FILE__, __LINE__); 
	    }
	  }
	  else if ((lexcode == AsciiLexer::AL_RIGHT_CURLY_) && (!gotanentry)) { 
	    // Completely empty record.
	    gettingrecord = false; 
	  }
	  else { 
	    throw FileFormatException("Unknown syntax", __FILE__, __LINE__); 
	  }
	}
      }  
      else
      {
        // Read a record of the form : value value value .... }
        for (int fieldindex = 1; (fieldindex <= numFields()); fieldindex++)
        {
          // Don't read a token for the 1st field - we already have it.
          if (fieldindex != 1)
          { lexcode       = risa_lexer->nextToken();}

	  // is this array field?
	  if ( isFieldArrayType(fieldindex) ) {
	    ArrayType *at = static_cast<ArrayType *>(getFieldType(fieldindex));
	    int length = at->getLength();
	    BasicType *bat = at->getBaseType();
	    // have the first element's lexcode in hand
	    setArrayFieldValue(fieldindex, bat, length, risa_lexer, lexcode);
	  } else {
	    const char * text = risa_lexer->tokenText();
	    setFieldValue(fieldindex, lexcode, text);
	  }
	}
      }
        
      /* Each record is ended by a newline. The last record can be followed
       * immediately by end of file. */
      if (result)
      {
        int lexcode = risa_lexer->nextToken();
          
        if (! ((lexcode == AsciiLexer::AL_EOF_) || 
              (lexcode == AsciiLexer::AL_NEWLINE_)))
        { throw FileFormatException("Bad end of record", __FILE__, __LINE__); }
      }
    }
    catch (...)
    {
      risa_error = true;
      risa_eof   = true;
      result     = false;
      
      // Reset the buffer to forget all the previous values.
      rs_buffer->reset();

      throw;  // Rethrows the original exception.
    }
  }
  
  if (!result) 
  {
    // Reset the buffer to forget all the previous values.
    rs_buffer->reset();
  }
  
  return result;
}

void RecordInputStream_Ascii::setArrayFieldValue
(int fieldindex, BasicType *type, int length, AsciiLexer* lexer, int lexcode)
{
  // create a vector of appropriate type
  // BasicType  *type = getFieldType(fieldindex);        
  int i;

  if (type->equals(BasicType::Int)) { 
    vector<int> vint = vector<int>();
    for(i=0; i<length; i++) {
      if(i) {
	lexcode = lexer->nextToken();
      }
      const char * text = lexer->tokenText();
      errno = 0;
      long integer = strtol(text, NULL, 10); 
      
      // check for over or under flow.
      if (errno == ERANGE || integer > INT_MAX || integer < INT_MIN) { 
	string message = "Overflow or underflow on int value = ";
	message += text;
	throw FileFormatException(message, __FILE__, __LINE__); 
      }
      vint.push_back(integer);
    } 
    rs_buffer->setIntArrayValue(fieldindex, vint);
    
  } else if (type->equals(BasicType::Real)) {
    vector<double> vdbl = vector<double>();
    for(i=0; i<length; i++) {
      if(i) {
	lexcode = lexer->nextToken();
      }
      const char * text = lexer->tokenText();
      errno = 0;
      double real = strtod(text, NULL);
      
      // check for overflow.
      if (errno == ERANGE) { 
	string message = "Overflow or underflow on real field with int value = ";
	message += text;
	throw FileFormatException(message, __FILE__, __LINE__); 
      }
      vdbl.push_back(real);
    }    
    rs_buffer->setRealArrayValue(fieldindex, vdbl);
  } else if (type->equals(BasicType::Bool)) {
    vector<bool> vbln = vector<bool>();
    for(i=0; i<length; i++) {
      if(i) {
	lexcode = lexer->nextToken();
      }
      const char * text = lexer->tokenText();
      errno = 0;
       if ((strcmp(text, "1") == 0)|| (strcmp(text, "0") == 0)) 
        {
	  vbln.push_back(atoi(text));
        }
        else { throw TypeException("Boolean values must be either 0 or 1.", __FILE__, __LINE__);}
    }
    rs_buffer->setBoolArrayValue(fieldindex, vbln);
  } else if (type->equals(BasicType::String)) {
    throw FileFormatException("Array of String is not supported", __FILE__, __LINE__);
  } else {
    throw FileFormatException("unsupported type", __FILE__, __LINE__);
  }

}

void RecordInputStream_Ascii::setFieldValue
(int fieldindex, int lexcode, const char * text)
{
  BasicType  * type = getFieldType(fieldindex);        
    
  switch (lexcode)
  {
    case AsciiLexer::AL_INT_VALUE_:
    {
      if (type->equals(BasicType::Int)) 
      { /* Normal integer field */
        // Convert the integer from text representation.
        errno = 0;
        long integer = strtol(text, NULL, 10); 
        
        // check for over or under flow.
        if (errno == ERANGE || integer > INT_MAX || integer < INT_MIN) 
        { 
	  string message = "Overflow or underflow on int value = ";
	  message += text;
	  throw FileFormatException(message, __FILE__, __LINE__); 
	}
        
        rs_buffer->setIntValue(fieldindex, (int) integer);
      }
      else if (type->equals(BasicType::Real)) 
      { /* A real field that happened to have an integer value */
        // Convert the double from text representation.
        errno = 0;
        double real = strtod(text, NULL);
      
        // check for overflow.
        if (errno == ERANGE) { 
	  string message = "Overflow or underflow on real field with int value = ";
	  message += text;
	  throw FileFormatException(message, __FILE__, __LINE__); 
	}
    
        rs_buffer->setRealValue(fieldindex, real);
      }
      else if (type->equals(BasicType::Bool)) 
      { /* Its a boolean, so must be either '1' or '0' */
        if ((strcmp(text, "1") == 0)|| (strcmp(text, "0") == 0)) 
        {
          rs_buffer->setBoolValue(fieldindex, (bool) atoi(text));
        }
        else { throw TypeException("Boolean values must be either 0 or 1.", __FILE__, __LINE__);}
      }
      else 
        { throw InternalError(__FILE__, __LINE__); }
      break;
    }
    case AsciiLexer::AL_REAL_VALUE_:
    {
      // Convert the double from text representation.
      errno = 0;
      double real = strtod(text, NULL);
      
      // check for overflow.
      if (errno == ERANGE) { 
	  string message = "overflow or underflow on real value = ";
	  message += text;
	  throw FileFormatException(message, __FILE__, __LINE__); 
      }
    
      rs_buffer->setRealValue(fieldindex, real); 
      break;
    }
    case AsciiLexer::AL_STRING_VALUE_:
    {
      rs_buffer->setStringValue(fieldindex, text);
      break;
    }
    default:
      string message = "unknown/missing value = ";
      message += text;
      throw FileFormatException(message, __FILE__, __LINE__);
  }
}

string RecordInputStream_Ascii::toString()
{
  return "hack";
}

/*******************************************************************************
* Ascii Record Output Stream Implementation.
*******************************************************************************/
RecordOutputStream_Ascii::RecordOutputStream_Ascii 
(RecordOutputStream * stream, string filename, bool writemetadata)
{
  // Open the file.
  rosa_outputstream = new ofstream(filename.c_str());

  // Check that it was opened successfully.
  if (! rosa_outputstream->good()) 
  { throw FileCreateException(filename, errno, __FILE__, __LINE__); }

  rosa_streamismine = true;

  initialize(stream, writemetadata);
}

RecordOutputStream_Ascii::RecordOutputStream_Ascii 
(int streamindex, string filename, bool writemetadata)
{
  // Get a reference to the stream referred to by the index.
  RecordOutputStream * stream = getStream(streamindex);

  // Open the file.
  rosa_outputstream = new ofstream(filename.c_str());

  // Check that it was opened successfully.
  if (! rosa_outputstream->good()) 
  { throw FileCreateException(filename, errno, __FILE__, __LINE__); }

  rosa_streamismine = true;

  initialize(stream, writemetadata);
}
 
    
RecordOutputStream_Ascii::RecordOutputStream_Ascii
(RecordOutputStream * stream, ostream * asciistream, bool writemetadata)
{
  rosa_outputstream = asciistream;
  rosa_streamismine = false;
  
  initialize(stream, writemetadata);
}

RecordOutputStream_Ascii::RecordOutputStream_Ascii
(int streamindex, ostream * asciistream, bool writemetadata)
{
  // Get a reference to the stream referred to by the index.
  RecordOutputStream * stream = getStream(streamindex);

  rosa_outputstream = asciistream;
  rosa_streamismine = false;
  
  initialize(stream, writemetadata);
}

void RecordOutputStream_Ascii::initialize
(RecordOutputStream * stream, bool writemetadata)
{
  stream->registerForUpdates(this);
  
  // We don't really need our own buffer (its unchanged) -- reference a copy.
  rs_buffer = stream->getBuffer();

  ros_isroot      = false;
  ros_inheritable = false;  // Says that this stream cannot be inherited.
  ros_closed      = false;
  
  // Add this to the list of output streams.
  addNewStream(this);

  if (writemetadata) { writeMetaData(); }
}  

RecordOutputStream_Ascii::~RecordOutputStream_Ascii() 
{
  if (rosa_streamismine)
    { delete rosa_outputstream; }
    
  removeOldStream(this);
  
  // We shared our buffer, so avoid deletion by the base class.
  rs_buffer = 0;
}
  
void RecordOutputStream_Ascii::pushRecord() 
{
  // Complain if the stream has already been closed.
  if (ros_closed)
  { throw EndofStreamException(__FILE__, __LINE__); }
  
  // Write out each field.  If a field does not have a value then
  // an exception will be thrown.
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    BasicType * type = getFieldType(fieldindex);

    // Put a single space between entries.
    if (fieldindex != 1)
      { (*rosa_outputstream) << " "; }
    
    if (type->isArrayType()) {
      ArrayType *at = static_cast<ArrayType *>(type);
      BasicType *bat = at->getBaseType();

      if (bat->equals(BasicType::Int)) {
	vector<int> vint = rs_buffer->getIntArrayValue(fieldindex);
	vector<int>::iterator itr = vint.begin();
	while(itr != vint.end()) {
	  (*rosa_outputstream) << *itr++ ;
	  if(itr != vint.end()) (*rosa_outputstream) << " ";
	}
      } else if (bat->equals(BasicType::Real)) {
	vector<double> vdbl = rs_buffer->getRealArrayValue(fieldindex);
	vector<double>::iterator itr = vdbl.begin();
	while(itr != vdbl.end()) {
	  (*rosa_outputstream) << *itr++ ;
	  if(itr != vdbl.end()) (*rosa_outputstream) << " ";
	}
      } else if(bat->equals(BasicType::Bool)) {
	vector<bool> vbln = rs_buffer->getBoolArrayValue(fieldindex);
	vector<bool>::iterator itr = vbln.begin();
	while(itr != vbln.end()) {
	  (*rosa_outputstream) << *itr++ ;
	  if(itr != vbln.end()) (*rosa_outputstream) << " ";
	}
      } else {
	// array of strings not supported
	abort();
      }
    } else {
      
      // Write out the value of the field.
      if (type->equals(BasicType::Int))
	{
	  (*rosa_outputstream) << rs_buffer->getIntValue(fieldindex);
	}
      else if (type->equals(BasicType::Real))
	{
	  // form is a GNU extension - used here to dump all significant figures.
	  //(*rosa_outputstream).form("%g", rs_buffer->getRealValue(fieldindex));

          // Good question what to do here. We opt for a lot of precision in
          // the general format (16 ought to do it for IEEE double precision)
          int oldprecision = (*rosa_outputstream).precision();
         (*rosa_outputstream) << setprecision(16);
         (*rosa_outputstream) <<  rs_buffer->getRealValue(fieldindex);
         (*rosa_outputstream) << setprecision(oldprecision);

	}
      else if (type->equals(BasicType::Bool))
	{
	  (*rosa_outputstream) << rs_buffer->getBoolValue(fieldindex);
	}
      else if (type->equals(BasicType::String))
	{ 
	  // Convert escaped characters -- does not modify original string.
	  string fieldvalue = rs_buffer->getStringValue(fieldindex);
	  escapeString(fieldvalue);
	  
	  // Print out escaped string.
	  (*rosa_outputstream) << "\"" << fieldvalue << "\"";
	}
      else
	{
	  abort();
	}
    }
  }
  // Print a new line to end the record.
  (*rosa_outputstream) << "\n";
  
  // Flush the buffer to push out the record straight away.
  rosa_outputstream->flush();
}
 

string RecordOutputStream_Ascii::toString()
{
  return RecordOutputStream::toString();
}

 
void RecordOutputStream_Ascii::writeMetaData()
{
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    (*rosa_outputstream) << getFieldType(fieldindex)->toString() << " ";
    
    // The fieldname could include escape characters (it probably won't)
    string fieldname = getFieldName(fieldindex);
    escapeString(fieldname);
    
    (*rosa_outputstream) << "\"" << fieldname << "\"\n";    
  }
  
  (*rosa_outputstream) << endl;
}

void RecordOutputStream_Ascii::escapeString(string & str)
{
  replaceAll(str, "\\", "\\\\");
  replaceAll(str, "\n", "\\n");
  replaceAll(str, "\"", "\\\"");
  replaceAll(str, "\t", "\\t");
}

void RecordOutputStream_Ascii::replaceAll
(string &str, const char * original, const char * replacewith)
{
  string::size_type findindex;
  string::size_type searchindex = 0;
  int length        = strlen (original);
  int replacelength = strlen (replacewith);
      
  while ((findindex = str.find(original, searchindex)) != string::npos)
  { 
    str.replace(findindex, length, replacewith); 
    searchindex = findindex + replacelength; 
  }
}
