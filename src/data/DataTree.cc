#include <Tile.h>
#include <Node.h>
#include <Frontier.h>
#include <DataTree.h>
#include <Distribution.h>
#include <Tessellation.h>
#include <RecordInputStream.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::DataTree)

using namespace BIE;

DataTree::DataTree
(RecordInputStream * inputstream, SampleDistribution* d, Tessellation* tess)
  : BaseDataTree()
{  
  this->fd_distributionfactory = d;
  this->fd_tess                = tess;
  fd_total                     = 0;
  fd_offgrid                   = 0;

  // Allocate a clone of the distribution to each tile in tessellation.
  for (int tileid = fd_tess->MinID(); tileid <= fd_tess->MaxID(); tileid++) {
    fd_tiledistributions[tileid] = fd_distributionfactory->New(); 
  }  

  // Read the data and build the distribution
  AddDataPoints(inputstream);

  // Create a frontier.
  fd_defaultfrontier = new Frontier(fd_tess);
}

void DataTree::AddDataPoints
(RecordInputStream * inputstream)
{
  // Check that the fields required by the distribution are in the input stream
  RecordType * streamtype = inputstream->getType();
  RecordType * disttype   = fd_distributionfactory->getRecordType();
  
  // Check that the fields required by the distribution factory are present.
  if (! streamtype->isSubset(disttype)) {
    throw TypeException(streamtype, disttype,__FILE__, __LINE__); 
  }
  
  // get the field indices of the coordinate fields - using indices
  // is faster than doing a name lookup all the time.
  int x_index = inputstream->getFieldIndex("x");
  int y_index = inputstream->getFieldIndex("y");
  
  RecordBuffer * databuffer = inputstream->getBuffer();

  // Keep reading data points until there are none left.
  // This point will be added to _all_ of the bins it falls into.
  while (inputstream->nextRecord())
  {
    bool used = false;      // True if this point was used in the distribution.
    vector<int> found;      // a vector of tileids

    // Get the coordinates of the point.
    double x = inputstream->getRealValue(x_index);
    double y = inputstream->getRealValue(y_index);

    // Find all the bins this point is in.
    fd_tess->FindAll(x, y, found);
    
    for(unsigned int foundindex = 0; foundindex <found.size(); foundindex++)
    {
      int binid = found[foundindex];

      if (fd_tiledistributions[binid]->AccumData(1.0, databuffer)) {
	used = true; 
      }
    }

    // Is point used in distribution?
    if (used) {
      fd_total++; 
    } else {
      fd_offgrid++; 
    }
  }
}

