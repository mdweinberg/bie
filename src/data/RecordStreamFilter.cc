#include <iomanip>

#include <BasicType.h>
#include <ArrayType.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStream.h>
#include <RecordStreamFilter.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordStreamFilter)

using namespace BIE;

/*******************************************************************************
* Make connection methods.
*******************************************************************************/
void RecordStreamFilter::connect
(string streamfieldname, string inputname)
{
  int streamfieldindex = filter_streamtype->getFieldIndex(streamfieldname);
  int inputindex       = getInputIndex(inputname);
  connect(streamfieldindex, inputindex);
}

void RecordStreamFilter::connect
(int streamfieldindex, string inputname)
{
  int inputindex       = getInputIndex(inputname);
  connect(streamfieldindex, inputindex);
}

void RecordStreamFilter::connect
(string streamfieldname, int inputindex)
{
  int streamfieldindex = filter_streamtype->getFieldIndex(streamfieldname);
  connect(streamfieldindex, inputindex);
}

void RecordStreamFilter::connect
(int streamfieldindex, int inputindex)
{
  bool isStreamFieldArrayType=false;
  ArrayType *at = NULL;

  // Check the range of the indices.
  if (! filter_streamtype->isValidFieldIndex(streamfieldindex))
  { throw NoSuchFieldException(streamfieldindex,__FILE__,__LINE__); }

  if (! isValidInputIndex(inputindex))
  { throw NoSuchFilterInputException(inputindex, __FILE__,__LINE__); }
  
  // Check to see we are not trying to modify an attached filter.
  if (filter_isattached)
  {  throw AttachedFilterException(__FILE__,__LINE__); }

  // Input indices start at one - but we start at zero.
  inputindex--;

  // Get the types of the fields we aren connecting.
  BasicType * inputtype       = (filter_input[inputindex]).inputtype;
  BasicType * streamfieldtype = filter_streamtype->getFieldType(streamfieldindex);

  // if streamfield is array then its base element type should match
  if (streamfieldtype->isArrayType()) {
    isStreamFieldArrayType=true;
    at = static_cast<ArrayType *>(streamfieldtype);
    streamfieldtype = at->getBaseType();
  }

  // If the types don't match then complain.
  if (! (inputtype->equals(streamfieldtype)))
  { 
    throw TypeException("Types of fields being connected do not match.", 
                        __FILE__,__LINE__); 
  }

  // Make the connection.
  if (filter_input[inputindex].isset)
  { 
    // This is a set input -- add the connection if not already there.
    int  numconnections = filter_inputcardinality[inputindex];
    
    // Search for a duplicate connection to this input.    
    bool gotduplicate   = false;
    for(int connection = 0; connection < numconnections; connection++)
    {
      if (filter_connections[inputindex][connection] == streamfieldindex)
      { 
        gotduplicate = true; 
	break;
      }
    }
    
    if (!gotduplicate)
    {
      // Add the connection at the end of the list.
      filter_connections[inputindex][numconnections] = streamfieldindex;
      filter_inputcardinality[inputindex]++;
      filter_input[inputindex].isarray = isStreamFieldArrayType;
    }
  }
  else 
  { 
    // This is a scalar input -- overwrite any existing connection.
    filter_connections[inputindex][0]   = streamfieldindex;
    filter_inputcardinality[inputindex] = 1;
    filter_input[inputindex].isarray = isStreamFieldArrayType;
  }

  // fix the outputs to be arraytypes if the input scalars are arraytype
  // set the output buffer too
  // Loop through the OUTPUT array until we come across a null record.
  if (isStreamFieldArrayType && !filter_input[inputindex].isset) {

    // by default (before connect) the output record buffer was created as scalar
    delete filter_outputbuffer;

    RecordType * outputtype = new RecordType();
    int output_num=0;
    while (!(filter_output[output_num].outputname.size() == 0 &&
	     filter_output[output_num].outputtype == 0 ))
      {
	// Insert the new field at the end of the type (+1 for field indices).
	RecordType * newtype;
	ArrayType *nat = new ArrayType(at);
	newtype = outputtype->insertField (output_num + 1, 
					   filter_output[output_num].outputname, 
					   nat); 
	
	// Record types are immutable so we have to clean up.
	delete outputtype;
	outputtype = newtype;
	output_num++;
      }
    
    // Create the output buffer.
    filter_outputbuffer = new RecordBuffer(outputtype);
  }
}

/*******************************************************************************
* Disconnection methods.
*******************************************************************************/
void RecordStreamFilter::disconnect
(string streamfieldname, string inputname) 
{
  int streamfieldindex = filter_streamtype->getFieldIndex(streamfieldname);
  int inputindex       = getInputIndex(inputname);
  disconnect(streamfieldindex, inputindex);
}

void RecordStreamFilter::disconnect
(int streamfieldindex, string inputname) 
{
  int inputindex       = getInputIndex(inputname);
  disconnect(streamfieldindex, inputindex);
}
  
void RecordStreamFilter::disconnect
(string streamfieldname, int inputindex) 
{
  int streamfieldindex = filter_streamtype->getFieldIndex(streamfieldname);
  disconnect(streamfieldindex, inputindex);
}

void RecordStreamFilter::disconnect
(int streamfieldindex, int inputindex) 
{
  // Check the range of the indices.
  if (! filter_streamtype->isValidFieldIndex(streamfieldindex))
  { throw NoSuchFieldException(streamfieldindex, __FILE__,__LINE__); }

  if (! isValidInputIndex(inputindex))
  { throw NoSuchFilterInputException(inputindex, __FILE__,__LINE__); }
  
  // Check to see we are not trying to modify an attached filter.
  if (filter_isattached) 
  { throw AttachedFilterException(__FILE__,__LINE__); }

  // Input indices start at one - but we start at zero.
  inputindex--;

  if (filter_input[inputindex].isset)
  { 
    // This is a set input -- add the connection if not already there.
    int  numconnections = filter_inputcardinality[inputindex];
    
    // Search for this connection   
    bool foundconnection = false;
    int  matchingindex   = 0;
     
    for(int connection = 0; connection < numconnections; connection++)
    {
      if (filter_connections[inputindex][connection] == streamfieldindex)
      { 
        foundconnection = true; 
	matchingindex   = connection;
	break;
      }
    }
    
    if (foundconnection)
    {
      // Shift the other connections down (overwriting the specified field).
      memmove(&(filter_connections[inputindex][matchingindex]), 
              &(filter_connections[inputindex][matchingindex+1]),
	      sizeof(int) * (numconnections - matchingindex - 1));
      
      // We have one less connection.
      filter_inputcardinality[inputindex]--;
     
      // Reset the end (now disused) field to zero.  
      filter_connections[inputindex][numconnections] = 0;
    }
    else
    { throw NoSuchConnection(__FILE__,__LINE__); }
  }
  else 
  { 
    if ((filter_inputcardinality[inputindex] != 1) ||
        (filter_connections[inputindex][0] != streamfieldindex))
    { throw NoSuchConnection(__FILE__,__LINE__); }
    
    // Reset the connection and the cardinality.
    filter_connections[inputindex][0]   = 0;
    filter_inputcardinality[inputindex] = 0;
  }

  filter_input[inputindex].isarray = false;
}

void RecordStreamFilter::disconnect (string inputname) 
{
  int inputindex       = getInputIndex(inputname);
  disconnect(inputindex);
}

void RecordStreamFilter::disconnect(int inputindex) 
{
  // Check the range of the indices.
  if (! isValidInputIndex(inputindex))
  { throw NoSuchFilterInputException(inputindex, __FILE__,__LINE__); }
  
  // Our indices are one lower
  inputindex--;
  
  // Check to see we are not trying to modify an attached filter.
  if (filter_isattached)
  {  throw AttachedFilterException(__FILE__,__LINE__); }

  if (filter_input[inputindex].isset) 
  { 
    // Erase all connections.
    memset(&filter_connections[inputindex][0], 0, 
           sizeof(int) * filter_streamtype->numFields());
  }
  else
  { 
    // Erase connection 
    filter_connections[inputindex][0] = 0; 
  }    
  
  filter_inputcardinality[inputindex] = 0;
}
  
void RecordStreamFilter::disconnect ()
{
  for (int fieldindex = 1; fieldindex <= numInputs(); fieldindex++)
  { disconnect(fieldindex); }
}
  
/*******************************************************************************
* Output field renaming methods.
*******************************************************************************/
void RecordStreamFilter::renameOutputField
(string outputname, string newname)
{
  int outputindex;
  
  try {
    outputindex = filter_outputbuffer->getFieldIndex(outputname);
  } 
  catch (NoSuchFieldException) 
  { throw NoSuchFilterOutputException (outputname,__FILE__,__LINE__); }

  renameOutputField(outputindex, newname);
}

void RecordStreamFilter::renameOutputField
(int outputindex, string newname)
{
  RecordBuffer * newbuffer;
  
  // To rename we must create a new buffer and then delete the old one
  // since record buffers are immutable.
  try {
    newbuffer = filter_outputbuffer->renameField(outputindex, newname);
  } 
  catch (NoSuchFieldException) 
  { throw NoSuchFilterOutputException (outputindex,__FILE__,__LINE__); }

  // BUG: should update the filter_outputthing too
  // (filter_output[outputindex]).outputname = newname;
  delete filter_outputbuffer;
  filter_outputbuffer = newbuffer;
}

/*******************************************************************************
* Connection status query methods.
*******************************************************************************/
bool RecordStreamFilter::isConnected()
{
  bool result = true;
  
  for (int inputindex = 0; inputindex < numInputs(); inputindex++)
  {
    if (! filter_inputcardinality[inputindex])
    {
      result = false;
      break;
    }    
  }

  return result;
}

bool RecordStreamFilter::isConnected(string inputname)
{
  int inputindex       = getInputIndex(inputname);
  return isConnected(inputindex);
}

bool RecordStreamFilter::isConnected (int inputindex)
{
  // Check the range of the index.
  if (! isValidInputIndex(inputindex)) 
  { throw NoSuchFilterInputException(inputindex,__FILE__,__LINE__); }

  // Return true if there is at least one connection 
  // (-1 since indices start at 1, and we start ar 0).
  return (bool) (filter_inputcardinality[inputindex-1]);
}

int RecordStreamFilter::getInputCardinality(string inputname) 
{
  int inputindex       = getInputIndex(inputname);
  return getInputCardinality(inputindex);
}

int RecordStreamFilter::getInputCardinality(int inputindex)
{
  // Check the range of the index.
  if (! isValidInputIndex(inputindex)) 
  { throw NoSuchFilterInputException(inputindex,__FILE__,__LINE__); }

  // (-1 since indices start at 1, and we start ar 0).
  return (filter_inputcardinality[inputindex-1]);
}

bool RecordStreamFilter::isAttached()
{ return filter_isattached; }

bool RecordStreamFilter::nameClash()
{
  bool result = false;
  
  for (int outputindex = 0; outputindex < filter_numoutputs; outputindex++)
  {
    // BUG: should not use the default name, could have been renamed
    // char * outputname = (filter_output[outputindex]).outputname;
    RecordType *rt = filter_outputbuffer->getType();
    int rtfieldindex = outputindex+1;
    string outputfieldname = rt->getFieldName(rtfieldindex);

    const char * outputname = outputfieldname.c_str();

    if (filter_streamtype->isValidFieldName(outputname))
    {
      result = true;
      break;
    }
  }

  return result;
}

bool RecordStreamFilter::isUseable()
{
  bool result;
 
  bool connected = isConnected();
  bool attached  = isAttached();
  bool nameclash = nameClash();
 
  if (connected && (!attached) && (!nameclash)) {
    result = true; 
  }
  else {
    result = false;
  }
  
  return result;
}


RecordBuffer * RecordStreamFilter::getBuffer()     
{ return filter_outputbuffer; }
  

RecordType   * RecordStreamFilter::getOutputType() 
{ return filter_outputbuffer->getType(); }


RecordType   * RecordStreamFilter::getStreamType() 
{ return filter_streamtype; }
  

int RecordStreamFilter::numInputs()
{ return filter_numinputs; }


int RecordStreamFilter::getInputIndex(string inputname) 
{
  int  result = 0;
  bool foundname = false;

  for (int inputindex = 0; inputindex < filter_numinputs; inputindex++)
  {
    if (inputname == filter_input[inputindex].inputname)
    { 
      result = inputindex + 1;   // Answer is in user's notion of index.
      foundname = true;
      break;
    }
  }
 
  if (! foundname) 
  {throw NoSuchFilterInputException(inputname,__FILE__,__LINE__); }

  return result;
}
  
string RecordStreamFilter::getInputName(int inputindex) 
{
  if (!isValidInputIndex(inputindex))
  { throw NoSuchFilterInputException(inputindex,__FILE__,__LINE__); }

  return filter_input[inputindex-1].inputname;  
}


bool RecordStreamFilter::isValidInputName(string inputname) 
{ 
  bool foundname = false;

  for (int inputindex = 0; inputindex < filter_numinputs; inputindex++)
  {
    if (inputname == filter_input[inputindex].inputname)
    { 
      foundname = true;
      break;
    }
  }
  
  return foundname;
} 
   
bool RecordStreamFilter::isValidInputIndex(int inputindex) 
{ 
  bool result = false;
  
  if ((inputindex >= 1) && (inputindex <= filter_numinputs)) 
  { result = true; }

  return result;
} 

string RecordStreamFilter::getDefaultOutputName(int outputindex) 
{
  if (!isValidOutputIndex(outputindex))
  { throw NoSuchFilterOutputException(outputindex,__FILE__,__LINE__); }

  return filter_output[outputindex-1].outputname;  
}

int RecordStreamFilter::getDefaultOutputIndex(string outputname) 
{
  int  result = 0;
  bool foundname = false;

  for (int outputindex = 0; outputindex < filter_numoutputs; outputindex++)
  {
    if (outputname == filter_output[outputindex].outputname)
    { 
      result = outputindex + 1;   // Answer is in user's notion of index.
      foundname = true;
      break;
    }
  }
 
  if (! foundname) 
  {throw NoSuchFilterOutputException(outputname,__FILE__,__LINE__); }

  return result;
}

bool RecordStreamFilter::isValidDefaultOutputName(string outputname) 
{ 
  bool foundname = false;

  for (int outputindex = 0; outputindex < filter_numoutputs; outputindex++)
  {
    if (outputname == filter_output[outputindex].outputname)
    { 
      foundname = true;
      break;
    }
  }
  
  return foundname;
} 
   
bool RecordStreamFilter::isValidOutputIndex(int outputindex) 
{ 
  bool result = false;
  
  if ((outputindex >= 1) && (outputindex <= filter_numoutputs)) 
  { result = true; }

  return result;
} 

void RecordStreamFilter::attachToStream (RecordStream * stream) 
{
  if (!isUseable()) 
  { throw UnusableFilterException(__FILE__,__LINE__); }
  
  if (!filter_streamtype->orderedEquals(stream->getType()))
  { 
    throw TypeException("Stream type used in filter construction does not match type of stream being attached.", 
			__FILE__,__LINE__); 
  }

  filter_isattached = true;
  filter_stream     = stream;  
}

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif
void RecordStreamFilter::initialize
(const filterIn  input[], 
 const filterOut output[],
 RecordStream   *         stream)
{
  filter_input      = (filterIn *)  input;
  filter_output     = (filterOut *) output;
  filter_streamtype = stream->getType();
  filter_isattached = false;

  #ifdef DEBUG_TESSTOOL
  ferr << "RecordStreamFilter::initialize input= "<< input <<" output =" << output << " stream=" << stream << endl;
  #endif
  // Loop through until we come across a null record - we find the number
  // of inputs and check for duplicate names.
  filter_numinputs = 0;	
  while (!(filter_input[filter_numinputs].inputname.size() == 0 &&
           filter_input[filter_numinputs].inputtype == 0 &&
           filter_input[filter_numinputs].isset     == 0 ))
  { 
    for (int inputindex = 0; inputindex < filter_numinputs; inputindex++)
    {
      if (filter_input[inputindex].inputname == 
	  filter_input[filter_numinputs].inputname)
	{ 
	  throw DuplicateFieldException(filter_input[inputindex].inputname,
					__FILE__,__LINE__); 
	}
    }

#ifdef DEBUG_TESSTOOL
    ferr << " input name=" << filter_input[filter_numinputs].inputname  << endl;
#endif
    filter_numinputs++; 
  }

#ifdef DEBUG_TESSTOOL
  ferr << "done with inputs filter_numinputs=" << filter_numinputs << endl;
#endif
  
  // Allocate memory for the connection related arrays.
  filter_connections      = vector< vector<int> > (filter_numinputs);
  filter_inputcardinality = vector< int > (filter_numinputs);
#ifdef DEBUG_TESSTOOL
  ferr << "after newing arrays" << endl;
  ferr.flush();
#endif
  // Set all cardinalities to 0.
  memset(&filter_inputcardinality[0], 0, sizeof(int)* filter_numinputs);
#ifdef DEBUG_TESSTOOL
  ferr << "after memset" << endl;
#endif

  // Allocate memory for the arrays holding input field connections.
  for (int inputindex = 0; inputindex < filter_numinputs; inputindex++)
  {
#ifdef DEBUG_TESSTOOL
    ferr << inputindex << " ";
    ferr.flush();
#endif
    if (filter_input[inputindex].isset) 
      { 
#ifdef DEBUG_TESSTOOL
	ferr << "allocate for mem connects isSet=true ";
	ferr.flush();
#endif
	filter_connections[inputindex] = vector<int>(filter_streamtype->numFields());
      } else
	{     
#ifdef DEBUG_TESSTOOL
	  ferr << "allocate for mem connects isSet=false ";
	  ferr.flush();
#endif
	  filter_connections[inputindex] = vector<int>(1); 
	}    
#ifdef DEBUG_TESSTOOL
    ferr << inputindex;
    for (unsigned nn=0; nn<filter_connections[inputindex].size(); nn++)
      ferr << setw(11) << filter_connections[inputindex][nn];
    ferr << endl << flush;
#endif
  }
  
  // Process the output specification.
  filter_numoutputs = 0;
  RecordType * outputtype = new RecordType();
#ifdef DEBUG_TESSTOOL
  ferr << "looping through outputs" << endl;
  ferr.flush();
#endif
  // Loop through the OUTPUT array until we come across a null record.
  while (!(filter_output[filter_numoutputs].outputname.size() == 0 &&
           filter_output[filter_numoutputs].outputtype == 0 ))
  {
    // Insert the new field at the end of the type (+1 for field indices).
    RecordType * newtype;
    newtype = outputtype->insertField (filter_numoutputs + 1, 
                                       filter_output[filter_numoutputs].outputname, 
				       filter_output[filter_numoutputs].outputtype); 

    // Record types are immutable so we have to clean up.
    delete outputtype;
    outputtype = newtype;

    filter_numoutputs++;
  }

  // Create the output buffer.
  filter_outputbuffer = new RecordBuffer(outputtype);
#ifdef DEBUG_TESSTOOL
  ferr << "RecordStreamFilter::initialize return" << endl;
  ferr.flush();
#endif
}  

bool RecordStreamFilter::isArray(int inputindex)
{
  // for this filter inpout index see 
  // if the streamindex connected to it is array
  if (filter_input[inputindex-1].isarray) {
    return true;
  } else {
    return false;
  }
}
  
/*******************************************************************************
* Get scalar input methods.
*******************************************************************************/
int RecordStreamFilter::getIntInput (string inputname)
{
  int inputindex = getInputIndex(inputname);
  return getIntInput(inputindex);
}

int RecordStreamFilter::getIntInput(int inputindex)
{
  if ((filter_input[inputindex-1]).isset) 
  { throw NotScalarInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][0];
  return (filter_stream->getBuffer())->getIntValue(streamindex);
}

double RecordStreamFilter::getRealInput (string inputname)
{
  int inputindex = getInputIndex(inputname);
  return getRealInput(inputindex);
}

double RecordStreamFilter::getRealInput (int inputindex)
{
  if ((filter_input[inputindex-1]).isset) 
  { throw NotScalarInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][0];
  return (filter_stream->getBuffer())->getRealValue(streamindex);
}

bool RecordStreamFilter::getBoolInput (string inputname)
{
  int inputindex = getInputIndex(inputname);
  return getBoolInput(inputindex);
}

bool   RecordStreamFilter::getBoolInput  (int inputindex)
{
  if ((filter_input[inputindex-1]).isset) 
  { throw NotScalarInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][0];
  return (filter_stream->getBuffer())->getBoolValue(streamindex);
}

string RecordStreamFilter::getStringInput(string inputname)
{
  int inputindex = getInputIndex(inputname);
  return getStringInput(inputindex);
}

string RecordStreamFilter::getStringInput(int inputindex)
{
  if ((filter_input[inputindex-1]).isset)
  { throw NotScalarInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][0];
  return (filter_stream->getBuffer())->getStringValue(streamindex);
}

vector<int> RecordStreamFilter::getIntArrayInput(int inputindex)
{
  if (!((filter_input[inputindex-1]).isset || (filter_input[inputindex-1]).isarray))
  { throw NotScalarInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][0];
  return (filter_stream->getBuffer())->getIntArrayValue(streamindex);
}

vector<double> RecordStreamFilter::getRealArrayInput(int inputindex)
{
  if (! ((filter_input[inputindex-1]).isset || (filter_input[inputindex-1]).isarray) )
  { throw NotScalarInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][0];
  return (filter_stream->getBuffer())->getRealArrayValue(streamindex);
}

vector<bool> RecordStreamFilter::getBoolArrayInput(int inputindex)
{
  if (!((filter_input[inputindex-1]).isset || (filter_input[inputindex-1]).isarray))
  { throw NotScalarInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][0];
  return (filter_stream->getBuffer())->getBoolArrayValue(streamindex);
}

/*******************************************************************************
* Get "set input" methods. 
*******************************************************************************/
int RecordStreamFilter::getIntInput (string inputname, int setindex)
{
  int inputindex = getInputIndex(inputname);
  return getIntInput(inputindex, setindex);
}

int RecordStreamFilter::getIntInput (int inputindex, int setindex)
{
  if (!((filter_input[inputindex-1]).isset))
  { throw NotSetInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][setindex-1];
  return (filter_stream->getBuffer())->getIntValue(streamindex);
}

double RecordStreamFilter::getRealInput (string inputname, int setindex)
{
  int inputindex = getInputIndex(inputname);
  return getRealInput(inputindex, setindex);
}

double RecordStreamFilter::getRealInput  (int inputindex, int setindex)
{
  if (!((filter_input[inputindex-1]).isset)) 
  { throw NotSetInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][setindex-1];
  return (filter_stream->getBuffer())->getRealValue(streamindex);
}

bool   RecordStreamFilter::getBoolInput (string inputname, int setindex)
{
  int inputindex = getInputIndex(inputname);
  return getIntInput(inputindex, setindex);
}

bool   RecordStreamFilter::getBoolInput  (int inputindex, int setindex)
{
  if (!((filter_input[inputindex-1]).isset))
  { throw NotSetInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][setindex-1];
  return (filter_stream->getBuffer())->getBoolValue(streamindex);
}

string RecordStreamFilter::getStringInput(string inputname, int setindex)
{
  int inputindex = getInputIndex(inputname);
  return getStringInput(inputindex, setindex);
}

string RecordStreamFilter::getStringInput(int inputindex, int setindex)
{
  if (!((filter_input[inputindex-1]).isset))
  { throw NotSetInputException(inputindex, __FILE__,__LINE__); }

  int streamindex = filter_connections[inputindex-1][setindex-1];
  return (filter_stream->getBuffer())->getStringValue(streamindex);
}

/*******************************************************************************
* setOutput methods.
*******************************************************************************/
void RecordStreamFilter::setIntOutput (string outputname, int value)
{ 
  int outputindex = getDefaultOutputIndex(outputname);
  filter_outputbuffer->setIntValue(outputindex, value);
}

void RecordStreamFilter::setIntOutput (int outputindex, int value)
{ filter_outputbuffer->setIntValue(outputindex, value);}

void RecordStreamFilter::setRealOutput (string outputname, double value)
{ 
  int outputindex = getDefaultOutputIndex(outputname);
  filter_outputbuffer->setRealValue(outputindex, value);
}

void RecordStreamFilter::setRealOutput (int outputindex, double value)
{ filter_outputbuffer->setRealValue(outputindex, value);}

void RecordStreamFilter::setBoolOutput (string outputname, bool value)
{
  int outputindex = getDefaultOutputIndex(outputname);
  filter_outputbuffer->setBoolValue(outputindex, value);
} 

void RecordStreamFilter::setBoolOutput (int outputindex, bool value)
{ filter_outputbuffer->setBoolValue(outputindex, value);}

void RecordStreamFilter::setStringOutput(string outputname, string value)
{
  int outputindex = getDefaultOutputIndex(outputname);
  filter_outputbuffer->setStringValue(outputindex, value);
}

void RecordStreamFilter::setStringOutput(int outputindex, string value)
{ filter_outputbuffer->setStringValue(outputindex, value);}

void RecordStreamFilter::setIntArrayOutput(int outputindex, vector<int> vint)
{
  filter_outputbuffer->setIntArrayValue(outputindex, vint);
}
void RecordStreamFilter::setRealArrayOutput(int outputindex, vector<double> vdbl)
{
  filter_outputbuffer->setRealArrayValue(outputindex, vdbl);
}
void RecordStreamFilter::setBoolArrayOutput(int outputindex, vector<bool> vbln)
{
  filter_outputbuffer->setBoolArrayValue(outputindex, vbln);
}

/*******************************************************************************
* To String.
*******************************************************************************/
string RecordStreamFilter::toString()
{
  std::ostringstream buffer;

  buffer << "INPUTS:\n";
  for (int inputindex = 1; inputindex <= filter_numinputs; inputindex++)
  {
    buffer << "   " << inputindex << ". " << getInputName(inputindex);
    buffer << "(" << (filter_input[inputindex-1].inputtype)->toString() << ", ";
    
    if (filter_input[inputindex-1].isset) 
    { 
      buffer << "set): {";
     
      for (int connection = 0; 
           connection < getInputCardinality(inputindex);
	   connection++)
      {
        int streamindex = filter_connections[inputindex-1][connection];
        buffer << filter_streamtype->getFieldName(streamindex);
	buffer << "(" << streamindex << ")";
	
	if (connection < getInputCardinality(inputindex) - 1)
	{ buffer << ", "; }
      }
      
      buffer << "}\n";
    }
    else 
    { 
      buffer << "scalar): ";
      
      if (isConnected(inputindex))
      {
        int streamindex = filter_connections[inputindex-1][0];
        buffer << filter_streamtype->getFieldName(streamindex);
	buffer << "(" << streamindex << ")";
      }
      else
      {
        buffer << "NOT CONNECTED";
      }
    
      buffer << "\n";
    }
  }
  
  buffer << "OUTPUTS:\n";
  for (int outputindex = 1; outputindex <= filter_numoutputs; outputindex++)
  {
    string defaultname = getDefaultOutputName(outputindex);
    string actualname  = filter_outputbuffer->getFieldName(outputindex);

    buffer << "   " << outputindex << ". " << actualname;
    buffer << " (" << filter_outputbuffer->getFieldType(outputindex)->toString();
    buffer << ")";
    
    if (defaultname != actualname)
    { buffer << " - renamed from default: " << defaultname; }
    
    buffer << endl;
  }

  buffer << "STATUS\n";

  buffer << "   All fields connected: ";
  if (isConnected()) { buffer << "YES\n"; } 
  else { buffer << "NO\n"; } 
    
  buffer << "   Filter attached to a stream: ";
  if (isAttached()) { buffer << "YES\n"; } 
  else { buffer << "NO\n"; } 

  buffer << "   Name clash in output: ";
  if (nameClash()) { buffer << "YES\n"; } 
  else { buffer << "NO\n"; } 
  
  //  buffer << '\0';  Not needed with stringstreams
  
  return buffer.str(); 
}
