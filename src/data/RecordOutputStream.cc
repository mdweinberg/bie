#include <iomanip>
#include <sstream>
#include <fstream>

#include <BasicType.h>
#include <RecordOutputStream.h>
#include <RecordType.h>
#include <RecordBuffer.h>
#include <RecordStreamFilter.h>
#include <BIEException.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordOutputStream)

using namespace BIE;

vector<RecordOutputStream*> RecordOutputStream::ros_inheritablestreams;

RecordOutputStream::RecordOutputStream(RecordType * streamtype)
{
  rs_buffer = new RecordBuffer(streamtype);
  
  // The new stream can be used by other streams + IS a root stream.
  ros_inheritable = true;
  ros_isroot      = true;
  ros_closed      = false;
  
  // Add this to the list of inheritable streams seen by the user.
  addNewStream(this);
  
  // The new stream's ancestor is null.
  ros_myinputstream = 0;
 
  // By default we do not have a filter.
  ros_filter = 0;
}

RecordOutputStream * RecordOutputStream::deleteField  (string fieldname) 
{
  RecordBuffer * newbuffer = rs_buffer->deleteField(fieldname);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::deleteField (int fieldindex)  
{
  RecordBuffer * newbuffer = rs_buffer->deleteField(fieldindex);
  return createDerivative(newbuffer);
}    

RecordOutputStream * RecordOutputStream::deleteRange 
(int startfield, int endfield) 
{   
  RecordBuffer * newbuffer = rs_buffer->deleteRange(startfield, endfield);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::selectFields(RecordType * selection)      
{
  RecordBuffer * newbuffer = rs_buffer->selectFields(selection);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::selectFields
(vector<int>* selection)      
{
  RecordBuffer * newbuffer = rs_buffer->selectFields(selection);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::selectFields
(vector<string>* selection)    
{
  RecordBuffer * newbuffer = rs_buffer->selectFields(selection);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::renameField
(string oldname, string newname) 
{
  RecordBuffer * newbuffer = rs_buffer->renameField(oldname, newname);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::renameField(int fieldindex, string newname) 
{
  RecordBuffer * newbuffer = rs_buffer->renameField(fieldindex, newname);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::moveField(string fieldname, int newposition)
{
  RecordBuffer * newbuffer = rs_buffer->moveField(fieldname, newposition);
  return createDerivative(newbuffer);
}

RecordOutputStream * RecordOutputStream::moveField(int oldposition, int newposition)
{
  RecordBuffer * newbuffer = rs_buffer->moveField(oldposition, newposition);
  return createDerivative(newbuffer);
}

void RecordOutputStream::setStringValue
(int fieldindex, string value)   
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setStringValue(fieldindex, value);
}

void RecordOutputStream::setIntValue (int fieldindex, int  value)   
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setIntValue(fieldindex, value);
}

void RecordOutputStream::setRealValue (int fieldindex, double value)   
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setRealValue(fieldindex, value);
}

void RecordOutputStream::setBoolValue  (int fieldindex, bool value)   
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setBoolValue(fieldindex, value);
}

void RecordOutputStream::setIntArrayValue  (int fieldindex, vector<int> value)   
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setIntArrayValue(fieldindex, value);
}

void RecordOutputStream::setRealArrayValue  (int fieldindex, vector<double> value)   
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setRealArrayValue(fieldindex, value);
}

void RecordOutputStream::setBoolArrayValue  (int fieldindex, vector<bool> value)   
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setBoolArrayValue(fieldindex, value);
}

void RecordOutputStream::setStringValue(string fieldname, string value) 
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setStringValue(fieldname, value);
}

void RecordOutputStream::setIntValue   (string fieldname, int value) 
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setIntValue(fieldname, value);
}

void RecordOutputStream::setRealValue  (string fieldname, double value) 
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setRealValue(fieldname, value);
}

void RecordOutputStream::setBoolValue  (string fieldname, bool value) 
{
  if (!ros_isroot) { throw NotRootException(__FILE__,__LINE__); }
  rs_buffer->setBoolValue(fieldname, value);
}

string RecordOutputStream::toString ()
{ 
  ostringstream sout;
  sout.setf(ios::left);
  sout << setw(18) << "Is root?"       << (ros_isroot ? "yes" : "no") << endl;
  sout << setw(18) << "Is inhertable?" << (ros_isroot ? "yes" : "no") << endl;
  sout << setw(18) << "Is closed?"     << (ros_isroot ? "yes" : "no") << endl;
  sout << endl << "RecordType:" << endl;

  RecordType *rt = getType();

  for (int fieldindex = 1; fieldindex <= rt->numFields(); fieldindex++) {
    string fieldname  = rt->getFieldName(fieldindex);
    string typestring = (rt->getFieldType(fieldindex))->toString();

    sout << "   Field #" << setw(4) << fieldindex
	 << setw(10) << fieldname
	 << setw(10) << typestring << endl;
  }
  
  return sout.str();
}



RecordOutputStream * RecordOutputStream::createDerivative 
(RecordBuffer * newbuffer)
{
  RecordOutputStream * result = new RecordOutputStream();

  // The existing stream provides update notification to this derivative.
  registerForUpdates(result);

  result->rs_buffer = newbuffer;

  // The new stream can be used by other streams + is not a root stream.
  result->ros_inheritable = true;
  result->ros_isroot      = false;
  result->ros_closed      = false;
  
  // Add this to the list of inheritable streams seen by the user.
  addNewStream(result);
  
  // The new stream's ancestor is this stream.
  result->ros_myinputstream = this;
  
  // By default we do not have a filter
  result->ros_filter = 0;
  
  return result;    
}

RecordOutputStream* RecordOutputStream::getStream(int streamindex)
{
  // Check index is within vector bounds (our indices are one lower).
  if (streamindex < 1 || (streamindex > (int)ros_inheritablestreams.size()))
  {  throw InvalidStreamIDException(streamindex,__FILE__,__LINE__); }
  
  RecordOutputStream * ros = ros_inheritablestreams[streamindex-1];
  
  if (!ros) 
  { throw InvalidStreamIDException(streamindex, __FILE__,__LINE__); }
  
  return ros;
}

void RecordOutputStream::addNewStream(RecordOutputStream * newstream)
{ 
  if (newstream->inheritable())
  { ros_inheritablestreams.insert(ros_inheritablestreams.end(), newstream); }
}

void RecordOutputStream::removeOldStream(RecordOutputStream * oldstream)
{ 
  for (int sindex = 0; sindex < (int)ros_inheritablestreams.size(); sindex++)
  {
    if (ros_inheritablestreams[sindex] == oldstream)
    {
      // Rather than removing index, null the reference.  
      ros_inheritablestreams[sindex] = 0;; 
      break;
    }
  }
}

vector<RecordOutputStream*> RecordOutputStream::getInheritableStreams()
{ return ros_inheritablestreams; }

void RecordOutputStream::registerForUpdates(RecordOutputStream * stream)
{
  // This stream must be able to provide updates (some can't).
  if (! ros_inheritable)
  { throw StreamInheritanceException(__FILE__, __LINE__); }
 
  ros_inheriting.push_back(stream);
}
  
RecordOutputStream *  RecordOutputStream::filterWith
(int position, RecordStreamFilter * filter)
{
  // Indicate that the filter is being used and plumb the inputs.
  filter->attachToStream(this);

  // Create a new buffer that inherits fields from this stream and the filter
  RecordBuffer* newbuffer = rs_buffer->insertRecord(position,filter->getBuffer());

  // Inherit this stream stream
  RecordOutputStream * result = createDerivative(newbuffer);

  result->ros_filter = filter;
    
  return result;    
}

RecordOutputStream *  RecordOutputStream::filterWith
(RecordStreamFilter * filter)
{
  // Indicate that the filter is being used and plumb the inputs.
  filter->attachToStream(this);

  // Create a new buffer that inherits fields from this stream
  RecordBuffer* newbuffer = new RecordBuffer(rs_buffer->getType());

  // Inherit this stream
  RecordOutputStream * result = createDerivative(newbuffer);

  result->ros_filter = filter;
    
  return result;    
}

void RecordOutputStream::pushRecord() 
{
  if (ros_isroot)
  {
    if (ros_closed)
    {  throw EndofStreamException(__FILE__, __LINE__); }
    
    if (! rs_buffer->hasValue())
    { throw NoValueException(__FILE__, __LINE__); }
  }

  // Call the filter **if there is one**.
#ifdef DEBUG_TESSTOOL
  ferr << "RecordOutputStream::pushRecord()" << endl;
#endif

  if (ros_filter != 0) 
  { 
#ifdef DEBUG_TESSTOOL
    ferr << "RecordOutputStream::pushRecord() calling compute() on filter" << endl; 
#endif
    ros_filter->compute(); 
  }

  // Push out the record to all inheriting streams.
  for (int streamindex = 0; streamindex < (int) ros_inheriting.size(); streamindex++)
  { ros_inheriting[streamindex]->pushRecord(); }
}

void RecordOutputStream::close()
{
  ros_closed = true;
  
  for (int streamindex = 0; streamindex < (int) ros_inheriting.size(); streamindex++)
  { ros_inheriting[streamindex]->close(); } 
}
