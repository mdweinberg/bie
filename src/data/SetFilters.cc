#include <SetFilters.h>
#include <BasicType.h>
#include <cmath>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::SumFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::MaxFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::MinFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::StatisticsFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::QuantileFilter)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::PSanityFilter)

using namespace BIE;

/*******************************************************************************
* Sum Filter.
*******************************************************************************/
// Input arguments.
const filterIn SumFilter::input[] =  
  { filterIn("X", BasicType::Real, true), filterIn() };

// Output arguments
const filterOut SumFilter::output[] = 
  { filterOut("total", BasicType::Real), filterOut() };

SumFilter::SumFilter (RecordStream * stream) 
{ 
  initialize(input, output, stream);  
  Xindex   = getInputIndex("X");
  sumindex = getDefaultOutputIndex("total"); 
}

void SumFilter::compute()
{
  double sum     = 0.0;
  int    setsize = getInputCardinality(Xindex);
  if (setsize > 1) {
    for (int setindex = 1; setindex <= setsize; setindex++) { 
      sum += getRealInput(Xindex, setindex);
    }
  } else {
    // the single input must be an array
    vector<double> vdbl = getRealArrayInput(Xindex);
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end()) {
      sum += *itr++;
    }
  }

  setRealOutput(sumindex, sum);
}

/*******************************************************************************
* Average Filter.
*******************************************************************************/
// Input arguments.
const filterIn AverageFilter::input [] =  
  { filterIn("X", BasicType::Real, true), filterIn() };

// Output arguments
const filterOut AverageFilter::output[] = 
  { filterOut("mean", BasicType::Real), filterOut() };

AverageFilter::AverageFilter (RecordStream * stream) 
{ 
  initialize(input, output, stream);  
  Xindex    = getInputIndex("X");
  meanindex = getDefaultOutputIndex("mean"); 
}

void AverageFilter::compute()
{
  double sum     = 0.0;
  double avg     = 0.0;
  int    setsize = getInputCardinality(Xindex);
  if (setsize > 1) {
    for (int setindex = 1; setindex <= setsize; setindex++) { 
      sum += getRealInput(Xindex, setindex);
    }
    avg = sum/setsize;
  } else {
    // the single input must be an array
    vector<double> vdbl = getRealArrayInput(Xindex);
    vector<double>::iterator itr = vdbl.begin();
    int count=0;
    while(itr != vdbl.end()) {
      sum += *itr++;
      count++;
    }
    avg = sum /count;
  }

  setRealOutput(meanindex, avg);
}

/*******************************************************************************
* Statistics Filter.
*******************************************************************************/
// Input arguments.
const filterIn StatisticsFilter::input [] =  
  { filterIn("X", BasicType::Real, true), filterIn() };

// Output arguments
const filterOut StatisticsFilter::output[] = 
  { 
    filterOut("mean", BasicType::Real), 
    filterOut("variance", BasicType::Real), 
    filterOut("max", BasicType::Real), 
    filterOut("min", BasicType::Real), 
    filterOut()
  };

StatisticsFilter::StatisticsFilter (RecordStream * stream) 
{ 
  initialize(input, output, stream);  
  Xindex    = getInputIndex("X");
  meanindex = getDefaultOutputIndex("mean"); 
  varindex = getDefaultOutputIndex("variance"); 
  maxindex = getDefaultOutputIndex("max"); 
  minindex = getDefaultOutputIndex("min"); 
}

void StatisticsFilter::compute()
{
  double x, avg=0.0, variance=0.0, max=0.0, min=0.0, sum=0.0;
  int count=0, maxi, mini;
  int setsize = getInputCardinality(Xindex);
  if (setsize > 1) {
    for (int setindex = 1; setindex <= setsize; setindex++) { 
      x = getRealInput(Xindex, setindex);
      sum += x;
      if (setindex==1) {
	max = x;
	maxi = 1;
	min = x;
	mini = 1;
      } else {
	if ( x > max) {
	  max = x;
	  maxi = setindex;
	}
	if ( x < min) {
	  min = x;
	  mini = setindex;
	}
      }
    }
    avg = sum/setsize;
    for (int setindex = 1; setindex <= setsize; setindex++) { 
      variance += pow((getRealInput(Xindex, setindex) - avg),2);
    }
    variance = variance /count;
  } else {
    // the single input must be an array
    bool first=true;
    vector<double> vdbl = getRealArrayInput(Xindex);
    vector<double>::iterator itr = vdbl.begin();
    count=0;
    while(itr != vdbl.end()) {
      x = *itr++;
      sum += x;
      if (first) {
	min = x;
	mini = 1;
	max = x;
	maxi = 1;
	first = false;
      } else {
	if ( x > max) {
	  max = x;
	  maxi = count;
	}
	if ( x < min) {
	  min = x;
	  mini = count;
	}
      }
      count++;
    }
    avg = sum /count;
    
    itr = vdbl.begin();
    while(itr != vdbl.end()) {
      variance += pow((*itr++ - avg),2);
    }
    variance = variance /count;
  }

  setRealOutput(meanindex, avg);
  setRealOutput(varindex, variance);
  setRealOutput(maxindex, max);
  setRealOutput(minindex, min);
}

/*******************************************************************************
* Quantile Filter.
*******************************************************************************/
// Input arguments.
const filterIn QuantileFilter::input [] =  
  { filterIn("X", BasicType::Real, true), filterIn() };

// Output arguments
const filterOut QuantileFilter::output[] = 
  { 
    filterOut("P(0.05)", BasicType::Real), 
    filterOut("P(0.10)", BasicType::Real), 
    filterOut("P(0.25)", BasicType::Real), 
    filterOut("P(0.50)", BasicType::Real), 
    filterOut("P(0.75)", BasicType::Real), 
    filterOut("P(0.90)", BasicType::Real), 
    filterOut("P(0.95)", BasicType::Real), 
    filterOut()
  };

QuantileFilter::QuantileFilter (RecordStream * stream) 
{ 
  initialize(input, output, stream);  
  Xindex    = getInputIndex("X");
  Pindex    = vector<int>(7);
  Pvalue    = vector<double>(7);

  Pindex[0] = getDefaultOutputIndex("P(0.05)"); 
  Pindex[1] = getDefaultOutputIndex("P(0.10)"); 
  Pindex[2] = getDefaultOutputIndex("P(0.25)"); 
  Pindex[3] = getDefaultOutputIndex("P(0.50)"); 
  Pindex[4] = getDefaultOutputIndex("P(0.75)"); 
  Pindex[5] = getDefaultOutputIndex("P(0.90)"); 
  Pindex[6] = getDefaultOutputIndex("P(0.95)"); 

  Pvalue[0] = 0.05;
  Pvalue[1] = 0.10;
  Pvalue[2] = 0.25;
  Pvalue[3] = 0.50;
  Pvalue[4] = 0.75;
  Pvalue[5] = 0.90;
  Pvalue[6] = 0.95;
}

void QuantileFilter::compute()
{
  vector<double> values;
  int setsize = getInputCardinality(Xindex);

  if (setsize > 1) {

    for (int setindex = 1; setindex <= setsize; setindex++)
      values.push_back(getRealInput(Xindex, setindex));

  } else {
				// the single input must be an array
    vector<double> vdbl = getRealArrayInput(Xindex);
    vector<double>::iterator itr = vdbl.begin();
    while(itr != vdbl.end())
      values.push_back(*itr++);

  }

				// Sort the data array

  sort(values.begin(), values.end());

				// Assign the values
  int indx;
  for (int i=0; i<7; i++) {
    indx = (int) rint(Pvalue[i]*values.size());
    indx = min<int>(max<int>(indx, 0), values.size()-1);
    setRealOutput(Pindex[i], values[indx]);
  }

}

/*******************************************************************************
* Max Filter.
*******************************************************************************/
// Input arguments.
const filterIn MaxFilter::input [] =  
  { filterIn("X", BasicType::Real, true), filterIn() };

// Output arguments
const filterOut MaxFilter::output[] = 
  { 
    filterOut("max", BasicType::Real), 
    filterOut("maxindex", BasicType::Int), 
    filterOut()
  };

MaxFilter::MaxFilter (RecordStream * stream) 
{ 
  initialize(input, output, stream);  
  x_index    = getInputIndex("X");
  maxindex = getDefaultOutputIndex("max"); 
  maxindex_index = getDefaultOutputIndex("maxindex"); 
}

void MaxFilter::compute()
{
  double max=0.0, x;
  int count=0, maxi=1;

  int setsize = getInputCardinality(x_index);
  if (setsize > 1) {
    
    for (int setindex = 1; setindex <= setsize; setindex++) { 
      x = getRealInput(x_index, setindex);
      if (setindex==1) {
	max = x;
	maxi = 1;
      } else {
	if ( x > max) {
	  max = x;
	  maxi = setindex;
	}
      }
    }
  } else {
    // the single input must be an array
    bool first = true;
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double>::iterator itr = vdbl.begin();
    count=0;

    while(itr != vdbl.end()) {
      x = *itr++;
      if (first) {
	max = x;
	maxi = 1;
	first = false;
      } else {
	if ( x > max) {
	  max = x;
	  maxi = count;
	}
      }

      count++;
    }
  }

  setRealOutput(maxindex, max);
  setIntOutput(maxindex_index, maxi);
}

/*******************************************************************************
* Min Filter.
*******************************************************************************/
// Input arguments.
const filterIn MinFilter::input [] =  
  { filterIn("X", BasicType::Real, true), filterIn() };

// Output arguments
const filterOut MinFilter::output[] = 
  { 
    filterOut("min", BasicType::Real), 
    filterOut("minindex", BasicType::Int), 
    filterOut()
  };

MinFilter::MinFilter (RecordStream * stream) 
{ 
  initialize(input, output, stream);  
  x_index    = getInputIndex("X");
  minindex = getDefaultOutputIndex("min"); 
  minindex_index = getDefaultOutputIndex("minindex"); 
}

void MinFilter::compute()
{
  double min=0.0, x;
  int count=0, mini=1;

  int    setsize = getInputCardinality(x_index);
  if (setsize > 1) {
    
    for (int setindex = 1; setindex <= setsize; setindex++) { 
      x = getRealInput(x_index, setindex);
      if (setindex==1) {
	min = x;
	mini = 1;
      } else {
	if ( x < min) {
	  min = x;
	  mini = setindex;
	}
      }
    }
  } else {
    // the single input must be an array
    bool first = true;
    vector<double> vdbl = getRealArrayInput(x_index);
    vector<double>::iterator itr = vdbl.begin();
    count=0;
    while(itr != vdbl.end()) {
      x = *itr++;
      if (first) {
	min = x;
	mini = 1;
	first = false;
      } else {
	if ( x < min) {
	  min = x;
	  mini = count;
	}
      }
      count++;
    }
  }

  setRealOutput(minindex, min);
  setIntOutput(minindex_index, mini);
}

/*******************************************************************************
* Sanity Filter.
*******************************************************************************/
// Input arguments.
const filterIn PSanityFilter::input [] =  
  { 
    filterIn("data",   BasicType::Real, true), 
    filterIn("theory", BasicType::Real, true), 
    filterIn()
  };

// Output arguments
const filterOut PSanityFilter::output[] = 
  { 
    filterOut("insane", BasicType::Int), 
    filterOut("maxinsane", BasicType::Int), 
    filterOut()
  };

PSanityFilter::PSanityFilter (RecordStream * stream) 
{ 
  initialize(input, output, stream);  
  data_index      = getInputIndex("data");
  theory_index    = getInputIndex("theory");
  insane_index    = getDefaultOutputIndex("insane"); 
  maxinsane_index = getDefaultOutputIndex("maxinsane"); 
}

void PSanityFilter::compute()
{
  double max=0.0, x, y;
  int count=0, maxi=0, insane=0;

  int setsize = getInputCardinality(data_index);

  if (setsize > 1) {
    
    for (int setindex = 1; setindex <= setsize; setindex++) { 
      x = getRealInput(data_index, setindex);
      y = getRealInput(theory_index, setindex);
      if (x>0.0 && y==0.0) {
	if (insane==0) {
	  max = x;
	  maxi = setindex;
	} else {
	  if (x>max) {
	    max = x;
	    maxi = setindex;
	  }
	}
	insane++;
      }
    }
  } else {
    // the single input must be an array
    vector<double> xdbl = getRealArrayInput(data_index);
    vector<double> ydbl = getRealArrayInput(theory_index);
    vector<double>::iterator xitr = xdbl.begin();
    vector<double>::iterator yitr = ydbl.begin();
    count=0;

    while(xitr != xdbl.end()) {
      x = *xitr++;
      y = *yitr++;

      if (x>0.0 && y==0.0) {
	if (insane==0) {
	  max = x;
	  maxi = count;
	} else {
	  if (x>max) {
	    max = x;
	    maxi = count;
	  }
	}
	insane++;
      }

      count++;
    }
  }
  
  setIntOutput(insane_index, insane);
  setIntOutput(maxinsane_index, maxi);
}

