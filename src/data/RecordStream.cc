#include <RecordStream.h>
#include <BasicType.h>
#include <TypedBuffer.h>
#include <RecordBuffer.h>

using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordStream)

RecordStream::~RecordStream() 
{  if (rs_buffer != 0) {delete rs_buffer; } }

bool RecordStream::hasValue()
  { return rs_buffer->hasValue(); }
  
TypedBuffer * RecordStream::getFieldBuffer(int fieldindex) 
  { return rs_buffer->getFieldBuffer(fieldindex); }
  
TypedBuffer * RecordStream::getFieldBuffer(string fieldname) 
  { return rs_buffer->getFieldBuffer(fieldname); }

RecordBuffer * RecordStream::getBuffer() {return rs_buffer;}

int RecordStream::getFieldIndex(string fieldname) 
  { return rs_buffer->getFieldIndex(fieldname); }
  
string RecordStream::getFieldName(int fieldindex) 
  { return rs_buffer->getFieldName(fieldindex); }
  
bool RecordStream::isValidFieldIndex(int fieldindex)
  { return rs_buffer->isValidFieldIndex(fieldindex); }
  
bool RecordStream::isValidFieldName(string fieldname)
  { return rs_buffer->isValidFieldName(fieldname); }

BasicType * RecordStream::getFieldType(int fieldindex) 
  { return rs_buffer->getFieldType(fieldindex); }
    
BasicType * RecordStream::getFieldType(string fieldname) 
  { return rs_buffer->getFieldType(fieldname); }
 
bool RecordStream::isFieldArrayType(int fieldindex) 
  { return rs_buffer->getFieldType(fieldindex)->isArrayType(); }
    
int RecordStream::numFields()
  { return rs_buffer->numFields(); }

RecordType * RecordStream::getType() 
  { return rs_buffer->getType(); }
