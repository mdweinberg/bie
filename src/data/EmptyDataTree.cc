#include <EmptyDataTree.h>
#include <Tile.h>
#include <Node.h>
#include <Frontier.h>
#include <OneBin.h>
#include <UniversalTessellation.h>

using namespace BIE;

BIE_CLASS_EXPORT_IMPLEMENT(BIE::EmptyDataTree)

EmptyDataTree::EmptyDataTree() : BaseDataTree()
{
  this->fd_distributionfactory = new NullDistribution();
  this->fd_tess                = new UniversalTessellation();
  fd_total                     = 0;
  fd_offgrid                   = 0;

  // Allocate a clone of the binned distribution to each tile in tessellation.
  for (int tileid = fd_tess->MinID(); tileid <= fd_tess->MaxID(); tileid++)
  { fd_tiledistributions[tileid] = fd_distributionfactory->New(); }  

  // Create a frontier.
  fd_defaultfrontier = new Frontier(fd_tess);
}

