#include <SpecialFilters.h>
#include <cmath>
#include <BasicType.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RPhiFilter)

using namespace BIE;

/*******************************************************************************
* R-Phi
*******************************************************************************/
const filterIn RPhiFilter::input [] = 
  { filterIn("x", BasicType::Real, false), 
    filterIn("y", BasicType::Real, false), 
    filterIn() };

const filterOut RPhiFilter::output[] = 
  { filterOut("r",   BasicType::Real), 
    filterOut("phi", BasicType::Real), 
    filterOut() };

RPhiFilter::RPhiFilter(RecordStream * stream)
{ 
  initialize(input, output, stream); 
  
  // Look up indices now to avoid a name look up with every computation.
  x_index = getInputIndex("x");
  y_index = getInputIndex("y");
  r_index = getDefaultOutputIndex("r");
  p_index = getDefaultOutputIndex("phi");
}

void RPhiFilter::compute()
{
  double x,y;
  if (!isArray(x_index)) {
    x = getRealInput(x_index);
    y = getRealInput(y_index);
    setRealOutput(r_index, sqrt(x*x+y*y));
    setRealOutput(p_index, atan2(y, x));
  } else {
    vector<double> xdbl = getRealArrayInput(x_index);
    vector<double> ydbl = getRealArrayInput(y_index);
    vector<double> rdbl = vector<double>();
    vector<double> pdbl = vector<double>();
    vector<double>::iterator xitr = xdbl.begin();
    vector<double>::iterator yitr = ydbl.begin();
    while(xitr != xdbl.end()) {
      x = *xitr++; y = *yitr++;
      rdbl.push_back(sqrt(x*x+y*y));
      pdbl.push_back(atan2(y, x));
    }
    setRealArrayOutput(r_index, rdbl);
    setRealArrayOutput(p_index, pdbl);
  }
}
