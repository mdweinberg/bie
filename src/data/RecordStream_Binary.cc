#include <RecordStream_Binary.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <iostream>
using namespace std;

#include <inttypes.h>
#include <RecordBuffer.h>
#include <BasicType.h>
#include <ArrayType.h>
#include <RecordType.h>
#include <BIEException.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordInputStream_Binary)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordOutputStream_Binary)

using namespace BIE;

enum {BINARYRS_INTTYPE,BINARYRS_REALTYPE,BINARYRS_BOOLTYPE,BINARYRS_STRINGTYPE,
      BINARYRS_INTARRAYTYPE,BINARYRS_REALARRAYTYPE,BINARYRS_BOOLARRAYTYPE};

/*******************************************************************************
* Macro to convert 64 bit doubles from host to network order.
*******************************************************************************/
#if defined(_BIG_ENDIAN)
  /* big-endian */
  #define ntohd(x) (x)
  #define htond(x) (x)
#else
  /* little endian */
  double ntohd(double x)
  {
    unsigned long int * xarray       = (unsigned long int*) &x;
    double value;
    unsigned long int * valuearray   = (unsigned long int*) &value;
    valuearray[0] = ntohl(xarray[1]);
    valuearray[1] = ntohl(xarray[0]);
    return value;
  }

  double htond(double x)
  {
    unsigned long int * xarray     = (unsigned long int*) &x;
    double value;
    unsigned long int * valuearray = (unsigned long int*) &value;
    valuearray[0] = htonl(xarray[1]);
    valuearray[1] = htonl(xarray[0]);
    return value;
  }
#endif

/*******************************************************************************
* Binary record input stream.
*******************************************************************************/
RecordInputStream_Binary::RecordInputStream_Binary 
(RecordType * type, string filename)
{
  // Common initialization must be first
  initialize();

  // Open the file.
  risb_binarystream = new ifstream(filename.c_str(), ios::in | ios::binary);
  
  // It is our responsibility to close the file.
  risb_streamismine = true;
  
  // Check that it was opened successfully.
  if (! risb_binarystream->good())
  { throw FileOpenException(filename, errno, __FILE__, __LINE__); }

  // Create new buffer with the specified type.
  rs_buffer = new RecordBuffer(type);
}
  
RecordInputStream_Binary::RecordInputStream_Binary
(RecordType * type, istream * binarystream)
{
  // Common initialization must be first
  initialize();

  // Copy the reference to the input stream
  risb_binarystream = binarystream;

  // Create new buffer with the specified type.
  rs_buffer = new RecordBuffer(type);
}

RecordInputStream_Binary::RecordInputStream_Binary
(string filename)
{ 
  // Common initialization must be first
  initialize();

  // Open the file.
  risb_binarystream = new ifstream(filename.c_str(), ios::in | ios::binary);
  
  // It is our responsibility to close the file.
  risb_streamismine = true;
  
  // Check that it was opened successfully.
  if (! risb_binarystream->good())
  { throw FileOpenException(filename, errno, __FILE__, __LINE__); }

  // Extract the type from the top of the file
  RecordType * type = readMetaData();
    
  // Create new buffer with the specified type.
  rs_buffer = new RecordBuffer(type);
  
  // We don't need the type any more.
  delete type;
}
  
RecordInputStream_Binary::RecordInputStream_Binary
(istream * asciistream)
{ 
  // Common initialization must be first
  initialize();

  risb_binarystream = asciistream;

  // Extract the type from the top of the file
  RecordType * type = readMetaData();
    
  // Create new buffer with the specified type.
  rs_buffer = new RecordBuffer(type);

  // We don't need the type any more.
  delete type;
}

RecordInputStream_Binary::~RecordInputStream_Binary()
{
  if (risb_streamismine) { delete risb_binarystream; }
}

void RecordInputStream_Binary::initialize()
{  
  rs_buffer = 0;

  risb_eof   = false;
  risb_error = false;

  risb_stringbuffersize = 0;
  risb_stringbuffer = 0;
  
  risb_streamismine = false;

  // This stream is a root stream.
  this->setAsRoot();
}

bool RecordInputStream_Binary::eos() {return risb_eof; }
  
bool RecordInputStream_Binary::error() {return risb_error; }

void RecordInputStream_Binary::setArrayFieldValue
(int fieldindex, BasicType *type, int length)
{
  // create a vector of appropriate type
  int i;

  if (type->equals(BasicType::Int)) { 
    vector<int> vint = vector<int>();
    for(i=0; i<length; i++) {
      long integer = readInt(); 
      vint.push_back(integer);
    } 
    rs_buffer->setIntArrayValue(fieldindex, vint);
    
  } else if (type->equals(BasicType::Real)) {
    vector<double> vdbl = vector<double>();
    for(i=0; i<length; i++) {
      double real = readReal();
      vdbl.push_back(real);
    }    
    rs_buffer->setRealArrayValue(fieldindex, vdbl);
  } else if (type->equals(BasicType::Bool)) {
    vector<bool> vbln = vector<bool>();
    for(i=0; i<length; i++) {
      vbln.push_back(readBool());
    }
    rs_buffer->setBoolArrayValue(fieldindex, vbln);
  } else if (type->equals(BasicType::String)) {
    throw FileFormatException("Array of String is not supported", __FILE__, __LINE__);
  } else {
    throw FileFormatException("unsupported type", __FILE__, __LINE__);
  }
}

bool RecordInputStream_Binary::nextRecord() 
{
  bool result = true;

  if (risb_eof || risb_error)
  {
    result = false;
  }
  else
  {
    try 
    {
      for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
      {
        BasicType * type = rs_buffer->getFieldType(fieldindex);
  
	// is this array field?
	if ( type->isArrayType() ) {
	  ArrayType *at = static_cast<ArrayType *>(type);
	  int length = at->getLength();
	  BasicType *bat = at->getBaseType();
	  // have the first element's lexcode in hand
	  setArrayFieldValue(fieldindex, bat, length);
	} else {
	  if (type->equals(BasicType::Int)) {  
	    rs_buffer->setIntValue(fieldindex, readInt());
	  }
	  else if (type->equals(BasicType::Real)) {  
	    rs_buffer->setRealValue(fieldindex, readReal());
	  }
	  else if (type->equals(BasicType::Bool))  {  
	    rs_buffer->setBoolValue(fieldindex, readBool());
	  }
	  else if (type->equals(BasicType::String)) {  
	    rs_buffer->setStringValue(fieldindex, readString()); 
	  }
	  else 
	    { throw InternalError(__FILE__,__LINE__);  } 
	}
        // Check we successfully got our value.
        if (risb_eof && fieldindex == 1) {
          result = false;
          break;
        }
        else if (risb_eof || risb_error) {
          throw FileFormatException("Unexpected EOF or error",__FILE__, __LINE__);
        }
      }
    }
    catch (...)
    {
      risb_error = true;
      result     = false;
      
      // Reset the buffer to forget all the previous values.
      rs_buffer->reset();

      throw;  // Rethrows the original exception.
    }
  }
    

  if (! result)
  {
    // Reset the buffer to forget all the previous values.
    rs_buffer->reset();
  }
  
  return result;
}

RecordType * RecordInputStream_Binary::readMetaData()
{
  RecordType * result = new RecordType(); 
  int len=0;

  int numfields = readInt();
  if (risb_eof || risb_error) 
  { throw FileFormatException("Unexpected EOF or error",__FILE__, __LINE__); }
  
  for (int fieldindex = 1; fieldindex <= numfields; fieldindex++)
  {
    // Read a type descriptor and a field name.
    int          typedescriptor = readInt();
    const char * fieldname      = readString();
    
    if (risb_eof || risb_error) 
    { throw FileFormatException("Unexpected EOF or error",__FILE__, __LINE__); }
    
    // Determine which basic type the field has from the integer code.
    BasicType * fieldtype;
    switch (typedescriptor)
    {
      case BINARYRS_INTTYPE:    {fieldtype = BasicType::Int; break;}
      case BINARYRS_REALTYPE:   {fieldtype = BasicType::Real; break;}
      case BINARYRS_BOOLTYPE:   {fieldtype = BasicType::Bool; break;}
      case BINARYRS_STRINGTYPE: {fieldtype = BasicType::String; break;}
      case BINARYRS_INTARRAYTYPE: {
	len = readInt();
	fieldtype = new ArrayType(BasicType::Int,len); 
	break;
      }
      case BINARYRS_REALARRAYTYPE: {
	len = readInt();
	fieldtype = new ArrayType(BasicType::Real,len); 
	break;
      }
      case BINARYRS_BOOLARRAYTYPE: {
	len = readInt();
	fieldtype = new ArrayType(BasicType::Bool,len); 
	break;
      }
      default: 
        throw FileFormatException("Unexpected type descriptor",__FILE__, __LINE__);
    }
    
    // Add the field to the record type.
    RecordType *newtype = result->insertField(fieldindex, fieldname, fieldtype);
    delete result;
    result = newtype;
  }
  
  return result;
}

void RecordInputStream_Binary::readStream(unsigned char * buffer, int bytes)
{
  if ( !(risb_eof || risb_error) )
  {
    // Only read if we haven't had an error already.
    //unsigned char* ->char* should be ok
    if(! risb_binarystream->read((char*)buffer, bytes))
    {
      if (risb_binarystream->eof()) { 
        risb_eof   = true; 
        risb_error = false;     }
      else { 
        risb_error = true; 
        risb_eof   = false;
      }
    }
  }
}

int RecordInputStream_Binary::readInt()
{
  int value = 0;
  readStream((unsigned char*) &value, 4);
 
  value = ntohl(value);
  
  return value;
}

double RecordInputStream_Binary::readReal()
{
  double value = 0;
  readStream((unsigned char*) &value, 8);

  value = ntohd(value);

  return value;
}

bool RecordInputStream_Binary::readBool()
{
  bool value = false;
  readStream((unsigned char*) &value, 1);  

  return value;
}

const char * RecordInputStream_Binary::readString()
{
  int stringsize = readInt();
  
  if (risb_eof || risb_error)
  {
    // We reached end of file or there was an error while reading the integer.
    risb_stringbuffer[0] = '\0';
  }
  else
  {
    // If our existing buffer is not big enough, then make it big enough times 2
    if(stringsize + 1 > risb_stringbuffersize)
    {
      delete risb_stringbuffer;
      risb_stringbuffersize = (stringsize+1)*2;
      risb_stringbuffer = new char[risb_stringbuffersize];
    }
    
    readStream((unsigned char*) risb_stringbuffer, stringsize);
    risb_stringbuffer[stringsize] = '\0';
  }
  
  return risb_stringbuffer;
}
  
/*******************************************************************************
* Binary record output stream.
*******************************************************************************/
RecordOutputStream_Binary::RecordOutputStream_Binary 
(RecordOutputStream * stream, string filename, bool writemetadata)
{
  // Open the file.
  rosb_outputstream = new ofstream(filename.c_str(), ios::out | ios::binary);

  // Check that it was opened successfully.
  if (! rosb_outputstream->good()) 
  { throw FileCreateException (filename, errno, __FILE__, __LINE__); }

  rosb_streamismine = true;

  initialize(stream, writemetadata);
}
    
RecordOutputStream_Binary::RecordOutputStream_Binary
(RecordOutputStream * stream, ostream * binarystream, bool writemetadata)
{
  rosb_outputstream = binarystream;
  rosb_streamismine = false;
  
  initialize(stream, writemetadata);
}

void RecordOutputStream_Binary::initialize
(RecordOutputStream * stream, bool writemetadata)
{
  stream->registerForUpdates(this);
  
  // We don't really need our own buffer (its unchanged) -- reference a copy.
  rs_buffer = stream->getBuffer();

  ros_isroot      = false;
  ros_inheritable = false;  // Says that this stream cannot be inherited.
  ros_closed      = false;

  addNewStream(this);
  
  if (writemetadata) { writeMetaData(); }
}  

RecordOutputStream_Binary::~RecordOutputStream_Binary() 
{
  if (rosb_streamismine)
    { delete rosb_outputstream; }
    
  removeOldStream(this);
  
  // We shared our buffer, so avoid deletion by the base class.
  rs_buffer = 0;
}
  
void RecordOutputStream_Binary::pushRecord() 
{
  // Complain if the stream has already been closed.
  if (ros_closed)
  { throw EndofStreamException(__FILE__, __LINE__); }
  
  // Write out each field.  If a field does not have a value then
  // an exception will be thrown.
  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    BasicType * type = getFieldType(fieldindex);
    
    if (type->isArrayType()) {
      ArrayType *at = static_cast<ArrayType *>(type);
      BasicType *bat = at->getBaseType();

      if (bat->equals(BasicType::Int)) {
	vector<int> vint = rs_buffer->getIntArrayValue(fieldindex);
	vector<int>::iterator itr = vint.begin();
	while(itr != vint.end()) {
	  writeInt(*itr++);
	}
      } else if (bat->equals(BasicType::Real)) {
	vector<double> vdbl = rs_buffer->getRealArrayValue(fieldindex);
	vector<double>::iterator itr = vdbl.begin();
	while(itr != vdbl.end()) {
	  writeReal(*itr++);
	}
      } else if(bat->equals(BasicType::Bool)) {
	vector<bool> vbln = rs_buffer->getBoolArrayValue(fieldindex);
	vector<bool>::iterator itr = vbln.begin();
	while(itr != vbln.end()) {
	  writeBool(*itr++);
	}
      } else {
	// array of strings not supported
	abort();
      }
    } else {
      // Write out the value of the field.
      if (type->equals(BasicType::Int)) { 
	writeInt(rs_buffer->getIntValue(fieldindex));
      }
      else if (type->equals(BasicType::Real)) {
	writeReal(rs_buffer->getRealValue(fieldindex));
      }
      else if (type->equals(BasicType::Bool)) {
	writeBool(rs_buffer->getBoolValue(fieldindex));
      }
      else if (type->equals(BasicType::String)) { 
	writeString(rs_buffer->getStringValue(fieldindex));
      }
      else
	{ abort(); }
    }
  }
}
  
void RecordOutputStream_Binary::writeMetaData()
{
  writeInt(numFields());

  for (int fieldindex = 1; fieldindex <= numFields(); fieldindex++)
  {
    BasicType * fieldtype = getFieldType(fieldindex);
  
    if (!fieldtype->isArrayType()) {
      if      (fieldtype->equals(BasicType::Int))   
	{ writeInt(BINARYRS_INTTYPE); }
      else if (fieldtype->equals(BasicType::Real))  
	{ writeInt(BINARYRS_REALTYPE);}
      else if (fieldtype->equals(BasicType::Bool))  
	{ writeInt(BINARYRS_BOOLTYPE);}
      else if (fieldtype->equals(BasicType::String))
	{ writeInt(BINARYRS_STRINGTYPE);}
      else 
	{abort(); }
    } else {
      ArrayType *at = static_cast<ArrayType*>(fieldtype);
      BasicType *bat = at->getBaseType();
      int len = at->getLength();
      if      (bat->equals(BasicType::Int))   
	{ writeInt(BINARYRS_INTARRAYTYPE); }
      else if (bat->equals(BasicType::Real))  
	{ writeInt(BINARYRS_REALARRAYTYPE);}
      else if (bat->equals(BasicType::Bool))  
	{ writeInt(BINARYRS_BOOLARRAYTYPE);}
      else 
	{abort(); }

      writeInt(len);
    }
    writeString(getFieldName(fieldindex));
  }
}

void RecordOutputStream_Binary::writeInt(int value)
{
  value = htonl(value);
  if (! rosb_outputstream->write((char*) &value, sizeof(int)))
  { abort(); }
  
}

void RecordOutputStream_Binary::writeReal(double value)
{
  value = htond(value);
  
  if (! rosb_outputstream->write((char*) &value, sizeof(double)))
  { abort(); }
}

void RecordOutputStream_Binary::writeBool(bool value)
{
  char boolvalue = (char) value;
  if (! rosb_outputstream->write((char*) &boolvalue, 1))
  { abort(); }
}

void RecordOutputStream_Binary::writeString(string value)
{
  writeInt(value.size());
  if (! rosb_outputstream->write((char*)value.c_str(), value.size()))  
  { abort(); }
}
