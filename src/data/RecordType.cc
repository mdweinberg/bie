#include <AsciiLexer.h>
#include <RecordType.h>
#include <BasicType.h>
#include <ArrayType.h>
#include <BIEException.h>
#include <cstdlib>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::RecordType)

using namespace BIE;

/*******************************************************************************
* Object lifetime - constructors and destructors.
*******************************************************************************/
RecordType::RecordType 
(RecordType * rt)
{  
  // Makes copies of the name and type vectors.
  this->record_fieldnames = rt->record_fieldnames;
  this->record_fieldtypes = rt->record_fieldtypes;
}

RecordType::RecordType 
(const fieldstruct * typetable)
{
  int fieldindex = 0;

  // Loop through the array until we reach a null record.
  while ((typetable[fieldindex].fieldname != 0) &&
         (typetable[fieldindex].fieldtype != 0))
  {
    // Check for a name clash with an earlier entry.
    if (isValidFieldName(typetable[fieldindex].fieldname))
    { throw DuplicateFieldException(typetable[fieldindex].fieldname, __FILE__, __LINE__); }
    
    // Append the name and the type
    record_fieldnames.insert ((record_fieldnames.begin() + fieldindex), 
                               typetable[fieldindex].fieldname);
    record_fieldtypes.insert ((record_fieldtypes.begin() + fieldindex), 
                              (BasicType *) typetable[fieldindex].fieldtype);

    fieldindex++;
  }
}

// get the length
int RecordType::getLength(AsciiLexer *lxr)
{
  int lexcode = lxr->nextToken();
  if (lexcode != AsciiLexer::AL_ATRATE_) {
    throw FileFormatException("Unknown field type", __FILE__, __LINE__);
  }
  lexcode = lxr->nextToken();
  if (lexcode != AsciiLexer::AL_INT_VALUE_) {
    throw FileFormatException("Unknown field type", __FILE__, __LINE__);
  }
  int length = atoi(lxr->tokenText());
  return length;
}

RecordType::RecordType 
(istream *istrm)
{
  int fieldindex = 1;
  AsciiLexer *lexer = new AsciiLexer(istrm);
  bool finished = false;
  
  try
  {
    while (! finished)
    {
      int lexcode = lexer->nextToken();
      cout << "readMetaData Ascii Lexer " << lexcode << "\n";
      BasicType * fieldtype = 0;
      bool isArray=false;

      if (lexcode == AsciiLexer::AL_LEFT_SQUAREBRACKET_) {
	lexcode = lexer->nextToken();
	isArray = true;
      }
  
      switch (lexcode)
      {
        case AsciiLexer::AL_NEWLINE_: 
          // Finished reading type header.
          finished = true;
          break;
        case AsciiLexer::AL_INT_KEYWORD_: 
	  if (!isArray) {
	    fieldtype = BasicType::Int;
	  } else {
	    int length = getLength(lexer);
	    fieldtype = new ArrayType(BasicType::Int, length);
	  }
          break;
        case AsciiLexer::AL_REAL_KEYWORD_:
	  if (!isArray) {
	    fieldtype = BasicType::Real;
	  } else {
	    int length = getLength(lexer);
	    fieldtype = new ArrayType(BasicType::Real, length);
      	  }
          break;
        case AsciiLexer::AL_BOOL_KEYWORD_:
	  if (!isArray) {
	    fieldtype = BasicType::Bool;
	  } else {
	    int length = getLength(lexer);
	    fieldtype = new ArrayType(BasicType::Bool, length);
	  }
          break;
        case AsciiLexer::AL_STRING_KEYWORD_:
	  if (!isArray) {
	    fieldtype = BasicType::String;
	  } else {
	    throw FileFormatException("Unknown field type: Array of string not implemented", 
				      __FILE__, __LINE__);
	  }
          break;
        default:
	  cout << "AsciiLexer Exception\n";
          throw FileFormatException("Unknown field type (should be int, real, bool or string", 
	                            __FILE__, __LINE__);
      }
      
	  cout << "AsciiLexer Exception did not throw correctly \n";
      if (!finished)
      {
        lexcode = lexer->nextToken();
      cout << "readMetaDatad Ascii Lexer 2 " << lexcode << "\n";
      
        if (lexcode != AsciiLexer::AL_STRING_VALUE_)
        { throw FileFormatException("Expected string value", __FILE__, __LINE__); }
      
        // Add this field to the record type
        this->insertField(fieldindex, lexer->tokenText(), fieldtype);
  
        // We created a new type object and do not need the old one.
        ///delete result;
        ///result = newtype;
      
        // Read a new line character.
        lexcode = lexer->nextToken();    
        if (lexcode != AsciiLexer::AL_NEWLINE_)
        { throw FileFormatException("Expected newline", __FILE__, __LINE__); }
      
        fieldindex++;
      }
    }
    
    // Check that there was at least one field.
    if (fieldindex == 1)
    { throw FileFormatException("Must be at least one field", __FILE__, __LINE__); }  
  }
  catch (...)
  {
    throw;  // Rethrows the original exception.
  }
    
}
/*******************************************************************************
* Operations on record types.
*******************************************************************************/
RecordType * RecordType::deleteField 
(string fieldname)
{
  // look up the index of the name and delegate to deleteField by index.
  int fieldindex       = getFieldIndex(fieldname);  
  RecordType * result  = deleteField(fieldindex);
  return result;
} 

RecordType * RecordType::deleteField 
(int fieldindex) 
{
  // Delegate to the delete range method.
  RecordType * result = deleteRange(fieldindex, fieldindex);
  return result;
}

RecordType * RecordType::deleteRange 
(int startindex, int endindex)
{
  RecordType * result  = new RecordType(this);
  
  // Check the field indices are valid.
  if (! isValidFieldIndex(startindex)) 
  { throw NoSuchFieldException(startindex,__FILE__, __LINE__); }

  if (! isValidFieldIndex(endindex)) 
  { throw NoSuchFieldException(endindex,__FILE__, __LINE__); }
  
  // Check the start index is less or equal to the end field.
  if (startindex > endindex)
  { throw BadRangeException(startindex, endindex, __FILE__, __LINE__); }  

  // Vectors start their index at zero, so decrement both indices.
  startindex--;
  endindex--;
  
  // Delete each field in the specified range.  Indices are funky because
  // the vectors rearrange themselves after each erase call. 
  int deleteindex = startindex;
  for (; startindex <= endindex; startindex++)
  {    
    (result->record_fieldnames).erase
         ((result->record_fieldnames).begin() + deleteindex);  
    (result->record_fieldtypes).erase
         ((result->record_fieldtypes).begin() + deleteindex);
  }
  
  return result;
}   

RecordType * RecordType::insertField
(int insertpos, string insertname, BasicType * inserttype)
{
  RecordType * result  = new RecordType(this);
  
  // Check the position argument is in the range 1 .. n+1.
  if (! (isValidFieldIndex(insertpos) || (insertpos == numFields() + 1)))
  { throw InsertPositionException(insertpos, __FILE__, __LINE__); }

  // If the name is used in existing type then throw an exception.
  if (isValidFieldName(insertname))
  { throw NameClashException(insertname, __FILE__, __LINE__); }
    
  // Vectors start indexing from 0, not 1.
  insertpos--;
  
  // Insert the name and type into the new record type.
  (result->record_fieldnames).insert
    ((result->record_fieldnames.begin() + insertpos), insertname);
  (result->record_fieldtypes).insert
    ((result->record_fieldtypes.begin() + insertpos), inserttype);

  return result;
}  

RecordType * RecordType::insertRecord
(int insertpos, RecordType * insertrec)
{
  RecordType * result  = new RecordType(this);
  
  // Check the position argument is in the range 1 .. n+1.
  if (! (isValidFieldIndex(insertpos) || (insertpos == numFields() + 1)))
  { throw InsertPositionException(insertpos, __FILE__, __LINE__); }

  for (int fieldindex = 1; fieldindex <= insertrec->numFields(); fieldindex++)
  {
    string      insertname = insertrec->getFieldName(fieldindex);
    BasicType * inserttype = insertrec->getFieldType(fieldindex);
    
    // If the name is used in the existing type then throw an exception.
    if (isValidFieldName(insertname))
    { throw NameClashException(insertname, __FILE__, __LINE__); }
    
    // The offset has 2 subtracted because vector indices start at zero.
    int offset = fieldindex + insertpos - 2;
    
    // Insert the name and type of the current field into the new record type.
    (result->record_fieldnames).insert
      ((result->record_fieldnames.begin() + offset), insertname);
    (result->record_fieldtypes).insert
      ((result->record_fieldtypes.begin() + offset), inserttype);
  }
  
  return result;
}
      
RecordType * RecordType::unionWithRecord
(RecordType * insertrec)
{
  RecordType * result  = new RecordType(this);
  
  for (int fieldindex = 1; fieldindex <= insertrec->numFields(); fieldindex++)
  {
    string      insertname  = insertrec->getFieldName(fieldindex);
    BasicType * inserttype  = insertrec->getFieldType(fieldindex);
    bool        insertfield = true;
    
    // See if the name is used in the existing type already.
    if (isValidFieldName(insertname))
    { 
      BasicType * fieldtype = getFieldType(insertname);
      
      // If the types of the field don't match then complain.
      if (! (fieldtype->equals(inserttype)))
      { 
        throw TypeException("When unioning, fields with the same name must have the same type", 
         __FILE__, __LINE__); 
      }
      
      // We don't insert the field since it is already there.
      insertfield = false;
    }
    
    if (insertfield)
    {
      // Add the field to the end of type we are composing.
      (result->record_fieldnames).insert
        ((result->record_fieldnames).end(), insertname);
      (result->record_fieldtypes).insert
        ((result->record_fieldtypes).end(), inserttype);
    }
  }
  
  return result;
} 

RecordType * RecordType::selectFields
(vector<int>* selection)      
{
  // Create an empty type to start with.
  RecordType * result = new RecordType();
  
  for (int selectindex = 0; selectindex < (int) selection->size(); selectindex++)
  { 
    int fieldindex = (*selection)[selectindex];
   
    // Check that this is a valid field index.
    if (!isValidFieldIndex(fieldindex))
    { throw NoSuchFieldException(fieldindex, __FILE__, __LINE__); }
    
    // Check for a duplicate reference to the index later in the list.
    for (int upperindex = selectindex + 1; 
         upperindex < (int) selection->size(); upperindex++)
    { 
      if ((*selection)[upperindex] == (*selection)[selectindex])
      {throw DuplicateFieldException((*selection)[upperindex],__FILE__, __LINE__); }
    }
    
    // Extract the name and type of the referenced field.
    string      fieldname = this->getFieldName(fieldindex);
    BasicType * fieldtype = this->getFieldType(fieldindex);
  
    // Insert the name and type into the new record type.
    (result->record_fieldnames).insert
      ((result->record_fieldnames).begin() + selectindex, fieldname);
    (result->record_fieldtypes).insert
      ((result->record_fieldtypes).begin() + selectindex, fieldtype);
  }

  return result;  
}


RecordType * RecordType::selectFields
(vector<string>* selection)
{
  // This vector will hold the indices corresponding to the names.
  vector<int> indices; 

  for(int selectindex = 0; selectindex < (int) selection->size(); selectindex++)
  {
    // Check for a duplicate in the list.
    for (int upperindex = selectindex + 1; 
         upperindex < (int) selection->size(); upperindex++)
    { 
      if ((*selection)[upperindex] == (*selection)[selectindex])
      {throw DuplicateFieldException((*selection)[upperindex],__FILE__, __LINE__); }
    }

    // Get the field index of this name in this type descriptor.
    int fieldindex = this->getFieldIndex((*selection)[selectindex]);
 
    // Insert this field index into the list of indices
    indices.insert(indices.begin() + selectindex, fieldindex);
  }

  // Delegate to the selectFields by index method.
  RecordType * result = this->selectFields(&indices);

  return result;
}
 
RecordType * RecordType::renameField
(string oldname, string newname)
{
  // Look up the index of the name & delegate to rename field by index 
  int fieldindex      = getFieldIndex(oldname);
  RecordType * result = renameField(fieldindex, newname);
  return result;
}    

RecordType * RecordType::renameField
(int fieldindex, string newname)
{
  RecordType * result  = new RecordType (this);
  
  // Check the field number is within the legal range for indices.
  if (! (result->isValidFieldIndex(fieldindex)))
  { throw NoSuchFieldException(fieldindex,__FILE__, __LINE__); } 
  
  // Check that there are no clashes with existing names.
  // "Renaming" to the same name is allowed.
  if ((result->isValidFieldName(newname)) && 
      (result->getFieldName(fieldindex) != newname))
  { throw  NameClashException(newname,__FILE__, __LINE__); }

  // Vector indices are one lower
  fieldindex--;
  
  (result->record_fieldnames)[fieldindex] = newname;

  return result;
}  
    
RecordType * RecordType::moveField
(string fieldname, int newposition)
{
  // Look up the index of the name and delegate to the move to by index method.
  int fieldindex      = getFieldIndex(fieldname);
  RecordType * result = moveField(fieldindex, newposition);
  return result;
}

RecordType * RecordType::moveField
(int oldposition, int newposition)
{
  RecordType * result = new RecordType(this);
  
  // Check the field indices are within the legal range for indices.
  if (! result->isValidFieldIndex(oldposition)) 
  { throw NoSuchFieldException(oldposition, __FILE__, __LINE__); } 

  if (! result->isValidFieldIndex(newposition)) 
  { throw NoSuchFieldException(newposition, __FILE__, __LINE__); } 
  
  // Vector indices are one lower.
  oldposition--;
  newposition--;
  
  // Get a copy of the name and type.
  string      fieldname = (result->record_fieldnames)[oldposition];
  BasicType * fieldtype = (result->record_fieldtypes)[oldposition];
  
  // First delete the old position.
  (result->record_fieldnames).erase
       ((result->record_fieldnames).begin() + oldposition);  
  (result->record_fieldtypes).erase
       ((result->record_fieldtypes).begin() + oldposition);

  // and then Insert the field in the new position
  (result->record_fieldnames).insert
    ((result->record_fieldnames.begin() + newposition), fieldname);
  (result->record_fieldtypes).insert
    ((result->record_fieldtypes.begin() + newposition), fieldtype);
  
  return result;
}

/*******************************************************************************
* Type query methods.
*******************************************************************************/
int RecordType::getFieldIndex
(string fieldname) 
{
  bool gotmatch    = false;
  int  matchindex  = 0;
  
  // Iterate through the fields looking for the fieldname.
  for (int fieldindex = 1; fieldindex <= numFields();fieldindex++)
  {
    if (fieldname == getFieldName(fieldindex))
    {
      gotmatch    = true;
      matchindex  = fieldindex;
      break;
    }
  }

  if (!gotmatch)
  { throw  NoSuchFieldException(fieldname, __FILE__, __LINE__); }
  
  return matchindex;
}  

string RecordType::getFieldName
(int fieldindex) 
{
  // Check the field index is within the legal range
  if (! isValidFieldIndex(fieldindex))
  { throw NoSuchFieldException(fieldindex, __FILE__, __LINE__); }

  // Vector indices are one lower than field indices
  fieldindex--;
    
  return record_fieldnames[fieldindex];
}

BasicType * RecordType::getFieldType
(string fieldname)
{
  // Look up the index of the name and delegate to get field type by index.
  int fieldindex      = getFieldIndex(fieldname);
  BasicType * result  = getFieldType(fieldindex);
  return result;
}

BasicType * RecordType::getFieldType
(int fieldindex)
{
  if (! isValidFieldIndex(fieldindex))
  { throw  NoSuchFieldException(fieldindex, __FILE__, __LINE__); }

  // Vector indices are one lower than field indices
  fieldindex--;

  return record_fieldtypes[fieldindex];  
}
  
int RecordType::numFields()
{
  return record_fieldnames.size();
}


bool RecordType::isValidFieldIndex
(int fieldindex)
{
  bool result = true;
  
  if ((fieldindex < 1) || ((unsigned) fieldindex > record_fieldnames.size()))
  { result = false; }
  
  return result;
}
 
    
bool RecordType::isValidFieldName
(string fieldname)
{
  bool result = false;

  // Iterate through the fields looking for the fieldname.
  for (int fieldindex = 1; fieldindex <= numFields();fieldindex++)
  {
    string thisfield = getFieldName(fieldindex);
    if (fieldname == thisfield)
    {
      result = true;
      break;
    }
  }

  return result;
}

/*******************************************************************************
* Record type comparison methods.
*******************************************************************************/
bool RecordType::equals 
(RecordType * rtype)
{
  return isSubset(rtype) && rtype->isSubset(this);
}

bool RecordType::orderedEquals(RecordType * rtype)
{
  return isPrefix(rtype) && rtype->isPrefix(this);
}

bool RecordType::isSubset 
(RecordType * rtype)
{
  bool result = true;

  // Basic check roots out obvious negative cases and prevents overrun.
  if (rtype->numFields() > this->numFields())
  {
    result = false;
  }
  else 
  {
    // Loop through each field in the subset candidate rtype.
    for (int fieldindex = 1; fieldindex <= rtype->numFields(); fieldindex++)
    {
      // Get the name and type of the field.
      string      fieldname = rtype->getFieldName(fieldindex);
      BasicType * fieldtype = rtype->getFieldType(fieldindex);
      
      if (!isValidFieldName(fieldname))
      {
        // A field with this name does not exist.
        result = false;
        break;
      }
      else
      {
	BasicType * foundtype = getFieldType(fieldname);
	
	if (!(fieldtype->equals(foundtype)))
	{
	  // Type does not match.
	  result = false;
	  break;
	}
      }
    }
  }

  return result;
}


bool RecordType::isPrefix 
(RecordType * rtype)
{
  bool result = true;

  if (rtype->numFields() > numFields())
  {
    result = false;
  }
  else
  {
    for (int fieldindex = 1; fieldindex <= rtype->numFields(); fieldindex++)
    {
      // Check the names match.
      if (rtype->getFieldName(fieldindex) != getFieldName(fieldindex))
      {
        result = false;
        break;
      }
      
      // Check that the types match.
      BasicType * fieldtype = rtype->getFieldType(fieldindex);
      
      if (! (fieldtype->equals(getFieldType(fieldindex))))
      {
        result = false;
	break;
      }
    }
  }

  return result;
}

/*
 * create header for stream
 */
string RecordType::createHeaderForStream()
{
  std::ostringstream buffer;
  if (numFields() == 0)
  { buffer << "The Empty Type"; }
  else
  {
    for (int fieldindex = 1; fieldindex <= numFields();fieldindex++)
    {
      string fieldname  = getFieldName(fieldindex);
      string typestring = (getFieldType(fieldindex))->toString();

      buffer << typestring << " \"" << fieldname << "\"\n" ;
    }
  }
  
  buffer << "\n";  // << ends; ends not needed for stringstreams

  return buffer.str();
}

/*******************************************************************************
* To String.
*******************************************************************************/
string RecordType::toString()
{
  // Can't concatenate ints/reals to strings in C++, so use a string stream.
  std::ostringstream buffer;
  
  if (numFields() == 0)
  { buffer << "The Empty Type"; }
  else
  {
    for (int fieldindex = 1; fieldindex <= numFields();fieldindex++)
    {
      string fieldname  = getFieldName(fieldindex);
      string typestring = (getFieldType(fieldindex))->toString();

      buffer << fieldindex << ". " << fieldname <<": " << typestring << "\n";
    }
  }
  
  //buffer << ends; not necessary for stingstreams

  return buffer.str();
}
