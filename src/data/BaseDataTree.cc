#include <DataTree.h>
#include <Tile.h>
#include <Node.h>
#include <Frontier.h>
#include <Distribution.h>
#include <Tessellation.h>
#include <RecordInputStream.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BaseDataTree)

using namespace BIE;

BaseDataTree::BaseDataTree()
{
  // map fd_tiledistributions is empty on construction
  fd_distributionfactory = 0;
  fd_tess = 0;
  fd_defaultfrontier = 0;
  fd_total = 0;
  fd_offgrid = 0;
}

// BaseDataTree::BaseDataTree(const BaseDataTree& p)
// {
//   fd_tiledistributions = p.fd_tiledistributions;
//   fd_distributionfactory = p.fd_distributionfactory;
//   fd_tess = p.fd_tess;
//   fd_defaultfrontier = p.fd_defaultfrontier;
//   fd_total = p.fd_total;
//   fd_offgrid = p.fd_offgrid;
// }

BaseDataTree::~BaseDataTree() {
}

SampleDistribution* BaseDataTree::First (Frontier * frontier)
{ 
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }

  return fd_tiledistributions[frontier->First()]; 
}
    
SampleDistribution* BaseDataTree::Last (Frontier * frontier)
{ 
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }

  return fd_tiledistributions[frontier->Last()]; 
}
    
SampleDistribution* BaseDataTree::Next (Frontier * frontier)
{
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }

  return fd_tiledistributions[frontier->Next()];
}
    
SampleDistribution* BaseDataTree::CurrentItem (Frontier * frontier)
{ 
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }

  return fd_tiledistributions[frontier->CurrentItem()];
}

void BaseDataTree::Reset (Frontier * frontier)
{ 
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }

  frontier->Reset();
}

bool BaseDataTree::IsDone (Frontier * frontier)
{ 
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }

  return frontier->IsDone();
}
    
Tile* BaseDataTree::CurrentTile(Frontier * frontier)
{ 
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }

  return fd_tess->GetTile(frontier->CurrentItem());
}
 

void BaseDataTree::SetDefaultFrontier(Frontier * frontier)
{
  if (frontier->getTessellation() != fd_tess)
  { throw InvalidFrontierException(__FILE__,__LINE__); }
  fd_defaultfrontier = frontier;
}

SampleDistribution* BaseDataTree::getDistribution(int tileid)
{
  if (! (fd_tess->IsValidTileID(tileid)))
  { throw TileIDException(tileid, fd_tess->MinID(), fd_tess->MaxID(), __FILE__, __LINE__); }
  
  return fd_tiledistributions[tileid];
}

int BaseDataTree::NumberItems()
{
  int n = 0;
  for (Reset(); !IsDone(); Next()) n++;
  return n;
}

void BaseDataTree::CountData()
{
  SampleDistribution *bd;
  double count = 0;

  for (Reset(); !IsDone(); Next()) {
    bd = CurrentItem();
    for (int j=0; j<bd->numberData(); j++) count += bd->getValue(j);
  }

  cout << "DataTree: count=" << count << "  Expected=" << Total()
       << endl;
}

// For debugging pre-serialization, cursory sanity check of the map
void BaseDataTree::Debug()
{
  if (0) {
    if (myid==0) {
      map<int, SampleDistribution*>::iterator it = fd_tiledistributions.begin();
      map<int, SampleDistribution*>::iterator iz = fd_tiledistributions.end();

      for (unsigned icnt=1; it!=iz; it++, icnt++) {
	cout << left << setw(8) << icnt << setw(4) << it->second->numberData();
	for (int j=0; j<it->second->numberData(); j++)
	  cout << setw(12) << it->second->getValue(j);
	cout << endl;
      }
    }
  }
}
