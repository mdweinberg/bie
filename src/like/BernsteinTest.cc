#include <cmath>
#include <vector>
#include <iomanip>

#include <values.h>

#include <BernsteinTest.h>
#include <BIEmpi.h>
#include <gfunction.h>
#include <Uniform.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BernsteinTest)

using namespace BIE;

//
// Constructor
//
BernsteinTest::BernsteinTest() : LikelihoodFunction()
{
  xmin        =  0.0;
  xmax        =  1.0;
  npts        =  40;
  first       =  true;
  beta        =  0.0;		// Unused here
  type        =  quartic;

  makeData();
}

//
// Constructor
//
BernsteinTest::BernsteinTest(int N) : LikelihoodFunction()
{
  xmin        =  0.0;
  xmax        =  1.0;
  npts        =  N;
  first       =  true;
  beta        =  0.0;		// Unused here
  type        =  quartic;

  makeData();
}

//
// Constructor
//
BernsteinTest::BernsteinTest(double b, int N) : LikelihoodFunction()
{
  xmin        =  0.0;
  xmax        =  1.0;
  npts        =  N;
  first       =  true;
  beta        =  b;
  type        = powerlaw;

  if (beta<=-1.0) {
    std::cout << "Beta must be > -1.0; setting beta=-1/2" << std::endl;
    beta = -0.5;
  }

  makeData();
}

//
// Constructor
//
BernsteinTest::BernsteinTest(int N, 
			     double c1, double c2,
			     double s1, double s2, 
			     double w1, double w2) : LikelihoodFunction()
{
  xmin        =  0.0;
  xmax        =  1.0;
  npts        =  N;
  first       =  true;
  beta        =  0.0;
  type        =  twobump;

  std::vector<double> v(5);

  v[0] = c1;			// Center
  v[1] = s1 * s1;		// Variance
  v[2] = w1;			// Weight
  c.push_back(v);

  v[0] = c2;			// Center
  v[1] = s2 * s2;		// Variance
  v[2] = w2;			// Weight
  c.push_back(v);

  makeData();
}


double BernsteinTest::PDF(double x)
{
  if (x<=0.0 || x>=1.0) return 0.0;

  switch (type) {
  case twobump:
    return 
      c[0][2]*exp(-0.5*(x-c[0][0])*(x-c[0][0])/c[0][1])/(c[0][3] - c[0][4]) +
      c[1][2]*exp(-0.5*(x-c[1][0])*(x-c[1][0])/c[1][1])/(c[1][3] - c[1][4]) ;
  case powerlaw:
    return pow(x, beta)*(1.0 + beta);
  case quartic:
  default:
    return 30.0*x*x*(1.0 - x)*(1.0 - x);
  }
}

double BernsteinTest::CDF(double x)
{
  if (x<=0.0) return 0.0;
  if (x>=1.0) return 1.0;

  switch (type) {
  case twobump:
    return 
      c[0][2]*
      (0.5*erf((x - c[0][0])/sqrt(2.0*c[0][1])) - c[0][4])/(c[0][3] - c[0][4]) +
      c[1][2]*
      (0.5*erf((x - c[1][0])/sqrt(2.0*c[1][1])) - c[1][4])/(c[1][3] - c[1][4]) ;
  case powerlaw:
    return pow(x, 1.0 + beta);
  case quartic:
  default:
    return x*x*x*(10.0 + (6.0*x - 15.0)*x);
  }
}

void BernsteinTest::makeData()
{
  //
  // Make two-bump normalization
  //

  if (type == twobump) {
    for (int i=0; i<2; i++) {
      c[i][3] = 0.5*erf((1.0 - c[i][0])/sqrt(2.0*c[i][1])); // P(<1)
      c[i][4] = 0.5*erf((0.0 - c[i][0])/sqrt(2.0*c[i][1])); // P(<0)
    }
  }

  //
  // Initialize the point distribution ("fake data")
  //

  data.resize(npts);

  if (myid==0) {

    // For selecting data variates
    //
    Uniform unit(0.0, 1.0, BIEgen); 

    // Make a mesh of the CDF
    //
    const size_t nmesh = 1000.0;
    std::vector<double> x(nmesh), y(nmesh);
    double dx = 1.0/(nmesh-1);
    for (size_t i=0; i<nmesh; i++) {
      x[i] = dx*i;
      y[i] = CDF(x[i]);
    }

				// Generate data
    for (size_t n=0; n<npts; n++) {
      double u = unit();
      std::vector<double>::iterator loc =
	std::upper_bound(y.begin(), y.end(), u);

      size_t bot = 0, top = npts - 1;

      if (loc == y.end()) {	// Assume u = 1
	bot = npts - 2;
      } else if (loc == y.begin()) {
	bot = 0;
	top = 1;
      } else {
	top = static_cast<size_t>(loc - y.begin());
	bot = top - 1;
      }

      double d = y[top] - y[bot];
      double a = (y[top] - u) / d;
      double b = (u - y[bot]) / d;
      double v = a*x[bot] + b*x[top];

      data[n] = v;
    }

				// Send data everywhere
    MPI_Bcast(&data[0], npts, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  } else {
				// Get data from Node 0
    MPI_Bcast(&data[0], npts, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  }

  if (myid==0) {
    std::ostringstream sout;
    sout << nametag << ".bernlike.point";
    ofstream out(sout.str().c_str());
    if (out) {
      for (size_t i=0; i<npts; i++) out << setw(15) << data[i] << endl;
    }
  }
}

void BernsteinTest::initialize(State *s)
{
  order = s->N() - 1;
  bern  = BernsteinPoly(order);
  first = false;
}

double BernsteinTest::LikeProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx)
{
  if (first) initialize(s);

  double ret = 0.0;
  for (size_t n=0; n<npts; n++) {
    double p = bern(*s, data[n]);
    if (p <= 0.0) ret += -DBL_MAX/(npts+1);
    else          ret += log(p);
  }

  return ret;
}

double BernsteinTest::CumuProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx, Fct1dPtr f)
{
  if (first) initialize(s);

  double ret = 0.0;
  for (size_t n=0; n<npts; n++) {
    double ans = 0.0;
    for (size_t j=0; j<=order; j++) 
      ans += (*s)[j] * bern.integral(j, data[n]);
    if (ans <= 0.0) ret += -DBL_MAX/(npts+1);
    else            ret += log(ans);
  }

  return ret;
}

//
// Used to label output
//
const std::string 
BernsteinTest::ParameterDescription(int i)
{
  std::ostringstream sout;
  sout << "C[" << i << "]";
  return sout.str();
}
