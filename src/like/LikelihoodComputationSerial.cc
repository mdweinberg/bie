#include <LikelihoodComputationSerial.h>

#include <Simulation.h>
#include <Model.h>
#include <Distribution.h>
#include <gfunction.h>
#include <BIEMutex.h>
#include <PointTessellation.h>
#include <gfunction.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif

BIE_CLASS_EXPORT_IMPLEMENT(BIE::LikelihoodComputationSerial)

using namespace BIE;

/**
   Serial implmentation of Likelihood computation.
*/

/// Constructor
LikelihoodComputationSerial::LikelihoodComputationSerial() : tess_tool(false)
{}

LikelihoodComputationSerial::~LikelihoodComputationSerial() {}

/// Determine liklihood for previous state.
double LikelihoodComputationSerial::LikelihoodPrevious(State &s, int indx)
{
  if (GetSimulation()->priorFrontier() == 0) return 0.0;

				// Install the old frontier
  setPriorFrontier();

  double ret = Likelihood(s, indx);
				// Restore the current frontier
  setCurrentFrontier();

  return ret;
}


/// Determine liklihood for given state.
double LikelihoodComputationSerial::Likelihood(State &s, int indx)
{
  LikeState ans = {0.0, true};
  int go = 0;

  if (mpi_used && tess_tool) {

    if (myid==0) {
      TessToolSynchronize();
      go = encode_command(go);
    }

    ++MPIMutex;
    MPI_Bcast(&go, 1, MPI_INT, 0, MPI_COMM_WORLD);
    --MPIMutex;

    decode_command(go);
  }

      
#ifdef DEBUG_TESSTOOL
  ferr << "Process " << myid << ": after synchronize" << endl << flush;
#endif

  vector<double> z;
  double norm = 0.0;
  Tile* t;
  SampleDistribution *sd;

  likelihood_count++;

				// Copy some stuff from the simulation
				//
  Model* _model          = GetModel();
  BaseDataTree* _dist    = GetBaseDataTree();
  Integration* _intgr    = GetIntegration();
  LikelihoodFunction* LP = GetLikeProb();

  _model->Initialize(s);

  
				// DEBUG showstates
				//
  if (debugflags & showstates) GetSimulation()->ReportState();

				// Compute model normalzation
				//
  if (dynamic_cast<PointTessellation*>(_dist->GetTessellation())) {
    double xmin, xmax, ymin, ymax;
    _dist->GetTessellation()->GetLimits(xmin, xmax, ymin, ymax);
    norm = _model->NormEval(xmin, xmax, ymin, ymax, true); 
  } else {

    _model->ResetCache();
    for (_dist->Reset(); !(_dist->IsDone()); _dist->Next())
      norm += _intgr->NormValue(_model, _dist->CurrentTile(), _dist->CurrentItem());
  }

  if (norm <= 0.0) {
    throw ImpossibleStateException(__FILE__, __LINE__);
  }

				// Set up for dumping tile info
				//
  if (sampleNext==1 && tess_tool) {
    InitializeTessToolTileDump();
#ifdef DEBUG_TESSTOOL
    ferr << "Process " << myid << ": tesstool tiles dump begin" << endl << flush;
#endif
  }
				// Compute log likelihood
				//
  _model->ResetCache();
  for (_dist->Reset(); !(_dist->IsDone()); _dist->Next()) {

    t  = _dist->CurrentTile();
    sd = _dist->CurrentItem();
      
    z  = _intgr->Value(_model, t, sd);

    try {
      ans.value += LP->LikeProb(z, sd, _dist->Total()/norm, t, &s, indx);
    }
    catch (ImpossibleStateException &e) {
      ans.good = false;
    }
      
    if (DEBUG_LIKE) {
      vector<double> zz = z;
      for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm;
      TileDump(t, sd, zz);
    }

    if (sampleNext==1 && tess_tool) {
      vector<double> zz = z;
      for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm;
      TessToolTileDump(t, sd, zz);
    }
  }
  
  if (sampleNext==1 && tess_tool) {
    FinalizeTessToolTileDump();
    sampleNext=0;
  }
    
  DEBUG_LIKE = false;


  if ( mpi_used && myid && sampleNext==1 && tess_tool) {
#ifdef DEBUG_TESSTOOL
    ferr << "Process " << myid << ": tesstool initialize . . . " 
	 << endl << flush;
#endif
    InitializeTessToolTileDump();
    FinalizeTessToolTileDump();
    sampleNext=0;
#ifdef DEBUG_TESSTOOL
    ferr << "Process " << myid << ": tesstool finalize and done" 
	 << endl << flush;
#endif
  }
  
  if (!ans.good) throw ImpossibleStateException(__FILE__, __LINE__);
  
  return ans.value;
}


/// Evaluate the model norm at a list of points.
void LikelihoodComputationSerial::normEvaluateList(Model *model, Tile *tile, 
			      const vector<double>& x,
			      const vector<double>& y,
			      SampleDistribution* sampleDistribution,
			      vector<double>& evaluations)
{
  cout << "Norm Evaluate Function has not been implemented for this module\n";
  // write the simple for loop!
}


/// List evaluation function for model points
void LikelihoodComputationSerial::modelEvaluateList(Model *model, Tile *tile, 
		       const vector<double>& x,
		       const vector<double>& y,
		       SampleDistribution* sampleDistribution,
		       vector< vector<double> >& evaluations)
{
  cout << "ModelEvaluate Function has not been implemented for this module\n";
  // write the simple for loop!
}

