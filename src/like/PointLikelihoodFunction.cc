#include <iomanip>

#include <Distribution.h>
#include <Tile.h>
#include <PointLikelihoodFunction.h>
#include <BIEmpi.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PointLikelihoodFunction)

double PointLikelihoodFunction::LikeProb(vector<double> &z, SampleDistribution* sd, 
					 double norm, Tile *t, State *s, int indx)
{
  double ans = 0.0;

  for (int j=0; j<sd->numberData(); j++) {
      
    if (z[j]<=0.0) {
      throw ImpossibleStateException(__FILE__, __LINE__);
    }

    // Print it out
    if (debugflags & badcell) {
      ostream cout(checkTable("debug_output"));
	
      double x1, x2, y1, y2;
      t->corners(x1, x2, y1, y2);
      cout << "Process " << myid << ": badcell, value=" 
	   << sd->getValue(j) << ", point: "
	   << x1 << " " << y1 << "\n";
      cout << "  state:\n";
      for (unsigned int i=0; i<s->size(); i++) cout << setw(12) << (*s)[i];
      cout << "\n";
      
    }
    
    else 
      ans += log(z[j]);
    
  }
    
  return ans;
}

