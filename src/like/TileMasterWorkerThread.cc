
#include <cstdlib>
using namespace std;

#include <TileMasterWorkerThread.h>

#include <BIEMutex.h>
#include <NormEvaluationRequest.h>
#include <ModelEvaluationRequest.h>
#include <InitEvaluationRequest.h>

#include <LikelihoodComputationMPITP.h>

using namespace BIE;


TileMasterWorkerThread::TileMasterWorkerThread(LikelihoodComputationMPITP *simulation)
{
  reInitialize(simulation);
}


void TileMasterWorkerThread::reInitialize(LikelihoodComputationMPITP *simulation)
{
  this->simulation = simulation;
  stopRequested = false;
  resetPauseRequested = false;

  // make sure workAvailable semaphore is at zero

  /* Deprecated usage
  while (workAvailable.getValue()) workAvailable.wait();
  */

  request = 0;


}


TileMasterWorkerThread::~TileMasterWorkerThread(){}

void TileMasterWorkerThread::Send(EvaluationRequest *request)
{
   if (request->sent){
    cerr << "TileMasterWorkerThread.Send:  ERROR. Attempt to evaluate request twice .\n";
    std::exit(1); 
   }

   if (this->request != 0){
     cerr << "TileMasterWorkerThread.Send:  ERROR.MasterWorkerThread busy\n";
      std::exit(1);
   }
   this->request = request;
   this->request->sent = true;
   workAvailable.post();
}

void TileMasterWorkerThread::run()
{
  // Let everyone know we're open for business
  simulation->tileRequestHandlerThread->masterWorkerRegister();
  
  while(!stopRequested){

     workAvailable.wait();

     if (resetPauseRequested){
       paused.post();
       resume.wait();
       if (stopRequested) break;
       simulation->tileRequestHandlerThread->masterWorkerRegister(); //ugly.Need to reannounce after reset
       continue;  // don't process a request; we might not have one.
     }

     if (stopRequested) break;

     NormEvaluationRequest *normRequest=0;
     ModelEvaluationRequest *modelRequest=0;

     if (request == 0){
       cout <<"Null request in TileMasterWorkerThread\n";
       std::exit(1);
     }

     if ( (normRequest = dynamic_cast<NormEvaluationRequest*>(request))){
       *(normRequest->result) = simulation->modelEvaluateNormWorker((int)normRequest->args[0],
								    normRequest->args[1],
								    normRequest->args[2]);
        normRequest->received = true;
        normRequest->finished->post();


     }else if ( (modelRequest = dynamic_cast<ModelEvaluationRequest*>(request))){

        TSLog cmdLog;
        // evaluate and copy result into caller's storage
        *(modelRequest->result) = simulation->modelEvaluateWorker((int)modelRequest->args[0],
								  modelRequest->args[1],
								  modelRequest->args[2],
								  cmdLog);
        // Let caller know we're done
        modelRequest->received = true;
        modelRequest->finished->post();

     }else if ( dynamic_cast<InitEvaluationRequest*>(request)){
       cerr << "Init evaluation request found in main loop of master worker thread\n";
       std::exit(1);
     }

     // compute some stats for debugging
     simulation->tileRequestHandlerThread->endTime[0] = MPI_Wtime();
     int executionTime = static_cast<int>((simulation->tileRequestHandlerThread->endTime[0] -
                                           simulation->tileRequestHandlerThread->startTime[0])*1000);


     if (executionTime<0){
       cerr << "Execution time < 0!\n";
       std::exit(1);
     }

     if (executionTime >= static_cast<int>(simulation->tileRequestHandlerThread->histo[0].size())){
       //cout << "Thresholding execution time "<< executionTime << " to ";
       executionTime = static_cast<int>(simulation->tileRequestHandlerThread->histo[0].size())-1;
       //cout <<  executionTime << "\n";
     }

     simulation->tileRequestHandlerThread->histo[0][executionTime]++;

     // Let everyone know we can take another request
     request = 0;
     simulation->tileRequestHandlerThread->masterWorkerAvailable();

  } // end while
}

void TileMasterWorkerThread::resetPause()
{
  if (resetPauseRequested) return;  // if already paused, return

  resetPauseRequested = true;
  workAvailable.post();  // get the thread unstuck
  paused.wait();  // wait until actually paused
}

void TileMasterWorkerThread::unPause()
{
  resetPauseRequested=false;
  resume.post();
}

void TileMasterWorkerThread::stop()
{
  // get to known state
  resetPause();

  // flag that we want to stop
  stopRequested = true;

  // release the thread
  unPause();

}



/*
 *  cleanup the thread object with Run() returns or Thread.exit() is called
 *  Clients stop the thread by setting the member variable "stopRequested,
 *  causing the loop in the Run() method to exit.
 */

void TileMasterWorkerThread::final()
{
    // The common c++ documentation swears this is an okay thing to do.
    delete this;  //egad!
}

