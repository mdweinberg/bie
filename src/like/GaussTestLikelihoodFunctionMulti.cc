#include <vector>
#include <iomanip>

#include <Distribution.h>
#include <Tile.h>
#include <GaussTestLikelihoodFunctionMulti.h>
#include <BIEmpi.h>
#include <gfunction.h>
#include <gaussQ.h>
#include <Uniform.h>
#include <Normal.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GaussTestLikelihoodFunctionMulti)

using namespace std;
using namespace BIE;

//
// Default values for synthetic data, if not specified explicitly in
// the constructor
//
static const unsigned nmix_def       = 2;
static const double   centers_def [] = {0.2,  0.9};
static const double   variance_def[] = {0.03, 0.03};
static const double   weights_def [] = {0.5,  0.5};

//
// Constructor
//
GaussTestLikelihoodFunctionMulti::GaussTestLikelihoodFunctionMulti() : 
  LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  ptwo = 9;
  N = 40000;
  dim = 2;
  nmix = nmix_def;
  levels = 1;
  point = false;

  for (unsigned n=0; n<nmix; n++) {
    centers .push_back(centers_def [n]);
    variance.push_back(variance_def[n]);
    weights .push_back(weights_def [n]);
  }

  makeArrays();
}

GaussTestLikelihoodFunctionMulti::
GaussTestLikelihoodFunctionMulti(int Ptwo, int num, int Levels) 
  : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  ptwo = Ptwo;
  levels = Levels;
  N = num;
  dim = 2;
  nmix = nmix_def;
  point = false;

  for (unsigned n=0; n<nmix; n++) {
    centers .push_back(centers_def [n]);
    variance.push_back(variance_def[n]);
    weights .push_back(weights_def [n]);
  }

  makeArrays();
}

GaussTestLikelihoodFunctionMulti::
GaussTestLikelihoodFunctionMulti(int Ptwo, int num, int Levels,
				 clivectord* cen0,
				 clivectord* var0,
				 clivectord* wgt0)
  : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  ptwo = Ptwo;
  levels = Levels;
  N = num;
  dim = 2;
  nmix = (*cen0)().size();
  point = false;

  //
  // Dimension sanity check
  //
  if (nmix != (*var0)().size() || nmix != (*wgt0)().size()) {
    ostringstream msg;
    msg << "GaussTestLikelihoodFunction: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size()
	<< " size(weights)="  << (*wgt0)().size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  for (unsigned n=0; n<nmix; n++) {
    centers .push_back((*cen0)()[n]);
    variance.push_back((*var0)()[n]);
    weights .push_back((*wgt0)()[n]);
  }

  makeArrays();
}

GaussTestLikelihoodFunctionMulti::
GaussTestLikelihoodFunctionMulti(int Ptwo, int num, int Levels,
				 std::vector<double> cen0,
				 std::vector<double> var0,
				 std::vector<double> wgt0)
  : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  ptwo = Ptwo;
  levels = Levels;
  N = num;
  dim = 2;
  nmix = cen0.size();
  point = false;

  //
  // Dimension sanity check
  //
  if (nmix != var0.size() || nmix != wgt0.size()) {
    ostringstream msg;
    msg << "GaussTestLikelihoodFunction: rank mismatch!"
	<< " size(center)="   << cen0.size() 
	<< " size(variance)=" << var0.size()
	<< " size(weights)="  << wgt0.size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  for (unsigned n=0; n<nmix; n++) {
    centers .push_back(cen0[n]);
    variance.push_back(var0[n]);
    weights .push_back(wgt0[n]);
  }

  makeArrays();
}

GaussTestLikelihoodFunctionMulti::
GaussTestLikelihoodFunctionMulti(int num, int Levels) : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  ptwo = 0;
  levels = Levels;
  N = num;
  dim = 2;
  nmix = nmix_def;
  point = true;

  for (unsigned n=0; n<nmix; n++) {
    centers .push_back(centers_def [n]);
    variance.push_back(variance_def[n]);
    weights .push_back(weights_def [n]);
  }

}

GaussTestLikelihoodFunctionMulti::
GaussTestLikelihoodFunctionMulti(int num, int Levels,
				 clivectord* cen0,
				 clivectord* var0,
				 clivectord* wgt0)
  : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  ptwo = 0;
  levels = Levels;
  N = num;
  dim = 2;
  nmix = (*cen0)().size();
  point = true;

  //
  // Dimension sanity check
  //
  if (nmix != (*var0)().size() || nmix != (*wgt0)().size()) {
    ostringstream msg;
    msg << "GaussTestLikelihoodFunction: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size()
	<< " size(weights)="  << (*wgt0)().size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  for (unsigned n=0; n<nmix; n++) {
    centers .push_back((*cen0)()[n]);
    variance.push_back((*var0)()[n]);
    weights .push_back((*wgt0)()[n]);
  }

}

GaussTestLikelihoodFunctionMulti::
GaussTestLikelihoodFunctionMulti(int num, int Levels,
				 std::vector<double> cen0,
				 std::vector<double> var0,
				 std::vector<double> wgt0)
  : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  ptwo = 0;
  levels = Levels;
  N = num;
  dim = 2;
  nmix = cen0.size();
  point = true;

  //
  // Dimension sanity check
  //
  if (nmix != var0.size() || nmix != wgt0.size()) {
    ostringstream msg;
    msg << "GaussTestLikelihoodFunction: rank mismatch!"
	<< " size(center)="   << cen0.size() 
	<< " size(variance)=" << var0.size()
	<< " size(weights)="  << wgt0.size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  for (unsigned n=0; n<nmix; n++) {
    centers .push_back(cen0[n]);
    variance.push_back(var0[n]);
    weights .push_back(wgt0[n]);
  }

}

void GaussTestLikelihoodFunctionMulti::SetLevels(int n)
{
  levels = n;

  makeArrays();
  makeSyntheticData();
}

void GaussTestLikelihoodFunctionMulti::SetDim(int n)
{
  switch (n) {
  case 1:
    dim = 1;
    break;
  case 2:
    dim = 2;
    break;
  default:
    dim = 2;
  }

}

void GaussTestLikelihoodFunctionMulti::makeArrays()
{
  nbins.clear();
  dx   .clear();
  fdata.clear();

  if (levels > ptwo) levels = ptwo;
  for (int l=0; l<levels; l++) {
    int n = 1<<(ptwo-l);
    nbins.push_front(n);
    dx.push_front((xmax-xmin)/n);
  }
  
}

void GaussTestLikelihoodFunctionMulti::makeSyntheticData()
{
  //
  // Initialize the comparison distribution ("fake data")
  //
  double x1, x2;
  vector<double> ddata(nbins.back(), 0.0);
  for (unsigned c=0; c<nmix; c++) {
    for (int i=0; i<nbins.back(); i++) {
      x1 = xmin + dx.back()*i;
      x2 = xmin + dx.back()*(i+1);
      ddata[i] += weights[c]*(  
			      erf( (x2 - centers[c])/sqrt(2.0*variance[c]) ) -
			      erf( (x1 - centers[c])/sqrt(2.0*variance[c]) )
			    ) * N;
    }
  }

  for (int i=0; i<nbins.back(); i++) ddata[i] = round(ddata[i]);

  fdata.push_front(ddata);
  for (int l=1; l<levels; l++) {
    ddata.clear();
    for (unsigned int n=0; n<fdata.front().size(); n++) {
      ddata.push_back(fdata.front()[n]+fdata.front()[n+1]);
      n++;
    }
    fdata.push_front(ddata);
  }

  generated = true;
}

void GaussTestLikelihoodFunctionMulti::makeSyntheticPointData()
{
  if (myid==0) {

    vector<double> frac = weights;
    for (unsigned c=1; c<nmix; c++) frac[c] += frac[c-1];
    
    Uniform unif(0.0, frac[nmix-1], BIEgen);
    Normal  norm(0.0, 1.0, BIEgen);

    for (unsigned c=0; c<nmix; c++) {
      cout << "Frac[" << c << "]=" << frac[c] << endl;
    }

    unsigned comp = 0;
    for (int i=0; i<N; i++) {
      double wh = unif();
      for (unsigned c=0; c<nmix; c++) {
	if (wh<=frac[c]) {
	  comp = c;
	  break;
	}
      }
      
      pdata.push_back(centers[comp] + sqrt(variance[comp])*norm());
      cdata.push_back(comp);
    }

				// Send data everywhere
    MPI_Bcast(&pdata[0], N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&cdata[0], N, MPI_INT   , 0, MPI_COMM_WORLD);

  } else {
				// Get data from Node 0
    pdata = vector<double>(N);
    cdata = vector<int>   (N);
    MPI_Bcast(&pdata[0], N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&cdata[0], N, MPI_INT   , 0, MPI_COMM_WORLD);
  }

  generated = true;
}


//
// Print data to a file for the current level
//
void GaussTestLikelihoodFunctionMulti::PrintData()
{
  ostringstream sout;
  int n = min<int>(current_level, levels-1);

  sout << nametag << ".data." << n;
  ofstream out(sout.str().c_str());

  if (out) {

    if (point) {
      for (int i=0; i<N; i++)
	out << setw(3)  << cdata[i]
	    << setw(18) << pdata[i] 
	    << endl;
    } else {
      for (int i=0; i<nbins[n]; i++) {
	out << setw(18) << xmin + dx[n]*(0.5+i) 
	    << setw(18) << fdata[n][i] 
	    << endl;
      }
    }
  } else {
    throw FileOpenException(sout.str(), errno, __FILE__, __LINE__);
  }
}


//
// The only variable we care about here is <State* s>
//
double GaussTestLikelihoodFunctionMulti::LikeProb
(std::vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx)
{
  ofstream out;
  double x1, x2, tst, val = 0.0, dif;

  int Mcur = s->M();
  int n = min<int>(current_level, levels-1);

  if (point) {
    
    if (not generated) makeSyntheticPointData();

    int Nend = N;
    for (int i=levels-1; i>current_level; i--) Nend /= 2;
    
    for (int i=0; i<Nend; i++) {

      if (s->Type() == StateInfo::None || s->Type() == StateInfo::Block) {
	dif = pdata[i] - (*s)[0];
	switch (dim) {
	case 1:
	  tst =  exp(-dif*dif/(2.0*variance[0])) / sqrt(2.0*M_PI*variance[0]);
	  break;
	case 2:
	default:
	  tst =  exp(-dif*dif/(2.0*(*s)[1])) / sqrt(2.0*M_PI*(*s)[1]);
	  break;
	}
	
      } else {
      
	tst = 0.0;

	for (int c=0; c<Mcur; c++) {
	  dif = pdata[i] - s->Phi(c, 0);
	  switch (dim) {
	  case 1:
	    tst += s->Wght(c) * exp(-dif*dif/(2.0*variance[c]))
	      / sqrt(2.0*M_PI*variance[c]);
	    break;
	  case 2:
	  default:
	    tst += s->Wght(c) * exp(-dif*dif/(2.0*s->Phi(c, 1)))
	      / sqrt(2.0*M_PI*s->Phi(c, 1));
	    break;
	  }
	}
      }
      
      tst = max<double>(0.0, tst);

      if (tst==0) {
      
	throw ImpossibleStateException(__FILE__, __LINE__);
	
      } else {
	
	val += log(tst);
	
      }

    }

  } else {

    if (not generated) makeSyntheticData();

    for (int i=0; i<nbins[n]; i++) {

      x1 = xmin + dx[n]*i;
      x2 = xmin + dx[n]*(i+1);

      if (s->Type() == StateInfo::None || s->Type() == StateInfo::Block) {

	switch (dim) {
	case 1:
	  tst = (  
		 erf( (x2 - (*s)[0])/sqrt(2.0*variance[0]) ) -
		 erf( (x1 - (*s)[0])/sqrt(2.0*variance[0]) )
		   ) * N;
	  break;
	case 2:
	default:
	  tst =  (  
		  erf( (x2 - (*s)[0])/sqrt(2.0*(*s)[1]) ) -
		  erf( (x1 - (*s)[0])/sqrt(2.0*(*s)[1]) )
		    ) * N;
	  break;
	}
	
      } else {

	tst = 0.0;

	for (int c=0; c<Mcur; c++) {
	  
	  switch (dim) {
	  case 1:
	    tst += s->Wght(c) *
	      (  
	       erf( (x2 - s->Phi(c, 0))/sqrt(2.0*variance[c]) ) -
	       erf( (x1 - s->Phi(c, 0))/sqrt(2.0*variance[c]) )
		 ) * N;
	    break;
	  case 2:
	  default:
	    tst += s->Wght(c) *
	      (  
	       erf( (x2 - s->Phi(c, 0))/sqrt(2.0*s->Phi(c, 1)) ) -
	       erf( (x1 - s->Phi(c, 0))/sqrt(2.0*s->Phi(c, 1)) )
		 ) * N;
	    break;
	  }
	}

      }
      
      tst = max<double>(0.0, tst);

      if (tst==0) {
	
	if (fdata[n][i]!=0) {
	  throw ImpossibleStateException(__FILE__, __LINE__);
	}
	
      } else {
	
	val += log(tst)*fdata[n][i] - tst - lgamma(1.0+fdata[n][i]);
	
      }
    }
  }
  

  return val;
}

//
// The only variable we care about here is <State* s> and Fct1dPtr
//
double GaussTestLikelihoodFunctionMulti::CumuProb
(std::vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx, Fct1dPtr f)
{
  ofstream out;
  double x1, x2, tst, val = 0.0, dif;

  int Mcur = s->M();
  int n = min<int>(current_level, levels-1);

  if (point) {

    if (not generated) makeSyntheticPointData();

    int Nend = N;
    for (int i=levels-1; i>current_level; i--) Nend /= 2;

    for (int i=0; i<Nend; i++) {

      if (s->Type() == StateInfo::None || s->Type() == StateInfo::Block) {

	dif = pdata[i] - (*s)[0];
	switch (dim) {
	case 1:
	  tst = 0.5*( 1.0 + erf(dif/sqrt(2.0*variance[0])) );
	  break;
	case 2:
	default:
	  tst = 0.5*( 1.0 + erf(dif/sqrt(2.0*(*s)[1])) );
	  break;
	}

      } else {

	tst = 0.0;

	for (int c=0; c<Mcur; c++) {
	  dif = pdata[i] - s->Phi(c, 0);
	  switch (dim) {
	  case 1:
	    tst += s->Wght(c) *
	      0.5*( 1.0 + erf(dif/sqrt(2.0*variance[c])) );
	    break;
	  case 2:
	  default:
	    tst += s->Wght(c) *
	      0.5*( 1.0 + erf(dif/sqrt(2.0*s->Phi(c, 1))) );
	    break;
	  }
	}
	
      }

      
      if (tst<=0) {		// Special case: _expect_ no points in bin
	
	if (fdata[n][i]!=0) {	// But there are points in bin: prob 0
	  tst = f->G(0.0);
	} else {		// And there are no points in bin: prob 1
	  tst = f->G(1.0);
	}
	
      } else {			// The poisson probability
	
	tst = f->G(exp(log(tst)*fdata[n][i] - tst - lgamma(1.0+fdata[n][i])));
	
      }
      
      // Take the log
      if (tst<=0) {
	val += -INFINITY;
      } else {
	val += log(tst);
      }
      
    }
  
  } else {

    if (not generated) makeSyntheticData();

    for (int i=0; i<nbins[n]; i++) {

      x1 = xmin + dx[n]*i;
      x2 = xmin + dx[n]*(i+1);

      if (s->Type() == StateInfo::None || s->Type() == StateInfo::Block) {

	switch (dim) {
	case 1:
	  tst = (
		 erf( (x2 - (*s)[0])/sqrt(2.0*variance[0]) ) -
		 erf( (x1 - (*s)[0])/sqrt(2.0*variance[0]) )
		 ) * N;
	  break;
	case 2:
	default:
	  tst = (
		 erf( (x2 - (*s)[0])/sqrt(2.0*(*s)[1]) ) -
		 erf( (x1 - (*s)[0])/sqrt(2.0*(*s)[1]) )
		 ) * N;
	  break;
	}

      } else {
	
	tst = 0.0;

	for (int c=0; c<Mcur; c++) {
	  switch (dim) {
	  case 1:
	    tst += s->Wght(c) *
	      (  
	       erf( (x2 - s->Phi(c, 0))/sqrt(2.0*variance[c]) ) -
	       erf( (x1 - s->Phi(c, 0))/sqrt(2.0*variance[c]) )
		 ) * N;
	    break;
	  case 2:
	  default:
	    tst += s->Wght(c) *
	      (  
	       erf( (x2 - s->Phi(c, 0))/sqrt(2.0*s->Phi(c, 1)) ) -
	       erf( (x1 - s->Phi(c, 0))/sqrt(2.0*s->Phi(c, 1)) )
		 ) * N;
	    break;
	  }
	}
      }
	
      if (tst<=0) {		// Special case: _expect_ no points in bin
	
	if (fdata[n][i]!=0) {	// But there are points in bin: prob 0
	  tst = f->G(0.0);
	} else {		// And there are no points in bin: prob 1
	  tst = f->G(1.0);
	}
	
      } else {			// The poisson probability
	
	tst = f->G(exp(log(tst)*fdata[n][i] - tst - lgamma(1.0+fdata[n][i])));
	
      }
	
      // Take the log
      
      if (tst<=0) {
	
	val += -INFINITY;
	
      } else {
	
	val += log(tst);
	
      }

    }

  }

  return val;
}

//
// Used to label output
//
const std::string GaussTestLikelihoodFunctionMulti::ParameterDescription(int i)
{
  string ret;

  switch(i) {
  case 0:
    ret = "Position\0";
    break;
  case 1:
    ret = "Variance\0";
    break;
  default:
    ret = "**Error**\0";
    break;
  }

  return ret;
}


