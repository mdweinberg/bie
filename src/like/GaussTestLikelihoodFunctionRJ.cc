#include <cmath>
#include <vector>
#include <iomanip>

#include <boost/math/special_functions.hpp>

#include <Distribution.h>
#include <Tile.h>
#include <GaussTestLikelihoodFunctionRJ.h>
#include <BIEmpi.h>
#include <gfunction.h>
#include <Uniform.h>
#include <Normal.h>
#include <gaussQ.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GaussTestLikelihoodFunctionRJ)

using namespace std;
using namespace BIE;

static const int    number0 = 2;
static const double weights0  [] = {0.5,  0.5};
static const double centers0  [] = {0.2,  0.9};
static const double variance0 [] = {0.03, 0.03};

int GaussTestLikelihoodFunctionRJ::nint = 6;

//
// Constructor
//
GaussTestLikelihoodFunctionRJ::GaussTestLikelihoodFunctionRJ() : LikelihoodFunction()
{
  xmin  = -1.0;
  xmax  =  2.0;
  nbins = 400;
  N     = 40000;
  dx    = (xmax - xmin)/nbins;
  dim   = 2;

  cauchyData  = false;
  cauchyModel = false;
  point       = false;

  defaultModel();
}

GaussTestLikelihoodFunctionRJ::GaussTestLikelihoodFunctionRJ(int Nbins, int num) 
  : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  nbins = Nbins;
  N = num;
  dx = (xmax - xmin)/nbins;
  dim = 2;

  cauchyData  = false;
  cauchyModel = false;
  point       = false;

  defaultModel();
}


GaussTestLikelihoodFunctionRJ::GaussTestLikelihoodFunctionRJ(int num) 
  : LikelihoodFunction()
{
  xmin = -1.0;
  xmax =  2.0;
  N = num;
  dx = (xmax - xmin)/nbins;
  dim = 2;

  cauchyData  = false;
  cauchyModel = false;
  point       = true;

  defaultModel();
}

void GaussTestLikelihoodFunctionRJ::defaultModel()
{
  number   = number0;

  centers  = vector<double>(number);
  variance = vector<double>(number);
  weights  = vector<double>(number);

  for (unsigned i=0; i<number; i++) {
    centers[i]  = centers0[i];
    variance[i] = variance0[i];
    weights[i]  = weights0[i];
  }

}

void GaussTestLikelihoodFunctionRJ::newModel(string file)
{
  ifstream in(file.c_str());

  if (in) {

    int number1;

    in >> number1;

    if (in) {
    
      vector<double> centers1 (number1);
      vector<double> variance1(number1);
      vector<double> weights1 (number1);

      for (int i=0; i<number1; i++) {
	in >> weights1[i];
	in >> centers1[i];
	in >> variance1[i];
      }

      if (in) {
	weights  = weights1;
	centers  = centers1;
	variance = variance1;
	
	number   = number1;

	if (point)
	  makeSyntheticPointData();
	else
	  makeSyntheticData();
      }
      else {
	cerr << "Error reading data from <" << file << ">" << endl;
      }
    }

  }

}

void GaussTestLikelihoodFunctionRJ::makeSyntheticPointData()
{
  //
  // Initialize the point distribution ("fake data")
  //
  if (myid==0) {
				// For selecting components
    Uniform unit(0.0, 1.0, BIEgen); 
    Normal  norm(0.0, 1.0, BIEgen);
    vector<double> cum(number, 1.0);
    cum[0] = weights[0];
    for (unsigned c=1; c<number-1; c++) cum[c] = cum[c-1] + weights[c];


				// Generate data
    pdata = vector<double>(N);
    double xmin=1.0e20, xmax=-1.0e20;
    for (int n=0; n<N; n++) {
      double wgt = unit();
      for (unsigned c=0; c<number; c++) {
	if (wgt <= cum[c]) {	// Select the component . . .
	  if (cauchyData)
	    pdata[n] = centers[c] + sqrt(variance[c])*tan(M_PI*(unit()-0.5));
	  else
	    pdata[n] = centers[c] + sqrt(variance[c])*norm();
	  break;
	}
      }
      xmin = min<double>(pdata[n], xmin);
      xmax = max<double>(pdata[n], xmax);
    }
    cout << "Min=" << xmin << " Max=" << xmax << endl;

				// Send data everywhere
    MPI_Bcast(&pdata[0], N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  } else {
				// Get data from Node 0
    pdata = vector<double>(N);
    MPI_Bcast(&pdata[0], N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  }

  generated = true;
}



void GaussTestLikelihoodFunctionRJ::makeSyntheticData()
{
  //
  // Initialize the comparison distribution ("fake data")
  //
  double x1, x2;
  fdata = vector<double>(nbins, 0.0);
  for (unsigned c=0; c<number; c++) {
    for (int i=0; i<nbins; i++) {
      x1 = xmin + dx*i;
      x2 = xmin + dx*(i+1);
      if (cauchyData)
	fdata[i] += weights[c]*(  
				atan((x2 - centers[c])/sqrt(variance[c]) ) -
				atan((x1 - centers[c])/sqrt(variance[c]) )
				  ) * N / M_PI;
      else
	fdata[i] += weights[c]*(  
				erf( (x2 - centers[c])/sqrt(2.0*variance[c]) ) -
				erf( (x1 - centers[c])/sqrt(2.0*variance[c]) )
				  ) * 0.5 * N;
    }
  }

  double sanity = 0.0;
  for (int i=0; i<nbins; i++) sanity += fdata[i]/N;
  cout << "Bin normalizaton sanity check: expect=1, found=" << sanity << endl;

  for (int i=0; i<nbins; i++) fdata[i] = round(fdata[i]);

  generated = true;
}


void GaussTestLikelihoodFunctionRJ::SetDim(int n)
{
  switch (n) {
  case 1:
    dim = 1;
    break;
  case 2:
    dim = 2;
    break;
  default:
    dim = 2;
  }

}


double GaussTestLikelihoodFunctionRJ::LikeProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx)
{
  return LikeProbTwo(s);
}

double GaussTestLikelihoodFunctionRJ::LikeProbTwo(State *s)
{
  static bool firstime = true;
  double val = 0.0;

  unsigned model = floor((*s)[0]+0.01), icomp, ibeg;
  if (model==0) {
    ibeg  = 1;			// Parameters begin at index 1
    icomp = 1;			// One component
  } else {
    ibeg  = 1 + dim;		// Parameters begin at index 2 or 3
    icomp = 2;
  }
  
  ofstream out;
  if (firstime) out.open("test.out");

  if (point) {
    
    if (not generated) makeSyntheticPointData();

    for (int n=0; n<N; n++) {

      double tst = 0.0;

      for (unsigned c=0; c<icomp; c++) {
	double wght = 1.0, cen, var;
	if (icomp>1) wght = weights[c];

	switch (dim) {
	case 1:
	  cen = (*s)[ibeg+c];
	  if (cauchyModel) {
	    tst += wght / (sqrt(variance[c])*M_PI) / 
	      ( 1.0 + (pdata[n] - cen)*(pdata[n] - cen)/variance[c] );
	  } else {
	    tst += wght *
	      exp( -(pdata[n] - cen)*(pdata[n] - cen)
		   /(2.0*variance[c]) ) / sqrt(2.0*M_PI*variance[c]);
	  }
	  break;
	case 2:
	default:
	  cen = (*s)[ibeg+c*2];
	  var = (*s)[ibeg+c*2+1];
	  if (cauchyModel) {
	    tst += wght / (sqrt(var)*M_PI) / 
	      ( 1.0 + (pdata[n] - cen)*(pdata[n] - cen)/var );
	  } else {
	    tst += wght *
	      exp( -(pdata[n] - cen)*(pdata[n] - cen)/(2.0*var) ) / 
	      sqrt(2.0*M_PI*var);
	  }
	  break;
	}
      }

      tst = max<double>(0.0, tst);

      if (tst==0.0) {		// Sanity check
	
	throw ImpossibleStateException(__FILE__, __LINE__);
      
      } else {
	
	val += log(tst);

      }

    }

    if (firstime) {
      out.precision(14);
      for (int n=0; n<N; n++) out << setw(22) << pdata[n] << endl;
    }
    
  } else {
      
    if (not generated) makeSyntheticData();

    double x1, x2, tst;

    for (int i=0; i<nbins; i++) {
      tst = 0.0;
      x1  = xmin + dx*i;
      x2  = xmin + dx*(i+1);

      for (unsigned c=0; c<icomp; c++) {

	double wght = 1.0, cen, var;
	if (icomp>1) wght = weights[c];

	switch (dim) {
	case 1:
	  cen = (*s)[ibeg+c];
	  if (cauchyModel) {
	    tst += wght / M_PI *
	      (  
	       atan( (x2 - cen)/sqrt(variance[c]) ) -
	       atan( (x1 - cen)/sqrt(variance[c]) )
		 ) * N;
	  } else {
	    tst += wght *
	      (  
	       erf( (x2 - cen)/sqrt(2.0*variance[c]) ) -
	       erf( (x1 - cen)/sqrt(2.0*variance[c]) )
		 ) * 0.5 * N;
	  }
	  break;
	case 2:
	default:
	  cen = (*s)[ibeg+c*2];
	  var = (*s)[ibeg+c*2+1];
	  if (cauchyModel) {
	    tst += wght / M_PI *
	      (  
	       atan( (x2 - cen)/sqrt(var) ) -
	       atan( (x1 - cen)/sqrt(var) )
		 ) * N;
	  } else {
	    tst += wght *
	      (  
	       erf( (x2 - cen)/sqrt(2.0*var) ) -
	       erf( (x1 - cen)/sqrt(2.0*var) )
		 ) * 0.5 * N;
	  }
	  break;
	}
      }
      
      tst = max<double>(0.0, tst);

      if (firstime)
	out << setw(3)  << setw(18) << xmin + dx*(0.5+i) 
	    << setw(18) << fdata[i] 
	    << setw(18) << tst
	    << endl;
      
      if (tst==0) {		// Sanity check
	
	if (fdata[i]!=0) {
	  throw ImpossibleStateException(__FILE__, __LINE__);
	}
	
      } else {
	
	val += log(tst)*fdata[i] - tst - lgamma(1.0+fdata[i]);
	
      }
    }
  }
    

  if (firstime) {
    firstime = false;
    switch (dim) {
    case 1:
      out << "# " 
	  << setw(14) << "Position"
	  << endl;
      out << "# " << setfill('-')
	  << setw(14) << "+"
	  << setfill(' ') << endl;
      out << "# " 
	  << setw(14) << (*s)[0]
	  << endl;
      out << "#" << endl;
      break;
    case 2:
    default:
      out << "# " 
	  << setw(14) << "Position"
	  << setw(14) << "Variance"
	  << endl;
      out << "# " << setfill('-')
	  << setw(14) << "+"
	  << setw(14) << "+"
	  << setfill(' ') << endl;
      out << "# " 
	  << setw(14) << (*s)[0]
	  << setw(14) << (*s)[1]
	  << endl;
      out << "#" << endl;
    }
    if (point)
      out << "# Point model" << endl;
    else
      out << "# Binned model" << endl;
  }

  return val;
}

double GaussTestLikelihoodFunctionRJ::CumuProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx, Fct1dPtr f)
{
  return CumuProbTwo(s, f);
}


double GaussTestLikelihoodFunctionRJ::CumuProbTwo(State *s, Fct1dPtr f)
{
  double val = 0.0, wght, cen, var;

  unsigned model = floor((*s)[0]+0.01), icomp, ibeg;
  if (model) {
    ibeg  = 1;
    icomp = 1;
  } else {
    ibeg = 1 + dim;
    icomp = 2;
  }
  
  if (point) {
    
    if (not generated) makeSyntheticPointData();

    for (int n=0; n<N; n++) {

      double tst = 0.0;

      for (unsigned c=0; c<icomp; c++) {
	if (icomp>1) wght = weights[c];
	else         wght = 1.0;

	switch (dim) {
	case 1:
	  cen = (*s)[ibeg+c];
	  if (cauchyModel) {
	    tst += wght *
	      (0.5 + atan((pdata[n] - cen)/sqrt(variance[c]))/M_PI);
	  } else {
	    tst += wght * 0.5 *
	      (1.0 + erf((pdata[n] - cen)/sqrt(2.0*variance[c])));
	  }
	  break;
	  
	case 2:
	default:
	  cen = (*s)[ibeg+c*2];
	  var = (*s)[ibeg+c*2+1];
	  if (cauchyModel) {
	    tst += wght *
	      (0.5 + atan((pdata[n] - cen)/sqrt(var))/M_PI);
	    
	  } else {
	    tst += wght * 0.5 *
	      (1.0 + erf((pdata[n] - cen)/sqrt(2.0*var)));
	  }
	  break;
	}
      }

      tst = f->G(max<double>(0.0, min<double>(1.0, tst)));

      if (tst<=0.0) {		// Sanity check
	
	val += -INFINITY;

      } else {
	
	val += log(tst);

      }

    }

  } else {
      
    if (not generated) makeSyntheticData();

    double x1, x, tst, J;
    if (lq == 0) lq = LegeQPtr(new LegeQuad(nint));

    for (int i=0; i<nbins; i++) {
      tst = 0.0;
      x1  = xmin + dx*i;

      for (int j=1; j<=nint; j++) {

	J = lq->weight(j);
	x = x1 + dx*lq->knot(j);

	for (unsigned c=0; c<icomp; c++) {
	  if (icomp>1) wght = weights[c];
	  else         wght = 1.0;

	  switch (dim) {
	  case 1:
	    cen = (*s)[ibeg+c];
	    if (cauchyModel) {
	      tst += J * wght *
		(0.5 + atan((x - cen)/sqrt(variance[c]))/M_PI);
	    } else {
	      tst += J * wght * 0.5 *
		(1.0 + erf((x - cen)/sqrt(2.0*variance[c])));
	    }
	    break;
	  
	  case 2:
	  default:
	    cen = (*s)[ibeg+c*2];
	    var = (*s)[ibeg+c*2+1];
	    if (cauchyModel) {
	      tst += J * wght *
		(0.5 + atan((x - cen)/sqrt(var))/M_PI);
	      
	    } else {
	      tst += J * wght * 0.5 *
		(1.0 + erf((x - cen)/sqrt(2.0*var)));
	    }
	    break;
	  }
	}
	
      }
				// Call the map function
      tst = f->G(tst);
      
      //
      // Take the log
      //      
      if (tst<=0) {
	
	val += -fdata[i]*INFINITY;
	
      } else {
	
	val += fdata[i]*log(tst);
	
      }
    }
  }
    
  return val;
}

//
// Used to label output
//
const std::string GaussTestLikelihoodFunctionRJ::ParameterDescription(int i)
{
  string ret;

  switch(i) {
  case 0:
    ret = "Position\0";
    break;
  case 1:
    ret = "Variance\0";
    break;
  default:
    ret = "**Error**\0";
    break;
  }

  return ret;
}


