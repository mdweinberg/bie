// This may look like C code, but it is really -*- C++ -*-

#include <gaussQ.h>
#include <LegeIntegration.h>
#include <BIEMutex.h>
#include <BIEmpi.h>
#include <LikelihoodComputation.h> 

BIE_CLASS_EXPORT_IMPLEMENT(BIE::LegeIntegration)

using namespace BIE;

LegeIntegration::LegeIntegration(int nu, int nv)
{
  U = LegeQPtr(new LegeQuad(nu));
  V = LegeQPtr(new LegeQuad(nv));
  nU = nu;
  nV = nv;
}

void LegeIntegration::ParameterChange(int nu, int nv)
{
  U = LegeQPtr(new LegeQuad(nu));
  V = LegeQPtr(new LegeQuad(nv));
  nU = nu;
  nV = nv;
}

double LegeIntegration::NormValue(Model* _model, Tile* _tile, 
				  SampleDistribution* _dist)
{
  double ans = 0.0;
  
  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      ans += _model->NormEval(_tile->X(U->knot(ii), V->knot(jj)), 
			      _tile->Y(U->knot(ii), V->knot(jj)),
			      _dist) *
	_tile->measure(U->knot(ii), V->knot(jj))
	* U->weight(ii) * V->weight(jj);
    }
  }

  return ans;
}

vector<double> LegeIntegration::Value(Model* _model, Tile* _tile,
				      SampleDistribution* _dist)
{
  vector<double> ans(_dist->numberData(), 0.0);
  vector<double> z(_dist->numberData());
  double factor;
  
  for (int jj=1; jj<=nV; jj++) {
    for (int ii=1; ii<=nU; ii++) {

      factor = _tile->measure(U->knot(ii), V->knot(jj))
	* U->weight(ii) * V->weight(jj);

      z = _model->Evaluate( _tile->X(U->knot(ii), V->knot(jj)), 
			    _tile->Y(U->knot(ii), V->knot(jj)), _dist);
      
      for (int j=0; j<_dist->numberData(); j++) {
	ans[j] += z[j] * factor;
      }
    }
  }
  
  return ans;
}




/**
 * Threaded integrator for tile-point parallelism
 * 
 */


double LegeIntegration::NormValue(LikelihoodComputation* p_likelihoodComputation,
				  Model* _model,
				  Tile* _tile, 
				  SampleDistribution* _dist)
{

  vector<double> x, y;
  
  // create list of points
  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      x.push_back(_tile->X(U->knot(ii), V->knot(jj)));
      y.push_back(_tile->Y(U->knot(ii), V->knot(jj)));

    }
  }
  

  // Evaluate list and wait for results. for each point sent in, get a distribution back
  int numPoints = x.size();

  vector< double > evaluations(numPoints);

  p_likelihoodComputation->normEvaluateList(_model, _tile,  x, y, _dist, evaluations);


  int pointNumber = 0;
  double ans = 0.0;
  
  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      ans += evaluations[pointNumber] * _tile->measure(U->knot(ii), V->knot(jj))
	  * U->weight(ii) * V->weight(jj);

      pointNumber++;
    }
  }

  return ans;
}



vector<double> LegeIntegration::Value(LikelihoodComputation* p_likelihoodComputation,
				      Model* _model,
                                      Tile* _tile,
				      SampleDistribution* _dist)
{

  vector<double> ans(_dist->numberData(), 0.0);
  vector<double> z(_dist->numberData());
  vector<double> x, y;
  double factor;
  
  
  // create list of points
  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      x.push_back(_tile->X(U->knot(ii), V->knot(jj)));
      y.push_back(_tile->Y(U->knot(ii), V->knot(jj)));

    }
  }
  

  // Evaluate list and wait for results. for each point sent in, get a distribution back
  int numPoints = x.size();

  vector< vector<double> > evaluations(numPoints, vector<double>(_dist->numberData()));


  p_likelihoodComputation->modelEvaluateList(_model, _tile,  x, y, _dist, evaluations);


  // compute integral from set of model evaluations
  int pointNumber = 0;
  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {
      factor = _tile->measure(U->knot(ii), V->knot(jj))
	* U->weight(ii) * V->weight(jj);

      for (int j=0; j< _dist->numberData(); j++) {
	ans[j] += evaluations[pointNumber][j] * factor;
      }

      pointNumber++;

    }
  }


#if CHECK_INTEGRATOR_SANITY
  // The sanity clause. For debugging only
  z = Value(_model, _tile, _dist);  // call single threaded integrator
  for (int j=0; j<_dist->numberData(); j++) {
    if (ans[j] != z[j]){
      cout << "Threaded and non-threaded integrators disagree!\n";
      cout << "Threaded["<< j << "] = " << ans[j] << "Nonthreaded = " << z[j] << "\n";
    }
  }
#endif
  return ans;

}










double LegeIntegration::NormValue(Model* _model, Tile* _tile,
				  SampleDistribution* _dist,
				  int irank, int nrank)
{
  double ans = 0.0;
  
  int counter=0;

  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      if ( counter++ % nrank == irank) {

	ans += _model->NormEval(_tile->X(U->knot(ii), V->knot(jj)), 
				_tile->Y(U->knot(ii), V->knot(jj)),
				_dist) *
	  _tile->measure(U->knot(ii), V->knot(jj))
	  * U->weight(ii) * V->weight(jj);
      }
    }
  }

  return ans;
}

vector<double> LegeIntegration::Value(Model* _model, Tile* _tile,
				      SampleDistribution* _dist,
				      int irank, int nrank, MPI_Comm& comm)
{
  vector<double> z(_dist->numberData());
  double *ans0 = new double [_dist->numberData()];
  double *ans1 = new double [_dist->numberData()];
  double factor;
  
  for (int k=0; k<_dist->numberData(); k++) ans1[k] = 0.0;

  int counter = 0;

  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      if ( counter++ % nrank == irank) {
	
	factor = _tile->measure(U->knot(ii), V->knot(jj))
	  * U->weight(ii) * V->weight(jj);

	z = _model->Evaluate( _tile->X(U->knot(ii), V->knot(jj)), 
			      _tile->Y(U->knot(ii), V->knot(jj)), _dist);

	for (int j=0; j<_dist->numberData(); j++) 
	  ans1[j] += z[j] * factor;
      }
    }
  }
    
  vector<double> ans;

  if (nrank>1) {
    ++MPIMutex;
    MPI_Reduce(ans1, ans0, _dist->numberData(), MPI_DOUBLE, MPI_SUM, 0, comm);
    --MPIMutex;

    if (irank==0)
      ans = vector<double>(ans0, ans0+_dist->numberData());
  } else {
    ans = vector<double>(ans1, ans1+_dist->numberData());
  }

  delete [] ans0;
  delete [] ans1;

  return ans;
}

