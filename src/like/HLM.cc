#include <new>
#include <sstream>
#include <typeinfo>

using namespace std;

#include <HLM.h>
#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::HLM)

using namespace BIE;

// Constructor
HLM::HLM(string file, int M)
{
  switch (M) {
  case 1:
    mod    = 1;			// Han and Carlin: Model 1
    fixdim = 10;
    break;
  case 2:
    mod = 2;			// Han and Carlin: Model 2
    fixdim = 7;
    break;
  default:
    {
      ostringstream out;
      out << "Model parameter must be 1 or 2; you entered: " << M;
      throw BadParameterException("HLM", out.str(),
				  __FILE__, __LINE__);
    }
  }

  //
  // Measurement times
  //
  times.push_back(0);
  times.push_back(2);
  times.push_back(6);
  times.push_back(12);
  times.push_back(18);

  //
  // Open and read data
  //
  ifstream in(file.c_str());
  if (in) {
    const unsigned lsiz = 80;	// Line size
    char line[lsiz];
    bool ok = false;

    subdim = 0;			// Count number of variable effects
    while (in.good()) {
      in.getline(line, lsiz);
      if (in.eof()) break;
      string rec(line);
      if (ok) {
	int i, c;
	elemHLM e;
	istringstream ins(line);
	ins >> i;
	ins >> e.drug;
	ins >> e.AIDS;
	for (unsigned j=0; j<5; j++) {
	  ins >> c;
	  if (c<9999) {
	    e.time.push_back(times[j]);
	    e.numb.push_back(c);
	    e.indx.push_back(j+1);
	    subdim++;
	  }
	}
	data.push_back(e);
      }
      if (rec.find("*****")) {
	in.getline(line, lsiz);
	ok = true;
      }
    }

  } else {
    throw FileOpenException(file, in.rdstate(), __FILE__, __LINE__);
  }

}


// Used to label output
const std::string HLM::ParameterDescription(int i)
{
  const char *labs[]  = {"Intrcpt", "Slope", "Change"};
  const char *group[] = {"All", "Drug", "AIDS"};
  ostringstream sout;

  if (i==0) sout << "Var";
  else if (i<fixdim) {
    if (mod==1) {
      int j= i-1 % 3;
      int k = (i-1)/3;
      sout << labs[k] << "(" << group[j] << ")";
    } else {
      int j= i-1 % 2;
      int k = (i-1)/2;
      sout << labs[k] << "(" << group[j] << ")";
    }
  } else {
    ostringstream sout;
    sout << "Beta[" << i-fixdim+1 << "]";
  }

  return sout.str();
}

double HLM::LocalLikelihood(State *s)
{
  double z2 = 0.0, z, y;

  if (mod==1) {

    int sc = fixdim;

    for (unsigned i=0; i<data.size(); i++) {
      z = 0;
      for (unsigned j=0; j<data[i].time.size(); j++) {
	y = 
	  (*s)[1] + 
	  (*s)[2]*data[i].time[j] + 
	  (*s)[3]*max<int>(0, data[i].time[j]-2);
	if (data[i].drug) y += 
		      (*s)[4] + 
		      (*s)[5]*data[i].time[j] + 
		      (*s)[6]*max<int>(0, data[i].time[j]-2);
	if (data[i].AIDS) y += 
		      (*s)[7] + 
		      (*s)[8]*data[i].time[j] + 
		      (*s)[9]*max<int>(0, data[i].time[j]-2);
	y +=
	  (*s)[sc] + 
	  (*s)[sc+1]*data[i].time[j] + 
	  (*s)[sc+2]*max<int>(0, data[i].time[j]-2);

	sc += 3;

	y -= data[i].numb[j];

	z += y*y;
      }
      
      z2 += 0.5*z/(*s)[0];
    }
      
  } else {

    int sc = fixdim;

    for (unsigned i=0; i<data.size(); i++) {
      z = 0;
      for (unsigned j=0; j<data[i].time.size(); j++) {
	y = 
	  (*s)[1] + 
	  (*s)[2]*data[i].time[j];
	if (data[i].drug) y += 
		      (*s)[3] + 
		      (*s)[4]*data[i].time[j];
	if (data[i].AIDS) y += 
		      (*s)[5] + 
		      (*s)[6]*data[i].time[j];
	y +=
	  (*s)[sc] + 
	  (*s)[sc+1]*data[i].time[j];

	sc += 2;

	y -= data[i].numb[j];

	z += y*y;
      }
      
      z2 += 0.5*z/(*s)[0];
    }
      
  }

  return -z2;
}
