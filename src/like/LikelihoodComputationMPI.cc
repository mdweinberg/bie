// This may look like C code, but it is really -*- C++ -*-

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

using namespace std;

#include <gvariable.h>
#include <BIEdebug.h>
#include <BIEException.h>
#include <LikelihoodComputationMPI.h>
#include <PointTessellation.h>

#include <BIEMutex.h>
#include <gfunction.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif

BIE_CLASS_EXPORT_IMPLEMENT(BIE::LikelihoodComputationMPI)

using namespace BIE;

// #undef USE_FACTORIAL
#define USE_FACTORIAL

#ifdef DEBUG
static Mutex IOMutex;
static ofstream oerr;
#endif

// -1 turns this off (until the counter wraps)
// 0 load balances on every step
int LikelihoodComputationMPI::NLOAD = -1;

// Rates global over all instances
double LikelihoodComputationMPI::beg;
double LikelihoodComputationMPI::end;

#define PRIOR_FRONTIER 64
#define CURRENT_FRONTIER 128
#define STEP_COMPLETE 256

LikelihoodComputationMPI::LikelihoodComputationMPI(StateInfo *si)
{
  // Initialize the working state
  sb      = State(si);

  // For MPI
  size    = si->Ntot;
  buff    = new double [size];
  sb      = vector<double>(size);
  acctime = 0.0;

  // Initial values
  beg = (double)myid/numprocs;
  end = (double)(myid+1)/numprocs;
  
  beglist = vector<double>(numprocs);
  endlist = vector<double>(numprocs);

  MPL_accum_time = 0.0;
  MPL_last_time  = 0.0;
  counter        = 0;
  
  parallel_type  = 0;
  
  if (myid==0) rate = vector<double>(numprocs, 0);
  
  nsub           = 8;
  parent_id      = 0;
  
#ifdef DEBUG
  ostringstream file;
  file << "debug." << myid;
  oerr.open(file.str().c_str(), ios::out|ios::app);
#endif

}

LikelihoodComputationMPI::~LikelihoodComputationMPI(void)
{
  delete [] buff;
#ifdef DEBUG
  oerr.close();
#endif
}

void LikelihoodComputationMPI::MPL_reset_timer(void)
{
  MPL_accum_time=0.0;
}

void LikelihoodComputationMPI::MPL_start_timer(void)
{
  ++MPIMutex;
  MPL_last_time = MPI_Wtime();
  --MPIMutex;
}

void LikelihoodComputationMPI::MPL_stop_timer(void)
{
  ++MPIMutex;
  double curtime = MPI_Wtime();
  --MPIMutex;
  
  MPL_accum_time += curtime - MPL_last_time;
  MPL_last_time = curtime;
}

double LikelihoodComputationMPI::MPL_read_timer(int reset)
{
  double save = MPL_accum_time;
  if (reset) MPL_accum_time = 0.0;
  return save;
}

void LikelihoodComputationMPI::load_balance(void)
{
  MPI_Status status;
  int go = 1;
  int mcnt = 0;
  double ttime, mean;
  
  if (myid == 0) {
    // Send signal to load balance
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
      --MPIMutex;
      // Receive times from slaves
    }
    
    rate[0] = mean = MPL_read_timer(1);
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Recv(&ttime, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 14, MPI_COMM_WORLD, &status);
      --MPIMutex;
      if (ttime>1.0e-4) {
	mean += ttime;
	mcnt++;
      }
      
      rate[status.MPI_SOURCE] = ttime;
      
    }
    mean /= mcnt;
    
#ifdef DEBUG
    cerr << "\nTimes:\n";
    for (int i=0; i<numprocs; i++)
      cerr << i << rate[i] << "\n";
    cerr << "\n";
#endif
    
    for (int i=0; i<numprocs; i++) {
      if (rate[i] < 1.0e-4)
	rate[i] = 0.1/mean;
      else
	rate[i] = 1.0/rate[i];
    }
    for (int i=1; i<numprocs; i++) rate[i] += rate[i-1];
    for (int i=0; i<numprocs; i++) rate[i] /= rate[numprocs-1];
    
    beg = 0.0;
    end = rate[0];
    
#ifdef DEBUG
    cerr << "\nFractions:\n";
    cerr << "0>" << rate[0]<< " 0.0 " << rate[0] << "\n";
    for (int i=1; i<numprocs; i++){
      cerr << i << "> " << rate[i]-rate[i-1] << " " << rate[i-1]
           <<  " " << rate[i] << "\n";
    }
    cerr << "\n";
#endif
    
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Send(&rate[i-1], 1, MPI_DOUBLE, i, 15, MPI_COMM_WORLD);
      MPI_Send(&rate[i], 1, MPI_DOUBLE, i, 16, MPI_COMM_WORLD);
      --MPIMutex;
    }
    
    // Counts number of likelihood routine calls
    counter = 0;		// Reset
    
  } else {
    acctime = MPL_read_timer(1);
    ++MPIMutex;
    MPI_Send(&acctime, 1, MPI_DOUBLE, 0, 14, MPI_COMM_WORLD);
    
    MPI_Recv(&beg, 1, MPI_DOUBLE, 0, 15, MPI_COMM_WORLD, &status);
    MPI_Recv(&end, 1, MPI_DOUBLE, 0, 16, MPI_COMM_WORLD, &status);
    --MPIMutex;
  }
  
#ifdef DEBUG
  cerr << "Process " << myid << ": received " << beg << "  " << end << "\n";
#endif
}

void LikelihoodComputationMPI::TileGranularity()
{
  parallel_type = 0;
}

void LikelihoodComputationMPI::IntegrationGranularity()
{
  parallel_type = 1;
}

void LikelihoodComputationMPI::Granularity(int itype)
{
  // Parse parallel type
  switch (itype) {
  case 0:
    parallel_type = 0; break;
  case 1:
    parallel_type = 1; break;
  default:
    {
      ostringstream out;
      out << "No such parallel type, " << itype;
      string message(out.str());
      
      throw BIEException("LikelihoodComputationMPI:", message, __FILE__, __LINE__);
    }
    
  }
  
}


void LikelihoodComputationMPI::enslave_worker(void * arg)
{
  parent_id = *((int*)arg);
  parent_id = 0;
  
  MPI_Status status;
  int go;
  
#ifdef DEBUG
  ++IOMutex;
  oerr << "Process " << myid << ": worker enslaved\n";
  --IOMutex;
#endif
  
  bool keep_going, enslaved = true;
  unsigned MM;
  int indx;

  while (enslaved) {		// Wait for commands
    
#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": LikelihoodComputationMPI waiting for command . . . "
	 << endl << flush;
    --IOMutex;
#endif


    childSem.wait();		// Wait to do a step

#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": LikelihoodComputationMPI worker gets run signal\n";
    --IOMutex;
#endif
    
    keep_going = true;

    while (keep_going) {

      ++MPIMutex;
      MPI_Recv(&go, 1, MPI_INT, parent_id, 11, MPI_COMM_WORLD, &status);
      --MPIMutex;
    
#ifdef DEBUG_TESSTOOL
      ferr << "LikelihoodComputationMPI::enslave_worker " << myid 
	   << " go=" << go << endl;
#endif
    
#ifdef DEBUG
      ++IOMutex;
      oerr << "Process " << myid << ": LikelihoodComputationMPI got command=" << go << endl;
      --IOMutex;
#endif
    
      // Is this a command to adjust the frontier?
      // 
      if (go & (PRIOR_FRONTIER | CURRENT_FRONTIER) )  {
      
	if (go & PRIOR_FRONTIER)   setPriorFrontier();
	if (go & CURRENT_FRONTIER) setCurrentFrontier();
      
      } else if (go & STEP_COMPLETE) {
      
#ifdef DEBUG
	++IOMutex;
	oerr << "Process " << myid << ": parent about to be freed"
	     << endl << flush;
	--IOMutex;
#endif
	keep_going = false;

	parentSem.post();	// Parent thread resumes

      } else {
      
	go = decode_command(go);
      
	// Orders to exit
	if (go == 0) {
	
	  keep_going = false;
	  enslaved = false;

	} else if (go == 1) {
	
	  load_balance();
	  
	} else if (go == 2) {
	
	  ++MPIMutex;
	  MPI_Recv(buff, size, MPI_DOUBLE, parent_id, 12, MPI_COMM_WORLD, 
		   &status);
	  MPI_Recv(&indx, 1, MPI_INT, parent_id, 17, MPI_COMM_WORLD,
		   &status);
	  MPI_Recv(&MM, 1, MPI_UNSIGNED, parent_id, 18, MPI_COMM_WORLD,
		   &status);
	  --MPIMutex;
	  // Unpack buffer
	  //
	  sb.Reset(MM);
	  BufferToState(sb, buff, size);
	
#ifdef DEBUG
	  ++IOMutex;
	  oerr << "Process " << myid << ": buffer received, parallel_type="
	       << parallel_type << endl << flush;
	  --IOMutex;
#endif
	  // Compute the likelihood
	  // 
	  try {
	    compute_likelihood_MPI(sb, indx);
	  } 
	  catch (ImpossibleStateException &e) {
	    cerr << e.getErrorMessage() << endl;
	    // Keep going ... this exception will be handled by the root process
	  }

	} else {
	  ostringstream out;
	  out << "no such command go=" << go;
	  throw BIEException("UndefinedCommand", out.str(), __FILE__, __LINE__);
	}

      }

    }

  }

}

LikeState LikelihoodComputationMPI::compute_likelihood_MPI(State& sb, int indx)
{
  LikeState ans;
  
  if (parallel_type==0) ans = compute_likelihood_MPI_tile(sb, indx);
  if (parallel_type==1) ans = compute_likelihood_MPI_intg(sb, indx);
  
  return ans;
}

void LikelihoodComputationMPI::get_working(State& s, int indx, int pflag)
{
  int go=2;
  
#ifdef DEBUG_TESSTOOL
  ferr << " LikelihoodComputationMPI::get_working myid=" << myid << " go=" << go << endl;
#endif
  
  
  if (pflag) set_frontier(1);
  
  if (!pflag) {
    TessToolSynchronize();
    go = encode_command(go);
  }
  
  if (myid) {
    cerr << "Process " << myid << ": in get_working . . . oops!\n";
#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": in get_working . . . oops!\n";
    --IOMutex;
#endif
  }
  // Pack buffer
  unsigned MM = s.M();
  StateToBuffer(s, buff, size);

  for (int i=1; i<numprocs; i++) {
    // Send signal to compute
    ++MPIMutex;
    MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
    
    // Send buffer
    MPI_Send(buff, size, MPI_DOUBLE, i, 12, MPI_COMM_WORLD);
    MPI_Send(&indx, 1, MPI_INT, i, 17, MPI_COMM_WORLD);
    MPI_Send(&MM, 1, MPI_UNSIGNED, i, 18, MPI_COMM_WORLD);
    --MPIMutex;
    
#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": sent go=" << go << " to #" << i
	 << endl << flush;
    --IOMutex;
#endif
  }
  
  // for performing pause/resume on root node
  if (!pflag) decode_command(go);
}

void LikelihoodComputationMPI::free_workers(void)
{
  if (myid==0) {
    int stop=0;
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Send(&stop, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
      --MPIMutex;
    }
    
#ifdef DEBUG
    cerr << "Process " << myid << ": workers sent the quit signal\n";
#endif
  } else {
				// Let the captured processes run
				// so that they can free themselves from 
				// their execution loop and exit
    childSem.post();
  }
      
  
}

/// Determine liklihood for previous state.
double LikelihoodComputationMPI::LikelihoodPrevious(State &s, int indx)
{
				// Don't do anything at the top level
  if (GetSimulation()->priorFrontier() == 0) return 0.0;
  
  if (myid) {

    childSem.post();		// Release the workers

#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": child released for one step,"
	 << " waiting with counts=" 
	 << MPIMutexEnter << ", " << MPIMutexLeave << endl;
    --IOMutex;
#endif

    parentSem.wait();		// Wait until workers are done
    
#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": parent leaving with counts="
	 << MPIMutexEnter << ", " << MPIMutexLeave << endl;
    --IOMutex;
#endif

    return 0.0;
  }
  
  get_working(s, indx, 1);
  
  // Install the old frontier
  setPriorFrontier();
  
  LikeState ans = compute_likelihood_MPI(s, indx);
  
  // Restore the current frontier
  setCurrentFrontier();
  set_frontier(0);
  
  // Step is DONE
  step_complete();

  // Throw exception for bad state
  if (!ans.good) throw ImpossibleStateException(__FILE__, __LINE__);


  return ans.value;
}


void LikelihoodComputationMPI::set_frontier(int pflag)
{
  int go = CURRENT_FRONTIER;
  if (pflag) go = PRIOR_FRONTIER;
  
  for (int i=1; i<numprocs; i++) {
    ++MPIMutex;
    MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
    --MPIMutex;
  }
}


void LikelihoodComputationMPI::step_complete()
{
  int go = STEP_COMPLETE;
  
  for (int i=1; i<numprocs; i++) {
    ++MPIMutex;
    MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
    --MPIMutex;
  }
}


double LikelihoodComputationMPI::Likelihood(State& s, int indx)
{
  if (myid) {

    childSem.post();		// Let the workers do something

#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": child released for one step,"
	 << " waiting with counts=" 
	 << MPIMutexEnter << ", " << MPIMutexLeave << endl;
    --IOMutex;
#endif

    parentSem.wait();		// Wait until work is done

#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": parent leaving with counts="
	 << MPIMutexEnter << ", " << MPIMutexLeave << endl;
    --IOMutex;
#endif

    return 0.0;
  }
  
  if (counter++ == NLOAD) load_balance();
  
  likelihood_count++;
  // DEBUG showstates
  if (debugflags & showstates) GetSimulation()->ReportState();
  
#ifdef DEBUG
  ++IOMutex;
  oerr << "Process " << myid << ": about to call get working"
       << endl << flush;
  --IOMutex;
#endif
  // Put slaves to work
  get_working(s, indx, 0);
  
  // Get final result
  LikeState ans = compute_likelihood_MPI(s, indx);

  // Signal that this step is done!
  step_complete();

  // Throw exception for bad state
  if (!ans.good) throw ImpossibleStateException(__FILE__, __LINE__);


  return ans.value;
}


LikeState LikelihoodComputationMPI::compute_likelihood_MPI_tile
(State& s, int indx)
{
  LikeState ret = {0.0, true};

  double norm0=0.0, norm=0.0;
  int bval1=0, bval=0;
  double val=0.0;
  int ibeg, iend;
  Tile *t;
  SampleDistribution *sd;
  
  // copy some stuff from the simulation
  Model* _model          = GetModel();
  BaseDataTree* _dist    = GetBaseDataTree();
  Integration* _intgr    = GetIntegration();
  LikelihoodFunction* LP = GetLikeProb();
  
  int nitems = _dist->NumberItems();
  vector<double> z;
  
  
  _model->Initialize(s);	// E.g. normalization . . .
  

  ibeg = (int)(beg*nitems + 0.5);
  iend = (int)(end*nitems + 0.5);
  
				// Compute model normalzation
				// 
  if (dynamic_cast<PointTessellation*>(_dist->GetTessellation())) {

    MPL_start_timer();		// Begin timing . . . 
    
    double xmin, xmax, ymin, ymax;
    _dist->GetTessellation()->GetLimits(xmin, xmax, ymin, ymax);
    norm0 = _model->NormEval(xmin, xmax, ymin, ymax, false); 

    MPL_stop_timer();		// Stop timing

  } else {

    if (ibeg<iend) {
    
      MPL_start_timer();		// Begin timing . . . 
    
    
      _dist->Reset();
      for (int i=0; i<ibeg; i++) _dist->Next();
      _model->ResetCache();
      for (int i=ibeg; i<iend; i++) {
	norm += _intgr->NormValue(_model, 
				  _dist->CurrentTile(), 
				  _dist->CurrentItem());
	_dist->Next();
      }
    }
    
    MPL_stop_timer();		// Stop timing
    
    ++MPIMutex;
    MPI_Allreduce(&norm, &norm0, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    --MPIMutex;

  }

  // Bad state
  if (norm0<=0.0) {

    // Print it out
    if (debugflags & badcell) {
      ostream cout(checkTable("debug_output"));
      
      cout << "Process " << myid << ": badstate, norm=" << norm0 << ": ";
      for (unsigned int i=0; i<s.size(); i++) cout << setw(12) << s[i];
      cout << "\n";
    }
    
    bval1 = 1;
  }

  // Don't bother computing likelihood for
  // bad state
  if (ibeg<iend && norm0>0.0) {
    
    // Temporary debug line
    // if (myid==0) cout << "Norm=" << norm0 << endl;
    
    // Compute log likelihood
    _dist->Reset();
    for (int i=0; i<ibeg; i++) _dist->Next();
    _model->ResetCache();
    
    if (sampleNext==1) {
      InitializeTessToolTileDump();
#ifdef DEBUG_TESSTOOL
      ferr << "Process " << myid << ": tesstool tiles ibeg=" 
	   << ibeg << " iend=" << iend << endl;
#endif
    }
    
    for (int i=ibeg; i<iend; i++) {
      
      t  = _dist->CurrentTile();
      sd = _dist->CurrentItem();
      
      z  = _intgr->Value(_model, t, sd);
      
      try {
	val += LP->LikeProb(z, sd, _dist->Total()/norm0, t, &s, indx);
      }
      catch (ImpossibleStateException &e) {
	bval1 = 1;
      }
      
      // Tile diagnostics 
      // (NB: this is the place for TessTool . . .)
      if (DEBUG_LIKE) {
	vector<double> zz = z;
	for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm0;
	TileDump(t, sd, zz);
      }

      if (sampleNext==1) {
	vector<double> zz = z;
	for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm0;
	TessToolTileDump(t, sd, zz);
      }
      
      _dist->Next();
    }
    
    if (sampleNext==1) {
      sampleNext=0;
      FinalizeTessToolTileDump();
    }
    
    MPL_stop_timer();		// Stop timing
  } 
  else if (sampleNext==1) {
    InitializeTessToolTileDump();
    FinalizeTessToolTileDump();
    sampleNext=0;
  }
  
  ++MPIMutex;
  MPI_Reduce(&val, &ret.value, 1, MPI_DOUBLE, MPI_SUM, parent_id, MPI_COMM_WORLD);
  MPI_Reduce(&bval1, &bval, 1, MPI_INT, MPI_SUM, parent_id, MPI_COMM_WORLD);
  --MPIMutex;
  
  ret.good = (bval==0) ? true : false;

  DEBUG_LIKE = false;
  
  return ret;
}

LikeState LikelihoodComputationMPI::compute_likelihood_MPI_intg
(State& s, int indx)
{
  LikeState ret = {0.0, true};

  ostream cout(checkTable("simulation_output"));
  
  double norm0=0.0, norm=0.0;
  int bval1=0, bval=0;
  double val=0.0;
  Tile *t;
  SampleDistribution *sd;
  
  // copy some stuff from the simulation for notational convenience
  Model* _model          = GetModel();
  BaseDataTree* _dist    = GetBaseDataTree();
  Integration* _intgr    = GetIntegration();
  LikelihoodFunction* LP = GetLikeProb();
  
  int nitems = _dist->NumberItems();
  vector<double> z;
  
  
  _model->Initialize(s);	// E.g. normalization . . .
  
  
				// Interaction list
  int irank, nl;
  typedef vector<int> ivec;
  static vector<ivec> intlist;
  static vector<int> ranklist;
  
  // Temporary allocation/reallocation
  // Move to separate member function
  static int nitems0 = 0;
  static MPI_Group world_group;
  static MPI_Comm *comm = NULL;
  static MPI_Group *group = NULL;
  
  // Reallocate?
  if (nitems0 != nitems) {
    
#ifdef DEBUG
    cout << "LikelihoodComputationMPI::intg: nitems=" << nitems
	 << " nitems0=" << nitems0 << endl;
#endif
    
    ++MPIMutex;
    
    if (nitems0) {
      for (int i=0; i<nitems0; i++) {
	if (comm[i])  MPI_Comm_free(&comm[i]);
	if (group[i]) MPI_Group_free(&group[i]);
      }
      delete [] comm;
      delete [] group;
    } else 
      MPI_Comm_group(MPI_COMM_WORLD, &world_group);
    
    // Set nsub
    
    nsub = max<int>( (int)((double)numprocs/nitems+0.5), 1 );
    
    // Make interaction list
    
    intlist = vector<ivec>(nitems);
    int item, value;
    int count=0;
    for (int i=0; i<nitems*nsub; i++) {
      item = (int)floor(static_cast<float>(i / nsub));
      value = count++ % numprocs;
      intlist[item].push_back(value);
    }
    
    if (myid==0) {
      cout << "\nPartition\n";
      cout << "----------\n";
      for (int i=0; i<nitems; i++) {
	cout << i << "> ";
	for (int j=0; j<(int)intlist[i].size(); j++)
	  cout << " " << intlist[i][j];
	cout << "\n";
      }
      cout << "\n";
    }
    // Reassign groups and communicators
    nitems0 = nitems;
    group = new MPI_Group [nitems];
    comm  = new MPI_Comm [nitems];
    
    for (int i=0; i<nitems; i++) {
      nl = intlist[i].size();
      int *glist = new int [nl];
      for (int j=0; j<nl; j++) glist[j] = intlist[i][j];
      int ret = MPI_Group_incl(world_group, nl, glist, &group[i]);
      if (MPI_SUCCESS != ret) cout << "Process " << myid << ": group failed\n";
      MPI_Comm_create(MPI_COMM_WORLD, group[i], &comm[i]);
      delete [] glist;
    }
    
    // Make rank list
    ranklist = vector<int>(nitems);
    int ircnt=0;
    for (int i=0; i<nitems; i++) {
      ranklist[i] = -1;
      
      for (int j=0; j<(int)intlist[i].size(); j++) {
	if (intlist[i][j] == myid) {
	  MPI_Comm_rank(comm[i], &irank);
	  ranklist[i] = irank;
	  ircnt++;
	}
      }
    }
    --MPIMutex;
    
  }
  
  
  // Compute model normalzation
  
  _dist->Reset();
  _model->ResetCache();
  for (int i=0; i<nitems; i++) {
    
    if (ranklist[i] > -1)
      norm += _intgr->NormValue(_model, _dist->CurrentTile(),
				_dist->CurrentItem(),
				ranklist[i], intlist[i].size() );
    
    _dist->Next();
  }
  
  ++MPIMutex;
  MPI_Allreduce(&norm, &norm0, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  --MPIMutex;
  
  // Bad state
  if (norm0<=0.0) {

    // Print it out
    if (debugflags & badcell) {
      ostream cout(checkTable("debug_output"));
      cout << "Process " << myid << ": badstate, norm=" << norm0 << ": ";
      for (unsigned int i=0; i<s.size(); i++) cout << setw(12) << s[i];
      cout << "\n";
    }

    bval1 = 1;

  }
  
  // Compute log likelihood
  _dist->Reset();
  _model->ResetCache();
  
#ifdef DEBUG_TESSTOOL
  int ttnum = 0;
#endif
  
  if (sampleNext==1) {
    InitializeTessToolTileDump();
  }
  
  for (int i=0; i<nitems; i++) {
    
    // Skip computation if bad state
    if (ranklist[i] > -1 && norm0>0.0) {
      
      t  = _dist->CurrentTile();
      sd = _dist->CurrentItem();
      
      z  = _intgr->Value(_model, t, sd, ranklist[i], 
			intlist[i].size(), comm[i]);
      
      if (ranklist[i]==0) {
	
	try {
	  val += LP->LikeProb(z, sd, _dist->Total()/norm0, t, &s, indx);
	}
	catch (ImpossibleStateException &e) {
	  bval1 = 1;
	}
      
	// Tile diagnostics (NB: this will be
	// replace by TessTool . . .)
	if (DEBUG_LIKE) TileDump(t, sd, z);
	if (sampleNext==1) {
	  TessToolTileDump(t, sd, z);
#ifdef DEBUG_TESSTOOL
	  ferr << "# " 
	       << setw(15) << t->X(0.0, 0.0) 
	       << setw(15) << t->Y(0.0, 0.0)
	       << setw(15) << t->X(1.0, 1.0) 
	       << setw(15) << t->Y(1.0, 1.0)
	       << endl;
	  ttnum++;
#endif
	}
	
      }
    }
    
    _dist->Next();
    
  }
  if (sampleNext==1) {
    sampleNext=0;
    FinalizeTessToolTileDump();
#ifdef DEBUG_TESSTOOL
    ferr << "Process " << myid << ": tesstool tiles=" << ttnum << endl;
#endif
  }
  
  ++MPIMutex;
  MPI_Reduce(&val, &ret.value, 1, MPI_DOUBLE, MPI_SUM, parent_id, MPI_COMM_WORLD);
  MPI_Reduce(&bval1, &bval, 1, MPI_INT, MPI_SUM, parent_id, MPI_COMM_WORLD);
  --MPIMutex;

  ret.good = (bval==0) ? true : false;
  
  DEBUG_LIKE = false;
  
  return ret;
}


// dummy function added to conform to new interface.
void LikelihoodComputationMPI::normEvaluateList(Model *model, Tile *tile, 
						const vector<double>& x,
						const vector<double>& y,
						SampleDistribution* sampleDistribution,
						vector<double>& evaluations)
{
  cout << "LikelihoodComputationMPI::normEvaluateList should never be called!\n";
  // write the simple for loop!
}


// dummy function added to conform to new interface.
void LikelihoodComputationMPI::modelEvaluateList(Model *model, Tile *tile, 
						 const vector<double>& x,
						 const vector<double>& y,
						 SampleDistribution* sampleDistribution,
						 vector< vector<double> >& evaluations)
{
  cout << "LikelihoodComputationMPI::modelEvaluateList should never be called!\n";
}

/*
  This const casting is ridiculous 
  but I'm not sure what else to do . . .
*/
void LikelihoodComputationMPI::pack_and_send() const
{
  if (myid==0) {
    *const_cast<double*>(&beglist[0]) = beg;
    *const_cast<double*>(&endlist[0]) = end;
    for (int n=1; n<numprocs; n++) {
      MPI_Recv(const_cast<double*>(&beglist[n]), 1, MPI_DOUBLE, n, 392, 
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(const_cast<double*>(&endlist[n]), 1, MPI_DOUBLE, n, 393, 
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
  } else {
    MPI_Send(&beg, 1, MPI_DOUBLE, 0, 392, MPI_COMM_WORLD);
    MPI_Send(&end, 1, MPI_DOUBLE, 0, 393, MPI_COMM_WORLD);
  }
}

