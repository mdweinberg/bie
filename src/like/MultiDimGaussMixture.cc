#include <new>
#include <typeinfo>

using namespace std;

#include <MultiDimGaussMixture.h>
#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MultiDimGaussMixture)

using namespace BIE;

// Used to label output
const std::string MultiDimGaussMixture::ParameterDescription(int i)
{
  string ret;
  ostringstream sout;
  sout << "Pos " << i+1;
  return sout.str();
}

// Likelihood function
double MultiDimGaussMixture::LocalLikelihood(State *s)
{
  double value = 0.0, z2, lf;
  size_t wsiz  = w.size(), j;

  for (size_t d=0; d<wsiz; d++) {
    for (j=0, z2=0.0, lf=0.0; j<s->size(); j++) {
      z2 += ((*s)[j] - c[d][j]) * ((*s)[j] - c[d][j]) / v[d][j];
      lf += log(2.0*M_PI*v[d][j]);
    }
    value += w[d] * exp(-0.5*z2 - 0.5*lf);
  }

  value = log(value);

  return value;
}
