#include <cstdlib>

#include <TileMPIThread.h>
#include <BIEMutex.h>
#include <TSLog.h>  // for performance debugging

using namespace BIE;

/* This thread only runs on tile masters in the tile_point
 * style parallelism.
 *
 * Continuously poll worker processes to see if any have
 * replied to processing requests.  Upon receipt of a
 * reply, pass the MPI status (which contains the identity
 * of the replier, to the tileRequestHandlerThread, which
 * will match up the reply to the request.
 *
 * This polls rather than blocks because we are using a 
 * non thread-safe version of MPI. Blocking in a receive
 * would preclude us from doing any sends while blocked -
 * a request that came in from an integrator could not
 * be for processing because this thread was blocked inside
 * of MPI.
 */

void TileMPIThread::run()
{
  int messageReceivedFlag;
  MPI_Status status;

#if DEBUG
  cout << "TileMPIThread.Run entered\n";
#endif



  // loop through reading results from worker processors
  

  while (!stopRequested){
      
    // poke MPI to see if anyone has sent anything back
    // Don't block here because blocking in a receive would lock all threads
    // out of the MPI library.  In particular,  No other thread could 
    // *send* messages (!).
    //  (A thread might send a message to initiate 
    //  the evaluation of a model point on a remote processor).

    // We could eek out some more performance here by doing a blocking probe
    // when all processors have outstanding requests.  But we'd have to worry
    // a bit about how to stop the thread.

    if (pauseRequested){
      paused.post();  // notify requester we are paused
      resume.wait();  // wait until told to resume
    }

    if (stopRequested) break;

    // if not all of the processors are working, and there is work available
    // block here to give tileRequestHandlerthread a chance to send the
    // work out. It's okay if this test is imprecise; it's just an optimization
    // to remove most of the looping in this thread.

    if ( ((!tileRequestHandlerThread->allWorking() && 
	   tileRequestHandlerThread->workAvailable()) ||

          tileRequestHandlerThread->noneWorking()
	 )  &&

        !pauseRequested && !stopRequested){

        tileRequestHandlerThread->loopEventWaitTime.start();
        tileRequestHandlerThread->MPIThreadLoopEvent.wait();
        tileRequestHandlerThread->loopEventWaitTime.stop();

        tileRequestHandlerThread->MPIThreadLoopEvent.reset(); // must reset to use it again
    }


       ++MPIMutex;
	  messageReceivedFlag = false;

	  // see if all processors are working on something, if so, *Block*
  	  if ((tileRequestHandlerThread->allWorking()&& 
	       !tileRequestHandlerThread->noMPISlaves()
              ) ||

              (!tileRequestHandlerThread->workAvailable() && 
               !tileRequestHandlerThread->noneWorking())

             ){

            tileRequestHandlerThread->probeTime.start();
	       MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            tileRequestHandlerThread->probeTime.stop();

	    messageReceivedFlag = true;

	  }else{
            tileRequestHandlerThread->IprobeTime.start();
	    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &messageReceivedFlag, &status);
            tileRequestHandlerThread->IprobeTime.stop();
	  }
       --MPIMutex;

       if (messageReceivedFlag){  // There is a message to be read
	 // normal return message. find associated request and handle
	 if (status.MPI_ERROR == 0){
  	   tileRequestHandlerThread->readAndProcessMessage(&status);
         }else{
	   cerr << "TileMPIThread. Probe returned error " << status.MPI_ERROR << "\n";
	   // abort();
         }


       }else{
	 // no message; sleep a bit and try again
	 //Sleep(1 /* ms */); // from thread package
	 //yield();
       }


  } // end while

  cout << "TileMPIThread exiting by request\n";
}

void TileMPIThread::reInitialize(TileRequestHandlerThread* newHandlerThread)
{
  //called at start of new compute likelihood.
  // probably should do something here;  for now nothing is pretty close.
  tileRequestHandlerThread = newHandlerThread;
}

// pause the target thread.  blocks until target is actually paused.
void TileMPIThread::pause()
{
  if (pauseRequested){
    cerr << "pauseRequested is true! Attempt to pause already paused TileMPIThread\n";
    abort();
  }

  /* Deprecated usage
  if (paused.getValue() >0){
    cerr << "paused semaphore >0! Attempt to pause non-paused TileMPIThread\n";
    abort();
  }
  */

  pauseRequested = true;

  tileRequestHandlerThread->MPIThreadLoopEvent.signal();

  paused.wait();  // wait until actually paused.
}


// continue a paused target thread
void TileMPIThread::unPause()
{
  // could check to make sure resume is 0 before posting. would allow multiple continues
  /* Deprecated usage
  if (resume.getValue() >0){
    cerr << "Attempt to resume non-paused TileMPIThread\n";
    abort();
  }
  */

  pauseRequested = false;
  resume.post();
}


/*
 *  cleanup the thread object when Run() returns or Thread.exit() is called.
 *  Clients stop the thread by setting the member variable "stopRequested,
 *  causing the loop in the Run() method to exit.
 */
void TileMPIThread::final()
{
  // The common c++ documentation swears this is an okay thing to do.
   delete this;  //egad!
}
