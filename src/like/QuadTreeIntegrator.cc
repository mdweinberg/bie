
#include <iostream>
#include <algorithm>

using namespace std;

#include <BIEException.h>
#include <QuadTreeIntegrator.h>

using namespace BIE;

int QuadTreeIntegrator::numevals;
int QuadTreeIntegrator::maxcurlevel;

// Top level constructor
QuadTreeIntegrator::
QuadTreeIntegrator(double a0, double a1, double b0, double b1,
		   int _dim, Integrand func, double _eps, 
		   int _minlevel, int _maxlevel)
{
				// X dimension
  a[0] = a0;
  a[1] = b0;
				// Y dimension
  b[0] = a1;
  b[1] = b1;
				// Tolerance
  eps = _eps;
				// Integrand
  dim = _dim;
  eval = func;
				// Links
  parent = NULL;
  child = NULL;
				// Set up level counting . . .
  minlevel = _minlevel;
  maxlevel = _maxlevel;
  level = 0;
  numevals = 0;

				// Create root node data
  root = new RootVars;
  root->root = this;
  root->value = vector<double>(dim, 0.0);
  root->error = vector<double>(dim, 0.0);
}

// Destructor
QuadTreeIntegrator::~QuadTreeIntegrator()
{
  if (child) {
    for (int j=0; j<4; j++) delete child[j];
    delete [] child;
  }

  if (this == root->root) delete root;
}

// Child constructor (clones parent)
QuadTreeIntegrator::QuadTreeIntegrator(QuadTreeIntegrator *p, int i, 
				       RootVars* _root)
{
  switch(i) {

  case 0:

    a[0] = p->a[0];
    b[0] = 0.5*(p->a[0] + p->b[0]);

    a[1] = p->a[1];
    b[1] = 0.5*(p->a[1] + p->b[1]);

    break;

  case 1:

    a[0] = 0.5*(p->a[0] + p->b[0]);
    b[0] = p->b[0];

    a[1] = p->a[1];
    b[1] = 0.5*(p->a[1] + p->b[1]);

    break;

  case 2:

    a[0] = 0.5*(p->a[0] + p->b[0]);
    b[0] = p->b[0];

    a[1] = 0.5*(p->a[1] + p->b[1]);
    b[1] = p->b[1];

    break;

  case 3:

    a[0] = p->a[0];
    b[0] = 0.5*(p->a[0] + p->b[0]);

    a[1] = 0.5*(p->a[1] + p->b[1]);
    b[1] = p->b[1];

    break;

  default:
    cerr << "You can't be here!\n";
  }

  eps = p->eps;
  dim = p->dim;
  eval = p->eval;

  parent = p;
  child = NULL;
  
  maxlevel = p->maxlevel;
  level = p->level+1;

  root = _root;
}

void QuadTreeIntegrator::add_to_master(vector<double> val, 
				       vector<double> err)
{
  for (int j=0; j<dim; j++) {
    root->value[j] += val[j];
    root->error[j] += err[j];
  }
}

void QuadTreeIntegrator::sub_from_master(vector<double> val, 
					 vector<double> err)
{
  for (int j=0; j<dim; j++) {
    root->value[j] -= val[j];
    root->error[j] -= err[j];
  }
}


double QuadTreeIntegrator::max_error(vector<double> err)
{
  double maxerr = 0.0;
  for (int j=0; j<dim; j++) maxerr = max<double>(maxerr, fabs(err[j]));
  return maxerr;
}

double QuadTreeIntegrator::max_rel_error(void)
{
  double maxerr = 0.0;
  for (int j=0; j<dim; j++) {
    if (fabs(root->value[j]) > 1.0e-12)
      maxerr = max<double>(maxerr, fabs(root->error[j]/root->value[j]));
  }
  return maxerr;
}


vector<double> QuadTreeIntegrator::Integral()
{
  bool minreached = false;
				// First one!
  compute();
  
  maxcurlevel = 0;
				// Descend the tree
  while (max_rel_error() > eps || !minreached) {

				// Sort to find node with worst absolute error
    sort(root->frontier.begin(), root->frontier.end());
    

				// DEBUG
    /*
    for (unsigned int i=0; i<root->frontier.size(); i++)
      cout << i << ": " << root->frontier[i].first << "\n";
    */
				// END DEBUG

				// Worst node
    QuadTreeIntegrator *p = root->frontier.back().second;

				// Record level (DEBUG)
    maxcurlevel = max<int>(maxcurlevel, p->level);

				// Level limit reached, return
    if (p->level>=maxlevel) {
      break;
    }
				// Minimum level reached, set flag
    if (p->level>=minlevel) {
      minreached = true;
    }
				// Remove this node from frontier
				// and its contribution from the total
    root->frontier.pop_back();
    sub_from_master(p->value, p->error);

				// Now create children
    p->child = new QuadTreeIntegrator* [4];
    for (int i=0; i<4; i++) {
      p->child[i] = new QuadTreeIntegrator(p, i, root);
      p->child[i]->compute();
    }

  }

				// Ok, return current value
  return root->value;
}


void QuadTreeIntegrator::compute()
{
//  Adaptive Quadrature for Multiple Integrals over N-Dimensional
//  Rectangular Regions
//
//  Author(s): A.C. Genz, A.A. Malik
//
//  This function computes, to an attempted specified accuracy, the value of
//  the integral over an n-dimensional rectangular region.
//
//  N Number of dimensions.
//  A,B One-dimensional arrays of length >= N . On entry A[i],  and  B[i],
//     contain the lower and upper limits of integration, respectively.
//  EPS    Specified relative accuracy.
//  RELERR Contains, on exit, an estimation of the relative accuray of RESULT.
//
//  Method:
//
//  An integration rule of degree seven is used together with a certain
//  strategy of subdivision.
//  For a more detailed description of the method see References.
//
//  Notes:
//
//   1.Multi-dimensional integration is time-consuming. For each rectangular
//     subregion, the routine requires function evaluations.
//     Careful programming of the integrand might result in substantial saving
//     of time.
//   2.Numerical integration usually works best for smooth functions.
//     Some analysis or suitable transformations of the integral prior to
//     numerical work may contribute to numerical efficiency.
//
//  References:
//
//   1.A.C. Genz and A.A. Malik, Remarks on algorithm 006:
//     An adaptive algorithm for numerical integration over
//     an N-dimensional rectangular region, J. Comput. Appl. Math. 6 (1980) 295-302.
//   2.A. van Doren and L. de Ridder, An adaptive algorithm for numerical
//     integration over an n-dimensional cube, J.Comput. Appl. Math. 2 (1976) 207-217.
//
//=========================================================================

  
  typedef vector<double> DVector;
  
  double ctr[15], wth[15], wthl[15];
  
  const double xl2 = 0.358568582800318073;
  const double xl4 = 0.948683298050513796;
  const double xl5 = 0.688247201611685289;
  const double w2  = 980./6561;
  const double w4  = 200./19683;
  const double wp2 = 245./486;
  const double wp4 = 25./729;
  
  double wn1[14] = { -0.193872885230909911, -0.555606360818980835,
		     -0.876695625666819078, -1.15714067977442459,  
		     -1.39694152314179743,  -1.59609815576893754,  
		     -1.75461057765584494,  -1.87247878880251983,
		     -1.94970278920896201,  -1.98628257887517146,  
		     -1.98221815780114818,  -1.93750952598689219,
		     -1.85215668343240347,  -1.72615963013768225};
  
  double wn3[14] = {  0.0518213686937966768,   0.0314992633236803330,
		      0.0111771579535639891,  -0.00914494741655235473,
		      -0.0294670527866686986, -0.0497891581567850424,
		      -0.0701112635269013768, -0.0904333688970177241,
		      -0.110755474267134071,  -0.131077579637250419,
		      -0.151399685007366752,  -0.171721790377483099, 
		      -0.192043895747599447,  -0.212366001117715794};
  
  double wn5[14] = {  0.871183254585174982e-01,  0.435591627292587508e-01,
		      0.217795813646293754e-01,  0.108897906823146873e-01,
		      0.544489534115734364e-02,  0.272244767057867193e-02,
		      0.136122383528933596e-02,  0.680611917644667955e-03,
		      0.340305958822333977e-03,  0.170152979411166995e-03,
		      0.850764897055834977e-04,  0.425382448527917472e-04,
		      0.212691224263958736e-04,  0.106345612131979372e-04};
  
  double wpn1[14] = { -1.33196159122085045, -2.29218106995884763,
		      -3.11522633744855959, -3.80109739368998611,
		      -4.34979423868312742, -4.76131687242798352,
		      -5.03566529492455417, -5.17283950617283939,
		      -5.17283950617283939, -5.03566529492455417,
		      -4.76131687242798352, -4.34979423868312742,
		      -3.80109739368998611, -3.11522633744855959};
  
  double wpn3[14] = {  0.0445816186556927292,  -0.0240054869684499309,
		       -0.0925925925925925875, -0.161179698216735251,
		       -0.229766803840877915,  -0.298353909465020564,
		       -0.366941015089163228,  -0.435528120713305891,
		       -0.504115226337448555,  -0.572702331961591218,
		       -0.641289437585733882,  -0.709876543209876532,
		       -0.778463648834019195,  -0.847050754458161859};
  
  int n = 2;
  DVector z(n);
  DVector result(dim, 0.0);
  DVector abserr(dim, 0.0);
  DVector tmp;
  
  //    int ifail = 3;
  if (n < 2 || n > 15) {
    throw DimValueException(__FILE__, __LINE__);
  }
  
  double twondm = pow(2.0, n);
  //    int ifncls = 0;
  //    bool ldv   = false;
  //    int irgnst = 2*n+3;
  //    int irlcls = (int)(twondm)  +2*n*(n+1)+1;
  
  // The original algorithm expected a parameter MAXPTS
  //   where MAXPTS = Maximum number of function evaluations to be allowed.
  //   Here we set MAXPTS to 1000*(the lowest possible value)
  //    int maxpts = 1000*irlcls;
  //    int minpts = 1;
  
  // The original agorithm expected a working space array WK of length IWK
  // with IWK Length ( >= (2N + 3) * (1 + MAXPTS/(2**N + 2N(N + 1) + 1))/2).
  // Here, this array is allocated dynamically
  
  for (int j=0; j<n; j++) {
    ctr[j] = (b[j] + a[j])*0.5;
    wth[j] = (b[j] - a[j])*0.5;
  }
  
  double rgnvol;
  DVector sum1(dim);
  DVector sum2(dim, 0.0), sum3(dim, 0.0), sum4(dim, 0.0), sum5(dim, 0.0);
  DVector difmax(dim, 0.0), f2(dim), f3(dim), dif(dim);
  DVector rgncmp(dim, 0.0);
  
  value = DVector(dim, 0.0);
  error  = DVector(dim);
  
  rgnvol = twondm;
  for (int j=0; j<n; j++) {
    rgnvol *= wth[j];
    z[j]    = ctr[j];
  }
  sum1 = eval(z[0], z[1]); //evaluate function
  numevals++;
  
  for (int j=0; j<n; j++) {
    z[j]    = ctr[j] - xl2*wth[j];
    tmp     = eval(z[0], z[1]);	for (int i=0; i<dim; i++) f2[i] = tmp[i];
    z[j]    = ctr[j] + xl2*wth[j];
    tmp     = eval(z[0], z[1]);	for (int i=0; i<dim; i++) f2[i] += tmp[i];
    wthl[j] = xl4*wth[j];
    z[j]    = ctr[j] - wthl[j];
    tmp     = eval(z[0], z[1]);	for (int i=0; i<dim; i++) f3[i] = tmp[i];
    z[j]    = ctr[j] + wthl[j];
    tmp     = eval(z[0], z[1]);	for (int i=0; i<dim; i++) f3[i] += tmp[i];
    for (int i=0; i<dim; i++) {
      sum2[i]   += f2[i];
      sum3[i]   += f3[i];
      dif[i]     = fabs(7*f2[i]-f3[i]-12*sum1[i]);
      difmax[i]  = max<double>(dif[i], difmax[i]);
    }
    z[j]    = ctr[j];
    numevals += 4;
  }
  
  for (int j=1; j<n; j++) {
    int j1 = j-1;
    for (int k=j;k<n;k++) {
      for (int l=0;l<2;l++) {
	wthl[j1] = -wthl[j1];
	z[j1]    = ctr[j1] + wthl[j1];
	for (int m=0;m<2;m++) {
	  wthl[k] = -wthl[k];
	  z[k]    = ctr[k] + wthl[k];
	  tmp = eval(z[0], z[1]);	for (int i=0; i<dim; i++) sum4[i] += tmp[i];
	  numevals++;
	}
      }
      z[k] = ctr[k];
    }
    z[j1] = ctr[j1];
  }
  
  for (int j=0;j<n;j++) {
    wthl[j] = -xl5*wth[j];
    z[j] = ctr[j] + wthl[j];
  }
  
 ouch:
  tmp = eval(z[0], z[1]);	for (int i=0; i<dim; i++) sum5[i] += tmp[i];
  numevals++;
  for (int j=0;j<n;j++) {
    wthl[j] = -wthl[j];
    z[j] = ctr[j] + wthl[j];
    if (wthl[j] > 0.0) goto ouch;
  }
  
  for (int i=0; i<dim; i++) {
    rgncmp[i] = rgnvol*(wpn1[n-2]*sum1[i]+wp2*sum2[i]+wpn3[n-2]*sum3[i]+wp4*sum4[i]);
    value[i] = wn1[n-2]*sum1[i]+w2*sum2[i]+wn3[n-2]*sum3[i]+w4*sum4[i]+wn5[n-2]*sum5[i];
    value[i] *= rgnvol;
    error[i]  = fabs(value[i]-rgncmp[i]);
  }
  
				// Add this result to total
  add_to_master(value, error);
  root->frontier.push_back(FrontierNode(max_error(error), this));
  
}

void QuadTreeIntegrator::PrintFrontier(ostream& out)
{
  if (child != NULL) 
    for (int j=0; j<4; j++) child[j]->PrintFrontier(out);
  else
    out 
      << " " << level
      << " " << a[0] 
      << " " << b[0]
      << " " << a[1]
      << " " << b[1]
      << "\n";
}
