// This may look like C code, but it is really -*- C++ -*-

#include <gaussQ.h>
#include <FivePointIntegration.h>
#include <BIEMutex.h>
#include <BIEmpi.h>
#include <LikelihoodComputation.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::FivePointIntegration)

using namespace BIE;

// todo. Write down what the requirements for a nesting integrator are


FivePointIntegration::FivePointIntegration()
{
  // initialize something!
}

FivePointIntegration::FivePointIntegration(double desiredRelativeError)
{
  this->desiredRelativeError = desiredRelativeError;
}

#if 0
FivePointIntegration::FivePointIntegration(int nu, int nv)
{
  //  U = new LegeQuad(nu);
  //  V = new LegeQuad(nv);
  //  nU = nu;
  //  nV = nv;
}

#endif

FivePointIntegration::~FivePointIntegration()
{
  //  delete U;
  //  delete V;
}

//void FivePointIntegration::ParameterChange(int nu, int nv)
//{
//  delete U;
//  delete V;
//  U = new LegeQuad(nu);
//  V = new LegeQuad(nv);
//  nU = nu;
//  nV = nv;
//}

double FivePointIntegration::NormValue(Model* p_model, Tile* p_tile, 
				  SampleDistribution* p_dist)
{


  double x[2], y[2];
  double fourPointIntegral=0.0;


  // get x and y corner values from tile
  p_tile->corners(x[0], x[1], y[0], y[1]);
  for(int i=0; i<2; i++){
    for(int j=0; j<2; j++){
      fourPointIntegral += p_model->NormEval( x[i], y[j], p_dist); 
    }
  }

  // Normalize by number of points and area
  fourPointIntegral = fourPointIntegral * 0.25 * fabs((y[1]-y[0])*(x[1]-x[0]));


  // Calculate the value at the center of the tile
  double onePointIntegral = p_model->NormEval( 0.5*(x[0]+x[1]), 0.5*(y[0]+y[1]), p_dist); 

  // Normalize by number of points and area
  onePointIntegral = onePointIntegral * 1.0 * fabs((y[1]-y[0])*(x[1]-x[0]));

  // Use the center point to calculate a five point integral using Simpson's rule
  double ans = (4.0*fourPointIntegral - onePointIntegral)/3.0;

  return ans;  


#if 0
  double ans = 0.0;
  
  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      ans += _model->NormEval(_tile->X(U->knot(ii), V->knot(jj)), 
			      _tile->Y(U->knot(ii), V->knot(jj)),
			      _dist) *
	_tile->measure(U->knot(ii), V->knot(jj))
	* U->weight(ii) * V->weight(jj);
    }
  }

  return ans;
#endif
}


/**
 *
 * 
 */
vector<double> FivePointIntegration::Value(Model* p_model, Tile* p_tile,
				      SampleDistribution* p_dist)
{

  double x[2], y[2];
  int numBins = p_dist->numberData();
  vector<double> fourPointIntegral(numBins, 0.0);

  vector<double> z(numBins);
  vector<double> ans(numBins);

  // get x and y corner values from tile
  p_tile->corners(x[0], x[1], y[0], y[1]);


  for(int i=0; i<2; i++){
    for(int j=0; j<2; j++){
      z = p_model->Evaluate( x[i], y[j], p_dist); 

      for (int k=0; k<numBins; k++) {
         fourPointIntegral[k] += z[k];
      }
    }
  }


  // Normalize by number of points and area
  // aah todo. Wrong for other than square tiles
  for (int k=0; k<numBins; k++) {
    fourPointIntegral[k] = fourPointIntegral[k] * 0.25 * fabs(y[1]-y[0])*(x[1]-x[0]);
  }

  // Calculate the value at the center of the tile
  vector<double> onePointIntegral(numBins, 0.0);

  // Evaluate model at center of tile
  z = p_model->Evaluate( 0.5*(x[0]+x[1]), 0.5*(y[0]+y[1]), p_dist); 

  // Normalize by number of points and area
  // aah todo. Probably wrong for other than square tiles
  for (int k=0; k<numBins; k++) {
     onePointIntegral[k] = z[k] * 1.0 * fabs(y[1]-y[0])*(x[1]-x[0]);
  }


  absError.resize(numBins);
  relativeError.resize(numBins);

  // Use the center point to calculate a five point integral using Simpson's rule
  for (int k=0; k< numBins; k++) {
    ans[k] = (4.0*fourPointIntegral[k] - onePointIntegral[k])/3.0;
    absError[k] = fabs(ans[k] - fourPointIntegral[k]);
    relativeError[k] = fabs(absError[k] * ans[k]);
  }


  return ans;  



#if 0

  vector<double> ans(_dist->numberData(), 0.0);
  vector<double> z(_dist->numberData());
  double factor;
  
  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {
      factor = _tile->measure(U->knot(ii), V->knot(jj))
	* U->weight(ii) * V->weight(jj);

      z = _model->Evaluate( _tile->X(U->knot(ii), V->knot(jj)), 
			    _tile->Y(U->knot(ii), V->knot(jj)), _dist);

      for (int j=0; j<_dist->numberData(); j++) 
	ans[j] += z[j] * factor;
    }
  }
  return ans;
#endif

  

}


/**
 *
 * 
 */
vector<double> FivePointIntegration::Value(LikelihoodComputation* p_likelihoodComputation,
					   Model* p_model, Tile* p_tile,
					   SampleDistribution* p_dist)
{

  double x[2], y[2];
  int numBins = p_dist->numberData();
  cout << "FivePointIntegration: numBins = numberData " << numBins << "\n";

  vector<double> fourPointIntegral(numBins, 0.0);

  vector<double> z(numBins);
  vector<double> ans(numBins);

  // get x and y corner values from tile
  p_tile->corners(x[0], x[1], y[0], y[1]);


  // threaded version
  vector<double> xi, yi;
  // create list of points to evaluate
  for(int i=0; i<2; i++){
    for(int j=0; j<2; j++){
      xi.push_back(x[i]);
      yi.push_back(y[j]);
    }
  }

  int numPoints = xi.size(); //4
  // evaluate list and wait for results. for each point sent in, get a distribution back
  vector< vector<double> > evaluations(numPoints, vector<double>(p_dist->numberData()));
  p_likelihoodComputation->modelEvaluateList(p_model, p_tile, xi, yi, p_dist, evaluations);

  for (int i=0; i< numPoints; i++){  // should be four for four point evaluation
    for (int k=0; k<numBins; k++) {
      fourPointIntegral[k] += evaluations[i][k];
    }
  }


  // Normalize by number of points and area
  // aah todo. Wrong for other than square tiles
  for (int k=0; k<numBins; k++) {
    fourPointIntegral[k] = fourPointIntegral[k] * 0.25 * fabs(y[1]-y[0])*(x[1]-x[0]);
  }

  // Calculate the value at the center of the tile
  vector<double> onePointIntegral(numBins, 0.0);

  // Evaluate model at center of tile
  z = p_model->Evaluate( 0.5*(x[0]+x[1]), 0.5*(y[0]+y[1]), p_dist); 

  // Normalize by number of points and area
  // aah todo. Probably wrong for other than square tiles
  for (int k=0; k<numBins; k++) {
     onePointIntegral[k] = z[k] * 1.0 * fabs(y[1]-y[0])*(x[1]-x[0]);
  }


  absError.resize(numBins);
  relativeError.resize(numBins);

  // Use the center point to calculate a five point integral using Simpson's rule
  for (int k=0; k< numBins; k++) {
    ans[k] = 4.0*(fourPointIntegral[k] - onePointIntegral[k])/3.0;
    absError[k] = fabs(ans[k] - fourPointIntegral[k]);
    relativeError[k] = fabs(absError[k] * ans[k]);
  }


  return ans;  
}



double FivePointIntegration::NormValue(Model* _model, Tile* _tile,
				  SampleDistribution* _dist,
				  int irank, int nrank)
{
  // should implement this to compare the different parallel schemes

  double ans = 0.0;
#if 0  
  int counter=0;

  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      if ( counter++ % nrank == irank) {

	ans += _model->NormEval(_tile->X(U->knot(ii), V->knot(jj)), 
				_tile->Y(U->knot(ii), V->knot(jj)),
				_dist) *
	  _tile->measure(U->knot(ii), V->knot(jj))
	  * U->weight(ii) * V->weight(jj);
      }
    }
  }

#endif
  return ans;

}

vector<double> FivePointIntegration::Value(Model* _model, Tile* _tile,
				      SampleDistribution* _dist,
				      int irank, int nrank, MPI_Comm& comm)
{
  vector<double> ans;

#if 0
  vector<double> z(_dist->numberData());
  double *ans0 = new double [_dist->numberData()];
  double *ans1 = new double [_dist->numberData()];
  double factor;
  
  for (int k=0; k<_dist->numberData(); k++) ans1[k] = 0.0;

  int counter = 0;

  for(int jj=1; jj<=nV; jj++) {
    for(int ii=1; ii<=nU; ii++) {

      if ( counter++ % nrank == irank) {
	
	factor = _tile->measure(U->knot(ii), V->knot(jj))
	  * U->weight(ii) * V->weight(jj);

	z = _model->Evaluate( _tile->X(U->knot(ii), V->knot(jj)), 
			      _tile->Y(U->knot(ii), V->knot(jj)), _dist);
      }
    }
  }
    
  ++MPIMutex;
  MPI_Reduce(ans1, ans0, _dist->numberData(), MPI_DOUBLE, 
	     MPI_SUM, 0, comm);
  --MPIMutex;


  if (irank==0)
    ans = vector<double>(ans0, ans0+_dist->numberData());

  delete [] ans0;
  delete [] ans1;

#endif
  return ans;

}

