#include <new>
#include <typeinfo>

using namespace std;

#include <MultiDimGaussTest.h>
#include <Distribution.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::MultiDimGaussTest)

using namespace BIE;

// Used to label output
const std::string MultiDimGaussTest::ParameterDescription(int i)
{
  string ret;

  ostringstream sout;
  sout << "Pos " << i+1;
  return sout.str();
}

double MultiDimGaussTest::LocalLikelihood(State *s)
{
  double z2 = 0.0;
  unsigned n = 0;
  for (unsigned j=0; j<s->size(); j++) {
    z2 += (*s)[j]*(*s)[j];
    n++;
  }

  return -z2/(2.0*var) - 0.5*log(2.0*M_PI*var)*n;
}
