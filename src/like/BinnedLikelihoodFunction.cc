#include <iomanip>

#include <Distribution.h>
#include <Tile.h>
#include <BinnedLikelihoodFunction.h>
#include <BIEmpi.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::BinnedLikelihoodFunction)

using namespace BIE;

double BinnedLikelihoodFunction::LikeProb(vector<double> &z, 
					  SampleDistribution* sd, double norm, 
					  Tile *t, State *s, int indx)
{
  double val = 0.0;

  for (int j=0; j<sd->numberData(); j++) {
      
    z[j] *= norm;

    if (z[j]<=0.0 && sd->getValue(j)>0.0) {

      if (sd->getValue(j)>threshval) {
	throw ImpossibleStateException(__FILE__, __LINE__);
      }

      // Print it out
      if (debugflags & badcell) {
	ostream cout(checkTable("debug_output"));
	
	double x1, x2, y1, y2;
	t->corners(x1, x2, y1, y2);
	cout << "Process " << myid << ": badcell, value=" 
	     << sd->getValue(j) << ", theory=" << z[j]
	     << ", corners: "
	     << x1 << " " << x2 << " " << y1 << " " << y2 << "\n";
	cout << "  state (size=" << s->size() << "):\n";
	for (unsigned int i=0; i<s->size(); i++) cout << setw(12) << (*s)[i];
	cout << "\n";
	
      }
    }

    else if (z[j]<=0.0 && sd->getValue(j)<=0.0) 
      val += 0.0; 
      
    else
      val += log(z[j])*sd->getValue(j) - z[j] 
#ifdef USE_FACTORIAL	    
	- lgamma(1.0+sd->getValue(j))
#endif
	;

  }

  return val;
}

