#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

using namespace std;

#include <gvariable.h>
#include <BIEdebug.h>
#include <BIEException.h>
#include <LikelihoodComputationMPITP.h>

#include <NormEvaluationRequest.h>
#include <ModelEvaluationRequest.h>

#include <TileMasterWorkerThread.h>

#include <Node.h>

#include <BIEMutex.h>
#include <gfunction.h>

#undef  MPITS_FINE_TRACE

#include <cc++/thread.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif

BIE_CLASS_EXPORT_IMPLEMENT(BIE::LikelihoodComputationMPITP)

using namespace ost;

using namespace BIE;

// #undef USE_FACTORIAL
#define USE_FACTORIAL

#ifdef DEBUG
static Mutex IOMutex;
static ofstream oerr;
#endif

// 0 load balances on every step
int BIE::LikelihoodComputationMPITP::NLOAD = -1;

// Rates global over all instances
double BIE::LikelihoodComputationMPITP::beg;
double BIE::LikelihoodComputationMPITP::end;

// Define MPI message tags for specific, non-top level messages
#define COMMAND_MSG_TAG 11

// Define commands for top-level messages of type COMMAND_MSG_TAG
#define SET_NORM_COMMAND 5
#define SET_WORKER_RECEIVE_FROM 6
#define PRIOR_FRONTIER 64
#define CURRENT_FRONTIER 128
#define STEP_COMPLETE 256

BIE::LikelihoodComputationMPITP::LikelihoodComputationMPITP(StateInfo* si)
{
  // Initialize the working state
  sb   = State(si);

  // For MPI
  size = si->Ntot;
  if (si->ptype == StateInfo::None || si->ptype == StateInfo::Block)
    cout << "LikelihoodComputationMPITP.constructor, size = " << size << endl;
  else
    cout << "LikelihoodComputationMPITP.constructor, Ndim, M, size = " 
	 << si->Ndim <<", " << si->M << ",  " << size << endl;
  
  buff    = vector<double>(size);
  sb      = vector<double>(size);
  acctime = 0.0;

  // Initial values
  //
  beg = (double)myid/numprocs;
  end = (double)(myid+1)/numprocs;
  
  MPL_accum_time = 0.0;
  MPL_last_time  = 0.0;
  counter        = 0;
  numModelEvals  = 0;
  
  parallel_type  = 2;
  
  if (!myid) rate = vector<double>(numprocs);
  
  nsub = 8;
  
  tileRequestHandlerThread = 0;
  tileMPIThread = 0;
  tileMasterWorkerThread = 0;
  
  slaveFreeCount = 0;
  slaveCreateCount = 0;
  
  _numTMs = -1;
  
#ifdef DEBUG
  ostringstream file;
  file << "debug." << myid;
  oerr.open(file.str().c_str(), ios::out|ios::app);
#endif
}

BIE::LikelihoodComputationMPITP::~LikelihoodComputationMPITP(void)
{
  if (tileRequestHandlerThread){
    // assumes thread is paused, which it should be.
    //
    tileRequestHandlerThread->stopPausedThread();
    //
    // the thread will eventually finish and clean up it's own storage
  }
  
  if (tileMPIThread){
    tileMPIThread->stopPausedThread();
    // the thread will eventually finish and clean up it's own storage
  }
  
  
  if (tileMasterWorkerThread){
    tileMasterWorkerThread->stop();
    // the thread will eventually finish and clean up it's own storage
  }
  
}

void BIE::LikelihoodComputationMPITP::MPL_reset_timer(void)
{
  MPL_accum_time=0.0;
}

void BIE::LikelihoodComputationMPITP::MPL_start_timer(void)
{
  ++MPIMutex;
  MPL_last_time = MPI_Wtime();
  --MPIMutex;
}

void BIE::LikelihoodComputationMPITP::MPL_stop_timer(void)
{
  ++MPIMutex;
  double curtime = MPI_Wtime();
  --MPIMutex;
  
  MPL_accum_time += curtime - MPL_last_time;
  MPL_last_time = curtime;
}

double BIE::LikelihoodComputationMPITP::MPL_read_timer(int reset)
{
  double save = MPL_accum_time;
  if (reset) MPL_accum_time = 0.0;
  return save;
}

void BIE::LikelihoodComputationMPITP::load_balance(void)
{
  MPI_Status status;
  int go = 1;
  int mcnt = 0;
  double ttime, mean;
  
  if (myid == 0) {
    //
    // Send signal to load balance
    //
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
      --MPIMutex;
      //
      // Receive times from slaves
      //
    }
    
    rate[0] = mean = MPL_read_timer(1);
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Recv(&ttime, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 14, MPI_COMM_WORLD, &status);
      --MPIMutex;
      if (ttime>1.0e-4) {
	mean += ttime;
	mcnt++;
      }
      
      rate[status.MPI_SOURCE] = ttime;
      
    }
    mean /= mcnt;
    
#ifdef DEBUG
    cerr << "\nTimes:\n";
    for (int i=0; i<numprocs; i++)
      cerr << i << rate[i] << "\n";
    cerr << "\n";
#endif
    
    for (int i=0; i<numprocs; i++) {
      if (rate[i] < 1.0e-4)
	rate[i] = 0.1/mean;
      else
	rate[i] = 1.0/rate[i];
    }
    for (int i=1; i<numprocs; i++) rate[i] += rate[i-1];
    for (int i=0; i<numprocs; i++) rate[i] /= rate[numprocs-1];
    
    beg = 0.0;
    end = rate[0];
    
#ifdef DEBUG
    cerr << "\nFractions:\n";
    cerr << "0>" << rate[0]<< " 0.0 " << rate[0] << "\n";
    for (int i=1; i<numprocs; i++){
      cerr << i << "> " << rate[i]-rate[i-1] << " " << rate[i-1]
           <<  " " << rate[i] << "\n";
    }
    cerr << "\n";
#endif
    
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Send(&rate[i-1], 1, MPI_DOUBLE, i, 15, MPI_COMM_WORLD);
      MPI_Send(&rate[i], 1, MPI_DOUBLE, i, 16, MPI_COMM_WORLD);
      --MPIMutex;
    }
    
    // Counts number of likelihood routine calls
    counter = 0;		// Reset
    
  } else {
    acctime = MPL_read_timer(1);
    ++MPIMutex;
    MPI_Send(&acctime, 1, MPI_DOUBLE, 0, 14, MPI_COMM_WORLD);
    
    MPI_Recv(&beg, 1, MPI_DOUBLE, 0, 15, MPI_COMM_WORLD, &status);
    MPI_Recv(&end, 1, MPI_DOUBLE, 0, 16, MPI_COMM_WORLD, &status);
    --MPIMutex;
  }
  
#ifdef DEBUG
  cerr << "Process " << myid << ": received " << beg << "  " << end << "\n";
#endif
}

void BIE::LikelihoodComputationMPITP::TileGranularity()
{
  parallel_type = 0;
}

void BIE::LikelihoodComputationMPITP::IntegrationGranularity()
{
  parallel_type = 1;
}

void BIE::LikelihoodComputationMPITP::TilePointGranularity()
{
  parallel_type = 2;
}

void BIE::LikelihoodComputationMPITP::Granularity(int itype)
{
  //
  // Parse parallel type
  //
#ifdef DEBUG
  cerr << "Granularity: set parallel type to " << itype << "\n";
#endif  
  switch (itype) {
  case 0:
    parallel_type = 0; break;
  case 1:
    parallel_type = 1; break;
  case 2:
    parallel_type = 2; break;
  default:
    {
      ostringstream out;
      out << "No such parallel type, " << itype;
      string message(out.str());
      
      throw BIEException("LikelihoodComputationMPITP:", message, 
			 __FILE__, __LINE__);
    }
    
  }
  
}


void BIE::LikelihoodComputationMPITP::enslave_worker()
{
  MPI_Status status;
  unsigned int MM;
  int command, indx;
  
#ifdef DEBUG
  cerr << "Process " << myid << ": worker enslaved \n";
#endif
  
  // Both tile masters and tile group slaves come through here.
  // tilemasters *always* receive from root
  // tileGroupWorkers switch from root to tilemaster and back again
  
  _workerReceiveFrom = 0; //root
  
  bool keep_going, enslaved = true;
  
  while(enslaved) {		// Wait for commands
    
    childSem.wait();
    
    keep_going = true;
    
    while (keep_going) {
      
      ++MPIMutex;
      MPI_Recv(&command, 1, MPI_INT, _workerReceiveFrom, 11, MPI_COMM_WORLD, &status);
      --MPIMutex;
      
      // Check for frontier command
      //
      if (command & (PRIOR_FRONTIER | CURRENT_FRONTIER) )  {
	
	if (command & PRIOR_FRONTIER)  setPriorFrontier();
	if (command & CURRENT_FRONTIER) setCurrentFrontier();
	
      } else if (command & STEP_COMPLETE) {
	
	keep_going = false;
	parentSem.post();
	
      } else {			// Otherwise, it's a likelihood command
				// 
	command = decode_command(command);
	
	TSLog cmdLog(true);	// log
	cmdLog.LogInt("Command Received ", command);
	
	if (command == 0) {
	  
	  keep_going = false;
	  enslaved = false;
	  
	} else if (command == 1) {
	  
	  load_balance();
	  
	} else if (command == 2) {
	  
	  ++MPIMutex;
	  MPI_Recv(&buff[0], size, MPI_DOUBLE, 0, 12, MPI_COMM_WORLD, &status);
	  MPI_Recv(&indx, 1, MPI_INT, 0, 17, MPI_COMM_WORLD, &status);
	  MPI_Recv(&MM, 1, MPI_UNSIGNED, 0, 18, MPI_COMM_WORLD, &status);
	  --MPIMutex;
	  
	  
	  // Unpack buffer
	  //
	  sb.Reset(MM);
	  BufferToState(sb, &buff[0], size);
	  
	  // Compute Likelihood
	  //
	  try {
	    
	    compute_likelihood_MPI(sb, indx);
	    
	  } catch (ImpossibleStateException &e) {
	    // Keep going ... this exception will be handled by the root process
	  }
	  
	} else if (command == 3) { // tile_and_point model norm call

	  // Treat this as a remote procedure call to evaluate the norm 
	  // at a point
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. tile_and_point norm call\n";
#endif
	  // aah TODO message tag should change
	  // get tileId, x and y for point evaluation... NORM
	  double args[3];
	  
	  cmdLog.LogIt("Start Norm Recv");     
	  ++MPIMutex;
	  MPI_Recv(args, 3, MPI_DOUBLE, _workerReceiveFrom, 20, MPI_COMM_WORLD, &status);
	  --MPIMutex;
	  cmdLog.LogIt("Finished Norm Recv");     
	  int requester = status.MPI_SOURCE;
	  
	  int tileId = (int) args[0];
	  double x = args[1];
	  double y = args[2];
	  
	  cmdLog.LogIt("Starting Norm Eval");     
	  double ans = modelEvaluateNormWorker(tileId, x, y);
	  cmdLog.LogIt("Finished Norm Eval");     
	  
	  
	  ++MPIMutex;
	  MPI_Send(&ans, 1, MPI_DOUBLE, requester, 21, MPI_COMM_WORLD);
	  --MPIMutex;
	  cmdLog.LogIt("Finished Norm Send");     
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. sent norm = " << ans << "\n";
#endif
	  
	} else if (command == 4) { // tile_and_point model eval call
	  // Treat this as a remote procedure call to evaluate the model at a point
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. tile_and_point eval call\n";
#endif
	  
	  // get x and y for point evaluation... eval
	  double args[3];
	  
	  cmdLog.LogIt("start Recv args");
	  ++MPIMutex;
	  MPI_Recv(args, 3, MPI_DOUBLE, _workerReceiveFrom, 22, MPI_COMM_WORLD, &status);
	  --MPIMutex;
	  int requester = status.MPI_SOURCE;
	  int tileId = (int) args[0];
	  double x = args[1];
	  double y = args[2];
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. tile_and_point eval: x = "<< x
	       << ", y = "<< y << ", tileId = "<< tileId << "\n";
#endif
	  
	  cmdLog.LogIt("modelEvalStart");
	  vector<double> z = modelEvaluateWorker(tileId, x, y, cmdLog);
	  cmdLog.LogIt("modelEvalEnd");
	  
	  // copy into dumb C-style buffer for MPI.
	  int zsize = z.size();
	  double *zcopy = new double[z.size()];
	  for(int i=0; i<zsize; i++){zcopy[i] = z[i];}        
	  
	  cmdLog.LogIt("Starting Send");
	  ++MPIMutex;
	  // todo. Make slaveToWorld a function with checking
	  MPI_Send(zcopy, z.size(), MPI_DOUBLE, requester, 23, MPI_COMM_WORLD);
	  --MPIMutex;
	  
	  cmdLog.LogIt("Sent");
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. sent eval, size="<< z.size()
	       << ", z[0] = " << zcopy[0] << "\n";
	  if (z.size()>1){
	    cout << "Process " << myid << ":enslaveWorker. sent eval, size="<< z.size() 
		 << ", z[1] = " << zcopy[1] << "\n";
	  }
#endif
	  
	  delete [] zcopy;
	  
	} else if (command == 5){ // set norm call
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. set norm call\n";
#endif
	  
	  double norm;
	  
	  ++MPIMutex;
	  MPI_Recv(&norm, 1, MPI_DOUBLE, _workerReceiveFrom, 24, MPI_COMM_WORLD, &status);
	  --MPIMutex;
	  
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. norm = "<< norm << "\n";
#endif
	  
	} else if (command == 6){ // set master. Block on commands from root or tilemaster?
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. change receive call to" 
	       << newWorkerReceiveFrom << "\n";
#endif
	  
	  int newWorkerReceiveFrom;
	  ++MPIMutex;
	  MPI_Recv(&newWorkerReceiveFrom, 1, MPI_INT, _workerReceiveFrom, 25, MPI_COMM_WORLD, &status);
	  --MPIMutex;
	  
	  _workerReceiveFrom = newWorkerReceiveFrom;
	  
#ifdef MPITS_FINE_TRACE
	  cout << "Process " << myid << ":enslaveWorker. change receive call to" 
	       << newWorkerReceiveFrom << "\n";
#endif
	  
	} else {
	  cout << "Illegal 'Command' Value = " << command << "\n";
	  exit(1);
	}
	
	cmdLog.LogIt("End Command");
	
      }
      
    }  // end while
    
  } // end while
  
}

LikeState BIE::LikelihoodComputationMPITP::compute_likelihood_MPI
(State& sb, int indx)
{
  LikeState ans;
  
  if (parallel_type==0) ans = compute_likelihood_MPI_tile(sb, indx);
  if (parallel_type==1) ans = compute_likelihood_MPI_intg(sb, indx);
  if (parallel_type==2) ans = compute_likelihood_MPI_tile_and_point(sb, indx);
  
  return ans;
}

// only runs on ROOT processor (0)
void BIE::LikelihoodComputationMPITP::get_working(State& s, int indx, int pflag)
{
  int go=2;
  
  if (pflag) set_frontier(1);
  
  if (!pflag) go = encode_command(go);
  
  if (myid) {
    cerr << "Process " << myid << " in get_working . . . oops!\n";
  }
  
  // Pack buffer
  unsigned MM = s.M();
  StateToBuffer(s, &buff[0], size);
  
  for (int i=1; i<numprocs; i++) {
    // Send signal to compute
    ++MPIMutex;
    MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
    
    // Send buffer
    MPI_Send(&buff[0], size, MPI_DOUBLE, i, 12, MPI_COMM_WORLD);
    MPI_Send(&indx, 1, MPI_INT, i, 17, MPI_COMM_WORLD);
    MPI_Send(&MM, 1, MPI_UNSIGNED, i, 18, MPI_COMM_WORLD);
    
    --MPIMutex;
  }
  
  // for performing pause/resume on root node
  if (!pflag) decode_command(go);
}

void BIE::LikelihoodComputationMPITP::set_frontier(int pflag)
{
  int go = CURRENT_FRONTIER;
  if (pflag) go = PRIOR_FRONTIER;
  
  for (int i=1; i<numprocs; i++) {
    ++MPIMutex;
    MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
    --MPIMutex;
  }
}

void BIE::LikelihoodComputationMPITP::free_workers(void)
{
  if (myid==0) {
    int stop=0;
    for (int i=1; i<numprocs; i++) {
      ++MPIMutex;
      MPI_Send(&stop, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
      --MPIMutex;
    }
    
#ifdef DEBUG
    cerr << "Process " << myid << ": workers sent the quit signal\n";
#endif
  } else {
    // Let the captured processes run
    // so that they can free themselves from 
    // their execution loop and exit
    childSem.post();
  }
  
  
}

// runs only on root node
double BIE::LikelihoodComputationMPITP::LikelihoodPrevious(State& s, int indx)
{
  if (GetSimulation()->priorFrontier() == 0) return 0.0;
  
  if (myid) {
    
    childSem.post();		// Release the workers
    
    parentSem.wait();		// Wait until workers are done
    
    return 0.0;
  }
  
  get_working(s, indx, 1);
  
  // Install the old frontier
  setPriorFrontier();
  
  // tilemaster on root node has the final answer
  LikeState ret = compute_likelihood_MPI(s, indx);
  
  // Restore the current frontier
  setCurrentFrontier();
  set_frontier(0);
  
  
  if (!ret.good) throw ImpossibleStateException(__FILE__, __LINE__);
  
  
  return ret.value;
}


void BIE::LikelihoodComputationMPITP::step_complete()
{
  int go = STEP_COMPLETE;
  
  for (int i=1; i<numprocs; i++) {
    ++MPIMutex;
    MPI_Send(&go, 1, MPI_INT, i, 11, MPI_COMM_WORLD);
    --MPIMutex;
  }
}


// runs only on root node
double BIE::LikelihoodComputationMPITP::Likelihood(State& s, int indx)
{
  if (myid) {
    
    childSem.post();		// Let the workers do something
    
#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": child released for one step,"
	 << " waiting with counts=" 
	 << MPIMutexEnter << ", " << MPIMutexLeave << endl;
    --IOMutex;
#endif
    
    parentSem.wait();		// Wait until work is done
    
#ifdef DEBUG
    ++IOMutex;
    oerr << "Process " << myid << ": parent leaving with counts="
	 << MPIMutexEnter << ", " << MPIMutexLeave << endl;
    --IOMutex;
#endif
    
    return 0.0;
  }
  
  if (counter++ == NLOAD) load_balance();
  likelihood_count++;
  
  // DEBUG showstates
  if (debugflags & showstates) GetSimulation()->ReportState();
  
  // Next call turns some into tilemasters and some into slaves for tile masters
  get_working(s, indx, 0);  
  
  // tilemaster on root node has the final answer
  LikeState ans = compute_likelihood_MPI(s, indx);
  
  // Signal that is done!
  step_complete();
  
  if (!ans.good) throw ImpossibleStateException(__FILE__, __LINE__);
  
  
  return ans.value;
}


LikeState BIE::LikelihoodComputationMPITP::compute_likelihood_MPI_tile(State& s, int indx)
{
  LikeState ans = {0.0, true};
  
  double norm0=0.0, norm=0.0;
  double val=0.0;
  int bval1=0, bval=0;
  int ibeg, iend;
  Tile *t;
  SampleDistribution *sd;
  
  // copy some stuff from the simulation
  Model* _model = GetModel();
  BaseDataTree* _dist = GetBaseDataTree();
  Integration* _intgr = GetIntegration();
  LikelihoodFunction* LP = GetLikeProb();
  
  int nitems = _dist->NumberItems();
  vector<double> z;
  
#if DEBUG
  cerr << "compute_likelihood_MPI_tile: entered\n";
#endif
  
  _model->Initialize(s);	// E.g. normalization . . .
  
  ibeg = (int)(beg*nitems + 0.5);
  iend = (int)(end*nitems + 0.5);
  
  if (ibeg<iend) {
    
    
    MPL_start_timer();		// Begin timing . . . 
    
    
				// Compute model normalzation
    
    _dist->Reset();
    for (int i=0; i<ibeg; i++) _dist->Next();
    _model->ResetCache();
    for (int i=ibeg; i<iend; i++) {
      norm += _intgr->NormValue(_model, _dist->CurrentTile(), 
				_dist->CurrentItem());
      _dist->Next();
    }
    
    MPL_stop_timer();		// Stop timing
  }
  
  ++MPIMutex;
  MPI_Allreduce(&norm, &norm0, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  --MPIMutex;
  
  // Bad state
  if (norm0<=0.0) {
    // Print it out
    if (debugflags & badcell) {
      ostream cout(checkTable("debug_output"));
      
      cout << "Process " << myid << ": badstate, norm=" << norm0 << ": ";
      for (unsigned int i=0; i<s.size(); i++) cout << setw(12) << s[i];
      cout << "\n";
    }
    
    bval1 = 1;
  }
  
  TessToolSynchronize();
  
  // Don't bother computing likelihood for
  // bad state
  if (ibeg<iend && norm0>0.0) {
    
    // Compute log likelihood
    _dist->Reset();
    for (int i=0; i<ibeg; i++) _dist->Next();
    _model->ResetCache();
    
    if (sampleNext==1) {
      InitializeTessToolTileDump();
      if (ibeg >= iend) {
	sampleNext=0;
	FinalizeTessToolTileDump();
      }
    }
    
    for (int i=ibeg; i<iend; i++) {
      
      t = _dist->CurrentTile();
      sd = _dist->CurrentItem();
      
      z = _intgr->Value(_model, t, sd);
      
      try {
	
	val += LP->LikeProb(z, sd, _dist->Total()/norm0, t, &s, indx);
	
      } catch (ImpossibleStateException &e) {
	
	bval1 = 1;
	
      }
      
      // Tile diagnostics 
      // (NB: this is the place for TessTool . . .)
      if (DEBUG_LIKE) {
	vector<double> zz = z;
	for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm0;
	TileDump(t, sd, zz);
      }
      
      if (sampleNext==1) {
	vector<double> zz = z;
	for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm0;
	TessToolTileDump(t, sd, zz);
      }
      
      _dist->Next();
    }
    
    if (sampleNext==1) {
      sampleNext=0;
      FinalizeTessToolTileDump();
    }
    
    MPL_stop_timer();		// Stop timing
  }
  else if (sampleNext==1) {
    InitializeTessToolTileDump();
    FinalizeTessToolTileDump();
    sampleNext=0;
  }
  
  
  
  ++MPIMutex;
  MPI_Reduce(&val, &ans.value, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&bval1, &bval, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  --MPIMutex;
  
  ans.good = (bval==0) ? true : false;
  
  DEBUG_LIKE = false;
  
  return ans;
}

LikeState BIE::LikelihoodComputationMPITP::compute_likelihood_MPI_intg(State& s, int indx)
{
  ostream cout(checkTable("simulation_output"));
  
#if DEBUG
  cerr << "compute_likelihood_MPI_intg: entered\n";
#endif
  
  LikeState ans = {0.0, true};
  double norm0=0.0, norm=0.0;
  double val=0.0;
  int bval1=0, bval=0;
  Tile *t;
  SampleDistribution *sd;
  
  // copy some stuff from the simulation
  Model* _model = GetModel();
  BaseDataTree* _dist = GetBaseDataTree();
  Integration* _intgr = GetIntegration();
  LikelihoodFunction* LP = GetLikeProb();
  
  int nitems = _dist->NumberItems();
  vector<double> z;
  
  
  _model->Initialize(s);	// E.g. normalization . . .
  
  
				// Interaction list
  int irank, nl;
  typedef vector<int> ivec;
  static vector<ivec> intlist;
  static vector<int> ranklist;
  
  // Temporary allocation/reallocation
  // Move to separate member function
  static int nitems0 = 0;
  static MPI_Group world_group;
  static MPI_Comm *comm = NULL;
  static MPI_Group *group = NULL;
  
  // Reallocate?
  if (nitems0 != nitems) {
    
    ++MPIMutex;
    
    if (nitems0) {
      for (int i=0; i<nitems0; i++) {
	if (comm[i]) MPI_Comm_free(&comm[i]);
	if (group[i]) MPI_Group_free(&group[i]);
      }
      delete [] comm;
      delete [] group;
    } else 
      MPI_Comm_group(MPI_COMM_WORLD, &world_group);
    
    // Set nsub
    
    nsub = max<int>( (int)((double)numprocs/nitems+0.5), 1 );
    
    // Make interaction list
    
    intlist = vector<ivec>(nitems);
    int item, value;
    int count=0;
    for (int i=0; i<nitems*nsub; i++) {
      item = (int)floor( static_cast<double>(i / nsub));
      value = count++ % numprocs;
      intlist[item].push_back(value);
    }
    
    if (myid==0) {
      cout << "\nPartition\n";
      cout << "----------\n";
      for (int i=0; i<nitems; i++) {
	cout << i << "> ";
	for (int j=0; j<(int)intlist[i].size(); j++)
	  cout << " " << intlist[i][j];
	cout << "\n";
      }
      cout << "\n";
    }
    // Reassign groups and communicators
    nitems0 = nitems;
    group = new MPI_Group [nitems];
    comm = new MPI_Comm [nitems];
    
    for (int i=0; i<nitems; i++) {
      nl = intlist[i].size();
      int *glist = new int [nl];
      for (int j=0; j<nl; j++) glist[j] = intlist[i][j];
      int ret = MPI_Group_incl(world_group, nl, glist, &group[i]);
      if (MPI_SUCCESS != ret) cout << "Process " << myid << ": group failed\n";
      MPI_Comm_create(MPI_COMM_WORLD, group[i], &comm[i]);
      delete [] glist;
    }
    
    // Make rank list
    ranklist = vector<int>(nitems);
    int ircnt=0;
    for (int i=0; i<nitems; i++) {
      ranklist[i] = -1;
      
      for (int j=0; j<(int)intlist[i].size(); j++) {
	if (intlist[i][j] == myid) {
	  MPI_Comm_rank(comm[i], &irank);
	  ranklist[i] = irank;
	  ircnt++;
	}
      }
    }
    --MPIMutex;
    
  }
  
  // for use by TessToolsender
  _numTMs = nitems;
  _tmList = vector<int>(_numTMs);
  for (int i=0; i<nitems; i++) {
    // the rank of root node in sub-group
    /*
      nl = intlist[i].size();
      int *glist = new int [nl];
      for (int j=0; j<nl; j++) glist[j] = intlist[i][j];
      int sroot;
      for (int j=0; j<nl; j++) {
      MPI_Comm_rank(group[i], glist[j], &sroot);
      if(sroot == 0) break;
      }
      _tmList[i] = sroot;
    */
    _tmList[i] = intlist[i][0];
  }
  
  // Compute model normalzation
  
  _dist->Reset();
  _model->ResetCache();
  for (int i=0; i<nitems; i++) {
    
    if (ranklist[i] > -1)
      norm += _intgr->NormValue(_model, _dist->CurrentTile(),
				_dist->CurrentItem(),
				ranklist[i], intlist[i].size() );
    
    _dist->Next();
  }
  
  ++MPIMutex;
  MPI_Allreduce(&norm, &norm0, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  --MPIMutex;
  
  // Bad state
  if (norm0<=0.0) {
    // Print it out
    if (debugflags & badcell) {
      ostream cout(checkTable("debug_output"));
      cout << "Process " << myid << ": badstate, norm=" << norm0 << ": ";
      for (unsigned int i=0; i<s.size(); i++) cout << setw(12) << s[i];
      cout << "\n";
    }
    
    bval1 = 1;
    
  }
  
  
  // Compute log likelihood
  _dist->Reset();
  _model->ResetCache();
  
  TessToolSynchronize();
  if (sampleNext==1) {
    InitializeTessToolTileDump();
  }
  
  for (int i=0; i<nitems; i++) {
    
    // Skip computation if bad state
    if (ranklist[i] > -1 && norm0>0.0) {
      
      t = _dist->CurrentTile();
      sd = _dist->CurrentItem();
      
      z = _intgr->Value(_model, t, sd, ranklist[i], 
			intlist[i].size(), comm[i]);
      
      if (ranklist[i]==0) {
	
	val += LP->LikeProb(z, sd, _dist->Total()/norm0, t, &s, indx);
	
      }
      
      // Tile diagnostics 
      // (NB: this is the place for TessTool . . .)
      if (DEBUG_LIKE && ranklist[i]==0)  TileDump(t, sd, z);
      if (sampleNext==1) {
	vector<double> zz = z;
	for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm0;
	TessToolTileDump(t, sd, zz);
      }
      
    }
    
    _dist->Next();
    
  }
  if (sampleNext==1) {
    sampleNext=0;
    FinalizeTessToolTileDump();
  }
  
  ++MPIMutex;
  MPI_Reduce(&val, &ans.value, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&bval1, &bval, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  --MPIMutex;
  
  ans.good = (bval==0) ? true : false;
  
  DEBUG_LIKE = false;
  
  return ans;
}

// Newer simulation stuff

// This is an evaluate function that is intended to be called from a
// multithreaded environment inside an integrator.  The calling thread
// will block until a reply is available

double BIE::LikelihoodComputationMPITP::normEvaluate
(Model* model, Tile* tile, double x, double y,
 SampleDistribution* sampleDistribution)
{
  // Adapt the model norm evaluate function so that it can be
  // evaluated on a different processor.
  
  // If it's not worth farming it out (for example, no free
  // processors). Just do it here in the current thread. Also do it
  // here if we're in "tile" mode.
  
  double value;
  
  if (false){  // do it here
    TSLog evLog;
    value = model->NormEval(x,y, sampleDistribution);
    evLog.LogIt("Finished Local Norm Eval");
    evLog.PrintIt();
    
    
  } else {  // do it on some other processor
    
    // There *has* to be a better way to get the tileID
    int tileId = tile->GetNode()->ID();
    
    Semaphore finished;
    NormEvaluationRequest request(tileId, x, y, finished, value);
    request.LogIt("Starting Remote Norm Eval");
    RemoteEvaluation(request);  // blocks until remote call returns value
    request.LogIt("Finished Remote NormEval");
    
    {
      request.LogIt("Starting Local Norm Eval");
      double localValue = model->NormEval(x,y, sampleDistribution);
      request.LogIt("Finished Local Norm Eval");
      
      if (value == localValue)
	request.LogDouble("Values match! ", value);
      else
	request.LogIt("Values don't match!");
      
    }
    
    request.PrintLog();
    
  }
  
  return value;
}

// adapter function
//
vector<double> 
BIE::LikelihoodComputationMPITP::modelEvaluate
(
 Model *model, Tile *tile, 
 double x, double y, SampleDistribution* sampleDistribution)
{
  // Adapt the model evaluate function so that it can be evaluated on
  // a different processor
  
  // If it's not worth farming it out (for example, no free
  // processors) just do it here in the current thread. Also do it
  // here if we're in "tile" mode.
  
  
  vector<double> z(sampleDistribution->numberData());
  
  if (false){  // do it here
    z = model->Evaluate(x,y, sampleDistribution);
    
  } else{  // do it on some other processor
    
    // There *has* to be a better way to get the tileID
    int tileId = tile->GetNode()->ID();
    
    Semaphore finished;
    ModelEvaluationRequest request(tileId, x,y, sampleDistribution->numberData(), finished, z);
    
    RemoteEvaluation(request);
    
  }
  
  return z;
}

/**
 *  Convenience method to evaluate a list of points in parallel
 *  Blocks until all are complete
 */
void  
BIE::LikelihoodComputationMPITP::normEvaluateList
(Model *model, Tile *tile, 
 const vector<double>& x,
 const vector<double>& y,
 SampleDistribution* sampleDistribution,
 vector<double>& evaluations)
{
  
  // short circuit for testing; later for optimization.
  //
  static bool inProcess=false; 
  
  int numPoints = evaluations.size();
  
  if (inProcess){
    for (int i=0; i<numPoints; i++){
      TSLog evalLog;
      evalLog.LogIt("Start Norm Eval");
      evaluations[i] = model->NormEval(x[i], y[i], sampleDistribution);
      evalLog.LogIt("End norm Eval");
      // evalLog.PrintIt();
    }
    
  } else {
    
    // TODO? There has to be a better way to do this!
    int tileId = tile->GetNode()->ID(); 
    
    Semaphore finishedRequests;
    
    vector< EvaluationRequest*> requestList(numPoints);
    
    for (int i=0; i<numPoints; i++){
      requestList[i] = new NormEvaluationRequest(tileId, x[i], y[i],
						 finishedRequests, evaluations[i]);
      
      tileRequestHandlerThread->enqueueRequest(requestList[i]);
      requestList[i]->LogIt("Enqueue Finished");
      requestList[i]->LogIt("Vacuous Log Message");
    }
    
    // Do "N" waits.  
    //
    for(int i=0; i<numPoints; i++){
#ifdef MPITS_FINE_TRACE
      cout << "MPITS.normEvaluate list. waiting for request " << i
	   << " of " << numPoints << "\n";
#endif
      
      finishedRequests.wait();
      
#ifdef MPITS_FINE_TRACE
      cout << "MPITS.normEvaluate list. finished wait for request " << i
	   << " of " << numPoints << "\n";
#endif
    }
    
    // delete requests
    //
    for(int i=0; i<numPoints; i++){
      // requestList[i]->PrintLog();
      delete requestList[i];
    }
  }
  numModelEvals += numPoints;
}

/**
   Convenience method to evaluate a list of points in parallel
   Blocks until all are complete
*/
void BIE::LikelihoodComputationMPITP::modelEvaluateList
(Model *model, Tile *tile, 
 const vector<double>& x,
 const vector<double>& y,
 SampleDistribution* sampleDistribution,
 vector< vector<double> >& evaluations)
{
  // Short circuit for testing; later for optimization.
  static bool inProcess=false;
  
  int numPoints = evaluations.size();
  
  if (inProcess){
    for (int i=0; i<numPoints; i++){
      TSLog evalLog;
      evalLog.LogIt("Start Model Eval");
      evaluations[i] = model->Evaluate(x[i], y[i], sampleDistribution);
      evalLog.LogIt("End Model Eval");
      //evalLog.PrintIt();
    }
    
  } else {
    
    // TODO? There has to be a better way to do this!
    //
    int tileId = tile->GetNode()->ID(); 
    
    Semaphore finishedRequests;
    
    vector< EvaluationRequest*> requestList(numPoints);
    
    for (int i=0; i<numPoints; i++){
      requestList[i] = new ModelEvaluationRequest(tileId, x[i], y[i],
						  sampleDistribution->numberData(), 
						  finishedRequests, evaluations[i]);
      
      tileRequestHandlerThread->enqueueRequest(requestList[i]);
      requestList[i]->LogIt("Enqueue Finished");
      requestList[i]->LogIt("Vacuous Log Message");
    }
    
    // Do "N" waits.  
    for(int i=0; i<numPoints; i++){
#ifdef MPITS_FINE_TRACE
      cout << "MPITS.modelEvaluate list. waiting for request " << i
	   << " of " << numPoints << "\n";
#endif
      
      finishedRequests.wait();
      
#ifdef MPITS_FINE_TRACE
      cout << "MPITS.modelEvaluate list. finished wait for request " << i
	   << " of " << numPoints << "\n";
#endif
    }
    
    // Verify remote results against local ones..
#ifdef MODEL_EVALUATE_LIST_VERIFY_RESULTS
    {
      int ssize = sampleDistribution->numberData();
      vector<double> ans(ssize);
      
      for (int i=0; i<numPoints; i++){
	
	requestList[i]->LogIt("Start sanity model eval");
	ans = model->Evaluate(x[i], y[i], sampleDistribution);
	requestList[i]->LogIt("End sanity model eval");
	
	for (int j=0; j<ssize; j++){
	  if (ans[j] != evaluations[i][j] ){
	    requestList[i]->LogIt("Remote evaluation doesn't match");
	    cout <<"Point " << i << "in process and remote process results disagree!\n";
	    cout << "inProcess["<< j << "] = " << ans[j] << "Remote = "
		 << evaluations[i][j] << "\n";
	    
	    requestList[i]->PrintLog();
	  } else {
	    requestList[i]->LogDouble("Model They match!",value);
	  }
	}
      }
    }
#endif
    
    // delete requests
    for(int i=0; i<numPoints; i++){
      //requestList[i]->PrintLog();
      delete requestList[i];
    }
  }
  numModelEvals += numPoints;
}

/**
   Evaluate the model at a point or compute the norm on a remote
   processor.  Only called on tile master processors.
 */
void BIE::LikelihoodComputationMPITP::RemoteEvaluation
(EvaluationRequest &request)
{
  tileRequestHandlerThread->enqueueRequest(&request);
  
  request.WaitForCompletion();  // wait for result
  
}


/**
   Two level Parallelization based on tiles, and then pointevaluators
   assigned to each tile.  On entry, our processor is already
   "slaved". It's a server waiting for commands.  If we're here, we've
   gotten the command to compute.

   Root node doesn't participate.  CHECK THIS!
 */
LikeState 
BIE::LikelihoodComputationMPITP::compute_likelihood_MPI_tile_and_point
(State& s, int indx)
{
  
  LikeState ans = {0.0, true};
  double norm0=0.0, norm=0.0;
  double val=0.0;
  int bval1=0, bval=0;
  int ibeg, iend;
  Tile *t;
  SampleDistribution *sd;
  
  // copy some stuff from the simulation
  //
  Model* _model = GetModel();
  BaseDataTree* _dist = GetBaseDataTree();
  Integration* _intgr = GetIntegration();
  LikelihoodFunction* LP = GetLikeProb();
  
  int numTiles = _dist->NumberItems(); // number of Tiles on the frontier
  vector<double> z;
  
#if MPITS_FINE_TRACE
  cerr << "compute_likelihood_MPI_tile_and_point: entered\n";
#endif
  
  
  // figure out tiles and groups
  // first figure out tiles
  
  // DO WE NEED TO NORMALIZE EVERY TIME????? 
  _model->Initialize(s);	// E.g. normalization . . .
  
  
  /* compute beginning and ending index of tiles assigned to this processor

     beg = my rank in comm_world/numprocs
     end = rank of myid+1 in comm_world/numprocs

     if numtiles>numprocs, ibeg = first tile for my processor, iend =
     last+1 tile for me

     if numtiles<numprocs  ibeg = tile group, iend = mostly ibeg

     ibeg = (int)(beg*numTiles + 0.5);
     iend = (int)(end*numTiles + 0.5);
  */
  
  // size of group including tile groupmaster. Always at least 1 (the
  // groupmaster) 
  //
  double averageGroupSize = max<double>(((double)numprocs)/numTiles,1);
  
  // OVERRIDE! Force all procs to one group for debugging
  // averageGroupSize = numprocs;
  
  int myTileGroupNumber = static_cast<int>( myid/averageGroupSize);
  int myTileGroupMaster = static_cast<int>( ceil(myTileGroupNumber*averageGroupSize));

  // true iff I am the master
  bool tileGroupMaster = (myid == myTileGroupMaster);

  
  /* compute beginning and ending index of tiles assigned to this tile Group

     beg = my rank in comm_world/numprocs
     end = rank of myid+1 in comm_world/numprocs

     if numtiles>numprocs, ibeg = first tile for my processor, iend =
     last+1 tile for me

     if numtiles<numprocs  ibeg = tile group, iend = mostly ibeg

     ibeg = (int)(beg*numTiles + 0.5);
     iend = (int)(end*numTiles + 0.5);
  */
  
  int nextTileGroupMaster = static_cast<int>(ceil((myTileGroupNumber+1)*averageGroupSize)); 
  int numTileGroups = static_cast<int>( (numprocs-1)/averageGroupSize)+1;
  
  ibeg = (int)( (((double)myTileGroupNumber)/numTileGroups) * numTiles + 0.5);
  iend = (int)( (((double)myTileGroupNumber+1)/numTileGroups)*numTiles + 0.5);
  
  int myTileGroupSize = nextTileGroupMaster - myTileGroupMaster;
  
  vector<int> groupIdToSystemId(myTileGroupSize, -1); // init to -1
  vector<int> systemIdToGroupId(numprocs, -1);
  
  // make a group of tile masters, so we can do reduces
  //
  int *tileMasterIds = new int[numprocs];
  int numTileMasters=0;
  for (int i=0; i<numprocs; i++){
    int tileGroupNumber = static_cast<int>(i/averageGroupSize);
				// if I am myTileGroupMaster
    if ( i == ceil(tileGroupNumber*averageGroupSize) ){
      tileMasterIds[numTileMasters++] = i;
    }
  }
  
  // for use by TessToolsender
  
  _numTMs = numTileMasters;
  _tmList = vector<int>(_numTMs);
  for(int i=0; i<_numTMs;i++) {
    _tmList[i] = tileMasterIds[i];
  }
  
  // make comm group
  ++MPIMutex;
  MPI_Group groupWorld;
  MPI_Group tileMasterGroup;
  MPI_Comm tileMasterComm;
  MPI_Comm_group(MPI_COMM_WORLD, &groupWorld);
  
  MPI_Group_incl(groupWorld, numTileMasters, tileMasterIds, &tileMasterGroup);
  MPI_Comm_create(MPI_COMM_WORLD, tileMasterGroup,&tileMasterComm); 
  
  
  delete [] tileMasterIds;

  // groupWorld deleted below, after if, because slave code needs it
  //
  MPI_Group_free(&tileMasterGroup);
  MPI_Group_free(&groupWorld);
  
  --MPIMutex;
  
  
  // _lastAverageGroupSize = averageGroupSize;
  
  
  Semaphore numberSlavesReady;
  
  if (tileGroupMaster){   // run the norm and value calculations
    
    MPL_start_timer();		// Begin timing . . . 
    
    
    // Start up helper threads
    
    // Set up mappings from in-group processorIds ,which are used by
    // the simulation, and in-system processorId, which are used by
    // MPI.

    for(int i=0; i<myTileGroupSize; i++){
      groupIdToSystemId[i] = myTileGroupMaster + i;
      systemIdToGroupId[myTileGroupMaster+i] = i;
    }
    
    cout.flush();

    // create queue for all tile slaves, start listening for free slaves
    //
    if (tileRequestHandlerThread == 0){
      tileRequestHandlerThread = 
	new TileRequestHandlerThread(myTileGroupSize,
				     &groupIdToSystemId,
				     &systemIdToGroupId,
				     0, 
				     &numberSlavesReady);
      
#ifdef  DEBUG
      cout << "MPITS: Tile request Handler Thread created = " << tileRequestHandlerThread << "\n";
#endif
      
      int startStatus = tileRequestHandlerThread->start();  // start 'er up
      
      if (startStatus != 0){
	cout << "LikelihoodComputationMPITP. Unable to start TileRequestHandlerThread. Aborting\n";
	exit(1);
      }
      
#ifdef  MPITS_FINE_TRACE
      cout << "LikelihoodComputationMPITP: Started up tilehandlerRequestthread\n";
#endif
    } else {  
      // thread already started; put it in an initial, clean
      // state. Thread is already Paused.
      //
      tileRequestHandlerThread->reInitialize(myTileGroupSize,
					     &groupIdToSystemId,
					     &systemIdToGroupId,
					     0,
					     &numberSlavesReady);  
      
      tileRequestHandlerThread->unPause();  
      
      
#ifdef  MPITS_FINE_TRACE
      cout << "LikelihoodComputationMPITP: ReStarted tilehandlerRequestthread\n";
#endif
    }
    
    // TileMPIThread  
    //
    // thread exists to channel all incoming messages through a single
    // point, because LAM MPI is not currently thread safe.
    //
    if (tileMPIThread == 0){
      tileMPIThread = new TileMPIThread(tileRequestHandlerThread);
      int startStatus = tileMPIThread->start();
      if (startStatus != 0){
	cout << "LikelihoodComputationMPITP. Unable to start TileMPIThread. Aborting\n";
	exit(1);
      }
      
      // Optimization. Pause MPI thread if there are no slaves to send messages
      //
      if (tileRequestHandlerThread->noMPISlaves()){ 
	tileMPIThread->pause();
      }
    } else {  
      // Thread already exists and is paused; reset it and restart it.
      //
      tileMPIThread->reInitialize(tileRequestHandlerThread);
      
      // Optimization. Leave MPI thread paused if there are no slave to send
      // messages
      //
      if (!tileRequestHandlerThread->noMPISlaves()){ 
	tileMPIThread->unPause();
      }
    }
    
    
    // CREATE_TILE_MASTER_THREAD
    //
    if (tileMasterWorkerThread == 0){
      
      tileMasterWorkerThread = new TileMasterWorkerThread(this);
      tileRequestHandlerThread->setTileMasterWorkerThread(tileMasterWorkerThread);
      
      int startStatus = tileMasterWorkerThread->start();
      if (startStatus != 0){
	cout << "LikelihoodComputationMPITP. Unable to start TileMasterWorkerThread. Aborting\n";
	exit(1);
      }
    } else {
      tileMasterWorkerThread->reInitialize(this);
      tileRequestHandlerThread->setTileMasterWorkerThread(tileMasterWorkerThread);
      tileMasterWorkerThread->unPause();
    }
    
    // Wait for all slaves to check in:
    // o Slaves send a message when they wake up
    // o It is matched to an init Request

    for (int i=0; i<myTileGroupSize-1; i++){
      numberSlavesReady.wait();
    }

    // Compute model normalzation
    
    _dist->Reset();
    for (int i=0; i<ibeg; i++) _dist->Next();
    _model->ResetCache();
    for (int i=ibeg; i<iend; i++) {

      // Bad mojo: the tile-point parallelism stuff uses the _dist
      // iterator to figure out what tile the integrator is in, when
      // the request is sent to another process.  This works okay so
      // long as the integrator is only ever called with
      // _dist->CurrentItem as the distribution.

      // norm += _intgr->NormValue(_model, _dist->CurrentTile(),
      //	      		   _dist->CurrentItem());
      
      norm += _intgr->NormValue(this, _model, _dist->CurrentTile(), 
				_dist->CurrentItem());
      
      _dist->Next();
    }
    
    MPL_stop_timer();		// Stop timing
    
    
    // This is a reduce across all tileMasters.  All processors must
    // have the correct norm value before *any* can proceed with model
    // evaluations.
    ++MPIMutex;
    MPI_Allreduce(&norm, &norm0, 1, MPI_DOUBLE, MPI_SUM, tileMasterComm);
    
    // Send norm to members of tileGroup
    for(int i=1; i<myTileGroupSize; i++){ // Don't send to self!
      int command=SET_NORM_COMMAND;

      // todo: combine these!

      // todo: MAKE SURE THIS WORKS FOR MUTANT TILE GROUP SIZES (like
      // the one left over if it doesn't diveide evenly!)
      
#ifdef  MPITS_FINE_TRACE
      cout << "MPITS. Sending norm (tag24) from "<< myid << " to " << myid+i << "\n";
#endif
      MPI_Send(&command, 1, MPI_INT,  myid+i, COMMAND_MSG_TAG, MPI_COMM_WORLD);
      MPI_Send(&norm0, 1, MPI_DOUBLE, myid+i, 24, MPI_COMM_WORLD);
    }
    --MPIMutex;
    
  }
  
  // Now all processors have correct norm computed across all tiles
  
  // Bad state
  if (norm0<=0.0) {
    // Print it out
    if (debugflags & badcell) {
      ostream cout(checkTable("debug_output"));
      
      cout << "Process " << myid << ": badstate, norm=" << norm0 << ": ";
      for (unsigned int i=0; i<s.size(); i++) cout << setw(12) << s[i];
      cout << "\n";
    }
    
    bval1 = 1;
    
  }
  
  //
  // End Norm compute phase
  //
  
  
  TessToolSynchronize();
  
  // Don't bother computing likelihood for
  // bad state
  //  if (ibeg<iend && norm0>0.0) {
  
  if (tileGroupMaster){
    if (norm0 > 0.0) {
      // Compute log likelihood
      _dist->Reset();
      for (int i=0; i<ibeg; i++) _dist->Next();
      
      _model->ResetCache();
      
      if (sampleNext==1) {
	InitializeTessToolTileDump();
      }
      for (int i=ibeg; i<iend; i++) {
	
	t = _dist->CurrentTile();
	sd = _dist->CurrentItem();
	
	z = _intgr->Value(this, _model, t, sd);
	
	try {
	  val += LP->LikeProb(z, sd, _dist->Total()/norm0, t, &s, indx);
	} catch (ImpossibleStateException &e) {
	  bval1 = 1;
	}
	
	
#ifdef MPITS_FINE_TRACE
	cout << "MPITS: tilemaster " << myid << " z[0]=" << z[0] << " val = "<< val << "\n";
#endif
	
	// Tile diagnostics 
	// (NB: this is the place for TessTool . . .)
	if (DEBUG_LIKE) TileDump(t, sd, z);
	
	if (sampleNext==1) {
	  vector<double> zz = z;
	  for (unsigned i=0; i<zz.size(); i++) zz[i] *= _dist->Total()/norm0;
	  TessToolTileDump(t, sd, zz);
	}
	
	_dist->Next();
	
      } // end for each tile
      
      if (sampleNext==1) {
	sampleNext=0;
	FinalizeTessToolTileDump();
      }
      
    } // end norm >0
    
    // All tilemasters now have a valid value
    
    
    // Clean up everything.  Do this before the reduce because the reduce is
    // the thing that synchronizes all of the tilemasters and the root.  Once
    // the reduce is over, the root is free to send us messages for another
    // likelihood computation.
    
    // clean up the helper threads.. NOPE. Current version keeps
    // threads around forever.

    // tileMPIThread->stop();  // tell thread to eventually stop and kill itself
    // tileRequestHandlerThread->stop();  // tell thread to stop and kill itself
    
    // put helper threads in known, safe states.
    
    
    if (tileMasterWorkerThread)  tileMasterWorkerThread->resetPause();
    //
    // if there are no MPI slaves then the thread is already paused.
    //
    if (!tileRequestHandlerThread->noMPISlaves()) tileMPIThread->pause();
    tileRequestHandlerThread->resetPause();
    
    DEBUG_LIKE = false;
    
    MPL_stop_timer();		// Stop timing
    
    // Release the slaves from this tile master. They go back (as we do) to
    // listening only to the root
    ++MPIMutex;
    
    for (int i=1; i<myTileGroupSize; i++){ // Don't send to self!
      int command=SET_WORKER_RECEIVE_FROM;
      int newReceiveFrom = 0;
#ifdef  MPITS_FINE_TRACE
      cout << "MPITS. Sending change of worker receive from "<< myid << " to " << myid+i << "\n";
#endif
      MPI_Send(&command, 1, MPI_INT,  myid+i, COMMAND_MSG_TAG, MPI_COMM_WORLD);
      MPI_Send(&newReceiveFrom, 1, MPI_INT, myid+i, 25, MPI_COMM_WORLD);
    }
    
    // Technically, this is a race condition.  We should wait here
    // until the slaves report back so we *know* they are now
    // listening to the root

    --MPIMutex;
    
    // This is a reduce across all tilemasters... after all tiles have
    // been computed.
    
    ++MPIMutex;
    MPI_Reduce(&val, &ans.value, 1, MPI_DOUBLE, MPI_SUM, 0, tileMasterComm);
    MPI_Reduce(&bval1, &bval, 1, MPI_INT, MPI_SUM, 0, tileMasterComm);
    --MPIMutex;
    
    ans.good = (bval==0) ? true : false;
    
#ifdef  MPITS_FINE_TRACE
    cout << "MPITS: tilemaster: after reduce\n";
#endif
    
    
    ++MPIMutex;
    MPI_Comm_free(&tileMasterComm);
    --MPIMutex;
    
    
    
    
  } else {  // end if tile master

    // We are a worker in a tile group.  do initialization So, what?
    // reset the model cache? is that it? And wait for evaluation
    // requests to come in?  TODO aah. I made this up
    
    _model->Initialize(sb); 
    _model->ResetCache();
    
    
    // Change where we get commands from to myTileGroupMaster
    // This is used as the "receive" argument in the main server loop
    _workerReceiveFrom = myTileGroupMaster;
    
    
    // For tile_point style parallelism, announce our availability to
    // our tile master
    int go=0;
    
    ++MPIMutex;
    MPI_Send(&go /*dummy */, 1, MPI_INT, myTileGroupMaster, 19, MPI_COMM_WORLD);
    --MPIMutex;
    
#ifdef  MPITS_FINE_TRACE
    cout << "MPITS: worker " << myid << " sending msg 19 to master " << myTileGroupMaster << "\n";
#endif
    
    // This will return to the main loop in "enslave_worker" and loop there...
  }
  
  
#ifdef  MPITS_FINE_TRACE
  cout << "LikelihoodComputationMPITP:computeTileAndPoint:  returning\n";
#endif
  
  return ans;
  
}


// RPC server functions
//  Use model and dist parameters that were set when slave function
//  started.  Called only on slaves.

//  Evaluate the model norm at a given point in a tile.
//  The model and full distribution are taken from the values given when this
//  class was constructed on the worker process.
//  tileId, x, y come from the tile master on a per request basis.
double BIE::LikelihoodComputationMPITP::modelEvaluateNormWorker
(int tileId, double x, double y)
{
  // get some stuff from the simulation
  Model* _model = GetModel();
  BaseDataTree* _dist = GetBaseDataTree();
  
  SampleDistribution *sd = _dist->getDistribution(tileId);
  
  return _model->NormEval(x,y, sd);
}


//  Evaluate the model norm at a given point in a tile.
//  The model and full distribution are taken from the values given when this
//  class was constructed on the worker process.
//  tileId, x, y come from the tile master on a per request basis.
vector<double> BIE::LikelihoodComputationMPITP::modelEvaluateWorker
(int tileId, double x, double y, TSLog &logger)
{
  
  logger.LogIt("ModelEvaluationWorker entered");
  
  BaseDataTree* _dist = GetBaseDataTree();
  SampleDistribution *sd = _dist->getDistribution(tileId);
  Model* _model = GetModel();
  
  
  logger.LogIt("Got sampledistribution entered");
  
#ifdef  MPITS_FINE_TRACE
  cout << "LikelihoodComputationMPITP.modelEvaluateWorker: tileId = " << tileId << ", numberData="
       << sd->numberData() << "\n";
#endif
  
  vector<double> ans;
  logger.LogIt("Starting model eval");
  ans = _model->Evaluate(x,y, sd);
  logger.LogIt("Finished model eval");
  return ans;
}

int BIE::LikelihoodComputationMPITP::getNumAndIdsOfTileMasters
(int *num, int **idlist)
{
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputationMPITP::getNumAndIdsOfTileMasters numprocs=" << numprocs << endl;
#endif
  int *list;
  if (parallel_type == 0) {
    list = new int[numprocs];
    *num = numprocs;
    for(int i=0; i<numprocs;i++)
      list[i] = i;
    *idlist = list;
#ifdef DEBUG_TESSTOOL
    ferr << "LikelihoodComputationMPITP::getNumAndIdsOfTileMasters returning "
	 << endl;
#endif
    return 0;
  }
  if (parallel_type == 1 || parallel_type == 2) {
    *num = _numTMs;
    *idlist = &_tmList[0];
#ifdef DEBUG_TESSTOOL
    ferr << "LikelihoodComputationMPITP::getNumAndIdsOfTileMasters returning "
	 << endl;
#endif
    return 0;
  }
  return -1;
}

/*
  This const casting is ridiculous 
  but I'm not sure what else to do . . .
*/
void LikelihoodComputationMPITP::pack_and_send() const
{
  if (myid==0) {
    *const_cast<double*>(&beglist[0]) = beg;
    *const_cast<double*>(&endlist[0]) = end;
    for (int n=1; n<numprocs; n++) {
      MPI_Recv(const_cast<double*>(&beglist[n]), 1, MPI_DOUBLE, n, 392, 
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(const_cast<double*>(&endlist[n]), 1, MPI_DOUBLE, n, 393, 
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
  } else {
    MPI_Send(&beg, 1, MPI_DOUBLE, 0, 392, MPI_COMM_WORLD);
    MPI_Send(&end, 1, MPI_DOUBLE, 0, 393, MPI_COMM_WORLD);
  }
}
