#include <cmath>
#include <vector>
#include <iomanip>

#include <Distribution.h>
#include <Tile.h>
#include <PyLikelihoodFunction.h>
#include <BIEmpi.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PyLikelihoodFunction)
BIE_CLASS_EXPORT_IMPLEMENT(BIE::Callback)

//
// The only variable we care about here is <State* s>
//
double PyLikelihoodFunction::LikeProb
(std::vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx)
{
				// Sanity check
  if (si) {
    if (si->Ntot != s->N()) {
      ostringstream ostr;
      ostr << "PyLikelihoodFunction: I expected a parameter vector of size =" 
	   << si->Ntot << " which disagrees with State vector of size " 
	   << s->N() << endl;
      throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
    }
  }

  return cb->Prob(s);
}

const std::string PyLikelihoodFunction::ParameterDescription(int i)
{
  if (si) 
    return si->ParameterDescription(i);
  else {
    std::ostringstream sout;
    sout << "Comp [" << i << "]";
    return sout.str();
  }
}


