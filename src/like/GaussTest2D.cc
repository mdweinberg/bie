
#include <vector>
#include <iomanip>

#include <GaussTest2D.h>
#include <Distribution.h>
#include <Tile.h>
#include <BIEmpi.h>
#include <gfunction.h>
#include <gaussQ.h>
#include <Uniform.h>
#include <Normal.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GaussTest2D)

using namespace std;
using namespace BIE;

//
// Default values for synthetic data, if not specified explicitly in
// the constructor
//
static const unsigned ncomp_def      = 1;
static const unsigned nbins_def      = 64;
static const double   centers_def [] = {0.5,  0.5 };
static const double   variance_def[] = {0.01, 0.02};
static const double   angle_def      = 0.0;

static inline bool isEven(int x) { return !(x&1); }
static inline bool isOdd (int x) { return  (x&1); }

//
// Constructor
//
GaussTest2D::GaussTest2D() :  LikelihoodFunction()
{
  N      = 1000;
  nbins  = nbins_def;
  levels = 1;
  llevel = -1;
  dim    = 2;
  mdim   = 1;
  ncomp  = ncomp_def;
  fixed2 = false;

  vector<double> cen, var;
  for (unsigned n=0; n<2; n++) {
    cen.push_back(centers_def [n]);
    var.push_back(variance_def[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);
  angles.  push_back(angle_def);

  makeSyntheticData();
}

GaussTest2D::
GaussTest2D(int num, int Nbins, int Levels) : LikelihoodFunction()
{
  N      = num;
  nbins  = Nbins;
  levels = max<int>(Levels, 1);
  llevel = -1;
  dim    = 2;
  mdim   = 1;
  ncomp  = ncomp_def;
  fixed2 = false;

  vector<double> cen, var, phi;
  for (unsigned n=0; n<2; n++) {
    cen.push_back(centers_def [n]);
    var.push_back(variance_def[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);
  angles  .push_back(angle_def);

  makeSyntheticData();
}

GaussTest2D::
GaussTest2D(int num, int Nbins, int Levels,
	    clivectord* cen0,
	    clivectord* var0) : LikelihoodFunction()
{
  N      = num;
  nbins  = Nbins;
  levels = max<int>(Levels, 1);
  llevel = -1;
  ncomp  = 1;
  dim    = 2;
  mdim   = 1;
  fixed2 = false;

  //
  // Dimension sanity check
  //
  if ((*cen0)().size() != (*var0)().size() && (*cen0)().size()<2) {
    ostringstream msg;
    msg << "GaussTest2D: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  vector<double> cen, var, phi;
  for (unsigned n=0; n<2; n++) {
    cen.push_back((*cen0)()[n]);
    var.push_back((*var0)()[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);
  angles  .push_back(angle_def);

  makeSyntheticData();
}

GaussTest2D::
GaussTest2D(int num, int Nbins, int Levels,
		vector<double>* cen0,
		vector<double>* var0) : LikelihoodFunction()
{
  N      = num;
  nbins  = Nbins;
  levels = max<int>(Levels, 1);
  llevel = -1;
  ncomp  = 1;
  dim    = 2;
  mdim   = 1;
  fixed2 = false;

  //
  // Dimension sanity check
  //
  if (cen0->size() != var0->size() && cen0->size()<2) {
    ostringstream msg;
    msg << "GaussTest2D: rank mismatch!"
	<< " size(center)="   << cen0->size() 
	<< " size(variance)=" << var0->size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  vector<double> cen, var, phi;
  for (unsigned n=0; n<2; n++) {
    cen.push_back((*cen0)[n]);
    var.push_back((*var0)[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);
  angles  .push_back(angle_def);

  makeSyntheticData();
}

GaussTest2D::
GaussTest2D(int num, int Nbins, int Levels,
		clivectord* wght, clivectord* cen0,
		clivectord* var0) : LikelihoodFunction()
{
  N      = num;
  nbins  = Nbins;
  levels = max<int>(Levels, 1);
  llevel = -1;
  ncomp  = (*wght)().size();
  dim    = 2;
  mdim   = 1;
  fixed2 = false;

  //
  // Dimension sanity check
  //
  if (ncomp*2 != (*cen0)().size() || ncomp*2 != (*var0)().size()) {
    ostringstream msg;
    msg << "GaussTest2D: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += (*wght)()[j];
    vector<double> cen, var, phi;
    for (unsigned n=0; n<2; n++) {
      cen.push_back((*cen0)()[j*2+n]);
      var.push_back((*var0)()[j*2+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
    angles  .push_back(angle_def);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  makeSyntheticData();
}

GaussTest2D::
GaussTest2D(int num, int Nbins, int Levels,
		vector<double>* wght, vector<double>* cen0,
		vector<double>* var0) : LikelihoodFunction()
{
  N      = num;
  nbins  = Nbins;
  levels = max<int>(Levels, 1);
  llevel = -1;
  ncomp  = wght->size();
  dim    = 2;
  mdim   = 1;
  fixed2 = false;

  //
  // Dimension sanity check
  //
  if (ncomp*2 != cen0->size() || ncomp*2 != var0->size()) {
    ostringstream msg;
    msg << "GaussTest2D: rank mismatch!"
	<< " size(center)="   << cen0->size() 
	<< " size(variance)=" << var0->size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += (*wght)[j];
    vector<double> cen, var, phi;
    for (unsigned n=0; n<2; n++) {
      cen.push_back((*cen0)[j*2+n]);
      var.push_back((*var0)[j*2+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
    angles  .push_back(angle_def);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  makeSyntheticData();
}

GaussTest2D::
GaussTest2D(int num, int Nbins, int Levels,
	    clivectord* wght, clivectord* cen0,
	    clivectord* var0, clivectord* phi0) : LikelihoodFunction()
{
  N      = num;
  nbins  = Nbins;
  levels = max<int>(Levels, 1);
  llevel = -1;
  ncomp  = (*wght)().size();
  dim    = 3;
  mdim   = 1;
  fixed2 = false;

  //
  // Dimension sanity check
  //
  if (ncomp*2 != (*cen0)().size() || ncomp*2 != (*var0)().size() ||
      ncomp != (*phi0)().size()) {
    ostringstream msg;
    msg << "GaussTest2D: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size()
	<< " size(angles)="   << (*phi0)().size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += (*wght)()[j];
    vector<double> cen, var, phi;
    for (unsigned n=0; n<2; n++) {
      cen.push_back((*cen0)()[j*2+n]);
      var.push_back((*var0)()[j*2+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
    angles  .push_back((*phi0)()[j]);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  makeSyntheticData();
}
GaussTest2D::
GaussTest2D(int num, int Nbins, int Levels,
	    vector<double>* wght, vector<double>* cen0,
	    vector<double>* var0, vector<double>* phi0) : LikelihoodFunction()
{
  N      = num;
  nbins  = Nbins;
  levels = max<int>(Levels, 1);
  llevel = -1;
  ncomp  = wght->size();
  dim    = 3;
  mdim   = 1;
  fixed2 = false;

  //
  // Dimension sanity check
  //
  if (ncomp*2 != cen0->size() || ncomp*2 != var0->size() ||
      ncomp != phi0->size()) {
    ostringstream msg;
    msg << "GaussTest2D: rank mismatch!"
	<< " size(center)="   << cen0->size() 
	<< " size(variance)=" << var0->size()
	<< " size(angles)="   << phi0->size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += (*wght)[j];
    vector<double> cen, var, phi;
    for (unsigned n=0; n<2; n++) {
      cen.push_back((*cen0)[j*2+n]);
      var.push_back((*var0)[j*2+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
    angles  .push_back((*phi0)[j]);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  makeSyntheticData();
}


void GaussTest2D::SetDim(int n)
{
  switch (n) {
  case 1:
    mdim = 1;
    break;
  case 2:
    mdim = 2;
    break;
  case 3:
    mdim = 3;
    break;
  default:
    mdim = 2;
  }

}


void GaussTest2D::makeSyntheticData()
{
  Uniform unit(0.0, 1.0, BIEgen);
  Normal  norm(0.0, 1.0, BIEgen);
  vector<double> y(2), z(2), zero(2, 0);
  vector< vector< vector<double> > > rot(ncomp);

  for (unsigned n=0; n<ncomp; n++) {
    rot[n] = vector< vector<double> >(2);
    for (int i=0; i<2; i++) rot[n][i] = vector<double>(2);
    rot[n][0][0] =  rot[n][1][1] = cos(angles[n]);
    rot[n][0][1] = -sin(angles[n]);
    rot[n][1][0] = -rot[n][0][1];
  }

  fdata = vector< vector<unsigned> >(nbins);
  for (unsigned n=0; n<nbins; n++) fdata[n] = vector<unsigned>(nbins, 0);

  nbins = max<int>(1, nbins);
  double dxy = 1.0/nbins;
  vector<int> nxy(2);

  bool ok;
  unsigned iend = N;
  if (myid!=0) iend = 0;

  for (unsigned i=0; i<iend; i++) {
    unsigned n = 0;
    double x = unit();
    for (n=0; n<ncomp-1; n++) {
      if (x<=weights[n]) break;
    }
    ok = true;

    for (unsigned j=0; j<2; j++) {
      y[j] = sqrt(variance[n][j])*norm();
    }

    z = zero;
    for (unsigned i1=0; i1<2; i1++) {
      for (unsigned i2=0; i2<2; i2++) {
	z[i1] += rot[n][i1][i2] * y[i2];
      }
    }

    for (unsigned j=0; j<2; j++) {
      z[j] += centers[n][j];
      if (z[j]<0.0 || z[j]>=1.0) ok = false;
      else nxy[j] = min<int>(floor(z[j]/dxy), nbins-1);
    }

    if (ok) fdata[nxy[0]][nxy[1]]++;
  }

  if (mpi_used) {
    for (unsigned n=0; n<nbins; n++)
      MPI_Bcast(&fdata[n][0], nbins, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  }
}

//
// Compute image for this level
//
void GaussTest2D::compute_level()
{
  if (current_level == llevel) return;

  nbins1 = nbins;
  for (int n=levels-1; n>current_level; n--) nbins1 /= 2;
  nbins1 = max<int>(nbins1, 1);
  llevel = current_level;

  ldata = vector< vector<unsigned> >(nbins1);
  for (unsigned n=0; n<nbins1; n++) ldata[n] = vector<unsigned>(nbins1, 0);

  unsigned rat = nbins/nbins1;
  total = 0;
  for (unsigned i=0; i<nbins; i++) {
    for (unsigned j=0; j<nbins; j++) {
      ldata[i/rat][j/rat] += fdata[i][j];
      total += fdata[i][j];
    }
  }

  cout << "Image for Level = " << current_level << " computed"
       << " with " << nbins1 << "/" << nbins << ", total = " << total << endl;
}

//
// Print data to a file for the current level
//
void GaussTest2D::PrintData()
{
  compute_level();

  if (myid) return;

  ostringstream sout;
  int n = min<int>(current_level, levels-1);

  sout << nametag << ".data." << n;
  ofstream out(sout.str().c_str());

  if (out) {
    unsigned isize = ldata.size();
    double dxy = 1.0/isize;
    for (unsigned i=0; i<isize; i++) {
      for (unsigned j=0; j<isize; j++)
	out << setw(18) << (0.5+i)*dxy
	    << setw(18) << (0.5+j)*dxy
	    << setw(18) << ldata[i][j]
	    << endl;
      out << endl;
    }
  } else {
    throw FileOpenException(sout.str(), errno, __FILE__, __LINE__);
  }
}


//
// The only variable we care about here is <State* s>
//
double GaussTest2D::LocalLikelihood(State *s)
{
#ifdef DEBUG_LIKE
  static unsigned cnt  = 0;
  static unsigned freq = 1000;
  ofstream tout;
  if (cnt % freq == 0) {
    ostringstream sout;
    sout << "likelihood_image_" << cnt << "." << myid;
    tout.open(sout.str().c_str());
  }
  cnt++;
#endif

  compute_level();


  vector<double> x(2), y(2), z(2), v(2), zero(2, 0);
  vector< vector< vector<double> > > rot;

  double tmp, tst, val = 0.0, vlc, angle;
  double dxy = 1.0/nbins1;

  unsigned Mcur = s->M();
  unsigned Ndim = s->Ndim();
  
  if ( (fixed2 && Ndim==2) || Ndim==3 ) {
    
    rot = vector< vector< vector<double> > >(Mcur);
      
    for (unsigned m=0; m<Mcur; m++) {
      angle = s->Phi(m, Ndim-1);
      rot[m] = vector< vector<double> >(2);
      for (int i=0; i<2; i++) rot[m][i] = vector<double>(2);
      rot[m][0][0] =  rot[m][1][1] = cos(angle);
      rot[m][0][1] =  sin(angle);
      rot[m][1][0] = -rot[m][0][1];
    }

  }

  unsigned indx;

  for (unsigned i=0; i<nbins1; i++) {
    z[0] = dxy*i;

    for (unsigned j=0; j<nbins1; j++) {
      z[1] = dxy*j;
      
      tst = 0.0;

      for (unsigned m=0; m<Mcur; m++) {

	if (fixed2)
	  for (unsigned k=0; k<2; k++) y[k] = x[k] =  z[k] - s->Ext(k);
	else
	  for (unsigned k=0; k<2; k++) y[k] = x[k] =  z[k] - s->Phi(m, k);
      
	if (rot.size()) {
	  for (unsigned k=0; k<2; k++) {
	    y[k] = 0.0;
	    for (unsigned l=0; l<2; l++) 
	      y[k] += rot[m][k][l]*x[l];
	  }
	}
	
	tmp = 1.0;
	for (unsigned k=0; k<2; k++) {

	  if ((fixed2 && Ndim==0) || (!fixed2 && Ndim==1) )
	    tmp *= 0.5*
	      ( erf( (y[k] + dxy)/sqrt(2.0*variance[0][k]) ) -
		erf( (y[k]      )/sqrt(2.0*variance[0][k]) )
		);
	  else {
	    if (fixed2 && Ndim>0) indx = 0;
	    else                  indx = 1;
	    tmp *= 0.5*
	      ( erf( (y[k] + dxy)/sqrt(2.0*s->Phi(m, indx)) ) - 
		erf( (y[k]      )/sqrt(2.0*s->Phi(m, indx)) )
		);
	  }
	  
	  tst += s->Wght(m) * tmp * total;

	}

      }


      tst = max<double>(0.0, tst);

      if (tst==0.0) {

	if (ldata[i][j]!=0) {
	  throw ImpossibleStateException(__FILE__, __LINE__);
	}

      } else {

	vlc = log(tst)*ldata[i][j] - tst - lgamma(1.0+ldata[i][j]);
	val += vlc;
	
#ifdef DEBUG_LIKE
	if (tout.good()) {
	  tout << setw(18) << z[0]+0.5*dxy 
	       << setw(18) << z[1]+0.5*dxy
	       << setw(18) << tst 
	       << setw(18) << ldata[i][j]
	       << setw(18) << vlc
	       << endl;
	}
#endif
      }
    }
#ifdef DEBUG_LIKE
    if (tout.good()) {
      tout << endl;
    }
#endif
  }
  
  return val;
}

//
// Used to label output
//

vector<string> GaussTest2D::ParameterLabels()
{
  vector<string> ret;

  if (fixed2) {
    if (mdim>0)
      ret.push_back("Var");
    if (mdim>1) 
      ret.push_back("Angle");
  } else {
    ret.push_back("Pos");
    if (mdim>1)
      ret.push_back("Var");
    if (mdim>2) 
      ret.push_back("Angle");
  }

  return ret;
}


vector<string> GaussTest2D::ExtendedLabels()
{
  vector<string> ret;

  if (fixed2) {
    ret.push_back("Pos(x)");
    ret.push_back("Pos(y)");
  }

  return ret;
}
