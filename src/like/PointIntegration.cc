#include <PointIntegration.h>
#include <BIEMutex.h>
#include <BIEmpi.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::PointIntegration)

using namespace BIE;


vector<double> PointIntegration::Value(Model* _model, Tile* _tile,
				      SampleDistribution* _dist)
{
  return  _model->Evaluate( _tile->X(0.5, 0.5), _tile->Y(0.5, 0.5), _dist);
}

vector<double> PointIntegration::Value(Model* _model, Tile* _tile,
				      SampleDistribution* _dist,
				      int irank, int nrank, MPI_Comm& comm)
{
  return  _model->Evaluate( _tile->X(0.5,0.5), _tile->Y(0.5, 0.5), _dist);
}

