#include <cmath>
#include <vector>
#include <iomanip>

#include <boost/math/special_functions.hpp>

#include <Distribution.h>
#include <Tile.h>
#include <GaussTestLikelihoodFunction.h>
#include <BIEmpi.h>
#include <gfunction.h>
#include <Uniform.h>
#include <Normal.h>
#include <Poisson.h>
#include <gaussQ.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GaussTestLikelihoodFunction)

using namespace std;
using namespace BIE;

static const int    number0 = 2;
static const double weights0  [] = {0.5,  0.5};
static const double centers0  [] = {0.2,  0.9};
static const double variance0 [] = {0.03, 0.03};

int GaussTestLikelihoodFunction::nint = 6;

//
// Constructor
//
GaussTestLikelihoodFunction::GaussTestLikelihoodFunction() : LikelihoodFunction()
{
  xmin        = -1.0;
  xmax        =  2.0;
  nbins       = 400;
  N           = 40000;
  dx          = (xmax - xmin)/nbins;
  dim         = 2;
  ncomp       = false;

  generated   = false;
  cauchyData  = false;
  cauchyModel = false;
  point       = false;

  defaultModel();
}

GaussTestLikelihoodFunction::GaussTestLikelihoodFunction(int Nbins, int num) 
  : LikelihoodFunction()
{
  xmin        = -1.0;
  xmax        =  2.0;
  nbins       = Nbins;
  N           = num;
  dx          = (xmax - xmin)/nbins;
  dim         = 2;
  ncomp       = false;

  generated   = false;
  cauchyData  = false;
  cauchyModel = false;
  point       = false;

  defaultModel();
}


GaussTestLikelihoodFunction::GaussTestLikelihoodFunction(int num) 
  : LikelihoodFunction()
{
  xmin        = -1.0;
  xmax        =  2.0;
  N           = num;
  dx          = (xmax - xmin)/nbins;
  dim         = 2;
  ncomp       = false;

  generated   = false;
  cauchyData  = false;
  cauchyModel = false;
  point       = true;

  defaultModel();
}

void GaussTestLikelihoodFunction::defaultModel()
{
  number   = number0;

  centers  = vector<double>(number);
  variance = vector<double>(number);
  weights  = vector<double>(number);

  for (unsigned i=0; i<number; i++) {
    centers [i] = centers0[i];
    variance[i] = variance0[i];
    weights [i] = weights0[i];
  }

}

void GaussTestLikelihoodFunction::newModel(string file, bool keep)
{
  ifstream in(file.c_str());

  if (in) {

    int number1;

    in >> number1;

    if (in) {
    
      vector<double> centers1 (number1);
      vector<double> variance1(number1);
      vector<double> weights1 (number1);

      for (int i=0; i<number1; i++) {
	in >> weights1[i];
	in >> centers1[i];
	in >> variance1[i];
      }

      if (in) {
	weights  = weights1;
	centers  = centers1;
	variance = variance1;
	
	number   = number1;

	if (!keep || !generated) {
	  if (point)
	    makeSyntheticPointData();
	  else
	    makeSyntheticData();
	}
      }
      else {
	cerr << "Error reading data from <" << file << ">" << endl;
      }
    }

  }

}

void GaussTestLikelihoodFunction::newModel
(clivectord* cen, clivectord* var, clivectord* wgt, bool keep)
{
  newModel((*cen)(), (*var)(), (*wgt)(), keep);
}

void GaussTestLikelihoodFunction::newModel
(std::vector<double> cen, std::vector<double> var, std::vector<double> wgt, bool keep)
{
  // Check rank
  size_t nc = cen.size();
  size_t nv = var.size();
  size_t nw = wgt.size();

  if (nc != nv || nc != nw) {
    cerr << "GaussTestLikelihoodFunction::newModel: error in vector ranks"
	 << endl;
    return;
  }

  weights  = wgt;
  centers  = cen;
  variance = var;
  number   = nc;

  if (!keep || !generated) {
    if (point)
      makeSyntheticPointData();
    else
      makeSyntheticData();
  }
}


void GaussTestLikelihoodFunction::makeSyntheticPointData()
{
  //
  // Initialize the point distribution ("fake data")
  //
  if (myid==0) {
				// For selecting components
    Uniform unit(0.0, 1.0, BIEgen); 
    Normal  norm(0.0, 1.0, BIEgen);
    vector<double> cum(number, 1.0);
    cum[0] = weights[0];
    for (unsigned c=1; c<number-1; c++) cum[c] = cum[c-1] + weights[c];


				// Generate data
    pdata = vector<double>(N);
    double xmin=1.0e20, xmax=-1.0e20;
    for (int n=0; n<N; n++) {
      double wgt = unit();
      for (unsigned c=0; c<number; c++) {
	if (wgt <= cum[c]) {	// Select the component . . .
	  if (cauchyData)
	    pdata[n] = centers[c] + sqrt(variance[c])*tan(M_PI*(unit()-0.5));
	  else
	    pdata[n] = centers[c] + sqrt(variance[c])*norm();
	  break;
	}
      }
      xmin = min<double>(pdata[n], xmin);
      xmax = max<double>(pdata[n], xmax);
    }
    cout << "Min=" << xmin << " Max=" << xmax << endl;

				// Send data everywhere
    MPI_Bcast(&pdata[0], N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  } else {
				// Get data from Node 0
    pdata = vector<double>(N);
    MPI_Bcast(&pdata[0], N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  }

  generated = true;

  // Dump data
  ostringstream sout;
  sout << nametag << ".gausslike.point";
  ofstream out(sout.str().c_str());
  if (out) {
    out << N << endl;
    for (int i=0; i<N; i++) out << setw(15) << pdata[i] << endl;
  }
}



void GaussTestLikelihoodFunction::makeSyntheticData()
{
  Poisson poi(0.0, BIEgen);

  //
  // Initialize the comparison distribution ("fake data")
  //
  double x1, x2;
  fdata = vector<double>(nbins, 0.0);
  for (unsigned c=0; c<number; c++) {
    for (int i=0; i<nbins; i++) {
      x1 = xmin + dx*i;
      x2 = xmin + dx*(i+1);
      if (cauchyData)
	fdata[i] += weights[c]*(  
				atan((x2 - centers[c])/sqrt(variance[c]) ) -
				atan((x1 - centers[c])/sqrt(variance[c]) )
				  ) * N / M_PI;
      else
	fdata[i] += weights[c]*(erf2((x1 - centers[c])/sqrt(2.0*variance[c]), 
				     (x2 - centers[c])/sqrt(2.0*variance[c]))
				) * 0.5 * N;
    }
  }

  double sanity = 0.0;
  for (int i=0; i<nbins; i++) sanity += fdata[i]/N;
  cout << "Bin normalizaton sanity check: expect=1, found=" << sanity << endl;


  for (int i=0; i<nbins; i++) fdata[i] = poi(round(fdata[i]));

  generated = true;

  // Dump data
  ostringstream sout;
  sout << nametag << ".gausslike.binned";
  ofstream out(sout.str().c_str());
  if (out) {
    out << nbins << endl;
    out << setw(15) << xmin << setw(15) << xmax << endl;
    for (int i=0; i<nbins; i++) out << setw(15) << fdata[i] << endl;
  }
}


void GaussTestLikelihoodFunction::SetDim(int n)
{
  switch (n) {
  case 1:
    dim = 1;
    break;
  case 2:
    dim = 2;
    break;
  default:
    dim = 2;
  }

}


double GaussTestLikelihoodFunction::LikeProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx)
{
  if (s->Type() == StateInfo::None)
    return LikeProbNonMixture(s);
  else if (s->Type() == StateInfo::Block)
    return LikeProbNonMixture(s);
  else
    return LikeProbMixture(s);
}

double GaussTestLikelihoodFunction::erf2(double x1, double x2)
{
  const double xcut = 5.0;

  if (x1*x2 <= 0.0) {
    double ans = erf(x2) - erf(x1);
    if (ans <= 0.0) {
      // std::cerr << "erf2_a(" << x1 << ", " << x2 << ")=" << ans << std::endl;
      const double teeny = std::pow(10.0, DBL_MIN_10_EXP);
      ans = teeny;
    }
    return ans;
  }
  
  double sgn = x1>0.0 ? 1.0 : -1.0;
  double xx1 = fabs(x1);
  double xx2 = fabs(x2);
  double xmx = std::max<double>(xx1, xx2);

  if (xmx<=xcut) {
    double ans = erf(x2) - erf(x1);
    if (ans <= 0.0) {
      // std::cerr << "erf2_b(" << x1 << ", " << x2 << ")=" << ans << std::endl;
      const double teeny = std::pow(10.0, DBL_MIN_10_EXP);
      ans = teeny;
    }
    return ans;
  }

  double t1  = erfc(xx1);
  double t2  = erfc(xx2);
  double ans = sgn*(t1 - t2);

  if (ans <= 0.0) {
    /*
    std::cerr << "erf2_c(" << x1 << ", " << x2 << ")=" 
	      << "(" << t1*sgn << " - " << t2*sgn << ")="
	      << ans << std::endl;
    */
    const double teeny = std::pow(10.0, DBL_MIN_10_EXP);
    ans = teeny;
  }
  return ans;
}

double GaussTestLikelihoodFunction::LikeProbNonMixture(State *s)
{
  double val = 0.0, tst;

  if (point) {
    
    if (not generated) makeSyntheticPointData();

    for (int n=0; n<N; n++) {
      
      tst = 0.0;

      if (ncomp) {
				// Iterate through the components
	for (unsigned j=0; j<number; j++) {

	  switch (dim) {
	  case 1:		// Positions only
	    if (cauchyModel) {
	      tst += weights[j] / (sqrt(variance[j])*M_PI) / 
		( 1.0 + (pdata[n] - (*s)[j])*(pdata[n] - (*s)[j])
		  /variance[0] );
	    } else {
	      tst += weights[j] * 
		exp( -(pdata[n] - (*s)[j])*(pdata[n] - (*s)[j])
		     /(2.0*variance[0]) ) / sqrt(2.0*M_PI*variance[0]);
	    }
	    break;
	  case 2:		// Positions and variance
	  default:
	    if (cauchyModel) {
	      tst += weights[j] / (sqrt((*s)[number+j])*M_PI) / 
		( 1.0 + (pdata[n] - (*s)[j])*(pdata[n] - (*s)[j])
		  /(*s)[number+j] );
	    } else {
	      tst += weights[j] * 
		exp( -(pdata[n] - (*s)[j])*(pdata[n] - (*s)[j])
		     /(2.0*(*s)[number+j]) ) / sqrt(2.0*M_PI*(*s)[number+j]);
	    }
	    break;
	  }
	}

      } else {			// Assume single component with unit weight

	switch (dim) {
	case 1:
	  if (cauchyModel) {
	    tst += 1.0 / (sqrt(variance[0])*M_PI) / 
	      ( 1.0 + (pdata[n] - (*s)[0])*(pdata[n] - (*s)[0])
		/variance[0] );
	  } else {
	    tst += 
	      exp( -(pdata[n] - (*s)[0])*(pdata[n] - (*s)[0])
		   /(2.0*variance[0]) ) / sqrt(2.0*M_PI*variance[0]);
	  }
	  break;
	case 2:
	default:
	  if (cauchyModel) {
	    tst += 1.0 / (sqrt((*s)[1])*M_PI) / 
	      ( 1.0 + (pdata[n] - (*s)[0])*(pdata[n] - (*s)[0])
		/(*s)[1] );
	  } else {
	    tst += 
	      exp( -(pdata[n] - (*s)[0])*(pdata[n] - (*s)[0])
		   /(2.0*(*s)[1]) ) / sqrt(2.0*M_PI*(*s)[1]);
	  }
	  break;
	}
      }
      
      tst = max<double>(0.0, tst);

      if (tst==0.0) {		// Sanity check
	
	throw ImpossibleStateException(__FILE__, __LINE__);
      
      } else {
	
	val += log(tst);

      }

    }

  } else {			// Binned version
    
    if (not generated) makeSyntheticData();

    double x1, x2, tst;

    for (int i=0; i<nbins; i++) {
      
      x1 = xmin + dx*i;
      x2 = xmin + dx*(i+1);

      tst = 0.0;
      
      if (ncomp) {
				// Iterate through the components
	for (unsigned j=0; j<number; j++) {

	  switch (dim) {
	  case 1:		// Positions only
	    if (cauchyModel) {
	      tst += weights[j] / M_PI *
		(  
		 atan( (x2 - (*s)[j])/sqrt(variance[j]) ) -
		 atan( (x1 - (*s)[j])/sqrt(variance[j]) )
		   ) * N;
	    } else {
	      tst += weights[j] *
		(erf2((x1 - (*s)[j])/sqrt(2.0*variance[j]), 
		      (x2 - (*s)[j])/sqrt(2.0*variance[j]) )
		 ) * 0.5 * N;
	    }
	    break;
	  case 2:		// Positions and variance
	  default:
	    if (cauchyModel) {
	      tst += weights[j] / M_PI *
		(  
		 atan( (x2 - (*s)[j])/sqrt((*s)[number+j]) ) -
		 atan( (x1 - (*s)[j])/sqrt((*s)[number+j]) )
		   ) * N;
	    } else {
	      tst += weights[j] *
		(erf2( (x1 - (*s)[j])/sqrt(2.0*(*s)[number+j]),
		       (x2 - (*s)[j])/sqrt(2.0*(*s)[number+j]) )
		 ) * 0.5 * N;
	    }
	    break;
	  }
	}
	
      } else {
	
	switch (dim) {
	case 1:
	  if (cauchyModel) {
	    tst = 1.0 / M_PI *
	      (  
	       atan( (x2 - (*s)[0])/sqrt(variance[0]) ) -
	       atan( (x1 - (*s)[0])/sqrt(variance[0]) )
		 ) * N;
	  } else {
	    tst = 
	      (erf2((x1 - (*s)[0])/sqrt(2.0*variance[0]), 
		    (x2 - (*s)[0])/sqrt(2.0*variance[0]) )
	       ) * 0.5 * N;
	  }
	  break;
	case 2:
	default:
	  if (cauchyModel) {
	    tst = 1.0 / M_PI *
	      (  
	       atan( (x2 - (*s)[0])/sqrt((*s)[1]) ) -
	       atan( (x1 - (*s)[0])/sqrt((*s)[1]) )
		 ) * N;
	  } else {
	    tst = 
	      (erf2( (x1 - (*s)[0])/sqrt(2.0*(*s)[1]),
		     (x2 - (*s)[0])/sqrt(2.0*(*s)[1]) )
	       ) * 0.5 * N;
	  }
	  break;
	}

      }
      
      tst = max<double>(0.0, tst);

      if (tst==0) {		// Sanity check
	
	if (fdata[i]!=0) {
	  throw ImpossibleStateException(__FILE__, __LINE__);
	}
	
      } else {
				// log Poisson
	val += log(tst)*fdata[i] - tst - lgamma(1.0+fdata[i]);
	
      }
    }
  }
    
  return val;
}

double GaussTestLikelihoodFunction::LikeProbMixture(State *s)
{
  static bool firstime = true;
  int Mcur = s->M();
  double val = 0.0;

  ofstream out;
  if (firstime) out.open("test.out");

  if (point) {
    
    if (not generated) makeSyntheticPointData();

    for (int n=0; n<N; n++) {

      double tst = 0.0;

      for (int c=0; c<Mcur; c++) {
	switch (dim) {
	case 1:
	  if (cauchyModel) {
	    tst += s->Wght(c) / (sqrt(variance[c])*M_PI) / 
	      ( 1.0 + (pdata[n] - s->Phi(c,0))*(pdata[n] - s->Phi(c,0))
		/variance[c] );
	  } else {
	    tst += s->Wght(c) *
	      exp( -(pdata[n] - s->Phi(c,0))*(pdata[n] - s->Phi(c,0))
		   /(2.0*variance[c]) ) / sqrt(2.0*M_PI*variance[c]);
	  }
	  break;
	case 2:
	default:
	  if (cauchyModel) {
	    tst += s->Wght(c) / (sqrt(s->Phi(c,1))*M_PI) / 
	      ( 1.0 + (pdata[n] - s->Phi(c,0))*(pdata[n] - s->Phi(c,0))
		/s->Phi(c,1) );
	  } else {
	    tst += s->Wght(c) *
	      exp( -(pdata[n] - s->Phi(c,0))*(pdata[n] - s->Phi(c,0))
		   /(2.0*s->Phi(c,1)) ) / sqrt(2.0*M_PI*s->Phi(c,1));
	  }
	  break;
	}
      }

      tst = max<double>(0.0, tst);

      if (tst==0.0) {		// Sanity check
	
	throw ImpossibleStateException(__FILE__, __LINE__);
      
      } else {
	
	val += log(tst);

      }

    }

    if (firstime) {
      out.precision(14);
      for (int n=0; n<N; n++) out << setw(22) << pdata[n] << endl;
    }
    
  } else {
      
    if (not generated) makeSyntheticData();

    double x1, x2, tst;

    for (int i=0; i<nbins; i++) {
      tst = 0.0;
      x1  = xmin + dx*i;
      x2  = xmin + dx*(i+1);

      for (int c=0; c<Mcur; c++) {

	switch (dim) {
	case 1:
	  if (cauchyModel) {
	    tst += s->Wght(c) / M_PI *
	      (  
	       atan( (x2 - s->Phi(c,0))/sqrt(variance[c]) ) -
	       atan( (x1 - s->Phi(c,0))/sqrt(variance[c]) )
		 ) * N;
	  } else {
	    tst += s->Wght(c) *
	      (erf2( (x1 - s->Phi(c,0))/sqrt(2.0*variance[c]),
		     (x2 - s->Phi(c,0))/sqrt(2.0*variance[c]) )
	       ) * 0.5 * N;
	  }
	  break;
	case 2:
	default:
	  if (cauchyModel) {
	    tst += s->Wght(c) / M_PI *
	      (  
	       atan( (x2 - s->Phi(c,0))/sqrt(s->Phi(c,1)) ) -
	       atan( (x1 - s->Phi(c,0))/sqrt(s->Phi(c,1)) )
		 ) * N;
	  } else {
	    tst += s->Wght(c) *
	      (erf2( (x1 - s->Phi(c,0))/sqrt(2.0*s->Phi(c,1)),
		     (x2 - s->Phi(c,0))/sqrt(2.0*s->Phi(c,1)) )
	       ) * 0.5 * N;
	  }
	  break;
	}
      }
      
      tst = max<double>(0.0, tst);

      if (firstime)
	out << setw(3)  << setw(18) << xmin + dx*(0.5+i) 
	    << setw(18) << fdata[i] 
	    << setw(18) << tst
	    << endl;
      
      if (tst==0) {		// Sanity check
	
	if (fdata[i]!=0) {
	  throw ImpossibleStateException(__FILE__, __LINE__);
	}
	
      } else {
	
	val += log(tst)*fdata[i] - tst - lgamma(1.0+fdata[i]);
	
      }
    }
  }
    

  if (firstime) {
    firstime = false;
    switch (dim) {
    case 1:
      out << "# " 
	  << setw(14) << "Weight" 
	  << setw(14) << "Position"
	  << endl;
      out << "# " << setfill('-')
	  << setw(14) << "+"
	  << setw(14) << "+"
	  << setfill(' ') << endl;
      out.precision(4);
      for (int c=0; c<Mcur; c++) {
	out << "# " 
	    << setw(14) << s->Wght(c)
	    << setw(14) << s->Phi(c,0)
	    << endl;
      }
      out << "#" << endl;
      break;
    case 2:
    default:
      out << "# " 
	  << setw(14) << "Weight" 
	  << setw(14) << "Position"
	  << setw(14) << "Variance"
	  << endl;
      out << "# " << setfill('-')
	  << setw(14) << "+"
	  << setw(14) << "+"
	  << setw(14) << "+"
	  << setfill(' ') << endl;
      out.precision(4);
      for (int c=0; c<Mcur; c++) {
	out << "# " 
	    << setw(14) << s->Wght(c)
	    << setw(14) << s->Phi(c,0)
	    << setw(14) << s->Phi(c,1)
	    << endl;
      }
      out << "#" << endl;
    }
    if (point)
      out << "# Point model" << endl;
    else
      out << "# Binned model" << endl;
  }

  return val;
}

double GaussTestLikelihoodFunction::CumuProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx, Fct1dPtr f)
{
  if (s->Type() == StateInfo::None)
    return CumuProbNonMixture(s, f);
  else if (s->Type() == StateInfo::Block)
    return CumuProbNonMixture(s, f);
  else
    return CumuProbMixture(s, f);
}

double GaussTestLikelihoodFunction::CumuProbNonMixture(State *s, Fct1dPtr f)
{
  double val = 0.0, tst;

  if (point) {
    
    if (not generated) makeSyntheticPointData();

    for (int n=0; n<N; n++) {

      tst = 0.0;

      if (ncomp) {
				// Iterate through the components
	for (unsigned j=0; j<number; j++) {

	  switch (dim) {
	  case 1:
	    if (cauchyModel) {
	      tst += weights[j] * 
		(0.5 + atan((pdata[n] - (*s)[j])/sqrt(variance[j]))/M_PI);
	    } else {
	      tst += weights[j] * 0.5 *
		(1.0 + erf((pdata[n] - (*s)[j])/sqrt(2.0*variance[j])));
	    }
	    break;
	  case 2:
	  default:
	    if (cauchyModel) {
	      tst += weights[j] * 
		(0.5 + atan((pdata[n] - (*s)[j])/sqrt((*s)[number+j]))/M_PI);
	      
	    } else {
	      tst += weights[j] * 0.5 * 
		(1.0 + erf((pdata[n] - (*s)[j])/sqrt(2.0*(*s)[number+j])));
	    }
	    break;
	  }
	}

      } else {
    
	switch (dim) {
	case 1:
	  if (cauchyModel) {
	    tst =
	      (0.5 + atan((pdata[n] - (*s)[0])/sqrt(variance[0]))/M_PI);
	  } else {
	    tst = 0.5 *
	      (1.0 + erf((pdata[n] - (*s)[0])/sqrt(2.0*variance[0])));
	  }
	  break;
	case 2:
	default:
	  if (cauchyModel) {
	    tst = 
	      (0.5 + atan((pdata[n] - (*s)[0])/sqrt((*s)[1]))/M_PI);
	    
	  } else {
	    tst = 0.5 * (1.0 + erf((pdata[n] - (*s)[0])/sqrt(2.0*(*s)[1])));
	  }
	  break;
	}

      }
    
      tst = f->G(tst);

      if (tst<=0.0) {		// Sanity check
	
	val += -INFINITY;

      } else {
	
	val += log(tst);

      }

    }
    
  } else {			// Binned version
      
    if (not generated) makeSyntheticData();

    double x1, x, tst, J;
    if (lq == 0) lq = LegeQPtr(new LegeQuad(nint));

    for (int i=0; i<nbins; i++) {
      tst = 0.0;
      x1  = xmin + dx*i;

      for (int j=1; j<=nint; j++) {

	J = lq->weight(j);
	x = x1 + dx*lq->knot(j);

	if (ncomp) {
				// Iterate through the components
	  for (unsigned k=0; k<number; k++) {

	    switch (dim) {
	    case 1:
	      if (cauchyModel) {
		tst += weights[k] * J * 
		  (0.5 + atan((x - (*s)[k])/sqrt(variance[k]))/M_PI);
	      } else {
		tst += weights[k] * J * 
		  0.5 * (1.0 + erf((x - (*s)[k])/sqrt(2.0*variance[k])));
	      }
	      break;
	  
	    case 2:
	    default:
	      if (cauchyModel) {
		tst += weights[k] * J * 
		  (0.5 + atan((x - (*s)[k])/sqrt((*s)[number+k]))/M_PI);
		
	      } else {
		tst += weights[k] * J * 
		  0.5 * (1.0 + erf((x - (*s)[k])/sqrt(2.0*(*s)[number+k])));
	      }
	      break;
	    }
	  }

	} else {
				// Single component
	  switch (dim) {
	  case 1:
	    if (cauchyModel) {
	      tst += J * (0.5 + atan((x - (*s)[0])/sqrt(variance[0]))/M_PI);
	    } else {
	      tst += J * 0.5 * (1.0 + erf((x - (*s)[0])/sqrt(2.0*variance[0])));
	    }
	    break;
	  
	  case 2:
	  default:
	    if (cauchyModel) {
	      tst += J * (0.5 + atan((x - (*s)[0])/sqrt((*s)[1]))/M_PI);
	      
	    } else {
	      tst += J * 0.5 * (1.0 + erf((x - (*s)[0])/sqrt(2.0*(*s)[1])));
	    }
	    break;
	  }
	}

      }
      
				// Call the map function
      tst = f->G(tst);
      
      //
      // Take the log
      //
      if (tst<=0) {
	
	val += -fdata[i]*INFINITY;

      } else {
	
	val += fdata[i]*log(tst);
	
      }
    }
  }
    
  return val;
}

double GaussTestLikelihoodFunction::CumuProbMixture(State *s, Fct1dPtr f)
{
  int Mcur = s->M();
  double val = 0.0;

  if (point) {
    
    if (not generated) makeSyntheticPointData();

    for (int n=0; n<N; n++) {

      double tst = 0.0;

      for (int c=0; c<Mcur; c++) {
	switch (dim) {
	case 1:
	  if (cauchyModel) {
	    tst += s->Wght(c) *
	      (0.5 + atan((pdata[n] - s->Phi(c,0))/sqrt(variance[c]))/M_PI);
	  } else {
	    tst += s->Wght(c) * 0.5 *
	      (1.0 + erf((pdata[n] - s->Phi(c,0))/sqrt(2.0*variance[c])));
	  }
	  break;
	  
	case 2:
	default:
	  if (cauchyModel) {
	    tst += s->Wght(c) *
	      (0.5 + atan((pdata[n] - s->Phi(c,0))/sqrt(s->Phi(c,1)))/M_PI);

	  } else {
	    tst += s->Wght(c) * 0.5 *
	      (1.0 + erf((pdata[n] - s->Phi(c,0))/sqrt(2.0*s->Phi(c,1))));
	  }
	  break;
	}
      }

      tst = f->G(max<double>(0.0, min<double>(1.0, tst)));

      if (tst<=0.0) {		// Sanity check
	
	val += -INFINITY;

      } else {
	
	val += log(tst);

      }

    }

  } else {

    if (not generated) makeSyntheticData();

    double x1, x, tst, J;
    if (lq == 0) lq = LegeQPtr(new LegeQuad(nint));

    for (int i=0; i<nbins; i++) {
      tst = 0.0;
      x1  = xmin + dx*i;

      for (int j=1; j<=nint; j++) {

	J = lq->weight(j);
	x = x1 + dx*lq->knot(j);

	for (int c=0; c<Mcur; c++) {
	  switch (dim) {
	  case 1:
	    if (cauchyModel) {
	      tst += J * s->Wght(c) *
		(0.5 + atan((x - s->Phi(c,0))/sqrt(variance[c]))/M_PI);
	    } else {
	      tst += J * s->Wght(c) * 0.5 *
		(1.0 + erf((x - s->Phi(c,0))/sqrt(2.0*variance[c])));
	    }
	    break;
	  
	  case 2:
	  default:
	    if (cauchyModel) {
	      tst += J * s->Wght(c) *
		(0.5 + atan((x - s->Phi(c,0))/sqrt(s->Phi(c,1)))/M_PI);
	      
	    } else {
	      tst += J * s->Wght(c) * 0.5 *
		(1.0 + erf((x - s->Phi(c,0))/sqrt(2.0*s->Phi(c,1))));
	    }
	    break;
	  }
	}
	
      }
				// Call the map function
      tst = f->G(tst);
      
      //
      // Take the log
      //      
      if (tst<=0) {
	
	val += -fdata[i]*INFINITY;
	
      } else {
	
	val += fdata[i]*log(tst);
	
      }
    }
  }
    
  return val;
}

//
// Used to label output
//
const std::string 
GaussTestLikelihoodFunction::ParameterDescription(int i)
{
  std::ostringstream sout;

  if (dim==1) {
    sout << "Position " << i+1;
  } else {
    int j = i/2 + 1;
    if (i % 2==0)
      sout << "Position " << j;
    else
      sout << "Variance " << j;
  }

  return sout.str();
}


