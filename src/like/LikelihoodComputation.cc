#include <LikelihoodComputation.h>
#include <TessToolSender.h>
#include <MPIStreamFilter.h>
#include <RecordOutputStream.h>
#include <BasicType.h>
#include <ArrayType.h>
#include <Node.h>
#include <bieTags.h>
#include <BIEMutex.h>
#include <BaseDataTree.h>

#ifdef DEBUG_TESSTOOL
extern ofstream ferr;
#endif

BIE_CLASS_EXPORT_IMPLEMENT(BIE::LikelihoodComputation)

using namespace BIE;


/**
   common functionality for Likelihood computations
*/

extern "C"
void *
call_any_threads_thread_call(void *atp)
{
  thrd_pass_LikeComp *tp = (thrd_pass_LikeComp *)atp;
  LikelihoodComputation *p = (LikelihoodComputation *)tp->t;
  if (tp->command)
    p -> enslave_worker((void*)&tp->id);

  cerr << "Process " << myid << ": Worker enslaved!!" << endl << flush;
  return NULL;
}

//
// Constructor
//
LikelihoodComputation::LikelihoodComputation() 
{ 
  likelihoodSem= new Semaphore(0);
  suspendSem= new Semaphore(0);

  tesstool = 0;

  detachTessTool 
   = requestSynchronization
   = sampleNext
   = switchCliOn 
   = suspendSimulation 
   = resumeSimulation = 0;

  _builtTTBaseStream = false;
  _sessionId = -1;
  _nextSessionId = -1;
  _simulation = 0;

  DEBUG_LIKE = false; 
  threading_on = false;
  t = new pthread_t;
}

//
// Destructor
//
LikelihoodComputation::~LikelihoodComputation() 
{
  delete t;
}

void LikelihoodComputation::TileDump(Tile *t, SampleDistribution *sd,
				     vector<double>& z)
{
  ostringstream sout;
  sout << "debug_like." << t->GetNode()->ID();
  ofstream out(sout.str().c_str());

  if (!out) return;

  out << "# Tile: ( " << t->X(0.0, 0.0) << " ," << t->Y(0.0, 0.0)
      << ") ( " << t->X(1.0, 1.0) << " ," << t->Y(1.0, 1.0) << ")" 
      << endl;

  out.setf(ios::scientific);

  if (dynamic_cast <BinnedDistribution*> (sd)) {
    
    BinnedDistribution *histo = (BinnedDistribution*)sd;
    
    for (int i=0; i<sd->numberData(); i++) {
      for (int j=0; j<sd->getdim(i); j++) {
	out << setw(16) << histo->getLow(i)[j]
	    << setw(16) << histo->getHigh(i)[j];
      }
      out << setw(16) << sd->getValue(i) << setw(16) << z[i] << endl;
    }

  } else if (dynamic_cast <PointDistribution*> (sd)) {
    PointDistribution *_point = (PointDistribution*)sd;

      if (sd->numberData()) {
	for (int j=0; j<sd->getdim(0); j++)
	  out << setw(16) << _point->Point()[j];
	out << setw(16) << sd->getValue(0) << setw(16) << z[0] << endl;
      }
    
  } else if (dynamic_cast <NullDistribution*> (sd)) {

    out << setw(16) << "This is an empty distribution" << endl;

  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);

}


void LikelihoodComputation::startThread()
{
  if (!Slaves()) return;

  td.t = (void *)this;
  td.command = 1;
  td.id = myid;
  
  cerr << "Process " << myid << ": about to enslave worker" << endl << flush;

  int errcode =  pthread_create(t, 0, call_any_threads_thread_call, (void *)&td);
  if (errcode) {
    cerr << "Process " << myid;
    cerr << " thread: cannot make thread " << td.id
	 << ", errcode=" << errcode << endl;
    exit(19);
  }

  threading_on = true;
}


void LikelihoodComputation::stopThread()
{
  if (!Slaves()) return;

  int errcode;
  void *retval;

  if ((errcode=pthread_join(*t, &retval))) {
    cerr << "Process " << myid;
    cerr << " thread: thread join " << td.id
	 << " failed, errcode=" << errcode << endl;
    exit(20);

  }

  cerr << "Process " << myid << ": thread joined" << endl << flush;

  threading_on = false;
}


void LikelihoodComputation::InitializeTessToolTileDump()
{
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::InitTessToolDump:: enter " << endl;
#endif
  if (!_builtTTBaseStream) {
    BuildTessToolTileDumpBaseStream();
  } else {
    // if we have built the base stream then we just need to send new
    // session header (session id,buff size etc) should check if the
    // mpifilter has been replaced... header should only be sent
    // exactly once
    MPIStreamFilter *filter = tesstool->GetMPIFilter();
    if (_nextSessionId == -1) {
      filter->startNewSession(_sessionId++);
    } else {
      filter->startNewSession(_nextSessionId);
      _sessionId = _nextSessionId;
    }
  }

#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::InitTessToolDump:: exit " << endl;
#endif
  
}

// for now all tiles have the same distribution(number of data)
// use after Reset() has been called on DataTree
void 
LikelihoodComputation::BuildTessToolTileDumpBaseStream()
{
  int j=0;
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::BuildTessToolTileDumpBaseStream:: enter " << endl;
#endif

  BaseDataTree* dis = GetBaseDataTree();
  SampleDistribution *sd = dis->CurrentItem();

#ifdef DEBUG_TESSTOOL
  ferr << "_dist=" << dis << " sd=" << sd<< endl;
#endif

  // build different record types
  //
  RecordType *rt=new RecordType();
  rt = rt->insertField(1, "NumRecords", BasicType::Int);
  rt = rt->insertField(2, "TileId", BasicType::Int);
  rt = rt->insertField(3, "X(0,0)", BasicType::Real);
  rt = rt->insertField(4, "Y(0,0)", BasicType::Real);
  rt = rt->insertField(5, "X(1,1)", BasicType::Real);
  rt = rt->insertField(6, "Y(1,1)", BasicType::Real);
  rt = rt->insertField(7, "NumDims", BasicType::Int);
  rt = rt->insertField(8, "NumData", BasicType::Int);

  // add fields
  //
  if (dynamic_cast <BinnedDistribution*> (sd)) {
#ifdef DEBUG_TESSTOOL
    ferr << " num data in sd=" << sd->numberData() << endl;
#endif

    ostringstream ostr;

    for (j=0; j<sd->getdim(0); j++) {
      ArrayType *hlo = new ArrayType(BasicType::Real, sd->numberData());
      ArrayType *hhi = new ArrayType(BasicType::Real, sd->numberData());
      ostr.str(""); ostr << "Histogram[" << j << "] Low";
      rt = rt->insertField(9+j, ostr.str(), hlo);
      ostr.str(""); ostr << "Histogram[" << j << "] High";
      rt = rt->insertField(9+j+1, ostr.str(), hhi);
    }
    j = 8+2*sd->getdim(0)+1;
    ArrayType *vat = new ArrayType(BasicType::Real, sd->numberData());
    ArrayType *dat = new ArrayType(BasicType::Real, sd->numberData());

    rt = rt->insertField(j, "Value Array", vat);
    rt = rt->insertField(j+1, "Prediction Array", dat);

  } else if (dynamic_cast <PointDistribution*> (sd)) {
#ifdef DEBUG_TESSTOOL
    ferr << " num data in sd=1" << endl;
#endif

    ArrayType *distat = new ArrayType(BasicType::Real, sd->getdim(0));
    ArrayType *vat = new ArrayType(BasicType::Real, 1);
    ArrayType *dat = new ArrayType(BasicType::Real, 1);
    rt = rt->insertField(9, "Point Distribution", distat);
    rt = rt->insertField(10, "Value Array", vat);
    rt = rt->insertField(11, "Prediction Array", dat);

  } else {
    throw NoSuchDistributionException(__FILE__, __LINE__);
  }
#ifdef DEBUG_TESSTOOL
  ferr << rt->toString() << endl;
#endif

  // construct a output stream
  //
  rostrm = new RecordOutputStream(rt);
#ifdef DEBUG_TESSTOOL
  ferr << "rostrm=" << rostrm << endl;
#endif
  tesstool->SetRecordType(rt);
  tesstool->SetMPIStream(rostrm);

  _builtTTBaseStream = true;

#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::BuildTessToolTileDumpBaseStream:: exit " << endl;
#endif  
}

RecordOutputStream*
LikelihoodComputation::AttachMPIFilterToTessTool(RecordOutputStream *rostrm)
{

  MPIStreamFilter *filter;
  filter = new MPIStreamFilter(rostrm, tesstool->GetMPIComm(), 0);
  RecordOutputStream *frostrm  = rostrm->filterWith(filter);
  return frostrm;
}

void 
LikelihoodComputation::UpdateTessTool(RecordType *rt, RecordOutputStream *rostrm, 
					   MPIStreamFilter *mpifilter)
{
  tesstool->SetRecordType(rt);
  tesstool->SetMPIStream(rostrm);
  tesstool->SetMPIFilter(mpifilter);
}

void LikelihoodComputation::FinalizeTessToolTileDump()
{
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::FinalizeTessToolDump:: enter " << endl;
#endif
  MPIStreamFilter *filter = tesstool->GetMPIFilter();
  filter->finishSession();
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::FinalizeTessToolDump:: end " << endl;
#endif
}

void LikelihoodComputation::TessToolTileDump(Tile *t, SampleDistribution *sd, vector<double>& z)
{

#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::TessToolDump:: enter " << endl;
#endif
  TessToolTileDumpToBaseStream(t, sd, z);
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::TessToolDump:: exit " << endl;
#endif
}

void 
LikelihoodComputation::TessToolTileDumpToBaseStream(Tile *t, SampleDistribution *sd, vector<double>& z)
{

#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::TessToolDump:: enter " << endl;
#endif
  RecordOutputStream *ro = tesstool->GetMPIStream();

  ro->setIntValue(1, sd->numberData());
  ro->setIntValue(2, t->GetNode()->ID());
  ro->setRealValue(3, t->X(0.0, 0.0));
  ro->setRealValue(4, t->Y(0.0, 0.0));
  ro->setRealValue(5, t->X(1.0, 1.0));
  ro->setRealValue(6, t->Y(1.0, 1.0));
  ro->setIntValue(7, sd->getdim(0));
  ro->setIntValue(8, sd->getDataSetSize());

  int i=0, j=0;
  if (dynamic_cast <BinnedDistribution*> (sd)) {
    BinnedDistribution *histo = (BinnedDistribution*)sd;
    for (j=0; j<sd->getdim(0); j++) {
      vector<double> hhi, hlo;
      for (i=0; i<sd->numberData(); i++) {
	hlo.push_back(histo->getLow(i)[j]);
	hhi.push_back(histo->getHigh(i)[j]);
      }
      ro->setRealArrayValue(9+2*j,hlo);
      ro->setRealArrayValue(9+2*j+1,hhi);
    }
    
    vector<double> dval;
    for (i=0; i<sd->numberData(); i++) {
      dval.push_back(sd->getValue(i));
    }

    j = 8+2*sd->getdim(0)+1;
    ro->setRealArrayValue(j, dval);
    ro->setRealArrayValue(j+1, z);

    // create record
    ro->pushRecord();
    
  } else if (dynamic_cast <PointDistribution*> (sd)) {
    PointDistribution *_point = (PointDistribution*)sd;
    
    vector<double> v;
    v.push_back(sd->getValue(0));

    ro->setRealArrayValue(9, _point->Point());
    ro->setRealArrayValue(10,v);
    ro->setRealArrayValue(11,z);

    ro->pushRecord();

  } else {
    throw NoSuchDistributionException(__FILE__, __LINE__);
  }

#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::TessToolDump:: exit " << endl;
#endif
}

void LikelihoodComputation::SetTessTool(TessToolSender *ttsender)
{
  tesstool = ttsender;
  tesstool->SetLikelihoodComputation(this);
}

void LikelihoodComputation::SampleNext(int sid)
{
  _nextSessionId = sid;
}

void LikelihoodComputation::SampleNext()
{
  if(myid==0) { // root
    requestSynchronization = 1;
  }
}

void LikelihoodComputation::SwitchOnCLI()
{
  // on root nodes, will encode go command
  switchCliOn = 1;
}

void LikelihoodComputation::SuspendSimulation()
{
  // on root nodes, will encode go command
  suspendSimulation = 1;
}

void LikelihoodComputation::ResumeSimulation()
{
  // on root nodes, will encode go command
  resumeSimulation = 1;
  ResumedSimulation();
}

void LikelihoodComputation::ResumedSimulation()
{
  // Called by the cli thread.  After this the cli thread disables
  // itself, we post on the likelihood semaphore
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::ResumedSimulation:: enter " << endl;
#endif
  suspendSem->post();
  resumeSimulation = 0;
  suspendedSimulation=0;
}

void LikelihoodComputation::SuspendedSimulation()
{
  // make the cli thread active... will be active until resume is called
  // wait on suspendedSimulation semaphore
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::SuspendedSimulation:: enter " << endl;
#endif
  if(myid) { // non-root
    switchCLISem->post();
  }
  suspendedSimulation=1;
  suspendSem->wait();
  suspendSimulation = 0;
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::SuspendedSimulation:: exit " << endl;
#endif
}

void LikelihoodComputation::SwitchedOnCLI()
{
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::SwitchedOnCLI:: enter " << endl;
#endif
  // on non-root nodes
  if (myid) { 
    switchCLISem->post();
  }
  // on all nodes...
  likelihoodSem->wait();
  switchCliOn = 0;
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::SwitchedOnCLI:: exit " << endl;
#endif
}

void LikelihoodComputation::TessToolSynchronize()
{
  if (tesstool != NULL) {
    if(requestSynchronization == 1) {
      // send the information on number of workers and their ids
#ifdef DEBUG_TESSTOOL
      ferr << "LikelihoodComputation::TessToolSynchronize:: Calling TessTool->Synchronize() " << endl;
#endif
      tesstool->Synchronize();
    }
  }
}

void LikelihoodComputation::DetachTessTool()
{
  if (tesstool != NULL) {
    if(detachTessTool == 1) {
      // send the information on number of workers and their ids
#ifdef DEBUG_TESSTOOL
      ferr << "LikelihoodComputation::DetachTessTool:: Calling TessTool->Detach() " << endl;
#endif
      tesstool->Detach();
    }
  }
}


int LikelihoodComputation::encode_command(int code)
{
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::encode_command:: enter " << endl;
#endif
  if (requestSynchronization == 1) { 
    requestSynchronization = 0;
    sampleNext = 1;
    return code | SAMPLE_NEXT_MASK;
  } else if(switchCliOn == 1) {
    return code | SWITCH_CLION_MASK;
  } else if(suspendSimulation == 1) {
    return code | SUSPEND_SIMULATION_MASK;
  } else if(detachTessTool == 1) {
    return code | DETACH_TESSTOOL_MASK;
  } else {
    return code;
  }
}

int LikelihoodComputation::decode_command(int code)
{
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::decode_command:: enter code=" << code << endl;
#endif
  if (code < 0) { 
    code &= ~COMMAND_MASK;
    if (code & SAMPLE_NEXT_MASK) {
      sampleNext = 1;
      return code & (~SAMPLE_NEXT_MASK);
    } else if (code & SUSPEND_SIMULATION_MASK) {
      SuspendedSimulation();
      return code & (~SUSPEND_SIMULATION_MASK);
    } else if (code & SWITCH_CLION_MASK) {
      SwitchedOnCLI();
      return code & (~SWITCH_CLION_MASK);
    } else if (code & DETACH_TESSTOOL_MASK) {
      DetachTessTool();
      return code & (~DETACH_TESSTOOL_MASK);
    } else {
      cerr << "Unknown command received code=" << code << endl;
    }
  } else {
    return code;
  }
  return code;
}

extern int numprocs;
int LikelihoodComputation::getNumAndIdsOfTileMasters(int *num, int **idlist)
{
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::getNumAndIdsOfTileMasters numprocs=" << numprocs << endl;
#endif
  int *list = new int[numprocs];
  *num = numprocs;
  for(int i=0; i<numprocs;i++)
    list[i] = i;
  *idlist = list;
#ifdef DEBUG_TESSTOOL
  ferr << "LikelihoodComputation::getNumAndIdsOfTileMasters returning " << endl;
#endif
  return 0;
}

void LikelihoodComputation::setPriorFrontier() 
{
  fd_temp = GetBaseDataTree()->GetDefaultFrontier();
  GetBaseDataTree()->SetDefaultFrontier(GetSimulation()->priorFrontier());
  current_level--;
}

void LikelihoodComputation::setCurrentFrontier() 
{
  GetBaseDataTree()->SetDefaultFrontier(fd_temp);
  current_level++;
}


void LikelihoodComputation::StateToBuffer(State& s, double* b, unsigned int size)
{
  const string exc_txt = "LikelihoodComputationBufferTooSmall";
  const string msg_txt = "Buffer too small in LikelihoodComputation";
  
  if (s.size() > size) 
    throw BIEException(exc_txt, msg_txt, __FILE__, __LINE__);
  for (unsigned int i=0; i<size; i++) b[i] = s[i];
}

void LikelihoodComputation::BufferToState(State& s, double* b, unsigned int size)
{
  const string exc_txt = "LikelihoodComputationBufferTooBig";
  const string msg_txt = "Buffer too big for state in LikelihoodComputation";
  
  if (size > s.size()) 
    throw BIEException(exc_txt, msg_txt, __FILE__, __LINE__);
  for (unsigned int i=0; i<size; i++) s[i] = b[i];
}


