#include <cmath>
#include <vector>
#include <iomanip>

#include <boost/math/special_functions.hpp>

#include <Distribution.h>
#include <Tile.h>
#include <FunnelTest.h>
#include <BIEmpi.h>
#include <gfunction.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::FunnelTest)

using namespace std;
using namespace BIE;

double FunnelTest::LikeProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx)
{
  size_t curd  = std::min<size_t>(s->N(), dim);
  double v     = (*s)[0];
  double var   = exp(v);
  double ret   = v + lvnorm - 0.5*curd*log(2.0*M_PI*var);

  for (size_t i=1; i<curd; i++) ret += -0.5*(*s)[i]*(*s)[i]/var;

  return ret;
}

double FunnelTest::CumuProb
(vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx, Fct1dPtr f)
{
  size_t curd = std::min<size_t>(s->N(), dim);
  double v    = (*s)[0];
  double var  = exp(v);
  double ret  = exp(v + lvnorm);

  for (size_t i=1; i<curd; i++) ret *= 0.5*(1.0 + erf((*s)[i]/sqrt(2.0*var)));
  
  return ret;
}

const std::string FunnelTest::ParameterDescription(int i)
{
  if (i==0) return "log(var)";
  std::ostringstream sout;
  sout << "x_" << i;
  return sout.str();
}
