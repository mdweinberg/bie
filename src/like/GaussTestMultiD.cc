#include <vector>
#include <iomanip>
#include <cmath>

#include <boost/math/special_functions.hpp>

#include <Distribution.h>
#include <Tile.h>
#include <GaussTestMultiD.h>
#include <BIEmpi.h>
#include <gfunction.h>
#include <gaussQ.h>
#include <Uniform.h>
#include <Normal.h>
#include <Gamma.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::GaussTestMultiD)

using namespace std;
using namespace BIE;

//
// Default values for synthetic data, if not specified explicitly in
// the constructor
//
static const unsigned ncomp_def      = 1;
static const unsigned ndim_def       = 10;
static const double   centers_def [] = {0.5,  0.5,  0.5,  0.5,  0.5,  0.5,  \
					0.5,  0.5,  0.5,  0.5 };
static const double   variance_def[] = {0.03, 0.03, 0.03, 0.03, 0.03, 0.03, \
					0.03, 0.03, 0.03, 0.03};
bool GaussTestMultiD::useAnalytic    = false;
int  GaussTestMultiD::cdfSamples     = 100000;
int  GaussTestMultiD::ncut           = 16;

static bool isEven(int x) { return !(x&1); }
// static bool isOdd (int x) { return  (x&1); }

//
// Constructor
//
GaussTestMultiD::GaussTestMultiD() :  LikelihoodFunction()
{
  N       = 1000;
  levels  = 1;
  mdim    = 1;
  ncomp   = ncomp_def;
  dim     = ndim_def;
  useBeta = false;
  varBeta = false;

  vector<double> cen, var;
  for (unsigned n=0; n<dim; n++) {
    cen.push_back(centers_def [n]);
    var.push_back(variance_def[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  makeSyntheticData();
  makeCDF();
}

GaussTestMultiD::
GaussTestMultiD(int num, int Levels) : LikelihoodFunction()
{
  N       = num;
  levels  = max<int>(Levels, 1);
  mdim    = 1;
  ncomp   = ncomp_def;
  dim     = ndim_def;
  useBeta = false;
  varBeta = false;

  vector<double> cen, var;
  for (unsigned n=0; n<dim; n++) {
    cen.push_back(centers_def [n]);
    var.push_back(variance_def[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  makeSyntheticData();
  makeCDF();
}

GaussTestMultiD::
GaussTestMultiD(int num, int Levels,
		clivectord* cen0,
		clivectord* var0, double alpha) : LikelihoodFunction()
{
  N       = num;
  levels  = max<int>(Levels, 1);
  ncomp   = 1;
  dim     = (*cen0)().size();
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (dim != (*var0)().size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  vector<double> cen, var;
  for (unsigned n=0; n<dim; n++) {
    cen.push_back((*cen0)()[n]);
    var.push_back((*var0)()[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  makeSyntheticData();
  makeCDF();
}

GaussTestMultiD::
GaussTestMultiD(int num, int Levels,
		std::vector<double> cen0,
		std::vector<double> var0, double alpha) : LikelihoodFunction()
{
  N       = num;
  levels  = max<int>(Levels, 1);
  ncomp   = 1;
  dim     = cen0.size();
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (dim != var0.size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << cen0.size() 
	<< " size(variance)=" << var0.size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  vector<double> cen, var;
  for (unsigned n=0; n<dim; n++) {
    cen.push_back(cen0[n]);
    var.push_back(var0[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  makeSyntheticData();
  makeCDF();
}

GaussTestMultiD::
GaussTestMultiD(string file, int Levels,
		clivectord* cen0,
		clivectord* var0, double alpha) : LikelihoodFunction()
{
  levels  = max<int>(Levels, 1);
  ncomp   = 1;
  dim     = (*cen0)().size();
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (dim != (*var0)().size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  vector<double> cen, var;
  for (unsigned n=0; n<dim; n++) {
    cen.push_back((*cen0)()[n]);
    var.push_back((*var0)()[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  readData(file);
  makeCDF();
}

GaussTestMultiD::
GaussTestMultiD(string file, int Levels,
		std::vector<double> cen0,
		std::vector<double> var0, double alpha) : LikelihoodFunction()
{
  levels  = max<int>(Levels, 1);
  ncomp   = 1;
  dim     = cen0.size();
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (dim != var0.size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << cen0.size() 
	<< " size(variance)=" << var0.size();

    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  vector<double> cen, var;
  for (unsigned n=0; n<dim; n++) {
    cen.push_back(cen0[n]);
    var.push_back(var0[n]);
  }
  weights = vector<double>(1,1);
  centers .push_back(cen);
  variance.push_back(var);

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  readData(file);
  makeCDF();
}

GaussTestMultiD::
GaussTestMultiD(int num, int Levels,
		clivectord* wght, clivectord* cen0,
		clivectord* var0, double alpha) : LikelihoodFunction()
{
  N       = num;
  levels  = max<int>(Levels, 1);
  ncomp   = (*wght)().size();
  dim     = (*cen0)().size()/ncomp;
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (ncomp*dim != (*var0)().size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += (*wght)()[j];
    vector<double> cen, var;
    for (unsigned n=0; n<dim; n++) {
      cen.push_back((*cen0)()[j*dim+n]);
      var.push_back((*var0)()[j*dim+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  makeSyntheticData();
  makeCDF();
}


GaussTestMultiD::
GaussTestMultiD(int num, int Levels,
		std::vector<double> wght, std::vector<double> cen0,
		std::vector<double> var0, double alpha) : LikelihoodFunction()
{
  N       = num;
  levels  = max<int>(Levels, 1);
  ncomp   = wght.size();
  dim     = cen0.size()/ncomp;
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (ncomp*dim != var0.size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << cen0.size() 
	<< " size(variance)=" << var0.size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += wght[j];
    vector<double> cen, var;
    for (unsigned n=0; n<dim; n++) {
      cen.push_back(cen0[j*dim+n]);
      var.push_back(var0[j*dim+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  makeSyntheticData();
  makeCDF();
}


GaussTestMultiD::
GaussTestMultiD(string file, int Levels,
		clivectord* wght, clivectord* cen0,
		clivectord* var0, double alpha) : LikelihoodFunction()
{
  levels  = max<int>(Levels, 1);
  ncomp   = (*wght)().size();
  dim     = (*cen0)().size()/ncomp;
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (ncomp*dim != (*var0)().size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << (*cen0)().size() 
	<< " size(variance)=" << (*var0)().size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += (*wght)()[j];
    vector<double> cen, var;
    for (unsigned n=0; n<dim; n++) {
      cen.push_back((*cen0)()[j*dim+n]);
      var.push_back((*var0)()[j*dim+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  readData(file);
  makeCDF();
}


GaussTestMultiD::
GaussTestMultiD(string file, int Levels,
		std::vector<double> wght, std::vector<double> cen0,
		std::vector<double> var0, double alpha) : LikelihoodFunction()
{
  levels  = max<int>(Levels, 1);
  ncomp   = wght.size();
  dim     = cen0.size()/ncomp;
  mdim    = 1;
  useBeta = false;
  varBeta = false;

  //
  // Dimension sanity check
  //
  if (ncomp*dim != var0.size()) {
    ostringstream msg;
    msg << "GaussTestMultiD: rank mismatch!"
	<< " size(center)="   << cen0.size() 
	<< " size(variance)=" << var0.size()
	<< endl;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }


  double frk = 0.0;
  for (unsigned j=0; j<ncomp; j++) {
    frk += wght[j];
    vector<double> cen, var;
    for (unsigned n=0; n<dim; n++) {
      cen.push_back(cen0[j*dim+n]);
      var.push_back(var0[j*dim+n]);
    }
    weights .push_back(frk);
    centers .push_back(cen);
    variance.push_back(var);
  }
  for (unsigned j=0; j<ncomp; j++) weights[j] /= frk;

  unit = UniformPtr(new Uniform(0.0, 1.0, BIEgen));
  norm = NormalPtr (new Normal (0.0, 1.0, BIEgen));
  gama = GammaPtr  (new Gamma  (0.5*dim,  BIEgen));

  if (alpha>0.0) {
    clivectord c(dim, 0.0);
    cout << "Center size: " << c().size() << endl;
    useBeta     = true;
    useAnalytic = true;
    beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
  }

  readData(file);
  makeCDF();
}


void GaussTestMultiD::Gaussian()
{
  if (beta.get()) {
    beta.reset();
    makeCDF();
  } else {
    cout << "GaussTestMultiD::useBetaR: Gaussian model is already in use!"
	 << endl;
  }
}

void GaussTestMultiD::BetaR(double alpha)
{
  if (beta.get()) {
    cout << "GaussTestMultiD::useBetaR: Beta model is already in use!"
	 << endl;
  } else {
    if (alpha>0.0) {
      clivectord c(dim, 0.0);
      cout << "Center size: " << c().size() << endl;
      useBeta     = true;
      useAnalytic = true;
      beta        = boost::shared_ptr<BetaRDist>(new BetaRDist(alpha, &c));
    }
  }
}

void GaussTestMultiD::newModel(clivectord* var0)
{
  newModel((*var0)());
}

void GaussTestMultiD::newModel(std::vector<double> var0)
{
  //
  // Sanity check
  //
  unsigned tdim = var0.size()/ncomp;
  if (tdim != dim) {
    ostringstream msg;
    msg << "GaussTestMultiD: Model dimension must be " << dim
	<< " with " << ncomp << " components!" << endl
	<< "GaussTestMultiD: Your input implies a dimension of " << tdim;
    throw DimNotMatchException(msg.str(), __FILE__, __LINE__);
  }

  //
  // Erase the old vector and install the new one
  //
  variance.erase(variance.begin(), variance.end());

  for (unsigned j=0; j<ncomp; j++) {
    vector<double> var;
    for (unsigned n=0; n<dim; n++) var.push_back(var0[j*dim+n]);
    variance.push_back(var);
  }
  
}


void GaussTestMultiD::SetDim(int n)
{
  switch (n) {
  case 1:
    mdim = 1;
    break;
  case 2:
    mdim = 2;
    break;
  default:
    mdim = 2;
  }

}

void GaussTestMultiD::makeSyntheticData()
{
  fdata.erase(fdata.begin(), fdata.end());
  for (unsigned i=0; i<N; i++) fdata.push_back(makeData());
}

//
// Sample the density and make the CDF
//
void GaussTestMultiD::makeCDF()
{
  if (useAnalytic) return;

  dd = PairDV(vector<double>(3, 0.0), vector<double>(dim));

  cdf = boost::shared_ptr<CDFGenerator>
    (new CDFGenerator(cdfSamples, ncut, static_cast<FctDVPtr>(this)));
}

//
// Dump the entire data set to a file
//
void GaussTestMultiD::dumpData(string s)
{
  ofstream out(s.c_str());

  if (out) {
    unsigned iend = fdata.size();
    out.write((const char *)&dim,  sizeof(unsigned));
    out.write((const char *)&iend, sizeof(unsigned));
    for (unsigned i=0; i<iend; i++) {
      for (unsigned j=0; j<dim; j++)
	out.write((const char *)&fdata[i][j], sizeof(double));
    }
  } else {
    throw FileOpenException(s, errno, __FILE__, __LINE__);
  }
}


//
// Read the entire data set from a file
//
void GaussTestMultiD::readData(string s)
{
  ifstream in(s.c_str());

  if (in) {
    unsigned idim;
    in.read((char *)&idim, sizeof(unsigned));
    if (idim != dim) {
      ostringstream sout;
      sout << "GaussTestMultiD::readData: internal dimension=" << dim
	   << " but file has dimension=" << idim << endl;
      in.close();
      throw DimNotMatchException(sout.str(), __FILE__, __LINE__);
    }
    in.read((char *)&N, sizeof(unsigned));
    fdata.erase(fdata.begin(), fdata.end());
    for (unsigned i=0; i<N; i++) {
      fdata.push_back(vector<double>(dim));
      for (unsigned j=0; j<dim; j++) {
	in.read((char *)&fdata.back()[j], sizeof(double));
	if (in.bad()) {
	  string msg ("GaussTestMultiD::readData: premature end of file?");
	  in.close();
	  throw FileFormatException(msg, __FILE__, __LINE__);
	}
      }
    }
  } else {
    throw FileOpenException(s, errno, __FILE__, __LINE__);
  }
}


//
// Print data to a file for the current level
//
void GaussTestMultiD::printData()
{
  ostringstream sout;
  int n = min<int>(current_level, levels-1);

  sout << nametag << ".data." << n;
  ofstream out(sout.str().c_str());

  if (out) {
    unsigned iend = fdata.size()*(n+1)/levels;
    for (unsigned i=0; i<iend; i++) {
      for (unsigned j=0; j<dim; j++)
	out << setw(18) << fdata[i][j];
      out << endl;
    }
  } else {
    throw FileOpenException(sout.str(), errno, __FILE__, __LINE__);
  }
}


double GaussTestMultiD::LikeProbSingle(State *s)
{
  double tmp1, tmp2, tst, val = 0.0;
  State st(*s);

  unsigned n    = min<int>(current_level, levels-1);
  unsigned iend = fdata.size()*(n+1)/levels;

  for (unsigned i=0; i<iend; i++) {

    tmp1 = 0.0;
    tmp2 = 1.0;
    switch (mdim) {
    case 1:
      if (useBeta) {
	for (unsigned j=0; j<dim; j++) {
	  st[j] = (fdata[i][j] - (*s)[j])/sqrt(variance[0][j]);
	  tmp2 *= sqrt(variance[0][j]);
	}
	if (varBeta)
	  tmp1 = varBetaPDF((*s)[dim], st);
	else
	  tmp1 = beta->PDF(st);
      } else {
	for (unsigned j=0; j<dim; j++) {
	  tmp1 += 
	    (fdata[i][j] - (*s)[j])*
	    (fdata[i][j] - (*s)[j])/(2.0*variance[0][j]);
	  tmp2 *= sqrt(2.0*M_PI*variance[0][j]);
	}
      }
      break;
    case 2:
    default:
      if (useBeta) {
	for (unsigned j=0; j<dim; j++) {
	  st[j] = (fdata[i][j] - (*s)[j*2]) / sqrt((*s)[j*2+1]);
	  tmp2 *= sqrt((*s)[j*2+1]);
	}
	if (varBeta)
	  tmp1 = varBetaPDF((*s)[2*dim], st);
	else
	  tmp1 = beta->PDF(st);
      } else {
	for (unsigned j=0; j<dim; j++) {
	  tmp1 += 
	    (fdata[i][j] - (*s)[j*2])*
	    (fdata[i][j] - (*s)[j*2])
	    /(2.0*(*s)[j*2+1]);
	  tmp2 *= sqrt(2.0*M_PI*(*s)[j*2+1]);
	}
      }
      break;
    }

    bool bad = false;

    if (useBeta) {
      if (tmp1>0.0 && tmp2>0.0)	tst = log(tmp1) - log(tmp2);
      else bad = true;
    } else {
      if (tmp2>0.0) tst = -tmp1 - log(tmp2);
      else bad = true;
    }

    if (bad) {

      throw ImpossibleStateException(__FILE__, __LINE__);

    } else {

      val += tst;

    }
  }
  
  return val;
}

double GaussTestMultiD::LikeProbMixture(State *s)
{
  double tmp1, tmp2, tst, val = 0.0;
  State st(*s);

  unsigned n    = min<int>(current_level, levels-1);
  unsigned iend = fdata.size()*(n+1)/levels;
  unsigned Mcur = s->M();
  unsigned q;
  
  for (unsigned i=0; i<iend; i++) {
    tst = 0.0;

    for (unsigned c=0; c<Mcur; c++) {
      tmp1 = 0.0;
      tmp2 = 1.0;
      switch (mdim) {
      case 1:
	q = c % ncomp;
	if (useBeta) {
	  for (unsigned j=0; j<dim; j++) {
	    st[j] = (fdata[i][j] - s->Phi(c,j))/sqrt(variance[q][j]);
	    tmp2 *= sqrt(variance[q][j]);
	  }
	  if (varBeta)
	    tmp1 = varBetaPDF(s->Phi(c, dim), st);
	  else
	    tmp1 = beta->PDF(st);
	} else {
	  for (unsigned j=0; j<dim; j++) {
	    tmp1 += 
	      (fdata[i][j] - s->Phi(c,j))*
	      (fdata[i][j] - s->Phi(c,j))/(2.0*variance[q][j]);
	    tmp2 *= sqrt(2.0*M_PI*variance[q][j]);
	  }
	}
	break;
      case 2:
      default:
	if (useBeta) {
	  for (unsigned j=0; j<dim; j++) {
	    st[j] = (fdata[i][j] - s->Phi(c,j*2)) /
	      sqrt(s->Phi(c,j*2+1));
	    tmp2 *= sqrt(s->Phi(c,j*2+1));
	  }
	  if (varBeta)
	    tmp1 = varBetaPDF(s->Phi(c,2*dim), st);
	  else
	    tmp1 = beta->PDF(st);
	} else {
	  for (unsigned j=0; j<dim; j++) {
	    tmp1 += 
	      (fdata[i][j] - s->Phi(c,j*2))*
	      (fdata[i][j] - s->Phi(c,j*2))
	      /(2.0*s->Phi(c,j*2+1));
	    tmp2 *= sqrt(2.0*M_PI*s->Phi(c,j*2+1));
	  }
	}
	break;
      }
      if (useBeta)
	tst += s->Wght(c) * tmp1/tmp2;
      else
	tst += s->Wght(c) * exp(-tmp1)/tmp2;
    }

    tst = max<double>(0.0, tst);

    if (tst==0) {
      
      throw ImpossibleStateException(__FILE__, __LINE__);
      
    } else {
      
      val += log(tst);
      
    }
  }
  
  return val;
}

//
// The only variable we care about here is <State* s> and Fct1dPtr to
// compute the G_phi function
//
double GaussTestMultiD::CumuProb_single(State *s, Fct1dPtr f)
{
  double tmp, arg, tst1, tst2, val = 0.0;
  State st(*s);

  unsigned n    = min<int>(current_level, levels-1);
  unsigned iend = fdata.size()*(n+1)/levels;

  vector<double> loc, scl;
  
  if (!useAnalytic) {
    switch (mdim) {
    case 1:
      for (unsigned n=0; n<dim; n++) {
	loc.push_back((*s)[n]);
	scl.push_back(sqrt(variance[0][n]));
      }
      break;
    case 2:
    default:
      for (unsigned n=0; n<dim; n++) {
	loc.push_back((*s)[n*2]);
	scl.push_back(sqrt((*s)[n*2+1]));
      }
      break;
    }
  }
    
  for (unsigned i=0; i<iend; i++) {
    
    if (useAnalytic) {
	
      tmp = 0.0;
      switch (mdim) {
      case 1:
	// Easy because multi-d gaussian is separable
	if (useBeta) {
	  for (unsigned j=0; j<dim; j++)
	    st[j] = (fdata[i][j] - (*s)[j])/sqrt(variance[0][j]);
	  if (varBeta)
	    tmp = varBetaCDF((*s)[dim], st);
	  else
	    tmp = beta->CDF(st);
	} else {
	  for (unsigned j=0; j<dim; j++) {
	    arg = fdata[i][j] - (*s)[j];
	    tmp += arg*arg/variance[0][j];
	  }
	}
	break;
      case 2:
      default:
	if (useBeta) {
	  for (unsigned j=0; j<dim; j++)
	    st[j] = (fdata[i][j] - (*s)[j*2]) / sqrt((*s)[j*2+1]);
	  if (varBeta)
	    tmp = varBetaCDF((*s)[2*dim], st);
	  else
	    tmp = beta->CDF(st);
	} else {
	  for (unsigned j=0; j<dim; j++) {
	    arg = fdata[i][j] - (*s)[j*2];
	    
	    tmp += arg*arg/(*s)[j*2+1];
	  }
	}
	break;
      }

      if (useBeta)
	tst1 = tmp;
      else
	tst1 = boost::math::gamma_p(0.5*dim, 0.5*tmp);

    } else {
      
      tst1 = cdf->FTheta(fdata[i], loc, scl);
      
    }
      
    // This the transformed CDF for the current particle (ith)

    tst2 = f->G(tst1);

    // Now we want to return the product (in log space)

    if (tst2<=0) {

      val += -INFINITY;

    } else {

      val += log(tst2);
      
    }
  }
  
  return val;
}

double GaussTestMultiD::CumuProb_mixture(State *s, Fct1dPtr f)
{
  double tmp, arg, tst1, tst2, val = 0.0;
  State st(*s);

  unsigned n    = min<int>(current_level, levels-1);
  unsigned iend = fdata.size()*(n+1)/levels;
  unsigned Mcur = s->M();
  unsigned q;
  
  vector<double> wght;
  vector< vector<double> > loc(Mcur);
  vector< vector<double> > scl(Mcur);
  
  if (!useAnalytic) {
    for (unsigned m=0; m<Mcur; m++) {
      wght.push_back(s->Wght(m));
      switch (mdim) {
      case 1:
	for (unsigned n=0; n<dim; n++) {
	  loc[m].push_back(s->Phi(m, n));
	  scl[m].push_back(sqrt(variance[m][n]));
	}
	break;
      case 2:
      default:
	for (unsigned n=0; n<dim; n++) {
	  loc[m].push_back(s->Phi(m, n*2  ));
	  scl[m].push_back(sqrt(s->Phi(m, n*2+1)));
	}
	break;
      }
    }
  }
    
  for (unsigned i=0; i<iend; i++) {
    tst1 = 0.0;	 // This will contain log(P) for this particle

    if (useAnalytic) {
      
      for (unsigned c=0; c<Mcur; c++) {
	tmp = 0.0;
	switch (mdim) {
	case 1:
	  // Easy because multi-d gaussian is separable
	  q = c % ncomp;
	  if (useBeta) {
	    for (unsigned j=0; j<dim; j++)
	      st[j] = (fdata[i][j] - s->Phi(c,j))/sqrt(variance[q][j]);
	    if (varBeta)
	      tmp = varBetaCDF(s->Phi(c,dim), st);
	    else
	      tmp = beta->CDF(st);
	  } else {
	    for (unsigned j=0; j<dim; j++) {
	      arg = fdata[i][j] - s->Phi(c,j);
	      tmp += arg*arg/variance[q][j];
	    }
	  }
	  break;
	case 2:
	default:
	  if (useBeta) {
	    for (unsigned j=0; j<dim; j++)
	      st[j] = (fdata[i][j] - s->Phi(c,j*2)) /
		sqrt(s->Phi(c,j*2+1));
	  if (varBeta)
	    tmp = varBetaCDF(s->Phi(c,2*dim), st);
	  else
	    tmp = beta->CDF(st);
	  } else {
	    for (unsigned j=0; j<dim; j++) {
	      arg = fdata[i][j] - s->Phi(c,j*2);
	      
	      tmp += arg*arg/s->Phi(c,j*2+1);
	    }
	  }
	  break;
	}
	if (useBeta)
	  tst1 += s->Wght(c) * tmp;
	else
	  tst1 += s->Wght(c) * boost::math::gamma_p(0.5*dim, 0.5*tmp);
      }
      
    } else {
      
      tst1 += cdf->FTheta(fdata[i], wght, loc, scl);
      
    }
      
    // This the transformed CDF for the current particle (ith)

    tst2 = f->G(tst1);

    // Now we want to return the product (in log space)

    if (tst2<=0) {

      val += -INFINITY;

    } else {

      val += log(tst2);

    }
  }
  
  return val;
}


double GaussTestMultiD::varBetaPDF(double shape, vector<double>& x)
{
  double norm = dim * pow(M_PI, 0.5*dim)*exp(-lgamma(0.5*dim+1.0)) * 
    boost::math::beta(shape, static_cast<double>(dim));
  
  double r = 0.0;
  for (unsigned i=0; i<dim; i++) r += x[i]*x[i];

  return 1.0/(norm*pow(1.0 + sqrt(r), shape+dim));

}

double GaussTestMultiD::varBetaCDF(double shape, vector<double>& x)
{
  double r = 0.0;
  for (unsigned i=0; i<dim; i++) r += x[i]*x[i];

  // This is the variable u in [0,1]
  double u = 1.0/(1.0 + sqrt(r));

  return boost::math::ibeta(shape, static_cast<double>(dim), u);
}


//
// Used to label output
//

const std::string GaussTestMultiD::ParameterDescription(int i)
{
  int D = static_cast<int>(dim);
  ostringstream sout;
  string ret = "**Error**";

  switch (mdim) {
  case 1:
    if (i<D) {
      sout << "Pos [" << i+1 << "]";
      ret = sout.str();
    }
    break;
  case 2:
  default:
    if (i<2*D) {
      if (isEven(i))
	sout << "Pos [" << i/2+1 << "]";
      else
	sout << "Var [" << i/2+1 << "]";
      ret = sout.str();
    }
    break;
  }

  return ret;
}


vector<double> GaussTestMultiD::makeData()
{
  vector<double> ddata(dim);
  
  if (myid==0) {
    unsigned n = 0;
    double x = (*unit)();
    for (n=0; n<ncomp-1; n++) {
      if (x<=weights[n]) break;
    }

    double sum = 0.0;
    for (unsigned j=0; j<dim; j++) ddata[j] = (*norm)();
    for (unsigned j=0; j<dim; j++) sum += ddata[j]*ddata[j];

    double r = sqrt(2.0*(*gama)());
    for (unsigned j=0; j<dim; j++) 
      ddata[j] = centers[n][j] + ddata[j]*r*sqrt(variance[n][j]/sum);

    if (mpi_used)  MPI_Bcast(&ddata[0], dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  } else {
    MPI_Bcast(&ddata[0], dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  }

  return ddata;
}


PairDV GaussTestMultiD::Sampler()
{
  const double fac = log(2.0*M_PI);
  dd.first[0] = 0.0;

  if (myid==0) {
    for (unsigned j=0; j<dim; j++) {
      double v = dd.second[j] = (*norm)();
      dd.first[0] += -0.5*v*v - fac;
    }
    dd.first[1] = dd.first[0];
    if (mpi_used)  {
      MPI_Bcast(&dd.first[0],    3, MPI_DOUBLE, 0, MPI_COMM_WORLD);
      MPI_Bcast(&dd.second[0], dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }

  } else {
    MPI_Bcast(&dd.first[0],    3, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dd.second[0], dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  }

  return dd;
}
