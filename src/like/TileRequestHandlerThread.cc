#include <cstdlib>
using namespace std;

#include <TileRequestHandlerThread.h>
#include <TileMasterWorkerThread.h>
#include <InitEvaluationRequest.h>
// these last two includes are for debugging.
#include <NormEvaluationRequest.h>
#include <ModelEvaluationRequest.h>
#include <TileMasterWorkerThread.h>

#include <BIEMutex.h>
using namespace BIE;

#undef TILE_REQUEST_HANDLER_FINE_TRACE

//construct thread with the number of workers to manage and mappings
//from id-in-group to id-in-system and vice-versa.// construct the thread
TileRequestHandlerThread::TileRequestHandlerThread(int tileGroupSize, 
						   vector<int> *groupIdToSystemId,
						   vector<int> *systemIdToGroupId,
						   vector<MPI_Comm> *comms,
						   Semaphore *numberSlavesReady)
  : startTime(groupIdToSystemId->size()), endTime(groupIdToSystemId->size()),
    histo(groupIdToSystemId->size(), vector<int>(10))
{

   stopRequested = false;
   resetRequested = false;

   workerAvailableSemaphoreCount = 0;
   requestAvailableSemaphoreCount = 0;

   commonInitialize(tileGroupSize, groupIdToSystemId, systemIdToGroupId, comms, numberSlavesReady);

   cout << "TileRequestHandlerThread. contructor finished\n";

};



//
// Thread should be "resetPause"d before this is called
void TileRequestHandlerThread::reInitialize(int tileGroupSize, 
					    vector<int> *groupIdToSystemId,
					    vector<int> *systemIdToGroupId,
					    vector<MPI_Comm> *comms,
					    Semaphore *numberSlavesReady)

{
  // parnoid leak avoidance.
  int pendingRequestSize = pendingRequest.size();

  for (int i=0; i<pendingRequestSize; i++){
    if (pendingRequest[i] != 0) {
      cerr << "Deleting leftover request in TileRequestHandlerThread.reInitialize!\n";
      delete pendingRequest[i];
      pendingRequest[i] = 0;
    }
  }

  commonInitialize(tileGroupSize, groupIdToSystemId, systemIdToGroupId, comms, numberSlavesReady);

  // caller will restart target thread by calling "unPause".
}


void TileRequestHandlerThread::setTileMasterWorkerThread(TileMasterWorkerThread* tileMasterWorkerThread)
{
  this->tileMasterWorkerThread = tileMasterWorkerThread;
}


/// Initialization common to both construction and reInitialization
void TileRequestHandlerThread::commonInitialize(int tileGroupSize, 
						vector<int> *groupIdToSystemId,
						vector<int> *systemIdToGroupId,
						vector<MPI_Comm> *comms,
						Semaphore *numberSlavesReady)
{
  // mildly tricky.  The master is not technically a worker. But the master 
  // has the first slot in the pendingRequests array.  The request will never
  // get responded to, and no work will ever be sent out through this mechanism to the master
  ++MPIMutex;
  this->numWorkersInTileGroup = tileGroupSize;  // includes master
   this->groupIdToSystemId = groupIdToSystemId;
   this->systemIdToGroupId = systemIdToGroupId;
   this->comms = comms;

   tileMasterWorkerThread = 0;

   pendingRequest.resize(numWorkersInTileGroup);

   if (requestQueue.size() != 0){
     cout << "TileRequestHandlerThread.reInitialize. Failed. Request queue not empty.\n";
     std::exit(1);
   }

   if (requestAvailableSemaphoreCount){
     cout << "TileRequestHandlerThread.reInitialize. Failed. requestAvailablesemaphore not zero.\n";
     std::exit(1);
   }

   for (int i=0; i<numWorkersInTileGroup; i++){
     // set system to expecting an init msg
     pendingRequest[i] = new InitEvaluationRequest(numberSlavesReady);

     {
       startTime[i] = MPI_Wtime();
     }

   }

   MPIThreadLoopEvent.reset();

 --MPIMutex;

   #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
      cout << "TileRequestHandlerThread. commonInitialize finished\n";
   #endif
}


/*
 *
*/
void TileRequestHandlerThread::enqueueRequest(EvaluationRequest *request)
{
  //assertion
  if (request == 0){
    cerr << "TileRequestHandlerThread.enqueueRequest: Null evaluation request\n";
    std::exit(1);
  }


  //++MPIMutex;
  ++requestQueueMutex;

    //cout << "TileRequestHandlerThread.enqueueRequest: before add size " << requestQueue.size() << "\n";
    requestQueue.push_back(request);
    #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
        cout << "TileRequestHandlerThread.enqueueRequest: after add size " << requestQueue.size() << "\n";
    #endif

    //cout << "TileRequestHandlerThread.enqueueRequest: just added request " << request << "\n";
  --requestQueueMutex;
  //  --MPIMutex;

  // increment counting semaphore. Contains number of outstanding requests
  requestAvailableSemaphore.post(); 
  requestAvailableSemaphoreCount++;


  #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
      cout << "TileRequestHandlerThread.enqueueRequest: Semaphore after add " 
	   << requestAvailableSemaphoreCount << "\n";
  #endif
}


/*
 *
 */
void TileRequestHandlerThread::run()
{

  // Request handler thread.  Each request for work from a slave processor causes
  // a thread (us) on the tile group master which is dedicated to communication to
  // that slave processor to wake up.

  // this function/thread only runs on the tile group master.
#if TILE_REQUEST_HANDLER_FINE_TRACE
  cout << "TileRequestHandlerThread.Run entered\n";
#endif

  //cout << "starting TRHT start-delay loop\n";
  {for(int i=0; i<10000000; i++);}  // slow this puppy up to let gdb attach
  cout << "finished TRHT start-delay loop\n";
 while (!stopRequested){
  while(!resetRequested){

    #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
        cout << "TileRequestHandlerThread.  waiting for worker available\n";
    #endif

    workerWaitTime.start();
      // wait for worker processor to request more work
      workerAvailableSemaphore.wait();  // counting semaphore
      workerAvailableSemaphoreCount--;
    workerWaitTime.stop();


    #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
        cout << "TileRequestHandlerThread.  waiting for work\n";
    #endif

    // wait for the work queue  to have something in it. 
    workWaitTime.start();
      requestAvailableSemaphore.wait();   // counting semaphore
      requestAvailableSemaphoreCount--;
    workWaitTime.stop();

    #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
        cout << "TileRequestHandlerThread.  Have worker and work\n";
    #endif

    if (resetRequested){
       #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
           cout << "TileRequestHandlerThread.  resetting by request\n";
       #endif

       continue;
    }

    // grab a request

    ++requestQueueMutex;
      EvaluationRequest *request = requestQueue.front();
      requestQueue.pop_front();
    --requestQueueMutex;

    request->LogIt("Dequeued in TRHT");

    //assertion
    if (request == 0){
      cout << "TileRequestHandlerThread: Null Request on RequestQueue!\n";
      std::exit(1);
    }

    // This is the routing decision.
    // find free worker - if there isn't one it's an error
    int freeWorker = -1;
    for(int i=0; i<numWorkersInTileGroup; i++){
      if (pendingRequest[i] == 0){
	freeWorker = i;
	break;
      }
    }


      // Send the request to the worker
      if (freeWorker != -1 ){
        if (freeWorker != 0){ // if not master, get MPI lock
	  thrqMutexHoldTime.start();
	  mutexWaitTime.start();
	    ++MPIMutex;
          mutexWaitTime.stop();
        }


  	  pendingRequest[freeWorker] = request;
       

	  if (request != 0){// assertion

          request->LogIt("about to Send in TRHT");

          startTime[freeWorker] = MPI_Wtime();
          // different transport if we are sending to masterworkerthread- no MPI


	  if (freeWorker == 0){ // master
	    tileMasterWorkerThread->Send(request);
          }else{
            request->Send( (*groupIdToSystemId)[freeWorker], comms);
	  }
          // CANT ASSUME REQUEST IS STILL VALID FROM THIS POINT ON

        if (freeWorker != 0){ // if not master, release MPI lock
	 --MPIMutex;
         thrqMutexHoldTime.stop();
        }

        MPIThreadLoopEvent.signal();  //  Nudge the reader thread so it can block on read if
                                 // it wants to

      }else{
	  cout << "Group to System id table null!\n";
	  std::exit(1);
      }
          
     }else{
       cout << "NO Free Worker found when sempaphore said there should be one!";
       std::exit(1);
     }


    // Result doesn't come back through here; goes directly to thread that queued request

   }  // while not resetting

    exitedMainLoop.post();
    resumeMainLoop.wait();
  }

}

/*
 * Process result message when it becomes available.
 * Doesn't run in the context of the TileRequestHandler thread! How twisty is that?
 * It's here so we can match up the reply with the request that was
 * saved in the TileRequestHandlerThread when it was sent out.
 * On entry an IPROBE has returned true indicating a message is available.
 */
void TileRequestHandlerThread::readAndProcessMessage(MPI_Status *probeStatus)
{
   int sender = probeStatus->MPI_SOURCE;
   if ( (sender< (*groupIdToSystemId)[0]) || (sender > (*groupIdToSystemId)[numWorkersInTileGroup-1])){
     cerr << "TileRequestHandlerThread: sender out of expected range: " << sender << 
       " Tag = " << probeStatus->MPI_TAG << "\n";
     std::exit(1);
     return;
   } 

   int worker = (*systemIdToGroupId)[sender];
   if ((worker<0) || (worker> numWorkersInTileGroup-1)){
      cerr << "TileRequestHandlerThread: worker out of expected range: " << sender << "\n";
      std::exit(1);
      return;
   } 
   EvaluationRequest *request = pendingRequest[worker];
   if (request == 0){
     cerr << "TileRequestHandlerThread: received result matched to null request!\n";
     cerr << "Worker = " << worker << "\n";
     cerr << "Sender = " << sender << "\n";
     cerr << "Msg Tag = " << probeStatus->MPI_TAG << "\n";
     for (int i=0; i<numWorkersInTileGroup; i++){
       cerr << "pendingRequest[" << i << "] = " << pendingRequest[i] << "\n";
     }
     abort();
   }

   // compute some stats for debugging
   endTime[worker] = MPI_Wtime();
   int executionTime = static_cast<int>((endTime[worker]-startTime[worker])*1000);
   if (executionTime<0){
     cerr << "Execution time < 0!\n";
     std::exit(1);
   }

   if (executionTime >= static_cast<int>(histo[worker].size())){
     //cout << "TRHT thresholding execution time from " << executionTime << "\n";
      executionTime = static_cast<int>(histo[worker].size())-1;
   }

   //cout << "THRT Execution Time = " << executionTime << "\n";

   histo[worker][executionTime]++;


   // Let requesthandler thread know there is a worker available
   pendingRequest[worker] = 0;

   request->LogIt("Posted result");
   workerAvailableSemaphore.post();
   workerAvailableSemaphoreCount++;


   if (request == 0){
     cout << "Unable to match received result message with request ! ignoring\n";
     std::exit(1);
   }
   

   // init requests are created here in TileRequestHandlerThread; there
   // is no caller to delete them so we do it here
   // If we were brave we could do a "delete this" in the initEvaluationRequestHandler
   // This capture of "initEvaluationRequest or not" must happen before Recv below
   // because after receive request may no longer be valid
   // Note.  Only MPI slave InitEvalRequests are deleted here.  The masterworkerthread
   // gets rid of its initevalrequest with a separate mechanism.

   InitEvaluationRequest *initEvaluationRequest = 
            dynamic_cast<InitEvaluationRequest*>(request);

   recvTime.start();
   request->Recv(probeStatus);  // call message-specific handler function to read and process message.
   recvTime.stop();

   // Can't assume anything about request at this point; caller may have finished
   // and deleted the request!

   if (initEvaluationRequest != 0){  // was set above
     initEvaluationRequest->LogIt("Deleted in Request Handler");
     delete initEvaluationRequest;
  }
}


/*
 *  Announce to world that master thread is available.
 *  Initialize thread by consuming InitEvaluationRequest that
 *  should be pending for the master.
 */
void TileRequestHandlerThread::masterWorkerRegister()
{
  EvaluationRequest *request = pendingRequest[0];
  InitEvaluationRequest *initEvaluationRequest = 
            dynamic_cast<InitEvaluationRequest*>(request);

  if (request == 0){
    cerr << "No pending initialization request for master worker thread during register\n";
    std::exit(1);
  }
  if (initEvaluationRequest == 0){
    cerr << "Pending request is not initialization request for master worker thread\n";
    std::exit(1);
  }

  //consume initialization request through explicit mechanism
  masterWorkerAvailable();

  delete initEvaluationRequest;  // leak not!
}




void TileRequestHandlerThread::masterWorkerAvailable()
{
  pendingRequest[0] = 0; // clear pending request array slot for master
  workerAvailableSemaphore.post();
  workerAvailableSemaphoreCount++;
}


// Tell the thread to stop itself at it's earliest convenience.  
// sets a flag, and then based on intimate knowledge of the thread, trick it into
// waking up if it's blocked by incrementing the semaphore that it waits on.
// Uses two flags so that the target thread doesn't free itself while we're still
// trying to do a post.

// THREAD MUST BE "RESETPAUSED" before calling this method
void TileRequestHandlerThread::stopPausedThread(){
   cout << "TileRequestHandlerThread.stop : queue size = " << requestQueue.size() << "\n";

   stopRequested = true;
   resumeMainLoop.post(); // stopRequestedFlag causes outer loop to exit.
}


// leaves the target thread exited from main loop and waiting on "waitToContinue" sempaphore
void TileRequestHandlerThread::resetPause()
{
   #ifdef TILE_REQUEST_HANDLER_FINE_TRACE
      cout << "TileRequestHandlerThread.pause : queue size = " << requestQueue.size() << "\n";
   #endif

   // set variables to let the thread to a known state
   resetRequested = true;
   workerAvailableSemaphore.post();  // unstick the thread if it's waiting for a worker     
   workerAvailableSemaphoreCount++;
   requestAvailableSemaphore.post(); // unstick the thread if it's waiting for a request
   requestAvailableSemaphoreCount++;

   // wait until target thread exits main loop
   exitedMainLoop.wait();

   resetRequested = false;

   // clear semaphores
   // not technically safe, but no-one should be accessing the semaphore at this point
   while (workerAvailableSemaphoreCount) {
     workerAvailableSemaphore.wait();
     workerAvailableSemaphoreCount--;
   }

   while (requestAvailableSemaphoreCount) {
     requestAvailableSemaphore.wait();
     requestAvailableSemaphoreCount--;
   }


  #if 0
   // print out statistics
   cout << "Execution Statistics\n";
   for(int i=0; i< static_cast<int>(histo.size()); i++){
     cout << "Worker " << i << "\n";
     for (int j=0; j<static_cast<int>(histo[i].size()); j++){
       cout << histo[i][j]<< "  ";
     }
     cout << "\n";
   }
   
   cout << "workerWaitTime = " << workerWaitTime.cummulativeTime << "\n";
   cout << "workWaitTime = " << workWaitTime.cummulativeTime << "\n";
   cout << "mutexWaitTime = " << mutexWaitTime.cummulativeTime << "\n";

   cout << "probeTime = " << probeTime.cummulativeTime << "\n";
   cout << "IprobeTime = " << IprobeTime.cummulativeTime << "\n";
   cout << "loopEventWaitTime" << loopEventWaitTime.cummulativeTime << "\n";

   cout << "thrqMutexHoldTime = " << thrqMutexHoldTime.cummulativeTime << "\n";

   cout << "recvTime = " << recvTime.cummulativeTime << "\n";
  #endif // dump statistics 
}

/*
 *
 */
// Let the thread start up again
void TileRequestHandlerThread::unPause()
{
   resumeMainLoop.post();   
}




/*
 * Helper function for TileMPIThread - tells if all slaves have a request on them
 * MPIMUtex is held by caller
 */
bool TileRequestHandlerThread::allWorking()
{
  int size = (int) pendingRequest.size();
  for (int i=0; i<size; i++){
    if (i == 0) continue;  //skip master. eventually remove this when we add ability for
    // tile master to serve requests.
    if ( pendingRequest[i] == 0) return false;
  }
  
  return true;

}

bool TileRequestHandlerThread::noneWorking()
{
  int size = (int) pendingRequest.size();
  for (int i=0; i<size; i++){
    if (i == 0) continue;  //skip master. eventually remove this when we add ability for
    // tile master to serve requests.
    if ( pendingRequest[i] != 0) return false;
  }
  
  return true;

}



bool TileRequestHandlerThread::noMPISlaves()
{
  int size = (int) pendingRequest.size();
  if (size <=1) return true;  // master counts as one
  return false;

}

 
bool TileRequestHandlerThread::workAvailable()
{
  return (requestAvailableSemaphoreCount !=0);
}


/*
 *  cleanup the thread object with Run() returns or Thread.exit() is called
 *  Clients stop the thread by setting the member variable "stopRequested,
 *  causing the loop in the Run() method to exit.
 */
void TileRequestHandlerThread::final()
{
  // The common c++ documentation swears this is an okay thing to do.
   delete this;  //egad!
}

