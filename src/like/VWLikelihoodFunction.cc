#include <cmath>
#include <vector>
#include <iomanip>

#include <Distribution.h>
#include <Tile.h>
#include <VWLikelihoodFunction.h>
#include <BIEmpi.h>
#include <gfunction.h>
#include <Uniform.h>
#include <Normal.h>
#include <gaussQ.h>

#include <boost/math/special_functions/legendre.hpp>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::VWLikelihoodFunction)

using namespace std;
using namespace BIE;

unsigned VWLikelihoodFunction::Nmesh = 10000;

//
// Constructor
//
VWLikelihoodFunction::VWLikelihoodFunction() : LikelihoodFunction()
{
}

VWLikelihoodFunction::VWLikelihoodFunction(int N,
					   StateInfo *SI,
					   LikelihoodFunction* f) 
  : LikelihoodFunction()
{
  Nmax   = N;
  si     = SI;
  F      = f;

  Nknots = Nmax*16;
  Ntot   = si->Ntot + 1 + Nmax;
    
  lq = LegeQPtr(new LegeQuad(Nknots));
  makeLookupTable();

  // DEBUG
  lcheck();
}

//
// The only variable we care about here is <State* s>
//
double VWLikelihoodFunction::LikeProb
(std::vector<double> &z, SampleDistribution* sd, 
 double norm, Tile *t, State *s, int indx)
{
				// Sanity check #1
  if (Ntot!=s->N()) {
    ostringstream ostr;
    ostr << "VWLikelihoodFunction: I expected a parameter vector of size =" 
	 << Ntot << " which disagrees with State vector of size " 
	 << s->N() << endl;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

				// Sanity check #2
  if (Nmax+1!=s->size()-si->Ntot) {
    ostringstream ostr;
    ostr << "VWLikelihoodFunction: your StateInfo input implies a"
	 << " function space of rank=" << s->N() - si->Ntot - 1
	 << " which disagrees with the specified rank of " << Nmax << endl;
    throw DimNotMatchException(ostr.str(), __FILE__, __LINE__);
  }

				// The nominal parameter vector
  State orig(si);
  orig.assign(s->begin(), s->begin()+si->Ntot);

				// The Legendre coefficient vector
  psi.assign(s->begin()+si->Ntot+1, s->end());

  // Get Likelihood and G_psi
  pair<double, double> ret = 
    F->LikeProb(z, sd, norm, t, &orig, indx, static_cast<Fct1dPtr>(this));

				// Multiply them (add in log)
  return ret.first + ret.second;
}

void VWLikelihoodFunction::makeLookupTable()
{
  lg_table = vector< vector<double> >(Nmax);

  dx = 1.0/(Nmesh-1);
  for (unsigned i=0; i<Nmesh; i++) {
    for (unsigned j=1; j<=Nmax; j++)
      lg_table[j-1].push_back(	// Legendre polynomials scaled to unit interval
			      sqrt(2.0*j+1.0) *
			      boost::math::legendre_p(j, 2.0*dx*i-1.0)
				);
  }
}

double VWLikelihoodFunction::getFct(double u, const vector<double>& v)
{
  int indx   = max<int>(0, min<int>(Nmesh-1, floor(u/dx)));
  double a   = (dx*(indx+1) - u)/dx;
  double b   = (u - dx*indx)/dx;
  double ret = 0.0;

  if (fabs(a + b - 1.0)>1.0e-10) {
    cerr << "VWLikelihoodFunction interpolation error:"
	 << " a=" << a << " b=" << b << endl;
  }

  for (unsigned i=0; i<Nmax; i++) 
    ret += v[i]*(a*lg_table[i][indx] + b*lg_table[i][indx+1]);

  return exp(ret);
}

double VWLikelihoodFunction::normalize(const vector<double>& v)
{
  double ret = 0.0;

  for (int i=1; i<=Nknots; i++)
    ret += lq->weight(i)*getFct(lq->knot(i), v);

  return ret;
}

vector<double> VWLikelihoodFunction::getLege(double u)
{
  int indx   = max<int>(0, min<int>(Nmesh-1, floor(u/dx)));
  double a   = (dx*(indx+1) - u)/dx;
  double b   = (u - dx*indx)/dx;

  if (fabs(a + b - 1.0)>1.0e-10) {
    cerr << "VWLikelihoodFunction interpolation error:"
	 << " a=" << a << " b=" << b << endl;
  }

  vector<double> ret(Nmax);
  for (unsigned i=0; i<Nmax; i++) 
    ret[i] = a*lg_table[i][indx] + b*lg_table[i][indx+1];

  return ret;
}

void VWLikelihoodFunction::lcheck()
{
  vector< vector<double> > check(Nmax);
  for (unsigned i=0; i<Nmax; i++) check[i] = vector<double>(i+1, 0.0);

  for (int k=1; k<=Nknots; k++) {
    vector<double> ret = getLege(lq->knot(k));
    for (unsigned i=0; i<Nmax; i++) {
      for (unsigned j=0; j<i+1; j++) {
	check[i][j] += lq->weight(k) * ret[i]*ret[j];
      }
    }
  }

  cout << "Legendre function norm check:" << endl
       << "-----------------------------" << endl;
  cout << setw(10) << " ";
  for (unsigned i=0; i<Nmax; i++) cout << setw(10) << right << i+1;
  cout << endl;
  for (unsigned i=0; i<Nmax; i++) {
    cout << setw(10) << left << i+1;
    for (unsigned j=0; j<i+1; j++) {
      cout << setw(10) << right << fixed << setprecision(6) << check[i][j];
    }
    cout << endl;
  }
  cout << endl << scientific;
}

const std::string VWLikelihoodFunction::ParameterDescription(int i)
{
  ostringstream sout;

  if (i<static_cast<int>(si->Ntot)) {
    sout << si->ParameterDescription(i);
  } else if (i==static_cast<int>(si->Ntot))
    sout << "tau";
  else {
    ostringstream sout;
    sout << "Psi[" << i-si->Ntot << "]";
  }

  return sout.str();
}


