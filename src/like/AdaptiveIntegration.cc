// This may look like C code, but it is really -*- C++ -*-

#include <gaussQ.h>
#include <Distribution.h>
#include <AdaptiveIntegration.h>
#include <Distribution.h>
#include <BIEMutex.h>
#include <BIEException.h>
#include <BIEmpi.h>

BIE_CLASS_EXPORT_IMPLEMENT(BIE::AdaptiveIntegration)

using namespace BIE;

int AdaptiveIntegration::maxlevels = 16;

static AdaptiveIntegration* wrapper_ptr;

vector<double> AdaptiveIntegrationNormWrapper(double x, double y)
{
  return vector<double>(1, wrapper_ptr->NormIntegrand(x, y));
}

vector<double> AdaptiveIntegrationValueWrapper(double x, double y)
{
  return wrapper_ptr->ValueIntegrand(x, y);
}

AdaptiveIntegration::AdaptiveIntegration(double Eps, double Dx0, double Dy0, 
					 double Dx, double Dy)
{
  eps = Eps;
  dx0 = Dx0;
  dy0 = Dy0;
  dx  = Dx;
  dy  = Dy;
}

void AdaptiveIntegration::ParameterChange(double Eps, double Dx0, double Dy0, 
					  double Dx, double Dy)
{
  eps = Eps;
  dx0 = Dx0;
  dy0 = Dy0;
  dx = Dx;
  dy = Dy;
}

double AdaptiveIntegration::NormIntegrand(double x, double y)
{
  return imodel->NormEval(itile->X(x, y), itile->Y(x, y), idist) * 
    itile->measure(x, y);
}


vector<double> AdaptiveIntegration::ValueIntegrand(double x, double y)
{
  vector<double> z = imodel->Evaluate( itile->X(x, y), itile->Y(x, y), idist);

  double factor = itile->measure(x, y);

  for (int j=0; j<idist->numberData(); j++) z[j] *= factor;
  
  return z;
}

double AdaptiveIntegration::NormValue(Model* _model, Tile* _tile, 
				      SampleDistribution *_dist)
{
  imodel = _model;
  itile = _tile;
  idist = _dist;

  double xSize = fabs( _tile->X(1.0, 1.0) - _tile->X(0.0, 0.0) );
  double ySize = fabs( _tile->Y(1.0, 1.0) - _tile->Y(0.0, 0.0) );

  int nU0 = (int)( xSize/(dx0 + 1.0e-8) );
  int nV0 = (int)( ySize/(dy0 + 1.0e-8) );

  int nU = (int)( xSize/(dx + 1.0e-8) );
  int nV = (int)( ySize/(dy + 1.0e-8) );

  int minlev = min<int>( (int)(log(static_cast<float>(max<int>(nU0, nV0)))/log(2.0)), maxlevels);
  
  int maxlev = min<int>( (int)(log(static_cast<float>(max<int>(nU, nV)))/log(2.0)), maxlevels);
  
  wrapper_ptr = this;

  QuadTreeIntegrator quad(0.0, 1.0, 0.0, 1.0, 1,
			  AdaptiveIntegrationNormWrapper, eps, 
			  minlev, maxlev);

  vector<double> ans = quad.Integral();

  return ans[0];
}

vector<double> AdaptiveIntegration::Value(Model* _model, Tile* _tile,
					  SampleDistribution* _dist)
{
  imodel = _model;
  itile = _tile;
  idist = _dist;

  double xSize = fabs( _tile->X(1.0, 1.0) - _tile->X(0.0, 0.0) );
  double ySize = fabs( _tile->Y(1.0, 1.0) - _tile->Y(0.0, 0.0) );

  int nU0 = (int)( xSize/(dx0 + 1.0e-8) );
  int nV0 = (int)( ySize/(dy0 + 1.0e-8) );

  int nU = (int)( xSize/(dx + 1.0e-8) );
  int nV = (int)( ySize/(dy + 1.0e-8) );

  int minlev = min<int>( (int)(log(static_cast<float>(max<int>(nU0, nV0)))/log(2.0)), maxlevels);

  int maxlev = min<int>( (int)(log(static_cast<float>(max<int>(nU, nV)))/log(2.0)), maxlevels);

  wrapper_ptr = this;

  QuadTreeIntegrator quad(0.0, 1.0, 0.0, 1.0, idist->numberData(),
			  AdaptiveIntegrationValueWrapper, eps, 
			  minlev, maxlev);

  vector<double> ans = quad.Integral();
  
  return ans;
}

double AdaptiveIntegration::NormValue(Model* _model, Tile* _tile,
				      SampleDistribution *_dist,
				      int irank, int nrank)
{
  string msg = "Make sure to use TileGranularity, not IntegrationGranularity.";
  throw InternalError(msg, __FILE__, __LINE__);
  return 0.0;
}

vector<double> AdaptiveIntegration::Value(Model* _model, Tile* _tile,
					  SampleDistribution* _dist,
					  int irank, int nrank, MPI_Comm& comm)
{
  string msg = "Make sure to use TileGranularity, not IntegrationGranularity.";
  throw InternalError(msg, __FILE__, __LINE__);
  return vector<double>(1);
}

