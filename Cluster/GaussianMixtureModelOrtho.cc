// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

// #include <VectorM.h>
#include <Distribution.h>
#include <GaussianMixtureModelOrtho.h>

using namespace BIE;


GaussianMixtureModelOrtho::
GaussianMixtureModelOrtho(int nmodl, int ndim, int mdim) :
  GaussianMixtureModel(ndim, mdim)
{
  nmod = nmodl;

  // Sanity check

  rdim = nmod*(nmod-1)/2;	// Number of rotations
  if (ndim != 2*nmod + rdim) {
    throw BIEException("In GaussianMixtureModelOrtho", 
		       "model size and ndim do not match", 
		       __FILE__, __LINE__);
  }

  // Need to make new rotation matrices
  
  rot = vector<Matrix>(rdim);
  for (int i=0; i<rdim; i++) rot[i].setsize(1, nmod, 1, nmod);
  evec.setsize(1, nmod, 1, nmod);  
}

void GaussianMixtureModelOrtho::makerot(vector<double> &p)
{
  double ccos, ssin;
  int icnt = 0;
  for (int k=1; k<nmod; k++) {
    for (int l=k+1; l<=nmod; l++) {

      // Make identity matrix
      rot[icnt].zero();
      for (int j=1; j<=nmod; j++) rot[icnt][j][j] = 1.0;

      // Construct rotation matrix
      ccos = cos(2.0*M_PI*p[2*nmod+icnt]);
      ssin = sin(2.0*M_PI*p[2*nmod+icnt]);

      rot[icnt][k][k] =  ccos;
      rot[icnt][l][l] =  ccos;
      rot[icnt][k][l] =  ssin;
      rot[icnt][l][k] = -ssin;
      
      icnt++;
    }
  }

  if (icnt != rdim) {
    throw BIEException("In GaussianMixtureModelOrtho", 
		       "sanity check icnt != rdim failure", 
		       __FILE__, __LINE__);
  }

  evec = rot[0];
  for (int k=1; k<rdim; k++) evec = evec * rot[k];


#ifdef DEBUG
				// Unitarity check (for debugging)
  Matrix check(1, nmod, 1, nmod);
  check = evec*evec.Transpose();

  double dmax = 0.0;
  for (int i=1; i<=nmod; i++) {
    for (int j=1; j<=nmod; j++) {
      if (i==j) 
	dmax = max<double>(dmax, fabs(check[i][j]-1.0));
      else
	dmax = max<double>(dmax, fabs(check[i][j]    ));
    }
  }
  
  if (dmax > 1.0e-10) {
    throw BIEException("In GaussianMixtureModelOrtho", 
		       "unitarity failure", 
		       __FILE__, __LINE__);
  }
#endif

}



// X and Y are ignorable here

vector<double> GaussianMixtureModelOrtho::EvaluatePoint(double x, double y, 
							PointDistribution *d)
{
  vector<double> ret(1, 0.0);
  vector<double> p = d->Point();
  
  double z = 0.0;
  double norm, fac;

  makerot(p);

  for (int k=0; k<M; k++) {

    fac = 0.0;
    for (int n=0; n<nmod; n++) {
      for (int l=0; l<nmod; l++) {
	for (int m=0; m<nmod; m++) {
	  fac += (p[l] - pt[k][l]) * (p[m] - pt[k][m]) *
	    evec[1+n][1+l] * (1.0/pt[k][n+nmod]) * evec[1+n][1+m]; 
	  // evec[1+l][1+n] * (1.0/pt[k][n+nmod]) * evec[1+m][1+n]; 
	}
      }
    }
    
    norm = 1.0;
    for (int n=0; n<nmod; n++) norm *= sqrt(2.0*M_PI*fabs(pt[k][n+nmod]));

    z += wt[k] * exp(-0.5*fac) / max<double>(norm, 1.0e-24);
  }

  ret[0] = z;

  return ret;
}


/// Used to label output
string GaussianMixtureModelOrtho::ParameterDescription(int i)
{
  string ret;

  if (i<0 || i>=Ndim)
    ret = "**Error**\0";
  else if (i < nmod) {
    ostringstream ostr;
    ostr << "Ctr [" << i << "]\0";
    ret = ostr.str();
  }
  else if (i < 2*nmod) {
    ostringstream ostr;
    ostr << "Sig [" << i-nmod << "]\0";
    ret = ostr.str();
  } else {
    ostringstream ostr;
    ostr << "Rot [" << i-2*nmod << "]\0";
    ret = ostr.str();
  }

  return ret;
}


void GaussianMixtureModelOrtho::check_bounds()
{
  good_bounds = true;
				// Weights
  for (int k=0; k<M; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;

				// Coord range
    for (int j=0; j<nmod; j++)
      if (pt[k][j] > AMAX || pt[k][j] < AMIN) good_bounds = false;

				// EV range
    for (int j=nmod; j<2*nmod; j++)
      if (pt[k][j] > BMAX || pt[k][j] < 0.0) good_bounds = false;

				// Rotation parameters
    for (int j=2*nmod; j<Ndim; j++)
      if (pt[k][j] >=0.0 || pt[k][j] <= 1.0) good_bounds = false;

  }

				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int k=0; k<M; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<Ndim; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
#endif
}


