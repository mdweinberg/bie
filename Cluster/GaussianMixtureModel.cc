// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

// #include <VectorM.h>
#include <Distribution.h>
#include <GaussianMixtureModel.h>

using namespace BIE;

/// Limits for model parameters
double GaussianMixtureModel::AMIN = -1.0e20;
double GaussianMixtureModel::AMAX =  1.0e20;
double GaussianMixtureModel::BMAX =  1.0e20;

/// Tolerance for diagonal trunction
double GaussianMixtureModel::ETOL = 0.995;

void GaussianMixtureModel::printSeparator(const char *label)
{
  const int wid = 75;

  int c = cout.fill('-');
  cout.setf(ios::left);

  cout << setw(wid) << "-" << endl;
  cout << setw(wid) << label << endl;
  cout << setw(wid) << "-" << endl;

  cout.setf(ios::right);
  cout.fill(c);
}

GaussianMixtureModel::GaussianMixtureModel(int ndim, int mdim)
{
  Ndim = ndim;
  M = mdim;
  Mcur = M;

  // State
  wt = vector<double>(M);
  pt = new vector<double>[M];
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

}

GaussianMixtureModel::~GaussianMixtureModel()
{
  delete [] pt;
}


void GaussianMixtureModel::Initialize(State& s)
{
  Mcur = (int)floor(s[0]+0.01);
  for (int k=0; k<Mcur; k++) wt[k] = s[1+k];
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s[1+Mcur+k*Ndim+j];
  }

  check_bounds();
}

void GaussianMixtureModel::Initialize(vector<double>& w, vector<double>*& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    pt[k] = p[k];
  }

  check_bounds();
}


