// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

// #include <VectorM.h>
#include <Distribution.h>
#include <BIEException.h>
#include <ColorSpaceAxiLegendre.h>

using namespace BIE;

ColorSpaceAxiLegendre::ColorSpaceAxiLegendre(int ndim, int mdim, 
					     double rmin, double rmax,
					     SampleDistribution *dist)
  : ColorSpace(ndim, mdim, dist)
{

  if (ndim < 3) throw DimNotMatchException(__FILE__, __LINE__);
  
  a = rmin;
  b = rmax;


				// Number of basis terms - 1
				// 
  nmax = ndim - 2;
				// Initialize basis array
				// 
  basis = vector<double>(max<int>(nmax+1,2));
}

void
ColorSpaceAxiLegendre::get_basis(double y)
{
  double ap = 0.5*(b + a), am = 0.5*(b - a);
  double x = (y - ap)/am;

  if (x<-1.0 || x>1.0)
    throw BIEException("In ColorSpaceAxiLegendre::get_basis", 
		       "argument to Legendre polynomial is out of range",
		       __FILE__, __LINE__);

  // Starting values
  //
  basis[0] = 1.0;
  basis[1] = x;
  
  // Recursion
  //
  for (int n=1; n<nmax; n++) 
    basis[n+1] = ( basis[n]*(2.0*n + 1.0)*x - basis[n-1]*n )/(1.0+n);
  
  // Normalization
  //
  for (int n=0; n<=nmax; n++) basis[n] *= sqrt(0.5*(2.0*n+1.0)/am);

}

