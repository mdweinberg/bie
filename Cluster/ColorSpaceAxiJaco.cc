// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

// #include <VectorM.h>
#include <Distribution.h>
#include <BIEException.h>
#include <ColorSpaceAxiJaco.h>

using namespace BIE;

double ColorSpaceAxiJaco::alpha=0.0;
double ColorSpaceAxiJaco::beta=1.0;

ColorSpaceAxiJaco::ColorSpaceAxiJaco(int ndim, int mdim, 
				     double rmin, double rmax,
				     SampleDistribution *dist)
  : ColorSpace(ndim, mdim, dist)
{

  if (ndim < 3) throw DimNotMatchException(__FILE__, __LINE__);
  
  a = rmin;
  b = rmax;
  use_rphi = false;
				// Number of basis terms - 1
				// 
  nmax = ndim - 2;
				// Initialize basis array
				// 
  basis = vector<double>(max<int>(nmax+1,2));
}


void ColorSpaceAxiJaco::UseRPhi()
{
  use_rphi = true;
}

void ColorSpaceAxiJaco::EnforceDistribution(int number)
{
  scan_number = number;
}


//
// If A = 0, y = 0.5*B*(x + 1) or (x + 1) = 2*y/B, x = 2*y/B-1
// Therefore, pow(1.0-x, ALPHA) * pow(1.0+x, BETA) = 
// pow(2*(1-y/B), ALPHA) * pow(2*y/B, BETA)
//
// If ALPHA=0, power coefficient in y.
//
// In particular:
//
//
//  I = int^B_0    dy y^BETA T_j(y) T_k(y)
//
//    = 0.5*B*int^1_{-1} dx (0.5*B*(1+x))^BETA T_j(y) T_k(y)
//
//    = (0.5*B)^(BETA+1) * int^1_{-1} dx (1+x)^BETA T_j(y) T_k(y)
//
// Ex: B=1, BETA=1:
//
// I = 1/4

void
ColorSpaceAxiJaco::get_basis(double y)
{
  double am = 0.5*(b - a);
  double ap = 0.5*(b + a);
  double x = (y - ap)/am;
				// Bounds on independent variable
				// 
  if (x<-1.0 || x>1.0) {
    ostringstream ostr;
    ostr << "argument to Jaco polynomial, x=" << x << " is out of range";
    throw BIEException("In ColorSpaceAxiJaco::get_basis", ostr.str(),
		       __FILE__, __LINE__);
  }
				// Short-hand variables
				// 
  double ab   = alpha + beta;
  double ab1  = ab  + 1.0;
  double ab2  = ab1 + 1.0;
  double a2b2 = alpha*alpha - beta*beta;

  double n2, A, B, C, D;

  // Recursion relation
  // 
  basis[0] = 1.0;
  basis[1] = 0.5*(2.0*(alpha+1.0) + ab2*(x - 1.0));
  
  // For alpha=0, beta=1, basis[1] = 0.5*(3*x-1)

  for (int n=1; n<nmax; n++) {

    n2 = 2.0*n;
    A = 2.0*(1.0+n)*(ab1+n)*(ab+n2);
    B = (n2+ab1)*a2b2;
    C = (n2+ab)*(n2+ab1)*(n2+ab2);
    D = -2.0*(alpha+n)*(beta+n)*(ab2+n2);

    basis[n+1] = ( (B + C*x)*basis[n] + D*basis[n-1] ) / A;
  }

  // Normalization
  //
  for (int n=0; n<nmax+1; n++) {
    basis[n] /= sqrt(pow(0.5*b, beta+1.0) * pow(2.0, ab1)/(2.0*n+ab1) *
		     exp(lgamma(alpha+1.0+n) + lgamma(beta+1.0+n)
			 - lgamma(1.0+n) - lgamma(ab1+n))
		     );
  }

  // 
  // If alpha=0, beta=1, a=0, b=1:
  // basis[0] norm = 1/sqrt( 0.25 * 4/2 * (1*1) / (1*1)) = sqrt(2)
  // basis[1] norm = 1/sqrt( 0.25 * 4/4 * (1*2) / (1*2)) = 2
  //
  // So linear term is prob to basis[1] + sqrt(2)*basis[2]
  
}
