#
# Open data file
#
fid = fopen("test4.data")
#
# Read header (6 fields)
#
fgetl(fid)
fgetl(fid)
fgetl(fid)
fgetl(fid)
fgetl(fid)
fgetl(fid)
#
# Blank line
#
fgetl(fid)
#
# Read data matrix
#
[data, count] = fscanf(fid, "%g", [6, inf]);
#
# Histogram colors
#
hist = histogram(data(3,:), -1, 2.5, 0.01);
#
# Normalize histogram for theoretical comparison
#
hist(:,2) = hist(:,2)/(200000*0.01);
#
# Theoretical distributions
#
thr1 = exp(-(0.5-hist(:,1)).**2*0.5/0.2)/sqrt(2*pi*0.2) * 0.5;
thr2 = exp(-(1.0-hist(:,1)).**2*0.5/0.2)/sqrt(2*pi*0.2) * 0.5;
#
# Label plot
#
xlabel("Color")
ylabel("dP")
#
# Plot it
#
plot(hist(:,1),hist(:,2),'-;data;', hist(:,1),thr1, '-;Component 1;', hist(:,1), thr2, '-;Component 2;');
#
#
#
