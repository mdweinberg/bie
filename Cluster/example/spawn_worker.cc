// worker
 
#include <iostream>
#include <iomanip>

inline void error(char *msg)
{
 std::cerr << "ERROR: " << msg << std::endl;
}

#include <mpi.h>

int main(int argc, char *argv[]) 
{ 
  int numprocs, myid, proc_namelen, size;
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  MPI_Comm parent; 


  MPI_Init(&argc, &argv); 

  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Get_processor_name(processor_name, &proc_namelen);

  MPI_Comm_get_parent(&parent); 
  if (parent == MPI_COMM_NULL) error("No parent!"); 

  MPI_Comm_remote_size(parent, &size); 

  MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  std::cerr << "Spawned process " << myid
	    << ": parent size=" << size
	    << std::endl;

  /* 
   * Parallel code here.  
   * The manager is represented as the process with rank 0 in (the remote 
   * group of) MPI_COMM_PARENT.  If the workers need to communicate among 
   * themselves, they can use MPI_COMM_WORLD. 
   */ 
  
  std::cerr << "Spawned process " << myid
	    << " out of " << numprocs
	    << " total on " << processor_name << std::endl;

  MPI_Status status;
  int value;

  for (int i=0; i<size; i++) {
    MPI_Recv(&value, 1, MPI_INT, i, 11, parent, &status);
  
    std::cerr << "Spawned process " << myid 
	      << " received value=" << value
	      << " from parent" << std::endl;
  }

  MPI_Finalize(); 
  return 0; 
} 
