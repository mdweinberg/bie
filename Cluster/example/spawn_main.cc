// manager

#include <iostream>
#include <iomanip>

#include <mpi.h> 

inline void error(char *msg)
{
 std::cerr << "ERROR: " << msg << std::endl;
}

int main(int argc, char *argv[]) 
{ 
  int world_size, universe_size, *universe_sizep, flag, myid; 
  MPI_Comm everyone;           // intercommunicator
  char worker_program[100] = "spawn_worker"; 
 
  MPI_Init(&argc, &argv); 
  MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
 
  MPI_Comm_spawn(worker_program, MPI_ARGV_NULL, 1,  
		 MPI_INFO_NULL, 0, MPI_COMM_WORLD, &everyone,  
		 (int *)MPI_ERRCODES_IGNORE); 

  std::cerr << "Process " << myid << ": comm=" << everyone << std::endl;

  /* 
   * Parallel code here. The communicator "everyone" can be used 
   * to communicate with the spawned processes, which have ranks 0,.. 
   * MPI_UNIVERSE_SIZE-1 in the remote group of the intercommunicator 
   * "everyone". 
   */ 
  
  int value;
  MPI_Status status;


  // Everybody send a message to the process
  MPI_Send(&myid, 1, MPI_INT, 0, 11, everyone);

  
  sleep(1);

  int numprocs;
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  std::cout << "Process " << myid 
	    << ": current size is " << numprocs << std::endl;

  MPI_Finalize(); 
  return 0; 
} 
