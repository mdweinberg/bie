// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

// #include <VectorM.h>
#include <Distribution.h>
#include <BIEException.h>
#include <ColorSpaceAxiCheb.h>

using namespace BIE;

ColorSpaceAxiCheb::ColorSpaceAxiCheb(int ndim, int mdim, 
				     double rmin, double rmax,
				     SampleDistribution *dist)
  : ColorSpace(ndim, mdim, dist)
{

  if (ndim < 3) throw DimNotMatchException(__FILE__, __LINE__);
  
  a = rmin;
  b = rmax;


				// Number of basis terms - 1
				// 
  nmax = ndim - 2;
				// Initialize basis array
				// 
  basis = vector<double>(max<int>(nmax+1,2));
}

void
ColorSpaceAxiCheb::get_basis(double y)
{
  double ap = 0.5*(b + a), am = 0.5*(b - a);
  double x = (y - ap)/am;

  if (x<-1.0 || x>1.0) {
    ostringstream ostr;
    ostr << "argument to Cheb polynomial, x=" << x << " is out of range";
    throw BIEException("In ColorSpaceAxiCheb::get_basis", ostr.str(),
		       __FILE__, __LINE__);
  }

  // Starting values
  //
  basis[0] = 1.0;
  basis[1] = x;
  
  // Recursion
  //
  for (int n=1; n<nmax; n++) 
    basis[n+1] = 2.0*x*basis[n] - basis[n-1];
  
  // Normalization
  //
  for (int n=0; n<=nmax; n++) basis[n] *= sqrt(2.0/M_PI/am)/sqrt(fabs(y)+1.0e-18);
  basis[0] /= M_SQRT2*sqrt(fabs(y)+1.0e-18);

}

