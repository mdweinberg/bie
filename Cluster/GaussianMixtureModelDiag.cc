// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

// #include <VectorM.h>
#include <Distribution.h>
#include <GaussianMixtureModelDiag.h>

using namespace BIE;

GaussianMixtureModelDiag::GaussianMixtureModelDiag(int ndim, int mdim)
  : GaussianMixtureModel(ndim, mdim)
{
  dim = Ndim/2;

  if (2*dim != Ndim) throw DimNotMatchException(__FILE__, __LINE__);
}

/// Used to label output
string GaussianMixtureModelDiag::ParameterDescription(int i)
{
  string ret;

  if (i<0 || i>=Ndim)
    ret = "**Error**\0";
  else if (i < dim) {
    ostringstream ostr;
    ostr << "Ctr [" << i << "]\0";
    ret = ostr.str();
  } else {
    ostringstream ostr;
    ostr << "Sig [" << i-dim << "]\0";
    ret = ostr.str();
  }

  return ret;
}


void GaussianMixtureModelDiag::check_bounds()
{
  good_bounds = true;
				// Weights
  for (int k=0; k<M; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;

				// Coord range
    for (int j=0; j<dim; j++)
      if (pt[k][j] > AMAX || pt[k][j] < AMIN) good_bounds = false;

				// EV range
    for (int j=dim; j<Ndim; j++)
      if (pt[k][j] > BMAX || pt[k][j] < 0.0) good_bounds = false;

  }

				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int k=0; k<M; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<Ndim; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
#endif
}


// X and Y are ignorable here

vector<double> GaussianMixtureModelDiag::EvaluatePoint(double x, double y, 
						       PointDistribution *d)
{
  vector<double> ret(1, 0.0);
  vector<double> p = d->Point();
  
  double z = 0.0;
  double norm, fac;

  for (int k=0; k<M; k++) {

    fac = 0.0;
    for (int n=0; n<dim; n++) {
      fac += (p[n] - pt[k][n]) * (p[n] - pt[k][n]) *(1.0/pt[k][n+dim]);
    }
    
    norm = 1.0;
    for (int n=0; n<dim; n++) norm *= sqrt(2.0*M_PI*fabs(pt[k][n+dim]));

    z += wt[k] * exp(-0.5*fac) / max<double>(norm, 1.0e-24);
  }

  ret[0] = z;

  return ret;
}

