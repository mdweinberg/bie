// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

#include <Covariance.H>

string CovarianceException::getErrorMessage()
{
  // This combines the error name, error message, 
  // and the throw locations.
  string buffer = (*errormessage).str();

  ostringstream wholemessage;
  wholemessage << exceptionname << ": " << buffer << endl;
  wholemessage << "Thrown from " << sourcefilename << ":" << sourcelinenumber;
  
  return wholemessage.str();
}

CovarianceException::CovarianceException
(string exceptionname, string message, 
 string sourcefilename, int sourcelinenumber)
{
  errormessage = new ostringstream;
  this->sourcefilename   = sourcefilename;
  this->sourcelinenumber = sourcelinenumber; 
  this->exceptionname = exceptionname;
  (*errormessage) << message;
}

CovarianceException::
CovarianceException(string sourcefilename, int sourcelinenumber)
{
  errormessage = new ostringstream;
  this->sourcefilename   = sourcefilename;
  this->sourcelinenumber = sourcelinenumber; 
}

Covariance::Covariance(int nmodl, int nmix)
{
  nmod = nmodl;
  mdim = nmix;

  // Sanity check

  rdim = nmod*(nmod-1)/2;	// Number of rotations

  // Make rotation matrices
  
  rot = vector<Matrix>(rdim);
  for (int i=0; i<rdim; i++) rot[i].setsize(1, nmod, 1, nmod);
  evec.setsize(1, nmod, 1, nmod);  
  covar.setsize(1, nmod, 1, nmod);
  invcov.setsize(1, nmod, 1, nmod);
}

Matrix Covariance::get_covariance(string file, int LEVEL, int ITER, int COMP)
{
  ifstream in(file.c_str());
  if (!in) {
    throw CovarianceException("Covariance exception", 
			      "can not open input file", 
			      __FILE__, __LINE__);
  }

  const int linesz = 2048;
  char line[linesz];

  vector<double> weights(mdim);
  vector< vector<double> > model(mdim);
  for (int i=0; i<mdim; i++) model[i] = vector<double>(nmod);

  int level;
  int iter;
  double prob;

  in.getline(line, linesz);	// Discard header
  in.getline(line, linesz);	// Get next line

  while (in) {
    istringstream sin(line);
    sin >> level;
    sin >> iter;
    sin >> prob;
    for (int i=0; i<mdim; i++) sin >> weights[i];
    for (int i=0; i<mdim; i++) {
      for (int j=0; j<2*nmod+rdim; j++) sin >> model[i][j];
    }

    if (LEVEL == level && ITER == iter) {
      makerot(model[COMP]);
      makecovar(model[COMP]);
      makeinvcovar(model[COMP]);
      return covar;
    }

    in.getline(line, linesz);	// Get next line
  }

  throw CovarianceException("Covariance exception", 
			    "can not find desired datum", 
			    __FILE__, __LINE__);

  return Matrix();
}

void Covariance::makecovar(vector<double> &p)
{
  covar.zero();

  for (int l=0; l<nmod; l++) {
    for (int m=0; m<nmod; m++) {
      for (int n=0; n<nmod; n++) {
	covar[l+1][m+1] += evec[1+l][1+n] * p[n+nmod] * evec[1+m][1+n]; 
      }
    }
  }
}


void Covariance::makeinvcovar(vector<double> &p)
{
  invcov.zero();

  for (int l=0; l<nmod; l++) {
    for (int m=0; m<nmod; m++) {
      for (int n=0; n<nmod; n++) {
	invcov[l+1][m+1] += evec[1+l][1+n] * 1.0/p[n+nmod] * evec[1+m][1+n]; 
      }
    }
  }
}


void Covariance::makerot(vector<double> &p)
{
  diag.erase(diag.begin(), diag.end());
  rotp.erase(rotp.begin(), rotp.end());

  for (int k=0; k<nmod; k++) diag.push_back(p[nmod+k]);

  double ccos, ssin;
  int icnt = 0;
  for (int k=1; k<nmod; k++) {
    for (int l=k+1; l<=nmod; l++) {

      // Make identity matrix
      rot[icnt].zero();
      for (int j=1; j<=nmod; j++) rot[icnt][j][j] = 1.0;

      // Construct rotation matrix
      ccos = cos(2.0*M_PI*p[2*nmod+icnt]);
      ssin = sin(2.0*M_PI*p[2*nmod+icnt]);

      rotp.push_back(2.0*M_PI*p[2*nmod+icnt]);

      rot[icnt][k][k] =  ccos;
      rot[icnt][l][l] =  ccos;
      rot[icnt][k][l] =  ssin;
      rot[icnt][l][k] = -ssin;
      
      icnt++;
    }
  }

  if (icnt != rdim) {
    throw CovarianceException("Covariance exception", 
			      "sanity check icnt != rdim failure", 
			      __FILE__, __LINE__);
  }

  evec = rot[0];
  for (int k=1; k<rdim; k++) evec = evec * rot[k];


				// Unitarity check (for debugging)
  Matrix check(1, nmod, 1, nmod);
  check = evec*evec.Transpose();

  double dmax = 0.0;
  for (int i=1; i<=nmod; i++) {
    for (int j=1; j<=nmod; j++) {
      if (i==j) 
	dmax = max<double>(dmax, fabs(check[i][j]-1.0));
      else
	dmax = max<double>(dmax, fabs(check[i][j]    ));
    }
  }
  
  if (dmax > 1.0e-10) {
    throw CovarianceException("Covariance exception", 
			      "unitarity failure", 
			      __FILE__, __LINE__);
  }



}
