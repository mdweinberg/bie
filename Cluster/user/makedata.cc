/* 
   makedata - Test clustering using empirical orthogonal functions

   Copyright (C) 2005 Martin Weinberg

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

using namespace std;

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>

#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <getopt.h>

#include "config.h"

#include "ACG.h"
#include "Uniform.h"
#include "Normal.h"
#include "VectorM.h"
#include "gaussQ.h"

#define EXIT_FAILURE 1

static void usage (int status);

// The name the program was run with, stripped of any leading path. 
char *program_name;

// Option flags and variables 

string oname, dname;
ofstream ofile, dfile;
long iseed = 11;
bool CHEB = false;

static struct option const long_options[] =
{
  {"output",	required_argument,	0, 'o'},
  {"data",	required_argument,	0, 'd'},
  {"cheb",	required_argument,	0, 'c'},
  {"number1",	required_argument,      0, '1'},
  {"number2",	required_argument,      0, '2'},
  {"dim",       required_argument,      0, 'N'},
  {"color1",	required_argument,      0,  0 },
  {"color2",	required_argument,      0,  0 },
  {"variance1",	required_argument,      0,  0 },
  {"variance2",	required_argument,      0,  0 },
  {"verbose",	no_argument,		0, 'v'},
  {"help",	no_argument,		0, 'h'},
  {"version",	no_argument,		0, 'V'},
  {0,		0, 			0,   0}
};
static int decode_switches (int argc, char **argv);

  
vector<double> get_basis(double y, int nmax)
{
  vector<double> val(max<int>(nmax+1,2));
  double ap = 0.5*(1.0 + 0.0);
  double am = 0.5*(1.0 - 0.0);
  double x = (y - ap)/am;

  // Starting values
  val[0] = 1.0;
  val[1] = x;
  
  if (CHEB) {

  // Recursion
  //
    for (int n=1; n<nmax; n++) 
      val[n+1] = 2.0*x*val[n] - val[n-1];
  
    // Normalization
    //
    for (int n=0; n<=nmax; n++) val[n] *= sqrt(2.0/M_PI/am);
    val[0] /= M_SQRT2;


  }  else {

    // Recursion
    for (int n=1; n<nmax; n++) 
      val[n+1] = ( val[n]*(2.0*n + 1.0)*x - val[n-1]*n )/(1.0+n);
  
    // Normalization
    for (int n=0; n<=nmax; n++) val[n] *= sqrt(2.0*n+1.0);

  }

  return val;
}

int N = 8;
int npoints1 = 20000;
int npoints2 = 20000;
double col1 = 0.2;
double var1 = 0.1;
double col2 = 1.2;
double var2 = 0.1;

int
main (int argc, char **argv)
{
  program_name = argv[0];
  
  int n = decode_switches (argc, argv);
  
  // Do the work 
  //
  ACG gen(iseed, 20);
  Uniform ran(0.0, 1.0, &gen);
  Normal nrm1(col1, var1, &gen);
  Normal nrm2(col2, var2, &gen);
  
  // Dimension of data vector
  //
  int C = 1;
  int dim = N + C; 		// Legendre bins + Color bins
  
  double y, phi, wght = 1.0/(npoints1 + npoints2);
  vector<double> ret(N), ans;
  
  VectorM mean(0, dim-1);
  MatrixM vars(0, dim-1, 0, dim-1);
  
  mean.zero();
  vars.zero();
  
  // Test basis
  //
  const int NINT = 40;
  LegeQuad lq(NINT);

  MatrixM test(0, N-1, 0, N-1);
  test.zero();
  
  double yy, wght2 = 1.0;

  for (int i=1; i<=NINT; i++) {
    
    ret = get_basis(lq.knot(i), N-1);

    if (CHEB) {
      yy = (lq.knot(i) - 0.5)/0.5;
      wght2 = 1.0/sqrt(1.0 - yy*yy);
    }

    for (int j=0; j<N-1; j++) {
      for (int k=0; k<N-1; k++) {
	test[j][k] += lq.weight(i) * wght2 *  ret[j]*ret[k];
      }
    } 
  }
  
  cout << endl;
  cout << "Basis test\n";
  test.print(cout);
  cout << endl;

  if (dfile) {
    dfile << "real \"x\"" << endl;
    dfile << "real \"y\"" << endl;
    dfile << "real \"attribute\"" << endl;
    for (int i=0; i<N; i++)
      dfile << "real \"coef " << i << "\"" << endl;
    dfile << endl;
  }

  // Up
  //
  for (int i=0; i<npoints1; i++) {
    
    y = pow(ran(), 0.333333333);
    ret = get_basis(y, N-1);

    ans.erase(ans.begin(), ans.end());
    ans.push_back(nrm1());
    ans.insert(ans.end(), ret.begin(), ret.end());
    
    assert( (int)ans.size() == N+1 );

    if (dfile) {
      phi = 2.0*M_PI*ran();
      dfile << setw(18) << y*cos(phi) << setw(18) << y*sin(phi);
      for (int j=0; j<dim; j++)	dfile << setw(18) << ans[j];
      dfile << endl;
    }

    for (int j=0; j<dim; j++) {
      mean[j] += wght * ans[j]; 
      for (int k=0; k<dim; k++) {
	vars[j][k] += wght * ans[j]*ans[k];
      }
    } 

  }
  
  // Flat
  //
  for (int i=0; i<npoints2; i++) {
    y = sqrt(ran());
    ret = get_basis(y, N-1);

    ans.erase(ans.begin(), ans.end());
    ans.push_back(nrm2());
    ans.insert(ans.end(), ret.begin(), ret.end());

    assert( (int)ans.size() == N+1 );

    if (dfile) {
      phi = 2.0*M_PI*ran();
      dfile << setw(18) << y*cos(phi) << setw(18) << y*sin(phi);
      for (int j=0; j<dim; j++)	dfile << setw(18) << ans[j];
      dfile << endl;
    }

    for (int j=0; j<dim; j++) {
      mean[j] += wght * ans[j]; 
      for (int k=0; k<dim; k++) {
	vars[j][k] += wght * ans[j]*ans[k];
      }
    } 
    
  }
  
  dfile.close();
  
  // Compute variance
  //
  for (int j=0; j<dim; j++) {
    for (int k=0; k<dim; k++) {
      // vars[j][k] -= mean[j]*mean[k];
    }
  } 
  
  // Perform SVD
  //
  MatrixM U(0, dim-1, 0, dim-1), V(0, dim-1, 0, dim-1);
  MatrixM W(0, dim-1, 0, dim-1);
  VectorM Z(0, dim-1);
  
  SVD(vars, U, V, Z);
  
  // Ortho test
  for (int i=0; i<2; i++) {
    double tmp = 0.0;
    for (int j=0; j<dim; j++)
      tmp += V[0][j]*V[i][j];
    cout << "i=" << i << " mod=" << tmp << endl;
  }

  for (int i=0; i<4; i++) {
    cout << setw(18) << Z[i];
    for (int j=0; j<dim; j++)
      cout << setw(18) << V[i][j];
    cout << endl;
  }
  
  W.zero();
  for (int j=0; j<dim; j++) W[j][j] = Z[j];
  
  
  cout << endl;
  cout << "Input variance\n";
  vars.print(cout);
  cout << endl;
  cout << endl;
  cout << "SVD check\n";
  (V*W*U.Transpose() - U*W*V.Transpose()).print(cout);
  cout << endl;
  cout << endl;
  cout << "U - V\n";
  (U - V).print(cout);
  cout << endl;
  cout << "Mean\n";
  mean.print(cout);
  cout << endl;
  
  if (ofile) {
    const int nsize = 100;
    double y;
    ofile << "# " << setw(13) << "Color";
    for (int j=0; j<N; j++) ofile << setw(15) << V[j][0];
    ofile << endl;
    ofile << "# " << setw(13) << "EV";
    for (int j=1; j<dim; j++) ofile << setw(15) << Z[j];
    ofile << endl;
    ofile << "# " << setw(13) << "Radius/#";
    for (int j=0; j<dim; j++) ofile << setw(15) << j;
    ofile << endl;

    for (int n=0; n<nsize; n++) {
      y = 1.0/nsize*(0.5+n);
      ofile << setw(15) << y;
      
      vector<double> ret = get_basis(y, N-1);
      for (int i=0; i<dim; i++) {
	double ans = 0.0;
	for (int j=0; j<N; j++) ans += V[i][j]*ret[j];
	ofile << setw(15) << ans;
      }
      {
	double ans = 0.0;
	for (int j=0; j<N; j++) ans += mean[j]*ret[j];
	ofile << setw(15) << ans;
      }
      

      ofile << endl;
    }
  }
      
  exit (0);
}

// Set all the option flags according to the switches specified.
// Return the index of the first non-option argument.

static int
decode_switches (int argc, char **argv)
{
  int c, option_index;
  
  while ((c = getopt_long (argc, argv, 
			   "v"	// verbose 
			   "1:" // Number in first set
			   "2:" // Number in second set
			   "N:"	// data dimension
			   "o:"	// output 
			   "d:"	// data
			   "s:"	// seed 
			   "c:"	// Chebshev flag 
			   "h"	// help 
			   "V",	// version 
			   long_options, &option_index)) != EOF)
    {
      switch (c)
	{
	case 0:
	  
	  if (string("color1").compare(long_options[option_index].name)==0)
	    col1 = atof(optarg);
	  
	  if (string("color2").compare(long_options[option_index].name)==0)
	    col2 = atof(optarg);
	  
	  if (string("variance1").compare(long_options[option_index].name)==0)
	    var1 = atof(optarg);
	  
	  if (string("variance2").compare(long_options[option_index].name)==0)
	    var2 = atof(optarg);
	  
	  break;

	case 'o':		// --output 
	  oname = optarg;
	  ofile.open(optarg);
	  if (!ofile)
	    {
	      cout << "cannot open " << optarg << " for writing" << endl;
	      exit(1);
	    }
	  break;

	case 'd':		// --data
	  dname = optarg;
	  dfile.open(optarg);
	  if (!dfile)
	    {
	      cout << "cannot open " << optarg << " for writing" << endl;
	      exit(1);
	    }
	  break;

	case 'V':
	  cout << "eof_test " << VERSION << endl;
	  exit (0);
	  
	case 'N':
	  N = atoi(optarg);
	  break;
	  
	case '1':
	  npoints1 = atoi(optarg);
	  break;
	  
	case '2':
	  npoints2 = atoi(optarg);
	  break;
	  
	case 'c':
	  CHEB = atoi(optarg) ? true : false;
	  break;
	  
	case 's':
	  iseed = atoi(optarg);
	  break;
	  
	case 'h':
	  usage (0);
	  
	default:
	  usage (EXIT_FAILURE);
	}
    }
  
  return optind;
}


static void
usage (int status)
{
  cout << program_name << " - \
Test clustering using empirical orthogonal functions" << endl;
  cout << "Usage: " << program_name << " [OPTION]... [FILE]..." << endl;
  cout << "\
Options:\n\
  -1, --number1 N1           number of points in first component\n\
  -2, --number2 N2           number of points in second component\n\
  -N, --dim data_dim         dimension of output data array\n\
  --color1 c1                color coordinate of first component\n\
  --color2 c2                color coordinate of second component\n\
  --variance1 v1             variance in color of first component\n\
  --variance2 v2             variance in color of second component\n\
  -o, --output NAME          send output to NAME instead of standard output\n\
  --verbose                  print more information\n\
  -h, --help                 display this help and exit\n\
  -V, --version              output version information and exit\n\
";
  exit (status);
}
