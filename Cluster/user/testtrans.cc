/* 
   makedata - Test orthgonal transformation

   Copyright (C) 2005 Martin Weinberg

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

using namespace std;

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>

#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <getopt.h>

#include "config.h"

#include "ACG.h"
#include "Uniform.h"
#include "Normal.h"
#include "VectorM.h"
#include "gaussQ.h"
#include "linalg.h"

#define EXIT_FAILURE 1

static void usage (int status);

// The name the program was run with, stripped of any leading path. 
char *program_name;

// Option flags and variables 

int npoints = 10000;
long iseed = 11;

static struct option const long_options[] =
{
  {"output", required_argument, 0, 'o'},
  {"number", required_argument, 0, 'n'},
  {"seed", required_argument, 0, 's'},
  {"help", no_argument, 0, 'h'},
  {"version", no_argument, 0, 'V'},
  {NULL, 0, NULL, 0}
};
static int decode_switches (int argc, char **argv);

  
void divider(ostream& out, const char *lab)
{
  int c = out.fill('-');
  out << setw(75) << '-' << endl;
  out << setw(75) << lab << endl;
  out << setw(75) << '-' << endl;
  out.fill(c);
}

int
main (int argc, char **argv)
{
  program_name = argv[0];
  
  int nargs = decode_switches (argc, argv);
  
  // Do the work 
  //
  ACG gen(iseed, 20);
  Normal nrm1(0.0, 4.0, &gen);
  Normal nrm2(0.0, 1.0, &gen);
  
  // Dimension of data vector
  //

  MatrixM cov(1, 2, 1, 2);
  cov.zero();
  
  // Compute covariance
  //

  double x, y, wght = 1.0/npoints;
  vector<double> z(2);

  for (int i=0; i<=npoints; i++) {
    
    x = nrm1();
    y = nrm2();

    z[0] = (x - y)/M_SQRT2;
    z[1] = (x + y)/M_SQRT2;
    
    for (int k=0; k<2; k++)
      for (int l=0; l<2; l++)
	cov[1+k][1+l] += z[k]*z[l]*wght;
  }
  
  cov *= 0.5;

  cout << endl;
  ostringstream sout;
  sout << "Covariance: det=" << determinant(cov);
  divider(cout, sout.str().c_str());
  cov.print(cout);
  cout << endl;

  MatrixM covE(1, 2, 1, 2);
  covE[1][1] = covE[2][2] = 5.0/4.0;
  covE[1][2] = covE[2][1] = 3.0/4.0;
  
  cout << endl;
  divider(cout, "Expected");
  covE.print(cout);
  cout << endl;


  // Compute eigenvalue problem
  //

  MatrixM evec = cov;
  VectorM eval = cov.Symmetric_Eigenvalues_GHQL(evec);

  cout << endl;
  divider(cout, "Eigenvalues");
  eval.print(cout);
  cout << endl;

  VectorM evalE(1, 2);
  evalE[1] = 2.0;
  evalE[2] = 0.5;

  cout << endl;
  divider(cout, "Expected");
  evalE.print(cout);
  cout << endl;

  cout << endl;
  divider(cout, "Eigenvector");
  evec.print(cout);
  cout << endl;

  MatrixM evecE(1, 2, 1, 2);
  evecE.zero();
  evecE += 1.0/M_SQRT2;
  evecE[1][2] *= -1.0;

  cout << endl;
  divider(cout, "Expected");
  evecE.print(cout);
  cout << endl;

  exit (0);
}

// Set all the option flags according to the switches specified.
// Return the index of the first non-option argument.

static int
decode_switches (int argc, char **argv)
{
  int c;
  
  while ((c = getopt_long (argc, argv, 
			   "n:"	// npoints
			   "s"	// seed
			   "h"	// help 
			   "V",	// version 
			   long_options, (int *) 0)) != EOF)
    {
      switch (c)
	{
	case 'V':
	  cout << "test_trans " << VERSION << endl;
	  exit (0);
	  
	case 'n':
	  npoints = atoi(optarg);
	  break;
	  
	case 's':
	  iseed = atoi(optarg);
	  break;
	  
	case 'h':
	  usage (0);
	  
	default:
	  usage (EXIT_FAILURE);
	}
    }
  
  return optind;
}


static void
usage (int status)
{
  cout << program_name << " - \
Test orthogonal transformation" << endl;
  cout << "Usage: " << program_name << " [OPTION]... [FILE]..." << endl;
  cout << "\
Options:\n\
  -s seed                    set random number seed\n\
  -n seed                    set number of random points\n\
  -h, --help                 display this help and exit\n\
  -V, --version              output version information and exit\n\
";
  exit (status);
}
