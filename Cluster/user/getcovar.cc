/* 
   getcovar - Read in simulation data, compute covariance from
   rotation parameters

   Copyright (C) 2005 Martin Weinberg

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

using namespace std;

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>

#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <getopt.h>

#include <Covariance.H>

#define EXIT_FAILURE 1

static void usage (int status);

// The name the program was run with, stripped of any leading path. 
char *program_name;

// Option flags and variables 

ofstream ofile;
string oname = "getcovar.out";
string dname = "script.data";
int level=0, iter=1000, comp=1;
int ndim=4, nmix=2;

static struct option const long_options[] =
{
  {"data",	required_argument,	0, 'd'},
  {"level",	required_argument,	0, 'l'},
  {"iter",	required_argument,	0, 'i'},
  {"comp",	required_argument,	0, 'c'},
  {"ndim",	required_argument,	0, 'n'},
  {"nmix",	required_argument,	0, 'm'},
  {"verbose",	no_argument,		0, 'v'},
  {"help",	no_argument,		0, 'h'},
  {0,		0,			0,   0}
};
static int decode_switches (int argc, char **argv);

int
main (int argc, char **argv)
{
  program_name = argv[0];
  
  int n = decode_switches (argc, argv);
  cout << "# tokens:" << n << endl;
  
  Covariance covar(ndim, nmix);

  cout.precision(2);
  cout.setf(ios::scientific);

  Matrix cov = covar.get_covariance(dname, level, iter, comp);
  Matrix invcov = covar.get_inv_covariance();

  Matrix ident =  invcov * cov;

  cout << endl;
  cout << "Ident:" << endl;
  for (int i=ident.getrlow(); i<=ident.getrhigh(); i++) {
    for (int j=ident.getclow(); j<=ident.getchigh(); j++)
      cout << setw(12) << ident[i][j];
    cout << endl;
  }
  cout << endl << endl;

  vector<double> diag = covar.get_diag();
  vector<double> rotp = covar.get_rotation();

  cout << "Diagonal:" << endl;
  for (unsigned i=0; i<diag.size(); i++) cout << setw(12) << diag[i];
  cout << endl << endl;

  cout << "Rotation angles:" << endl;
  for (unsigned i=0; i<rotp.size(); i++) cout << setw(12) << rotp[i];
  cout << endl << endl;

  cout << "Covariance:" << endl;
  for (int i=cov.getrlow(); i<=cov.getrhigh(); i++) {
    for (int j=cov.getclow(); j<=cov.getchigh(); j++)
      cout << setw(12) << cov[i][j];
    cout << endl;
  }
  cout << endl << endl;

  cout << "Inverse covariance:" << endl;
  for (int i=invcov.getrlow(); i<=invcov.getrhigh(); i++) {
    for (int j=invcov.getclow(); j<=invcov.getchigh(); j++)
      cout << setw(12) << invcov[i][j];
    cout << endl;
  }
  cout << endl;

  exit (0);
}

// Set all the option flags according to the switches specified.
// Return the index of the first non-option argument.

static int
decode_switches (int argc, char **argv)
{
  int c;
  
  while ((c = getopt_long (argc, argv, 
			   "v"	// verbose 
			   "o:"	// output file
			   "d:"	// data
			   "l:"	// level
			   "i:"	// iteration #
			   "c:"	// which component
			   "n:"	// number of dimensions
			   "m:"	// number of components
			   "h",	// usage
			   long_options, (int *) 0)) != EOF)
    {
      switch (c)
	{
	case 'o':		// --output 
	  oname = optarg;
	  ofile.open(optarg);
	  if (!ofile)
	    {
	      cout << "cannot open " << optarg << " for writing" << endl;
	      exit(1);
	    }
	  break;

	case 'd':		// --data
	  dname = optarg;
	  break;

	case 'l':		// --level
	  level = atoi(optarg);
	  break;

	case 'i':		// --iter
	  iter = atoi(optarg);
	  break;

	case 'c':		// --comp
	  comp = atoi(optarg);
	  break;

	case 'n':		// --ndim
	  ndim = atoi(optarg);
	  break;

	case 'm':		// --nmix
	  nmix = atoi(optarg);
	  break;

	case 'h':
	  usage (0);
	  
	default:
	  usage (EXIT_FAILURE);
	}
    }
  
  return optind;
}


static void
usage (int status)
{
  cout << program_name << " - \
Test clustering using empirical orthogonal functions" << endl;
  cout << "Usage: " << program_name << " [OPTION]... [FILE]..." << endl;
  cout << "\
Options:\n\
  -o, --output NAME          send output to NAME instead of standard output\n\
  -d, --data   NAME          input file containing simulation output data\n\
  -l, --level  level         select level for multilevel simulation\n\
  -i, --iter   iter          select iteration number from the level\n\
  -c, --comp   comp          select the mixture component for this iteration\n\
  -n, --ndim   ndim          model dimensionality\n\
  -m, --nmix   nmix          number of components in the mixture\n\
  --verbose                  print more information\n\
  -h, --help                 display this help and exit\n\
  -V, --version              output version information and exit\n\
";
  exit (status);
}
