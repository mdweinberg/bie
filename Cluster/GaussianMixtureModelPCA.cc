// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>
using namespace std;

#include <VectorM.h>
#include <Distribution.h>
#include <GaussianMixtureModelPCA.h>

using namespace BIE;


GaussianMixtureModelPCA::
GaussianMixtureModelPCA(int ndim, int mdim, DataTree* _full) :
  GaussianMixtureModel(ndim, mdim)
{
  _full->GetDefaultFrontier()->UpDownLevels(100);

  SampleDistribution * _dist = _full->First();

  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    throw NoSuchDistributionException(__FILE__, __LINE__);

  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);

  dim = ndim/2;

  mean.setsize(1, dim);
  covar.setsize(1, dim, 1, dim);
  eval.setsize(1, dim);
  evec.setsize(1, dim, 1, dim);

  vector<double> p;
  
  int cnt=0;
  mean.zero();
  covar.zero();

  while (!_full->IsDone()) {
    
    p = ((PointDistribution *)_dist)->Point();

				// 
				// Accumulate mean and covariance
    for (int i=0; i<dim; i++) {
      mean[1+i] += p[i];
      for (int j=0; j<dim; j++)
	covar[1+i][1+j] += p[i]*p[j];
    }

				// 
				// Get next tile
    _dist = _full->Next();
    cnt++;
  }

  cout << __FILE__ 
       << ": count=" << cnt
       << "  items=" << _full->NumberItems()
       << endl;

  _full->CountData();
  _full->GetDefaultFrontier()->UpDownLevels(-100);
  _full->CountData();


  // Compute mean
  for (int i=0; i<dim; i++) mean[1+i] /= cnt;

  // Compute covariance
  for (int i=0; i<dim; i++)
      for (int j=0; j<dim; j++)
	covar[1+i][1+j] = covar[1+i][1+j]/cnt - mean[1+i]*mean[1+j];

  // Compute eigenvalues and eigenvectors
  VectorM eval = covar.Symmetric_Eigenvalues_GHQL(evec);
  
  // Evec check
  printSeparator("Eval");

  eval.print(cout);

  printSeparator("Ev * Ev^T");

  (evec * evec.Transpose()).print(cout);

  printSeparator("Ev^T * Ev");

  (evec.Transpose() * evec).print(cout);

  printSeparator("Ev * D * Ev^T");

  MatrixM D(1, dim, 1, dim);
  D.zero();
  for (int i=1; i<=dim; i++) D[i][i] = eval[i];
  (evec * D * evec.Transpose()).print(cout);

  printSeparator("Ev * D * Ev^T - covar");

  (evec * D * evec.Transpose() - covar).print(cout);

  printSeparator("Covar");

  covar.print(cout);

  printSeparator();

}

// X and Y are ignorable here

vector<double> GaussianMixtureModelPCA::EvaluatePoint(double x, double y, 
						      PointDistribution *d)
{
  vector<double> ret(1, 0.0);
  vector<double> p = d->Point();
  
  double z = 0.0;
  double norm, fac;

  for (int k=0; k<M; k++) {

    fac = 0.0;
    for (int n=0; n<dim; n++) {
      for (int l=0; l<dim; l++) {
	for (int m=0; m<dim; m++) {
	  fac += (p[l] - pt[k][l]) * (p[m] - pt[k][m]) *
	    evec[1+l][1+n] * (1.0/pt[k][n+dim]) * evec[1+m][1+n]; 
	}
      }
    }
    
    norm = 1.0;
    for (int n=0; n<dim; n++) norm *= sqrt(2.0*M_PI*fabs(pt[k][n+dim]));

    z += wt[k] * exp(-0.5*fac) / max<double>(norm, 1.0e-24);
  }

  ret[0] = z;

  return ret;
}

/// Used to label output
string GaussianMixtureModelPCA::ParameterDescription(int i)
{
  string ret;

  if (i<0 || i>=Ndim)
    ret = "**Error**\0";
  else if (i < dim) {
    ostringstream ostr;
    ostr << "Ctr [" << i << "]\0";
    ret = ostr.str();
  } else {
    ostringstream ostr;
    ostr << "Sig [" << i-dim << "]\0";
    ret = ostr.str();
  }

  return ret;
}


void GaussianMixtureModelPCA::check_bounds()
{
  good_bounds = true;
				// Weights
  for (int k=0; k<M; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;

				// Coord range
    for (int j=0; j<dim; j++)
      if (pt[k][j] > AMAX || pt[k][j] < AMIN) good_bounds = false;

				// EV range
    for (int j=dim; j<Ndim; j++)
      if (pt[k][j] > BMAX || pt[k][j] < 0.0) good_bounds = false;

  }

				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int k=0; k<M; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<Ndim; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
#endif
}


