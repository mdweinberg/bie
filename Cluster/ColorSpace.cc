// This may look like C code, but it is really -*- C++ -*-

#include <iomanip>
#include <sstream>

using namespace std;

// #include <VectorM.h>
#include <Distribution.h>
#include <ColorSpace.h>

using namespace BIE;

/// Limits for model parameters
double ColorSpace::AMIN = -1.0e20;
double ColorSpace::AMAX =  1.0e20;
double ColorSpace::BMIN = -1.0e20;
double ColorSpace::BMAX =  1.0e20;
double ColorSpace::MINP =  0.0;


void ColorSpace::printSeparator(const char *label)
{
  const int wid = 75;

  int c = cout.fill('-');
  cout.setf(ios::left);

  cout << setw(wid) << "-" << endl;
  cout << setw(wid) << label << endl;
  cout << setw(wid) << "-" << endl;

  cout.setf(ios::right);
  cout.fill(c);
}


ColorSpace::ColorSpace(int ndim, int mdim, SampleDistribution* _dist)
{
  Ndim = ndim;
  M = mdim;
  Mcur = M;

  // State
  //
  wt = vector<double>(M);
  pt = new vector<double>[M];
  for (int k=0; k<M; k++) pt[k] = vector<double>(Ndim);

  if (dynamic_cast <BinnedDistribution*> (_dist)) {

    BinnedDistribution *histo = (BinnedDistribution*)_dist;
    type = binned;
    

				// Cache the bin boundaries for the data
				// histogram
				// Sanity check
    if (histo->getdim(0) != 1) {
      ostringstream msg;
      msg << "BinnedDistribution is " << histo->getdim(0)
	  << " dimensional, model assumes 1 color band\n";
      bomb(msg.str());
    }
    

				// Cache the bin boundaries for the data
				// histogram
    nbins = histo->numberData();
    for (int i=0; i<nbins; i++) {
      lowb.push_back(histo->getLow(i)[0]);
      highb.push_back(histo->getHigh(i)[0]);
    }

    bins = vector<double>(nbins);

  } else if (dynamic_cast <PointDistribution*> (_dist)) {
    type = point;
    nbins = 1;
  }
  else
    throw NoSuchDistributionException(__FILE__, __LINE__);
  
				// Default range scanning values
  scan_number = 0;		// 0 points in [a, b] (e.g. off to start)
  use_rphi = false;
}


ColorSpace::~ColorSpace()
{
  delete [] pt;
}


void ColorSpace::Initialize(State& s)
{
  Mcur = (int)floor(s[0]+0.01);

  for (int k=0; k<Mcur; k++) wt[k] = s[1+k];
  for (int k=0; k<Mcur; k++) {
    for (int j=0; j<Ndim; j++) pt[k][j] = s[1+Mcur+k*Ndim+j];
  }

  cache.clear();
  check_bounds();
}

void ColorSpace::Initialize(vector<double>& w, vector<double>*& p)
{
  wt = w;
  for (int k=0; k<M; k++) {
    pt[k] = p[k];
  }

  cache.clear();
  check_bounds();
}


vector<double> ColorSpace::EvaluatePoint(double x, double y, 
					 PointDistribution *d)
{
  vector<double> p = d->Point();
  
  return Eval(x, y, p);
}


vector<double> ColorSpace::EvaluateBinned(double x, double y, 
					  BinnedDistribution *d)
{
  Duple duple(x, y);
  if ( (pcache=cache.find(duple)) == cache.end()) {
    generate(x, y);
    cache[duple] = bins;
    return bins;
  }

  return pcache->second;
}



double ColorSpace::NormEval(double x, double y, SampleDistribution *sd)
{
  if (type == point) return 1.0;

  if (!good_bounds) return 1.0;

  Duple duple(x, y);
  if ( cache.find(duple) == cache.end()) {
    generate(x, y);
    cache[duple] = bins;
  }

  pcache=cache.find(duple);
  double norm = 0.0;
  for (int j=0; j<nbins; j++)  norm += (pcache->second)[j];
  
  return norm;
}


void ColorSpace::generate(double x, double y)
{
  if (type == point) return;

  double r;
  double prob;
  bool nok = false;

  if (use_rphi)
    r = x;
  else
    r = sqrt(x*x + y*y);

  if (r<a || r>b) {
    for (int j=0; j<nbins; j++) bins[j] = MINP;
    return;
  }
  else
    get_basis(r);


  for (int j=0; j<nbins; j++) bins[j] = 0.0;

				// Loop over each components
				//
  for (int k=0; k<Mcur; k++) {
      
    prob = 2.0/(b*b - a*a);	// P(0)=1 w/ unit norm over range
				// 
    for (int n=1; n<=nmax; n++) prob += pt[k][n+1] * basis[n];
    if (prob<=0.0) nok = true;
    if (use_rphi) prob *= r;
    
				// 
				// Begin integration over each bin
    for (int j=0; j<nbins; j++) {
    
      bins[j] += wt[k]*prob*0.5*
	(
	 erf((highb[j] - pt[k][0])/sqrt(2.0*fabs(pt[k][1]))) - 
	 erf((lowb[j]  - pt[k][0])/sqrt(2.0*fabs(pt[k][1]))) 
	 );
      
    }
    
  }
    

  if (nok) for (int j=0; j<nbins; j++) bins[j] = MINP;

}



vector<double> ColorSpace::Eval(double x, double y, vector<double>& p)
{
  vector<double> ret(1, 0.0);
  
  double r;

  if (use_rphi)
    r = x;
  else
    r = sqrt(x*x + y*y);

  if (r<a || r>b) return ret;

  get_basis(r);

  double z = 0.0;
  double prob;

  for (int k=0; k<Mcur; k++) {

    prob = 2.0/(b*b - a*a);	// P(0)=1 w/ unit norm over range
				// 
    for (int n=1; n<=nmax; n++) prob += pt[k][n+1] * basis[n];
    
				// Multiply by Gaussian prob from attribute
				// 
    prob *= exp(-(p[0] - pt[k][0])*(p[0] - pt[k][0])/(2.0*fabs(pt[k][1]))) /
      sqrt(2.0*M_PI*fabs(pt[k][1]));

				// Return teensy value MINP, 
				// if prob is negative
    if (prob<0.0) return ret;

				// State is Ok . . .
				// accumulate
    z += wt[k] * prob;
  }

  ret[0] = z;

  return ret;
}


/// Used to label output
string ColorSpace::ParameterDescription(int i)
{
  string ret;

  if (i<0 || i>=Ndim)
    ret = "**Error**";
  else if (i == 0)
    ret = "Color";
  else if (i == 1)
    ret = "Sigma";
  else {
    ostringstream ostr;
    ostr << "Coef [" << i-1 << "]";
    ret = ostr.str();
  }
  
  return ret;
}


void ColorSpace::check_bounds()
{
  good_bounds = true;
				// Weights
				//
  for (int k=0; k<Mcur; k++) {
    if (wt[k] < 0.0 || wt[k] > 1.0) good_bounds = false;

				// Color range
				// 
    if (pt[k][0] > AMAX || pt[k][0] < AMIN) good_bounds = false;

				// Coef range
				// 
    for (int j=1; j<Ndim; j++)
      if (pt[k][j] > BMAX || pt[k][j] < BMIN) good_bounds = false;

  }
				// Scan range
				//
  if (scan_number) {

    double r, dr = (b - a)/scan_number;

    for (int i=0; i<scan_number; i++) {

      r = a + dr*i;
      get_basis(r);

      double prob;

      for (int k=0; k<Mcur; k++) {

	prob = 2.0/(b*b - a*a);	// P(0)=1 w/ unit norm over range
				// 
	for (int n=1; n<=nmax; n++) prob += pt[k][n+1] * basis[n];
    
	if (prob<=0.0) good_bounds = false;
      }
    }

  }

				// DEBUG
#ifdef DEBUG
  if (good_bounds == false) {
    cout << "State rejected:\n";
    for (int k=0; k<Mcur; k++) {
      cout << setw(15) << wt[k];
      for (int j=0; j<Ndim; j++) cout << setw(15) << pt[k][j];
      cout << "\n";
    }
  }
#endif
}


