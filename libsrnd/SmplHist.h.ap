// This may look like C code, but it is really -*- C++ -*-
/* 
Copyright (C) 1988 Free Software Foundation
    written by Dirk Grunwald (grunwald@cs.uiuc.edu)

This file is part of the GNU C++ Library.  This library is free
software; you can redistribute it and/or modify it under the terms of
the GNU Library General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your
option) any later version.  This library is distributed in the hope
that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU Library General Public License for more details.
You should have received a copy of the GNU Library General Public
License along with this library; if not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef SampleHistogram_h
#define SampleHistogram_h 1

#include <iostream>
using namespace std;

#include <SmplStat.h>

@include_persistence

namespace BIE {

  extern const int SampleHistogramMinimum;
  extern const int SampleHistogramMaximum;

  /**
   * A simple histogram class that puts values into buckets and uses the
   * SampleStatictic superclass to compute running statistics.
   */
  class @persistent(SampleHistogram) : public @super(SampleStatistic) 
  {
    
  protected:
    /// Total number of buckets.  There are overflow buckets on each side
    /// of the actual requested area.
    unsigned short @autopersist(howManyBuckets);
    
    /// Array that holds the number of items in each bucket.
    int * bucketCount;
    @autopersist_array(int,bucketCount,howManyBuckets);
    
    /// Array holding the upper limit for each bucket.
    double *bucketLimit;
    @autopersist_array(double,bucketLimit,howManyBuckets);


  public:
    
    /// Constructs a new SampleHistogram.  The default bucket width is a tenth
    /// of the different between low and hi.
    SampleHistogram(double low, double hi, double bucketWidth = -1.0);

    /// Cleans up the memory allocated by an instance.
    ~SampleHistogram();

    /// Sets all bucket counts to zero.
    virtual void reset();
    
    /// Adds a value to the sample histogram and sample statistic superclass.
    virtual void operator+=(double);

    /// Returns the number of items in the bucket the given value would
    /// go into.
    int similarSamples(double);

    /// Returns the number of buckets, including the 2 overflow buckets.
    int buckets();

    /// Returns the theshold value for the ith bucket.
    double bucketThreshold(int i);
    
    /// Returns the number of the number of values in the ith bucket.
    int inBucket(int i);
    
    /// Prints debugging information to the given stream.
    void printBuckets(ostream&);

    @persistent_end
  };


  inline int SampleHistogram:: buckets() { return(howManyBuckets); };

  inline double SampleHistogram:: bucketThreshold(int i) {
    if (i < 0 || i >= howManyBuckets)
      error("invalid bucket access");
    return(bucketLimit[i]);
  }

  inline int SampleHistogram:: inBucket(int i) {
    if (i < 0 || i >= howManyBuckets)
      error("invalid bucket access");
    return(bucketCount[i]);
  }

} // namespace BIE
  
#endif
