/* 
Copyright (C) 1988 Free Software Foundation
    written by Dirk Grunwald (grunwald@cs.uiuc.edu)

Rewrite by Martin D. Weinberg for better computational efficiency.

This file is part of the GNU C++ Library.  This library is free
software; you can redistribute it and/or modify it under the terms of
the GNU Library General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your
option) any later version.  This library is distributed in the hope
that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU Library General Public License for more details.
You should have received a copy of the GNU Library General Public
License along with this library; if not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <Random.h>
#include <Poisson.h>
#include <Normal.h>

BIE_CLASS_EXPORT_IMPLEMENT(Poisson)

double Poisson::eval(double lambda)
{
  if (lambda < 30.0  ) return PoissonSmall(lambda);
  if (lambda < 5000.0) return PoissonLarge(lambda);
  // Very large
  return Normal(lambda, lambda, pGenerator)();
}

double Poisson::PoissonSmall(double lambda)
{
  // Algorithm due to Donald Knuth, 1969.
  double p = 1.0, L = exp(-lambda);
  double k = 0;
  do
    {
      k++;
      p *= pGenerator -> asDouble();
    }
  while (p > L);
  return k - 1;
}

double Poisson::PoissonLarge(double lambda)
{
  // "Rejection method PA" from "The Computer Generation of Poisson
  // Random Variables" by A. C. Atkinson Journal of the Royal
  // Statistical Society Series C (Applied Statistics) Vol. 28,
  // No. 1. (1979). The article is on pages 29-35. The algorithm given
  // here is on page 32.

  double c = 0.767 - 3.36/lambda;
  double beta = M_PI/sqrt(3.0*lambda);
  double alpha = beta*lambda;
  double k = log(c) - lambda - log(beta);
  
  while (1) {
    double u   = pGenerator -> asDouble();
    double x   = (alpha - log((1.0 - u)/u))/beta;
    double n   = (int) floor(x + 0.5);
    if (n < 0) continue;
    double v   = pGenerator -> asDouble();
    double y   = alpha - beta*x;
    double tmp = 1.0 + exp(y);
    double lhs = y + log(v/(tmp*tmp));
    double rhs = k + n*log(lambda) - lgamma(n+1);
    if (lhs <= rhs) {
      return n;
    }
  }
}
