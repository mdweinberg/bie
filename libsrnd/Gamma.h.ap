// This may look like C code, but it is really -*- C++ -*-

#ifndef _Gamma_h
#define _Gamma_h

#include <Random.h>

@include_persistence

/**
   Generate Gamma variates
   Adapted from TOMS 599 by MDW
*/
class @persistent(Gamma) : public @super(Random)  {

 private:
				// Global variables from TOMS 599
  float @ap(b), @ap(cc), @ap(dd), @ap(e), @ap(p), @ap(q);
  float @ap(rr), @ap(s), @ap(t), @ap(u), @ap(v), @ap(w), @ap(x);
  float @ap(q0), @ap(s2), @ap(aa), @ap(aaa), @ap(si);


 protected:
  double @autopersist(pAlpha);
  bool   @autopersist(haveCachedNormal);
  double @autopersist(cachedNormal);
  double getNormal();
    
 public:
  /** Constructor
      @param alpha is the shape parameter
      @param gen   is a pointer to the random number generator instance
  */
  Gamma(double alpha, RNG *gen);

  //! Get the shape parameter
  double alpha();

  //! Set the shape paramter
  double alpha(double x);

  //! Operator to return the variate
  virtual double operator()();

  @persistent_end
};

BOOST_SERIALIZATION_SHARED_PTR(Gamma)
typedef boost::shared_ptr<Gamma> GammaPtr;
    
inline Gamma::Gamma(double alpha, RNG *gen) : Random(gen)
{
  aa = aaa = 0.0;
  pAlpha = alpha;
  haveCachedNormal = false;
}

inline double Gamma::alpha() { return pAlpha; }

inline double Gamma::alpha(double x) {
  double tmp = pAlpha;
  pAlpha = x;
  return tmp;
}

#endif
