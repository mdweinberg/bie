#include <Cauchy.h>

BIE_CLASS_EXPORT_IMPLEMENT(Cauchy)

double Cauchy::operator()()
{
  return( posn + gamma*tan(0.5*M_PI*( pGenerator->asDouble() - 0.5 )) );
}
