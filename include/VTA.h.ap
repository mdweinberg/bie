// This is really -*- C++ -*-

#ifndef VTA_H
#define VTA_H

#include <deque>
#include <vector>
#include <StateFile.h>
#include <ScaleFct.h>

#include <boost/thread.hpp>

/** 
    \class VTA
    \brief Volume tessellation algorithm
    \author Martin Weinberg
    \date July 2009

    
    Integrate the probability over the sample distribution using the
    Volume Tessellation Algorithm 
*/
class VTA
{
private:

  bool measr, debug, uniq, lims;
  std::string tag;
  int scale;
  double scexp;

  //! Debug file counter
  boost::mutex mutex;
  static int fileCtr;

  int incrementCounter() {
    boost::mutex::scoped_lock scoped_lock(mutex);
    return ++fileCtr;
  }

  double vfrac;

public:
  //@{
  //! Manually set upper and lower bounds
  static std::vector<double> lowerBound;
  static std::vector<double> upperBound;
  //@}

  //! Use MPI (false by default)
  static bool useMPI;

  //! Use MPI for tree computation (false by default)
  static bool treMPI;

  //! Use nearest neighbor tree (false by default)
  static bool useNNT;

  //! Use ORB (false by default)
  static bool useORB;

  //! Use TNT (false by default)
  static bool useTNT;

  //! The maximum offset (in log) from the peak probability value
  static double deltaP;

  //! Maximum difference in probability values for quadrature
  static double dPmin;

  //! Number of levels for 2^n tree (default = 6)
  static int TNTlev;

  //! The number of returned points in the measure grid
  unsigned nY;

  //! Volume enclosing the points
  double Xvolume;

  //! The tessellated volume in parameter space
  std::map<int, double> Yvolume;
  //@{
  /** The lower, mean, and upper rectangle values of the quadrature
      for the marginal likelhood (VTA2) */
  std::map<int, double> Llower, Lmean, Lupper;
  //@}
  //@{
  /** The lower, mean, and upper rectangle values of the quadrature
      for the marginal prior (VTA2) */
  std::map<int, double> Rlower, Rmean, Rupper;
  //@}

  //@{
  /** The median, lower, upper, and mean values for probability values in
      each cell (VTA1) */
  std::map<int, std::vector<double> > reslt, lower, upper, vmean;
  //@}

  //@{
  //! Constructors
  VTA()
  {
    for (unsigned l=0; l<2; l++)
      reslt[l] = lower[l] = upper[l] = vmean[l] = std::vector<double>(4, 0.0);
    measr = debug = uniq = lims = false;
    scale = 0;
    scexp = 1;
    vfrac = 1.0;
  }
  VTA(int SCALE, bool UNIQ, double SCEXP=1) : uniq(UNIQ), scexp(SCEXP)
  {
    setScl(SCALE);
    for (unsigned l=0; l<2; l++)
      reslt[l] = lower[l] = upper[l] = vmean[l] = std::vector<double>(4, 0.0);
    measr = debug = lims = false;
    vfrac = 1.0;
  }
  //@}

  //@{
  //! Set flags

  //! Compute the measure function
  void Measure(bool v=true) { measr = v; }
  //! Print out debugging info (second parameter: print cell limits)
  void setDBG (bool v=true, bool w=false) { debug = v; lims = w; }
  //! Use only unique points
  void setUniq(bool v=true) { uniq  = v; }
  //! Scale parameter space, mapping the R^d into U^d
  void setScl (int  v=0);
  //! Print out cell/partition diagnostics
  void setLims(bool v=true) { lims  = v; }
  //! Print out measure info
  void setFile(std::string s){ tag   = s; }
  
  //@}

  //! Compute the results
  void compute(int cut, int Lkts, bool logM, double trimV,
	       std::deque<element>::iterator first,
	       std::deque<element>::iterator last ,
	       int mid=-1);

  //! Set the file counter
  void setCounter(int n) { fileCtr = n; }

  //! Get the trimmed fraction
  double getFraction() { return vfrac; }

};


#endif // VTA_H

