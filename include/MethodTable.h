// This is really -*- C++ -*-

#ifndef METHOD_TABLE_H
#define METHOD_TABLE_H

class CliArgList;
class AData;
#include "BIEException.h"
#include "GlobalTable.h"
#include <string>
#include <vector>
#include <map>

/** 
 * MethodTable: This class is our form of a reflective meta-object.
 * It holds details of classes and methods, and can be used to dynamically
 * invoke constructors and methods at run time. 
 */
class MethodTable
{
private:
  
  class _class_info;
  /**
   * A class that holds information about a method accesible through the
   * reflection mechanism.
   */
  class _method_info
  {
  public:
    /// Name of the method.
    char *method_name;
    /// Unique method signature incorporating class name, method name
    /// and argument types.
    char *arg_signature;
    /// True if this method is a static method.
    bool staticmethod;
    /// True if this method can be invoked from CLI.
    bool cli_invokable;
    /// Array holding pointers to argument information.
    CliArgList *alist;
    /// Return type.
    AData *ret;
    /// Character array holding a description of the method.
    string desc;
    /// Pointer back to class declaration.
    _class_info * classinfo;
  };
  
  /**
   * A class that holds information about a class accessible through the 
   * reflection mechanism. 
   */
  class _class_info
  {
  public:
    /// Name of the class.
    char *class_name;
    /// Number of methods in the class.
    int num_methods;
    /// If true, this class can be used in CLI.
    bool cli_class;
    /// Array of pointers to method information.
    _method_info *methods;
    /// Name of super class - NULL if there is no super class.
    char *super_class;
    /// Character array holding a description of the class.
    string desc;
  };
  
  /// Holds info for all the reflected classes
  static _class_info *classes;

  /// The number of reflected classes
  static int num_classes;
  
  /// Returns a pointer to the object _class_info object holding
  /// details of the given class.  Returns NULL if there is no such class.
  static _class_info * getClassInfo(const char * classname, bool cliquery);
  
  /// Given a list of methods with the same name, this chooses the one
  /// that best matches the arguments.  If none match, or the choice
  /// is not clear, an exception is thrown.
  static _method_info * choose_best_method
  (vector<_method_info*> & possibles, string method_name, 
   CliArgList *args, bool cliquery);
  
  enum {EXACT_MATCH = 0, CONVERSION_MATCH = 1, NOT_COMPATIBLE = 2};
  
  /// Returns a constant value indicating the level of compatibility an
  /// actual argument has with a formal argument definition.
  static int get_compatibility_level(const AData * formal, 
				     const AData * actual, bool cliquery);
  
  /// Returns a new list of arguments that have had values coerced where
  /// necessary.
  static CliArgList * coerce_arguments 
  (CliArgList * formal_params, CliArgList * actual_params);
  
  /// Get a list of callable methods
  static vector<_method_info *> get_callable_methods
  (const char *class_name, const char *method_name, bool cliquery);
  
  /**
     This class creats an inheritance tree from the class list to be
     used by the CLI for user help

     An empty instance of this class is created by the MethodTable by
     default and populated the first time one of the methods is called.
     The public methods are:
     <ol>
     <li> get_derived(string base): returns a vector of names that 
     inherent from "base"
     <li> print_graph(): prints a "tabbed" inheritance graph for all
     the classes
     </ol>
  */
  class _inheritance_tree
  {
    class _itnode 
    {
    public:
      /// Null constructor
      _itnode() : parent(0), level(0) {}

      /// Constructor
      _itnode(string& n, _itnode *p, bool cli) : 
	name(n), parent(p), cli_cls(cli) 
      {
	level = p->level + 1;
      }

      /// My class name
      string name;

      /// Pointer to parent
      _itnode *parent;

      /// Known to the CLI
      bool cli_cls;

      /// Level in the inheritance tree
      unsigned level;

      /// List of immediate child classes
      map<string, _itnode*> next;
      
      /// List of all of my descendants
      map<string, _itnode*> all;

    } root;

    /// Helper to crate descendant list
    void add_to_all(_itnode *new_node, _itnode *parent);

  public:
    /// Constructor
    _inheritance_tree(_class_info *classes, int num_classes);

    /// Destructor (will delete all element pointers)
    ~_inheritance_tree();

    /// Get list of children of a particular parent
    vector<string> get_derived(string root, bool cli);

    /// Print entire graph
    void print_graph(_itnode* p=0);

  };
  
  static _inheritance_tree *the_tree;

public:
  
  /// Set the type for this datum
  static void setType(AData *a , char typecode);
  
  /// Returns a new AData object that has had value coerced to formal
  /// value if this was required.
  static AData * coerce_argument(const AData *formal_arg, const AData *actual_param);
  
  //! Class has not been defined
  class NoSuchClassException : public BIE::BIEException 
  {
  public: 
    //! Constructor
    NoSuchClassException(string classname, string sourcefilename, int sourcelinenumber);
  };
  
  //! I don't have a constructor!
  class NoConstructorException : public BIE::BIEException 
  {
  public: 
    //! Constructor
    NoConstructorException(string classname, string sourcefilename, int sourcelinenumber);
  };
  
  //! CLI does not know about the requested method
  class NoSuchMethodException : public BIE::BIEException {
    //! Constructor
  public: NoSuchMethodException(string classname, string methodname, string sourcefilename, int sourcelinenumber);
  };
  
  //! Incorrect return type
  class ReturnTypeException : public BIE::BIEException 
  {
  public: 
    //! Constructor
    ReturnTypeException(string methodargstring, string sourcefilename, int sourcelinenumber);
  };
  
  //! Argument does not match defined type
  class ArgumentMismatchException : public BIE::BIEException 
  {
  public: 
    //! Constructor
    ArgumentMismatchException(string methodname, string sourcefilename, int sourcelinenumber);
  };
  
  //! Parser does not know which method to call
  class AmbiguousCallException : public BIE::BIEException 
  {
  public: 
    //! Constructor
    AmbiguousCallException(string methodname, string sourcefilename, int sourcelinenumber);
  };
  
  /// Initializes the class and method info by parsing the method string.
  static void initialize(string& method_string);
  
  /**
    Dynamically creates a new object of the specified class by invoking 
    the constructor with matching arguments.
    This method matches argument lists using the same preferences as C++.  
    Here are the steps taken:
    <ol>
    <li> Search class's list of methods for  methods named after the class.  
         Methods in superclasses are not considered.
    <li> Gathers list of constructors by looking at names
    <li> Chooses best match using C++ rules, e.g. prefer Con(int) to Con(double)
         when argument is an int or bool.
    </ol>
   */
  static void invoke_constructor
  (const char *class_name, CliArgList *args, AData *return_value, bool cliinvocation);
  
  /**
     Dynamically calls a method on the specified object.
     This method matches argument lists using the same preferences as C++.  
     Here are the steps taken:
     <ol>
     <li> Search class's list of methods for methods with the same name.
     <li> If there are no methods with the same name consider superclass.
     <li> Chooses best match using C++ rules, e.g. prefer a(int) to
          a(double) when argument is an int or bool.
     <li> Does not compare argument lists of methods in different
          class scopes - if a method is overloaded and there are others
	  in superclass that are still to be available, they should be
	  declared with C++'s 'using' keyword, and a stylized comment.
     </ol>
     For more details of these preferences, see Bjarne Stroustrup's,
     "The C++ Programming Language", third edition, section 7.4 on
     overloaded Function Names, and 15.2.2 on 'using' declarations.
   */
  static void invoke_method(const char *class_name, const char *method_name, 
			    CliArgList *args, AData *return_value, 
			    void *object, bool cliinvocation,
			    bool staticmethod = false);
  
  /**
    Dynamically calls a static method (a method call without an object
    instance).  Uses the same matching technique as invoke_method.
   */
  static void invoke_static(const char *class_name, const char *method_name, 
			    CliArgList *args, AData *return_value, 
			    bool cliinvocation);
  
  /** Returns true if the type of the second argument is compatible with
      or can be converted to the type of the first. 
  */
  static bool is_compatible_type(const AData *a1, const AData *a2, 
				 bool cliquery);
  
  /// Returns true if the type of the second argument is compatible with
  /// or can be converted to the type of the first.
  static bool is_compatible_return(const AData *a1, const AData *a2, 
				   bool cliquery);
  
  /// Returns true if the first class is a superclass of the second class.
  static bool is_compatible_class 
  (const char *class_to_match, const char *actual_class_name, bool cliquery);
  
  /// This returns a description of the class if the class exists.  If 
  /// calling from CLI, the class and method must be CLI accessible.  Returns 
  /// NULL if there is no such method or it is not accessible.
  static string get_class_desc(const char *class_name, bool cliquery);
  
  /// This returns a description of the method if the class and method exist.
  /// If calling from CLI, the class and method must be CLI accessible.  
  /// Returns NULL if there is no such method or it is not accessible.
  static string get_method_desc
  (const char *class_name, const char *method_name, bool cliquery);
  
  /// Fills in a string with a description of the arguments to a methog or
  /// constructor.
  static string get_method_arg_string(_method_info & m);
  
  /// Returns the name of the superclass of the specified class or returns
  /// NULL if there is no superclass.
  static const char* get_super_classname(const char *classname, bool cliquery);
  
  /// Used by CLI, this prints a list of the classes available in CLI.
  static void print_classes();
  
  /// Used by CLI, this prints a list of the methods available in CLI for
  /// the specified class.
  static void print_methods(const char *class_name);
  
  /// Returns true if a classname is valid.
  static bool isValidClassName(const char * class_name, bool cliquery);

  /// Used by CLI; get list of children of a particular parent
  static vector<string> get_derived(string root, bool cli_only=false);

  /// Used by CLI; print entire inheritance tree
  static void print_graph();

};

#endif
