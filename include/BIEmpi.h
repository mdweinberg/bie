// This is really -*- C++ -*-

#ifndef BIEmpi_h
#define BIEmpi_h

#include <mpi.h>

namespace BIE {
  //@{
  /// MPI variables
  extern MPI_Comm MPI_COMM_SLAVE;
  extern MPI_Comm CLI_COMM_WORLD;
  extern int numprocs, slaves, myid, proc_namelen;
  extern char processor_name[MPI_MAX_PROCESSOR_NAME];
  //@}
}

void mpi_init(int argc, char** argv);
void mpi_quit();

#endif
