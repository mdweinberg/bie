// This is really -*- C++ -*-

#ifndef VWPrior_h
#define VWPrior_h

#include <cfloat>
#include <cmath>

#include <boost/shared_ptr.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <MixturePrior.h>
#include <EnsembleKD.h>
#include <Normal.h>

@include_persistence

namespace BIE {
  
/**
   The Gaussian process prior for goodness-of-fit tests from
   Verdinelli & Wasserman (1998, Annals of Statistics, 26, 1215-1241)

   For consistence, this is a mixture with one component.  I should
   improve this interface to facilitate use of mixtures and non-mixtures.
*/
//+ CLICLASS VWPrior SUPER MixturePrior
class @persistent(VWPrior) : public @super(Prior) 
{

public:
  
  //! Maximum value for the tau "shape" hyperparameter (default: 10)
  static double taumax;

  //! Maximum value for the psi coefficient parameters (default: 10)
  static double psimax;

  //! Default constructor.
  //+ CLICONSTR
  VWPrior();

  //+ CLICONSTR StateInfo* int double Prior*
  /** Constructor with a center variance and limit values for the
      variational components and the KD ensemble instance for
      original, unaugmented subspace.  The total prior will be the
      joint probability of the nominal prior probability and the VW
      prior.

      @param si is the StateInfo for the augmented state
      @param mmax is the number of orthogonal functions in the
      non-parametric function space
      @param w is the width of the smoothing hyperparameter  
      @param pri is the prior for the nominal model
   */
  VWPrior(BIE::StateInfo* si, int mmax, double w, BIE::Prior *pri);

  //! Destructor
  virtual ~VWPrior() {}

  //! Object factor (clone)
  VWPrior* New();
   
  //+ CLIMETHOD void setLimits double double
  //! Reset the maximum tau and psi values
  void setLimits(double tau, double psi) { taumax = tau; psimax = psi; }

  //! Differential distribution function P(x)
  virtual double PDF(BIE::State& x);
  
  //! Log of differential distribution function P(x)
  virtual double logPDF(BIE::State& x);

  //! This is not implemented, die
  virtual double logPDFMarginal(int M, int n, const std::vector<double>& V) 
  {
    const char bad[] = "This is an augmented parameter space, even though the embedded model is a mixture";
    throw PriorException(bad, __FILE__, __LINE__);
    return 0.0;
  }

  //! Cumulative distribution function P(>x)
  virtual double CDF(BIE::State& x);

  //! Lower bound on distribution
  virtual std::vector<double> lower(void);
  
  //! Upper bound on distribution
  virtual std::vector<double> upper(void);
  
  //! Return mean of distribution
  virtual std::vector<double> Mean(void);
  
  //! Return standard deviation of distribution (mulitvariate)
  virtual std::vector<double> StdDev(void);
  
  //! Return specifided momemnt of distribution (mulitvariate)
  virtual std::vector<double> Moments(int i);

  //@{
  //! Return random variate from distribution
  virtual BIE::State Sample(void);
  virtual BIE::State Sample(int m) { return Sample(); }
  //@}
  
  //! @name Prior distributions sampling
  //@{
  /// Return weights and components separately
  virtual void SamplePrior(int& M, std::vector<double>& wght, 
			   std::vector< std::vector<double> >& phi);
  virtual void SamplePrior(int M, int n, std::vector<double>& V)
  {
    V = Sample();
  }
  /// Return the state (mixture or non-mixture)
  void SamplePrior(BIE::State *s);
  //@}


private:
  //! Configuration parameters
  int @autopersist(Nmax);
  double @autopersist(W), @autopersist(WW);

  //! The nominal prior instance
  Prior* @autopersist(P);

  //! The Legendre coefficient vectors
  vector<double> @autopersist(cj), @autopersist(cj2);
  boost::shared_ptr<Normal> @autopersist(normal);

  @persistent_end
};

} // namespace BIE

#endif

