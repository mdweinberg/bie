// This is really -*- C++ -*-


#ifndef DifferentialEvolution_h
#define DifferentialEvolution_h

@include_persistence

#include <LikelihoodComputation.h>
#include <MHWidth.h>
#include <MHWidthOne.h>
#include <Simulation.h>
#include <Chain.h>

namespace BIE {
  
  //! @addtogroup simulation
  //! @{

  class Prior;
  
  //+ CLICLASS DifferentialEvolution SUPER Simulation
  //! Implements the Differential Evolution (DE) algorithm 
  //! following the manuscript of Cajo J. F. Ter Braak 
  //! (Stat Comput 2006, 16:239-249)
  //!
  //! \par Intro
  //! Differential evolution uses parallel running Markov Chains with
  //! overdispersed initial conditions to set width of the proposal
  //! function.  It, therefore, is adaptatively capable of handling
  //! nondifferentiable, nonlinear and multimodal objective
  //! functions.
  //!
  //! \par Brief description
  //! The algorithm works as follows.  At each iteration, sometimes
  //! called a generation in the DE literature, new state vectors are
  //! shifted by the difference of a random selection of state vectors
  //! chosen from other chain plus a random variate chosen from a
  //! "background" distribution with unbounded support.
  //!
  //! \par Parameters
  //! The shift length is determined from the optimal estimate of the
  //! unimodal normal (Roberts and Rosenthal 2001) to be: 
  //! \f[ \gamma
  //! = {2.38\over\sqrt{(2d)}}
  //! \f] 
  //! where \f$d\f$ is the number of dimensions.  This can be changed
  //! manually using the NewGamma member.  In particular, the default
  //! value of \f$\gamma\f$ may need to be reduced from the optimal to 
  //! achieve a healthy acceptence rate for proposed new states.
  //!
  //! \par Input file
  //! The fourth parameter is string value for the
  //! file containing the chain-independent random variate parameters.
  //! The first line is the distribution type followed by the width of
  //! the symmetric sampling distribution for the weight parameters.
  //! Each successive line describes specifies sampling distribution for
  //! each parameter in the mixture model and contains four values: the
  //! symmetric sampling distribution, the width, and the upper and lower
  //! limit for the parameter.  The available symmetric distributions are
  //! listed in the enum #Distrib.
  //!
  //! \par Parameter range mapping
  //! All finite parameters ranges \f$(a, b)\f$ may be mapped using a
  //! Logit function, e.g.
  //! \f[
  //! q = M(x, a, b) = \log\left( {x-a\over b-x} \right)
  //! \f]
  //! and
  //! \f[
  //! M^{-1}(q, a, b) = {b + ae^{-q}\over 1+e^{-q}}.
  //! \f]
  //! A standard linear mapping is better under many situations so linear 
  //! mapping is the default (see #SetLinearMapping).
  //!
  //! \par Control methods
  //! There are two obvious possibilities for arranging the computation.
  //! <ol>
  //! <li> The parallel likelihood classes should be used with 
  //! #parallel control only.  The parallel likelihood classes 
  //! LikelihoodComputationMPI and LikelihoodComputationMPITP assume 
  //! that each process will enter the likelihood computation code.</li>
  //! <li> A serial likelihood class should be used with 
  //! #serial control only.  A serial likelihood computation is only 
  //! entered by the process which calls it.</li>
  //! </ol>
  //!
  //! \par Diagnostic output
  //! A call to the #EnableLogging member function will cause the state
  //! of each individual chain to be logged to a separate file.  The
  //! chain files will have names of the form <tt>nametag.chainlog.k</tt>
  //! where \ref Variables "nametag" is the global nametag variable and k 
  //! is the chain number.
  //!
  //! \par Example
  //! See <tt>script27</tt> in <tt>scripts/Splats/parallel</tt>
  //!
  class @persistent(DifferentialEvolution) : public @super(Simulation) 
  {
    
  public:

    /**@name Global variables */
    //@{

    //! Parallelization type
    enum Control {
      parallel,		/*!< root controls all chain updates. */
      serial		/*!< chains are run at each node. */
    };

    //! Symmetric distribution
    enum Distrib {
      uniform,		/*!< uniform distribution centered at zero. */
      normal,		/*!< Normal distribution with given sigma. */
      cauchy,		/*!< Cauchy distribution with given scale. */
    };
      

    //! Current control
    static Control @autopersist(cntrl);

    //! Minimum number of chains
    static int @autopersist(minmc);

    //! Number of iterations allowed for finding a good initial state
    static int @autopersist(state_iter);
    
    //! Small pos double "machine" constant (default: double=1.0e-12)
    static double @autopersist(tiny);

    //! Frequency in steps for setting \f$gamma=1\f$ (default: 10)
    static unsigned @autopersist(jfreq);

    //! Use linear or logit mapping for variables (default: false)
    static bool @autopersist(linear);

    //! Number of states for acceptance rate queue
    static unsigned @autopersist(nfifo);

    //@}

    //+ CLICONSTR StateInfo* int string BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm*

    //+ CLICONSTR StateInfo* int string BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*

    //+ CLICONSTR StateInfo* int clivectordist* BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm*

    //+ CLICONSTR StateInfo* int clivectordist* BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*

    //! Constructors for data streams
    //@{
    DifferentialEvolution(BIE::StateInfo* si, int number, string deinit,
			  BIE::BaseDataTree* d,
			  BIE::Model* m,
			  BIE::Integration* i,
			  BIE::Converge* c,
			  BIE::Prior* mp,
			  BIE::LikelihoodComputation* l,
			  BIE::MCAlgorithm* mca,
			  BIE::Simulation *last=NULL) : 
      Simulation(si, d, m, i, c, mp, l, mca, last) 
      { 
	initialize(number); 
	initializeSampler(deinit);
      }



#ifndef SWIG
    DifferentialEvolution(StateInfo* si, int number, clivectordist* v,
			  BaseDataTree* d, Model* m, Integration* i,
			  Converge* c, Prior* mp, LikelihoodComputation* l,
			  MCAlgorithm* mca, Simulation *last=NULL) : 
      Simulation(si, d, m, i, c, mp, l, mca, last) 
      { 
	initialize(number);
	initializeSampler((*v)());
      }
#endif

    DifferentialEvolution(BIE::StateInfo* si, int number,
			  std::vector<Distribution*> v,
			  BIE::BaseDataTree* d,
			  BIE::Model* m,
			  BIE::Integration* i,
			  BIE::Converge* c,
			  BIE::Prior* mp,
			  BIE::LikelihoodComputation* l,
			  BIE::MCAlgorithm* mca,
			  BIE::Simulation *last=NULL) : 
      Simulation(si, d, m, i, c, mp, l, mca, last) 
      { 
	initialize(number);
	initializeSampler(v);
      }

    //@}

    //+ CLICONSTR StateInfo* int string Converge* Prior* LikelihoodComputation* MCAlgorithm*

    //+ CLICONSTR StateInfo*  int string Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*

    //+ CLICONSTR StateInfo* int clivectordist* Converge* Prior* LikelihoodComputation* MCAlgorithm*

    //+ CLICONSTR StateInfo* int clivectordist* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*

    //@{
    //! Constructor for user-defined likelihood
    DifferentialEvolution(BIE::StateInfo* si, int number, string deinit,
			  BIE::Converge* c,
			  BIE::Prior* mp, BIE::LikelihoodComputation* l,
			  BIE::MCAlgorithm* mca, BIE::Simulation *last=NULL) :
      Simulation(si, c, mp, l, mca, last) 
      { 
	initialize(number);
	initializeSampler(deinit);
      }

#ifndef SWIG
    DifferentialEvolution(StateInfo* si, int number, clivectordist* v,
			  Converge* c, Prior* mp, LikelihoodComputation* l,
			  MCAlgorithm* mca, Simulation *last=NULL) :
      Simulation(si, c, mp, l, mca, last) 
      { 
	initialize(number);
	initializeSampler((*v)());
      }
#endif

    DifferentialEvolution(BIE::StateInfo* si, int number,
			  std::vector<BIE::Distribution*> v,
			  BIE::Converge* c, BIE::Prior* mp,
			  BIE::LikelihoodComputation* l,
			  BIE::MCAlgorithm* mca, BIE::Simulation *last=NULL) :
      Simulation(si, c, mp, l, mca, last) 
      { 
	initialize(number);
	initializeSampler(v);
      }

    //! Null constructor
    DifferentialEvolution() { unit = 0;}

    //@}
      
    //! Destructor
    virtual ~DifferentialEvolution();

    //! Initialize new state (override from Simulation)
    //@{
    //! Compute the state from the cardinality, weights and parameters
    void NewState(int Mcur, std::vector<double>& w,
		  std::vector< std::vector<double> >& p);
    //! Compute the state from the Chain value
    void NewState(BIE::Chain& ch);
    //! Compute the state from the State value
    void NewState(BIE::State& s);
    //@}

    //! Initialize all the chains
    virtual void Initialize();

    //! Reinitialize with new class instances
    virtual void Reinitialize(BIE::MHWidth* width, BIE::Prior *mp);

    //! Set global parameters
    //@{
    //+ CLIMETHOD void SetControl int
    /**
       Choose parallelization control
       (parallel=0, serial=1)
    */
    void SetControl(int);
    //@}

    /**@name Members which report and diagnose current state */
    //@{

    //! Return current posterior probability value (override)
    double GetValue();
    
    //! Return current prior value (override)
    double GetPrior();
    
    //! Return current likelihood probability value
    double GetLikelihood();

    //! Return current state (override)
    BIE::State GetState();
    
    //! Print passed state in ascii with labels (override)
    virtual void ReportState();

    //! Add state to log file
    //@{
    virtual void LogState(std::string& logfile);
    virtual void LogState(int level, int iterno, std::string& outfile);
    //@}


    //! Print current state (override)
    void PrintState() { chains[0].PrintState(); }


    //! override of Simulation method to provide diagnostics specific
    //! to DifferentialEvolution
    virtual void PrintStepDiagnostic();

    //! override of Simulation method to provide state specific diagnostics.
    //! Here, the method provides mixing statistics.
    virtual void PrintStateDiagnostic();

    //! Return vector with fraction of acceptances for each chain
    vector<double> GetStat(void);

    //+ CLIMETHOD void NewNumber int
    //! Specify the number of desired chains
    void NewNumber(int);

    //+ CLIMETHOD void NewGamma double
    //! Change the shift length from the default
    void NewGamma(double p) { gamma0 = p; }

    //+ CLIMETHOD void EnableLogging
    //! Enable caching per chain
    void EnableLogging() { caching = true; CreateChains(); }

    //+ CLIMETHOD void SetLinearMapping bool
    //! Change mapping from logit to linear (or vice versa)
    void SetLinearMapping(bool b) { linear = b; }

    //+ CLIMETHOD void SetJumpFreq int
    //! Change jump frequency for multimodel mixing (default: 10)
    void SetJumpFreq(int n) { jfreq = n; }

    //! Print out simulation specific diagnostics
    void AdditionalInfo();

    //@}

  protected:
    
    //! Constructor utility
    void initialize(int number);

    //! The Markov Chain implementation
    virtual void MCMethod();

    /** The default (not mode-swapping) value for the proposal
	direction magntiude */
    double @autopersist(gamma0);

    //! The current value of gamma
    double @autopersist(gamma);

    //! Internal counter for mode swapping
    unsigned @autopersist(ncount);

    //! Used to flag logging of each chain state
    bool @autopersist(caching);

    //! Random distribution for parameters when generating trial states
    vector<Distribution*> @autopersist(distrib);

    //! Flag the epsilon distribution list as locally instantiated
    bool @autopersist(local_dist);

    //! State variables
    //@{
    //! The index of the current fiducial chain
    unsigned int @autopersist(R0);

    //! Does the chain need to be updated?
    vector<unsigned char> @autopersist(update_flag);

    //! For MPI, chains that need to be updated on each node
    vector<unsigned char> @autopersist(update_flag1);

    //! Tally of proposals for each chain 
    vector<unsigned long> @autopersist(nstat0);

    //! Tally of accepted proposals for each chain 
    vector<unsigned long> @autopersist(nstat1);

    //! Used to compute chain acceptence rates
    deque< vector<unsigned char> > @autopersist(arate);
    //@}

    //! Create the chain structures
    void CreateChains();

    //! Attempt to find a good state from the prior distribution for chain k
    void SampleNewState(int k);

    //! Use the parallel version (called by SampleNewState())
    void SampleNewStateParallel(int k);

    //! Serial sampling init
    void NewStateSerialInit();

    //! Status vector for sampling new states
    vector<unsigned short> fail;

    //! Use the serial version (called by SampleNewState())
    void SampleNewStateSerial(int k);

    //! Check the status of the initialization
    void NewStateSerialStatus();
    
    //! Trial Chain instance
    Chain chainT;

    //! Compute the trial state from the selected particles
    void TrialState(int zero, int one, int two);

    /** Initialize the random part of the sampler from the input file
	@param epsfile */
    void initializeSampler(string epsfile);

    /** Initialize the random part of the sampler a distribution vector
	@param epsdist */
    void initializeSampler(vector<Distribution*>& epsdist);

    //!	Convergence check
    //@{
    int @autopersist(totalnum);
    int @autopersist(totaltry);
    //@}

    //! Unit interval variates
    Uniform * @autopersist(unit);

    //! For gathering diagnostic data
    //@{
    void ResetChainStats();
    //@}

    //! True if state values have been computed for at least the first time
    bool @autopersist(chains_initialized);

    //! Logit distribution
    double Logit(double x) {
      x = max<double>(tiny, min<double>(1.0-tiny, x));
      return log(x/(1.0 - x)); 
    }

    //! The inverse Logit distribution
    double InverseLogit(double q) { return 1.0/(1.0+exp(-q)); }

    //! Map from finite to infinite interval
    double Map(double x, double a, double b) {
      x = (max<double>(a, min<double>(x, b)) - a)/(b - a);
      if (linear)
	return x;
      else
	return Logit(x);
    }

    //! Map from infinite back to the finite interval
    double InverseMap(double q, double a, double b) {
      if (linear)
	return a  + (b - a)*q;
      else
	return a  + (b - a)*InverseLogit(q);
    }

    //! Lower limit on values imposed by prior
    vector<double> @autopersist(_lower);

    //! Upper limit on values imposed by prior
    vector<double> @autopersist(_upper);

private:
    // Save the RNG
    //
    template<class Archive>
    void post_save(Archive &ar, const unsigned int file_version) const {
      vector<Serializable*> s;
      s.push_back(BIEgen);
      ar << BOOST_SERIALIZATION_NVP(s);
    }
    // Reassign pointer to fiducial chain and restore the RNG
    //
    template<class Archive>
    void post_load(Archive &ar, const unsigned int file_version) {
      vector<Serializable *>s;
      ar >> BOOST_SERIALIZATION_NVP(s);
      BIEgen = dynamic_cast<BIEACG*>(s[0]);
      chfid = &chains[R0];
    }

    @persistent_end_split

  };

  //! @}

}  
#endif
