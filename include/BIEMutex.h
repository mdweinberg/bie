#ifndef BIEMUTEX
#define BIEMUTEX 1

#include <cc++/thread.h>

using namespace ost;

// define a mutex to get MPI to work with threads 
extern Mutex MPIMutex;

// Semphores for Likelhood control
extern Semaphore parentSem;
extern Semaphore childSem;

#ifdef DEBUG
extern int MPIMutexEnter, MPIMutexLeave;
#endif

// Short-hand operators

inline void operator ++(Mutex &m)
{
  m.enterMutex();
#ifdef DEBUG
  MPIMutexEnter++;
#endif 
};
                
inline void operator --(Mutex &m)
{
  m.leaveMutex();
#ifdef DEBUG
  MPIMutexLeave++;
#endif 
};


#endif
