// This is really -*- C++ -*-


#ifndef BIEconfig_h
#define BIEconfig_h

#include <string>
using namespace std;

//! Classes specific to the Bayesian Inference Engine
namespace BIE {

  //! Describe the types native distributions
  enum DistributionTypes {
    uniform, 			// 0
    normal, 			// 1
    normal_limits, 		// 2
    exponential, 		// 3
    exponential_limits,		// 4
    weibull, 			// 5
    weibull_limits,		// 6
    inverse_gamma, 		// 7
    inverse_gamma_limits,	// 8
    cauchy,	 		// 9
    cauchy_limits		// 10
  };

  static string DistributionName[] = {
    "Uniform", 			// 0
    "Gaussian",			// 1
    "Gaussian(limits)",		// 2
    "Exponential", 		// 3
    "Exponential(limits)", 	// 4
    "Weibull", 			// 5
    "Weibull(limits)"		// 6
    "InverseGamma", 		// 7
    "InverseGamma(limits)"	// 8
    "Caucny",	 		// 9
    "Cauchy(limits)"		// 10
  };

  /// Define the basic data type for a "real"
  typedef float real;
}

#include "State.h"

#endif



