// This is really -*- C++ -*-

#ifndef AsciiLexer_h
#define AsciiLexer_h

#include <fstream>
#include <string>
using namespace std;

//aah why oh why did I have to do this?
//I'm trying to rename the lexer so that it doesn't conflict
//with the other lexer in the CLI

// Extreme hackery

#undef yyFlexLexer
#define yyFlexLexer AscFlexLexer

// Use the private include guard in FlexLexer.h to
// prevent us from including the file a second time
// (some of it is purposely left unguarded, but
// it has a broken implementation)

#ifndef __FLEX_LEXER_H
 #include <FlexLexer.h>
#endif

// aah added for debugging experiment
//#undef yyFlexLexer

namespace BIE {
  class AsciiLexer;
}

/**
* A simple ascii lexer used by the RecordInputStream_Ascii class.
* The bulk of the class is implemented by code automatically produced
* by FLEX.  An AsciiLexer object can be created by passing the name
* of a file, or an open stream.  To advance to the next token in the 
* stream, use the nextToken() method -- this returns a code describing the
* type of token discover.  To obtain the text of the token, 
* call the tokenText() method.  The text of the token is NULL (0)
* until nextToken is called.
*
* The flexer's tokens are as follows:  newline, string literals (including
* the escaped characters \\n, \\t, \\\\, and \", in their usual interpretion)
* integer literals, floating point literals (including INF, NaN), 
* keywords INT, BOOL, REAL, STRING (all case insensitive),
* and several single character symbols.  See the symbol enumeration
* for a full list.
*
* This class inherits from two different classes - BaseObject (persistence)
* and yyFlexLexer (Flex).  I didn't see any simple way around this with the tools
* we currently have.
*
* \see RecordInputStream_Ascii
*/
class BIE::AsciiLexer : public yyFlexLexer{

public:
  /// Creates a new lexer object by opening the specified file.
  /// FileException is thrown if the file does not exist.
  AsciiLexer(string filename);

  /// Creates a new lexer object which reads from the specified
  /// input stream.
  AsciiLexer(istream * stream);
  
  /// destructor - closes the input file if it was opened by this class.
  ~AsciiLexer();
  
  /// Gets the nextToken from the input file.  The code returned by this routine
  /// is one of the codes in the enumeration defined below.
  inline int nextToken() { return yylex(); }
  
  /// Returns the text of the token.  The memory belongs to FLEX, and
  /// should not be interfered with!
  inline const char * tokenText() { return al_stripped; }

  /// Codes returned by the lexer method nextToken().
  enum {AL_NEWLINE_, AL_INT_KEYWORD_, AL_REAL_KEYWORD_, AL_BOOL_KEYWORD_,
        AL_STRING_KEYWORD_, AL_STRING_VALUE_, AL_INT_VALUE_, AL_REAL_VALUE_,
        AL_COMMA_, AL_EQUALS_, AL_LEFT_CURLY_, AL_RIGHT_CURLY_, 
	AL_LEFT_SQUAREBRACKET_, AL_ATRATE_, 
        AL_EOF_, AL_ERROR_};

private:
  /// The main lexer method created by flex.
  int yylex();
  
  /// The processed token, with quotes stripped out and escape sequences 
  /// replaced.  Memory is managed by flex.
  char     * al_stripped;

  /// Flag which is true if we opened the stream (meaning we should close it too).
  bool       al_streamismine;
  
  /// Name of the file we are reading, if known

  /// Input file.
  istream * al_inputstream; 
};

#endif
