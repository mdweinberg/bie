// -*- C++ -*-

#ifndef TSLog_h
#define TSLog_h

#include <BIEmpi.h>
#include <stdio.h>
#include <iostream>
using namespace std;

namespace BIE {

  /// Wall clock timing for use in LikelihoodComputationMPITP
  class TSLog 
  {
  public:

    //! Use a ring buffer for quick and dirty timestamp logging
    //@{
    /// Number of entries in the ring buffer
    static const int circBufNumEntries=30;
    /// The ring buffer
    char circBuf[circBufNumEntries][100];
    /// The current position in the ring buffer
    volatile int circBufPos;
    /// The time zoer point
    double startTime;
    /// Set to true to disable timing
    bool disabled;

    //! Create a time logger (disabled to start)
    TSLog() 
      {
	circBufPos = 0;
	startTime = MPI_Wtime();
	disabled = false;
      }

    //! Create a time logger (enabled or disabled baed on OnorOff value)
    TSLog(bool OnorOff)
      {
	circBufPos = 0;
	startTime = MPI_Wtime();
	disabled = !OnorOff;
      }


    //! Enter a string into the log with the tagged by the current offset
    void LogIt(const char* buf)
    {
      if (disabled) return;

      double now = MPI_Wtime();
      double elapsedTime = now-startTime;

      char timeString[100];
      sprintf(timeString, "Time:(%f)%f - ", now, elapsedTime);
      strcpy(circBuf[circBufPos], timeString);
      strcat(circBuf[circBufPos],buf);

      circBufPos++;
      
      if (circBufPos >= circBufNumEntries) circBufPos = 0;
    }
    
    /** Enter a string and integer into the log with the tagged by the
	current offset */
    void LogInt(const char* buf, int i)
    {
      char intString[100];
      sprintf(intString, "%s%d", buf, i);
      LogIt(intString);
    }

    /** Enter a string and double into the log with the tagged by the
	current offset */
    void LogDouble(const char* buf, double d)
    {
      char doubleString[100];
      sprintf(doubleString, "%s%f", buf, d);
      LogIt(doubleString);
    }
    
    //! Print the log to stdout
    void PrintIt()
    {
      if (disabled) return;
      
      cout << "Log " << this << "\n";
      
      for (int i=0; i<circBufPos; i++){
	cout << "  [" << i << "] " << circBuf[i] << "\n";
      }
      cout <<  "\n";
    }
  };  //end class

} // end namespace


#endif //TSLog_h
