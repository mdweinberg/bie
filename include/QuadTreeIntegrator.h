// This is really -*- C++ -*-

#ifndef QuadTreeIntegrator_h
#define QuadTreeIntegrator_h

#include <math.h>

#include <vector>

namespace BIE {

  /**
     Cubature algorithm 

     based on:

     Adaptive Quadrature for Multiple Integrals over N-Dimensional
     Rectangular Regions

     Author(s): A.C. Genz, A.A. Malik

     Adapted by: M. Weinberg
  */
  class QuadTreeIntegrator {
    
  public:
    
    /// Function prototype
    typedef vector<double>(*Integrand)(double, double);
    
    /// Value instance keys for recursion
    typedef pair<double, QuadTreeIntegrator*> FrontierNode;
    
				// Global variables

#ifndef SWIG
    //! Root node for quadtree integrator
    struct RootVars {
      /// Rote node
      QuadTreeIntegrator *root;
      /// Frontier list
      vector<FrontierNode> frontier;
      /// Current value
      vector<double> value;
      /// Current error
      vector<double> error;
    };
#endif
    
    /**
       Constructor for root node

	a0, a1   -- endpoints of x-interval
	b0, b1   -- endpoints of y-interval
	dim      -- dimension of vector returned by func
	func     -- intgrand function
	eps      -- desired relative error
	minlevel -- maximum number of branches in quad tree
	maxlevel -- maximum number of branches in quad tree
    */
    QuadTreeIntegrator(double a0, double a1, double b0, double b1,
		       int dim, Integrand func, double eps, 
		       int minlevel, int maxlevel);
    /// Destructor -- recursively delete tree
    ~QuadTreeIntegrator();

    /// Return value of intgral
    vector<double> Integral();

    /// Absolute error estimate
    vector<double> AbsError() { return root->error; }

    /// Maximum error estimate
    double MaxAbsError() { return max_error(root->error); }

    /// Maximum relative error estimate
    double MaxRelError() { return max_rel_error(); }
    
    /// Relative error estimate
    vector<double> RelError() { 
      vector<double> ret = root->error;
      for (unsigned i=0; i<ret.size(); i++)
	ret[i] /= fabs(root->value[i]) + 1.0e-18;
      return ret; }

    /// Number of function evaluations
    int NumEvals() { return numevals; }

    /// Return highest level of tree used
    int MaxLevel() { return maxcurlevel; }

    /// Print tile corners in frontier after call Intgral() (for debugging)
    void PrintFrontier(ostream& out);
    
  private:
    double a[2];		// Min interval
    double b[2];		// Max interval
    double eps;			// tolerance
    
    QuadTreeIntegrator* parent;
    QuadTreeIntegrator** child;
    
    
    int dim;			// Dimension of integrand
    Integrand eval;		// Integrand function
    static int numevals;  // Number of evals (for diagnostic purposes)
    
    int minlevel;		// Minimum number of decents
    int maxlevel;		// Maximum number of decents
    int level;			// Current level
    static int maxcurlevel;	// Maximum level currently in tree
    
    vector<double> value;
    vector<double> error;
    
    /** Constructor for children
	parent   -- pointer to parent node
	which    -- index from 1,2,3,4 describing the node counter clockwise
	            from lower right in a Eulidian metric
	root     -- pointer to master data
    */
    QuadTreeIntegrator(QuadTreeIntegrator* parent, int which, RootVars* root);


				// Compute value and error, update frontier
    void compute();
				// Update master values
    
				// Add current value and error estimate to
				// total
    void add_to_master(vector<double> val, vector<double> err);
    
				// Remove this node's value and error from
				// total
    void sub_from_master(vector<double> val, vector<double> err);
    
				// Maximum error at this node
    double max_error(vector<double> err);
    
				// Maximum relative error
    double max_rel_error(void);
    
    
				// Master data
    struct RootVars *root;
  };
  
  
}

#endif
