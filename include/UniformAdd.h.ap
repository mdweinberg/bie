// This is really -*- C++ -*-

#ifndef UniformAdd_h
#define UniformAdd_h

@include_persistence

namespace BIE {

  //+ CLICLASS UniformAdd SUPER TransitionProb
  /**
     Uniform disk of given width about fiducial point
  */
  class @persistent(UniformAdd) : public @super(TransitionProb)
  {
  protected:

  public:
    //@{
    /** 
	Constructor: dim is the dimension, the often-used dim=1 is
	treated as a special case
    */
    //+ CLICONSTR
    //! Null constructor
    UniformAdd() {}
    //+ CLICONSTR int
    //! Constructor with mixture number and component model dimension
    UniformAdd(int N) : TransitionProb(N) {}
    //@}
    
    //@{
    //! Sample the distribution

    //! The single dimension case
    double operator()(double x, double w) 
    {
      return x + w*(*unit)();
    }

    //! The arbitrary dimension case
    std::vector<double> operator()(std::vector<double>& x, std::vector<double>& w)
    {
      unsigned sz = x.size();
      vector<double> y(sz);
      for (unsigned n=0; n<sz; n++) y[n] = x[n] + w[n]*(*unit)();
      return y;
    }
    //@}

    //@{
    //! Return the transition probability
    double prob(double x, double y, double w) 
    {
      if (fabs( 0.5*(y - x)/w ) < 1.0) return 0.5/w;
      else return 0.0;
    }

    double prob(std::vector<double>& x, std::vector<double>&y, std::vector<double>& w)
    {
      double ret = 1.0;
      unsigned sz = x.size();
      for (unsigned n=0; n<sz; n++) {
	if (fabs( 0.5*(y[n] - x[n])/w[n] ) < 1.0) ret *= 0.5/fabs(w[n]);
	else ret *= 0.0;
      }
      return ret;
    }
    //@}

    @persistent_end
  };

} // namespace BIE

#endif
