// This is really -*- C++ -*-

#ifndef ADATA_H
#define ADATA_H

#include <iostream>
#include <cstring>

#include "Serializable.h"

#include "gfunction.h"
#include "Node.h"
#include "CliArgList.h"
#include "clivector.h"

class CliArgList;

extern char * stralloc(const char * str);

/**
 * This is a value container.  It is used to store variables and
 * method arguments in the CLI.  A built-in types (various sizes,
 * signed and unsigned) are supported, as are STL strings, char*, and
 * pointers to instances of the classes reflected using the stylized
 * comments and extracted by the perl script "galext"
 *
 * \see Symtab
 * \see SimpleExpr
 * \see Expr
 * \see CliArgList
 * \see GlobalTable
 */
class AData : public BIE::Serializable 
{
  
public:
  
  /// Constructor that specifies the type that will be held.
  AData(int32 typecode);
  
  /// Empty constructor - contents filled in later.
  AData();
  
  /// Constructor for an AData object holding a pointer to an object.
  AData(void * pointer, const char * typestring);
  
  /// Destructor - cleans up the type string and anything else we allocated.
  ~AData();
  
  /// These codes decribe the things that can possibily be stored in
  /// an AData object.  These values are used to set the type field.
  enum VALUE_TYPE { bool_TYPE = -100, 
		    uint32_TYPE,  int32_TYPE,
		    float_TYPE,   double_TYPE,
		    string_TYPE,  charstar_TYPE,
		    pointer_TYPE, variable_TYPE,
		    arrayelement_TYPE, arrayelementptr_TYPE,
		    variablearrayindex_TYPE, arrayindex_TYPE,
		    array_TYPE, unset_TYPE };
  
#define DECLARE_GET_SET_IS(typename,prefix)	\
  typename get_ ## prefix() const;		\
  void set_ ## prefix(typename arg);		\
  bool is_ ## prefix() const;
  
  DECLARE_GET_SET_IS(bool,bool)
  DECLARE_GET_SET_IS(uint32,uint32)
  DECLARE_GET_SET_IS(int32,int32)
  DECLARE_GET_SET_IS(float,float)
  DECLARE_GET_SET_IS(double,double)
  DECLARE_GET_SET_IS(const char *,charstar)
  DECLARE_GET_SET_IS(void *,pointer)
  DECLARE_GET_SET_IS(const char *,variable)
  DECLARE_GET_SET_IS(CliArgList *,arrayelement)
  DECLARE_GET_SET_IS(CliArgList *,arrayelementptr)
  DECLARE_GET_SET_IS(const char *,variablearrayindex)
  DECLARE_GET_SET_IS(uint32,arrayindex)
  DECLARE_GET_SET_IS(void*,array)
  
  //! Gets the string in this container
  const char * get_string() const;
  //! Sets the string in this container
  void set_string(string str);
  //! Does this contain a string?
  bool is_string() const;
  
#undef DECLARE_GET_SET_IS
  
  //! Sets the type of value stored in this container.
  void setType(int32 typecode) { this->type = typecode; }
  
  //! Gets the type of the value stored in this container.
  int32 getType() const { return this->type; }
  
  //! Sets the next AData object if this object is part of a list.
  void setNext(AData * next) { this->next = next; }
  
  //! Gets the next AData object if this object is part of a list.
  AData * getNext() const { return this->next; }
  
  //! Returns the type string.
  const char * getTypeString() const { return this->type_str; }
  
  //! Sets the type string.
  void setTypeString(const char * str)
  {
    if (this->type_str != 0) { delete [] this->type_str; }
    this->type_str  = new char [strlen(str) + 1];
    strcpy(this->type_str, str);
  }
  
  //! Return a string with the data type
  string get_type_name();
  
  //! Return the variable name
  const char * getForVariable() const { return this->for_variable; }

  //! Set the variable name
  void setForVariable(const char * str)
  { 
    if (this->for_variable) { delete [] this->for_variable; }
    this->for_variable = stralloc(str); 
  }
  
  //! Print the value of the datum according to its type
  void print_value(ostream & outputstream) const;
  
  //! Copies the value at the address given by casting to the current type
  void copy_memory_to_value(void * address);
  
  //! Writes the value to the address given by casting to the current type
  void copy_value_to_memory (void *address);
  
  //! Copies the contents of the object passed in.  
  void copy(const AData *data);
  
private:
  
  //! Union for holding data values.
  union {
    // Actual values a cli variable can have.
    bool   bool_value;
    uint32 uint32_value;
    int32  int32_value;
    float  float_value;
    double double_value;
    char  *string_value;
    char  *charstar_value;
    void  *pointer_value;
    void  *array_value;
    
    // Values used in expression evaluation.
    char  *variable_value;        // holds unresolved variable names.
    CliArgList  * arrayelement_value;   // holds two AData items 1) array name 2) index
    CliArgList  * arrayelementptr_value; // holds two AData items 1) array name 2) index
    char     * variablearrayindex_value;   // holds a variable array index.
    uint32     arrayindex_value;   // holds a literal array index
  } value;
  
  //! This field specifies the type of the data being stored in the union.
  int32 type;
  
  //! Describes the type of an object when the data being stored is a pointer.
  char *type_str;
  
  //! Used to create linked lists of AData objects.
  AData *next;
  
  //! Holds the length of the array when the item being stored is an array.
  uint32 arraylength;
  
  //! Name of the a "for variable", if this is being used to hold a "for variable".
  char * for_variable;
  
  // PERSISTENCE code
  // We definitely need a custom serlize() routine for this class..
private:
  friend class boost::serialization::access;
  
  // need to split save/load just for the pointer_TYPE
  template<class Archive>
  void save(Archive & ar, const unsigned int version) const {
    string type_str_s = (type_str==NULL)?"":type_str;
    string for_variable_s = (for_variable==NULL)?"":type_str;
    
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);
    
    try {
      ar << BOOST_SERIALIZATION_NVP(type);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    try {    
      ar << BOOST_SERIALIZATION_NVP(type_str_s);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    try {
      ar << BOOST_SERIALIZATION_NVP(next);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    try {
      ar << BOOST_SERIALIZATION_NVP(arraylength);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    try {
      ar << BOOST_SERIALIZATION_NVP(for_variable_s);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    
    string string_s;
    
    // for pointer_TYPE (why did Byn split this assignment?)
    Serializable* pointer_value_static = (Serializable*)value.pointer_value;
    Serializable* pointer_value_dynamic = 
      dynamic_cast<Serializable*>(pointer_value_static);
    
    switch(type) {
    case bool_TYPE:
      try {
        ar << BOOST_SERIALIZATION_NVP(value.bool_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case uint32_TYPE:
      try {
        ar << BOOST_SERIALIZATION_NVP(value.uint32_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case int32_TYPE:
      try {
        ar << BOOST_SERIALIZATION_NVP(value.int32_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case float_TYPE:
      try {
        ar << BOOST_SERIALIZATION_NVP(value.float_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case double_TYPE:
      try {
        ar << BOOST_SERIALIZATION_NVP(value.double_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case string_TYPE:
      string_s = value.string_value;
      try {
        ar << BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case charstar_TYPE:
      string_s = value.charstar_value;
      try {
        ar << BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case pointer_TYPE:
      if (value.pointer_value == NULL) {
        // just save it
        Serializable* tmp = NULL;
        try {
          ar << boost::serialization::make_nvp("pointer_value", tmp);
          BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
        }
      }
      else if (pointer_value_dynamic != NULL) {
        // value.pointer_value is a subclass of Serializable
        try {
          ar << boost::serialization::make_nvp("pointer_value",
                                               pointer_value_dynamic);
        } catch (::boost::archive::archive_exception & e) {
	  cout << "AData:" << __LINE__ << " pointer_value: " << type_str_s << endl;
          throw new BoostSerializationException(string(e.what()),
                                                "pointer_value: " + type_str_s,
                                                __FILE__,__LINE__);
        } catch (BoostSerializationException * e) {
	  cout << "AData:" << __LINE__ << " pointer_value: " << type_str_s << endl;
          throw new BoostSerializationException(e,"pointer_value: " + type_str_s,
                                                __FILE__,__LINE__); 
        }
      }
      else {
        // value.pointer_value isn't a subclass of Serializable. We'll
        // try to serialize and hope for the best this will only
        // succeed if non-intrusive version of serialize() function
        // for this class is defined somewhere
	/*         ar << boost::serialization::make_nvp("pointer_value", */
	/*                                             value.pointer_value); */
        // BLOW UP
        throw InternalError("Can't save this type.",__FILE__,__LINE__);
      }
      break;
    case variable_TYPE:
      string_s = value.variable_value;
      try {
        ar << BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case arrayelement_TYPE:
      try {
        ar << BOOST_SERIALIZATION_NVP(value.arrayelement_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case arrayelementptr_TYPE:
      try {
        ar << BOOST_SERIALIZATION_NVP(value.arrayelementptr_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case variablearrayindex_TYPE:
      string_s = value.variablearrayindex_value;
      try {
        ar << BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case arrayindex_TYPE:
      // uint32
      try {
        ar << BOOST_SERIALIZATION_NVP(value.arrayindex_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case array_TYPE:
      // I don't think this type is ever really used, at least within
      // the persistence context.. Alistair left similar comments in
      // src/AData.cc
      throw InternalError("array_TYPE not implemented.",__FILE__,__LINE__);
      break;
    case unset_TYPE:
      break;
    default:
      // BLOW UP
      throw InternalError("Unrecognized data stored in AData object.",__FILE__,__LINE__);
      break;
    }
  }
  
  template<class Archive>
  void load(Archive & ar, const unsigned int version) {
    string type_str_s, for_variable_s;
    
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Serializable);       
    
    try {
      ar >> BOOST_SERIALIZATION_NVP(type);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    try {
      ar >> BOOST_SERIALIZATION_NVP(type_str_s);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    setTypeString(type_str_s.c_str());
    try {
      ar >> BOOST_SERIALIZATION_NVP(next);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    try {
      ar >> BOOST_SERIALIZATION_NVP(arraylength);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    try {
      ar >> BOOST_SERIALIZATION_NVP(for_variable_s);
      BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
    }
    if (for_variable_s.size() > 0) {
      setForVariable(for_variable_s.c_str());
    }
    
    string string_s;
    
    switch(type) {
    case bool_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.bool_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case uint32_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.uint32_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case int32_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.int32_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case float_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.float_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case double_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.double_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case string_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      if (value.string_value != NULL) {
        delete [] value.string_value;
      }
      value.string_value = new char[strlen(string_s.c_str()) + 1];
      strcpy(value.string_value, string_s.c_str());
      break;
    case charstar_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      if (value.charstar_value != NULL) {
        delete [] value.charstar_value;
      }
      value.charstar_value  = new char[strlen(string_s.c_str()) + 1];
      strcpy(value.charstar_value, string_s.c_str());
      break;
    case pointer_TYPE:
      Serializable* p;
      try {
	ar >> boost::serialization::make_nvp("pointer_value", p);
	BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      value.pointer_value = p;
      break;
    case variable_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      if (value.variable_value != NULL) {
        delete [] value.variable_value;
      }
      value.variable_value = new char[strlen(string_s.c_str()) + 1];
      strcpy(value.variable_value, string_s.c_str());
      break;
    case arrayelement_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.arrayelement_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case arrayelementptr_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.arrayelementptr_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case variablearrayindex_TYPE:
      try {
        ar >> BOOST_SERIALIZATION_NVP(string_s);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      if (value.variablearrayindex_value != NULL) {
        delete [] value.variablearrayindex_value;
      }
      value.variablearrayindex_value =
        new char[strlen(string_s.c_str()) + 1];
      strcpy(value.variablearrayindex_value, string_s.c_str());
      break;
    case arrayindex_TYPE:
      // uint32
      try {
        ar >> BOOST_SERIALIZATION_NVP(value.arrayindex_value);
        BIE_CATCH_BOOST_SERIALIZATION_EXCEPTION;
      }
      break;
    case array_TYPE:
      // I don't think this type is ever really used, at least within
      // the persistence context.. Alistair left similar comments in
      // src/AData.cc
      throw InternalError("array_TYPE not implemented.",
			  __FILE__,__LINE__);
      break;
    case unset_TYPE:
      break;
    default:
      // BLOW UP
      throw InternalError("Unrecognized data stored in AData object.",
			  __FILE__,__LINE__);
      break;
    }
  }
  
  BOOST_SERIALIZATION_SPLIT_MEMBER();
};

BIE_CLASS_EXPORT_KEY(AData)

#endif
