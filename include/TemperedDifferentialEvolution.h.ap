// This is really -*- C++ -*-

#ifndef TemperedDifferentialEvolution_h
#define TemperedDifferentialEvolution_h

#include <LikelihoodComputation.h>
#include <MHWidth.h>
#include <MHWidthOne.h>
#include <Simulation.h>
#include <DifferentialEvolution.h>
#include <Chain.h>

@include_persistence

namespace BIE
{

  //! \addtogroup simulation
  //! @{

  //+ CLICLASS TemperedDifferentialEvolution SUPER DifferentialEvolution
  /**
     Implements the Differential Evolution (DE) algorithm 
     (see DifferentialEvolution) including Tempered Transistion
     steps every <code>tfreq</code> steps (see TemperedSimulation)

     \par Temperature levels
     The temperature levels \f$j=0,\ldots,N\f$ are defined by:
     \f[
     P_j \propto P_0^{j/N} = e^{\beta_j \log P_0}
     \f]
     or
     \f[
     P_j \propto e^{\beta_j \log P_0}
     \beta_j = exp(-\log(T_{max})*j/N)
     \f]
     and the Metropolis-Hastings width is multiplied by the factor
     \f[
     f_j = \beta_j^{-\tau}
     \f]
     where \f$\tau\f$ is the paramater <code>tpow</code>.

     \par Paremeters
     @param Ninter is the number of mixing steps between tempering steps 
     (default: 10)
     @param tfreq is the number steps between tempered transition states
     (default: 11)
     @param tpow is the scale factor $T^{tpow}$ for proposal function
     (default: 0.5, exact for a Gaussian)
     @param minmc is the minimum number of temperature levels.  If not
     overriddednm explicitly, the constructor will set number of
     levels so that there are the square root of the parameter rank
     per logarithmic interval in temperature. (default: 4)
  */
  class @persistent(TemperedDifferentialEvolution) : public @super(DifferentialEvolution)
  {
  private:
    int @ap(Nlevels), pk;
    unsigned @ap(tcount);
    unsigned @ap(jcount);

  public:

    /// Number of mixing steps between tempering (default 10)
    static int @autopersist(Ninter);

    /// Scale factor $T^{tpow}$ for proposal function
    static double @autopersist(tpow);

    /// Minimum number of temperature states
    static int @autopersist(minmc);

    /// Frequency in steps for a tempered step (default: 11)
    static unsigned @autopersist(tfreq);

    //+ CLICONSTR StateInfo* int string int double BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm*
    //+ CLICONSTR StateInfo* int string int double BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*
    //+ CLICONSTR StateInfo* int int double clivectordist* BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm*
    //+ CLICONSTR StateInfo* int int double clivectordist* BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*
    //! Constructor for models with data streams
    //@{
    TemperedDifferentialEvolution(BIE::StateInfo* si, int mchains, 
				  std::string deinit, int minmc_p, double maxT,
				  BIE::BaseDataTree* d, BIE::Model* m,
				  BIE::Integration* i, BIE::Converge* c,
				  BIE::Prior* mp, BIE::LikelihoodComputation* l,
				  BIE::MCAlgorithm* mca, BIE::Simulation* last=NULL) :  
      DifferentialEvolution(si, mchains, deinit, d, m, i, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }
    
#ifndef SWIG
    TemperedDifferentialEvolution(BIE::StateInfo* si, int mchains, 
				  int minmc_p, double maxT,
				  BIE::clivectordist* v, BIE::BaseDataTree* d,
				  BIE::Model* m, BIE::Integration* i, BIE::Converge* c,
				  BIE::Prior* mp, BIE::LikelihoodComputation* l,
				  BIE::MCAlgorithm* mca, BIE::Simulation* last=NULL) :  
      DifferentialEvolution(si, mchains, v, d, m, i, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }
#endif

    TemperedDifferentialEvolution(BIE::StateInfo* si, int mchains, 
				  int minmc_p, double maxT,
				  std::vector<BIE::Distribution*> v,
				  BIE::BaseDataTree* d,
				  BIE::Model* m, BIE::Integration* i,
				  BIE::Converge* c, BIE::Prior* mp,
				  BIE::LikelihoodComputation* l,
				  BIE::MCAlgorithm* mca,
				  BIE::Simulation* last=NULL) :  
      DifferentialEvolution(si, mchains, v, d, m, i, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }
    //@}

    //+ CLICONSTR StateInfo* int string int double Converge* Prior* LikelihoodComputation* MCAlgorithm*
    //+ CLICONSTR StateInfo* int string int double Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*
    //+ CLICONSTR StateInfo* int int double clivectordist* Converge* Prior* LikelihoodComputation* MCAlgorithm*
    //+ CLICONSTR StateInfo* int int double clivectordist* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*
    //! Constructor for models with user-defined likelihood functions
    //@{
    TemperedDifferentialEvolution(BIE::StateInfo* si, int mchains, 
				  std::string deinit, int minmc_p, double maxT,
				  BIE::Converge* c, BIE::Prior* mp,
				  BIE::LikelihoodComputation* l,
				  BIE::MCAlgorithm* mca,
				  BIE::Simulation* last=NULL) :
      DifferentialEvolution(si, mchains, deinit, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }

#ifndef SWIG
    TemperedDifferentialEvolution(BIE::StateInfo* si, int mchains, 
				  int minmc_p, double maxT,
				  clivectordist* v, BIE::Converge* c, BIE::Prior* mp,
				  BIE::LikelihoodComputation* l, BIE::MCAlgorithm* mca,
				  BIE::Simulation* last=NULL) :
      DifferentialEvolution(si, mchains, v, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }
#endif

    TemperedDifferentialEvolution(BIE::StateInfo* si, int mchains, 
				  int minmc_p, double maxT,
				  std::vector<BIE::Distribution*> v,
				  BIE::Converge* c, BIE::Prior* mp,
				  BIE::LikelihoodComputation* l,
				  BIE::MCAlgorithm* mca,
				  BIE::Simulation* last=NULL) :
      DifferentialEvolution(si, mchains, v, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }
    //@}

    //+ CLIMETHOD void SetTempFreq int
    //! Change tempering frequency for mixing (default: 11)
    void SetTempFreq(int n) { tfreq = n; }

    //+ CLIMETHOD void SetEquilSteps int
    //! Set number of DE steps at each temperature level (default: 10)
    void SetEquilSteps(int n) { Ninter = n; }

    //+ CLIMETHOD void SetTpow double
    //! Set the scale factor $T^{tpow}$ for proposal function
    void SetTpow(double x) { tpow = x; }

    //+ CLIMETHOD void SetMinMC int
    //! Set the minimum number of temperature states
    void SetMinMC(int n) { minmc = n; }

    /// override of Simulation method to provide diagnostics specific
    /// to TemperedDifferentialEvolution
    virtual void PrintStepDiagnostic();


  protected:

    //! Constructor utility
    void Initialize(int minmc_p, double maxT);


    //! The log prob of the state moving UP the temperature ladder
    vector<double> @autopersist(up);

    //! The log prob of the state moving DOWN the temperature ladder
    vector<double> @autopersist(down);

    //! The 1/KT values for the temperature ladder
    vector<double> @autopersist(beta);

    //! The multiplicative width factors for the ladder
    vector<double> @autopersist(factor);

    //! The number of swapped states on the current step
    int @autopersist(curswp);

    //! The number of swap attempts on the current step
    int @autopersist(curtry);

    //! The cumulative number of swapped states (since last query)
    int @autopersist(totswp);

    //! The cmulative number of swap attempts (since last query)
    int @autopersist(tottry);

    //! The maximum tempering temperature
    double @autopersist(MaxT);

    //! Tally of proposals for each temperature level
    //@{
    vector<unsigned long> @autopersist(mstat0), @autopersist(stat0T);
    //@}

    //! Tally of accepted proposals for each level
    //@{
    vector<unsigned long> @autopersist(mstat1), @autopersist(stat1T);
    //@}

    //! Tempered statistics counter active
    bool @autopersist(tstat);

    //! Run the entire Monte Carlo algorithm (including tempering)
    virtual void MCMethod();

    //! Take a single DE step only
    virtual void OneDifferentialEvolutionStep();

    //! Heat chains to temperature level k
    virtual void PowerUP(int);

    //! Cool chains to temperature level k
    virtual void PowerDOWN(int);

    @persistent_end
  };

  //! @}

}

#endif
