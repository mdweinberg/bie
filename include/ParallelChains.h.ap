// This is really -*- C++ -*-

#ifndef ParallelChains_h
#define ParallelChains_h

@include_persistence

#include <LikelihoodComputation.h>
#include <MHWidth.h>
#include <MHWidthOne.h>
#include <Simulation.h>
#include <Chain.h>

namespace BIE
{
  
  //! \addtogroup simulation
  //! @{

  class Prior;
  
  //+ CLICLASS ParallelChains SUPER Simulation
  //! Implements parallel temperering or the parallel hierarchical sampler
  //! \par
  //! Both algorithms use an ensemble of Markov chains, run in
  //! parallel, each at a successively higher temperature.  A
  //! temperature of \f$T=1\f$ is the initial cold chain.
  //! \par
  //! The total number of chains is given by the maximum temperature, \p MaxT,
  //! for a state vector of dimension Ntot:
  //! \f[
  //! Mchains = 1 + \log(MaxT)*\sqrt(Ntot)
  //! \f]
  //! 
  //! \par "Temperature spacing"
  //! 
  //! 
  //! Temperature for a chain is chosen as follows:
  //! \f[
  //! T_j = T_{max}^{(j/(Mchain-1))^a}, j\in[0,Mchain-1]
  //! \f]
  //! <ul>
  //! <li> \f$a=1.0\f$ is logarithmic (e.g pure geometric spacing). This is the default.
  //! <li> \f$a>1.0\f$ weighted towards values of \f$T\f$ smaller than geometric
  //! <li> \f$a<1.0\f$ weighted towards values of \f$T\f$ larger than geometric
  //! <li> \f$a=0.23\f$ gives values of \f$T\f$ approximately linear from 1 to \f$T_{max}\f$
  //! <li> \f$a<0.23\f$ gives values of \f$T\f$ strongly weighted toward \f$T_{max}\f$
  //! </ul>
  //! 
  //! The second exponent, \f$t\f$ is used to scale the proposal
  //! width by a power of the temperature.  Gaussian scaling, the
  //! default is \f$t=1/2\f$.
  //! 
  //! The member function #SetTempExp may be used to rest either or
  //! both of these parameters.
  //! 
  //! \par "Parallel tempering (standard)"
  //! 
  //! At each step, with probability \p swapprob, one proposes to swap the
  //! states of two adjecent temperature chains.  With probability 1 - \p swapprob,
  //! all chains are updated at their assigned tempratures.
  //! \par "Parallel hierarchical sampler"
  //! At each step, one chain is proposed to be swapped with the cold
  //! chain.  The remaining \p Mchain-2 schains are updated at their
  //! assigned tempratures.
  //! \note Chain initialization
  //! The fiducial, cold chain is initialized from the user-supplied 
  //! initial state file.  The initialization of the warm state can be
  //! done in two way depending on the static variable #initial.
  //! <ol>
  //! <li> If #initial = user_supplied, all chains are assigned the 
  //! user-supplied initial value.
  //! <li> If #initial = prior_sampled, all chains but the fiducial chain 
  //! have initial values sampled from the prior distribution.
  //! </ol>
  //! \note Control methods
  //! <ol>
  //! <li> The parallel likelihood classes should be used with 
  //! #parallel control only.  The parallel likelihood classes 
  //! LikelihoodComputationMPI and LikelihoodComputationMPITP assume 
  //! that each process will enter the likelihood computation code.
  //! <li> A serial likelihood class should be used with 
  //! #serial control only.  A serial likelihood computation is only 
  //! entered by the process which calls it.
  //! </ol>
  //! \note Diagnostic output
  //! A call to the #EnableLogging member function will cause the state
  //! of each individual chain to be logged to a separate file.  The
  //! chain files will have names of the form "nametag.chainlog.k"
  //! where nametag is the global nametag variable and k is the chain
  //! number.  The first five lines record the following values for
  //! each chain: the number of components in the mixture, rank of the
  //! parameter vector, the inverse temperature \f$\beta=1/T\f$, the
  //! Metropolis-Hastings width factor, and the weighting factor.
  //! 
  //! \par References
  //! \par
  //! Geyer, C. J. 1991, "Markov chain Monte Carlo maximum
  //! likelihood." In Keramidas, E. M., editor, Computing Science
  //! and Statistics, Proceedings of the 23rd Symposium on the
  //! Interface, pages 156-163, Seattle, WA. Interface Foundation of
  //! North America.
  //! \par
  //! Hukushima and Nemoto 1996, J. Phys. Soc. Japan, 65, 1604-1620.
  //! \par
  //! Rigat, F. 2006, "MCMC inference using parallel hierarchical 
  //! sampling," preprint.
  class @persistent(ParallelChains) : public @super(Simulation) 
  {
    
  public:

    /**@name Global variables */
    //@{

    //! Algorithm types
    enum Algorithm {
      standard,		/*!< standard parallel chains algorithm. */
      hierarchical	/*!< hierarchical parallel chains algorithm. */
    };

    //! Parallelization type
    enum Control {
      parallel,		/*!< root controls all chain updates. */
      serial		/*!< chains are run at each node. */
    };

    //! Initialization type
    enum Initial {
      user_supplied,	      /*!< initial state specified by user. */
      prior_sampled	    /*!< initial state selected from prior. */
    };

    //! Current algorithm
    static Algorithm @autopersist(algo);

    //! Current control
    static Control @autopersist(cntrl);

    //! Initialization method
    static Initial @autopersist(initial);

    //! Swap probability
    static double @autopersist(swapprob);

    //! Change temperature spacing (apow=1 is geometric spacing)
    static double @autopersist(apow);

    //! Scale factor $T^{tpow}$ for proposal function
    static double @autopersist(tpow);

    //! Minimum number of temperature states
    static int @autopersist(minmc);

    //! Number of iterations allowed for finding a good initial state
    static int @autopersist(state_iter);

    //@}

    //! Constructor
    //@{
    //! Null constructor that initializes the Uniform pointer
    ParallelChains() : unit(0) {}

    //+ CLICONSTR StateInfo* int double MHWidth* BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm*
    ParallelChains(BIE::StateInfo* si, int minmc_p, double maxT, 
		   BIE::MHWidth* width,
		   BIE::BaseDataTree* d,
		   BIE::Model* m,
		   BIE::Integration* i,
		   BIE::Converge* c,
		   BIE::Prior* mp,
		   BIE::LikelihoodComputation* l,
		   BIE::MCAlgorithm* mca) : 
      Simulation(si, width, d, m, i, c, mp, l, mca, 0) 
      { Initialize(minmc_p, maxT); }


    //+ CLICONSTR StateInfo* int double MHWidth* BaseDataTree* Model* Integration* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*
    ParallelChains(BIE::StateInfo* si, int minmc_p, double maxT, 
		   BIE::MHWidth* width,
		   BIE::BaseDataTree* d,
		   BIE::Model* m,
		   BIE::Integration* i,
		   BIE::Converge* c,
		   BIE::Prior* mp,
		   BIE::LikelihoodComputation* l,
		   BIE::MCAlgorithm* mca,
		   BIE::Simulation *last) : 
      Simulation(si, width, d, m, i, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }

    //+ CLICONSTR StateInfo* int double MHWidth* Converge* Prior* LikelihoodComputation* MCAlgorithm* Simulation*
    ParallelChains(BIE::StateInfo* si, int minmc_p, double maxT, 
		   BIE::MHWidth* width,
		   BIE::Converge* c,
		   BIE::Prior* mp,
		   BIE::LikelihoodComputation* l,
		   BIE::MCAlgorithm* mca,
		   BIE::Simulation *last) : 
      Simulation(si, width, c, mp, l, mca, last) 
      { Initialize(minmc_p, maxT); }

    //+ CLICONSTR StateInfo* int double MHWidth* Converge* Prior* LikelihoodComputation* MCAlgorithm*
    ParallelChains(BIE::StateInfo* si, int minmc_p, double maxT, 
		   BIE::MHWidth* width,
		   BIE::Converge* c,
		   BIE::Prior* mp,
		   BIE::LikelihoodComputation* l,
		   BIE::MCAlgorithm* mca) : 
      Simulation(si, width, c, mp, l, mca, 0) { Initialize(minmc_p, maxT); }
    //@}
    
    //! Destructor
    virtual ~ParallelChains();

    //! Initialize new state (override from Simulation)
    //@{
    void NewState(int Mcur, std::vector<double>& w,
		  std::vector< std::vector<double> >& p);
    void NewState(BIE::Chain& ch);
    void NewState(BIE::State& s);
    //@}

    //! Initialize all the chains
    virtual void Initialize();

    //! Reinitialize with new class instances
    virtual void Reinitialize(BIE::MHWidth* width, BIE::Prior *mp);

    //! Set global parameters
    //@{
    //+ CLIMETHOD void SetAlgorithm int
    /**
       Choose between parallel tempering and hierachical tempering
       (standard=0, hierarchical=1)
    */
    void SetAlgorithm(int);

    //+ CLIMETHOD void SetControl int
    /**
       Choose parallelization control
       (parallel=0, serial=1)
    */
    void SetControl(int);

    //+ CLIMETHOD void SetInitial int
    /**
       Choose initialization type
       (user_supplied=0, prior_sampled=1)
    */
    void SetInitial(int);

    //+ CLIMETHOD void SetTempExp double double
    /**
       Choose temperature scaling parameters
    */
    void SetTempExp(double a, double t) {
      apow = a;
      tpow = t;
    }

    //@}

    //! Set chains widths to new values
    //@{
    //+ CLIMETHOD void SetNewWidths clivectord*
    //@{
    //! Set chains widths to new values (vector input)
    void SetNewWidths(std::vector<double> *wid);
#ifndef SWIG
    void SetNewWidths(clivectord *wid) { SetNewWidths(&(*wid)()); }
#endif
    //@}
    //+ CLIMETHOD void SetNewWidths string
    //! Set chains widths to new values (file name input)
    void SetNewWidths(string wid);
    //@}

    /**@name Members which report and diagnose current state */
    //@{

    //! Return current posterior probability value (override)
    double GetValue();
    
    //! Return current prior value (override)
    double GetPrior();
    
    //! Return current likelihood probability value
    double GetLikelihood();

    //! Return current state (override)
    BIE::State GetState();
    
    //! Print passed state in ascii with labels (override)
    virtual void ReportState();

    //! Print current state (override)
    void PrintState() { chains[0].PrintState(); }


    //! override of Simulation method to provide diagnostics specific
    //! to temperedsimulation
    virtual void PrintStepDiagnostic();

    //! override of Simulation method to provide state specific diagnostics.
    //! Here, the method provides mixing statistics.
    virtual void PrintStateDiagnostic();

    //! Return vector with fraction of swaps for each chain
    std::vector<double> GetMixstat(void);

    //! Return vector with fraction of acceptences for each chain
    std::vector<double> GetSweepstat(void);

    //+ CLIMETHOD void NewNumber int
    //! Specify the number of desired chains
    void NewNumber(int);

    //+ CLIMETHOD void NewSwapProb double
    //! Change the swap probabability
    void NewSwapProb(double p) { swapprob = p; }

    //+ CLIMETHOD void EnableLogging
    //! Enable caching per chain
    void EnableLogging() { caching = true; CreateChains(); }

    //! Print out simulation specific diagnostics
    void AdditionalInfo();

    //@}

  protected:
    
    //! Constructor utility
    void Initialize(int minmc_p, double maxT);

    //! Execute a Markov Chain step
    virtual void MCMethod();

    //! True if chains states are logged to files
    bool @autopersist(caching);

    //! Number of attempts so far
    unsigned @autopersist(ncount);

    //! Config parameters
    //@{
    //! Temperature for tempering ladder
    double @autopersist(MaxT);
    //@}

    //! State variables
    //@{
    //! List of chains that must be globally sychronized
    vector<unsigned char> @autopersist(update_flag);

    //! List of chains on an individual node that must sychronized
    vector<unsigned char> @autopersist(update_flag1);

    //! Number of in-chain update attempts
    vector<unsigned long> @autopersist(stat0);

    //! Number of in-chain acceptances per chain
    vector<unsigned long> @autopersist(stat1);

    //! Number of swap attempts per chain
    vector<unsigned long> @autopersist(nstat0);

    //! Number of swap acceptances per chain
    vector<unsigned long> @autopersist(nstat1);

    //! Number of standard MCMC updates attempted per chain
    vector<unsigned long> @autopersist(mstat0);

    //! Number of standard  MCMC updates accepted per chain
    vector<unsigned long> @autopersist(mstat1);

    //! Maximum dimension of state vector
    int  @autopersist(Ntot);
    //@}

    //! Create the chain structures
    void CreateChains();

    //! Attempt to find a good state from the prior distribution for chain k
    //@{

    //! Sample a new state for the kth chain (will select one of the two below)
    void SampleNewState(int k);

    //! Sample a new state for the kth chain (parallel version)
    void SampleNewStateParallel(int k);

    //! Sample a new state for the kth chain (serial version)
    void SampleNewStateSerial(int k);

    //! Status vector for sampling new states
    vector<unsigned short> fail;

    //! Prepare for chain initialization
    void NewStateSerialInit();

    //! Check the status of the initialization
    void NewStateSerialStatus();

    //@}

    //!	Convergence check
    //@{
    //! The number swaps
    int @autopersist(swapnum);

    //! The number of attempted swaps
    int @autopersist(swaptry);
    //@}

    //! Unit interval variates
    Uniform * @autopersist(unit);

    //! For gathering diagnostic data
    //@{
    //! Resets the counters
    void ResetChainStats();
    //! Gathers in-chain rates across all processes
    void getChainCounts();
    //@}

    //! True if state values have been computed for at least the first time
    bool @autopersist(chains_initialized);

    //! User-defined widths for MH sampling
    vector<double> @autopersist(user_widths);

  private:
    //
    // Save the RNG
    //
    template<class Archive>
    void post_save(Archive &ar, const unsigned int file_version) const {
      vector<Serializable*> s;
      s.push_back(BIEgen);
      ar << BOOST_SERIALIZATION_NVP(s);
    }
    //
    // Reassign pointer to fiducial chain and restore the RNG
    //
    template<class Archive>
    void post_load(Archive &ar, const unsigned int file_version) {
      vector<Serializable *>s;
      ar >> BOOST_SERIALIZATION_NVP(s);
      BIEgen = dynamic_cast<BIEACG*>(s[0]);
      chfid = &chains[0];
    }

    @persistent_end_split
      
  };
  
  //! @}

}  
#endif
