#ifndef GFUNCTION_H
#define GFUNCTION_H

#include <unistd.h>
//#include <stdiostream.h> aah is this used???

#include <iostream>
using namespace std;

#include <string>
#include "gvariable.h"
#undef PACKAGE
#include <config.h>

#ifdef HAVE_IEEEFP_H
  /** Define isinf using the fpclass function.  If we don't have
      <ieeefp.h> then just assume we have isinf - the compiler will
      complain if we don't
  */  
  #include <ieeefp.h>
  
  extern int isinf(double x);
#endif

extern char * stralloc(const char * str);

extern long get_cur_vm(pid_t);
extern long get_max_vm();

namespace BIE {

  //@{
  //! Check the configuration table for a variable name
  extern streambuf * checkTable(const char *function_name);
  extern streambuf * checkTable(const char *function_name, 
				const char *print_value);
  //@}
  //! For renaming std out
  extern ofstream out;

}

#endif

