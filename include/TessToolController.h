// This is really -*- C++ -*-

#ifndef TessToolController_h
#define TessToolController_h

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "TessToolVTK.h"
#include "TessToolReader.h"
#include "TessToolReceiver.h"
#include "CliCommandThread.h"
#include "CliOutputReceiveThread.h"
#include "TessToolConsole.h"

#include <cc++/thread.h>

using namespace std;
using namespace ost;

namespace BIE { 
  
  class TessToolGUI;
  class TessToolController;
  
  
  //! Main thread for TessTool GUI and visualizer
  class TessToolController: public Thread 
  {
    
    
  private:
    int _argc;
    char **_argv;
    bool _stop;
    
    Semaphore *_controllerVTK;
    Semaphore *_controllerCLI;
    Semaphore *_controllerRX;
    
    Semaphore *_rcvrThreadSem;
    Semaphore *_clisem;
    Semaphore *_working;
    Semaphore *_mainsem;
    
    TessToolDataStream *_rcvrthread;
    CliCommandThread *_clithread;
    CliOutputReceiveThread *_clilogger;
    ConsoleLogThread *_consoleLog;
    TessToolVTK *_vtkThread;
    
    TessToolGUI *_gui;
    
    bool _sentInitialScript;
    bool _startedCLICommandLoop;
    bool _startedVTK;
    int _xwinWidth, _xwinHeight;
    
    char *_scriptFileName;
    char *_workingDirName;
    char *_dataFileName;
    
				// Sample interval for pooling loops
    staticexpr const int sample1 = 2;
    staticexpr const int sample2 = 3;

  public:
    //! The constructor
    TessToolController(int argc, char *argv[], Semaphore *mainSem);
    //! The destructor
    ~TessToolController();
    //! Begin the thread
    void run();
    //! Send a script file to the tool
    void sendScriptFile(string scriptfile);
    //! Send a command to the tool
    istream *sendCommand(string cmd); 
    //! Are the communication sockets available?
    bool isTessToolReceiverConnected();
    //! Return the GUI instance
    TessToolGUI *getGUI() { return _gui; };
    //! Return the VTK isntance
    TessToolVTK *getVTK() { return _vtkThread; };
    
    //! Begin parsing CLI commands
    void startCLICommandLoop();
    //! Start the VTK loop
    void startVTK();
    //@{
    //! Member functions needed by the GUI
    void sampleNext();
    void visualize();
    void updateBIEOutputView(char *buffer);
    void updateVTK();
    void setHueLow(float l);
    void setHueHigh(float l);
    void setSaturationLow(float l);
    void setSaturationHigh(float l);
    void setValueLow(float l);
    void setValueHigh(float l);
    void setAlphaLow(float l);
    void setAlphaHigh(float l);
    void setVTKWindow(int width, int height);
    vtkCellPicker *getVTKCellPicker();
    vtkRenderer *getVTKRenderer();
    vtkRenderWindow *getVTKRenderWindow();
    vtkGtkRenderWindowInteractor *getVTKRenderWindowInteractor();
    void initVTK();
    void setScalarName(char *);
    void setScalarInfoName(char *);
    void setScriptFileName(char *fn);
    char* getScriptFileName();
    void setWorkingDirName(char *fn);
    void setDataFileName(char *fn);
    char* getDataFileName();
    void quitTessTool();
    
    void setScalarMean();
    void setScalarVariance();
    void setScalarMax();
    void setScalarMin();
    void setScalarMaxDifferenceModelData();
    
    static TessToolController* _guiController;
    //@}
  };
  
}
#endif
