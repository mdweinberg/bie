#ifndef TessToolGPL_H
#define TessToolGPL_H

#include <string>
#include <vector>
#include <iterator>
#include <sstream>

#include <Node.h>
#include <QuadGrid.h>
#include <SquareTile.h>
#include <RecordInputStream_MPI.h>
#include <bieTags.h>

#include "TestUserState.h"
#include "PersistenceControl.h"
#include "StateTransClosure.h"
#include "StateTransClosure_Test.h"
#include "gvariable.h"

using namespace std;

namespace BIE { 
  
  //! Structure for sorting rectangular grid output
  struct GPL_record {
    //! x coordinate
    double x;
    //! y coordinate
    double y;
    //! sort key
    double s;
  };

  static ofstream tterr;
  
  //! Data output handler (not threaded)
  class TessToolGPL {
    
    typedef enum {TESS_POINT=0, TESS_QUADGRID} TessType;
    
  public:
    
    //@{
    //! Constructors
    TessToolGPL();
    TessToolGPL(string pdir);
    //@}

    //! Destructor
    ~TessToolGPL();
    
    //@{
    //! Members to connect data sets and streams
    void SetSourceStream(RecordInputStream_MPI *mpistrm) {_mpistrm = mpistrm;};
    void initGPL();
    void createNewDataSet(ostream& out, int indx, bool order=true);
    //@}
    
  private:

    void listTileData(Node *node, double scalar);
    void outputData(ostream& out, bool ordered);

    void initialize();
    void initializeGPL();
    void initializeTessellation();
    
    double getScalar();

    GPL_record record;
    vector<GPL_record> cells, stanza;
    BasicType *curtype;

    Tessellation *_tess;
    TessType _tessType; // Point, QuadGrid, .....
    RecordInputStream_MPI *_mpistrm;
    
    bool _initTessellation, _initGPL;
    
    string _scalarNameString, _scalarInfoNameString, _persistencedir;

    int _scalarIndex, _scalarInfoIndex;
    string _tmpScalarNameString, _tmpScalarInfoNameString;
    bool _scalarNameInitialized, _scalarInfoNameInitialized, 
      _scalarNameChanged, _scalarInfoNameChanged; // indices will be recomputed
    bool _firstDataSet;
    int _sampleNum;
  };
  
}
#endif
