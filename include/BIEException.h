// -*- C++ -*-

#ifndef BIEException_h
#define BIEException_h

#include <vector>
#include <string>
#include <sstream>
#include <mpi.h>		// mpi must be defined before netcdf
#define MPI_INCLUDED		// to cope with netcdf macros (sigh)
#include <netcdf.h>
#undef  MPI_INCLUDED

using namespace std;

namespace BIE { 

  /** Defines an Error Handler base class BIEException to handle
      exceptions
  */
  class BIEException 
  {
  public:
    //! Generic exception
    BIEException(string exceptionname, string message,
                 string sourcefilename, int sourcelinenumber);

    //! Copy constructor
    BIEException(const BIEException& e);

    //! Destructor.
    virtual ~BIEException() {}

    //! Returns an error message suitable for printing to the user.
    string getErrorMessage();
    
  protected:
    //! Protected so it is only called by properly implemented classes.
    BIEException(string sourcefile, int linenumber);

    //! Friendly name of the exception.
    string exceptionname;

    //! Error message describing the error in more detail.
    ostringstream errormessage;

  protected:
    //! Source file where throw occured.
    string sourcefilename;

    //! Line number of throw.
    int sourcelinenumber;
  };


  //! Used when execution reaches a point it should not reach.
  class InternalError : public BIEException 
  {
  public:
    //! Use this when you reach an unexpected state.
    //@{
    InternalError(string sourcefilename, int sourcelinenumber);

    InternalError(string msg, 
		  string sourcefilename, int sourcelinenumber);

    InternalError(int err, string msg, 
		  string sourcefilename, int sourcelinenumber);
    //@}
  };
    
  class RecordType;
  class BasicType;

  //! handle Type related exceptions in DataStreams.
  class TypeException : public BIEException 
  {  
  public:
    //@{
    /**
       Thrown when there was an attempt to access a field as though it
       had a different type.  Each constructor allows different
       diagnostic info to be passed to error message.
    */
    TypeException(BasicType * attempted, BasicType* actual,
                  string sourcefilename, int sourcelinenumber);

    //! Constructor
    TypeException(RecordType * inputtype, RecordType * expectedtype,
                  string sourcefilename, int sourcelinenumber);

    //! Constructor
    TypeException(BasicType * unsupported, 
		  string sourcefilename, int sourcelinenumber);

    //! Constructor
    TypeException(string error, 
		  string sourcefilename, int sourcelinenumber);
    //@}
  };

  //! No value related exceptions in dataStreams
  class NoValueException : public BIEException 
  { 
  public:
    //! Constructor
    NoValueException(string sourcefilename, int sourcelinenumber);
  };
  
  /** Handle the case of no such field related exceptions in
      DataStreams
  */
  class NoSuchFieldException : public BIEException 
  { 
  public:
    //! Constructor: supply field name
    NoSuchFieldException(string fieldname,
			 string sourcefilename, int sourcelinenumber);

    //! Constructor: supply field index
    NoSuchFieldException(int fieldindex,
			 string sourcefilename, int sourcelinenumber);
  };
  
  //! Detected error in setting input
  class NotSetInputException : public BIEException 
  {
  public:
    //! Constructor: supply input index
    NotSetInputException(int inputindex, 
			 string sourcefilename, int sourcelinenumber);
  };
  
  //! Expected a scalar but didn't get one
  class NotScalarInputException : public BIEException 
  {
  public:
    //! Constructor
    NotScalarInputException(int inputindex, 
			    string sourcefilename, int sourcelinenumber);
  };
  
  //! No such filter related exceptions in DataStreams.
  class NoSuchFilterInputException : public BIEException 
  {
  public:
    //! Constructor: supply input index
    NoSuchFilterInputException(int inputindex,
			       string sourcefilename, int sourcelinenumber);

    //! Constructor: supply input name
    NoSuchFilterInputException(string inputname, 
			       string sourcefilename, int sourcelinenumber);
  };

  //! No such filter output related exceptions in DataStreams.
  class NoSuchFilterOutputException : public BIEException 
  {
  public:
    //! Constructor: supply output index
    NoSuchFilterOutputException(int outputindex, 
				string sourcefilename, int sourcelinenumber);

    //! Constructor: supply output name
    NoSuchFilterOutputException(string outputname, 
				string sourcefilename, int sourcelinenumber);
  };
  
  //! Bad range related exception in DataStreams
  class BadRangeException : public BIEException 
  { 
  public:
    //! Constructor
    BadRangeException(int startindex, int endindex, 
                      string sourcefilename, int sourcelinenumber);
  };
  
  //! Name clash related exceptions in DataStreams
  class NameClashException : public BIEException
  { 
  public:
    //! Constructor
    NameClashException(string clashname, 
		       string sourcefilename, int sourcelinenumber);
  };
  
  //! Duplicate filed related exceptions in DataStreams.
  class DuplicateFieldException : public BIEException 
  { 
  public:
    //! Constructor: supply field index
    DuplicateFieldException(int fieldindex, 
			    string sourcefilename, int sourcelinenumber);

    //! Constructor: supply field name
    DuplicateFieldException(string fieldname, 
			    string sourcefilename, int sourcelinenumber);
  };
  
  //! Handles stream inheritance related exceptions in DataStreams.
  class StreamInheritanceException : public BIEException 
  { 
  public:
    //! Reports stream inheritance error.
    StreamInheritanceException(string sourcefilename, int sourcelinenumber);
  };
    
  //! Handles open file related exceptions.
  class FileOpenException : public BIEException 
  {
    public: 
    //! Reports filename, errno error, and location of throw
    FileOpenException(string filename, int errno_, 
		      string sourcefilename, int sourcelinenumber);
  };

  //! Handle file creation related exceptions.
  class FileCreateException : public BIEException 
  {
    public: 
    //! Reports filename, errno error, and location of throw
    FileCreateException(string filename, int errno_, 
			string sourcefilename, int sourcelinenumber);
  };
  
  //! Handle file format related exceptions for DataStreams
  class FileFormatException : public BIEException 
  {
  public:
    //! Constructor: supply diagnostic message
    FileFormatException(string message, 
			string sourcefilename, int sourcelinenumber);
  };
  
  //! handle end of streams related exceptions in DataStreams
  class EndofStreamException : public BIEException 
  { 
  public:
    //! Constructor
    EndofStreamException(string sourcefilename, int sourcelinenumber);
  };
  
  //! "Not root" related exceptions in DataStreams.
  class NotRootException : public BIEException 
  { 
  public:
    //! Constructor
    NotRootException(string sourcefilename, int sourcelinenumber);
  };
  
  //! No such connection related exceptions in DataStreams
  class NoSuchConnection : public BIEException 
  { 
  public:
    //! Constructor
    NoSuchConnection(string sourcefilename, int sourcelinenumber);
  };
  
  //! Unusable filter related exceptions in DataStreams.
  class UnusableFilterException : public BIEException 
  { 
  public:
    //! Constructor
    UnusableFilterException(string sourcefilename, int sourcelinenumber);
  };
  
  //! Attached filter related exceptions in DataStreams
  class AttachedFilterException : public BIEException 
  {
  public:
    //! Constructor
    AttachedFilterException(string sourcefilename, int sourcelinenumber);
  };

  //! "Insert position" related exceptions in DataStreams.
  class InsertPositionException : public BIEException 
  {
  public:
    //! Constructor: supply position index causing the problem
    InsertPositionException(int insertpos, 
			    string sourcefilename, int sourcelinenumber);
  };

  //! Invalid stream related exceptions in DataStreams.
  class InvalidStreamIDException : public BIEException 
  {
  public:
    //! Constructor: supply stream id #
    InvalidStreamIDException(int streamid, 
			     string sourcefilename, int sourcelinenumber);
  };

  //! CLI related exceptions
  class CliException : public BIEException 
  { 
  public:
    //! Constructor: supply diagnostic message
    CliException(string message, 
		 string sourcefilename, int sourcelinenumber);
  };

  //! Class not exist related exceptions in CLI
  class ClassNotExistException: public CliException 
  {
  public:
    //! Constructor: supply class name
    ClassNotExistException(const char *class_name, 
			   string sourcefilename, int sourcelinenumber);
  };
 
  //! Thrown when an method is called for an object that does not exist
  class MethodCallOnNonObjectException : public BIEException 
  {
    public:
    //! Pass the location of the throw
    MethodCallOnNonObjectException(string sourcefilename, int sourcelinenumber);
  };

  //! Method not exist related exceptions in CLI
  class MethodNotExistException: public BIEException 
  {
  public:
    //! Constructor: supply method name
    MethodNotExistException(const char *method_name, 
			    string sourcefilename, int sourcelinenumber);
  };

  //! Method has different arguments related exceptions in CLI
  class MethodDiffArgException: public CliException 
  {
  public:
    //! Constructor: supply method name
    MethodDiffArgException(const char *method_name, 
			   string sourcefilename, int sourcelinenumber);
  };

  //! Method has different type related exceptions in CLI
  class MethodDiffRetTypeException: public CliException 
  {
  public:
    //! Constructor: supply method name
    MethodDiffRetTypeException(const char *method_name, 
			       string sourcefilename, int sourcelinenumber);
  };

  //! Variable not exist related exceptions in CLI
  class VarNotExistException: public BIEException 
  {
  public:
    //! Constructor: supply variable name
    VarNotExistException(string var, 
			 string sourcefilename, int sourcelinenumber);
  };

  //! Type mismatch related exceptions in CLI
  class TypeMisMatchException: public BIEException {
  public:
    //! Constructor
    TypeMisMatchException(string sourcefilename, int sourcelinenumber);
  };

  //! Expression evaluation related exceptions in CLI
  class EvalExprException: public BIEException 
  {
  public:
    //! Constructor
    EvalExprException(string sourcefilename, int sourcelinenumber);
  };

  //! Prior-related exceptions
  class PriorException: public BIEException 
  {
  public:
    //! Constructor: supply diagnostic message
    PriorException(const char *error_msg, 
		   string sourcefilename, int sourcelinenumber);
  };

  //! File existence related exceptions
  class FileNotExistException: public BIEException 
  {
  public:
    //! Constructor: supply diagnostic error message and file name
    FileNotExistException(string error_msg, string filename, 
			  string sourcefilename, int sourcelinenumber);
  };

  //! Dimension not supported related exceptions
  class DimNotSupportException: public BIEException 
  {
  public:
    //! Constructor
    DimNotSupportException(string sourcefilename, int sourcelinenumber);
  };

  //! Type not supported related exceptions
  class TypeNotSupportException: public BIEException 
  {
  public:
    //! Constructor: supply unsupported requested type
    TypeNotSupportException(string type, 
			    string sourcefilename, int sourcelinenumber);
  };

  //! Dimension not matched related exceptions
  class DimNotMatchException: public BIEException 
  {
  public:
    //! Constructor
    DimNotMatchException(string sourcefilename, int sourcelinenumber);

    //! Constructor with leading message
    DimNotMatchException(string msg, 
			 string sourcefilename, int sourcelinenumber);
  };

  //! Dirichlet-distributed value related exceptions
  class DirichletValueException: public BIEException
  {
  public:
    //! Constructor
    DirichletValueException(string sourcefilename, int sourcelinenumber);
  };

  //! Dirichlet sum related exceptions
  class DirichletSumException: public BIEException 
  {
  public:
    //! Constructor: supply incorrect sum value
    DirichletSumException(double val, 
			  string sourcefilename, int sourcelinenumber);
  };

  //! Dirichlet moment computation related exceptions
  class DirichletMomException: public BIEException 
  {
  public:
    //! Constructor
    DirichletMomException(string sourcefilename, int sourcelinenumber);
  };

  //! Prior type related exceptions
  class PriorTypeException: public BIEException 
  {
  public:
    //! Constructor: supply requested type
    PriorTypeException(int type, 
		       string sourcefilename, int sourcelinenumber);
  };
  
  //! Dimension value related exceptions
  class DimValueException: public BIEException 
  {
  public:
    //! Constructor
    DimValueException(string sourcefilename, int sourcelinenumber);
  };

  //! Tessellation overlap related exceptions
  class TessellationOverlapException: public BIEException 
  {
  public:
    //! Constructor
    TessellationOverlapException(string sourcefilename, int sourcelinenumber);
  };

  //! KDDistance related exceptions.
  class KSDistanceException : public BIEException 
  {
  public:
    //! Constructor
    KSDistanceException(string sourcefilename, int sourcelinenumber);
  
    //! Constructor: supply dimension index and and total # dimensions
    KSDistanceException(int distindex, int dimensions, 
                        string sourcefilename, int sourcelinenumber);
    //! Constructor: supply distance indices and record types
    KSDistanceException(int distoneindex, RecordType * distonetype, 
                        int disttwoindex, RecordType * disttwotype,
			string sourcefilename, int sourcelinenumber);
  };

  //! handle set field related exceptions
  class DataSetFieldException : public BIEException 
  {
  public:  
    //! Constructor
    DataSetFieldException(string field, string msg, 
                          string sourcefilename, int sourcelinenumber);
  };

  //! Handles overflow of string buffers.
  class StringBufferOverflowException : public BIEException 
  {
  public:
    //! Constructor
    StringBufferOverflowException 
    (int bufferlen, int requiredlen, 
     string sourcefilename, int sourcelinenumber);
  };
  
  //! Handles errors when using the NetCDF library
  class NetCDFException : public BIEException 
  {
  public:
    /**
       Use this when netcdf return code is not NC_NOERR.
       Takes netcdf status indicator and turns it into error message.
    */
    NetCDFException(int ncstatus, string sourcefilename, int sourcelinenumber);
  };
  
  //! Handles cases where NetCDF format does not meet expectations
  class NetCDFFormatException : public BIEException 
  {
  public:
    //! Handles case where a variable is not one dimensional.
    NetCDFFormatException (string varname, int varid, int vardim, 
                           string netcdffilename, 
			   string sourcefilename, int sourcelinenumber);
    /**
       Handle case where a variable does not have an unlimited
       array length.
    */
    NetCDFFormatException (string varname, int varid, string netcdffilename, 
                           string sourcefilename, int sourcelinenumber);
    
    //! Handle case where a variable has a type we don't use.
    NetCDFFormatException (string varname, int varid, string netcdffilename, 
                           nc_type vartype, string sourcefilename, 
			   int sourcelinenumber);

    //! Handle case where user tries to include String field in NetCDF file.
    NetCDFFormatException (string fieldname, int fieldindex,
                           string sourcefilename, int sourcelinenumber);  
  };
  
  //! Can not retrieve data type
  class GetTypeException : public BIEException 
  {
  public:
    //! Constructor
    GetTypeException(int status, string sourcefilename, int sourcelinenumber);
  };
  
  //! Exception used when tile id is invalid
  class TileIDException : public BIEException 
  {
  public:
    /**
       Creates error message containing offending tile id, min and max 
       values for tile id, and the location of the throw.
    */
    TileIDException (int tileid, int mintileid, int maxtileid, 
                     string sourcefilename, int sourcelinenumber);
  };
  
  /**
   Exception used when the specified frontier is not valid in the
   context is is being used in.
  */
  class InvalidFrontierException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    InvalidFrontierException(string sourcefilename, int sourcelinenumber);
  };

  //! Exception used when distribution type casting fails
  class NoSuchDistributionException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    NoSuchDistributionException(string sourcefilename, int sourcelinenumber);
  };

  //! Exception used when distribution type is not implemented
  class NoPointDistributionException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    NoPointDistributionException(string sourcefilename, int sourcelinenumber);
  };

  //! This is not a binned distribution as expected
  class NoBinnedDistributionException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    NoBinnedDistributionException(string sourcefilename, int sourcelinenumber);
  };

  //! This distribution is incompatible with its application
  class InappropriateDistributionException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    InappropriateDistributionException
      (string stype, 
       string sourcefilename, int sourcelinenumber);
  };

  //! Exception used by TessTool
  class TessToolException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    TessToolException(string msg, string sourcefilename, int sourcelinenumber);
  };

  //! Exception used by LikelihoodComputation to indicate an impossible state
  class ImpossibleStateException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    ImpossibleStateException(string sourcefilename, int sourcelinenumber);

    //! Reports the location of the throw and adds a message
    ImpossibleStateException(string msg, 
			     string sourcefilename, int sourcelinenumber);
  };

  //! Exception generated for bad Metropolis Hastings proposal input files
  class MetropolisHastingsException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    MetropolisHastingsException(string file, 
				string sourcefilename, int sourcelinenumber);
  };


  //! Exception generated for an uninitialized state
  class EmptyStateException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    EmptyStateException(string sourcefilename, int sourcelinenumber);
  };

  //! Exception generated for bad State dimension
  class StateCreateException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    StateCreateException(unsigned idim, 
			 string sourcefilename, int sourcelinenumber);
  };

  //! Exception generated for bad State bounds
  class StateBoundsException : public BIEException 
  {
  public:
    //! Reports the location of the throw and dimension index
    StateBoundsException(unsigned indx,
			 string sourcefilename, int sourcelinenumber);
    
    //! Reports the location of the throw, dimension index and value
    StateBoundsException(unsigned indx, unsigned dmax, 
			 string sourcefilename, int sourcelinenumber);
  };

  //! Exception generated for logging error on resume
  class ResumeLogException : public BIEException 
  {
  public:
    //! Reports the location of the throw and a diagnostic message
    ResumeLogException(string message,
		       string sourcefilename, int sourcelinenumber);
  };

  //! Exception generated for a bad parameter value
  class BadParameterException : public BIEException 
  {
  public:
    //! Reports the location of the throw and a diagnostic message
    BadParameterException(string classname, string message,
			  string sourcefilename, int sourcelinenumber);
  };

  //! StateInfo blocking error
  class StateBlockingException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    StateBlockingException(string sourcefilename, int sourcelinenumber);
  };
  
  class State;

  //! StateInfo blocking error
  class NonExistentBlock : public StateBlockingException 
  {
  public:
    /** Reports the location of the throw, block index, and State
	variable used to construct a diagnostic message
    */
    NonExistentBlock(int j, const State* s, 
		     string sourcefilename, int sourcelinenumber);
  };

  //! Galphat specific error
  class GalphatException : public BIEException 
  {
  public:
    //! Reports the location of the throw.
    GalphatException(string sourcefilename, int sourcelinenumber);
  };

  //! Galphat specific error
  class GalphatBadCode : public GalphatException
  {
  public:
    /** Report config file classname, method name, diagnostic message
	and throw location
    */
    GalphatBadCode(string classname, string method, string message, 
		   string sourcefilename, int sourcelinenumber);
  };

  //! Galphat specific error
  class GalphatBadConfig : public GalphatException 
  {
  public:
    /** Report config file classname, method name, diagnostic message
	and throw location
    */
    GalphatBadConfig(string configfile, 
		     string classname, string method, string message, 
		     string sourcefilename, int sourcelinenumber);
  };

  //! Galphat specific error
  class GalphatBadParameter : public GalphatException 
  {
  public:
    /** Report config file classname, method name, diagnostic message
	and throw location
    */
    GalphatBadParameter(string classname, string method, string message,
			string sourcefilename, int sourcelinenumber);
  };

} // namespace BIE

  
#endif
