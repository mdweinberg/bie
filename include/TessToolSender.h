// This is really -*- C++ -*-

#ifndef TessToolSender_h
#define TessToolSender_h

#include <vector>
#include <string>
#include "mpi.h"
#include "MPIStreamFilter.h"
#include "MPICommunicationSession.h"
#include "RecordType.h"
#include "RecordOutputStream.h"
#include "Tessellation.h"
//#include "LikelihoodComputation.h"


namespace BIE { 
  
  class LikelihoodComputation;
  
  //+ CLICLASS TessToolSender
  //! Tess Tool Sender: For setting up sending/receiving data from Tess Tool.
  class TessToolSender {
  public:
    //+ CLICONSTR string bool
    //! Constructs a connected TessTool sender object.
    TessToolSender(string name, bool remote=false);

    //+ CLICONSTR bool
    //! Constructs a connected TessTool sender object with default name.
    TessToolSender(bool remote);

    //+ CLICONSTR
    //! Constructs a connected TessTool sender object with defaults
    TessToolSender();

    /// Destructor
    ~TessToolSender();

    //@{
    //! Allow sender to manipulate streams and send via MPI
    void Synchronize();
    void Detach();
    void SetSessionId(int level, int step);
    void SetLikelihoodComputation(LikelihoodComputation *);
    MPI_Comm GetMPIComm();
    
    void SetRecordType(RecordType *rt);
    void SetMPIStream(RecordOutputStream *mpistrm);
    void SetMPIFilter(MPIStreamFilter *filter);
    void SetMPISession(MPICommunicationSession *session) {_session = session;};
    
    RecordType *GetRecordType();
    RecordOutputStream *GetMPIStream();
    MPIStreamFilter *GetMPIFilter();
    MPICommunicationSession *GetMPISession() {return _session; };
    void persistTessellation();
    bool attachFilter(RecordStreamFilter* fil, int stackindex);
    bool attachFilter(RecordStreamFilter* fil);
    bool attachSelectionFilter(vector<string> fil, int stackindex);
    bool attachSelectionFilter(vector<string> fil);
    bool printFilterChain();
    bool reinitializeFilterChain();
    bool attachMPIFilter();
    bool replaceFilter(RecordStreamFilter* fil, int stackindex);
    bool attachDefaultFunctionSelectionFilter();
    //@}
    
  private:
    bool _remote;

    void init();
    void writeTessellationStore(Tessellation *tess);
    template<class T> void insertInVector(vector<T>& vec, int index, T item);
    
    char *_port;
    char *_name;
    MPI_Comm _tessComm;
    LikelihoodComputation *_likely;
    
    // save state from current 
    RecordType *_rt;                    // base record type
    RecordOutputStream *_mpistrm;       // base record stream
    MPIStreamFilter *_filter;           // Sink MPI Filter
    
    vector <RecordOutputStream*> _inputStreams;
    vector <RecordStreamFilter*> _filters;
    vector < vector<string> > _selectionFilters;
    vector <bool> _isSelectionFilter;
    vector <RecordOutputStream*> _filteredStreams;
    bool _mpiStreamCreated, _mpiFilterAttached, 
      _defaultFunctionSelectionFilterAttached;
    
    MPICommunicationSession *_session;
    
    int _sessionId;
    int _level, _step, _iter;
    
    // wrote/send tessellation
    bool _persistedTessellation;
  };
}

#endif
