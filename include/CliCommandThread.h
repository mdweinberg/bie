#ifndef CliCommandThread_h
#define CliCommandThread_h

#include <string>
#include <cc++/thread.h>
#include <mpi.h>
#include <sstream>

#include "CliOutputReceiveThread.h"

#ifdef  CCXX_NAMESPACES
 using namespace std;
 using namespace ost;
#endif

namespace BIE { 

  // CLICLASS CliCommandThread 
  //! CliCommandThread: For communicating with CLI.
  class CliCommandThread: public Thread {
  public:
   // CLICONSTR string int
   //! Constructs a CLICommandThread
   CliCommandThread(string hostname, int port);

   //! Destructor
   ~CliCommandThread();

   //! Start the thread
   void run();
   //! Get the next data sample
   void sampleNext();
   //! Terminate the thread
   void finish();
   //! Send command to CLI
   void sendCommand(string& cmd);
   //! Send script to CLI
   void sendScript(const string& filename);
   //! Get the reply stream
   istream *getReply();
   //! Get the command reply stream
   istream *getCommandReply();
   //! Get the semaphore controling my excution
   Semaphore *getSemaphore() { return _qsem;};
   //! Give me a control semaphore
   void setControllerSemaphore (Semaphore *controller) { _controller = controller; };

   //! Get the console output thread that 
   CliOutputReceiveThread *getConsoleLogger();

  private:
   int writeCommand();
   int writeCommand(const string& msg);
   int writeCommand(istream *strm);
   int readPrompt();
   int readReply();
   int readReply(int socketid);
   void notifyController() {  _controller->post(); };

   char *_hname;
   int _port;
   int _socketid, _console_socketid;
   char *_sendBuffer;
   char *_receiveBuffer;
   int _receiveBufferSize, _sendBufferSize, _cmdBufferSize;
   Semaphore *_qsem;
   Semaphore *_controller;
   char *_clicmd;
   CliOutputReceiveThread *_consoleLogger;
   bool _loop;
  };
}
#endif
