#ifndef ModelEvaluationRequest_h
#define ModelEvaluationRequest_h

#include "EvaluationRequest.h"
#include <BIEMutex.h>
#include <cc++/thread.h>
#include <vector>
#include <BIEmpi.h>

using namespace ost;

#undef MODEL_EVALUATION_FINE_TRACE

namespace BIE {

  /**
     hold a request for the evaluation of a model at a point.
     writes the result back into caller supplied storage, and posts on
     a caller supplied semaphore when complete
  */
  class ModelEvaluationRequest : public EvaluationRequest {
  public:
    //! Constructor
    ModelEvaluationRequest(int tileId, double x, double y, int numEvalsAtPoint,
                           Semaphore& finished,
			   vector<double>& result
                           )
      {
        LogIt("Request construction");
        // load up array that will be used as a buffer to MPI_send
        args[0] = tileId;
	args[1] = x;
        args[2] = y;

        this->finished = &finished; // remember address of semaphore to clock when we're done
        this->result = &result;  // remember address of vector to store result in.

        returnSize = numEvalsAtPoint;  // number of doubles in return value

	//        ++MPIMutex;
	//          MPI_Comm_rank(MPI_COMM_WORLD, &myId); // save id for debugging
        //--MPIMutex;
	this->myId = myid; // GLOBAL from BIEmpi.cc
      }

    virtual ~ModelEvaluationRequest(){}

    virtual void Send(int destination, vector<MPI_Comm>*comms)
      {
        if (sent){
	  cerr << "ModelEvaluationRequest.Send:  ERROR. Attempt to send message twice from a Model request.\n";
          exit(1);
        }

	// first do a send to to the server to get its attention.  Rethink This.
        int command = 4;

        LogIt("Model just before Mutex");
	
        sendMutexWaitTime.start();
        ++MPIMutex;
        sendMutexWaitTime.stop();


            LogIt("Model just before first send");
            sendCmdTime.start();
            MPI_Send(&command, 1, MPI_INT, destination, 11, MPI_COMM_WORLD);  // send command id
	    //            MPI_Send(&command, 1, MPI_INT, 0, 11, (*comms)[destination]);  // send command id
            sendCmdTime.stop();

            LogIt("Model between sends");

            // Next send the actual message
            sendEvalTime.start();
            MPI_Send(args, sizeof(args)/sizeof(double), MPI_DOUBLE, destination, 22, MPI_COMM_WORLD);
	    //            MPI_Send(args, sizeof(args)/sizeof(double), MPI_DOUBLE, 0, 22, (*comms)[destination]);
            sendEvalTime.stop();

            LogIt("Model After 2nd send");
        --MPIMutex;

        sent = true;

        #ifdef MODEL_EVALUATION_FINE_TRACE
          cout << "ModelEvaluationsRequest.Send: Sent model eval request from " << myId << " to " << destination << "\n";
        #endif
      }
     

    // Receive and process a message for which we have a probeStatus from MPI.
    // The message is expected to be a reply to our request for evaluation.
    virtual void Recv(MPI_Status *probeStatus)
      {
         int tag  = probeStatus->MPI_TAG;
         int sender = probeStatus->MPI_SOURCE;
         int messageReturnSize;

         MPI_Status status;

         #ifdef MODEL_EVALUATION_FINE_TRACE
            cout << "ModelEvaluationsRequest.Recv: message received from " << sender << " on " << myId << "\n";         
         #endif

         if (tag !=23){
           cerr << "ModelEvaluationRequest.Recv Tag mismatch in MPI probe. Expected 23 recieved " << tag << "\n";
	   cerr << "Process: "<< myId << " Sender = " << sender << "\n";
	   exit(1);
	 }

         if (received){
	  cout << "ModelEvaluationRequest.Recv ERROR. Attempt to receive two results from a request.\n";
          exit(1);
         }         

         double *z = new double[returnSize];

         ++MPIMutex;
            MPI_Get_count(probeStatus, MPI_DOUBLE, &messageReturnSize);
         --MPIMutex;

         if (messageReturnSize != returnSize){
            cerr << "ModelEvaluationRequest.Recv: Error - returned value wrong size from modelEvaluation.  Expecting " <<
  	    returnSize << " got " << messageReturnSize << "\n";

            cerr << "Process: "<< myId << " Sender = " << sender << "\n";

            exit(1);
         }


         ++MPIMutex;
            MPI_Recv(z, returnSize, MPI_DOUBLE, sender, 23, MPI_COMM_WORLD, &status); //shouldn't block
         --MPIMutex;
         received = true;


         result->resize(returnSize); // return value

         // copy buffer into vector
         for(int i=0; i<returnSize; i++) (*result)[i] = z[i];

         delete[] z;

         
         #ifdef MODEL_EVALUATION_FINE_TRACE
            cout << "ModelEvaluationsRequest.Recv: completion posted from " << sender << " on " << myId << "\n";         
         #endif

         finished->post();  // notify the user's thread that requested the evaluation

      }

    //! Argument variable
    double args[3];
    //! Pointer to evaluated result
    vector<double> *result;
    //! The size of the result
    int returnSize;

  private:    
    int myId;
  };
}
#endif // ModelEvaluationRequest_h
