// -*- C++ -*-

#ifndef _BernsteinPoly_h
#define _BernsteinPoly_h

@include_persistence

namespace BIE {
  
  /**
     The Bernstein basis polynomial of degree \f$n\f$ is defined as
     \f[
     B_{n,k}(x) = {{n \choose k}} (1-x)^{n-k} x^k
     \f]
     where \f$k = 0, \ldots, n\f$ and \f${n \choose k}\f$ is the standard
     combinatorial function.  For an arbitrary value of \f$n\f$, the set
     \f$B_{n,k}\f$ forms a basis for the space of polynomials of degree
     \f$n\f$ or less.
     
     One may show from this definition that a basis function can be written
     in terms of the standard power basis:
     \f[
     B_{n,k}(x) = \sum_{i=k}^n (-1)^{i-k} {n \choose k} {i \choose k} x^i.
     \f]   
     This relation may be "inverted" to show that
     \f[
     x^k = \sum_{i=k-1}^{n-1} \frac{{i \choose k}B_{n,k}(x)}{{n \choose k}}.
     \f]   
     
     Our interest in these polymomials is motivated by their use in
     approximating any function in the unit interval.  Of course, this may
     be extended to any interval by an appropriate transformation.  For any
     function \f$f(x)\f$, the Bernstein polynomial is defined as
     \f[
     {\tilde B}[n,f](x) = \sum_{k=0}^n f(k/n) B_{n,k}(x)
     \f]
     and one may show that
     \f[
     \lim_{n\rightarrow\infty} {\tilde B}[n,f](x) \rightarrow f(x).
     \f]
     
     This basis has a number of other useful properties:
     <ul>
     <li> Each function \f$B_{n,k}(x)\f$ is positive definite in the unit
     interval \f$x\in(0,1)\f$ and is zero at the endpoints \f$x=0, 1\f$.
     <li> For all \f$n>0\f$, the function \f$B_{n,k}(x)\f$ has a maximum at
     \f$x=k/n\f$.
     <li> The sum
     \f[
     \sum_{k=1}^n B_{n,k}(x) = 1
     \f]
     for all $x$.
     <li> For $n>0$, the Bernstein basis polynomial has two useful
     recursion relations. First, an "upward" relation that combines two
     lower-order members to get a higher-order member:
     \f[
     B_{n,k}(x) = ( 1 - x ) B_{n-1,k}(x) + x B_{n-1,k-1}(x)
     \f]   
     where we assume that \f$B_{n-1,-1}(x)\equiv0\f$ and
     \f$B_{n-1,n}(x)\equiv0\f$.  Second, a "downward" relation that
     combines to higher-order members to get a lower-order member:
     \f[
     B_{n,k}(x) = \frac{\left[(n+1-k) B_{n+1,k}(x) + (k+1) B_{n+1,k+1}(x) \right]}{n+1}.
     \f]    
     <li> A derivative relation:
     \f[
     \frac{d B_{n,k}(x)}{dx} = n B_{n-1,k-1}(x) - B_{n-1,k}(x).
     \f]

     This class is intended to be used internally by models.
  */
  class @persistent_root(BernsteinPoly)
  {
  private:
    
    size_t              @autopersist(n);
    int                 @autopersist(bIndx);
    double              @autopersist(lastX);
    std::vector<double> @autopersist(poly);
      
  public:
    
    //! Null Constructor
    BernsteinPoly();

    //! Constructor: define polynomial of order n
    BernsteinPoly(int order);

    //! Constructor: define polynomial of order n, and State block j
    BernsteinPoly(int order, int j);

    //! Copy constructor
    BernsteinPoly(const BernsteinPoly& p)
    {
      n      = p.n;
      bIndx  = p.bIndx;
      lastX  = p.lastX;
      poly   = p.poly;
    }

    //! Return polynomial basis as a vector
    const std::vector<double>& operator()(double x);

    //! Return a single value for the kth basis function
    double operator()(unsigned k, double x);

    /** Approximation polynomial, state input
	
	If the State block variable is set, it will attempt to use
	that block as coefficients, if the dimension matches that of
	the Bernstein basis.

	If the State block variable is unset, it will attempt to use
	the entire vector as coefficients.
     */
    double operator()(BIE::State* s, double x);

    //! Approximation polynomial, vector input
    double operator()(std::vector<double>& v, double x);

    /** Integral of the kth basis function
	\f[
	\int B_{n, k}(x) dx = \frac{1}{k+1}\sum_{m=n+1}^{k+1} B_{m,k+1}(x)
	\f]
    */
    double integral(int k, double x);

    @persistent_end
  };
  
}

#endif
