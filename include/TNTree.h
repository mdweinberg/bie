#ifndef TNTREE_H
#define TNTREE_H

#include <mpi.h>

#include <cassert>
#include <istream>
#include <ostream>
#include <vector>
#include <list>
#include <set>

#include <boost/shared_ptr.hpp>

template< typename T >
class TNTree
{
public:

  friend class TwoNTree;

  class Node;
  class Branch;
  class Leaf;
  
  typedef boost::shared_ptr<Node>   NodePtr;
  typedef boost::shared_ptr<Branch> BranchPtr;
  typedef boost::shared_ptr<Leaf>   LeafPtr;

  typedef std::pair<std::vector<int>, unsigned int> leafValue;

  /** 
      \class listElem
      \brief A simple pair container for iterating the frontier
      \see Reset()
      \see Next()
  */
  class listElem {
  public:
    //! Vector of the data values
    std::vector<T> pValues;

    //! The number of datums in the parent node
    unsigned pCount;

    //! The level
    unsigned pLevel;

    //! The index
    std::vector<int> pIndex;

    //! Size
    unsigned pSize;

    //! The volume for this node
    double pVol;

    //! Pointer to the parent node
    NodePtr parent;

    //! Default constructor
    listElem() : pCount(0), pLevel(0), pSize(0), pVol(0) {}

  };


  //@{
  /** 
      Constructors
  */
  TNTree( int size, int d, int bucket, const T& emptyValue = T(0) );
  TNTree( const TNTree<T>& o );
  //@}

  //@{
  //! Accessors
  int size() const;
  const T& emptyValue() const;
  //@}

  //@{
  //! Memory diagnostics
  static unsigned long branchBytes();
  static unsigned long leafBytes();
  unsigned long bytes() const;
  int nodes() const;
  int nodesAtSize( int size ) const;
  //@}
  
  //@{
  //! Frontier iterator
  void Reset();
  listElem* Next();
  listElem* find( std::vector<int>& indx );
  //@}

  //@{
  //! Mutators
  void setEmptyValue( const T& emptyValue );
  void swap( TNTree<T>& o );
  TNTree<T>& operator= ( TNTree<T> o );
  //@}

  //@{
  //! Indexing operators
  const T& operator() ( std::vector<int>& indx ) const;
  const T& at( std::vector<int>& indx ) const;

  void add( std::vector<int>& indx, const T& value );
  //@}

  //! Assign minimum field value for each node
  void computeMinValue(std::vector< std::vector<double> >& data, unsigned dindx)
  {
    getMinValue(root_, data, dindx);
  }

  void getMinValue(NodePtr n, std::vector< std::vector<double> >& data,
		   unsigned dindx);

  double TrimFrontier(double fraction);

protected:
  //! listElem instance for frontier
  listElem elem;

  //! Maximum level
  int maxLev_;

  //! Target bucket size
  int bucket_;

  //! Dimension of the index for each datum
  int dim_;

  //! For identifying the type of node (for polymorphic access)
  enum NodeType { BranchNode=1, LeafNode=2 };

  NodePtr& root();
  const NodePtr root() const;
  //@{
  //! The iterator and frontier list for the current tree
  typename std::map<Node*, NodePtr>::iterator current;
  typename std::map<Node*, NodePtr> frontier;
  //@}
  
  static void deleteNode( NodePtr node );
  
private:
  //@{
  //! Recursive helper functions
  unsigned long bytesRecursive( const NodePtr node );
  int  nodesRecursive( const NodePtr node );
  int  nodesAtSizeRecursive( int targetSize, int size, NodePtr node );
  void assignListRecursive( NodePtr node, std::vector<int> indx );
  void gatherData(NodePtr node, LeafPtr leaf, 
		  typename std::set<NodePtr>& nodes, 
		  typename std::set<NodePtr>::iterator& ld);
  void sanityCheck(NodePtr node, unsigned& branches, unsigned& leaves );
  void nodeDist(int toggle);
  void splitLeaf(NodePtr &n);
  //@}
  
  //! For debugging checks
  static bool TNTdebug;

public:

  //@{
  //! Node classes
  
  /**
     \class Node
     \brief Base class for all nodes
  */
  class Node
  {
  public:

    //! For debugging
    static int maxLev;

    //! Minimum field value
    double minv_;

    //! Associated minimum Leaf value
    leafValue leaf_;

    //! Bit length size
    int size_;

    //! Data dimension
    int dim_;

    //! Level
    int level_;

    //! Sibling index
    int sib_;

    //! Pointer to parent
    NodePtr parent_;
    
    //! Data count
    unsigned number_;

    //! Index
    std::vector<int> index_;

    //! Return an enum identifying the node type.  \see NodeType
    NodeType type() const;
    
    //! Dimension of the data
    int Dim() const { return dim_; }

    //! Level
    int Lev() const { return level_; }

    //! Division size
    int Size() const { return size_; }

    //! Node volume
    double Volume() 
    {
      if (complete_)
	return complete_vol;
      else
	return exp(-log(2.0)*dim_*level_);
    }

    //! Is this node is at an edge?
    bool Edge()
    {
      int high = (1 << level_) - 1;
      for (int n=0; n<dim_; n++) {
	if (index_[n] == 0 || index_[n] == high) return true;
      }
      return false;
    }

    /** Assign sibling index
     * \param i is the full index for a point in this node
     */
    void Sib(const std::vector<int>& i)
    {
      sib_ = childID(i);
    }

    /**
     * Generate child index
     * \param indx is full index
     */
    int childID(const std::vector<int>& indx)
    {
      // Correct node?
      if (TNTree<T>::TNTree::TNTdebug) {
	if (badIndex(indx)) {
	  std::cerr << "INDEX FAILURE" << std::endl;
	}
      }
      int n = 0;
      for (int d=0; d<dim_; d++) n += (indx[d] & size_/2) ? (1<<d) : 0;
      return n;
    }

    /** Check node index
     * \param i is the full index for a point in this node
     */
    bool badIndex(const std::vector<int>& i)
    {
				// Check each dimension serially
      for (int n=0; n<dim_; n++) {
	int size = size_ << level_;
	int indx = 0;		// Begin at root level
	for (int j=0; j<level_; j++) {
	  size >>= 1;
	  indx <<= 1;
	  indx |= !!(i[n] & size);
	}
	if (indx != index_[n]) return true;
      }
      return false;
    }

  protected:
    //! Root node constructor for base
    Node( NodeType type, int size, int dim );

    //! Constructor for base
    Node( NodeType type, NodePtr parent, const std::vector<int>& i );

    //! For volume completion
    bool   complete_;
    double complete_vol;

  private:
    //! Node type
    NodeType type_;
  };

  /**
     \class Branch
     \brief Non leaf node
  */
  class Branch : public Node
  {
  public:

    //! Constructor for a branch with parent \a parent and index \a i
    Branch( NodePtr parent, std::vector<int>& i );

    /**
       Constructor for a root node branch with size \a size, data
       dimension \a dim and index \a i
    */
    Branch( int size, int dim );

    //! Copy constructor
    Branch( const Branch& b );

    /** Return a pointer to the child node specified by the full index
	(const version) */
    const NodePtr child( std::vector<int>& i ) const;
    
    /** Return a pointer to the child node specified by the full index
	(mutable version) */
    NodePtr& child( std::vector<int>& i );

    /** Return a pointer to the child node specified by index
	(const version) */
    const NodePtr child( int index ) const;

    /** Return a pointer to the child node specified by index
	(mutable version) */
    NodePtr& child( int index );

  private:
    Branch& operator= ( Branch b );

  private:
    //! Pointers to children
    std::vector<NodePtr> children;
  };

  /**
     \class Leaf
     \brief Contains a data list
  */
  class Leaf : public Node
  {
  public:
    typedef std::pair<std::vector<int>, T> leafValue;

    //@{
    //! Constructors
    Leaf(const std::vector<int>& i, const T& v, int size, int dim);
    Leaf(const std::vector<int>& i, const T& v, NodePtr p);
    Leaf(const leafValue& lv, NodePtr p);
    Leaf(const leafValue& lv, NodePtr p, double vol);
    //@}
    
    //@{
    //! Return the values for this leaf
    std::vector<leafValue>& values();
    leafValue& values(int i);
    //@}

    //@{
    //! Add value
    void add(const leafValue& v);
    void add(const std::vector<int>& i, const T& v);
    //@}

  private:
    //! Data value
    std::vector<leafValue> values_;
  };
  //@}

private:
  NodePtr root_;
  T emptyValue_;
  int size_;
};

#include "TNTree.inc"

#endif
