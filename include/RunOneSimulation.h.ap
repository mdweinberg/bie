// This is really -*- C++ -*-

#ifndef RunOneSimulation_h
#define RunOneSimulation_h

#include <RunSimulation.h>
#include <Distribution.h>
#include <Ensemble.h>
#include <Tile.h>
#include <Model.h>
#include <Integration.h>
#include <PostMixturePrior.h>
#include <InitialMixturePrior.h>
#include <DataTree.h>
#include <CountConverge.h>
#include <SubsampleConverge.h>
#include <MappedGrid.h>
#include <KdTessellation.h>
#include <QuadGrid.h>
#include <CountConverge.h>
#include <SubsampleConverge.h>
#include <LikelihoodComputation.h>

#include <BIEmpi.h>
#include <TemperedSimulation.h>
#include <ReversibleJump.h>
#include <MetropolisHastings.h>
#include <gvariable.h>

#include <map>

@include_persistence

namespace BIE {
  //+ CLICLASS RunOneSimulation SUPER RunSimulation
  /** Simulate the posterior defined Simulation instance.  The
      posterior will be returned in the Ensemble instance.
  */
  class @persistent(RunOneSimulation) : public @super(RunSimulation) 
  {
  public:
    
    //+ CLICONSTR int double Ensemble* Prior* Simulation*
    /** Constructor
	@param nsteps is the maximum number of steps for thsi simulation
	@param wscale is the Post[Mixture]Prior::width_factor
	@param sstat  is the instance of an Ensemble to hold and process the posterior sample
	@param prior  is the instance of an Prior that defines the prior distribution for this model
	@param sim    is the simulation method for this simulation
    */
    RunOneSimulation(int nsteps, double wscale,
		     BIE::Ensemble *sstat, BIE::Prior *prior,
		     BIE::Simulation *sim);

    //! Null constructor
    RunOneSimulation() : enslaved(false) {}

    //! Destructor
    virtual ~RunOneSimulation() {}

    //+ CLIMETHOD void Run
    //! Run the actual simulation (without threads)
    void Run();

    //+ CLIMETHOD void Resume
    //! Resume restored simulation (without threads)
    void Resume();

    //+ CLIMETHOD void ResumeLog
    //! Rollback state log file on resume
    void ResumeLog() { resumelog = true; }

    //+ CLIMETHOD void SetTessTool TessToolSender*
    //! Define a TessTool commuincator for this simulation
    void SetTessTool(BIE::TessToolSender *tt);

    //+ CLIMETHOD void SampleNext
    //! Flag a TessTool sample from the running simulation
    void SampleNext();

    //+ CLIMETHOD void SwitchOnCLI
    //! Return control to the CLI
    void SwitchOnCLI();

    //+ CLIMETHOD void SetNewSteps int
    /** Set a new number of steps for this simulation (e.g. to continue
	from a checkpoint after the maximum number of steps had been reached)
    */
    void SetNewSteps( int n ) { nsteps = n; }

    //+ CLIMETHOD void SuspendSimulation
    //! Request that the running simulation be suspended
    virtual void SuspendSimulation();

    //+ CLIMETHOD void ResumeSimulation
    //! Resume the suspended simulation
    virtual void ResumeSimulation();

    //+ CLIMETHOD void SetAutoTessTool int
    //! Request TessTool updates every @param freq iterations
    virtual void SetAutoTessTool(int freq);

  private:

    // Frontier * @autopersist(lastFrontier);
    Ensemble * @autopersist(sstat); 
    Prior * @autopersist(prior);
    Simulation * @autopersist(sim);

    int @autopersist(ttfreq);

    string @autopersist(prefixed_metfile);

    bool enslaved;
    bool @autopersist(resumelog);
    int @autopersist(nsteps);
    int @autopersist(level);
    double @autopersist(wscale);
    int @autopersist(nmix);			// Two component mixture
    int @autopersist(ndim);			// Two model parameters

    //! Use ordered mixture prior by some component parameter
    bool @autopersist(ordered);

    void initial();
    void resumeLog();

    int @autopersist(steps);
    int @autopersist(clev);

    // print useful info to let the user know where we are
    template<class Archive>
    void post_load(Archive &ar, const unsigned int file_version) {
      ostream cout(checkTable("simulation_output"));
      /*
      cout << "On Level " << clev 
	   << ", starting from Step : " << steps << endl;
      */
    }
    
    @persistent_end_split
};

}

#endif

