#ifndef GLOBAL_TABLE_H
#define GLOBAL_TABLE_H

class MethodTable;
class AData;

#include <vector>
#include <string>
#include <iostream>
using namespace std;
#include <cc++/config.h>

/**
   This structure is used to declare an array containing details of
   CLI's global variables.  This array is automatically created by
   the script that processes stylized comments in CLI.  Only global
   variables with corresponding stylized comments are placed in this
   array.
*/
typedef struct {
  //! Variable name
  const char *name; 
  //! Variable type
  int32 type; 
  //! Variable type name
  const char *type_str; 
  //! Address of the object
  void *addr;
} global_table_struct;

/**
   Table holding details of the global variables.
*/
extern std::vector<global_table_struct> global_variables;

/** 
    This class reads, sets and prints CLI's global variable values.  It reads
    and modifies the array automatically created by the stylized comment
    processor.  Input and output is done using the AData class.
*/
class GlobalTable 
{

public:
  /**
     This grabs the value from the global variable's memory location,
     and places the value in the AData object passed in.  AData.data.pval
     should point to the global variable's memory location, the AData.type
     field should be set to the global variable's type, and where 
  */
  static void get_value(AData *var, const char *type_str = 0);
  
  /**
     Looks up the global table for a variable with the given name.  If the
     variable exist, the method returns non-zero, and the value is returned
     in the AData object.  Otherwise, zero is returned.
  */
  static bool get_variable(const char *var, AData *value);
  
  /**
     Looks up the global variable table for a variable with the given name, and
     sets the value if it exists.  Exceptions are thrown when the type of 
     value and variable do not match, or where the variable doesn't exist.
  */
  static void set_variable(const char *var, AData *value);

  /**
     Prints out the value of the variable to the given stream.
     Returns non-zero if successful, and zero otherwise.
  */
  static bool print_value(const char *var, ostream & outputstream);
  
  //! Prints out the values of all global variables to the console.
  static void print_all_values(ostream & outputstream);
  
  //! Returns all the global variable names.
  static vector<string> get_all_variable_names();
};

#endif
