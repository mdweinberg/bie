#ifndef MPIStreamFilter_h
#define MPIStreamFilter_h

#include "RecordStreamFilter.h"
#include "RecordBuffer.h"
#include <mpi.h>


namespace BIE { 

  class MPICommunicationSession;

  //! MPI Stream Filter: Buffers the output to send via MPI.
  class MPIStreamFilter : public RecordStreamFilter {
  public:
    //! Constructs a connected filter for buffering output.
    MPIStreamFilter(BIE::RecordStream * stream, MPI_Comm communicator,
		    int sessionId, int bufferCapacityInWords=1<<11);
    //! Destructor
    virtual ~MPIStreamFilter();
    
    /// \cond

    void compute();
    void startNewSession(int newSessionId);
    void finishSession();
    void flush();
    void setHeaderRecordBuffer(RecordBuffer *hdrbuf);
    static void *packRecordBuffer(RecordBuffer *rb, int* packedsize);
    static int getRecordBufferSizeInBytes(RecordType *rt);

    friend class MPICommunicationSession;

    /// \endcond

  private:
    typedef void (BIE::MPIStreamFilter::*storeFn)(int fieldindex);

    void storeInt(int fieldindex);
    void storeReal(int fieldindex);
    void storeBool(int fieldindex);

    RecordType *_rtype;
    int _numFields;
    void *_buffer;
    void *_bufferIndex;
    int _bufferSize;
    int _recordSize;
    int _capacityInRecords;
    int _numRecords;
    storeFn *_sfn;
    filterIn *input;
    filterOut *output;
    
    bool _inSession;

    MPICommunicationSession *_mpiSendSession;
    static int _defaultBufferSize;

    RecordBuffer *_hdrBuffer;
    void *_hdr;
  };
}
#endif
