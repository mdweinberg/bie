#ifndef _AsciiProgressBar_h
#define _AsciiProgressBar_h

extern 
void AsciiProgressBar(ostream& out, const int total_size, 
		      const int count, const int step=10, const bool val=false);

#endif
