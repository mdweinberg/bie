#ifndef TileMPIThread_h
#define TileMPIThread_h

#include "TileRequestHandlerThread.h"

#include <cc++/thread.h>
using namespace ost;

namespace BIE{

  /** A helper class that handles all of the MPI communication for the tile group
      master processor in a tile_point MPI simulation. 
      Only runs in the tile master process.
  */

  class TileMPIThread : public Thread{
  public:
    /// Construct the thread with a reference to the sole TileRequestHandlerThread
    TileMPIThread( TileRequestHandlerThread *t) 
       {
           tileRequestHandlerThread = t;
           stopRequested = false;
           pauseRequested = false;
       };

    virtual ~TileMPIThread(){};
  
    /// run the thread that continuously polls the worker processers for replies.
    virtual void run ();

    /// accessor
    TileRequestHandlerThread* getTileRequestHandlerThread()
       {
            return  tileRequestHandlerThread;
       }

    /// called to make sure state is okay at start of new compute likelihood.
    void reInitialize(TileRequestHandlerThread* t);

    /// called to pause the thread in a known place
    void pause();

    /// called to restart the thread from a pause.
    void unPause();


    /// Set a flag requesting that the TileMPIThread exit its Run method.
    virtual void stopPausedThread() 
       {
            stopRequested = true;
	    unPause();
	    cout << "TileMPIThread.stop: Entered";
       }

 
    /// Method called by thread framework when Run exits. Used here to free self
    void final();

 private:
    TileRequestHandlerThread *tileRequestHandlerThread;
    volatile bool stopRequested;
    volatile bool pauseRequested;

    Semaphore paused;
    Semaphore resume;

  };

}






#endif // TileMPIThread_h
