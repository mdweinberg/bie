// This is really -*- C++ -*-

#ifndef MetropolisHastings_h
#define MetropolisHastings_h

#include <iostream>
#include <iomanip>

using namespace std;

#include <Simulation.h>
#include <LikelihoodComputation.h>
#include <StandardMC.h>

@include_persistence

namespace BIE
{
  
  class MixturePrior;
  
  //! \addtogroup mcalgorithm Markov chain algorithms
  //! @{

  //+ CLICLASS MetropolisHastings SUPER StandardMC
  /**
     Implements standard Metropolis Hastings MCMC step in the Sweep() method
  */
  class @persistent(MetropolisHastings) : public @super(StandardMC) 
  {
    
  public:

    //+ CLICONSTR
    //! Constructor
    MetropolisHastings() { algoname = "Metropolis Hastings"; }

    //! Update state based on a single sweep
    virtual bool Sweep(BIE::Simulation* sim, BIE::Chain *ch);

    @persistent_end
  };

  //! @}

} // namespace BIE

#endif
