#ifndef NormEvaluationRequest_h
#define NormEvaluationRequest_h

#include "EvaluationRequest.h"
#include <BIEMutex.h>
#include <iostream>
using namespace std;

#undef NORM_EVALUATION_FINE_TRACE

namespace BIE{

  //! Evaluate the normalization for the model
  class NormEvaluationRequest : public EvaluationRequest {
  public:
    //! Constructor
    NormEvaluationRequest(int tileId, double x, double y, Semaphore& finished, double& result)
      {
        // load up array that will be used as a buffer to MPI_send
        args[0] = tileId;
	args[1] = x;
        args[2] = y;

        this->finished = &finished;
        this->result = &result;

        // ++MPIMutex;
        //  MPI_Comm_rank(MPI_COMM_WORLD, &myId); //save id for debugging
        // --MPIMutex;
	this->myId = myid;  // GLOBAL from BIEmpi.cc

      }

    //! Destructor
    virtual ~NormEvaluationRequest(){};

    //! Send the message for a norm request
    virtual void Send(int destination, vector<MPI_Comm>*comms)
      {
        if (sent){
	  cout << "NormEvaluationRequest.Send: ERROR. Attempt to send message twice from a Norm request.\n";
          exit(1);
        }

	// first do a send to to the server to get its attention.  Rethink This.
        int command = 3;
        LogIt("Norm just before Sends");
        LogInt("Destination = ", destination);

        sendMutexWaitTime.start();
        ++MPIMutex;
        sendMutexWaitTime.stop();

           sendCmdTime.start();
           MPI_Send(&command, 1, MPI_INT, destination, 11, MPI_COMM_WORLD);  // send command id
	   //            MPI_Send(&command, 1, MPI_INT, 0, 11, (*comms)[destination]);  // send command id
           sendCmdTime.stop();

           // Next send the actual message
           sendEvalTime.start();
           MPI_Send(args, sizeof(args)/sizeof(double), MPI_DOUBLE, destination, 20, MPI_COMM_WORLD);
	   //            MPI_Send(args, sizeof(args)/sizeof(double), MPI_DOUBLE, 0, 20, (*comms)[destination]);
           sendEvalTime.stop();

        --MPIMutex;
        LogIt("Norm finished sends");
   
        sent = true;

        #ifdef NORM_EVALUATION_FINE_TRACE
          cout << "NormEvaluationRequest.Send: Sent norm eval request from " << myId << " to " << destination << "\n";
	#endif

      }
     
    //! Check for pending info
    virtual void Recv(MPI_Status *probeStatus)
      {
         int tag  = probeStatus->MPI_TAG;
         int sender = probeStatus->MPI_SOURCE;

         MPI_Status status;
         int returnSize;
         double z;

         if (tag !=21){
           cerr << "NormEvaluationRequest.Recv: Tag mismatch in MPI probe. Expected 21 recieved " << tag << "\n";
	   cerr << "Process: "<< myId << " Sender = " << sender << "\n";
           exit(1);
	 }

         if (received){
	  cerr << "NormEvaluationRequest.Recv: ERROR. Attempt to receive two results from a request.\n";
	   cerr << "Process: "<< myId << " Sender = " << sender << "\n";
          exit(1);
         }         
 
         ++MPIMutex;
           MPI_Get_count(probeStatus, MPI_DOUBLE, &returnSize);
         --MPIMutex;

         if (returnSize != 1){
            cout << "NormEvaluationRequest.Recv Error - returned value wrong size from normEvaluation.  Expecting " <<
  	     " 1 got " << returnSize << "\n";
	   cerr << "Process: "<< myId << " Sender = " << sender << "\n";
           exit(1);
         }


         ++MPIMutex;
            MPI_Recv(&z, 1, MPI_DOUBLE, sender, 21, MPI_COMM_WORLD, &status); //shouldn't block
         --MPIMutex;

	 //TODO Check Status!
         received = true;

        // copy return value back to caller
        *result = z;

        // notify the user's thread that requested the evaluation
        finished->post();

      }

    //! Argument string
    double args[3];
    //! Pointer to result
    double *result;

  private:    
    int myId; // for debugging
  };
}
#endif // NormEvaluationRequest_h
