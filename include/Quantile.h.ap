// This is really -*- C++ -*-

#ifndef Quantile_h
#define Quantile_h

@include_persistence

#include <vector>

namespace BIE {
  
  //! Quantile processor
  class @persistent_root(Quantile)
  {
  private:
    std::vector<unsigned> @ap(hist), @ap(cuml);
    double @ap(xmin), @ap(xmax), @ap(dx);
    unsigned @ap(n);
    bool @ap(good), @ap(comp);

    void cumulate()
    {
      if (comp) return;
      cuml.resize(n, 0);
      cuml[0] = hist[0];
      for (size_t i=1; i<n; i++)
	cuml[i] = hist[i] + cuml[i-1];
      comp = true;
    }

  public:

    //! Null constructor
    Quantile() { good = false; }

    //! Constructor
    Quantile(double xmin, double xmax, double n);

    //! Copy constructor
    Quantile(const Quantile& p);

    //! Reset the counters
    void reset()
    {
      if (!good) return;
      hist.resize(n, 0);
    }

    //! Add a new value
    void add(double x);

    //! Return the quantile value
    double operator()(double y);

    @persistent_end

  };

}
#endif
