// This is really -*- C++ -*-

#ifndef GalaxyModelOneD_h
#define GalaxyModelOneD_h

#include <deque>

#include <gaussQ.h>
#include <Model.h>
#include <Histogram1D.h>

#include <CacheGalaxyModel.h>

@include_persistence

namespace BIE {
  
  //+ CLICLASS GalaxyModelOneD SUPER Model
  //! Simple test galaxy model with one flux and line-of-sight caching 
  //! 
  //! The line-of-sight quantities that are parameter independent
  //! are precomputed and cached for each line of sight.
  //! 
  //! This is implemented with a hash map and a FIFO queue that is
  //! set by default to an infinite number of elements.  This may be
  //! changed using the CacheLimit() member function.
  class @persistent(GalaxyModelOneD) : public @super(Model)
  {
  public:

    //+ CLICONSTR int int SampleDistribution*
    //! Constructor 
    GalaxyModelOneD(int ndim, int mdim, BIE::SampleDistribution *_dist);

    //! Null constructor 
    GalaxyModelOneD();

    //! Destructor 
    ~GalaxyModelOneD();

    //+ CLIMETHOD void SetKnots int
    //! Reset number of integration points
    void SetKnots(int num) 
    { 
      NUM = num;
      delete intgr;
      intgr = new JacoQuad(NUM, ALPHA, BETA);
    }

    //+ CLIMETHOD void LogLineOfSight double
    //! Logarithmic spacing of integration knots along the line of sight
    void LogLineOfSight(double Smin)
    { 
      logs = true;
      smin = Smin;
    }

    //+ CLIMETHOD void LinearLineOfSight
    //! Linear spacing of integration knots along the line of sight
    void LinearLineOfSight()
    { 
      logs = false;
    }

    //+ CLIMETHOD void SetExtinction double double
    //! Radial and Vertical size of extinction slab
    void SetExtinction(double A, double Z) {A1 = A; Z1 = Z;}

    //+ CLIMETHOD void ExtinctionCoefficient double
    //! Set value of extinction coefficient (nominally k-band)
    void ExtinctionCoefficient(double ak) { AK = ak; }

    //+ CLIMETHOD void CacheLimit int
    //! Maximum number of cache elements (default=0, unlimited)
    void CacheLimit(int num) { cache_limit=num; }

    //+ CLIMETHOD void ModelLimits double double double double
    //! Set limiting values for model parameters
    void ModelLimits(double amin, double amax, double hmin, double hmax) {
      AMIN = amin;
      AMAX = amax;
      HMIN = hmin;
      HMAX = hmax;
    }

    //+ CLIMETHOD void MaximumDistance double
    //! Set line-of-sight extent
    void MaximumDistance(double rmax) { RMAX=rmax; }

    //! Initialize state dependent part of calculation
    //@{
    //! From State vector
    void Initialize(BIE::State&);
    //! From component weights and component parameter vectors
    void Initialize(std::vector<double>& w, std::vector< std::vector<double> >& p);
    //@}

    //! Reset model cache
    void ResetCache() {}

    //! Compute normalization of tiles
    virtual double NormEval(double x, double y, BIE::SampleDistribution *d=NULL);

    //! Compute normalization for point likelihood
    virtual double NormEval(double x, double y);

    //! Integration measure
    virtual double NormEvalMeasure(double x, double y) { return cos(y); }

    //! Main method returning source density
    std::vector<double> Evaluate(double x, double y, BIE::SampleDistribution *d=NULL);

    //! Identify labels for StateInfo
    std::vector<string> ParameterLabels();

    /** @name Global parameters */
    //@{
    
    //! Minimum magnitude (default 6);
    static double @ap(LMAG);

    //! Maximum magnitude (default 16);
    static double @ap(HMAG);

    //! Extinction scale length (default 20 [kpc])
    static double @ap(A1);

    //! Extinction scale height (default 100 [pc])
    static double @ap(Z1);

    //! Standard candle magnitude (default -4.0)
    static double @ap(K0);

    //! Std dev in standard candle magnitudes (default 0.25)
    static double @ap(SIGK);

    //! Number of integration knots for Jacobi quadrature (default 200)
    static int @ap(NUM);

    //! Jacobi quadrature parameters (default: 0, 0)
    static double @ap(ALPHA);
    static double @ap(BETA);

    //! Observers position (kpc) (default 8.0)
    static double @ap(R0);

    // Maximum radius (kpc) (default 20.0)
    static double @ap(RMAX);

    //! Extinction (mags) (default 0.1)
    static double @ap(AK);

    //! Limits for model parameters
    //@{
    //! Minimum scale length
    static double @ap(AMIN);		// Defaults: 0.2
    //! Maximum scale length
    static double @ap(AMAX);		//           8.0
    //! Minimum scale height
    static double @ap(HMIN);		//          50.0
    //! Maximum scale height
    static double @ap(HMAX);		//        1200.0
    //@}

    //! Log line of sight
    //@{
    //! Flag
    static bool @ap(logs);		// Default: false
    //! Minimum los
    static double @ap(smin);		// Default: 0.01
    //@}

    //@}

  protected:

    //! Constants
    //@{
    //! \f$\ln(10)\f$
    static double @ap(Log10);
    //! Number of radians per degree
    static double @ap(onedeg);
    //@}

    //! Cache limit exceeded
    bool cache_full;

    //! Maximum number of sight lines kept in cache
    unsigned int @ap(cache_limit);

    //! Maintain memory store for cache elements
    virtual void manageCache(coordPair&);

    //! Maximum number of components in mixture
    int @ap(M);

    //! Current number of components in mixture
    int @ap(Mcur);

    //! Number of model dimensions
    int @ap(Ndim);

    //! Constants related to record type.
    //@{
    //! Descriptor string for scale length
    static const char* @no_autopersist(LENGTH_FIELDNAME);
    //! Descriptor string for scale height
    static const char* @no_autopersist(HEIGHT_FIELDNAME);
    //! Descriptor strings for all other parameters
    static const char* @no_autopersist(PARAM_NAMES)[];
    //@}

    //! Histogram components
    //@{
    //! Number of bins
    int @ap(nbins);
    //! Low value for bin edges
    vector<double> @ap(lowb);
    //! Low value for bin edges
    vector<double> @ap(highb);
    //@}

    //! Integrator
    JacoQuad *@ap(intgr);

    //! Component weights
    vector<double> @ap(wt);
    //! Parameter vectors for each component
    vector< vector<double> > @ap(pt);

    //! Compute values along the line-of-sight
    virtual void generate(double L, double B, SampleDistribution *sd=NULL);

    //! Compute the bin predictions for the given model paraemeters
    virtual void compute_bins();

    //! Flag is true if parameter values are in bounds
    bool @ap(good_bounds);

    //! Check for in-bounds parameters and set flag
    void check_bounds();

    //! The line-of-sight element cache
    mmapGalCM cache;
    
    //! Cache iterator
    mmapGalCM::iterator mit;

    //! List of cache coordinates (for management)
    deque<coordPair> cacheList;

    //! Current cache element
    CacheGalaxyModel *current;

    //! Evaluation type (e.g. binned or point)
    EvalType @ap(type);

    @persistent_end
  };

}

#endif
