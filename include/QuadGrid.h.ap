// This is -*- C++ -*-

#ifndef QuadGrid_h
#define QuadGrid_h

#include <string>

@include_persistence

#include <Tessellation.h>

namespace BIE
{
  
  //! \addtogroup tess Tessellation types 
  //! @{
  
  //+ CLICLASS QuadGrid SUPER Tessellation
  /**
     A hierarchical, recursive, rectangular tessellation of space. 
     Starting with one tile encompassing the whole tessellation, each tile 
     is recursively split into four equally sized tiles.
     
  */
  class @persistent(QuadGrid) : public @super(Tessellation) {
    
  public:
    //+ CLICONSTR Tile* double double double double int
    /** 
	Constructs a QuadGrid tessellation.
	\param factory A tile object used as the basis for all tile objects
	created when constructing the tessellation.
	\param minx Lower x coordinate boundary of tessellation.
	\param maxx Upper x coordinate boundary of tessellation.
	\param miny Lower y coordinate boundary of tessellation.
	\param maxy Upper y coordinate boundary of tessellation.
	\param depth Integer specifying the depth of the tessellation (how many 
	times the top rectangular region is recursively split)
    */
    QuadGrid(BIE::Tile * factory, double minx, double maxx, 
	     double miny, double maxy, int depth);
    
    //! Destructor
    virtual ~QuadGrid() {}
    
    std::vector<int> GetRootTiles();
    std::vector<BIE::Node*> GetRootNodes();
    
  private:
    
    // The depth of the tessellation.
    int @autopersist(maxdepth);
    
    //! Tessellation tree root.
    Node * @autopersist(treeroot);
    
    //! Tile factory
    Tile * @autopersist(tilefactory);
    
    //! Performs tessellation of the space
    void tessellate(Node **node, int depth, int tileid, 
		    double minx, double miny, double maxx, double maxy);
    
    @persistent_end
  };
  
  //! @}
  
} // namespace BIE
#endif
