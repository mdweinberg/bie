#ifndef TessToolVTK_H
#define TessToolVTK_H

#include <string>
#include <vector>
#include <iterator>
#include <sstream>

#include <vtk/vtkIndent.h>
#include <vtk/vtkActor.h>
#include <vtk/vtkCamera.h>
#include <vtk/vtkProperty.h>
#include <vtk/vtkRenderWindow.h>
#include <vtk/vtkRenderer.h>
#include <vtk/vtkRenderWindowInteractor.h>
#include <vtk/vtkXRenderWindowInteractor.h>
#include "vtkGtkRenderWindowInteractor.h"

#include <vtk/vtkCellArray.h>
#include <vtk/vtkPolyData.h>
#include <vtk/vtkOutlineFilter.h>
#include <vtk/vtkFeatureEdges.h>
#include <vtk/vtkExtractEdges.h>
#include <vtk/vtkCleanPolyData.h>
#include <vtk/vtkClipPolyData.h>
#include <vtk/vtkSelectPolyData.h>
#include <vtk/vtkShrinkPolyData.h>


#include <vtk/vtkFloatArray.h>
#include <vtk/vtkPolyDataMapper.h>
#include <vtk/vtkStructuredGrid.h>
#include <vtk/vtkStructuredGridToPolyDataFilter.h>
#include <vtk/vtkStructuredGridGeometryFilter.h>
#include <vtk/vtkLookupTable.h>

#include <vtk/vtkCallbackCommand.h>
#include <vtk/vtkCamera.h>

#include <vtk/vtkCellPicker.h>
#include <vtk/vtkTextMapper.h>
#include <vtk/vtkTextProperty.h>
#include <vtk/vtkActor2D.h>
#include <vtk/vtkPoints.h>

#include <vtk/vtkGenericCell.h>
#include <vtk/vtkCellData.h>

#undef String
#undef Bool
#undef Int
#undef Status

#include <PersistenceControl.h>
#include <StateTransClosure.h>
#include <StateTransClosure_Test.h>
#include <TestUserState.h>
#include <gvariable.h>

#ifdef  CCXX_NAMESPACES
using namespace std;
using namespace ost;
#endif

namespace BIE { 
  
  static ofstream tterr;
  
  class Node;
  class RecordInputStream_MPI;
  class TessToolVTKInteractor;
  
  //! VTK call back for tile picker
  class vtkGtkCallback : public vtkCallbackCommand {
  public:
    //! Factory
    static vtkGtkCallback *New() {return new vtkGtkCallback; }
    //! Run the call back
    virtual void Execute(vtkObject *caller, unsigned long eid, void *cdata);
    
    //@{
    //! References
    vtkTextMapper *textMapper;
    vtkActor2D *textActor;
    vtkRenderWindow *renWin;
    //@}
  };
  

  //! TessTool thread for managing the rendered scene
  class TessToolVTK : public Thread {
    
    typedef enum {TESS_POINT=0, TESS_QUADGRID} TessType;
    
  public:
    
    //@{
    //! The size of the canvas
    static int default_height;
    static int default_width;
    //@}
    
    //@{
    //! Constructors
    TessToolVTK();
    TessToolVTK(string persistencedir);
    TessToolVTK(int w, int h);
    TessToolVTK(string pdir, int w, int h);
    //@}

    //! Destructor
    ~TessToolVTK();

    //! Begin the thread
    void run();


    //@{
    //! Other members

    void SetSourceStream(RecordInputStream_MPI *mpistrm) {_mpistrm = mpistrm;};
    void Render();
    Semaphore *getSemaphore() {return _vtksem;};
    void setProducerSemaphore(Semaphore *sem) {_producer = sem;};
    bool isTessellationInitialized() { return _initTessellation; };
    
    void setHueLow(float l);
    void setHueHigh(float l);
    void setSaturationLow(float l);
    void setSaturationHigh(float l);
    void setValueLow(float l);
    void setValueHigh(float l);
    void setAlphaLow(float l);
    void setAlphaHigh(float l);
    void setPersistenceDir(char* dir) { _persistencedir = string(dir); }
    
    void initVTK();
    vtkCellPicker *getVTKCellPicker() {return _picker;};
    vtkRenderer *getVTKRenderer() {return _renderer;};
    vtkRenderWindow *getVTKRenderWindow() {return _renWin;};
    vtkGtkRenderWindowInteractor *getVTKRenderWindowInteractor() {return iren;};
    
    void setScalarName(string sn);
    void setScalarName(char *sn);
    void setScalarInfoName(string sinf);
    void setScalarInfoName(char *sinf);
    //@}
    
  private:

    int addTileToPoints(Node *node, int offset, vtkPoints *points,  
			vtkCellArray *cells, vtkFloatArray *scalars, double scalar);
    void initialize();
    void initializeVTK();
    void initializeTessellation();
    
    double getScalar();
    double getScalarInfo();

    int getIndex(string s);
    void updateScalarIndices();
    void createNewDataSet();
    void RenderNewData();
    
    Tessellation *_tess;
    TessType _tessType; // Point, QuadGrid, .....
    RecordInputStream_MPI *_mpistrm;
    Semaphore *_vtksem;
    Semaphore *_producer;
    
    vtkPolyData *_polydata;
    vtkPolyDataMapper* _polymapper;
    vtkLookupTable *_lut;
    vtkRenderWindow *_renWin;
    void *_Xid;
    int _XWinHeight, _XWinWidth;
    vtkRenderer *_renderer;
    vtkCellPicker *_picker;
    vtkGtkRenderWindowInteractor *iren;
    vtkGtkCallback *_annotatePick;
    
    TessToolVTKInteractor *vtkInteractor;
    
    vtkStructuredGrid *sgrid;
    vtkPolyDataMapper *sgridMapper;
    vtkActor *sgridActor;
    
    float _hueLow, _hueHigh, _saturationLow, _saturationHigh, _valueLow, _valueHigh,
      _alphaLow, _alphaHigh;
    
    
    bool _initTessellation, _initVTK;
    
    string _scalarNameString, _scalarInfoNameString, _persistencedir;
    int _scalarIndex, _scalarInfoIndex;
    string _tmpScalarNameString, _tmpScalarInfoNameString;
    bool _scalarNameInitialized, _scalarInfoNameInitialized, 
      _scalarNameChanged, _scalarInfoNameChanged; // indices will be recomputed
    
    bool _firstDataSet;
    int _sampleNum;
  };
  
  //! Call back for picking tiles
  class vtkBIECallback : public vtkCallbackCommand {
  public:
    //! Factory
    static vtkBIECallback *New() {return new vtkBIECallback; }
    //! Execute the call back
    virtual void Execute(vtkObject *caller, unsigned long eid, void *cdata);
    
    //@{
    //! References
    vtkTextMapper *textMapper;
    vtkActor2D *textActor;
    vtkRenderWindow *renWin;
    //@}
  };
  
}
#endif
