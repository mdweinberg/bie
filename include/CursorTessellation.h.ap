// This is really -*- C++ -*-

#ifndef CursorTessellation_h
#define CursorTessellation_h

#include <string>
#include <algorithm>
#include <vector>

#include <unordered_map>
#include <boost/serialization/unordered_map.hpp>

#include "Node.h"
#include "Tessellation.h"

@include_persistence

namespace BIE {

  class RecordInputStream;

  //+ CLICLASS CursorTessellation SUPER Tessellation
  //! A pseudo tessellation to support point bayesian simulation
  //! for sequential update.
  //! 
  //! All of the points are simply a linear array and the nodes are arranged
  //! as  a singly linked list. Always uses PointTile tiles.
  //! 
  //! To fit in with the existing architecture, the tree is not populated
  //! with points here.  The full distribution does that, by reading in
  //! the points again. The full distribution will ask the tesslation
  //! for all of the "tiles" that a given point is in.  This module
  //! will return a list of those tiles (potentially one per level).
  //! The full distribution will then populate the tile.
  //! 
  //! Somewhat inefficient, I suppose, this could be improved
  class @persistent(CursorTessellation) : public @super(Tessellation)
  {
    
  public:
    //+ CLICONSTR RecordInputStream* int
    /** Construct Tessellation from a record input stream @param ris and
	@param ssize number of values.
    */
    CursorTessellation(BIE::RecordInputStream* ris, int ssize);
    
    //+ CLICONSTR RecordInputStream* int double double double double
    /** Construct Tessellation from a record input stream @param ris and
	@param ssize number of values.  Include range limits for x
	(@param minx and @param maxx) and y (@param miny and @param
	maxy) coordinates.
    */
    CursorTessellation(BIE::RecordInputStream * ris, int ssize,
		       double minx, double miny, double maxx, double maxy);
    
    //! Destructor
    virtual ~CursorTessellation();
    
    //! Returns the tile id of the tree root.
    std::vector<int> GetRootTiles();
    
    //! Returns the root node.
    std::vector<BIE::Node*> GetRootNodes();
    
    
    /** Override. Find all tiles in the tessellation tree with the given
	coordinates */
    virtual void FindAll(double x, double y, std::vector<int> &found);
    
    
  private:

    //! Multimap kludge to get around the serialization problem
    typedef std::unordered_map<twodcoords, vector<Node *>, hashCoords, compxy> MType;

    //! Internal hashmap to find topmost node associated with data item
    MType @autopersist(nodemap); 
    
    //! The tessellation tree
    vector<MonoNode*> @autopersist(root);
    
    //! Size of root array
    int @autopersist(numRoots);
    
    //! Tile factory
    Tile* @autopersist(tilefactory);
    
    //! Sample size
    int @autopersist(Nsize);
    
    //! The sampled data points used in the creation of the tree.
    vector<twodcoords> @no_autopersist(sampledata);
    
    //! Perform tessellation of the space
    void tessellate(double minx, double miny, double maxx, double maxy);
    
    //! Common initialization routine used by both constructors.
    void initialize(RecordInputStream * ris,
		    double minx, double miny, double maxx, double maxy);  
    
  private:

    void Debug();

    template<class Archive>
      void pre_serialize(Archive &ar, const unsigned int file_version) 
    {
      Debug();
    }

    @persistent_end
  };
  
} // namespace BIE

#endif

