// This is really -*- C++ -*-


#ifndef SimpleStat_h
#define SimpleStat_h

#include <BIEconfig.h>
#include <Distribution.h>

@include_persistence

namespace BIE {

//+ CLICLASS SimpleStat SUPER SampleDistribution
/**
   Simple case of a distribution: running mean and std dev with
   distribution provided by multivariate Gaussian probability
*/
class @persistent(SimpleStat) : public @super(SampleDistribution) {

public:
  //+ CLICONSTR int
  //! Constructor for $n$ dimensional data
  SimpleStat(int n);

  //! Null constructor
  SimpleStat() : normal(0) {}

  //! Destructor
  ~SimpleStat();
  
  //! Accumulate sample data
  bool AccumData(std::vector<double>&, std::vector<double>&);

  //! Required member but not used here
  bool AccumulateData(double v, BIE::RecordBuffer* datapoint) { 
    cerr << "Not implemented for a SimpleStat . .  \n";
    return false; 
  }

  //! Evaluate the probability density
  double PDF(BIE::State&);

  //! Evaluate the probability density (return log value)
  double logPDF(BIE::State&);

  //! Evaluate the cumulative probability
  double CDF(BIE::State&);

  //! Return the lower limits of the domain
  std::vector<double> lower();

  //! Return the upper limits of the domain
  std::vector<double> upper();
  
  //! Return the mean
  std::vector<double> Mean();

  //! Return the standard deviation
  std::vector<double> StdDev();

  //! Return moments of order n
  std::vector<double> Moments(int n);
  
  //! Sample the distribution
  BIE::State Sample();
  
  //! Factory
  SimpleStat* New() {
    return new SimpleStat(dimen);
  }

private:
  int @autopersist(dimen);
  int @autopersist(num);
  vector<double> @autopersist(mom1);
  vector<double> @autopersist(mom2);
  vector<double> @autopersist(mean);
  vector<double> @autopersist(var);
  vector<double> @autopersist(ret);
  Normal * @autopersist(normal);
  bool @autopersist(computed);

  void ComputeDistribution();

  @persistent_end
};

} // namespace BIE
#endif
