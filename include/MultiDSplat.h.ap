// This is really -*- C++ -*-

#ifndef MultiDSplat_h
#define MultiDSplat_h

#include <Model.h>

@include_persistence

namespace BIE {
  
  //+ CLICLASS MultiDSplat SUPER Model
  //! A simple test model based on N-dim Gaussian splats.
  class @persistent(MultiDSplat) : public @super(Model)
  {
  public:

    /**@name Global parameters */
    //{@
    /// Default dispersion
    static double @autopersist(SIG);

    //+ CLICONSTR int int
    //+ CLICONSTR int int clivectord*
    //+ CLICONSTR int int clivectord* bool
    //! Constructor
#ifndef SWIG
    MultiDSplat(int ndim, int mdim, clivectord* v=0, bool width=false);
#endif
    MultiDSplat(int ndim, int mdim, std::vector<double> v, bool width=false);

    /// Initialize state dependent part of calculation
    void Initialize(BIE::State&);
    void Initialize(std::vector<double>& w, std::vector< std::vector<double> >& p);

    //@{
    /// Integrated norm (for point likelihood)
    virtual double NormEval(double xmin, double xmax, 
			    double ymin, double ymax) 
    { return 1.0; }

    virtual double NormEval(double xmin, double xmax, 
			    double ymin, double ymax, bool serial)
    { return 1.0; }
    //@}

    /// Compute normalization of tiles
    double NormEval(double x, double y, BIE::SampleDistribution *d) 
    {
      throw NoBinnedDistributionException(__FILE__, __LINE__);
      return 0.0;
    }

    /// Main method returning binned source density
    std::vector<double> EvaluateBinned(double x, double y, BIE::BinnedDistribution *d)
    {
      throw NoBinnedDistributionException(__FILE__, __LINE__);
      return vector<double>(1);
    }

    /// Main method returning point source density
    std::vector<double> EvaluatePoint(double x, double y, BIE::PointDistribution *d);

    /// Identify labels for StateInfo
    std::vector<std::string> ParameterLabels();

  private:
    /// Number in mixture
    int @autopersist(M), @autopersist(Mcur);

    /// Number of model dimensions
    int @autopersist(Ndim), @autopersist(Ntot);

    vector<double> @autopersist(wt), @autopersist(var);
    vector < vector<double> > @autopersist(pos);
    vector<string> @autopersist(params);

    @persistent_end
  };
}

#endif
