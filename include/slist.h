
#ifndef BIE_slist_h
#define BIE_slist_h

// currently the singly linked listof choice is in the gcc extensions
#include <ext/slist>

// However, the extensions declare it in a gcc namespace
using ::__gnu_cxx::slist;


#endif//  BIE_slist_h
