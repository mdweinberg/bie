// This is really -*- C++ -*-


#ifndef Table_h
#define Table_h

#include <vector>
using namespace std;

/**
   Performs linear interpolation and 2-point derivative computation on a table
*/
class Table
{
private:
  int ntab;
  int even_xx;

  int Vlocate(double x, const vector<double>& xx);
  int Vlocate_with_guard(double x, const vector<double>& xx);
  double odd2(double, const vector<double>&, const vector<double>&, int e=0);
  double drv2(double, const vector<double>&, const vector<double>&, int e=0);

public:

  //! Use version of grid search which ensures that interpolation stays on grid
  static bool use_guard;

  //! The abscissa for the table
  vector<double> xx;
  //! The ordinate for the table
  vector<double> yy;

  //! Constructor: provide number of table entries $n$
  Table(int n);

  /** @name Access members */
  //{@
  //! For given ordinate, request abscissa
  double get_x(double y);  
  //! For given abscissa, request ordinate
  double get_y(double x);
  //! For given abscissa, request derivative
  double get_drv(double x);
  //}@


  //! Sets and exploits even abscissa intervals (significant speed up)
  void set_even(void) {even_xx=1;}
};

#endif
