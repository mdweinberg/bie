#ifndef MPICommunicationSession_h
#define MPICommunicationSession_h

#include "mpi.h"
#include "bieTags.h"
#include "MPIStreamFilter.h"

namespace BIE { 

  //! MPI Communication Session: For sending/receiving MPIStreamFilter data using via MPI.
  class MPICommunicationSession {
  public:
    //! Constructs a connected filter for buffering output.
    MPICommunicationSession(MPIStreamFilter *filter, int sessionId, MPI_Comm comm);
    //! Destructor
    ~MPICommunicationSession();
    //! Initiate a new session
    void startNewSession(int newSessionId);
    //! Start a transmission
    void initializeSendTx();
    //! End a transmission
    void finishSendTx();
    //! Send the data
    void send();
    //! Unpack the data
    static MPI_Datatype convertRecordToDatatype(RecordType *rt);
  
  private:

    int _sessionId;
    MPIStreamFilter *_filter;

    MPI_Comm _communicator;
    MPI_Datatype _sendDtype;

    // send
    int _numSends;
  };
}
#endif
