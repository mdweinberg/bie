// This is really -*- C++ -*-

#ifndef TessToolReader_h
#define TessToolReader_h

#include <string>
#include <cc++/thread.h>
#include <mpi.h>
#include <sstream>
#include "RecordStream_Ascii.h"
#include "TessToolDataStream.h"

#include <RecordType.h>

#ifdef  CCXX_NAMESPACES
using namespace std;
using namespace ost;
#endif

namespace BIE { 
  
  // CLICLASS TessToolReader 
  //! Tess Tool Reader: For setting up receiving data by Tess Tool.
  class TessToolReader: public TessToolDataStream {
  public:
    // CLICONSTR
    //! Constructs a TessTool reader object.
    TessToolReader();
    
    //! Destructor
    ~TessToolReader();
    
    //@{
    //! Allow reader to run, manipulate data, and detach
    void run();
    void SampleNext();
    void finish();
    void Detach();
    
    int readBuffer(unsigned char* val, int size);
    void setFile(char* name);
    //@}
    
  private:
    
    void Synchronize();
    void GetData();
    char* _name;
    ifstream _in;
    int _dtsize;
  };
}
#endif
