#ifndef TessToolConsole_H
#define TessToolConsole_H

#include <string>
#include <cc++/thread.h>
#include <iterator>
#include <sstream>
#include <fstream>

#include <CliOutputReceiveThread.h>

#ifdef  CCXX_NAMESPACES
 using namespace std;
 using namespace ost;
#endif

namespace BIE { 


  class TessToolController;
  //! Thread to manage console output for tesstooldriver
  class ConsoleLogThread: public Thread {
  public:
    //! Constructor
    ConsoleLogThread(CliOutputReceiveThread *rcvthrd);
    //! Destructor
    ~ConsoleLogThread();
    //! Begin the thread
    void run();
    //! Assign the controller instance
    void setController(TessToolController *controller);

  private:
   ofstream _console;
   Semaphore *_producer;
   Semaphore *_consumer;
   CliOutputReceiveThread *_clircvthrd;
   bool work;
   TessToolController *_controller;
  };

}
#endif
