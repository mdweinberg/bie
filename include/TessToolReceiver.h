// This is really -*- C++ -*-

#ifndef TessToolReceiver_h
#define TessToolReceiver_h

#include "TessToolDataStream.h"

#ifdef  CCXX_NAMESPACES
using namespace std;
using namespace ost;
#endif

namespace BIE { 
  
  // CLICLASS TessToolReceiver 
  //! Tess Tool Receiver: For setting up receiving data by Tess Tool.
  class TessToolReceiver: public TessToolDataStream {
  public:
    // CLICONSTR string
    //! Constructs a connected TessTool receiver object.
    TessToolReceiver(string name);
    
    // CLICONSTR int* char*** string
    //! Constructs a connected TessTool receiver object.
    TessToolReceiver(int *argc, char ***argv, string name);

    //! Destructor
    ~TessToolReceiver();
    
    //@{
    //! Allow receiver to run, get data, and detach
    void run();
    void SampleNext();
    void Detach();
    
    bool isConnectedToTessToolSender() { return _connectedToTessToolSender; };
    //@}
    
  private:
    
    void Synchronize();
    void GetData();
    
    bool _connectedToTessToolSender;
    int _sessionId;
    
    MPI_Comm _bieComm;
    int _bieSize;
    char *_port;
    
    int *_argc;
    char ***_argv;
    
    int _level, _step, _iter;

    int _numTMs;
    int *_tmList;
    
    MPI_Status *_status;
    MPI_Request *_request;
    int *_flag, *_completed, _numCompleted;
    
    MPI_Datatype _mpidtype;
    
    int filter_sessionId, filter_capacityInRecords;
    
    void **_receivedBuffer;
    int _lastInProgress;
    
  };
}
#endif
