// This is really -*- C++ -*-

#ifndef LinearRegression_h
#define LinearRegression_h

#include <LikelihoodFunction.h>

#include <Serializable.h>

@include_persistence

namespace BIE {

  //+ CLICLASS LinearRegression SUPER LikelihoodFunction
  //! A very simple multidimensional linear regression model for point data
  //!
  //! The data is read from a file, one line per record.  Each line
  //! has some number of fields (or columns).  The column for the 
  //! "independent variable" (x) is specified as pos1 and the column for 
  //! "dependent variable" (y) is pos2.  The first field has index 0.
  //!
  //! @ingroup likefunc
  class @persistent(LinearRegression) : public @super(LikelihoodFunction)
  {
  public:

    //+ CLICONSTR int int string
    //! Constructor 
    LinearRegression(int pos1, int pos2, std::string datafile);

    //! This is likelihood function
    double LikeProb(std::vector<double> &z, BIE::SampleDistribution* sd, 
		    double norm, BIE::Tile *t, BIE::State *s, int indx) 
    { return LocalLikelihood(s); }

    /// Label parameters
    std::vector<std::string> ParameterLabels();

  private:

    /// Data name headers
    //{@
    static const char* @no_autopersist(FIELDNAME1);
    static const char* @no_autopersist(FIELDNAME2);
    static const char* @no_autopersist(FIELDNAME3);
    static const char* @no_autopersist(PARAM_NAMES)[];
    //@}

    /// Data columns
    unsigned @ap(col1), @ap(col2);

    /// Data
    vector< vector<double> > @ap(data);

    /// Mean of data vector
    vector<double> @ap(dmean);

    /// This is likelihood function
    double LocalLikelihood(State* s);

    @persistent_end
  };

} // namespace BIE
#endif
