// -*- C++ -*-

#include <values.h>
#include <cstring>
#include <fstream>
#include <sstream>

//=============================================================================
// 2^n-tree
//=============================================================================

/**
   Enable debugging checks if true
*/
template< typename T >
bool TNTree<T>::TNTree::TNTdebug = true;

/**
   \class TNTree
   \brief Generic \f$2^n\f$ tree template

   \author Martin Weinberg
   \date March 2011

   This class template implements a \f$2^n\f$ tree.

   \param T Type of the data in the tree. This must be copyable and
   default-constructible.  It is assumed to be a container of data
   with a defined data count.

   \param size Size of \f$2^n\f$ tree, in nodes. Should be a power of
   two. For example, an \f$2^n\f$ tree with \a size = 256 will
   represent a hypercube divided into \f$256^n\f$ nodes. <b>Must be a
   power of two.</b>

   \param dim is the dimension of the data

   \param bucket is the target cell size

   \param emptyValue This is the value that will be returned when
   accessing regions of the N-D volume where no node has been
   allocated. In other words, instead of following a null node
   pointer, this value is returned. Since the \f$2^n\f$ tree root is
   initially a null pointer, the whole volume is initialized to this
   value.
 */
template< typename T >
TNTree<T>::TNTree( int size, int dim, int bucket, const T& emptyValue )
{
  // Initialize parameters
  //
  dim_        = dim;
  size_       = size;
  bucket_     = bucket;
  emptyValue_ = emptyValue;
  
  // Make sure size is power of two.
  //
  if (((size - 1) & size)) {
    std::ostringstream sout;
    sout << "level count [" << size << "] is not a power of two!";
    throw sout.str();
  }
  
  // Compute the power of two
  //
  maxLev_ = 0;
  while ((size >>= 1))  maxLev_++;
}

/**
 * Performs a deep copy of an \f$2^n\f$ tree. All branch pointers will be
 * followed recursively and new nodes will be allocated.
 *
 * \param o TNTree to be copied.
 */
template< typename T >
TNTree<T>::TNTree( const TNTree<T>& o )
{
  // Initialize parameters
  //
  size_       = o.size_;
  dim_        = o.dim_ ;
  emptyValue_ = o.emptyValue_;

  // Node initialization
  //
  if ( !o.root_ ) {
    root_ = 0;
  } else {
    switch ( o.root_->type() ) {
    case BranchNode:
      root_ = BranchPtr(new Branch ( *reinterpret_cast<Branch *>(o.root_) ) );
      break;
    case LeafNode:
      root_ = LeafPtr  (new Leaf   ( *reinterpret_cast<Leaf   *>(o.root_) ) );
      frontier[root_.get()] = root_;
      break;
    }
  }

}

/**
 * Swaps the \f$2^n\f$ tree's contents with another's. This is a cheap
 * operation as only the root pointers are swapped, not the whole
 * structure.
 */
template< typename T >
void TNTree<T>::swap( TNTree<T>& o )
{
  // This can throw.
  std::swap( emptyValue_, o.emptyValue_ );  

  // These can't.
  std::swap( root_, o.root_ );
  std::swap( size_, o.size_ );
}

/**
 * Assigns to this \f$2^n\f$ tree the contents of \f$2^n\f$ tree \a o.
 */
template< typename T >
TNTree<T>& TNTree<T>::operator= ( TNTree<T> o )
{
  swap(o);
  return *this;
}

/**
 * \return Size of \f$2^n\f$ tree, in nodes, as specified in the
 * constructor.
 */
template< typename T >
int TNTree<T>::size() const
{
  return size_;
}

/**
 * \return Value of empty nodes, as specified in the constructor.
 * \see setEmptyValue()
 */
template< typename T >
const T& TNTree<T>::emptyValue() const
{
  return emptyValue_;
}

/**
 * Sets the value of empty nodes to \a emptyValue.
 * \see setEmptyValue()
 */
template< typename T >
void TNTree<T>::setEmptyValue( const T& emptyValue )
{
  emptyValue_ = emptyValue;
}

/**
 * \return Pointer to \f$2^n\f$ tree's root node.
 */
template< typename T >
typename TNTree<T>::NodePtr& TNTree<T>::root()
{
  return root_;
}

/**
 * Const version of above.
 */
template< typename T >
const typename TNTree<T>::NodePtr TNTree<T>::root() const
{
  return root_;
}

/**
 * \return Value at index indx. If no node exists at this index, the
 * value returned by emptyValue() is returned.
 *
 */
template< typename T >
const T& TNTree<T>::at( std::vector<int>& indx ) const
{
  if (static_cast<int>(indx.size()) != dim_) {
    throw std::string("Index array rank is inconsistent with the dimension");
  }
  for (int d=0; d<dim_; d++) {
    if ( indx[d] < 0 || indx[d] >= size_ ) {
      throw std::string("Index value is beyond the grid size");
    }
  }
  
  Node* n = root_;
  std::vector<int>  tindx(dim_);

  while (1) {
    if (!n) {
      return emptyValue_;
    }
    else if ( n->type() == BranchNode ) {
      for (int d=0; d<dim_; d++) tindx[d] = !!(indx[d] & n->size_/2);
      n = reinterpret_cast<Branch*>(n)->child(tindx);
    } else {
      assert( n->type() == LeafNode );
      return reinterpret_cast<Leaf*>(n)->value();
    }
  }
}

/**
 * Synonym of at().
 */
template< typename T >
const T& TNTree<T>::operator() ( std::vector<int>& indx ) const
{
    return at(indx);
}

/**
 * \return listElem pointer at index indx from the frontier. If no
 * node exists at this index, null is returned.
 */
template< typename T >
typename TNTree<T>::listElem* TNTree<T>::find( std::vector<int>& indx )
{
  if (static_cast<int>(indx.size()) != dim_) {
    throw std::string("Index array rank is inconsistent with the dimension");
  }

  for (int d=0; d<dim_; d++) {
    if ( indx[d] < 0 || indx[d] >= size_ ) {
      throw std::string("Index value is beyond the grid size");
    }
  }
  
  NodePtr n = root_;

  while (1) {
    if (!n) {
      return 0;
    }
    else if ( n->type() == BranchNode ) {
      n = boost::static_pointer_cast<Branch>(n)->child(indx);
    } else {
      assert( n->type() == LeafNode );

      elem.pVol   = n->Volume();
      elem.pSize  = size_/n->size_;
      elem.pLevel = n->level_;
      elem.pIndex = n->index_;
      
      LeafPtr l = boost::static_pointer_cast<Leaf>(n);
      elem.pValues.erase(elem.pValues.begin(), elem.pValues.end());
      for (unsigned n=0; n<l->values().size(); n++) 
	elem.pValues.push_back(l->values(n).second);

      return &elem;
    }
  }
}


/**
 * Add a value to the tree.
 */
template< typename T >
void TNTree<T>::add( std::vector<int>& indx, const T& value )
{
  if (static_cast<int>(indx.size()) != dim_) {
    throw std::string("Index array rank is inconsistent with the dimension");
  }
  for (int d=0; d<dim_; d++) {
    if ( indx[d] < 0 || indx[d] >= size_ ) {
      throw std::string("Index value is with the grid size");
    }
  }
  
  // Need to make the root node (which is a leaf by default)
  if (root_ == 0) {
    root_ = LeafPtr(new Leaf(indx, value, size_, dim_));
    frontier[root_.get()] = root_;
  }

  NodePtr lst;
  NodePtr cur = root_;
  unsigned lev = 0;

  while (static_cast<int>(lev) <= maxLev_) {
    if (!cur) {
      // Make a leaf, add the point
      cur = LeafPtr(new Leaf(indx, value, lst));
      frontier[cur.get()] = cur;
      // Add the child to its parent
      boost::static_pointer_cast<Branch>(lst)->child(cur->sib_) = cur;
      return;
    }
    else if ( cur->type() == BranchNode ) {
      // Find the correct child
      cur->number_++;
      BranchPtr b = boost::static_pointer_cast<Branch>(cur);
      lst = cur;
      cur = b->child(indx);
      lev++;
    } 
    else if ( cur->type() == LeafNode ) {
      // This is the leaf!
      LeafPtr l = boost::static_pointer_cast<Leaf>(cur);
      if (!l) {
	std::cerr << "Error: no node" << std::endl;
      }
      if (static_cast<int>(cur->number_+1) > bucket_ && cur->level_ < maxLev_) {
	splitLeaf(cur);
      } else {
	l->add(indx, value);
	return;
      }
    } else {
      throw std::string("Index value is incompatible with the grid size");
    }
  }
}


/**
 * Make branch out of an oversized leaf
 */
template< typename T >
void TNTree<T>::splitLeaf(NodePtr &n)
{
  assert( n->type() == LeafNode );
  
  BranchPtr b, u;
  LeafPtr l = boost::static_pointer_cast<Leaf>(n);
  // Are we at the root node?
  if (l->parent_==0) {
    root_ = b = BranchPtr(new Branch(size_, dim_));
  } else {
    //
    // Replace current leaf node with a new branch
    //
    b = BranchPtr(new Branch(n->parent_, l->values(0).first));
    b->sib_ = n->sib_;
    bool ok = true;
    for (int j=0; j<n->dim_; j++) {
      if (n->index_[j] != b->index_[j]) ok = false;
    }
    if (!ok) {
      std::cerr << "CLONE KEY ERROR" << std::endl;
    }
    //
    // Reassign the branch to the parent
    //
    u = boost::static_pointer_cast<typename TNTree<T>::Branch>(n->parent_);
    if (n->sib_ != u->childID(l->values(0).first) ) {
      std::cerr << "SIBLING KEY ERROR" << std::endl;
    }
    u->child(n->sib_) = b;
  }

  current = frontier.find(n.get());
  if (current != frontier.end()) {
    frontier.erase(current);
  } else {
    std::cerr << "FAILURE: couldn't find current! (" << n.get() << ")" 
	      << std::endl;
  }

  for (unsigned i=0; i<l->values().size(); i++) {
    std::pair<std::vector<int>, T> lv = l->values(i);
    int n = b->childID(lv.first);
    if (b->child(n))
      boost::static_pointer_cast<Leaf>(b->child(n))->add(lv);
    else {
      LeafPtr p = LeafPtr(new Leaf(lv, b));
      b->child(n)       = p;
      frontier[p.get()] = p;
    }
  } 


  n = boost::static_pointer_cast<Node>(b);
}

/**
 * \Return Number of bytes a branch node occupies.
 */
template< typename T >
unsigned long TNTree<T>::branchBytes()
{
  return sizeof(Branch);
}

/**
 * \return Number of bytes a leaf node occupies.
 */
template< typename T >
unsigned long TNTree<T>::leafBytes()
{
  return sizeof(Leaf);
}

/**
 * \return Total number of nodes in the \f$2^n\f$ tree.
 */
template< typename T >
int TNTree<T>::nodes() const
{
  return nodesRecursive(root_);
}

/**
 * Helper function for nodes() method.
 */
template< typename T >
int TNTree<T>::nodesRecursive( const NodePtr node )
{
  if ( !node.get() ) {
    return 0;
  }

  int n = 1;
  if ( node->type() == BranchNode ) {
    for ( int i = 0; i < (1<<dim_); ++i ) {
      n += nodesRecursive( reinterpret_cast<const Branch*>(node)->child(i) );
    }
  }
  
  return n;
}

/**
 * \return Total number of bytes the \f$2^n\f$ tree occupies.
 *
 * \remarks Memory fragmentation may make the actual memory usage
 * significantly higher.
 */
template< typename T >
unsigned long TNTree<T>::bytes() const
{
  return bytesRecursive(root_) + sizeof(*this);
}

/**
 * Helper function for bytes() method.
 */
template< typename T >
unsigned long TNTree<T>::bytesRecursive( const NodePtr node )
{
  if ( !node.get() ) {
    return 0;
  }
  
  unsigned long b = 0;
  switch ( node->type() ) {
  case BranchNode:
    b = sizeof(Branch);
    for ( int i = 0; i < (1<<dim_); ++i ) {
      b += bytesRecursive( reinterpret_cast<const Branch*>(node)->child(i) );
    }
    break;
    
  case LeafNode:
    b = sizeof(Leaf);
    break;
    
  }
  
  return b;
}

/**
 * \return Number of nodes of at size \a size. For example, the root (if
 * allocated) is the single node of size 1. At size <i>n</i> there may be a
 * maximum of 2<sup><i>n</i></sup> nodes.
 */
template< typename T >
int TNTree<T>::nodesAtSize( int size ) const
{
  return nodesAtSizeRecursive( size, size_, root_ );
}

/**
 * Helper function for nodesAtSize() method.
 */
template< typename T >
int TNTree<T>::nodesAtSizeRecursive( int targetSize, int size, 
				     NodePtr node )
{
  if (node.get()) {
    if ( size == targetSize ) {
      return 1;
    }

    if ( node->type() == BranchNode ) {
      int sum = 0;
      std::vector<bool> tbit(dim_);
      for (int n=0; n<(1<<dim_); n++) {
	for (int d=0; d<dim_; d++) tbit[d] = n & (1<<d);
	sum += 
	  nodesAtSizeRecursive( targetSize, size/2,
				reinterpret_cast<Branch*>(node)->child(tbit) );
      }
      return sum;
    }
  }
  
  return 0;
}

/**
 * Base class constructor 
 * \remarks Root node
 */
template< typename T >
TNTree<T>::Node::Node( NodeType type, int size, int dim )
{
  type_        = type;
  dim_         = dim;
  sib_         = 0;
  level_       = 0;
  size_        = size;
  minv_        = MAXDOUBLE; 
  leaf_        = leafValue(std::vector<int>(), INT_MAX);
  index_       = std::vector<int>(dim_, 0);
  number_      = 0;
  complete_    = false;
  complete_vol = 0.0;
}


/**
 * Base class constructor 
 * \remarks Assigns the parent node for walking the tree
 */
template< typename T >
TNTree<T>::Node::Node( NodeType type, NodePtr parent,
		       const std::vector<int>& i)
{
  type_        = type;
  parent_      = parent;
  dim_         = parent_->dim_;
  level_       = parent_->level_ + 1;
  size_        = parent_->size_/2;
  index_       = parent_->index_;
  number_      = parent_->number_;
  minv_        = MAXDOUBLE; 
  leaf_        = leafValue(std::vector<int>(), INT_MAX);
  sib_         = parent->childID(i);
  complete_    = false;
  complete_vol = 0.0;

  // Compute new index
  for (int n=0; n<dim_; n++) {
    index_[n] <<= 1;
    index_[n] |= !!(i[n] & size_);
  }
}


/**
 * \return The nodetype for polymorphic manipulation
 * \remarks Assigns the parent node for walking the tree
 */
template< typename T >
typename TNTree<T>::NodeType TNTree<T>::Node::type() const
{
  return type_;
}

/**
 * Root node branch node constructor 
 * \remarks Assigns the parent node for walking the tree
 */
template< typename T >
TNTree<T>::Branch::Branch(int size, int dim)
  : Node(BranchNode, size, dim)
{
  children = std::vector<NodePtr>(1<<Node::dim_);
}

/**
 * Branch node constructor 
 * \remarks Assigns the parent node for walking the tree
 */
template< typename T >
TNTree<T>::Branch::Branch(NodePtr p, std::vector<int>& i) 
  : Node(BranchNode, p, i)
    
{
  unsigned ntot = 1<<Node::dim_;
  children = std::vector<NodePtr>(ntot);
}

/**
 * Branch node copy constructor 
 */
template< typename T >
TNTree<T>::Branch::Branch( const Branch& b ) 
  : Node(BranchNode, b.Size(), b.Dim())
{
  TNTree<T>::Node::index_  = b.index_;
  TNTree<T>::Node::number_ = b.number_;
  TNTree<T>::Node::minv_   = b.minv_;
  TNTree<T>::Node::leaf_   = b.leaf_;
  TNTree<T>::Node::sib_    = b->sib_;

  for ( int i = 0; i < (1<<dim_); ++i ) {
    if ( b.child(i) ) {
      switch ( b.child(i)->type() ) {
      case BranchNode:
	child(i) = new Branch(   *reinterpret_cast<const Branch   *>(b.child(i)) );
	break;
      case LeafNode:
	child(i) = new Leaf(     *reinterpret_cast<const Leaf     *>(b.child(i)) );
	frontier[child(i).get()] = child(i);
	break;
      }
    }
    else {
      child(i) = 0;
    }
  }
}


/**
 * Node accessor (const version)
 * \param bits is the binary bit string defining the child
 */
template< typename T >
const typename TNTree<T>::NodePtr
TNTree<T>::Branch::child( std::vector<int>& i) const
{
  return children[TNTree<T>::Node::childID(i)];
}

/**
 * Node accessor (mutable version)
 * \param bits is the binary bit string defining the child
 */
template< typename T >
typename TNTree<T>::NodePtr&
TNTree<T>::Branch::child( std::vector<int>& i )
{
  return children[TNTree<T>::Node::childID(i)];
}

template< typename T >
const typename TNTree<T>::NodePtr TNTree<T>::Branch::child( int index ) const
{
  if ( index < 0 || index >= (1<<dim_) ) {
    throw std::string("Index is inconsistent with the dimension");
  }

  return children[index];
}

template< typename T >
typename TNTree<T>::NodePtr& TNTree<T>::Branch::child( int index )
{
  if ( index < 0 || index >= (1<<Node::dim_) ) {
    throw std::string("Index is inconsistent with the dimension");
  }

  return children[index];
}


template< typename T >
TNTree<T>::Leaf::Leaf(const std::vector<int>&i, const T& v, int size, int dim)
  : Node(LeafNode, size, dim)
{
  TNTree<T>::Node::number_ = 1;
  values_ = std::vector<leafValue>(1, leafValue(i, v));
}

template< typename T >
TNTree<T>::Leaf::Leaf(const std::vector<int>&i, const T& v, NodePtr p)
  : Node(LeafNode, p, i)
{
  TNTree<T>::Node::number_ = 1;
  values_ = std::vector<leafValue>(1, leafValue(i, v));
}

template< typename T >
TNTree<T>::Leaf::Leaf(const leafValue& lv, NodePtr p)
  : Node(LeafNode, p, lv.first)
{
  TNTree<T>::Node::number_   = 1;
  TNTree<T>::Node::complete_ = false;
  values_ = std::vector<leafValue>(1, lv);
}

template< typename T >
TNTree<T>::Leaf::Leaf(const leafValue& lv, NodePtr p, double vol)
  : Node(LeafNode, p, lv.first)
{
  TNTree<T>::Node::number_      = 1;
  TNTree<T>::Node::complete_vol = vol;
  TNTree<T>::Node::complete_    = true;
  values_ = std::vector<leafValue>(1, lv);
}

template< typename T >
void TNTree<T>::Leaf::add(const std::vector<int>& i, const T& v)
{
  TNTree<T>::Node::number_++;
  values_.push_back(leafValue(i, v));
}

template< typename T >
void TNTree<T>::Leaf::add(const leafValue& lv)
{
  TNTree<T>::Node::number_++;
  values_.push_back(lv);
}

template< typename T >
std::vector< std::pair<std::vector<int>, T> >& TNTree<T>::Leaf::values()
{
  return values_;
}

template< typename T >
std::pair<std::vector<int>, T>& TNTree<T>::Leaf::values(int i)
{
  return values_[i];
}

/** Recursively decend the tree and assign parents.  This call makes
    the frontier list and sets it to the first leaf.  It must be
    called before the Next() member.

    \see Next()
 */
template< typename T >
void TNTree<T>::Reset()
{
  current = frontier.begin();
}


/** \return Return the next pointer/index pair on the frontier.  If
    the frontier is exhausted, we return a null pointer and empty
    index.

    \see Reset()
 */
template< typename T >
typename TNTree<T>::listElem* TNTree<T>::Next()
{
  if (current == frontier.end()) return 0x0;

  NodePtr cur = current->second;

  assert ( cur->type() == LeafNode );

  if (cur->parent_)
    elem.pCount = cur->parent_->number_;
  else
    elem.pCount = cur->number_;
  
  elem.pVol   = cur->Volume();
  elem.pSize  = size_/cur->size_;
  elem.pLevel = cur->level_;
  elem.pIndex = cur->index_;
  elem.parent = cur->parent_;
  
  LeafPtr l = boost::static_pointer_cast<Leaf>(cur);
  elem.pValues.erase(elem.pValues.begin(), elem.pValues.end());
  for (unsigned n=0; n<l->values().size(); n++) 
    elem.pValues.push_back(l->values(n).second);

  current++;
  return &elem;
}

/**
 * Sanity check
 */
template< typename T >
void TNTree<T>::sanityCheck(NodePtr node, 
			    unsigned& branches, unsigned& leaves)
{
  if (node == 0x0) return;

  unsigned num = 1<<dim_;
  Branch *b;

  switch ( node->type() ) {
  case BranchNode:
    branches++;
    b = reinterpret_cast<Branch*>(node);
    for (unsigned i=0; i<num; i++) sanityCheck(b->child(i), branches, leaves);
    break;
  case LeafNode:
    leaves++;
    break;
  default:
    std::cerr << "Unknown node!" << std::endl;
  }
}

/**
 * Count diagnostics 
 * \param Z should be 0 for intial sample, 1 for final sample
 * \remarks Data will be written to stdout for Z=1
 */
template< typename T >
void TNTree<T>::nodeDist(int Z)
{
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if (myid) return;

  Reset();

  typedef std::pair<int, int> counter;
  static  std::map <int, counter> histo;
  if (Z==0) histo.erase(histo.begin(), histo.end());

  Node** p;
  while (p=Next()) {
    int length = (*p)->values().size();
    if (histo.find(length) == histo.end()) {
      if (Z==0)
	histo[length] = counter(1, 0);
      else
	histo[length] = counter(0, 1);
    } else {
      counter cnt = histo[length];
      if (Z==0)	cnt.first++;
      else      cnt.second++;
      histo[length] = cnt;
    }
  }

  if (Z==1) {
				// Open output stream for append
    std::ofstream out("tntree_dist.debug", std::ios::out | std::ios::app);

				// Separator
    out << std::endl 
	<< std::setfill('+') << std::setw(68) << '+'
	<< std::endl << std::setfill(' ');
				// Header
    out << std::right 
	<< std::setw(12) << "Number |"
	<< std::setw(12) << "# init |"
	<< std::setw(12) << "# final |"
	<< std::setw(12) << "Delta # |"
	<< std::setw(18) << "Percent change |"
	<< std::endl;
				// Separator
    out << std::setfill('+') << std::setw(68) << '+'
	<< std::endl << std::setfill(' ');

    
    for (std::map<int, counter>::iterator 
	   it=histo.begin(); it!=histo.end(); it++) {
      
      double frac = 100.0;
      if (it->second.first) {
	frac = 100.0*(it->second.second - it->second.first)/it->second.first;
      }
      
      out << std::right 
	  << std::setw(12) << it->first
	  << std::setw(12) << it->second.first
	  << std::setw(12) << it->second.second
	  << std::setw(12) << it->second.second - it->second.first
	  << std::setw(18) << std::setprecision(2) << std::fixed << frac
	  << std::endl;
    }
				// Separator
    out << std::setfill('+') << std::setw(68) << '+'
	<< std::endl << std::setfill(' ');
  }
}

template< typename T >
void TNTree<T>::getMinValue
(NodePtr n, std::vector< std::vector<double> >& data, unsigned dindx)
{
  //------------------------------------------------------------
  // Is this node a branch or a leaf?
  //------------------------------------------------------------
  if ( n->type() == BranchNode ) {
    
    int ntot = 1<<dim_;
    
    BranchPtr b = boost::static_pointer_cast<Branch>(n);
				// Recursive call until leaves are
				// reached . . .
    for ( int i = 0; i < ntot; ++i ) {
      if ( b->child(i) ) {
	getMinValue(b->child(i), data, dindx);
	if (b->minv_ > b->child(i)->minv_) {
	  b->minv_ = b->child(i)->minv_;
	  b->leaf_ = b->child(i)->leaf_;
	}
      }
    }
      
  } else {
      
    LeafPtr lf = boost::static_pointer_cast<Leaf>(n);

    std::vector<leafValue> vals = lf->values();

    double   minV  = MAXDOUBLE;
    unsigned minI  = MAXINT;
    for ( size_t k = 0; k<vals.size(); k++ ) {
      if (minV > data[vals[k].second][dindx]) {
	minV = data[vals[k].second][dindx];
	minI = vals[k].second;
      }
    }

    lf->minv_ = minV;
    lf->leaf_ = leafValue(lf->index_, minI);
  }
}


template< typename T >
double TNTree<T>::TrimFrontier(double fraction)
{
  typename std::multimap<double, Node*> sort_frontier;
  unsigned long count = 0, sofar = 0;
  for (current=frontier.begin(); current!=frontier.end(); current++) {
    sort_frontier.insert(std::pair<double, Node*>(current->first->minv_, 
						  current->first));
    count += current->first->number_;
  }

  
  typename std::multimap<double, Node* >::iterator it;
  unsigned long target = count - 
    static_cast<unsigned long>(floor(count*fraction));
  for (it=sort_frontier.begin(); it!=sort_frontier.end(); it++) {
    sofar += it->second->number_;
    if (sofar >= target) break;
  }

  typename std::map<Node*, NodePtr> new_frontier;
  
  for (; it!=sort_frontier.end(); it++) {
    new_frontier.insert(std::pair<Node*, NodePtr>(it->second, 
						  frontier[it->second]));
  }

  frontier = new_frontier;

  return static_cast<double>(count-sofar)/count;
}

