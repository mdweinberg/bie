// This is really -*- C++ -*-

#ifndef MultiNWishartDist_h
#define MultiNWishartDist_h

#include <cmath>
#include <cfloat>

#include <VectorM.h>
#include <Distribution.h>
#include <Normal.h>
#include <Gamma.h>

@include_persistence

namespace BIE
{
  
  //! \addtogroup distribution
  //! @{
  
  //+ CLICLASS MultiNWishartDist SUPER Distribution
  /**
     Multivariate normal with a hierarchical Wishart distribution prior
     @param n is the Wishart number of degrees of freedom
     @param D is the diagonal of the scale matrix
     @param copies is the number independent samples in each full parameter vector
     
     Wishart distribution with \f$n\f$ degrees of freedom
     Parameters:
     <ul>
     <li> \f$n > p-1\f$ degrees of freedom (real)
     <li> \f$\mathbf{V} > 0\f$ is the \f$p\times p\f$ positive definite scale matrix
     <li> support: \f$\mathbf{W}\f$ \f$p\times p\f$ positive definite matrices
     <li> pdf: 
     \f[ 
     p(\mathbf{W}|\mathbf{V},n) = \frac{\left|\mathbf{W}\right|^{(n-p-1)/2}}{2^{np/2}\left|\mathbf{V}\right|^{n/2}\Gamma_p(\frac{n}{2})}
     \exp\left(-\frac{1}{2}\mbox{Tr}(\mathbf{V}^{-1}\mathbf{W})\right)
     \f]
     where \f$\Gamma_p(\cdot)\f$ is the multivariate gamma function defined as
     \f[
     \Gamma_p(n/2)= \pi^{p(p-1)/4}\Pi_{j=1}^p \Gamma\left[ n/2+(1-j)/2\right]. 
     \f]
     <li> mean: \f$n \mathbf{V}\f$
     <li> mode: \f$(n-p-1)\mathbf{V}\f$ for \f$n \geq p+1\f$
     <li> variance: \f$ n(v_{ij}^2+v_{ii}v_{jj})\f$
     </ul>
     
     Sampling is based on the Bartlett decomposition of a matrix
     \f$\mathbf{W}\f$ from a p-variate Wishart distribution with scale matrix
     \f$\mathbf{V}\f$ and \f$n\f$ degrees of freedom is the factorization:
     \f[
     {\textbf W} = {\textbf L}{\textbf A}{\textbf A}^T{\textbf L}^T
     \f]
     where \f$\mathbf{L}\f$ is the Cholesky decomposition of \f$\mathbf{V}\f$, 
     and  
     \f[
     \mathbf{A} =
     \pmatrix{
     \sqrt{c_1} & 0 & 0 & \cdots & 0\cr
     n_{21} & \sqrt{c_2} &0 & \cdots& 0 \cr
     n_{31} & n_{32} & \sqrt{c_3} & \cdots & 0\cr
     \vdots & \vdots & \vdots &\ddots & \vdots \cr
     n_{p1} & n_{p2} & n_{p3} &\cdots & \sqrt{c_p}\cr
     }
     \f]
     where \f$c_i \sim \chi^2_{n-i+1}\f$ and \f$n_{ij} \sim N(0,1)\f$.
  */
  class @persistent(MultiNWishartDist) : public @super(Distribution) {
    
  public:
    
    //+ CLICONSTR double clivectord* int
    //@{
    //! Constructor with shape, diagonal elements, and replications
#ifndef SWIG
    MultiNWishartDist(double s, clivectord* d, int copies);
#endif
    MultiNWishartDist(double s, std::vector<double> d, int copies);
    //@}
    
    //! Destructor
    virtual ~MultiNWishartDist() {}
    
    //! Object factor (clone)
    BIE::Distribution* New();
    
    //! Differential distribution function P(x)
    virtual double PDF(BIE::State& x);
    
    //! Log of differential distribution function P(x)
    virtual double logPDF(BIE::State& x);
    
    //! Cumulative distribution function P(>x)
    virtual double CDF(BIE::State& x);
    
    //! Lower bound on distribution
    virtual std::vector<double> lower(void);
    
    //! Upper bound on distribution
    virtual std::vector<double> upper(void);
    
    //! Return mean of distribution
    virtual std::vector<double> Mean(void);
    
    //! Return standard deviation of distribution (mulitvariate)
    virtual std::vector<double> StdDev(void);
    
    //! Return specifided momemnt of distribution (mulitvariate)
    virtual std::vector<double> Moments(int i);
    
    //! Return random variate from distribution
    virtual BIE::State Sample(void);
    
  private:
    void realize();
    
    int @autopersist(M);
    unsigned @autopersist(N);
    double @autopersist(shape), @autopersist(det);
    vector<double> @autopersist(diag);
    vector<GammaPtr> @autopersist(gamma);
    NormalPtr @autopersist(normal);
    MatrixM @ap(A), @ap(C), @ap(W), @ap(eigen);
    VectorM @autopersist(evals);
    
    @persistent_end
  };
  
  //! @}
  
} // namespace BIE
#endif

