/**
   This header defines a pointer type to avoid 32bit/64bit
   pointer code in every routine that needs it
 */

#ifndef _pointer_h
#define _pointer_h


#if defined (__alpha__) \
||defined(__ia64__)     \
||defined(__ppc64__)    \
||defined(__s390x__)    \
||defined(__x86_64__)

typedef uint64 BIEpointer;

#else

typedef uint32 BIEpointer;

#endif

#endif
