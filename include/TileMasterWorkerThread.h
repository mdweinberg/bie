
#ifndef TileMasterWorkerThread_h
#define TileMasterWorkerThread_h

#include <BIEmpi.h>
#include <cc++/thread.h>
using namespace ost;

//namespace BIE{
//  //
//}

//#include "MPITemperedSimulation.h"


namespace BIE{

  class LikelihoodComputationMPITP;
  class EvaluationRequest;

  /**
     A helper class that holds a worker thread that runs on the tile master,
     so the tile master can help do the work when it's not busy doing something
     else.
  */
  class TileMasterWorkerThread : public Thread {

   public:

    //@{
    //! Constructors
    TileMasterWorkerThread();
    TileMasterWorkerThread(LikelihoodComputationMPITP* simulation);
    //@}
    //! Destructor
    virtual ~TileMasterWorkerThread();

    //! A new simulation
    void reInitialize(LikelihoodComputationMPITP* simulation);

    //! Send a request
    void Send(EvaluationRequest *request);

    /// called to permanently stop and kill thread
    void stop();

    /// run the thread Override of thread framework method
    virtual void run ();

    /// called to pause thread and set it's state to that just like construction
    void resetPause();

    /// called to resume a thread that has been resetPause'd
    void unPause();

    /// implement Final method from framework to self-delete thread when it completes
    virtual void final();

  private:
    LikelihoodComputationMPITP *simulation;
    EvaluationRequest *request;
    Semaphore workAvailable;
    bool stopRequested;
    bool resetPauseRequested;
    Semaphore paused;
    Semaphore resume;
  };

} // end namespace

#endif
