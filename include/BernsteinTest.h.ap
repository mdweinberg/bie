// This is really -*- C++ -*-

#ifndef BernsteinTest_h
#define BernsteinTest_h

#include <LikelihoodFunction.h>
#include <BernsteinPoly.h>

@include_persistence

namespace BIE {
  
  //+ CLICLASS BernsteinTest SUPER LikelihoodFunction

  //! A "user-defined" likelihood function for testing the Bernstein
  //! polynomial-basis based non-parametric function method
  //!
  //! The "data" is either a simple quartic kernel on the unit interval,
  //! \f[
  //! f(x) = 30 x^2(1-x)^2,
  //! \f]
  //! which is centered at 1/2 and should be well fit by a Bernstein basis.
  //! Or a power law with a user-specified index \f$\beta>-1\f$.
  //!
  //! @ingroup likefunc
  class @persistent(BernsteinTest) : public @super(LikelihoodFunction) 
  {
    
  private:

    //! The Bernstein basis instance
    BernsteinPoly       @ap(bern);

    //! Order
    size_t              @ap(order);

    //! Initialize Bernstein on first likelihood call (to get dimensionality)
    bool                @ap(first);

    //! The generated data
    std::vector<double> @ap(data);

    //! Lower and upper limits
    double              @ap(xmin), @ap(xmax);

    //! Number of data points
    size_t              @ap(npts);
    
    //! Data densities
    enum Density {
      quartic,		/*!< a quartic density distribution   */
      powerlaw,		/*!< a power-law density distribution */
      twobump		/*!< multimodal made from two normals */
    };

    //! Density type
    Density             @ap(type);

    //! Power law index
    double              @ap(beta);

    //! Centers, widths, weights, norms
    std::vector< std::vector<double> > @ap(c);

    //! Make the quartic kernel distributed data
    void makeData();

    //@{
    //! Kernel fct
    double PDF(double x);
    double CDF(double x);
    //@}


    //! Initialization member
    void initialize(State* s);

  public:

    //+ CLICONSTR 
    //+ CLICONSTR int
    //@{
    //! This simple verson assumes that the coefficient vector from
    //! the #State describes the \f$n+1\f$ coefficients of the
    //! Bernstein basis, and uses the quartic data with generated
    //! \f$N\f$ points (\f$N=40\f$ by default.
    //! @param N is the number of generated data points
    BernsteinTest();
    BernsteinTest(int N);
    //@}

    //+ CLICONSTR double
    //+ CLICONSTR double int
    //! This version uses a power law with index \f$beta\f$ to generate
    //! data in the unit interval.
    //! @param beta is the power-law index
    //! @param N is the number of generated data points
    BernsteinTest(double b, int N=40);

    //+ CLICONSTR int double double
    //+ CLICONSTR int double double double double
    //+ CLICONSTR int double double double double double double 
    //! This version uses a two-component Gaussian with given center,
    //! width and weight to generate data in the unit interval.
    //! @param N is the number of generated data points
    //! @param c1 is the center of Component 1
    //! @param c2 is the center of Component 2
    //! @param s1 is the width of Component 1
    //! @param s2 is the width of Component 2
    //! @param w1 is the weight of Component 1
    //! @param w2 is the weight of Component 2
    BernsteinTest(int N, double c1, double c2, double s1=0.15, double s2=0.15, 
		  double w1=0.7, double w2=0.3);

    //! Destructor
    virtual ~BernsteinTest() {}
    
    //! This is likelihood function
    double LikeProb(std::vector<double> &z, BIE::SampleDistribution* sd, 
		    double norm, BIE::Tile *t, BIE::State *s, int indx);
    
    //! The joint cumulative function
    double CumuProb(std::vector<double> &z, BIE::SampleDistribution* sd, 
		    double norm, BIE::Tile *t, BIE::State *s, int indx, Fct1dPtr f);

    //! Label output
    const std::string ParameterDescription(int i);

    @persistent_end
  };

} //namespace BIE

#endif
