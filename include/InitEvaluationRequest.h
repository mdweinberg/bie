#ifndef InitEvaluationRequest_h
#define InitEvaluationRequest_h

#include "EvaluationRequest.h"

#include <BIEMutex.h>
#include <cc++/thread.h>

using namespace ost;

#undef INIT_EVALUATION_REQUEST_FINE_TRACE

namespace BIE{

  // The first "request" for processing sent from the tile master to
  // each tile worker. The requests are not queued on the tile master;
  // rather they are explicitly sent by the handler thread and jammed into
  // the pendingrequests queue. Forcing an initial synchronization like
  // this lets us know  that the workers are alive and well and waiting
  // for requests; also the pendingrequest array is initialized.


  //! the pendingrequests array is initialized properly.
  class InitEvaluationRequest : public EvaluationRequest {
  public:
    //! The constructor
    InitEvaluationRequest(Semaphore *numberSlavesReady)
      {
         this->numberSlavesReady = numberSlavesReady;

         //aah todo Pass id in so we don't have to get it from MPI.
	 //         ++MPIMutex;
	 //            MPI_Comm_rank(MPI_COMM_WORLD, &myId); //save id for debugging
	 //         --MPIMutex;
         this->myId = myid;  //GLOBAL FROM BIEmpi.cc
      }

    virtual ~InitEvaluationRequest(){};

    virtual void Send(int destination, vector<MPI_Comm>*comms){};
     

    virtual void Recv(MPI_Status *probeStatus)
      {
         int tag  = probeStatus->MPI_TAG;
         int sender = probeStatus->MPI_SOURCE;
         MPI_Status status;
         int returnSize;
         double z;

         if (tag !=19){
           cerr << "InitEvaluationRequst.Recv Tag mismatch in MPI probe. Expected 19 recieved " << tag << "\n";
	   cerr << "Process: "<< myId << " Sender = " << sender << "\n";
	   exit(1);
	 }

         if (received){
 	   cerr << "InitEvaluationRequest.Recv ERROR. Attempt to receive two results associated with the samerequest.\n";
	   cerr << "Process: "<< myId << " Sender = " << sender << "\n";

          exit(1);
         }         
 
         ++MPIMutex;
            MPI_Get_count(probeStatus, MPI_INT, &returnSize);
         --MPIMutex;

         if (returnSize != 1){
            cerr << "InitEvaluationRequest.Recv Error - returned value wrong size from modelEvaluation.  Expecting " <<
  	     " 1 got " << returnSize << "\n";
	    cerr << "Process: "<< myId << " Sender = " << sender << "\n";

            exit(1);
         }


         ++MPIMutex;
           MPI_Recv(&z, 1, MPI_INT, sender, 19, MPI_COMM_WORLD, &status); //shouldn't block
         --MPIMutex;

         #ifdef INIT_EVALUATION_REQUEST_FINE_TRACE
            cout << "InitEvaluationRequest.Received   ";
            cout << "Process: "<< myId << " Sender = " << sender << "\n";
         #endif

         Process();
      }

    //! The computation is ready
    void Process()
    {
      // mark message as received, and sender as available
      received = true;
      // cast away volatility so we can call non-volatile methods
      const_cast<Semaphore*>(numberSlavesReady)->post();
    }

  private:    
    int myId; // for debugging
    volatile Semaphore *numberSlavesReady;
  };
}
#endif // InitEvaluationRequest_h
