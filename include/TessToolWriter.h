// This is really -*- C++ -*-

#ifndef TessToolWriter_h
#define TessToolWriter_h

#include <string>
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include "RecordStream_Ascii.h"
#include "MPICommunicationSession.h"
#include "bieTags.h"

namespace BIE { 
  
  //! Tess Tool Writer: For setting up receiving data by Tess Tool.
  class TessToolWriter {
  public:

    //! Constructs a TessTool writer object.
    TessToolWriter(const std::string ofile, const MPI_Comm bieComm);

    //! Destructor
    ~TessToolWriter();
    
    //! Get the data and write the file (returns true if successful)
    bool CacheData();

  private:
    
    MPI_Comm _bieComm;
    
    bool _first;
    
    static int _instance;
    ofstream wwerr;

    int _level, _step, _iter;

    int _numTMs;
    int *_tmList;
    int _bufSizeInWords;
    int _numRecordsInBuffer;
    
    MPI_Status *_status;
    MPI_Request *_request;
    int *_flag, *_completed, _numCompleted;
    
    MPI_Datatype _mpidtype;
    RecordType *_rtype;
    int _typeSizeInBytes;
    char *_scratchDtypeBuffer;
    int _scratchBufferSizeInWords;
    
    int filter_sessionId, filter_capacityInRecords, filter_recordSize;
    
    void **_receivedBuffer;
    void *_currentBuffer;
    
    std::string _ofile;
    
    // scratch
    //
    std::string _str;
    istringstream *_iss;
    RecordInputStream_Ascii * _ris;

  };
}
#endif
