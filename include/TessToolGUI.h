// This is really -*- C++ -*-

#ifndef TessToolGUI_h
#define TessToolGUI_h

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cc++/thread.h>

#include <gtk/gtk.h>

#include "interface.h"
#include "support.h"

using namespace std;
using namespace ost;

namespace BIE { 
  
  class TessToolController;
  class TessToolGUI;
  
  
  //! Main thread for GTK TessTool GUI
  class TessToolGUI: public Thread {
  public:
    //! Constructor
    TessToolGUI(int argc, char *argv[], TessToolController *controller);
    //! Destructor
    ~TessToolGUI();
    //! Begin the thread
    void run();
    //! Request GUI update
    void updateBIEOutputView(char *txt);
    
    //! The GUI instance
    static TessToolGUI* _gui;
    //! Send buffer
    static char *_buffer;
    //! The buffer size
    static int _bufferSize;
    
    //@{
    /// states for the gui usage protocol
    bool scriptSent, ttRxInitialized, ttVTKInitialized;
    bool scalarSelected, scalarInfoSelected;
    bool dataFetchRequested, dataVisualized, dataStreamStarted;
    //@}
    
  private:
    int _argc;
    char **_argv;
    bool _work;
    
    TessToolController *_controller;
    
  };
  
}

namespace TESSTOOL {
  
  void update_bie(char *txt);
  void setHueLow(float l);
  void setHueHigh(float l);
  void setSaturationLow(float l);
  void setSaturationHigh(float l);
  void setValueLow(float l);
  void setValueHigh(float l);
  void setAlphaLow(float l);
  void setAlphaHigh(float l);
  void setVTKWindow(void *xid, int width, int height);
  void initVTK();
  void *getVTKRenderWindow();
  void *getVTKRenderer();
  void *getVTKCellPicker();
  void setScalarMean();
  void setScalarVariance();
  void setScalarMax();
  void setScalarMin();
  void setScalarMaxDifferenceModelData();
  void setScalarName(char *n);
  void setScalarInfoName(char *n);
  void setWorkingDirName(char *fn);
  void setDataFileName(char *fn);
  void setScriptFileName(char *fn);
  char *getScriptFileName();
  void sendScriptFile(char* filename);
  char *sendCommand(char* cmd, int* replySize, int *replyLines);
  void startCLICommandLoop();
  void startVTK();
  void updateVTK();
  void processData();
  void sampleNext();
  void visualize();
  void quitTessTool();
}

#endif
