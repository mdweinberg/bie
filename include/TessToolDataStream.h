// This is really -*- C++ -*-

#ifndef TessToolDataStream_h
#define TessToolDataStream_h

#include <string>
#include <cc++/thread.h>
#include <mpi.h>
#include <sstream>
#include "RecordStream_Ascii.h"

#include <RecordType.h>

#ifdef  CCXX_NAMESPACES
using namespace std;
using namespace ost;
#endif

namespace BIE { 
  
  // CLICLASS TessToolDataStream 
  //! Tess Tool Data Stream: Base class for receiving data by Tess Tool.
  class TessToolDataStream: public Thread 
  {
  public:
    
    //! Constructor
    TessToolDataStream();

    //! Begin the thread
    virtual void run() = 0;
    
    //@{
    //! Data stream members
    RecordType *GetRecordType() { return _rtype; }
    int readBuffer(unsigned char* val, int size);
    Semaphore *getSemaphore() { return _tsem; }
    void setConsumerSemaphore(Semaphore *consumer) { _consumerSem = consumer; }

    void SynchronizeCommand();
    virtual bool isConnectedToTessToolSender() { return false; }
    //@}
    
  protected:
    
    //@{
    //! Data stream members and internal variables

    virtual void GetData() = 0;
    virtual void Synchronize() = 0;
    void notifyConsumer();
    bool parse_command();
    
    char *_name;
    bool _first;
    
    int _bufSizeInWords;
    int _numRecordsInBuffer;
    
    RecordType *_rtype;
    int _typeSizeInBytes;
    char *_scratchDtypeBuffer;
    int _scratchBufferSizeInWords;
    
    int filter_recordSize;
    
    void *_currentBuffer;
    void *_currentBufferPointer;
    void *_currentBufferEnd;
    
    // scratch
    string _str;
    istringstream *_iss;
    RecordInputStream_Ascii * _ris;
    
    Semaphore *_tsem;
    Semaphore *_consumerSem;
    
    char _tcmd[1024];

    //@}
  };
}
#endif
