// This is really -*- C++ -*-

#ifndef GRanalyze_h
#define GRanalyze_h

#include <mpi.h>

#include <deque>

#include <Ensemble.h>

#include <OutlierMask.h>

extern double inv_student_t_1sided(double alpha, double df);

@include_persistence

namespace BIE {
  
  //+ CLICLASS GRanalyze
  //! Analyze MC convergence usin the multiple chain analyses as desribed
  //! in Gelman and Rubin (1992)
  //!
  //! \par Intro and brief description
  //! This implements the convergence metric developed by Gelman and
  //! Rubin for measuring convergence of multiple chains.  The measure
  //! uses both the within- and between-chain variance estimate the
  //! total variance of the pooled chain variance.  From this they
  //! compute the ratio of the pooled variance to in-chain variance as
  //! an estimate of the reduction in variance that might be expected
  //! from a convergenced simulation.  They call this the "scale
  //! reduction".  A score of 1 indicates convergence.  Since a score
  //! of 1 is difficult to achieve, Gelman and Rubin recommend using
  //! 1.2 or 1.1 to declare convergence.  We assume 1.2 by default but
  //! this value may be changed using the #setRhatMax method.
  //!
  //! \par Outlier detection
  //! We use Grubbs' test (Grubbs 1969 and Stefansky 1972) to identify
  //! outliers in a univariate data set. It assumes that the
  //! underlying distribution is normal and this is appropriate for
  //! converged MCMC output.  Grubbs' test detects one outlier at a
  //! time, and successive outliers are iteratively removed from the
  //! dataset.
  //!
  //! \par Grubbs' test
  //! The Grubbs' test defined the following: 
  //! \f[ 
  //! G = {\max|x_i - {\bar x}|\over \sigma_s} 
  //! \f] 
  //! where \f$x_i\f$ are the \f$i=1,\ldots,N\f$ chain samples for a
  //! particular parameter, \f${\bar x}\f$ is the sample mean and
  //! \f$\sigma_s\f$ is the root of sample variance (standard
  //! deviation), respectively.  In other words, the Grubbs test
  //! statistic is the largest absolute deviation from the sample mean
  //! in units of the sample standard deviation.
  //! Let the confidence level be \f$\alpha\f$.  Then the hypothesis of no
  //! outliers is rejected if 
  //! \f[ G > {N-1\over\sqrt{N}}
  //! \sqrt{t^2_{\alpha/(2N),N-2}\over N - 2 + t^2_{\alpha/(2N),N-2}}
  //! \f]
  //! where \f$t_{\alpha/(2N),N-2}\f$ denoting the critical value of the
  //! Student t-distribution with \f$N-2\f$ degrees of freedom and a
  //! significance level of \f$\alpha/(2N)\f$.
  //!
  //! \par How to enable Grubbs' test
  //! To diagnose aberrant chains, Grubbs' test may be used by setting
  //! the outlier confidence \f$\alpha\f$ using the #setAlpha method.
  //! A value \f$\alpha=0\f$ implies no outlier testing.  Grubbs' test
  //! will only be applied after a specified number of steps have been
  //! computed; this is set by the #setNoutlier method.  The default
  //! value is 500.
  //!
  //! \par Other parameters
  //! The remaining parameters are similar to the SubsampleConverge
  //! routine.  In particular #setNgood chooses the number of states
  //! to compute after convergence is obtained and and #setNskip
  //! chooses the number of steps to skip between convergence checks.
  //!
  //! \ingroup utilities
  //!
  class @persistent_root(GRanalyze)
    {
    private:

      // Verbose printing for debugging in ML bootstrap routine
      //
      bool @autopersist(dbg_verbose);

      // Verbose output
      //
      bool @autopersist(verbose);

      // Interval reports
      bool @autopersist(report);

      // Convenient data types
      typedef std::vector< std::vector<double> > chainType;
      typedef std::deque<chainType> mcmcType;
      typedef std::pair<int, int> Ielem;
      typedef std::pair<double, Ielem> Pmap_value;
      typedef std::vector<Pmap_value> Pmap;
      typedef Pmap::iterator iterType;
      typedef std::reverse_iterator<iterType> RiterType;
      typedef std::array<double, 3> BSret;

      // Routines
      //
      double GR(unsigned endpt, unsigned psize);

      // For RJTwo models:
      //
      double GR(unsigned endpt, unsigned psize, 
		int T, int N1, int N2);

      std::vector<double>
	MixingFraction(unsigned endpt, unsigned psize);
      
      BSret bootstrap(int nconverge, unsigned sampN, double probD);

      // Number of chains
      unsigned @autopersist(nchain);

      // Use this level
      unsigned @autopersist(level);

      // Number of states in ensemble before testing
      unsigned @autopersist(maxit);

      // Number of steps between tests
      unsigned @autopersist(nskip);

      // Confidence interval for two-sided Grubbs test
      double @autopersist(alpha);

      // Maximum offset for accepting outlier in addition to Grubbs
      // test
      double @autopersist(poffset);

      // Maximum number of outliers
      int @autopersist(maxout);

      // For RJTwo models
      // ----------------
      int @autopersist(T);	// # of common parameters
      int @autopersist(N1);	// # of Model 1 parameters
      int @autopersist(N2);	// # of Model 2 parameters
  
      double @ap(MaxR);		// GR statistic threshold

      enum MaskType {grubbs, rosner};
      MaskType @ap(maskType);
				// Mask data
      std::vector<unsigned char> @ap(mask);
				// Print the mask
      void printMask();

				// The ensemble instance
      Ensemble* @ap(_ens);

				// The probability data
      mcmcType @ap(prob);
				// The state data
      mcmcType @ap(data);

      // Converged state
      int @ap(nconverge);	// Index of converged state

    public:

      //+ CLICONSTR Ensemble* int
      //+ CLICONSTR Ensemble* int int
      /** 
      The parameter <code>m</code> determines the "burn-in" handling
      procedure.  If \f$m>0\f$, the most recently computed \f$m\f$
      steps are used to determine convergence.  If \f$m=0\f$, the most
      recent half of the steps are used. In all cases, \f$N_{skip}\f$
      retained steps are required before a convergence test is
      attempted.  Therefore if \f$m>0\f$, the first test will be
      computed after \f$max(m,N_{skip})\f$ steps.  <code>d</code> is
      an Ensemble instance used to accumulate statistics about the
      posterior simulation. <code>id</code> is used to tag diagnostic
      output files

      Note: this is a change from older versions that used \f$m<0\f$
      to request that the first \f$|m|\f$ steps be discarded.  This
      option resulted in nearly the same behavior as \f$m>0\f$ for
      long simulations and and otherwise caused confusion.  \f$m>=0\f$
      is now enforced on construction for compatibility with older
      applications.
      */
      GRanalyze(BIE::Ensemble* d, int Nchain, int Level=0);
      
      //+ CLIMETHOD void setParameters int int int
      //! Set number of states per analysis, analysis interval and max outliers
      void setParameters(int N, int n, int m)
      {
	maxit  = abs(N);
	nskip  = n;
	maxout = m;
      }

      //+ CLIMETHOD void setGrubbs
      //! Set outlier statistic to Grubbs' method (this is the default)
      void setGrubbs() { maskType = grubbs; }

      //+ CLIMETHOD void setRosner
      //! Set outlier statistic to Rosner's algorithm
      void setRosner() { maskType = rosner; }

      //+ CLIMETHOD void setRJdim int int
      //+ CLIMETHOD void setRJdim int int int
      //! For reversible jump models: # of Model 1 parameters and
      //! # of Model 2 parameters and number of common parameters
      void setRJdim(int p1, int p2, int common=0)
      {
	N1 = p1;
	N2 = p2;
	T  = common;
      }

      //+ CLIMETHOD void ReportOff
      void ReportOff() { report = false; }

      //+ CLIMETHOD void ReportOn
      void ReportOn()  { report = true; }

      //+ CLIMETHOD void Quiet
      //! Suppress the diagnostic output
      void Quiet()     { verbose = false; }

      //+ CLIMETHOD void Verbose
      //! Turn on the diagnostic output
      void Verbose()   { verbose = true; }

      //+ CLIMETHOD void setPoffset double
      /**
	 Set the minimum log probability offset from the maximally
	 probably state for an outlier to be flagged in addition
	 Grubbs' test.  In other words, outliers must also have
	 \f$\log(P)<\max\{\log(P)\}+P_{offset}\f$.  This additional
	 requirement may be entirely disabled by setting
	 \f$P_{offset}=0\f$.
      */
      void setPoffset(double z) { poffset = z; }

      //+ CLIMETHOD void setRhatMax double
      //! Set correlation coefficient threshold (default: 1.2)
      void setRhatMax(double r) { MaxR = r; }

      //+ CLIMETHOD void setAlpha double
      //! Set outlier detection confidence: 1 - alpha (default: 0, means off)
      void setAlpha(double a) { alpha = a; }

      //+ CLIMETHOD void Compute
      /** After construction, a typical workflow would be to set
	  parameters using the various member functions such as
	  setParameters(), setRhatMax(), setAlpha(), etc., followed by
	  Compute(). */
      void Compute();

      //+ CLIMETHOD double MarginalLikelihood double int
      //! Estimate marginal likelihood using the trimmed box method
      //! @param probD is the marginal likelihood prob offset target
      //! @param sampN Number of bootstrap samples
      double MarginalLikelihood(double probD, int sampN);

      //+ CLIMETHOD Ensemble* cleanConverged
      //! Return an Ensemble instance containing the cleaned, converged chain
      Ensemble* cleanConverged();
      
      @persistent_end

    };
}

#endif
