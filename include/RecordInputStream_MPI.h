// This is really -*- C++ -*-

#ifndef RecordInputStream_MPI_H
#define RecordInputStream_MPI_H

#include "RecordType.h"
#include "RecordInputStream.h"
#include "TessToolDataStream.h"

namespace BIE {

  //! Make an input filter from an MPI communicated data stream
  class RecordInputStream_MPI : public RecordInputStream 
  {

  public:
    /// Constructor
    RecordInputStream_MPI(RecordType *rt, TessToolDataStream *ttrcvr);

    /// Destrudtor
    ~RecordInputStream_MPI();

    /// Reads next record from file and puts the values in the record buffer.
    bool nextRecord();
    
    /// Returns true if the end of the MPI stream has been reached.
    bool eos();

    /// Returns true if an error has occured while reading the MPI stream
    bool error();

    /// Return the tile id from the stream
    int getTileId();

    /// Lookup the NumRecords field data from the stream
    int getNumRecords();

    /// Lookup field with name Scalar, if absent, return the field after TileId
    double getScalar();

  private:
    int readInt();
    double readReal();
    bool readBool();
    const char* readString();

    /// When true, it indicates we have reached end of file.
    bool rismpi_eof;
    
    /// Indicates there was previously an error when reading the stream.
    bool rismpi_error;  
    bool rismpi_streamismine;

    TessToolDataStream *_ttrcvr;
  };
}
#endif
