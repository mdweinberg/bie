#ifndef KSDistance_h
#define KSDistance_h

#include <vector>

namespace BIE {

  class SampleDistribution;
  
  /**
  * The KS statistic offers a way of comparing two one dimensional 
  * distributions, and gives the probability that two distributions come 
  * from the same underlying distribution.  The method is based on the 
  * maximam distance between the cummulative distribution function of 
  * each distribution.  This implementation is based the code in
  * Numerical Recipes in C : The Art of Scientific Computing; 
  * William H. Press, et al
  *
  * \todo This statistic breaks down where the binning of the distributions
  * is not similar.  Where the binning of distributions is different, 
  * linear interpolation can be used to smooth the CDF of each distribution -
  * this would provide a more sensible result.
  */
  class KSDistance {
  public:
    /// Constructs a KS distance object - values for the constants used 
    /// for convergence testing during calculation of the statistic can
    /// be given explicit values if appropriate.
    KSDistance(int convergelim = 100, double esp1 = 0.001, double esp2 = 1.0e-8)
     { CONVERGELIMIT = convergelim;  EPS1 = esp1; EPS2 = esp2; }
         
    /// Returns the KS distance for a set of of binned distributions.
    /// The binning of the distributions need not be the same, but
    /// the quality of the result will suffer if they are not.
    double ksdistance(vector<SampleDistribution*> distributions);
    
    /// Returns the probability that the two distributions are drawn
    /// from the same underlying distributions.
    double kssignificance(SampleDistribution*, SampleDistribution*);
  private:
    double EPS1, EPS2;
    int CONVERGELIMIT;
  };
}

#endif
