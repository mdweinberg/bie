#ifndef RLINE_h
#define RLINE_h

namespace BIE {

/** Using readline reads and returns it character by character, NULL on EOF. 
*/
  extern char rl_gets();
};

#endif
