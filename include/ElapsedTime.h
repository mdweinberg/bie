// This is really -*- C++ -*-

#ifndef ElapsedTime_h
#define ElapsedTime_h

#include <BIEmpi.h>
#include <stdio.h>

namespace BIE{

  //! An MPI stop watch
  class ElapsedTime 
  {
  public:

    /// The total time so far
    double cummulativeTime;

    /// The start time
    double startTime;

    /// Constructor
    ElapsedTime() 
    {
      cummulativeTime=0;
      startTime=0;
    }
    
    /// Start the timer
    void start()
    {
      startTime = MPI_Wtime();
    }

    /// Stop the timer
    void stop()
    {
      double stopTime =  MPI_Wtime();
      cummulativeTime += (stopTime - startTime);
    }

 }; //end classs

} // end namespace

#endif //ElapsedTime
