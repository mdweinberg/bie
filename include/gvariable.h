// This is really -*- C++ -*-

#ifndef GVARIABLE_H
#define GVARIABLE_H

#include <cc++/thread.h>
#include <string>
#include <fstream>

#include "SymTab.h"
#include "BIEACG.h"

using namespace std;

class SymTab;
class CLICheckpointManager;

namespace BIE {

  /**
   * \defgroup Variables BIE global variables
   */
  /*@{*/

  /**
     Flag various conditions for (e.g.) model debugging
     "showstates" prints the state before likelihood compuation.
     "badcell" prints the state if it gives zero norm.
     "showlos" prints the model and data for one line of sight
  */     
  enum DebugFlags {showstates=1, badcell=2, showlos=4};

  //  typedef _IO_ostream_withassign ostream_with_assign; aah is this used?

  // GLOBAL int exampleint
  // extern int exampleint;

  // GLOBAL double exampledouble 
  // extern double exampledouble;

  // GLOBAL char* examplestring 
  //! An example global variable (used only for testing
  extern char examplestring[];

  //+ GLOBAL bool logfile
  //! Define a default log file
  extern bool logfile;

  //+ GLOBAL bool mstat
  //! Accumulate and display subspace statistics to console, if true
  extern bool mstat;

  //+ GLOBAL string nametag
  //! The identifying label for the current simulation
  extern string nametag;

  //+ GLOBAL string outfile
  //! Used as an output file or suffix for diagnostic output
  extern string outfile;

  //+ GLOBAL string homedir
  //! The user's home directory, defined on initialization
  extern string homedir;

  //+ GLOBAL string cntrlfile
  //! Read by a running simulation to control the run state (deprecated)
  extern string cntrlfile;

  //+ GLOBAL string histfile
  //! CLI readline history file
  extern string histfile;

  //+ GLOBAL bool mpi_used
  //! Defined by the system to indicate that MPI is used on invocation
  extern bool mpi_used;

  //+ GLOBAL int debugflags
  //! Bit flag to turn on verbose reporting (see enum DebugFlags)
  extern int debugflags;
 
  //+ GLOBAL bool socket_used
  //! Used by TessTool to indicate control will be to and from a network socket
  extern bool socket_used;

  //+ GLOBAL bool gui_used
  //! Used to indicate that a GUI should be launched
  extern bool gui_used;

  //+ GLOBAL int simulationInProgress
  //! Indicates to monitor threads that a simulation is running
  extern int simulationInProgress;

  //! Used by the parser (lexer)
  extern int firstCommandChar;

  //! Used by the LikelihoodComputation to keep simulation sychronous
  extern int suspendedSimulation;
  
  //! Used by the LikelihoodComputation to keep simulation sychronous
  extern ost::Semaphore *switchCLISem;

  //! Used by the LikelihoodComputation to keep simulation sychronous
  extern ost::Semaphore *likelihoodSem;

  //! Pointer to random number generator
  extern BIEACG *BIEgen;

  //! Used by CLI to keep track of TessTool usage
  extern bool ttool;

  //! Used by CLI to manipulate input buffer
  extern char ch_inbuf[80];

  //! Used by CLI to manipulate output buffer
  extern char ch_logbuf[80];

  //! Used by CLI to manipulate console buffer
  extern char ch_conbuf[80];

  //! Current CLI input stream
  extern ifstream inFile;

  //! Current CLI log stream
  extern ofstream logFile;

  //! Current CLI output stream
  extern ofstream clioutFile;

  //! Used by CLI reassign the standard input
  extern std::streambuf *inbuf;

  //! Used by CLI reassign the standard output
  extern std::streambuf *outbuf;

  //! Used by CLI reassign the standard error
  extern std::streambuf *errbuf;

  //! CLI-saved initial standard output
  extern ostream* saved_cout;

  //! CLI-saved initial standard error
  extern ostream* saved_cerr;

  //! CLI-saved initial standard input
  extern istream* saved_cin;

  //! The current console file (if file output is desired)
  extern ofstream consoleFile;

  /** Output to slave processes is routed here (if MPI is used and
      slave output is desired) */
  extern ofstream slaveFile;

  //! Will contain the CLI propmt string
  extern char prompt[];

  //+ GLOBAL string helpdir
  //! Location of the cli help files
  extern string helpdir;
 
  //+ GLOBAL string visualizedir
  //! Location of the visualization utility
  extern string visualizedir;
 
  //+ GLOBAL string clidir
  //! Used to locate the help files
  extern string clidir;
  
  //+ GLOBAL string histfile
  //! Name of CLI readline history cache
  extern string histfile;
  
  //+ GLOBAL bool restrictcli
  //! Used by GlobalArray and MethodTable
  extern bool restrictcli;
  
  //+ GLOBAL bool restart
  //! Simulation is restarting (if true)
  extern bool restart;
  
  //+ GLOBAL bool printaddressesincli
  //! Used during regression testing to avoid mismatches in addresses.
  extern bool printaddressesincli;
  
  //! Contains info about all the methods
  extern string methodstring;

  //! Current unused (remove?)
  extern ofstream test_o;

  //! True if cli is actively accepting user input
  extern bool interactive;

  //! For evaluation timing
  extern int likelihood_count;

  //+ GLOBAL bool sample_discrete
  /** Change default sampling in EnsembleStat.  This would be done,
      ideally, with a static method but static methods are not supported
      in the cli
  */
  extern bool sample_discrete;

  //+ GLOBAL int current_level
  /** The value of the current level for use by user
      Set by RunOneSimulation, RunMultilevelSimulation and 
      by LikelihoodComputation
  */
  extern int current_level;

  //+ GLOBAL int savehist
  //! Size of CLI readline history cache
  extern int savehist;

  /// For tesstool
  //@{
  //! Read buffer error stream
  extern ofstream rferr;
  //! Write buffer error stream
  extern ofstream wrerr;
  //@}
  
  //+ GLOBAL double beta0
  //! The default base 1/kT value
  extern double beta0;

  //+ GLOBAL string persistType
  //! The default boost archive type
  extern string persistType;

  /*@}*/
}

// Global pointer to symbol table
extern SymTab *st;

// Global pointer to Checkpoint Manager
extern CLICheckpointManager *ckm;
#endif

