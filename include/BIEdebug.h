// -*- C++ -*-



#ifndef _BIEdebug_h
#define _BIEdebug_h

#include <ostream>

using namespace std;

namespace BIE {

  class Frontier;
  
  //@{
  //! Helper functions to print info about the current tessellation frontier
  void printFrontierTiles(Frontier * frontier);
  void printFrontierTiles(Frontier * frontier, ostream& out);
  void printFrontierTilesSM(Frontier * frontier);
  //@}

}

#endif
