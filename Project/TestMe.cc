#include "TestMe.h"
#include "BIEmpi.h"

BOOST_CLASS_EXPORT_IMPLEMENT(BIE::TestMe)

void TestMe::printMe()
{
  if (myid==0) {
    std::cout << std::setw(30) << sdata << " : " << idata << std::endl;
  }
}
